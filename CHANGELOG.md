# Change Log

## maapi.net v9.11.0 (2024-04-25) (MSDB 7.1.1)

### Details v9.11.0

1. Compatibility with Oracle 19c when importing item schemes. Fix a store procedure that used the JSON data type that is available from Oracle 21c.
1. Mapping Sets with Transcodings could not be removed
1. Mapping Store Import/Export fixes

### Tickets v9.11.0

The following bugs have been corrected:

- SDMXRI-2397: SDMXRI minor issues found in v9.10.0-(2024-01-04)
- SDMXRI-2394: MAWEB NET: ORACLE 19c

## maapi.net v9.10.0 (2024-03-27) (MSDB 7.1)

### Details v9.10.0

1. Use a hybrid more performant way to import codelists and other item schemes.The item names, description and annotations are now stored as JSON. This change allows bulk insert of items.
1. Support storing/retrieving SDMX 3.0.0 metadata attribute usage
1. Many bug fixes
   1. Caching resulted to incorrect references when the imported file contained artefacts with the same id.

### Tickets v9.10.0

The following new features added:

- SDMXRI-2286: SDMX RI implement import codelists performance improvements
- SDMXRI-2197: SDMXRI support storing/retrieving SDMX 3.0.0 DSD Metadata attribute usage info (.net) (DataStructure,SDMX3.0)

The following bugs have been corrected:

- SDMXRI-2356: SDMXRI minor issues found in v9.9.0-(2024-02-09)

## maapi.net v9.9.0 (2024-02-09) (MSDB 7.0.5)

### Tickets v9.9.0

The following new features added:

- SDMXRI-2355: Update vulnerable 3rd party dependencies
- SDMXRI-2255: Update MariaDB version support to LTS 10.11
- SDMXRI-2288: SDMXRI WS performance improvements (SDMXRI)
- SDMXRI-2330: MAWEB and Windows authentication
- SDMXRI-2260: Deadlock & locking management - internal structure info reads and writes (StructureDb) (OECD)

The following bugs have been corrected:

- SDMXRI-2319: SDMXRI minor issues found in v9.8.0-alpha (2023-12-14)
- SDMXRI-2341: Maweb: DomainUserRole
- SDMXRI-2329: MAWEB: db connection wizzard and advanced parameter
- SDMXRI-2312: MAWEB .NET&Java- Import fails when importing content constraint file
- SDMXRI-2304: .NET - Registry Endpoint cannot be edited
- SDMXRI-2325: CLONE - MAWEBApp- New NsiWs type REST endpoint cannot be saved, in Oracle Mapping Store db-.NET
- SDMXRI-2294: ISTAT : Dataset creation > update in DB

## maapi.net v9.5.0-alpha (2023-09-xx)  (MSDB 7.0)

- Include changes from 8.18.1 listed below

## maapi.net v8.18.1 (2023-08-17) (MSDB 6.20)

### Details v8.18.1

1. Reverted request for HTTP 304 to be returned as it doesn't allow a body

### Tickets v8.18.1

The following new features were reverted:

- SDMXRI-1869: Instead of HTTP 422 return HTTP 304 for non-updated artefacts where no differences were found (OECD)

## maapi.net v8.18.0 (2023-08-01) (MSDB 6.20) (AUTHDB v1.0)

### Details v8.18.0

1. Use [central package management](https://learn.microsoft.com/en-us/nuget/consume-packages/central-package-management), versions of dependencies from all projects are stored in `Directory.Packages.props`.
1. Use HTTP Code 304 in case there were no differences when attempting to update an artefact
1. HCL improvements
1. Optional validation of the `EMBARGO_TIME` DSD attribute when present

### Tickets v8.18.0

The following new features added:

- SDMXRI-2161: Publish pre-release nuget packages to nuget.org (OECD)
- SDMXRI-1869: Instead of HTTP 422 return HTTP 304 for non-updated artefacts where no differences were found (OECD)
- SDMXRI-2187: The codes of a HCL should be returned in the order of their creation (OECD,PULL_REQUEST)
- SDMXRI-2180: Restricted access to confidential or embargoed data (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1968: "Procedure or function 'INSERT_HCL_CODE' expects parameter '@p_lcd_id', which was not supplied." error when submitting HCL

## maapi.net-develop v8.17.0 (2023-06-07)

### Details v8.17.0

1. When updatedAfter filter is used and the replace and delete datasets are queried the dimension filter should be extended with is null statement to return higher level attributes.
1. Generated warning for duplicate concepts or codes in Cube Regions of Content Constraints

### Tickets v8.17.0

The following new features added:

- SDMXRI-2147: UpdatedAfter data queries should return higher level attributes when filtering is applied  (OECD,PULL_REQUEST)
- SDMXRI-2028: Expecting error when uploading content constraint with duplicated codes in a repeated concept

## maapi.net v8.16.0 (2023-05-04) (MSDB 6.20)

### Details v8.16.0

1. Merged changes from OECD related to:
   1. Faster import Content Constraints
   1. Make `updatedAfter` work with content constrain filtering at attributes
1. Fix a bug where allowed content constraints were returned as actual when requested as stubs

### Tickets v8.16.0

The following new features added:

- SDMXRI-2147: UpdatedAfter data queries should return higher level attributes when filtering is applied  (OECD,PULL_REQUEST)
- SDMXRI-2137: Improve performance of function to save the actual content constraint (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-2046: An allowed content constraint is returned as "Actual" when retrieving as stub


## maapi.net v9.4.0-alpha (2023-07-13)  (MSDB 7.0)

### Details v9.4.0-alpha

1. Support SDMX 3.0.0 MeasureRelationship
1. bugfixes
   1. Could not save Mapping Set for final Dataflow on databases where empty value is different than `null` (Mariadb and SQL Server)
   1. fix generating sql for structure version requests; improve handling of multiple versions and wildcards
   1. Queries run outside transaction

### Tickets v9.4.0-alpha

The following new features added:

- SDMXRI-2173: MeasureRelationship support in SDMXRI (.NET) (DataStructure,SDMX3.0)

The following bugs have been corrected:

- SDMXRI-2176: MAWEB.NET : save a mapping set under SQL SERVER MS
- SDMXRI-2174: SDMXRI minor issues found in v9.3.0-alpha (2023-06-

## maapi.net v9.3.0-alpha (2023-06-16) (MSDB 7.0)

### Details v9.3.0-alpha

1. Return warning when duplicate codes found in CubeRegion
1. Many bug fixes

### Tickets v9.3.0-alpha

The following new features added:

- SDMXRI-1901: SdmxSource support SDMX 2.0.0 JSON structure format writer (.net) (SDMX3.0)

The following bugs have been corrected:

- SDMXRI-2162:  SDMXRI minor issues found in v9.2.0-alpha.2 (2023-05-18)
- SDMXRI-2028: Expecting error when uploading content constraint with duplicated codes in a repeated concept
- SDMXRI-2158: Dataset and DDB Connection issues in MAWEB.NET 9.2.0-alpha

## maapi.net v9.2.0-alpha (2023-05-19)

### Details v9.2.0-alpha

1. Specific changes from 8.16.0 merged
1. Complete feature for transforming between transcoding rules and content constraints
1. Many bugfixes

### Tickets v9.2.0-alpha

The following new features added:

- SDMXRI-2055: Merge changes from 8.15.1 to 9.0.0-alpha in SDMXRI (Part2)
- SDMXRI-2137: Improve performance of function to save the actual content constraint (OECD,PULL_REQUEST)
- SDMXRI-1977: Contraints from transcoding and vice versa (Remaining work from SDMXRI-1840) (SDMX3.0)

The following bugs have been corrected:

- SDMXRI-2046: An allowed content constraint is returned as "Actual" when retrieving as stub
- SDMXRI-2030: SDMXRI minor issues found in v9.0.0-alpha.2 (2022-11-03))

## maapi.net v9.1.0-alpha (2023-04-07) (MSDB 7.0-alpha)

### Details v9.1.0-alpha

1. Includes changes from 8.15.1 release
1. Increased SDMX 3.0.0 and SDMX REST 2.0.0 compliance
1. More existing features made to work with the wildcard mechanism in MADB7
1. Note the 7.0 MSDB has been updated, so in order to test the MSDB needs to be initialized again or upgrade from a version before 7.0

### Tickets v9.1.0-alpha

The following new features added:

- SDMXRI-2055: Merge changes from 8.15.1 to 9.0.0-alpha in SDMXRI (Part2)
- SDMXRI-1840: SDMX 3.0.0 - MADB 7/MAWS migration Retrieve/Persist Entities (2021.0333-QTM4,Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1839: SDMX 3.0.0 - MADB 7/MAWS migration - Retrieve/Persist SDMX artefacts (2021.0333-QTM4,Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1925: Available Constraint (SDMX 2.1) / availability 3.0 / special partial requests (SOAP 2.0) (SDMX3.0)
- SDMXRI-1975: Dataflow Upgrade work with wildcard/MADB7 (2022.0146–QTM3,SDMX3.0)
- SDMXRI-1908: SDMXRI SDMX 3.0 Support for DSD 3.0 Array mapping (SDMX3.0)
- SDMXRI-2044: Merge changes from 8.15.1 to 9.0.0-alpha in SDMXRI (2022.0146–QTM3)
- SDMXRI-2043: Additional testing for Public release Mapping Assistant 8.x
- SDMXRI-1910: SDMXRI SDMX 3.0 Support for DSD 3.0 Measures (2022.0146–QTM3,SDMX3.0)
- SDMXRI-2048: SDMX 3.0 Support for importing multiple DSD 3.0 Measures (SDMX3.0)
- SDMXRI-1909: MAWEB SDMX 3.0 Support for DSD 3.0 Measures (backend) (2022.0146–QTM3,SDMX3.0)
- SDMXRI-1907: MAWS SDMX 3.0 Support for DSD 3.0 Array mapping (MAWS Backend) (SDMX3.0)
- SDMXRI-1920: SDMXRI SDMX 3.0.0  referencepartial detail in Structure requests (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1923: SDMXRI Data Registration with SDM 3.0.0 semantic/wildcard mechanism (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1990: Update 3rd party dependencies for the public release (.NET) (2022.0146–QTM1)
- SDMXRI-1966: RI WS, performance issue (2022.0146–QTM1)
- SDMXRI-1866: MariaDB 10.6.* not compatible with MAWEB (2022.0146–QTM1)
- SDMXRI-1919: SDMX 3.0.0 Update non-final DSD (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1922: Feature flag auto delete mapping sets/datasets with SDMX 3.0.0 wildcards/versioning (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1918: SDMX 3.0.0 Update Category Scheme (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1921: SDMX 3.0.0 Update non-final Artefacts (Package_1_SDMX3.0,SDMX3.0)

The following bugs have been corrected:

- SDMXRI-1989: Issue when adding new mapping store
- SDMXRI-2030: SDMXRI minor issues found in v9.0.0-alpha.2 (2022-11-03))
- SDMXRI-2041: MAWEB GUI: Oracle connectionstring list
- SDMXRI-1967: MAWEB: User management error

## maapi.net v8.15.1 (2023-02-22) (MSDB 6.20)

### Details v8.15.1

1. Improve DB name and server handling with new MariaDB/MySQL driver
1. Use username as DB Name in Oracle

### Tickets v8.15.1

The following bugs have been corrected:

- SDMXRI-2041: MAWEB GUI: Oracle connectionstring list

## maapi.net v8.15.0 (2023-01-26)

### Details v8.15.0

1. For the SDMX REST `updatedAfter` feature; in addition to existing  support different SQL Queries per dataset action. It requires a Dataset with `EditorType` equal to `MultiQuery` and a JSON message containing the SQL Queries. (Experimental)

### Tickets v8.15.0

The following new features added:

- SDMXRI-2027: Update MappingSet/Dataset to allow storing different SQL queries for different dataset actions (Updated,Deleted,Inserted,Active)  (OECD,PULL_REQUEST)

## maapi.net v8.14.0 (2022-12-21) (MSDB 6.20)

Target Mapping Store v6.20.1 which includes the fix for MariaDB 10.6 `OFFSET` keyword.

## maapi.net v8.13.0 (2022-11-25) (MSDB 6.20)

### Details v8.13.0

1. Target Mapping Store database 6.20
1. Update 3rd party dependencies, includes vulnerability fixes and updated DB drivers
1. Improve complete stub support via REST
1. Fix issues related to User managements where Mapping Store is on Oracle

### Tickets v8.13.0

The following new features added:

- SDMXRI-1966: RI WS, performance issue
- SDMXRI-1990: Update 3rd party dependencies for the public release (.NET)
- SDMXRI-1866: MariaDB 10.6.* not compatible with MAWEB

The following bugs have been corrected:

- SDMXRI-1947: detail=allcompletestubs parameter doesn't return all related artefact's annotations
- SDMXRI-1989: Issue when adding new mapping store
- SDMXRI-1967: MAWEB: User management error

## maapi.net v9.0.0-alpha.2 (2022-11-03) (MSDB 7.0.0-alpha2)

### Known issues (9.0.0-alpha 2)

1. Updating category schemes doesn't work
1. Data availability doesn't always work
1. reference partial is not working

### Details v9.0.0-alpha.2

1. Re-implement update of non-final Artefacts and DSD functionality in MADB7
   1. Category Scheme updating will be included in the next release
1. Many fixes

### Tickets v9.0.0-alpha.2

The following new features added:

- SDMXRI-1840: SDMX 3.0.0 - MADB 7/MAWS migration Retrieve/Persist Entities (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1839: SDMX 3.0.0 - MADB 7/MAWS migration - Retrieve/Persist SDMX artefacts (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1919: SDMX 3.0.0 Update non-final DSD (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1921: SDMX 3.0.0 Update non-final Artefacts (Package_1_SDMX3.0,SDMX3.0)

## maapi.net v9.0.0-alpha (2022-09-30) (MSDB 7.0.0-alpha)

### Known issues

1. Updating artefacts doesn't work
1. Importing Content constraints will fail
1. Data availability doesn't always work
1. reference partial is not working

### Details v9.0.0-alpha

1. Alpha release that targets the MSDB 7.0
1. Major changes in Transcoding entities especially Time Transcoding

### Tickets v9.0.0-alpha

The following new features added:

- SDMXRI-1840: SDMX 3.0.0 - MADB 7/MAWS migration Retrieve/Persist Entities (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1839: SDMX 3.0.0 - MADB 7/MAWS migration - Retrieve/Persist SDMX artefacts (Package_1_SDMX3.0,SDMX3.0)
- SDMXRI-1866: MariaDB 10.6.* not compatible with MAWEB

## maapi.net v8.12.2 (2022-09-27) (MSDB 6.19)

### Details v8.12.2

1. Bug fix when recording actions without an authenticated user

### Tickets v8.12.2

The following bugs have been corrected:

- SDMXRI-1943: Fix UserAction management when Thread.CurrentPrincipal.Identity.Name is empty

## maapi.net v8.12.1 (2022-08-15)

### Details v8.12.1

1. Improve support for `isMultiLingual` in SDMX 2.1 DSD

### Tickets v8.12.1

The following bugs have been corrected:

- SDMXRI-1835: Wrong behaviour of 'isMultiLingual' property

## maapi.net v8.12.0 (2022-07-08) (MSDB 6.19)

### Details v8.12.0

1. Produce an error/warning when importing a StructureSet with ConceptSchemeMap
   1. An error when the StructureSet has no CodelistMap/StructureMap
   1. A warning when the StructureSet has also CodelistMap/StructureMap

### Tickets v8.12.0

The following new features added:

- SDMXRI-1495: Provide clear error message "Not implemented" when creating StructureSet with ConceptSchemeMap (OECD)

## maapi.net v8.11.1 (2022-06-17) (MSDB 6.19)

### Details v8.11.1

1. Disable encryption of connection strings on Linux since DPAPI is not supported by .NET on Linux.
1. Use updated DB initialization for MariaDB 6.19 to work with Mariadb 10.6
1. Async calls fixes

### Tickets v8.11.1

The following new features added:

- SDMXRI-1866: MariaDB 10.6.* not compatible with MAWEB
- SDMXRI-1855: MAWEB .NET support running under Linux

The following bugs have been corrected:

- SDMXRI-1872: "Operations that change non-concurrent collections must have exclusive access" error in NSI WS when getting data

## Breaking change

All command line tools now target .NET 6

## maapi.net v8.11.0 (2022-04-14) (MSDB 6.19)

### Details v8.11.0

1. Tests and command line tools now target .NET 6
1. Improve performance when deleting an hierarchical codelist
1. Fix issue which prevented the update of content constraints if validity was set
1. Fix regressions related to mapping set auto-deletion

### Tickets v8.11.0

The following new features added:

- SDMXRI-1775: SDMX RI .NET migrate to .NET 6 (future-QTM)
- SDMXRI-1829: Improve performance of hierarchical code list deletion (OECD)

The following bugs have been corrected:

- SDMXRI-1846: ContentConstraintImportEngine prevents update of content constraints when StartDate and EndDate attributes are set
- SDMXRI-1842: DryIoc Exception in maapi.net tool v8.9.2
- SDMXRI-1834: Error while trying to delete a dataflow with mappingsets

## maapi.net v8.9.2 (2022-02-25)(MSDB 6.19)

### Details v8.9.2

1. Delete also Datasets when cascade deleting Mapping Sets for deleted Dataflows
*Note* this applies only when deleting Dataflows and cascade delete Mapping Sets is enabled.

### Tickets v8.9.2

The following bugs have been corrected:

- SDMXRI-1771: Leftover mappingset Db related records after DataFlow deletion

## maapi.net v8.9.1 (2022-02-03) (MSDB 6.19)

### Details v8.9.1

1. Metadata support in Datasets
1. Improve performance in firstNObservations data queries

### Tickets v8.9.1

The following new features added:

- SDMXRI-1795: Data retriever metadata support (OECD,PULL_REQUEST)
- SDMXRI-1758: Improve the performance of firstNObserverions queries and fix the result set when a range is requested. (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1808: Error at dataflow retrieval with detail=referencepartial parameter when related category scheme is partial

## maapi.net v8.9.0 (2021-12-17) (MSDB 6.18)

### Details v8.9.0

1. Support mapping of metadata attributes

### Tickets v8.9.0

The following new features added:

- SDMXRI-1772: Support mapping of metadata similar to existing mapping of data (OECD,PULL_REQUEST)

## maapi.net v8.8.0 (2021-11-05)

### Details v8.8.0

1. Test Client functionality in MA WEB; fix issues related to nsiws endpoint entity
1. Fix wrong value for structureURL in codelist & other artefacts

### Tickets v8.8.0

The following new features added:

- SDMXRI-1564: integrate the Test Client inside the MA WEB (future-QTM)

The following bugs have been corrected:

- SDMXRI-1748: Wrong value for structureURL in codelist

## maapi.net v8.7.0 (2021-09-23)(MSDB v6.17)

### Details v8.7.0

1. Introduce the changes under SDMX 3.0 REST API
1. IREF autonumber is now taken from a sequence in the database


### Tickets v8.7.0

The following new features added:

- SDMXRI-1750: Allow retrieving data with constrained but non-provided optional attributes (OECD,PULL_REQUEST)
- SDMXRI-1565: SDMX 3.0 REST WS implementation (future-QTM)
- SDMXRI-1561: IREF autonumber (future-QTM)

## maapi.net v8.6.0 (2021-09-02)

### Details v8.6.0

1. Return Sql queries generated for data
1. User actions can only be seen from a user who is admin

### Tickets v8.6.0

The following new features added:

- SDMXRI-1564: integrate the Test Client inside the MA WEB (future-QTM)

The following bugs have been corrected:

- SDMXRI-1738: SDMXRI minor issues found in v8.5.0 (2021-08-05)

## maapi.net v8.5.0 (2021-08-12)(MSDB v6.16)

### Details v8.5.0

1. Created new entity type, `nsiws`, in order to store and retrieve web service urls
1. Fix NPE in mysql servernames
1. Fixed a bug where Mapping Sets that were created with previous versions of Mapping Assistant were not listed
1. Fixed a bug where getting user actions was throwing an exception
1. Fixed a bug where updating a non-final codelist returned an incomplete status message
1. Implemented structure part for SDMX 3.0 REST WS
    1. Support for multiple values with comma as separator
    2. Support for wildcards

### Tickets v8.5.0

The following new features added:

- SDMXRI-1564: integrate the Test Client inside the MA WEB (future-QTM)
- SDMXRI-1730: When mapping contains pre-generated period_start and period_end columns for TIME, generated SQL query uses incorrect parameters from passed REST query (startPeriod, endPeriod)	
- SDMXRI-1565: SDMX 3.0 REST WS implementation (future-QTM)

The following bugs have been corrected:

- SDMXRI-1717: SDMXRI minor issues found in v8.4.0 (2021-07-08)
- SDMXRI-1737: list Mapping Sets created with old MA
- SDMXRI-1591: fix retrieving user action entities
- SDMXRI-1676: Improve successful message when updating a non-final codelist
- SDMXRI-1496: deleting local codes of dataset column leaves orphans records
- SDMXRI-1691: Unable to upload non-final MSD

## Merged from Public release 8.0.10 (2021-07-29)

### Details 8.0.10

1. Proxy settings could not be saved in Oracle in some cases
1. Local codes were not deleted when transcoding rules were deleted

### Tickets v8.0.10

The following bugs have been corrected:

- SDMXRI-1496: MAWS.NET deleting local codes of dataset column leaves orphans records
- SDMXRI-1692: SDMXRI minor issues found in v8.0.9 (2021-06-17) and 8.3.0

## maapi.net v8.4.1 (2021-07-15) (MSDB v6.15)

### Details v8.4.1

1. Updated the msdb.sql package version (6.15.1) to correctly label MSDB 6.15 version.

### Tickets v8.4.1

The following bugs have been corrected:

- SDMXRI-1716: MAWS.NET 8.4.0 initializes msdb to 6.14 (ISTAT)

## maapi.net v8.4.0 (2021-07-08) (MSDB v6.15)

### Details v8.4.0

1. Introduce SQL pagination for data requests containing the header parameter "Range"
1. use authorization scope only for the calls that require it
1. Fix put/get proxy settings on Oracle MSDB

### Tickets v8.4.0

The following new features added:

- SDMXRI-1693: Introduce SQL pagination for range requests. (OECD)
- SDMXRI-1340: SDMXRI use AuthManagement web service (OECD,QTM9-2019.0281)

The following bugs have been corrected:

- SDMXRI-1692: SDMXRI minor issues found in v8.0.9 (2021-06-17) and 8.3.0

## maapi.net v8.3.1 (2021-07-01) (MSDB v6.14)

### Details v8.3.1

1. Authorization fixes. Minimize the number of checks

### Tickets v8.3.1

The following new features added:

- SDMXRI-1340: SDMXRI use AuthManagement web service (OECD,QTM9-2019.0281)

## maapi.net v8.3.0 (2021-06-22) (MSDB v6.14)

### Details v8.3.0

1. Support reading DDB settings from a config file. Only one DDB connection string is supported right now.
1. Finish MAWS migration to use new authorization system.
1. Support for pre-formatted time dimension mapping
1. Use time range in generated and available content constraints

### Tickets v8.3.0

The following new features added:

- SDMXRI-1673: NSI WS should take data database connection parameters from configuration (OECD,PULL_REQUEST)
- SDMXRI-1340: SDMXRI use AuthManagement web service (OECD,QTM9-2019.0281)
- SDMXRI-1538: Enrich existing data retriever to take advantage of a mapped query with Time range columns (future-QTM)

The following bugs have been corrected:

- SDMXRI-1679: SDMXRI minor issues found in v8.0.8 (2021-05-28) and 8.2.0
- SDMXRI-1527: For "available" ContentConstraint replace ReferencePeriod by CubeRegion-TimeRange

## maapi.net v8.2.0 (2021-05-28) (MSDB v6.13)

### Details v8.2.0

1. Should not be able to delete a dataset if it used by a mapping set
1. Asynchronous data retrieval
1. Fixes in dataset inner join, union and cross join
1. Mapping Set valid to/from where culture sensitive
1. Improve error message when a content constraint is submitted for a non-existent dataflow

### Tickets v8.2.0

The following new features added:

- SDMXRI-1638: MAWEB : delete dataset linked to a mapping set (PUBLIC_2020,future-QTM)
- SDMXRI-1622: Make rest data retreival asynchronous  (OECD,future-QTM)
- SDMXRI-1616: SRI.NET integrate changes from public / fix issues in develop branch (OECD,future-QTM)

The following bugs have been corrected:

- SDMXRI-1649: MAWEB JAVA and NET : dataset menu inner join is not working
- SDMXRI-1650: NSI WS NET: dataset menu, UNION doesn't work
- SDMXRI-1651: MAWEB NET: dataset menu, Cross JOIN function doesn't work
- SDMXRI-1535: Content constraint structure message validation
- SDMXRI-1623: PIT issues

## maapi.net v8.1.3 (2021-04-28) (MSDB v6.13)

### Details v8.1.3

1. Allow setting the header dataset action in the configuration file.
1. Integrating the changes from the public release branches back to develop and Fixing business cases & tests that broke in develop branch.
1. Added Regex validation of Party/Id.
1. Improve error/status message for all structure updates
1. Automatic deletion of mapping sets belonging to the dataflow when the dataflow is deleted
1. Fixed a bug where a content constraint that was not compliant with SDMX standards was accepted when submitted.

### Tickets v8.1.3

The following new features added:

- SDMXRI-1507: NSIWS Retrieve annotations for artefacts stored as stubs in MSDB (ISTAT,QTM9-2019.0281)
- SDMXRI-1432: Make StructureUsage/DataSetID/DataSetAction configurable per dataflow (.NET) (QTM10-2019.0281)
- SDMXRI-1616: SRI.NET integrate changes from public / fix issues in develop branch (OECD)
- SDMXRI-1582: Regex validation of Party/Id (OECD,PULL_REQUEST)
- SDMXRI-1594: Improve error/status message for all structure updates (OECD,PULL_REQUEST)
- SDMXRI-1536: Automatic deletion of mapping sets belonging to the dataflow when the dataflow is deleted (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1535: Content constraint structure message validation
- SDMXRI-1623: PIT issues
- SDMXRI-1537: Not possible to update parent relations in a non-final codelist used in a DSD
- SDMXRI-1574: Possible bug related category retrieval

## maapi.net v8.1.2 (2021-01-08) (MSDB v6.12)

### Details v8.1.2

1. Merge changes from v8.0.3
1. Point in time fixes from OECD
1. Authentication/Authorization fixes
1. Mapping Set ignored patch particle in dataflow versions
1. Workaround deadlocks in SQL Server for handling multiple data requests

### Tickets v8.1.2

The following bugs have been corrected:

- SDMXRI-1539: MappingSetEntityRetriever retrieves entities of version 1.0.0 when version 1.0 is requested
- SDMXRI-1515: Fix select statements having reserved SQL words
- SDMXRI-1516: Extend the length of MAPPING_SET table's ID column
- SDMXRI-1418: Foreign Key constraint violation exception when creating SDMX artefacts
- SDMXRI-1445: Allow referencing non-final codelists in a non-final hierarchical codelist
- SDMXRI-1442: Issues related to the management of Metadata Structure Definition
- SDMXRI-1458: In TextFormatTypesQueryEngine class minValue and maxValue facets of TextFormat are not saved to database

## maapi.net v8.1.1 (2020-11-25) (MSDB v6.12)

### Details v8.1.1

1. Target MSDB v6.12
1. Further work for Point in Time from OECD
1. Support for updating non-final that is referenced by other artefacts
   1. Item Schemes
   1. Dataflows
   1. Data Structures
      1. Cross Sectional attributes are not fully supported
   1. Items/Components that are referenced by other artefacts are not removed
1. Fix Annotation update with multiple text entries
1. Fix issue that generated errors when querying actualconstraints and allowedconstraints with version.
1. Fix internal server error when trying to delete or replace a codelist that is referenced by a concept scheme.
1. Counting observations in some cases would ignore startPeriod and endPeriod in available content constraint queries
1. MinValue and MaxValue are now saved into the MSDB
1. Fixed issues updating final/non-final MSD

### Tickets v8.1.1

The following new features added:

- SDMXRI-1336: Point in Time support (OECD,PULL_REQUEST,QTM10-2019.0281)
- SDMXRI-1277: Support updating referenced non-final item scheme artefacts (.NET) (OECD,PUBLIC_2020,QTM6-2019.0281)
- SDMXRI-1340: SDMXRI use AuthManagement web service (OECD)

The following bugs have been corrected:

- SDMXRI-1516: Extend the length of MAPPING_SET table's ID column
- SDMXRI-1515: Fix select statements having reserved SQL words
- SDMXRI-1418: Foreign Key constraint violation exception when creating SDMX artefacts
- SDMXRI-1445: Allow referencing non-final codelists in a non-final hierarchical codelist
- SDMXRI-1442: Issues related to the management of Metadata Structure Definition
- SDMXRI-1458: In TextFormatTypesQueryEngine class minValue and maxValue facets of TextFormat are not saved to database

## maapi.net v8.1.0 (2020-10-16) (MSDB v6.11)

### Details v8.1.0

1. Target MSDB v6.11
1. A Dataflow can now have multiple Mapping Sets.
   1. One Mapping Set can be active.
   1. Mapping sets have validity dates.
1. Fixed updating final MSD
1. Allow a non-final MSD to reference a non-final concept scheme.
1. Fix saving minValue and maxValue facets when saving a DSD

### Tickets v8.1.0

The following new features added:

- SDMXRI-1340: SDMXRI use AuthManagement web service (OECD)
- SDMXRI-1336: Point in Time support (OECD)

The following bugs have been corrected:

- SDMXRI-1442: Issues related to the management of Metadata Structure Definition (OECD)
- SDMXRI-1458: In TextFormatTypesQueryEngine class minValue and maxValue facets of TextFormat are not saved to database (OECD)

## maapi.net v8.0.6 (2021-04-09)  (MSDB v6.10)

### Details v8.0.6

1. Check if target and source dataflow exist before starting to copy/upgrade
1. Non-Time Dimension components with format date time mapped to DateTime columns would produce non ISO 8601 date time
1. Fixed issue not being update to update parent code in a non-final codelist in a DSD

### Tickets v8.0.6

The following new features added:

- SDMXRI-1619: Copy mapping set between dataflow (future-QTM)

The following bugs have been corrected:

- SDMXRI-1621: SDMXRI minor issues found in v8.0.5 (2021-03-18)
- SDMXRI-1537: Not possible to update parent relations in a non-final codelist used in a DSD

## maapi.net v8.0.5 (2021-03-12) (MSDB v6.10)

### Details v8.0.5

1. Fix issue with empty/null local codes showing in transcoding
1. Add new tests for storing and retrieving `isPresentational` value
1. Backport a bugfix from OECD that retrieves mapping sets of dataflow version 1.0.0 when version 1.0 is requested

### Tickets v8.0.5

The following bugs have been corrected:

- SDMXRI-1542: SDMXRI minor issues found in v8.0.3 (2020-12-17)
- SDMXRI-1539: MappingSetEntityRetriever retrieves entities of version 1.0.0 when version 1.0 is requested

Create new tests for

- SDMXRI-1116: Metadata Structure - "isPresentational" field no saved

## maapi.net v8.0.4 (2021-01-28) (MSDB v6.10)

### Details v8.0.4

1. Use different config file when storing AuthDB connection settings
1. Support generating allowed Content Constraints from Transcoding rules
1. Generate JDBC URL for DDB so it works with Java

### Tickets v8.0.4

The following new features added:

- SDMXRI-1546: MAWS.NET: authDB connection string in separate configuration file (.net) (PUBLIC_2020,QTM10-2019.0281)
- SDMXRI-1530: Content Constraints from Transcoding Rules  (ISTAT,QTM9-2019.0281)
- SDMXRI-1552: MAWS.NET Generate JDBC URL for DDB (FUTURE-QTM)

## maapi.net v8.0.3 (2020-12-17) (MSDB v6.10)

### Details v8.0.3

1. Support for configurable AUTHDB connection string
1. Updating dataflow annotations returned a warning
1. ContentConstraints were not correctly generated from transcoding if there was a mis-configuration in Frequency/Time Dimension transcodings.
1. Temporary allow only request to make SDMX artefact changes to workaround Mapping Store schema issues.
1. Many bugfixes

### Tickets v8.0.3

The following new features added:

- SDMXRI-1509: MAWS.NET: authDB connection string configuration (.net) (PUBLIC_2020,QTM10-2019.0281)

The following bugs have been corrected:

- SDMXRI-1466: Annotation update issue
- SDMXRI-1476: wrongly formatted content constraint is generated
- SDMXRI-1510: deadlocks in MS SQL during deletion of artefacts within parallel requests in NSI.WS
- SDMXRI-1518: MAWEB/WS issues with 2020-11-19 release

## maapi.net v8.0.2 (2020-11-18) (MSDB v6.10)

### Details v8.0.2

1. Support for updating non-final that is referenced by other artefacts
   1. Item Schemes
   1. Dataflows
   1. Data Structures
      1. Cross Sectional attributes are not fully supported
   1. Items/Components that are referenced by other artefacts are not removed
1. Fix Annotation update with multiple text entries
1. Fix issue that generated errors when querying actualconstraints and allowedconstraints with version.
1. Fix internal server error when trying to delete or replace a codelist that is referenced by a concept scheme.

### Tickets v8.0.2

The following new features added:

- SDMXRI-1277: Support updating referenced non-final item scheme artefacts (.NET) (OECD,PUBLIC_2020,QTM6-2019.0281)
- SDMXRI-1452: Support updating referenced non-final DSD (.NET) (PUBLIC_2020,QTM10-2019.0281)

The following bugs have been corrected:

- SDMXRI-1466: Annotation update issue
- SDMXRI-1391: Querying actualconstraint or allowedconstraint by version fails
- SDMXRI-1418: Foreign Key constraint violation exception when creating SDMX artefacts (OECD, PULL_REQUEST)

## maapi.net v8.0.1 (2020-10-22) (MSDB v6.10)

### Details v8.0.1

1. Support for updating non-final that is referenced by other artefacts
   1. Final status is updated
   1. Codelist and Concept Scheme. New items are added, removed items are removed (except items that are used).
   1. Transcoding rules are removed
   1. Dataflow
   1. DSD, support for DSD is incomplete.
1. MinValue and MaxValue are now saved into the MSDB
1. Fixed issues updating final/non-final MSD

### Tickets v8.0.1

The following new features added:

- SDMXRI-1277: Support updating referenced non-final item scheme artefacts (.NET) (OECD,PUBLIC_2020,QTM6-2019.0281) (PARTIAL)

The following bugs have been corrected:

- SDMXRI-1442: Issues related to the management of Metadata Structure Definition
- SDMXRI-1458: In TextFormatTypesQueryEngine class minValue and maxValue facets of TextFormat are not saved to database (OECD, PULL_REQUEST)

## maapi.net v8.0.0 (2020-09-24)

### Details v8.0.0

The Header Datasetid is now set to the id of the datastructure unless there is a datasetid defined in the default header in the configuration file
Streaming reader can now parse a header JSON that contains Names in multiple languages
Small bugfixes in SDMXRI
Streaming reader can now parse an Advanced Time Transcoding

### Tickets v8.0.0

The following new features added:

- SDMXRI-1428: Make DatasetID (header) configurable per WS instance (.NET)
- SDMXRI-1436: MAWS Support multiple languages in Header sender/receiver (.net)

The following bugs have been corrected:

- SDMXRI-1440: MAWEB/WS issues with 2020-09-03 release
- SDMXRI-1405: Support Advanced Time Trascoding in MAWS (.NET)

## maapi.net v1.26.5 (2020-09-02) (MSDB v6.10)

### Details v1.26.5

1. Bug fixes
   1. Registry not saving/retrieving Proxy, Public and Upgrades
   1. SDMX Header Name not saved
   1. Fix error message when importing a Content Constraint with missing dependencies

### Tickets v1.26.5

The following bugs have been corrected:

- SDMXRI-1421: MAWEB/WS issues with 2020-07-23 release
- SDMXRI-1438: MAWS.NET not saving header name
- SDMXRI-1419: NSI WS error "Only maintainable are supported by this implementation." when submitting structures

## maapi.net v1.26.4 (2020-07-23) (MSDB v6.10)

### Details v1.26.4

1. Fix issues in header template, such as ignore `dataSetAgencyId` and receiver contacts
1. Various bugfixes, including
   1. Improved Provision agreement import/retrieval support, Metadaflow and better error reporting
   1. MariaDB bug fixes

### Tickets v1.26.4

The following bugs have been corrected:

- SDMXRI-1411: MAWS ver. 1.26.1:  Issues in the header_template fields
- SDMXRI-1400: MAWEB/WS issues with 2020-07-03 release
- SDMXRI-1405: Support Advanced Time Trascoding in MAWS (.NET) (Output only)

## maapi.net v1.26.3 (2020-07-02) (MSDB v6.10)

### Details v1.26.3

1. New method in interface for interacting with MSDB that allows retrieving more information from the connection string.

### Tickets v1.26.3

The following new features added:

- SDMXRI-1388: MAWEB Mapping Store screen. Takes too long to load in some cases (.NET) (PUBLIC_2020)

## maapi.net v1.26.2 (2020-06-12) (MSDB v6.10)

### Details v1.26.2

1. Manage hierarchies of items in the queries using dots (see https://github.com/sdmx-twg/sdmx-rest/blob/master/v2_1/ws/rest/docs/4_3_structural_queries.md)
1. The same category (with the same id) can appear on multiple nodes in the hierarchy
1. Manage "references=dataflow" properly
1. Support for Ignore Production Flag for data. 
1. Concept core representation support, text format and Codelists

### Tickets v1.26.2

The following new features added:

- SDMXRI-1308: Manage hierarchical categoryschemes (OECD,QTM6-2019.0281, PULL_REQUEST)
- SDMXRI-1219: Restore the ignoreProductionFlagForData parameter (.NET) (ISTAT,QTM6-2019.0281)
- SDMXRI-1265: Support Concept Scheme Core representation store/retrieval/references (.NET) (OECD,QTM6-2019.0281)

## maapi.net v1.26.0 (2020-05-13) (MSDB v6.9)

### Details v1.26.0

1. Only one content constraint of the same type that is attached to the same artefact can apply to a specific component.
   1. Validity dates are considered
1. Multiple enhancement to the maapi tool (Estat.Sri.Mapping.Tool.exe)
   1. Pass connection string in command line
   1. New option for upgrade command to initialize the MSDB if it is not initialized
   1. Better error reporting
   1. MAAPI tool can packaged in a (Linux based) Docker image

### Tickets v1.26.0

The following new features added:

- SDMXRI-1172: Apply rules for multiple content constraints (of same type) attached to same artefact (OECD,QTM2-2019.0281)
- SDMXRI-1125: Provide Linux-based Docker image for NSI web service + MSDB/AuthDB (OECD,QTM2-2019.0281)
- SDMXRI-1123: Enhancements for command line MAAPI Tool (OECD,QTM2-2019.0281)

The following bugs have been corrected:

- SDMXRI-1327: MAWS update user category/dataflow association

## maapi.net v1.25.4 (2020-04-09) (MSDB v6.9)

### Details v1.25.4

1. Automatic category creation is now configurable
1. Do not report as error/warning updates to non-final SDMX artefacts
1. Do not report as multi-status the combination of successful insert/updates to SDMX artefacts
1. Fix error handling in streaming with Mapping Assistant entities
1. Various bug fixes related to local data streaming

### Tickets v1.25.4

The following new features added:

- SDMXRI-1110: Make automatic Category creations from categorisation configurable (OECD,QTM2-2019.0281)

The following bugs have been corrected:

- SDMXRI-1317: MAWS.NET error responses broken after streaming
- SDMXRI-1316: MAWEB/WS issues with 2020-03-19 release
- SDMXRI-1267: Not possible to update names/annotations in non-final artefacts that are referenced by other artefacts (OECD)

## maapi.net v1.25.3 (2020-03-19) (MSDB v6.9)

### Details v1.25.3

1. Data Source configuration per dataflow support. This allows to send different data source per dataflow in data registrations.
1. It now targets MSDB v6.9
1. Various bugfixes

### Tickets v1.25.3

The following new features added:

- SDMXRI-852: Implementation of Data Registation/monitor: Configure data source per dataflow (.net) (QTM2-2019.0281)
- SDMXRI-968: Implementation of Data Registation/monitor: Configure data source per dataflow (SQL) (QTM2-2019.0281)

The following bugs have been corrected:

- SDMXRI-1290: Annotation Title is still fixed to 70 characters

## maapi.net v1.25.2 (2020-02-27) (MSDB v6.8)

### Details v1.25.2

1. Import/Export format has changed. It now uses a json format that supports all entities
1. Include SDMX v2.0 Cross Sectional metadata in SDMX v2.1 XML and JSON output as annotations and concept roles
1. Reference partial support for CategorySchemes from OECD
1. Support updating non-final artefacts that are referenced by other artefacts
1. Many bug fixes and tests related to streaming

### Tickets v1.25.2

The following new features added:

- SDMXRI-950: re-use json messages for export/import (.net) (QTM2-2018-SC000941)
- SDMXRI-1150: Include v2.0 CrossSectional information in SDMX-JSON (.net) (QTM2-2019.0281)
- SDMXRI-1292: Implement partial CategorySchemes through detail=referencepartial parameter
- SDMXRI-1267: Not possible to update names/annotations in non-final artefacts that are referenced by other artefacts (OECD)
- SDMXRI-1061: re-use json messages for export/import (MAWEB) (QTM2-2018-SC000941)

Regressions from the following features were fixed:

- SDMXRI-737: MA API/WS Stream local data (.net) (QTM2-2018-SC000941)

## maapi.net v1.25.1 (2020-02-07) (MSDB v6.8)

### Details v1.25.1

1. Fix issue related to `referencepartial`, concept schemes and DSD with components sharing the same concept identity
1. Fix regressions found with streaming
   1. Error reporting
   1. Authorization not working correctly

### Tickets v1.25.1

The following new features added:

- SDMXRI-950: re-use json messages for export/import (.net) (QTM2-2018-SC000941) Partially

The following bugs or regressions have been corrected:

- SDMXRI-1214: "Semantic Error - Duplicate language `it` for TextType" when using referencepartial
- SDMXRI-737: MA API/WS Stream local data (.net) (QTM2-2018-SC000941)

## maapi.net v1.25.0 (2020-01-20) (MSDB v6.8)

### Details v1.25.0

1. Target MSDB v6.8
1. Test projects and command line tool project now target .NET Core 3.1.
1. Parsing entities from a stream now implemented
1. Support for including NSIWS URL in the response of full artefacts requested as stubs
1. Bug fixes related to deleting

### Tickets v1.25.0

The following new features added:

- SDMXRI-1244: NSI WS & MAWS target .NET Core 3.1
- SDMXRI-737: MA API/WS Stream local data (.net) (QTM2-2018-SC000941)
- SDMXRI-1140: Mechanism to set the NSI WS URL in returned stub artefacts (stored in full in MASTORE) (OECD,QTM2-2019.0281)
- SDMXRI-1208: SDMX ID increase to 255 characters in MSDB (Oracle/MariaDB) (OECD,sync-needed)
- SDMXRI-1052: SDMX ID increase to 255 characters in MSDB (SQL) (OECD,QTM2-2018-SC000941)

The following bugs have been corrected:

- SDMXRI-1271: Issue with creation and deletion of content constraints in some cases
- SDMXRI-1222: Referencepartial parameter for request with references doesn't work anymore

## maapi.net v1.24.9 (2019-12-20)(MSDB v6.7)

### Details v1.24.9

1. Introduce streaming of data/metadata for MAWS. Only writers are enabled due to issues found in readers.
1. Improve and correct references resolution (all, parents, children, parents and siblings and specific structures)
1. Many bugfixes related to MAWS and MAWEB

### Tickets v1.24.9

The following new features added:

- SDMXRI-737: MA API/WS Stream local data (.net) (QTM2-2018-SC000941)
- SDMXRI-1194: Support for references parent/children for PA, Structure Set, MSD and MDF + Constraints (.NET) (ISTAT,OECD,QTM2-2019.0281)
- SDMXRI-1115: Creating ContentConstraint from Transcodings Language (ISTAT)

The following bugs were fixed:

- SDMXRI-1240: FW: Issues on Mpping Assistant WS ver.1.24.5

## maapi.net v1.24.6 (2019-11-19) (MSDB v6.7)

### Details v1.24.6

1. Another attempt to remove the Value element under TimeRange in Content Constraint
1. Replace isEqualVersion UDF with plain SQL

### Tickets v1.24.6

The following new features added:

- SDMXRI-1127: Replace UDF dbo.isEqualVersion by native SQL command (OECD)

The following bugs have been corrected:

- SDMXRI-899: Support for Content Constraint Reference Period and Time Range (.NET) (COLLAB,OECD,QTM2-2018-SC000941,SDMXTOOLSTF)

## maapi.net v1.24.5 (2019-11-15)(MSDB v6.7)

### Details v1.24.5

1. Fix bug in Query editor plugin failing to add multiple criteria for one column
1. Added additional checks to avoid building empty transcodings
1. Show a better message when replacing non-final artefacts
1. *NOTE* MSDB schema is referenced as a NuGet package. It is no longer a submodule

### Tickets v1.24.5

The following bugs have been corrected:

- SDMXRI-1212: FW: Mapping Assistant: issue on adding constraints (filters) to a dataset
- SDMXRI-1174: MA WS creates "empty" transcoding (i.e. transcodings without transcoding rules) for those components that do not belong to the cube region of the content constraint.
- SDMXRI-1171: Re-submitting some existing artefact is reported as "Created"

## maapi.net v1.24.3 (2019-10-25) (MSDB v6.7)

### Details v1.24.3

1. Improve performance when importing HCL
1. A lot of fixes to bugs and regressions related to stubs
   1. Could not categorise stub dataflows
   1. Structure/Service URL not retrieved from MSDB
1. Automatic generation of content constraint and transcoding rules fixes
1. Bugfixes related to MAWEB

### Tickets v1.24.3

The following new features added:

- SDMXRI-1198: Improve performance when importing HCL (.net) (ISTAT,OECD)

The following bugs have been corrected:

- SDMXRI-1147: Cannot categorise Dataflows that are defined as external references
- SDMXRI-1188: MAWEB issues in the 11/10/2019 release
- SDMXRI-1174: MA WS creates "empty" transcoding (i.e. transcodings without transcoding rules) for those components that do not belong to the cube region of the content constraint.
- SDMXRI-1173: MAWS empty constraints for /rest/mappingset/contentconstraint/{MASid}/{mappingsetId}/{componentId
- SDMXRI-1189: Request /rest/mappingset/contentconstraint/{MASid}/{mappingsetId} never includes TIME_PERIOD in the Cube Region
- SDMXRI-1137: Rest stub parameter results in isExternalReference="true" and `structureURL="http://need/to/changeit"`

## maapi.net v1.24.2 (2019-10-11) (MSDB v6.7)

### Details v1.24.2

1. Tools were using an old version of DryIOC dll

### Tickets v1.24.2

The following bugs have been corrected:

- SDMXRI-1139: Fix inconsistent DryIoc.dll nuget versions

## maapi.net v1.24.1 (2019-09-27) (MSDB v6.7)

### Tickets v1.24.1

The following new features added:

- SDMXRI-1021: NSI WS support updating the parent codes of existing final codelist(.net) (ISTAT,QTM2-2018-SC000941)
- SDMXRI-1037: Bug with codelist ID longer than 50 chars during dataflow structure retrieval with referencepartial parameter  (QTM2-2018-SC000941)

## maapi.net v1.24.0 (2019-09-09) (MSDB v6.7)

### Details v1.24.0

1. New methods for deleting the children of specific entities.
   1. The local codes of a dataset column
   1. The column description of a dataset column
   1. The transcoding of a mapping
1. Bugfixes for issue related to advance time transcoding which were discovered while porting the changes to Java

### Tickets v1.24.0

The following new features added:

- SDMXRI-947: MAWS delete children entities of one parent (.net) (QTM2-2018-SC000941)
- SDMXRI-720: Do not include URN in structural metadata output (COLLAB,OECD)

The following bugs were fixed:

- SDMXRI-1049: Java SDMX v2.1 Time Range support extra effort (QTM2-2018-SC000941)

## maapi.net v1.23.0 (2019-09-02) (MSDB v6.7)

### Details v1.23.0

1. Further improvements in error messages when importing structual metadata
1. Support importing DSD that use SDMX v2.0 standalone Concepts. Those are used by ECB.
   1. Adding new standalone concepts is not yet possible if they are used by DSDs.
1. Fix issues with MSD/MDF importing and deleting.

### Tickets v1.23.0

The following new features added:

- SDMXRI-854: NSI ws - enhanced status and error messages (.net) (OECD)
- SDMXRI-784: Make STANDALONE_CONCEPT_SCHEME final in order to allow it to be referenced by a DSD (.net) (COLLAB,OECD)

The following bugs have been corrected:

- SDMXRI-1116: Metadata Structure - "isPresentational" field no saved
- SDMXRI-1112: Deletion of Metadata Structure Definition from the SDMX-RI WS

## maapi.net v1.22.0 (2019-08-23) (MSDB v6.7)

### Tickets v1.22.0

The following new features added:

- SDMXRI-899: Support for Content Constraint Reference Period and Time Range (.NET)
- SDMXRI-854: NSI ws - enhanced status and error messages (.net)
- SDMXRI-871: ContentConstraint retriever puts excluded key values as included

## maapi.net v1.21.0 (2019-07-22) (MSDB v6.7)

### Details v1.21.0

1. Uses [MSDB v6.7](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.7)
1. Support for storing and retrieving SDMX stubs including the URL for structure or service
1. Support for updating the parent item of existing items and the parent item of new items
1. Support for Content Constraints that attach to:
   1. Dataset
   1. Metadataset
   1. Data Provider
   1. Simple datasource
   1. Complementing existing maintainable artefact attachment support,  queryable data source support
1. Include History support using a new special mapping of type `VALID_TO`

### Tickets v1.21.0

The following new features added:

- SDMXRI-1078: SDMX RI support Stubs (.NET) (ISTAT,OECD)
- SDMXRI-860: Content Constraint support attaching to Item and Data Sources (.net)(QTM6-2018.SC000641)
- SDMXRI-1021: NSI WS support updating the parent codes of existing final codelist(.net) (ISTAT,QTM2-2018-SC000941)
- SDMXRI-861: Implement ''include history'' in SDMX REST (.net) (QTM6-2018.SC000641)

## maapi.net v1.20.0 (2019-06-25) (MSDB v6.6)

## Details v1.20.0

1. Bugfixes related to Oracle DDB connections
1. Support testing connections without saving

## Tickets v1.20.0

The following new features added:

- SDMXRI-1054: MAWS.NET test connections without saving (QTM2-2018-SC000941,sync-needed)

The following bugs have been corrected:

- SDMXRI-1027: MAWS post DDB connection Oracle

## maapi.net v1.19.0 (2019-05-24) (MSDB v6.6)

## Details v1.19.0

1. Uses MSDB v6.6. Code updated to work updated store procedures
1. Support retrieving content constraint based on their type, actual vs allowed
1. Proxy settings support from MSDB and configuration
1. Bugfixes

## Tickets v1.19.0

The following new features added:

- SDMXRI-934: Merge JDBC into master for release of jdbc DDB connection support (.NET ) (QTM6-2018.SC000641)
- SDMXRI-711: Get feature for (Valid and/or Actual) ContentContraints (.net) (COLLAB,OECD,QTM6-2018.SC000641)
- SDMXRI-924: Store proxy/new registry settings in MSDB and MAAPI/WS (.net) (QTM6-2018.SC000641)

The following bugs have been corrected:

- SDMXRI-1027: MAWS post DDB connection Oracle
- Retrieving local code in some cases fails if they are cached codes

## maapi.net v1.18.0 (2019-04-25) (MSDB v6.5)

## Details v1.18.0

1. Support Content Constraints that attach to Metadataflows, Metadatastructure definitions and Provision Agreements
1. Support querying for item scheme items

## Tickets v1.18.0

The following new features added:

- SDMXRI-859: Support for content constraints attaching to other maintainable artefacts (.net) (QTM6-2018.SC000641)
- SDMXRI-713: Get references for specific items (in ItemSchemes) only .net (COLLAB,OECD,QTM6-2018.SC000641)

The following bugs have been corrected:

- SDMXRI-1023: MAWS .NET DataSet editor responds with Extra data XML instead of JSON
- SDMXRI-1014: Content Constrants from transcoding - no cube region
- SDMXRI-842:  Fix tests for Support A10 frequency in SDMX RI (QTM1-2018.SC000641)

## maapi.net v1.17.1 (2019-03-08) (MSDB v6.5)

## Details v1.17.1

1. Fix a bug where a Mapping Set could not be deleted.

## Tickets v1.17.1

The following bugs have been corrected:

- SDMXRI-952: MAWS/MAAPI .NET cannot delete a Mapping Set

## maapi.net v1.17.0 (2019-02-15) (MSDB v6.5)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5

## Details v1.17.0

1. Support retrieving data and available constraint when frequencies with multipliers are used.

## Tickets v1.17.0

The following new features added:

- SDMXRI-842: Support A10 frequency in SDMX RI (QTM1-2018.SC000641)

## maapi.net v1.16.0 (2019-02-07)  (MSDB v6.5)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5

## Details v1.16.0

1. Try to workaround the blocked dll issue in the Estat.Sri.Mapping.Tool.exe and provide documentation in case it still fails.
1. Created some performance tests using `Nunit` to test concurency issues reported by OECD.
1. Fixed Provision Agreement construction.
1. Fixed a regression on SQL server where querying data with time dimension criteria but without FREQ and the `TIME_PERIOD` column type is an `int`.

## Tickets v1.16.0

The following new features added:

- SDMXRI-827: Document loadFromRemoteSources for SRI .NET applications (QTM1-2018.SC000641)
- SDMXRI-869:  Duplicated DSDs and other artefacts in ARTEFACT table (3rd-level-support)

The following bugs have been corrected:

- SDMXRI-872: Cannot query data with time period criteria without FREQ and TIME_PERIOD DB column an int on sqlserver
- SDMXRI-874: NSI WS .NET produces incorrect Provision Agreement structure

## maapi.net v1.15.0 (2019-01-15) (MSDB v6.5)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5

## Details v1.15.0

*WARNING* this version targets .NET Standard 2.0

1. Switched to package references. As a result Visual Studio versions earlier than 2017 are no longer supported.
1. This version targets .NET Standard 2.0. Applications using SdmxSource must target either
   1. .NET Standard 2.0 or later
   1. .NET Framework 4.6.2 or later
   1. .NET Core 2.1 or later
1. The tests require .NET Core 2.1
1. Bugfixes for issues found while testing for SDMXRI-841
1. Fix regression storing Header template

## Tickets v1.15.0

The following new features added:

- SDMXRI-838: SDMX RI libraries to target .NET Standard 2.0 (QTM1-2018.SC000641)
- SDMXRI-841: Testing the RI workflow from the DDB until the SDMX File generation. (QTM1-2018.SC000641)

The following bugs have been corrected:

- SDMXRI-867: Cannot add SDMX Header template via MAWS

## maapi.net v1.14.0 (2018-10-31) (MSDB v6.5)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5 (or partially v5.3)

## Details v1.14.0

1. Allow adding new items to final item scheme. This is disabled by default. It can be enabled in configuration.
1. Fixes in Query Editor plugin JSON parser/writer
1. Some initial work for XML Authentiation store for MAWS and NSI WS
1. Fixes related to data availability, specifically an error occurred when a key was present.
1. Fix query time criteria handling on .NET when no time transcoding is used

## Tickets v1.14.0

The following new features added:

- SDMXRI-824: Adding items into finalized item schemes
- SDMXRI-825: MAWS / MA_WEB integration (QTM-ma-frontend,QTM5-2018.SC000641)
- SDMXRI-740: XML backend for new Authentication mechanism (QTM1-2018.SC000641)
- SDMXRI-721: Adjust REST data availability according the SDMX TWG proposal (COLLAB,OECD,QTM1-2018.SC000641)

The following bugs have been corrected:

- SDMXRI-739: No data returned when querying 2010 data from 2010-01-01 to 2010-12-31
- SDMXRI-663: MA query editor to use JSON (.NET) (QTM6-2017.SC000281)

## maapi.net v1.13.0 (2018-09-26) (MSDB v6.5)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5 (or partially v5.3)

## Details v1.13.0

1. More work towards data availability. Available mode is not yet implemented
1. Log the execution of SQL statements in a CSV file. Disabled by default. Can be enabled in log4net configuration.
1. The API for updating an entity has been updated to be more user friendly.
1. Fix issue with Content Constraints importing

## Tickets v1.13.0

The following new features added:

- SDMXRI-721: Adjust REST data availability according the SDMX TWG proposal (COLLAB,OECD,QTM1-2018.SC000641)
- SDMXRI-589: Log duration of execution of SQL Queries  (COLLAB,QTM3-2017.SC000281,QTM6-2017.SC000281,QTM7-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-661: MA API for updating an entity (.NET) (QTM6-2017.SC000281)

The following bugs have been corrected:

- SDMXRI-778: NSI WS import Constraints with ReferencePeriod results to error

## maapi.net v1.12.1 (2018-08-07)

This version depends on MSDB v6.5 which contains new tables, stored procedures and indexes.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.5)

## Details v1.12.1

1. Bugfixes related to time transcoding and transcoding in general

## Tickets v1.12.1

The following bugs have been corrected:

- SDMXRI-772: NSI WS .NET Cannot retrieve data when Time Transcoding is used

## maapi.net v1.12.0 (2018-08-03)

This version depends on MSDB v6.5 which contains new tables, stored procedures and indexes.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.5)

## Details v1.12.0

1. Transcoding support for Time Ranges
1. Data Registration tool now supports proxies and updating existing registrations
1. MSDB scripts split into multiple files.
1. Use JSON for Query Editor plugin. New tool to convert old XML to JSON included.
1. Support of SDMX REST v1.2.0 feature related to HTTP Header `If-Modified` using the `DATAFLOW.DATA_LAST_UPDATED` field.
1. New features from OECD
   1. Support for retrieving content constraints as parents of DSDs and dataflows.
   1. Support for new SDMX-REST feature, [reference partial dependencies](https://github.com/sdmx-twg/sdmx-rest/blob/develop/v2_1/ws/rest/docs/4_3_structural_queries.md#parameters-used-to-further-describe-the-desired-results)

## Tickets v1.12.0

The following new features added:

- SDMXRI-755: Partial codelists  review of .NET implementation (QTM1-2018.SC000641)
- SDMXRI-637: Split Mapping Store DDL scripts (QTM6-2017.SC000281)
- SDMXRI-605: Extend SDMX RI permission roles (COLLAB,QTM6-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-663: MA query editor to use JSON (.NET) (QTM6-2017.SC000281)
- SDMXRI-675: POST & Update support in .NET (QTM6-2017.SC000281)
- SDMXRI-673: SDMX RI  support for SDMX REST v1.2.0  (QTM6-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-667: SDMX v2.1 Time Range in .NET (QTM6-2017.SC000281)
- SDMXRI-683: .NET Proxy support in DataRegistration (QTM6-2017.SC000281)
- SDMXRI-158: Implementation of support of constraints in the SDMX-RI

The following bugs have been corrected:

- SDMXRI-749: Problem with turkich language
- SDMXRI-568: SDMX RI Authentication, outside Mapping Store (QTM3-2017.SC000281,QTM7-2017.SC000281)

## maapi.net v1.11.0 (2018-07-02)

This version depends on MSDB v6.4 which contains bugfixes and performance improvements.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.4)

## Details v1.11.0

1. Header template refactoring. It now uses the SdmxSource `IHeader` and the SDMX RI header retriever to ensure maximum compatibility.
1. Authentication/Authorization related bug fixes
1. Data availability related changes. Only exact mode implemented. See [latest changes](https://github.com/sdmx-twg/sdmx-rest/blob/develop/v2_1/ws/rest/docs/4_6_1_other_queries.md)

## Tickets v1.11.0

The following new features added:

- SDMXRI-568: SDMX RI Authentication, outside Mapping Store
- SDMXRI-550: Allow updating of non-final properties of a final artefact
- SDMXRI-721: Adjust REST data availability according the SDMX TWG proposal
- SDMXRI-715: Allow using non-final codelists for attributes

## maapi.net v1.10.0 (2018-04-30)

This version depends on MSDB v6.4 which contains bugfixes and performance improvements.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.4)

## Details v1.10.0

1. Changed the behaviour of Append/Replace of Structure to be treated the same.

## Tickets v1.10.0

- SDMXRI-708: Change behavior of SDMX RR Append/Replace and REST POST/PUT
- SDMXRI-596: Retrieve Provision Agreement and fix Actual flag in Content Constraints retrieval

## maapi.net v1.9.0 (2018-03-15) (MSDB v6.4)

## Details v1.9.0

This version depends on MSDB v6.4 which contains bugfixes and performance improvements.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.4)

This release includes a new library for interacting and manupilating the new AUTH DB.
The schema can be found at [authdb](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/authdb.sql/browse?at=refs%2Fheads%2F1.0)

*Note* the `authdb` is added as a GIT submodule. For GIT user please run the following command to retrieve and initialize the submodules.

```sh
git submodule update --init
```

## Tickets v1.9.0

The following new features added:

- SDMXRI-568: SDMX RI Authentication, outside Mapping Store

## maapi.net v1.8.0 (2017-12-08) (MSDB v6.4)

### Detailed description v1.8.0

This version depends on MSDB v6.4 which contains bugfixes and performance improvements.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.4)

This release includes:

1. A command line tool, `DataMonitorJob.exe` modified to work with non standard Global Registry REST API for submitting data registrations. See [Tool](Tool.md).

### Tickets v1.8.0

The following new features added:

- SDMXRI-651: Change the data registration client of SRI-WS to support the Global SDMX Registry REST API

## maapi.net v1.7.0 (2017-12-08) (MSDB v6.4)

### Detailed description v1.7.0

This version depends on MSDB v6.4 which contains bugfixes and performance improvements.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.4)

This release includes:

1. A new command line tool, `DataMonitorJob.exe`. When run it will check if there are data updates for each provision agreement and send data registrations to the configured SDMX Registry. See [Tool](Tool.md).
1. On the fly DSD support at Mapping Assistant API/WS. It is now possible to create a DSD/Dataflow and a Mapping Set from a DataSet.
1. Updating the non-final attributes names, descriptions and annotations of *final* Artefacts is now possible.
1. New API for submitting structural metadata that tries to follow the Java API, [IStructureSubmitter](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/estat_sdmxsource_extension.net/browse/EstatSdmxSourceExtension/Manager/IStructureSubmitter.cs?at=1.8.0) and a factory for the Mapping Store implementation [StructureSubmitMappingStoreFactory](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/src/Estat.Sri.MappingStore.Store/Factory/StructureSubmitMappingStoreFactory.cs?at=refs%2Fheads%2F1.7.0)
1. A new command line option `-f` to the Estat.Sri.Mapping.Tool.exe from OECD to force initialization/upgrade.
1. Better error reporting in case of appending, replacing or deleting structural metadata.

### Tickets v1.7.0

The following new features added:

- SDMXRI-610: Implementation of Data Registration functionality .NET
- SDMXRI-208: Support OnTheFly DSD in Mapping Assistant
- SDMXRI-550: Allow updating of non-final properties of a final artefact
- SDMXRI-536: OECD follow up meeting on some NSI WS related questions
- SDMXRI-456: "Internal Server Error" message instead of the specific message returned previously

## maapi.net v1.6.0 (2017-10-31) (MSDB v6.3)

### Detailed description v1.6.0

As a result of on going work on On The Fly DSD a new version of MSDB has been released and MAAPI depends on it.
*Note* On The Fly DSD support is not included in this release.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.3)

Other changes include:

1. Support for importing non-final structural metadata that depends on non-final structural  metadata. Please note support is limited to Dataflow to DataStructure, DataStructure to Codelist and Concept Scheme.
1. It is now possible to retrieve Mapping Sets, Mappings and Transcodings from MSDB v5.3. It needs the following configuration setting in order to be enabled:

```xml
<appSettings>
  <add key="MappingStoreVersion53" value="5.3"/>
```

### Tickets fixed in this release v1.6.0

The following new features added:

- SDMXRI-540: Allow to import a dsd in which there is a reference to a not final codelist
- SDMXRI-583: NSI WS should also support latest public release Mapping Store v5.3

## maapi.net v1.5.1 (2017-10-18)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.2

### Detailed description v1.5.1

This is a bug fix release. It fixes issues related to Mapping Assistant DB id and IIS.

## maapi.net v1.5.0 (2017-10-06) (MSDB v6.2)

### Detailed description v1.5.0

*IMPORTANT* Information for the users of the `MappingStoreRetrieval.dll` (or nuget package). It been merged into the MA API. The DLL and NuGet package names have changed!

- SDMX RI DataRetriever and Structure Retriever use the MAAPI (`Estat.Sri.Mapping.Api`) to retrieve the mapping sec configuration from the Mapping Assistant DB
- The DLL names and NuGet packages for mapping store retrieval/store have been modified to reflect better their functionality
  - `MappingStoreRetrieval.NuGet` changed to `Estat.Sri.Sdmx.MappingStore.Retrieve`
  - `Estat.Sri.MappingStore.Store` changed to `Estat.Sri.Sdmx.MappingStore.Store`
  - In addition versions of the above packages have synchronized with MAAPI so the latest versions are 1.5.0
  - Namespaces remain the same but some classes/interface have moved to `Estat.SdmxSource.Extension.NuGet` (v1.6.1 or higher), `Estat.Sri.Mapping.Api` (v1.5.0 or higher) or `Estat.Sri.Mapping.Store` (v1.5.0 or higher)
- The `MappingSetRetriever` is obsolete. It has been replaced by [IComponentMappingManager](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/src/Estat.Sri.Mapping.Api/Manager/IComponentMappingManager.cs?at=1.5.0)

Other changes:

- Support for new API for copying entities. See [ICopyManager](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/src/Estat.Sri.Mapping.Api/Manager/ICopyManager.cs?at=refs%2Fheads%2F1.5.0)
- Support for DataSet editors backend.
  - Manager interface [IDataSetEditorManager](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/src/Estat.Sri.Mapping.Api/Manager/IDataSetEditorManager.cs?at=refs%2Fheads%2F1.5.0)
  - [QueryEditor project](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/src/Estat.Sri.Plugin.Editor.QueryEditor?at=1.5.0)
  - [CustomEditor project](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/src/Estat.Sri.Plugin.Editor.CustomQueryEditor?at=refs%2Fheads%2F1.5.0)
- Support for generating DataSet columns from an SQL Query. API [IDataSetColumnGeneratorManager](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/src/Estat.Sri.Mapping.Api/Manager/IDataSetColumnGeneratorManager.cs?at=1.5.0)
- Support of using external config file

### Tickets fixed in this release v1.5.0

The following new features added:

- SDMXRI-501: Mapping Assistant API default implementation using Mapping Assistant DB (Java/.NET) final release. This includes:
  - SDMXRI-557: Copy between Mapping Store DBs and within a MSDB
  - SDMXRI-553: Query Editor plugin backend support
  - SDMXRI-552: Migrate DR and SR to use MA API
- SDMXRI-500/SDMXRI-558: support multiple mapping stores (NSI & MA WS)

## maapi.net v1.4.0 (2017-09-01) (MSDB v6.2)

### Detailed description v1.4.0

Support for recording and querying User actions has been added.
Meaning adding/remove/updating entities like Mapping Set, Dataset, DDB Connection will be recorded into the Mapping Assistant DB. As a result a new version of MSDB has been released and MAAPI depends on it.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.2)

### Tickets fixed in this release v1.4.0

The following new features added:

- SDMXRI-498: User action support

## maapi.net v1.3.0 (2017-08-05)

### Detailed description v1.3.0

Support for a default value for transcoding has been added. This is the value that will be used when there is no transcoding for a local value or when the value is null or empty.
Support for transcoding uncoded components.
For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.1)

Support for import/export.

1. all entities in a XML file
1. all entities + SDMX artefacts in zip file

The new API is:

1. `IStoreImportManager` for import
1. `IStoreExportManager` for export

*Waning regarding import/export* Please note that the XML used is still in draft. It doesn't support new features like Registry, Uncoded transcdoding, Default value and others. But also old features like Time Period transcoding.
It is very likely that a new version will be released when the Java version is ready.

### Tickets fixed in this release v1.3.0

The following new features added:

- SDMXRI-497: Migration of N to 1 components is now done in SQL when upgrading to MSDB v6.1
- SDMXRI-496: Import/Export (SDMXRI-114)
- SDMXRI-468: Support of transcoding/mapping of emtpy values
- SDMXRI-469: Transcoding for uncoded DSD components

## MAAPI.NET 1.2.0 (2017-07-31)

### Detailed description v1.2.0

This release adds support for upgrading dataflows using Structure Sets using the `IDataflowUpgradeManager` [Test](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/test/Estat.Sri.Mapping.MappingStore.Tests/DataflowUpgradeTest.cs?at=refs%2Fheads%2F1.2.0)

Also when upgrading the Mapping Store database, the Component Mappings with more than one Component will be split to 1 component Component Mappings.

The DDB plugins were refactored to work with Web Services. Right now MariaDB/MySQL, Oracle and SQL Server are supported. Also implemented is an ODBC plugin

### Tickets fixed in this release v1.2.0

The following new features added:

- SDMXRI-496: Mapping Assistant API default implementation using Mapping Assistant DB (Java/.NET) second release
  - SDMXRI-358: Using Structure Set to copy mapping from one DSD version to another
  - SDMXRI-155: Use Registry as a client to download structural metadata from it to MSDB (Included in the previous release)
  - DDB Provider plugin API refactoring to work with Web
- SDMXRI-497: Mapping Assistant DB changes second release
  - Migrate each mapping 1 column to N components to multiple mappings for each component
  - Registry tables/store procedures (Included in the previous release)

The following bugs were fixed:

- Oracle SQL scripts for initialization and upgrading to v6.0 had some syntax errors.
- In addition many bugs related to retrieving and persisting entities were corrected.

## MAAPI.NET v1.1.2(beta) 2017-07-03

- Bugfixes, admin user was not created
- Switched to old style VS 2015 (and earlier) .csproj and package.config from project.json
  - The reason is that project.json is not supported in VS 2017

## MAAPI.NET v1.0.0(beta) 2017-05-22

Major improvements and refactoring. Note it is still work in progress.

## MAAPI.NET v1.0.0(alpha1) 2017-04-07

Initial release
