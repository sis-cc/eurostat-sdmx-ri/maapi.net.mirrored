#https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/docker/building-net-docker-images?view=aspnetcore-3.1
#https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-publish

#---- Build mapping tool ----
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /app
ARG NUGET_REPO
RUN if [ -n "$NUGET_REPO" ]; then dotnet nuget add source "$NUGET_REPO" -n local_repo; fi
COPY Estat.Sri.Mapping.sln Directory.*.props *.cs ./
COPY src ./src

RUN dotnet sln list | grep test | xargs dotnet sln remove
RUN dotnet restore Estat.Sri.Mapping.sln
RUN dotnet build --no-restore Estat.Sri.Mapping.sln
RUN dotnet publish --no-restore src/Estat.Sri.Mapping.Tool/ -c Release -o out 

#---- Build an image containing the mapping tool
FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS runtime

WORKDIR /app

COPY --from=build /app/out/ .

ENTRYPOINT ["dotnet", "Estat.Sri.Mapping.Tool.dll"]
