// -----------------------------------------------------------------------
// <copyright file="TestTimeMappingAdvanced.cs" company="EUROSTAT">
//   Date Created : 2018-5-30
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace MappingStoreRetrieval.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;
    using Estat.Sri.Mapping.MappingStore.Builder.MappingLogic;
    using Estat.Sri.MappingStoreRetrieval.Config;

    using Moq;

    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    [TestFixture]
    internal class TestTimeMappingAdvanced
    {
        private const string DurationColumnName = "DURATION_COLUMN";

        private const string CriteriaColumn = "CRITERIA_COLUMN";

        private const string EndYearMonthColumn = "END_YEAR_MONTH_COLUMN";

        private const string StartYearMonthColumn = "START_YEAR_MONTH_COLUMN";

        private const string EndYearColumn = "END_YEAR_COLUMN";

        private const string EndPeriodColumn = "END_PERIOD_COLUMN";

        private const string StartYearColumn = "START_YEAR_COLUMN";

        private const string StartPeriodColumn = "START_PERIOD_COLUMN";

        private const string StartDateColumn = "START_DATE_COLUMN";

        private const string EndDateColumn = "END_DATE_COLUMN";

        private const string StartIsoColumn = "START_ISO_COLUMN";

        private const string EndIsoColumn = "END_ISO_COLUMN";

        [TestCaseSource(nameof(GetTestCasesForAdvanced))]
        public string AdvancedTimeTranscodingShouldGenerateWhereClauses(TimeDimensionRangeCriteriaValue advanceTimeMapping,  ISdmxDate startDate, ISdmxDate endDate, string criteriaValue)
        {
            var whereClause = advanceTimeMapping.GenerateWhere(startDate, endDate, criteriaValue);

            // we don't care about white space in the test so we remove it except where it matters
            whereClause = whereClause.Replace(" AS ", "#AS#").Replace(" ", string.Empty).Replace("#AS#", " AS ").ToUpperInvariant();
            var openPara = whereClause.Count(x => x == '(');
            var closePara = whereClause.Count(x => x == ')');
            Assert.That(openPara, Is.EqualTo(closePara), whereClause);
            return whereClause;
        }

        [TestCaseSource(nameof(GetTestCasesForAdvancedMap))]
        public string AdvancedTimeTranscodingShouldMap(TimeDimensionRangeCriteriaValue advanceTimeMapping, string criteriaValue, IDataReader reader)
        {
            return advanceTimeMapping.MapComponent(reader, criteriaValue);
        }

        private static IDataReader GetMockDataReader(TimeTranscodingAdvancedEntity advancedEntity, string criteriaValue)
        {
            var columns = advancedEntity.TimeFormatConfigurations.Values.SelectMany(t =>t.GetColumns()).Select(c => c.Name).Distinct().ToList();
            columns.Add(advancedEntity.CriteriaColumn.Name);
            var moq = new Mock<IDataReader>();
            moq.Setup(m => m.FieldCount).Returns(columns.Count);
            for (int i = 0; i < columns.Count; i++)
            {
                int x = i;
                var name = columns[x];
                moq.Setup(r => r.GetName(x)).Returns(name);
                moq.Setup(r => r.GetOrdinal(name)).Returns(x);
                switch (name)
                {
                    case DurationColumnName:
                        moq.Setup(r => r.GetFieldType(x)).Returns(typeof(string));
                        moq.Setup(r => r.GetValue(x)).Returns("P1M");
                        moq.Setup(r => r.GetString(x)).Returns("P1M");
                        break;
                    case CriteriaColumn:
                        moq.Setup(r => r.GetFieldType(x)).Returns(typeof(string));
                        moq.Setup(r => r.GetValue(x)).Returns(criteriaValue);
                        moq.Setup(r => r.GetString(x)).Returns(criteriaValue);
                        break;
                    case EndDateColumn:
                        moq.Setup(r => r.GetFieldType(x)).Returns(typeof(DateTime));
                        moq.Setup(r => r.GetValue(x)).Returns(new DateTime(2007, 01, 31));
                        break;
                    case StartDateColumn:
                        moq.Setup(r => r.GetFieldType(x)).Returns(typeof(DateTime));
                        moq.Setup(r => r.GetValue(x)).Returns(new DateTime(2007, 01, 01));
                        break;
                    case EndIsoColumn:
                        moq.Setup(r => r.GetFieldType(x)).Returns(typeof(string));
                        moq.Setup(r => r.GetValue(x)).Returns("2007-01-31");
                        break;
                    case StartIsoColumn:
                        moq.Setup(r => r.GetFieldType(x)).Returns(typeof(string));
                        moq.Setup(r => r.GetValue(x)).Returns("2007-01-01");
                        break;
                    case EndPeriodColumn:
                        moq.Setup(r => r.GetFieldType(x)).Returns(typeof(string));
                        moq.Setup(r => r.GetValue(x)).Returns("MAR");
                        break;
                    case StartPeriodColumn:
                        moq.Setup(r => r.GetFieldType(x)).Returns(typeof(string));
                        moq.Setup(r => r.GetValue(x)).Returns("JAN");
                        break;
                    case StartYearColumn:
                        moq.Setup(r => r.GetFieldType(x)).Returns(typeof(string));
                        moq.Setup(r => r.GetValue(x)).Returns("2007");
                        break;
                    case EndYearColumn:
                        moq.Setup(r => r.GetFieldType(x)).Returns(typeof(string));
                        moq.Setup(r => r.GetValue(x)).Returns("2007");
                        break;
                    case StartYearMonthColumn:
                        moq.Setup(r => r.GetFieldType(x)).Returns(typeof(string));
                        moq.Setup(r => r.GetValue(x)).Returns("2007-JAN");
                        break;
                     case EndYearMonthColumn:
                        moq.Setup(r => r.GetFieldType(x)).Returns(typeof(string));
                        moq.Setup(r => r.GetValue(x)).Returns("2007-MAR");
                         break;
                    default:
                        Trace.WriteLine($"column name : {name} not mapped");
                        throw new ArgumentOutOfRangeException($"column name : {name} not mapped");
                }
            }

            return moq.Object;
        }

        private static IEnumerable<TestCaseData> GetTestCasesForAdvanced()
        {
            ISdmxDate startDate = new SdmxDateCore("2010-05");
            ISdmxDate endDate = new SdmxDateCore("2015-12");

            TimeTranscodingAdvancedEntity advancedEntity = BuildAdvancedEntity();
            var startEndFormatConfig = BuildStartEndFormatConfig();
            var startEndDateFormatConfig = BuildStartEndDateFormatConfig();
            var startEndMonthly = BuildStartEndNoStandardormatConfig();
            var startEndMonthly4 = BuildStartEndNoStandardormatConfig4Columns();
            advancedEntity.Add(startEndFormatConfig);
            advancedEntity.Add(startEndDateFormatConfig);
            advancedEntity.Add(startEndMonthly);
            advancedEntity.Add(startEndMonthly4);

            var durationMapping = BuildDurationMapping();
            var dictionary = BuildDictionary(durationMapping);
            var startDurationConfig = BuildStartDurationConfig(durationMapping);
            advancedEntity.Add(startDurationConfig);

            var endDurationConfig = BuildEndDurationConfig(durationMapping);
            advancedEntity.Add(endDurationConfig);

            int x = 0;
            string nameFormat = "{{m}}({0:000},{{3}},{1})";
            // iso start end
            string name = "ISO start/end";
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), null, null, startEndFormatConfig.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), startDate, null, startEndFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END')AND(((START_ISO_COLUMN>='2010-05-01'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), startDate, endDate, startEndFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END')AND((((START_ISO_COLUMN>='2010-05-01')))AND(((END_ISO_COLUMN<='2015-12-31')))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), null, endDate, startEndFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END')AND(((END_ISO_COLUMN<='2015-12-31'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), null, null, startEndFormatConfig.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), startDate, null, startEndFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END')AND(((START_ISO_COLUMN>='2010-05-01'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), startDate, endDate, startEndFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END')AND((((START_ISO_COLUMN>='2010-05-01')))AND(((END_ISO_COLUMN<='2015-12-31')))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), null, endDate, startEndFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END')AND(((END_ISO_COLUMN<='2015-12-31'))))").SetName(string.Format(nameFormat, x++, name));

            // date start end
            name = "date start/end";
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), null, null, startEndDateFormatConfig.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), startDate, null, startEndDateFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_DATE')AND(((START_DATE_COLUMN>='2010-05-01'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), startDate, endDate, startEndDateFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_DATE')AND((((START_DATE_COLUMN>='2010-05-01')))AND(((END_DATE_COLUMN<='2015-12-31')))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), null, endDate, startEndDateFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_DATE')AND(((END_DATE_COLUMN<='2015-12-31'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), null, null, startEndDateFormatConfig.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), startDate, null, startEndDateFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_DATE')AND(((START_DATE_COLUMN>=DATE'2010-05-01'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), startDate, endDate, startEndDateFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_DATE')AND((((START_DATE_COLUMN>=DATE'2010-05-01')))AND(((END_DATE_COLUMN<=DATE'2015-12-31')))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), null, endDate, startEndDateFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_DATE')AND(((END_DATE_COLUMN<=DATE'2015-12-31'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.MySqlName), null, null, startEndDateFormatConfig.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.MySqlName), startDate, null, startEndDateFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_DATE')AND(((START_DATE_COLUMN>=DATE'2010-05-01'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.MySqlName), startDate, endDate, startEndDateFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_DATE')AND((((START_DATE_COLUMN>=DATE'2010-05-01')))AND(((END_DATE_COLUMN<=DATE'2015-12-31')))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.MySqlName), null, endDate, startEndDateFormatConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_DATE')AND(((END_DATE_COLUMN<=DATE'2015-12-31'))))").SetName(string.Format(nameFormat, x++, name));

            // monthly start end
            name = "monthly start/end";
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), null, null, startEndMonthly.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), null, null, startEndMonthly.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.MySqlName), null, null, startEndMonthly.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));

            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), startDate, null, startEndMonthly.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD')AND((((SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),1,4)='2010')AND((SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='MAY')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='JUN')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='JUL')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='AUG')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='SEP')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='OCT')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='NOV')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='DEC')))OR((SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),1,4)>='2011')))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), startDate, endDate, startEndMonthly.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD')AND(((((SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),1,4)='2010')AND((SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='MAY')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='JUN')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='JUL')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='AUG')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='SEP')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='OCT')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='NOV')OR(SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='DEC')))OR((SUBSTRING(CAST(START_YEAR_MONTH_COLUMN AS VARCHAR),1,4)>='2011'))))AND(((SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),1,4)<='2014')OR((SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),1,4)='2015')AND((SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='JAN')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='FEB')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='MAR')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='APR')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='MAY')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='JUN')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='JUL')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='AUG')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='SEP')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='OCT')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='NOV')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='DEC')))))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), null, endDate, startEndMonthly.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD')AND(((SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),1,4)<='2014')OR((SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),1,4)='2015')AND((SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='JAN')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='FEB')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='MAR')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='APR')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='MAY')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='JUN')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='JUL')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='AUG')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='SEP')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='OCT')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='NOV')OR(SUBSTRING(CAST(END_YEAR_MONTH_COLUMN AS VARCHAR),6,3)='DEC'))))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), startDate, null, startEndMonthly.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD')AND((((SUBSTR(START_YEAR_MONTH_COLUMN,1,4)='2010')AND((SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='MAY')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='JUN')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='JUL')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='AUG')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='SEP')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='OCT')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='NOV')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='DEC')))OR((SUBSTR(START_YEAR_MONTH_COLUMN,1,4)>='2011')))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), startDate, endDate, startEndMonthly.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD')AND(((((SUBSTR(START_YEAR_MONTH_COLUMN,1,4)='2010')AND((SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='MAY')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='JUN')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='JUL')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='AUG')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='SEP')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='OCT')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='NOV')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='DEC')))OR((SUBSTR(START_YEAR_MONTH_COLUMN,1,4)>='2011'))))AND(((SUBSTR(END_YEAR_MONTH_COLUMN,1,4)<='2014')OR((SUBSTR(END_YEAR_MONTH_COLUMN,1,4)='2015')AND((SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='JAN')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='FEB')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='MAR')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='APR')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='MAY')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='JUN')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='JUL')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='AUG')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='SEP')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='OCT')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='NOV')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='DEC')))))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), null, endDate, startEndMonthly.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD')AND(((SUBSTR(END_YEAR_MONTH_COLUMN,1,4)<='2014')OR((SUBSTR(END_YEAR_MONTH_COLUMN,1,4)='2015')AND((SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='JAN')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='FEB')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='MAR')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='APR')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='MAY')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='JUN')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='JUL')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='AUG')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='SEP')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='OCT')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='NOV')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='DEC'))))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.MySqlName), startDate, null, startEndMonthly.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD')AND((((SUBSTR(START_YEAR_MONTH_COLUMN,1,4)='2010')AND((SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='MAY')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='JUN')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='JUL')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='AUG')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='SEP')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='OCT')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='NOV')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='DEC')))OR((SUBSTR(START_YEAR_MONTH_COLUMN,1,4)>='2011')))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.MySqlName), startDate, endDate, startEndMonthly.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD')AND(((((SUBSTR(START_YEAR_MONTH_COLUMN,1,4)='2010')AND((SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='MAY')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='JUN')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='JUL')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='AUG')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='SEP')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='OCT')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='NOV')OR(SUBSTR(START_YEAR_MONTH_COLUMN,6,3)='DEC')))OR((SUBSTR(START_YEAR_MONTH_COLUMN,1,4)>='2011'))))AND(((SUBSTR(END_YEAR_MONTH_COLUMN,1,4)<='2014')OR((SUBSTR(END_YEAR_MONTH_COLUMN,1,4)='2015')AND((SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='JAN')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='FEB')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='MAR')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='APR')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='MAY')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='JUN')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='JUL')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='AUG')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='SEP')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='OCT')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='NOV')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='DEC')))))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.MySqlName), null, endDate, startEndMonthly.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD')AND(((SUBSTR(END_YEAR_MONTH_COLUMN,1,4)<='2014')OR((SUBSTR(END_YEAR_MONTH_COLUMN,1,4)='2015')AND((SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='JAN')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='FEB')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='MAR')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='APR')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='MAY')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='JUN')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='JUL')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='AUG')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='SEP')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='OCT')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='NOV')OR(SUBSTR(END_YEAR_MONTH_COLUMN,6,3)='DEC'))))))").SetName(string.Format(nameFormat, x++, name));

            // monthly start end 4 columns
            name = "monthly start/end 4 columns";
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), null, null, startEndMonthly4.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), null, null, startEndMonthly4.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.MySqlName), null, null, startEndMonthly4.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));

            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), startDate, null, startEndMonthly4.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD_4COL')AND((((SUBSTRING(CAST(START_YEAR_COLUMN AS VARCHAR),1,4)='2010')AND((SUBSTRING(CAST(START_PERIOD_COLUMN AS VARCHAR),1,3)='APR')OR(SUBSTRING(CAST(START_PERIOD_COLUMN AS VARCHAR),1,3)='JUL')OR(SUBSTRING(CAST(START_PERIOD_COLUMN AS VARCHAR),1,3)='OCT')))OR((SUBSTRING(CAST(START_YEAR_COLUMN AS VARCHAR),1,4)>='2011')))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), startDate, endDate, startEndMonthly4.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD_4COL')AND(((((SUBSTRING(CAST(START_YEAR_COLUMN AS VARCHAR),1,4)='2010')AND((SUBSTRING(CAST(START_PERIOD_COLUMN AS VARCHAR),1,3)='APR')OR(SUBSTRING(CAST(START_PERIOD_COLUMN AS VARCHAR),1,3)='JUL')OR(SUBSTRING(CAST(START_PERIOD_COLUMN AS VARCHAR),1,3)='OCT')))OR((SUBSTRING(CAST(START_YEAR_COLUMN AS VARCHAR),1,4)>='2011'))))AND(((SUBSTRING(CAST(END_YEAR_COLUMN AS VARCHAR),1,4)<='2014')OR((SUBSTRING(CAST(END_YEAR_COLUMN AS VARCHAR),1,4)='2015')AND((SUBSTRING(CAST(END_PERIOD_COLUMN AS VARCHAR),1,3)='MAR')OR(SUBSTRING(CAST(END_PERIOD_COLUMN AS VARCHAR),1,3)='JUN')OR(SUBSTRING(CAST(END_PERIOD_COLUMN AS VARCHAR),1,3)='SEP')OR(SUBSTRING(CAST(END_PERIOD_COLUMN AS VARCHAR),1,3)='DEC')))))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.SqlServerName), null, endDate, startEndMonthly4.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD_4COL')AND(((SUBSTRING(CAST(END_YEAR_COLUMN AS VARCHAR),1,4)<='2014')OR((SUBSTRING(CAST(END_YEAR_COLUMN AS VARCHAR),1,4)='2015')AND((SUBSTRING(CAST(END_PERIOD_COLUMN AS VARCHAR),1,3)='MAR')OR(SUBSTRING(CAST(END_PERIOD_COLUMN AS VARCHAR),1,3)='JUN')OR(SUBSTRING(CAST(END_PERIOD_COLUMN AS VARCHAR),1,3)='SEP')OR(SUBSTRING(CAST(END_PERIOD_COLUMN AS VARCHAR),1,3)='DEC'))))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), startDate, null, startEndMonthly4.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD_4COL')AND((((SUBSTR(START_YEAR_COLUMN,1,4)='2010')AND((SUBSTR(START_PERIOD_COLUMN,1,3)='APR')OR(SUBSTR(START_PERIOD_COLUMN,1,3)='JUL')OR(SUBSTR(START_PERIOD_COLUMN,1,3)='OCT')))OR((SUBSTR(START_YEAR_COLUMN,1,4)>='2011')))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), startDate, endDate, startEndMonthly4.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD_4COL')AND(((((SUBSTR(START_YEAR_COLUMN,1,4)='2010')AND((SUBSTR(START_PERIOD_COLUMN,1,3)='APR')OR(SUBSTR(START_PERIOD_COLUMN,1,3)='JUL')OR(SUBSTR(START_PERIOD_COLUMN,1,3)='OCT')))OR((SUBSTR(START_YEAR_COLUMN,1,4)>='2011'))))AND(((SUBSTR(END_YEAR_COLUMN,1,4)<='2014')OR((SUBSTR(END_YEAR_COLUMN,1,4)='2015')AND((SUBSTR(END_PERIOD_COLUMN,1,3)='MAR')OR(SUBSTR(END_PERIOD_COLUMN,1,3)='JUN')OR(SUBSTR(END_PERIOD_COLUMN,1,3)='SEP')OR(SUBSTR(END_PERIOD_COLUMN,1,3)='DEC')))))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.OracleName), null, endDate, startEndMonthly4.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD_4COL')AND(((SUBSTR(END_YEAR_COLUMN,1,4)<='2014')OR((SUBSTR(END_YEAR_COLUMN,1,4)='2015')AND((SUBSTR(END_PERIOD_COLUMN,1,3)='MAR')OR(SUBSTR(END_PERIOD_COLUMN,1,3)='JUN')OR(SUBSTR(END_PERIOD_COLUMN,1,3)='SEP')OR(SUBSTR(END_PERIOD_COLUMN,1,3)='DEC'))))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.MySqlName), startDate, null, startEndMonthly4.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD_4COL')AND((((SUBSTR(START_YEAR_COLUMN,1,4)='2010')AND((SUBSTR(START_PERIOD_COLUMN,1,3)='APR')OR(SUBSTR(START_PERIOD_COLUMN,1,3)='JUL')OR(SUBSTR(START_PERIOD_COLUMN,1,3)='OCT')))OR((SUBSTR(START_YEAR_COLUMN,1,4)>='2011')))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.MySqlName), startDate, endDate, startEndMonthly4.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD_4COL')AND(((((SUBSTR(START_YEAR_COLUMN,1,4)='2010')AND((SUBSTR(START_PERIOD_COLUMN,1,3)='APR')OR(SUBSTR(START_PERIOD_COLUMN,1,3)='JUL')OR(SUBSTR(START_PERIOD_COLUMN,1,3)='OCT')))OR((SUBSTR(START_YEAR_COLUMN,1,4)>='2011'))))AND(((SUBSTR(END_YEAR_COLUMN,1,4)<='2014')OR((SUBSTR(END_YEAR_COLUMN,1,4)='2015')AND((SUBSTR(END_PERIOD_COLUMN,1,3)='MAR')OR(SUBSTR(END_PERIOD_COLUMN,1,3)='JUN')OR(SUBSTR(END_PERIOD_COLUMN,1,3)='SEP')OR(SUBSTR(END_PERIOD_COLUMN,1,3)='DEC')))))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, null, MappingStoreDefaultConstants.MySqlName), null, endDate, startEndMonthly4.CriteriaValue).Returns("((CRITERIA_COLUMN='START_END_NO_STD_4COL')AND(((SUBSTR(END_YEAR_COLUMN,1,4)<='2014')OR((SUBSTR(END_YEAR_COLUMN,1,4)='2015')AND((SUBSTR(END_PERIOD_COLUMN,1,3)='MAR')OR(SUBSTR(END_PERIOD_COLUMN,1,3)='JUN')OR(SUBSTR(END_PERIOD_COLUMN,1,3)='SEP')OR(SUBSTR(END_PERIOD_COLUMN,1,3)='DEC'))))))").SetName(string.Format(nameFormat, x++, name));

            // start / duration
            name = "start duration";
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.SqlServerName), null, null, startDurationConfig.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.SqlServerName), startDate, null, startDurationConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_DUR')AND(((START_ISO_COLUMN>='2010-05-01'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.SqlServerName), startDate, endDate, startDurationConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_DUR')AND(((START_ISO_COLUMN>='2010-05-01')AND(START_ISO_COLUMN<='2015-12-01'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.SqlServerName), null, endDate, startDurationConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_DUR')AND(((START_ISO_COLUMN<='2015-12-01'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.OracleName), null, null, startDurationConfig.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.OracleName), startDate, null, startDurationConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_DUR')AND(((START_ISO_COLUMN>='2010-05-01'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.OracleName), startDate, endDate, startDurationConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_DUR')AND(((START_ISO_COLUMN>='2010-05-01')AND(START_ISO_COLUMN<='2015-12-01'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.OracleName), null, endDate, startDurationConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='START_DUR')AND(((START_ISO_COLUMN<='2015-12-01'))))").SetName(string.Format(nameFormat, x++, name));

            // duration / end
            name = "duration/end";
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.SqlServerName), null, null, endDurationConfig.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.SqlServerName), startDate, null, endDurationConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='END_DUR')AND(((END_ISO_COLUMN>='2010-05-01'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.SqlServerName), startDate, endDate, endDurationConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='END_DUR')AND(((END_ISO_COLUMN>='2010-05-01')AND(END_ISO_COLUMN<='2015-12-31'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.SqlServerName), null, endDate, endDurationConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='END_DUR')AND(((END_ISO_COLUMN<='2015-12-31'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.OracleName), null, null, endDurationConfig.CriteriaValue).Returns("").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.OracleName), startDate, null, endDurationConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='END_DUR')AND(((END_ISO_COLUMN>='2010-05-01'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.OracleName), startDate, endDate, endDurationConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='END_DUR')AND(((END_ISO_COLUMN>='2010-05-01')AND(END_ISO_COLUMN<='2015-12-31'))))").SetName(string.Format(nameFormat, x++, name));
            yield return new TestCaseData(new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.OracleName), null, endDate, endDurationConfig.CriteriaValue).Returns("((CRITERIA_COLUMN='END_DUR')AND(((END_ISO_COLUMN<='2015-12-31'))))").SetName(string.Format(nameFormat, x++, name));
        }

        private static TimeTranscodingAdvancedEntity BuildAdvancedEntity()
        {
            TimeTranscodingAdvancedEntity advancedEntity = new TimeTranscodingAdvancedEntity();
            advancedEntity.CriteriaColumn = new DataSetColumnEntity() { Name = CriteriaColumn, EntityId = "1" };
            return advancedEntity;
        }

        private static DurationMappingEntity BuildDurationMapping()
        {
            var durationMapping = new DurationMappingEntity() { EntityId ="5", Column = new DataSetColumnEntity() { EntityId = "6", Name = DurationColumnName }};
            return durationMapping;
        }

        private static Dictionary<string, IComponentMappingBuilder> BuildDictionary(DurationMappingEntity durationMapping)
        {
            IComponentMappingBuilder durationMappingBuilder = new ComponentMapping1To1(durationMapping, null);
            var dictionary = new Dictionary<string, IComponentMappingBuilder>();
            dictionary.Add(durationMapping.EntityId, durationMappingBuilder);
            return dictionary;
        }

        private static TimeFormatConfiguration BuildStartEndFormatConfig()
        {
            TimeFormatConfiguration startEndFormatConfig = new TimeFormatConfiguration();
            startEndFormatConfig.CriteriaValue = "START_END";
            startEndFormatConfig.OutputFormat = TimeFormat.TimeRange;
            var startConfig = TimeParticleConfiguration.CreateIsoFormat(new DataSetColumnEntity() { EntityId = "1", Name = StartIsoColumn } );
            var endConfig = TimeParticleConfiguration.CreateIsoFormat(new DataSetColumnEntity() { EntityId = "2", Name = EndIsoColumn });
            startEndFormatConfig.SetConfiguration(startConfig, endConfig);
            return startEndFormatConfig;
        }

        private static TimeFormatConfiguration BuildStartDurationConfig(DurationMappingEntity mappingEntity)
        {
            TimeFormatConfiguration startEndFormatConfig = new TimeFormatConfiguration();
            startEndFormatConfig.CriteriaValue = "START_DUR";
            startEndFormatConfig.OutputFormat = TimeFormat.TimeRange;
            var startConfig = TimeParticleConfiguration.CreateIsoFormat(new DataSetColumnEntity() { EntityId = "1", Name = StartIsoColumn } );
            startEndFormatConfig.SetConfiguration(startConfig, mappingEntity);
            return startEndFormatConfig;
        }

        private static TimeFormatConfiguration BuildEndDurationConfig(DurationMappingEntity mappingEntity)
        {
            TimeFormatConfiguration startEndFormatConfig = new TimeFormatConfiguration();
            startEndFormatConfig.CriteriaValue = "END_DUR";
            startEndFormatConfig.OutputFormat = TimeFormat.TimeRange;
            var endConfig = TimeParticleConfiguration.CreateIsoFormat(new DataSetColumnEntity() { EntityId = "2", Name = EndIsoColumn });
            startEndFormatConfig.SetConfiguration(mappingEntity, endConfig);
            return startEndFormatConfig;
        }

        private static TimeFormatConfiguration BuildStartEndDateFormatConfig()
        {
            TimeFormatConfiguration startEndFormatConfig = new TimeFormatConfiguration();
            startEndFormatConfig.CriteriaValue = "START_END_DATE";
            startEndFormatConfig.OutputFormat = TimeFormat.TimeRange;
            var startConfig = TimeParticleConfiguration.CreateDateFormat(new DataSetColumnEntity() { EntityId = "1", Name = StartDateColumn } );
            var endConfig = TimeParticleConfiguration.CreateDateFormat(new DataSetColumnEntity() { EntityId = "2", Name = EndDateColumn });
          
            startEndFormatConfig.SetConfiguration(startConfig, endConfig);
            return startEndFormatConfig;
        }

        private static TimeFormatConfiguration BuildStartEndNoStandardormatConfig4Columns()
        {
            TimeFormatConfiguration startEndFormatConfig = new TimeFormatConfiguration();
            startEndFormatConfig.CriteriaValue = "START_END_NO_STD_4COL";
            startEndFormatConfig.OutputFormat = TimeFormat.TimeRange;
            var startConfig = new TimeParticleConfiguration();
            startConfig.Format = DisseminationDatabaseTimeFormat.Quarterly;
            startConfig.Year = new TimeRelatedColumnInfo();
            startConfig.Year.Column = new DataSetColumnEntity() { EntityId = "1", Name = StartYearColumn };
            startConfig.Year.Start = 0;
            startConfig.Year.Length = 4;
            startConfig.Period = new PeriodTimeRelatedColumnInfo();
            startConfig.Period.Start = 0;
            startConfig.Period.Length = 3;
            startConfig.Period.Column = new DataSetColumnEntity() { EntityId = "2", Name = StartPeriodColumn };
            for (int i = 1, q = 1; i < 5; i++, q += 3)
            {
                string monthName = CultureInfo.InvariantCulture.DateTimeFormat.GetAbbreviatedMonthName(q).ToUpperInvariant();

                startConfig.AddRule(TimeFormat.QuarterOfYear, i, monthName);
            }

            var endConfig = new TimeParticleConfiguration();
            endConfig.Format = DisseminationDatabaseTimeFormat.Quarterly;
            endConfig.Year = new TimeRelatedColumnInfo();
            endConfig.Year.Column = new DataSetColumnEntity() { EntityId = "3", Name = EndYearColumn };
            endConfig.Year.Start = 0;
            endConfig.Year.Length = 4;
            endConfig.Period = new PeriodTimeRelatedColumnInfo();
            endConfig.Period.Start = 0;
            endConfig.Period.Length = 3;
            endConfig.Period.Column = new DataSetColumnEntity() { EntityId = "4", Name = EndPeriodColumn };
            for (int i = 1, q = 3; i < 5; i++, q += 3)
            {
                string monthName = CultureInfo.InvariantCulture.DateTimeFormat.GetAbbreviatedMonthName(q).ToUpperInvariant();

                endConfig.AddRule(TimeFormat.QuarterOfYear, i, monthName);
            }
            startEndFormatConfig.SetConfiguration(startConfig, endConfig);
            return startEndFormatConfig;
        }

        private static TimeFormatConfiguration BuildStartEndNoStandardormatConfig()
        {
            TimeFormatConfiguration startEndFormatConfig = new TimeFormatConfiguration();
            startEndFormatConfig.CriteriaValue = "START_END_NO_STD";
            startEndFormatConfig.OutputFormat = TimeFormat.TimeRange;
            var startConfig = new TimeParticleConfiguration();
            startConfig.Format = DisseminationDatabaseTimeFormat.Monthly;
            startConfig.Year = new TimeRelatedColumnInfo();
            startConfig.Year.Column = new DataSetColumnEntity() { EntityId = "1", Name = StartYearMonthColumn };
            startConfig.Year.Start = 0;
            startConfig.Year.Length = 4;
            startConfig.Period = new PeriodTimeRelatedColumnInfo();
            startConfig.Period.Start = 5;
            startConfig.Period.Length = 3;
            startConfig.Period.Column = new DataSetColumnEntity() { EntityId = "1", Name = StartYearMonthColumn };
            for (int i = 1; i < 13; i++)
            {
                string monthName = CultureInfo.InvariantCulture.DateTimeFormat.GetAbbreviatedMonthName(i).ToUpperInvariant();

                startConfig.AddRule(TimeFormat.Month, i, monthName);
            }

            var endConfig = new TimeParticleConfiguration();
            endConfig.Format = DisseminationDatabaseTimeFormat.Monthly;
            endConfig.Year = new TimeRelatedColumnInfo();
            endConfig.Year.Column = new DataSetColumnEntity() { EntityId = "2", Name = EndYearMonthColumn };
            endConfig.Year.Start = 0;
            endConfig.Year.Length = 4;
            endConfig.Period = new PeriodTimeRelatedColumnInfo();
            endConfig.Period.Start = 5;
            endConfig.Period.Length = 3;
            endConfig.Period.Column = new DataSetColumnEntity() { EntityId = "2", Name = EndYearMonthColumn };
            for (int i = 1; i < 13; i++)
            {
                string monthName = CultureInfo.InvariantCulture.DateTimeFormat.GetAbbreviatedMonthName(i).ToUpperInvariant();

                endConfig.AddRule(TimeFormat.Month, i, monthName);
            }
            startEndFormatConfig.SetConfiguration(startConfig, endConfig);
            return startEndFormatConfig;
        }

        private static IEnumerable<TestCaseData> GetTestCasesForAdvancedMap()
        {
            ISdmxDate startDate = new SdmxDateCore("2010-05");
            ISdmxDate endDate = new SdmxDateCore("2015-12");

            TimeTranscodingAdvancedEntity advancedEntity = BuildAdvancedEntity();
            var startEndFormatConfig = BuildStartEndFormatConfig();
            var startEndDateFormatConfig = BuildStartEndDateFormatConfig();
            var startEndMonthly = BuildStartEndNoStandardormatConfig();
            var startEndMonthly4 = BuildStartEndNoStandardormatConfig4Columns();
            advancedEntity.Add(startEndFormatConfig);
            advancedEntity.Add(startEndDateFormatConfig);
            advancedEntity.Add(startEndMonthly);
            advancedEntity.Add(startEndMonthly4);

            var durationMapping = BuildDurationMapping();
            var dictionary = BuildDictionary(durationMapping);
            var startDurationConfig = BuildStartDurationConfig(durationMapping);
            advancedEntity.Add(startDurationConfig);

            var endDurationConfig = BuildEndDurationConfig(durationMapping);
            advancedEntity.Add(endDurationConfig);

            int x = 0;
            string nameFormat = "{{m}}({0:000} {{3}} {1})";
            // iso start end
            string name = "ISO start/end";
            TimeDimensionRangeCriteriaValue timeRangeSqlServer = new TimeDimensionRangeCriteriaValue(advancedEntity, dictionary, MappingStoreDefaultConstants.SqlServerName);
            yield return new TestCaseData(timeRangeSqlServer, startEndFormatConfig.CriteriaValue, GetMockDataReader(advancedEntity, startEndFormatConfig.CriteriaValue)).Returns("2007-01-01/P30D").SetName(string.Format(nameFormat, x++, name));
            // date start end
            name = "date start/end";
            yield return new TestCaseData(timeRangeSqlServer, startEndDateFormatConfig.CriteriaValue, GetMockDataReader(advancedEntity, startEndDateFormatConfig.CriteriaValue)).Returns("2007-01-01/P30D").SetName(string.Format(nameFormat, x++, name));
            // monthly start end
            name = "monthly start/end"; 
            // TODO verify that 2007-01-01/2007-03-31 = 2007-01-01/P2M30D
            yield return new TestCaseData(timeRangeSqlServer, startEndMonthly.CriteriaValue, GetMockDataReader(advancedEntity, startEndMonthly.CriteriaValue)).Returns("2007-01-01/P2M30D").SetName(string.Format(nameFormat, x++, name));
            // monthly start end 4 columns
            name = "monthly start/end 4 columns";
            yield return new TestCaseData(timeRangeSqlServer, startEndMonthly4.CriteriaValue, GetMockDataReader(advancedEntity, startEndMonthly4.CriteriaValue)).Returns("2007-01-01/P2M30D").SetName(string.Format(nameFormat, x++, name));
            // start / duration
            name = "start duration";
            yield return new TestCaseData(timeRangeSqlServer, startDurationConfig.CriteriaValue, GetMockDataReader(advancedEntity, startDurationConfig.CriteriaValue)).Returns("2007-01-01/P1M").SetName(string.Format(nameFormat, x++, name));
            // duration / end
            name = "duration/end";
            yield return new TestCaseData(timeRangeSqlServer, endDurationConfig.CriteriaValue, GetMockDataReader(advancedEntity, endDurationConfig.CriteriaValue)).Returns("2006-12-31/P1M").SetName(string.Format(nameFormat, x++, name));
        }
    }
}