// -----------------------------------------------------------------------
// <copyright file="TestCategorisationRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2017-01-02
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStoreRetrieval.Extensions;

namespace MappingStoreRetrieval.Tests
{
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Test unit for <see cref="CategorisationRetrievalEngine"/>
    /// </summary>
    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    [Category("RequiresDB")]
    public class TestCategorisationRetrievalEngine
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(TestCategorisationRetrievalEngine));

        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        /// <summary>
        /// The categorisation retrieval engine
        /// </summary>
        private readonly CategorisationRetrievalEngine _categorisationRetrievalEngine;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestCategorisationRetrievalEngine"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public TestCategorisationRetrievalEngine(string connectionName)
        {
            var connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(connectionName);
            var mappingStoreDb = new Database(connectionStringSettings);
            this._categorisationRetrievalEngine = new CategorisationRetrievalEngine(mappingStoreDb, DataflowFilter.Any);
        }

        [Test]
        public void TestRetrieve()
        {
            var structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                .SetMaintainableTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation))
                .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                .Build();

            var categorisationMutableObjects = this._categorisationRetrievalEngine.Retrieve(structureQuery);
            Assert.That(categorisationMutableObjects, Is.Not.Null.Or.Empty);
        }
    }
}