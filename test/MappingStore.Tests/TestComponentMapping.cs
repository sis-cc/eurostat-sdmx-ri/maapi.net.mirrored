// -----------------------------------------------------------------------
// <copyright file="TestComponentMapping.cs" company="EUROSTAT">
//   Date Created : 2017-06-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Factory.MappingLogic;

    using NUnit.Framework;

    using DataSetColumnEntity = Estat.Sri.Mapping.Api.Model.DataSetColumnEntity;

    public class TestComponentMapping
    {
        [Test]
        public void ShouldGenerateValidWhereClausesFor1to1T()
        {
            var mapping = BuildComponentMapping1To1Transcoding();
            var componentWhere = mapping.GenerateComponentWhere("M", OperatorType.Exact);
            Assert.That(componentWhere, Is.EqualTo(" (( FREQ_COL = 'M''X' )) "));
        }

        [Test]
        public void ShouldGenerateValidWhereClausesFor1to1()
        {
            var mapping = Buil1To1ComponentMapping();
            var componentWhere = mapping.GenerateComponentWhere("M'X", OperatorType.Exact);
            Assert.That(componentWhere, Is.EqualTo(" ( FREQ_COL = 'M''X' ) "));
        }

        [Test]
        public void ShouldGenerateValidWhereClausesFor1toN()
        {
            var mapping = BuildComponentMapping1ToNTranscoding();
            var componentWhere = mapping.GenerateComponentWhere("M", OperatorType.Exact);
            Assert.That(componentWhere, Is.EqualTo(" ( (FREQ_COL = 'M''X'  AND PERIOD_COL = '''P1X'  )  )"));
        }

        [Test]
        public void ShouldGenerateValidWhereClausesForConstant()
        {
            var mapping = Buil1To1ComponentMappingConstant();
            var componentWhere = mapping.GenerateComponentWhere("M'X", OperatorType.Exact);
            Assert.That(componentWhere, Is.EqualTo(" ( 'A' = 'M''X'  ) "));
        }

        /// <summary>
        ///     Builds the frequency component mapping.
        /// </summary>
        /// <returns>
        ///     The <see cref="IComponentMapping" />.
        /// </returns>
        private static IComponentMappingBuilder BuildComponentMapping1To1Transcoding()
        {
            var freqComponent = new Component() { ObjectId = "FREQ", EntityId = "6"};
            var mapping = new ComponentMappingEntity() {  Type = "A", EntityId = "10"};
            var columnEntity = new DataSetColumnEntity() { Name = "FREQ_COL", EntityId = "11"};
            mapping.GetColumns().Add(columnEntity);
            mapping.Component = freqComponent;
            mapping.TranscodingId = "1";

            List<TranscodingRuleEntity> transcodingRules = new List<TranscodingRuleEntity>();
            var localCodes = new[] { "M'X", "QX", "AX" };
            var sdmxCodes = new[] { "M", "Q", "AX" };
            for (int i = 0; i < localCodes.Length; i++)
            {
                TranscodingRuleEntity rule = new TranscodingRuleEntity();
                rule.LocalCodes = new[]
                                      {
                                          new LocalCodeEntity()
                                              {
                                                  ObjectId = localCodes[i],
                                                  ParentId = columnEntity.EntityId
                                              },
                                      };
                rule.DsdCodeEntity = new IdentifiableEntity() { ObjectId = sdmxCodes[i] };
                transcodingRules.Add(rule);
            }

            return new ComponentMappingTranscodingFactory().GetBuilder(mapping, new TranscodingRuleMap(transcodingRules, mapping), null);
        }


        /// <summary>
        ///     Builds the frequency component mapping.
        /// </summary>
        /// <returns>
        ///     The <see cref="IComponentMapping" />.
        /// </returns>
        private static IComponentMappingBuilder BuildComponentMapping1ToNTranscoding()
        {
            var freqComponent = new Component() { ObjectId = "FREQ" , EntityId = "6"};
            var mapping = new ComponentMappingEntity() {  Type = "A", EntityId = "10" };
            var columnEntity = new DataSetColumnEntity() { Name = "FREQ_COL", EntityId = "11"};
            var columnEntity2 = new DataSetColumnEntity() { Name = "PERIOD_COL", EntityId = "12"};
            mapping.GetColumns().Add(columnEntity);
            mapping.GetColumns().Add(columnEntity2);
            mapping.Component = freqComponent;
            List<TranscodingRuleEntity> transcodingRules = new List<TranscodingRuleEntity>();
           
            mapping.TranscodingId = "1";
            var localCodes = new[] { "M'X", "QX", "AX" };
            var localCodes2 = new[] { "'P1X", "P'X", "P%X" };
            var sdmxCodes = new[] { "M", "Q", "AX" };
            for (int i = 0; i < localCodes.Length; i++)
            {
                TranscodingRuleEntity rule = new TranscodingRuleEntity();
                rule.LocalCodes = new[]
                                      {
                                          new LocalCodeEntity()
                                              {
                                                  ObjectId = localCodes[i],
                                                  ParentId = columnEntity.Name
                                              },

                                          new LocalCodeEntity()
                                              {
                                                  ObjectId = localCodes2[i],
                                                  ParentId = columnEntity2.Name
                                              },
                                      };
                rule.DsdCodeEntity = new IdentifiableEntity() { ObjectId = sdmxCodes[i] };
                transcodingRules.Add(rule);
            }

            return new ComponentMappingTranscodingFactory().GetBuilder(mapping, new TranscodingRuleMap(transcodingRules, mapping), null);
        }


        /// <summary>
        ///     Builds the frequency component mapping.
        /// </summary>
        /// <returns>
        ///     The <see cref="IComponentMapping" />.
        /// </returns>
        private static IComponentMappingBuilder Buil1To1ComponentMapping()
        {
            var freqComponent = new Component() { EntityId = "6", ObjectId = "FREQ" };
            var mapping = new ComponentMappingEntity() {  Type = "A", EntityId = "10"};
            var columnEntity = new DataSetColumnEntity() { EntityId = "11", Name = "FREQ_COL" };
            mapping.GetColumns().Add(columnEntity);
            mapping.Component = freqComponent;
          

            return new ComponentMapping1To1Factory().GetBuilder(mapping, null, null);
        }

        /// <summary>
        ///     Builds the frequency component mapping.
        /// </summary>
        /// <returns>
        ///     The <see cref="IComponentMapping" />.
        /// </returns>
        private static IComponentMappingBuilder Buil1To1ComponentMappingConstant()
        {
            var freqComponent = new Component() { EntityId = "6", ObjectId = "FREQ" };
            var mapping = new ComponentMappingEntity() {  Type = "A", EntityId = "10"};
            mapping.ConstantValue = "A";
            mapping.Component = freqComponent;

            return new ComponentMapping1CFactory().GetBuilder(mapping, null, null);
        }
    }
}