// -----------------------------------------------------------------------
// <copyright file="TestSdmxStructureTypeExtensions.cs" company="EUROSTAT">
//   Date Created : 2013-10-02
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Threading;
using Estat.Sri.Sdmx.MappingStore.Retrieve.Helper;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;

namespace MappingStoreRetrieval.Tests
{
    using NUnit.Framework;

    /// <summary>
    /// Test unit for <see cref="AsyncLocalStructureServiceUriHelper"/>
    /// </summary>

    [TestFixture]
    public class TestStructureServiceUriHelpers
    {
        public enum ExistingUriTreatment
        {
            KeepExistingUri,
            OverWrite
        }

        public enum HelperInitialization
        {
            InitializationRequired,
            InitializationNotRequired
        }

        private Uri _defaultUri = new Uri("https://some.existing.value/set/already/");

        [TestCase(ExistingUriTreatment.KeepExistingUri, HelperInitialization.InitializationNotRequired)]
        [TestCase(ExistingUriTreatment.OverWrite, HelperInitialization.InitializationNotRequired)]
        [TestCase(ExistingUriTreatment.KeepExistingUri, HelperInitialization.InitializationRequired)]
        [TestCase(ExistingUriTreatment.OverWrite, HelperInitialization.InitializationRequired)]
        public void TestServiceUrl(ExistingUriTreatment existingUriTreatment, HelperInitialization helperInitialization)
        {
            IStructureServiceUriHelper structureServiceUriHelper = new AsyncLocalStructureServiceUriHelper();

            structureServiceUriHelper.SetStructureServiceUriType(StructureServiceUriType.ServiceUri);
            if (helperInitialization == HelperInitialization.InitializationRequired)
            {
                structureServiceUriHelper.SetBaseUri("http", "testserver:8080", "testBase");
            }
            else
            {
                structureServiceUriHelper.SetBaseUri(null, null, null);
            }

            var dataflow = new DataflowMutableCore();
            dataflow.Id = "TESTDF";
            dataflow.AgencyId = "TESTAGENCY";
            dataflow.Version = "1.1.1";

            structureServiceUriHelper.AddStructureServiceUri(dataflow);

            var expectedServiceURL = helperInitialization == HelperInitialization.InitializationRequired
                ? "http://testserver:8080/testBase/"
                : "/";

            Assert.IsNotNull(dataflow.ServiceURL);
            StringAssert.AreEqualIgnoringCase(expectedServiceURL, dataflow.ServiceURL.ToString());
            Assert.IsNull(dataflow.StructureURL);

            structureServiceUriHelper.AddStructureServiceUri(dataflow, existingUriTreatment == ExistingUriTreatment.OverWrite);

            Assert.IsNotNull(dataflow.ServiceURL);
            StringAssert.AreEqualIgnoringCase(expectedServiceURL, dataflow.ServiceURL.ToString());
            Assert.IsNull(dataflow.StructureURL);

            dataflow.ServiceURL = _defaultUri;

            structureServiceUriHelper.AddStructureServiceUri(dataflow, existingUriTreatment == ExistingUriTreatment.OverWrite);

            Assert.IsNotNull(dataflow.ServiceURL);
            if (existingUriTreatment == ExistingUriTreatment.OverWrite)
            {
                StringAssert.AreEqualIgnoringCase(expectedServiceURL, dataflow.ServiceURL.ToString());
            }
            else
            {
                Assert.AreEqual(_defaultUri, dataflow.ServiceURL);
            }
            Assert.IsNull(dataflow.StructureURL);
        }

        [TestCase(ExistingUriTreatment.KeepExistingUri, HelperInitialization.InitializationNotRequired)]
        [TestCase(ExistingUriTreatment.OverWrite, HelperInitialization.InitializationNotRequired)]
        [TestCase(ExistingUriTreatment.KeepExistingUri, HelperInitialization.InitializationRequired)]
        [TestCase(ExistingUriTreatment.OverWrite, HelperInitialization.InitializationRequired)]
        public void TestStructureUrl(ExistingUriTreatment existingUriTreatment,
            HelperInitialization helperInitialization)
        {
            IStructureServiceUriHelper structureServiceUriHelper = new AsyncLocalStructureServiceUriHelper();

            if (helperInitialization == HelperInitialization.InitializationRequired)
            {
                structureServiceUriHelper.SetBaseUri("http", "testserver:8080", "testBase");
                structureServiceUriHelper.SetStructureServiceUriType(StructureServiceUriType.StructureUri);
            }
            else
            {
                structureServiceUriHelper.SetBaseUri(null, null, null);
                structureServiceUriHelper.SetStructureServiceUriType(default(StructureServiceUriType));
            }

            var dataflow = new DataflowMutableCore();
            dataflow.Id = "TESTDF";
            dataflow.AgencyId = "TESTAGENCY";
            dataflow.Version = "1.1.1";

            structureServiceUriHelper.AddStructureServiceUri(dataflow);

            var expectedStructureURL = helperInitialization == HelperInitialization.InitializationRequired
                ? "http://testserver:8080/testBase/dataflow/TESTAGENCY/TESTDF/1.1.1"
                : "dataflow/TESTAGENCY/TESTDF/1.1.1";

            Assert.IsNotNull(dataflow.StructureURL);
            StringAssert.AreEqualIgnoringCase(expectedStructureURL, dataflow.StructureURL.ToString());
            Assert.IsNull(dataflow.ServiceURL);

            structureServiceUriHelper.AddStructureServiceUri(dataflow,
                existingUriTreatment == ExistingUriTreatment.OverWrite);

            Assert.IsNotNull(dataflow.StructureURL);
            StringAssert.AreEqualIgnoringCase(expectedStructureURL, dataflow.StructureURL.ToString());
            Assert.IsNull(dataflow.ServiceURL);

            dataflow.StructureURL = _defaultUri;

            structureServiceUriHelper.AddStructureServiceUri(dataflow,
                existingUriTreatment == ExistingUriTreatment.OverWrite);

            Assert.IsNotNull(dataflow.StructureURL);
            if (existingUriTreatment == ExistingUriTreatment.OverWrite)
            {
                StringAssert.AreEqualIgnoringCase(expectedStructureURL, dataflow.StructureURL.ToString());
            }
            else
            {
                Assert.AreEqual(_defaultUri, dataflow.StructureURL);
            }

            Assert.IsNull(dataflow.ServiceURL);
        }
    }
}