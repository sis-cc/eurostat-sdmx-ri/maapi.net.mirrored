﻿// -----------------------------------------------------------------------
// <copyright file="TestMutableMaintainableExtensions.cs" company="EUROSTAT">
//   Date Created : 2014-12-10
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System.Linq;

    using Estat.Sri.MappingStoreRetrieval.Extensions;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Test unit for <see cref="MutableMaintainableExtensions"/>
    /// </summary>
    [TestFixture]
    public class TestMutableMaintainableExtensions
    {

        /// <summary>
        /// Test unit for <see cref="MutableMaintainableExtensions.NormalizeSdmxv20DataStructure(Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure.IDataStructureMutableObject)"/> 
        /// </summary>
        [Test]
        public void TestNormalizeDsdWithCodedDimensionForSDMXv21()
        {
            IDimensionMutableObject dimension = new DimensionMutableCore();
            dimension.TimeDimension = true;
            dimension.Id = DimensionObject.TimeDimensionFixedId;
            dimension.ConceptRef = new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "TIME_PERIOD");
            var structureReference = new StructureReferenceImpl("TEST_AGENCY", "CL_TIME_PERIOD", "1.0", SdmxStructureEnumType.CodeList);
            dimension.Representation = new RepresentationMutableCore() { Representation = structureReference };
            IDataStructureMutableObject dsd = new DataStructureMutableCore() { Id = "TEST_DSD", AgencyId = "TEST", Version = "1.0" };
            dsd.AddName("en", "TEST_DSD");
            dsd.AddPrimaryMeasure(new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "OBS_VALUE"));
            dsd.AddDimension(dimension);
            var timeDimensionPre = dsd.GetDimension(DimensionObject.TimeDimensionFixedId);
            Assert.IsNotNull(timeDimensionPre.GetEnumeratedRepresentation());
            dsd.NormalizeSdmxv20DataStructure();

            var timeDimension = dsd.GetDimension(DimensionObject.TimeDimensionFixedId);
            Assert.IsNull(timeDimension.GetEnumeratedRepresentation());
            var annotation = timeDimension.Annotations.FirstOrDefault(o => string.Equals(o.Title, "CODED_TIME_DIMENSION"));
            Assert.IsNotNull(annotation);
            Assert.AreEqual(annotation.Type, string.Format("{0}:{1}({2})", structureReference.AgencyId, structureReference.MaintainableId, structureReference.Version));
            Assert.IsNotEmpty(annotation.Text);
        }
    }
}