// -----------------------------------------------------------------------
// <copyright file="TestDsdRetreival.cs" company="EUROSTAT">
//   Date Created : 2015-01-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace MappingStoreRetrieval.Tests
{
    using System.Linq;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using log4net.Config;
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    using System;
    using System.IO;
    using System.Reflection;
    using log4net;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Model.Error;
    using Estat.Sdmxsource.Extension.Engine;
    using DataRetriever.Test;
    using DryIoc;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

    /// <summary>
    /// Test unit for testing retrieval of DSD
    /// </summary>
    [TestFixture("sqlserver_scratch")]
    public class TestDsdRetreival : TestBase
    {
        IDataStructureObject _dsd;

        public TestDsdRetreival(string storeId)
            : base(storeId)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
        }

        [OneTimeSetUp] 
        public void OneTimeSetUp()
        {
            InitializeMappingStore();

            // add structures
            string structuresFile = @"tests/test-sdmxv3.0.0-DSD-full.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = StructureSubmitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // retrieve DSD
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV3StructureDocument)
                .SetMaintainableTarget(SdmxStructureEnumType.Dsd)
                .SetMaintainableIds("STS")
                .SetAgencyIds("ESTAT")
                .SetVersionRequests(new VersionRequestCore("2.0"))
                .Build();
            var connectionStringSettings = GetConnectionStringSettings();
            var mappingStoreDatabase = new Database(connectionStringSettings);
            var dsdRetrievalEngine = new DsdRetrievalEngine(mappingStoreDatabase);
            var dsdMutable = dsdRetrievalEngine.Retrieve(structureQuery).Single();

            Assert.IsNotNull(dsdMutable);
            _dsd = dsdMutable.ImmutableInstance;
        }

        [Test]
        public void TestComponentRepresentation()
        {
            // measure max occurs = 2 & text format
            IMeasure testMeasure = _dsd.Measures.Single(m => m.Id.Equals("MEASURE_1", StringComparison.Ordinal));
            Assert.AreEqual(1, testMeasure.Representation.MinOccurs);
            Assert.AreEqual(2, testMeasure.Representation.MaxOccurs.Occurrences);
            Assert.AreEqual("urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ESTAT:STS_SCHEME(1.0).OBS_VALUE",
                testMeasure.ConceptRef.TargetUrn.AbsoluteUri);
            Assert.AreEqual(TextEnumType.Double, testMeasure.Representation.TextFormat.TextType.EnumType);

            // attribute max occurs = unbounded & enumeration reference
            IAttributeObject testAttribute = _dsd.GetAttribute("OBS_CONF");
            Assert.AreEqual(2, testAttribute.Representation.MinOccurs);
            Assert.IsTrue(testAttribute.Representation.MaxOccurs.IsUnbounded);
            Assert.IsNotNull(testAttribute.Representation.Representation);
            Assert.AreEqual("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ECB:CL_OBS_CONF(1.0)",
                testAttribute.Representation.Representation.TargetUrn.AbsoluteUri);

            // random checks
            testAttribute = _dsd.GetAttribute("OBS_COM");
            Assert.IsNull(testAttribute.Representation);
            Assert.AreEqual(UsageType.Optional, testAttribute.Usage);

            testAttribute = _dsd.GetAttribute("OBS_STATUS");
            Assert.IsNotNull(testAttribute.Representation);
            Assert.AreEqual("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ECB:CL_OBS_STATUS(1.0)",
                testAttribute.Representation.Representation.TargetUrn.AbsoluteUri);
            Assert.AreEqual(1, testAttribute.Representation.MinOccurs);
            Assert.AreEqual(1, testAttribute.Representation.MaxOccurs.Occurrences);
            Assert.IsNull(testAttribute.Representation.TextFormat);
            Assert.AreEqual(UsageType.Mandatory, testAttribute.Usage);
        }

        // TODO: see if we need to rewrite any of these tests for msdb 7.0
        // Moved old but still valid cases to TestDsdRetrievalForSDMX2x

    }
}