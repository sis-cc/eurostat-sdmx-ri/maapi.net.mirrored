// -----------------------------------------------------------------------
// <copyright file="TestSpecialMutableObjectRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-04-17
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// Test unit for <see cref="TestSpecialMutableObjectRetrievalManagerCodelist"/>
    /// </summary>
    [Category("RequiresDB")]
    public class TestSpecialMutableObjectRetrievalManagerCodelist
    {
        /// <summary>
        /// The _from mutable builder.
        /// </summary>
        private static readonly StructureReferenceFromMutableBuilder _fromMutableBuilder = new StructureReferenceFromMutableBuilder();

        /// <summary>
        /// Initializes a new instance of the <see cref="TestSpecialMutableObjectRetrievalManager"/> class.
        /// </summary>
        public TestSpecialMutableObjectRetrievalManagerCodelist()
        {
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["MappingStoreRetrieversFactory"]);
        }

        /// <summary>
        /// Test unit for <see cref="SpecialMutableObjectRetrievalManager" />
        /// </summary>
        /// <param name="input">The input.</param>
        [TestCaseSource(nameof(GetDimensions), new object[] { "odp" })]
        [TestCaseSource(nameof(GetDimensions), new object[] { "sqlserver" })]
        [TestCaseSource(nameof(GetDimensions), new object[] { "mysql" })]
        public void TestGetMutableCodelistObjectsWithDataflow(TestInput input)
        {
            var structureReference = input.DataflowRef;
            var source = input.Dimension;

            ISpecialMutableObjectRetrievalManager special = new SpecialMutableObjectRetrievalManager(input.Settings);
            ISet<ICodelistMutableObject> mutableCodelistObjects = special.GetMutableCodelistObjects(source.Representation.Representation.MaintainableReference, structureReference, source.ConceptRef.ChildReference.Id, false, null);
            if (!ObjectUtil.ValidCollection(mutableCodelistObjects))
            {
                mutableCodelistObjects = special.GetMutableCodelistObjects(source.Representation.Representation.MaintainableReference, structureReference, source.ConceptRef.ChildReference.Id, true, null);
            }

            if (!ObjectUtil.ValidCollection(mutableCodelistObjects))
            {
                mutableCodelistObjects = special.GetMutableCodelistObjects(source.Representation.Representation.MaintainableReference, null);
            }

            Assert.IsNotEmpty(mutableCodelistObjects, structureReference + " -- " + source.ConceptRef);
        }

        /// <summary>
        /// Gets the dimensions.
        /// </summary>
        /// <returns>
        /// The dataflows and its dimensions
        /// </returns>
        private static IEnumerable<TestInput> GetDimensions(string connectionName)
        {
            var settings = ConfigurationManager.ConnectionStrings[connectionName];
            ISdmxMutableObjectRetrievalManager manager = new MappingStoreRetrievalManager(settings);
            ISet<IDataflowMutableObject> mutableDataflowObjects = manager.GetMutableDataflowObjects(new MaintainableRefObjectImpl(), false, false);
            foreach (var dataflowObject in mutableDataflowObjects)
            {
                var structureReference = _fromMutableBuilder.Build(dataflowObject).MaintainableReference;
                IDataStructureMutableObject dataStructureMutableObject = manager.GetMutableDataStructureObjects(dataflowObject.DataStructureRef.MaintainableReference, false, false).First();
                foreach (var source in dataStructureMutableObject.Dimensions.Where(o => !o.TimeDimension && o.Representation != null && o.Representation.Representation != null && o.Representation.Representation.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.CodeList))
                {
                    yield return new TestInput(structureReference, source, settings);
                }
            }
        }

        /// <summary>
        /// The test input.
        /// </summary>
        public class TestInput
        {
            public ConnectionStringSettings Settings { get; }

            /// <summary>
            /// Initializes a new instance of the <see cref="TestInput"/> class.
            /// </summary>
            /// <param name="dataflowRef">The dataflow reference.</param>
            /// <param name="dimension">The dimension.</param>
            public TestInput(IMaintainableRefObject dataflowRef, IDimensionMutableObject dimension, ConnectionStringSettings settings)
            {
                Settings = settings;
                this.DataflowRef = dataflowRef;
                this.Dimension = dimension;
            }

            /// <summary>
            /// Gets the dataflow reference.
            /// </summary>
            /// <value>
            /// The dataflow reference.
            /// </value>
            public IMaintainableRefObject DataflowRef { get; private set; }

            /// <summary>
            /// Gets the dimension.
            /// </summary>
            /// <value>
            /// The dimension.
            /// </value>
            public IDimensionMutableObject Dimension { get; private set; }

            /// <summary>
            /// Returns a string that represents the current object.
            /// </summary>
            /// <returns>
            /// A string that represents the current object.
            /// </returns>
            public override string ToString()
            {
                return string.Format(CultureInfo.InvariantCulture, "DataflowRef: {0}, Dimension: {1}", this.DataflowRef, this.Dimension != null ? this.Dimension.Id : "null");
            }
        }
    }
}