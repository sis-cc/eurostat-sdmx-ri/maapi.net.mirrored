// -----------------------------------------------------------------------
// <copyright file="TestCategorisationRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2017-01-02
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStoreRetrieval.Extensions;
using Estat.Sri.MappingStoreRetrieval.Helper;

namespace MappingStoreRetrieval.Tests
{
    using System.Linq;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Test unit for <see cref="CategorisationRetrievalEngine"/>
    /// </summary>
    [TestFixture, Ignore("RetrieveFromReferenced do not exist since MSDB 7.0")]
    [System.Obsolete("since MSDB 7.0; Obsoleted Test Class.")]
    public class TestRetrieveFromReferenced
    {
    //    /// <summary>
    //    /// The _log
    //    /// </summary>
    //    private static readonly ILog _log = LogManager.GetLogger(typeof(TestRetrieveFromReferenced));

    //    /// <summary>
    //    /// The connection string helper
    //    /// </summary>
    //    private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

    //    private RetrievalEngineContainer _retrievalEngineContainer;

    //    /// <summary>
    //    /// Initializes a new instance of the <see cref="TestCategorisationRetrievalEngine"/> class.
    //    /// </summary>
    //    /// <param name="connectionName">Name of the connection.</param>
    //    public TestRetrieveFromReferenced(string connectionName)
    //    {
    //        var connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(connectionName);
    //        var mappingStoreDb = new Database(connectionStringSettings);
    //        _retrievalEngineContainer = new RetrievalEngineContainer(mappingStoreDb);
    //    }

    //    [Test]
    //    public void TestCategorisationCodelist()
    //    {
    //        var categorisationMutableObjects = this._retrievalEngineContainer.CategorisationRetrievalEngine.RetrieveFromReferenced(new StructureReferenceImpl()
    //        {
    //            AgencyId = "ESTAT",
    //            Version = "1.0",
    //            MaintainableId = "CL_AGE",
    //            MaintainableStructureEnumType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList)
    //        }, ComplexStructureQueryDetailEnumType.Full);
    //        Assert.That(categorisationMutableObjects, Is.Not.Null.Or.Empty);
    //        Assert.That(categorisationMutableObjects.Count, Is.EqualTo(1));
    //    }

    //    [Test]
    //    public void TestCategorisationDataflow()
    //    {
    //        var categorisationMutableObjects = this._retrievalEngineContainer.CategorisationRetrievalEngine.RetrieveFromReferenced(new StructureReferenceImpl()
    //        {
    //            AgencyId = "IT",
    //            Version = "1.0",
    //            MaintainableId = "144_110",
    //            MaintainableStructureEnumType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)
    //        }, ComplexStructureQueryDetailEnumType.Full);
    //        Assert.That(categorisationMutableObjects, Is.Not.Null.Or.Empty);
    //        Assert.That(categorisationMutableObjects.Count, Is.EqualTo(3));
    //    }

    //    [Test]
    //    public void TestDataflowCodelist()
    //    {
    //        var contentConstraintMutableObjects = this._retrievalEngineContainer.ContentConstraintRetrievalEngine.RetrieveFromReferenced(new StructureReferenceImpl()
    //        {
    //            AgencyId = "IT1",
    //            Version = "1.0",
    //            MaintainableId = "31_124",
    //            MaintainableStructureEnumType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)
    //        }, ComplexStructureQueryDetailEnumType.Full);
    //        Assert.That(contentConstraintMutableObjects, Is.Not.Null.Or.Empty);
    //        Assert.That(contentConstraintMutableObjects.Count, Is.EqualTo(2));
    //    }

    //    [Test]
    //    public void TestMSDConceptScheme()
    //    {
    //        var definitionMutableObjects = this._retrievalEngineContainer.MsdRetrieverEngine.RetrieveFromReferenced(new StructureReferenceImpl()
    //        {
    //            AgencyId = "ESTAT",
    //            Version = "3.0",
    //            MaintainableId = "SDMX_CDC",
    //            MaintainableStructureEnumType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme)
    //        }, ComplexStructureQueryDetailEnumType.Full);
    //        Assert.That(definitionMutableObjects, Is.Not.Null.Or.Empty);
    //        Assert.That(definitionMutableObjects.Count, Is.EqualTo(1));
    //    }

    //    [Test]
    //    public void TestMSDOrganisationUnitScheme()
    //    {
    //        var definitionMutableObjects = this._retrievalEngineContainer.MsdRetrieverEngine.RetrieveFromReferenced(new StructureReferenceImpl()
    //        {
    //            AgencyId = "ESTAT",
    //            Version = "1.0",
    //            MaintainableId = "ESTAT_ORGANISATIONS",
    //            MaintainableStructureEnumType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.OrganisationUnitScheme)
    //        }, ComplexStructureQueryDetailEnumType.Full);
    //        Assert.That(definitionMutableObjects, Is.Not.Null.Or.Empty);
    //        Assert.That(definitionMutableObjects.Count, Is.EqualTo(1));
    //    }

    //    [Test]
    //    public void TestPADataflow()
    //    {
    //        var provisionAgreementMutableObjects = this._retrievalEngineContainer.ProvisionAgreementRetrievalEngine.RetrieveFromReferenced(new StructureReferenceImpl()
    //        {
    //            AgencyId = "ESTAT",
    //            Version = "1.9",
    //            MaintainableId = "NAMAIN_IDC_N",
    //            MaintainableStructureEnumType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)
    //        }, ComplexStructureQueryDetailEnumType.Full);
    //        Assert.That(provisionAgreementMutableObjects, Is.Not.Null.Or.Empty);
    //        Assert.That(provisionAgreementMutableObjects.Count, Is.EqualTo(5));
    //    }
    }
}