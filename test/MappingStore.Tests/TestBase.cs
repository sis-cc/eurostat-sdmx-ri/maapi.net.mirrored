// -----------------------------------------------------------------------
// <copyright file="TestBase.cs" company="EUROSTAT">
//   Date Created : 2020-02-07
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace DataRetriever.Test
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using DryIoc;
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using log4net;

    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.Util.ResourceBundle;
    using Org.Sdmxsource.XmlHelper;

    /// <summary>
    /// The test base.
    /// </summary>
    public abstract class TestBase
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(TestBase));
        private readonly Container _container;
        protected readonly IMappingStoreManager mappingStoreManager;
        protected readonly IConfigurationStoreManager connectionStringManager;
        protected readonly IStructureSubmitterEngine StructureSubmitterEngine;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestBase"/> class.
        /// </summary>
        protected TestBase(string storeId)
        {
            SdmxException.SetMessageResolver(new MessageDecoder());
            try
            {
                StoreId = storeId;
                _container = new Container(rules => rules
                    .With(FactoryMethod.ConstructorWithResolvableArguments)
                    .WithoutThrowOnRegisteringDisposableTransient());

                Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
                string[] folders = { "Estat.Sri.Plugin.SqlServer", "Estat.Sri.Plugin.Oracle", "Estat.Sri.Plugin.Mysql" };
                List<FileInfo> plugins = new List<FileInfo>();
                foreach (var folder in folders)
                {
                    plugins.Add(new FileInfo(string.Format("../../../../../src/{0}/bin/Debug/netstandard2.0/{0}.dll", folder)));
                    plugins.Add(new FileInfo(string.Format("../../../../../src/{0}/bin/Debug/netstandard2.0/{0}.pdb", folder)));
                }
                var assemblies = new List<Assembly>();
                foreach (var fileInfo in plugins)
                {
                    var destPath = Path.Combine(TestContext.CurrentContext.WorkDirectory, fileInfo.Name);
                    if (!File.Exists(destPath) || File.GetLastWriteTime(destPath) < fileInfo.LastWriteTime)
                    {
                        fileInfo.CopyTo(destPath, true);
                    }
                }
                foreach (var fileInfo in plugins)
                {
                    var destPath = Path.Combine(TestContext.CurrentContext.WorkDirectory, fileInfo.Name);
                    if (fileInfo.Extension.EndsWith("dll", StringComparison.OrdinalIgnoreCase))
                    {
                        assemblies.Add(Assembly.LoadFile(destPath));
                    }
                }
                assemblies.AddRange(new[] { typeof(IDatabaseProviderManager).Assembly, typeof(DatabaseManager).Assembly });
                _container.RegisterMany(assemblies, type => !typeof(IEntity).IsAssignableFrom(type));

                MappingStoreIoc.Register<RetrievalEngineContainerFactory>("MappingStoreRetrieversFactory");
                _container.Register<IStructureParsingManager, StructureParsingManager>(
                    made: Made.Of(() => new StructureParsingManager()));
                _container.Register<IStructureWriterFactory, SdmxStructureWriterFactory>();
                _container.Register<IStructureWriterManager, StructureWriterManager>();
                _container.Register<IReadableDataLocationFactory, ReadableDataLocationFactory>();
                mappingStoreManager = _container.Resolve<IMappingStoreManager>();
                connectionStringManager = _container.Resolve<IConfigurationStoreManager>();
                MappingStoreIoc.Container.RegisterMany(
                new[] { typeof(IEntityRetrieverManager).Assembly, typeof(EntityPeristenceFactory).Assembly },
                    type => !typeof(IEntity).IsAssignableFrom(type),
                    reuse: Reuse.Singleton,
                    made: FactoryMethod.ConstructorWithResolvableArguments,
                    setup: Setup.With(allowDisposableTransient: true),
                    ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);
                MappingStoreIoc.Container.Register<IStructureParsingManager, StructureParsingManager>(
                    Reuse.Singleton, 
                    ifAlreadyRegistered: IfAlreadyRegistered.Keep, 
                    made: FactoryMethod.ConstructorWithResolvableArguments);

                var submitFactory = new StructureSubmitMappingStoreFactory((storeId) => GetConnectionStringSettings());
                StructureSubmitterEngine = submitFactory.GetEngine(storeId);
            }
            catch (Exception e)
            {
                _log.Error(e);
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Gets or sets the store identifier.
        /// </summary>
        /// <value>
        /// The store identifier.
        /// </value>
        public string StoreId { get; set; }

        /// <summary>
        /// Gets the containers
        /// </summary>
        /// <value>
        /// </value>
        public Container IoCContainer
        {
            get
            {
                return _container;
            }
        }

        /// <summary>
        /// Gets the connection string settings.
        /// </summary>
        /// <returns></returns>
        protected ConnectionStringSettings GetConnectionStringSettings()
        {
            var connectionString = this.connectionStringManager.GetSettings<ConnectionStringSettings>()
                .FirstOrDefault(stringSettings => stringSettings.Name == this.StoreId);
            return connectionString;
        }

        protected void InitializeMappingStore()
        {
            var engineByStoreId = this.mappingStoreManager.GetEngineByStoreId(StoreId);
            var actionResult = engineByStoreId.Initialize(new DatabaseIdentificationOptions() { StoreId = StoreId });
            Assert.That(actionResult.Status, Is.EqualTo(StatusType.Success), string.Join(", ", actionResult.Messages));
        }

        protected ISdmxObjects ReadStructures(string filepath, SdmxSchemaEnumType sdmxSchema)
        {
            FileInfo fileInfo = new FileInfo(filepath);
            using (IReadableDataLocation rdl = _container.Resolve<IReadableDataLocationFactory>().GetReadableDataLocation(fileInfo))
            {
                IStructureWorkspace workspace = _container.Resolve<IStructureParsingManager>().ParseStructures(rdl);

                ISdmxObjects structureBeans = workspace.GetStructureObjects(false);

                XMLParser.ValidateXml(rdl, sdmxSchema);

                return structureBeans;
            }
        }
    }
}