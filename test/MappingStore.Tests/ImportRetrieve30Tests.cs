using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Estat.Sdmxsource.Extension.Constant;
using Estat.Sdmxsource.Extension.Model.Error;
using Estat.Sri.MappingStoreRetrieval.Engine;
using Estat.Sri.MappingStoreRetrieval.Manager;
using log4net.Config;
using log4net;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using YourNamespace;

namespace MappingStoreRetrieval.Tests
{
    [TestFixture("sqlserver_scratch")]
    public class ImportRetrieve30Tests : DataRetriever.Test.TestBase
    {
        public ImportRetrieve30Tests(string storeId)
            : base(storeId)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            InitializeMappingStore();

            string structuresFile = @"tests/test-sdmxv3.0.0-DSD-full.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = StructureSubmitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }
        }
        [Test]
        public void TestSentinel()
        {
            // add dsd
            string structuresFile = @"tests/dsd_new_feature_example.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = StructureSubmitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // retrieve DSD
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV3StructureDocument)
                .SetMaintainableTarget(SdmxStructureEnumType.Dsd)
                .SetMaintainableIds("DSD_NEW_FEATURE")
                .SetAgencyIds("EXAMPLE")
                .SetVersionRequests(new VersionRequestCore("1.0.3"))
                .Build();
            var connectionStringSettings = GetConnectionStringSettings();
            var mappingStoreDatabase = new Database(connectionStringSettings);
            var dsdRetrievalEngine = new DsdRetrievalEngine(mappingStoreDatabase);
            var dsdMutable = dsdRetrievalEngine.Retrieve(structureQuery).Single();


            DiffReport diffReport = new DiffReport(sdmxObjects.DataStructures.First(), dsdMutable.ImmutableInstance);

            Assert.IsEmpty(diffReport.GetDifferences());
            var sentinelValues = dsdMutable.GetAttribute("OBS_PRE_BREAK").Representation.TextFormat.SentinelValues;
            Assert.AreEqual("-1", sentinelValues.First().Value);

            Assert.AreEqual(1, sentinelValues.First().Names.Count);


        }

        [Test]
        public void TestCascade()
        {
            // add dsd
            string structuresFile = @"tests/datacontentconstraint.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = StructureSubmitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // retrieve content constraint
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV3StructureDocument)
                .SetMaintainableTarget(SdmxStructureEnumType.ContentConstraint)
                .SetMaintainableIds("CN_DATA")
                .SetAgencyIds("EXAMPLE")
                .SetVersionRequests(new VersionRequestCore("1.4.220-draft"))
                .Build();
            
            var connectionStringSettings = GetConnectionStringSettings();
            var mappingStoreDatabase = new Database(connectionStringSettings);
            var contentConstraintRetrievalEngine = new ContentConstraintRetrievalEngine(mappingStoreDatabase);
            var contentConstraint = contentConstraintRetrievalEngine.Retrieve(structureQuery).Single();


            DiffReport diffReport = new DiffReport(sdmxObjects.ContentConstraintObjects.First(), contentConstraint.ImmutableInstance);

            Assert.IsEmpty(diffReport.GetDifferences());

            //check dimensions
            Assert.AreEqual(contentConstraint.IncludedCubeRegion.KeyValues.Count, 3);
            //check cascasde
            var cascadeValues = contentConstraint.IncludedCubeRegion.KeyValues.Where(x => x.Id == "CURRENCY").First();
            Assert.AreEqual(cascadeValues.KeyValues.Count, 2);
            
            foreach (var value in cascadeValues.KeyValues)
            {
                Assert.True(cascadeValues.IsCascadeValue(value));
            }

            //check attributes and measures
            Assert.AreEqual(contentConstraint.IncludedCubeRegion.AttributeValues.Count, 2);

            //check attribute
            Assert.AreEqual(contentConstraint.IncludedCubeRegion.AttributeValues.Where(x => x.Id == "OBS_STATUS").First().KeyValues[0], "A");
            //check measure
            Assert.AreEqual(contentConstraint.IncludedCubeRegion.AttributeValues.Where(x => x.Id == "MEASURE_1").First().KeyValues[0], "123456789.123456789");
        }

        [Test]
        public void TestCubeRegionWildCard()
        {
            // add dsd
            string structuresFile = @"tests/CC_wc_incl_3.0.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = StructureSubmitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // retrieve content constraint
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV3StructureDocument)
                .SetMaintainableTarget(SdmxStructureEnumType.ContentConstraint)
                .SetMaintainableIds("CONS_ACTUAL_SSTSCONS_PROD_M_TUTORIAL")
                .SetAgencyIds("ESTAT")
                .SetVersionRequests(new VersionRequestCore("2.0"))
                .Build();

            var connectionStringSettings = GetConnectionStringSettings();
            var mappingStoreDatabase = new Database(connectionStringSettings);
            var contentConstraintRetrievalEngine = new ContentConstraintRetrievalEngine(mappingStoreDatabase);
            var contentConstraint = contentConstraintRetrievalEngine.Retrieve(structureQuery).Single();


            DiffReport diffReport = new DiffReport(sdmxObjects.ContentConstraintObjects.First(), contentConstraint.ImmutableInstance);

            Assert.IsEmpty(diffReport.GetDifferences());

            //check dimensions
            Assert.AreEqual(contentConstraint.IncludedCubeRegion.KeyValues.Count, 2);
            //check cascasde
            var wildcardKV = contentConstraint.IncludedCubeRegion.KeyValues.Where(x => x.Id == "ADJUSTMENT").First();
            Assert.AreEqual(wildcardKV.KeyValues.Count, 1);
            Assert.AreEqual(wildcardKV.KeyValues[0], "C%");

            var timePeriodKV = contentConstraint.IncludedCubeRegion.KeyValues.Where(x => x.Id == "TIME_PERIOD").First();
            Assert.AreEqual(timePeriodKV.TimeRange.IsStartInclusive, true);
            Assert.AreEqual(timePeriodKV.TimeRange.IsEndInclusive, true);
        }

        [Test]
        public void TestDSDWithAttributeUsageInfo()
        {
            // add msd reference
            string msdFile = @"tests/MSDv21-TEST.xml";
            var sdmxObjects = ReadStructures(msdFile, SdmxSchemaEnumType.VersionTwoPointOne);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = StructureSubmitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }
            // add dsd
            string structuresFile = @"tests/dsd-metadata-in-use.xml";
            
            sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            importResults = StructureSubmitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // retrieve dsd
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV3StructureDocument)
                .SetMaintainableTarget(SdmxStructureEnumType.Dsd)
                .SetMaintainableIds("DSD_MSD_REF")
                .SetAgencyIds("EXAMPLE")
                .SetVersionRequests(new VersionRequestCore("1.0.3"))
                .Build();

            var connectionStringSettings = GetConnectionStringSettings();
            var mappingStoreDatabase = new Database(connectionStringSettings);
            var dsdRetrievalEngine = new DsdRetrievalEngine(mappingStoreDatabase);
            var dsd = dsdRetrievalEngine.Retrieve(structureQuery).Single();


            DiffReport diffReport = new DiffReport(sdmxObjects.DataStructures.First(), dsd.ImmutableInstance);

            Assert.IsEmpty(diffReport.GetDifferences());

            //check attribute usage info
            //we have two metadata attributes
            Assert.AreEqual(dsd.AttributeList.MetadataAttributes.Count, 2);

            Assert.AreEqual(dsd.AttributeList.MetadataAttributes[0].MetadataAttributeReference, "CONTACT_ORGANISATION");
            Assert.True(dsd.AttributeList.MetadataAttributes[0].DimensionReferences.Contains("CURRENCY"));
            Assert.AreEqual(dsd.AttributeList.MetadataAttributes[0].AttachmentLevel, AttributeAttachmentLevel.DimensionGroup);

            Assert.AreEqual(dsd.AttributeList.MetadataAttributes[1].MetadataAttributeReference, "ORGANISATION_UNIT");
            Assert.AreEqual(dsd.AttributeList.MetadataAttributes[1].AttachmentLevel, AttributeAttachmentLevel.Observation);
        }
    }
}
