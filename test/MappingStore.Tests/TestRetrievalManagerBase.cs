﻿// -----------------------------------------------------------------------
// <copyright file="TestRetrievalManagerBase.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.MappingStoreRetrieval.Builder;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// The test retrieval manager base.
    /// </summary>
    public class TestRetrievalManagerBase
    {
        #region Constants

        /// <summary>
        ///     The dummy URL.
        /// </summary>
        private const string DummyUrl = "http://dummy/url";

        #endregion

        /// <summary>
        /// The _from mutable.
        /// </summary>
        private static readonly StructureReferenceFromMutableBuilder _fromMutable = new StructureReferenceFromMutableBuilder();

        #region Methods

        /// <summary>
        /// Makes multiple requests to help benchmarking or profiling
        /// </summary>
        /// <param name="api">The <see cref="ISdmxMutableObjectRetrievalManager" /> implementation to test.</param>
        /// <param name="structureEnumType">Type of the structure.</param>
        protected static void Burn(ISdmxMutableObjectRetrievalManager api, SdmxStructureEnumType structureEnumType)
        {
            var structureType = SdmxStructureType.GetFromEnum(structureEnumType);
            {
                if (structureType.IsMaintainable)
                {
                    IStructureReference query = new StructureReferenceImpl(new MaintainableRefObjectImpl(), structureType);
                    switch (structureType.EnumType)
                    {
                        case SdmxStructureEnumType.Categorisation:
                        case SdmxStructureEnumType.CategoryScheme:
                        case SdmxStructureEnumType.CodeList:
                        case SdmxStructureEnumType.ConceptScheme:
                        case SdmxStructureEnumType.Dataflow:
                        case SdmxStructureEnumType.Dsd:
                        case SdmxStructureEnumType.HierarchicalCodelist:
                            for (int i = 0; i < 100; i++)
                            {
                                ISet<IMaintainableMutableObject> maintainables = api.GetMutableMaintainables(query, false, false);
                                Assert.IsNotEmpty(maintainables, structureType.ToString());
                            }

                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Validates the get maintainables.
        /// </summary>
        /// <param name="api">The <see cref="ISdmxMutableObjectRetrievalManager" /> implementation to test.</param>
        /// <param name="structureEnumType">Type of the structure.</param>
        protected static void ValidateGetMaintainables(ISdmxMutableObjectRetrievalManager api, SdmxStructureEnumType structureEnumType)
        {
            var structureType = SdmxStructureType.GetFromEnum(structureEnumType);
            {
                if (structureType.IsMaintainable)
                {
                    IStructureReference query = new StructureReferenceImpl(new MaintainableRefObjectImpl(), structureType);
                    switch (structureType.EnumType)
                    {
                        case SdmxStructureEnumType.Categorisation:
                        case SdmxStructureEnumType.CategoryScheme:
                        case SdmxStructureEnumType.CodeList:
                        case SdmxStructureEnumType.ConceptScheme:
                        case SdmxStructureEnumType.Dataflow:
                        case SdmxStructureEnumType.Dsd:
                        case SdmxStructureEnumType.HierarchicalCodelist:
                            {
                                ISet<IMaintainableMutableObject> maintainables = api.GetMutableMaintainables(query, false, false);
                                Assert.IsNotEmpty(maintainables, structureType.ToString());
                                foreach (var maintainable in maintainables)
                                {
                                    if (maintainable.StructureType.EnumType == SdmxStructureEnumType.Categorisation)
                                    {
                                        if (ObjectUtil.ValidCollection(maintainable.Annotations) && maintainable.Annotations[0].FromAnnotation() == CustomAnnotationType.CategorySchemeNodeOrder)
                                        {
                                            Trace.Write(maintainable.Id);
                                            Trace.Write(":");
                                            Trace.WriteLine(maintainable.Annotations[0].ValueFromAnnotation());
                                        }
                                    }
                                }

                                Assert.IsTrue(maintainables.All(o => o.StructureType.EnumType == structureType.EnumType), structureType.ToString());
                                IMutableObjects objects = new MutableObjectsImpl(maintainables.Where(o => !string.Equals("MA", o.AgencyId)));
                                ISdmxObjects immutableObjects = objects.ImmutableObjects;
                                Assert.IsNotNull(immutableObjects, structureType.ToString());
                                foreach (ICategorySchemeObject cs in immutableObjects.CategorySchemes)
                                {
                                    Assert.IsNotEmpty(cs.Items, structureType.ToString(), cs.Urn);
                                }

                                foreach (ICodelistObject cs in immutableObjects.Codelists)
                                {
                                    Assert.IsNotEmpty(cs.Items, cs.Urn.ToString());
                                }

                                foreach (IConceptSchemeObject cs in immutableObjects.ConceptSchemes)
                                {
                                    Assert.IsNotEmpty(cs.Items, structureType.ToString(), cs.Urn);
                                }

                                foreach (IDataflowObject cs in immutableObjects.Dataflows)
                                {
                                    Assert.IsNotNull(cs.DataStructureRef, structureType.ToString(), cs.Urn);
                                }

                                foreach (IDataStructureObject cs in immutableObjects.DataStructures)
                                {
                                    Assert.IsNotEmpty(cs.DimensionList.Dimensions, structureType.ToString(), cs.Urn);
                                }

                                foreach (IHierarchicalCodelistObject cs in immutableObjects.HierarchicalCodelists)
                                {
                                    Assert.IsNotEmpty(cs.Hierarchies, structureType.ToString(), cs.Urn);
                                    Assert.IsNotEmpty(cs.CodelistRef, structureType.ToString(), cs.Urn);
                                }
                            }

                            break;
                        case SdmxStructureEnumType.ProvisionAgreement:
                        case SdmxStructureEnumType.Registration:
                        case SdmxStructureEnumType.Subscription:
                        case SdmxStructureEnumType.ContentConstraint:
                        case SdmxStructureEnumType.AttachmentConstraint:
                            Assert.Throws(typeof(SdmxNotImplementedException), () => api.GetMutableMaintainables(query, false, false), structureType.ToString());
                            break;
                        default:
                            Assert.Throws(typeof(NotImplementedException), () => api.GetMutableMaintainables(query, false, false), structureType.ToString());
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Validates the get maintainables stub.
        /// </summary>
        /// <param name="api">The <see cref="ISdmxMutableObjectRetrievalManager" /> implementation to test.</param>
        /// <param name="structureEnumType">Type of the structure.</param>
        protected static void ValidateGetMaintainablesStub(ISdmxMutableObjectRetrievalManager api, SdmxStructureEnumType structureEnumType)
        {
            var structureType = SdmxStructureType.GetFromEnum(structureEnumType);
            {
                if (structureType.IsMaintainable)
                {
                    IStructureReference query = new StructureReferenceImpl(new MaintainableRefObjectImpl(), structureType);
                    switch (structureType.EnumType)
                    {
                        case SdmxStructureEnumType.Categorisation:
                            break;
                        case SdmxStructureEnumType.CategoryScheme:
                        case SdmxStructureEnumType.CodeList:
                        case SdmxStructureEnumType.ConceptScheme:
                        case SdmxStructureEnumType.Dataflow:
                        case SdmxStructureEnumType.Dsd:
                        case SdmxStructureEnumType.HierarchicalCodelist:
                            {
                                ISet<IMaintainableMutableObject> maintainables = api.GetMutableMaintainables(query, false, true);
                                Assert.IsNotEmpty(maintainables, structureType.ToString());
                                Assert.IsTrue(maintainables.All(o => o.StructureType.EnumType == structureType.EnumType), structureType.ToString());
                                IMutableObjects objects = new MutableObjectsImpl();
                                foreach (IMaintainableMutableObject source in maintainables.Where(o => !string.Equals("MA", o.AgencyId)))
                                {
                                    source.StructureURL = new Uri(DummyUrl);
                                    objects.AddIdentifiable(source);
                                }

                                ISdmxObjects immutableObjects = objects.ImmutableObjects;
                                Assert.IsNotNull(immutableObjects, structureType.ToString());
                                foreach (ICategorySchemeObject cs in immutableObjects.CategorySchemes)
                                {
                                    Assert.IsEmpty(cs.Items);
                                }

                                foreach (ICodelistObject cs in immutableObjects.Codelists)
                                {
                                    Assert.IsEmpty(cs.Items);
                                }

                                foreach (IConceptSchemeObject cs in immutableObjects.ConceptSchemes)
                                {
                                    Assert.IsEmpty(cs.Items);
                                }

                                foreach (IDataflowObject cs in immutableObjects.Dataflows)
                                {
                                    Assert.IsNull(cs.DataStructureRef);
                                }

                                foreach (IDataStructureObject cs in immutableObjects.DataStructures)
                                {
                                    Assert.IsNull(cs.DimensionList);
                                    Assert.IsNull(cs.AttributeList);
                                    Assert.IsNull(cs.MeasureList);
                                }

                                foreach (IHierarchicalCodelistObject cs in immutableObjects.HierarchicalCodelists)
                                {
                                    Assert.IsEmpty(cs.Hierarchies);
                                    Assert.IsEmpty(cs.CodelistRef);
                                }
                            }

                            break;
                        case SdmxStructureEnumType.ProvisionAgreement:
                        case SdmxStructureEnumType.Registration:
                        case SdmxStructureEnumType.Subscription:
                        case SdmxStructureEnumType.ContentConstraint:
                        case SdmxStructureEnumType.AttachmentConstraint:
                            Assert.Throws(typeof(SdmxNotImplementedException), () => api.GetMutableMaintainables(query, false, true), structureType.ToString());
                            break;
                        default:
                            Assert.Throws(typeof(NotImplementedException), () => api.GetMutableMaintainables(query, false, true), structureType.ToString());
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Validate the <see cref="ISdmxMutableObjectRetrievalManager.GetMutableMaintainables" />
        /// </summary>
        /// <param name="api">The <see cref="ISdmxMutableObjectRetrievalManager" /> to test</param>
        /// <param name="structureEnumType">Type of the structure.</param>
        protected static void ValidateGetMutableMaintableResolve(ISdmxMutableObjectRetrievalManager api, SdmxStructureEnumType structureEnumType)
        {
            var structureType = SdmxStructureType.GetFromEnum(structureEnumType);
            {
                if (structureType.IsMaintainable)
                {
                    IStructureReference query = new StructureReferenceImpl(new MaintainableRefObjectImpl(), structureType);
                    switch (structureType.EnumType)
                    {
                        case SdmxStructureEnumType.Categorisation:
                            {
                                ISet<IMaintainableMutableObject> list = api.GetMutableMaintainables(query, false, false);
                                Assert.IsNotEmpty(list);
                                var mutables = new MutableObjectsImpl(list);
                                foreach (var categorisation in mutables.Categorisations)
                                {
                                    // DF
                                    var referebcedDataflows = from dataflow in mutables.Dataflows select _fromMutable.Build(dataflow);
                                    CollectionAssert.Contains(referebcedDataflows, categorisation.StructureReference);

                                    // Category Scheme
                                    var categoryScheme = (from m in mutables.CategorySchemes select _fromMutable.Build(m)).ToArray();
                                    CollectionAssert.Contains(categoryScheme, new StructureReferenceImpl(categorisation.CategoryReference.MaintainableReference, categorisation.CategoryReference.MaintainableStructureEnumType));
                                }
                            }

                            break;
                        case SdmxStructureEnumType.CategoryScheme:
                        case SdmxStructureEnumType.CodeList:
                        case SdmxStructureEnumType.ConceptScheme:
                            {
                                ISet<IMaintainableMutableObject> list = api.GetMutableMaintainables(query, false, false);
                                var objects = (from l in list where l.StructureType.EnumType == structureType.EnumType select l).ToArray();
                                Assert.AreEqual(objects.Length, list.Count);
                            }

                            break;
                        case SdmxStructureEnumType.Dataflow:
                            {
                                ISet<IMaintainableMutableObject> list = api.GetMutableMaintainables(query, false, false);
                                var mutables = new MutableObjectsImpl(list);
                                foreach (var dataflow in mutables.Dataflows)
                                {
                                    // DSD
                                    var dsds = (from m in list where m.StructureType.EnumType == SdmxStructureEnumType.Dsd select _fromMutable.Build(m)).Distinct().ToArray();
                                    CollectionAssert.Contains(dsds, dataflow.DataStructureRef);
                                }
                            }

                            break;
                        case SdmxStructureEnumType.Dsd:
                            {
                                ISet<IMaintainableMutableObject> list = api.GetMutableMaintainables(query, false, false);
                                var mutables = new MutableObjectsImpl(list);
                                foreach (var dsd in mutables.DataStructures)
                                {
                                    var components = new List<IComponentMutableObject>(dsd.Dimensions);
                                    if (dsd.AttributeList != null)
                                    {
                                        components.AddRange(dsd.AttributeList.Attributes);
                                    }

                                    components.Add(dsd.PrimaryMeasure);

                                    // CL
                                    var codelistsUsed = from componentMutableObject in components
                                                        where
                                                            componentMutableObject.Representation != null && componentMutableObject.Representation.Representation != null
                                                             && componentMutableObject.Representation.Representation.MaintainableStructureEnumType.EnumType
                                                             == SdmxStructureEnumType.CodeList
                                                        select componentMutableObject.Representation.Representation;
                                    IEnumerable<StructureReferenceImpl> gotCodelists = from mutableObject in list
                                                                                       where mutableObject.StructureType.EnumType == SdmxStructureEnumType.CodeList
                                                                                       select
                                                                                           new StructureReferenceImpl(
                                                                                           mutableObject.AgencyId, mutableObject.Id, mutableObject.Version, mutableObject.StructureType);
                                    var codelistUsedSet = new HashSet<IStructureReference>(codelistsUsed);
                                    var cross = dsd as ICrossSectionalDataStructureMutableObject;
                                    if (cross != null)
                                    {
                                        codelistUsedSet.UnionWith(cross.MeasureDimensionCodelistMapping.Values);
                                    }

                                    CollectionAssert.IsSubsetOf(codelistUsedSet, gotCodelists.Distinct());

                                    // Concept Scheme
                                    IEnumerable<IMaintainableRefObject> conceptsUsed = from c in components select c.ConceptRef.MaintainableReference;
                                    IEnumerable<MaintainableRefObjectImpl> gotConceptSchemes = from mutableObject in list
                                                                                               where mutableObject.StructureType.EnumType == SdmxStructureEnumType.ConceptScheme
                                                                                               select new MaintainableRefObjectImpl(mutableObject.AgencyId, mutableObject.Id, mutableObject.Version);

                                    CollectionAssert.IsSubsetOf(conceptsUsed.Distinct(), gotConceptSchemes.Distinct());
                                }
                            }

                            break;
                        case SdmxStructureEnumType.HierarchicalCodelist:
                            {
                                ISet<IMaintainableMutableObject> list = api.GetMutableMaintainables(query, false, false);
                                var mutables = new MutableObjectsImpl(list);

                                // CL
                                IEnumerable<IStructureReference> codelistsUsed = (from hcl in mutables.HierarchicalCodelists from cl in hcl.CodelistRef select cl.CodelistReference).Distinct();

                                IEnumerable<StructureReferenceImpl> gotCodelists = (from mutableObject in mutables.Codelists
                                                                                    select
                                                                                        new StructureReferenceImpl(
                                                                                        mutableObject.AgencyId, mutableObject.Id, mutableObject.Version, mutableObject.StructureType)).Distinct();

                                CollectionAssert.AreEquivalent(codelistsUsed, gotCodelists);
                            }

                            break;
                        case SdmxStructureEnumType.ProvisionAgreement:
                        case SdmxStructureEnumType.Registration:
                        case SdmxStructureEnumType.Subscription:
                        case SdmxStructureEnumType.ContentConstraint:
                        case SdmxStructureEnumType.AttachmentConstraint:
                            Assert.Throws(typeof(SdmxNotImplementedException), () => api.GetMutableMaintainable(query, false, false), structureType.ToString());
                            break;
                        default:
                            Assert.Throws(typeof(NotImplementedException), () => api.GetMutableMaintainable(query, false, false), structureType.ToString());
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// The validate get mutable maintainable.
        /// </summary>
        /// <param name="api">
        /// The <see cref="ISdmxMutableObjectRetrievalManager"/> implementation to test.
        /// </param>
        /// <param name="structureEnumType">
        /// The structure Type.
        /// </param>
        protected static void ValidateGetMutableMaintainable(ISdmxMutableObjectRetrievalManager api, SdmxStructureEnumType structureEnumType)
        {
            var structureType = SdmxStructureType.GetFromEnum(structureEnumType);
            {
                if (structureType.IsMaintainable)
                {
                    IStructureReference query = new StructureReferenceImpl(new MaintainableRefObjectImpl(), structureType);
                    switch (structureType.EnumType)
                    {
                        case SdmxStructureEnumType.CategoryScheme:
                        case SdmxStructureEnumType.CodeList:
                        case SdmxStructureEnumType.ConceptScheme:
                        case SdmxStructureEnumType.Dataflow:
                        case SdmxStructureEnumType.Dsd:
                        case SdmxStructureEnumType.HierarchicalCodelist:
                        case SdmxStructureEnumType.Categorisation:
                            {
                                ISet<IMaintainableMutableObject> list = api.GetMutableMaintainables(query, false, false);
                                Assert.IsNotEmpty(list, structureType.ToString());
                                IMaintainableMutableObject first = list.First(o => !o.AgencyId.Equals("MA"));
                                query = new StructureReferenceImpl(first.AgencyId, first.Id, null, structureType);
                                IMaintainableMutableObject maintainables = api.GetMutableMaintainable(query, false, false);
                                Assert.IsNotNull(maintainables, structureType.ToString());
                                Assert.IsTrue(maintainables.StructureType.EnumType == structureType.EnumType, structureType.ToString());
                                IMaintainableObject immutableInstance = maintainables.ImmutableInstance;
                                Assert.IsNotNull(immutableInstance, structureType.ToString());
                                var immutableObjects = new SdmxObjectsImpl();
                                immutableObjects.AddIdentifiable(immutableInstance);
                                foreach (ICategorySchemeObject cs in immutableObjects.CategorySchemes)
                                {
                                    Assert.IsNotEmpty(cs.Items, structureType.ToString());
                                }

                                foreach (ICodelistObject cs in immutableObjects.Codelists)
                                {
                                    Assert.IsNotEmpty(cs.Items, structureType.ToString());
                                }

                                foreach (IConceptSchemeObject cs in immutableObjects.ConceptSchemes)
                                {
                                    Assert.IsNotEmpty(cs.Items, structureType.ToString());
                                }

                                foreach (IDataflowObject cs in immutableObjects.Dataflows)
                                {
                                    Assert.IsNotNull(cs.DataStructureRef, structureType.ToString());
                                }

                                foreach (IDataStructureObject cs in immutableObjects.DataStructures)
                                {
                                    Assert.IsNotEmpty(cs.DimensionList.Dimensions, structureType.ToString());
                                }

                                foreach (IHierarchicalCodelistObject cs in immutableObjects.HierarchicalCodelists)
                                {
                                    Assert.IsNotEmpty(cs.Hierarchies, structureType.ToString());
                                    Assert.IsNotEmpty(cs.CodelistRef, structureType.ToString());
                                }
                            }

                            break;
                        case SdmxStructureEnumType.ProvisionAgreement:
                        case SdmxStructureEnumType.Registration:
                        case SdmxStructureEnumType.Subscription:
                        case SdmxStructureEnumType.ContentConstraint:
                        case SdmxStructureEnumType.AttachmentConstraint:
                            Assert.Throws(typeof(SdmxNotImplementedException), () => api.GetMutableMaintainable(query, false, false), structureType.ToString());
                            break;
                        default:
                            Assert.Throws(typeof(NotImplementedException), () => api.GetMutableMaintainable(query, false, false), structureType.ToString());
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// The validate get mutable maintainable.
        /// </summary>
        /// <param name="api">The <see cref="ISdmxMutableObjectRetrievalManager" /> implementation to test.</param>
        /// <param name="structureEnumType">Type of the structure.</param>
        protected static void ValidateGetMutableMaintainableStub(ISdmxMutableObjectRetrievalManager api, SdmxStructureEnumType structureEnumType)
        {
            var structureType = SdmxStructureType.GetFromEnum(structureEnumType);
            {
                if (structureType.IsMaintainable)
                {
                    IStructureReference query = new StructureReferenceImpl(new MaintainableRefObjectImpl(), structureType);
                    switch (structureType.EnumType)
                    {
                        case SdmxStructureEnumType.Categorisation:

                            ////    {
                            ////        query = new StructureReferenceImpl(null, null, null, SdmxStructureEnumType.Dataflow);
                            ////        var list = api.GetMutableMaintainables(query, false, false);
                            ////        Assert.IsNotEmpty(list);
                            ////        var first = list.First(o => !o.AgencyId.Equals("MA"));
                            ////        query = new StructureReferenceImpl(first.AgencyId, first.Id, null, structureType);
                            ////        var maintainables = api.GetMutableMaintainable(query, false, false);
                            ////        Assert.IsNotNull(maintainables, structureType.ToString());
                            ////        Assert.IsTrue(maintainables.StructureType.EnumType == structureType.EnumType, structureType.ToString());
                            ////        var immutableObjects = maintainables.ImmutableInstance;
                            ////        Assert.IsNotNull(immutableObjects, structureType.ToString());
                            ////    }
                            break;
                        case SdmxStructureEnumType.CategoryScheme:
                        case SdmxStructureEnumType.CodeList:
                        case SdmxStructureEnumType.ConceptScheme:
                        case SdmxStructureEnumType.Dataflow:
                        case SdmxStructureEnumType.Dsd:
                        case SdmxStructureEnumType.HierarchicalCodelist:
                            {
                                ISet<IMaintainableMutableObject> list = api.GetMutableMaintainables(query, false, false);
                                Assert.IsNotEmpty(list, structureType.ToString());
                                IMaintainableMutableObject first = list.First(o => !o.AgencyId.Equals("MA"));
                                query = new StructureReferenceImpl(first.AgencyId, first.Id, null, structureType);
                                IMaintainableMutableObject maintainables = api.GetMutableMaintainable(query, false, true);
                                maintainables.StructureURL = new Uri(DummyUrl);
                                Assert.IsNotNull(maintainables, structureType.ToString());
                                Assert.IsTrue(maintainables.StructureType.EnumType == structureType.EnumType, structureType.ToString());
                                IMaintainableObject immutableInstance = maintainables.ImmutableInstance;
                                Assert.IsNotNull(immutableInstance, structureType.ToString());
                                var immutableObjects = new SdmxObjectsImpl();
                                immutableObjects.AddIdentifiable(immutableInstance);
                                foreach (ICategorySchemeObject cs in immutableObjects.CategorySchemes)
                                {
                                    Assert.IsEmpty(cs.Items);
                                }

                                foreach (ICodelistObject cs in immutableObjects.Codelists)
                                {
                                    Assert.IsEmpty(cs.Items);
                                }

                                foreach (IConceptSchemeObject cs in immutableObjects.ConceptSchemes)
                                {
                                    Assert.IsEmpty(cs.Items);
                                }

                                foreach (IDataflowObject cs in immutableObjects.Dataflows)
                                {
                                    Assert.IsNull(cs.DataStructureRef);
                                }

                                foreach (IDataStructureObject cs in immutableObjects.DataStructures)
                                {
                                    Assert.IsNull(cs.DimensionList);
                                    Assert.IsNull(cs.AttributeList);
                                    Assert.IsNull(cs.MeasureList);
                                }

                                foreach (IHierarchicalCodelistObject cs in immutableObjects.HierarchicalCodelists)
                                {
                                    Assert.IsEmpty(cs.Hierarchies);
                                    Assert.IsEmpty(cs.CodelistRef);
                                }
                            }

                            break;
                        case SdmxStructureEnumType.ProvisionAgreement:
                        case SdmxStructureEnumType.Registration:
                        case SdmxStructureEnumType.Subscription:
                        case SdmxStructureEnumType.ContentConstraint:
                        case SdmxStructureEnumType.AttachmentConstraint:
                            Assert.Throws(typeof(SdmxNotImplementedException), () => api.GetMutableMaintainable(query, false, true), structureType.ToString());
                            break;
                        default:
                            Assert.Throws(typeof(NotImplementedException), () => api.GetMutableMaintainable(query, false, true), structureType.ToString());
                            break;
                    }
                }
            }
        }

        #endregion
    }
}