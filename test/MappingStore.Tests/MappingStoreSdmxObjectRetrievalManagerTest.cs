﻿// -----------------------------------------------------------------------
// <copyright file="MappingStoreSdmxObjectRetrievalManagerTest.cs" company="EUROSTAT">
//   Date Created : 2014-04-04
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System.Configuration;

    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Test unit for <see cref="MappingStoreSdmxObjectRetrievalManager"/>
    /// </summary>
    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    [Category("RequiresDB")]
    public class MappingStoreSdmxObjectRetrievalManagerTest
    {
        private readonly ISdmxObjectRetrievalManager _sdmxObjectRetrievalManager;

        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public MappingStoreSdmxObjectRetrievalManagerTest(string name)
        {
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["MappingStoreRetrieversFactory"]);
            ConnectionStringSettings css = this._connectionStringHelper.GetConnectionStringSettings(name);
            this._sdmxObjectRetrievalManager = new MappingStoreSdmxObjectRetrievalManager(css);
        }

        /// <summary>
        /// Test unit for <see cref="MappingStoreSdmxObjectRetrievalManager.GetMaintainable(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IStructureReference,bool)"/> 
        /// </summary>
        [TestCase(true)]
        [TestCase(false)]
        public void TestGetMaintainable(bool returnStub)
        {
            var maintainableObject = this._sdmxObjectRetrievalManager.GetMaintainableObject<ICodelistObject>(new StructureReferenceImpl("MA", "SDMX_M_PERIODS", null, SdmxStructureEnumType.CodeList), returnStub, false);
            Assert.NotNull(maintainableObject);
        }
        
        [TestCase(false, false)]
        [TestCase(false, true)]
        [TestCase(true, false)]
        [TestCase(true, true)]
        public void TestGetCodelists(bool returnLatest, bool returnStub)
        {
            var maintainableObject = this._sdmxObjectRetrievalManager.GetMaintainableObjects<ICodelistObject>(new MaintainableRefObjectImpl(), returnLatest, returnStub);
            Assert.NotNull(maintainableObject);
            Assert.IsNotEmpty(maintainableObject);
        }

        [TestCase(false, false)]
        [TestCase(false, true)]
        [TestCase(true, false)]
        [TestCase(true, true)]
        public void TestGetDataflows(bool returnLatest, bool returnStub)
        {
            var maintainableObject = this._sdmxObjectRetrievalManager.GetMaintainableObjects<IDataflowObject>(new MaintainableRefObjectImpl(), returnLatest, returnStub);
            Assert.NotNull(maintainableObject);
            Assert.IsNotEmpty(maintainableObject);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void TestGetCategorisations(bool returnStub)
        {
            var maintainableObject = this._sdmxObjectRetrievalManager.GetMaintainableObjects<ICategorisationObject>(new MaintainableRefObjectImpl(), returnStub, false);
            Assert.NotNull(maintainableObject);
            Assert.IsNotEmpty(maintainableObject);
        }

        [TestCase(false, false)]
        [TestCase(false, true)]
        [TestCase(true, false)]
        [TestCase(true, true)]
        public void TestGetCategoryScheme(bool returnLatest, bool returnStub)
        {
            var maintainableObject = this._sdmxObjectRetrievalManager.GetMaintainableObjects<ICategorisationObject>(new MaintainableRefObjectImpl(), returnLatest, returnStub);
            Assert.NotNull(maintainableObject);
            Assert.IsNotEmpty(maintainableObject);
        }

        [TestCase(false, false)]
        [TestCase(false, true)]
        [TestCase(true, false)]
        [TestCase(true, true)]
        public void TestGetDsd(bool returnLatest, bool returnStub)
        {
            var maintainableObject = this._sdmxObjectRetrievalManager.GetMaintainableObjects<IDataStructureObject>(new MaintainableRefObjectImpl(), returnLatest, returnStub);
            Assert.NotNull(maintainableObject);
            Assert.IsNotEmpty(maintainableObject);
        }

        [TestCase(false, false)]
        [TestCase(false, true)]
        [TestCase(true, false)]
        [TestCase(true, true)]
        public void TestGetHcl(bool returnLatest, bool returnStub)
        {
            var maintainableObject = this._sdmxObjectRetrievalManager.GetMaintainableObjects<IHierarchicalCodelistObject>(new MaintainableRefObjectImpl(), returnLatest, returnStub);
            Assert.NotNull(maintainableObject);
            Assert.IsNotEmpty(maintainableObject);
        }

        [TestCase(false, false)]
        [TestCase(false, true)]
        [TestCase(true, false)]
        [TestCase(true, true)]
        public void TestGetConceptScheme(bool returnLatest, bool returnStub)
        {
            var maintainableObject = this._sdmxObjectRetrievalManager.GetMaintainableObjects<IConceptSchemeObject>(new MaintainableRefObjectImpl(), returnLatest, returnStub);
            Assert.NotNull(maintainableObject);
            Assert.IsNotEmpty(maintainableObject);
        }
    }
}