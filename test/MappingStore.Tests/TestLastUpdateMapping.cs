// -----------------------------------------------------------------------
// <copyright file="TestLastUpdateMapping.cs" company="EUROSTAT">
//   Date Created : 2016-11-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder.MappingLogic;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    using TimeRangeCore = Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex.TimeRangeCore;

    /// <summary>
    /// The test last update mapping.
    /// </summary>
    [TestFixture]
    public class TestLastUpdateMapping
    {
        /// <summary>
        /// The DDB
        /// </summary>
        private readonly Database _ddb;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestLastUpdateMapping"/> class.
        /// </summary>
        public TestLastUpdateMapping()
        {
            var connectionStringSettings = new ConnectionStringSettings() { ProviderName = "System.Data.SqlClient" };
            this._ddb = new Database(connectionStringSettings);
        }

        /// <summary>
        /// Tests the constant.
        /// </summary>
        [Test]
        public void TestConstant()
        {
            var entity = new LastUpdatedEntity() { EntityId = "1"  };
            entity.ConstantValue = "2013-02-02";

           LastUpdateMapping mapping = new LastUpdateMapping(entity, this._ddb);
           var output = mapping.GenerateWhere(new ITimeRange[] { new TimeRangeCore(false, new SdmxDateCore("2013-01-01"), null, false, false),  });
           Assert.That(output, Is.Null);
        }

        /// <summary>
        /// Tests the constant no results.
        /// </summary>
        [Test]
        public void TestConstantNoResults()
        {
            var entity = new LastUpdatedEntity() { EntityId = "1"  };
            entity.ConstantValue = "2014-02-02";

            LastUpdateMapping mapping = new LastUpdateMapping(entity, this._ddb);
            Assert.Throws<SdmxNoResultsException>(() => mapping.GenerateWhere(new ITimeRange[] { new TimeRangeCore(true, new SdmxDateCore("2013-01-01"), new SdmxDateCore("2014-01-01"), false, false) }));
        }

        /// <summary>
        /// Tests the where clause.
        /// </summary>
        /// <param name="timeRanges">The time ranges.</param>
        /// <returns>The where clause</returns>
        [TestCaseSource(typeof(GenerateTestWhereClauses))]
        public string TestWhereClause(ITimeRange[] timeRanges)
        {
            var entity = new LastUpdatedEntity() { EntityId = "1"  };

            entity.Column = new DataSetColumnEntity() { Name = "COL", EntityId = "1"};
            LastUpdateMapping mapping = new LastUpdateMapping(entity, this._ddb);
           return mapping.GenerateWhere(timeRanges).WhereClause;
        }

        /// <summary>
        /// Tests the where clause.
        /// </summary>
        /// <param name="timeRanges">The time ranges.</param>
        /// <returns>The where clause</returns>
        [TestCaseSource(typeof(GenerateTestWhereClauses))]
        public string TestTypeOfParameters(ITimeRange[] timeRanges)
        {
            var entity = new LastUpdatedEntity() { EntityId = "1" };
            entity.Column = new DataSetColumnEntity() { Name = "COL", EntityId = "1"};
            LastUpdateMapping mapping = new LastUpdateMapping(entity, this._ddb);
            var generateWhere = mapping.GenerateWhere(timeRanges);
            Assert.That(generateWhere.Parameters.All(parameter => parameter.DbType == DbType.DateTime));
            Assert.That(generateWhere.Parameters.Select(parameter => parameter.Value), Is.All.TypeOf<DateTime>());
            return generateWhere.WhereClause;
        }

        /// <summary>
        /// Generate sample data for <see cref="TestLastUpdateMapping.TestWhereClause"/>
        /// </summary>
        /// <seealso cref="System.Collections.IEnumerable" />
        private class GenerateTestWhereClauses : IEnumerable
        {
            /// <summary>
            /// Returns an enumerator that iterates through a collection.
            /// </summary>
            /// <returns>
            /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
            /// </returns>
            public IEnumerator GetEnumerator()
            {
                var startDate = new SdmxDateCore("2010-01-01");
                var endDate = new SdmxDateCore("2011-12-31");
                var timeRange2010to2011 = new TimeRangeCore(true, startDate, endDate, true, true);
                var timeRange2010to2011ex = new TimeRangeCore(true, startDate, endDate, false, false);

                var timeRange2010 = new TimeRangeCore(false, startDate, null, true, true);
                var timeRange2010ex = new TimeRangeCore(false, startDate, null, false, true);

                var timeRangeto2011 = new TimeRangeCore(false, null, endDate, true, true);
                var timeRangeto2011ex = new TimeRangeCore(false, null, endDate, true, false);

                yield return new TestCaseData((object)new ITimeRange[] { timeRange2010to2011 }).Returns("((COL >= @last_up_start_param0) AND (COL <= @last_up_end_param0))").SetName("Inclusive IsRange");
                yield return new TestCaseData((object)new ITimeRange[] { timeRange2010to2011ex }).Returns("((COL > @last_up_start_param0) AND (COL < @last_up_end_param0))").SetName("Exclusive IsRange");
                yield return new TestCaseData((object)new ITimeRange[] { timeRange2010to2011, timeRange2010to2011ex }).Returns("((COL >= @last_up_start_param0) AND (COL <= @last_up_end_param0)) AND ((COL > @last_up_start_param1) AND (COL < @last_up_end_param1))").SetName("Exclusive and Inclusive IsRange");
                yield return new TestCaseData((object)new ITimeRange[] { timeRange2010 }).Returns("(COL >= @last_update_param0)").SetName("Inclusive Start");
                yield return new TestCaseData((object)new ITimeRange[] { timeRange2010ex }).Returns("(COL > @last_update_param0)").SetName("Exclusive Start");
                yield return new TestCaseData((object)new ITimeRange[] { timeRangeto2011 }).Returns("(COL <= @last_update_param0)").SetName("Inclusive End");
                yield return new TestCaseData((object)new ITimeRange[] { timeRangeto2011ex }).Returns("(COL < @last_update_param0)").SetName("Exclusive End");
                yield return new TestCaseData((object)new ITimeRange[] { timeRange2010, timeRange2010to2011, timeRangeto2011ex, timeRange2010to2011ex }).Returns("(COL >= @last_update_param0) AND ((COL >= @last_up_start_param1) AND (COL <= @last_up_end_param1)) AND (COL < @last_update_param2) AND ((COL > @last_up_start_param3) AND (COL < @last_up_end_param3))").SetName("Exclusive and Inclusive IsRange");
            }
        }
    }
}