// -----------------------------------------------------------------------
// <copyright file="TestAuthCachedAdvancedStructureRetriever.cs" company="EUROSTAT">
//   Date Created : 2014-10-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System.Configuration;

    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Structureparser.Workspace;
    using Org.Sdmxsource.Util.Io;

    //[Category("RequiresDB")]




    [TestFixture, Ignore("AuthCachedAdvancedStructureRetriever, AuthAdvancedStructureRetriever are obsolete")]
    [System.Obsolete("since MSDB 7.0; Obsoleted Test Class.")]
    public class TestAuthCachedAdvancedStructureRetriever
    {

        /////// <summary>
        /////// The connection string helper
        /////// </summary>
        ////private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        ////[TestCase("tests/GetCodelist.xml", "sqlserver")]
        ////public void TestGetCodelist(string file, string connection)
        ////{
        ////    IQueryParsingManager parsingManager = new QueryParsingManager(SdmxSchemaEnumType.Null);
        ////    IQueryWorkspace queryWorkspace;
        ////    using (IReadableDataLocation dataLocation = new FileReadableDataLocation(file))
        ////    {
        ////        queryWorkspace = parsingManager.ParseQueries(dataLocation);
        ////    }

        ////    MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["MappingStoreRetrieversFactory"]);
        ////    var connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(connection);
        ////    IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalManager = new AuthAdvancedStructureRetriever(connectionStringSettings);
        ////    var authCachedAdvancedStructureRetriever = new AuthCachedAdvancedStructureRetriever(retrievalManager);
        ////    var maintainableMutableObjects = authCachedAdvancedStructureRetriever.GetMutableMaintainables(
        ////        queryWorkspace.ComplexStructureQuery.StructureReference,
        ////        queryWorkspace.ComplexStructureQuery.StructureQueryMetadata.StructureQueryDetail,
        ////        null);
        ////    Assert.IsNotEmpty(maintainableMutableObjects);
        ////}
    }
}