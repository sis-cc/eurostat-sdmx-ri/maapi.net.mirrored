// -----------------------------------------------------------------------
// <copyright file="TestRetrievalContainer.cs" company="EUROSTAT">
//   Date Created : 2018-03-19
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;
using System.Linq;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace MappingStoreRetrieval.Tests
{
    [TestFixture,Ignore("Structures do not exist in the mastore_test docker image")]
    internal class TestRetrievalContainer
    {
        // TODO: see if we need to rewrite any of these tests for msdb 7.0

        //[Test]
        //public void TestProvisionAgreement()
        //{
        //    var retrievalEngineContainer = new RetrievalEngineContainer(new Database(ConfigurationManager.ConnectionStrings["sqlserver"]));
        //    var agencyId = new ComplexTextReferenceCore(null, TextSearch.FromTextSearchEnumType(TextSearchEnumType.Equal), "ESTAT");
        //    var clid = new ComplexTextReferenceCore(null, TextSearch.FromTextSearchEnumType(TextSearchEnumType.Equal), "DATA_REG_TEST");

        //    IComplexVersionReference versionReference = new ComplexVersionReferenceCore(TertiaryBool.ParseBoolean(false), "1", null, null);

        //    IComplexStructureReferenceObject structureRef = new ComplexStructureReferenceCore(agencyId, clid, versionReference, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow), null, null, null, null);
        //    var provisionAgreement = retrievalEngineContainer.ProvisionAgreementRetrievalEngine.Retrieve(structureRef, ComplexStructureQueryDetailEnumType.Full).FirstOrDefault();
        //    Assert.That(provisionAgreement, Is.Not.Null);
        //    Assert.That(provisionAgreement.DataproviderRef, Is.Not.Null);
        //    Assert.That(provisionAgreement.StructureUsage, Is.Not.Null);
        //}

        //[Test]
        //public void TestReferenceFrom()
        //{
        //    var retrievalEngineContainer = new RetrievalEngineContainer(new Database(ConfigurationManager.ConnectionStrings["sqlserver"]));
        //    var agencyId = new ComplexTextReferenceCore(null, TextSearch.FromTextSearchEnumType(TextSearchEnumType.Equal), "ESTAT");
        //    var clid = new ComplexTextReferenceCore(null, TextSearch.FromTextSearchEnumType(TextSearchEnumType.Equal), "DATA_REG_TEST");

        //    IComplexVersionReference versionReference = new ComplexVersionReferenceCore(TertiaryBool.ParseBoolean(false), "1", null, null);

        //    IComplexStructureReferenceObject structureRef = new ComplexStructureReferenceCore(agencyId, clid, versionReference, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow), null, null, null, null);
        //    var provisionAgreementFromDataFlow = retrievalEngineContainer.ProvisionAgreementRetrievalEngine.RetrieveFromReferenced(new StructureReferenceImpl(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow))
        //    {
        //        AgencyId="ESTAT",
        //        Version = "1.9",
        //        MaintainableId = "NAMAIN_IDC_N"
        //    }, ComplexStructureQueryDetailEnumType.Full).FirstOrDefault();

        //    Assert.That(provisionAgreementFromDataFlow,Is.Not.Null);

        //    var provisionAgreementFromDataProvider = retrievalEngineContainer.ProvisionAgreementRetrievalEngine.RetrieveFromReferenced(new StructureReferenceImpl(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProviderScheme))
        //    {
        //        AgencyId = "ESTAT",
        //        Version = "1.0",
        //        MaintainableId = "DATA_PROVIDERS"
        //    }, ComplexStructureQueryDetailEnumType.Full).FirstOrDefault();

        //    Assert.That(provisionAgreementFromDataProvider, Is.Not.Null);
        //}
    }
}