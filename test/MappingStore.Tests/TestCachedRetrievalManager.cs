﻿// -----------------------------------------------------------------------
// <copyright file="TestCachedRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System.Collections.Generic;
    using System.Configuration;

    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    /// <summary>
    ///     Test unit for <see cref="CachedRetrievalManager" />
    /// </summary>
    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    [Category("RequiresDB")]
    public class TestCachedRetrievalManager : TestMappingStoreRetrievalManager
    {
        public TestCachedRetrievalManager(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Returns the retrieval manager.
        /// </summary>
        /// <param name="css">
        ///     The connection string settings
        /// </param>
        /// <returns>
        /// The <see cref="ISdmxMutableObjectRetrievalManager"/>.
        /// </returns>
        protected override ISdmxMutableObjectRetrievalManager GetRetrievalManager(ConnectionStringSettings css)
        {
            var main = base.GetRetrievalManager(css);
            return new CachedRetrievalManager(null, main);
        }

        /// <summary>
        /// Returns the authorization retrieval manager.
        /// </summary>
        /// <param name="css">
        ///     The connection string settings
        /// </param>
        /// <returns>
        /// The <see cref="ISdmxMutableObjectRetrievalManager"/>.
        /// </returns>
        protected override IAuthSdmxMutableObjectRetrievalManager GetAuthRetrievalManager(ConnectionStringSettings css)
        {
            var authMain = base.GetAuthRetrievalManager(css);
            var main = base.GetRetrievalManager(css);
            return new AuthCachedRetrievalManager(null, authMain);
        }
    }
}