using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Estat.Sri.Sdmx.MappingStore.Retrieve.Helper;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Util.Extensions;

namespace MappingStoreRetrieval.Tests
{
    [TestFixture]
    public class TextFormatValueUtilsTest
    {
        [Test]
        public void TestSentinelToJson()
        {
            ISentinelValueMutableObject sentinelMutableBean = new SentinelValueMutableCore();
            sentinelMutableBean.Value = "-100";
            sentinelMutableBean.AddName("en", "Not applicable");

            ISentinelValueMutableObject sentinelMutableBean2 = new SentinelValueMutableCore();
            sentinelMutableBean2.Value = "test value";
            sentinelMutableBean2.AddName("en", "name en");
            sentinelMutableBean2.AddName("fr", "name fr");
            sentinelMutableBean2.AddDescription("it", "desc it");
            sentinelMutableBean2.AddDescription("el", "desc el");

            IRepresentationMutableObject representationMutableObject = new RepresentationMutableCore();
            representationMutableObject.TextFormat = new TextFormatMutableCore();
            representationMutableObject.TextFormat.SentinelValues.Add(sentinelMutableBean);
            representationMutableObject.TextFormat.SentinelValues.Add(sentinelMutableBean2);


            var textFormatBean = new TextFormatObjectCore(representationMutableObject.TextFormat, null);
            String json = TextFormatValueUtils.CreateJsonFromTextFormatProperties(textFormatBean);

            Assert.IsNotNull(json);
            Assert.AreEqual("{\"t\":\"sentinel\",\"s\":[{\"v\":\"-100\",\"n\":[{\"l\":\"en\",\"v\":\"Not applicable\"}],\"d\":[]},{\"v\":\"test value\",\"n\":[{\"l\":\"en\",\"v\":\"name en\"},{\"l\":\"fr\",\"v\":\"name fr\"}],\"d\":[{\"l\":\"it\",\"v\":\"desc it\"},{\"l\":\"el\",\"v\":\"desc el\"}]}]}", json);
        }

        [Test]
        public void TestJsonToSentinel()
        {
            String json = "{\"t\":\"sentinel\",\"s\":[{\"v\":\"-100\",\"n\":[{\"l\":\"en\",\"v\":\"Not applicable\"}],\"d\":[]},{\"v\":\"test value\",\"n\":[{\"l\":\"en\",\"v\":\"name en\"},{\"l\":\"fr\",\"v\":\"name fr\"}],\"d\":[{\"l\":\"it\",\"v\":\"desc it\"},{\"l\":\"el\",\"v\":\"desc el\"}]}]}";

            var textFormatMutableBean = new TextFormatMutableCore();
            TextFormatValueUtils.PopulateTextFormatPropertiesFromJson(textFormatMutableBean, json);

            Assert.AreEqual(2, textFormatMutableBean.SentinelValues.Count());
            var sentinelMutableBean = textFormatMutableBean.SentinelValues[0];

            Assert.AreEqual("-100", sentinelMutableBean.Value);
            Assert.AreEqual(1, sentinelMutableBean.Names.Count());
            Assert.AreEqual("en", sentinelMutableBean.Names[0].Locale);
            Assert.AreEqual("Not applicable", sentinelMutableBean.Names[0].Value);
            Assert.IsNull(sentinelMutableBean.Descriptions);

            var sentinelMutableBean2 = textFormatMutableBean.SentinelValues[1];
            Assert.AreEqual("test value", sentinelMutableBean2.Value);

            Assert.AreEqual(2, sentinelMutableBean2.Names.Count());
            Assert.AreEqual("en", sentinelMutableBean2.Names[0].Locale);
            Assert.AreEqual("name en", sentinelMutableBean2.Names[0].Value);
            Assert.AreEqual("fr", sentinelMutableBean2.Names[1].Locale);
            Assert.AreEqual("name fr", sentinelMutableBean2.Names[1].Value);
          
            Assert.AreEqual(2, sentinelMutableBean2.Descriptions.Count());
            Assert.AreEqual("it", sentinelMutableBean2.Descriptions[0].Locale);
            Assert.AreEqual("desc it", sentinelMutableBean2.Descriptions[0].Value);
            Assert.AreEqual("el", sentinelMutableBean2.Descriptions[1].Locale);
            Assert.AreEqual("desc el", sentinelMutableBean2.Descriptions[1].Value);
        }
    }
}
