// -----------------------------------------------------------------------
// <copyright file="TestHashAndSalt.cs" company="EUROSTAT">
//   Date Created : 2017-05-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Estat.Sri.Mapping.MappingStore.Engine;
    using Estat.Sri.MappingStoreRetrieval.Builder;

    using NUnit.Framework;

    public class TestHashAndSalt
    {
        private readonly HashBuilder _hashBuilder;
        private readonly SaltBuilder _saltBuilder;

        private readonly BytesConverter _bytesConverter;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public TestHashAndSalt()
        {
            this._bytesConverter = new BytesConverter();
            this._hashBuilder = new HashBuilder(this._bytesConverter);
            this._saltBuilder = new SaltBuilder();
        }

        [Test]
        public void TestSaltIsNotNullAndHasSize16()
        {
            var result = this._saltBuilder.Build();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Length, Is.EqualTo(16));
        }

        [Test]
        public void TestSaltDoesNotRepeatInSmallNumbers()
        {
            ISet<string> set = new HashSet<string>(StringComparer.Ordinal);
            for (int i = 0; i < 1000; i++)
            {
                var str = this._bytesConverter.ConvertToHex(this._saltBuilder.Build());
                Assert.That(set.Add(str));
            }
        }

        [Test]
        public void TestConvertFroMToHex()
        {
            var salt = this._saltBuilder.Build();
            var hex = this._bytesConverter.ConvertToHex(salt);
            var roundTripSalt = this._bytesConverter.ConvertFromHex(hex);
            Assert.That(salt.SequenceEqual(roundTripSalt));
        }

        [Test]
        public void TestConvertToHexSaltSize()
        {
            var salt = this._saltBuilder.Build();
            var hex = this._bytesConverter.ConvertToHex(salt);
            Assert.That(Encoding.ASCII.GetBytes(hex).Length, Is.EqualTo(32));
        }

        [Test]
        public void TestHashBuilderProducesTheSameOutputForSameInput()
        {
            var salt = this._saltBuilder.Build();
            var hash = this._hashBuilder.Build("test123", salt, "SHA-512");
            var hash2 = this._hashBuilder.Build("test123", salt, "SHA-512");
            Assert.That(hash.SequenceEqual(hash2));
        }

        [Test]
        public void TestHashSize()
        {
            var salt = this._saltBuilder.Build();
            var hash = this._hashBuilder.Build("test123", salt, "SHA-512");
            var hex = this._bytesConverter.ConvertToHex(hash);
            Assert.That(Encoding.ASCII.GetBytes(hex).Length, Is.EqualTo(128));
        }

        [Test]
        public void TestHashBuilderProducesTheDifferentOutputForDifferentSalt()
        {
            var salt = this._saltBuilder.Build();
            var hash = this._hashBuilder.Build("test123", salt, "SHA-512");
            var salt2 = this._saltBuilder.Build();
            var hash2 = this._hashBuilder.Build("test123", salt2, "SHA-512");
            Assert.That(!hash.SequenceEqual(hash2));
        }

        [Test]
        public void TestHashBuilderProducesTheDifferentOutputForDifferentPass()
        {
            var salt = this._saltBuilder.Build();
            var hash = this._hashBuilder.Build("test123", salt, "SHA-512");
            var hash2 = this._hashBuilder.Build("test456", salt, "SHA-512");
            Assert.That(!hash.SequenceEqual(hash2));
        }

        [Test]
        public void TestCreatePassword()
        {
            UserConverterEngine engine = new UserConverterEngine();
            var password = "MAWStest*1";
            var details = engine.From(password);
            var algorithm = details.Algorithm;
            var pass = details.Password;
            var salt = details.Salt;
            var saltBytes = this._bytesConverter.ConvertFromHex(salt);
            var providedPasswordBytes = this._hashBuilder.Build(password, saltBytes, algorithm);
            var providedPasswordHex = this._bytesConverter.ConvertToHex(providedPasswordBytes);
            Assert.That(providedPasswordHex, Is.EqualTo(pass));
        }
    }
}