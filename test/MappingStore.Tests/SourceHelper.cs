﻿// -----------------------------------------------------------------------
// <copyright file="SourceHelper.cs" company="EUROSTAT">
//   Date Created : 2017-03-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    public static class SourceHelper
    {
        /// <summary>
        /// Gets the dataflows.
        /// </summary>
        /// <returns>The dataflows of the current DB</returns>
        public static IEnumerable<TMutable> GetArtefact<TMutable>(string connectionName, SdmxStructureEnumType type, Func<ISdmxObjects, IEnumerable<TMutable>> getFromObjects, Func<IEnumerable<TMutable>> getFromDb) 
            where TMutable : IMaintainableMutableObject
        {
            FileInfo dataflows = new FileInfo(connectionName + "-" + type + ".xml");
            IStructureParsingManager parsingManager = new StructureParsingManager();
            IEnumerable<TMutable> dataflowObjects;
            if (dataflows.Exists)
            {
                dataflowObjects = getFromObjects(dataflows.GetSdmxObjects(parsingManager)).ToArray();
            }
            else
            {
                dataflowObjects = getFromDb().ToArray();
                IStructureWriterManager writerManager = new StructureWriterManager();

                IHeader header = new HeaderImpl("ZZ", "ZZ");
                var sdmxObjects = new SdmxObjectsImpl(header, dataflowObjects.Select(o => o.ImmutableInstance));
                var format = new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument));
                using (var outputStream = dataflows.Create())
                {
                    writerManager.WriteStructures(sdmxObjects, format, outputStream);
                }
            }

            return dataflowObjects;
        }
    }
}