﻿// -----------------------------------------------------------------------
// <copyright file="TestSdmxStructureTypeExtensions.cs" company="EUROSTAT">
//   Date Created : 2013-10-02
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System.Configuration;

    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;

    /// <summary>
    /// Test unit for <see cref="SdmxStructureTypeExtensions"/>
    /// </summary>
    [TestFixture]
    public class TestSdmxStructureTypeExtensions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestSdmxStructureTypeExtensions"/> class.
        /// </summary>
        public TestSdmxStructureTypeExtensions()
        {
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["MappingStoreRetrieversFactory"]);
        }

        /// <summary>
        /// Test unit for <see cref="SdmxStructureTypeExtensions.IsDefault{Int32}" />
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="expectedResult">if set to <c>true</c> [expected result].</param>
        [TestCase(0, true)]
        [TestCase(1, false)]
        [TestCase(-1, false)]
        public void TestIsDefaultInt(int value, bool expectedResult)
        {
            Assert.AreEqual(expectedResult, value.IsDefault(), "( {0} == {1} ) != {2}", value, default(int), expectedResult);
        }

        /// <summary>
        /// Test unit for <see cref="SdmxStructureTypeExtensions.IsDefault{String}" />
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="expectedResult">if set to <c>true</c> [expected result].</param>
        [TestCase(null, true)]
        [TestCase("", false)]
        [TestCase("Test", false)]
        public void TestIsDefaultString(string value, bool expectedResult)
        {
            Assert.AreEqual(expectedResult, value.IsDefault(), "( {0} == {1} ) != {2}", value, default(string), expectedResult);
        }

        /// <summary>
        /// Test unit for <see cref="SdmxStructureTypeExtensions.IsDefault{ISdmxMutableObjectRetrievalManager}" />
        /// </summary>
        [Test]
        public void TestIsDefaultISdmxMutableObjectRetrievalManagerNull()
        {
            bool expectedResult = true;
            ISdmxMutableObjectRetrievalManager value = null;
            Assert.AreEqual(expectedResult, value.IsDefault(), "( {0} == {1} ) != {2}", value, default(string), expectedResult);
        }

        /// <summary>
        /// Test unit for <see cref="SdmxStructureTypeExtensions.IsDefault{ISdmxMutableObjectRetrievalManager}" />
        /// </summary>
        [Test]
        public void TestIsDefaultISdmxMutableObjectRetrievalManagerNotNull()
        {
            bool expectedResult = false;
            var value = "Not null";
            Assert.AreEqual(expectedResult, value.IsDefault(), "( {0} == {1} ) != {2}", value, default(string), expectedResult);
        }
    }
}