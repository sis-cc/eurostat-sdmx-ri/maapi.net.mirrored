// -----------------------------------------------------------------------
// <copyright file="TestTimeMapping.cs" company="EUROSTAT">
//   Date Created : 2014-07-11
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Utils;

namespace MappingStoreRetrieval.Tests
{
    using System.Collections;
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;
    using Estat.Sri.Mapping.MappingStore.Builder.MappingLogic;
    using Estat.Sri.Mapping.MappingStore.Factory.MappingLogic;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStoreRetrieval.Config;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    using DataSetColumnEntity = Estat.Sri.Mapping.Api.Model.DataSetColumnEntity;
    using TimeTranscodingEntity = Estat.Sri.Mapping.Api.Model.TimeTranscodingEntity;
    using TranscodingEntity = Estat.Sri.Mapping.Api.Model.TranscodingEntity;

    /// <summary>
    ///     The test Time Dimension Mapping
    /// </summary>
    [TestFixture]
    public class TestTimeMapping
    {
        #region Public Methods and Operators

        /// <summary>
        /// Tests the time 2 column mapping.
        /// </summary>
        /// <param name="yearStart">The year start.</param>
        /// <param name="yearLen">Length of the year.</param>
        /// <param name="periodStart">The period start.</param>
        /// <param name="periodLen">Length of the period.</param>
        /// <param name="databaseType">Type of the database.</param>
        /// <param name="expectedResult">The expected result.</param>
        /// <param name="dateFrom">The date from.</param>
        /// <param name="dateTo">The date to.</param>
        [TestCase(0, 4, 5, 3, MappingStoreDefaultConstants.SqlServerName, 
            "((  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),1,4) = '2005' ) and (  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'MAY' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'JUN' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'JUL' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'AUG' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'SEP' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'OCT' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'NOV' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'DEC' )) )  or (  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),1,4) = '2006' ) and (  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'JAN' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'FEB' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'MAR' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'APR' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'MAY' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'JUN' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'JUL' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'AUG' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,3) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 5, 3, MappingStoreDefaultConstants.OracleName, 
            "((  ( SUBSTR(PERIOD_COL,1,4) = '2005' ) and (  ( SUBSTR(PERIOD_COL,6,3) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'JUN' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'JUL' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'AUG' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'SEP' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'OCT' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'NOV' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'DEC' )) )  or (  ( SUBSTR(PERIOD_COL,1,4) = '2006' ) and (  ( SUBSTR(PERIOD_COL,6,3) = 'JAN' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'FEB' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'MAR' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'APR' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'JUN' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'JUL' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'AUG' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 5, 3, MappingStoreDefaultConstants.MySqlName, 
            "((  ( SUBSTR(PERIOD_COL,1,4) = '2005' ) and (  ( SUBSTR(PERIOD_COL,6,3) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'JUN' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'JUL' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'AUG' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'SEP' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'OCT' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'NOV' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'DEC' )) )  or (  ( SUBSTR(PERIOD_COL,1,4) = '2006' ) and (  ( SUBSTR(PERIOD_COL,6,3) = 'JAN' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'FEB' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'MAR' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'APR' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'JUN' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'JUL' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'AUG' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 5, 3, MappingStoreDefaultConstants.PCAxisName, 
            "((  ( SUBSTR(PERIOD_COL,1,4) = '2005' ) and (  ( SUBSTR(PERIOD_COL,6,3) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'JUN' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'JUL' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'AUG' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'SEP' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'OCT' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'NOV' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'DEC' )) )  or (  ( SUBSTR(PERIOD_COL,1,4) = '2006' ) and (  ( SUBSTR(PERIOD_COL,6,3) = 'JAN' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'FEB' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'MAR' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'APR' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'JUN' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'JUL' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'AUG' ) or  ( SUBSTR(PERIOD_COL,6,3) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 5, -1, MappingStoreDefaultConstants.SqlServerName, 
            "((  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),1,4) = '2005' ) and (  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'MAY' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'JUN' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'JUL' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'AUG' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'SEP' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'OCT' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'NOV' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'DEC' )) )  or (  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),1,4) = '2006' ) and (  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'JAN' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'FEB' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'MAR' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'APR' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'MAY' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'JUN' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'JUL' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'AUG' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 5, -1, MappingStoreDefaultConstants.OracleName, 
            "((  ( SUBSTR(PERIOD_COL,1,4) = '2005' ) and (  ( SUBSTR(PERIOD_COL,6) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUN' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUL' ) or  ( SUBSTR(PERIOD_COL,6) = 'AUG' ) or  ( SUBSTR(PERIOD_COL,6) = 'SEP' ) or  ( SUBSTR(PERIOD_COL,6) = 'OCT' ) or  ( SUBSTR(PERIOD_COL,6) = 'NOV' ) or  ( SUBSTR(PERIOD_COL,6) = 'DEC' )) )  or (  ( SUBSTR(PERIOD_COL,1,4) = '2006' ) and (  ( SUBSTR(PERIOD_COL,6) = 'JAN' ) or  ( SUBSTR(PERIOD_COL,6) = 'FEB' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAR' ) or  ( SUBSTR(PERIOD_COL,6) = 'APR' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUN' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUL' ) or  ( SUBSTR(PERIOD_COL,6) = 'AUG' ) or  ( SUBSTR(PERIOD_COL,6) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 5, -1, MappingStoreDefaultConstants.MySqlName, 
            "((  ( SUBSTR(PERIOD_COL,1,4) = '2005' ) and (  ( SUBSTR(PERIOD_COL,6) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUN' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUL' ) or  ( SUBSTR(PERIOD_COL,6) = 'AUG' ) or  ( SUBSTR(PERIOD_COL,6) = 'SEP' ) or  ( SUBSTR(PERIOD_COL,6) = 'OCT' ) or  ( SUBSTR(PERIOD_COL,6) = 'NOV' ) or  ( SUBSTR(PERIOD_COL,6) = 'DEC' )) )  or (  ( SUBSTR(PERIOD_COL,1,4) = '2006' ) and (  ( SUBSTR(PERIOD_COL,6) = 'JAN' ) or  ( SUBSTR(PERIOD_COL,6) = 'FEB' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAR' ) or  ( SUBSTR(PERIOD_COL,6) = 'APR' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUN' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUL' ) or  ( SUBSTR(PERIOD_COL,6) = 'AUG' ) or  ( SUBSTR(PERIOD_COL,6) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 5, -1, MappingStoreDefaultConstants.PCAxisName, 
            "((  ( SUBSTR(PERIOD_COL,1,4) = '2005' ) and (  ( SUBSTR(PERIOD_COL,6) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUN' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUL' ) or  ( SUBSTR(PERIOD_COL,6) = 'AUG' ) or  ( SUBSTR(PERIOD_COL,6) = 'SEP' ) or  ( SUBSTR(PERIOD_COL,6) = 'OCT' ) or  ( SUBSTR(PERIOD_COL,6) = 'NOV' ) or  ( SUBSTR(PERIOD_COL,6) = 'DEC' )) )  or (  ( SUBSTR(PERIOD_COL,1,4) = '2006' ) and (  ( SUBSTR(PERIOD_COL,6) = 'JAN' ) or  ( SUBSTR(PERIOD_COL,6) = 'FEB' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAR' ) or  ( SUBSTR(PERIOD_COL,6) = 'APR' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUN' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUL' ) or  ( SUBSTR(PERIOD_COL,6) = 'AUG' ) or  ( SUBSTR(PERIOD_COL,6) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]

        //// same year
        [TestCase(0, 4, 5, -1, MappingStoreDefaultConstants.SqlServerName, 
            "((  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),1,4) = '2005' ) and (  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'JAN' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'FEB' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'MAR' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'APR' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'MAY' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'JUN' )) ) )", 
            "2005-01", "2005-06")]
        [TestCase(0, 4, 5, -1, MappingStoreDefaultConstants.OracleName, 
            "((  ( SUBSTR(PERIOD_COL,1,4) = '2005' ) and (  ( SUBSTR(PERIOD_COL,6) = 'JAN' ) or  ( SUBSTR(PERIOD_COL,6) = 'FEB' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAR' ) or  ( SUBSTR(PERIOD_COL,6) = 'APR' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUN' )) ) )", 
            "2005-01", "2005-06")]
        [TestCase(0, 4, 5, -1, MappingStoreDefaultConstants.MySqlName, 
            "((  ( SUBSTR(PERIOD_COL,1,4) = '2005' ) and (  ( SUBSTR(PERIOD_COL,6) = 'JAN' ) or  ( SUBSTR(PERIOD_COL,6) = 'FEB' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAR' ) or  ( SUBSTR(PERIOD_COL,6) = 'APR' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUN' )) ) )", 
            "2005-01", "2005-06")]
        [TestCase(0, 4, 5, -1, MappingStoreDefaultConstants.PCAxisName, 
            "((  ( SUBSTR(PERIOD_COL,1,4) = '2005' ) and (  ( SUBSTR(PERIOD_COL,6) = 'JAN' ) or  ( SUBSTR(PERIOD_COL,6) = 'FEB' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAR' ) or  ( SUBSTR(PERIOD_COL,6) = 'APR' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUN' )) ) )", 
            "2005-01", "2005-06")]
        public void TestTime1ColumnMapping(int yearStart, int yearLen, int periodStart, int periodLen, string databaseType, string expectedResult, string dateFrom, string dateTo)
        {
            var mappingEntity = new  TimeDimensionMappingEntity() {   EntityId = "130" , TimeMappingType = TimeDimensionMappingType.TranscodingWithFrequencyDimension};
            var dataSetColumnEntity = new DataSetColumnEntity() { EntityId = "1",  Name = "PERIOD_COL" };
            var timeTranscodingEntity = new TimeTranscodingEntity() { ParentId = "4", Frequency = "M" };
            timeTranscodingEntity.Year = new TimeTranscoding() {Column = dataSetColumnEntity, Length = yearLen, Start = yearStart};
            timeTranscodingEntity.Period = new PeriodTimeTranscoding() { Column = dataSetColumnEntity, Length = periodLen, Start = periodStart };
            var localCodes = new[] { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
            var sdmxCodes = new[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
            for (int i = 0; i < localCodes.Length; i++)
            {
                timeTranscodingEntity.Period.AddRule(sdmxCodes[i], localCodes[i]);
            }
            var oldStyle = new List<TimeTranscodingEntity>();
            oldStyle.Add(timeTranscodingEntity);
            mappingEntity.Transcoding = TimeTranscodingConversionHelper.Convert(oldStyle, "FREQ");
            var expression = new TimeExpressionEntity(timeTranscodingEntity, mappingEntity);
            ITimeDimensionMappingBuilder mapping = new TimeDimension1Column(mappingEntity, expression, databaseType);
            string generateWhere = mapping.GenerateWhere(new SdmxDateCore(dateFrom), new SdmxDateCore(dateTo));
            Assert.NotNull(generateWhere);
            Assert.AreEqual(expectedResult, generateWhere.Trim());
        }

        /// <summary>
        /// Tests the time1 column mapping multi.
        /// </summary>
        /// <param name="databaseType">
        /// Type of the database.
        /// </param>
        /// <param name="expectedResult">
        /// The expected result.
        /// </param>
        /// <param name="dateFrom">
        /// The date from.
        /// </param>
        /// <param name="dateTo">
        /// The date to.
        /// </param>
        /// <param name="freqs">
        /// The frequencies.
        /// </param>
        /// <param name="exprs">
        /// The expressions.
        /// </param>
        [TestCase(MappingStoreDefaultConstants.SqlServerName, 
            "( ( ((  (( FREQ_COL LIKE 'MX%' ))  ) and ( ((  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),1,4) = '2005' ) and (  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'JAN' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'FEB' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'MAR' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'APR' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'MAY' ) or  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),6,100) = 'JUN' )) ) ) )) ) OR  ( ((  (( FREQ_COL LIKE 'AX%' ))  ) and ( ( ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),1,4) >= '2005' ) and  ( SUBSTRING(CAST(PERIOD_COL AS VARCHAR),1,4) <= '2005' )) )) ))", 
            "2005-01", "2005-06", new[] { "M", "A" })]
        [TestCase(MappingStoreDefaultConstants.OracleName, 
            "( ( ((  (( FREQ_COL LIKE 'MX%' ))  ) and ( ((  ( SUBSTR(PERIOD_COL,1,4) = '2005' ) and (  ( SUBSTR(PERIOD_COL,6) = 'JAN' ) or  ( SUBSTR(PERIOD_COL,6) = 'FEB' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAR' ) or  ( SUBSTR(PERIOD_COL,6) = 'APR' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUN' )) ) ) )) ) OR  ( ((  (( FREQ_COL LIKE 'AX%' ))  ) and ( ( ( SUBSTR(PERIOD_COL,1,4) >= '2005' ) and  ( SUBSTR(PERIOD_COL,1,4) <= '2005' )) )) ))", 
            "2005-01", "2005-06", new[] { "M", "A" })]
        [TestCase(MappingStoreDefaultConstants.MySqlName, 
            "( ( ((  (( FREQ_COL LIKE 'MX%' ))  ) and ( ((  ( SUBSTR(PERIOD_COL,1,4) = '2005' ) and (  ( SUBSTR(PERIOD_COL,6) = 'JAN' ) or  ( SUBSTR(PERIOD_COL,6) = 'FEB' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAR' ) or  ( SUBSTR(PERIOD_COL,6) = 'APR' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUN' )) ) ) )) ) OR  ( ((  (( FREQ_COL LIKE 'AX%' ))  ) and ( ( ( SUBSTR(PERIOD_COL,1,4) >= '2005' ) and  ( SUBSTR(PERIOD_COL,1,4) <= '2005' )) )) ))", 
            "2005-01", "2005-06", new[] { "M", "A" })]
        [TestCase(MappingStoreDefaultConstants.PCAxisName, 
            "( ( ((  (( FREQ_COL LIKE 'MX%' ))  ) and ( ((  ( SUBSTR(PERIOD_COL,1,4) = '2005' ) and (  ( SUBSTR(PERIOD_COL,6) = 'JAN' ) or  ( SUBSTR(PERIOD_COL,6) = 'FEB' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAR' ) or  ( SUBSTR(PERIOD_COL,6) = 'APR' ) or  ( SUBSTR(PERIOD_COL,6) = 'MAY' ) or  ( SUBSTR(PERIOD_COL,6) = 'JUN' )) ) ) )) ) OR  ( ((  (( FREQ_COL LIKE 'AX%' ))  ) and ( ( ( SUBSTR(PERIOD_COL,1,4) >= '2005' ) and  ( SUBSTR(PERIOD_COL,1,4) <= '2005' )) )) ))", 
            "2005-01", "2005-06", new[] { "M", "A" })]
        public void TestTime1ColumnMappingMulti(string databaseType, string expectedResult, string dateFrom, string dateTo, string[] freqs)
        {
            var mappingEntity = new TimeDimensionMappingEntity()
            {
                EntityId = "130", TimeMappingType = TimeDimensionMappingType.TranscodingWithFrequencyDimension
            };
            var periodColumn = new DataSetColumnEntity() { EntityId = "1",  Name = "PERIOD_COL"};
            var oldStyle = new List<TimeTranscodingEntity>();
            for (int index = 0; index < freqs.Length; index++)
            {
                var freq = freqs[index];
                var timeTranscodingEntity = BuildTimeTranscodingEntity(freq);

                oldStyle.Add(timeTranscodingEntity);
            }

            mappingEntity.Transcoding = TimeTranscodingConversionHelper.Convert(oldStyle, "FREQ");
            IComponentMappingBuilder frequencyMapping = BuildFrequencyComponentMapping();
            ITimeDimensionFrequencyMappingBuilder mapping =
                new TimeDimensionFrequencyFactory(
                    new TimeDimensionMappingWithTranscodingFactory(),
                    new TimeDimensionMappingWithoutTranscoding()).GetBuilder(new TimeDimensionMappingSettings(mappingEntity, databaseType, frequencyMapping));
            Assert.That(mapping, Is.Not.Null);
            string generateWhere = mapping.GenerateWhere(new SdmxDateCore(dateFrom), new SdmxDateCore(dateTo), null);
            Assert.NotNull(generateWhere);
            Assert.AreEqual(expectedResult, generateWhere.Trim());
        }

        [TestCase(MappingStoreDefaultConstants.SqlServerName,
    "(((  (( FREQ_COL LIKE 'AX%' ))  ) and ( ( ( CAST(PERIOD_COL AS VARCHAR) = '2005' ))  )) OR ((  ( FREQ_COL LIKE 'S%' )  ) and ( ( ( CAST(PERIOD_COL AS VARCHAR) = '2005-S1' ))  )) OR ((  ( FREQ_COL LIKE 'T%' )  ) and ( ( ( CAST(PERIOD_COL AS VARCHAR) >= '2005-T1' ) and  ( CAST(PERIOD_COL AS VARCHAR) <= '2005-T2' ))  )) OR ((  (( FREQ_COL LIKE 'QX%' ))  ) and ( ( ( CAST(PERIOD_COL AS VARCHAR) >= '2005-Q1' ) and  ( CAST(PERIOD_COL AS VARCHAR) <= '2005-Q2' ))  )) OR ((  (( FREQ_COL LIKE 'MX%' ))  ) and ( ( ( CAST(PERIOD_COL AS VARCHAR) >= '2005-01' ) and  ( CAST(PERIOD_COL AS VARCHAR) <= '2005-06' ))  )) OR ((  ( FREQ_COL LIKE 'W%' )  ) and ( ( ( CAST(PERIOD_COL AS VARCHAR) >= '2004-W53' ) and  ( CAST(PERIOD_COL AS VARCHAR) <= '2005-W26' ))  )) OR ((  ( FREQ_COL LIKE 'D%' )  ) and ( ( ( CAST(PERIOD_COL AS VARCHAR) >= '2005-01-01' ) and  ( CAST(PERIOD_COL AS VARCHAR) <= '2005-06-30' ))  )) OR ((  ( FREQ_COL LIKE 'H%' )  ) and ( ( ( CAST(PERIOD_COL AS VARCHAR) >= '2005-01-01T00:00:00' ) and  ( CAST(PERIOD_COL AS VARCHAR) <= '2005-06-30T23:59:59' ))  )) OR ((  ( FREQ_COL LIKE 'I%' )  ) and ( ( ( CAST(PERIOD_COL AS VARCHAR) >= '2005-01-01T00:00:00' ) and  ( CAST(PERIOD_COL AS VARCHAR) <= '2005-06-30T23:59:59' ))  )) OR ((  (( FREQ_COL LIKE 'AX%' ))  ) and ( ( ( CAST(PERIOD_COL AS VARCHAR) = '2005-A1' ))  )) OR ((  (( FREQ_COL LIKE 'MX%' ))  ) and ( ( ( CAST(PERIOD_COL AS VARCHAR) >= '2005-M01' ) and  ( CAST(PERIOD_COL AS VARCHAR) <= '2005-M06' ))  )) OR ((  ( FREQ_COL LIKE 'D%' )  ) and ( ( ( CAST(PERIOD_COL AS VARCHAR) >= '2005-D001' ) and  ( CAST(PERIOD_COL AS VARCHAR) <= '2005-D181' ))  )))",
            "2005-01-01", "2005-06-30")]
        [TestCase(MappingStoreDefaultConstants.OracleName,
    "(((  (( FREQ_COL LIKE 'AX%' ))  ) and ( ( ( PERIOD_COL = '2005' ))  )) OR ((  ( FREQ_COL LIKE 'S%' )  ) and ( ( ( PERIOD_COL = '2005-S1' ))  )) OR ((  ( FREQ_COL LIKE 'T%' )  ) and ( ( ( PERIOD_COL >= '2005-T1' ) and  ( PERIOD_COL <= '2005-T2' ))  )) OR ((  (( FREQ_COL LIKE 'QX%' ))  ) and ( ( ( PERIOD_COL >= '2005-Q1' ) and  ( PERIOD_COL <= '2005-Q2' ))  )) OR ((  (( FREQ_COL LIKE 'MX%' ))  ) and ( ( ( PERIOD_COL >= '2005-01' ) and  ( PERIOD_COL <= '2005-06' ))  )) OR ((  ( FREQ_COL LIKE 'W%' )  ) and ( ( ( PERIOD_COL >= '2004-W53' ) and  ( PERIOD_COL <= '2005-W26' ))  )) OR ((  ( FREQ_COL LIKE 'D%' )  ) and ( ( ( PERIOD_COL >= '2005-01-01' ) and  ( PERIOD_COL <= '2005-06-30' ))  )) OR ((  ( FREQ_COL LIKE 'H%' )  ) and ( ( ( PERIOD_COL >= '2005-01-01T00:00:00' ) and  ( PERIOD_COL <= '2005-06-30T23:59:59' ))  )) OR ((  ( FREQ_COL LIKE 'I%' )  ) and ( ( ( PERIOD_COL >= '2005-01-01T00:00:00' ) and  ( PERIOD_COL <= '2005-06-30T23:59:59' ))  )) OR ((  (( FREQ_COL LIKE 'AX%' ))  ) and ( ( ( PERIOD_COL = '2005-A1' ))  )) OR ((  (( FREQ_COL LIKE 'MX%' ))  ) and ( ( ( PERIOD_COL >= '2005-M01' ) and  ( PERIOD_COL <= '2005-M06' ))  )) OR ((  ( FREQ_COL LIKE 'D%' )  ) and ( ( ( PERIOD_COL >= '2005-D001' ) and  ( PERIOD_COL <= '2005-D181' ))  )))",
            "2005-01-01", "2005-06-30")]
        [TestCase(MappingStoreDefaultConstants.MySqlName,
    "(((  (( FREQ_COL LIKE 'AX%' ))  ) and ( ( ( PERIOD_COL = '2005' ))  )) OR ((  ( FREQ_COL LIKE 'S%' )  ) and ( ( ( PERIOD_COL = '2005-S1' ))  )) OR ((  ( FREQ_COL LIKE 'T%' )  ) and ( ( ( PERIOD_COL >= '2005-T1' ) and  ( PERIOD_COL <= '2005-T2' ))  )) OR ((  (( FREQ_COL LIKE 'QX%' ))  ) and ( ( ( PERIOD_COL >= '2005-Q1' ) and  ( PERIOD_COL <= '2005-Q2' ))  )) OR ((  (( FREQ_COL LIKE 'MX%' ))  ) and ( ( ( PERIOD_COL >= '2005-01' ) and  ( PERIOD_COL <= '2005-06' ))  )) OR ((  ( FREQ_COL LIKE 'W%' )  ) and ( ( ( PERIOD_COL >= '2004-W53' ) and  ( PERIOD_COL <= '2005-W26' ))  )) OR ((  ( FREQ_COL LIKE 'D%' )  ) and ( ( ( PERIOD_COL >= '2005-01-01' ) and  ( PERIOD_COL <= '2005-06-30' ))  )) OR ((  ( FREQ_COL LIKE 'H%' )  ) and ( ( ( PERIOD_COL >= '2005-01-01T00:00:00' ) and  ( PERIOD_COL <= '2005-06-30T23:59:59' ))  )) OR ((  ( FREQ_COL LIKE 'I%' )  ) and ( ( ( PERIOD_COL >= '2005-01-01T00:00:00' ) and  ( PERIOD_COL <= '2005-06-30T23:59:59' ))  )) OR ((  (( FREQ_COL LIKE 'AX%' ))  ) and ( ( ( PERIOD_COL = '2005-A1' ))  )) OR ((  (( FREQ_COL LIKE 'MX%' ))  ) and ( ( ( PERIOD_COL >= '2005-M01' ) and  ( PERIOD_COL <= '2005-M06' ))  )) OR ((  ( FREQ_COL LIKE 'D%' )  ) and ( ( ( PERIOD_COL >= '2005-D001' ) and  ( PERIOD_COL <= '2005-D181' ))  )))",
            "2005-01-01", "2005-06-30")]
        public void TestTimeSingle(string databaseType, string expectedResult, string dateFrom, string dateTo)
        {
            var mappingEntity = new  TimeDimensionMappingEntity() {   EntityId = "130", TimeMappingType = TimeDimensionMappingType.MappingWithColumn};
            mappingEntity.SimpleMappingColumn = "PERIOD_COL";
            IComponentMappingBuilder frequencyMapping = BuildFrequencyComponentMapping();
            ITimeDimensionFrequencyMappingBuilder mapping =
                new TimeDimensionFrequencyFactory(
                    new TimeDimensionMappingWithTranscodingFactory(),
                    new TimeDimensionMappingWithoutTranscoding()).GetBuilder(new TimeDimensionMappingSettings(mappingEntity, null, databaseType, frequencyMapping));
            Assert.That(mapping, Is.Not.Null);
            string generateWhere = mapping.GenerateWhere(new SdmxDateCore(dateFrom), new SdmxDateCore(dateTo), null);
            Assert.NotNull(generateWhere);
            Assert.AreEqual(expectedResult, generateWhere.Trim());
        }

        [TestCase(MappingStoreDefaultConstants.SqlServerName,
            "", new [] { "M", "A" })]
        [TestCase(MappingStoreDefaultConstants.OracleName,
            "", new [] { "M", "A" })]
        [TestCase(MappingStoreDefaultConstants.MySqlName,
            "", new [] { "M", "A" })]
        [TestCase(MappingStoreDefaultConstants.PCAxisName,
            "", new [] { "M", "A" })]
        [TestCase(MappingStoreDefaultConstants.SqlServerName,
           "", new [] { "M" })]
        [TestCase(MappingStoreDefaultConstants.OracleName,
            "", new [] { "M" })]
        [TestCase(MappingStoreDefaultConstants.MySqlName,
            "", new [] { "M" })]
        [TestCase(MappingStoreDefaultConstants.PCAxisName,
            "", new [] { "M" })]
        [TestCase(MappingStoreDefaultConstants.SqlServerName,
           "", new [] { "A" })]
        [TestCase(MappingStoreDefaultConstants.OracleName,
            "", new [] { "A" })]
        [TestCase(MappingStoreDefaultConstants.MySqlName,
            "", new [] { "A" })]
        [TestCase(MappingStoreDefaultConstants.PCAxisName,
            "", new [] { "A" })]
        public void TestTime1ColumnMappingMultiNoCriteria(string databaseType, string expectedResult, string [] freqs)
        {
            var mappingEntity = new  TimeDimensionMappingEntity() {   EntityId = "130", TimeMappingType = TimeDimensionMappingType.TranscodingWithFrequencyDimension};
            
            var oldStyle = new List<TimeTranscodingEntity>();
            for (int index = 0; index < freqs.Length; index++)
            {
                var freq = freqs [index];
                var timeTranscodingEntity = BuildTimeTranscodingEntity(freq);

                oldStyle.Add(timeTranscodingEntity);
            }

            mappingEntity.Transcoding = TimeTranscodingConversionHelper.Convert(oldStyle, "FREQ");
            IComponentMappingBuilder frequencyMapping = BuildFrequencyComponentMapping();
            ITimeDimensionFrequencyMappingBuilder mapping =
              new TimeDimensionFrequencyFactory(
                  new TimeDimensionMappingWithTranscodingFactory(),
                  new TimeDimensionMappingWithoutTranscoding()).GetBuilder(new TimeDimensionMappingSettings(mappingEntity,  databaseType, frequencyMapping));
            string generateWhere = mapping.GenerateWhere(null, null, null);
            Assert.NotNull(generateWhere);
            Assert.AreEqual(expectedResult, generateWhere.Trim());
        }

        /// <summary>
        /// Tests the time 2 column mapping.
        /// </summary>
        /// <param name="expr">
        /// The expression.
        /// </param>
        /// <param name="databaseType">
        /// Type of the database.
        /// </param>
        /// <param name="expectedResult">
        /// The expected result.
        /// </param>
        /// <param name="dateFrom">
        /// The date from.
        /// </param>
        /// <param name="dateTo">
        /// The date to.
        /// </param>
        [TestCase(0, 0, 0, 0, MappingStoreDefaultConstants.SqlServerName, 
            "((  ( SUBSTRING(CAST(YEAR_INT AS VARCHAR),1,4) = '2001' ) and ( ( MONTH = 'JAN' ) or ( MONTH = 'FEB' ) or ( MONTH = 'MAR' ) or ( MONTH = 'APR' ) or ( MONTH = 'MAY' ) or ( MONTH = 'JUN' ) or ( MONTH = 'JUL' ) or ( MONTH = 'AUG' ) or ( MONTH = 'SEP' ) or ( MONTH = 'OCT' ) or ( MONTH = 'NOV' ) or ( MONTH = 'DEC' )) )  or  ( ( SUBSTRING(CAST(YEAR_INT AS VARCHAR),1,4) >= '2002' ) and  ( SUBSTRING(CAST(YEAR_INT AS VARCHAR),1,4) <= '2002' )) or (  ( SUBSTRING(CAST(YEAR_INT AS VARCHAR),1,4) = '2003' ) and ( ( MONTH = 'JAN' ) or ( MONTH = 'FEB' )) ) )", 
            "2001-01", "2003-02")]
        [TestCase(0, 0, 0, 0, MappingStoreDefaultConstants.OracleName, 
            "((  ( SUBSTR(YEAR_INT,1,4) = '2001' ) and ( ( MONTH = 'JAN' ) or ( MONTH = 'FEB' ) or ( MONTH = 'MAR' ) or ( MONTH = 'APR' ) or ( MONTH = 'MAY' ) or ( MONTH = 'JUN' ) or ( MONTH = 'JUL' ) or ( MONTH = 'AUG' ) or ( MONTH = 'SEP' ) or ( MONTH = 'OCT' ) or ( MONTH = 'NOV' ) or ( MONTH = 'DEC' )) )  or  ( ( SUBSTR(YEAR_INT,1,4) >= '2002' ) and  ( SUBSTR(YEAR_INT,1,4) <= '2002' )) or (  ( SUBSTR(YEAR_INT,1,4) = '2003' ) and ( ( MONTH = 'JAN' ) or ( MONTH = 'FEB' )) ) )", 
            "2001-01", "2003-02")]
        [TestCase(0, 0, 0, 0, MappingStoreDefaultConstants.MySqlName, 
            "((  ( SUBSTR(YEAR_INT,1,4) = '2001' ) and ( ( MONTH = 'JAN' ) or ( MONTH = 'FEB' ) or ( MONTH = 'MAR' ) or ( MONTH = 'APR' ) or ( MONTH = 'MAY' ) or ( MONTH = 'JUN' ) or ( MONTH = 'JUL' ) or ( MONTH = 'AUG' ) or ( MONTH = 'SEP' ) or ( MONTH = 'OCT' ) or ( MONTH = 'NOV' ) or ( MONTH = 'DEC' )) )  or  ( ( SUBSTR(YEAR_INT,1,4) >= '2002' ) and  ( SUBSTR(YEAR_INT,1,4) <= '2002' )) or (  ( SUBSTR(YEAR_INT,1,4) = '2003' ) and ( ( MONTH = 'JAN' ) or ( MONTH = 'FEB' )) ) )", 
            "2001-01", "2003-02")]
        [TestCase(0, 0, 0, 0, MappingStoreDefaultConstants.PCAxisName, 
            "((  ( SUBSTR(YEAR_INT,1,4) = '2001' ) and ( ( MONTH = 'JAN' ) or ( MONTH = 'FEB' ) or ( MONTH = 'MAR' ) or ( MONTH = 'APR' ) or ( MONTH = 'MAY' ) or ( MONTH = 'JUN' ) or ( MONTH = 'JUL' ) or ( MONTH = 'AUG' ) or ( MONTH = 'SEP' ) or ( MONTH = 'OCT' ) or ( MONTH = 'NOV' ) or ( MONTH = 'DEC' )) )  or  ( ( SUBSTR(YEAR_INT,1,4) >= '2002' ) and  ( SUBSTR(YEAR_INT,1,4) <= '2002' )) or (  ( SUBSTR(YEAR_INT,1,4) = '2003' ) and ( ( MONTH = 'JAN' ) or ( MONTH = 'FEB' )) ) )", 
            "2001-01", "2003-02")]
        [TestCase(0, 4, 0, 3, MappingStoreDefaultConstants.SqlServerName, 
            "((  ( SUBSTRING(CAST(YEAR_INT AS VARCHAR),1,4) = '2005' ) and (  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'MAY' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'JUN' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'JUL' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'AUG' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'SEP' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'OCT' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'NOV' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'DEC' )) )  or (  ( SUBSTRING(CAST(YEAR_INT AS VARCHAR),1,4) = '2006' ) and (  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'JAN' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'FEB' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'MAR' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'APR' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'MAY' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'JUN' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'JUL' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'AUG' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,3) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 0, 3, MappingStoreDefaultConstants.OracleName, 
            "((  ( SUBSTR(YEAR_INT,1,4) = '2005' ) and (  ( SUBSTR(MONTH,1,3) = 'MAY' ) or  ( SUBSTR(MONTH,1,3) = 'JUN' ) or  ( SUBSTR(MONTH,1,3) = 'JUL' ) or  ( SUBSTR(MONTH,1,3) = 'AUG' ) or  ( SUBSTR(MONTH,1,3) = 'SEP' ) or  ( SUBSTR(MONTH,1,3) = 'OCT' ) or  ( SUBSTR(MONTH,1,3) = 'NOV' ) or  ( SUBSTR(MONTH,1,3) = 'DEC' )) )  or (  ( SUBSTR(YEAR_INT,1,4) = '2006' ) and (  ( SUBSTR(MONTH,1,3) = 'JAN' ) or  ( SUBSTR(MONTH,1,3) = 'FEB' ) or  ( SUBSTR(MONTH,1,3) = 'MAR' ) or  ( SUBSTR(MONTH,1,3) = 'APR' ) or  ( SUBSTR(MONTH,1,3) = 'MAY' ) or  ( SUBSTR(MONTH,1,3) = 'JUN' ) or  ( SUBSTR(MONTH,1,3) = 'JUL' ) or  ( SUBSTR(MONTH,1,3) = 'AUG' ) or  ( SUBSTR(MONTH,1,3) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 0, 3, MappingStoreDefaultConstants.MySqlName, 
            "((  ( SUBSTR(YEAR_INT,1,4) = '2005' ) and (  ( SUBSTR(MONTH,1,3) = 'MAY' ) or  ( SUBSTR(MONTH,1,3) = 'JUN' ) or  ( SUBSTR(MONTH,1,3) = 'JUL' ) or  ( SUBSTR(MONTH,1,3) = 'AUG' ) or  ( SUBSTR(MONTH,1,3) = 'SEP' ) or  ( SUBSTR(MONTH,1,3) = 'OCT' ) or  ( SUBSTR(MONTH,1,3) = 'NOV' ) or  ( SUBSTR(MONTH,1,3) = 'DEC' )) )  or (  ( SUBSTR(YEAR_INT,1,4) = '2006' ) and (  ( SUBSTR(MONTH,1,3) = 'JAN' ) or  ( SUBSTR(MONTH,1,3) = 'FEB' ) or  ( SUBSTR(MONTH,1,3) = 'MAR' ) or  ( SUBSTR(MONTH,1,3) = 'APR' ) or  ( SUBSTR(MONTH,1,3) = 'MAY' ) or  ( SUBSTR(MONTH,1,3) = 'JUN' ) or  ( SUBSTR(MONTH,1,3) = 'JUL' ) or  ( SUBSTR(MONTH,1,3) = 'AUG' ) or  ( SUBSTR(MONTH,1,3) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 0, 3, MappingStoreDefaultConstants.PCAxisName, 
            "((  ( SUBSTR(YEAR_INT,1,4) = '2005' ) and (  ( SUBSTR(MONTH,1,3) = 'MAY' ) or  ( SUBSTR(MONTH,1,3) = 'JUN' ) or  ( SUBSTR(MONTH,1,3) = 'JUL' ) or  ( SUBSTR(MONTH,1,3) = 'AUG' ) or  ( SUBSTR(MONTH,1,3) = 'SEP' ) or  ( SUBSTR(MONTH,1,3) = 'OCT' ) or  ( SUBSTR(MONTH,1,3) = 'NOV' ) or  ( SUBSTR(MONTH,1,3) = 'DEC' )) )  or (  ( SUBSTR(YEAR_INT,1,4) = '2006' ) and (  ( SUBSTR(MONTH,1,3) = 'JAN' ) or  ( SUBSTR(MONTH,1,3) = 'FEB' ) or  ( SUBSTR(MONTH,1,3) = 'MAR' ) or  ( SUBSTR(MONTH,1,3) = 'APR' ) or  ( SUBSTR(MONTH,1,3) = 'MAY' ) or  ( SUBSTR(MONTH,1,3) = 'JUN' ) or  ( SUBSTR(MONTH,1,3) = 'JUL' ) or  ( SUBSTR(MONTH,1,3) = 'AUG' ) or  ( SUBSTR(MONTH,1,3) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 0, -1, MappingStoreDefaultConstants.SqlServerName, 
            "((  ( SUBSTRING(CAST(YEAR_INT AS VARCHAR),1,4) = '2005' ) and (  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'MAY' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'JUN' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'JUL' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'AUG' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'SEP' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'OCT' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'NOV' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'DEC' )) )  or (  ( SUBSTRING(CAST(YEAR_INT AS VARCHAR),1,4) = '2006' ) and (  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'JAN' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'FEB' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'MAR' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'APR' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'MAY' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'JUN' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'JUL' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'AUG' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 0, -1, MappingStoreDefaultConstants.OracleName, 
            "((  ( SUBSTR(YEAR_INT,1,4) = '2005' ) and (  ( SUBSTR(MONTH,1) = 'MAY' ) or  ( SUBSTR(MONTH,1) = 'JUN' ) or  ( SUBSTR(MONTH,1) = 'JUL' ) or  ( SUBSTR(MONTH,1) = 'AUG' ) or  ( SUBSTR(MONTH,1) = 'SEP' ) or  ( SUBSTR(MONTH,1) = 'OCT' ) or  ( SUBSTR(MONTH,1) = 'NOV' ) or  ( SUBSTR(MONTH,1) = 'DEC' )) )  or (  ( SUBSTR(YEAR_INT,1,4) = '2006' ) and (  ( SUBSTR(MONTH,1) = 'JAN' ) or  ( SUBSTR(MONTH,1) = 'FEB' ) or  ( SUBSTR(MONTH,1) = 'MAR' ) or  ( SUBSTR(MONTH,1) = 'APR' ) or  ( SUBSTR(MONTH,1) = 'MAY' ) or  ( SUBSTR(MONTH,1) = 'JUN' ) or  ( SUBSTR(MONTH,1) = 'JUL' ) or  ( SUBSTR(MONTH,1) = 'AUG' ) or  ( SUBSTR(MONTH,1) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 0, -1, MappingStoreDefaultConstants.MySqlName, 
            "((  ( SUBSTR(YEAR_INT,1,4) = '2005' ) and (  ( SUBSTR(MONTH,1) = 'MAY' ) or  ( SUBSTR(MONTH,1) = 'JUN' ) or  ( SUBSTR(MONTH,1) = 'JUL' ) or  ( SUBSTR(MONTH,1) = 'AUG' ) or  ( SUBSTR(MONTH,1) = 'SEP' ) or  ( SUBSTR(MONTH,1) = 'OCT' ) or  ( SUBSTR(MONTH,1) = 'NOV' ) or  ( SUBSTR(MONTH,1) = 'DEC' )) )  or (  ( SUBSTR(YEAR_INT,1,4) = '2006' ) and (  ( SUBSTR(MONTH,1) = 'JAN' ) or  ( SUBSTR(MONTH,1) = 'FEB' ) or  ( SUBSTR(MONTH,1) = 'MAR' ) or  ( SUBSTR(MONTH,1) = 'APR' ) or  ( SUBSTR(MONTH,1) = 'MAY' ) or  ( SUBSTR(MONTH,1) = 'JUN' ) or  ( SUBSTR(MONTH,1) = 'JUL' ) or  ( SUBSTR(MONTH,1) = 'AUG' ) or  ( SUBSTR(MONTH,1) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]
        [TestCase(0, 4, 0, -1, MappingStoreDefaultConstants.PCAxisName, 
            "((  ( SUBSTR(YEAR_INT,1,4) = '2005' ) and (  ( SUBSTR(MONTH,1) = 'MAY' ) or  ( SUBSTR(MONTH,1) = 'JUN' ) or  ( SUBSTR(MONTH,1) = 'JUL' ) or  ( SUBSTR(MONTH,1) = 'AUG' ) or  ( SUBSTR(MONTH,1) = 'SEP' ) or  ( SUBSTR(MONTH,1) = 'OCT' ) or  ( SUBSTR(MONTH,1) = 'NOV' ) or  ( SUBSTR(MONTH,1) = 'DEC' )) )  or (  ( SUBSTR(YEAR_INT,1,4) = '2006' ) and (  ( SUBSTR(MONTH,1) = 'JAN' ) or  ( SUBSTR(MONTH,1) = 'FEB' ) or  ( SUBSTR(MONTH,1) = 'MAR' ) or  ( SUBSTR(MONTH,1) = 'APR' ) or  ( SUBSTR(MONTH,1) = 'MAY' ) or  ( SUBSTR(MONTH,1) = 'JUN' ) or  ( SUBSTR(MONTH,1) = 'JUL' ) or  ( SUBSTR(MONTH,1) = 'AUG' ) or  ( SUBSTR(MONTH,1) = 'SEP' )) ) )", 
            "2005-05", "2006-09")]

        //// same year
        [TestCase(0, 4, 0, -1, MappingStoreDefaultConstants.SqlServerName, 
            "((  ( SUBSTRING(CAST(YEAR_INT AS VARCHAR),1,4) = '2005' ) and (  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'JAN' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'FEB' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'MAR' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'APR' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'MAY' ) or  ( SUBSTRING(CAST(MONTH AS VARCHAR),1,100) = 'JUN' )) ) )", 
            "2005-01", "2005-06")]
        [TestCase(0, 4, 0, -1, MappingStoreDefaultConstants.OracleName, 
            "((  ( SUBSTR(YEAR_INT,1,4) = '2005' ) and (  ( SUBSTR(MONTH,1) = 'JAN' ) or  ( SUBSTR(MONTH,1) = 'FEB' ) or  ( SUBSTR(MONTH,1) = 'MAR' ) or  ( SUBSTR(MONTH,1) = 'APR' ) or  ( SUBSTR(MONTH,1) = 'MAY' ) or  ( SUBSTR(MONTH,1) = 'JUN' )) ) )", 
            "2005-01", "2005-06")]
        [TestCase(0, 4, 0, -1, MappingStoreDefaultConstants.MySqlName, 
            "((  ( SUBSTR(YEAR_INT,1,4) = '2005' ) and (  ( SUBSTR(MONTH,1) = 'JAN' ) or  ( SUBSTR(MONTH,1) = 'FEB' ) or  ( SUBSTR(MONTH,1) = 'MAR' ) or  ( SUBSTR(MONTH,1) = 'APR' ) or  ( SUBSTR(MONTH,1) = 'MAY' ) or  ( SUBSTR(MONTH,1) = 'JUN' )) ) )", 
            "2005-01", "2005-06")]
        [TestCase(0, 4, 0, -1, MappingStoreDefaultConstants.PCAxisName, 
            "((  ( SUBSTR(YEAR_INT,1,4) = '2005' ) and (  ( SUBSTR(MONTH,1) = 'JAN' ) or  ( SUBSTR(MONTH,1) = 'FEB' ) or  ( SUBSTR(MONTH,1) = 'MAR' ) or  ( SUBSTR(MONTH,1) = 'APR' ) or  ( SUBSTR(MONTH,1) = 'MAY' ) or  ( SUBSTR(MONTH,1) = 'JUN' )) ) )", 
            "2005-01", "2005-06")]
        public void TestTime2ColumnMapping(int yearStart, int yearLen, int periodStart, int periodLen, string databaseType, string expectedResult, string dateFrom, string dateTo)
        {
            var mappingEntity = new  TimeDimensionMappingEntity() {   EntityId = "130", TimeMappingType = TimeDimensionMappingType.TranscodingWithFrequencyDimension};
            var yearColumn = new DataSetColumnEntity() { EntityId = "1",  Name = "YEAR_INT" };
            var periodColumn = new DataSetColumnEntity() { EntityId = "2",  Name = "MONTH" };
            var timeTranscodingEntity = new TimeTranscodingEntity() { EntityId = "4", Frequency = "M" };
            timeTranscodingEntity.Year = new TimeTranscoding();
            timeTranscodingEntity.Year.Column = yearColumn;
            timeTranscodingEntity.Year.Start = yearStart;
            timeTranscodingEntity.Year.Length = yearLen;
            timeTranscodingEntity.Period = new PeriodTimeTranscoding();
            timeTranscodingEntity.Period.Column = periodColumn;
            timeTranscodingEntity.Period.Start = periodStart;
            timeTranscodingEntity.Period.Length = periodLen;

            var localCodes = new[] { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
            var sdmxCodes = new[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
            
            for (int i = 0; i < localCodes.Length; i++)
            {
                timeTranscodingEntity.Period.AddRule(sdmxCodes[i], localCodes[i]);
            }

            var oldStyle = new List<TimeTranscodingEntity>();
            oldStyle.Add(timeTranscodingEntity);
            mappingEntity.Transcoding = TimeTranscodingConversionHelper.Convert(oldStyle, "FREQ");
            var mapping = new TimeDimension2Column(mappingEntity, new TimeExpressionEntity(timeTranscodingEntity, mappingEntity), databaseType);
            string generateWhere = mapping.GenerateWhere(new SdmxDateCore(dateFrom), new SdmxDateCore(dateTo));
            Assert.NotNull(generateWhere);
            Assert.AreEqual(expectedResult, generateWhere.Trim());
        }

        #endregion

        #region Methods

        /// <summary>
        /// Builds the time transcoding entity.
        /// </summary>
        /// <param name="frequencyValue">
        /// The frequency value.
        /// </param>
        /// <returns>
        /// The time transcoding.
        /// </returns>
        private static TimeTranscodingEntity BuildTimeTranscodingEntity(string frequencyValue)
        {
            var timeTranscodingEntity = new TimeTranscodingEntity() { EntityId = "4", Frequency = frequencyValue };
            timeTranscodingEntity.Year = new TimeTranscoding();
            timeTranscodingEntity.Year.Column = new DataSetColumnEntity() { Name = "PERIOD_COL", EntityId = "1" };
            timeTranscodingEntity.Year.Start = 0;
            timeTranscodingEntity.Year.Length = 4;
            switch (frequencyValue)
            {
                case "A":
                    break;
                case "M":
                    {
                        timeTranscodingEntity.Period = new PeriodTimeTranscoding();
                        timeTranscodingEntity.Period.Column = timeTranscodingEntity.Year.Column;
                        timeTranscodingEntity.Period.Start = 5;
                        timeTranscodingEntity.Period.Length = -1;

                        timeTranscodingEntity.Period.Rules = new List<TranscodingRuleEntity>();
                        var localCodes = new[] { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
                        var sdmxCodes = new[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
                        for (int i = 0; i < localCodes.Length; i++)
                        {
                            timeTranscodingEntity.Period.AddRule(sdmxCodes[i], localCodes[i]);
                        }
                    }

                    break;
                case "Q":
                    {
                        timeTranscodingEntity.Period = new PeriodTimeTranscoding();
                        timeTranscodingEntity.Period.Column = timeTranscodingEntity.Year.Column;
                        timeTranscodingEntity.Period.Start = 5;
                        timeTranscodingEntity.Period.Length = -1;

                        timeTranscodingEntity.Period.Rules = new List<TranscodingRuleEntity>();
                        var localCodes = new[] { "JAN", "APR", "JUL", "OCT" };
                        var sdmxCodes = new[] { "Q1", "Q2", "Q3", "Q4" };
                        for (int i = 0; i < localCodes.Length; i++)
                        {
                            timeTranscodingEntity.Period.AddRule(sdmxCodes[i], localCodes[i]);
                        }
                    }

                    break;
            }

            return timeTranscodingEntity;
        }

        /// <summary>
        ///     Builds the frequency component mapping.
        /// </summary>
        /// <returns>
        ///     The <see cref="IComponentMappingBuilder" />.
        /// </returns>
        private static IComponentMappingBuilder BuildFrequencyComponentMapping()
        {
            var freqComponent =new Component { EntityId = "6", ObjectId = "FREQ" };
            var mapping = new  ComponentMappingEntity() {  Type = "A", EntityId = "10"};
            var columnEntity = new DataSetColumnEntity() { EntityId = "11",  Name = "FREQ_COL" };
            mapping.GetColumns().Add(columnEntity);
            mapping.Component = freqComponent;
            var transcoding  = new TranscodingEntity() { EntityId = "12" };
            var localCodes = new[] { "MX", "QX", "AX" };
            var sdmxCodes = new[] { "M", "Q", "A" };
            var rules = new List<TranscodingRuleEntity>();
            for (int i = 0; i < localCodes.Length; i++)
            {
                var rule = new TranscodingRuleEntity() { EntityId = "12" };
                rule.DsdCodeEntity = new IdentifiableEntity() { ObjectId = sdmxCodes[i] };
                rule.LocalCodes = new[] { new LocalCodeEntity() { ParentId = columnEntity.EntityId, ObjectId = localCodes[i] }, };
                rules.Add(rule);
            }

            return new ComponentMapping1To1T(mapping, new TranscodingRuleMap(rules, mapping), null);
        }

        #endregion
    }
}