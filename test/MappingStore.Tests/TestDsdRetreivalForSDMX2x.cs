// -----------------------------------------------------------------------
// <copyright file="TestDsdRetreival.cs" company="EUROSTAT">
//   Date Created : 2015-01-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace MappingStoreRetrieval.Tests
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using DataRetriever.Test;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using log4net;
    using log4net.Config;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

    /// <summary>
    /// Test unit for testing retrieval of DSD
    /// </summary>
    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class TestDsdRetreivalForSDMX2x : TestBase
    {
        private DsdRetrievalEngine retrievalEngine;

        public TestDsdRetreivalForSDMX2x(string storeId)
            : base(storeId)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
            var connectionStringSettings = GetConnectionStringSettings();
            var mappingStoreDatabase = new Database(connectionStringSettings);
            retrievalEngine = new DsdRetrievalEngine(mappingStoreDatabase);
        }

        /// <summary>
        /// Test unit for attached measure
        /// </summary>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="dsdId">The DSD identifier.</param>
        /// <param name="version">The version.</param>
        /// <param name="attributeId">The attribute identifier.</param>
        [TestCase("ESTAT", "DEMOGRAPHY", "2.3", "OBS_STATUS")]
        public void TestAttachMeasures(string agencyId, string dsdId, string version, string attributeId)
        {
            ICrossSectionalDataStructureMutableObject dsd = RetrieveDsd(agencyId, dsdId, version) as ICrossSectionalDataStructureMutableObject;
            Assert.NotNull(dsd);
            Assert.IsTrue(dsd.AttributeToMeasureMap.ContainsKey(attributeId));
            Assert.IsNotEmpty(dsd.AttributeToMeasureMap[attributeId]);
        }
        
        /// <summary>
        /// Test unit for attached measure
        /// </summary>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="dsdId">The DSD identifier.</param>
        /// <param name="version">The version.</param>
        /// <param name="attributeId">The attribute identifier.</param>
        [TestCase("ESTAT", "DEMOGRAPHY", "2.3", "DEMO", "CL_DEMO")]
        public void TestCrossMeasureDimensionCodelist(string agencyId, string dsdId, string version, string measureDimensionId, string codelist)
        {
            ICrossSectionalDataStructureMutableObject dsd = RetrieveDsd(agencyId, dsdId, version) as ICrossSectionalDataStructureMutableObject;
            Assert.NotNull(dsd);
            Assert.IsTrue(dsd.MeasureDimensionCodelistMapping.ContainsKey(measureDimensionId));
            IStructureReference codelistRef = dsd.MeasureDimensionCodelistMapping[measureDimensionId];
            Assert.That(codelistRef, Is.Not.Null);
            Assert.That(codelistRef.MaintainableStructureEnumType.EnumType, Is.EqualTo(SdmxStructureEnumType.CodeList));
            Assert.That(codelistRef.MaintainableId, Is.EqualTo(codelist));
        }
        private IDataStructureMutableObject RetrieveDsd(string agencyId, string dsdId, string version)
        {
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.Null)
            .SetAgencyIds(agencyId)
            .SetMaintainableIds(dsdId)
            .SetVersionRequests(new VersionRequestCore(version))
            .SetMaintainableTarget(SdmxStructureEnumType.Dsd)
            .Build();
            // single because we provide specific id , agency and version and type
            return retrievalEngine.Retrieve(structureQuery).Single() ;
        }

        /// <summary>
        /// Test unit for testing groups
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="dsdId">The DSD identifier.</param>
        /// <param name="version">The version.</param>
        /// <param name="attributeId">The attribute identifier.</param>
        [TestCase("ESTAT", "STS", "2.0", "UNIT", "Sibling")]
        public void TestGroup(string agencyId, string dsdId, string version, string attributeId, string groupId)
        {
            var dsd = RetrieveDsd(agencyId, dsdId, version);
            Assert.NotNull(dsd.AttributeList);
            Assert.IsNotEmpty(dsd.Attributes);
            var attribute = dsd.Attributes.First(o => o.Id.Equals(attributeId, StringComparison.Ordinal));
            Assert.AreEqual(attribute.AttachmentGroup, groupId);
        }

        /// <summary>
        /// Test unit for testing groups
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="dsdId">The DSD identifier.</param>
        /// <param name="version">The version.</param>
        /// <param name="dimensionId">The dimension identifier.</param>
        /// <param name="conceptRoleId">The concept role identifier.</param>
        [TestCase("ESTAT", "DEMOGRAPHY", "2.3", "COUNTRY", "ENTITY")]
        public void TestConceptRole(string agencyId, string dsdId, string version, string dimensionId, string conceptRoleId)
        {
            var dsd = RetrieveDsd(agencyId, dsdId, version);
            Assert.NotNull(dsd.DimensionList);
            Assert.IsNotEmpty(dsd.Dimensions);
            var dimension = dsd.Dimensions.First(o => o.Id.Equals(dimensionId, StringComparison.Ordinal));
            Assert.IsTrue(dimension.ConceptRole.Any(reference => reference.FullId.Equals(conceptRoleId, StringComparison.Ordinal)));
        }
    }
}