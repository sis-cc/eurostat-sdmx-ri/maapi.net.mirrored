// -----------------------------------------------------------------------
// <copyright file="TestCodelistRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2014-06-13
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStoreRetrieval.Extensions;

namespace MappingStoreRetrieval.Tests
{
    using System.Diagnostics;
    using System.Linq;

    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Test unit for <see cref="CodeListRetrievalEngine"/>
    /// </summary>
    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    [Category("RequiresDB")]
    public class TestCodelistRetrievalEngine
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log;

        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        /// <summary>
        /// The codelist retrieval engine
        /// </summary>
        private readonly CodeListRetrievalEngine _codelistRetrievalEngine;

        /// <summary>
        /// The mapping store database
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        /// Initializes static members of the <see cref="TestCodelistRetrievalEngine"/> class. 
        /// </summary>
        static TestCodelistRetrievalEngine()
        {
            _log = LogManager.GetLogger(typeof(TestCodelistRetrievalEngine));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestCodelistRetrievalEngine"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public TestCodelistRetrievalEngine(string connectionName)
        {
            var connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(connectionName);
            this._mappingStoreDb = new Database(connectionStringSettings);
            this._codelistRetrievalEngine = new CodeListRetrievalEngine(this._mappingStoreDb);
        }

        // TODO: see if we need to rewrite any of these tests for msdb 7.0

        ///// <summary>
        ///// Test unit for <see cref="ArtefactRetrieverEngine{T}.Retrieve(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject,Org.Sdmxsource.Sdmx.Api.Constants.ComplexStructureQueryDetailEnumType,Estat.Sri.MappingStoreRetrieval.Constants.VersionQueryType,System.Collections.Generic.IList{Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject})"/> 
        ///// </summary>
        //[Test]
        //public void TestRetrieve()
        //{
        //    Stopwatch stopwatch = new Stopwatch();
        //    stopwatch.Start();
        //    var codelistMutableObjects = this._codelistRetrievalEngine.Retrieve(new MaintainableRefObjectImpl().CreateComplexStructureReferenceObject(SdmxStructureEnumType.CodeList, false), ComplexStructureQueryDetailEnumType.Full);
        //    stopwatch.Stop();
        //    Assert.IsNotEmpty(codelistMutableObjects);
        //    var count = codelistMutableObjects.SelectMany(o => o.Items).Count();
        //    var time = count / 2; // 1/2 ms per code ?
        //    Assert.LessOrEqual(stopwatch.ElapsedMilliseconds, time);
        //}

        ///// <summary>
        ///// Test unit for <see cref="ArtefactRetrieverEngine{T}.Retrieve(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject,Org.Sdmxsource.Sdmx.Api.Constants.ComplexStructureQueryDetailEnumType,Estat.Sri.MappingStoreRetrieval.Constants.VersionQueryType,System.Collections.Generic.IList{Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject})"/> 
        ///// </summary>
        //[Test]
        //[Ignore("This CL might not exist in the MSDB.")]
        //public void TestRetrieveCodesWithAnnotation()
        //{
        //    Stopwatch stopwatch = new Stopwatch();
        //    stopwatch.Start();
        //    var codelistMutableObjects = this._codelistRetrievalEngine.Retrieve(new MaintainableRefObjectImpl("TEST", "CL_TEST", "1.0").CreateComplexStructureReferenceObject(SdmxStructureEnumType.CodeList,false), ComplexStructureQueryDetailEnumType.Full);
        //    stopwatch.Stop();
        //    _log.InfoFormat("Retrieved in {0}", stopwatch.Elapsed);
        //    Assert.IsNotEmpty(codelistMutableObjects);
        //    var testCl = codelistMutableObjects.Single();
        //    Assert.IsTrue(testCl.Items.All(o => o.Annotations.Count > 0));
        //    stopwatch.Start();

        //    var sortedCodes = testCl.Items.OrderBy(
        //        o =>
        //        {
        //            var orderAnnotation = o.Annotations.FirstOrDefault(mutableObject => mutableObject.Type == "@ORDER");
        //            int orderValue;
        //            return orderAnnotation != null && int.TryParse(orderAnnotation.Title, out orderValue) ? orderValue : 0;
        //        }).ToArray();

        //    stopwatch.Stop();
        //    _log.InfoFormat("TITLE Sorted {1} codes in {0}", stopwatch.Elapsed, sortedCodes.Length);
        //}

        ///// <summary>
        ///// Test unit for <see cref="ArtefactRetrieverEngine{T}.Retrieve(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject,Org.Sdmxsource.Sdmx.Api.Constants.ComplexStructureQueryDetailEnumType,Estat.Sri.MappingStoreRetrieval.Constants.VersionQueryType,System.Collections.Generic.IList{Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject})"/> 
        ///// </summary>
        //[Test]
        //[Ignore("This CL might not exist in the MSDB.")]
        //public void TestRetrieveCodesWithOrderAnnotationText()
        //{
        //    Stopwatch stopwatch = new Stopwatch();
        //    stopwatch.Start();
        //    var codelistMutableObjects = this._codelistRetrievalEngine.Retrieve(new MaintainableRefObjectImpl("IMF", "CL_INDICATOR", "1.0").CreateComplexStructureReferenceObject(SdmxStructureEnumType.CodeList,false), ComplexStructureQueryDetailEnumType.Full);
        //    stopwatch.Stop();
        //    _log.InfoFormat("TEXT Retrieved in {0}", stopwatch.Elapsed);
        //    stopwatch.Reset();
        //    _log.InfoFormat("Reset in {0}", stopwatch.Elapsed);

        //    var testCl = codelistMutableObjects.First();
        //    Assert.IsNotEmpty(codelistMutableObjects);
        //    Assert.IsTrue(testCl.Items.All(o => o.Annotations.Count > 0 && o.Annotations.All(mutableObject => mutableObject.Text.Count > 0)));
        //    stopwatch.Start();

        //    var sortedCodes = testCl.Items.OrderBy(
        //        o =>
        //            {
        //                var orderAnnotation = o.Annotations.FirstOrDefault(mutableObject => mutableObject.Type == "@ORDER");
        //                int orderValue;
        //                return orderAnnotation != null && int.TryParse(orderAnnotation.Text.First().Value, out orderValue) ? orderValue : 0;
        //            }).ToArray();

        //    stopwatch.Stop();
        //    _log.InfoFormat("TEXT Sorted {1} codes in {0}", stopwatch.Elapsed, sortedCodes.Length);
        //}

        ///// <summary>
        ///// Test unit for <see cref="ArtefactRetrieverEngine{T}.Retrieve(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject,Org.Sdmxsource.Sdmx.Api.Constants.ComplexStructureQueryDetailEnumType,Estat.Sri.MappingStoreRetrieval.Constants.VersionQueryType,System.Collections.Generic.IList{Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject})"/> 
        ///// </summary>
        //[Test]
        //[Ignore("This CL might not exist in the MSDB.")]
        //public void TestRetrieveCodesWithOrderAnnotationTitle()
        //{
        //    Stopwatch stopwatch = new Stopwatch();
        //    stopwatch.Start();
        //    var codeListRetrievalEngine = new CodeListRetrievalEngine(this._mappingStoreDb);
        //    var codelistMutableObjects = codeListRetrievalEngine.Retrieve(new MaintainableRefObjectImpl("IMF", "CL_INDICATOR", "1.1").CreateComplexStructureReferenceObject(SdmxStructureEnumType.CodeList,false), ComplexStructureQueryDetailEnumType.Full);
        //    stopwatch.Stop();
        //    _log.InfoFormat("TITLE Retrieved in {0}", stopwatch.Elapsed);
        //    stopwatch.Reset();
        //    _log.InfoFormat("Reset in {0}", stopwatch.Elapsed);
        //    var testCl = codelistMutableObjects.First();
        //    Assert.IsNotEmpty(codelistMutableObjects);
        //    Assert.IsTrue(testCl.Items.All(o => o.Annotations.Count > 0));
        //    stopwatch.Start();

        //    var sortedCodes = testCl.Items.OrderBy(
        //        o =>
        //        {
        //            var orderAnnotation = o.Annotations.FirstOrDefault(mutableObject => mutableObject.Type == "@ORDER");
        //            int orderValue;
        //            return orderAnnotation != null && int.TryParse(orderAnnotation.Title, out orderValue) ? orderValue : 0;
        //        }).ToArray();

        //    stopwatch.Stop();
        //    _log.InfoFormat("TITLE Sorted {1} codes in {0}", stopwatch.Elapsed, sortedCodes.Length);
        //}

        ///// <summary>
        ///// Test unit for <see cref="ArtefactRetrieverEngine{T}.Retrieve(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject,Org.Sdmxsource.Sdmx.Api.Constants.ComplexStructureQueryDetailEnumType,Estat.Sri.MappingStoreRetrieval.Constants.VersionQueryType,System.Collections.Generic.IList{Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject})"/> 
        ///// </summary>
        //[Test]
        //[Ignore("The CORDER field does not exist in Mapping Store Schema. This test should be deleted.")]
        //public void TestRetrieveCodesWithOrderNoAnnotationCORDER()
        //{
        //    Stopwatch stopwatch = new Stopwatch();
        //    stopwatch.Start();
        //    var codeListRetrievalEngine = new CodeListRetrievalEngine(this._mappingStoreDb, " ORDER BY T.CORDER");
        //    var maintainableRefObjectImpl = new MaintainableRefObjectImpl("IMF", "CL_INDICATOR", "1.2").CreateComplexStructureReferenceObject(SdmxStructureEnumType.CodeList,false);
        //    var codelistMutableObjects = codeListRetrievalEngine.Retrieve(maintainableRefObjectImpl, ComplexStructureQueryDetailEnumType.Full);
        //    stopwatch.Stop();
        //    _log.InfoFormat("CORDER Retrieved in {0}", stopwatch.Elapsed);
        //    stopwatch.Reset();
        //    _log.InfoFormat("Reset in {0}", stopwatch.Elapsed);
        //    var testCl = codelistMutableObjects.First();
        //    Assert.IsNotEmpty(codelistMutableObjects);
        //    Assert.IsFalse(testCl.Items.All(o => o.Annotations.Count > 0));
        //}
    }
}