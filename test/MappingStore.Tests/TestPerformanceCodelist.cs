// -----------------------------------------------------------------------
// <copyright file="TestPerformanceCodelist.cs" company="EUROSTAT">
//   Date Created : 2014-07-11
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStoreRetrieval.Extensions;

namespace MappingStoreRetrieval.Tests
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Linq;

    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     The test performance codelist.
    /// </summary>
    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    [Category("RequiresDB")]
    public class TestPerformanceCodelist
    {
        #region Fields

        /// <summary>
        /// The _code list retrieval engine.
        /// </summary>
        private readonly CodeListRetrievalEngine _codeListRetrievalEngine;

        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TestPerformanceCodelist"/> class. 
        /// </summary>
        /// <param name="connectionName">
        /// Name of the connection.
        /// </param>
        public TestPerformanceCodelist(string connectionName)
        {
            ConnectionStringSettings connectionString = this._connectionStringHelper.GetConnectionStringSettings(connectionName);
            var database = new Database(connectionString);
            this._codeListRetrievalEngine = new CodeListRetrievalEngine(database);
        }

        #endregion

        #region Public Methods and Operators

        // TODO: see if we need to rewrite any of these tests for msdb 7.0

        ///// <summary>
        /////     Tests the get all codelists.
        ///// </summary>
        //[Test]
        //public void TestGetAllCodelists()
        //{
        //    var stopwatch = new Stopwatch();
        //    stopwatch.Start();
        //    ISet<ICodelistMutableObject> codelistMutableObjects = this._codeListRetrievalEngine.Retrieve(
        //        new MaintainableRefObjectImpl().CreateComplexStructureReferenceObject(SdmxStructureEnumType.CodeList,false), 
        //        ComplexStructureQueryDetailEnumType.Full);
        //    stopwatch.Stop();
        //    var count = codelistMutableObjects.SelectMany(o => o.Items).Count();
        //    var time = (count / 2) + 1000; // 1/2 ms per code ?
        //    Assert.LessOrEqual(stopwatch.ElapsedMilliseconds, time);
        //    Assert.IsNotEmpty(codelistMutableObjects);
        //}

        ///// <summary>
        /////     Tests the get all codelists agency.
        ///// </summary>
        //[Test]
        //public void TestGetAllCodelistsAgency()
        //{
        //    var stopwatch = new Stopwatch();
        //    stopwatch.Start();
        //    ISet<ICodelistMutableObject> codelistMutableObjects = this._codeListRetrievalEngine.Retrieve(
        //        new MaintainableRefObjectImpl("IT1", null, null).CreateComplexStructureReferenceObject(SdmxStructureEnumType.CodeList,false), 
        //        ComplexStructureQueryDetailEnumType.Full);
        //    stopwatch.Stop();
        //    if (codelistMutableObjects.Count > 0)
        //    {
        //        var count = codelistMutableObjects.SelectMany(o => o.Items).Count();
        //        var time = (count / 2) + 1000; // 1/2 ms per code ?
        //        Assert.LessOrEqual(stopwatch.ElapsedMilliseconds, time);
        //    }
        //}

        #endregion
    }
}