// -----------------------------------------------------------------------
// <copyright file="TestSpecialMutableObjectRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-04-17
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// Test unit for <see cref="SpecialMutableObjectRetrievalManager"/>
    /// </summary>
    [Category("RequiresDB")]
    [Ignore("The MappingStoreRetrievalManager used in the TestCaseSource GetCodelists, is obsolete.")]
    public class TestSpecialMutableObjectRetrievalManager
    {
        /// <summary>
        /// The _from mutable builder.
        /// </summary>
        private static readonly StructureReferenceFromMutableBuilder _fromMutableBuilder = new StructureReferenceFromMutableBuilder();
        static TestSpecialMutableObjectRetrievalManager()
        {
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["MappingStoreRetrieversFactory"]);
        }

        /// <summary>
        /// Test unit for <see cref="SpecialMutableObjectRetrievalManager" />
        /// </summary>
        /// <param name="input">The input.</param>
        [TestCaseSource(nameof(GetCodelists), new object[] { "odp" })]
        [TestCaseSource(nameof(GetCodelists), new object[] { "sqlserver" })]
        [TestCaseSource(nameof(GetCodelists), new object[] { "mysql" })]
        public void TestGetMutableCodelistObjects(TestCodelistBox input)
        {
            var codelistObject = input.Codelist;
            ISpecialMutableObjectRetrievalManager special = new SpecialMutableObjectRetrievalManager(input.Settings);
            IStructureReference structureReference = _fromMutableBuilder.Build(codelistObject);
            var mutableCodelistObjects = special.GetMutableCodelistObjects(structureReference.MaintainableReference, new List<string> { codelistObject.Items.First().Id }).First();
            Assert.AreEqual(1, mutableCodelistObjects.Items.Count);

            string[] strings = (from l in codelistObject.Items select l.Id).ToArray();
            var codelistObjects = special.GetMutableCodelistObjects(structureReference.MaintainableReference, strings).First();
            Assert.AreEqual(codelistObject.Items.Count, codelistObjects.Items.Count);
        }

        /// <summary>
        /// Test unit for <see cref="SpecialMutableObjectRetrievalManager" />
        /// </summary>
        /// <param name="input">The input.</param>
        [TestCaseSource(nameof(GetCodelists), new object[] { "odp" })]
        [TestCaseSource(nameof(GetCodelists), new object[] { "sqlserver" })]
        [TestCaseSource(nameof(GetCodelists), new object[] { "mysql" })]
        public void TestGetMutableCodelistObjectsDeep(TestCodelistBox input)
        {
            var codelistObject = input.Codelist;
            ISpecialMutableObjectRetrievalManager special = new SpecialMutableObjectRetrievalManager(input.Settings);
            IStructureReference structureReference = _fromMutableBuilder.Build(codelistObject);
            var first = new[] { codelistObject.Items.First().Id };
            var all = codelistObject.Items.Select(o => o.Id).ToArray();
            var subsets = new[] { first, all }.ToArray();
            foreach (var subset in subsets)
            {
                var mutableCodelistObjects = special.GetMutableCodelistObjects(structureReference.MaintainableReference, subset).First();

                Assert.AreEqual(subset.Length, mutableCodelistObjects.Items.Count);

                // we don't care about the position of names/descriptions in a collection...
                // But in DeepEquals only SdmxSource does .. 
                SortNames(mutableCodelistObjects);
                SortNames(codelistObject);

                ICodelistObject fullCodelist = codelistObject.ImmutableInstance;
                ICodelistObject partialCodelst = mutableCodelistObjects.ImmutableInstance;
                IDictionary<string, ICode> codeSet = fullCodelist.Items.ToDictionary(code => code.Id, StringComparer.Ordinal);
                foreach (var item in partialCodelst.Items)
                {
                    ICode fromFull;
                    if (!codeSet.TryGetValue(item.Id, out fromFull))
                    {
                        Assert.Fail("Could not find code at ");
                    }

                    // Partial codes may not have a parent code.
                    if (string.Equals(item.ParentCode, fromFull.ParentCode))
                    {
                        bool condition = item.DeepEquals(fromFull, true);
                        Assert.IsTrue(condition, "URN :{2}. Annotation count {0} vs {1}", fromFull.Annotations.Count, item.Annotations.Count, item.Urn);
                    }
                }
            }
        }

        /// <summary>
        /// Test unit for <see cref="SpecialMutableObjectRetrievalManager" />
        /// </summary>
        /// <param name="input">The input.</param>
        [TestCaseSource(nameof(GetCodelists), new object[] { "odp" })]
        [TestCaseSource(nameof(GetCodelists), new object[] { "sqlserver" })]
        [TestCaseSource(nameof(GetCodelists), new object[] { "mysql" })]
        public void TestGetMutableCodelistObjectsDeepNoSubSet(TestCodelistBox input)
        {
            var codelistObject = input.Codelist;
            ISpecialMutableObjectRetrievalManager special = new SpecialMutableObjectRetrievalManager(input.Settings);
            IStructureReference structureReference = _fromMutableBuilder.Build(codelistObject);
            var codelistMutableObjects = special.GetMutableCodelistObjects(structureReference.MaintainableReference);
            Assert.That(codelistMutableObjects, Is.Not.Null);
            Assert.That(codelistMutableObjects, Is.Not.Empty);
            var mutableCodelistObjects = codelistMutableObjects.First();

            SortNames(mutableCodelistObjects);
            SortNames(codelistObject);

            ICodelistObject fullCodelist = codelistObject.ImmutableInstance;
            ICodelistObject partialCodelst = mutableCodelistObjects.ImmutableInstance;

            foreach (var item in partialCodelst.Items)
            {
                ICode fromFull = fullCodelist.GetCodeById(item.Id);
                Assert.That(fromFull, Is.Not.Null);
                Assert.IsTrue(item.DeepEquals(fromFull, true), "URN :{2}. Annotation count {0} vs {1}", fromFull.Annotations?.Count, item.Annotations?.Count, item.Urn);
            }
        }

        /// <summary>
        /// Sorts the names.
        /// </summary>
        /// <param name="mutableCodelistObjects">The mutable codelist objects.</param>
        private static void SortNames(IItemSchemeMutableObject<ICodeMutableObject> mutableCodelistObjects)
        {
            SortText(mutableCodelistObjects.Names);
            SortText(mutableCodelistObjects.Descriptions);
            for (int i = 0; i < mutableCodelistObjects.Items.Count; i++)
            {
                SortText(mutableCodelistObjects.Items[i].Names);
                SortText(mutableCodelistObjects.Items[i].Descriptions);
            }
        }

        /// <summary>
        /// Sorts the text.
        /// </summary>
        /// <param name="names">The names.</param>
        private static void SortText(ICollection<ITextTypeWrapperMutableObject> names)
        {
            var texts = new List<ITextTypeWrapperMutableObject>(names);
            texts.Sort((o, mutableObject) => string.Compare(o.Locale, mutableObject.Locale, StringComparison.Ordinal));
            for (int i = 0; i < texts.Count; i++)
            {
                var text = texts[i];
                text.Value = text.Value.Replace("\r\n", "\n");
            }
            names.Clear();
            names.AddAll(texts);
        }

        /// <summary>
        /// Gets the code lists.
        /// </summary>
        /// <returns>The code lists</returns>
        private static IEnumerable<TestCodelistBox> GetCodelists(string connectionName)
        {
            //var settings = ConfigurationManager.ConnectionStrings[connectionName];
            //ISdmxMutableObjectRetrievalManager manager = new MappingStoreRetrievalManager(settings);

            //Func<IEnumerable<ICodelistMutableObject>> getFromDb = () => manager.GetMutableCodelistObjects(new MaintainableRefObjectImpl(), false, false);
            //Func<ISdmxObjects, IEnumerable<ICodelistMutableObject>> getFromObjects = (objects) => objects.Codelists.Select(o => o.MutableInstance);

            //return SourceHelper.GetArtefact(connectionName, SdmxStructureEnumType.CodeList, getFromObjects, getFromDb).Select(o => new TestCodelistBox(o, settings));

            // MappingStoreRetrievalManager class is obsolete
            // trying to build this static test case source, prevents other tests from running
            return new List<TestCodelistBox>();
        }

        /// <summary>
        /// wrap <see cref="ICodelistMutableObject"/> to override the ToString()
        /// </summary>
        public class TestCodelistBox
        {
            public ConnectionStringSettings Settings { get; }

            /// <summary>
            /// Initializes a new instance of the <see cref="TestCodelistBox"/> class.
            /// </summary>
            /// <param name="codelist">The codelist.</param>
            public TestCodelistBox(ICodelistMutableObject codelist, ConnectionStringSettings settings)
            {
                Settings = settings;
                this.Codelist = codelist;
            }

            /// <summary>
            /// Gets the codelist.
            /// </summary>
            /// <value>
            /// The codelist.
            /// </value>
            public ICodelistMutableObject Codelist { get; private set; }

            /// <summary>
            /// Returns a string that represents the current object.
            /// </summary>
            /// <returns>
            /// A string that represents the current object.
            /// </returns>
            public override string ToString()
            {
                return string.Format(CultureInfo.InvariantCulture, "{0}, {1}", _fromMutableBuilder.Build(this.Codelist).TargetUrn, Settings.Name);
            }
        }
    }
}