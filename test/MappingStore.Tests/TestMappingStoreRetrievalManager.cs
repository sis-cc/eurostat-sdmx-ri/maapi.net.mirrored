// -----------------------------------------------------------------------
// <copyright file="TestMappingStoreRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System.Configuration;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     Test unit for <see cref="MappingStoreRetrievalManager" />
    /// </summary>
    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    [Category("RequiresDB")]
    public class TestMappingStoreRetrievalManager : TestRetrievalManagerBase
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(TestMappingStoreRetrievalManager));

        /// <summary>
        /// The _connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        /// <summary>
        /// Initializes a new instance of the <see cref="TestMappingStoreRetrievalManager"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public TestMappingStoreRetrievalManager(string name)
        {
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(name);
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["MappingStoreRetrieversFactory"]);
        }

        #region Public Methods and Operators
        /// <summary>
        /// Tests the code list get parent.
        /// </summary>
        [Test]
        public void TestCodeListGetParent()
        {
            var sdmxMutableObjectRetrievalManager = this.GetRetrievalManager(this._connectionStringSettings);
            Assert.NotNull(sdmxMutableObjectRetrievalManager);
        }

        /// <summary>
        /// Test unit for <see cref="IAuthSdmxMutableObjectRetrievalManager.GetMutableCategorisation" /> with no allowed dataflows
        /// </summary>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        [Test]
        [Ignore("the code covers categorisations for more than dataflows")]
        public void TestGetMutableDataflowAllowedCategorisationLatestNoResults([Values(true, false)]bool returnStub, [Values(true, false)] bool returnLatest)
        {
            IAuthSdmxMutableObjectRetrievalManager api = this.GetAuthRetrievalManager(this._connectionStringSettings);
            var dataflowObjects = api.GetMutableCategorisation(new MaintainableRefObjectImpl(), returnLatest, returnStub, new IMaintainableRefObject[0]);
            Assert.IsNull(dataflowObjects);
        }
        [Test]
        public void GetBigDataflow()
        {
            _log.Info("Started GetBigDataflow");
            var connectionString = _connectionStringSettings;
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(connectionString);
            IRestStructureQuery structureQuery = new RESTStructureQueryCore(
              StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full),
              StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.Descendants),
              null,
              new StructureReferenceImpl("IT1", "61_69", "1.0", SdmxStructureEnumType.Dataflow), 
              false);

            var retrievalManager = new MutableWrapperSdmxObjectRetrievalManager(api, null);
            _log.Info("Starting get dataflow and descendants");
           var sdmxObjects = retrievalManager.GetMaintainables(structureQuery);
            _log.Info("Done get dataflow and descendants");
            Assert.That(sdmxObjects, Is.Not.Null);
            Assert.That(sdmxObjects.Codelists, Is.Not.Empty);

        }

        [Test]
        public void GetDsd()
        {
            _log.Info("Started GetDsd");
            var connectionString = _connectionStringSettings;
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(connectionString);
            IRestStructureQuery structureQuery = new RESTStructureQueryCore(
              StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full),
              StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.None),
              null,
              new StructureReferenceImpl("ESTAT", null, "2.2", SdmxStructureEnumType.Dsd),
              false);

            var retrievalManager = new MutableWrapperSdmxObjectRetrievalManager(api, null);
            _log.Info("Starting get dataflow and descendants");
            var sdmxObjects = retrievalManager.GetMaintainables(structureQuery);
            _log.Info("Done get dataflow and descendants");
            Assert.That(sdmxObjects, Is.Not.Null);
            Assert.That(sdmxObjects.DataStructures, Is.Not.Empty);

        }

        /// <summary>
        /// Test unit for <see cref="IAuthSdmxMutableObjectRetrievalManager.GetMutableDataflowObjects" /> with no allowed dataflows
        /// </summary>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        [Test]
        public void TestGetMutableDataflowAllowedCategorisationlatestOneResult([Values(true, false)]bool returnStub, [Values(true, false)] bool returnLatest)
        {
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(this._connectionStringSettings);
            var mutableDataflowObjects = api.GetMutableCategorisationObjects(new MaintainableRefObjectImpl(), false, false);
            var first = mutableDataflowObjects.First();
            IMaintainableRefObject maintainableRef = first.StructureReference.MaintainableReference;
            IMaintainableRefObject maintainableRef2 = new MaintainableRefObjectImpl(first.AgencyId, first.Id + "OO", first.Version);
            IAuthSdmxMutableObjectRetrievalManager authApi = this.GetAuthRetrievalManager(this._connectionStringSettings);
            var dataflowObjects = authApi.GetMutableCategorisation(new MaintainableRefObjectImpl(), returnLatest, returnStub, new[] { maintainableRef, maintainableRef2 });
            Assert.IsNotNull(dataflowObjects);
        }

        /// <summary>
        /// Test unit for <see cref="IAuthSdmxMutableObjectRetrievalManager.GetMutableCategorisationObjects" /> with no allowed dataflows
        /// </summary>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        [Test]
        [Ignore("the code covers categorisations for more than dataflows")]
        public void TestGetMutableDataflowAllowedCategorisationsNoResults([Values(true, false)]bool returnStub, [Values(true, false)] bool returnLatest)
        {
            IAuthSdmxMutableObjectRetrievalManager api = this.GetAuthRetrievalManager(this._connectionStringSettings);
            var dataflowObjects = api.GetMutableCategorisationObjects(new MaintainableRefObjectImpl(), returnLatest, returnStub, new IMaintainableRefObject[0]);
            Assert.IsEmpty(dataflowObjects);
        }

        /// <summary>
        /// Test unit for <see cref="IAuthSdmxMutableObjectRetrievalManager.GetMutableDataflowObjects" /> with no allowed dataflows
        /// </summary>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        [Test]
        public void TestGetMutableDataflowAllowedCategorisationsOneResult([Values(true, false)] bool returnLatest)
        {
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(this._connectionStringSettings);
            var mutableDataflowObjects = api.GetMutableCategorisationObjects(new MaintainableRefObjectImpl(), false, false);
            var first = mutableDataflowObjects.First();
            IMaintainableRefObject maintainableRef = first.StructureReference.MaintainableReference;
            IMaintainableRefObject maintainableRef2 = new MaintainableRefObjectImpl(first.AgencyId, first.Id + "OO", first.Version);
            IAuthSdmxMutableObjectRetrievalManager authApi = this.GetAuthRetrievalManager(this._connectionStringSettings);
            var dataflowObjects = authApi.GetMutableCategorisationObjects(new MaintainableRefObjectImpl(), returnLatest, false, new[] { maintainableRef, maintainableRef2 });
            Assert.IsNotEmpty(dataflowObjects);
            Assert.AreEqual(1, dataflowObjects.Select(o => o.StructureReference).Where(x=>x.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.Dataflow ).Distinct().Count());
        }

        /// <summary>
        /// Test unit for <see cref="IAuthSdmxMutableObjectRetrievalManager.GetMutableDataflow" /> with no allowed dataflows
        /// </summary>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        [Test]
        public void TestGetMutableDataflowAllowedDataflowLatestNoResults([Values(true, false)]bool returnStub, [Values(true, false)] bool returnLatest)
        {
            IAuthSdmxMutableObjectRetrievalManager api = this.GetAuthRetrievalManager(this._connectionStringSettings);
            var dataflowObjects = api.GetMutableDataflow(new MaintainableRefObjectImpl(), returnLatest, returnStub, new IMaintainableRefObject[0]);
            Assert.IsNull(dataflowObjects);
        }

        /// <summary>
        /// Test unit for <see cref="IAuthSdmxMutableObjectRetrievalManager.GetMutableDataflowObjects" /> with no allowed dataflows
        /// </summary>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        [Test]
        public void TestGetMutableDataflowAllowedDataflowlatestOneResult([Values(true, false)]bool returnStub, [Values(true, false)] bool returnLatest)
        {
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(this._connectionStringSettings);
            var mutableDataflowObjects = api.GetMutableDataflowObjects(new MaintainableRefObjectImpl(), false, false);
            var first = mutableDataflowObjects.First();
            IMaintainableRefObject maintainableRef = new MaintainableRefObjectImpl(first.AgencyId, first.Id, first.Version);
            IMaintainableRefObject maintainableRef2 = new MaintainableRefObjectImpl(first.AgencyId, first.Id + "OO", first.Version);
            IAuthSdmxMutableObjectRetrievalManager authApi = this.GetAuthRetrievalManager(this._connectionStringSettings);
            var dataflowObjects = authApi.GetMutableDataflow(maintainableRef, returnLatest, returnStub, new[] { maintainableRef, maintainableRef2 });
            Assert.IsNotNull(dataflowObjects);
        }

        /// <summary>
        /// Test unit for <see cref="IAuthSdmxMutableObjectRetrievalManager.GetMutableDataflowObjects" /> with no allowed dataflows
        /// </summary>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        [Test]
        public void TestGetMutableDataflowAllowedDataflowsNoResults([Values(true, false)]bool returnStub, [Values(true, false)] bool returnLatest)
        {
            IAuthSdmxMutableObjectRetrievalManager api = this.GetAuthRetrievalManager(this._connectionStringSettings);
            var dataflowObjects = api.GetMutableDataflowObjects(new MaintainableRefObjectImpl(), returnLatest, returnStub, new IMaintainableRefObject[0]);
            Assert.IsEmpty(dataflowObjects);
        }

        /// <summary>
        /// Test unit for <see cref="IAuthSdmxMutableObjectRetrievalManager.GetMutableDataflowObjects" /> with no allowed dataflows
        /// </summary>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        [Test]
        public void TestGetMutableDataflowAllowedDataflowsOneResult([Values(true, false)]bool returnStub, [Values(true, false)] bool returnLatest)
        {
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(this._connectionStringSettings);
            var mutableDataflowObjects = api.GetMutableDataflowObjects(new MaintainableRefObjectImpl(), false, false);
            var first = mutableDataflowObjects.First();
            IMaintainableRefObject maintainableRef = new MaintainableRefObjectImpl(first.AgencyId, first.Id, first.Version);
            IMaintainableRefObject maintainableRef2 = new MaintainableRefObjectImpl(first.AgencyId, first.Id + "OO", first.Version);
            IAuthSdmxMutableObjectRetrievalManager authApi = this.GetAuthRetrievalManager(this._connectionStringSettings);
            var dataflowObjects = authApi.GetMutableDataflowObjects(maintainableRef, returnLatest, returnStub, new[] { maintainableRef, maintainableRef2 });
            Assert.IsNotEmpty(dataflowObjects);
            Assert.AreEqual(1, dataflowObjects.Count);
        }

        /// <summary>
        /// Test unit for <see cref="IAuthSdmxMutableObjectRetrievalManager.GetMutableMaintainable" /> with no allowed dataflows
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        [Test]
        [Ignore("the code covers categorisations for more than dataflows")]
        public void TestGetMutableDataflowAllowedMaintainableNoResults([Values(SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Dataflow)]SdmxStructureEnumType type, [Values(true, false)]bool returnStub, [Values(true, false)] bool returnLatest)
        {
            IAuthSdmxMutableObjectRetrievalManager api = this.GetAuthRetrievalManager(this._connectionStringSettings);
            var dataflowObjects = api.GetMutableMaintainable(new StructureReferenceImpl(SdmxStructureType.GetFromEnum(type)), returnLatest, returnStub, new IMaintainableRefObject[0]);
            Assert.IsNull(dataflowObjects);
        }

        /// <summary>
        /// Test unit for <see cref="IAuthSdmxMutableObjectRetrievalManager.GetMutableDataflowObjects" /> with no allowed dataflows
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        [Test]
        public void TestGetMutableDataflowAllowedMaintainableOneResult([Values(SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Dataflow)]SdmxStructureEnumType type, [Values(true, false)]bool returnStub, [Values(true, false)] bool returnLatest)
        {
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(this._connectionStringSettings);
            var mutableDataflowObjects = api.GetMutableCategorisationObjects(new MaintainableRefObjectImpl(), false, false);
            var first = mutableDataflowObjects.First();
            IMaintainableRefObject maintainableRef = first.StructureReference.MaintainableReference;
            IMaintainableRefObject maintainableRef2 = new MaintainableRefObjectImpl(first.AgencyId, first.Id + "OO", first.Version);
            IAuthSdmxMutableObjectRetrievalManager authApi = this.GetAuthRetrievalManager(this._connectionStringSettings);
            var dataflowObjects = authApi.GetMutableMaintainable(new StructureReferenceImpl(type == SdmxStructureEnumType.Categorisation ? new MaintainableRefObjectImpl() : maintainableRef, type), returnLatest, returnStub, new[] { maintainableRef, maintainableRef2 });
            Assert.IsNotNull(dataflowObjects);
        }

        /// <summary>
        /// Test unit for <see cref="IAuthSdmxMutableObjectRetrievalManager.GetMutableMaintainables" /> with no allowed dataflows
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        [Test]
        [Ignore("the code covers categorisations for more than dataflows")]
        public void TestGetMutableDataflowAllowedMaintainablesNoResults([Values(SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Dataflow)]SdmxStructureEnumType type, [Values(true, false)]bool returnStub, [Values(true, false)] bool returnLatest)
        {
            IAuthSdmxMutableObjectRetrievalManager api = this.GetAuthRetrievalManager(this._connectionStringSettings);
            var dataflowObjects = api.GetMutableMaintainables(new StructureReferenceImpl(SdmxStructureType.GetFromEnum(type)), returnLatest, returnStub, new IMaintainableRefObject[0]);
            Assert.IsEmpty(dataflowObjects);
        }

        /// <summary>
        /// Test unit for <see cref="IAuthSdmxMutableObjectRetrievalManager.GetMutableMaintainables" /> with no allowed dataflows
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        [Test]
        public void TestGetMutableDataflowAllowedMaintainablessOneResult([Values(SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.Dataflow)]SdmxStructureEnumType type, [Values(true, false)] bool returnLatest)
        {
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(this._connectionStringSettings);
            var mutableDataflowObjects = api.GetMutableCategorisationObjects(new MaintainableRefObjectImpl(), false, false);
            var first = mutableDataflowObjects.First();
            IMaintainableRefObject maintainableRef = first.StructureReference.MaintainableReference;
            IMaintainableRefObject maintainableRef2 = new MaintainableRefObjectImpl(first.AgencyId, first.Id + "OO", first.Version);
            IAuthSdmxMutableObjectRetrievalManager authApi = this.GetAuthRetrievalManager(this._connectionStringSettings);
            var dataflowObjects = authApi.GetMutableMaintainables(new StructureReferenceImpl(type == SdmxStructureEnumType.Categorisation ? new MaintainableRefObjectImpl() : maintainableRef, type), returnLatest, false, new[] { maintainableRef, maintainableRef2 });
            Assert.IsNotEmpty(dataflowObjects);
            Assert.AreEqual(
                1,
                type == SdmxStructureEnumType.Categorisation ? dataflowObjects.Cast<ICategorisationMutableObject>().Select(o => o.StructureReference).Where(x => x.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.Dataflow).Distinct().Count() : dataflowObjects.Count);
        }

        /// <summary>
        /// Test unit for <see cref="ISdmxMutableObjectRetrievalManager.GetMutableMaintainables"/>
        /// </summary>
        /// <param name="structureEnumType">
        /// The structure Type.
        /// </param>
        [TestCase(SdmxStructureEnumType.CodeList)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Categorisation)]
        public void TestGetMutableMaintainable(SdmxStructureEnumType structureEnumType)
        {
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(this._connectionStringSettings);
            ValidateGetMutableMaintainable(api, structureEnumType);
        }

        /// <summary>
        /// Test unit for <see cref="ISdmxMutableObjectRetrievalManager.GetMutableMaintainables"/>
        /// </summary>
        /// <param name="structureEnumType">
        /// The structure Type.
        /// </param>
        [TestCase(SdmxStructureEnumType.CodeList)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Categorisation)]
        public void TestGetMutableMaintainableStub(SdmxStructureEnumType structureEnumType)
        {
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(this._connectionStringSettings);
            ValidateGetMutableMaintainableStub(api, structureEnumType);
        }

        /// <summary>
        /// Test unit for <see cref="ISdmxMutableObjectRetrievalManager.GetMutableMaintainables"/>
        /// </summary>
        /// <param name="structureEnumType">
        /// The structure type.
        /// </param>
        [TestCase(SdmxStructureEnumType.CodeList)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Categorisation)]
        public void TestGetMutableMaintainables(SdmxStructureEnumType structureEnumType)
        {
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(this._connectionStringSettings);
            ValidateGetMaintainables(api, structureEnumType);
        }

        /// <summary>
        /// Test unit for <see cref="ISdmxMutableObjectRetrievalManager.GetMutableMaintainables"/>
        /// </summary>
        /// <param name="structureEnumType">
        /// The structure Type.
        /// </param>
        [TestCase(SdmxStructureEnumType.CodeList)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Categorisation)]
        [Ignore("For profiling only. Warning they might take a lot of time to complete.")]
        public void TestGetMutableMaintainablesBurn(SdmxStructureEnumType structureEnumType)
        {
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(this._connectionStringSettings);

            Burn(api, structureEnumType);
        }

        /// <summary>
        /// Test unit for <see cref="ISdmxMutableObjectRetrievalManager.GetMutableMaintainables"/>
        /// </summary>
        /// <param name="structureEnumType">
        /// The structure Type.
        /// </param>
        [TestCase(SdmxStructureEnumType.CodeList)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Categorisation)]
        public void TestGetMutableMaintainablesStub(SdmxStructureEnumType structureEnumType)
        {
            ISdmxMutableObjectRetrievalManager api = this.GetRetrievalManager(this._connectionStringSettings);
            ValidateGetMaintainablesStub(api, structureEnumType);
        }

        #endregion

        /// <summary>
        /// Returns the retrieval manager.
        /// </summary>
        /// <param name="css">
        ///     The connection string settings
        /// </param>
        /// <returns>
        /// The <see cref="ISdmxMutableObjectRetrievalManager"/>.
        /// </returns>
        protected virtual ISdmxMutableObjectRetrievalManager GetRetrievalManager(ConnectionStringSettings css)
        {
            return new MappingStoreRetrievalManager(css);
        }

        /// <summary>
        /// Returns the authorization retrieval manager.
        /// </summary>
        /// <param name="css">
        ///     The connection string settings
        /// </param>
        /// <returns>
        /// The <see cref="ISdmxMutableObjectRetrievalManager"/>.
        /// </returns>
        protected virtual IAuthSdmxMutableObjectRetrievalManager GetAuthRetrievalManager(ConnectionStringSettings css)
        {
            return new AuthMappingStoreRetrievalManager(css, this.GetRetrievalManager(css));
        }
    }
}