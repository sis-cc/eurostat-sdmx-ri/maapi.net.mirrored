// -----------------------------------------------------------------------
// <copyright file="TestPartialConceptSchemeRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2018-06-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using Estat.Sri.MappingStoreRetrieval.Extensions;

namespace MappingStoreRetrieval.Tests
{
    using System.Diagnostics;
    using System.Linq;

    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Estat.Sri.MappingStoreRetrieval.Extensions;

    /// <summary>
    /// Test unit for <see cref="PartialConceptSchemeRetrievalEngine"/>
    /// </summary>
    [TestFixture("sqlserver")]
    [Category("RequiresDB")]
    public class TestPartialConceptSchemeRetrievalEngine
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log;

        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();
        
        private readonly PartialConceptSchemeRetrievalEngine _partialConceptSchemeRetrievalEngine;

        private readonly ConceptSchemeRetrievalEngine _conceptSchemeRetrievalEngine;

        /// <summary>
        /// The mapping store database
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        /// Initializes static members of the <see cref="TestPartialConceptSchemeRetrievalEngine"/> class. 
        /// </summary>
        static TestPartialConceptSchemeRetrievalEngine()
        {
            _log = LogManager.GetLogger(typeof(TestPartialConceptSchemeRetrievalEngine));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestPartialConceptSchemeRetrievalEngine"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public TestPartialConceptSchemeRetrievalEngine(string connectionName)
        {
            var connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(connectionName);
            this._mappingStoreDb = new Database(connectionStringSettings);
            this._partialConceptSchemeRetrievalEngine = new PartialConceptSchemeRetrievalEngine(this._mappingStoreDb);
            this._conceptSchemeRetrievalEngine = new ConceptSchemeRetrievalEngine(this._mappingStoreDb);
        }

        // TODO: see if we need to rewrite any of these tests for msdb 7.0

        ///// <summary>
        ///// 
        ///// </summary>
        //[Test]
        //public void TestRetrieve()
        //{
        //    Stopwatch stopwatch = new Stopwatch();

        //    var conceptSchemeRef = new StructureReferenceImpl("ESTAT", "STS_SCHEME", "1.0", SdmxStructureEnumType.ConceptScheme);
        //    var dsdRef = new StructureReferenceImpl("ESTAT", "STS_OECD", "1.0", SdmxStructureEnumType.Dsd);

        //    stopwatch.Start();
        //    var partialConceptSchemeMutableObjects = this._partialConceptSchemeRetrievalEngine.Retrieve(conceptSchemeRef, dsdRef);
        //    stopwatch.Stop();

        //    var partialConceptsCount = partialConceptSchemeMutableObjects.SelectMany(o => o.Items).Count();

        //    _log.Debug("Partial ConceptSchemes Retrieved: " + partialConceptSchemeMutableObjects.Count);
        //    _log.Debug("Concepts:" + partialConceptsCount + ", Time: " + stopwatch.ElapsedMilliseconds);
        //    _log.Debug("IsPartial: " + partialConceptSchemeMutableObjects.First().IsPartial);

        //    stopwatch.Start();

        //    var conceptSchemeMutableObjects = this._conceptSchemeRetrievalEngine.Retrieve(conceptSchemeRef.CreateComplexStructureReferenceObject(SdmxStructureEnumType.ConceptScheme, false), ComplexStructureQueryDetailEnumType.Full);
        //    stopwatch.Stop();

        //    var conceptsCount = conceptSchemeMutableObjects.SelectMany(o => o.Items).Count();

        //    _log.Debug("ConceptSchemes Retrieved: " + conceptSchemeMutableObjects.Count);
        //    _log.Debug("Concepts:" + conceptsCount + ", Time: " + stopwatch.ElapsedMilliseconds);
        //    _log.Debug("IsPartial: " + conceptSchemeMutableObjects.First().IsPartial);

        //    //TODO
        //    //Assert.IsNotEmpty(partialConceptSchemeMutableObjects);
        //    //Assert.LessOrEqual(stopwatch.ElapsedMilliseconds, time);
        //}
    }
}
