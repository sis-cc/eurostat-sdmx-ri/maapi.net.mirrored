// -----------------------------------------------------------------------
// <copyright file="TestUpdateStatusMapping.cs" company="EUROSTAT">
//   Date Created : 2016-11-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder.MappingLogic;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    using DataSetColumnEntity = Estat.Sri.Mapping.Api.Model.DataSetColumnEntity;

    /// <summary>
    /// Tests for <see cref="UpdateStatusMapping"/>
    /// </summary>
    [TestFixture]
    public class TestUpdateStatusMapping
    {
        /// <summary>
        /// The DDB
        /// </summary>
        private readonly Database _ddb;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestUpdateStatusMapping"/> class.
        /// </summary>
        public TestUpdateStatusMapping()
        {
            var connectionStringSettings = new ConnectionStringSettings() { ProviderName = "System.Data.SqlClient" };
            this._ddb = new Database(connectionStringSettings);
        }

        /// <summary>
        /// Tests the constant exists.
        /// </summary>
        /// <param name="constantValue">The constant value.</param>
        /// <param name="requestedAction">The requested action.</param>
        [TestCase(ObservationActionEnumType.Added, ObservationActionEnumType.Added)]
        [TestCase(ObservationActionEnumType.Added, ObservationActionEnumType.Active)]
        [TestCase(ObservationActionEnumType.Updated, ObservationActionEnumType.Updated)]
        [TestCase(ObservationActionEnumType.Updated, ObservationActionEnumType.Active)]
        public void TestConstant(ObservationActionEnumType constantValue, ObservationActionEnumType requestedAction)
        {
            var entity = new UpdateStatusEntity(new ComponentMappingEntity() { EntityId = "1", ConstantValue = constantValue.ToString() });
            UpdateStatusMapping mapping = new UpdateStatusMapping(entity, this._ddb);
            mapping.GenerateWhere(ObservationAction.GetFromEnum(requestedAction));
        }


        /// <summary>
        /// Tests the constant exists.
        /// </summary>
        /// <param name="constantValue">The constant value.</param>
        /// <param name="requestedAction">The requested action.</param>
        [TestCase(ObservationActionEnumType.Added, ObservationActionEnumType.Updated)]
        [TestCase(ObservationActionEnumType.Updated, ObservationActionEnumType.Added)]
        [TestCase(ObservationActionEnumType.Added, ObservationActionEnumType.Deleted)]
        [TestCase(ObservationActionEnumType.Updated, ObservationActionEnumType.Deleted)]
        public void TestConstantNoResults(ObservationActionEnumType constantValue, ObservationActionEnumType requestedAction)
        {
            var entity = new UpdateStatusEntity(new ComponentMappingEntity() { EntityId = "1", ConstantValue = constantValue.ToString() });
            UpdateStatusMapping mapping = new UpdateStatusMapping(entity, this._ddb);
            Assert.That(() => mapping.GenerateWhere(ObservationAction.GetFromEnum(requestedAction)), Throws.TypeOf<SdmxNoResultsException>());
        }

        /// <summary>
        /// Tests the where clause generation.
        /// </summary>
        /// <param name="requestedAction">The requested action.</param>
        /// <param name="transcodedValues">The transcoded values.</param>
        /// <returns>The <see cref="System.String" />.</returns>
        [TestCase(ObservationActionEnumType.Active, new[] { ObservationActionEnumType.Updated, ObservationActionEnumType.Added, ObservationActionEnumType.Deleted }, ExpectedResult = "(TEST_COLUMN in (@added_param, @updated_param))")]
        [TestCase(ObservationActionEnumType.Active, new[] { ObservationActionEnumType.Updated, ObservationActionEnumType.Deleted }, ExpectedResult = "(TEST_COLUMN = @updated_param)")]
        [TestCase(ObservationActionEnumType.Updated, new[] { ObservationActionEnumType.Updated, ObservationActionEnumType.Added, ObservationActionEnumType.Deleted }, ExpectedResult = "(TEST_COLUMN = @action_param)")]
        [TestCase(ObservationActionEnumType.Deleted, new[] { ObservationActionEnumType.Updated, ObservationActionEnumType.Added, ObservationActionEnumType.Deleted }, ExpectedResult = "(TEST_COLUMN = @action_param)")]
        [TestCase(ObservationActionEnumType.Added, new[] { ObservationActionEnumType.Updated, ObservationActionEnumType.Added, ObservationActionEnumType.Deleted }, ExpectedResult = "(TEST_COLUMN = @action_param)")]
        public string TestWhereClause(ObservationActionEnumType requestedAction, ObservationActionEnumType[] transcodedValues)
        {
            const string ColumnName = "TEST_COLUMN";

            var componentMappingEntity = new ComponentMappingEntity() { EntityId = "1" };
            componentMappingEntity.GetColumns().Add(new DataSetColumnEntity() { Name = ColumnName, EntityId = "2" });
            var entity = new UpdateStatusEntity(componentMappingEntity);
            for (int i = 0; i < transcodedValues.Length; i++)
            {
                string code = i.ToString(CultureInfo.InvariantCulture);
                var obsAction = ObservationAction.GetFromEnum(transcodedValues[i]);
                entity.Add(obsAction.ObsAction, code);
            }

            UpdateStatusMapping mapping = new UpdateStatusMapping(entity, this._ddb);
            var sqlClause = mapping.GenerateWhere(ObservationAction.GetFromEnum(requestedAction));
            return sqlClause.WhereClause;
        }

        /// <summary>
        /// Tests the where clause generation.
        /// </summary>
        /// <param name="requestedAction">The requested action.</param>
        /// <param name="transcodedValues">The transcoded values.</param>
        /// <returns>The <see cref="System.String" />.</returns>
        [TestCase(ObservationActionEnumType.Deleted, new[] { ObservationActionEnumType.Updated, ObservationActionEnumType.Added })]
        [TestCase(ObservationActionEnumType.Added, new[] { ObservationActionEnumType.Updated, ObservationActionEnumType.Deleted })]
        public void TestWhereClauseNoResults(ObservationActionEnumType requestedAction, ObservationActionEnumType[] transcodedValues)
        {
            const string ColumnName = "TEST_COLUMN";

            var componentMappingEntity = new ComponentMappingEntity() { EntityId = "1" };
            componentMappingEntity.GetColumns().Add(new DataSetColumnEntity() { Name = ColumnName, EntityId = "2" });
            var entity = new UpdateStatusEntity(componentMappingEntity);
            for (int i = 0; i < transcodedValues.Length; i++)
            {
                string code = i.ToString(CultureInfo.InvariantCulture);
                var obsAction = ObservationAction.GetFromEnum(transcodedValues[i]);
                entity.Add(obsAction.ObsAction, code);
            }

            UpdateStatusMapping mapping = new UpdateStatusMapping(entity, this._ddb);
            Assert.That(() => mapping.GenerateWhere(ObservationAction.GetFromEnum(requestedAction)), Throws.TypeOf<SdmxNoResultsException>());
        }

        /// <summary>
        /// Tests the where clause generation.
        /// </summary>
        /// <param name="requestedAction">The requested action.</param>
        /// <param name="transcodedValues">The transcoded values.</param>
        /// <returns>The <see cref="System.String" />.</returns>
        [TestCase(ObservationActionEnumType.Active, new[] { ObservationActionEnumType.Updated, ObservationActionEnumType.Added, ObservationActionEnumType.Deleted }, ExpectedResult = 2)]
        [TestCase(ObservationActionEnumType.Updated, new[] { ObservationActionEnumType.Updated, ObservationActionEnumType.Added, ObservationActionEnumType.Deleted }, ExpectedResult = 1)]
        public int TestDbParameterCount(ObservationActionEnumType requestedAction, ObservationActionEnumType[] transcodedValues)
        {
            const string ColumnName = "TEST_COLUMN";

            var componentMappingEntity = new ComponentMappingEntity() { EntityId = "1" };

            componentMappingEntity.GetColumns().Add(new DataSetColumnEntity() { Name = ColumnName, EntityId = "2" });
            var entity = new UpdateStatusEntity(componentMappingEntity);
            for (int i = 0; i < transcodedValues.Length; i++)
            {
                string code = i.ToString(CultureInfo.InvariantCulture);
                var obsAction = ObservationAction.GetFromEnum(transcodedValues[i]);
                entity.Add(obsAction.ObsAction, code);
            }

            UpdateStatusMapping mapping = new UpdateStatusMapping(entity, this._ddb);
            var sqlClause = mapping.GenerateWhere(ObservationAction.GetFromEnum(requestedAction));
            return sqlClause.Parameters.Count;
        }

        /// <summary>
        /// Tests the where clause generation.
        /// </summary>
        /// <param name="requestedAction">The requested action.</param>
        /// <param name="transcodedValues">The transcoded values.</param>
        /// <returns>The <see cref="System.String" />.</returns>
        [TestCase(ObservationActionEnumType.Updated, new[] { ObservationActionEnumType.Updated, ObservationActionEnumType.Added, ObservationActionEnumType.Deleted }, ExpectedResult = "@action_param")]
        public string TestDbParameterNames(ObservationActionEnumType requestedAction, ObservationActionEnumType[] transcodedValues)
        {
            const string ColumnName = "TEST_COLUMN";
            var componentMappingEntity = new ComponentMappingEntity() { EntityId = "1"};

            componentMappingEntity.GetColumns().Add(new DataSetColumnEntity() { Name = ColumnName, EntityId = "2" });
            var entity = new UpdateStatusEntity(componentMappingEntity);

            for (int i = 0; i < transcodedValues.Length; i++)
            {
                string code = i.ToString(CultureInfo.InvariantCulture);
                var obsAction = ObservationAction.GetFromEnum(transcodedValues[i]);
                entity.Add(obsAction.ObsAction, code);
            }

            UpdateStatusMapping mapping = new UpdateStatusMapping(entity, this._ddb);
            var sqlClause = mapping.GenerateWhere(ObservationAction.GetFromEnum(requestedAction));
            return sqlClause.Parameters.First().ParameterName;
        }
    }
}