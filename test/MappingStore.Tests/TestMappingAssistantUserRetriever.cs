// -----------------------------------------------------------------------
// <copyright file="TestMappingAssistantUserRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-05-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System.Configuration;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.MappingStore.Store.Engine;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    [Category("RequiresDB")]
    public class TestMappingAssistantUserRetriever
    {
        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        /// <summary>
        /// The connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        private readonly MappingAssistantUserRetriever _mappingAssistantUserRetriever;

        private SriAuthorizationFactory _factory;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public TestMappingAssistantUserRetriever(string name)
        {
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["MappingStoreRetrieversFactory"]);
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(name);
            var database = new Database(this._connectionStringSettings);
            this._mappingAssistantUserRetriever = new MappingAssistantUserRetriever(new HashBuilder(new BytesConverter()), new BytesConverter(), database, name);
            var maintainableRefRetriever = new MaintainableRefRetrieverEngine(database);
            this._factory = new SriAuthorizationFactory((type, l) => maintainableRefRetriever.RetrievesUrnMapForUser(type, l), new CategorisationRetrievalEngine(database, DataflowFilter.Any));
        }

        [TestCase("user1", "123")]
        public void TestGetPrincipal(string userName, string password)
        {
            var principal = this._mappingAssistantUserRetriever.GetPrincipal(userName, password);
            Assert.That(principal, Is.Not.Null);
            Assert.That(principal.Identity.IsAuthenticated);
            Assert.That(principal.Identity.Name, Is.EqualTo(userName));
        }
        [TestCase("user1", "321")]
        [TestCase("user2", "123")]
        public void TestGetPrincipalUnauthorized(string userName, string password)
        {
            Assert.That(() => this._mappingAssistantUserRetriever.GetPrincipal(userName, password), Throws.TypeOf<SdmxUnauthorisedException>());
        }


        [TestCase("user1", "123")]
        public void TestShouldBeMappingAssistantUser(string userName, string password)
        {
            var principal = this._mappingAssistantUserRetriever.GetPrincipal(userName, password);
            Assert.That(principal, Is.InstanceOf(typeof(MappingAssistantUser)));
            var mauser = (MappingAssistantUser)principal;
            Assert.That(mauser.UserId, Is.GreaterThan(0));
            Assert.That(mauser.StoreId, Is.EqualTo(this._connectionStringSettings.Name));
        }

        [TestCase("user1", "123")]
        public void TestShouldGetAllowedDataflows(string userName, string password)
        {
            var principal = this._mappingAssistantUserRetriever.GetPrincipal(userName, password);
            var dataflowAuthorizationEngine = this._factory.GetEngine(principal);
            Assert.That(dataflowAuthorizationEngine, Is.Not.Null);
            var retrieveAuthorizedDataflows = dataflowAuthorizationEngine.RetrieveAuthorizedDataflows();
            Assert.That(retrieveAuthorizedDataflows, Is.Not.Empty);
        }

        [TestCase("user1", "123", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).6.157", AuthorizationStatus.Authorized)]
        [TestCase("user1", "123", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30", AuthorizationStatus.Authorized)]
        [TestCase("user1", "123", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30.31", AuthorizationStatus.Authorized)]
        [TestCase("user1", "123", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30.264D", AuthorizationStatus.Authorized)]
        [TestCase("user1", "123", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:157_80(1.0)", AuthorizationStatus.Authorized)]
        [TestCase("user1", "123", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:31_124(1.0)", AuthorizationStatus.Authorized)]
        [TestCase("user1", "123", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:264D_264_SALDI(1.0)", AuthorizationStatus.Authorized)]
        [TestCase("user1", "123", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:HC06(1.0)", AuthorizationStatus.Authorized)]
        [TestCase("user1", "123", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:HC06(1.1)", AuthorizationStatus.Denied)]
        [TestCase("user1", "123", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:158_149(1.0)", AuthorizationStatus.Denied)]
        [TestCase("user1", "123", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_A(1.0)", AuthorizationStatus.Denied)]
        [TestCase("user1", "123", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).6", AuthorizationStatus.Denied)]
        public void TestShouldAllowedToAccessDataflow(string userName, string password, string urn, AuthorizationStatus expectedStatus)
        {
            var principal = this._mappingAssistantUserRetriever.GetPrincipal(userName, password);
            var dataflowAuthorizationEngine = this._factory.GetEngine(principal);
            Assert.That(dataflowAuthorizationEngine, Is.Not.Null);
            var retrieveAuthorizedDataflows = dataflowAuthorizationEngine.GetAuthorization(new StructureReferenceImpl(urn), PermissionType.CanReadData);
            Assert.That(retrieveAuthorizedDataflows, Is.EqualTo(expectedStatus));
        }
    }
}