// -----------------------------------------------------------------------
// <copyright file="TestDimensionRefs.cs" company="EUROSTAT">
//   Date Created : 2014-06-13
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStoreRetrieval.Extensions;

namespace MappingStoreRetrieval.Tests
{
    using System.Collections.Generic;
    using System.Configuration;

    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    [Category("RequiresDB")]
    public class TestDimensionRefs
    {

        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        private readonly ConnectionStringSettings _connectionStringSettings;

        private readonly Database _database;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public TestDimensionRefs(string connectionName)
        {
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(connectionName);
            this._database = new Database(this._connectionStringSettings);
        }

        // TODO: see if we need to rewrite any of these tests for msdb 7.0

        //[Test]
        //public void TestDsdRetrieval()
        //{
        //   var retrievalEngine = new DsdRetrievalEngine(this._database);
        //   ISet<IDataStructureMutableObject> dataStructureMutableObjects = retrievalEngine.Retrieve(new MaintainableRefObjectImpl().CreateComplexStructureReferenceObject(SdmxStructureEnumType.Dsd,false), ComplexStructureQueryDetailEnumType.Full);
        //   Assert.NotNull(dataStructureMutableObjects);
        //}
    }
}