using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Estat.Sdmxsource.Extension.Constant;
using Estat.Sdmxsource.Extension.Model.Error;
using Estat.Sri.MappingStoreRetrieval.Engine;
using Estat.Sri.MappingStoreRetrieval.Manager;
using log4net.Config;
using log4net;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using YourNamespace;

namespace MappingStoreRetrieval.Tests
{
    [TestFixture("sqlserver_scratch")]
    public class TestDsdWithSentinel : DataRetriever.Test.TestBase
    {
        public TestDsdWithSentinel(string storeId)
            : base(storeId)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            InitializeMappingStore();
        }
        [Test]
        public void TestSentinel()
        {
            // add dsd
            string structuresFile = @"tests/dsd_new_feature_example.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = StructureSubmitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // retrieve DSD
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV3StructureDocument)
                .SetMaintainableTarget(SdmxStructureEnumType.Dsd)
                .SetMaintainableIds("DSD_NEW_FEATURE")
                .SetAgencyIds("EXAMPLE")
                .SetVersionRequests(new VersionRequestCore("1.0.3"))
                .Build();
            var connectionStringSettings = GetConnectionStringSettings();
            var mappingStoreDatabase = new Database(connectionStringSettings);
            var dsdRetrievalEngine = new DsdRetrievalEngine(mappingStoreDatabase);
            var dsdMutable = dsdRetrievalEngine.Retrieve(structureQuery).Single();


            DiffReport diffReport = new DiffReport(sdmxObjects.DataStructures.First(), dsdMutable.ImmutableInstance);

            Assert.IsEmpty(diffReport.GetDifferences());
            var sentinelValues = dsdMutable.GetAttribute("OBS_PRE_BREAK").Representation.TextFormat.SentinelValues;
            Assert.AreEqual("-1", sentinelValues.First().Value);

            Assert.AreEqual(1, sentinelValues.First().Names.Count);

        }
    }
}
