// -----------------------------------------------------------------------
// <copyright file="TestDatabase.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;

    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    /// <summary>
    /// The test database.
    /// </summary>
    [TestFixture]
    public class TestDatabase
    {

        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        #region Public Methods and Operators
        /// <summary>
        /// Tests the transaction.
        /// </summary>
        /// <param name="name">The name.</param>
        [TestCase("sqlserver")]
        [TestCase("odp")]
        [TestCase("mysql")]
        [Category("RequiresDB")]
        public void TestTransaction(string name)
        {
            ConnectionStringSettings css = this._connectionStringHelper.GetConnectionStringSettings(name);

            var database = new Database(css);
            using (var connection = database.CreateConnection())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    var transactionDatabase = new Database(database, transaction);
                    
                    using (DbCommand dbCommand = transactionDatabase.CreateCommand(CommandType.Text, "select * from artefact"))
                    {
                        using (IDataReader executeReader = transactionDatabase.ExecuteReader(dbCommand))
                        {
                            Assert.True(executeReader.Read());
                        }
                    }

                    transaction.Rollback();
                }
            }
        }

        /// <summary>
        /// Test unit for <see cref="Database.BuildParameterName"/>
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="expectedResult">
        /// The expected Result.
        /// </param>
        [TestCase("sqlserver", "@id")]
        [TestCase("odp", ":id")]
        [TestCase("mysql", "@id")]
        public void TestBuildParameterName(string name, string expectedResult)
        {
            ConnectionStringSettings css = this._connectionStringHelper.GetConnectionStringSettings(name);
            var database = new Database(css);
            Assert.AreEqual(expectedResult, database.BuildParameterName("id"));
        }

        /// <summary>
        /// Test unit for <see cref="Database.CreateCommand"/>
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        [TestCase("sqlserver")]
        [TestCase("odp")]
        [TestCase("mysql")]
        public void TestCreateCommand(string name)
        {
            ConnectionStringSettings css = this._connectionStringHelper.GetConnectionStringSettings(name);
            var database = new Database(css);
            for (int i = 0; i < 1000; i++)
            {
                using (DbCommand dbCommand = database.CreateCommand(CommandType.Text, "select * from artefact"))
                {
                    Assert.NotNull(dbCommand);
                }
            }
        }

        /// <summary>
        /// Test unit for <see cref="Database.ExecuteReader(System.Data.Common.DbCommand, System.String, System.String, System.Int32)"/>
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        [TestCase("sqlserver")]
        [TestCase("odp")]
        [TestCase("mysql")]
        public void TestExecuteReader(string name)
        {
            ConnectionStringSettings css = this._connectionStringHelper.GetConnectionStringSettings(name);
            var database = new Database(css);
            using (DbCommand dbCommand = database.CreateCommand(CommandType.Text, "select * from artefact"))
            {
                using (IDataReader executeReader = database.ExecuteReader(dbCommand))
                {
                    Assert.True(executeReader.Read());
                }
            }
        }

        /// <summary>
        /// Test unit for <see cref="Database.GetSqlStringCommandParam"/>
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        [TestCase("sqlserver")]
        [TestCase("odp")]
        [TestCase("mysql")]
        public void TestGetSqlStringCommandParam(string name)
        {
            ConnectionStringSettings css = this._connectionStringHelper.GetConnectionStringSettings(name);
            var database = new Database(css);
            DbParameter agency = database.CreateInParameter("p1", DbType.String, "MA");
            DbParameter version = database.CreateInParameter("p2", DbType.String, "1.0");
            string query = string.Format(
                CultureInfo.InvariantCulture, "select * from artefact_view where version = {0} and agency = {1}", database.BuildParameterName("p2"), database.BuildParameterName("p1"));

            using (DbCommand dbCommand = database.GetSqlStringCommandParam(query, agency, version))
            {
                using (IDataReader executeReader = database.ExecuteReader(dbCommand))
                {
                    Assert.True(executeReader.Read());
                }
            }

            DbParameter agencya = database.CreateInParameter("p1", DbType.String, "MA");
            DbParameter versiona = database.CreateInParameter("p2", DbType.String, "1.0");
            using (DbCommand dbCommand = database.GetSqlStringCommandParam(query, versiona, agencya))
            {
                using (IDataReader executeReader = database.ExecuteReader(dbCommand))
                {
                    Assert.True(executeReader.Read());
                }
            }
        }

        #endregion
    }
}