// -----------------------------------------------------------------------
// <copyright file="TestICrossReferenceMutableRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Util.Extensions;

namespace MappingStoreRetrieval.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     Test unit for <see cref="ICrossReferenceMutableRetrievalManager" />
    /// </summary>
    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    [Category("RequiresDB")]
    public class TestICrossReferenceMutableRetrievalManager
    {
        /// <summary>
        /// The _from mutable builder.
        /// </summary>
        private static readonly StructureReferenceFromMutableBuilder _fromMutableBuilder = new StructureReferenceFromMutableBuilder();

        /// <summary>
        /// The _connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        /// <summary>
        /// The retrieval container
        /// </summary>
        private readonly IRetrievalEngineContainer _retrievalContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestICrossReferenceMutableRetrievalManager"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public TestICrossReferenceMutableRetrievalManager(string name)
        {
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["MappingStoreRetrieversFactory"]);
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(name);
            this._retrievalContainer = new RetrievalEngineContainerFactory().GetRetrievalEngineContainer(new Database(this._connectionStringSettings));
        }

        #region Public Methods and Operators

        /// <summary>
        /// Test unit for
        /// <see cref="ICrossReferenceMutableRetrievalManager.GetCrossReferencingStructures(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IStructureReference,bool,Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureType[])" />
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        [TestCase(SdmxStructureEnumType.Categorisation, true)]
        [TestCase(SdmxStructureEnumType.Categorisation, false)]
        [TestCase(SdmxStructureEnumType.Dataflow, true)]
        [TestCase(SdmxStructureEnumType.Dataflow, false)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist, true)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist, false)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, true)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, false)]
        [TestCase(SdmxStructureEnumType.CodeList, true)]
        [TestCase(SdmxStructureEnumType.CodeList, false)]
        [TestCase(SdmxStructureEnumType.ConceptScheme, true)]
        [TestCase(SdmxStructureEnumType.ConceptScheme, false)]
        [TestCase(SdmxStructureEnumType.Dsd, true)]
        [TestCase(SdmxStructureEnumType.Dsd, false)]
        public void TestCrossReferencedAuthResultsStructures(SdmxStructureEnumType type, bool returnStub)
        {
            var api = new AuthMappingStoreRetrievalManager(this._retrievalContainer, new MappingStoreRetrievalManager(this._retrievalContainer));

            var mutableDataflowObjects = api.GetMutableCategorisationObjects(new MaintainableRefObjectImpl(), false, false, null);
            var first = mutableDataflowObjects.First();
            IMaintainableRefObject maintainableRef = first.StructureReference.MaintainableReference;
            IMaintainableRefObject maintainableRef2 = new MaintainableRefObjectImpl(first.AgencyId, first.Id + "OO", first.Version);
            IAuthCrossReferenceMutableRetrievalManager crossReferenceMutableRetrieval = new AuthCrossReferenceRetrievalManager(this._retrievalContainer, api);
            ValidateGetMutableMaintableResolve(api, crossReferenceMutableRetrieval, type, new[] { maintainableRef, maintainableRef2 }, 1, returnStub);
        }

        /// <summary>
        /// Test unit for
        /// <see cref="ICrossReferenceMutableRetrievalManager.GetCrossReferencingStructures(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IStructureReference,bool,Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureType[])" />
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="returnStub">The return Stub.</param>
        [TestCase(SdmxStructureEnumType.Categorisation, true)]
        [TestCase(SdmxStructureEnumType.Categorisation, false)]
        [TestCase(SdmxStructureEnumType.Dataflow, true)]
        [TestCase(SdmxStructureEnumType.Dataflow, false)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist, true)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist, false)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, true)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, false)]
        [TestCase(SdmxStructureEnumType.CodeList, true)]
        [TestCase(SdmxStructureEnumType.CodeList, false)]
        [TestCase(SdmxStructureEnumType.ConceptScheme, true)]
        [TestCase(SdmxStructureEnumType.ConceptScheme, false)]
        [TestCase(SdmxStructureEnumType.Dsd, true)]
        [TestCase(SdmxStructureEnumType.Dsd, false)]
        public void TestCrossReferencedAuthStructures(SdmxStructureEnumType type, bool returnStub)
        {
            var sapi = new MappingStoreRetrievalManager(this._retrievalContainer);
            var oapi = new AuthMappingStoreRetrievalManager(this._connectionStringSettings, sapi);
            var api = new AuthCachedRetrievalManager(null, oapi);
            IAuthCrossReferenceMutableRetrievalManager crossReferenceMutableRetrieval = new AuthCrossReferenceRetrievalManager(this._retrievalContainer, api);
            ValidateGetMutableMaintableResolve(api, crossReferenceMutableRetrieval, type, new IMaintainableRefObject[0], 0, returnStub);
        }

        /// <summary>
        /// Test unit for
        ///     <see cref="ICrossReferenceMutableRetrievalManager.GetCrossReferencingStructures(IStructureReference,bool,Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureType[])"/>
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="returnStub">
        /// The return Stub.
        /// </param>
        [TestCase(SdmxStructureEnumType.Categorisation, true)]
        [TestCase(SdmxStructureEnumType.Categorisation, false)]
        [TestCase(SdmxStructureEnumType.Dataflow, true)]
        [TestCase(SdmxStructureEnumType.Dataflow, false)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist, true)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist, false)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, true)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, false)]
        [TestCase(SdmxStructureEnumType.CodeList, true)]
        [TestCase(SdmxStructureEnumType.CodeList, false)]
        [TestCase(SdmxStructureEnumType.ConceptScheme, true)]
        [TestCase(SdmxStructureEnumType.ConceptScheme, false)]
        [TestCase(SdmxStructureEnumType.Dsd, true)]
        [TestCase(SdmxStructureEnumType.Dsd, false)]
        public void TestCrossReferencingAuthResultsStructures(SdmxStructureEnumType type, bool returnStub)
        {
            var sapi = new MappingStoreRetrievalManager(this._retrievalContainer);
            var oapi = new AuthMappingStoreRetrievalManager(this._connectionStringSettings, sapi);
            var api = new AuthCachedRetrievalManager(null, oapi);
            IAuthCrossReferenceMutableRetrievalManager crossReferenceMutableRetrieval = new AuthCrossReferenceRetrievalManager(this._retrievalContainer, api);
            var mutableDataflowObjects = sapi.GetMutableCategorisationObjects(new MaintainableRefObjectImpl(), false, false);
            var first = mutableDataflowObjects.First();
            IMaintainableRefObject maintainableRef = first.StructureReference.MaintainableReference;
            IMaintainableRefObject maintainableRef2 = new MaintainableRefObjectImpl(first.AgencyId, first.Id + "OO", first.Version);
            ValidateReferencing(api, crossReferenceMutableRetrieval, type, new[] { maintainableRef, maintainableRef2 }, 1, returnStub);
        }

        /// <summary>
        /// Test unit for
        /// <see cref="ICrossReferenceMutableRetrievalManager.GetCrossReferencingStructures(IStructureReference,bool,Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureType[])" />
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        [TestCase(SdmxStructureEnumType.Categorisation, true)]
        [TestCase(SdmxStructureEnumType.Categorisation, false)]
        [TestCase(SdmxStructureEnumType.Dataflow, true)]
        [TestCase(SdmxStructureEnumType.Dataflow, false)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist, true)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist, false)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, true)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, false)]
        [TestCase(SdmxStructureEnumType.CodeList, true)]
        [TestCase(SdmxStructureEnumType.CodeList, false)]
        [TestCase(SdmxStructureEnumType.ConceptScheme, true)]
        [TestCase(SdmxStructureEnumType.ConceptScheme, false)]
        [TestCase(SdmxStructureEnumType.Dsd, true)]
        [TestCase(SdmxStructureEnumType.Dsd, false)]
        public void TestCrossReferencingAuthStructures(SdmxStructureEnumType type, bool returnStub)
        {
            var sapi = new MappingStoreRetrievalManager(this._retrievalContainer);
            var oapi = new AuthMappingStoreRetrievalManager(this._connectionStringSettings, sapi);
            var api = new AuthCachedRetrievalManager(null, oapi);
            IAuthCrossReferenceMutableRetrievalManager crossReferenceMutableRetrieval = new AuthCrossReferenceRetrievalManager(this._retrievalContainer, api);
            ValidateReferencing(api, crossReferenceMutableRetrieval, type, new IMaintainableRefObject[0], 0, returnStub);
        }

        /// <summary>
        /// Test unit for
        /// <see cref="ICrossReferenceMutableRetrievalManager.GetCrossReferencingStructures(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IStructureReference,Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureType[])" />
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        [TestCase(SdmxStructureEnumType.Categorisation, true)]
        [TestCase(SdmxStructureEnumType.Categorisation, false)]
        [TestCase(SdmxStructureEnumType.Dataflow, true)]
        [TestCase(SdmxStructureEnumType.Dataflow, false)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist, true)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist, false)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, true)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, false)]
        [TestCase(SdmxStructureEnumType.CodeList, true)]
        [TestCase(SdmxStructureEnumType.CodeList, false)]
        [TestCase(SdmxStructureEnumType.ConceptScheme, true)]
        [TestCase(SdmxStructureEnumType.ConceptScheme, false)]
        [TestCase(SdmxStructureEnumType.Dsd, true)]
        [TestCase(SdmxStructureEnumType.Dsd, false)]
        public void TestCrossReferencingStructures(SdmxStructureEnumType type, bool returnStub)
        {
            var oapi = new MappingStoreRetrievalManager(this._retrievalContainer);
            var api = new CachedRetrievalManager(null, oapi);
            var crossReferenceMutableRetrieval = new CrossReferenceRetrievalManager(api, this._retrievalContainer);
            ValidateReferencing(api, crossReferenceMutableRetrieval, type, returnStub);
        }

        /// <summary>
        /// Test unit for
        ///     <see cref="ICrossReferenceMutableRetrievalManager.GetCrossReferencedStructures(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IStructureReference,Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureType[])"/>
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="detail">
        /// The detail.
        /// </param>
        [TestCase(SdmxStructureEnumType.Categorisation, true)]
        [TestCase(SdmxStructureEnumType.Categorisation, false)]
        [TestCase(SdmxStructureEnumType.Dataflow, true)]
        [TestCase(SdmxStructureEnumType.Dataflow, false)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist, true)]
        [TestCase(SdmxStructureEnumType.HierarchicalCodelist, false)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, true)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, false)]
        [TestCase(SdmxStructureEnumType.CodeList, true)]
        [TestCase(SdmxStructureEnumType.CodeList, false)]
        [TestCase(SdmxStructureEnumType.ConceptScheme, true)]
        [TestCase(SdmxStructureEnumType.ConceptScheme, false)]
        [TestCase(SdmxStructureEnumType.Dsd, true)]
        [TestCase(SdmxStructureEnumType.Dsd, false)]
        public void TestGetCrossReferencedStructures(SdmxStructureEnumType type, bool returnStub)
        {
            var oapi = new MappingStoreRetrievalManager(this._retrievalContainer);
            var api = new CachedRetrievalManager(null, oapi);
            var crossReferenceMutableRetrieval = new CrossReferenceRetrievalManager(api, this._retrievalContainer);
            ValidateGetMutableMaintableResolve(api, crossReferenceMutableRetrieval, type, returnStub);
        }

        [Test]
        public void TestCrossReferenceFromDsd()
        {
            var mappingStoreDb = new Database(this._connectionStringSettings);
            var authAdvancedStructureRetriever = new AuthAdvancedStructureRetriever(mappingStoreDb);
            var cachedRetriever = new AuthCachedAdvancedStructureRetriever(authAdvancedStructureRetriever);
            var structureReference = new StructureReferenceImpl(new MaintainableRefObjectImpl(), SdmxStructureEnumType.Dsd).ToComplex();
            var dsds = authAdvancedStructureRetriever.GetMutableDataStructureObjects(structureReference, ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full));
            IAuthCrossReferenceMutableRetrievalManager manager = new AuthCrossReferenceRetrievalManager(cachedRetriever, mappingStoreDb);
            var crossReferencedStructures = manager.GetCrossReferencedStructures(dsds.First(), false.GetStructureQueryDetail(), null);
            Assert.That(crossReferencedStructures, Is.Not.Null.Or.Empty);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validate the <see cref="ISdmxMutableObjectRetrievalManager.GetMutableMaintainableWithReferences"/>
        /// </summary>
        /// <param name="api">
        ///     The <see cref="ISdmxMutableObjectRetrievalManager"/> to test
        /// </param>
        /// <param name="crossReference">
        ///     The cross Reference.
        /// </param>
        /// <param name="type">
        ///     The structure type
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        /// <param name="expected">
        ///     The expected.
        /// </param>
        /// <param name="returnStub"></param>
        protected static void ValidateGetMutableMaintableResolve(IAuthSdmxMutableObjectRetrievalManager api, IAuthCrossReferenceMutableRetrievalManager crossReference, SdmxStructureEnumType type, IList<IMaintainableRefObject> allowedDataflows, int expected, bool returnStub)
        {
            SdmxStructureType structureType = SdmxStructureType.GetFromEnum(type);
            IStructureReference query = new StructureReferenceImpl(new MaintainableRefObjectImpl(), structureType);
            switch (structureType.EnumType)
            {
                case SdmxStructureEnumType.Categorisation:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false, null);
                        Assert.IsNotEmpty(startList);
                        var got = new List<IMaintainableRefObject>();
                        foreach (var mutableObject in startList)
                        {
                            try
                            {
                                IList<IMaintainableMutableObject> list = crossReference.GetCrossReferencedStructures(mutableObject, returnStub.GetStructureQueryDetail(), allowedDataflows);

                                var categorisation = mutableObject as ICategorisationMutableObject;
                                got.AddRange(from l in list where l.StructureType.EnumType == SdmxStructureEnumType.Dataflow select _fromMutableBuilder.Build(l).MaintainableReference);

                                Assert.NotNull(categorisation);
                                if (expected > 0)
                                {
                                    // DF
                                    StructureReferenceImpl[] dataflow =
                                        (from m in list where m.StructureType.EnumType == SdmxStructureEnumType.Dataflow select new StructureReferenceImpl(m.AgencyId, m.Id, m.Version, m.StructureType))
                                            .ToArray();
                                    Assert.AreEqual(1, dataflow.Length);
                                    CollectionAssert.Contains(dataflow, categorisation.StructureReference);

                                    // Category Scheme
                                    MaintainableRefObjectImpl[] categoryScheme =
                                        (from m in list where m.StructureType.EnumType == SdmxStructureEnumType.Dataflow select new MaintainableRefObjectImpl(m.AgencyId, m.Id, m.Version)).ToArray();
                                    Assert.AreEqual(1, categoryScheme.Length);
                                    CollectionAssert.Contains(categoryScheme, categorisation.StructureReference.MaintainableReference);
                                }
                                else
                                {
                                    Assert.IsEmpty(list);
                                }
                            }
                            catch (SdmxReferenceException)
                            {
                            }
                        }
                        
                        CollectionAssert.IsSubsetOf(got.Distinct(), allowedDataflows);
                    }

                    break;
                case SdmxStructureEnumType.CategoryScheme:
                case SdmxStructureEnumType.CodeList:
                case SdmxStructureEnumType.ConceptScheme:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false, null);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencedStructures(mutableObject, returnStub.GetStructureQueryDetail(), allowedDataflows);
                            Assert.IsEmpty(list);
                        }
                    }

                    break;
                case SdmxStructureEnumType.Dataflow:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false, null);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencedStructures(mutableObject, returnStub.GetStructureQueryDetail(), allowedDataflows);
                                Assert.IsNotEmpty(list);
                                Assert.GreaterOrEqual(list.Count, expected);
                                var dataflow = mutableObject as IDataflowMutableObject;
                                Assert.NotNull(dataflow);

                                // DSD
                                var dsds = (from m in list where m.StructureType.EnumType == SdmxStructureEnumType.Dsd select m).Distinct().ToArray();
                                Assert.AreEqual(1, dsds.Length);
                                IMaintainableMutableObject dsd = dsds[0];
                                Assert.AreEqual(SdmxStructureEnumType.Dsd, dsd.StructureType.EnumType);
                                Assert.IsTrue(dataflow.DataStructureRef.MaintainableReference.Equals(new MaintainableRefObjectImpl(dsd.AgencyId, dsd.Id, dsd.Version)));
                            
                        }
                    }

                    break;
                case SdmxStructureEnumType.Dsd:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false, null);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencedStructures(mutableObject, returnStub.GetStructureQueryDetail(), allowedDataflows);

                            var dsd = mutableObject as IDataStructureMutableObject;
                            Assert.NotNull(dsd);
                            if (!dsd.Stub)
                            {
                                var components = new List<IComponentMutableObject>(dsd.Dimensions);
                                if (dsd.AttributeList != null)
                                {
                                    components.AddRange(dsd.AttributeList.Attributes);
                                }

                                components.Add(dsd.PrimaryMeasure);

                                // CL
                                var codelistsUsed = from componentMutableObject in components
                                                    where
                                                        componentMutableObject.Representation != null && componentMutableObject.Representation.Representation != null
                                                        && componentMutableObject.Representation.Representation.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.CodeList
                                                    select componentMutableObject.Representation.Representation;
                                IEnumerable<StructureReferenceImpl> gotCodelists = from m in list
                                                                                   where m.StructureType.EnumType == SdmxStructureEnumType.CodeList
                                                                                   select new StructureReferenceImpl(m.AgencyId, m.Id, m.Version, m.StructureType);
                                var codelistUsedSet = new HashSet<IStructureReference>(codelistsUsed);
                                var cross = dsd as ICrossSectionalDataStructureMutableObject;
                                if (cross != null)
                                {
                                    codelistUsedSet.UnionWith(cross.MeasureDimensionCodelistMapping.Values);
                                }

                                CollectionAssert.AreEquivalent(codelistUsedSet, gotCodelists.Distinct());

                                // Concept Scheme
                                IEnumerable<IMaintainableRefObject> conceptsUsed = (from c in components select c.ConceptRef.MaintainableReference).Distinct();

                                IEnumerable<IMaintainableRefObject> gotConceptSchemes = (from m in list
                                                                                           where m.StructureType.EnumType == SdmxStructureEnumType.ConceptScheme
                                                                                           select new MaintainableRefObjectImpl(m.AgencyId, m.Id, m.Version)).Distinct();
                                ISet<IMaintainableRefObject> conceptsReferenced = new HashSet<IMaintainableRefObject>(conceptsUsed);
                                conceptsReferenced.UnionWith(dsd.Dimensions.Where(o => o.MeasureDimension).Select(o => o.Representation.Representation.MaintainableReference));
                                conceptsReferenced.UnionWith(dsd.Dimensions.SelectMany(o => o.ConceptRole).Select(reference => reference.MaintainableReference));
                                if (dsd.Attributes != null)
                                {
                                    conceptsReferenced.UnionWith(
                                        dsd.Attributes.SelectMany(o => o.ConceptRoles)
                                            .Select(reference => reference.MaintainableReference));
                                }

                                CollectionAssert.AreEquivalent(conceptsReferenced, gotConceptSchemes);
                            }
                        }
                    }

                    break;
                case SdmxStructureEnumType.HierarchicalCodelist:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false, allowedDataflows);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencedStructures(mutableObject, returnStub.GetStructureQueryDetail(), allowedDataflows);
                            var hcl = mutableObject as IHierarchicalCodelistMutableObject;
                            Assert.NotNull(hcl);

                            // CL
                            IEnumerable<IStructureReference> codelistsUsed = (from cl in hcl.CodelistRef select cl.CodelistReference).Distinct();

                            IEnumerable<StructureReferenceImpl> gotCodelists =
                                (from m in list where m.StructureType.EnumType == SdmxStructureEnumType.CodeList select new StructureReferenceImpl(m.AgencyId, m.Id, m.Version, m.StructureType))
                                    .Distinct();

                            CollectionAssert.AreEquivalent(codelistsUsed, gotCodelists);
                        }
                    }

                    break;
                case SdmxStructureEnumType.Registration:
                case SdmxStructureEnumType.Subscription:
                case SdmxStructureEnumType.AttachmentConstraint:
                    Assert.Throws(typeof(SdmxNotImplementedException), () => api.GetMutableMaintainable(query, false, false, allowedDataflows), structureType.ToString());
                    break;
                default:
                    Assert.Throws(typeof(NotImplementedException), () => api.GetMutableMaintainable(query, false, false, allowedDataflows), structureType.ToString());
                    break;
            }
        }

        /// <summary>
        /// Validate the <see cref="ISdmxMutableObjectRetrievalManager.GetMutableMaintainableWithReferences"/>
        /// </summary>
        /// <param name="api">
        ///     The <see cref="ISdmxMutableObjectRetrievalManager"/> to test
        /// </param>
        /// <param name="crossReference">
        ///     The cross Reference.
        /// </param>
        /// <param name="type">
        ///     The structure type
        /// </param>
        /// <param name="returnStub"></param>
        protected static void ValidateGetMutableMaintableResolve(ISdmxMutableObjectRetrievalManager api, ICrossReferenceMutableRetrievalManager crossReference, SdmxStructureEnumType type, bool returnStub)
        {
            SdmxStructureType structureType = SdmxStructureType.GetFromEnum(type);
            IStructureReference query = new StructureReferenceImpl(new MaintainableRefObjectImpl(), structureType);
            switch (structureType.EnumType)
            {
                case SdmxStructureEnumType.Categorisation:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencedStructures(mutableObject, returnStub.GetStructureQueryDetail());

                            var categorisation = mutableObject as ICategorisationMutableObject;
                            Assert.NotNull(categorisation);
                            if (!categorisation.Stub)
                            {
                                // DF
                                StructureReferenceImpl[] dataflow =
                                    (from m in list where m.StructureType.EnumType == SdmxStructureEnumType.Dataflow select new StructureReferenceImpl(m.AgencyId, m.Id, m.Version, m.StructureType))
                                        .ToArray();
                                Assert.AreEqual(1, dataflow.Length);
                                CollectionAssert.Contains(dataflow, categorisation.StructureReference);

                                // Category Scheme
                                MaintainableRefObjectImpl[] categoryScheme =
                                    (from m in list where m.StructureType.EnumType == SdmxStructureEnumType.Dataflow select new MaintainableRefObjectImpl(m.AgencyId, m.Id, m.Version)).ToArray();
                                Assert.AreEqual(1, categoryScheme.Length);
                                CollectionAssert.Contains(categoryScheme, categorisation.StructureReference.MaintainableReference);
                            }
                        }
                    }

                    break;
                case SdmxStructureEnumType.CategoryScheme:
                case SdmxStructureEnumType.CodeList:
                case SdmxStructureEnumType.ConceptScheme:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencedStructures(mutableObject, returnStub.GetStructureQueryDetail());
                            Assert.IsEmpty(list);
                        }
                    }

                    break;
                case SdmxStructureEnumType.Dataflow:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencedStructures(mutableObject, returnStub.GetStructureQueryDetail());
                            Assert.IsNotEmpty(list,_fromMutableBuilder.Build(mutableObject).ToString());
                            var dataflow = mutableObject as IDataflowMutableObject;
                            Assert.NotNull(dataflow);
                            if (!dataflow.Stub)
                            {
                                // DSD
                                var dsds = (from m in list where m.StructureType.EnumType == SdmxStructureEnumType.Dsd select m).Distinct().ToArray();
                                Assert.AreEqual(1, dsds.Length);
                                IMaintainableMutableObject dsd = dsds[0];
                                Assert.AreEqual(SdmxStructureEnumType.Dsd, dsd.StructureType.EnumType);
                                Assert.IsTrue(dataflow.DataStructureRef.MaintainableReference.Equals(new MaintainableRefObjectImpl(dsd.AgencyId, dsd.Id, dsd.Version)));
                            }
                        }
                    }

                    break;
                case SdmxStructureEnumType.Dsd:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencedStructures(mutableObject, returnStub.GetStructureQueryDetail());

                            var dsd = mutableObject as IDataStructureMutableObject;
                            Assert.NotNull(dsd);
                            if (!dsd.Stub)
                            {
                                var components = new List<IComponentMutableObject>(dsd.Dimensions);
                                if (dsd.AttributeList != null)
                                {
                                    components.AddRange(dsd.AttributeList.Attributes);
                                }

                                components.Add(dsd.PrimaryMeasure);

                                // CL
                                var codelistsUsed = from componentMutableObject in components
                                                    where
                                                        componentMutableObject.Representation != null && componentMutableObject.Representation.Representation != null
                                                        && componentMutableObject.Representation.Representation.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.CodeList
                                                    select componentMutableObject.Representation.Representation;
                                IEnumerable<StructureReferenceImpl> gotCodelists = from m in list
                                                                                   where m.StructureType.EnumType == SdmxStructureEnumType.CodeList
                                                                                   select new StructureReferenceImpl(m.AgencyId, m.Id, m.Version, m.StructureType);
                                var codelistUsedSet = new HashSet<IStructureReference>(codelistsUsed);
                                var cross = dsd as ICrossSectionalDataStructureMutableObject;
                                if (cross != null)
                                {
                                    codelistUsedSet.UnionWith(cross.MeasureDimensionCodelistMapping.Values);
                                }

                                CollectionAssert.AreEquivalent(codelistUsedSet, gotCodelists.Distinct());

                                // Concept Scheme
                                IEnumerable<IMaintainableRefObject> conceptsUsed = (from c in components select c.ConceptRef.MaintainableReference).Union(from c in dsd.Dimensions where c.MeasureDimension select  c.Representation.Representation.MaintainableReference).Union(components.SelectMany(o => o.GetConceptRoles().Select(reference => reference.MaintainableReference)));
                                IEnumerable<MaintainableRefObjectImpl> gotConceptSchemes = from m in list
                                                                                           where m.StructureType.EnumType == SdmxStructureEnumType.ConceptScheme
                                                                                           select new MaintainableRefObjectImpl(m.AgencyId, m.Id, m.Version);

                                CollectionAssert.AreEquivalent(conceptsUsed.Distinct(), gotConceptSchemes.Distinct());
                            }
                        }
                    }

                    break;
                case SdmxStructureEnumType.HierarchicalCodelist:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencedStructures(mutableObject, returnStub.GetStructureQueryDetail());
                            var hcl = mutableObject as IHierarchicalCodelistMutableObject;
                            Assert.NotNull(hcl);

                            // CL
                            IEnumerable<IStructureReference> codelistsUsed = (from cl in hcl.CodelistRef select cl.CodelistReference).Distinct();

                            IEnumerable<StructureReferenceImpl> gotCodelists =
                                (from m in list where m.StructureType.EnumType == SdmxStructureEnumType.CodeList select new StructureReferenceImpl(m.AgencyId, m.Id, m.Version, m.StructureType))
                                    .Distinct();

                            CollectionAssert.AreEquivalent(codelistsUsed, gotCodelists);
                        }
                    }

                    break;
                case SdmxStructureEnumType.Registration:
                case SdmxStructureEnumType.Subscription:
                case SdmxStructureEnumType.AttachmentConstraint:
                    Assert.Throws(typeof(SdmxNotImplementedException), () => api.GetMutableMaintainable(query, false, false), structureType.ToString());
                    break;
                default:
                    Assert.Throws(typeof(NotImplementedException), () => api.GetMutableMaintainable(query, false, false), structureType.ToString());
                    break;
            }
        }

        /// <summary>
        /// Validate referencing.
        /// </summary>
        /// <param name="api">
        ///     The API.
        /// </param>
        /// <param name="crossReference">
        ///     The cross reference.
        /// </param>
        /// <param name="type">
        ///     The structure type
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        /// <param name="expected">
        ///     The expected count.
        /// </param>
        /// <param name="returnStub"></param>
        private static void ValidateReferencing(IAuthSdmxMutableObjectRetrievalManager api, IAuthCrossReferenceMutableRetrievalManager crossReference, SdmxStructureEnumType type, IList<IMaintainableRefObject> allowedDataflows, int expected, bool returnStub)
        {
            SdmxStructureType structureType = SdmxStructureType.GetFromEnum(type);
            IStructureReference query = new StructureReferenceImpl(new MaintainableRefObjectImpl(), structureType);
            switch (structureType.EnumType)
            {
                case SdmxStructureEnumType.Categorisation:
                case SdmxStructureEnumType.HierarchicalCodelist:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false, null);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencingStructures(mutableObject, returnStub.GetStructureQueryDetail(), allowedDataflows);
                            Assert.IsEmpty(list);
                        }
                    }

                    break;
                case SdmxStructureEnumType.CategoryScheme:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false, null);

                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencingStructures(mutableObject, returnStub.GetStructureQueryDetail(), allowedDataflows);
                            if (!returnStub)
                            {
                                if (list.Count > 0)
                                {
                                    if (expected > 0)
                                    {
                                        // categorisations
                                        CollectionAssert.AllItemsAreInstancesOfType(list, typeof(ICategorisationMutableObject));
                                    }

                                    var ddataflowRef = (from d in list.Cast<ICategorisationMutableObject>() select d.StructureReference.MaintainableReference).Distinct().ToArray();
                                    CollectionAssert.IsSubsetOf(ddataflowRef, allowedDataflows);
                                }
                            }
                        }
                    }

                    break;
                case SdmxStructureEnumType.Dataflow:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false, null);
                        foreach (var mutableObject in startList)
                        {
                            var maintainableRefObject = _fromMutableBuilder.Build(mutableObject).MaintainableReference;
                            bool contains = allowedDataflows.Contains(maintainableRefObject);
                            var crossReferencingStructures = crossReference.GetCrossReferencingStructures(mutableObject, returnStub.GetStructureQueryDetail(), allowedDataflows);
                            if (contains)
                            {
                                var list = crossReferencingStructures;
                                Assert.IsNotEmpty(list);

                                if (expected > 0)
                                {
                                    // categorisations
                                    CollectionAssert.AllItemsAreInstancesOfType(list, typeof(ICategorisationMutableObject));
                                }
                            }
                            else
                            {
                                // FIXME Content Constraints use allowed dataflows
                                Assert.IsEmpty(crossReferencingStructures.Where(x => x.StructureType.EnumType != SdmxStructureEnumType.ContentConstraint));
                            }
                        }
                    }

                    break;
                case SdmxStructureEnumType.CodeList:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false, null);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencingStructures(mutableObject, returnStub.GetStructureQueryDetail(), allowedDataflows);

                            // DSD or HCL
                            Assert.False(list.Any(o => o.StructureType.EnumType != SdmxStructureEnumType.Dsd && o.StructureType.EnumType != SdmxStructureEnumType.HierarchicalCodelist));
                        }
                    }

                    break;
                case SdmxStructureEnumType.ConceptScheme:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false, null);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencingStructures(mutableObject, returnStub.GetStructureQueryDetail(), allowedDataflows);

                            // DSD
                            CollectionAssert.AllItemsAreInstancesOfType(list, typeof(IDataStructureMutableObject));
                        }
                    }

                    break;

                case SdmxStructureEnumType.Dsd:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false, null);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencingStructures(mutableObject, returnStub.GetStructureQueryDetail(), allowedDataflows);

                            // DSD
                            CollectionAssert.AllItemsAreInstancesOfType(list, typeof(IDataflowMutableObject));
                        }
                    }

                    break;

                case SdmxStructureEnumType.Registration:
                case SdmxStructureEnumType.Subscription:
                case SdmxStructureEnumType.AttachmentConstraint:
                    Assert.Throws(typeof(SdmxNotImplementedException), () => api.GetMutableMaintainable(query, false, false, null), structureType.ToString());
                    break;
                default:
                    Assert.Throws(typeof(NotImplementedException), () => api.GetMutableMaintainable(query, false, false, null), structureType.ToString());
                    break;
            }
        }

        /// <summary>
        /// Validate referencing.
        /// </summary>
        /// <param name="api">
        ///     The API.
        /// </param>
        /// <param name="crossReference">
        ///     The cross reference.
        /// </param>
        /// <param name="type">
        ///     The structure type
        /// </param>
        /// <param name="returnStub"></param>
        private static void ValidateReferencing(ISdmxMutableObjectRetrievalManager api, ICrossReferenceMutableRetrievalManager crossReference, SdmxStructureEnumType type, bool returnStub)
        {
            SdmxStructureType structureType = SdmxStructureType.GetFromEnum(type);
            IStructureReference query = new StructureReferenceImpl(new MaintainableRefObjectImpl(), structureType);
            switch (structureType.EnumType)
            {
                case SdmxStructureEnumType.Categorisation:
                case SdmxStructureEnumType.HierarchicalCodelist:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencingStructures(mutableObject, returnStub.GetStructureQueryDetail());
                            Assert.IsEmpty(list);
                            IMutableObjects mutableObjects = new MutableObjectsImpl(list);
                            var immutableObjects = mutableObjects.ImmutableObjects;
                            Assert.NotNull(immutableObjects);
                        }
                    }

                    break;
                case SdmxStructureEnumType.CategoryScheme:
                case SdmxStructureEnumType.Dataflow:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencingStructures(mutableObject, returnStub.GetStructureQueryDetail());
                            Assert.False(list.Any(o => !o.StructureType.EnumType.IsOneOf(SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.ProvisionAgreement, SdmxStructureEnumType.ContentConstraint, SdmxStructureEnumType.StructureSet)));
                            IMutableObjects mutableObjects = new MutableObjectsImpl(list);
                            var immutableObjects = mutableObjects.ImmutableObjects;
                            Assert.NotNull(immutableObjects);
                        }
                    }

                    break;
                case SdmxStructureEnumType.CodeList:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencingStructures(mutableObject, returnStub.GetStructureQueryDetail());

                            // DSD or HCL
                            Assert.False(list.Any(o => o.StructureType.EnumType != SdmxStructureEnumType.Dsd && o.StructureType.EnumType != SdmxStructureEnumType.HierarchicalCodelist));
                            IMutableObjects mutableObjects = new MutableObjectsImpl(list);
                            var immutableObjects = mutableObjects.ImmutableObjects;
                            Assert.NotNull(immutableObjects);
                        }
                    }

                    break;
                case SdmxStructureEnumType.ConceptScheme:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencingStructures(mutableObject, returnStub.GetStructureQueryDetail());

                            // DSD
                            CollectionAssert.AllItemsAreInstancesOfType(list, typeof(IDataStructureMutableObject));
                            IMutableObjects mutableObjects = new MutableObjectsImpl(list);
                            var immutableObjects = mutableObjects.ImmutableObjects;
                            Assert.NotNull(immutableObjects);
                        }
                    }

                    break;

                case SdmxStructureEnumType.Dsd:
                    {
                        ISet<IMaintainableMutableObject> startList = api.GetMutableMaintainables(query, false, false);
                        Assert.IsNotEmpty(startList);
                        foreach (var mutableObject in startList)
                        {
                            var list = crossReference.GetCrossReferencingStructures(mutableObject, returnStub.GetStructureQueryDetail());

                            // DSD
                            CollectionAssert.AllItemsAreInstancesOfType(list, typeof(IDataflowMutableObject));
                            IMutableObjects mutableObjects = new MutableObjectsImpl(list);
                            var immutableObjects = mutableObjects.ImmutableObjects;
                            Assert.NotNull(immutableObjects);
                        }
                    }

                    break;

                case SdmxStructureEnumType.Registration:
                case SdmxStructureEnumType.Subscription:
                case SdmxStructureEnumType.AttachmentConstraint:
                    Assert.Throws(typeof(SdmxNotImplementedException), () => api.GetMutableMaintainable(query, false, false), structureType.ToString());
                    break;
                default:
                    Assert.Throws(typeof(NotImplementedException), () => api.GetMutableMaintainable(query, false, false), structureType.ToString());
                    break;
            }
        }

        #endregion
    }
}