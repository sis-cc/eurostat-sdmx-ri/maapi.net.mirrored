﻿// -----------------------------------------------------------------------
// <copyright file="TestMappingAssistantUser.cs" company="EUROSTAT">
//   Date Created : 2017-05-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStoreRetrieval.Tests
{
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using NUnit.Framework;

    public class TestMappingAssistantUser
    {
        [TestCase(PermissionType.AdminRole, PermissionType.AdminRole,  true)]
        [TestCase(PermissionType.AdminRole, PermissionType.CanIgnoreProductionFlag,  true)]
        [TestCase(PermissionType.AdminRole, PermissionType.WsUserRole,  true)]
        [TestCase(PermissionType.WsUserRole, PermissionType.CanIgnoreProductionFlag,  false)]
        [TestCase(PermissionType.WsUserRole, PermissionType.AdminRole,  false)]
        [TestCase(PermissionType.WsUserRole, PermissionType.CanReadData,  true)]
        [TestCase(PermissionType.WsUserRole, PermissionType.CanImportData,  false)]
        [TestCase(PermissionType.WsUserRole, PermissionType.CanReadStructuralMetadata,  true)]
        [TestCase(PermissionType.WsUserRole, PermissionType.CanImportStructures,  false)]
        [TestCase(PermissionType.WsUserRole, PermissionType.DomainUserRole,  false)]
        [TestCase(PermissionType.DomainUserRole, PermissionType.DomainUserRole,  true)]
        [TestCase(PermissionType.DomainUserRole, PermissionType.WsUserRole,  true)]
        [TestCase(PermissionType.DomainUserRole, PermissionType.StructureImporterRole,  false)]
        [TestCase(PermissionType.DomainUserRole, PermissionType.DataImporterRole,  false)]
        [TestCase(PermissionType.DomainUserRole, PermissionType.CanModifyStoreSettings,  false)]
        [TestCase(PermissionType.DomainUserRole, PermissionType.CanIgnoreProductionFlag,  true)]
        public void TestRoleFlags(PermissionType userPermission, PermissionType requestedAttributeOrRole, bool expectedValue)
        {
            Assert.That(userPermission.HasFlag(requestedAttributeOrRole), Is.EqualTo(expectedValue));
        }

        [TestCase(PermissionType.AdminRole, PermissionType.AdminRole, true)]
        [TestCase(PermissionType.AdminRole, PermissionType.CanIgnoreProductionFlag, true)]
        [TestCase(PermissionType.AdminRole, PermissionType.WsUserRole, true)]
        [TestCase(PermissionType.WsUserRole, PermissionType.CanIgnoreProductionFlag, false)]
        [TestCase(PermissionType.WsUserRole, PermissionType.AdminRole, false)]
        [TestCase(PermissionType.WsUserRole, PermissionType.CanReadData, true)]
        [TestCase(PermissionType.WsUserRole, PermissionType.CanImportData, false)]
        [TestCase(PermissionType.WsUserRole, PermissionType.CanReadStructuralMetadata, true)]
        [TestCase(PermissionType.WsUserRole, PermissionType.CanImportStructures, false)]
        [TestCase(PermissionType.WsUserRole, PermissionType.DomainUserRole, false)]
        [TestCase(PermissionType.DomainUserRole, PermissionType.DomainUserRole, true)]
        [TestCase(PermissionType.DomainUserRole, PermissionType.WsUserRole, true)]
        [TestCase(PermissionType.DomainUserRole, PermissionType.StructureImporterRole, false)]
        [TestCase(PermissionType.DomainUserRole, PermissionType.DataImporterRole, false)]
        [TestCase(PermissionType.DomainUserRole, PermissionType.CanModifyStoreSettings, false)]
        [TestCase(PermissionType.DomainUserRole, PermissionType.CanIgnoreProductionFlag, true)]
        public void TestInRole(PermissionType userPermission, PermissionType requestedAttributeOrRole, bool expectedValue)
        {
            MappingAssistantUser user = new MappingAssistantUser(new UserCoordinates(0, "test"), new GenericIdentity("test"), userPermission);
            Assert.That(user.IsInRole(requestedAttributeOrRole.ToString()), Is.EqualTo(expectedValue));
        }
    }
}