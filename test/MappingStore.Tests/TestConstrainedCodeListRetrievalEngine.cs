// -----------------------------------------------------------------------
// <copyright file="TestConstrainedCodeListRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2018-05-30
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStoreRetrieval.Extensions;

namespace MappingStoreRetrieval.Tests
{
    using System.Diagnostics;
    using System.Linq;

    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Test unit for <see cref="TestConstrainedCodeListRetrievalEngine"/>
    /// </summary>
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [Category("RequiresDB")]
    public class TestConstrainedCodeListRetrievalEngine
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log;

        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        /// <summary>
        /// The codelist retrieval engine
        /// </summary>
        private readonly ConstrainedCodeListRetrievalEngine _constrainedCodelistRetrievalEngine;

        private readonly CodeListRetrievalEngine _codeListRetrievalEngine;

        /// <summary>
        /// The mapping store database
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        /// Initializes static members of the <see cref="TestConstrainedCodeListRetrievalEngine"/> class. 
        /// </summary>
        static TestConstrainedCodeListRetrievalEngine()
        {
            _log = LogManager.GetLogger(typeof(TestConstrainedCodeListRetrievalEngine));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestConstrainedCodeListRetrievalEngine"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public TestConstrainedCodeListRetrievalEngine(string connectionName)
        {
            var connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(connectionName);
            this._mappingStoreDb = new Database(connectionStringSettings);
            this._constrainedCodelistRetrievalEngine = new ConstrainedCodeListRetrievalEngine(this._mappingStoreDb);
            this._codeListRetrievalEngine = new CodeListRetrievalEngine(this._mappingStoreDb);
        }

        // TODO: see if we need to rewrite any of these tests for msdb 7.0

        ///// <summary>
        ///// Test unit for <see cref="ArtefactRetrieverEngine{T}.Retrieve(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject,Org.Sdmxsource.Sdmx.Api.Constants.ComplexStructureQueryDetailEnumType,Estat.Sri.MappingStoreRetrieval.Constants.VersionQueryType,System.Collections.Generic.IList{Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject})"/> 
        ///// </summary>
        //[Test]
        //public void TestRetrieve()
        //{
        //    IStructureReference codelistRef = new StructureReferenceImpl("ECB", "CL_ADJUSTMENT", "1.0", SdmxStructureEnumType.CodeList);
        //    IStructureReference codelistRef2 = new StructureReferenceImpl("ECB", "CL_ADJUSTMENT", "1.1", SdmxStructureEnumType.CodeList);

        //    IStructureReference structureRef = new StructureReferenceImpl(new MaintainableRefObjectImpl("ESTAT", "STS", "2.0"), SdmxStructureEnumType.Dsd);

        //    Stopwatch stopwatch = new Stopwatch();
        //    stopwatch.Start();
        //    var codelistMutableObjects = _codeListRetrievalEngine.Retrieve(codelistRef.CreateComplexStructureReferenceObject(SdmxStructureEnumType.CodeList, false), ComplexStructureQueryDetailEnumType.Full);
        //    stopwatch.Stop();
        //    _log.Info("Time to retrieve codelist: " + stopwatch.ElapsedMilliseconds);

        //    stopwatch.Start();
        //    codelistMutableObjects = this._constrainedCodelistRetrievalEngine.Retrieve(codelistRef, structureRef);
        //    stopwatch.Stop();
        //    _log.Info("Time to retrieve constrained codelist: " + stopwatch.ElapsedMilliseconds);


        //    stopwatch.Start();
        //    codelistMutableObjects = this._constrainedCodelistRetrievalEngine.Retrieve(codelistRef2, structureRef);
        //    stopwatch.Stop();

        //    Assert.IsNotEmpty(codelistMutableObjects);
        //}
    }
}
