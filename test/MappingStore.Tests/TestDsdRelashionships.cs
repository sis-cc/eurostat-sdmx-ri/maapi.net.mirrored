using System.Linq;
using Estat.Sri.MappingStoreRetrieval.Engine;
using Estat.Sri.MappingStoreRetrieval.Manager;
using log4net.Config;
using NUnit.Framework;

using Org.Sdmxsource.Sdmx.Api.Constants;

using System;
using System.IO;
using System.Reflection;
using log4net;
using Estat.Sdmxsource.Extension.Constant;
using Estat.Sdmxsource.Extension.Model.Error;
using Estat.Sdmxsource.Extension.Engine;
using DataRetriever.Test;
using DryIoc;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
namespace MappingStoreRetrieval.Tests
{

    /// <summary>
    /// Test unit for testing retrieval of DSD
    /// </summary>
    [TestFixture("sqlserver_scratch")]
    public class TestDsdRelashionships : TestBase
    {
        public TestDsdRelashionships(string storeId)
            : base(storeId)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            InitializeMappingStore();
        }
        [Test]
        public void TestMeasureRelationship()
        {
            // add dsd
            string structuresFile = @"tests/datastructure-3.0-with-relationships.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = StructureSubmitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // retrieve DSD
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV3StructureDocument)
                .SetMaintainableTarget(SdmxStructureEnumType.Dsd)
                .SetMaintainableIds("DSD_RELATIONSHIPS")
                .SetAgencyIds("EXAMPLE")
                .SetVersionRequests(new VersionRequestCore("1.0.3"))
                .Build();
            var connectionStringSettings = GetConnectionStringSettings();
            var mappingStoreDatabase = new Database(connectionStringSettings);
            var dsdRetrievalEngine = new DsdRetrievalEngine(mappingStoreDatabase);
            var dsdMutable = dsdRetrievalEngine.Retrieve(structureQuery).Single();

            Assert.IsNotNull(dsdMutable);
            var dsd = dsdMutable.ImmutableInstance;
            //Should have TIME_FORMAT attribute as Dimension and link to 2 dimensions
            Assert.AreEqual(dsd.Attributes.First(x => x.Id == "TIME_FORMAT").AttachmentLevel, AttributeAttachmentLevel.DimensionGroup);
            Assert.AreEqual(dsd.Attributes.First(x => x.Id == "TIME_FORMAT").DimensionReferences.Count, 2);
            //OBS_STATUS has measure relationship to OBS_VALUE
            Assert.AreEqual(dsd.Attributes.First(x => x.Id == "OBS_STATUS").MeasureRelationships.First(), "OBS_VALUE");
            //OBS_PRE_BREAK has measure relationship to MEASURE1 and MEASURE2
            Assert.IsTrue(dsd.Attributes.First(x => x.Id == "OBS_PRE_BREAK").MeasureRelationships.Contains("MEASURE1"));
            Assert.IsTrue(dsd.Attributes.First(x => x.Id == "OBS_PRE_BREAK").MeasureRelationships.Contains("MEASURE2"));
        }

        [Test]
        public void TestCrossDSDMeasures()
        {
            // add dsd
            string structuresFile = @"tests/ESTAT+DEMOGRAPHY+2.1.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = StructureSubmitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // retrieve DSD
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                .SetMaintainableTarget(SdmxStructureEnumType.Dsd)
                .SetMaintainableIds("DEMOGRAPHY")
                .SetAgencyIds("ESTAT")
                .SetVersionRequests(new VersionRequestCore("2.3"))
                .Build();
            var connectionStringSettings = GetConnectionStringSettings();
            var mappingStoreDatabase = new Database(connectionStringSettings);
            var dsdRetrievalEngine = new DsdRetrievalEngine(mappingStoreDatabase);
            var dsdMutable = dsdRetrievalEngine.Retrieve(structureQuery).Single();
        
            var crossDsd = dsdMutable as ICrossSectionalDataStructureMutableObject;
            Assert.IsNotNull(crossDsd);
            
            //For cross dsd the measures should be in the map
            Assert.AreEqual(crossDsd.AttributeToMeasureMap.Count(), 1);
            Assert.AreEqual(crossDsd.AttributeToMeasureMap["OBS_STATUS"].Count, 14);
            //Make sure measures are not added in the MeasureRelationships
            Assert.False(crossDsd.Attributes.SelectMany(x=>x.MeasureRelationships).Any());
        }

        [Test]
        public void Test20NotCross()
        {
            // add dsd
            string structuresFile = @"tests/ESTAT+SSTSCONS_PROD_M+2.0.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = StructureSubmitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // retrieve DSD
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV2StructureDocument)
                .SetMaintainableTarget(SdmxStructureEnumType.Dsd)
                .SetMaintainableIds("STS")
                .SetAgencyIds("ESTAT")
                .SetVersionRequests(new VersionRequestCore("2.0"))
                .Build();
            var connectionStringSettings = GetConnectionStringSettings();
            var mappingStoreDatabase = new Database(connectionStringSettings);
            var dsdRetrievalEngine = new DsdRetrievalEngine(mappingStoreDatabase);
            var dsdMutable = dsdRetrievalEngine.Retrieve(structureQuery).Single();

            //Make sure measures are not added in the MeasureRelationships for Non Cross 2.0
            var crossDsd = dsdMutable as ICrossSectionalDataStructureMutableObject;
            Assert.IsNull(crossDsd);
            Assert.False(dsdMutable.Attributes.SelectMany(x => x.MeasureRelationships).Any());
        }
    }
}