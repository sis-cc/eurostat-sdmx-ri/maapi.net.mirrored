create user msdb_scratch identified by 123;
GRANT CREATE SESSION TO msdb_scratch CONTAINER=ALL;
GRANT ALL PRIVILEGES TO msdb_scratch CONTAINER=ALL;