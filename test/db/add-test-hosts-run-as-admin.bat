@echo off

SET NEWLINE=^& echo.

FIND /C /I "mariadbhost" %WINDIR%\system32\drivers\etc\hosts
IF %ERRORLEVEL% NEQ 0 ECHO %NEWLINE%^127.0.0.1 mariadbhost>>%WINDIR%\System32\drivers\etc\hosts

FIND /C /I "oraclehost" %WINDIR%\system32\drivers\etc\hosts
IF %ERRORLEVEL% NEQ 0 ECHO %NEWLINE%^127.0.0.1 oraclehost>>%WINDIR%\System32\drivers\etc\hosts

type %WINDIR%\System32\drivers\etc\hosts 

pause