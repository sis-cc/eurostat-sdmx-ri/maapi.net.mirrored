// -----------------------------------------------------------------------
// <copyright file="TestAnnotations.cs" company="EUROSTAT">
//   Date Created : 2016-09-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace MappingStore.Store.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using Estat.Sri.MappingStore.Store.Manager;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Newtonsoft.Json;
    using System.Text;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;

    /// <summary>
    /// The test annotations.
    /// </summary>
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class TestAnnotations : TestBase
    {
        /// <summary>
        /// The _connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The _structure parsing manager.
        /// </summary>
        private readonly IStructureParsingManager _structureParsingManager = new StructureParsingManager();

        /// <summary>
        /// Initializes a new instance of the <see cref="TestAnnotations"/> class. 
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        /// <param name="connectionStringName">
        /// The connection String Name.
        /// </param>
        public TestAnnotations(string connectionStringName) : base(connectionStringName)
        {
            this._connectionStringSettings = ConfigurationManager.ConnectionStrings[connectionStringName];
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            this.InitializeMappingStore(this.StoreId);
        }

        /// <summary>
        /// Tests the DSD multi lingual annotations.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase("tests/v21/NA_PENS+ESTAT+1.0.xml")]
        public void TestDsdMultiLingualAnnotations(string file)
        {
            var sdmxObjects = this.GetAndStoreSdmxObjects(file);

            ISdmxObjectRetrievalManager retrievalManager = new MappingStoreSdmxObjectRetrievalManager(this._connectionStringSettings);

            foreach (var dataStructureObject in sdmxObjects.DataStructures.Where(o => o.Annotations.Count > 0))
            {
                var retrievedDsd = Retrieve<IDataStructureMutableObject>(dataStructureObject.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
                Assert.That(retrievedDsd, Is.Not.Null);
                AssertAnnotations(retrievedDsd, dataStructureObject);
            }
        }

        /// <summary>
        /// Tests the DSD multi lingual annotations.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0.xml")]
        [TestCase("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0-NoProduction.xml")]
        public void TestDataflowDoNotInsertNonProductionAnnotation(string file)
        {
            var sdmxObjects = this.GetAndStoreSdmxObjects(file);

            ISdmxObjectRetrievalManager retrievalManager = new MappingStoreSdmxObjectRetrievalManager(this._connectionStringSettings);

            foreach (var sdmxObject in sdmxObjects.Dataflows)
            {
                var actual = Retrieve<IDataflowMutableObject>(sdmxObject.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
                Assert.That(actual, Is.Not.Null, sdmxObject.ToString());
                Assert.That(actual.Annotations, Has.Count.EqualTo(1));
                Assert.That(actual.Annotations, Has.All.Property("Type").EqualTo("NonProductionDataflow"));
            }
        }

        /// <summary>
        /// Tests the DSD Component multi lingual annotations.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase("tests/v21/NA_PENS+ESTAT+1.0.xml")]
        public void TestDsdComponentMultiLingualAnnotations(string file)
        {
            var sdmxObjects = this.GetAndStoreSdmxObjects(file);

            ISdmxObjectRetrievalManager retrievalManager = new MappingStoreSdmxObjectRetrievalManager(this._connectionStringSettings);

            foreach (var dataStructureObject in sdmxObjects.DataStructures)
            {
                var retrievedDsd = Retrieve<IDataStructureMutableObject>(dataStructureObject.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
                Assert.That(retrievedDsd, Is.Not.Null);
                foreach (var component in dataStructureObject.Components)
                {
                    var retrievedComponent = retrievedDsd.GetComponent(component.Id);
                    AssertAnnotations(retrievedComponent, component);
                }
            }
        }

        /// <summary>
        /// Tests the DSD Component multi lingual annotations.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase("tests/v21/NA_PENS+ESTAT+1.0.xml")]
        public void TestCodeMultiLingualAnnotations(string file)
        {
            var sdmxObjects = this.GetAndStoreSdmxObjects(file);

            ISdmxObjectRetrievalManager retrievalManager = new MappingStoreSdmxObjectRetrievalManager(this._connectionStringSettings);

            foreach (var cl in sdmxObjects.Codelists)
            {
                var actual = Retrieve<ICodelistMutableObject>(cl.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
                Assert.That(actual, Is.Not.Null);
                foreach (var code in cl.Items)
                {
                    var actualCode = actual.GetCodeById(code.Id);
                    AssertAnnotations(actualCode, code);
                }
            }
        }

        /// <summary>
        /// Compares the annotations.
        /// </summary>
        /// <param name="actualAnnotableObject">The actual annotation-able object.</param>
        /// <param name="originalAnnotableObject">The original annotation-able object.</param>
        private static void AssertAnnotations(IAnnotableObject actualAnnotableObject, IAnnotableObject originalAnnotableObject)
        {
            Assert.That(actualAnnotableObject.Annotations.Count, Is.EqualTo(originalAnnotableObject.Annotations.Count));
            Assert.That(actualAnnotableObject.Annotations, Has.No.Null);
            foreach (var annotation in originalAnnotableObject.Annotations)
            {
                var first = actualAnnotableObject.Annotations.First(a => string.Equals(a.Id ?? string.Empty, annotation.Id ?? string.Empty, StringComparison.Ordinal) && string.Equals(a.Title ?? string.Empty, annotation.Title ?? string.Empty, StringComparison.Ordinal) && Equals(a.Uri, annotation.Uri));
                Assert.That(first.Text, Is.EquivalentTo(annotation.Text).Using(new Comparison<ITextTypeWrapper>((w1, w2) => string.Compare(w1.Locale, w2.Locale, StringComparison.OrdinalIgnoreCase) + string.Compare(w1.Value, w2.Value, StringComparison.Ordinal))), 
                    "parent: '{2}' = '{0}' vs '{1}'", ConvertToString(annotation) , ConvertToString(first), annotation.Parent);
            }
        }

        private static string ConvertToString(IAnnotation annotable)
        {
            Assert.That(annotable.Text, Is.Not.Null);
            Assert.That(annotable.Text, Has.No.Null);
            return string.Join(", ", annotable.Text.Select(
                x => string.Format(CultureInfo.InvariantCulture, "{0}:{1}", x.Locale, x.Value)));
        }

        /// <summary>
        /// Gets the and store SDMX objects.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>The <see cref="ISdmxObjects"/></returns>
        private ISdmxObjects GetAndStoreSdmxObjects(string file)
        {
            var inputFile = new FileInfo(file);
            var sdmxObjects = inputFile.GetSdmxObjects(this._structureParsingManager);

            return this.StoreSdmxObjects(sdmxObjects);
        }

        /// <summary>
        /// Stores the SDMX objects.
        /// </summary>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        /// <returns>The <see cref="ISdmxObjects"/></returns>
        private ISdmxObjects StoreSdmxObjects(ISdmxObjects sdmxObjects)
        {
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();
            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            manager.DeleteStructures(sdmxObjects);
            manager.SaveStructures(sdmxObjects);
            return sdmxObjects;
        }
    }
}