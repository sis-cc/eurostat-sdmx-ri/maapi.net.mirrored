// -----------------------------------------------------------------------
// <copyright file="TestBase.cs" company="EUROSTAT">
//   Date Created : 2016-10-24
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStore.Store.Tests
{
    using System.Configuration;

    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using DryIoc;
    using Estat.Sri.Mapping.Api.Manager;
    using log4net;
    using System.IO;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Reflection;
    using System;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Util.Io;
    using Estat.Sri.Mapping.Api.Constant;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Util.ResourceBundle;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    /// <summary>
    /// The test base.
    /// </summary>
    public abstract class TestBase
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(TestBase));

        private static readonly object _syncObject = new object();

        /// <summary>
        /// The container
        /// </summary>
        private readonly Container _container;

        private readonly IMappingStoreManager _mappingStoreManager;

        private readonly IConfigurationStoreManager _connectionStringManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestBase"/> class.
        /// </summary>
        public TestBase(string storeId)
        {
            try
            {
                StoreId = storeId;
                _container =
                    new Container(
                        rules =>
                        rules.With(FactoryMethod.ConstructorWithResolvableArguments)
                            .WithoutThrowOnRegisteringDisposableTransient());
                Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
                string[] folders = { "Estat.Sri.Plugin.SqlServer", "Estat.Sri.Plugin.Oracle", "Estat.Sri.Plugin.Mysql" };
                List<FileInfo> plugins = new List<FileInfo>();
                foreach (var folder in folders)
                {
                    plugins.Add(new FileInfo(string.Format("../../../../../src/{0}/bin/Debug/netstandard2.0/{0}.dll", folder)));
                    plugins.Add(new FileInfo(string.Format("../../../../../src/{0}/bin/Debug/netstandard2.0/{0}.pdb", folder)));
                }

                var assemblies = new List<Assembly>();
                foreach (var fileInfo in plugins)
                {
                    var destPath = Path.Combine(TestContext.CurrentContext.WorkDirectory, fileInfo.Name);
                    if (!File.Exists(destPath) || File.GetLastWriteTime(destPath) < fileInfo.LastWriteTime)
                    {
                        fileInfo.CopyTo(destPath, true);
                    }

                }
                foreach (var fileInfo in plugins)
                {
                    var destPath = Path.Combine(TestContext.CurrentContext.WorkDirectory, fileInfo.Name);
                    if (fileInfo.Extension.EndsWith("dll", StringComparison.OrdinalIgnoreCase))
                    {
                        assemblies.Add(Assembly.LoadFile(destPath));
                    }
                }
                

                assemblies.AddRange(new[] { typeof(IDatabaseProviderManager).Assembly, typeof(DatabaseManager).Assembly });
                foreach (var assembly in assemblies)
                {
                    var list = new List<Assembly>()
                    {
                        assembly
                    };
                    _container.RegisterMany(list, type => !typeof(IEntity).IsAssignableFrom(type));
                }
                
                //_container.Register<IEntityAuthorizationManager, StubAuthorizationManager>();
                _container.Unregister<IEntityAuthorizationManager>();
                MappingStoreIoc.Register<RetrievalEngineContainerFactory>("MappingStoreRetrieversFactory");
                _container.Register<IStructureParsingManager, StructureParsingManager>(
                    made: Made.Of(() => new StructureParsingManager()));
                _container.Register<IStructureWriterFactory, SdmxStructureWriterFactory>();
                _container.Register<IStructureWriterManager, StructureWriterManager>();
                _container.Register<IReadableDataLocationFactory, ReadableDataLocationFactory>();
                _mappingStoreManager = _container.Resolve<IMappingStoreManager>();
                _connectionStringManager = _container.Resolve<IConfigurationStoreManager>();
                SdmxException.SetMessageResolver(new MessageDecoder());
                MappingStoreIoc.Container.RegisterMany(
                new[] { typeof(IEntityRetrieverManager).Assembly, typeof(EntityPeristenceFactory).Assembly },
                type => !typeof(IEntity).IsAssignableFrom(type),
                reuse: Reuse.Singleton,
                made: FactoryMethod.ConstructorWithResolvableArguments,
                setup: Setup.With(allowDisposableTransient: true),
                ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);
                MappingStoreIoc.Container.Register<IStructureParsingManager, StructureParsingManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep, made: FactoryMethod.ConstructorWithResolvableArguments);
            }
            catch (Exception e)
            {
                _log.Error(e);

                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Gets or sets the store identifier.
        /// </summary>
        /// <value>
        /// The store identifier.
        /// </value>
        public string StoreId { get; set; }

        /// <summary>
        /// Gets the containers
        /// </summary>
        /// <value>
        /// </value>
        public Container IoCContainer
        {
            get
            {
                return _container;
            }
        }

        protected ConnectionStringSettings GetConnectionStringSettings()
        {

            var storeId = StoreId;
            var msdbSettings = GetConnectionStringSettings(storeId);
            return msdbSettings;
        }

        protected ConnectionStringSettings GetConnectionStringSettings(string storeId)
        {
            var msdbSettings =
                _connectionStringManager.GetSettings<ConnectionStringSettings>()
                    .FirstOrDefault(settings => settings.Name.Equals(storeId, StringComparison.Ordinal));
            return msdbSettings;
        }

        protected void InitializeMappingStore(string storeId)
        {
            var engineByStoreId = this._mappingStoreManager.GetEngineByStoreId(storeId);
            lock (_syncObject)
            {
                var actionResult = engineByStoreId.Initialize(new DatabaseIdentificationOptions() { StoreId = storeId });
                Assert.That(actionResult.Status, Is.EqualTo(StatusType.Success), string.Join("==============\n", actionResult.Messages));
            }
        }

        protected IRetrievalEngineContainer GetRetrievalEngineContainer()
        {
            return MappingStoreIoc.Container.Resolve<IRetrievalEngineContainerFactory>(serviceKey: MappingStoreIoc.ServiceKey)
                .GetRetrievalEngineContainer(new Database(this.GetConnectionStringSettings()));
        }

        protected static ICommonStructureQuery CreateQuery(IStructureReference structureReference, ComplexStructureQueryDetailEnumType detail)
        {
            return CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                .SetStructureIdentification(structureReference)
                .SetRequestedDetail(detail)
                .Build();
        }

        protected RetrievalEngineContainer GetRetriever()
        {
            return new RetrievalEngineContainer(new Database(this.GetConnectionStringSettings()));
        }

        protected IEnumerable<T> Retrieve<T>(IStructureReference structureReference, ComplexStructureQueryDetailEnumType detail) where T : IMaintainableMutableObject
        {
            var query = CreateQuery(structureReference, detail);
            return (IEnumerable<T>)this.GetRetriever().GetEngine(structureReference.MaintainableStructureEnumType).Retrieve(query);
        }
    }
}