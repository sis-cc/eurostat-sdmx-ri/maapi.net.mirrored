// -----------------------------------------------------------------------
// <copyright file="StubTests.cs" company="EUROSTAT">
//   Date Created : 2019-10-17
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Estat.Sri.MappingStore.Store.Manager;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Util.Extension;

namespace MappingStore.Store.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class StubTests : TestBase
    {
        private ISdmxObjects _toDelete;
        private readonly IStructureParsingManager structureParsingManager = new StructureParsingManager();
        public StubTests(string storeId) : base(storeId)
        {
        }

        [OneTimeSetUp]
        public void Initialize()
        {
            this.InitializeMappingStore(this.StoreId);
            var db = new Database(this.GetConnectionStringSettings());
        }

        [SetUp]
        public void IniializeConditionally()
        {
            if (_toDelete != null)
            {
                Initialize();
                _toDelete = null;
            }
        }

        [TearDown]
        public void DeleteOldObjects()
        {
            // give the DBs some air to breath 
            Thread.Sleep(1000);
            if (_toDelete != null)
            {
                _toDelete.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Delete);
                IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

                var manager = new MappingStoreManager(this.GetConnectionStringSettings(), artefactImportStatuses);
                manager.DeleteStructures(_toDelete);
                _toDelete = null;
            }
        }
        
        private void AddToDelete(ISdmxObjects structureObjects)
        {
            _toDelete = structureObjects;
        }

        [Test]
        public void CategorizeStubDataflow()
        {
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this.GetConnectionStringSettings(), artefactImportStatuses);
            // import category schemes
            FileInfo structureFile = new FileInfo("tests/v21/CategoryScheme.xml");
            ISdmxObjects sdmxObjects = structureFile.GetSdmxObjects(this.structureParsingManager);

            var scheme=  sdmxObjects.CategorySchemes.First();
            var category = scheme.Items.First();

            IDataflowMutableObject dataflow = new DataflowMutableCore();
            dataflow.Id = "TEST_DF";
            dataflow.AgencyId = "TEST";
            dataflow.Version = "1.0.0";
            dataflow.AddName("en", "Test stub dataflow");
            dataflow.Stub = true;
            dataflow.ExternalReference = TertiaryBool.ParseBoolean(true);
            dataflow.FinalStructure = TertiaryBool.ParseBoolean(true);
            dataflow.StructureURL = new Uri("http://localhost/ws/rest/dataflow/TEST/TEST_DF/1.0.0");
            var dataflowObject = dataflow.ImmutableInstance;
            sdmxObjects.AddDataflow(dataflowObject);
            
            ICategorisationMutableObject categorisation = new CategorisationMutableCore();
            categorisation.Id = "TEST123";
            categorisation.AgencyId = "TEST";
            categorisation.AddName("en", "Categorisation referencing stub dataflow");
            categorisation.CategoryReference = category.AsReference;
            categorisation.StructureReference = dataflowObject.AsReference;
            Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme.ICategorisationObject categorisationImmutable = categorisation.ImmutableInstance;
            sdmxObjects.AddCategorisation(categorisationImmutable);

            manager.SaveStructures(sdmxObjects);
            AddToDelete(sdmxObjects);
            Assert.That(artefactImportStatuses, Is.All.Matches<ArtefactImportStatus>(x => x.ImportMessage.Status == Estat.Sri.MappingStore.Store.ImportMessageStatus.Success));

            var roundTrip = Retrieve<ICategorisationMutableObject>(categorisationImmutable.AsReference, ComplexStructureQueryDetailEnumType.Full).First();
            Assert.That(roundTrip, Is.Not.Null);
            Assert.That(roundTrip.StructureReference, Is.Not.Null);
            Assert.That(roundTrip.StructureReference, Is.EqualTo(categorisation.StructureReference));
        }
    }
}
