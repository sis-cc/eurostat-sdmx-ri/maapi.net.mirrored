// -----------------------------------------------------------------------
// <copyright file="TestAnnotationsWhere.cs" company="EUROSTAT">
//   Date Created : 2016-09-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace MappingStore.Store.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.MappingStore.Store.Manager;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    /// The test annotations.
    /// </summary>
    [Ignore("Only used in deprecated SOAP 2.1")]
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class TestAnnotationsWhere : TestBase
    {
        /// <summary>
        /// The codelist identifier prefix
        /// </summary>
        private const string CodelistIdPrefix = "CL_TEST";

        /// <summary>
        /// The codelist agency identifier
        /// </summary>
        private const string CodelistAgencyId = "TEST";

        /// <summary>
        /// The codelist version
        /// </summary>
        private const string CodelistVersion = "1.0";

        /// <summary>
        /// The _connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The SDMX structure type
        /// </summary>
        private readonly SdmxStructureType _sdmxStructureType;

        /// <summary>
        /// The cache
        /// </summary>
        private readonly Dictionary<string, ICodelistMutableObject> _cache = new Dictionary<string, ICodelistMutableObject>(StringComparer.Ordinal);

        /// <summary>
        /// The advanced
        /// </summary>
        private readonly AdvancedStructureRetriever _advanced;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestAnnotationsWhere" /> class.
        /// Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        /// <param name="connectionStringName">The connection String Name.</param>
        public TestAnnotationsWhere(string connectionStringName) : base(connectionStringName)
        {
            this._connectionStringSettings = ConfigurationManager.ConnectionStrings[connectionStringName];
            this.InitializeMappingStore(this.StoreId);
            var count = 0;
            var ids = new[] { "TEST_ID", null, string.Empty };
            var titles = new[] { "TEST_TITLE", null, string.Empty };
            var types = new[] { "TEST_TYPE", null, string.Empty };
            var texts = new[] { "TEST_TEXT", null, string.Empty };

            ISdmxObjects objects = new SdmxObjectsImpl();
            try
            {
                foreach (var id in ids)
                {
                    foreach (var title in titles)
                    {
                        foreach (var type in types)
                        {
                            foreach (var text in texts)
                            {
                                var codelist = BuildCodelist(CodelistIdPrefix + count, id, title, type, text);
                                var immutableInstance = codelist.ImmutableInstance;
                                objects.AddIdentifiable(immutableInstance);
                                _cache.Add(GetAnnotationHash(id, title, type, text), codelist);
                                count++;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogManager.GetLogger(typeof(TestAnnotationsWhere)).Error(e);
                throw;
            }

            this.StoreSdmxObjects(objects);
            _sdmxStructureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList);
            _advanced = new AdvancedStructureRetriever(new RetrievalEngineContainer(new Database(this._connectionStringSettings)));
        }

        /// <summary>
        /// Tests the annotation query.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="type">The type.</param>
        /// <param name="text">The text.</param>
        [Test]
        public void TestAnnotationQueryWithAnnotationWhere([Values("TEST_ID", null, "")] string id, [Values("TEST_TITLE", null, "")] string title, [Values("TEST_TYPE", null, "")]string type, [Values("TEST_TEXT", null, "")] string text)
        {
            var codelist = _cache[GetAnnotationHash(id, title, type, text)];
            var immutableInstance = codelist.ImmutableInstance;
            var agencyId = new ComplexTextReferenceCore(null, TextSearch.FromTextSearchEnumType(TextSearchEnumType.Equal), codelist.AgencyId);
            var clid = new ComplexTextReferenceCore(null, TextSearch.FromTextSearchEnumType(TextSearchEnumType.Equal), codelist.Id);

            IComplexVersionReference versionReference = new ComplexVersionReferenceCore(TertiaryBool.ParseBoolean(false), codelist.Version, null, null);

            var typeRef = BuildComplexTextReferenceEqual(type);
            var titleRef = BuildComplexTextReferenceEqual(title);
            IComplexTextReference textRef = null;
            if (!string.IsNullOrWhiteSpace(text))
            {
                textRef = new ComplexTextReferenceCore("en", TextSearchEnumType.Equal, text);
            }

            ComplexAnnotationReferenceCore complexAnnotationReferenceCore = null;
            if (!string.IsNullOrWhiteSpace(text) || !string.IsNullOrWhiteSpace(title) || !string.IsNullOrWhiteSpace(type))
            {
                complexAnnotationReferenceCore = new ComplexAnnotationReferenceCore(typeRef, titleRef, textRef);
            }

            IComplexStructureReferenceObject structureRef = new ComplexStructureReferenceCore(agencyId, clid, versionReference, codelist.StructureType, complexAnnotationReferenceCore, null, null, null);

            var codelists = _advanced.GetMutableCodelistObjects(structureRef, ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full));
            Assert.That(codelists, Is.Not.Empty);
            Assert.That(codelists.Count, Is.EqualTo(1));
            Assert.That(codelists.First().ImmutableInstance.DeepEquals(immutableInstance, false), Is.True);
        }

        /// <summary>
        /// Tests the annotation query.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="type">The type.</param>
        /// <param name="text">The text.</param>
        [Test]
        public void TestAnnotationQueryArtefactWithoutAnnotationsWithAnnotationWhere([Values("TEST_ID", null, "")] string id, [Values("TEST_TITLE", null, "")] string title, [Values("TEST_TYPE", null, "")]string type, [Values("TEST_TEXT", null, "")] string text)
        {
            var codelist = _cache[GetAnnotationHash(null, null, null, null)];
            var immutableInstance = codelist.ImmutableInstance;
            var agencyId = new ComplexTextReferenceCore(null, TextSearch.FromTextSearchEnumType(TextSearchEnumType.Equal), codelist.AgencyId);
            var clid = new ComplexTextReferenceCore(null, TextSearch.FromTextSearchEnumType(TextSearchEnumType.Equal), codelist.Id);

            IComplexVersionReference versionReference = new ComplexVersionReferenceCore(TertiaryBool.ParseBoolean(false), codelist.Version, null, null);

            var typeRef = BuildComplexTextReferenceEqual(type);
            var titleRef = BuildComplexTextReferenceEqual(title);
            IComplexTextReference textRef = null;
            if (!string.IsNullOrWhiteSpace(text))
            {
                textRef = new ComplexTextReferenceCore("en", TextSearchEnumType.Equal, text);
            }

            ComplexAnnotationReferenceCore complexAnnotationReferenceCore = null;
            if (!string.IsNullOrWhiteSpace(text) || !string.IsNullOrWhiteSpace(title) || !string.IsNullOrWhiteSpace(type))
            {
                complexAnnotationReferenceCore = new ComplexAnnotationReferenceCore(typeRef, titleRef, textRef);
            }

            IComplexStructureReferenceObject structureRef = new ComplexStructureReferenceCore(agencyId, clid, versionReference, codelist.StructureType, complexAnnotationReferenceCore, null, null, null);

            var codelists = _advanced.GetMutableCodelistObjects(structureRef, ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full));

            if (complexAnnotationReferenceCore == null)
            {
                Assert.That(codelists, Is.Not.Empty);
                Assert.That(codelists.Count, Is.EqualTo(1));
                Assert.That(codelists.First().ImmutableInstance.DeepEquals(immutableInstance, false), Is.True);
            }
            else
            {
                Assert.That(codelists, Is.Empty);
            }
        }

        /// <summary>
        /// Tests the annotation query.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="type">The type.</param>
        /// <param name="text">The text.</param>
        [Test]
        public void TestAnnotationQueryWithOutAnnotationWhere([Values("TEST_ID", null, "")] string id, [Values("TEST_TITLE", null, "")] string title, [Values("TEST_TYPE", null, "")]string type, [Values("TEST_TEXT", null, "")] string text)
        {
            var codelist = _cache[GetAnnotationHash(id, title, type, text)];
            var immutableInstance = codelist.ImmutableInstance;
            var agencyId = new ComplexTextReferenceCore(null, TextSearch.FromTextSearchEnumType(TextSearchEnumType.Equal), codelist.AgencyId);
            var clid = new ComplexTextReferenceCore(null, TextSearch.FromTextSearchEnumType(TextSearchEnumType.Equal), codelist.Id);

            IComplexVersionReference versionReference = new ComplexVersionReferenceCore(TertiaryBool.ParseBoolean(false), codelist.Version, null, null);
          
            IComplexStructureReferenceObject structureRef = new ComplexStructureReferenceCore(agencyId, clid, versionReference, codelist.StructureType, null, null, null, null);

            var codelists = _advanced.GetMutableCodelistObjects(structureRef, ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full));
            Assert.That(codelists, Is.Not.Empty);
            Assert.That(codelists.Count, Is.EqualTo(1));
            Assert.That(codelists.First().ImmutableInstance.DeepEquals(immutableInstance, false), Is.True);
        }

        /// <summary>
        /// Builds the complex text reference equal.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The <see cref="IComplexTextReference"/></returns>
        private static IComplexTextReference BuildComplexTextReferenceEqual(string type)
        {
            IComplexTextReference typeRef = null;
            if (!string.IsNullOrWhiteSpace(type))
            {
                typeRef = new ComplexTextReferenceCore(null, TextSearchEnumType.Equal, type);
            }

            return typeRef;
        }

        /// <summary>
        /// Builds the test codelist.
        /// </summary>
        /// <param name="codelistId">The codelist identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="type">The type.</param>
        /// <param name="text">The text.</param>
        /// <returns>
        /// The <see cref="ICodelistMutableObject" />
        /// </returns>
        private static ICodelistMutableObject BuildCodelist(string codelistId, string id, string title, string type, string text)
        {
            ICodelistMutableObject codelist = new CodelistMutableCore() { Id = codelistId, AgencyId = CodelistAgencyId, Version = CodelistVersion };
            codelist.AddName("en", "Test codelist");
            var code = new CodeMutableCore() { Id = "TEST01" };
            code.AddName("en", "Test code");
            codelist.AddItem(code);

            if (!string.IsNullOrWhiteSpace(text) || !string.IsNullOrWhiteSpace(title) || !string.IsNullOrWhiteSpace(type))
            {
                AddAnnotation(
                    codelist,
                    id,
                    title,
                    type,
                    null,
                    !string.IsNullOrWhiteSpace(text) ? new TextTypeWrapperMutableCore("en", text) : null);
            }

            return codelist;
        }

        /// <summary>
        /// Adds the annotation to the specified <paramref name="mutable"/>.
        /// </summary>
        /// <param name="mutable">The mutable.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="type">The type.</param>
        /// <param name="url">The URL.</param>
        /// <param name="texts">The texts.</param>
        private static void AddAnnotation(IAnnotableMutableObject mutable, string id, string title, string type, Uri url, params ITextTypeWrapperMutableObject[] texts)
        {
            IAnnotationMutableObject annotation = new AnnotationMutableCore() { Id = id, Type = type, Title = title, Uri = url };

            if (texts != null)
            {
                foreach (var text in texts)
                {
                    if (text != null)
                    {
                        annotation.AddText(text);
                    }
                }
            }

            mutable.AddAnnotation(annotation);
        }

        /// <summary>
        /// Gets the annotation hash.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="type">The type.</param>
        /// <param name="text">The text.</param>
        /// <returns>The hash</returns>
        private static string GetAnnotationHash(string id, string title, string type, string text)
        {
            var nulValue = "(null)";
            var format = string.Format(CultureInfo.InvariantCulture, "id='{0}'_title='{1}'_text='{2}'_type='{3}'", id ?? nulValue, title ?? nulValue, text ?? nulValue, type ?? nulValue);
            return format;
        }

        /// <summary>
        /// Stores the SDMX objects.
        /// </summary>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        private void StoreSdmxObjects(ISdmxObjects sdmxObjects)
        {
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();
            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            manager.DeleteStructures(sdmxObjects);
            manager.SaveStructures(sdmxObjects);
        }
    }
}