using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Estat.Sri.MappingStore.Store.Manager;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Factory;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Util.Io;

namespace MappingStore.Store.Tests
{

    [Ignore("Not clear if the test is correct")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    [TestFixture("sqlserver")]
    public class InsertPerformanceTests : TestBase
    {
        private int CodeCount = (int)Math.Pow(10,3);
        private readonly ICodelistObject _codelistObject;

        public InsertPerformanceTests(string storeId) : base(storeId)
        {
            InitializeMappingStore(StoreId);
            _codelistObject = CreateBigCodelist();
        }

        [Test]
        public void InsertBigCodelist()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            IList<ArtefactImportStatus> listOfStatus = new List<ArtefactImportStatus>();
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>("MappingStoreRetrieversFactory");
            var manager = new MappingStoreManager(GetConnectionStringSettings(), listOfStatus);
            manager.SaveStructure(_codelistObject);
            stopwatch.Stop();
            var seconds = stopwatch.Elapsed.TotalSeconds;
            Trace.Write("Save time:" + stopwatch.Elapsed);
            var container = new RetrievalEngineContainer(new Database(this.GetConnectionStringSettings()));
            var structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                    .SetMaintainableTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList))
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                    .SetStructureIdentification(_codelistObject.AsReference)
                    .Build();
            var codelistFromDb = container.CodeListRetrievalEngine.Retrieve(structureQuery).First();
            
            Assert.True(codelistFromDb.Items.Any());
            Assert.AreEqual(codelistFromDb.Items.Count(), CodeCount);
            Assert.AreEqual(codelistFromDb.Items[0].Names.Count(), 3);
            Assert.AreEqual(codelistFromDb.Items[0].Descriptions.Count(), 3);
            Assert.AreEqual(codelistFromDb.Items[0].Names[2].Locale, "de");
            Assert.AreEqual(codelistFromDb.Items[0].Names[2].Value, "name2");
            Assert.AreEqual(codelistFromDb.Items[0].Descriptions[2].Locale, "de");
            Assert.AreEqual(codelistFromDb.Items[0].Descriptions[2].Value, "Test Description2");
            Assert.AreEqual(codelistFromDb.Items[0].Annotations.Count(), 3);
            Assert.AreEqual(codelistFromDb.Items[0].Annotations[0].Title, "test0");
            Assert.AreEqual(codelistFromDb.Items[0].Annotations[0].Type, "testType0");
            Assert.AreEqual(codelistFromDb.Items[0].Annotations[0].Uri, "urn:test0");
            Console.WriteLine($"For {CodeCount} item insert time is {seconds} seconds");
            Assert.That(seconds, Is.LessThan(30));
        }

        [Test]
        public void InsertAndUpdateBigCodelist()
        {
            ConfigManager.Config.InsertNewItems = true;

            Stopwatch stopwatch = Stopwatch.StartNew();
            IList<ArtefactImportStatus> listOfStatus = new List<ArtefactImportStatus>();
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>("MappingStoreRetrieversFactory");
            var manager = new MappingStoreManager(GetConnectionStringSettings(), listOfStatus);
            manager.SaveStructure(CreateBigCodelist());
            stopwatch.Stop();
            var seconds = stopwatch.Elapsed.TotalSeconds;
            Trace.Write("Save time:" + stopwatch.Elapsed);
            var container = new RetrievalEngineContainer(new Database(this.GetConnectionStringSettings()));
            var structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                    .SetMaintainableTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList))
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                    .SetStructureIdentification(_codelistObject.AsReference)
                    .Build();
            var updatedCodelist = CreateBigCodelist();
            var updateCodelistMutable = updatedCodelist.MutableInstance;
            var items = updatedCodelist.MutableInstance.Items;

            items[0].Names[1].Locale = "en-US";
            items[0].Names[1].Value = "Updated";
            items[1].Descriptions[2].Locale = "RO";
            items[1].Descriptions[2].Value = "Updated";
            items[2].Annotations[0].Title = "Title Updated";
            items[2].Annotations[0].Type = "Type Updated";
            
            //do we remove items
            //items.RemoveAt(7);
            var newItem = new CodeMutableCore()
            {
                Id = "NEW",
            };
            newItem.AddAnnotation("TITLE", "TYPE", "foo://example.com");
            newItem.AddName("EN", "Name Value");
            newItem.AddDescription("EN", "Desc Value");
            items.Add(newItem);
            updateCodelistMutable.Items.Clear();
            foreach(var item in items)
            {
                updateCodelistMutable.AddItem(item);
            }

            manager.SaveStructure(updateCodelistMutable.ImmutableInstance);
            var codelistFromDb = container.CodeListRetrievalEngine.Retrieve(structureQuery).First();

            Assert.True(codelistFromDb.Items.Any());
            Assert.AreEqual(codelistFromDb.Items.Count(), 11);
            Assert.AreEqual(codelistFromDb.Items.First(x => x.Id == "Code000000").Names.Count(), 3);
            Assert.AreEqual(codelistFromDb.Items.First(x => x.Id == "Code000000").Descriptions.Count(), 3);
            Assert.AreEqual(codelistFromDb.Items.First(x => x.Id == "Code000000").Names[1].Locale, "en-US");
            Assert.AreEqual(codelistFromDb.Items.First(x=>x.Id == "Code000000").Names[1].Value, "Updated");
            Assert.AreEqual(codelistFromDb.Items.First(x => x.Id == "Code000001").Descriptions[2].Locale, "RO");
            Assert.AreEqual(codelistFromDb.Items.First(x=>x.Id == "Code000001").Descriptions[2].Value, "Updated");
            Assert.AreEqual(codelistFromDb.Items.First(x=>x.Id == "Code000002").Annotations.Count(), 3);
            Assert.AreEqual(codelistFromDb.Items.First(x=>x.Id == "Code000002").Annotations[0].Title, "Title Updated");
            Assert.AreEqual(codelistFromDb.Items.First(x=>x.Id == "Code000002").Annotations[0].Type, "Type Updated");
            Assert.AreEqual(codelistFromDb.Items.First(x=>x.Id == "NEW").Names[0].Value, "Name Value");
            Assert.AreEqual(codelistFromDb.Items.First(x=>x.Id == "NEW").Descriptions[0].Value, "Desc Value");
            Assert.AreEqual(codelistFromDb.Items.First(x=>x.Id == "NEW").Annotations[0].Type, "TYPE");
            Assert.AreEqual(codelistFromDb.Items.First(x => x.Id == "NEW").Annotations[0].Title, "TITLE");
        }

        [TestCase("tests/v21/structure.xml")]
        public void InsertStructureFilePerformance(string file)
        {
            Stopwatch sw = new Stopwatch();
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();
            var manager = new MappingStoreManager(this.GetConnectionStringSettings(), artefactImportStatuses);
            IStructureParsingManager structureParsingManager = new StructureParsingManager();
            ISdmxObjects structureObjects;
            using (IReadableDataLocation readable = new FileReadableDataLocation(file))
            {
                IStructureWorkspace structureWorkspace = structureParsingManager.ParseStructures(readable);
                structureObjects = structureWorkspace.GetStructureObjects(false);
            }

            sw.Start();
            manager.SaveStructures(structureObjects);
            sw.Stop();


            Console.WriteLine("Save time:" + sw.Elapsed);
            var seconds = sw.Elapsed.TotalSeconds;
            Assert.That(seconds, Is.LessThan(30));

        }
        private ICodelistObject CreateBigCodelist()
        {
            ICodelistMutableObject codelist = new CodelistMutableCore
            { Id = "CL_TEST", AgencyId = "TEST" };
            codelist.FinalStructure = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);
            codelist.AddName("en", "Test for " + nameof(TestConcurrency));
            for (var i = 0; i < CodeCount; i++)
            {
                ICodeMutableObject code = new CodeMutableCore
                { Id = "Code" + i.ToString("000000", CultureInfo.InvariantCulture) };
                ///add multiple text,descriptions and annotations
                for (int j = 0; j < 3; j++)
                {
                    code.AddName(localeCache[j], "name" + j);
                    code.AddDescription(localeCache[j], "Test Description" + j);
                    code.AddAnnotation("test"+ j, "testType" + j, "urn:test"+  j);
                }
                codelist.AddItem(code);
            }

            return codelist.ImmutableInstance;
        }
        private string[] localeCache = new[] { "en", "es", "de" };
    }

    
}
