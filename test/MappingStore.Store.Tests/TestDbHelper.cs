﻿// -----------------------------------------------------------------------
// <copyright file="TestDbHelper.cs" company="EUROSTAT">
//   Date Created : 2014-09-11
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStore.Store.Tests
{
    using Estat.Sri.MappingStoreRetrieval.Helper;

    using NUnit.Framework;

    [TestFixture]
    public class TestDbHelper
    {
        [TestCase("select distinct from (select a,b,c,d from table t) virtualDataSet", "virtualDataSet{0}", "virtualDataSet0")]
        [TestCase("select distinct from (select a,b,c,d from table t) virtualDataSet where paok ole", "virtualDataSet{0}", "virtualDataSet0")]
        [TestCase("select distinct from (select a,b,c,d from table t) virtualDataSetPaok", "virtualDataSet{0}", "virtualDataSet")]
        public void TestFindUnsedFromAlias(string input, string template, string expectedValue)
        {
           Assert.AreEqual(expectedValue, DbHelper.FindUnsedFromAlias(input, template)); 
        }

        [TestCase("select distinct X from (select a,b,c,d from table t) virtualDataSet", "(select distinct X from (select a,b,c,d from table t) virtualDataSet) virtualDataSet0")]
        [TestCase("select distinct X from (select a,b,c,d from table t) VIRTUALdATAsET", "(select distinct X from (select a,b,c,d from table t) VIRTUALdATAsET) virtualDataSet0")]
        [TestCase("select distinct X from (select a,b,c,d from table t) virtualDataSet where paok ole", "(select distinct X from (select a,b,c,d from table t) virtualDataSet where paok ole) virtualDataSet0")]
        [TestCase("select distinct X from (select a,b,c,d from table t) virtualDataSetPaok", "(select distinct X from (select a,b,c,d from table t) virtualDataSetPaok) virtualDataSet")]
        [TestCase(" select distinct X from taSetPaok", "( select distinct X from taSetPaok) virtualDataSet")]
        [TestCase(" SELECT DISTINCT x FROM TAsETpAOK", "( SELECT DISTINCT x FROM TAsETpAOK) virtualDataSet")]
        [TestCase("select\r\ndistinct X from taSetPaok", "(select\r\ndistinct X from taSetPaok) virtualDataSet")]
        [TestCase("select\r\n    distinct X from taSetPaok", "(select\r\n    distinct X from taSetPaok) virtualDataSet")]
        [TestCase("taSetPaok", "taSetPaok")]
        [TestCase("selectCustomer", "selectCustomer")]
        public void TestNormaliseFromValue(string input, string expectedValue)
        {
            Assert.AreEqual(expectedValue, DbHelper.NormaliseFromValue(input));
        }
    }
}