// -----------------------------------------------------------------------
// <copyright file="TestDataflowInProduction.cs" company="EUROSTAT">
//   Date Created : 2017-10-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStore.Store.Tests
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;

    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Manager;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class TestDataflowInProduction : TestBase
    {
        /// <summary>
        /// The _connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The _structure parsing manager.
        /// </summary>
        private readonly IStructureParsingManager _structureParsingManager = new StructureParsingManager();

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public TestDataflowInProduction(string connectionStringName) : base(connectionStringName)
        {
            this._connectionStringSettings = ConfigurationManager.ConnectionStrings[connectionStringName];
        }

        [SetUp]
        public void Initialize()
        {
            this.InitializeMappingStore(this.StoreId);
        }


        /// <summary>
        /// Tests the DSD multi lingual annotations.
        /// </summary>
        [Test]
        public void ShouldBeAbleToSetDataflowInProduction()
        {
            var firstImport = new FileInfo("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0-no-cat.xml");
            var setInProductionImport = new FileInfo("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0_only_df-set-to-production.xml");
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();
            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);

            var sdmxObjects = firstImport.GetSdmxObjects(this._structureParsingManager);

            manager.DeleteStructures(sdmxObjects);
            artefactImportStatuses.Clear();
            manager.SaveStructures(sdmxObjects);
            Assert.That(artefactImportStatuses.Select(status => status.ImportMessage), Has.All.Property("Status").EqualTo(ImportMessageStatus.Success), string.Join("\n", artefactImportStatuses.Where(status => status.ImportMessage.Status != ImportMessageStatus.Success).Select(status => status.ImportMessage.Message)));

            foreach (var sdmxObject in sdmxObjects.Dataflows)
            {
                var actual = Retrieve<IDataflowMutableObject>(sdmxObject.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
                Assert.That(actual, Is.Not.Null, sdmxObject.ToString());
                Assert.That(actual.Annotations, Has.Count.EqualTo(1));
                Assert.That(actual.Annotations, Has.All.Property("Type").EqualTo("NonProductionDataflow"));
            }

            var dataflowObject = setInProductionImport.GetSdmxObjects(this._structureParsingManager);
            artefactImportStatuses.Clear();
            manager.SaveStructures(dataflowObject);
            Assert.That(artefactImportStatuses.Select(status => status.ImportMessage), Has.All.Property("Status").EqualTo(ImportMessageStatus.Success), string.Join("\n", artefactImportStatuses.Where(status => status.ImportMessage.Status != ImportMessageStatus.Success).Select(status => status.ImportMessage.Message)));
             foreach (var sdmxObject in dataflowObject.Dataflows)
            {
                var actual = Retrieve<IDataflowMutableObject>(sdmxObject.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
                Assert.That(actual, Is.Not.Null, sdmxObject.ToString());
                Assert.That(actual.Annotations, Has.Count.EqualTo(0));
                // Assert.That(actual.Annotations, Has.All.Property("Type").EqualTo("NonProductionDataflow"));
            }
        }

        /// <summary>
        /// Tests the DSD multi lingual annotations.
        /// </summary>
        [Test]
        public void ShouldBeAbleRemoveDataflowFromProduction()
        {
            var firstImport = new FileInfo("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0-no-cat.xml");
            var setInProductionImport = new FileInfo("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0_only_df-set-to-production.xml");
            var removeFromProductionImport = new FileInfo("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0_only_df-remove-from-production.xml");
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();
            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);

            var sdmxObjects = firstImport.GetSdmxObjects(this._structureParsingManager);

            manager.DeleteStructures(sdmxObjects);
            artefactImportStatuses.Clear();
            manager.SaveStructures(sdmxObjects);
            Assert.That(artefactImportStatuses.Select(status => status.ImportMessage), Has.All.Property("Status").EqualTo(ImportMessageStatus.Success), string.Join("\n", artefactImportStatuses.Where(status => status.ImportMessage.Status != ImportMessageStatus.Success).Select(status => status.ImportMessage.Message)));

            foreach (var sdmxObject in sdmxObjects.Dataflows)
            {
                var actual = Retrieve<IDataflowMutableObject>(sdmxObject.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
                Assert.That(actual, Is.Not.Null, sdmxObject.ToString());
                Assert.That(actual.Annotations, Has.Count.EqualTo(1));
                Assert.That(actual.Annotations, Has.All.Property("Type").EqualTo("NonProductionDataflow"));
            }

            var dataflowObject = setInProductionImport.GetSdmxObjects(this._structureParsingManager);
            artefactImportStatuses.Clear();
            manager.SaveStructures(dataflowObject);
            Assert.That(artefactImportStatuses.Select(status => status.ImportMessage), Has.All.Property("Status").EqualTo(ImportMessageStatus.Success), string.Join("\n", artefactImportStatuses.Where(status => status.ImportMessage.Status != ImportMessageStatus.Success).Select(status => status.ImportMessage.Message)));
            foreach (var sdmxObject in dataflowObject.Dataflows)
            {
                var actual = Retrieve<IDataflowMutableObject>(sdmxObject.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
                Assert.That(actual, Is.Not.Null, sdmxObject.ToString());
                Assert.That(actual.Annotations, Has.Count.EqualTo(0));
                // Assert.That(actual.Annotations, Has.All.Property("Type").EqualTo("NonProductionDataflow"));
            }

            var removeFromProductionDataflow = removeFromProductionImport.GetSdmxObjects(this._structureParsingManager);
            artefactImportStatuses.Clear();
            manager.SaveStructures(removeFromProductionDataflow);
            Assert.That(artefactImportStatuses.Select(status => status.ImportMessage), Has.All.Property("Status").EqualTo(ImportMessageStatus.Success), string.Join("\n", artefactImportStatuses.Where(status => status.ImportMessage.Status != ImportMessageStatus.Success).Select(status => status.ImportMessage.Message)));
            foreach (var sdmxObject in removeFromProductionDataflow.Dataflows)
            {
                var actual = Retrieve<IDataflowMutableObject>(sdmxObject.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
                Assert.That(actual, Is.Not.Null, sdmxObject.ToString());
                Assert.That(actual.Annotations, Has.Count.EqualTo(1));
                Assert.That(actual.Annotations, Has.All.Property("Type").EqualTo("NonProductionDataflow"));
            }
        }
    }
}