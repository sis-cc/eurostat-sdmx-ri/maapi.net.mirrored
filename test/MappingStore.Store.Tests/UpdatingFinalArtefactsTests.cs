// -----------------------------------------------------------------------
// <copyright file="UpdatingFinalArtefactsTests.cs" company="EUROSTAT">
//   Date Created : 2017-11-17
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using DryIoc;
using Estat.Sdmxsource.Extension.Factory;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.MappingStore.Store.Factory;
using Estat.Sri.MappingStoreRetrieval.Manager;
using log4net;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Output;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Structureparser.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Estat.Sri.MappingStoreRetrieval.Config;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace MappingStore.Store.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class UpdatingFinalArtefactsTests : TestBase
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log;

        /// <summary>
        /// The structure writer manager
        /// </summary>
        private readonly IStructureWriterManager _structureWriterManager = new StructureWriterManager();

        private readonly ICodelistObject _baseCodelist;

        /// <summary>
        /// Initializes static members of the <see cref="UpdatingFinalArtefactsTests"/> class.
        /// </summary>
        static UpdatingFinalArtefactsTests()
        {
            _log = LogManager.GetLogger(typeof(UpdatingFinalArtefactsTests));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestMappingStoreManager"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public UpdatingFinalArtefactsTests(string connectionName) : base(connectionName)
        {
            try
            {
                this.InitializeMappingStore(this.StoreId);
                _baseCodelist = BuildCodelist(10).ImmutableInstance;
                IoCContainer.Register<ConnectionStringFactory>();
                IoCContainer.Register(made: Made.Of(r => ServiceInfo.Of<ConnectionStringFactory>(), m => m.Function));
                IoCContainer.Register<IStructureSubmitFactory, StructureSubmitMappingStoreFactory>();
                IoCContainer.Register<IStructureSubmitter, StructureSubmitter>(reuse: Reuse.Singleton);
            }
            catch (Exception ex)
            {
         //       Debugger.Launch();
                _log.Error(ex);
                throw;
            }
        }

        [TearDown]
        public void Cleanup()
        {
            ConfigManager.Config.InsertNewItems = false;
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            var sdmxObjectsToDelete = new SdmxObjectsImpl(DatasetAction.GetFromEnum(DatasetActionEnumType.Delete));
            sdmxObjectsToDelete.AddCodelist(_baseCodelist);
            submitter.SubmitStructures(this.StoreId, sdmxObjectsToDelete);
        }

        [SetUp]
        public void Init()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var sdmxObjectsToAdd = new SdmxObjectsImpl(DatasetAction.GetFromEnum(DatasetActionEnumType.Append));
            sdmxObjectsToAdd.AddCodelist(_baseCodelist);
            var result = submitter.SubmitStructures(this.StoreId, sdmxObjectsToAdd);
            Assert.That(result, Is.Not.Empty);
            Assert.That(result.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));
        }
        [Test]
        public void ShouldUpdateRemoveCodesParent()
        {
            ConfigManager.Config.InsertNewItems = true;
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;

            var parentCode = mutable.Items[0].Id;
            ICodeMutableObject updatedCode = mutable.Items[1];
            updatedCode.ParentCode = parentCode;

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            // Now remove the parent  code of the code and submit this to the database
            updatedCode.ParentCode = null;
            updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace); 
            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));


            ConfigManager.Config.InsertNewItems = false;
            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));

            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
            Assert.That(codelist.Items.Count, Is.EqualTo(_baseCodelist.Items.Count));

            var updatedCodeFromDB = codelist.GetCodeById(updatedCode.Id);
            Assert.That(updatedCodeFromDB, Is.Not.Null);
            Assert.That(updatedCodeFromDB.ParentCode, Is.Null);
        }
          [Test]
        public void ShouldUpdateRemoveCodesParentFail()
        {
            // this part depends on the add new parent code to existing code to be implemented
            ConfigManager.Config.InsertNewItems = true;
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;

            var parentCode = mutable.Items[0].Id;
            ICodeMutableObject updatedCode = mutable.Items[1];
            updatedCode.ParentCode = parentCode;

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            // Now remove the parent  code of the code and submit this to the database
            updatedCode.ParentCode = null;
            updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace); 
            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            // ensure we disable it 
            ConfigManager.Config.InsertNewItems = false;
            replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Warning));


            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));

            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
            Assert.That(codelist.Items.Count, Is.EqualTo(_baseCodelist.Items.Count));

            var updatedCodeFromDB = codelist.GetCodeById(updatedCode.Id);
            Assert.That(updatedCodeFromDB, Is.Not.Null);
            Assert.That(updatedCodeFromDB.ParentCode, Is.EqualTo(parentCode));
        }
        [Test]
        public void ShouldUpdateReplaceCodesParentFail()
        {
            // this part depends on the add new parent code to existing code to be implemented
            ConfigManager.Config.InsertNewItems = true;
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;

            var parentCode = mutable.Items[0].Id;
            ICodeMutableObject updatedCode = mutable.Items[1];
            updatedCode.ParentCode = parentCode;

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            // Now change the parent  code of the code and submit this to the database
            var parentCode2 = mutable.Items[2].Id;
            updatedCode.ParentCode = parentCode2;
            updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            // ensure that we disabled that insert new items
            ConfigManager.Config.InsertNewItems = false;
            replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Warning));

            ICommonStructureQuery query = CreateQuery(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub);

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));

            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
            Assert.That(codelist.Items.Count, Is.EqualTo(_baseCodelist.Items.Count));

            var updatedCodeFromDB = codelist.GetCodeById(updatedCode.Id);
            Assert.That(updatedCodeFromDB, Is.Not.Null);
            Assert.That(updatedCodeFromDB.ParentCode, Is.EqualTo(parentCode));
        }

        [Test]
        public void ShouldUpdateReplaceCodesParent()
        {
            ConfigManager.Config.InsertNewItems = true;
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;

            var parentCode = mutable.Items[0].Id;
            ICodeMutableObject updatedCode = mutable.Items[1];
            updatedCode.ParentCode = parentCode;

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            // Now change the parent  code of the code and submit this to the database
            var parentCode2 = mutable.Items[2].Id;
            updatedCode.ParentCode = parentCode2;
            updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace); 
            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));


            ConfigManager.Config.InsertNewItems = false;
            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));

            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
            Assert.That(codelist.Items.Count, Is.EqualTo(_baseCodelist.Items.Count));

            var updatedCodeFromDB = codelist.GetCodeById(updatedCode.Id);
            Assert.That(updatedCodeFromDB, Is.Not.Null);
            Assert.That(updatedCodeFromDB.ParentCode, Is.EqualTo(parentCode2));
        }
        [Test]
        public void ShouldUpdateExistingCodesNewParentFail()
        {
            ConfigManager.Config.InsertNewItems = false;
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;

            var parentCode = mutable.Items[0].Id;
            ICodeMutableObject updatedCode = mutable.Items[1];
            updatedCode.ParentCode = parentCode;

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Warning));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));

            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
            Assert.That(codelist.Items.Count, Is.EqualTo(_baseCodelist.Items.Count));

            var updatedCodeFromDB = codelist.GetCodeById(updatedCode.Id);
            Assert.That(updatedCodeFromDB, Is.Not.Null);
            Assert.That(updatedCodeFromDB.ParentCode, Is.Null);
        }
        [Test]
        public void ShouldUpdateExistingCodesNewParent()
        {
            ConfigManager.Config.InsertNewItems = true;
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;

            var parentCode = mutable.Items[0].Id;
            ICodeMutableObject updatedCode = mutable.Items[1];
            updatedCode.ParentCode = parentCode;

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);
            ConfigManager.Config.InsertNewItems = false;

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));

            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
            Assert.That(codelist.Items.Count, Is.EqualTo(_baseCodelist.Items.Count));

            var updatedCodeFromDB = codelist.GetCodeById(updatedCode.Id);
            Assert.That(updatedCodeFromDB, Is.Not.Null);
            Assert.That(updatedCodeFromDB.ParentCode, Is.EqualTo(parentCode));
        }

        [Test]
        public void ShouldInsertNewCodeWithParentFail()
        {
            ConfigManager.Config.InsertNewItems = false;
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;

            var parentCode = mutable.Items[0].Id;
            ICodeMutableObject code = new CodeMutableCore() { Id = "NEW_CODE", ParentCode = parentCode};
            code.AddName("en", "TEST new code" );
            mutable.AddItem(code);

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Warning));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
            Assert.That(codelist.Items.Count, Is.EqualTo(_baseCodelist.Items.Count ));
            var newCode = codelist.GetCodeById("NEW_CODE");
            Assert.That(newCode, Is.Null);
        }

        [Test]
        public void ShouldInsertNewCodeWithParent()
        {
            ConfigManager.Config.InsertNewItems = true;
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;

            var parentCode = mutable.Items[0].Id;
            ICodeMutableObject code = new CodeMutableCore() { Id = "NEW_CODE", ParentCode = parentCode};
            code.AddName("en", "TEST new code" );
            mutable.AddItem(code);

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);
            ConfigManager.Config.InsertNewItems = false;

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
            Assert.That(codelist.Items.Count, Is.EqualTo(_baseCodelist.Items.Count + 1));
            var newCode = codelist.GetCodeById("NEW_CODE");
            Assert.That(newCode, Is.Not.Null);
            Assert.That(newCode.ParentCode, Is.EqualTo(parentCode));
        }

        [Test]
        public void ShouldInsertNewCategoriesithParent()
        {
            ConfigManager.Config.InsertNewItems = true;
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Append);
            ICategorySchemeMutableObject mutable = new CategorySchemeMutableCore();
            mutable.Id = "TEST_CS";
            mutable.AgencyId = "TEST";
            mutable.Version = "1.0";
            mutable.AddName("en", "PAOK OLE");

            var root = AddRoot(mutable, "ROOT_CAT");
            var child1 = AddChildCategory(root);
            var child2 = AddChildCategory(root);
            var child3 = AddChildCategory(root);
            var child11 = AddChildCategory(child1);
            var child12 = AddChildCategory(child1);
            var child121 = AddChildCategory(child12);
            var child31 = AddChildCategory(child3);
            updateSdmxObject.AddCategoryScheme(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);
            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            // add one new leaf to existing tree
            var id1210 = AddChildCategory(child121).Id;

            // add 1lvl + leaf
            var newroot = AddChildCategory(root);
            var id41 = AddChildCategory(newroot).Id;

            // add new leaf to existing root with siblings
            var id32 = AddChildCategory(child3).Id;

            // add new leaf to existing root
            var id21 = AddChildCategory(child2).Id;

            // add new root + child
            var root2 = AddRoot(mutable, "ROOT_CAT2");
            var rc1 = AddChildCategory(root2).Id;

            // move child11 to child31
            child1.Items.Remove(child11);
            child31.AddItem(child11);

            updateSdmxObject.AddCategoryScheme(mutable.ImmutableInstance);
            replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);
            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            ConfigManager.Config.InsertNewItems = false;
            var fromDB = Retrieve<ICategorySchemeMutableObject>(mutable.ImmutableInstance.AsReference, ComplexStructureQueryDetailEnumType.Full).First().ImmutableInstance;
            Assert.That(fromDB.Items.Count, Is.EqualTo(mutable.Items.Count));
            var new121 = fromDB.GetCategory(root.Id, child1.Id, child12.Id, child121.Id, id1210);
            Assert.That(new121, Is.Not.Null);
            Assert.That(fromDB.GetCategory(root.Id, newroot.Id, id41), Is.Not.Null);
            Assert.That(fromDB.GetCategory(root.Id, child3.Id, id32), Is.Not.Null);
            Assert.That(fromDB.GetCategory(root.Id, child2.Id, id21), Is.Not.Null);
            Assert.That(fromDB.GetCategory(root2.Id, rc1), Is.Not.Null);
            Assert.That(fromDB.GetCategory(root.Id, child3.Id, child31.Id, child11.Id), Is.Not.Null);
        }

        private static ICategoryMutableObject AddRoot(ICategorySchemeMutableObject mutable, string rootCat)
        {
            ICategoryMutableObject root = new CategoryMutableCore();
            root.Id = rootCat;
            root.AddName("en", "Root item");
            mutable.AddItem(root);
            return root;
        }

        private static ICategoryMutableObject AddChildCategory(ICategoryMutableObject root)
        {
            ICategoryMutableObject child1 = new CategoryMutableCore();
            var parentId = root.Id;
            var itemCount = root.Items?.Count ?? 0;

            child1.Id = string.Format(CultureInfo.InvariantCulture, "{0}_{1}", parentId, itemCount);
            child1.AddName("en", string.Format(CultureInfo.InvariantCulture,"Child of {0}, {1}", parentId, itemCount));
            root.AddItem(child1);
            return child1;
        }

        [Test]
        public void ShouldUpdateCodelistNonFinalAttributes()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            mutable.Uri = new Uri("http://localhost/ws/rest/codelist/");
            mutable.StartDate = new DateTime(2000, 1, 1);
            mutable.EndDate = new DateTime(2008, 6, 1);
            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();
            Assert.That(codelist.StartDate, Is.EqualTo(mutable.StartDate));
            Assert.That(codelist.EndDate, Is.EqualTo(mutable.EndDate));
            Assert.That(codelist.Uri, Is.EqualTo(mutable.Uri));
        }

        [Test]
        public void ShouldUpdateCodelistNameWithSameLocale()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            mutable.Names.Clear();
            mutable.AddName("en", "TEST2");
            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();
            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));
        }

        [Test]
        public void ShouldUpdateCodelistCodeNameWithSameLocale()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            var code = mutable.Items[2];
            code.Names.Clear();
            code.AddName("en", "Test Code 3000");
            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();
            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = codelist.GetCodeById(code.Id);
            Assert.That(retrievedCode, Is.Not.Null);
            Assert.That(retrievedCode.Id, Is.EqualTo(code.Id));
            Assert.That(retrievedCode.Names.Count, Is.EqualTo(1));
            Assert.That(retrievedCode.Names[0].Locale, Is.EqualTo(code.Names[0].Locale));
            Assert.That(retrievedCode.Names[0].Value, Is.EqualTo(code.Names[0].Value));
        }

        [Test]
        public void ShouldUpdateCodelistCodeNewAnnotation()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            var code = mutable.Items[2];
            var annotation = code.AddAnnotation(null, "TEST_TYPE", null);

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();
            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = codelist.GetCodeById(code.Id);
            Assert.That(retrievedCode, Is.Not.Null);
            Assert.That(retrievedCode.Id, Is.EqualTo(code.Id));
            Assert.That(retrievedCode.Annotations.Count, Is.EqualTo(1));
            Assert.That(retrievedCode.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(retrievedCode.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(retrievedCode.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(retrievedCode.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(retrievedCode.Annotations[0].Text.Count, Is.EqualTo(annotation.Text.Count));
        }

        [Test]
        public void ShouldUpdateCodelistCodeNewAnnotationUpdate()
        {
            ShouldUpdateCodelistCodeNewAnnotation();
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            var code = mutable.Items[2];
            var annotation = code.AddAnnotation(null, "TEST_TYPE", "http://localhost/ws");

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();
            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = codelist.GetCodeById(code.Id);
            Assert.That(retrievedCode, Is.Not.Null);
            Assert.That(retrievedCode.Id, Is.EqualTo(code.Id));
            Assert.That(retrievedCode.Annotations.Count, Is.EqualTo(1));
            Assert.That(retrievedCode.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(retrievedCode.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(retrievedCode.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(retrievedCode.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(retrievedCode.Annotations[0].Text.Count, Is.EqualTo(annotation.Text.Count));
        }

        [Test]
        public void ShouldUpdateCodelistCodeNewAnnotationUpdateWithText()
        {
            ShouldUpdateCodelistCodeNewAnnotation();
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            var code = mutable.Items[2];
            var annotation = code.AddAnnotation(null, "TEST_TYPE", null);
            annotation.AddText("en", "TEST 123");
            annotation.AddText("de", "DE TEST 123");
            annotation.AddText("fr", "DE TEST 123");

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();
            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = codelist.GetCodeById(code.Id);
            Assert.That(retrievedCode, Is.Not.Null);
            Assert.That(retrievedCode.Id, Is.EqualTo(code.Id));
            Assert.That(retrievedCode.Annotations.Count, Is.EqualTo(1));
            Assert.That(retrievedCode.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(retrievedCode.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(retrievedCode.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(retrievedCode.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(retrievedCode.Annotations[0].Text.Count, Is.EqualTo(annotation.Text.Count));
            for (int i = 0; i < annotation.Text.Count; i++)
            {
                ITextTypeWrapperMutableObject expected = annotation.Text[i];
                var retrieved = retrievedCode.Annotations[0].Text.FirstOrDefault(t => t.Locale.Equals(expected.Locale));
                Assert.That(retrieved, Is.Not.Null);
                Assert.That(retrieved.Value, Is.EqualTo(expected.Value));
            }
        }

        [Test]
        public void ShouldUpdateCodelistCodeNewAnnotationUpdateWithTextRemove()
        {
            ShouldUpdateCodelistCodeNewAnnotationUpdateWithText();
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            var code = mutable.Items[2];
            var annotation = code.AddAnnotation(null, "TEST_TYPE", null);
            annotation.AddText("en", "TEST 123");
            annotation.AddText("de", "DE TEST 123");
            annotation.AddText("fr", "LU TEST 123");
            var annotation2 = code.AddAnnotation("SOMEHTING_ELSE", "TEH", null);

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();
            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = codelist.GetCodeById(code.Id);
            Assert.That(retrievedCode, Is.Not.Null);
            Assert.That(retrievedCode.Id, Is.EqualTo(code.Id));
            Assert.That(retrievedCode.Annotations.Count, Is.EqualTo(code.Annotations.Count));
            for(int x = 0; x < code.Annotations.Count; x++)
            {
                Assert.That(retrievedCode.Annotations[x].Id, Is.EqualTo(code.Annotations[x].Id));
                Assert.That(retrievedCode.Annotations[x].Title, Is.EqualTo(code.Annotations[x].Title));
                Assert.That(retrievedCode.Annotations[x].Type, Is.EqualTo(code.Annotations[x].Type));
                Assert.That(retrievedCode.Annotations[x].Uri, Is.EqualTo(code.Annotations[x].Uri));
                Assert.That(retrievedCode.Annotations[x].Text.Count, Is.EqualTo(code.Annotations[x].Text.Count));
                for (int i = 0; i < code.Annotations[x].Text.Count; i++)
                {
                    Assert.That(retrievedCode.Annotations[0].Text[i].Locale, Is.EqualTo(code.Annotations[x].Text[i].Locale));
                    Assert.That(retrievedCode.Annotations[0].Text[i].Value, Is.EqualTo(code.Annotations[x].Text[i].Value));
                }
            }
        }

        [Test]
        public void ShouldUpdateCodelistCodeNameWithSameLocaleAndAddNewDescription()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            var code = mutable.Items[2];
            code.Names.Clear();
            code.AddName("en", "Test Code 3000");
            code.AddDescription("en", "Test Code Description 3000");
            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();
            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = codelist.GetCodeById(code.Id);
            Assert.That(retrievedCode, Is.Not.Null);
            Assert.That(retrievedCode.Id, Is.EqualTo(code.Id));
            Assert.That(retrievedCode.Names.Count, Is.EqualTo(1));
            Assert.That(retrievedCode.Names[0].Locale, Is.EqualTo(code.Names[0].Locale));
            Assert.That(retrievedCode.Names[0].Value, Is.EqualTo(code.Names[0].Value));
            Assert.That(retrievedCode.Descriptions.Count, Is.EqualTo(1));
            Assert.That(retrievedCode.Descriptions[0].Locale, Is.EqualTo(code.Descriptions[0].Locale));
            Assert.That(retrievedCode.Descriptions[0].Value, Is.EqualTo(code.Descriptions[0].Value));
        }

        [Test]
        public void ShouldAddCodelistNameWithNewLocale()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            mutable.AddName("el", "TEST2");
            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();
            Assert.That(codelist.Names.Count, Is.EqualTo(2));
            AssertNamesAreEqual(codelist.Names, mutable.Names);
        }

        [Test]
        public void ShouldUpdateCodelistRemoveLocaleAddlocaleTocode()
        {
            ShouldAddCodelistNameWithNewLocale();
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            var code = mutable.Items[0];
            code.AddName("el", "Μια δοκιμή");
            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();
            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));
            var retrievedCode = codelist.ImmutableInstance.GetCodeById(code.Id);
            var retrievedNames = retrievedCode.Names;
            var expectedNames = code.Names;
            AssertNamesAreEqual(retrievedNames, expectedNames);
        }

        private static void AssertNamesAreEqual(
            IList<ITextTypeWrapperMutableObject> retrievedNames,
            IList<ITextTypeWrapperMutableObject> expectedNames)
        {
            var immutable = retrievedNames.Select(x => new TextTypeWrapperImpl(x, null)).Cast<ITextTypeWrapper>().ToArray();
            AssertNamesAreEqual(immutable, expectedNames);
        }
        private static void AssertNamesAreEqual(IList<ITextTypeWrapper> retrievedNames, IList<ITextTypeWrapperMutableObject> expectedNames)
        {
            Assert.That(retrievedNames.Count, Is.EqualTo(expectedNames.Count));
            foreach (var mutableName in expectedNames)
            {
                Assert.That(
                    retrievedNames,
                    Has.One.Matches<ITextTypeWrapper>(x => string.Equals(x.Locale, mutableName.Locale)));
                Assert.That(
                    retrievedNames,
                    Has.One.Matches<ITextTypeWrapper>(x =>
                        string.Equals(x.Locale, mutableName.Locale) && string.Equals(x.Value, mutableName.Value)));
            }
        }

        [Test]
        public void ShouldAddCodelistNewAnnotation()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            var annotation = mutable.AddAnnotation("TEST_TITLE", "TEST_TYPE", null);
            annotation.AddText("en", "Annotation Text");

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();

            Assert.That(codelist.Annotations.Count, Is.EqualTo(1));
            Assert.That(codelist.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(codelist.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(codelist.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(codelist.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(codelist.Annotations[0].Text.Count, Is.EqualTo(1));
            Assert.That(codelist.Annotations[0].Text[0].Locale, Is.EqualTo(annotation.Text[0].Locale));
            Assert.That(codelist.Annotations[0].Text[0].Value, Is.EqualTo(annotation.Text[0].Value));
        }

        [Test]
        public void ShouldUpdateCodelistWithNewAnnotationAndThenUpdateAnnotation()
        {
            ShouldAddCodelistNewAnnotation();

            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            var annotation = mutable.AddAnnotation("TEST_TITLE2", "TEST_TYPE2", null);
            annotation.AddText("en", "Annotation Text2");

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();

            Assert.That(codelist.Annotations.Count, Is.EqualTo(1));
            Assert.That(codelist.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(codelist.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(codelist.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(codelist.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(codelist.Annotations[0].Text.Count, Is.EqualTo(1));
            Assert.That(codelist.Annotations[0].Text[0].Locale, Is.EqualTo(annotation.Text[0].Locale));
            Assert.That(codelist.Annotations[0].Text[0].Value, Is.EqualTo(annotation.Text[0].Value));
        }

        [Test]
        public void ShouldUpdateCodelistWithNewAnnotationAndThenUpdateAnnotationTextOnly()
        {
            ShouldAddCodelistNewAnnotation();

            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            var annotation = mutable.AddAnnotation("TEST_TITLE", "TEST_TYPE", null);
            annotation.AddText("en", "Annotation Text3");

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();

            Assert.That(codelist.Annotations.Count, Is.EqualTo(1));
            Assert.That(codelist.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(codelist.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(codelist.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(codelist.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(codelist.Annotations[0].Text.Count, Is.EqualTo(1));
            Assert.That(codelist.Annotations[0].Text[0].Locale, Is.EqualTo(annotation.Text[0].Locale));
            Assert.That(codelist.Annotations[0].Text[0].Value, Is.EqualTo(annotation.Text[0].Value));
        }

        [Test]
        public void ShouldUpdateCodelistWithNewAnnotationAndThenUpdateAnnotationAddNewText()
        {
            ShouldAddCodelistNewAnnotation();

            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var mutable = _baseCodelist.MutableInstance;
            var annotation = mutable.AddAnnotation("TEST_TITLE", "TEST_TYPE", null);
            annotation.AddText("en", "Annotation Text");
            annotation.AddText("de", "DE text");

            updateSdmxObject.AddCodelist(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var count = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Stub).Count();
            Assert.That(count, Is.EqualTo(1));
            var codelist = Retrieve<ICodelistMutableObject>(_baseCodelist.AsReference, ComplexStructureQueryDetailEnumType.Full).First();

            Assert.That(codelist.Annotations.Count, Is.EqualTo(1));
            Assert.That(codelist.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(codelist.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(codelist.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(codelist.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(codelist.Annotations[0].Text.Count, Is.EqualTo(2));
            AssertNamesAreEqual(codelist.Annotations[0].Text, annotation.Text);
        }

        /// <summary>
        /// Builds the codelist.
        /// </summary>
        /// <param name="limit">The limit.</param>
        /// <returns>The final codelist</returns>
        private static ICodelistMutableObject BuildCodelist(int limit)
        {
            ICodelistMutableObject codelist = new CodelistMutableCore()
            {
                Id = "CL_TEST_DELETE_BIG_CODELIST",
                AgencyId = "TEST",
                Version = "1.0"
            };

            codelist.FinalStructure = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);

            codelist.AddName("en", "TEST");
            for (int i = 0; i < limit; i++)
            {
                var id = i.ToString(CultureInfo.InvariantCulture);
                ICodeMutableObject code = new CodeMutableCore() { Id = "ID" + id };
                code.AddName("en", "TEST" + id);
                code.AddName("fr", "FR TEST" + id);
                codelist.AddItem(code);
            }

            return codelist;
        }
    }
}
