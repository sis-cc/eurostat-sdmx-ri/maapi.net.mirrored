// -----------------------------------------------------------------------
// <copyright file="TestMappingStoreManager.cs" company="EUROSTAT">
//   Date Created : 2013-04-30
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

namespace MappingStore.Store.Tests
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.Json;
    using System.Text.Json.Serialization;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Manager;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using log4net;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmxsource.Util.Io;
    using YourNamespace;

    /// <summary>
    /// Test unit for <see cref="MappingStoreManager"/>
    /// </summary>
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class TestMappingStoreManager : TestBase
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log;

        /// <summary>
        /// The _connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The structure writer manager
        /// </summary>
        private readonly IStructureWriterManager _structureWriterManager = new StructureWriterManager();

        private readonly IRetrievalEngineContainer retrievalEngineContainer;

        private ISdmxObjects _toDelete;

        /// <summary>
        /// Initializes static members of the <see cref="TestMappingStoreManager"/> class.
        /// </summary>
        static TestMappingStoreManager()
        {
             _log = LogManager.GetLogger(typeof(TestMappingStoreManager));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestMappingStoreManager"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public TestMappingStoreManager(string connectionName) : base(connectionName)
        {
            var db = new Database(this.GetConnectionStringSettings());
            retrievalEngineContainer = new RetrievalEngineContainer(db);
            _connectionStringSettings = GetConnectionStringSettings();
        }

        [OneTimeSetUp]
        public void Initialize()
        {
            this.InitializeMappingStore(this.StoreId);
        }

        [SetUp]
        public void IniializeConditionally()
        {
            if (_toDelete != null)
            {
                Initialize();
                _toDelete = null;
            }
        }

        [TearDown]
        public void DeleteOldObjects()
        {
            // give the DBs some air to breath 
            Thread.Sleep(1000);
            if (_toDelete != null)
            {
                _toDelete.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Delete);
                IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

                var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
                manager.DeleteStructures(_toDelete);
                _toDelete = null;
            }
        }

        [Test]
        public void TestImportTwiceWithAppendShouldNotReturnSdmxForbidden()
        {
            const int limit = 10;
            ICodelistMutableObject codelist = BuildCodelist(limit);

            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var structureObjects = new SdmxObjectsImpl(codelist.ImmutableInstance);
            AddToDelete(structureObjects);
            manager.DeleteStructures(structureObjects);
            artefactImportStatuses.Clear();

            structureObjects.Action = DatasetAction.FromDatasetActionEnumType(DatasetActionEnumType.Append);
            manager.SaveStructures(structureObjects);
            var status = artefactImportStatuses.Single();
            Assert.AreEqual(ImportMessageStatus.Success, status.ImportMessage.Status, status.ImportMessage.Message);

            artefactImportStatuses.Clear();

            manager.SaveStructures(structureObjects);
            ArtefactImportStatus newStatus;
            // not sure what we tested here
            //if (codelist.FinalStructure != null && codelist.FinalStructure.IsTrue ) 
            { 
                Assert.That(artefactImportStatuses, Has.Count.EqualTo(1));
                newStatus = artefactImportStatuses.Single();
            }
            //else
            //{
            //    Assert.That(artefactImportStatuses, Has.Count.EqualTo(2));
            //    newStatus = artefactImportStatuses.Last();
            //}
            Assert.AreEqual(ImportMessageStatus.Success, newStatus.ImportMessage.Status, newStatus.ImportMessage.Message);
        }

        private void AddToDelete(ISdmxObjects structureObjects)
        {
            _toDelete = structureObjects;
        }

        [Test]
        public void TestWithDeleteShouldReturnSdmxNoResults()
        {
            const int limit = 10;
            ICodelistMutableObject codelist = BuildCodelist(limit);

            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var structureObjects = new SdmxObjectsImpl(codelist.ImmutableInstance);

            AddToDelete(structureObjects);
            manager.DeleteStructures(structureObjects);
            artefactImportStatuses.Clear();

            structureObjects.Action = DatasetAction.FromDatasetActionEnumType(DatasetActionEnumType.Delete);
            manager.DeleteStructures(structureObjects);
            var status = artefactImportStatuses.Single();
            Assert.AreEqual(ImportMessageStatus.Error, status.ImportMessage.Status, status.ImportMessage.Message);

            Assert.That(status.ImportMessage.Exception, Is.TypeOf<SdmxNoResultsException>());
        }

        [Test]
        public void TestImportWithReplaceShouldNotReturnSdmxNoResults()
        {
            const int limit = 10;
            ICodelistMutableObject codelist = BuildCodelist(limit);

            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var structureObjects = new SdmxObjectsImpl(codelist.ImmutableInstance);

            AddToDelete(structureObjects);
            manager.DeleteStructures(structureObjects);
            artefactImportStatuses.Clear();

            structureObjects.Action = DatasetAction.FromDatasetActionEnumType(DatasetActionEnumType.Replace);
            manager.SaveStructures(structureObjects);
            var status = artefactImportStatuses.Single();
            Assert.AreEqual(ImportMessageStatus.Success, status.ImportMessage.Status, status.ImportMessage.Message);

            Assert.That(status.ImportMessage.Exception, Is.Null);
        }

        [Test]
        public void ShouldNotDeleteIfInUse()
        {
            var sdmxObjects = GetStructureObjects("tests/v20/ESTAT+STS+2.0.xml", new string[0]);
            var getADsd = sdmxObjects.DataStructures.First();
            var codelistRef = getADsd.GetDimensions().Where(x => x.HasCodedRepresentation()).Select(x => x.Representation.Representation).First();
            var codelistToDelete = sdmxObjects.GetCodelists(codelistRef).First();

            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);

            // fail safe delete structures
            manager.DeleteStructures(sdmxObjects);
            artefactImportStatuses.Clear();

            AddToDelete(sdmxObjects);
            // save the structures
            manager.SaveStructures(sdmxObjects);
            Assert.That(artefactImportStatuses.All(x => x.ImportMessage.Status == ImportMessageStatus.Success), "Error messages:[{0}]", string.Join(", ", artefactImportStatuses.Select(x => x.ImportMessage.Message)));
            artefactImportStatuses.Clear();

            manager.DeleteStructure(codelistToDelete);
            var status = artefactImportStatuses.Single();
            Assert.That(status.ImportMessage.Status, Is.EqualTo(ImportMessageStatus.Error));
            Assert.That(status.ImportMessage.Exception, Is.TypeOf<SdmxConflictException>());
            var conflict = (SdmxConflictException)status.ImportMessage.Exception;
//            Assert.That(conflict.StructureReference, Is.Not.Empty);
//            Assert.That(conflict.StructureReference.First(), Is.EqualTo(getADsd.AsReference));
        }

        [Test]
        public void TestImportFollowedWithDeleteShouldReturnSuccess()
        {
            const int limit = 10;
            ICodelistMutableObject codelist = BuildCodelist(limit);

            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var structureObjects = new SdmxObjectsImpl(codelist.ImmutableInstance);

            AddToDelete(structureObjects);
            manager.DeleteStructures(structureObjects);
            artefactImportStatuses.Clear();

            structureObjects.Action = DatasetAction.FromDatasetActionEnumType(DatasetActionEnumType.Append);
            manager.SaveStructures(structureObjects);
            var status = artefactImportStatuses.Single();
            Assert.AreEqual(ImportMessageStatus.Success, status.ImportMessage.Status, status.ImportMessage.Message);

            artefactImportStatuses.Clear();

            structureObjects.Action = DatasetAction.FromDatasetActionEnumType(DatasetActionEnumType.Delete);
            manager.DeleteStructures(structureObjects);
            var newStatus = artefactImportStatuses.Single();
            Assert.AreEqual(ImportMessageStatus.Success, newStatus.ImportMessage.Status, newStatus.ImportMessage.Message);
            Assert.That(newStatus.ImportMessage.Exception, Is.Null);
        }

        [Test]
        public void TestImportWithAppendAndReplaceShouldReturnSuccess()
        {
            const int limit = 10;
            ICodelistMutableObject codelist = BuildCodelist(limit);

            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var structureObjects = new SdmxObjectsImpl(codelist.ImmutableInstance);

            AddToDelete(structureObjects);
            manager.DeleteStructures(structureObjects);
            artefactImportStatuses.Clear();

            structureObjects.Action = DatasetAction.FromDatasetActionEnumType(DatasetActionEnumType.Append);
            manager.SaveStructures(structureObjects);
            var status = artefactImportStatuses.Single();
            Assert.AreEqual(ImportMessageStatus.Success, status.ImportMessage.Status, status.ImportMessage.Message);

            artefactImportStatuses.Clear();

            structureObjects.Action = DatasetAction.FromDatasetActionEnumType(DatasetActionEnumType.Replace);
            manager.SaveStructures(structureObjects);
            ArtefactImportStatus newStatus;
            //if (codelist.FinalStructure != null && codelist.FinalStructure.IsTrue ) 
            { 
                Assert.That(artefactImportStatuses, Has.Count.EqualTo(1));
                newStatus = artefactImportStatuses.Single();
            }
            //else
            //{
            //    Assert.That(artefactImportStatuses, Has.Count.EqualTo(2));
            //    newStatus = artefactImportStatuses.Last();
            //}

            Assert.AreEqual(ImportMessageStatus.Success, newStatus.ImportMessage.Status, newStatus.ImportMessage.Message);
            Assert.That(newStatus.ImportMessage.Exception, Is.Null);
        }

        [Test]
        public void TestImportDeleteBigCodelist()
        {
            const int limit = 100000;
            ICodelistMutableObject codelist = BuildCodelist(limit);

            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var structureObjects = new SdmxObjectsImpl(codelist.ImmutableInstance);
            AddToDelete(structureObjects);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            manager.DeleteStructures(structureObjects);
            sw.Stop();
            _log.DebugFormat("First delete time {0}", sw.Elapsed);
            sw.Restart();
            manager.SaveStructures(structureObjects);
            sw.Stop();
            _log.DebugFormat("Save time {0}", sw.Elapsed);
            sw.Restart();
            manager.DeleteStructures(structureObjects);
            sw.Stop();
            _log.DebugFormat("2nd delete time {0}", sw.Elapsed);
        }

        private static ICodelistMutableObject BuildCodelist(int limit)
        {
            ICodelistMutableObject codelist = new CodelistMutableCore()
            {
                Id = "CL_TEST_DELETE_BIG_CODELIST",
                AgencyId = "TEST",
                Version = "1.0"

            };

            codelist.AddName("en", "TEST");
            for (int i = 0; i < limit; i++)
            {
                var id = i.ToString(CultureInfo.InvariantCulture);
                ICodeMutableObject code = new CodeMutableCore() { Id = "ID" + id };
                code.AddName("en", "TEST" + id);
                code.AddName("fr", "FR TEST" + id);
                codelist.AddItem(code);
            }

            return codelist;
        }

        /// <summary>
        /// Test unit for <see cref="MappingStoreManager.SaveStructures" />
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="dependencies">The dependencies.</param>
        [TestCaseSource(typeof(TestSourceGenerator))]
        public void TestDeleteStructures(string file, params string[] dependencies)
        {
            var artefactCount = GetArtefactCount(this._connectionStringSettings);
            var itemCount = GetItemCount(this._connectionStringSettings);
            var annotationCount = GetAnnotationCount(this._connectionStringSettings);

            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var structureObjects = GetStructureObjects(file, dependencies);

            AddToDelete(structureObjects);
            manager.SaveStructures(structureObjects);
            Assert.That(artefactImportStatuses.Any(status => status.ImportMessage.Status == ImportMessageStatus.Success), "Expected success but got {0}", string.Join(',', artefactImportStatuses.Where(x => x.ImportMessage.Status != ImportMessageStatus.Success).Select(z => z.ImportMessage.Message)));

            manager.DeleteStructures(structureObjects);

            Assert.AreEqual(artefactCount, GetArtefactCount(this._connectionStringSettings));
            Assert.AreEqual(itemCount, GetItemCount(this._connectionStringSettings));
            Assert.AreEqual(annotationCount, GetAnnotationCount(this._connectionStringSettings));
            Assert.IsFalse(HasOrphanedAnnotationRecords(this._connectionStringSettings));
            Assert.IsNotEmpty(artefactImportStatuses);
            Assert.That(artefactImportStatuses.Any(status => status.ImportMessage.Status == ImportMessageStatus.Success), "Expected success but got {0}", string.Join(',', artefactImportStatuses.Where(x => x.ImportMessage.Status != ImportMessageStatus.Success).Select(z => z.ImportMessage.Message)));
        }

        /// <summary>
        /// Test unit for <see cref="MappingStoreManager.SaveStructures" />
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="dependencies">The dependencies.</param>
        [TestCaseSource(typeof(TestSourceGenerator))]

        public void TestImportStructuresVerify(string file, params string[] dependencies)
        {
            Stopwatch sw = new Stopwatch();
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var structureObjects = GetStructureObjects(file, dependencies);
            AddToDelete(structureObjects);
            sw.Start();
            manager.DeleteStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Delete time:" + sw.Elapsed);
            sw.Restart();
            artefactImportStatuses.Clear();
            manager.SaveStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Save time:" + sw.Elapsed);

            Assert.IsNotEmpty(artefactImportStatuses);
            foreach (var artefactImportStatuse in artefactImportStatuses)
            {
                if (artefactImportStatuse.ImportMessage.StructureReference.MaintainableStructureEnumType != SdmxStructureEnumType.Categorisation)
                {
                    Assert.AreEqual(ImportMessageStatus.Success, artefactImportStatuse.ImportMessage.Status, artefactImportStatuse.ImportMessage.Message);
                }
            }

            foreach (var original in structureObjects.GetAllMaintainables())
            {
                if (original.StructureType.EnumType != SdmxStructureEnumType.Categorisation)
                {
                    IStructureReference structureReference = original.AsReference;
                    Uri urn = original.Urn;
                    ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.Null)
                    .SetStructureIdentification(structureReference)
                    .Build();

                    IMutableObjects retrieveStructures = new MutableObjectsImpl(retrievalEngineContainer.GetEngine(original.StructureType).Retrieve(structureQuery));
                    ISdmxObjects immutableObjects = retrieveStructures.ImmutableObjects;
                    ISet<IMaintainableObject> maintinablesFromMappingStore = immutableObjects.GetMaintainables(original.StructureType.EnumType);
                    Assert.IsNotEmpty(maintinablesFromMappingStore, urn.ToString());

                    var fromMappingStore = maintinablesFromMappingStore.SingleOrDefault(o => o.AsReference.Equals(structureReference));
                    Assert.IsNotNull(fromMappingStore, urn.ToString());
                }
            }
        }

           /// <summary>
        /// Test unit for <see cref="MappingStoreManager.SaveStructures" />
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="dependencies">The dependencies.</param>
        [TestCase("tests/v21/contentconstraint_stubs.xml")]

        public void TestImportActualAllowedContentConstraints(string file)
        {
            Stopwatch sw = new Stopwatch();
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var structureObjects = GetStructureObjects(file, null);
            var inputContentConstraints = structureObjects.ContentConstraintObjects;

            int expectedActualCount = inputContentConstraints.Count(x => x.IsDefiningActualDataPresent);
            int expectedAllowedCount = inputContentConstraints.Count(x => !x.IsDefiningActualDataPresent);

            AddToDelete(structureObjects);
            sw.Start();
            manager.DeleteStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Delete time:" + sw.Elapsed);
            sw.Restart();
            artefactImportStatuses.Clear();
            manager.SaveStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Save time:" + sw.Elapsed);

            Assert.IsNotEmpty(artefactImportStatuses);
            foreach (var artefactImportStatuse in artefactImportStatuses)
            {
                if (artefactImportStatuse.ImportMessage.StructureReference.MaintainableStructureEnumType != SdmxStructureEnumType.Categorisation)
                {
                    Assert.AreEqual(ImportMessageStatus.Success, artefactImportStatuse.ImportMessage.Status, artefactImportStatuse.ImportMessage.Message);
                }
            }
            var complexStructureQueryDetail = ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full);
            var actualReference = new StructureReferenceImpl(null, SdmxStructureEnumType.ActualConstraint);
            ICommonStructureQuery actualQuery = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.Null)
                    .SetMaintainableTarget(SdmxStructureEnumType.ActualConstraint)
                    .SetRequestedDetail(complexStructureQueryDetail)
                    .Build();

            var actualMutable = retrievalEngineContainer.ContentConstraintRetrievalEngine.Retrieve(actualQuery);

            var actual = new HashSet<IContentConstraintObject>(actualMutable.Select(x => x.ImmutableInstance));

            var allowedReference = new StructureReferenceImpl(null, SdmxStructureEnumType.AllowedConstraint);
            var allowedQuery = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.Null)
                    .SetMaintainableTarget(SdmxStructureEnumType.AllowedConstraint)
                    .SetRequestedDetail(complexStructureQueryDetail)
                    .Build();
            var allowedMutable = retrievalEngineContainer.ContentConstraintRetrievalEngine.Retrieve(actualQuery);
            var allowed = new HashSet<IContentConstraintObject>(allowedMutable.Select(x => x.ImmutableInstance));
            var allQuery = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.Null)
                    .SetMaintainableTarget(SdmxStructureEnumType.ContentConstraint)
                    .SetRequestedDetail(complexStructureQueryDetail)
                    .Build();
            var allMutable = retrievalEngineContainer.ContentConstraintRetrievalEngine.Retrieve(allQuery);
            var all = new HashSet<IContentConstraintObject>(allMutable.Select(x => x.ImmutableInstance));
            // issue with retrieving content constraints stored as stub 
            Assert.That(actual, Is.EquivalentTo(structureObjects.ContentConstraintObjects.Where(x => x.IsDefiningActualDataPresent)));
            Assert.That(allowed, Is.EquivalentTo(structureObjects.ContentConstraintObjects.Where(x => !x.IsDefiningActualDataPresent)));
            Assert.That(actual, Has.Count.EqualTo(expectedActualCount));
            Assert.That(allowed, Has.Count.EqualTo(expectedAllowedCount));
            Assert.That(actual.All(x => x.IsDefiningActualDataPresent));
            Assert.That(allowed.All(x => !x.IsDefiningActualDataPresent));
            Assert.That(all, Has.Count.EqualTo(expectedAllowedCount + expectedActualCount));
        }


        /// <summary>
        /// The test import DSD should not create a dataflow.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase("tests/v20/ESTAT+STS+2.0.xml")]
        public void TestImportDsdShouldNotCreateADataflow(string file)
        {
            _log.InfoFormat("Testing against {0} for file {1}", this._connectionStringSettings.ProviderName, file);
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var sdmxObjects = new FileInfo(file).GetSdmxObjects(new StructureParsingManager());
            
            AddToDelete(sdmxObjects);
            // some sanity checks
            Assert.That(sdmxObjects.Dataflows, Is.Empty);
            Assert.That(sdmxObjects.DataStructures, Is.Not.Empty);
            Assert.That(sdmxObjects.DataStructures.Any(o => o.IsFinal.IsTrue));

            // save the structures
            manager.SaveStructures(sdmxObjects);

            // no dataflow should be added
            ICommonStructureQuery query = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.Null)
            .SetMaintainableTarget(SdmxStructureEnumType.Dataflow)
            .Build();
            var mutableDataflowObjects = retrievalEngineContainer.DataflowRetrievalEngine.Retrieve(query);
            Assert.That(mutableDataflowObjects, Is.Empty);
        }

        /// <summary>
        /// Test unit for <see cref="MappingStoreManager.SaveStructures" />
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="dependencies">The dependencies.</param>
        [TestCaseSource(typeof(TestSourceGenerator))]

        public void TestImportStructures(string file, params string[] dependencies)
        {
            _log.InfoFormat("Testing against {0} for file {1}", this._connectionStringSettings.ProviderName, file);
            Stopwatch sw = new Stopwatch();
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var structureObjects = GetStructureObjects(file, dependencies);
          
            AddToDelete(structureObjects);
            sw.Start();
            manager.DeleteStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Delete time:" + sw.Elapsed);
            artefactImportStatuses.Clear();
            sw.Restart();
            manager.SaveStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Save time:" + sw.Elapsed);

            Assert.IsNotEmpty(artefactImportStatuses);
            foreach (var artefactImportStatuse in artefactImportStatuses)
            {
                if (artefactImportStatuse.ImportMessage.StructureReference.MaintainableStructureEnumType != SdmxStructureEnumType.Categorisation)
                {
                    Assert.AreEqual(ImportMessageStatus.Success, artefactImportStatuse.ImportMessage.Status, artefactImportStatuse.ImportMessage.Message);
                }
            }
        }
        /// <summary>
        /// Test unit for <see cref="MappingStoreManager.SaveStructures" />
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="dependencies">The dependencies.</param>
        [TestCaseSource(typeof(TestSourceGenerator))]

        public void TestImportStructuresRetrieveAsStubs(string file, params string[] dependencies)
        {
            _log.InfoFormat("Testing against {0} for file {1}", this._connectionStringSettings.ProviderName, file);
            Stopwatch sw = new Stopwatch();
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var structureObjects = GetStructureObjects(file, dependencies);
          
            AddToDelete(structureObjects);
            sw.Start();
            manager.DeleteStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Delete time:" + sw.Elapsed);
            artefactImportStatuses.Clear();
            sw.Restart();
            manager.SaveStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Save time:" + sw.Elapsed);

            Assert.IsNotEmpty(artefactImportStatuses);
            foreach (var artefactImportStatuse in artefactImportStatuses)
            {
                if (artefactImportStatuse.ImportMessage.StructureReference.MaintainableStructureEnumType != SdmxStructureEnumType.Categorisation)
                {
                    Assert.AreEqual(ImportMessageStatus.Success, artefactImportStatuse.ImportMessage.Status, artefactImportStatuse.ImportMessage.Message);
                }
            }
            Uri uri = new Uri("http://change.me");
            using DataflowFilterContext filterContext = new DataflowFilterContext(Estat.Sri.MappingStoreRetrieval.Constants.DataflowFilter.Any);
            foreach (var original in structureObjects.GetAllMaintainables().Where(o => o.StructureType.EnumType != SdmxStructureEnumType.Categorisation))
            {
                var stubOriginal = original.GetStub(uri, false);
                IStructureReference structureReference = original.AsReference;
                Uri urn = original.Urn;

                ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.Null)
                .SetStructureIdentification(structureReference)
                .SetRequestedDetail(ComplexStructureQueryDetailEnumType.Stub)
                .Build();
                IMutableObjects retrieveStructures = new MutableObjectsImpl(retrievalEngineContainer.GetEngine(original.StructureType).Retrieve(structureQuery));
                ISdmxObjects immutableObjects = retrieveStructures.ImmutableObjects;
                ISet<IMaintainableObject> maintinablesFromMappingStore = immutableObjects.GetMaintainables(original.StructureType.EnumType);
                Assert.AreEqual(maintinablesFromMappingStore.Count, 1, "Expected one artefact for {0}", structureReference);
                IMaintainableObject fromMappingStore = maintinablesFromMappingStore.First();
                Assert.IsTrue(fromMappingStore.IsExternalReference.IsTrue, "Deep equal failed for artefact {0}", structureReference);
                Assert.IsTrue(stubOriginal.DeepEquals(fromMappingStore, false), "Deep equal failed for artefact \n{0}\n vs \n{1}\n", GetDetails(original), GetDetails(fromMappingStore));
            }
        }

        private static string GetDetails(IMaintainableObject maintainableObject) 
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("{");
            sb.AppendLine(maintainableObject.ToString());
            sb.AppendLine(JsonSerializer.Serialize(maintainableObject.Names));
            sb.AppendLine(JsonSerializer.Serialize(maintainableObject.Descriptions));
            sb.AppendFormat("Validity from {0}, {1}\n", maintainableObject.StartDate, maintainableObject.EndDate);
            sb.AppendFormat("Is final {0}\n", maintainableObject.IsFinal.IsTrue);
            sb.AppendFormat("Is external {0}\n", maintainableObject.IsExternalReference.IsTrue);
            if (maintainableObject is IContentConstraintObject constraint) 
            {
                sb.AppendFormat("Is actual {0}", constraint.IsDefiningActualDataPresent);
            }
            sb.AppendLine("}");
            return sb.ToString();
            
        }

        /// <summary>
        /// Tests the import structures twice.
        /// </summary>
        /// <param name="file">The file.</param>
        [TestCase("tests/v21/IT1+CL_FREQ+1.11.xml")]
        public void TestImportStructuresTwice(string file)
        {
            _log.InfoFormat("Testing against {0} for file {1}", this._connectionStringSettings.ProviderName, file);
            Stopwatch sw = new Stopwatch();
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            IStructureParsingManager structureParsingManager = new StructureParsingManager();
            ISdmxObjects structureObjects;
            using (IReadableDataLocation readable = new FileReadableDataLocation(file))
            {
                IStructureWorkspace structureWorkspace = structureParsingManager.ParseStructures(readable);
                structureObjects = structureWorkspace.GetStructureObjects(false);
            }

            AddToDelete(structureObjects);
            sw.Start();
            structureObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Information);
            manager.DeleteStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Delete time:" + sw.Elapsed);
            artefactImportStatuses.Clear();
            sw.Restart();
            manager.SaveStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Save time:" + sw.Elapsed);

            Assert.IsNotEmpty(artefactImportStatuses);
            foreach (var artefactImportStatuse in artefactImportStatuses)
            {
                if (artefactImportStatuse.ImportMessage.StructureReference.MaintainableStructureEnumType != SdmxStructureEnumType.Categorisation)
                {
                    Assert.AreEqual(ImportMessageStatus.Success, artefactImportStatuse.ImportMessage.Status, artefactImportStatuse.ImportMessage.Message);
                }
            }

            sw.Restart();
            manager.SaveStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Save2 time:" + sw.Elapsed);
        }

        /// <summary>
        /// Test unit for <see cref="MappingStoreManager.SaveStructures" />
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="dependencies">The dependencies.</param>
        [TestCaseSource(typeof(TestSourceGenerator))]

        public void TestImportStructuresDeepEqual(string file, params string[] dependencies)
        {
            if ("tests/v20/ESTAT+DEMOGRAPHY+2.1.xml".Equals(file))
            {
                Assert.Ignore("This DSD has an attribute with big TextFormat which doesn't fit in the MSDB");
            }

            //if ("tests/v21/CNS_PR+UN+1.1.xml".Equals(file))
            //{
            //    Assert.Ignore("The retrieval of ContentConstraint's Periodicity, Offset and Tolerance is not implemented");

            //}

            Stopwatch sw = new Stopwatch();
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();
            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            var structureObjects = GetStructureObjects(file, dependencies);
            if (structureObjects.OrganisationUnitSchemes.Count > 0)
            {
                Assert.Ignore("Retrival of Organisation Unit Scheme is not complete (Parent is not retrieved)");
            }

            if (structureObjects.StructureSets.Count > 0)
            {
                Assert.Ignore("MSDB doesn't support storing the full Structure Set");
            }

            AddToDelete(structureObjects);
            sw.Start();
            manager.DeleteStructures(structureObjects);
            sw.Stop();
            artefactImportStatuses.Clear();
            Trace.WriteLine("Delete time:" + sw.Elapsed);
            sw.Restart();
            manager.SaveStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Save time:" + sw.Elapsed);

            Assert.IsNotEmpty(artefactImportStatuses);
            foreach (var artefactImportStatuse in artefactImportStatuses)
            {
                if (artefactImportStatuse.ImportMessage.StructureReference.MaintainableStructureEnumType != SdmxStructureEnumType.Categorisation)
                {
                    Assert.AreEqual(ImportMessageStatus.Success, artefactImportStatuse.ImportMessage.Status, artefactImportStatuse.ImportMessage.Message);
                }
            }

            foreach (var original in structureObjects.GetAllMaintainables().Where(o => o.StructureType.EnumType != SdmxStructureEnumType.Categorisation))
            {
                IStructureReference structureReference = original.AsReference;
                Uri urn = original.Urn;
                var mutableOriginal = original.MutableInstance;

                ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.Null)
                .SetStructureIdentification(structureReference)
                .Build();
                IMutableObjects retrieveStructures = new MutableObjectsImpl(retrievalEngineContainer.GetEngine(original.StructureType).Retrieve(structureQuery));
                ISdmxObjects immutableObjects = retrieveStructures.ImmutableObjects;
                if (original.StructureType.EnumType == SdmxStructureEnumType.Dataflow)
                {
                    continue;
                }

                ISet<IMaintainableObject> maintinablesFromMappingStore = immutableObjects.GetMaintainables(original.StructureType.EnumType);
                Assert.IsNotEmpty(maintinablesFromMappingStore, urn.ToString());

                var fromMappingStore = maintinablesFromMappingStore.SingleOrDefault(o => o.AsReference.Equals(structureReference));
                Assert.IsNotNull(fromMappingStore, urn.ToString());

                mutableOriginal.FinalStructure = fromMappingStore.IsFinal;
                if (mutableOriginal.StructureType.EnumType == SdmxStructureEnumType.Dsd)
                {
                    // TODO FIX THIS either in the queries or the insert order. 
                    var originalDsd = mutableOriginal as IDataStructureMutableObject;
                    var fromMsdb = fromMappingStore as IDataStructureObject;
                    if (originalDsd != null && fromMsdb != null)
                    {
                        for (int i = 0; i < originalDsd.Groups.Count; i++)
                        {
                            originalDsd.Groups[i].DimensionRef.Clear();
                            originalDsd.Groups[i].DimensionRef.AddAll(fromMsdb.Groups[i].DimensionRefs);
                        }

                        if (originalDsd.Attributes != null)
                        {
                            for (int i = 0; i < originalDsd.Attributes.Count; i++)
                            {
                                var attributeMutableObject = originalDsd.Attributes[i];
                                CollectionAssert.AreEquivalent(attributeMutableObject.DimensionReferences, fromMsdb.Attributes[i].DimensionReferences);
                                attributeMutableObject.DimensionReferences.Clear();
                                attributeMutableObject.DimensionReferences.AddAll(fromMsdb.Attributes[i].DimensionReferences);
                            }
                        }
                    }
                }

               NormalizeItemOrder(fromMappingStore, mutableOriginal);
               IMaintainableObject original2 = mutableOriginal.ImmutableInstance;

                Org.Sdmxsource.Sdmx.Api.Model.Diff.IDiffReport report = new DiffReport(fromMappingStore, original2);
                Assert.IsEmpty(report.GetDifferences());
                var deep = fromMappingStore.DeepEquals(original2, true, report);
                Assert.IsEmpty(report.GetDifferences());
               // CheckIfTheyAreEqual(fromMappingStore, original2);
            }
        }

        /// <summary>
        /// Test unit for <see cref="MappingStoreManager.SaveStructures"/> 
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0_only_df.xml")]
        [TestCase("tests/v20/ESTAT_CPI_v1.0.xml")]
        [TestCase("tests/v20/ESTAT_STS2GRP_v2.2.xml", IgnoreReason = "Check why this test should have failed.")]
        public void TestImportStructuresFailures(string file)
        {
            Stopwatch sw = new Stopwatch();
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();
            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);
            IStructureParsingManager structureParsingManager = new StructureParsingManager();
            ISdmxObjects structureObjects;
            using (IReadableDataLocation readable = new FileReadableDataLocation(file))
            {
                IStructureWorkspace structureWorkspace = structureParsingManager.ParseStructures(readable);
                structureObjects = structureWorkspace.GetStructureObjects(false);
            }

            AddToDelete(structureObjects);
            sw.Start();
            manager.DeleteStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Delete time:" + sw.Elapsed);
            artefactImportStatuses.Clear();
            sw.Restart();
            manager.SaveStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Save time:" + sw.Elapsed);

            Assert.IsNotEmpty(artefactImportStatuses);
            Assert.IsTrue(artefactImportStatuses.Any(status => status.ImportMessage.Status == ImportMessageStatus.Error));
        }

        /// <summary>
        /// Gets the structure objects.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="dependencies">
        /// The dependencies.
        /// </param>
        /// <returns>
        /// The <see cref="ISdmxObjects"/>.
        /// </returns>
        private static ISdmxObjects GetStructureObjects(string file, string[] dependencies)
        {
            IStructureParsingManager structureParsingManager = new StructureParsingManager();
            List<FileInfo> inputFiles = new List<FileInfo>() { new FileInfo(file) };
            if (dependencies != null)
            {
                inputFiles.AddRange(dependencies.Select(s => new FileInfo(s)));
            }

            ISdmxObjects structureObjects = new SdmxObjectsImpl();
            foreach (var inputFile in inputFiles)
            {
                structureObjects.Merge(inputFile.GetSdmxObjects(structureParsingManager));
            }

            return structureObjects;
        }

        /// <summary>
        /// Normalizes the item order.
        /// </summary>
        /// <param name="fromMappingStore">From mapping store.</param>
        /// <param name="mutableOriginal">The mutable original.</param>
        private static void NormalizeItemOrder(ISdmxObject fromMappingStore, IMaintainableMutableObject mutableOriginal)
        {
            if (fromMappingStore.StructureType != mutableOriginal.StructureType)
            {
                return; 
            }

            if (!mutableOriginal.Stub && (mutableOriginal.ExternalReference == null || !mutableOriginal.ExternalReference.IsTrue))
            {
                mutableOriginal.StructureURL = null;
                mutableOriginal.ServiceURL = null;
            }

                switch (fromMappingStore)
                {
                    case IConceptSchemeObject conceptSchemeObject:
                        {
                            // remove representation
                            var conceptSchemeMutableObject = (IConceptSchemeMutableObject)mutableOriginal;
                            NormalizeItemOrder(conceptSchemeObject, conceptSchemeMutableObject);
                            break;
                        }
                    case ICodelistObject codelistObject:
                    {
                        NormalizeItemOrder(codelistObject, (ICodelistMutableObject)mutableOriginal);
                        break;
                    }
                    case IDataProviderScheme dataProviderScheme:
                    {
                        NormalizeItemOrder(dataProviderScheme, (IDataProviderSchemeMutableObject)mutableOriginal);
                        break;
                    }
                case IDataConsumerScheme dataProviderScheme:
                    {
                        NormalizeItemOrder(dataProviderScheme, (IDataConsumerSchemeMutableObject)mutableOriginal);
                        break;
                    }
                case IAgencyScheme dataProviderScheme:
                    {
                        NormalizeItemOrder(dataProviderScheme, (IAgencySchemeMutableObject)mutableOriginal);
                        break;
                    }



                    default:
                        {
                            var hcl = fromMappingStore as IHierarchicalCodelistObject;
                            if (hcl != null)
                            {
                                var hclMutable = (IHierarchicalCodelistMutableObject)mutableOriginal;
                                var orderedCodelistRef = (from o in hcl.CodelistRef join c in hclMutable.CodelistRef on o.Alias equals c.Alias select c).ToArray();
                                hclMutable.CodelistRef.Clear();
                                hclMutable.CodelistRef.AddAll(orderedCodelistRef);
                            }

                            break;
                        }
                }
        }

        /// <summary>
        /// Normalizes the item order.
        /// </summary>
        /// <typeparam name="TIm">The type of the immutable.</typeparam>
        /// <typeparam name="TMut">The type of the mutable.</typeparam>
        /// <param name="immutable">The immutable.</param>
        /// <param name="mutable">The mutable.</param>
        private static void NormalizeItemOrder(IKeyValues immutable, IKeyValuesMutable mutable)
        {
            var orderedCodes = (from o in immutable.Values join c in mutable.KeyValues on o equals c select c).ToArray();
            mutable.KeyValues.Clear();
            mutable.KeyValues.AddAll(orderedCodes); 
        }

        /// <summary>
        /// Normalizes the item order.
        /// </summary>
        /// <typeparam name="TIm">The type of the immutable.</typeparam>
        /// <typeparam name="TMut">The type of the mutable.</typeparam>
        /// <param name="immutable">The immutable.</param>
        /// <param name="mutable">The mutable.</param>
        private static void NormalizeItemOrder<TIm, TMut>(IItemSchemeObject<TIm> immutable, IItemSchemeMutableObject<TMut> mutable) where TIm : IItemObject where TMut : IItemMutableObject
        {
            var orderedCodes = (from o in immutable.Items join c in mutable.Items on o.Id equals c.Id select c).ToArray();
            mutable.Items.Clear();
            mutable.Items.AddAll(orderedCodes); 
        }
        /// <summary>
        /// Get the maintainable count
        /// </summary>
        /// <param name="css">The ConnectionStringSettings.</param>
        /// <returns>The maintainable count</returns>
        private static long GetArtefactCount(ConnectionStringSettings css)
        {
            Database database = DatabasePool.GetDatabase(css);
            using (DbConnection dbConnection = database.CreateConnection())
            {
                dbConnection.Open();
                using (DbCommand command = database.GetSqlStringCommand("select count(*) from ARTEFACT"))
                {
                    command.Connection = dbConnection;
                    return Convert.ToInt64(command.ExecuteScalar(), CultureInfo.InvariantCulture);
                }
            }
        }

        /// <summary>
        /// Get the item/maintainable count
        /// </summary>
        /// <param name="css">The ConnectionStringSettings.</param>
        /// <returns>The item count</returns>
        private static long GetItemCount(ConnectionStringSettings css)
        {
            Database database = DatabasePool.GetDatabase(css);
            using (DbConnection dbConnection = database.CreateConnection())
            {
                dbConnection.Open();
                using (DbCommand command = database.GetSqlStringCommand("select count(*) from ITEM"))
                {
                    command.Connection = dbConnection;
                    return Convert.ToInt64(command.ExecuteScalar(), CultureInfo.InvariantCulture);
                }
            }
        }

        /// <summary>
        /// Get the item/maintainable count
        /// </summary>
        /// <param name="css">
        /// The ConnectionStringSettings.
        /// </param>
        /// <returns>
        /// The <see cref="long"/>.
        /// </returns>
        private static long GetAnnotationCount(ConnectionStringSettings css)
        {
            Database database = DatabasePool.GetDatabase(css);
            using (DbConnection dbConnection = database.CreateConnection())
            {
                dbConnection.Open();
                using (DbCommand command = database.GetSqlStringCommand("select count(*) from ANNOTATION"))
                {
                    command.Connection = dbConnection;
                    return Convert.ToInt64(command.ExecuteScalar(), CultureInfo.InvariantCulture);
                }
            }
        }

        /// <summary>
        /// Determines whether there are any orphaned annotation records in the specified connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>
        /// True if there are orphaned records.
        /// </returns>
        private static bool HasOrphanedAnnotationRecords(ConnectionStringSettings connection)
        {
            Database database = DatabasePool.GetDatabase(connection);
            var value = database.ExecuteScalar("select count(*) FROM ANNOTATION WHERE ANN_ID NOT IN ( SELECT ANN_ID as ID FROM ARTEFACT_ANNOTATION UNION SELECT ANN_ID as ID FROM COMPONENT_ANNOTATION UNION SELECT ANN_ID as ID FROM GROUP_ANNOTATION )", new DbParameter[0]);
            return Convert.ToInt64(value, CultureInfo.InvariantCulture) != 0;
        }

        /// <summary>
        /// Checks if they are equal.
        /// </summary>
        /// <param name="fromMappingStore">From mapping store.</param>
        /// <param name="original2">The original2.</param>
        private void CheckIfTheyAreEqual(IMaintainableObject fromMappingStore, IMaintainableObject original2)
        {
            var contentConstraint = fromMappingStore as IContentConstraintObject;
            if (contentConstraint != null)
            {
                var mutable = contentConstraint.MutableInstance;
                var immutable = original2 as IContentConstraintObject;
                Assert.That(immutable, Is.Not.Null);
                if (mutable.IncludedCubeRegion != null)
                {
                    foreach (var keyValuesMutable in mutable.IncludedCubeRegion.KeyValues)
                    {
                        var expectedKeyValues =
                            immutable.IncludedCubeRegion.KeyValues.FirstOrDefault(x =>
                                x.Id.Equals(keyValuesMutable.Id));
                        if (expectedKeyValues != null)
                        {
                            NormalizeItemOrder(expectedKeyValues, keyValuesMutable);
                        }
                    }

                    foreach (var keyValuesMutable in mutable.IncludedCubeRegion.AttributeValues)
                    {
                        var expectedKeyValues =
                            immutable.IncludedCubeRegion.AttributeValues.FirstOrDefault(x =>
                                x.Id.Equals(keyValuesMutable.Id));
                        if (expectedKeyValues != null)
                        {
                            NormalizeItemOrder(expectedKeyValues, keyValuesMutable);
                        }
                    }
                }

                fromMappingStore = mutable.ImmutableInstance;
            }
            Stack<ISdmxObject> stack = new Stack<ISdmxObject>();
            stack.Push(fromMappingStore);

           var originalObjects = original2.Composites;
            bool deepEquals = fromMappingStore.DeepEquals(original2, false);
            ISdmxObject lastIdentifiableObjectA = null;
            ISdmxObject lastIdentifiableObjectB = null;
            List<SdmxStructureEnumType> problematicPath = new List<SdmxStructureEnumType>();
            while (!deepEquals && stack.Count > 0)
            {
                var current = stack.Pop();
                foreach (var sdmxObject in current.Composites)
                {
                    var problem = originalObjects.FirstOrDefault(o => o.Equals(sdmxObject) && !o.DeepEquals(sdmxObject, false));
                    if (problem != null && sdmxObject.Composites.Count > 0)
                    {
                        lastIdentifiableObjectA = sdmxObject;
                        lastIdentifiableObjectB = problem;
                        stack.Push(sdmxObject);
                        problematicPath.Add(sdmxObject.StructureType.EnumType);
                    }
                    else if (problem != null)
                    {
                        lastIdentifiableObjectA = sdmxObject;
                        lastIdentifiableObjectB = problem;
                    }
                }
            }

            if (!deepEquals)
            {
                var filePrefix = Guid.NewGuid().ToString("D");
                var header = new HeaderImpl("I" + filePrefix, "ZZ9");
                var sdmxStructureFormat = new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument));
                using (var outputStream = File.Create(filePrefix + "A" + ".xml"))
                {
                    this._structureWriterManager.WriteStructure(fromMappingStore, header, sdmxStructureFormat, outputStream);
                }

                using (var outputStream = File.Create(filePrefix + "B" + ".xml"))
                {
                    this._structureWriterManager.WriteStructure(original2, header, sdmxStructureFormat, outputStream);
                }
            }

            if (lastIdentifiableObjectA != null)
            {
                var detailedCompare = DetailedCompare(lastIdentifiableObjectA, lastIdentifiableObjectB);

                var differences = string.Join(",\n", detailedCompare.Select(variance => variance.ToString()));
                var newDeepEquals = lastIdentifiableObjectA.DeepEquals(lastIdentifiableObjectB, false);

                Uri urnA = lastIdentifiableObjectA.GetParent<IIdentifiableObject>(true)?.Urn;
                Uri urnB = lastIdentifiableObjectB?.GetParent<IIdentifiableObject>(true)?.Urn;
                
                Assert.IsTrue(newDeepEquals, string.Format("{0} vs {1}: {2}", urnA, urnB, differences));
            }

            Assert.IsTrue(deepEquals, string.Format("{0} vs {1}, path: {2}", fromMappingStore.Urn, original2.Urn, string.Join(',', problematicPath.Select(x => x.ToString()))));

        }

        /// <summary>
        /// Detailed comparison of two objects
        /// </summary>
        /// <typeparam name="T">
        /// Object type
        /// </typeparam>
        /// <param name="val1">
        /// The val1.
        /// </param>
        /// <param name="val2">
        /// The val2.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private static List<Variance> DetailedCompare<T>(T val1, T val2)
        {
            List<Variance> variances = new List<Variance>();
            var fi = val1.GetType().GetProperties();
            foreach (var f in fi)
            {
                if (f.GetIndexParameters().Length == 0 
                    && !f.Name.EndsWith("Composites", StringComparison.Ordinal) 
                    && !f.Name.EndsWith("Parent", StringComparison.Ordinal) 
                    && !f.Name.EndsWith("CrossReferences", StringComparison.Ordinal))
                {
                    Variance v = new Variance();
                    v.Prop = f.Name;
                    v.ValA = f.GetValue(val1, null);
                    v.ValB = f.GetValue(val2, null);
                    var collectionA = v.ValA as ICollection;
                    var sdmxObject = v.ValA as ISdmxObject;
                    if (collectionA != null)
                    {
                        var collectionB = v.ValB as ICollection;
                        v.CollectionA = collectionA;
                        v.CollectionB = collectionB;
                        if (collectionB == null || collectionB.Count != collectionA.Count)
                        {
                            variances.Add(v);
                        }
                    }
                    else if (sdmxObject != null)
                    {
                        var sdmxObjectB = v.ValB as ISdmxObject;
                        if (sdmxObjectB == null || !sdmxObject.DeepEquals(sdmxObjectB, false))
                        {
                            variances.Add(v);
                        }
                    }
                    else if (!Equals(v.ValA, v.ValB))
                    {
                        variances.Add(v);
                    }
                }
            }

            return variances;
        }

        /// <summary>
        /// Holds a diff
        /// </summary>
        private class Variance
        {
            public string Prop { get; set; }
            public object ValA { get; set; }
            public object ValB { get; set; }

            public ICollection CollectionA { get; set; }
            public ICollection CollectionB { get; set; }

            public ISdmxObject SdmxObjectA { get; set; }
            public ISdmxObject SdmxObjectB { get; set; }

            /// <summary>
            /// Returns a string that represents the current object.
            /// </summary>
            /// <returns>
            /// A string that represents the current object.
            /// </returns>
            public override string ToString()
            {
                var valA = this.ValA;
                var a = NormilizeString(valA);
                var b = NormilizeString(ValB);
                return $"[Prop: {this.Prop}, valA: {a}, valB: {b}]";
            }

            private static string NormilizeString(object valA)
            {
                var a = valA?.ToString();
                var representation = valA as IRepresentation;
                if (representation != null)
                {
                    a = string.Format("[Representation : {0}, TextFormat: {1}]", representation.Representation?.TargetUrn, representation.TextFormat?.TextType?.ToEnumType());
                }
                return a;
            }
        }

    }
}