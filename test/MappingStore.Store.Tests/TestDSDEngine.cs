using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DryIoc;
using Estat.Sdmxsource.Extension.Constant;
using Estat.Sdmxsource.Extension.Factory;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.MappingStore.Store;
using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStore.Store.Factory;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Util.Extension;

namespace MappingStore.Store.Tests
{
    [TestFixture("odp")]
    [TestFixture("mysql")]
    [TestFixture("sqlserver")]
    public class TestDSDEngine : TestBase
    {
        [Test]
        public void ShouldInsetDSD()
        {
            IStructureParsingManager parsingManager = IoCContainer.Resolve<IStructureParsingManager>();
            var maintainables = new FileInfo("tests/v21/dsd.xml").GetSdmxObjects(parsingManager);
            IoCContainer.Register<ConnectionStringFactory>();
            IoCContainer.Register(made: Made.Of(r => ServiceInfo.Of<ConnectionStringFactory>(), m => m.Function));
            IoCContainer.Register<IStructureSubmitFactory, StructureSubmitMappingStoreFactory>();
            IoCContainer.Register<IStructureSubmitter, StructureSubmitter>(reuse: Reuse.Singleton);
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            var statuses = submitter.SubmitStructures(this.StoreId, maintainables).Select(x=>x.Status);
            
            Assert.That(statuses.All(x=>x == ResponseStatus.Success),Is.True);

        }

        [SetUp]
        public void Initialize()
        {
            this.InitializeMappingStore(this.StoreId);
        }

        public TestDSDEngine(string storeId)
            : base(storeId)
        {
        }
    }
}
