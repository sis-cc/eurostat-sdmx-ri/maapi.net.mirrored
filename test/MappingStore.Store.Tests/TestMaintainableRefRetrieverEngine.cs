// -----------------------------------------------------------------------
// <copyright file="TestMaintainableRefRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2016-09-26
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStore.Store.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;

    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Engine;
    using Estat.Sri.MappingStore.Store.Manager;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Test the <see cref="TestMaintainableRefRetrieverEngine"/>.
    /// </summary>
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class TestMaintainableRefRetrieverEngine : TestBase
    {
        /// <summary>
        /// The _connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The maintainable reference engine
        /// </summary>
        private readonly MaintainableRefRetrieverEngine _maintainableRefEngine;

        /// <summary>
        /// The cache
        /// </summary>
        private readonly Dictionary<string, Tuple<ISdmxObjects, IList<ArtefactImportStatus>>> _cache;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestMaintainableRefRetrieverEngine"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public TestMaintainableRefRetrieverEngine(string connectionName) : base(connectionName)
        {
            var connectionStringSettings = ConfigurationManager.ConnectionStrings[connectionName];
            this._maintainableRefEngine = new MaintainableRefRetrieverEngine(new Database(connectionStringSettings));
            this._connectionStringSettings = connectionStringSettings;
            this._cache = new Dictionary<string, Tuple<ISdmxObjects, IList<ArtefactImportStatus>>>(StringComparer.Ordinal);
            this.InitializeMappingStore(this.StoreId);
        }

        /// <summary>
        /// Tests the import category reference.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="urn">The urn.</param>
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=ESTAT:ESTAT_DATAFLOWS_SCHEME(1.1)")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:ESTAT_DATAFLOWS_SCHEME(1.1).14")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:ESTAT_DATAFLOWS_SCHEME(1.1).14.14200")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:ESTAT_DATAFLOWS_SCHEME(1.1).14.14200.SSTSCONS")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:ESTAT_DATAFLOWS_SCHEME(1.1).15")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:ESTAT_DATAFLOWS_SCHEME(1.1).15.14200")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:ESTAT_DATAFLOWS_SCHEME(1.1).15.14200.SSTSCONS")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:ESTAT_DATAFLOWS_SCHEME(1.1).15.CENSUS")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Categorisation=ESTAT:2056562427_1929415840(1.0)")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ESTAT:STS_SCHEME(1.0).STS_INSTITUTION")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=ESTAT:STS_SCHEME(1.0)")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ESTAT:CL_UNIT_MULT(1.0)")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.codelist.Code=ESTAT:CL_TIME_FORMAT(1.0).PT1M")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.codelist.Code=ESTAT:CL_TIME_FORMAT(1.0).PT1M")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_M(2.0)")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(2.0)")]
        public void TestRetrieve(string file, string urn)
        {
            var tuple = this.SaveSdmxObjects(file);
            IList<ArtefactImportStatus> artefactImportStatuses = tuple.Item2;

            var structureReference = new StructureReferenceImpl(urn);
            var pk = this._maintainableRefEngine.Retrieve(structureReference);
            Assert.That(pk, Is.GreaterThan(0));
            if (!structureReference.HasChildReference())
            {
                Assert.That(artefactImportStatuses.Any(importStatus => importStatus.PrimaryKeyValue == pk && importStatus.ImportMessage.StructureReference.Equals(structureReference)));
            }
        }

        /// <summary>
        /// Tests the import category reference.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="structure">The structure.</param>
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", SdmxStructureEnumType.Dataflow)]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", SdmxStructureEnumType.Categorisation)]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", SdmxStructureEnumType.CategoryScheme)]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", SdmxStructureEnumType.Category)]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", SdmxStructureEnumType.Dsd)]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", SdmxStructureEnumType.CodeList)]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", SdmxStructureEnumType.Code)]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", SdmxStructureEnumType.ConceptScheme)]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml", SdmxStructureEnumType.Concept)]
        public void TestGetUrnMap(string file, SdmxStructureEnumType structure)
        {
            var tuple = this.SaveSdmxObjects(file);
            var sdmxObjects = tuple.Item1;

            var sdmxStructureType = SdmxStructureType.GetFromEnum(structure);
            var urnMap = this._maintainableRefEngine.RetrievesUrnMap(sdmxStructureType);
            Assert.That(urnMap, Is.Not.Empty);
            Assert.That(urnMap.Keys, Is.All.GreaterThan(0));

            if (sdmxStructureType.IsMaintainable)
            {
                foreach (var maintainableObject in sdmxObjects.GetMaintainables(sdmxStructureType))
                {
                    var reference = maintainableObject.AsReference;
                    Assert.That(urnMap.Values, Has.Some.EqualTo(reference.MaintainableUrn.ToString()));
                }
            }
            else
            {
                foreach (var maintainableObject in sdmxObjects.GetMaintainables(sdmxStructureType.MaintainableStructureType))
                {
                    foreach (var identifiableComposite in maintainableObject.IdentifiableComposites)
                    {
                        var reference = identifiableComposite.AsReference;
                        Assert.That(urnMap.Values, Has.Some.EqualTo(reference.TargetUrn.ToString()));
                    }
                }
            }
        }

        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml")]
        public void TestCategory(string file)
        {
            var tuple = this.SaveSdmxObjects(file);
            var sdmxObjects = tuple.Item1;

            var sdmxStructureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category);
            var urnMap = this._maintainableRefEngine.RetrievesUrnMap(sdmxStructureType);
            Assert.That(urnMap, Is.Not.Empty);
            Assert.That(urnMap.Keys, Is.All.GreaterThan(0));
            foreach (var categorySchemeObject in sdmxObjects.CategorySchemes)
            {
                Queue<ICategoryObject> categoryObjects = new Queue<ICategoryObject>(categorySchemeObject.Items);
                while (categoryObjects.Count > 0)
                {
                    var category = categoryObjects.Dequeue();
                    var reference = category.AsReference;
                    Assert.That(urnMap.Values, Has.Some.EqualTo(reference.TargetUrn.ToString()));
                    foreach (var categoryObject in category.Items)
                    {
                        categoryObjects.Enqueue(categoryObject);
                    }
                }
            }
        }

        /// <summary>
        /// Saves the SDMX objects.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>The tuple with the SDMX Objects and the list of artefact import status</returns>
        private Tuple<ISdmxObjects, IList<ArtefactImportStatus>> SaveSdmxObjects(string file)
        {
            Tuple<ISdmxObjects, IList<ArtefactImportStatus>> tuple;
            if (this._cache.TryGetValue(file, out tuple))
            {
                return tuple;
            }

            var sdmxObjects = new FileInfo(file).GetSdmxObjects(new StructureParsingManager());
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);

            // delete structure first if any
            manager.DeleteStructures(sdmxObjects);

            artefactImportStatuses.Clear();

            // import them again
            manager.SaveStructures(sdmxObjects);

            Assert.That(artefactImportStatuses.Select(status => status.ImportMessage.Status), Is.All.EqualTo(ImportMessageStatus.Success), "The first failure is at {0}", artefactImportStatuses.Where(status => status.ImportMessage.Status != ImportMessageStatus.Success).Select(status => status.ImportMessage.Message + status.ImportMessage.StructureReference).FirstOrDefault());
            var saveSdmxObjects = Tuple.Create(sdmxObjects, artefactImportStatuses);
            this._cache.Add(file, saveSdmxObjects);
            return saveSdmxObjects;
        }
    }
}