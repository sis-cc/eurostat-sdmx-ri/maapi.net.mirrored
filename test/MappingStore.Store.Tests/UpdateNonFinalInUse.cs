// -----------------------------------------------------------------------
// <copyright file="UpdateNonFinalInUse.cs" company="EUROSTAT">
//   Date Created : 2020-10-21
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using NUnit.Framework;
using DryIoc;
using Estat.Sdmxsource.Extension.Factory;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.MappingStore.Store.Factory;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using System.IO;
using Org.Sdmxsource.Sdmx.Util.Extension;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using System.Linq;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Util.Extensions;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.ConceptScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
using System.ComponentModel;

namespace MappingStore.Store.Tests
{

    //[TestFixture("odp")]
    [TestFixture("sqlserver")]
    //[TestFixture("mysql")]

    class UpdateNonFinalInUse : TestBase
    {

        private static readonly ILog _log = LogManager.GetLogger(typeof(UpdateNonFinalInUse));

        private readonly ISdmxObjects _toDelete;
        private readonly ISdmxObjects _toAdd;
        private readonly RetrievalEngineContainer _container;

        public UpdateNonFinalInUse(string connectionName) : base(connectionName)
        {
            try
            {
                this.InitializeMappingStore(this.StoreId);
                IoCContainer.Register<ConnectionStringFactory>();
                IoCContainer.Register(made: Made.Of(r => ServiceInfo.Of<ConnectionStringFactory>(), m => m.Function));
                IoCContainer.Register<IStructureSubmitFactory, StructureSubmitMappingStoreFactory>();
                IoCContainer.Register<IStructureSubmitter, StructureSubmitter>(reuse: Reuse.Singleton);
                var smdxObjects = new FileInfo("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-IsFinal=False.xml").GetSdmxObjects(IoCContainer.Resolve<IStructureParsingManager>());
                // add a dependent artefact to dataflow
                IContentConstraintMutableObject contentConstraint = new ContentConstraintMutableCore();
                contentConstraint.Id = "TEST_CN";
                contentConstraint.Version = "1.0";
                contentConstraint.AgencyId = "TEST";
                contentConstraint.AddName("en", "Dependency for dataflow");
                contentConstraint.ConstraintAttachment = new ContentConstraintAttachmentMutableCore();
                contentConstraint.ConstraintAttachment.AddStructureReference(smdxObjects.Dataflows.First().AsReference);
                KeyValuesMutableImpl keyvalue = new KeyValuesMutableImpl() { Id = "FREQ" };
                keyvalue.KeyValues.Add("A");
                contentConstraint.IncludedCubeRegion = new CubeRegionMutableCore();
                contentConstraint.IncludedCubeRegion.AddKeyValue(keyvalue);
                smdxObjects.AddContentConstraintObject(contentConstraint.ImmutableInstance);
                IDataProviderSchemeMutableObject dataProviderScheme = new DataProviderSchemeMutableCore();
                dataProviderScheme.AgencyId = "TEST";
                dataProviderScheme.AddName("en", "TEST");
                var dp1 = new DataProviderMutableCore();
                dp1.AddName("en", "First");
                dp1.Id = "DP1";
                dataProviderScheme.AddItem(dp1);
                var dp2 = new DataProviderMutableCore();
                dp2.AddName("en", "First");
                dp2.Id = "DP2";
                dataProviderScheme.AddItem(dp2);

                var immutableScheme = dataProviderScheme.ImmutableInstance;
                smdxObjects.AddDataProviderScheme(immutableScheme);

                var provisionAgreement = new ProvisionAgreementMutableCore();
                provisionAgreement.Id = "TEST_PA";
                provisionAgreement.AgencyId = "TEST";
                provisionAgreement.Version = "1.0";
                provisionAgreement.AddName("en", "boilerPlate");

                provisionAgreement.DataproviderRef = immutableScheme.Items[0].AsReference;
                provisionAgreement.StructureUsage = smdxObjects.Dataflows.First().AsReference;
                smdxObjects.AddProvisionAgreement(provisionAgreement.ImmutableInstance);

                _toDelete = new SdmxObjectsImpl(DatasetActionEnumType.Delete);
                _toAdd = new SdmxObjectsImpl(DatasetActionEnumType.Append);
                _toDelete.Merge(smdxObjects);
                _toAdd.Merge(smdxObjects);
                // check TestBase.GetRetrievalEngineContainer() if we want to use this instead
                _container = new RetrievalEngineContainer(new Database(this.GetConnectionStringSettings()));
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        [TearDown]
        public void Cleanup()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            submitter.SubmitStructures(this.StoreId, _toDelete);
        }

        [SetUp]
        public void Init()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var result = submitter.SubmitStructures(this.StoreId, _toAdd);
            Assert.That(result, Is.Not.Empty);
            Assert.That(result.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));
        }

        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        [TestCase(SdmxStructureEnumType.CodeList)]
        public void ShouldUpdateArtefactNonFinalAttributes(SdmxStructureEnumType sdmxStructure)
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.GetMaintainables(sdmxStructure).First();
            var mutable = identifiableObject.MutableInstance;
            mutable.Uri = new Uri("http://localhost/ws/rest/codelist/");
            mutable.StartDate = new DateTime(2000, 1, 1);
            mutable.EndDate = new DateTime(2008, 6, 1);
            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var retriever = GetRetrievalEngineContainer()
                .GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure));
            ICommonStructureQuery structureQuery = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Stub);
            var artefacts = retriever.Retrieve(structureQuery);

            Assert.That(artefacts.Count(), Is.EqualTo(1));

            var artefact = artefacts.Single();
            Assert.That(artefact.StartDate, Is.EqualTo(mutable.StartDate));
            Assert.That(artefact.EndDate, Is.EqualTo(mutable.EndDate));
            Assert.That(artefact.Uri, Is.EqualTo(mutable.Uri));
        }

        [Test]
        public void ShouldUpdateArtefactFinalAttributes()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            foreach (IMaintainableObject maintainable in _toAdd.GetAllMaintainables())
            {
                // Categorisation is added twice once when we replace the 
                if (maintainable.StructureType.EnumType.IsOneOf(SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.DataProviderScheme, SdmxStructureEnumType.Dsd))
                {
                    continue;
                }

                var mutable = maintainable.MutableInstance;
                mutable.FinalStructure = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);
                updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            }

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success),
                string.Join('\n', replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));


            foreach (IMaintainableObject maintainable in _toAdd.GetAllMaintainables())
            {
                if (maintainable.StructureType.EnumType.IsOneOf(SdmxStructureEnumType.Categorisation, SdmxStructureEnumType.DataProviderScheme, SdmxStructureEnumType.Dsd))
                {
                    continue;
                }

                var structureQueryStub = CreateQuery(maintainable.AsReference, ComplexStructureQueryDetailEnumType.Stub);
                var count = _container.GetEngine(maintainable.StructureType).Retrieve(structureQueryStub).Count();
                Assert.That(count, Is.EqualTo(1));
                var structureQueryFull = CreateQuery(maintainable.AsReference, ComplexStructureQueryDetailEnumType.Full);
                var codelist = _container.GetEngine(maintainable.StructureType).Retrieve(structureQueryFull).First();
                Assert.That(codelist.FinalStructure.IsTrue, codelist.ImmutableInstance.AsReference.MaintainableUrn.ToString());
            }
        }

        private ICommonStructureQuery CreateQuery(IStructureReference identification, ComplexStructureQueryDetailEnumType detail)
        {
            return CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.Other, null)
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(detail))
                    .SetStructureIdentification(identification)
                    .Build();
        }

        [Test]
        public void ShouldAddRemoveConceptRole()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.DataStructures.First(x => !x.IsFinal.IsTrue);
            var mutable = identifiableObject.MutableInstance;

            // first we add a concept role
            IDimensionMutableObject conceptRolesDim = mutable.Dimensions[1];
            var concept = _toAdd.ConceptSchemes.First().Items.First();
            conceptRolesDim.ConceptRole.Add(concept.AsReference);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success),
                string.Join('\n', replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var structureQueryStub = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Full);
            var artefact = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryFull).First().ImmutableInstance as IDataStructureObject;

            Assert.That(!artefact.IsFinal.IsTrue);
            Assert.That(artefact.GetDimension(conceptRolesDim.Id), Is.Not.Null);
            Assert.That(artefact.GetDimension(conceptRolesDim.Id).ConceptRole, Has.Count.EqualTo(1));
            Assert.That(artefact.GetDimension(conceptRolesDim.Id).ConceptRole[0].Equals(concept.AsReference));

            // remove a concept role and add 2 new ones
            conceptRolesDim.ConceptRole.Clear();
            var concept2 = _toAdd.ConceptSchemes.First().Items[1];
            var concept3 = _toAdd.ConceptSchemes.First().Items[2];
            conceptRolesDim.ConceptRole.Add(concept2.AsReference);
            conceptRolesDim.ConceptRole.Add(concept3.AsReference);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success),
                string.Join('\n', replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));
            artefact = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryFull).First().ImmutableInstance as IDataStructureObject;
            Assert.That(!artefact.IsFinal.IsTrue);
            Assert.That(artefact.GetDimension(conceptRolesDim.Id), Is.Not.Null);
            Assert.That(artefact.GetDimension(conceptRolesDim.Id).ConceptRole, Has.Count.EqualTo(2));
            Assert.That(artefact.GetDimension(conceptRolesDim.Id).ConceptRole[0].Equals(concept2.AsReference));
            Assert.That(artefact.GetDimension(conceptRolesDim.Id).ConceptRole[1].Equals(concept3.AsReference));
        }

        [Test]
        public void ShouldAddRemoveAttGroup()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var dsd = _toAdd.DataStructures.First(x => !x.IsFinal.IsTrue);
            var dsdMutable = dsd.MutableInstance;

            // add an existing dimension to a group
            int numOfDimRefs = dsdMutable.Groups.First().DimensionRef.Count;
            dsdMutable.Groups.First().DimensionRef.Add("FREQ");

            // add a new group
            var newGroup = new GroupMutableCore()
            {
                Id = "NewGroup",
                Urn = new Uri("urn:sdmx:org.sdmx.infomodel.datastructure.GroupDimensionDescriptor=ESTAT:STS(2.0).Sister"),
            };
            newGroup.DimensionRef.Add("FREQ");
            dsdMutable.Groups.Add(newGroup);

            updateSdmxObject.AddIdentifiable(dsdMutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success),
                string.Join('\n', replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var structureQueryStub = CreateQuery(dsd.AsReference, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(dsd.StructureType).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(dsd.AsReference, ComplexStructureQueryDetailEnumType.Full);
            var artefact = _container.GetEngine(dsd.StructureType).Retrieve(structureQueryFull).First().ImmutableInstance as IDataStructureObject;

            Assert.That(!artefact.IsFinal.IsTrue);

            Assert.That(artefact.Groups.First().DimensionRefs, Has.Count.EqualTo(numOfDimRefs + 1));
            Assert.That(artefact.Groups, Has.Count.EqualTo(2));
        }

        [Test, Ignore("in .NET this is filtered at SDMX XML level in SdmxSource, see Sdmxv2ConceptRoleBuilder")]
        public void ShouldNotAddSpecialConceptRole()
        {
            // TODO This test may not be valid if the filtering happens at SdmxSource level
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.DataStructures.First(x => !x.IsFinal.IsTrue);
            var mutable = identifiableObject.MutableInstance;

            // first we add a concept role
            var attribute = mutable.GetAttribute("TIME_FORMAT");
            StructureReferenceImpl specialConceptRole = new StructureReferenceImpl("ESTAT", "COMPONENT_ROLES", "1.0", SdmxStructureEnumType.Concept, "TIME_FORMAT");
            attribute.AddConceptRole(specialConceptRole);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success),
                string.Join('\n', replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var retriever = new MappingStoreRetrievalManager(this.GetConnectionStringSettings());
            var count = retriever.GetMutableMaintainables(identifiableObject.AsReference, false, true).Count;
            Assert.That(count, Is.EqualTo(1));
            var artefact = retriever.GetMutableMaintainable(identifiableObject.AsReference, false, false).ImmutableInstance as IDataStructureObject;
            Assert.That(!artefact.IsFinal.IsTrue);
            Assert.That(artefact.GetAttribute(attribute.Id), Is.Not.Null);
            Assert.That(artefact.GetAttribute(attribute.Id).ConceptRoles, Is.Null.Or.Empty);
        }

        [Test]
        public void ShouldAddRemoveComponents()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.DataStructures.First(x => !x.IsFinal.IsTrue);
            var mutable = identifiableObject.MutableInstance;

            string removed = mutable.Dimensions[1].Id;
            mutable.Dimensions[1].Id = "NEW_ID";
            // update groups as well
            foreach (var group in mutable.Groups)
            {
                if (group.DimensionRef.Contains(removed))
                {
                    group.DimensionRef.Remove(removed);
                    group.DimensionRef.Add("NEW_ID");
                }
            }
            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success),
                string.Join('\n', replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var structureQueryStub = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Full);
            var artefact = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryFull).First().ImmutableInstance as IDataStructureObject;
            Assert.That(!artefact.IsFinal.IsTrue);
            Assert.That(artefact.GetDimension(removed), Is.Null);
            Assert.That(artefact.GetDimension("NEW_ID"), Is.Not.Null);
        }

        [Test]
        public void ShouldUpdateRemoveRepresentation()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            ISdmxObjects updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            IDataStructureObject identifiableObject = _toAdd.DataStructures.First(x => !x.IsFinal.IsTrue);
            IDataStructureMutableObject mutable = identifiableObject.MutableInstance;
            // update representation (change codelist)
            IStructureReference newRepresentation = _toAdd.Codelists.First(codelistObject => string.Equals(codelistObject.Id, "CL_UNIT", StringComparison.OrdinalIgnoreCase)).AsReference;
            IRepresentationMutableObject representation = mutable.GetDimension("FREQ").Representation;
            representation.Representation = newRepresentation;
            // remove representation (remove codelist)
            mutable.GetAttribute("OBS_STATUS").Representation = null;
            // submit updated structures
            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);
            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success),
                string.Join('\n', replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));
            // verify results
            ICommonStructureQuery structureQueryStub = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));

            ICommonStructureQuery structureQueryFull = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Full);
            var artefact = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryFull).First().ImmutableInstance as IDataStructureObject;
            Assert.IsFalse(artefact.IsFinal.IsTrue);
            // verify updated representation
            Assert.AreEqual(newRepresentation, artefact.GetDimension("FREQ").Representation.Representation);
            // verify removed representation
            Assert.IsNull(artefact.GetAttribute("OBS_STATUS").Representation);
        }

        [Test]
        public void ShouldNotAllowComponentTypeChange()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            ISdmxObjects updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            IDataStructureObject identifiableObject = _toAdd.DataStructures.First(x => !x.IsFinal.IsTrue);
            IDataStructureMutableObject mutable = identifiableObject.MutableInstance;
            // change component type (and add concept role, which should be ignored)
            mutable.RemoveComponent("FREQ");
            IAttributeMutableObject changedType = mutable.GetAttribute("OBS_COM");
            changedType.Id = "FREQ";
            changedType.AddConceptRole(changedType.ConceptRef);
            // add concept role
            IDimensionMutableObject refArea = mutable.GetDimension("REF_AREA");
            refArea.ConceptRole.Add(refArea.ConceptRef.CreateCopy());
            // add concept role to attribute
            IAttributeMutableObject obsConf = mutable.GetAttribute("OBS_CONF");
            obsConf.AddConceptRole(obsConf.ConceptRef.CreateCopy());
            // submit updated structures
            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);
            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Warning),
                string.Join('\n', replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));
            Assert.IsTrue(replaceResult
                .SelectMany(r => r.Messages).SelectMany(m => m.Text).Select(t => t.Value)
                .Any(v => v.Contains("the following were not updated due to changed type: Data Attribute FREQ", StringComparison.OrdinalIgnoreCase)),
                string.Join('\n', replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));
            // verify results
            ICommonStructureQuery structureQueryStub = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));

            ICommonStructureQuery structureQueryFull = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Full);
            var artefact = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryFull).First().ImmutableInstance as IDataStructureObject;
            Assert.IsFalse(artefact.IsFinal.IsTrue);
            // verify concept roles
            Assert.IsNull(artefact.GetAttribute("OBS_COM")); // id was changed to FREQ => it gets removed
            Assert.IsNull(artefact.GetAttribute("FREQ")); // no new attribute is created (type change didn't go through)
            Assert.IsNotNull(artefact.GetDimension("FREQ")); // the dimension is still there (type change didn't go through)
            Assert.IsFalse(artefact.GetDimension("FREQ").ConceptRole.Any()); // concept roles were not added as expected
            Assert.IsTrue(artefact.GetDimension("REF_AREA").ConceptRole.Any()); // concept role added
            Assert.IsTrue(artefact.GetAttribute("OBS_CONF").ConceptRoles.Any()); // concept role added
        }

        [Test]
        public void ShouldAddRemoveCodes()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.Codelists.First(x => !x.IsFinal.IsTrue);
            var mutable = identifiableObject.MutableInstance;

            mutable.FinalStructure = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);
            string removed = mutable.Items[0].Id;
            var itemWithNewParent = mutable.Items[1];
            itemWithNewParent.ParentCode = "TEST";
            mutable.RemoveItem(removed);
            mutable.CreateItem("TEST", "A name");
            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success),
                string.Join('\n', replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var structureQueryStub = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Full);
            var codelist = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryFull).First().ImmutableInstance as ICodelistObject;
            Assert.That(codelist.IsFinal.IsTrue);
            Assert.That(codelist.GetCodeById(removed), Is.Null);
            Assert.That(codelist.GetCodeById("TEST"), Is.Not.Null);
            Assert.That(codelist.GetCodeById(itemWithNewParent.Id), Is.Not.Null);
            Assert.That(codelist.GetCodeById(itemWithNewParent.Id).ParentCode, Is.EqualTo("TEST"));
        }

        [Test]
        public void ShouldAddRemoveDataProviders()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);

            var identifiableObject = _toAdd.DataProviderSchemes.First(x => !x.IsFinal.IsTrue);
            var mutable = identifiableObject.MutableInstance;

            string notRemoved = mutable.Items[0].Id;
            string removed = mutable.Items[1].Id;
            mutable.RemoveItem(removed);
            mutable.CreateItem("TEST", "A name");
            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success),
                string.Join('\n', replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var structureQueryStub = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Full);
            var codelist = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryFull).First() as DataProviderSchemeMutableCore;
            Assert.That(codelist.Items.FirstOrDefault(x => x.Id.Equals(removed)), Is.Null);
            Assert.That(codelist.Items.FirstOrDefault(x => x.Id.Equals(notRemoved)), Is.Not.Null);
            Assert.That(codelist.Items.FirstOrDefault(x => x.Id.Equals("TEST")), Is.Not.Null);
        }

        [Test]
        public void ShouldAddRemoveConcepts()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.ConceptSchemes.First(x => !x.IsFinal.IsTrue);
            var mutable = identifiableObject.MutableInstance;

            mutable.FinalStructure = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);
            string removedButUsed = mutable.Items[0].Id;
            mutable.CreateItem("TEST", "A name");
            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success),
                string.Join('\n', replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var structureQueryStub = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(identifiableObject.AsReference, ComplexStructureQueryDetailEnumType.Full);
            var codelist = _container.GetEngine(identifiableObject.StructureType).Retrieve(structureQueryFull).First().ImmutableInstance as ConceptSchemeObjectCore;
            Assert.That(codelist.IsFinal.IsTrue);
            Assert.That(codelist.GetItemById(removedButUsed), Is.Not.Null);
            Assert.That(codelist.GetItemById("TEST"), Is.Not.Null);
        }

    }
}
