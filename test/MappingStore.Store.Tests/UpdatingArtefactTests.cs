// -----------------------------------------------------------------------
// <copyright file="UpdatingArtefactTests.cs" company="EUROSTAT">
//   Date Created : 2017-11-20
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using DryIoc;
using Estat.Sdmxsource.Extension.Constant;
using Estat.Sdmxsource.Extension.Factory;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.MappingStore.Store.Factory;
using Estat.Sri.MappingStoreRetrieval.Manager;
using log4net;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
using Org.Sdmxsource.Sdmx.Util.Extension;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Util.Extensions;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sdmxsource.Extension.Extension;
using Estat.Sri.MappingStoreRetrieval.Extensions;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
using System.Security.Cryptography;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;

namespace MappingStore.Store.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class UpdatingArtefactTests : TestBase
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log;

        private readonly ISdmxObjects _toDelete;
        private readonly ISdmxObjects _toAdd;
        private readonly RetrievalEngineContainer _container;

        /// <summary>
        /// Initializes static members of the <see cref="UpdatingFinalArtefactsTests"/> class.
        /// </summary>
        static UpdatingArtefactTests()
        {
            _log = LogManager.GetLogger(typeof(UpdatingArtefactTests));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestMappingStoreManager"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public UpdatingArtefactTests(string connectionName) : base(connectionName)
        {
            try
            {
                this.InitializeMappingStore(this.StoreId);
                IoCContainer.Register<ConnectionStringFactory>();
                IoCContainer.Register(made: Made.Of(r => ServiceInfo.Of<ConnectionStringFactory>(), m => m.Function));
                IoCContainer.Register<IStructureSubmitFactory, StructureSubmitMappingStoreFactory>();
                IoCContainer.Register<IStructureSubmitter, StructureSubmitter>(reuse: Reuse.Singleton);
                var smdxObjects = new FileInfo("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-2cat-small.xml").GetSdmxObjects(IoCContainer.Resolve<IStructureParsingManager>());
                _toDelete = new SdmxObjectsImpl(DatasetActionEnumType.Delete);
                _toAdd = new SdmxObjectsImpl(DatasetActionEnumType.Append);
                _toDelete.Merge(smdxObjects);
                _toAdd.Merge(smdxObjects);
                _container = new RetrievalEngineContainer(new Database(this.GetConnectionStringSettings()));
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        [TearDown]
        public void Cleanup()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            submitter.SubmitStructures(this.StoreId, _toDelete);
        }

        [SetUp]
        public void Init()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var result = submitter.SubmitStructures(this.StoreId, _toAdd);
            Assert.That(result, Is.Not.Empty);
            Assert.That(result.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));
        }

        [Test]
        public void ShouldNotBeAbleToDeleteDsdAndNotGetDbException()
        {
            var dataflowQuery = CommonStructureQueryCore
                .Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                .SetMaintainableTarget(SdmxStructureEnumType.Dataflow)
                .Build();
            var dataflow = this.GetRetrievalEngineContainer().DataflowRetrievalEngine.Retrieve(dataflowQuery).First();
            var dsdQuery = CommonStructureQueryCore
                .Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                .SetStructureIdentification(dataflow.DataStructureRef)
                .SetRequestedDetail(ComplexStructureQueryDetailEnumType.Stub)
                .Build();
            var dsd = this.GetRetrievalEngineContainer().DSDRetrievalEngine.Retrieve(dsdQuery).First().ImmutableInstance;

            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            var objects = new SdmxObjectsImpl(DatasetActionEnumType.Delete);
            objects.AddDataStructure(dsd);
            var result = submitter.SubmitStructures(this.StoreId, objects);
            Assert.That(result, Is.Not.Empty);
            Assert.That(result[0].StructureReference, Is.EqualTo(dsd.AsReference));
            Assert.That(result[0].Status, Is.EqualTo(ResponseStatus.Failure));
            Assert.That(result[0].Messages, Is.Not.Empty);
            Assert.That(result[0].Messages[0].ErrorCode, Is.Not.Null);
            Assert.That(result[0].Messages[0].ErrorCode.HttpRestErrorCode, Is.EqualTo(409));
            Assert.That(result[0].Messages[0].ErrorCode, Is.EqualTo(SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.Conflict)));
        }

        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        public void ShouldUpdateArtefactNonFinalAttributes(SdmxStructureEnumType sdmxStructure)
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.GetMaintainables(sdmxStructure).First();
            var mutable = identifiableObject.MutableInstance;
            mutable.Uri = new Uri("http://localhost/ws/rest/codelist/");
            mutable.StartDate = new DateTime(2000, 1, 1);
            mutable.EndDate = new DateTime(2008, 6, 1);
            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryFull).First();
            Assert.That(maintainable.StartDate, Is.EqualTo(mutable.StartDate));
            Assert.That(maintainable.EndDate, Is.EqualTo(mutable.EndDate));
            Assert.That(maintainable.Uri, Is.EqualTo(mutable.Uri));
        }

        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        public void ShouldUpdateArtefactNameWithSameLocale(SdmxStructureEnumType sdmxStructure)
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.GetMaintainables(sdmxStructure).First();
            var mutable = identifiableObject.MutableInstance;
            mutable.Names.Clear();
            mutable.AddName("en", "TEST2");
            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryFull).First();

            Assert.That(maintainable.Names.Count, Is.EqualTo(1));
            Assert.That(maintainable.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(maintainable.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));
        }

        [Test]
        public void ShouldUpdateCategoryNameWithSameLocale()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;
            var code = mutable.Items[0].Items[0];
            code.Names.Clear();
            code.AddName("en", "Test Code 3000");
            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var codelist = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryFull).First();

            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = FindFirstCategory(codelist.ImmutableInstance, code.Id);
            Assert.That(retrievedCode, Is.Not.Null);
            Assert.That(retrievedCode.Id, Is.EqualTo(code.Id));
            Assert.That(retrievedCode.Names.Count, Is.EqualTo(1));
            Assert.That(retrievedCode.Names[0].Locale, Is.EqualTo(code.Names[0].Locale));
            Assert.That(retrievedCode.Names[0].Value, Is.EqualTo(code.Names[0].Value));
        }

        [Test]
        public void ShouldUpdatComponentNewAnnotation()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var dsd = _toAdd.DataStructures.First();
            var mutable = dsd.MutableInstance;
            var code = mutable.GetDimension("REF_AREA");
            var annotation = code.AddAnnotation(null, "TEST_TYPE", null);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var structureQueryStub = CreateQuery(dsd.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.DSDRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(dsd.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var retrievedDsd = _container.DSDRetrievalEngine.Retrieve(structureQueryFull).First();

            Assert.That(retrievedDsd.Names.Count, Is.EqualTo(1));
            Assert.That(retrievedDsd.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(retrievedDsd.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = retrievedDsd.GetDimension(code.Id);
            Assert.That(retrievedCode, Is.Not.Null);
            Assert.That(retrievedCode.Id, Is.EqualTo(code.Id));
            Assert.That(retrievedCode.Annotations.Count, Is.EqualTo(1));
            Assert.That(retrievedCode.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(retrievedCode.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(retrievedCode.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(retrievedCode.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(retrievedCode.Annotations[0].Text.Count, Is.EqualTo(annotation.Text.Count));
        }

        [Test]
        public void ShouldUpdatComponentUpdateAnnotation()
        {
            ShouldUpdatComponentNewAnnotation();
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var dsd = _toAdd.DataStructures.First();
            var mutable = dsd.MutableInstance;
            var code = mutable.GetDimension("REF_AREA");
            var annotation = code.AddAnnotation(null, "TEST_TYPE", "http://localhost/ws");

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var structureQueryStub = CreateQuery(dsd.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.DSDRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(dsd.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var retrievedDsd = _container.DSDRetrievalEngine.Retrieve(structureQueryFull).First();

            Assert.That(retrievedDsd.Names.Count, Is.EqualTo(1));
            Assert.That(retrievedDsd.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(retrievedDsd.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = retrievedDsd.GetDimension(code.Id);
            Assert.That(retrievedCode, Is.Not.Null);
            Assert.That(retrievedCode.Id, Is.EqualTo(code.Id));
            Assert.That(retrievedCode.Annotations.Count, Is.EqualTo(1));
            Assert.That(retrievedCode.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(retrievedCode.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(retrievedCode.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(retrievedCode.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(retrievedCode.Annotations[0].Text.Count, Is.EqualTo(annotation.Text.Count));
        }

        [Test]
        public void ShouldUpdatComponentUpdateAnnotationWithText()
        {
            ShouldUpdatComponentNewAnnotation();
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var dsd = _toAdd.DataStructures.First();
            var mutable = dsd.MutableInstance;
            var code = mutable.GetDimension("REF_AREA");
            var annotation = code.AddAnnotation(null, "TEST_TYPE", null);
            annotation.AddText("en", "TEST 123");
            annotation.AddText("de", "DE TEST 123");
            annotation.AddText("fr", "DE TEST 123");

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var structureQueryStub = CreateQuery(dsd.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.DSDRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(dsd.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var retrievedDsd = _container.DSDRetrievalEngine.Retrieve(structureQueryFull).First().ImmutableInstance;
            Assert.That(retrievedDsd.Names.Count, Is.EqualTo(1));
            Assert.That(retrievedDsd.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(retrievedDsd.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = retrievedDsd.GetDimension(code.Id);
            AssetAnnotations(code, retrievedCode);
        }

        [Test]
        public void ShouldUpdatComponentUpdateAnnotationWithTextRemove()
        {
            ShouldUpdatComponentUpdateAnnotationWithText();
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var dsd = _toAdd.DataStructures.First();
            var mutable = dsd.MutableInstance;
            var code = mutable.GetDimension("REF_AREA");
            var annotation = code.AddAnnotation(null, "TEST_TYPE", null);
            annotation.AddText("en", "TEST 123");
            annotation.AddText("it", "DE TEST 123");

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var structureQueryStub = CreateQuery(dsd.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.DSDRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(dsd.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var retrievedDsd = _container.DSDRetrievalEngine.Retrieve(structureQueryFull).First().ImmutableInstance;
            Assert.That(retrievedDsd.Names.Count, Is.EqualTo(1));
            Assert.That(retrievedDsd.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(retrievedDsd.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = retrievedDsd.GetDimension(code.Id);
            AssetAnnotations(code, retrievedCode);
        }

        private ICommonStructureQuery CreateQuery(IStructureReference identification, SdmxStructureEnumType target, ComplexStructureQueryDetailEnumType detail)
        {
            return CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                    .SetMaintainableTarget(SdmxStructureType.GetFromEnum(target))
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(detail))
                    .SetStructureIdentification(identification)
                    .Build();
        }

        [Test]
        public void ShouldUpdatCategoryNewAnnotation()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;
            var code = mutable.Items[0].Items[0];
            var annotation = code.AddAnnotation(null, "TEST_TYPE", null);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));



            var structureQuery = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var categorySchemes = _container.CategorySchemeRetrievalEngine.Retrieve(structureQuery);
            var count = categorySchemes.Count();
            Assert.That(count, Is.EqualTo(1));

            var codelist = categorySchemes.First();
            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = FindFirstCategory(codelist.ImmutableInstance, code.Id);
            Assert.That(retrievedCode, Is.Not.Null);
            Assert.That(retrievedCode.Id, Is.EqualTo(code.Id));
            Assert.That(retrievedCode.Annotations.Count, Is.EqualTo(1));
            Assert.That(retrievedCode.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(retrievedCode.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(retrievedCode.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(retrievedCode.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(retrievedCode.Annotations[0].Text.Count, Is.EqualTo(annotation.Text.Count));
        }

        [Test]
        public void ShouldUpdateCategoryNewAnnotationUpdate()
        {
            ShouldUpdatCategoryNewAnnotation();
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;
            var code = mutable.Items[0].Items[0];
            var annotation = code.AddAnnotation(null, "TEST_TYPE", "http://localhost/ws");

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var codelist = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryFull).First();

            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = FindFirstCategory(codelist.ImmutableInstance, code.Id);
            Assert.That(retrievedCode, Is.Not.Null);
            Assert.That(retrievedCode.Id, Is.EqualTo(code.Id));
            Assert.That(retrievedCode.Annotations.Count, Is.EqualTo(1));
            Assert.That(retrievedCode.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(retrievedCode.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(retrievedCode.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(retrievedCode.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(retrievedCode.Annotations[0].Text.Count, Is.EqualTo(annotation.Text.Count));
        }

        [Test]
        public void ShouldUpdateCategoryNewAnnotationUpdateWithText()
        {
            ShouldUpdatCategoryNewAnnotation();
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;
            var code = mutable.Items[0].Items[0];
            var annotation = code.AddAnnotation(null, "TEST_TYPE", null);
            annotation.AddText("en", "TEST 123");
            annotation.AddText("de", "DE TEST 123");
            annotation.AddText("fr", "DE TEST 123");

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var codelist = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryFull).First();
            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = FindFirstCategory(codelist.ImmutableInstance, code.Id);
            AssetAnnotations(code, retrievedCode);
        }

        [Test]
        public void ShouldUpdateCategoryNewAnnotationUpdateWithTextRemove2()
        {
            ShouldUpdateCategoryNewAnnotationUpdateWithText();
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;
            var code = mutable.Items[0].Items[0];
            var annotation = code.AddAnnotation(null, "TEST_TYPE", null);
            annotation.AddText("en", "TEST 123");
            annotation.AddText("it", "IT TEST 123");

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var codelist = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryFull).First();
            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = FindFirstCategory(codelist.ImmutableInstance, code.Id);
            Assert.That(retrievedCode, Is.Not.Null);
            Assert.That(retrievedCode.Id, Is.EqualTo(code.Id));
            Assert.That(retrievedCode.Annotations.Count, Is.EqualTo(code.Annotations.Count));
            for (int x = 0; x < code.Annotations.Count; x++)
            {
                Assert.That(retrievedCode.Annotations[x].Id, Is.EqualTo(code.Annotations[x].Id));
                Assert.That(retrievedCode.Annotations[x].Title, Is.EqualTo(code.Annotations[x].Title));
                Assert.That(retrievedCode.Annotations[x].Type, Is.EqualTo(code.Annotations[x].Type));
                Assert.That(retrievedCode.Annotations[x].Uri, Is.EqualTo(code.Annotations[x].Uri));
                Assert.That(retrievedCode.Annotations[x].Text.Count, Is.EqualTo(code.Annotations[x].Text.Count));
                for (int i = 0; i < code.Annotations[x].Text.Count; i++)
                {
                    Assert.That(retrievedCode.Annotations[0].Text[i].Locale, Is.EqualTo(code.Annotations[x].Text[i].Locale));
                    Assert.That(retrievedCode.Annotations[0].Text[i].Value, Is.EqualTo(code.Annotations[x].Text[i].Value));
                }
            }
        }

        [Test]
        public void ShouldUpdateCategoryNewAnnotationUpdateWithTextRemove()
        {
            ShouldUpdateCategoryNewAnnotationUpdateWithText();
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;
            var code = mutable.Items[0].Items[0];
            var annotation = code.AddAnnotation(null, "TEST_TYPE", null);
            annotation.AddText("en", "TEST 123");
            annotation.AddText("de", "DE TEST 123");
            var annotation2 = code.AddAnnotation("SOMEHTING_ELSE", "TEH", null);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var codelist = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryFull).First();

            Assert.That(codelist.Names.Count, Is.EqualTo(1));
            Assert.That(codelist.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(codelist.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedCode = FindFirstCategory(codelist.ImmutableInstance, code.Id);
            AssetAnnotations(code, retrievedCode);
        }

        [Test]
        public void ShouldAddNewCategory()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;
            var expectedItem = new CategoryMutableCore();
            expectedItem.Id = "TEST_ID";
            expectedItem.AddName("en", "Testing 123");
            var annotation = expectedItem.AddAnnotation(null, "TEST_TYPE", null);
            annotation.AddText("en", "TEST 123");
            annotation.AddText("de", "DE TEST 123");
            var annotation2 = expectedItem.AddAnnotation("SOMEHTING_ELSE", "TEH", null);
            ICategoryMutableObject root = mutable.Items[0];
            ICategoryMutableObject firstLevel = root.Items[0];
            firstLevel.AddItem(expectedItem);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryFull).First();

            Assert.That(maintainable.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(maintainable.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedItem = maintainable.ImmutableInstance.GetCategory(root.Id, firstLevel.Id, expectedItem.Id);
            AssetAnnotations(expectedItem, retrievedItem);
        }

        [Test]
        public void ShouldAddNewCategoryWithSameIdOnDifferentLevel()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;
            var expectedItem = new CategoryMutableCore
            {
                Id = "SSTSRTD"
            };
            expectedItem.AddName("en", "NEW - SODI - Short-term Statistics on Retail Trade");

            ICategoryMutableObject root = mutable.Items[0];
            root.AddItem(expectedItem);

            ICategorySchemeObject updatedCategoryScheme = mutable.ImmutableInstance;
            updateSdmxObject.AddIdentifiable(updatedCategoryScheme);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryFull).First().ImmutableInstance;

            var expected = updatedCategoryScheme.IdentifiableComposites.Where(x => x.GetFullIdPath(false).EndsWith("." + expectedItem.Id)).Select(x => x.GetFullIdPath(false)).OrderBy(x => x).ToArray();
            var actual = maintainable.IdentifiableComposites.Where(x => x.GetFullIdPath(false).EndsWith("." + expectedItem.Id)).Select(x => x.GetFullIdPath(false)).OrderBy(x => x).ToArray();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void ShouldAddNewRootCategory()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;
            var expectedItem = new CategoryMutableCore();
            expectedItem.Id = "TEST_ID";
            expectedItem.AddName("en", "Testing 123");
            var annotation = expectedItem.AddAnnotation(null, "TEST_TYPE", null);
            annotation.AddText("en", "TEST 123");
            annotation.AddText("de", "DE TEST 123");
            var annotation2 = expectedItem.AddAnnotation("SOMEHTING_ELSE", "TEH", null);
            mutable.AddItem(expectedItem);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryFull).First();

            Assert.That(maintainable.Names.Count, Is.EqualTo(1));
            Assert.That(maintainable.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(maintainable.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedItem = maintainable.ImmutableInstance.GetCategory(expectedItem.Id);
            AssetAnnotations(expectedItem, retrievedItem);
        }

        [Test]
        public void ShouldAddNewCategoriesWithChildren()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;
            var expectedItem = new CategoryMutableCore();
            expectedItem.Id = "TEST_ID";
            expectedItem.AddName("en", "Testing 123");
            var annotation = expectedItem.AddAnnotation(null, "TEST_TYPE", null);
            annotation.AddText("en", "TEST 123");
            annotation.AddText("de", "DE TEST 123");
            var annotation2 = expectedItem.AddAnnotation("SOMEHTING_ELSE", "TEH", null);
            ICategoryMutableObject root = mutable.Items[0];
            ICategoryMutableObject firstLevel = root.Items[0];
            ICategoryMutableObject newWithChild = new CategoryMutableCore();
            newWithChild.Id = "TEST_WITH_CHILD";
            newWithChild.AddName("en", "Testing 123");
            newWithChild.AddItem(expectedItem);
            firstLevel.AddItem(newWithChild);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryFull).First();

            Assert.That(maintainable.Names.Count, Is.EqualTo(1));
            Assert.That(maintainable.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(maintainable.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var retrievedItem = maintainable.ImmutableInstance.GetCategory(root.Id, firstLevel.Id, newWithChild.Id, expectedItem.Id);
            AssetAnnotations(expectedItem, retrievedItem);
        }

        [Test]
        public void ShouldAddNewCategoriesReplaceExisting()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;
            var expectedItem = new CategoryMutableCore();
            expectedItem.Id = "TEST_ID";
            expectedItem.AddName("en", "Testing 123");
            var annotation = expectedItem.AddAnnotation(null, "TEST_TYPE", null);
            annotation.AddText("en", "TEST 123");
            annotation.AddText("de", "DE TEST 123");
            var annotation2 = expectedItem.AddAnnotation("SOMEHTING_ELSE", "TEH", null);
            ICategoryMutableObject root = mutable.Items[0];
            ICategoryMutableObject firstLevel = root.Items[0];
            expectedItem.Items.AddAll(firstLevel.Items);

            // remove firstLevel
            root.Items.RemoveAt(0);

            root.AddItem(expectedItem);

            var categorisation = _toAdd.Categorisations.First();

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryFull).First();

            Assert.That(maintainable.Names.Count, Is.EqualTo(1));
            Assert.That(maintainable.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(maintainable.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));
            ICategorySchemeObject immutableInstance = maintainable.ImmutableInstance;
            var retrievedItem = immutableInstance.GetCategory(root.Id, expectedItem.Id);
            AssetAnnotations(expectedItem, retrievedItem);
            Assert.That(immutableInstance.GetCategory(root.Id, firstLevel.Id), Is.Null);

            ///categorisation should be updated with the new path
            var structureQueryFullCategorisation = CreateQuery(categorisation.AsReference, SdmxStructureEnumType.Categorisation, ComplexStructureQueryDetailEnumType.Full);
            var categorisationMutable = _container.CategorisationRetrievalEngine.Retrieve(structureQueryFullCategorisation).FirstOrDefault();

            Assert.That(categorisationMutable, Is.Not.Null);
            Assert.True(categorisationMutable.CategoryReference.FullId.Contains(retrievedItem.AsReference.FullId));
        }


        [Test]
        public void ShouldMoveCategory()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance as CategorySchemeMutableCore;
            var categorisation = _toAdd.Categorisations.First();

            ///move category one step up
            
            
            //referencedCategory.
            ICategoryMutableObject root = mutable.Items[0];
            ICategoryMutableObject firstLevel = root.Items[0];
            ICategoryMutableObject secondLevel = firstLevel.Items.First(x=>x.Id == categorisation.CategoryReference.IdentifiableIds.Last());
            firstLevel.Items.Remove(secondLevel);

            root.AddItem(secondLevel);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryFull).First();

            Assert.That(maintainable.Names.Count, Is.EqualTo(1));
            Assert.That(maintainable.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(maintainable.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            var movedCategory = maintainable.ImmutableInstance.GetCategory(root.Id, secondLevel.Id);
            Assert.That(movedCategory, Is.Not.Null);
            Assert.That(maintainable.ImmutableInstance.GetCategory(root.Id, firstLevel.Id), Is.Not.Null);
            Assert.That(maintainable.ImmutableInstance.GetCategory(root.Id, firstLevel.Id, secondLevel.Id), Is.Null);

            ///categorisation should be updated with the new path
            var structureQueryFullCategorisation = CreateQuery(categorisation.AsReference, SdmxStructureEnumType.Categorisation, ComplexStructureQueryDetailEnumType.Full);
            var categorisationMutable = _container.CategorisationRetrievalEngine.Retrieve(structureQueryFullCategorisation).FirstOrDefault();

            Assert.That(categorisationMutable, Is.Not.Null);
            Assert.That(categorisationMutable.CategoryReference.FullId,Is.EqualTo(movedCategory.AsReference.FullId));
        }

        [Test]
        public void ShouldRemoveCategories()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;
            var categorisation = _toAdd.Categorisations.First();

            ICategoryMutableObject root = mutable.Items[0];
            ICategoryMutableObject firstLevel = root.Items[0];
            root.Items.RemoveAt(0);


            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(categorySchemeObject.AsReference, SdmxStructureEnumType.Category, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.CategorySchemeRetrievalEngine.Retrieve(structureQueryFull).First();

            Assert.That(maintainable.Names.Count, Is.EqualTo(1));
            Assert.That(maintainable.Names[0].Locale, Is.EqualTo(mutable.Names[0].Locale));
            Assert.That(maintainable.Names[0].Value, Is.EqualTo(mutable.Names[0].Value));

            Assert.That(maintainable.ImmutableInstance.GetCategory(root.Id, firstLevel.Id), Is.Null);

            ///categorisation should be deleted if we delete the category
            var structureQueryFullCategorisation = CreateQuery(categorisation.AsReference, SdmxStructureEnumType.Categorisation, ComplexStructureQueryDetailEnumType.Full);
            var categorisationMutable = _container.CategorisationRetrievalEngine.Retrieve(structureQueryFullCategorisation).FirstOrDefault();

            Assert.That(categorisationMutable, Is.Null);
        }


        [Test]
        public void ShouldAddDeleteCategorisationsWhenCategoriesAreRemoved()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var categorySchemeObject = _toAdd.CategorySchemes.First();
            var mutable = categorySchemeObject.MutableInstance;

            var categorisationObject = _toAdd.Categorisations.First();

            ICategoryMutableObject root = mutable.Items[0];
            ICategoryMutableObject firstLevel = root.Items[0];
            root.Items.RemoveAt(0);


            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            
            var structureQueryFull = CreateQuery(categorisationObject.AsReference, SdmxStructureEnumType.Categorisation, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.CategorisationRetrievalEngine.Retrieve(structureQueryFull).FirstOrDefault();

            Assert.That(maintainable, Is.Null);
        }

        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        public void ShouldAddArtefactNameWithNewLocale(SdmxStructureEnumType sdmxStructure)
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.GetMaintainables(sdmxStructure).First();
            var mutable = identifiableObject.MutableInstance;
            mutable.AddName("el", "TEST2");
            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var structureQueryStub = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryFull).First();

            Assert.That(maintainable.Names.Count, Is.EqualTo(2));
            foreach (var text in mutable.Names)
            {
                Assert.That(maintainable.Names, Has.One.Property(nameof(ITextTypeWrapperMutableObject.Locale)).EqualTo(text.Locale));
                var value = maintainable.Names.First(x => x.Locale.Equals(text.Locale));
                Assert.That(value.Value, Is.EqualTo(text.Value));
            }
        }

        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        public void ShouldAddArtefactNewAnnotation(SdmxStructureEnumType sdmxStructure)
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.GetMaintainables(sdmxStructure).First();
            var mutable = identifiableObject.MutableInstance;
            var annotation = mutable.AddAnnotation("TEST_TITLE", "TEST_TYPE", null);
            annotation.AddText("en", "Annotation Text");

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var structureQueryStub = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryFull).First();

            FilterSpecialAnnotations(maintainable);
            Assert.That(maintainable.Annotations.Count, Is.EqualTo(1));
            Assert.That(maintainable.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(maintainable.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(maintainable.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(maintainable.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(maintainable.Annotations[0].Text.Count, Is.EqualTo(1));
            Assert.That(maintainable.Annotations[0].Text[0].Locale, Is.EqualTo(annotation.Text[0].Locale));
            Assert.That(maintainable.Annotations[0].Text[0].Value, Is.EqualTo(annotation.Text[0].Value));
        }

        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        public void ShouldUpdateArtefactWithNewAnnotationAndThenUpdateAnnotation(SdmxStructureEnumType sdmxStructure)
        {
            ShouldAddArtefactNewAnnotation(sdmxStructure);

            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.GetMaintainables(sdmxStructure).First();
            var mutable = identifiableObject.MutableInstance;
            var annotation = mutable.AddAnnotation("TEST_TITLE2", "TEST_TYPE2", null);
            annotation.AddText("en", "Annotation Text2");

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryFull).First();

            FilterSpecialAnnotations(maintainable);
            Assert.That(maintainable.Annotations.Count, Is.EqualTo(1));
            Assert.That(maintainable.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(maintainable.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(maintainable.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(maintainable.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(maintainable.Annotations[0].Text.Count, Is.EqualTo(1));
            Assert.That(maintainable.Annotations[0].Text[0].Locale, Is.EqualTo(annotation.Text[0].Locale));
            Assert.That(maintainable.Annotations[0].Text[0].Value, Is.EqualTo(annotation.Text[0].Value));
        }

        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        public void ShouldUpdateArtefactWithNewAnnotationAndThenUpdateAnnotationTextOnly(SdmxStructureEnumType sdmxStructure)
        {
            ShouldAddArtefactNewAnnotation(sdmxStructure);

            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.GetMaintainables(sdmxStructure).First();
            var mutable = identifiableObject.MutableInstance;
            var annotation = mutable.AddAnnotation("TEST_TITLE", "TEST_TYPE", null);
            annotation.AddText("en", "Annotation Text3");

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryFull).First();

            FilterSpecialAnnotations(maintainable);
            Assert.That(maintainable.Annotations.Count, Is.EqualTo(1));
            Assert.That(maintainable.Annotations[0].Id, Is.EqualTo(annotation.Id));
            Assert.That(maintainable.Annotations[0].Title, Is.EqualTo(annotation.Title));
            Assert.That(maintainable.Annotations[0].Type, Is.EqualTo(annotation.Type));
            Assert.That(maintainable.Annotations[0].Uri, Is.EqualTo(annotation.Uri));
            Assert.That(maintainable.Annotations[0].Text.Count, Is.EqualTo(1));
            Assert.That(maintainable.Annotations[0].Text[0].Locale, Is.EqualTo(annotation.Text[0].Locale));
            Assert.That(maintainable.Annotations[0].Text[0].Value, Is.EqualTo(annotation.Text[0].Value));
        }

        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        public void ShouldUpdateCodelistWithNewAnnotationAndThenUpdateAnnotationAddNewText(SdmxStructureEnumType sdmxStructure)
        {
            ShouldAddArtefactNewAnnotation(sdmxStructure);

            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var identifiableObject = _toAdd.GetMaintainables(sdmxStructure).First();
            var mutable = identifiableObject.MutableInstance;
            var annotation = mutable.AddAnnotation("TEST_TITLE", "TEST_TYPE", null);
            annotation.AddText("en", "Annotation Text");
            annotation.AddText("de", "DE text");

            var expectedInstance = mutable.ImmutableInstance;
            updateSdmxObject.AddIdentifiable(expectedInstance);

            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

            var structureQueryStub = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Stub);
            var count = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryStub).Count();
            Assert.That(count, Is.EqualTo(1));
            var structureQueryFull = CreateQuery(identifiableObject.AsReference, sdmxStructure, ComplexStructureQueryDetailEnumType.Full);
            var maintainable = _container.GetEngine(SdmxStructureType.GetFromEnum(sdmxStructure)).Retrieve(structureQueryFull).First();

            FilterSpecialAnnotations(maintainable);
            AssetAnnotations(mutable, maintainable.ImmutableInstance);
        }


        [Test]
        public void ShouldUpdateMetadataAttributeUsage() {

        var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
         //import references
         var references = new FileInfo("tests/v21/MSDv21-TEST.xml").GetSdmxObjects(IoCContainer.Resolve<IStructureParsingManager>());

         submitter.SubmitStructures(StoreId, references);
         
        // load the structures from file
         var smdxObjects = new FileInfo("tests/v300/dsd-metadata-in-use.xml").GetSdmxObjects(IoCContainer.Resolve<IStructureParsingManager>());
        
        // it should not exist before importing it
        var identifiableObject = smdxObjects.DataStructures.First();
        var structureQueryStub = CreateQuery(identifiableObject.AsReference, SdmxStructureEnumType.Dsd, ComplexStructureQueryDetailEnumType.Full);
        var count = _container.GetEngine(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd)).Retrieve(structureQueryStub).Count();
        Assert.AreEqual(0, count);

        // import structures
        var insertResult = submitter.SubmitStructures(StoreId, smdxObjects);

        Assert.That(insertResult, Is.Not.Empty);

        var mutableDsd = _container.GetEngine(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd)).Retrieve(structureQueryStub).First() as IDataStructureMutableObject;
            // Change metadata attribute usage's dimension reference
            mutableDsd.Dimensions.Add(new DimensionMutableCore() { Id = "TEST" ,ConceptRef = new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=EXAMPLE:EXAMPLE_CONCEPTS(1.0.0).TEST") });
        mutableDsd.AttributeList.MetadataAttributes[0].DimensionReferences = new List<string>() { "TEST"};
        mutableDsd.FinalStructure = TertiaryBool.ParseBoolean(false);

        updateSdmxObject.AddDataStructure(mutableDsd.ImmutableInstance);

        var updateResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

        Assert.That(updateResult, Is.Not.Empty);
        Assert.That(updateResult.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));

        mutableDsd = _container.GetEngine(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd)).Retrieve(structureQueryStub).First() as IDataStructureMutableObject;
        List<IMetadataAttributeUsageMutableObject> updatedList = mutableDsd.AttributeList.MetadataAttributes.ToList();

        Assert.True(updatedList.Any());
        Assert.AreEqual(2, updatedList.Count);
        Assert.True(updatedList.Exists(x=>x.DimensionReferences.Contains("TEST")));
     
    }

    private ICategoryObject FindFirstCategory(ICategorySchemeObject artefact, string id)
        {
            var stack = new Stack<ICategoryObject>(artefact.Items);
            while(stack.Count > 0)
            {
                var current = stack.Pop();
                if (current.Id.Equals(id))
                {
                    return current;
                }

                foreach(var item in current.Items)
                {
                    stack.Push(item);
                }
            }

            return null;
        }

        /// <summary>
        /// Assets the annotations.
        /// </summary>
        /// <param name="expectedItem">The expected item.</param>
        /// <param name="retrievedItem">The retrieved item.</param>
        private static void AssetAnnotations(IIdentifiableMutableObject expectedItem, IIdentifiableObject retrievedItem)
        {
            Assert.That(retrievedItem, Is.Not.Null);
            Assert.That(retrievedItem.Id, Is.EqualTo(expectedItem.Id));
            Assert.That(retrievedItem.Annotations.Count, Is.EqualTo(expectedItem.Annotations.Count));
            for (int x = 0; x < expectedItem.Annotations.Count; x++)
            {
                Assert.That(retrievedItem.Annotations[x].Id, Is.EqualTo(expectedItem.Annotations[x].Id));
                Assert.That(retrievedItem.Annotations[x].Title, Is.EqualTo(expectedItem.Annotations[x].Title));
                Assert.That(retrievedItem.Annotations[x].Type, Is.EqualTo(expectedItem.Annotations[x].Type));
                Assert.That(retrievedItem.Annotations[x].Uri, Is.EqualTo(expectedItem.Annotations[x].Uri));
                Assert.That(retrievedItem.Annotations[x].Text.Count, Is.EqualTo(expectedItem.Annotations[x].Text.Count));
                var codeAnnotationText = expectedItem.Annotations[x].Text;

                foreach (var t1 in codeAnnotationText)
                {
                    var retrievedCodeText = retrievedItem.Annotations[0].Text
                        .First(t => t.Locale.Equals(t1.Locale));
                    Assert.That(retrievedCodeText.Locale, Is.EqualTo(t1.Locale));
                    Assert.That(retrievedCodeText.Value, Is.EqualTo(t1.Value));
                }
            }
        }

        /// <summary>
        /// Filters the special annotations.
        /// </summary>
        /// <param name="maintainableMutableObject">The maintainable mutable object.</param>
        private void FilterSpecialAnnotations(IMaintainableMutableObject maintainableMutableObject)
        {
            if (maintainableMutableObject.StructureType.EnumType == SdmxStructureEnumType.Dataflow)
            {
                var annotationMutableObjects = maintainableMutableObject.Annotations.Where(a => a.FromAnnotation() == CustomAnnotationType.NonProductionDataflow).ToList();
                foreach (var production in annotationMutableObjects)
                {
                    maintainableMutableObject.Annotations.Remove(production);
                }
            }
        }

        private class ConnectionStringFactory
        {
            readonly IConfigurationStoreManager _configurationStoreManager;

            public ConnectionStringFactory(IConfigurationStoreManager configurationStoreManager)
            {
                if (configurationStoreManager == null)
                {
                    throw new ArgumentNullException(nameof(configurationStoreManager));
                }

                _configurationStoreManager = configurationStoreManager;
            }
            
            public Func<string, ConnectionStringSettings> Function
            {
                get
                {
                    return x => this._configurationStoreManager.GetSettings<ConnectionStringSettings>().First(s => s.Name.Equals(x));
                }
            }
        }

    }


}
