using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DryIoc;
using Estat.Sdmxsource.Extension.Constant;
using Estat.Sdmxsource.Extension.Factory;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.MappingStore.Store.Factory;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using log4net;
using NUnit.Framework;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.Util.Extension;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;

namespace MappingStore.Store.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class UpdateNonFinalArtefactsAndProperties : TestBase
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log;

        private readonly ISdmxObjects _toDelete;
        private readonly ISdmxObjects _toAdd;

        /// <summary>
        /// Initializes static members of the <see cref="UpdatingFinalArtefactsTests"/> class.
        /// </summary>
        static UpdateNonFinalArtefactsAndProperties()
        {
            _log = LogManager.GetLogger(typeof(UpdatingArtefactTests));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestMappingStoreManager"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public UpdateNonFinalArtefactsAndProperties(string connectionName) : base(connectionName)
        {
            try
            {
                this.InitializeMappingStore(this.StoreId);
                IoCContainer.Register<ConnectionStringFactory>();
                IoCContainer.Register(made: Made.Of(r => ServiceInfo.Of<ConnectionStringFactory>(), m => m.Function));
                IoCContainer.Register<IStructureSubmitFactory, StructureSubmitMappingStoreFactory>();
                IoCContainer.Register<IStructureSubmitter, StructureSubmitter>(reuse: Reuse.Singleton);
                var smdxObjects = new FileInfo("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-2cat-small.xml").GetSdmxObjects(IoCContainer.Resolve<IStructureParsingManager>());
                _toDelete = new SdmxObjectsImpl(DatasetActionEnumType.Delete);
                _toAdd = new SdmxObjectsImpl(DatasetActionEnumType.Append);
                _toDelete.Merge(smdxObjects);
                _toAdd.Merge(smdxObjects);

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        [TearDown]
        public void Cleanup()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            submitter.SubmitStructures(this.StoreId, _toDelete);
        }

        [SetUp]
        public void Init()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var result = submitter.SubmitStructures(this.StoreId, _toAdd);
            Assert.That(result, Is.Not.Empty);
            Assert.That(result.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));
        }


        [Test]
        public void ShouldUpdateNonFinalPropertiesInFinalArtefact()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var dataflowObject = _toAdd.Dataflows.First();
            var mutable = dataflowObject.MutableInstance;
            var newName = "Test Name";
            var newDescription = "Test Description";

            //Test Insert
            mutable.AddName("de",newName);
            mutable.AddDescription("en",newDescription);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var container = new RetrievalEngineContainer(new Database(this.GetConnectionStringSettings()));
            var structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                    .SetMaintainableTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow))
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                    .SetStructureIdentification(dataflowObject.AsReference)
                    .Build();
            var updatedDataflow = container.DataflowRetrievalEngine.Retrieve(structureQuery).First();
           
            Assert.True(updatedDataflow.Names.ToList().Exists(x=>x.Value == newName));
            Assert.True(updatedDataflow.Descriptions.ToList().Exists(x => x.Value == newDescription));


            //Test Update
            var newNameUpdated = "new name update";
            var newDescriptionUpated = "new desc update";
            mutable.AddName("de", newNameUpdated);
            mutable.AddDescription("en", newDescriptionUpated);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            submitter.SubmitStructures(StoreId, updateSdmxObject);

            updatedDataflow = container.DataflowRetrievalEngine.Retrieve(structureQuery).First();

            Assert.True(updatedDataflow.Names.ToList().Exists(x => x.Value == newNameUpdated));
            Assert.True(updatedDataflow.Descriptions.ToList().Exists(x => x.Value == newDescriptionUpated));

            //Delete

            mutable.Names.RemoveAt(mutable.Names.Count-1);
            mutable.Descriptions.RemoveAt(mutable.Descriptions.Count-1);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            submitter.SubmitStructures(StoreId, updateSdmxObject);

            updatedDataflow = container.DataflowRetrievalEngine.Retrieve(structureQuery).First();

            Assert.False(updatedDataflow.Names.ToList().Exists(x => x.Value == newNameUpdated));
            Assert.False(updatedDataflow.Descriptions.ToList().Exists(x => x.Value == newDescriptionUpated));

        }

        [Test]
        public void ShouldUpdateNonFinalPropertiesInItems()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var codelist = _toAdd.Codelists.First();
            var codelistMutable = codelist.MutableInstance;
            var mutable = codelist.MutableInstance.Items[0];
            var newName = "Test Name";
            var newDescription = "Test Description";
            //Test Insert
            mutable.AddName("de", newName);
            mutable.AddDescription("en", newDescription);
            codelistMutable.Items[0] = mutable;
            updateSdmxObject.AddIdentifiable(codelistMutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var container = new RetrievalEngineContainer(new Database(this.GetConnectionStringSettings()));
            var structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                    .SetMaintainableTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow))
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                    .SetStructureIdentification(codelist.AsReference)
                    .Build();
            var updatedCodelist = container.CodeListRetrievalEngine.Retrieve(structureQuery).First();

            Assert.True(updatedCodelist.Items[0].Names.ToList().Exists(x => x.Value == newName));
            Assert.True(updatedCodelist.Items[0].Descriptions.ToList().Exists(x => x.Value == newDescription));


            //Test Update
            var newNameUpdated = "new name update";
            var newDescriptionUpated = "new desc update";
            mutable.AddName("de", newNameUpdated);
            mutable.AddDescription("en", newDescriptionUpated);
            codelistMutable.Items[0] = mutable;
            updateSdmxObject.AddIdentifiable(codelistMutable.ImmutableInstance);
            submitter.SubmitStructures(StoreId, updateSdmxObject);

            updatedCodelist = container.CodeListRetrievalEngine.Retrieve(structureQuery).First();

            Assert.True(updatedCodelist.Items[0].Names.ToList().Exists(x => x.Value == newNameUpdated));
            Assert.True(updatedCodelist.Items[0].Descriptions.ToList().Exists(x => x.Value == newDescriptionUpated));

            //Delete

            mutable.Names.RemoveAt(mutable.Names.Count - 1);
            mutable.Descriptions.RemoveAt(mutable.Descriptions.Count - 1);
            codelistMutable.Items[0] = mutable;
            updateSdmxObject.AddIdentifiable(codelistMutable.ImmutableInstance);
            submitter.SubmitStructures(StoreId, updateSdmxObject);

            updatedCodelist = container.CodeListRetrievalEngine.Retrieve(structureQuery).First();

            Assert.False(updatedCodelist.Items[0].Names.ToList().Exists(x => x.Value == newNameUpdated));
            Assert.False(updatedCodelist.Items[0].Descriptions.ToList().Exists(x => x.Value == newDescriptionUpated));

        }

        [Test]
        public void ShouldUpdateNonFinalPropertiesInArtefactTable()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var dataflowObject = _toAdd.Categorisations.First();

            var mutable = dataflowObject.MutableInstance;
            var validFrom = DateTime.Today;
            var validTo = DateTime.Today.AddDays(2);
            var uri = new Uri("http://www.test.com/");
            mutable.FinalStructure = TertiaryBool.ParseBoolean(false);
            mutable.Uri = uri;
            mutable.StartDate = validFrom;
            mutable.EndDate = validTo;

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var container = new RetrievalEngineContainer(new Database(this.GetConnectionStringSettings()));
            var structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                    .SetMaintainableTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow))
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                    .SetStructureIdentification(dataflowObject.AsReference)
                    .Build();
            var updatedCategorisation = container.CategorisationRetrievalEngine.Retrieve(structureQuery).First();

            Assert.That(updatedCategorisation.Uri,Is.EqualTo(uri));
            Assert.That(updatedCategorisation.StartDate, Is.EqualTo(validFrom));
            Assert.That(updatedCategorisation.EndDate, Is.EqualTo(validTo));
        }

        [Test]
        public void ShouldUpdateAnnotationsForArtefact()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var dataflowObject = _toAdd.Dataflows.First();
            var mutable = dataflowObject.MutableInstance;
            var newName = "Test Name";

            //Test Insert
            mutable.AddAnnotation(newName, "type", "http://www.test.com/");

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var container = new RetrievalEngineContainer(new Database(this.GetConnectionStringSettings()));
            var structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                    .SetMaintainableTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow))
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                    .SetStructureIdentification(dataflowObject.AsReference)
                    .Build();
            var updatedDataflow = container.DataflowRetrievalEngine.Retrieve(structureQuery).First();

            Assert.True(updatedDataflow.Annotations.ToList().Exists(x => x.Title == newName));


            //Test Update
            var newNameUpdated = "new name update";
            mutable.Annotations.First(x=> x.Title == newName).Title = newNameUpdated;

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            submitter.SubmitStructures(StoreId, updateSdmxObject);

            updatedDataflow = container.DataflowRetrievalEngine.Retrieve(structureQuery).First();

            Assert.True(updatedDataflow.Annotations.ToList().Exists(x => x.Title == newNameUpdated));

            //Delete

            mutable.Annotations.RemoveAt(mutable.Annotations.Count - 1);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            submitter.SubmitStructures(StoreId, updateSdmxObject);

            updatedDataflow = container.DataflowRetrievalEngine.Retrieve(structureQuery).First();

            Assert.False(updatedDataflow.Annotations.ToList().Exists(x => x.Title == newNameUpdated));

        }


        [Test]
        public void ShouldAddAnItemToItemSchemeWhenPartialIsFalse()
        {
            ConfigManager.Config.InsertNewItems = true;
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();

            var updateSdmxObject = new SdmxObjectsImpl(DatasetActionEnumType.Replace);
            var codelistObject = _toAdd.Codelists.First(x=>x.Partial == false);
            var mutable = codelistObject.MutableInstance;
            var testingId1 = "testingId1";
            var testingId2 = "testingId2";
            var testingName1 = "Test";
            var testingName2 = "Testing";
            var c1 = new CodeMutableCore() { Id = testingId1 };
            c1.AddName("en", testingName1);
            mutable.AddItem(c1);
            var c2 = new CodeMutableCore() { Id = testingId2 };
            c2.AddName("en", testingName2);
            mutable.AddItem(c2);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            var replaceResult = submitter.SubmitStructures(StoreId, updateSdmxObject);

            Assert.That(replaceResult, Is.Not.Empty);
            Assert.That(replaceResult.All(x => x.Status == ResponseStatus.Success), string.Join("\n", replaceResult.SelectMany(x => x.Messages).SelectMany(m => m.Text).Select(t => t.Value)));

            var container = new RetrievalEngineContainer(new Database(this.GetConnectionStringSettings()));
            var structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                    .SetMaintainableTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow))
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                    .SetStructureIdentification(codelistObject.AsReference)
                    .Build();
            var updatedCodelist = container.CodeListRetrievalEngine.Retrieve(structureQuery).First();

            var newItem1 = updatedCodelist.Items.ToList().First(x => x.Id == testingId1);
            var newItem2 = updatedCodelist.Items.ToList().First(x => x.Id == testingId2);
            Assert.IsNotNull(newItem1);
            Assert.IsNotNull(newItem2);
            Assert.IsTrue(newItem1.Names.ToList().Exists(x => x.Value == testingName1));
            Assert.IsTrue(newItem2.Names.ToList().Exists(x => x.Value == testingName2));


            ////update
            var testingName3 = "Test3";
            var mutableCode = mutable.GetCodeById(testingId2);
            mutableCode.Names.Clear();
            mutableCode.AddName("en", testingName3);

            updateSdmxObject.AddIdentifiable(mutable.ImmutableInstance);
            submitter.SubmitStructures(StoreId, updateSdmxObject);

            updatedCodelist = container.CodeListRetrievalEngine.Retrieve(structureQuery).First();

            newItem1 = updatedCodelist.Items.ToList().First(x => x.Id == testingId1);
            newItem2 = updatedCodelist.Items.ToList().First(x => x.Id == testingId2);

            Assert.IsNotNull(newItem1);
            Assert.IsNotNull(newItem2);
            Assert.IsTrue(newItem1.Names.ToList().Exists(x => x.Value == testingName1));
            Assert.IsTrue(newItem2.Names.ToList().Exists(x => x.Value == testingName3));
        }
    }
}
