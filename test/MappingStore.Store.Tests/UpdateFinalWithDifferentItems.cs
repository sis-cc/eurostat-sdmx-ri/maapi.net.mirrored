// -----------------------------------------------------------------------
// <copyright file="UpdateFinalWithDifferentItems.cs" company="EUROSTAT">
//   Date Created : 2018-2-13
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace MappingStore.Store.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;

    using DryIoc;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Model.Error;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.MappingStore.Store.Factory;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class UpdateFinalWithDifferentItems : TestBase
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(UpdateFinalWithDifferentItems));

        /// <summary>
        /// The current SDMX objects (Workaround to slow test performance of Maria DB on VM when re-initializing)
        /// </summary>
        private ISdmxObjects _currentSdmxObjects;
        /// <summary>
        /// The structure writer manager
        /// </summary>
        private readonly IStructureWriterManager _structureWriterManager = new StructureWriterManager();

        private ISdmxObjects _lue;

        private ISdmxObjects _lel;

        private ISdmxObjects _ent;

        public UpdateFinalWithDifferentItems(string storeId)
            : base(storeId)
        {
            try
            {
                this.InitializeMappingStore(this.StoreId);
                IoCContainer.Register<ConnectionStringFactory>();
                IoCContainer.Register(made: Made.Of(r => ServiceInfo.Of<ConnectionStringFactory>(), m => m.Function));
                IoCContainer.Register<IStructureSubmitFactory, StructureSubmitMappingStoreFactory>();
                IoCContainer.Register<IStructureSubmitter, StructureSubmitter>(reuse: Reuse.Singleton);
                IStructureParsingManager parsingManager = IoCContainer.Resolve<IStructureParsingManager>();
                _lue = new FileInfo("tests/v20/SDMX_20_ESTAT+EGR_CORE_LEU+1.0.xml").GetSdmxObjects(parsingManager);
                _lel = new FileInfo("tests/v20/SDMX_20_ESTAT+EGR_CORE_LEL+1.0.xml").GetSdmxObjects(parsingManager);
                _ent = new FileInfo("tests/v20/SDMX_20_ESTAT+EGR_CORE_ENT+1.0.xml").GetSdmxObjects(parsingManager);
                _currentSdmxObjects = new SdmxObjectsImpl();
                _currentSdmxObjects.Merge(_lel);
                _currentSdmxObjects.Merge(_lue);
                _currentSdmxObjects.Merge(_ent);
                _currentSdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Delete);
            }
            catch (Exception e)
            {
                _log.Error(e.Message, e);
                throw;
            }
        }

        
        [SetUp]
        public void Init()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            submitter.SubmitStructures(this.StoreId, _currentSdmxObjects);
        }

        [TearDown]
        public void Cleanup()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            submitter.SubmitStructures(this.StoreId, _currentSdmxObjects);
        }

        [Test]
        public void ShouldReportExtraConcept()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            var result = submitter.SubmitStructures(this.StoreId, _lel);
            Assert.That(result.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success),  GetMessage(result));
            result = submitter.SubmitStructures(this.StoreId, _lue);
            Assert.That(result.All(x => x.Status != Estat.Sdmxsource.Extension.Constant.ResponseStatus.Failure));
            result = submitter.SubmitStructures(this.StoreId, _ent);
            Assert.That(result.Any(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Failure));
            Assert.That(result.Any(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Warning));
            Assert.That(result.Any(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Failure && x.StructureReference.TargetReference.EnumType == SdmxStructureEnumType.Dsd));
            var dsdFailureForMissingConcept = result.First(
                x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Failure
                     && x.StructureReference.TargetReference.EnumType == SdmxStructureEnumType.Dsd);
            Assert.That(dsdFailureForMissingConcept.Messages, Is.Not.Empty);
            Assert.That(dsdFailureForMissingConcept.Messages[0].ErrorCode.EnumType, Is.EqualTo(SdmxErrorCodeEnumType.NoResultsFound));

            var conceptSchemeNotUpdated = result.First(
                x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Warning
                     && x.StructureReference.TargetReference.EnumType == SdmxStructureEnumType.ConceptScheme);
            Assert.That(conceptSchemeNotUpdated.Messages, Is.Not.Empty);
            Assert.That(conceptSchemeNotUpdated.Messages[0].ErrorCode.EnumType, Is.EqualTo(SdmxErrorCodeEnumType.SemanticError));
        }

        [Test]
        public void ShouldReportMissingConcept()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            var result = submitter.SubmitStructures(this.StoreId, _ent);
            Assert.That(result.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success), GetMessage(result));
            result = submitter.SubmitStructures(this.StoreId, _lue);
            Assert.That(result.Any(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Warning),  GetMessage(result));

            var conceptSchemeNotUpdated = result.First(
                x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Warning
                     && x.StructureReference.TargetReference.EnumType == SdmxStructureEnumType.ConceptScheme);
            Assert.That(conceptSchemeNotUpdated.Messages, Is.Not.Empty);
            Assert.That(conceptSchemeNotUpdated.Messages[0].ErrorCode.EnumType, Is.EqualTo(SdmxErrorCodeEnumType.SemanticError));
        }

        private static string GetMessage(IList<IResponseWithStatusObject> result)
        {
            return string.Join(",", result.Where(o => o.Status != ResponseStatus.Success).SelectMany(o => o.Messages.SelectMany(x => x.Text)).Select(x => x.Value));
        }

        [Test]
        public void ShouldReportMissingConcept2()
        {
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            var result = submitter.SubmitStructures(this.StoreId, _ent);
            Assert.That(result.All(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success));
            result = submitter.SubmitStructures(this.StoreId, _lel);
            Assert.That(result.All(x => x.Status != Estat.Sdmxsource.Extension.Constant.ResponseStatus.Failure));
            result = submitter.SubmitStructures(this.StoreId, _lue);
            Assert.That(result.All(x => x.Status != Estat.Sdmxsource.Extension.Constant.ResponseStatus.Failure));  
        }
    }
}