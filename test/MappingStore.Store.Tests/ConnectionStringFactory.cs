// -----------------------------------------------------------------------
// <copyright file="ConnectionStringFactory.cs" company="EUROSTAT">
//   Date Created : 2018-2-13
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace MappingStore.Store.Tests
{
    using System;
    using System.Configuration;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Manager;

    internal class ConnectionStringFactory
    {
        readonly IConfigurationStoreManager _configurationStoreManager;

        public ConnectionStringFactory(IConfigurationStoreManager configurationStoreManager)
        {
            if (configurationStoreManager == null)
            {
                throw new ArgumentNullException(nameof(configurationStoreManager));
            }

            _configurationStoreManager = configurationStoreManager;
        }
            
        public Func<string, ConnectionStringSettings> Function
        {
            get
            {
                return x => this._configurationStoreManager.GetSettings<ConnectionStringSettings>().First(s => s.Name.Equals(x));
            }
        }
    }
}