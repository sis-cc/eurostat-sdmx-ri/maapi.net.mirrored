// -----------------------------------------------------------------------
// <copyright file="DataMonitorTest.cs" company="EUROSTAT">
//   Date Created : 2017-12-4
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace MappingStore.Store.Tests
{
    using System;
    using System.Configuration;
    using System.Linq;

    using DryIoc;
    using Estat.Sri.Mapping.Api.Manager;

    using NUnit.Framework;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Data.Monitor;
    using Estat.Sri.Data.Registrator;
    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using System.IO;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using System.Net;
    using Estat.Sdmxsource.Extension.Engine.WebService;
    using System.Data.Common;
    using Estat.Sdmxsource.Extension.Model.Error;

    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class DataMonitorTest : TestBase
    {
        private readonly IDatabaseProviderManager _manager;

        public DataMonitorTest(string connectionName) : base(connectionName)
        {
           
            _manager = IoCContainer.Resolve<IDatabaseProviderManager>();
            IoCContainer.Register<ConnectionStringFactory>();
            IoCContainer.Register(made: Made.Of(r => ServiceInfo.Of<ConnectionStringFactory>(), m => m.Function));
            IoCContainer.Register<IStructureSubmitFactory, StructureSubmitMappingStoreFactory>();
            IoCContainer.Register<IStructureSubmitter, StructureSubmitter>(reuse: Reuse.Singleton);
            InitializeMappingStore(this.StoreId);
            var smdxObjects = new FileInfo("tests/v21/DATA_REG_TEST.xml").GetSdmxObjects(IoCContainer.Resolve<IStructureParsingManager>());
          
            var submitter = IoCContainer.Resolve<IStructureSubmitter>();
            var results = submitter.SubmitStructures(this.StoreId, smdxObjects);
            Assume.That(results, 
                Is.All.Matches<IResponseWithStatusObject>(x => x.Status == Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success),
                string.Join('\n', results.Where(x => x.Status != Estat.Sdmxsource.Extension.Constant.ResponseStatus.Success).SelectMany(x=>x.Messages)));
        }

        [Test]
        public void TestMonitor()
        {
            var database = new Database(this.GetConnectionStringSettings());
            var registrationCount = database.ExecuteScalar("select count(*) from N_REGISTRATION", new DbParameter[0]);
            DataRegistrator dataRegistrator = new DataRegistrator(this.StoreId, new SubmitRegistrationEngine(), IoCContainer.Resolve<IStructureSubmitter>(), new DataRegistratorSettings());
            var monitor = new DataMonitor(database, dataRegistrator, new DataMonitorSettings());
            monitor.Monitor();
            var registrationCount2 = database.ExecuteScalar("select count(*) from N_REGISTRATION", new DbParameter[0]);
            Assert.That(registrationCount2, Is.EqualTo(registrationCount));
        }


        [Test,Ignore("Test host no longer available")]
        public void TestMonitorWithUpdate()
        {
            var database = new Database(this.GetConnectionStringSettings());
            database.ExecuteNonQueryFormat("UPDATE DATAFLOW SET DATA_LAST_UPDATE={0}", database.CreateInParameter("lastUpdate", System.Data.DbType.DateTime, DateTime.UtcNow));

            var registrationCount = Convert.ToInt64(database.ExecuteScalar("select count(*) from N_REGISTRATION", new DbParameter[0]));
            DataRegistrator dataRegistrator = new DataRegistrator(this.StoreId, new SubmitRegistrationEngine(), IoCContainer.Resolve<IStructureSubmitter>(), new DataRegistratorSettings());
            var monitor = new DataMonitor(database, dataRegistrator, new DataMonitorSettings());
            monitor.Monitor();
            var registrationCount2 = Convert.ToInt64(database.ExecuteScalar("select count(*) from N_REGISTRATION", new DbParameter[0]));
            Assert.That(registrationCount2, Is.GreaterThan(registrationCount));
        }
        [Test,Ignore("Test host no longer available")]
        public void TestMonitorWithUpdateReplace()
        {
            var database = new Database(this.GetConnectionStringSettings());
            database.ExecuteNonQueryFormat("UPDATE DATAFLOW SET DATA_LAST_UPDATE={0}", database.CreateInParameter("lastUpdate", System.Data.DbType.DateTime, DateTime.UtcNow.AddDays(-1.0)));

            var registrationCount = Convert.ToInt64(database.ExecuteScalar("select count(*) from N_REGISTRATION", new DbParameter[0]));
            DataRegistrator dataRegistrator = new DataRegistrator(this.StoreId, new SubmitRegistrationEngine(), IoCContainer.Resolve<IStructureSubmitter>(), new DataRegistratorSettings());
            var monitor = new DataMonitor(database, dataRegistrator, new DataMonitorSettings());
            monitor.Monitor();
            var registrationCount2 = Convert.ToInt64(database.ExecuteScalar("select count(*) from N_REGISTRATION", new DbParameter[0]));
            Assert.That(registrationCount2, Is.GreaterThan(registrationCount));

            database.ExecuteNonQueryFormat("UPDATE DATAFLOW SET DATA_LAST_UPDATE={0}", database.CreateInParameter("lastUpdate", System.Data.DbType.DateTime, DateTime.UtcNow));
            
            monitor.Monitor();
            var registrationCount3 = Convert.ToInt64(database.ExecuteScalar("select count(*) from N_REGISTRATION", new DbParameter[0]));
            Assert.That(registrationCount2, Is.EqualTo(registrationCount3));
        }

        private class ConnectionStringFactory
        {
            readonly IConfigurationStoreManager _configurationStoreManager;

            public ConnectionStringFactory(IConfigurationStoreManager configurationStoreManager)
            {
                if (configurationStoreManager == null)
                {
                    throw new ArgumentNullException(nameof(configurationStoreManager));
                }

                _configurationStoreManager = configurationStoreManager;
            }

            public Func<string, ConnectionStringSettings> Function
            {
                get
                {
                    return x => this._configurationStoreManager.GetSettings<ConnectionStringSettings>().First(s => s.Name.Equals(x));
                }
            }
        }
    }
}