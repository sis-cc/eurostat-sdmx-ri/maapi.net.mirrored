// -----------------------------------------------------------------------
// <copyright file="TestConcurrency.cs" company="EUROSTAT">
//   Date Created : 2019-01-14
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Threading;
using Estat.Sdmxsource.Extension.Extension;
using Estat.Sri.MappingStore.Store.Manager;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Engine;
using Estat.Sri.MappingStoreRetrieval.Factory;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;

namespace MappingStore.Store.Tests
{
    [TestFixture("odp")]
    [TestFixture("mysql")]
    [TestFixture("sqlserver")]
    public class TestConcurrency : TestBase
    {
        private const int CodeCount = 1000000;
        private readonly ICodelistObject _codelistObject;

        public TestConcurrency(string storeId) : base(storeId)
        {
            InitializeMappingStore(StoreId);
            _codelistObject = CreateBigCodelist();
        }

        private void InsertCodelist()
        {
            IList<ArtefactImportStatus> listOfStatus = new List<ArtefactImportStatus>();
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>("MappingStoreRetrieversFactory");
            var manager = new MappingStoreManager(GetConnectionStringSettings(), listOfStatus);
            manager.SaveStructure(_codelistObject);
        }

        // TODO: see if we need to rewrite any of these tests for msdb 7.0

        //private void TestResult()
        //{
        //    var db = new Database(GetConnectionStringSettings());
        //    var result = db.ExecuteScalar(
        //        "select count(ART_ID) from ARTEFACT where ID='CL_TEST'",
        //        new List<DbParameter>());
        //    var toLong = Convert.ToInt64(result, CultureInfo.InvariantCulture);
        //    Assert.That(toLong, Is.EqualTo(1));

        //    var retrievalEngine = new CodeListRetrievalEngine(db);
        //    var codelist = retrievalEngine.Retrieve(
        //        _codelistObject.AsReference.ToComplex(),
        //        ComplexStructureQueryDetailEnumType.Full).Single();
        //    Assert.That(codelist.Items.Count, Is.EqualTo(CodeCount));
        //}

        private ICodelistObject CreateBigCodelist()
        {
            ICodelistMutableObject codelist = new CodelistMutableCore
                { Id = "CL_TEST", AgencyId = "TEST" };
            codelist.FinalStructure = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);
            codelist.AddName("en", "Test for " + nameof(TestConcurrency));
            for (var i = 0; i < CodeCount; i++)
            {
                ICodeMutableObject code = new CodeMutableCore
                    { Id = "Code" + i.ToString("000000", CultureInfo.InvariantCulture) };
                code.AddName("en", code.Id);
                codelist.AddItem(code);
            }

            return codelist.ImmutableInstance;
        }

        //[Test,Ignore("It takes a long of time")]
        //public void ShouldNotInsertTheSameCodelist()
        //{
        //    var threads = new Queue<Thread>();
        //    for (var i = 0; i < 10; i++)
        //    {
        //        var t = new Thread(InsertCodelist);
        //        threads.Enqueue(t);
        //        t.Start();
        //    }

        //    while (threads.Count > 0)
        //    {
        //        var thread = threads.Dequeue();
        //        thread.Join();
        //    }

        //    TestResult();
        //}
    }
}