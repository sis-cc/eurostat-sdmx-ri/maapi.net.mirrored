﻿// -----------------------------------------------------------------------
// <copyright file="TestDataSourceAndProvisionAgreement.cs" company="EUROSTAT">
//   Date Created : 2017-04-13
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Estat.Sri.MappingStore.Store;
using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStore.Store.Manager;
using Estat.Sri.MappingStore.Store.Model;
using NUnit.Framework;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Util.Io;

namespace MappingStore.Store.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class TestDataSourceAndProvisionAgreement : TestBase
    {
        private readonly List<ConnectionStringSettings> _listOfCss = new List<ConnectionStringSettings>();
        private List<long> _ids = new List<long>();

        public TestDataSourceAndProvisionAgreement(string storeId) : base(storeId)
        {
        }
        [SetUp]
        public void Initialize()
        {
            this.InitializeMappingStore(this.StoreId);
        }

        [Test]
        public void ShouldInsertADataSource()
        {
            var css = GetConnectionStringSettings();
            this._listOfCss.Add(css);
            var dataSourceImportEngine = new DataSourceImportEngine();
            
            QueryableDataSourceType data = new QueryableDataSourceType()
            {
                DataURL = new Uri("http://www.test.org"),
                isRESTDatasource = false,
                isWebServiceDatasource = true,
                WADLURL = new Uri("http://www.test.org"),
                WSDLURL = new Uri("http://www.test.org")
            };
            IDataSource dataSource = new DataSourceCore(data, AgencySchemeCore.CreateDefaultScheme());
            long dataSourceId;
            using (DbTransactionState state = DbTransactionState.Create(css))
            {
                dataSourceId = dataSourceImportEngine.Insert(state, dataSource);
                state.Commit();
            }

            this._ids.Add(dataSourceId);
            Assert.That(dataSourceId, Is.Not.EqualTo(-1));
            Console.WriteLine("inserted row with id {0}", dataSourceId);
        }
    
        [Test]
        public void ShouldInsertAListOfDataSource()
        {
            var css = GetConnectionStringSettings();
            this._listOfCss.Add(css);
            var dataSourceImportEngine = new DataSourceImportEngine();

            var data = new QueryableDataSourceType()
            {
                DataURL = new Uri("http://www.test.org"),
                isRESTDatasource = false,
                isWebServiceDatasource = true,
                WADLURL = new Uri("http://www.test.org"),
                WSDLURL = new Uri("http://www.test.org")
            };
            var data1 = new QueryableDataSourceType()
            {
                DataURL = new Uri("http://www.test1.org"),
                isRESTDatasource = false,
                isWebServiceDatasource = true,
                WADLURL = new Uri("http://www.test1.org"),
                WSDLURL = new Uri("http://www.test1.org")
            };


            var data2 = new QueryableDataSourceType()
            {
                DataURL = new Uri("http://www.test2.org"),
                isRESTDatasource = false,
                isWebServiceDatasource = true,
                WADLURL = new Uri("http://www.test2.org"),
                WSDLURL = new Uri("http://www.test2.org")
            };



            IDataSource dataSource = new DataSourceCore(data, AgencySchemeCore.CreateDefaultScheme());
            IDataSource dataSource1 = new DataSourceCore(data1, AgencySchemeCore.CreateDefaultScheme());
            IDataSource dataSource2 = new DataSourceCore(data2, AgencySchemeCore.CreateDefaultScheme());
            using (DbTransactionState state = DbTransactionState.Create(css))
            {
                this._ids = dataSourceImportEngine.Insert(state, new List<IDataSource>()
                                                                     {
                                                                         dataSource,
                                                                         dataSource1,
                                                                         dataSource2
                                                                     }).ToList();
                state.Commit();
            }
            Assert.That(this._ids.Count(), Is.EqualTo(3));
            Console.WriteLine("inserted {0}", this._ids.Count);
        }

        [Test]
        public void ShouldInsertAProvisionAgreement()
        {
            var css = GetConnectionStringSettings();
            this._listOfCss.Add(css);
           Stopwatch sw = new Stopwatch();
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();
             IStructureParsingManager structureParsingManager = new StructureParsingManager();
            var manager = new MappingStoreManager(css, artefactImportStatuses);
            ISdmxObjects structureObjects;
            using (IReadableDataLocation readable = new FileReadableDataLocation("tests/V21/test-pa.xml"))
            {
                IStructureWorkspace structureWorkspace = structureParsingManager.ParseStructures(readable);
                structureObjects = structureWorkspace.GetStructureObjects(false);
            }

            sw.Start();
            manager.DeleteStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Delete time:" + sw.Elapsed);
            artefactImportStatuses.Clear();
            sw.Restart();
            manager.SaveStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Save time:" + sw.Elapsed);

            Assert.IsNotEmpty(artefactImportStatuses);
            foreach (var artefactImportStatuse in artefactImportStatuses)
            {
                if (artefactImportStatuse.ImportMessage.StructureReference.MaintainableStructureEnumType != SdmxStructureEnumType.Categorisation)
                {
                    Assert.AreEqual(ImportMessageStatus.Success, artefactImportStatuse.ImportMessage.Status, artefactImportStatuse.ImportMessage.Message);
                }
            }

            sw.Start();
            manager.DeleteStructures(structureObjects);
            sw.Stop();
            Trace.WriteLine("Delete2 time:" + sw.Elapsed);
        }
    }
}