// -----------------------------------------------------------------------
// <copyright file="TestSourceGenerator.cs" company="EUROSTAT">
//   Date Created : 2017-07-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStore.Store.Tests
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using NUnit.Framework;

    /// <summary>
    /// The test source generator.
    /// </summary>
    public class TestSourceGenerator : IEnumerable
    {
        /// <summary>
        /// The test files. Stored in a static field for faster resolution by Re-Sharper
        /// </summary>
        private static readonly TestCaseData[] _testFiles = GenerateTestFiles().ToArray();

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator GetEnumerator()
        {
            return _testFiles.GetEnumerator();
        }

        /// <summary>
        /// Generates the test files.
        /// </summary>
        /// <returns>The Test cases</returns>
        private static IEnumerable<TestCaseData> GenerateTestFiles()
        {
            var noargs = new string[0];
            yield return new TestCaseData("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME.xml", noargs);
            yield return new TestCaseData("tests/v20/CENSAGR_CAPOAZ_GEN+IT1+1.3.xml", noargs);
            yield return new TestCaseData("tests/v20/CENSUSHUB+ESTAT+1.1_alllevels.xml", noargs);
            yield return new TestCaseData("tests/v20/CL_SEX_v1.1.xml", noargs);
            yield return new TestCaseData("tests/v20/EGR_1_TS+ESTAT+1.4.xml", noargs);
            yield return new TestCaseData("tests/v20/CS_M_INDEX.xml", noargs);
            yield return new TestCaseData("tests/v20/ESTAT+DEMOGRAPHY+2.1.xml", noargs);
            yield return new TestCaseData("tests/v20/ESTAT+HCL_SAMPLE+2.0.xml", noargs);
            yield return new TestCaseData("tests/v20/ESTAT+HCL_SAMPLE_NZ+2.1.xml", noargs);
            yield return new TestCaseData("tests/v20/ESTAT+STS+2.0.xml", noargs);
            yield return new TestCaseData("tests/v20/ESTAT+TESTLEVELS+1.0.xml", noargs);
            yield return new TestCaseData("tests/v20/ESTAT_CPI_v1.0_fixed.xml", noargs);
            yield return new TestCaseData("tests/v20/QueryStructureResponse.xml", noargs);
            yield return new TestCaseData("tests/v21/ESTAT_STS_3.1.xml", noargs);
            yield return new TestCaseData("tests/v21/sdmxv2.1-ESTAT+STS+2.0.xml", noargs);
            yield return new TestCaseData("tests/v21/sdmxv2.1-CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME.xml", noargs);
            yield return new TestCaseData("tests/v21/sdmxv2.1-ESTAT+HCL_SAMPLE_NZ+2.1.xml", noargs);
            yield return new TestCaseData("tests/v21/sdmxv2.1-ESTAT+HCL_SAMPLE+2.0.xml", noargs);
            yield return new TestCaseData("tests/v21/StructureSet-sdmxv2.1-ESTAT+STS+2.0.xml", noargs);
            yield return new TestCaseData("tests/v21/AGENCIES+ESTAT+1.0.xml", noargs);
            yield return new TestCaseData("tests/v21/TEST_5+ESTAT+1.0.xml", noargs);
            yield return new TestCaseData("tests/v21/DP+ESTAT+1.0.xml", noargs);
            yield return new TestCaseData("tests/v21/DC+ESTAT+1.0.xml", noargs);
            yield return new TestCaseData("tests/v21/CNS_PR+UN+1.1.xml", noargs);
            yield return new TestCaseData("tests/v21/DSD_AGR_POLIND_IT+IT1+2.1.xml", noargs);
            yield return new TestCaseData("tests/v21/CNS_PR+UN+1.1-actual.xml", noargs);
            yield return new TestCaseData("tests/v21/CNS_PR+UN+1.1-allowed.xml", noargs);
            yield return new TestCaseData("tests/v21/CN_DP_DS_MDS.xml", noargs);
            yield return new TestCaseData("tests/v21/CN_SimpleDataSource.xml", noargs);
            yield return new TestCaseData("tests/v21/CNS_PR+UN+1.1-withQueryableSource.xml", noargs);
            yield return new TestCaseData("tests/v21/CL_ECOFIN_INDICATOR small.xml", noargs);
            yield return new TestCaseData("tests/v21/MSDv21-TEST.xml", noargs);
            yield return new TestCaseData("tests/v21/structure.xml", new [] {"tests/v20/structure.xml"}).Ignore("Test is too big for DB running on docker containers");
            yield return new TestCaseData("tests/v21/MSDv21-TEST2.xml", noargs);
            yield return new TestCaseData("tests/v21/MSDv21-TEST2_Presentational.xml", noargs);
            yield return new TestCaseData("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-IsFinal=False.xml", noargs);
            yield return new TestCaseData("tests/v21/MetadataflowWithMSDv21-TEST.xml", noargs);
            yield return new TestCaseData("tests/v21/test-pa.xml", noargs);
            yield return new TestCaseData("tests/v21/SDMX_CN_2009.xml", noargs);
            yield return new TestCaseData("tests/v21/ESTAT+SDMXREG-282+1.1.xml", new[] { "tests/v21/ESTAT+STS_SCHEME+1.0.xml", "tests/v21/ECB+CL_AREA_EE+1.0.xml", "tests/v21/ECB+CL_ADJUSTMENT+1.0.xml", "tests/v21/ESTAT+CL_TIME_FORMAT+1.0.xml", "tests/v21/ECB+CL_DECIMALS+1.0.xml", "tests/v21/ESTAT+CL_AVAILABILITY+1.0.xml", "tests/v21/ECB+CL_OBS_CONF+1.0.xml", "tests/v21/ESTAT+CL_UNIT_MULT+1.0.xml", "tests/v21/ECB+CL_OBS_STATUS+1.0.xml", "tests/v21/ESTAT+CL_STS_INSTITUTION+1.0.xml", "tests/v21/ESTAT+CL_STS_BASE_YEAR+1.0.xml", "tests/v21/ESTAT+CL_STS_ACTIVITY+1.0.xml", "tests/v21/ESTAT+CL_STS_INDICATOR+1.0.xml" });
            yield return new TestCaseData("tests/v21/SDMX21_ESTAT+NATACC_MSD+1.5.xml", noargs);
            yield return new TestCaseData("tests/v21/SDMX21_ESTAT+ESMS_MSD+1.0.xml").Ignore("Syntax error");
            yield return new TestCaseData("tests/v20/ESTAT+ESMS_MSD+1.0_Extended.xml", new[] { "tests/v21/ESTAT+ESTAT_ORGANISATIONS+1.0.xml" });
            yield return new TestCaseData("tests/v20/ESTAT+NATACC_MSD+1.5_Standard.xml", new[] { "tests/v21/ESTAT+ESTAT_ORGANISATIONS+1.0.xml", "tests/v20/ESTAT+ESTAT_DATAFLOWS_SCHEME+1.0_Standard.xml" });
            yield return new TestCaseData("tests/v21/SDMX21_MFD_ESTAT+NATACC_ESQRSNA_A+1.5.xml", new[] { "tests/v21/SDMX21_ESTAT+NATACC_MSD+1.5.xml" });
            yield return new TestCaseData("tests/v21/codelist_stubs.xml", noargs);
            yield return new TestCaseData("tests/v21/dataflow_stubs.xml", noargs);
            yield return new TestCaseData("tests/v21/contentconstraint_stubs.xml", noargs);
            yield return new TestCaseData("tests/v21/ContentConstaintWithValues.xml", noargs);
            yield return new TestCaseData("tests/v21/ConceptSchemeCoreRepresentation.xml", noargs);
            yield return new TestCaseData("tests/v21/OECD-MSD_TEST-1.0-all.xml", noargs);
        }
    }
}