// -----------------------------------------------------------------------
// <copyright file="TestCategoryReference.cs" company="EUROSTAT">
//   Date Created : 2016-09-23
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace MappingStore.Store.Tests
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;

    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Engine;
    using Estat.Sri.MappingStore.Store.Manager;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using System;
    using Estat.Sri.Utils.Config;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Estat.Sri.MappingStore.Store.Extension;

    /// <summary>
    /// The test category reference.
    /// </summary>
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class TestCategoryReference : TestBase
    {
        /// <summary>
        /// The _connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The parser
        /// </summary>
        private readonly IStructureParsingManager _parser = new StructureParsingManager();

        /// <summary>
        /// The map
        /// </summary>
        private readonly Dictionary<string, ISdmxObjects> _map = new Dictionary<string, ISdmxObjects>(StringComparer.Ordinal);

        /// <summary>
        /// Initializes a new instance of the <see cref="TestCategoryReference"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public TestCategoryReference(string connectionName) : base(connectionName)
        {
            this._connectionStringSettings = ConfigurationManager.ConnectionStrings[connectionName];
            var testRecreateCategories = GetSdmxObjects("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-new-cat.xml");
            var catScheme = GetSdmxObjects("tests/v21/CategoryScheme.xml");
            var duplCategory = GetSdmxObjects("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-2cat-dupl-category.xml");
            var twoCategories = GetSdmxObjects("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-2cat.xml");
            var full = GetSdmxObjects("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml");

            this.InitializeMappingStore(this.StoreId);
            var allSdmxObjects = new SdmxObjectsImpl();
            allSdmxObjects.Merge(testRecreateCategories);
            allSdmxObjects.Merge(catScheme);
            allSdmxObjects.Merge(duplCategory);
            allSdmxObjects.Merge(twoCategories);
            allSdmxObjects.Merge(full);

            var categorisations = allSdmxObjects.Categorisations.ToArray();
            foreach (var cat in categorisations)
            {
                allSdmxObjects.RemoveCategorisation(cat);
            }

            var categoryScchemes = allSdmxObjects.CategorySchemes.ToArray();
            foreach (var cat in categoryScchemes)
            {
                allSdmxObjects.RemoveCategoryScheme(cat);
            }

            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);

            manager.SaveStructures(allSdmxObjects);

            Assert.That(artefactImportStatuses.Select(status => status.ImportMessage.Status), Is.All.EqualTo(ImportMessageStatus.Success));
        }

        /// <summary>
        /// Tests re-creating missing categories
        /// </summary>
        /// <param name="file">The file.</param>
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-new-cat.xml")]
        public void TestRecreateCategories(string file)
        {
            var sdmxObjects = _map[file];
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();
            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);

            // delete structure first if any
            manager.DeleteStructures(sdmxObjects);

            artefactImportStatuses.Clear();

            ConfigurationProvider.CreateStubCategory = true;
            // import them again
            manager.SaveStructures(sdmxObjects);
            ConfigurationProvider.CreateStubCategory = false;

            Assert.That(artefactImportStatuses.Select(status => status.ImportMessage.Status), Is.All.EqualTo(ImportMessageStatus.Success));

            ISdmxObjectRetrievalManager retrievalManager = new MappingStoreSdmxObjectRetrievalManager(this._connectionStringSettings);

            var categorisationObjects = Retrieve<ICategorisationMutableObject>(new StructureReferenceImpl(null, SdmxStructureEnumType.Categorisation), ComplexStructureQueryDetailEnumType.Full);
            Assert.That(categorisationObjects, Is.Not.Empty);

            foreach (var categorisationObject in categorisationObjects)
            {
                var original = sdmxObjects.GetCategorisations(categorisationObject.ImmutableInstance.AsReference).First();
                Assert.That(categorisationObject.CategoryReference.FullId, Is.EqualTo(original.CategoryReference.FullId));
                Assert.That(categorisationObject.CategoryReference.ChildReference.ChildReference, Is.Not.Null, categorisationObject.CategoryReference.ToString());
            }

            // delete structures again
            manager.DeleteStructures(sdmxObjects);
        }

        /// <summary>
        /// Tests the import category reference.
        /// </summary>
        /// <param name="file">The file.</param>
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-2cat.xml")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-2cat-dupl-category.xml")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml")]
        [TestCase("tests/v21/CategoryScheme.xml")]
        public void TestImportCategoryReference(string file)
        {
            var sdmxObjects = _map[file];
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);

            // delete structure first if any
            manager.DeleteStructures(sdmxObjects);

            artefactImportStatuses.Clear();

            // import them again
            manager.SaveStructures(sdmxObjects);

            Assert.That(artefactImportStatuses.Select(status => status.ImportMessage.Status), Is.All.EqualTo(ImportMessageStatus.Success));

            // delete structures again
            manager.DeleteStructures(sdmxObjects);
        }

        /// <summary>
        /// Tests the import category reference.
        /// </summary>
        /// <param name="file">The file.</param>
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-2cat.xml")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-2cat-dupl-category.xml")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml")]
        public void TestImportCategoryReferenceWithCategorisation(string file)
        {
            var sdmxObjects = _map[file];
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);

            // delete structure first if any
            manager.DeleteStructures(sdmxObjects);

            artefactImportStatuses.Clear();

            // import them again
            manager.SaveStructures(sdmxObjects);

            Assert.That(artefactImportStatuses.Select(status => status.ImportMessage.Status), Is.All.EqualTo(ImportMessageStatus.Success));

            ISdmxObjectRetrievalManager retrievalManager = new MappingStoreSdmxObjectRetrievalManager(this._connectionStringSettings);

            var categorisationObjects = Retrieve<ICategorisationMutableObject>(new StructureReferenceImpl(null, SdmxStructureEnumType.Categorisation), ComplexStructureQueryDetailEnumType.Full);
            Assert.That(categorisationObjects, Is.Not.Empty);

            foreach (var categorisationObject in categorisationObjects)
            {
                var original = sdmxObjects.GetCategorisations(categorisationObject.ImmutableInstance.AsReference).First();
                Assert.That(categorisationObject.CategoryReference.FullId, Is.EqualTo(original.CategoryReference.FullId));
                Assert.That(categorisationObject.CategoryReference.ChildReference.ChildReference, Is.Not.Null, categorisationObject.CategoryReference.ToString());
            }

            // delete structures again
            manager.DeleteStructures(sdmxObjects);
        }

        /// <summary>
        /// Tests the import category reference.
        /// </summary>
        /// <param name="file">The file.</param>
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-2cat.xml")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full.xml")]
        [TestCase("tests/v21/sdmxv2.1-ESTAT+STS+2.0-Full-2cat-dupl-category.xml")]
        [TestCase("tests/v21/CategoryScheme.xml")]
        public void TestGetCategoryReference(string file)
        {
            var sdmxObjects = _map[file];
            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();

            var manager = new MappingStoreManager(this._connectionStringSettings, artefactImportStatuses);

            // delete structure first if any
            manager.DeleteStructures(sdmxObjects);

            artefactImportStatuses.Clear();

            // import them again
            manager.SaveStructures(sdmxObjects);

            Assert.That(artefactImportStatuses.Select(status => status.ImportMessage.Status), Is.All.EqualTo(ImportMessageStatus.Success));

            MaintainableRefRetrieverEngine retrievalManager = new MaintainableRefRetrieverEngine(new Database(this._connectionStringSettings));

            var categorySchemes = retrievalManager.RetrievesUrnMap(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category));
            Assert.That(categorySchemes, Is.Not.Empty);
            Assert.That(categorySchemes.Keys, Is.All.GreaterThan(0));

            // delete structures again
            manager.DeleteStructures(sdmxObjects);
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>The <see cref="ISdmxObjects"/></returns>
        private ISdmxObjects GetSdmxObjects(string fileName)
        {
            var sdmxObjects = new FileInfo(fileName).GetSdmxObjects(_parser);
            var catOnly = new SdmxObjectsImpl();
            catOnly.AddIdentifiables(sdmxObjects.CategorySchemes);
            catOnly.AddIdentifiables(sdmxObjects.Categorisations);

            _map.Add(fileName, catOnly);
            return sdmxObjects;
        }
    }
}