// -----------------------------------------------------------------------
// <copyright file="TrackChangesTests.cs" company="EUROSTAT">
//   Date Created : 2018-08-17
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using Estat.Sri.Mapping.Api.Model;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    internal class TrackChangesTests
    {
        [Test]
        public void TestIdentifiableEntity()
        {
            IdentifiableEntity entity = new IdentifiableEntity()
            {
                StoreId = "1",
                EntityId = "1",
                Owner = "1",
                ParentId = "1",
                ObjectId = "!"
            };

            var entityObjectId = "other value";
            entity.ObjectId = entityObjectId;
            var entityPatch = entity.PatchRequest.First();
            Assert.That(entityPatch.Op,Is.EqualTo("replace"));
            Assert.That(entityPatch.Path, Is.EqualTo("ObjectId"));
            Assert.That(entityPatch.Value,Is.EqualTo(entityObjectId));
        }

        [Test]
        public void TestColumnDescriptionEntity()
        {
            var entity = new ColumnDescriptionSourceEntity()
            {
                StoreId = "1",
                EntityId = "1",
                Owner = "1",
                ParentId = "1",
                DescriptionField = "1",
                DescriptionTable = "1",
                RelatedField = "1"
            };

            var descriptionField = "description field";
            entity.DescriptionField = descriptionField;
            entity.DescriptionTable = "description table";
            entity.RelatedField = "xxx";
            var entityPatch = entity.PatchRequest.First();
            Assert.That(entityPatch.Op, Is.EqualTo("replace"));
            Assert.That(entityPatch.Path, Is.EqualTo("DescriptionField"));
            Assert.That(entityPatch.Value, Is.EqualTo(descriptionField));
            Assert.That(entity.PatchRequest.Count,Is.EqualTo(3));
        }
    }
}