using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Manager;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Estat.Sri.MappingStoreRetrieval.Factory;
using Estat.Sri.MappingStoreRetrieval.Helper;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;

    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class ConnectionEntityTests
    {
        /// <summary>
        /// The container
        /// </summary>
        private readonly Container _container;

        /// <summary>
        /// The store identifier
        /// </summary>
        private readonly string _storeId;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionEntityTests"/> class.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        public ConnectionEntityTests(string storeId)
        {
            _container =
                  new Container(
                      rules =>
                      rules.With(FactoryMethod.ConstructorWithResolvableArguments)
                          .WithoutThrowOnRegisteringDisposableTransient());
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
            string[] folders = { "Estat.Sri.Plugin.SqlServer", "Estat.Sri.Plugin.Oracle", "Estat.Sri.Plugin.Mysql" };
            List<FileInfo> plugins = new List<FileInfo>();
            foreach (var folder in folders)
            {
                plugins.Add(new FileInfo(string.Format("../../../../../src/{0}/bin/Debug/netstandard2.0/{0}.dll", folder)));
                plugins.Add(new FileInfo(string.Format("../../../../../src/{0}/bin/Debug/netstandard2.0/{0}.pdb", folder)));
            }

            var assemblies = new List<Assembly>();
            foreach (var fileInfo in plugins)
            {
                var destPath = Path.Combine(TestContext.CurrentContext.WorkDirectory, fileInfo.Name);
                if (!File.Exists(destPath) || File.GetLastWriteTime(destPath) < fileInfo.LastWriteTime)
                {
                    fileInfo.CopyTo(destPath, true);
                }

            }
            foreach (var fileInfo in plugins)
            {
                var destPath = Path.Combine(TestContext.CurrentContext.WorkDirectory, fileInfo.Name);
                if (fileInfo.Extension.EndsWith("dll", StringComparison.OrdinalIgnoreCase))
                {
                    var bytes = File.ReadAllBytes(destPath);
                    assemblies.Add(Assembly.Load(bytes));
                }
            }

            assemblies.AddRange(new[] { typeof(IDatabaseProviderManager).Assembly, typeof(DatabaseManager).Assembly });
            _container.RegisterMany(assemblies
               ,
                type => !typeof(IEntity).IsAssignableFrom(type));
            //_container.Register<IEntityAuthorizationManager, StubAuthorizationManager>();
            _container.Unregister<IEntityAuthorizationManager>();
            _container.Register<IStructureParsingManager, StructureParsingManager>(made: Made.Of(() => new StructureParsingManager()));
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>("MappingStoreRetrieversFactory");
            _storeId = storeId;
        }
        [TestCase("oracle_easy_connect", "Data Source=localhost/xe;User ID=MASTORE;Password=123", "Oracle.ManagedDataAccess.Client" )]
        [TestCase("oracle_tnalias", "Data Source=myserver;User ID=MASTORE;Password=123", "Oracle.ManagedDataAccess.Client" )]
        [TestCase("oracle_connectiondesc", "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=MyHost)(PORT=MyPort))(CONNECT_DATA=(SERVICE_NAME=MyOracleSID)));User ID=MASTORE;Password=123", "Oracle.ManagedDataAccess.Client" )]
        [TestCase("sqlserver", "Data Source=sodi-test,1433;Initial Catalog=madb-latest;User Id=mauser;Password=123", "System.Data.SqlClient" )]
        [TestCase("mariadbwithport", "server=localhost:3307;user id=mauser;password=123;database=mastore;default command timeout=120", "MySql.Data.MySqlClient")]
        [TestCase("mariadb", "server=localhost;user id=mauser;password=123;database=MASTORE;default command timeout=120", "MySql.Data.MySqlClient" )]
        public void TestPersistConnectionEntityUpdate(string name, string connectionString, string providerName)
        {
            // Before doing anything we need to resolve the Manager instances that will be used
            // First we resolve the persistance manager
            IEntityPersistenceManager persistManager = _container.Resolve<IEntityPersistenceManager>();

            // Then the retrieval manager
            IEntityRetrieverManager retrievalManager = _container.Resolve<IEntityRetrieverManager>();

            // finally the DB PRovider specific manager
            IDatabaseProviderManager dbProviderManager = _container.Resolve<IDatabaseProviderManager>();

            // The input connection setting, a Connection String pointing to a Dissemination database
            var ddbConnectionString = new System.Configuration.ConnectionStringSettings(name, connectionString, providerName);

            // Below we start interacting with the Mapping Assistant API
            // The goals are :
            // * Add the ddbConnectionString to the Mapping Store Database
            // * Retrieve it

            // The Connection settings are special, in the sense that we need the corresponding Database Provide plugin assistance to 
            // convert them to various forms and to be able to store them
            // First we need to get the Database Provider specific engine using the dbProviderManager and the ddbConnectionString ProviderName
            IDatabaseProviderEngine dbPluginEngine = dbProviderManager.GetEngineByProvider(ddbConnectionString.ProviderName);

            // Then we use the engine from above to convert the input ddbConnectionString to IConnectionEntity
            IConnectionEntity ddbSettings = dbPluginEngine.SettingsBuilder.CreateConnectionEntity(ddbConnectionString);
            
            // we need to say to which store should we store this entity
            ddbSettings.StoreId = this._storeId;
            
            // Now we need to take those ddbSettings and save them into mapping store database
            // And we use this manager to Add the ddbSettings to the Mapping Store database
            var addedEntity = persistManager.Add(ddbSettings);
            
            // Next task is to retrieve the DDB Connection settings from Mapping Store database
            // again the pattern here is to get first the engine from the retrievalManager
            IEntityRetrieverEngine<IConnectionEntity> retrievalEngine = retrievalManager.GetRetrieverEngine<IConnectionEntity>(this._storeId);

            // And then use the retrieval engine to get the entities
            // The engine needs to parameters
            // The entity query
            IEntityQuery entityQuery = new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, addedEntity.EntityId)};

            // The detail parameter
            Detail detail = Detail.Full;

            // So we pass those parameters to the retrievalEngine and get the DDB setting entities from the Mapping Store DDB
            IEnumerable<IConnectionEntity> ddbSettingsEntities = retrievalEngine.GetEntities(entityQuery, detail);

            // Go through each entry, print it out and then update it
            foreach (IConnectionEntity entity in ddbSettingsEntities)
            {
                var csstring = dbPluginEngine.SettingsBuilder.CreateConnectionString(entity);
                Console.WriteLine("Name: '{0}', Provider: '{1}', ConnectionString: '{2}", csstring.Name, csstring.ProviderName, csstring.ConnectionString);

                // Comment the following line if you wish store the DDB settings inside the file
                var userSetting = entity.Settings["user id"];
                userSetting.Value = "test123";
                persistManager.Add(entity);
            }

            // So we pass those parameters to the retrievalEngine and get the DDB setting entities from the Mapping Store DDB
            ddbSettingsEntities = retrievalEngine.GetEntities(entityQuery, detail);

            // Go through each entry, print it out and then delete it
            foreach (IConnectionEntity entity in ddbSettingsEntities)
            {
                var csstring = dbPluginEngine.SettingsBuilder.CreateConnectionString(entity);
                Console.WriteLine("Name: '{0}', Provider: '{1}', ConnectionString: '{2}", csstring.Name, csstring.ProviderName, csstring.ConnectionString);

                // Comment the following line if you wish store the DDB settings inside the file
                persistManager.Delete<IConnectionEntity>(entity.StoreId, entity.EntityId);
            }
        }
        [Test]
        public void TestPersistConnectionEntity()
        {
            // Before doing anything we need to resolve the Manager instances that will be used
            // First we resolve the persistance manager
            IEntityPersistenceManager persistManager = _container.Resolve<IEntityPersistenceManager>();

            // Then the retrieval manager
            IEntityRetrieverManager retrievalManager = _container.Resolve<IEntityRetrieverManager>();

            // finally the DB PRovider specific manager
            IDatabaseProviderManager dbProviderManager = _container.Resolve<IDatabaseProviderManager>();

            // The input connection setting, a Connection String pointing to a Dissemination database
            var ddbConnectionString = new System.Configuration.ConnectionStringSettings("test", "Server=localhost;Database=H2;Uid=testuser;Pwd=123;", "MySql.Data.MySqlClient");

            // Below we start interacting with the Mapping Assistant API
            // The goals are :
            // * Add the ddbConnectionString to the Mapping Store Database
            // * Retrieve it

            // The Connection settings are special, in the sense that we need the corresponding Database Provide plugin assistance to 
            // convert them to various forms and to be able to store them
            // First we need to get the Database Provider specific engine using the dbProviderManager and the ddbConnectionString ProviderName
            IDatabaseProviderEngine dbPluginEngine = dbProviderManager.GetEngineByProvider(ddbConnectionString.ProviderName);

            // Then we use the engine from above to convert the input ddbConnectionString to IConnectionEntity
            IConnectionEntity ddbSettings = dbPluginEngine.SettingsBuilder.CreateConnectionEntity(ddbConnectionString);

            // set the Mapping Store we want to save this
            ddbSettings.StoreId = this._storeId;

            // Now we need to take those ddbSettings and save them into mapping store database
            // So we need to get the persistence engine from the persistManager for IConnectionEntity type and mapping store identifier : mastore
            var addedEntity = persistManager.Add(ddbSettings);

            // Next task is to retrieve the DDB Connection settings from Mapping Store database
            // again the pattern here is to get first the engine from the retrievalManager
            IEntityRetrieverEngine<IConnectionEntity> retrievalEngine = retrievalManager.GetRetrieverEngine<IConnectionEntity>(this._storeId);

            // And then use the retrieval engine to get the entities
            // The engine needs to parameters
            // The entity query
            EntityQuery entityQuery = new EntityQuery();

            // we add the added entity ID to the query
            entityQuery.EntityId = new Criteria<string>(OperatorType.Exact, addedEntity.EntityId);

            // The detail parameter
            Detail detail = Detail.Full;

            // So we pass those parameters to the retrievalEngine and get the DDB setting entities from the Mapping Store DDB
            IEnumerable<IConnectionEntity> ddbSettingsEntities = retrievalEngine.GetEntities(entityQuery, detail);

            // Go through each entry, print it out and then delete it
            foreach (IConnectionEntity entity in ddbSettingsEntities)
            {
                IDatabaseProviderEngine currentdbPluginEngine = dbProviderManager.GetEngineByType(entity.DatabaseVendorType);
                var csstring = currentdbPluginEngine.SettingsBuilder.CreateConnectionString(entity);
                Console.WriteLine("Name: '{0}', Provider: '{1}', ConnectionString: '{2}", csstring.Name, csstring.ProviderName, csstring.ConnectionString);

                // Comment the following line if you wish store the DDB settings inside the file
                persistManager.Delete<IConnectionEntity>(this._storeId,  entity.EntityId);
            }
        }
    }
}
