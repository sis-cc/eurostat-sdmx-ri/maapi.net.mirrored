using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    class TemplateMappingRetrieverTests
    {
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();


        [Test]
        public void RetrieveTemplateMappings()
        {
            ConnectionStringSettings css = this._connectionStringHelper.GetConnectionStringSettings("sqlserver");

            var database = new Database(css);
            var retriever = new TemplateMappingRetriever(database);
            var entities = retriever.GetEntities(new EntityQuery(),Api.Constant.Detail.Full);
            Assert.AreEqual(entities.Count(), 2);
            Assert.AreEqual(entities.First().Transcodings.Count, 2);
            Assert.AreEqual(entities.First().TimeTranscoding.Count, 2);
        }
    }
}
