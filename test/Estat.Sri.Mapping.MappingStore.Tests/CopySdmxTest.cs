﻿// -----------------------------------------------------------------------
// <copyright file="CopySdmxTest.cs" company="EUROSTAT">
//   Date Created : 2017-09-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;

    using Estat.Sri.MappingStore.Store.Manager;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Persist;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    public class CopySdmxTest
    {

        public void CopySdmx(Database sourceMappingStore, ConnectionStringSettings targetMappingStore, Uri dataflowUrn)
        {
            // we convert the urn to IStructureReference
            var dataflowReference = new StructureReferenceImpl(dataflowUrn);

            // then check if the dataflow already exists
            ISdmxObjectRetrievalManager targetRetrievalManager = GetRetrievalManager(new Database(targetMappingStore));
            var targetExists = targetRetrievalManager.GetMaintainableObject<IDataflowObject>(dataflowReference, true, false);
            if (targetExists != null)
            {
                // we don't need to copy. it exists
                return;
            }

            // since it doesn't exist we get the dataflow and its descendants
            ISdmxObjectRetrievalManager retrievalManager = GetRetrievalManager(sourceMappingStore);

            // so first we build the query
            IRestStructureQuery structureQuery = new RESTStructureQueryCore(
                StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full),
                StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.Descendants), 
                null,
                dataflowReference,
                false);

            // and the we retrieve
            var sdmxOjects = retrievalManager.GetMaintainables(structureQuery);
            
            // finally we store the sdmxObjects in the target DB
            IStructurePersistenceManager persistenceManager = GetPersistManager(targetMappingStore);
            persistenceManager.SaveStructures(sdmxOjects);

        }

        private IStructurePersistenceManager GetPersistManager(ConnectionStringSettings destMappingStore)
        {
            return new MappingStoreManager(destMappingStore, new List<ArtefactImportStatus>());
            
        }

        public ISdmxObjectRetrievalManager GetRetrievalManager(Database database)
        {
            var retrievalEngineContainer = new RetrievalEngineContainer(database);
            ISdmxMutableObjectRetrievalManager mutableObjectRetrievalManager = new MappingStoreRetrievalManager(retrievalEngineContainer);
             return new MutableWrapperSdmxObjectRetrievalManager(mutableObjectRetrievalManager, null);
        }
    }
}