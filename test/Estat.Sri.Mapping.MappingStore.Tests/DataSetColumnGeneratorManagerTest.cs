﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DryIoc;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Builder;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    class DataSetColumnGeneratorManagerTest : BaseTestClassWithContainer
    {

        [Test]
        public void RetrivedatasetColumnsEntitys()
        {

            List<string> columnNames = new List<string>
            {
                "ATECO_2007",
                "CL_IMPORTO1",
                "AREEAMB",
                "ITTER107",
                "CLLVT",
                "TIPO_DATO29",
                "TIME_PERIOD",
                "OBS_VALUE",
                "FLAGS"
            };

            IEntityRetrieverManager entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            ILocalCodeRetrieverManager localCodeRetrieverManager = this.IoCContainer.Resolve<ILocalCodeRetrieverManager>();
            IBuilder<Database, DdbConnectionEntity> databaseBuilder = this.IoCContainer.Resolve<IBuilder<Database, DdbConnectionEntity>>();

            DataSetColumnGeneratorManager dataSetColumnGeneratorManager = new DataSetColumnGeneratorManager(entityRetrieverManager, databaseBuilder, localCodeRetrieverManager);

            var columns = dataSetColumnGeneratorManager.GetDatasetColumns("sqlserver", "2");

            foreach(var column in columns)
            {
                Assert.That(columnNames.Contains(column.Name));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTestClassWithContainer"/> class.
        /// </summary>
        public DataSetColumnGeneratorManagerTest()
            : base("sqlserver")
        {
        }

    }
}
