using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStore.Store.Extension;
using NSubstitute;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    public class PersistenceTests
    {

        //1. The user changes the name of a Mapping Set with id “3” and clicks at Save button

        //2. The Mapping Assistant Web Application will detect that only name has changed and generate a JSON Patch with the following contents in JSON format:

        //{ “op” : “name”, “path”: “/name”, “value” : “new name” }

        //3. The Mapping Assistant Web Application issues a HTTP PATCH with the above contents for the resource URL : “/store/dev/entity/mappingset/3”

        //4. The Mapping Assistant Web Service receives at its method the entity type “MappingSet”, entity id “3” and the class representing a JSON Patch in C#/Java, e.g. JsonPatch.

        //5. The Mapping Assistant Web Service converts the JsonPatch to a model class representing an update, e.g. UpdateInfo together with entity type and ID. The UpdateInfo it is passed to the Mapping Assistant API EntityPersistenceManager implementation.

        //6. The EntityPersistenceManager locates the EntityPersistenceFactory implementation for the MappingSet and then uses it to create or get the EntityPersistenceEngine for the MappingSet. Then passes the UpdateInfo to the EntityPersistenceEngine.

        //7. The EntityPersistenceEngine uses internal class to generate the SQL statement(s) for the UpdateInfo object. In this case it will generate the following SQL statement

        //UPDATE MAPPING_SET SET NAME={name_param} where MAP_SET_ID={key_param}

        //8. Another class will set the parameters e.g. using DbParameter in .NET and execute the statement.

        //9. Depending on the results the method will not return or will throw an exception.
        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldUpdateAMappingSetName(string mappingStoreIdentifier)
        {
            PatchRequest patch = new PatchRequest() { new PatchDocument("replace", "name", "new value") };
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            ConnectionStringRetriever connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            entityPersistenceEngine.Update(patch,EntityType.MappingSet,"3");
        }

        //{ “op” : “remove”, “path”: “/categories/urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:CH(1.0).CH_1” },
        //{ “op” : “replace”, “path”: “/name”, “value” : “new name” },
        //{ “op” : “add”, “path”: “/dataflows”, “value” : “urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:CENSUSHUB_Q_XS1(1.0)” }
        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldUpdateDdbConnectionSettings(string mappingStoreIdentifier)
        {
            var patch = new PatchRequest()
            {
                new PatchDocument("remove", "/categoryId/urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:CH(1.0).CH_1"),
                new PatchDocument("replace", "/" + nameof(DdbConnectionEntity.Name),"new name3"),
                new PatchDocument("add", "/dataflowId","urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:CENSUSHUB_Q_XS1(1.0)")
            };
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            entityPersistenceEngine.Update(patch, EntityType.DdbConnectionSettings, "3");

        }


        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldUpdateDataSetColumn(string mappingStoreIdentifier)
        {
            var patch = new PatchRequest()
            {
                new PatchDocument("replace", "/name", "new name3"),
                new PatchDocument("remove", "/description")
            };
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            entityPersistenceEngine.Update(patch, EntityType.DataSetColumn, "3");
        }


        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldUpdateDataSet(string mappingStoreIdentifier)
        {
            var patch = new PatchRequest()
            {
                new PatchDocument("remove", "/categoryId/urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:CH(1.0).CH_1"),
                new PatchDocument("replace", "/name","new name3"),
                new PatchDocument("add", "/dataflowId","urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:CENSUSHUB_Q_XS1(1.0)")
            };
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            entityPersistenceEngine.Update(patch, EntityType.DataSet, "3");
        }


        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldUpdateLocalCode(string mappingStoreIdentifier)
        {
            var patch = new PatchRequest()
            {
                new PatchDocument("replace", "/id","new value"),
                new PatchDocument("replace", "/" + nameof(LocalCodeEntity.ParentId),"6")
            };
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            entityPersistenceEngine.Update(patch, EntityType.LocalCode, "19553");
        }


        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldUpdateMapping(string mappingStoreIdentifier)
        {
            var patch = new PatchRequest()
            {
                new PatchDocument("remove", "/columnId"),
                new PatchDocument("replace", "/type","B"),
                new PatchDocument("add", "/componentId","6")
            };
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            entityPersistenceEngine.Update(patch, EntityType.Mapping, "1");
        }

        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldUpdateATranscoding(string mappingStoreIdentifier)
        {
            var patch = new PatchRequest()
            {
                new PatchDocument("replace", "/expression","myExpression"),
                new PatchDocument("replace", "/time_transcoding/data_column","7"),
                new PatchDocument("replace", "/time_transcoding/frequency","HalfOfYear"),
                new PatchDocument("replace", "/time_transcoding/year/start","3"),
                new PatchDocument("replace", "/time_transcoding/year/column","11"),
                new PatchDocument("replace", "/time_transcoding/period/column","12"),
                new PatchDocument("replace", "/time_transcoding/period/length","2"),
                new PatchDocument("replace", "/time_transcoding/period/items","{\"code\":{\"entityId\" : \"1\"},\"local_codes\":{\"entityId\":\"2\"}"),
                new PatchDocument("add", "/script","{\"name\" : \"myScriptName\",\"content\" : \"myScriptContent\"} ")
            };
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPersistenceEngine = new TranscodingPersistenceEngine(new DatabaseManager(configurationStoreManager), mappingStoreIdentifier);
            entityPersistenceEngine.Update(patch, EntityType.Transcoding, "187");
        }


        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldInsertComponentMapping(string mappingStoreIdentifier)
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            var entity = new ComponentMappingEntity()
            {
               ParentId = "1",
               Type = "my type",
               ConstantValue = "constant"
            };
            entityPersistenceEngine.Add(entity);
        }


        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldInsertDescSource(string mappingStoreIdentifier)
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            var entity = new ColumnDescriptionSourceEntity()
            {
               DescriptionTable = "my table",
               RelatedField = "my related field",
               DescriptionField = "my description field",
               ParentId = "23"
            };
            entityPersistenceEngine.Add(entity);
        }


        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldInsertDataset(string mappingStoreIdentifier)
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            var entity = new DatasetEntity()
            {
               Name = "some name",
               Query = "some query",
               OrderByClause = "some order by clause", 
               ParentId = "3",
               Description = "some description",
               JSONQuery = "some xml query",
               EditorType = "some editor type",
            };
            entityPersistenceEngine.Add(entity);
        }


        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldInsertDatasetColumn(string mappingStoreIdentifier)
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            var entity = new DataSetColumnEntity()
            {
                Name = "some name",
                ParentId = "3",
                Description = "some description",
                LastRetrieval = DateTime.Now,
                Owner = "some owner",
            };
            entityPersistenceEngine.Add(entity);
        }

        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldInsertDatabaseConnection(string mappingStoreIdentifier)
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            var entity = new DdbConnectionEntity()
            {
                Name = "some name",
                AdoConnString = "ado conn",
                DbType = "SqlServer",
                DbUser = "ddb",
                Password = "my pass"
            };
            entityPersistenceEngine.Add(entity);
        }



        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldInsertATranscoding(string mappingStoreIdentifier)
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPersistenceEngine = new TranscodingPersistenceEngine(new DatabaseManager(configurationStoreManager), mappingStoreIdentifier);
            var entity = new TranscodingEntity()
            {
                Expression = "some expression",
                ParentId = "1",
                Script = new List<TranscodingScriptEntity>()
                {
                    new TranscodingScriptEntity()
                    {
                        ScriptContent = "script",
                        ScriptTile = "title"
                    }
                },
                TimeTranscoding = new List<TimeTranscodingEntity>() { new TimeTranscodingEntity()
                {
                   DateColumn = new DataSetColumnEntity() { EntityId = "1"},
                   Frequency = "HalfOfYear",
                   Period = new PeriodTimeTranscoding()
                   {
                       Length = 1,
                       Start = 0,
                      Column = new DataSetColumnEntity() { EntityId = "3"}
                   },
                   Year = new TimeTranscoding()
                   {
                       Column = new DataSetColumnEntity() { EntityId = "5"},
                       Length = 5,
                       Start = 1
                   }
                }
            }};
            entityPersistenceEngine.Add(entity);
        }

        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldDeleteDdbConnectionSettings(string mappingStoreIdentifier)
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            entityPersistenceEngine.Delete("19",EntityType.DdbConnectionSettings);
        }


        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldUpdateADescSource(string mappingStoreIdentifier)
        {
            var patch = new PatchRequest()
            {
                new PatchDocument("replace", "/" + nameof(ColumnDescriptionSourceEntity.DescriptionField),"B"),
            };
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            entityPersistenceEngine.Update(patch, EntityType.DescSource, "1");
        }

        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void UpdateAHeaderTemplate(string mappingStoreIdentifier)
        {
            var patch = new PatchRequest()
            {
                new PatchDocument("replace", "/name", "Transmission Name"),
                new PatchDocument("replace", "/dataSetAgencyId", "1"),
                new PatchDocument("replace", "/source", "Source"),
                new PatchDocument("replace", "/test", "0"),
                new PatchDocument("replace", "/sender/name", "my sender"),
                new PatchDocument("replace", "/sender/id", "my sender id"),
                new PatchDocument("replace", "/sender/contact/name", "contact name"),
                new PatchDocument("replace", "/sender/contact/department", "department name"),
                new PatchDocument("replace", "/sender/contact/role", "role"),
                new PatchDocument("replace", "/sender/contact/email", "email"),
                new PatchDocument("replace", "/receiver/10/contact/name","receiver name"),
            };

            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] {connectionStringSettings});
            var databaseManager = new DatabaseManager(configurationStoreManager);
            var entityPersistenceEngine = new HeaderTemplatePersistenceEngine<HeaderEntity>(databaseManager,mappingStoreIdentifier,new CommandsFromUpdateInfo(), new EntityIdFromUrn(databaseManager.GetDatabase(mappingStoreIdentifier)));
            entityPersistenceEngine.Update(patch, EntityType.Header, "2");
        }

        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void UpdateAHeaderTemplateAndInsertParty(string mappingStoreIdentifier)
        {
            var patch = new PatchRequest()
            {
                new PatchDocument("replace", "/name", "Transmission Name"),
                new PatchDocument("replace", "/dataSetAgencyId", "1"),
                new PatchDocument("replace", "/source", "Source"),
                new PatchDocument("replace", "/test", "0"),
                new PatchDocument("add", "/sender/name", "my sender"),
                new PatchDocument("add", "/sender/id", "my sender id"),
                new PatchDocument("add", "/sender/contact/name", "contact name"),
                new PatchDocument("add", "/sender/contact/department", "department name"),
                new PatchDocument("add", "/sender/contact/role", "role"),
                new PatchDocument("add", "/sender/contact/email", "email"),
                new PatchDocument("remove", "/receiver/3"),
            };

            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { connectionStringSettings });
            var databaseManager = new DatabaseManager(configurationStoreManager);
            var entityPersistenceEngine = new HeaderTemplatePersistenceEngine<HeaderEntity>(databaseManager, mappingStoreIdentifier, new CommandsFromUpdateInfo(), new EntityIdFromUrn(databaseManager.GetDatabase(mappingStoreIdentifier)));
            entityPersistenceEngine.Update(patch, EntityType.Header, "2");
        }

        [TearDown]
        public void TearDown()
        {
            foreach (var mappingStoreIdentifier in new string[] { "odp" , "mysql", "sqlserver" })
            {
                var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
                var connectionStringHelper = new ConnectionStringRetriever();
                var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
                configurationStoreManager.GetSettings<ConnectionStringSettings>()
                   .Returns(new[] { connectionStringSettings });

                var database = new DatabaseManager(configurationStoreManager).GetDatabase(mappingStoreIdentifier);
                var commands = new List<CommandDefinition>
                {
                    new CommandDefinition("Delete from DATAFLOW_DATASET where DF_ID = 20571"), new CommandDefinition("Delete from DATAFLOW_DB_CONNECTION where DF_ID = 20571"),
                    new CommandDefinition("Delete from COM_COL_MAPPING_COMPONENT where MAP_ID=1 and COMP_ID=6")
                };
                database.ExecuteCommands(commands.ToArray());
            }
            
        }
    }
}
