using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Plugin.SqlServer.Builder;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    class SqlServerConnectionBuilderTests
    {
        SqlServerConnectionBuilder builder = new SqlServerConnectionBuilder("SqlServer");
        

        [Test]
        public void CreateDdbConnectionWithInputPortAsInt()
        {
            IConnectionEntity connection = new ConnectionEntity()
            {
                Name = "Test",
                DbName = "TestDissemination",
                DatabaseVendorType = "sqlserver"
            };
            connection.Settings.Add("Server", new ConnectionParameterEntity() { Value = "localhost" });
            connection.Settings.Add("Port", new ConnectionParameterEntity() { Value = 1134});
            var ddbConnectionEntity = builder.CreateDdbConnectionEntity(connection);
            Assert.That(ddbConnectionEntity.JdbcConnString.Contains("1134"), Is.True);
        }

        [Test]
        public void CreateDdbConnectionWithInputPortAsString()
        {
            IConnectionEntity connection = new ConnectionEntity()
            {
                Name = "Test",
                DbName = "TestDissemination",
                DatabaseVendorType = "sqlserver"
            };
            connection.Settings.Add("Server", new ConnectionParameterEntity() { Value = "localhost" });
            connection.Settings.Add("Port", new ConnectionParameterEntity() { Value = "1134" });
            var ddbConnectionEntity = builder.CreateDdbConnectionEntity(connection);
            Assert.That(ddbConnectionEntity.JdbcConnString.Contains("1134"), Is.True);
        }

        [Test]
        public void CreateDdbConnectionWithInputPortNull()
        {
            IConnectionEntity connection = new ConnectionEntity()
            {
                Name = "Test",
                DbName = "TestDissemination",
                DatabaseVendorType = "sqlserver"
            };
            connection.Settings.Add("Server", new ConnectionParameterEntity() { Value = "localhost" });
            connection.Settings.Add("Port", new ConnectionParameterEntity() { Value = null });
            var ddbConnectionEntity = builder.CreateDdbConnectionEntity(connection);
            Assert.That(ddbConnectionEntity.JdbcConnString.Contains("1134"), Is.False);
        }
    }
}
