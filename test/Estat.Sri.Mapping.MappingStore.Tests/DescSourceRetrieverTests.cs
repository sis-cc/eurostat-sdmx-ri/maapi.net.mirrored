using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Config;
using NSubstitute;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class DescSourceRetrieverTests
    {
        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        private readonly ConnectionStringSettings _connectionStringSettings;

        public DescSourceRetrieverTests(string name)
        {
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(name);
        }

        [Test]
        public void GetAllDescSources()
        {
            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DescSourceRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<ColumnDescriptionSourceEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.AnyValue, null));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
        }

        [Test]
        public void GetADescSource()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DescSourceRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<ColumnDescriptionSourceEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.Exact, "1"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
        }


        [Test]
        public void GetADescSourceBasedOnParentId()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DescSourceRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<ColumnDescriptionSourceEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.ParentId.Returns(new Criteria<string>(OperatorType.Exact, "4"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
        }

        [Test]
        public void GetAHeaderBasedOnParentIdExclusion()
        {
            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DescSourceRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<ColumnDescriptionSourceEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.ParentId.Returns(new Criteria<string>(OperatorType.Not | OperatorType.Contains, "2"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
        }
    }
}