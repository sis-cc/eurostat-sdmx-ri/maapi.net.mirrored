// -----------------------------------------------------------------------
// <copyright file="TimeTranscodingPersistTest.cs" company="EUROSTAT">
//   Date Created : 2017-04-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Estat.Sri.Mapping.Api.Utils;

    /// <summary>
    /// The transcoding rule persist test.
    /// </summary>
    [TestFixture("msdb_scratch.oracle")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.sqlserver")]
    public class TimeTranscodingPersistTest : BaseTestClassWithContainer
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(TimeTranscodingPersistTest));

        /// <summary>
        /// The connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The retriever manager
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;

        /// <summary>
        /// The persist manager
        /// </summary>
        private readonly IEntityPersistenceManager _persistManager;

        /// <summary>
        /// The retrieval manager
        /// </summary>
        private readonly IRetrievalEngineContainerFactory _retrievalManager;

        private MappingSetEntity _mappingSetEntity;
        private DataSetColumnEntity[] _datasetColumns;

        /// <summary>
        /// Initializes a new instance of the <see cref="TranscodingRulePersistTest"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public TimeTranscodingPersistTest(string connectionName) : base(connectionName)
        {
          
            this._retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            this._persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
              _connectionStringSettings = GetConnectionStringSettings();
            _retrievalManager = this.IoCContainer.Resolve<IRetrievalEngineContainerFactory>();
        }

        [OneTimeSetUp]
        public void ReBuildMappingStore()
        {
            try
            {
                InitializeMappingStore(this.StoreId);
                var dataflow = BuildStructuralMetadata();
                var connection = AddADdbConnection();
                var dataset = AddDataSet(connection, dataflow.DataStructureRef, 0);
                this._mappingSetEntity = AddSourceMappingSet(dataflow, dataset.EntityId);
            }
            catch (Exception e)
            {
                _log.Error(e);
                Console.WriteLine(e);
                throw;
            }
        }

        [Test]
        public void ShouldTimeTranscodingAnnual()
        {
            var transcodingEntity = new TimeDimensionMappingEntity();
            transcodingEntity.ParentId = _mappingSetEntity.EntityId;
            transcodingEntity.StoreId = _mappingSetEntity.StoreId;

            TimeTranscodingEntity timeTranscodingEntity = new TimeTranscodingEntity();
            timeTranscodingEntity.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.Year).FrequencyCode;
            timeTranscodingEntity.IsDateTime = false;
            timeTranscodingEntity.Year = new TimeTranscoding() { Column = GetColumns().First(), Length = 4, Start = 0};
            var timeTranscodings = new List<TimeTranscodingEntity>();
            timeTranscodings.Add(timeTranscodingEntity);
            transcodingEntity.ConvertFromTranscoding(timeTranscodings, "FREQ");
            var addedTranscodingEntity = _persistManager.Add(transcodingEntity);
 
            Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
            var retrievedEntity = _retrieverManager.GetEntities<TimeDimensionMappingEntity>(StoreId, _mappingSetEntity.QueryForThisEntityId(), Detail.Full).First();

            Assert.That(retrievedEntity.EntityId, Is.EqualTo(addedTranscodingEntity.EntityId));
            Assert.That(retrievedEntity.ParentId, Is.EqualTo(addedTranscodingEntity.ParentId));
            Assert.That(retrievedEntity.Transcoding, Is.Not.Null);
            Assert.That(retrievedEntity.Transcoding.IsFrequencyDimension, Is.True);
            Assert.That(retrievedEntity.Transcoding.TimeFormatConfigurations, Has.Exactly(1).Items);
            var retrievedTranscodings = TimeTranscodingConversionHelper.Convert(retrievedEntity.Transcoding);
            TimeTranscodingEntity retrievedAnnualTimeTranscoding = retrievedTranscodings.First();
            Assert.That(retrievedAnnualTimeTranscoding.Frequency, Is.EqualTo(timeTranscodingEntity.Frequency));
            Assert.That(retrievedAnnualTimeTranscoding.Year, Is.Not.Null);
            Assert.That(retrievedAnnualTimeTranscoding.Period, Is.Null);
            Assert.That(retrievedAnnualTimeTranscoding.Year.Length, Is.EqualTo(timeTranscodingEntity.Year.Length));
            Assert.That(retrievedAnnualTimeTranscoding.Year.Start, Is.EqualTo(timeTranscodingEntity.Year.Start));
            AssertDelete();
        }

        [Test]
        public void ShouldTimeTranscodingMonthly1Column()
        {
            var transcodingEntity = new TimeDimensionMappingEntity();
            transcodingEntity.ParentId = _mappingSetEntity.EntityId;
            transcodingEntity.StoreId = _mappingSetEntity.StoreId;

            TimeTranscodingEntity timeTranscodingEntity = new TimeTranscodingEntity();
            timeTranscodingEntity.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.Month).FrequencyCode;
            timeTranscodingEntity.IsDateTime = false;
            timeTranscodingEntity.Year = new TimeTranscoding() { Column = GetColumns().First(), Length = 4, Start = 0};
            PeriodTimeTranscoding monthly = new PeriodTimeTranscoding();
            for (int m = 1; m <= 12; m++)
            {
                monthly.AddRule(m.ToString("00", CultureInfo.InvariantCulture), m.ToString("x8"));
            }
            monthly.Length = 8;
            monthly.Start = 5;
            monthly.Column = GetColumns().First();

            timeTranscodingEntity.Period = monthly;

            var timeTranscodings = new List<TimeTranscodingEntity>();
            timeTranscodings.Add(timeTranscodingEntity);
            transcodingEntity.ConvertFromTranscoding(timeTranscodings, "FREQ");
            var addedTranscodingEntity = _persistManager.Add(transcodingEntity);

            Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
            var retrievedEntity = _retrieverManager.GetEntities<TimeDimensionMappingEntity>(StoreId, _mappingSetEntity.QueryForThisEntityId(), Detail.Full).First();

            Assert.That(retrievedEntity.EntityId, Is.EqualTo(addedTranscodingEntity.EntityId));
            Assert.That(retrievedEntity.ParentId, Is.EqualTo(addedTranscodingEntity.ParentId));
            Assert.That(retrievedEntity.Transcoding, Is.Not.Null);
            Assert.That(retrievedEntity.Transcoding.IsFrequencyDimension, Is.True);
            Assert.That(retrievedEntity.Transcoding.TimeFormatConfigurations, Has.Exactly(1).Items);
            var retrievedTranscodings = TimeTranscodingConversionHelper.Convert(retrievedEntity.Transcoding);
            TimeTranscodingEntity retrievedTimeTranscodingEntity = retrievedTranscodings.First();
            Assert.That(retrievedTimeTranscodingEntity.Frequency, Is.EqualTo(timeTranscodingEntity.Frequency));
            Assert.That(retrievedTimeTranscodingEntity.Year, Is.Not.Null);
            Assert.That(retrievedTimeTranscodingEntity.Period, Is.Not.Null);
            Assert.That(retrievedTimeTranscodingEntity.Year.Length, Is.EqualTo(timeTranscodingEntity.Year.Length));
            Assert.That(retrievedTimeTranscodingEntity.Year.Start, Is.EqualTo(timeTranscodingEntity.Year.Start));
            Assert.That(retrievedTimeTranscodingEntity.Period.Start, Is.EqualTo(timeTranscodingEntity.Period.Start));
            Assert.That(retrievedTimeTranscodingEntity.Period.Length, Is.EqualTo(timeTranscodingEntity.Period.Length));

            // It fails here and a fix needs to be implemented.
            Assert.That(retrievedTimeTranscodingEntity.Period.Rules, Is.Not.Null.And.Not.Empty.And.Count.EqualTo(12));
            Assert.That(retrievedTimeTranscodingEntity.Period.Rules[8].DsdCodeEntity.ObjectId, Is.EqualTo("09"));
            Assert.That(retrievedTimeTranscodingEntity.Period.Rules[8].LocalCodes.First().ObjectId, Is.EqualTo(9.ToString("x8")));

            AssertDelete();
        }

        [Test]
        public void ShouldTimeTranscodingAnnualMonthly1Column()
        {
            var transcodingEntity = new TimeDimensionMappingEntity();
            transcodingEntity.ParentId = _mappingSetEntity.EntityId;
            transcodingEntity.StoreId = _mappingSetEntity.StoreId;
            var timeTranscodings = new List<TimeTranscodingEntity>();

            TimeTranscodingEntity monthlyTranscoding = new TimeTranscodingEntity();
            monthlyTranscoding.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.Month).FrequencyCode;
            monthlyTranscoding.IsDateTime = false;
            monthlyTranscoding.Year = new TimeTranscoding() { Column = GetColumns().First(), Length = 4, Start = 0 };
            PeriodTimeTranscoding monthly = new PeriodTimeTranscoding();
            for (int m = 1; m <= 12; m++)
            {
                monthly.AddRule(m.ToString("00", CultureInfo.InvariantCulture), m.ToString("x8"));
            }
            monthly.Length = 8;
            monthly.Start = 5;
            monthly.Column = GetColumns().First();

            monthlyTranscoding.Period = monthly;

            timeTranscodings.Add(monthlyTranscoding);

            TimeTranscodingEntity annualTranscoding = new TimeTranscodingEntity();
            annualTranscoding.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.Year).FrequencyCode;
            annualTranscoding.IsDateTime = false;
            annualTranscoding.Year = new TimeTranscoding() { Column = GetColumns().First(), Length = 4, Start = 0 };
            timeTranscodings.Add(annualTranscoding);

            transcodingEntity.ConvertFromTranscoding(timeTranscodings, "FREQ");
            var addedTranscodingEntity = _persistManager.Add(transcodingEntity);

            Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
            var retrievedEntity = _retrieverManager.GetEntities<TimeDimensionMappingEntity>(StoreId, _mappingSetEntity.QueryForThisEntityId(), Detail.Full).First();

            Assert.That(retrievedEntity.EntityId, Is.EqualTo(addedTranscodingEntity.EntityId));
            Assert.That(retrievedEntity.ParentId, Is.EqualTo(addedTranscodingEntity.ParentId));
            Assert.That(retrievedEntity.Transcoding, Is.Not.Null);
            Assert.That(retrievedEntity.Transcoding.IsFrequencyDimension, Is.True);
            Assert.That(retrievedEntity.Transcoding.TimeFormatConfigurations, Has.Exactly(2).Items);
            var retrievedTranscodings = TimeTranscodingConversionHelper.Convert(retrievedEntity.Transcoding);
            TimeTranscodingEntity retrievedTimeTranscodingEntity = retrievedTranscodings.First(entity => entity.Frequency.Equals("M"));
            Assert.That(retrievedTimeTranscodingEntity.Frequency, Is.EqualTo(monthlyTranscoding.Frequency));
            Assert.That(retrievedTimeTranscodingEntity.Year, Is.Not.Null);
            Assert.That(retrievedTimeTranscodingEntity.Period, Is.Not.Null);
            Assert.That(retrievedTimeTranscodingEntity.Year.Length, Is.EqualTo(monthlyTranscoding.Year.Length));
            Assert.That(retrievedTimeTranscodingEntity.Year.Start, Is.EqualTo(monthlyTranscoding.Year.Start));
            Assert.That(retrievedTimeTranscodingEntity.Period.Start, Is.EqualTo(monthlyTranscoding.Period.Start));
            Assert.That(retrievedTimeTranscodingEntity.Period.Length, Is.EqualTo(monthlyTranscoding.Period.Length));

            // It fails here and a fix needs to be implemented.
            Assert.That(retrievedTimeTranscodingEntity.Period.Rules, Is.Not.Null.And.Not.Empty.And.Count.EqualTo(12));
            Assert.That(retrievedTimeTranscodingEntity.Period.Rules[8].DsdCodeEntity.ObjectId, Is.EqualTo("09"));
            Assert.That(retrievedTimeTranscodingEntity.Period.Rules[8].LocalCodes.First().ObjectId, Is.EqualTo(9.ToString("x8")));

            TimeTranscodingEntity retrievedAnnualTimeTranscoding = retrievedTranscodings.First(entity => entity.Frequency.Equals("A"));
            Assert.That(retrievedAnnualTimeTranscoding.Frequency, Is.EqualTo(annualTranscoding.Frequency));
            Assert.That(retrievedAnnualTimeTranscoding.Year, Is.Not.Null);
            Assert.That(retrievedAnnualTimeTranscoding.Period, Is.Null);
            Assert.That(retrievedAnnualTimeTranscoding.Year.Length, Is.EqualTo(annualTranscoding.Year.Length));
            Assert.That(retrievedAnnualTimeTranscoding.Year.Start, Is.EqualTo(annualTranscoding.Year.Start));

            AssertDelete();
        }

        [Test]
        public void ShouldTimeTranscodingAnnualMonthlyQuarterly1Column()
        {
            var transcodingEntity = new TimeDimensionMappingEntity();
            transcodingEntity.ParentId = _mappingSetEntity.EntityId;
            transcodingEntity.StoreId = _mappingSetEntity.StoreId;
            var timeTranscodings = new List<TimeTranscodingEntity>();

            TimeTranscodingEntity monthlyTranscoding = new TimeTranscodingEntity();
            monthlyTranscoding.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.Month).FrequencyCode;
            monthlyTranscoding.IsDateTime = false;
            monthlyTranscoding.Year = new TimeTranscoding() { Column = GetColumns().First(), Length = 4, Start = 0 };
            PeriodTimeTranscoding monthly = new PeriodTimeTranscoding();
            for (int m = 1; m <= 12; m++)
            {
                monthly.AddRule(m.ToString("00", CultureInfo.InvariantCulture), m.ToString("x8"));
            }
            monthly.Length = 8;
            monthly.Start = 5;
            monthly.Column = GetColumns().First();

            monthlyTranscoding.Period = monthly;

            timeTranscodings.Add(monthlyTranscoding);
            TimeTranscodingEntity quarterlyTranscoding = new TimeTranscodingEntity();
            quarterlyTranscoding.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.QuarterOfYear).FrequencyCode;
            quarterlyTranscoding.IsDateTime = false;
            quarterlyTranscoding.Year = new TimeTranscoding() { Column = GetColumns().First(), Length = 4, Start = 0 };
            PeriodTimeTranscoding quarterly = new PeriodTimeTranscoding();
            for (int m = 1; m <= 4; m++)
            {
                quarterly.AddRule(m.ToString("'Q'0", CultureInfo.InvariantCulture), m.ToString("x8"));
            }
            quarterly.Length = 8;
            quarterly.Start = 5;
            quarterly.Column = GetColumns().First();

            quarterlyTranscoding.Period = quarterly;

            timeTranscodings.Add(quarterlyTranscoding);
            TimeTranscodingEntity annualTranscoding = new TimeTranscodingEntity();
            annualTranscoding.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.Year).FrequencyCode;
            annualTranscoding.IsDateTime = false;
            annualTranscoding.Year = new TimeTranscoding() { Column = GetColumns().First(), Length = 4, Start = 0 };
            timeTranscodings.Add(annualTranscoding);

            transcodingEntity.ConvertFromTranscoding(timeTranscodings, "FREQ");
            var addedTranscodingEntity = _persistManager.Add(transcodingEntity);

            Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
            var retrievedEntity = _retrieverManager.GetEntities<TimeDimensionMappingEntity>(StoreId, _mappingSetEntity.QueryForThisEntityId(), Detail.Full).First();

            Assert.That(retrievedEntity.EntityId, Is.EqualTo(addedTranscodingEntity.EntityId));
            Assert.That(retrievedEntity.ParentId, Is.EqualTo(addedTranscodingEntity.ParentId));
            Assert.That(retrievedEntity.Transcoding, Is.Not.Null);
            Assert.That(retrievedEntity.Transcoding.IsFrequencyDimension, Is.True);
            Assert.That(retrievedEntity.Transcoding.TimeFormatConfigurations, Has.Exactly(3).Items);
            var retrievedTranscodings = TimeTranscodingConversionHelper.Convert(retrievedEntity.Transcoding);
            TimeTranscodingEntity retrievedMonthlyTimeTranscoding = retrievedTranscodings.First(entity => entity.Frequency.Equals("M"));
            Assert.That(retrievedMonthlyTimeTranscoding.Frequency, Is.EqualTo(monthlyTranscoding.Frequency));
            Assert.That(retrievedMonthlyTimeTranscoding.Year, Is.Not.Null);
            Assert.That(retrievedMonthlyTimeTranscoding.Period, Is.Not.Null);
            Assert.That(retrievedMonthlyTimeTranscoding.Year.Length, Is.EqualTo(monthlyTranscoding.Year.Length));
            Assert.That(retrievedMonthlyTimeTranscoding.Year.Start, Is.EqualTo(monthlyTranscoding.Year.Start));
            Assert.That(retrievedMonthlyTimeTranscoding.Period.Start, Is.EqualTo(monthlyTranscoding.Period.Start));
            Assert.That(retrievedMonthlyTimeTranscoding.Period.Length, Is.EqualTo(monthlyTranscoding.Period.Length));

            Assert.That(retrievedMonthlyTimeTranscoding.Period.Rules, Is.Not.Null.And.Not.Empty.And.Count.EqualTo(12));
            Assert.That(retrievedMonthlyTimeTranscoding.Period.Rules[8].DsdCodeEntity.ObjectId, Is.EqualTo("09"));
            Assert.That(retrievedMonthlyTimeTranscoding.Period.Rules[8].LocalCodes.First().ObjectId, Is.EqualTo(9.ToString("x8")));


            TimeTranscodingEntity retrievedQuarterlyTimeTranscoding = retrievedTranscodings.First(entity => entity.Frequency.Equals("Q"));
            Assert.That(retrievedQuarterlyTimeTranscoding.Frequency, Is.EqualTo(quarterlyTranscoding.Frequency));
            Assert.That(retrievedQuarterlyTimeTranscoding.Year, Is.Not.Null);
            Assert.That(retrievedQuarterlyTimeTranscoding.Period, Is.Not.Null);
            Assert.That(retrievedQuarterlyTimeTranscoding.Year.Length, Is.EqualTo(quarterlyTranscoding.Year.Length));
            Assert.That(retrievedQuarterlyTimeTranscoding.Year.Start, Is.EqualTo(quarterlyTranscoding.Year.Start));
            Assert.That(retrievedQuarterlyTimeTranscoding.Period.Start, Is.EqualTo(quarterlyTranscoding.Period.Start));
            Assert.That(retrievedQuarterlyTimeTranscoding.Period.Length, Is.EqualTo(quarterlyTranscoding.Period.Length));

            // It fails here and a fix needs to be implemented.
            Assert.That(retrievedQuarterlyTimeTranscoding.Period.Rules, Is.Not.Null.And.Not.Empty.And.Count.EqualTo(4));
            Assert.That(retrievedQuarterlyTimeTranscoding.Period.Rules[1].DsdCodeEntity.ObjectId, Is.EqualTo("Q2"));
            Assert.That(retrievedQuarterlyTimeTranscoding.Period.Rules[3].LocalCodes.First().ObjectId, Is.EqualTo(4.ToString("x8")));

            TimeTranscodingEntity retrievedAnnualTimeTranscoding = retrievedTranscodings.First(entity => entity.Frequency.Equals("A"));
            Assert.That(retrievedAnnualTimeTranscoding.Frequency, Is.EqualTo(annualTranscoding.Frequency));
            Assert.That(retrievedAnnualTimeTranscoding.Year, Is.Not.Null);
            Assert.That(retrievedAnnualTimeTranscoding.Period, Is.Null);
            Assert.That(retrievedAnnualTimeTranscoding.Year.Length, Is.EqualTo(annualTranscoding.Year.Length));
            Assert.That(retrievedAnnualTimeTranscoding.Year.Start, Is.EqualTo(annualTranscoding.Year.Start));

            AssertDelete();
        }

        [Test]
        public void ShouldTimeTranscodingAnnualMonthlyQuarterly2Column()
        {
            TimeDimensionMappingEntity transcodingEntity = new TimeDimensionMappingEntity();
            transcodingEntity.ParentId = _mappingSetEntity.EntityId;
            transcodingEntity.StoreId = _mappingSetEntity.StoreId;
            var timeTranscodings = new List<TimeTranscodingEntity>();

            TimeTranscodingEntity monthlyTranscoding = new TimeTranscodingEntity();
            monthlyTranscoding.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.Month).FrequencyCode;
            monthlyTranscoding.IsDateTime = false;
            monthlyTranscoding.Year = new TimeTranscoding() { Column = GetColumns().First(), Length = 4, Start = 0 };
            PeriodTimeTranscoding monthly = new PeriodTimeTranscoding();
            for (int m = 1; m <= 12; m++)
            {
                monthly.AddRule(m.ToString("00", CultureInfo.InvariantCulture), m.ToString("x8"));
            }
            monthly.Length = 8;
            monthly.Start = 5;
            monthly.Column = GetColumns().First();

            monthlyTranscoding.Period = monthly;

            timeTranscodings.Add(monthlyTranscoding);
            TimeTranscodingEntity quarterlyTranscoding = new TimeTranscodingEntity();
            quarterlyTranscoding.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.QuarterOfYear).FrequencyCode;
            quarterlyTranscoding.IsDateTime = false;
            quarterlyTranscoding.Year = new TimeTranscoding() { Column = GetColumns().First(), Length = 4, Start = 0 };
            PeriodTimeTranscoding quarterly = new PeriodTimeTranscoding();
            for (int m = 1; m <= 4; m++)
            {
                quarterly.AddRule(m.ToString("'Q'0", CultureInfo.InvariantCulture), m.ToString("x8"));
            }
            quarterly.Length = 8;
            quarterly.Start = 5;
            quarterly.Column = GetColumns().Last();

            quarterlyTranscoding.Period = quarterly;

            timeTranscodings.Add(quarterlyTranscoding);
            TimeTranscodingEntity annualTranscoding = new TimeTranscodingEntity();
            annualTranscoding.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.Year).FrequencyCode;
            annualTranscoding.IsDateTime = false;
            annualTranscoding.Year = new TimeTranscoding() { Column = GetColumns().First(), Length = 4, Start = 0 };
            timeTranscodings.Add(annualTranscoding);

            transcodingEntity.ConvertFromTranscoding(timeTranscodings, "FREQ");
            var addedTranscodingEntity = _persistManager.Add(transcodingEntity);

            Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
            var retrievedEntity = _retrieverManager.GetEntities<TimeDimensionMappingEntity>(StoreId, _mappingSetEntity.QueryForThisEntityId(), Detail.Full).First();

            Assert.That(retrievedEntity.EntityId, Is.EqualTo(addedTranscodingEntity.EntityId));
            Assert.That(retrievedEntity.ParentId, Is.EqualTo(addedTranscodingEntity.ParentId));
            Assert.That(retrievedEntity.Transcoding, Is.Not.Null);
            Assert.That(retrievedEntity.Transcoding.IsFrequencyDimension, Is.True);
            Assert.That(retrievedEntity.Transcoding.TimeFormatConfigurations, Has.Exactly(3).Items);
            var retrievedTranscodings = TimeTranscodingConversionHelper.Convert(retrievedEntity.Transcoding);
            TimeTranscodingEntity retrievedMonthlyTimeTranscoding = retrievedTranscodings.First(entity => entity.Frequency.Equals("M", StringComparison.Ordinal));
            Assert.That(retrievedMonthlyTimeTranscoding.Frequency, Is.EqualTo(monthlyTranscoding.Frequency));
            Assert.That(retrievedMonthlyTimeTranscoding.Year, Is.Not.Null);
            Assert.That(retrievedMonthlyTimeTranscoding.Period, Is.Not.Null);
            Assert.That(retrievedMonthlyTimeTranscoding.Year.Length, Is.EqualTo(monthlyTranscoding.Year.Length));
            Assert.That(retrievedMonthlyTimeTranscoding.Year.Start, Is.EqualTo(monthlyTranscoding.Year.Start));
            Assert.That(retrievedMonthlyTimeTranscoding.Period.Start, Is.EqualTo(monthlyTranscoding.Period.Start));
            Assert.That(retrievedMonthlyTimeTranscoding.Period.Length, Is.EqualTo(monthlyTranscoding.Period.Length));

            Assert.That(retrievedMonthlyTimeTranscoding.Period.Rules, Is.Not.Null.And.Not.Empty.And.Count.EqualTo(12));
            Assert.That(retrievedMonthlyTimeTranscoding.Period.Rules[8].DsdCodeEntity.ObjectId, Is.EqualTo("09"));
            Assert.That(retrievedMonthlyTimeTranscoding.Period.Rules[8].LocalCodes.First().ObjectId, Is.EqualTo(9.ToString("x8")));


            TimeTranscodingEntity retrievedQuarterlyTimeTranscoding = retrievedTranscodings.First(entity => entity.Frequency.Equals("Q"));
            Assert.That(retrievedQuarterlyTimeTranscoding.Frequency, Is.EqualTo(quarterlyTranscoding.Frequency));
            Assert.That(retrievedQuarterlyTimeTranscoding.Year, Is.Not.Null);
            Assert.That(retrievedQuarterlyTimeTranscoding.Period, Is.Not.Null);
            Assert.That(retrievedQuarterlyTimeTranscoding.Year.Length, Is.EqualTo(quarterlyTranscoding.Year.Length));
            Assert.That(retrievedQuarterlyTimeTranscoding.Year.Start, Is.EqualTo(quarterlyTranscoding.Year.Start));
            Assert.That(retrievedQuarterlyTimeTranscoding.Period.Start, Is.EqualTo(quarterlyTranscoding.Period.Start));
            Assert.That(retrievedQuarterlyTimeTranscoding.Period.Length, Is.EqualTo(quarterlyTranscoding.Period.Length));

            // It fails here and a fix needs to be implemented.
            Assert.That(retrievedQuarterlyTimeTranscoding.Period.Rules, Is.Not.Null.And.Not.Empty.And.Count.EqualTo(4));
            Assert.That(retrievedQuarterlyTimeTranscoding.Period.Rules[1].DsdCodeEntity.ObjectId, Is.EqualTo("Q2"));
            Assert.That(retrievedQuarterlyTimeTranscoding.Period.Rules[3].LocalCodes.First().ObjectId, Is.EqualTo(4.ToString("x8")));

            TimeTranscodingEntity retrievedAnnualTimeTranscoding = retrievedTranscodings.First(entity => entity.Frequency.Equals("A"));
            Assert.That(retrievedAnnualTimeTranscoding.Frequency, Is.EqualTo(annualTranscoding.Frequency));
            Assert.That(retrievedAnnualTimeTranscoding.Year, Is.Not.Null);
            Assert.That(retrievedAnnualTimeTranscoding.Period, Is.Null);
            Assert.That(retrievedAnnualTimeTranscoding.Year.Length, Is.EqualTo(annualTranscoding.Year.Length));
            Assert.That(retrievedAnnualTimeTranscoding.Year.Start, Is.EqualTo(annualTranscoding.Year.Start));
            AssertDelete();
        }

        private void AssertDelete()
        {
            _persistManager.Delete<TimeDimensionMappingEntity>(this.StoreId, _mappingSetEntity.EntityId);
            var enumerable = _retrieverManager.GetEntities<TimeDimensionMappingEntity>(StoreId, _mappingSetEntity.QueryForThisParentId(), Detail.Full).ToArray();
            Assert.That(enumerable, Has.Exactly(0).Items);
        }

        [Test]
        public void ShouldTimeTranscodingMonthly2Column()
        {
            var transcodingEntity = new TimeDimensionMappingEntity();
            transcodingEntity.ParentId = _mappingSetEntity.EntityId;
            transcodingEntity.StoreId = _mappingSetEntity.StoreId;

            TimeTranscodingEntity timeTranscodingEntity = new TimeTranscodingEntity();
            timeTranscodingEntity.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.Month).FrequencyCode;
            timeTranscodingEntity.IsDateTime = false;
            timeTranscodingEntity.Year = new TimeTranscoding() { Column = GetColumns().First(), Length = 4, Start = 0 };
            PeriodTimeTranscoding monthly = new PeriodTimeTranscoding();
            for (int m = 1; m <= 12; m++)
            {
                monthly.AddRule(m.ToString("00", CultureInfo.InvariantCulture), m.ToString("x8"));
            }
            monthly.Length = -1;
            monthly.Start = 0;
            monthly.Column = GetColumns().Last();

            timeTranscodingEntity.Period = monthly;

            var timeTranscodings = new List<TimeTranscodingEntity>();
            timeTranscodings.Add(timeTranscodingEntity);
            transcodingEntity.ConvertFromTranscoding(timeTranscodings, "FREQ");
            var addedTranscodingEntity = _persistManager.Add(transcodingEntity);

            Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
            var retrievedEntity = _retrieverManager.GetEntities<TimeDimensionMappingEntity>(StoreId, _mappingSetEntity.QueryForThisEntityId(), Detail.Full).First();

            Assert.That(retrievedEntity.EntityId, Is.EqualTo(addedTranscodingEntity.EntityId));
            Assert.That(retrievedEntity.ParentId, Is.EqualTo(addedTranscodingEntity.ParentId));
            Assert.That(retrievedEntity.Transcoding, Is.Not.Null);
            Assert.That(retrievedEntity.Transcoding.IsFrequencyDimension, Is.True);
            Assert.That(retrievedEntity.Transcoding.TimeFormatConfigurations, Has.Exactly(1).Items);
            var retrievedTranscodings = TimeTranscodingConversionHelper.Convert(retrievedEntity.Transcoding);
            TimeTranscodingEntity retrievedTimeTranscodingEntity = retrievedTranscodings.First();
            Assert.That(retrievedTimeTranscodingEntity.Frequency, Is.EqualTo(timeTranscodingEntity.Frequency));
            Assert.That(retrievedTimeTranscodingEntity.Year, Is.Not.Null);
            Assert.That(retrievedTimeTranscodingEntity.Period, Is.Not.Null);
            Assert.That(retrievedTimeTranscodingEntity.Year.Length, Is.EqualTo(timeTranscodingEntity.Year.Length));
            Assert.That(retrievedTimeTranscodingEntity.Year.Start, Is.EqualTo(timeTranscodingEntity.Year.Start));
            Assert.That(retrievedTimeTranscodingEntity.Period.Start, Is.EqualTo(timeTranscodingEntity.Period.Start));
            Assert.That(retrievedTimeTranscodingEntity.Period.Length, Is.EqualTo(timeTranscodingEntity.Period.Length));

            // It fails here and a fix needs to be implemented.
            Assert.That(retrievedTimeTranscodingEntity.Period.Rules, Is.Not.Null.And.Not.Empty.And.Count.EqualTo(12));
            Assert.That(retrievedTimeTranscodingEntity.Period.Rules[8].DsdCodeEntity.ObjectId, Is.EqualTo("09"));
            Assert.That(retrievedTimeTranscodingEntity.Period.Rules[8].LocalCodes.First().ObjectId, Is.EqualTo(9.ToString("x8")));

            AssertDelete();
        }

   
        private IDataflowObject BuildStructuralMetadata()
        {
            IMutableObjects mutableObjects = new MutableObjectsImpl();
            IConceptSchemeMutableObject conceptScheme = new ConceptSchemeMutableCore();
            SetupMaintainable(conceptScheme, "TEST_CS");

            AddConcept(conceptScheme, "FREQ");
            AddConcept(conceptScheme, "REF_AREA");
            AddConcept(conceptScheme, DimensionObject.TimeDimensionFixedId);
            AddConcept(conceptScheme, PrimaryMeasure.FixedId);

            mutableObjects.AddConceptScheme(conceptScheme);

            ICodelistMutableObject cl = new CodelistMutableCore();
            SetupMaintainable(cl, "CL_FREQ");

            var c1 = new CodeMutableCore() { Id = "A" };
            c1.AddName("en", "Annual");
            cl.AddItem(c1);
            var c2 = new CodeMutableCore() { Id = "M" };
            c2.AddName("en", "Monthly");
            cl.AddItem(c2);

            mutableObjects.AddCodelist(cl);

            IDataStructureMutableObject dsd = new DataStructureMutableCore();
            SetupMaintainable(dsd, "TEST_DSD");
            dsd.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "FREQ"),
                new StructureReferenceImpl("TEST", "CL_FREQ", "1.0", SdmxStructureEnumType.CodeList));
            dsd.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "REF_AREA"),
                new StructureReferenceImpl("TEST", "CL_FREQ", "1.0", SdmxStructureEnumType.CodeList));

            IDimensionMutableObject timeDimension = new DimensionMutableCore();
            timeDimension.ConceptRef = new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, DimensionObject.TimeDimensionFixedId);
            timeDimension.TimeDimension = true;
            dsd.AddDimension(timeDimension);

            dsd.AddPrimaryMeasure(
                new StructureReferenceImpl(
                    "TEST",
                    "TEST_CS",
                    "1.0",
                    SdmxStructureEnumType.Concept,
                    PrimaryMeasure.FixedId));
            mutableObjects.AddDataStructure(dsd);

            var dataflow = new DataflowMutableCore(dsd.ImmutableInstance);
            mutableObjects.AddDataflow(dataflow);
            var artefactImportStatuses = new List<ArtefactImportStatus>();
            var structurePersistenceManager =
                new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(
                    _connectionStringSettings,
                    artefactImportStatuses);

            structurePersistenceManager.SaveStructures(mutableObjects.ImmutableObjects);
            Assert.That(
                artefactImportStatuses.All(status => status.ImportMessage.Status != ImportMessageStatus.Error),
                string.Join("\n", artefactImportStatuses.Select(status => status.ImportMessage.Message)));

            return dataflow.ImmutableInstance;
        }

        private void SetupMaintainable(IMaintainableMutableObject maintainable, string id)
        {
            maintainable.Version = "1.0";
            maintainable.AgencyId = "TEST";
            maintainable.Id = id;
            maintainable.AddName("en", $"Test {id}");
            maintainable.FinalStructure = TertiaryBool.ParseBoolean(true);
        }

        private void AddConcept(IConceptSchemeMutableObject cs, string id)
        {
            var c = new ConceptMutableCore() {Id = id};
            c.AddName("en", $"Name of $id");
            cs.AddItem(c);
        }
        
        private IList<DataSetColumnEntity> GetColumns()
        {
            if (this._datasetColumns == null)
            {
                var datasetEntityId = _mappingSetEntity.DataSetId;
                var datasetColumns =
                    _retrieverManager.GetEntities<DataSetColumnEntity>(
                        StoreId,
                        datasetEntityId.QueryForThisParentId(),
                        Detail.Full).ToArray();


                this._datasetColumns = datasetColumns.Where(entity => entity.Name.Contains("_TIME_PERIOD", StringComparison.OrdinalIgnoreCase)).ToArray();
            }
            return this._datasetColumns;
        }

        private MappingSetEntity AddSourceMappingSet(IDataflowObject dataflowObject, string datasetEntityId)
        {
            MappingSetEntity mappingSetEntity = new MappingSetEntity();
            mappingSetEntity.StoreId = StoreId;
            mappingSetEntity.DataSetId = datasetEntityId;
            mappingSetEntity.Name = "Test Mapping Set for " + dataflowObject.Id;
            mappingSetEntity.ParentId = dataflowObject.Urn.ToString();
            mappingSetEntity.Description = "a description";

            var addedMappingSet = _persistManager.Add(mappingSetEntity);

            var query = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument).SetStructureIdentification(dataflowObject.DataStructureRef).Build();
            var dsdAndDepds = _retrievalManager.GetRetrievalEngineContainer(new Database(this._connectionStringSettings)).DSDRetrievalEngine.Retrieve(query);

            var dsd = dsdAndDepds.First().ImmutableInstance;

            // this test needs a time dimension
            Assert.That(dsd.TimeDimension, Is.Not.Null);

            return mappingSetEntity;
        }
    }
}