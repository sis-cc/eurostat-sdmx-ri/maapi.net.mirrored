﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Config;
using NSubstitute;
using NUnit.Framework;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    public class StoredProcedureCommandDefinitionBuilderTests
    {
        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldCreateAStoreProcedureCommandDefinition(string mappingStoreIdentifier)
        {
            //var providerName = "System.Data.SqlClient";
            //var storedProcedureCommandDefinitionBuilder = new StoredProcedureCommandDefinitionBuilder();
            //var patchRequest = new PatchRequest()
            //{
            //    new PatchDocument("add","ItemId",""),
            //    new PatchDocument("add","ColumnId","1")
            //};
            //var commandDefinitions = storedProcedureCommandDefinitionBuilder.GetCommands(TODO, "1", providerName);
            //var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            //var connectionStringHelper = new ConnectionStringRetriever();
            //var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            //configurationStoreManager.GetSettings<ConnectionStringSettings>()
            //   .Returns(new[] { connectionStringSettings });
            //var database = new DatabaseManager(configurationStoreManager).GetDatabase(mappingStoreIdentifier);
            //database.ExecuteCommandsWithReturnId(commandDefinitions.First());
        }
    }
}
