using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Config;
using NSubstitute;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class ComponentMappingEngineTest
    {
        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        private readonly ConnectionStringSettings _connectionStringSettings;

        public ComponentMappingEngineTest(string name)
        {
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(name);
        }

        [Test]
        public void GetAComponentMapping()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var mappingStoreManager = Substitute.For<IMappingStoreManager>();

            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            var databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new ComponentMappingRetrieverFactory(databaseManager, mappingStoreManager));
            var engine = retrieverManager.GetRetrieverEngine<ComponentMappingEntity>(this._connectionStringSettings.Name);
            var query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.Exact, "86"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
            //Assert.That(result.First().Name, Is.EqualTo("TIME_PERIOD"));
        }

        [Test]
        public void GetAComponentMappingBasedOnParentIdCriteria()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var mappingStoreManager = Substitute.For<IMappingStoreManager>();

            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            var databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new ComponentMappingRetrieverFactory(databaseManager, mappingStoreManager));
            var engine = retrieverManager.GetRetrieverEngine<ComponentMappingEntity>(this._connectionStringSettings.Name);
            var query = Substitute.For<IEntityQuery>();
            query.ParentId.Returns(new Criteria<string>(OperatorType.Exact, "2"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
            //Assert.That(result.Count(), Is.EqualTo(10));
        }

        [Test]
        public void GetAllComponentMapping()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var mappingStoreManager = Substitute.For<IMappingStoreManager>();

            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            var databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new ComponentMappingRetrieverFactory(databaseManager, mappingStoreManager));
            var engine = retrieverManager.GetRetrieverEngine<ComponentMappingEntity>(this._connectionStringSettings.Name);
            var query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.AnyValue, null));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
        }
    }
}