using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Config;
using NSubstitute;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class RetrieverHeaderTests
    {
        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        private readonly ConnectionStringSettings _connectionStringSettings;

        public RetrieverHeaderTests(string name)
        {
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(name);
        }

        [Test]
        public void GetAllHeaders()
        {
            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new HeaderRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<HeaderEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.AnyValue, null));
            var result = engine.GetEntities(query, Detail.Full).ToArray();

        }

        [Test]
        public void GetAHeader()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new HeaderRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<HeaderEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.Exact, "96"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
            //Assert.That(result.First().EntityId, Is.Not.Null);
            //Assert.That(result.First().ParentId, Is.Not.Null);
        }


        [Test]
        public void GetAHeaderBasedOnAdditionalCriteria()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new HeaderRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<HeaderEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>()
            {
                { EntityType.DataSet.ToString(),new Criteria<string>(OperatorType.Exact,"3" )}
            });
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
        }


        [Test]
        public void GetAHeaderBasedOnParentId()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new HeaderRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<HeaderEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.ParentId.Returns(new Criteria<string>(OperatorType.Exact, "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_M(2.0)"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
        }

        [Test]
        public void GetAHeaderBasedOnParentIdExclusion()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new HeaderRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<HeaderEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.ParentId.Returns(new Criteria<string>(OperatorType.Not | OperatorType.Contains, "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_M(2.0)"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
        }
    }
}