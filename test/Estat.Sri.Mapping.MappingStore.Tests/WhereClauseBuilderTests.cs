using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Builder.MappingLogic;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NSubstitute;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    class WhereClauseBuilderTests
    {
        private Database _database;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public WhereClauseBuilderTests()
        {

            ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();
            var _connectionStringSettings = _connectionStringHelper.GetConnectionStringSettings("sqlserver");
            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { _connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            this._database = databaseManager.GetDatabase("sqlserver");
        }

        [Test]
        public void ShouldCreateStringWithLessThanEqual()
        {
            var whereClauseBuilder = new WhereClauseBuilder(this._database);
            var whereClauseParameters = whereClauseBuilder.Build(new Criteria<string>(OperatorType.LessThanOrEqual, "5"), "Test");
            Assert.That(whereClauseParameters.WhereClause,Is.EqualTo("( Test <= @Test0 )"));
        }

        [Test]
        public void ShouldCreateStringWithMoreThanAndEqual()
        {
            var whereClauseBuilder = new WhereClauseBuilder(this._database);
            var whereClauseParameters = whereClauseBuilder.Build(new Criteria<string>(OperatorType.GreaterThanOrEqual, "5"), "Test");
            Assert.That(whereClauseParameters.WhereClause, Is.EqualTo("( Test => @Test0 )"));
        }

        [Test]
        public void ShouldCreateInClauseWith5Items()
        {
            var mappedValues = new []{"'A'", "'B'", "'C'", "'D'", "'E'"};
            string inClause = ComponentMappingCommon.GenerateInClause("[FREQ]", mappedValues).Replace(" ", "", StringComparison.Ordinal);
            Assert.That(inClause, Is.EqualTo("(([FREQ]in('A','B','C','D','E')))"));
        }
       
        [Test]
        public void ShouldCreateInClauseWith1500Items()
        {
            var mappedValues = Enumerable.Range(0, 5000).Select(i => string.Format(CultureInfo.InvariantCulture, "'{0}'", i)).ToArray();
            string inClause = ComponentMappingCommon.GenerateInClause("[FREQ]", mappedValues).Replace(" ", "", StringComparison.Ordinal);

            var expected1 = "([FREQ]in(" + string.Join(",", mappedValues.Take(1000)) + ")OR";
            var expected2 = "[FREQ]in(" + string.Join(",", mappedValues.Skip(1000).Take(1000)) + ")OR";
            var expected3 = "[FREQ]in(" + string.Join(",", mappedValues.Skip(2000).Take(1000)) + ")OR";
            var expected4 = "[FREQ]in(" + string.Join(",", mappedValues.Skip(3000).Take(1000)) + ")OR";
            var expected5 = "[FREQ]in(" + string.Join(",", mappedValues.Skip(4000).Take(1000)) + "))";

            Assert.That(inClause, Is.EqualTo(expected1 + expected2 + expected3 + expected4 + expected5));
        }
    }
}
