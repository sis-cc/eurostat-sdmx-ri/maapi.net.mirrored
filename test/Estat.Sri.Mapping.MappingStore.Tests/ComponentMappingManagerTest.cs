// -----------------------------------------------------------------------
// <copyright file="ComponentMappingManagerTest.cs" company="EUROSTAT">
//   Date Created : 2017-09-11
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder.MappingLogic;
    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Model;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture("msdb_scratch.oracle")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.sqlserver")]
    public class ComponentMappingManagerTest : BaseTestClassWithContainer
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ComponentMappingManagerTest));

        /// <summary>
        /// The retriever manager
        /// </summary>
        private readonly IEntityRetrieverManager _entityRetriever;

        /// <summary>
        /// The persist manager
        /// </summary>
        private readonly IEntityPersistenceManager _persistenceManager;

        /// <summary>
        /// The connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        private IDataStructureObject _dataStructureObject;

        private IDataflowObject _dataflowObject;
        private DatasetEntity _dataset;
        private MappingSetEntity _mappingSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTestClassWithContainer"/> class.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        public ComponentMappingManagerTest(string storeId)
            : base(storeId)
        {
            this._entityRetriever = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            this._persistenceManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            _connectionStringSettings = GetConnectionStringSettings();
            ReBuildMappingStore();
        }

        [Test]
        public void TestComponentMapping()
        {
            var componentMappingManager = IoCContainer.Resolve<IComponentMappingManager>();
            var componentMappingContainer = componentMappingManager.GetBuilders(StoreId, _dataflowObject, _dataStructureObject, null, false);
            Assert.That(componentMappingContainer, Is.Not.Null);
            Assert.That(componentMappingContainer.ComponentMappingBuilders, Is.Not.Null.And.Not.Empty);
            Assert.That(componentMappingContainer.ComponentMappings, Is.Not.Null.And.Not.Empty);
            Assert.That(componentMappingContainer.Dataset, Is.Not.Null);
            Assert.That(componentMappingContainer.DisseminationConnection, Is.Not.Null);
            Assert.That(componentMappingContainer.TimeDimensionBuilder, Is.Not.Null);
            Assert.That(componentMappingContainer.TimeDimensionMapping, Is.Not.Null);
            var frequencyMapping = componentMappingContainer.ComponentMappingBuilders["FREQ"];
            Assert.That(frequencyMapping, Is.InstanceOf(typeof(ComponentMapping1To1)));
            Assert.That(componentMappingContainer.TimeDimensionBuilder, Is.InstanceOf(typeof(TimeDimensionMultiFrequency)));
        }

        [Test]
        public void ShouldAddNewMappingSet()
        {
            var mappingSet = GetMappingSetEntity("test name 1", _dataset.EntityId, _dataflowObject.Urn.ToString());
            mappingSet.EntityId = string.Empty;
            DeleteByName(mappingSet);
            _persistenceManager.Add(mappingSet);

            var addedMappingSet = _entityRetriever.GetEntities<MappingSetEntity>(StoreId, mappingSet.EntityId.QueryForThisEntityId(), Detail.Full)
                    .Single();

            Assert.IsNotNull(addedMappingSet);
        }

       [Test]
        public void ShouldAddLastUpdateMapping()
        {
            var mappingSet = GetMappingSetEntity("test name 1", _dataset.EntityId, _dataflowObject.Urn.ToString());
            mappingSet.EntityId = string.Empty;
            DeleteByName(mappingSet);
            _persistenceManager.Add(mappingSet);

            var addedMappingSet = _entityRetriever.GetEntities<MappingSetEntity>(StoreId, mappingSet.EntityId.QueryForThisEntityId(), Detail.Full)
                    .Single();

            Assert.IsNotNull(addedMappingSet);
            LastUpdatedEntity mapping = new LastUpdatedEntity();
            mapping.EntityId = addedMappingSet.EntityId;
            mapping.ParentId = addedMappingSet.EntityId;
            mapping.StoreId = StoreId;
            mapping.Column = new DataSetColumnEntity() { Name = "LAST_UP_COL" };
            _persistenceManager.Add(mapping);
            List<LastUpdatedEntity> entities = _entityRetriever.GetEntities<LastUpdatedEntity>(StoreId, addedMappingSet.QueryForThisParentId(), Detail.Full).ToList();
            Assert.That(entities, Is.Not.Null.And.Not.Empty);
            Assert.That(entities.Count, Is.EqualTo(1));
            var entity = entities[0];
            Assert.That(entity.EntityId, Is.EqualTo(addedMappingSet.EntityId));
            Assert.That(entity.Column, Is.Not.Null);
            Assert.That(entity.Column.Name, Is.EqualTo(mapping.Column.Name));
        }
       [Test]
        public void ShouldAddLastUpdateMappingConstant()
        {
            var mappingSet = GetMappingSetEntity("test name 9", _dataset.EntityId, _dataflowObject.Urn.ToString());
            mappingSet.EntityId = string.Empty;
            DeleteByName(mappingSet);
            _persistenceManager.Add(mappingSet);

            var addedMappingSet = _entityRetriever.GetEntities<MappingSetEntity>(StoreId, mappingSet.EntityId.QueryForThisEntityId(), Detail.Full)
                    .Single();

            Assert.IsNotNull(addedMappingSet);
            var mapping = new LastUpdatedEntity
            {
                EntityId = addedMappingSet.EntityId,
                ParentId = addedMappingSet.EntityId,
                StoreId = StoreId,
                Constant = new DateTime(2022, 1, 10)
            };
            _persistenceManager.Add(mapping);
            List<LastUpdatedEntity> entities = _entityRetriever.GetEntities<LastUpdatedEntity>(StoreId, addedMappingSet.QueryForThisParentId(), Detail.Full).ToList();
            Assert.That(entities, Is.Not.Null.And.Not.Empty);
            Assert.That(entities.Count, Is.EqualTo(1));
            var entity = entities[0];
            Assert.That(entity.EntityId, Is.EqualTo(addedMappingSet.EntityId));
            Assert.That(entity.Column, Is.Null);
            Assert.That(entity.Constant, Is.EqualTo(mapping.Constant));
        }

        [Test]
        public void ShouldAddValidToMapping()
        {
            var mappingSet = GetMappingSetEntity("test name 1", _dataset.EntityId, _dataflowObject.Urn.ToString());
            mappingSet.EntityId = string.Empty;
            DeleteByName(mappingSet);
            _persistenceManager.Add(mappingSet);

            var addedMappingSet = _entityRetriever.GetEntities<MappingSetEntity>(StoreId, mappingSet.EntityId.QueryForThisEntityId(), Detail.Full)
                    .Single();

            Assert.IsNotNull(addedMappingSet);
            var mapping = new ValidToMappingEntity
            {
                EntityId = addedMappingSet.EntityId,
                ParentId = addedMappingSet.EntityId,
                StoreId = StoreId,
                Column = new DataSetColumnEntity() { Name = "VALID_TO_COL" }
            };
            _persistenceManager.Add(mapping);
            List<ValidToMappingEntity> entities = _entityRetriever.GetEntities<ValidToMappingEntity>(StoreId, addedMappingSet.QueryForThisParentId(), Detail.Full).ToList();
            Assert.That(entities, Is.Not.Null.And.Not.Empty);
            Assert.That(entities.Count, Is.EqualTo(1));
            var entity = entities[0];
            Assert.That(entity.EntityId, Is.EqualTo(addedMappingSet.EntityId));
            Assert.That(entity.Column, Is.Not.Null);
            Assert.That(entity.Column.Name, Is.EqualTo(mapping.Column.Name));
        }
       [Test]
        public void ShouldAddValidToMappingConstant()
        {
            var mappingSet = GetMappingSetEntity("test name 9", _dataset.EntityId, _dataflowObject.Urn.ToString());
            mappingSet.EntityId = string.Empty;
            DeleteByName(mappingSet);
            _persistenceManager.Add(mappingSet);

            var addedMappingSet = _entityRetriever.GetEntities<MappingSetEntity>(StoreId, mappingSet.EntityId.QueryForThisEntityId(), Detail.Full)
                    .Single();

            Assert.IsNotNull(addedMappingSet);
            var mapping = new ValidToMappingEntity
            {
                EntityId = addedMappingSet.EntityId,
                ParentId = addedMappingSet.EntityId,
                StoreId = StoreId,
                Constant = new DateTime(2022, 1, 10)
            };
            _persistenceManager.Add(mapping);
            List<ValidToMappingEntity> entities = _entityRetriever.GetEntities<ValidToMappingEntity>(StoreId, addedMappingSet.QueryForThisParentId(), Detail.Full).ToList();
            Assert.That(entities, Is.Not.Null.And.Not.Empty);
            Assert.That(entities.Count, Is.EqualTo(1));
            var entity = entities[0];
            Assert.That(entity.EntityId, Is.EqualTo(addedMappingSet.EntityId));
            Assert.That(entity.Column, Is.Null);
            Assert.That(entity.Constant, Is.EqualTo(mapping.Constant));
        }


        [Test]
        public void ShouldAddUpdateStatusMapping()
        {
            var mappingSet = GetMappingSetEntity("test name 1", _dataset.EntityId, _dataflowObject.Urn.ToString());
            mappingSet.EntityId = string.Empty;
            DeleteByName(mappingSet);
            _persistenceManager.Add(mappingSet);

            var addedMappingSet = _entityRetriever.GetEntities<MappingSetEntity>(StoreId, mappingSet.EntityId.QueryForThisEntityId(), Detail.Full)
                    .Single();

            Assert.IsNotNull(addedMappingSet);
            var mapping = new UpdateStatusEntity()
            {
                EntityId = addedMappingSet.EntityId,
                ParentId = addedMappingSet.EntityId,
                StoreId = StoreId,
                Column = new DataSetColumnEntity() { Name = "VALID_TO_COL" }
            };
            _persistenceManager.Add(mapping);
            List<UpdateStatusEntity> entities = _entityRetriever.GetEntities<UpdateStatusEntity>(StoreId, addedMappingSet.QueryForThisParentId(), Detail.Full).ToList();
            Assert.That(entities, Is.Not.Null.And.Not.Empty);
            Assert.That(entities.Count, Is.EqualTo(1));
            var entity = entities[0];
            Assert.That(entity.EntityId, Is.EqualTo(addedMappingSet.EntityId));
            Assert.That(entity.Column, Is.Not.Null);
            Assert.That(entity.Column.Name, Is.EqualTo(mapping.Column.Name));
            Assert.That(entity.Transcoding, Is.Empty);
        }


        [Test]
        public void ShouldAddUpdateStatusMappingWithTranscoding()
        {
            var mappingSet = GetMappingSetEntity("test name 1", _dataset.EntityId, _dataflowObject.Urn.ToString());
            mappingSet.EntityId = string.Empty;
            DeleteByName(mappingSet);
            _persistenceManager.Add(mappingSet);

            var addedMappingSet = _entityRetriever.GetEntities<MappingSetEntity>(StoreId, mappingSet.EntityId.QueryForThisEntityId(), Detail.Full)
                    .Single();

            Assert.IsNotNull(addedMappingSet);
            var mapping = new UpdateStatusEntity
            {
                EntityId = addedMappingSet.EntityId,
                ParentId = addedMappingSet.EntityId,
                StoreId = StoreId,
                Column = new DataSetColumnEntity() { Name = "VALID_TO_COL" }
            };
            mapping.Add(ObservationActionEnumType.Added, "insert");
            mapping.Add(ObservationActionEnumType.Updated, "replace");
            mapping.Add(ObservationActionEnumType.Deleted, "removed");
            _persistenceManager.Add(mapping);
            List<UpdateStatusEntity> entities = _entityRetriever.GetEntities<UpdateStatusEntity>(StoreId, addedMappingSet.QueryForThisParentId(), Detail.Full).ToList();
            Assert.That(entities, Is.Not.Null.And.Not.Empty);
            Assert.That(entities.Count, Is.EqualTo(1));
            var entity = entities[0];
            Assert.That(entity.EntityId, Is.EqualTo(addedMappingSet.EntityId));
            Assert.That(entity.Column, Is.Not.Null);
            Assert.That(entity.Column.Name, Is.EqualTo(mapping.Column.Name));
            Assert.That(entity.Transcoding, Is.Not.Empty);
            Assert.That(entity.HasTranscoding());
            Assert.That(entity.Transcoding.Count, Is.EqualTo(3));
            Assert.That(entity.Transcoding["insert"], Is.EqualTo(ObservationActionEnumType.Added));
            Assert.That(entity.Transcoding["replace"], Is.EqualTo(ObservationActionEnumType.Updated));
            Assert.That(entity.Transcoding["removed"], Is.EqualTo(ObservationActionEnumType.Deleted));

        }
        [Test]
        public void ShouldAddUpdateStatusConstant()
        {
            var mappingSet = GetMappingSetEntity("test name 9", _dataset.EntityId, _dataflowObject.Urn.ToString());
            mappingSet.EntityId = string.Empty;
            DeleteByName(mappingSet);
            _persistenceManager.Add(mappingSet);

            var addedMappingSet = _entityRetriever.GetEntities<MappingSetEntity>(StoreId, mappingSet.EntityId.QueryForThisEntityId(), Detail.Full)
                    .Single();

            Assert.IsNotNull(addedMappingSet);
            var mapping = new UpdateStatusEntity
            {
                EntityId = addedMappingSet.EntityId,
                ParentId = addedMappingSet.EntityId,
                StoreId = StoreId,
                Constant = ObservationAction.GetFromEnum(ObservationActionEnumType.Updated)
                };
            _persistenceManager.Add(mapping);
            List<UpdateStatusEntity> entities = _entityRetriever.GetEntities<UpdateStatusEntity>(StoreId, addedMappingSet.QueryForThisParentId(), Detail.Full).ToList();
            Assert.That(entities, Is.Not.Null.And.Not.Empty);
            Assert.That(entities.Count, Is.EqualTo(1));
            var entity = entities[0];
            Assert.That(entity.EntityId, Is.EqualTo(addedMappingSet.EntityId));
            Assert.That(entity.Column, Is.Null);
            Assert.That(entity.Constant, Is.EqualTo(mapping.Constant));
            Assert.That(entity.Transcoding, Is.Empty);
        }


        [Test]
        public void ShouldUpdateValidFromOfMappingSet()
        {
            var mappingSet = GetMappingSetEntity("test name 2", _dataset.EntityId, _dataflowObject.Urn.ToString());
            DeleteByName(mappingSet);
            _persistenceManager.Add(mappingSet);

            var addedMappingSet = _entityRetriever.GetEntities<MappingSetEntity>(StoreId, mappingSet.EntityId.QueryForThisEntityId(), Detail.Full).Single();
            Assert.That(addedMappingSet.ValidFrom, Is.Null);
            var dateTime = DateTime.Parse("2025-01-13T14:30:00");
            mappingSet.ValidFrom = dateTime;
            _persistenceManager.Add(mappingSet);
            addedMappingSet = _entityRetriever.GetEntities<MappingSetEntity>(StoreId, mappingSet.EntityId.QueryForThisEntityId(), Detail.Full).Single();
            Assert.That(addedMappingSet.ValidFrom, Is.EqualTo(dateTime));
        }

        [Test]
        public void ShouldUpdateNameOfMappingSet()
        {
            var mappingSet = GetMappingSetEntity("test name 3", _dataset.EntityId, _dataflowObject.Urn.ToString());
            DeleteByName(mappingSet);
            _persistenceManager.Add(mappingSet);
            string newName = "NewID_" + mappingSet.GetHashCode();
            mappingSet.Name = newName;
            _persistenceManager.Add(mappingSet);
            var addedMappingSet = _entityRetriever.GetEntities<MappingSetEntity>(StoreId, mappingSet.EntityId.QueryForThisEntityId(), Detail.Full).Single();
            Assert.That(addedMappingSet.Name, Is.EqualTo(newName));
        }

        [Test]
        public void ShouldNotAddNewMappingSetWithOverlappingValidFromTo()
        {
            var mappingSet = GetMappingSetEntity("test name 4", _dataset.EntityId, _dataflowObject.Urn.ToString());
            DeleteByName(mappingSet);
            mappingSet.ValidFrom = DateTime.Parse("2025-01-13T14:30:00");
            mappingSet.ValidTo = DateTime.Parse("2026-01-13T14:30:00");
            _persistenceManager.Add(mappingSet);

            mappingSet.EntityId = string.Empty;
            mappingSet.Name = "NewMappingSet";
            mappingSet.ValidFrom = DateTime.Parse("2025-01-14T14:30:00");
            mappingSet.ValidTo = DateTime.Parse("2026-01-13T14:30:00");
            Assert.Throws<ValidationException>(() => _persistenceManager.Add(mappingSet));
        }

        [Test]
        public void MetadataFlagSaved()
        {
            var mappingSet = GetMappingSetEntity("metadata mapping", _dataset.EntityId, _dataflowObject.Urn.ToString());
            DeleteByName(mappingSet);
            mappingSet.IsMetadata = true;
            mappingSet.Description = $"metadata mapping test {DateTime.Now:dd.MM.yyyy hh:mm:ss}";
            _persistenceManager.Add(mappingSet);
            var addedMappingSet = _entityRetriever.GetEntities<MappingSetEntity>(StoreId, mappingSet.EntityId.QueryForThisEntityId(), Detail.Full).Single();

            Assert.AreEqual(mappingSet.IsMetadata, addedMappingSet.IsMetadata);
        }

        [Test]
        public void ComponentMappingEntityMetadataFlagSaved()
        {
            var mappingSet = GetMappingSetEntity("component mapping test", _dataset.EntityId, _dataflowObject.Urn.ToString());
            DeleteByName(mappingSet);
            mappingSet.IsMetadata = true;
            _persistenceManager.Add(mappingSet);

            Assert.That(mappingSet.EntityId, Is.Not.Null.Or.Empty);

            var entity = new ComponentMappingEntity()
            {
                ParentId = mappingSet.EntityId,
                Type = "test",
                ConstantValue = "constant",
                StoreId = this.StoreId,
                IsMetadata = true,
                MetadataAttributeSdmxId = "TEST"
            };

            this._persistenceManager.Add(entity);

            var addedComponentMappingEntity = _entityRetriever
                .GetEntities<ComponentMappingEntity>(StoreId, entity.EntityId.QueryForThisEntityId(), Detail.Full)
                .Single();

            Assert.IsNotNull(addedComponentMappingEntity.Component);
            Assert.AreEqual(entity.EntityId, addedComponentMappingEntity.EntityId);
            Assert.AreEqual(entity.IsMetadata, addedComponentMappingEntity.IsMetadata);
            Assert.AreEqual(entity.MetadataAttributeSdmxId, addedComponentMappingEntity.MetadataAttributeSdmxId);
        }

        private void ReBuildMappingStore()
        {
            try
            {
                InitializeMappingStore(this.StoreId);
                _dataflowObject = BuildStructuralMetadata();
                var connection = AddADdbConnection();
                this._dataset = AddDataSet(connection, _dataflowObject.DataStructureRef, 0);
                this._mappingSet = AddSourceMappingSet(_dataflowObject, _dataset.EntityId);
            }
            catch (Exception e)
            {
                _log.Error(e);
                Console.WriteLine(e);
                throw;
            }
        }

        private IDataflowObject BuildStructuralMetadata()
        {
            IMutableObjects mutableObjects = new MutableObjectsImpl();
            IConceptSchemeMutableObject conceptScheme = new ConceptSchemeMutableCore();
            SetupMaintainable(conceptScheme, "TEST_CS");

            AddConcept(conceptScheme, "FREQ");
            AddConcept(conceptScheme, DimensionObject.TimeDimensionFixedId);
            AddConcept(conceptScheme, PrimaryMeasure.FixedId);

            mutableObjects.AddConceptScheme(conceptScheme);

            ICodelistMutableObject cl = new CodelistMutableCore();
            SetupMaintainable(cl, "CL_FREQ");

            var c1 = new CodeMutableCore() { Id = "A" };
            c1.AddName("en", "Annual");
            cl.AddItem(c1);
            var c2 = new CodeMutableCore() { Id = "M" };
            c2.AddName("en", "Monthly");
            cl.AddItem(c2);

            mutableObjects.AddCodelist(cl);

            IDataStructureMutableObject dsd = new DataStructureMutableCore();
            SetupMaintainable(dsd, "TEST_DSD");
            dsd.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "FREQ"),
                new StructureReferenceImpl("TEST", "CL_FREQ", "1.0", SdmxStructureEnumType.CodeList));

            IDimensionMutableObject timeDimension = new DimensionMutableCore();
            timeDimension.ConceptRef = new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, DimensionObject.TimeDimensionFixedId);
            timeDimension.TimeDimension = true;
            dsd.AddDimension(timeDimension);

            dsd.AddPrimaryMeasure(
                new StructureReferenceImpl(
                    "TEST",
                    "TEST_CS",
                    "1.0",
                    SdmxStructureEnumType.Concept,
                    PrimaryMeasure.FixedId));
            mutableObjects.AddDataStructure(dsd);

            _dataStructureObject = dsd.ImmutableInstance;
            var dataflow = new DataflowMutableCore(_dataStructureObject);
            mutableObjects.AddDataflow(dataflow);
            var artefactImportStatuses = new List<ArtefactImportStatus>();
            var structurePersistenceManager =
                new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(
                    _connectionStringSettings,
                    artefactImportStatuses);

            structurePersistenceManager.SaveStructures(mutableObjects.ImmutableObjects);
            Assert.That(
                artefactImportStatuses.All(status => status.ImportMessage.Status != ImportMessageStatus.Error),
                string.Join("\n", artefactImportStatuses.Select(status => status.ImportMessage.Message)));

            return dataflow.ImmutableInstance;
        }

        private void DeleteByName<T>(T entity) where T : ISimpleNameableEntity
        {
            var entities = _entityRetriever.GetEntities<T>(this.StoreId,
                new EntityQuery() { ObjectId = new Criteria<string>(OperatorType.Exact, entity.Name) }, Detail.IdOnly);
            foreach (var simpleNameableEntity in entities)
            {
                _persistenceManager.Delete<T>(StoreId, simpleNameableEntity.EntityId);
            }
        }
        private void SetupMaintainable(IMaintainableMutableObject maintainable, string id)
        {
            maintainable.Version = "1.0";
            maintainable.AgencyId = "TEST";
            maintainable.Id = id;
            maintainable.AddName("en", $"Test {id}");
            maintainable.FinalStructure = TertiaryBool.ParseBoolean(true);
        }

        private void AddConcept(IConceptSchemeMutableObject cs, string id)
        {
            var c = new ConceptMutableCore() { Id = id };
            c.AddName("en", $"Name of $id");
            cs.AddItem(c);
        }

        private MappingSetEntity AddSourceMappingSet(IDataflowObject dataflowObject, string datasetEntityId)
        {
            var sourceUrn = dataflowObject.Urn.ToString();
            var mappingSetEntity = GetMappingSetEntity("test name", datasetEntityId, sourceUrn);

            var addedMappingSet = _persistenceManager.Add(mappingSetEntity);

            var datasetColumns = _entityRetriever.GetEntities<DataSetColumnEntity>(
                StoreId,
                datasetEntityId.QueryForThisParentId(),
                Detail.Full).ToArray();

            Assert.That(dataflowObject, Is.Not.Null, sourceUrn);
            var mutableObjects = new MutableObjectsImpl();
            mutableObjects.AddIdentifiables(GetMutableArtefats<IDataStructureMutableObject>(StoreId, dataflowObject.DataStructureRef).ToArray());

            // try to get dependencies manually since we don't have access to CommonSdmxObjectRetrieval impl in maapi

            var dsdAndDepds = mutableObjects.ImmutableObjects;

            var dsd = dsdAndDepds.DataStructures.First();

            foreach (var component in dsd.Components)
            {
                ComponentMappingEntity componentMapping = new ComponentMappingEntity();
                componentMapping.StoreId = StoreId;
                componentMapping.ParentId = addedMappingSet.EntityId;
                componentMapping.Type = ComponentMappingType.Normal.AsMappingStoreType();

                ICodelistObject codelist = null;
                if (component.HasCodedRepresentation())
                {
                    codelist = GetMutableArtefats<ICodelistMutableObject>(StoreId, component.Representation.Representation).First().ImmutableInstance;
                }

                var dataAttribute = component as IAttributeObject;
                var dimension = component as IDimension;
                var dataSetColumnEntities = datasetColumns.Where(entity => entity.Name.Contains("_" + component.Id)).ToArray();
                if ((dimension == null || !dimension.TimeDimension))
                {
                    // no transcoding yet
                    dataSetColumnEntities = dataSetColumnEntities.Where(entity => entity.Name.EndsWith(component.Id)).Take(1).ToArray();
                }

                componentMapping.SetColumns(dataSetColumnEntities);

                componentMapping.Component = new Component { ObjectId = component.Id };

                if (dataSetColumnEntities.Length == 0)
                {
                    if (dataAttribute == null || dataAttribute.Mandatory)
                    {
                        componentMapping.ConstantValue = codelist != null ? codelist.Items.First().Id : "Test value";
                    }
                }

                var addedComponentEntity = _persistenceManager.Add(componentMapping);

                if (dataSetColumnEntities.Length > 1)
                {
                    if (dimension != null && dimension.TimeDimension)
                    {
                        TimeDimensionMappingEntity timeDimensionMapping = new TimeDimensionMappingEntity();
                        timeDimensionMapping.EntityId = addedMappingSet.EntityId;
                        timeDimensionMapping.StoreId = addedMappingSet.StoreId;
                        TimeTranscodingEntity timeTranscodingEntity = new TimeTranscodingEntity();
                        timeTranscodingEntity.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.QuarterOfYear).FrequencyCode;
                        timeTranscodingEntity.Period = new PeriodTimeTranscoding();
                        timeTranscodingEntity.Period.Column = dataSetColumnEntities[1];
                        timeTranscodingEntity.Period.AddRule("Q1", "T1");
                        timeTranscodingEntity.Period.AddRule("Q2", "T2");
                        timeTranscodingEntity.Period.Start = 1;
                        timeTranscodingEntity.Period.Length = 1;

                        timeTranscodingEntity.Year = new TimeTranscoding();
                        timeTranscodingEntity.Year.Column = dataSetColumnEntities[0];
                        timeTranscodingEntity.Year.Start = 0;
                        timeTranscodingEntity.Year.Length = 0;

                        timeDimensionMapping.ConvertFromTranscoding(new[] { timeTranscodingEntity }, dsd.FrequencyDimension.Id);

                        _persistenceManager.Add(timeDimensionMapping);
                    }
                    else if (codelist != null)
                    {
                        // TODO test script
                        List<TranscodingRuleEntity> rules = new List<TranscodingRuleEntity>();
                        foreach (var item in codelist.Items)
                        {
                            TranscodingRuleEntity transcodingRule = new TranscodingRuleEntity();
                            transcodingRule.ParentId = addedComponentEntity.EntityId;
                            transcodingRule.StoreId = addedComponentEntity.StoreId;

                            transcodingRule.DsdCodeEntity = new IdentifiableEntity(EntityType.Sdmx)
                            {
                                ObjectId = item.Id
                            };
                            transcodingRule.LocalCodes = new List<LocalCodeEntity>();
                            for (int index = 0; index < dataSetColumnEntities.Length; index++)
                            {
                                var dataSetColumnEntity = dataSetColumnEntities[index];
                                var localCodeEntity = new LocalCodeEntity
                                {
                                    ObjectId =
                                                                  string.Format(
                                                                      CultureInfo.InvariantCulture,
                                                                      "{0}_{1}_{2}",
                                                                      dataSetColumnEntity.Name,
                                                                      item.Id,
                                                                      index),
                                    ParentId = dataSetColumnEntity.Name
                                };
                                transcodingRule.LocalCodes.Add(localCodeEntity);
                            }

                            rules.Add(transcodingRule);
                        }

                        _persistenceManager.AddEntities(rules);
                    }
                    else
                    {
                        // TODO test script
                        List<TranscodingRuleEntity> rules = new List<TranscodingRuleEntity>();
                        var uncodedValues = new string[] { "uncoded 1", "uncoded 2" };
                        foreach (var uncodedValue in uncodedValues)
                        {

                            TranscodingRuleEntity transcodingRule = new TranscodingRuleEntity();
                            transcodingRule.ParentId = addedComponentEntity.EntityId;
                            transcodingRule.StoreId = addedComponentEntity.StoreId;
                            transcodingRule.UncodedValue = uncodedValue;
                            transcodingRule.LocalCodes = new List<LocalCodeEntity>();
                            for (int index = 0; index < dataSetColumnEntities.Length; index++)
                            {
                                var dataSetColumnEntity = dataSetColumnEntities[index];
                                var localCodeEntity = new LocalCodeEntity
                                {
                                    ObjectId =
                                                                  string.Format(
                                                                      CultureInfo.InvariantCulture,
                                                                      "{0}_{1}_{2}",
                                                                      dataSetColumnEntity.Name,
                                                                      uncodedValue,
                                                                      index),
                                    ParentId = dataSetColumnEntity.Name
                                };
                                transcodingRule.LocalCodes.Add(localCodeEntity);
                            }

                            rules.Add(transcodingRule);
                        }
                        _persistenceManager.AddEntities(rules);
                    }
                }
            }

            return addedMappingSet;
        }

        private MappingSetEntity GetMappingSetEntity(string name, string datasetEntityId, string sourceUrn)
        {
            return
                new MappingSetEntity
                {
                    StoreId = StoreId,
                    DataSetId = datasetEntityId,
                    Name = name,
                    ParentId = sourceUrn,
                    Description = "a description"
                };
        }
    }
}