﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System.Configuration;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Engine;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Config;

    using NSubstitute;

    using NUnit.Framework;

    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class RetrieveMappingSetTest
    {
        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        private readonly ConnectionStringSettings _connectionStringSettings;

        public RetrieveMappingSetTest(string name)
        {
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(name);
        }

        [Test]
        public void GetAllMappingSet()
        {
            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] {this._connectionStringSettings});
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new MappingSetFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<MappingSetEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.AnyValue, null));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
            //Assert.That(result.First().Name, Is.Not.Null);

        }

        [Test]
        public void GetAMappingSet()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] {this._connectionStringSettings});
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new MappingSetFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<MappingSetEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.Exact, "1"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
            //Assert.That(result.First().Name, Is.Not.Null);
        }


        [Test]
        public void GetAMappingSetBasedOnAdditionalCriteria()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] {this._connectionStringSettings});
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new MappingSetFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<MappingSetEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>()
            {
                {EntityType.DataSet.ToString(), new Criteria<string>(OperatorType.Exact, "3")}
            });
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
        }


        [Test]
        public void GetAMappingSetBasedOnParentId()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] {this._connectionStringSettings});
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new MappingSetFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<MappingSetEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.ParentId.Returns(new Criteria<string>(OperatorType.Exact, "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_M(2.0)"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
            //Assert.That(result.First().Name, Is.EqualTo("typea"));
        }

        [Test]
        public void GetAMappingSetBasedOnParentIdExclusion()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] {this._connectionStringSettings});
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new MappingSetFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<MappingSetEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.ParentId.Returns(new Criteria<string>(OperatorType.Not | OperatorType.Contains, "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_M(2.0)"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
            //Assert.That(result.First().Name, Is.EqualTo("MS_6_157_80"));
        }
    }
}
