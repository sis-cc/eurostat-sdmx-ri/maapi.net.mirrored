using System.Configuration;
using System.IO;
using System.Linq;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Config;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NSubstitute;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{

    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class DatasetPersistenceTests: BaseTestClassWithContainer
    {
        public DatasetPersistenceTests(string storeId) : base(storeId)
        {
        }

        [Test]
        public void ShouldUpdateADataset()
        {
            var patch = new PatchRequest
            {
                new PatchDocument("replace", "name", "MyDS"),
                new PatchDocument("replace", "description", "replace test"),
                new PatchDocument("replace", "query", "SELECT * FROM TEST"),
                new PatchDocument("replace", "userId", "1"),
                new PatchDocument("replace", "editorType", "org.estat.ma.gui.userControls.dataset.QueryEditor.CustomQueryEditor, MappingAssistant")
            };
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] {connectionStringSettings});
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(StoreId);
            entityPersistenceEngine.Update(patch, EntityType.DataSet, "44");
        }

        [Test]
        public void ShouldUpdateADatasetWithPermissions()
        {
            var input = @"[

    {
        ""op"": ""replace"",
		""path"": ""/description"",
		""value"": """"

    },
	{
                ""op"": ""replace"",
		""path"": ""/query"",
		""value"": ""select STS_SDMX_DATA1.id as STS_SDMX_DATA1_id, STS_SDMX_DATA1.FREQ as STS_SDMX_DATA1_FREQ, STS_SDMX_DATA1.REF_AREA as STS_SDMX_DATA1_REF_AREA, STS_SDMX_DATA1.ADJUSTMENT as STS_SDMX_DATA1_ADJUSTMENT, STS_SDMX_DATA1.STS_INDICATOR as STS_SDMX_DATA1_STS_INDICATOR, STS_SDMX_DATA1.STS_ACTIVITY as STS_SDMX_DATA1_STS_ACTIVITY, STS_SDMX_DATA1.STS_INSTITUTION as STS_SDMX_DATA1_STS_INSTITUTION, STS_SDMX_DATA1.STS_BASE_YEAR as STS_SDMX_DATA1_STS_BASE_YEAR, STS_SDMX_DATA1.TIME_PERIOD as STS_SDMX_DATA1_TIME_PERIOD, STS_SDMX_DATA1.OBS_VALUE as STS_SDMX_DATA1_OBS_VALUE, STS_SDMX_DATA1.OBS_COM as STS_SDMX_DATA1_OBS_COM, STS_SDMX_DATA1.OBS_CONF as STS_SDMX_DATA1_OBS_CONF, STS_SDMX_DATA1.OBS_PRE_BREAK as STS_SDMX_DATA1_OBS_PRE_BREAK, STS_SDMX_DATA1.OBS_STATUS as STS_SDMX_DATA1_OBS_STATUS, STS_SDMX_DATA1.AVAILABILITY as STS_SDMX_DATA1_AVAILABILITY, STS_SDMX_DATA1.BREAKS as STS_SDMX_DATA1_BREAKS, STS_SDMX_DATA1.COLLECTION as STS_SDMX_DATA1_COLLECTION, STS_SDMX_DATA1.DOM_SER_IDS as STS_SDMX_DATA1_DOM_SER_IDS, STS_SDMX_DATA1.TIME_FORMAT as STS_SDMX_DATA1_TIME_FORMAT, STS_SDMX_DATA1.UNIT_INDEX_BASE as STS_SDMX_DATA1_UNIT_INDEX_BASE, STS_SDMX_DATA1.COMPILATION as STS_SDMX_DATA1_COMPILATION, STS_SDMX_DATA1.COVERAGE as STS_SDMX_DATA1_COVERAGE, STS_SDMX_DATA1.DECIMALS as STS_SDMX_DATA1_DECIMALS, STS_SDMX_DATA1.NAT_TITLE as STS_SDMX_DATA1_NAT_TITLE, STS_SDMX_DATA1.SOURCE_AGENCY as STS_SDMX_DATA1_SOURCE_AGENCY, STS_SDMX_DATA1.SOURCE_PUB as STS_SDMX_DATA1_SOURCE_PUB, STS_SDMX_DATA1.TITLE as STS_SDMX_DATA1_TITLE, STS_SDMX_DATA1.TITLE_COMPL as STS_SDMX_DATA1_TITLE_COMPL, STS_SDMX_DATA1.UNIT as STS_SDMX_DATA1_UNIT, STS_SDMX_DATA1.UNIT_MULT as STS_SDMX_DATA1_UNIT_MULT\r\n from STS_SDMX_DATA STS_SDMX_DATA1\r\n""

    },
	{
                ""op"": ""replace"",
		""path"": ""/name"",
		""value"": ""Copy of type_a (1)""

    },
	{
                ""op"": ""replace"",
		""path"": ""/permissions"",
		""value"": {
                    ""urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:CH(1.0).CH_1"": ""read_write"",
			""urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:EGR_CORE_LEU(1.0)"": ""read_write"",
			""urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=SDMXSOURCE:DF_WDI(1.0)"": ""read_write"",
			""urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:EGR_CORE_LEL(1.0)"": ""read_write"",
			""urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:NAMAIN_IDC_N(1.9)"": ""read_write""

        }
            }
]";

            JToken token = null;
            using (TextReader reader = new StringReader(input))
            using (var jsonTextReader = new JsonTextReader(reader))
            {
                var inputtoken = JToken.Load(jsonTextReader);
                JArray array = (JArray)inputtoken;
                foreach (JObject row in array)
                {
                    token = row.GetValue("value");
                }
            }
            var patch = new PatchRequest
            {
                new PatchDocument("replace", "/permissions",token)
            };
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(StoreId);
            entityPersistenceEngine.Update(patch, EntityType.DataSet, "44");

            var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            var dataSetEntityRetriever = entityRetrieverManager.GetRetrieverEngine<DatasetEntity>(StoreId);
            var dataset = dataSetEntityRetriever.GetEntities(
                    new EntityQuery { EntityId = new Criteria<string>(OperatorType.Exact, "44") },
                    Detail.Full).FirstOrDefault();
            Assert.That(dataset.Permissions.Count(), Is.EqualTo(5));
        }
    }
}