﻿namespace Estat.Sri.Mapping.MappingStore.Tests
{
    public interface IRoundTripEntitiesForMADB70
    {
        void DatabaseConnectionEntityTest(string mappingStoreIdentifier);
        void DatasetcolumnEntityTest(string mappingStoreIdentifier);
        void DatasetEntityTest(string mappingStoreIdentifier);
        void DescSourceEntityTest(string mappingStoreIdentifier);
        void MappingSetEntityTest(string mappingStoreIdentifier);
        void ShouldInsertAdvanceTimeTranscodingWithDuration(string mappingStoreIdentifier);
        void TestComponentMappingWithColumns(string mappingStoreIdentifier);
        void TestTimeTranscodingRule(string mappingStoreIdentifier);
        void TestTranscodingRule(string mappingStoreIdentifier);
    }
}