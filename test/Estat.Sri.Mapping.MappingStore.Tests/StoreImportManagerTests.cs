// -----------------------------------------------------------------------
// <copyright file="StoreImportManagerTests.cs" company="EUROSTAT">
//   Date Created : 2017-08-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using DryIoc;
using Estat.Sdmxsource.Extension.Engine;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.MappingStore.Store.Factory;
using Estat.Sri.MappingStoreRetrieval.Factory;
using Estat.Sri.MappingStoreRetrieval.Helper;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.XmlHelper;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Util.Io;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("sqlserver", "msdb_scratch.sqlserver")]
    [TestFixture("oracle", "msdb_scratch.oracle")]
    [TestFixture("mariadb", "msdb_scratch.mariadb")]
    internal class StoreImportManagerTests : BaseTestClassWithContainer
    {
        private readonly string _target;
        private IStructureSubmitterEngine _submitterEngine;
        private readonly IReadableDataLocationFactory _readableDataLocationFactory = new ReadableDataLocationFactory();
        private readonly IStructureParsingManager _parsingManager = new StructureParsingManager();

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public StoreImportManagerTests(string source, string target)
            : base(source)
        {
            this._target = target;
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["MappingStoreRetrieversFactory"]);
            var submitFactory = new StructureSubmitMappingStoreFactory((storeId) => GetConnectionStringSettings());
            _submitterEngine = submitFactory.GetEngine(source);
        }

        private static void Error(object sender, ValidationEventArgs args)
        {
            throw args.Exception;
        }

        [Test]
        public void ShouldExport()
        {
            var storeExportManager = this.IoCContainer.Resolve<IStoreExporttManager>();
            var storeImportManager = this.IoCContainer.Resolve<IStoreImportManager>();
            var exportFile = new FileInfo("importExport.json");
            using (var fileStream = exportFile.Create())
            {
                storeExportManager.Export(fileStream, Encoding.UTF8, this.StoreId);
            }
        }

        [Test]
        public void ShouldImportInNewMSDB()
        {
            string structuresFile = @"tests/dsd_new_feature_example.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = _submitterEngine.SubmitStructures(sdmxObjects);
            var storeImportManager = this.IoCContainer.Resolve<IStoreImportManager>();
            var storeExportManager = this.IoCContainer.Resolve<IStoreExporttManager>();
            var exportFile = new FileInfo("importExport.zip");
            using (var fileStream = exportFile.Create())
            {
                storeExportManager.ExportWithSdmx(fileStream, Encoding.UTF8, this.StoreId);
            }

            this.InitializeMappingStore(this._target);
            using (var fileStream = File.OpenRead("importExport.zip"))
            {
                storeImportManager.ImportWithSdmx(fileStream, Encoding.UTF8, this._target);
            }
        }

        [Test]
        public void ShouldImportWithNewNamesBecauseTheDataIsAlreadyInTheDb()
        {
            var storeExportManager = this.IoCContainer.Resolve<IStoreExporttManager>();
            var exportFile = new FileInfo("importExport.xml");
            using (var fileStream = exportFile.Create())
            {
                storeExportManager.Export(fileStream, Encoding.UTF8, this.StoreId);
            }
            var storeImportManager = this.IoCContainer.Resolve<IStoreImportManager>();
            using (var fileStream = exportFile.OpenRead())
            {
                storeImportManager.Import(fileStream, Encoding.UTF8, this.StoreId);
            }

            // FIXME test doesn't verify anything
        }
        [Test]
        public void ShouldExportWithoutPassword()
        {
            var storeExportManager = this.IoCContainer.Resolve<IStoreExporttManager>();
            using MemoryStream memoryStream = new MemoryStream();
            storeExportManager.Export(memoryStream, Encoding.UTF8, this.StoreId);
            var export = Encoding.UTF8.GetString(memoryStream.ToArray());
            Assert.IsTrue(export.Contains("ddb", System.StringComparison.Ordinal));
            JObject o = JObject.Parse(export);
            var properties = o.SelectTokens("$.mappingStore.ddb[*].properties").SelectMany(p => p).Cast<JProperty>();
            foreach (var property in properties)
            {
                Assert.AreNotEqual("password", property.Name.ToLowerInvariant());
                Assert.AreNotEqual("pwd", property.Name.ToLowerInvariant());
                Assert.IsFalse(property.ToString().Contains("\"123\"", System.StringComparison.OrdinalIgnoreCase));
            }
        }


        private ISdmxObjects ReadStructures(string filepath, SdmxSchemaEnumType sdmxSchema)
        {
            FileInfo fileInfo = new FileInfo(filepath);
            using (IReadableDataLocation rdl = _readableDataLocationFactory.GetReadableDataLocation(fileInfo))
            {
                IStructureWorkspace workspace = _parsingManager.ParseStructures(rdl);

                ISdmxObjects structureBeans = workspace.GetStructureObjects(false);

                XMLParser.ValidateXml(rdl, sdmxSchema);

                return structureBeans;
            }
        }
    }
}