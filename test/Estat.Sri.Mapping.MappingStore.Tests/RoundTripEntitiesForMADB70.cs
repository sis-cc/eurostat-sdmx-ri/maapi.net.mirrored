using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NSubstitute;
using NUnit.Framework;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.Api.Model.AdvancedTime;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Estat.Sri.Mapping.Api.Extension;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Utils;
using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using DataSetColumnEntity = Estat.Sri.Mapping.Api.Model.DataSetColumnEntity;
using MappingSetEntity = Estat.Sri.Mapping.Api.Model.MappingSetEntity;
using TimeTranscodingEntity = Estat.Sri.Mapping.Api.Model.TimeTranscodingEntity;
using TranscodingEntity = Estat.Sri.Mapping.Api.Model.TranscodingEntity;
using System.Threading;
using System.Security.Principal;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using System.Diagnostics.CodeAnalysis;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStore.Store;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.Mapping.Api.Exceptions;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class RoundTripEntitiesForMADB70 : BaseTestClassWithContainer
    {
        /// <summary>
        /// The persist manager
        /// </summary>
        private readonly IEntityPersistenceManager _persistManager;

        private readonly IEntityRetrieverManager _retrieveManager;
   /// <summary>
        /// The connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;
        private IEqualityComparer<DataSetColumnEntity> comparer;

        public RoundTripEntitiesForMADB70(string storeId) : base(storeId)
        {
            this._persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            this._retrieveManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            _connectionStringSettings = GetConnectionStringSettings();
            comparer = new DataSetColumnNameComparer();
        }
        

        [Test]
        public void DatabaseConnectionEntityTest()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(this.StoreId);
            var name = "unique name 1";
            var entity = new DdbConnectionEntity()
            {
                Name = name,
                AdoConnString = "ado conn",
                DbType = "SqlServer",
                DbUser = "ddb",
                Password = "my pass",
                Description = "This test cannot be run twice unless the delete is also called",
                IsEncrypted = true,
                StoreId = this.StoreId // TODO do we need this ?
                
            };
            DeleteByName(entity);
            _persistManager.Add(entity);
            //retrieve
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DdbConnectioSettingsRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<DdbConnectionEntity>(connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>()
            {
                { "Name",new Criteria<string>(OperatorType.Exact,name )}
            });
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsTrue(result.Any());
            //update
            
            //TODO check update  
        //    name = "update name";
            //var patch = new PatchRequest()
            //{
            //    new PatchDocument("replace", "/" + nameof(DdbConnectionEntity.Name),name),
            //};
            
            //entityPersistenceEngine.Update(patch, EntityType.DdbConnectionSettings, result.First().EntityId.ToString());

            //query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>()
            //{
            //    { "Name",new Criteria<string>(OperatorType.Exact,name )}
            //});
            //result = engine.GetEntities(query, Detail.Full).ToArray();

            //Assert.IsTrue(result.Any());

            //delete
            entityPersistenceEngine.Delete(result.First().EntityId.ToString(),EntityType.DdbConnectionSettings);
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>()
            {
                { "Name",new Criteria<string>(OperatorType.Exact,name )}
            });
            result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsTrue(!result.Any());
        }

        [Test]
        public void DescSourceEntityTest()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(this.StoreId);
            var entity = new ColumnDescriptionSourceEntity()
            {
                DescriptionTable = "my table",
                RelatedField = "my related field",
                DescriptionField = "my description field",
                ParentId = "23"
            };
            var id = entityPersistenceEngine.Add(entity);

            //retrieve
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DescSourceRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<ColumnDescriptionSourceEntity>(connectionStringSettings.Name);
            IEntityQuery query = new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, id.ToString() )};
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsTrue(result.Any());


            //update
            var updatedDescField = "updatedDescField";
            var patch = new PatchRequest()
            {
                new PatchDocument("replace", "/" + nameof(ColumnDescriptionSourceEntity.DescriptionField),updatedDescField),
            };
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            entityPersistenceEngine.Update(patch, EntityType.DescSource, id.ToString());
            result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.That(result.FirstOrDefault().DescriptionField, Is.EqualTo(updatedDescField));

            //delete
            entityPersistenceEngine.Delete(id.ToString(), EntityType.DescSource);
            result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsTrue(!result.Any());
        }

        [Test]
        public void DatasetcolumnEntityTest()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(this.StoreId);
            var entity = new DataSetColumnEntity()
            {
                ParentId = "49",
                LastRetrieval = DateTime.Today,
                Name = "test",
                Description = "test"
                
            };
            var id = entityPersistenceEngine.Add(entity);

            //retrieve
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DatasetColumnRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<DataSetColumnEntity>(connectionStringSettings.Name);
            IEntityQuery query = new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, id.ToString()) };
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsTrue(result.Any());


            //update
            //var name = "updatedDescField";
            //var patch = new PatchRequest()
            //{
            //    new PatchDocument("replace", "/" + nameof(DataSetColumnEntity.Name),name),
            //};
            //configurationStoreManager.GetSettings<ConnectionStringSettings>()
            //   .Returns(new[] { connectionStringSettings });
            //entityPersistenceEngine.Update(patch, EntityType.DescSource, id.ToString());
            //result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result.FirstOrDefault().Name, Is.EqualTo(name));

            //delete
            entityPersistenceEngine.Delete(id.ToString(), EntityType.DataSetColumn);
            result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsTrue(!result.Any());
        }

        [Test]
        public void DatasetEntityTest()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(this.StoreId);
            
            var categoryUrn = "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:ESTAT_DATAFLOWS_SCHEME(1.1).14.14200.SSTSCONS";
            var dataflowUrn = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:161_267(1.0)";

            var entity = new DatasetEntity()
            {
                ParentId = "108",
                Query = "test query",
                JSONQuery = "Json query",
                Name = "some name",
                StoreId = StoreId,
                DatasetPropertyEntity = new DatasetPropertyEntity()
                {
                    CrossApplyColumn = "SID",
                    CrossApplySupported = true,
                    OrderByTimePeriodAsc = "SID ASC, PERIOD_START ASC, PERIOD_END DESC",
                    OrderByTimePeriodDesc = "SID DESC, PERIOD_START DESC, PERIOD_END ASC"
                },
                Permissions = new Dictionary<string, string>()
                {
                    { categoryUrn, "read_write" },
                    { dataflowUrn, "read_write" }
                } 
            };
            DeleteByName(entity);
            var insertedEntity = this._persistManager.Add(entity);

            //retrieve
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DatasetRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<DatasetEntity>(connectionStringSettings.Name);
            IEntityQuery query = new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, insertedEntity.EntityId.ToString()) };
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsTrue(result.Any());
            Assert.That(result.FirstOrDefault().DatasetPropertyEntity, Is.Not.Null);
            Assert.IsTrue(result.FirstOrDefault().Permissions.Any());
            Assert.That(result.FirstOrDefault().Permissions.ElementAt(0).Key, Is.EqualTo(categoryUrn));
            Assert.That(result.FirstOrDefault().Permissions.ElementAt(1).Key, Is.EqualTo(dataflowUrn));

            //update
            //var name = "name";
            //var patch = new PatchRequest()
            //{
            //    new PatchDocument("replace", "/" + nameof(DataSetColumnEntity.Name),name),
            //};
            //configurationStoreManager.GetSettings<ConnectionStringSettings>()
            //   .Returns(new[] { connectionStringSettings });
            //entityPersistenceEngine.Update(patch, EntityType.DataSet, id.ToString());
            //result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result.FirstOrDefault().Name, Is.EqualTo(name));

            //delete
            entityPersistenceEngine.Delete(insertedEntity.EntityId.ToString(), EntityType.DataSet);
            result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsTrue(!result.Any());
        }

        [Test]
        public void UpdateADataset()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(this.StoreId);

            var categoryUrn = "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:ESTAT_DATAFLOWS_SCHEME(1.1).14.14200.SSTSCONS";
            var dataflowUrn = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:DEMOGRAPHY(1.0)";

            var entity = new DatasetEntity()
            {
                ParentId = "108",
                Query = "test query",
                JSONQuery = "Json query",
                Name = "some name",
                StoreId = StoreId,
                DatasetPropertyEntity = new DatasetPropertyEntity()
                {
                    CrossApplyColumn = "SID",
                    CrossApplySupported = true,
                    OrderByTimePeriodAsc = "SID ASC, PERIOD_START ASC, PERIOD_END DESC",
                    OrderByTimePeriodDesc = "SID DESC, PERIOD_START DESC, PERIOD_END ASC"
                },
                Permissions = new Dictionary<string, string>()
                {
                    { categoryUrn, "read_write" },
                    { dataflowUrn, "read_write" }
                }
            };
            DeleteByName(entity);
            var insertedEntity = this._persistManager.Add(entity);

            //retrieve
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DatasetRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<DatasetEntity>(connectionStringSettings.Name);
            IEntityQuery query = new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, insertedEntity.EntityId.ToString()) };
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsTrue(result.Any());
            Assert.That(result.FirstOrDefault().DatasetPropertyEntity, Is.Not.Null);
            Assert.IsTrue(result.FirstOrDefault().Permissions.Any());
            Assert.That(result.FirstOrDefault().Permissions.ElementAt(0).Key, Is.EqualTo(categoryUrn));
            Assert.That(result.FirstOrDefault().Permissions.ElementAt(1).Key, Is.EqualTo(dataflowUrn));

            //update
            var name = "name";
            var description = "my new description";
            var newQuery = "just some query";
            var permissions = @"{
    ""urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:157_80(1.0)"": ""read_write"",
    ""urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:161_267(1.0)"": ""read_write"",
    ""urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:31_124(1.0)"": ""read_write"",
    ""urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:HC06(1.0)"": ""read_write"",
    ""urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:STS(3.1)"": ""read_write"",
    ""urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ESTAT:ESTAT_DATAFLOWS_SCHEME(1.1).14.14200.SSTSIND"": ""read_write"",
    
  }";
            var patch = new PatchRequest()
            {
                new PatchDocument("replace", "/" + nameof(DataSetColumnEntity.Name),name),

                new PatchDocument("replace", "/" + nameof(DataSetColumnEntity.Description),description),

                new PatchDocument("replace", "/" + nameof(DataSetEntity.Query),newQuery),

                //new PatchDocument("replace","/permissions" ,permissions),
            };
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            entityPersistenceEngine.Update(patch, EntityType.DataSet, result.FirstOrDefault().EntityId.ToString());
            result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.That(result.FirstOrDefault().Name, Is.EqualTo(name));

            //delete
            entityPersistenceEngine.Delete(insertedEntity.EntityId.ToString(), EntityType.DataSet);
            result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsTrue(!result.Any());
        }

        [Test]
        public void MappingSetEntityTest()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(this.StoreId);
            var database = new Database(connectionStringSettings);
            // use these dataset columns 
            WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder(database);
            var criteria = new Criteria<long>(OperatorType.Exact, 50);
            WhereClauseParameters whereClauseParameters = whereClauseBuilder.Build(criteria, "DS_ID");
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select COL_ID from DATASET_COLUMN where {0}", whereClauseParameters.WhereClause);
            var valuePairs = whereClauseParameters.Parameters.ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);

            var datasetColIds = database.Query<long>(sqlQuery, param).ToArray();
            var outputColumnId = string.Empty;
            var fromColumnId = string.Empty;
            var toColumnId = string.Empty;

            if (datasetColIds.Count() > 3)
            {
                outputColumnId = datasetColIds[0].ToString();
                fromColumnId = datasetColIds[1].ToString();
                toColumnId = datasetColIds[2].ToString();
            }
            var entity = new MappingSetEntity()
            {
                ParentId = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:161_267(1.0)",
                DataSetId = "49",
                ValidFrom = DateTime.Today,
                ValidTo = DateTime.Today.AddDays(1),
                IsMetadata = false,
                Name = "My Mappingset",
                Description = "My Mapping Set description",
                TimePreFormattedEntity = new TimePreFormattedEntity()
                {
                    EntityId = null,
                    OutputColumn = new DataSetColumnEntity() { EntityId = outputColumnId },
                    FromColumn = new DataSetColumnEntity() { EntityId = fromColumnId },
                    ToColumn = new DataSetColumnEntity() { EntityId = toColumnId }
                },
                StoreId = this.StoreId
            };
            DeleteByName(entity);
            var insertedEntity = this._persistManager.Add(entity);

            //retrieve
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new MappingSetFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<MappingSetEntity>(connectionStringSettings.Name);
            IEntityQuery query = new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, insertedEntity.EntityId.ToString()) };
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsTrue(result.Any());
            Assert.That(result.FirstOrDefault().TimePreFormattedEntity, Is.Not.Null);

            //delete
            _persistManager.Delete<MappingSetEntity>(this.StoreId, insertedEntity.EntityId);
            result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsTrue(!result.Any());
        }

        private void DeleteByName<T>(T entity) where T : ISimpleNameableEntity
        {
            var entities = _retrieveManager.GetEntities<T>(this.StoreId,
                new EntityQuery() { ObjectId = new Criteria<string>(OperatorType.Exact, entity.Name) }, Detail.IdOnly);
            foreach (var simpleNameableEntity in entities)
            {
                _persistManager.Delete<T>(StoreId, simpleNameableEntity.EntityId);
            }
        }


        private static TimeFormatConfiguration AddStartDurationDateTime(DurationMappingEntity roundTripEntity, IList<DataSetColumnEntity> dataSetColumnEntities)
        {
            var timeFormatConfiguration = new TimeFormatConfiguration();
            timeFormatConfiguration.OutputFormat = TimeFormat.TimeRange;
            var startConfig = TimeParticleConfiguration.CreateDateFormat(dataSetColumnEntities[1]);
            var durationMap = roundTripEntity;
            timeFormatConfiguration.SetConfiguration(startConfig, durationMap);
            return timeFormatConfiguration;
        }

        [Test]
        public void TestComponentMappingWithColumns()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            var mappingStoreManager = Substitute.For<IMappingStoreManager>();
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new MappingSetFactory(databaseManager),new DatasetColumnRetrieverFactory(databaseManager),new ComponentMappingRetrieverFactory(databaseManager, mappingStoreManager));
            var x = retrieverManager.GetEntities<MappingSetEntity>(StoreId, new EntityQuery() { }, Detail.Full);
            var mappingSetEntities = x.FirstOrDefault(setEntity => setEntity.EntityId == "40");
            var datasetColumns = retrieverManager.GetEntities<DataSetColumnEntity>(
                this.StoreId,
                mappingSetEntities.DataSetId.QueryForThisParentId(),
                Detail.Full).Distinct(comparer);

            var entity = new ComponentMappingEntity()
            {
                ParentId = mappingSetEntities.EntityId,
                Component = new Component() { ObjectId = "BREAK", EntityId = "1" },
                StoreId = this.StoreId
            };
            entity.SetColumns(datasetColumns.ToArray());
            string entityId = null;
            try
            {
                var entityWithEntityId = this._persistManager.Add(entity);
                entityId = entityWithEntityId.EntityId;
                Assert.That(entityId, Is.Not.Null.Or.Empty);

                var roundTripEntity = retrieverManager.GetEntities<ComponentMappingEntity>(
                    StoreId,
                    entityId.QueryForThisEntityId(),
                    Detail.Full).Single();
                Assert.That(roundTripEntity.Component, Is.Not.Null);
                Assert.That(roundTripEntity.EntityId, Is.EqualTo(entityId));
                Assert.That(roundTripEntity.GetColumns(), Is.Not.Null);
                Assert.That(roundTripEntity.GetColumns(), Is.Not.Empty);
                Assert.AreEqual(roundTripEntity.GetColumns().Count, entity.GetColumns().Count);
                Assert.AreEqual(roundTripEntity.GetColumns().Count, entity.GetColumns().Distinct().Count());
                Assert.AreEqual(roundTripEntity.GetColumns().OrderBy(columnEntity => columnEntity.Name).First().Name, entity.GetColumns().OrderBy(columnEntity => columnEntity.Name).First().Name);
                Assert.That(roundTripEntity.Component.ObjectId, Is.EqualTo(entity.Component.ObjectId));
            }
            finally
            {
                if (entityId != null)
                {
                    this._persistManager.Delete<ComponentMappingEntity>(StoreId, entityId);
                }
            }
        }

        [Test]
        public void ShouldInsertComponentMappingWithColumnsAndDefaultValue()
        {
            var x = _retrieveManager.GetEntities<MappingSetEntity>(StoreId, new EntityQuery() { }, Detail.Full);
            var mappingSetEntities = x.First(setEntity => setEntity.EntityId == "40");
            var datasetColumns = _retrieveManager.GetEntities<DataSetColumnEntity>(
                this.StoreId,
                mappingSetEntities.DataSetId.QueryForThisParentId(),
                Detail.Full).Distinct(comparer);

            // get existing mappings and delete BREAK 
            var existingBreakMapping = _retrieveManager.GetEntities<ComponentMappingEntity>(StoreId, mappingSetEntities.QueryForThisParentId(), Detail.Full).FirstOrDefault(c => c.Component.ObjectId.Equals("BREAK", StringComparison.Ordinal));
            if (existingBreakMapping != null)
            {
                _persistManager.Delete<ComponentMappingEntity>(StoreId, existingBreakMapping.EntityId);
            }

            var entity = new ComponentMappingEntity()
            {
                ParentId = mappingSetEntities.EntityId,
                Component = new Component() {ObjectId="BREAK" },
                DefaultValue = "TEST123",
                StoreId = this.StoreId
            };
            entity.SetColumns(datasetColumns.ToArray());
            string entityId = null;
            try
            {
                var entityWithEntityId = this._persistManager.Add(entity);
                entityId = entityWithEntityId.EntityId;
                Assert.That(entityId, Is.Not.Null.Or.Empty);

                var roundTripEntity = _retrieveManager.GetEntities<ComponentMappingEntity>(
                    StoreId,
                    entityId.QueryForThisEntityId(),
                    Detail.Full).Single();
                Assert.That(roundTripEntity.Component, Is.Not.Null);
                Assert.That(roundTripEntity.EntityId, Is.EqualTo(entityId));
                Assert.That(roundTripEntity.GetColumns(), Is.Not.Null);
                Assert.That(roundTripEntity.GetColumns(), Is.Not.Empty);
                Assert.That(roundTripEntity.GetColumns().Count, Is.EqualTo(entity.GetColumns().Count));
                Assert.AreEqual(roundTripEntity.GetColumns().Count, entity.GetColumns().Distinct().Count());
                Assert.AreEqual(roundTripEntity.GetColumns().OrderBy(columnEntity => columnEntity.Name).First().Name, entity.GetColumns().OrderBy(columnEntity => columnEntity.Name).First().Name);
                Assert.That(roundTripEntity.DefaultValue, Is.Not.Null.And.Not.Empty);
                Assert.That(roundTripEntity.DefaultValue, Is.EqualTo(entity.DefaultValue));
                Assert.That(roundTripEntity.Component.ObjectId, Is.EqualTo(entity.Component.ObjectId));
            }
            finally
            {
                if (entityId != null)
                {
                    this._persistManager.Delete<ComponentMappingEntity>(StoreId, entityId);
                }
            }
        }


        [Test]
        public void ShouldInsertComponentMappingWithComponentConstantAndNoEntityId()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            var mappingStoreManager = Substitute.For<IMappingStoreManager>();
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new MappingSetFactory(databaseManager), new DatasetColumnRetrieverFactory(databaseManager), new ComponentMappingRetrieverFactory(databaseManager, mappingStoreManager));
            var x = retrieverManager.GetEntities<MappingSetEntity>(StoreId, new EntityQuery() { }, Detail.Full);
            var mappingSetEntities = x.FirstOrDefault(setEntity => setEntity.EntityId == "40");

            var entity = new ComponentMappingEntity()
            {
                ParentId = mappingSetEntities.EntityId,
                ConstantValue = "constant",
                Component = new Component() { ObjectId = "BREAK" },
                StoreId = this.StoreId
            };

            var entityWithEntityId = this._persistManager.Add(entity);
            Assert.That(entityWithEntityId.EntityId, Is.Not.Null.Or.Empty);

            var roundTripEntity = retrieverManager.GetEntities<ComponentMappingEntity>(
                StoreId,
                entityWithEntityId.EntityId.QueryForThisEntityId(),
                Detail.Full).Single();
            Assert.That(roundTripEntity.Component, Is.Not.Null);
            Assert.That(roundTripEntity.ConstantValue, Is.EqualTo(entity.ConstantValue));
            Assert.That(roundTripEntity.EntityId, Is.EqualTo(entityWithEntityId.EntityId));
            Assert.That(roundTripEntity.GetColumns(), Is.Empty);
            Assert.That(roundTripEntity.Component.ObjectId, Is.EqualTo(entity.Component.ObjectId));
            this._persistManager.Delete<ComponentMappingEntity>(StoreId, roundTripEntity.EntityId);
        }

        [Test]
        public void TestTranscodingRule()
        {
            IEntityRetrieverManager retrieverManager = _retrieveManager;
            var someRandomComponentMapping =  _retrieveManager.GetEntities<ComponentMappingEntity>(this.StoreId, new EntityQuery(), Detail.Full).First(x => x.GetColumns().Count == 1 && !x.HasTranscoding());
            var datasetColumnName = someRandomComponentMapping.GetColumns().First().Name;
            List<TranscodingRuleEntity> transcodingRules = new List<TranscodingRuleEntity>();
            var localCodes = new[] { "M'X", "QX", "AX" };
            var sdmxCodes = new[] { "M", "Q", "AX" };
            for (int i = 0; i < localCodes.Length; i++)
            {
                TranscodingRuleEntity rule = new TranscodingRuleEntity();
                rule.LocalCodes = new[]
                                      {
                                          new LocalCodeEntity()
                                              {
                                                  ObjectId = localCodes[i],
                                                  ParentId = datasetColumnName
                                              },
                                      };
                rule.DsdCodeEntity = new IdentifiableEntity() { ObjectId = sdmxCodes[i] };
                rule.StoreId = this.StoreId;
                rule.ParentId = someRandomComponentMapping.EntityId;
                transcodingRules.Add(rule);
            }
            var entities = new List<TranscodingRuleEntity>();
            try
            {
                entities = this._persistManager.AddEntities(transcodingRules).ToList();

                var roundTripEntity = retrieverManager.GetEntities<TranscodingRuleEntity>(
                    StoreId,
                   someRandomComponentMapping.QueryForThisParentId(),
                    Detail.Full).First();
                Assert.That(roundTripEntity.DsdCodeEntity, Is.Not.Null);
                Assert.AreEqual(roundTripEntity.DsdCodeEntity.ObjectId, sdmxCodes[0]);
                Assert.AreEqual(roundTripEntity.LocalCodes[0].ObjectId, localCodes[0]);
                Assert.AreEqual(roundTripEntity.LocalCodes[0].ParentId, datasetColumnName);

                var newLocalCode = "new local code";
                var newSDMXCode = "new sdmx code";
                var patchRequest = new PatchRequest()
                {
                    new PatchDocument("replace","/local_code",newLocalCode),
                    new PatchDocument("replace","/code",newSDMXCode),
                };
                var engine = this._persistManager.GetEngine<TranscodingRuleEntity>(this.StoreId);
                engine.Update(patchRequest, Api.Constant.EntityType.TranscodingRule, roundTripEntity.EntityId);
                
                roundTripEntity = retrieverManager.GetEntities<TranscodingRuleEntity>(
                   StoreId,
                   entities.First().ParentId.QueryForThisParentId(),
                   Detail.Full).First();
                Assert.That(roundTripEntity.DsdCodeEntity, Is.Not.Null);
                Assert.AreEqual(roundTripEntity.DsdCodeEntity.ObjectId, newSDMXCode);
                Assert.AreEqual(roundTripEntity.LocalCodes[0].ObjectId, newLocalCode);
            }
            finally
            {
                if (entities != null)
                {
                    foreach (var entityId in entities.Select(x => x.EntityId))
                    { 
                        this._persistManager.Delete<TranscodingRuleEntity>(StoreId, entityId); 
                    }
                }
            }
        }
  [Test]
        public void TestTimeEntity()
        {
            var entityId = "-1";
            try 
            {
                var x = _retrieveManager.GetEntities<MappingSetEntity>(StoreId, new EntityQuery() { }, Detail.Full);
                var mappingSetEntities = x.First(setEntity => setEntity.EntityId == "40");
                entityId = mappingSetEntities.EntityId;
                var datasetColumns = _retrieveManager.GetEntities<DataSetColumnEntity>(
                    this.StoreId,
                    mappingSetEntities.DataSetId.QueryForThisParentId(),
                    Detail.Full);
                var entity = new TimeDimensionMappingEntity()
                {
                    EntityId = mappingSetEntities.EntityId,
                    ParentId = mappingSetEntities.EntityId,
                    StoreId = this.StoreId
                };

                var timeColumn = datasetColumns.First(columnEntity => columnEntity.Name.Contains(DimensionObject.TimeDimensionFixedId, StringComparison.Ordinal));
                entity.SimpleMappingColumn = timeColumn.Name;
                var addedTranscodingEntity = _persistManager.Add(entity);

                Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
                var retrievedEntity = _retrieveManager.GetEntities<TimeDimensionMappingEntity>(StoreId, addedTranscodingEntity.QueryForThisEntityId(), Detail.Full).First();
                entityId = retrievedEntity.EntityId;
                Assert.That(retrievedEntity.EntityId, Is.EqualTo(addedTranscodingEntity.EntityId));
                Assert.That(retrievedEntity.ParentId, Is.EqualTo(addedTranscodingEntity.ParentId));
                Assert.That(retrievedEntity.Transcoding, Is.Null);
                Assert.IsFalse(retrievedEntity.HasTranscoding());
                Assert.That(retrievedEntity.SimpleMappingColumn, Is.EqualTo(timeColumn.Name));
            }
            finally
            {
                  this._persistManager.Delete<TimeDimensionMappingEntity>(StoreId, entityId);
            }
        }

  [Test]
        public void TestTimePreFormatedEntity()
        {
            var entityId = "-1";
            try 
            {
                var x = _retrieveManager.GetEntities<MappingSetEntity>(StoreId, new EntityQuery() { }, Detail.Full);
                var mappingSetEntities = x.First(setEntity => setEntity.EntityId == "40");
                entityId = mappingSetEntities.EntityId;
                var datasetColumns = _retrieveManager.GetEntities<DataSetColumnEntity>(
                    this.StoreId,
                    mappingSetEntities.DataSetId.QueryForThisParentId(),
                    Detail.Full).ToArray();
                var entity = new TimeDimensionMappingEntity()
                {
                    EntityId = mappingSetEntities.EntityId,
                    ParentId = mappingSetEntities.EntityId,
                    StoreId = this.StoreId
                };

                TimePreFormattedEntity preFormattedEntity = new TimePreFormattedEntity();
                preFormattedEntity.FromColumn = datasetColumns[0];
                preFormattedEntity.ToColumn = datasetColumns[1];
                preFormattedEntity.OutputColumn = datasetColumns[2];
                entity.PreFormatted = preFormattedEntity;
                
                var addedTranscodingEntity = _persistManager.Add(entity);

                Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
                var retrievedEntity = _retrieveManager.GetEntities<TimeDimensionMappingEntity>(StoreId, addedTranscodingEntity.QueryForThisEntityId(), Detail.Full).First();
                entityId = retrievedEntity.EntityId;
                Assert.That(retrievedEntity.EntityId, Is.EqualTo(addedTranscodingEntity.EntityId));
                Assert.That(retrievedEntity.ParentId, Is.EqualTo(addedTranscodingEntity.ParentId));
                Assert.That(retrievedEntity.Transcoding, Is.Null);
                Assert.IsFalse(retrievedEntity.HasTranscoding());
                Assert.That(retrievedEntity.SimpleMappingColumn, Is.Null);
                Assert.That(retrievedEntity.PreFormatted, Is.Not.Null);
                Assert.That(retrievedEntity.PreFormatted.FromColumn.Name, Is.EqualTo(preFormattedEntity.FromColumn.Name));
                Assert.That(retrievedEntity.PreFormatted.OutputColumn.Name, Is.EqualTo(preFormattedEntity.OutputColumn.Name));
                Assert.That(retrievedEntity.PreFormatted.ToColumn.Name, Is.EqualTo(preFormattedEntity.ToColumn.Name));
            }
            finally
            {
                  this._persistManager.Delete<TimeDimensionMappingEntity>(StoreId, entityId);
            }
        }
        [Test]
        public void TestTimeTranscodingRule()
        {
            var entityId = "-1";
            try 
            {
                var x = _retrieveManager.GetEntities<MappingSetEntity>(StoreId, new EntityQuery() { }, Detail.Full);
                var mappingSetEntities = x.First(setEntity => setEntity.EntityId == "40");
                entityId = mappingSetEntities.EntityId;
                var datasetColumns = _retrieveManager.GetEntities<DataSetColumnEntity>(
                    this.StoreId,
                    mappingSetEntities.DataSetId.QueryForThisParentId(),
                    Detail.Full);
                var entity = new TimeDimensionMappingEntity()
                {
                    EntityId = mappingSetEntities.EntityId,
                    ParentId = mappingSetEntities.EntityId,
                    StoreId = this.StoreId
                };

                var timeColumn = datasetColumns.First(columnEntity => columnEntity.Name.Contains(DimensionObject.TimeDimensionFixedId, StringComparison.Ordinal));
                TimeTranscodingEntity timeTranscodingEntity = new TimeTranscodingEntity();
                timeTranscodingEntity.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.Year).FrequencyCode;
                timeTranscodingEntity.IsDateTime = false;
                timeTranscodingEntity.Year = new TimeTranscoding() { Column = timeColumn, Length = 4, Start = 0 };
                var oldStyleTranscodings = new List<TimeTranscodingEntity>();
                oldStyleTranscodings.Add(timeTranscodingEntity);
                entity.Transcoding = TimeTranscodingConversionHelper.Convert(oldStyleTranscodings, "FREQ");
                var addedTranscodingEntity = _persistManager.Add(entity);

                Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
                Assert.That(addedTranscodingEntity.GetEntityId(), Is.GreaterThan(0));
                var retrievedEntity = _retrieveManager.GetEntities<TimeDimensionMappingEntity>(StoreId, addedTranscodingEntity.QueryForThisEntityId(), Detail.Full).First();
                entityId = retrievedEntity.EntityId;
                Assert.That(retrievedEntity.EntityId, Is.EqualTo(addedTranscodingEntity.EntityId));
                Assert.That(retrievedEntity.ParentId, Is.EqualTo(addedTranscodingEntity.ParentId));
                Assert.That(retrievedEntity.Transcoding, Is.Not.Null);
                Assert.That(retrievedEntity.HasTranscoding());
                Assert.That(retrievedEntity.Transcoding.IsFrequencyDimension, Is.True);
                TimeTranscodingEntity retrievedAnnualTimeTranscoding = TimeTranscodingConversionHelper.Convert(retrievedEntity.Transcoding).Single();
                Assert.That(retrievedAnnualTimeTranscoding.Frequency, Is.EqualTo(timeTranscodingEntity.Frequency));
                Assert.That(retrievedAnnualTimeTranscoding.Year, Is.Not.Null);
                Assert.That(retrievedAnnualTimeTranscoding.Period, Is.Null);
                Assert.That(retrievedAnnualTimeTranscoding.Year.Length, Is.EqualTo(timeTranscodingEntity.Year.Length));
                Assert.That(retrievedAnnualTimeTranscoding.Year.Start, Is.EqualTo(timeTranscodingEntity.Year.Start));
            }
            finally
            {
                  this._persistManager.Delete<TimeDimensionMappingEntity>(StoreId, entityId);
            }
        }

        [Test]
        public void ShouldInsertAdvanceTimeTranscodingWithDuration()
        {
            // TODO store retrieve TimeDimensionMaping as whole
            // var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            // var connectionStringHelper = new ConnectionStringRetriever();
            // var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            // configurationStoreManager.GetSettings<ConnectionStringSettings>()
            //    .Returns(new[] { connectionStringSettings });
            // DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            // IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new MappingSetFactory(databaseManager));
            // var engine = retrieverManager.GetRetrieverEngine<MappingSetEntity>(this.StoreId);
            var mappingSetEntities = _retrieveManager.GetEntities<MappingSetEntity>(this.StoreId, new EntityQuery(), Detail.Full).First();
            var datasetcolumns = _retrieveManager.GetEntities<DataSetColumnEntity>(this.StoreId, new DatasetEntity() {EntityId = mappingSetEntities.DataSetId}.QueryForThisParentId(), Detail.Full).ToList();
            

            // get time dimension commponent mapping
            var componentMappingEntities = _retrieveManager.GetEntities<TimeDimensionMappingEntity>(StoreId, mappingSetEntities.QueryForThisParentId(), Detail.Full);
            var timeDimensionMapping = componentMappingEntities.Single();

            // add time dimension transcoding (advanced)
            var durationMappingEntity = new DurationMappingEntity() { ConstantValue = "P1Y", };

            TimeTranscodingAdvancedEntity advancedEntity = new TimeTranscodingAdvancedEntity();
            advancedEntity.CriteriaColumn = timeDimensionMapping.GetColumns()[0];
            TimeFormatConfiguration timeFormatConfiguration = AddStartDurationDateTime(durationMappingEntity, datasetcolumns);
            advancedEntity.TimeFormatConfigurations.Add("A", timeFormatConfiguration);

            var timeDimensionMappingEntity = new TimeDimensionMappingEntity();
            timeDimensionMappingEntity.Transcoding = advancedEntity;
            timeDimensionMappingEntity.EntityId = mappingSetEntities.EntityId;
            timeDimensionMappingEntity.ParentId = mappingSetEntities.EntityId;
            timeDimensionMappingEntity.StoreId = StoreId;
            var returnValue = this._persistManager.Add(timeDimensionMappingEntity);

            var roundtripTimeMapping = _retrieveManager.GetEntities<TimeDimensionMappingEntity>(
                this.StoreId,
                returnValue.QueryForThisEntityId(),
                Detail.Full).FirstOrDefault();

            Assert.That(roundtripTimeMapping, Is.Not.Null);
            Assert.That(roundtripTimeMapping.Transcoding, Is.Not.Null);
            Assert.That(roundtripTimeMapping.TimeMappingType, Is.EqualTo(TimeDimensionMappingType.TranscodingWithCriteriaColumn));
            Assert.That(roundtripTimeMapping.Transcoding.CriteriaColumn, Is.Not.Null);
            Assert.That(roundtripTimeMapping.Transcoding.CriteriaColumn.Name, Is.EqualTo(advancedEntity.CriteriaColumn.Name));
            var timeFormatConfigurations = roundtripTimeMapping.Transcoding.TimeFormatConfigurations;
            Assert.That(timeFormatConfigurations, Is.Not.Empty);
            Assert.That(timeFormatConfigurations.Count, Is.EqualTo(1));
            Assert.That(timeFormatConfigurations.ContainsKey("A"));
            var formatConfiguration = timeFormatConfigurations.First().Value;
            Assert.That(formatConfiguration.StartConfig, Is.Not.Null);
            Assert.That(formatConfiguration.EndConfig, Is.Null);
            Assert.That(formatConfiguration.DurationMap, Is.Not.Null);
            Assert.That(formatConfiguration.DurationMap.ConstantValue, Is.EqualTo(durationMappingEntity.ConstantValue));
            Assert.That(formatConfiguration.OutputFormat, Is.EqualTo(timeFormatConfiguration.OutputFormat));
            Assert.That(formatConfiguration.StartConfig.Format, Is.EqualTo(timeFormatConfiguration.StartConfig.Format));
            Assert.That(formatConfiguration.StartConfig.DateColumn.Name, Is.EqualTo(timeFormatConfiguration.StartConfig.DateColumn.Name));
            Assert.That(formatConfiguration.StartConfig.Year, Is.Null);
            Assert.That(formatConfiguration.StartConfig.Period, Is.Null);

            this._persistManager.Delete<TimeDimensionMappingEntity>(StoreId, roundtripTimeMapping.EntityId);
        }

        [Test]
        public void TestHeaderEntity()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            var mappingStoreManager = Substitute.For<IMappingStoreManager>();
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new HeaderRetrieverFactory(databaseManager));
            HeaderEntity headerEntity = new HeaderEntity();
            headerEntity.ParentId = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:61_69(1.0)";
            headerEntity.Name = "testing123";
            headerEntity.Description = "description";
            Dictionary<string, string> additionalProperties = new Dictionary<string, string> { { "DataSetAgency", "Test Agency" } };

            IList<ITextTypeWrapper> names = new List<ITextTypeWrapper>();
            names.Add(new TextTypeWrapperImpl("en", "Romania INE", null));
            IList<IContact> contacts = new List<IContact>();
            var senderContact = new ContactCore();
            senderContact.Name.Add(new TextTypeWrapperImpl("en", "Smith", senderContact));
            senderContact.Departments.Add(new TextTypeWrapperImpl("en", "CENSUS", senderContact));
            senderContact.Role.Add(new TextTypeWrapperImpl("en", "Admin", senderContact));
            senderContact.Email.Add("test@somewhere.org");

            contacts.Add(senderContact);
            IParty sender = new PartyCore(names, "RO1", contacts, null);

            IList<ITextTypeWrapper> receiverName = new List<ITextTypeWrapper>();
            receiverName.Add(new TextTypeWrapperImpl("en", "Ministry of Truth", null));

            IList<IContact> receiverContacts = new List<IContact>();
            var receiverContact = new ContactCore();
            receiverContact.Name.Add(new TextTypeWrapperImpl("en", "someone", null));
            receiverContact.Role.Add(new TextTypeWrapperImpl("en", "aRole", null));
            receiverContact.Email.Add("someone@example.com");
            receiverContacts.Add(receiverContact);

            IParty receiver = new PartyCore(receiverName, "ZZ9", receiverContacts, null);
            headerEntity.SdmxHeader = new HeaderImpl(additionalProperties, null, null, null, null, null, null, null, null, null, null, null, null, new IParty[0], sender, false);
            headerEntity.SdmxHeader.AddName(new TextTypeWrapperImpl("en", "header name", null));
            headerEntity.SdmxHeader.AddReciever(receiver);

            headerEntity.StoreId = this.StoreId;

            //there should be only one header record for each dataflow
            while (true)
            {
                try {
                    var headerForDataflow = retrieverManager.GetEntities<HeaderEntity>(
                    StoreId,
                    headerEntity.ParentId.QueryForThisParentId(),
                    Detail.Full).Single();


                    this._persistManager.Delete<HeaderEntity>(StoreId, headerForDataflow.EntityId);
                }
                catch(ResourceNotFoundException )
                {
                    //no more headers
                    break;
                }
            }
            var entityWithEntityId = this._persistManager.Add(headerEntity);
            Assert.That(entityWithEntityId.EntityId, Is.Not.Null.Or.Empty);

            var roundTripEntity = retrieverManager.GetEntities<HeaderEntity>(
                StoreId,
                entityWithEntityId.EntityId.QueryForThisEntityId(),
                Detail.Full).Single();

            Assert.AreEqual(roundTripEntity.ParentId, headerEntity.ParentId);
            Assert.IsNotNull(roundTripEntity.SdmxHeader);
            Assert.IsNotNull(roundTripEntity.SdmxHeader.Sender);
            Assert.AreEqual(roundTripEntity.SdmxHeader.Sender.Contacts[0].Email[0], "test@somewhere.org");
            Assert.AreEqual(roundTripEntity.SdmxHeader.Receiver[0].Contacts[0].Role[0].Value, "aRole");


            IList<IContact> receiverContacts2 = new List<IContact>();
            var receiverContact2 = new ContactCore();
            receiverContact2.Name.Add(new TextTypeWrapperImpl("en", "someone1", null));
            receiverContact2.Role.Add(new TextTypeWrapperImpl("en", "aRole1", null));
            receiverContact2.Email.Add("someone1@example.com");
            receiverContacts2.Add(receiverContact2);

            IParty receiver2 = new PartyCore(receiverName, "ZZ10", receiverContacts, null);
            headerEntity.SdmxHeader.AddReciever(receiver2);

            this._persistManager.Update(headerEntity);

            roundTripEntity = retrieverManager.GetEntities<HeaderEntity>(
                StoreId,
                headerEntity.EntityId.QueryForThisEntityId(),
                Detail.Full).Single();

            Assert.AreEqual(roundTripEntity.ParentId, headerEntity.ParentId);
            Assert.IsNotNull(roundTripEntity.SdmxHeader);
            Assert.IsNotNull(roundTripEntity.SdmxHeader.Receiver);
            Assert.AreEqual(roundTripEntity.SdmxHeader.Receiver[1].Contacts[0].Role[0].Value, headerEntity.SdmxHeader.Receiver[1].Contacts[0].Role[0].Value);

            this._persistManager.Delete<HeaderEntity>(StoreId, roundTripEntity.EntityId);
        }

        [Test]
        public void TestRegistryEntity()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            var mappingStoreManager = Substitute.For<IMappingStoreManager>();
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new RegistryRetrieverFactory(databaseManager));
            var initialName = "New Registry Connection";
            var updateName = "Updated Name";
            RegistryEntity registryEntity = new RegistryEntity();
            registryEntity.EntityId = "1";
            registryEntity.Name = initialName;
            registryEntity.Description = "mmm";
            registryEntity.Technology = "Rest";
            registryEntity.Url = "rrr";
            registryEntity.UserName = "eee";

            registryEntity.StoreId = this.StoreId;
            var entityWithEntityId = this._persistManager.Add(registryEntity);
            Assert.That(entityWithEntityId.EntityId, Is.Not.Null.Or.Empty);

            var roundTripEntity = retrieverManager.GetEntities<RegistryEntity>(
                StoreId,
                entityWithEntityId.EntityId.QueryForThisEntityId(),
                Detail.Full).Single();

            Assert.That(roundTripEntity.Name, Is.EqualTo(initialName));

            registryEntity.Name = updateName;

            this._persistManager.Update(registryEntity);

            roundTripEntity = retrieverManager.GetEntities<RegistryEntity>(
                StoreId,
                registryEntity.EntityId.QueryForThisEntityId(),
                Detail.Full).Single();

            Assert.That(roundTripEntity.Name, Is.EqualTo(updateName));

            this._persistManager.Delete<RegistryEntity>(StoreId, roundTripEntity.EntityId);

            roundTripEntity = retrieverManager.GetEntities<RegistryEntity>(
                StoreId,
                entityWithEntityId.EntityId.QueryForThisEntityId(),
                Detail.Full).FirstOrDefault();

            Assert.Null(roundTripEntity);
        }

        [Test]
        public void TestNSIwsEntity()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            var mappingStoreManager = Substitute.For<IMappingStoreManager>();
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new NsiwsRetrieverFactory(databaseManager));
            var initialName = "New NSI Connection";
            var updateName = "Updated Name";
            NsiwsEntity registryEntity = new NsiwsEntity();
            registryEntity.EntityId = "1";
            registryEntity.Name = initialName;
            registryEntity.Description = "mmm";
            registryEntity.Technology = "Rest";
            registryEntity.Url = "rrr";
            registryEntity.UserName = "eee";

            registryEntity.StoreId = this.StoreId;
            var entityWithEntityId = this._persistManager.Add(registryEntity);
            Assert.That(entityWithEntityId.EntityId, Is.Not.Null.Or.Empty);
        }


        [Test]
        public void TestUserEntity()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<UserEntity>(this.StoreId);

            var username = "user2";
            var categoryPermission = "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30";
            var dataflowPermission = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:HC06(1.0)";
            var newCategoryPermission = "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30.264D";
            UserEntity userEntity = new UserEntity();
            userEntity.UserName = username;
            userEntity.EditedBy = "admin";
            userEntity.EditDate = System.DateTime.Today;
            userEntity.Dataspace = "testDataspace";
            userEntity.IsGroup = true;
            userEntity.AddPermission(categoryPermission, "read_write");
            userEntity.AddPermission(dataflowPermission, "read_write");
            userEntity.StoreId = this.StoreId;

            var existingUserQuery = new EntityQuery();
            existingUserQuery.AddAdditionalCriteria(nameof(UserEntity.UserName), new Criteria<string>(Api.Constant.OperatorType.Exact, userEntity.UserName));

            entityPersistenceEngine.Add(userEntity);

            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new UserRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<UserEntity>(this._connectionStringSettings.Name);
            var query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>
            {
                {nameof(UserEntity.UserName), new Criteria<string>(OperatorType.Exact, username)}
            });
            var result = engine.GetEntities(query, Detail.Full).Single();
            Assert.That(result, Is.Not.Null);
            Assert.True(result.Permissions.ContainsKey(categoryPermission));
            Assert.True(result.Permissions.ContainsKey(dataflowPermission));

            userEntity.RemovePermission(categoryPermission);
            userEntity.AddPermission(newCategoryPermission, "read_write");

            this._persistManager.Update(userEntity);

            result = engine.GetEntities(query, Detail.Full).Single();
            Assert.That(result, Is.Not.Null);
            Assert.True(result.Permissions.ContainsKey(newCategoryPermission));
            Assert.True(result.Permissions.ContainsKey(dataflowPermission));

            this._persistManager.Delete<UserEntity>(StoreId, username);

            result = engine.GetEntities(query, Detail.Full).FirstOrDefault();
            Assert.That(result, Is.Null);
        }


        [Test]
        public void TestTranscodingScriptEntity()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            var mappingStoreManager = Substitute.For<IMappingStoreManager>();
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new MappingSetFactory(databaseManager), new DatasetColumnRetrieverFactory(databaseManager), new ComponentMappingRetrieverFactory(databaseManager, mappingStoreManager));
            var x = retrieverManager.GetEntities<MappingSetEntity>(StoreId, new EntityQuery() { }, Detail.Full);
            var mappingSetEntities = x.FirstOrDefault(setEntity => setEntity.EntityId == "40");

            var entity = new ComponentMappingEntity()
            {
                ParentId = mappingSetEntities.EntityId,
                ConstantValue = "constant",
                Component = new Component() { ObjectId = "BREAK" },
                StoreId = this.StoreId,
                Script = new List<TranscodingScriptEntity>()
                {
                    new TranscodingScriptEntity()
                    {
                        ScriptContent = "script",
                        ScriptTile = "title"
                    }
                }
            };

            var entityWithEntityId = this._persistManager.Add(entity);
            Assert.That(entityWithEntityId.EntityId, Is.Not.Null.Or.Empty);

            var roundTripEntity = retrieverManager.GetEntities<ComponentMappingEntity>(
                StoreId,
                entityWithEntityId.EntityId.QueryForThisEntityId(),
                Detail.Full).Single();
            Assert.That(roundTripEntity.Component, Is.Not.Null);
            Assert.That(roundTripEntity.ConstantValue, Is.EqualTo(entity.ConstantValue));
            Assert.That(roundTripEntity.EntityId, Is.EqualTo(entityWithEntityId.EntityId));
            Assert.That(roundTripEntity.GetColumns(), Is.Empty);
            Assert.That(roundTripEntity.Component.ObjectId, Is.EqualTo(entity.Component.ObjectId));
            this._persistManager.Delete<ComponentMappingEntity>(StoreId, roundTripEntity.EntityId);
        }

        [Test]
        public void TestUserActionEntity()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            var mappingStoreManager = Substitute.For<IMappingStoreManager>();
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new UserActionRetrieverFactory(databaseManager), new DatasetColumnRetrieverFactory(databaseManager));

            var entity = new ColumnDescriptionSourceEntity()
            {
                DescriptionTable = "my table",
                RelatedField = "my related field",
                DescriptionField = "my description field",
                ParentId = "23"
            };
            GenericIdentity identity = new GenericIdentity("test");

            System.Threading.Thread.CurrentPrincipal =
               new GenericPrincipal(
                   identity,
                   new string[] { "Role1", "Role2" }
               );
            entity.StoreId = this.StoreId;
            var entityWithEntityId = this._persistManager.Add(entity);

            //we need to get the id of the dataset because that is what is recorded in user action table
            var datasetColumn = retrieverManager.GetEntities<DataSetColumnEntity>(
               StoreId,
               new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityWithEntityId.ParentId.ToString()) },
               Detail.Full).FirstOrDefault();

            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>()
            {
                {nameof(UserActionEntity.Id), new Criteria<string>(OperatorType.Exact, datasetColumn.ParentId)}
            });
            this._persistManager.Delete<ColumnDescriptionSourceEntity>(StoreId, entityWithEntityId.EntityId);

            var userActions = retrieverManager.GetEntities<UserActionEntity>(
                StoreId,
                query,
                Detail.Full);

            //there should be 2 user actions : one for insert and one for delete
            Assert.That(userActions, Is.Not.Null);
            Assert.That(userActions.Count, Is.EqualTo(2));

            foreach (var action in userActions)
            {
                this._persistManager.Delete<UserActionEntity>(StoreId, action.EntityId);
            }
            var userAction = retrieverManager.GetEntities<UserActionEntity>(
                StoreId,
                entityWithEntityId.EntityId.QueryForThisEntityId(),
                Detail.Full).FirstOrDefault();

            Assert.Null(userAction); 
        }

        [Test]
        public void TestTemplateMapping()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            var timeTranscodingEntity = new TimeTranscodingEntity()
            {
                Frequency = "Q",
                Period = new PeriodTimeTranscoding()
                {
                    Start = 1,
                    Length = 4
                },
                Year = new TimeTranscoding()
                {
                    Start = 2,
                    Length = 3
                }

            };
            var templateMapping = new TemplateMapping()
            {
                ColumnDescription = "testDesc",
                ColumnName = "REF_AREA",
                ComponentId = "1",
                ComponentType = "Dimension",
                ConceptId = 19496,
                ItemSchemeId = new StructureReferenceImpl("MA", "SDMX_Q_PERIODS", "1.0.0", SdmxStructureEnumType.CodeList),
                Transcodings = new Dictionary<string, string>()
                {
                    { "Quarterly","Q" },
                    { "Testign","T"}
                },
                TimeTranscoding = new List<TimeTranscodingEntity>()
                {
                    timeTranscodingEntity
                },
                Name = "test13",
                Description = "test13",
            };
            templateMapping.StoreId = StoreId;
            var addedEntity = this._persistManager.Add(templateMapping);

            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new TemplateMappingRetrieverFactory(databaseManager));
            var returnEntity = retrieverManager.GetEntities<TemplateMapping>(
                StoreId,
                addedEntity.EntityId.QueryForThisEntityId(),
                Detail.Full).Single();
            Assert.IsNotNull(returnEntity);
            Assert.That(returnEntity.Transcodings.Count, Is.EqualTo(2));
            Assert.That(returnEntity.TimeTranscoding.Count, Is.EqualTo(1));

            var timeTranscodingEntity1 =
                new TimeTranscodingEntity()
                {
                    DateColumn = new DataSetColumnEntity()
                    {
                        LastRetrieval = DateTime.Today
                    },
                    Frequency = "M",
                    EntityId = "5"
                };
            templateMapping.TimeTranscoding.Add(timeTranscodingEntity1);
            this._persistManager.Update(templateMapping);

            returnEntity = retrieverManager.GetEntities<TemplateMapping>(
               StoreId,
               templateMapping.EntityId.QueryForThisEntityId(),
               Detail.Full).Single();

            Assert.IsNotNull(returnEntity);
            Assert.That(returnEntity.Transcodings.Count, Is.EqualTo(2));
            Assert.That(returnEntity.TimeTranscoding.Count, Is.EqualTo(2));

            this._persistManager.Delete<TemplateMapping>(StoreId, addedEntity.EntityId);
      }

        [Test]
        public void TestMeasureMapping() {

            var mappingSetEntities = _retrieveManager.GetEntities<MappingSetEntity>(StoreId, new EntityQuery() { }, Detail.Full).FirstOrDefault(setEntity => setEntity.EntityId == "40");
            var datasetColumns = _retrieveManager.GetEntities<DataSetColumnEntity>(
                this.StoreId,
                mappingSetEntities.DataSetId.QueryForThisParentId(),
                Detail.Full).Distinct(comparer);


            var dsd = CreateDsdIfNecessary();
            var entity = new ComponentMappingEntity()
            {
                ParentId = mappingSetEntities.EntityId,
                Component = new Component() { ObjectId = dsd.Measures[0].Id, EntityId = "1" },
                StoreId = this.StoreId
            };
            entity.SetColumns(datasetColumns.ToArray());
            string entityId = null;
            try
            {
                var entityWithEntityId = this._persistManager.Add(entity);
                entityId = entityWithEntityId.EntityId;
                Assert.That(entityId, Is.Not.Null.Or.Empty);

                var roundTripEntity = _retrieveManager.GetEntities<ComponentMappingEntity>(
                    StoreId,
                    entityId.QueryForThisEntityId(),
                    Detail.Full).Single();
                Assert.That(roundTripEntity.Component, Is.Not.Null);
                Assert.That(roundTripEntity.EntityId, Is.EqualTo(entityId));
                Assert.That(roundTripEntity.GetColumns(), Is.Not.Null);
                Assert.That(roundTripEntity.GetColumns(), Is.Not.Empty);
                Assert.AreEqual(roundTripEntity.GetColumns().Count, entity.GetColumns().Count);
                Assert.AreEqual(roundTripEntity.GetColumns().Count, entity.GetColumns().Distinct().Count());
                Assert.AreEqual(roundTripEntity.GetColumns().OrderBy(columnEntity => columnEntity.Name).First().Name, entity.GetColumns().OrderBy(columnEntity => columnEntity.Name).First().Name);
                Assert.That(roundTripEntity.Component.ObjectId, Is.EqualTo(entity.Component.ObjectId));
            }
            finally
            {
                if (entityId != null)
                {
                    this._persistManager.Delete<ComponentMappingEntity>(StoreId, entityId);
                }
            }
        }

        [Test]
        public void ShouldInsertMeasureMappingWithColumnsAndDefaultValue()
        {
            var x = _retrieveManager.GetEntities<MappingSetEntity>(StoreId, new EntityQuery() { }, Detail.Full);
            var mappingSetEntities = x.First(setEntity => setEntity.EntityId == "40");
            var datasetColumns = _retrieveManager.GetEntities<DataSetColumnEntity>(
                this.StoreId,
                mappingSetEntities.DataSetId.QueryForThisParentId(),
                Detail.Full).Distinct(comparer);

            var dsd = CreateDsdIfNecessary();

            var entity = new ComponentMappingEntity()
            {
                ParentId = mappingSetEntities.EntityId,
                Component = new Component() { ObjectId = dsd.Measures[0].Id },
                DefaultValue = "TEST123",
                StoreId = this.StoreId
            };
            entity.SetColumns(datasetColumns.ToArray());
            string entityId = null;
            try
            {
                var entityWithEntityId = this._persistManager.Add(entity);
                entityId = entityWithEntityId.EntityId;
                Assert.That(entityId, Is.Not.Null.Or.Empty);

                var roundTripEntity = _retrieveManager.GetEntities<ComponentMappingEntity>(
                    StoreId,
                    entityId.QueryForThisEntityId(),
                    Detail.Full).Single();
                Assert.That(roundTripEntity.Component, Is.Not.Null);
                Assert.That(roundTripEntity.EntityId, Is.EqualTo(entityId));
                Assert.That(roundTripEntity.GetColumns(), Is.Not.Null);
                Assert.That(roundTripEntity.GetColumns(), Is.Not.Empty);
                Assert.That(roundTripEntity.GetColumns().Count, Is.EqualTo(entity.GetColumns().Count));
                Assert.AreEqual(roundTripEntity.GetColumns().Count, entity.GetColumns().Distinct().Count());
                Assert.AreEqual(roundTripEntity.GetColumns().OrderBy(columnEntity => columnEntity.Name).First().Name, entity.GetColumns().OrderBy(columnEntity => columnEntity.Name).First().Name);
                Assert.That(roundTripEntity.DefaultValue, Is.Not.Null.And.Not.Empty);
                Assert.That(roundTripEntity.DefaultValue, Is.EqualTo(entity.DefaultValue));
                Assert.That(roundTripEntity.Component.ObjectId, Is.EqualTo(entity.Component.ObjectId));
            }
            finally
            {
                if (entityId != null)
                {
                    this._persistManager.Delete<ComponentMappingEntity>(StoreId, entityId);
                }
            }
        }

        [Test]
        public void TestTranscodingRuleForMeasureMapping()
        {
            var x = _retrieveManager.GetEntities<MappingSetEntity>(StoreId, new EntityQuery() { }, Detail.Full);
            var mappingSetEntities = x.First(setEntity => setEntity.EntityId == "40");
            var datasetColumns = _retrieveManager.GetEntities<DataSetColumnEntity>(
                this.StoreId,
                mappingSetEntities.DataSetId.QueryForThisParentId(),
                Detail.Full).Distinct(comparer);

            var dsd = CreateDsdIfNecessary();

            var entity = new ComponentMappingEntity()
            {
                ParentId = mappingSetEntities.EntityId,
                Component = new Component() { ObjectId = dsd.Measures[0].Id },
                DefaultValue = "TEST123",
                StoreId = this.StoreId
            };
            entity.SetColumns(datasetColumns.ToArray());

            var mappingForMeasure = this._persistManager.Add(entity);
            var datasetColumnName = mappingForMeasure.GetColumns().First().Name;
           
            
            List<TranscodingRuleEntity> transcodingRules = new List<TranscodingRuleEntity>();
            var localCodes = new[] { "M'X", "QX", "AX" };
            var sdmxCodes = new[] { "M", "Q", "AX" };
            for (int i = 0; i < localCodes.Length; i++)
            {
                TranscodingRuleEntity rule = new TranscodingRuleEntity();
                rule.LocalCodes = new[]
                                      {
                                          new LocalCodeEntity()
                                              {
                                                  ObjectId = localCodes[i],
                                                  ParentId = datasetColumnName
                                              },
                                      };
                rule.DsdCodeEntity = new IdentifiableEntity() { ObjectId = sdmxCodes[i] };
                rule.StoreId = this.StoreId;
                rule.ParentId = mappingForMeasure.EntityId;
                transcodingRules.Add(rule);
            }
            var entities = new List<TranscodingRuleEntity>();
            try
            {
                entities = this._persistManager.AddEntities(transcodingRules).ToList();

                var roundTripEntity = _retrieveManager.GetEntities<TranscodingRuleEntity>(
                    StoreId,
                   mappingForMeasure.QueryForThisParentId(),
                    Detail.Full).First();
                Assert.That(roundTripEntity.DsdCodeEntity, Is.Not.Null);
                Assert.AreEqual(roundTripEntity.DsdCodeEntity.ObjectId, sdmxCodes[0]);
                Assert.AreEqual(roundTripEntity.LocalCodes[0].ObjectId, localCodes[0]);
                Assert.AreEqual(roundTripEntity.LocalCodes[0].ParentId, datasetColumnName);

                var newLocalCode = "new local code";
                var newSDMXCode = "new sdmx code";
                var patchRequest = new PatchRequest()
                {
                    new PatchDocument("replace","/local_code",newLocalCode),
                    new PatchDocument("replace","/code",newSDMXCode),
                };
                var engine = this._persistManager.GetEngine<TranscodingRuleEntity>(this.StoreId);
                engine.Update(patchRequest, Api.Constant.EntityType.TranscodingRule, roundTripEntity.EntityId);

                roundTripEntity = _retrieveManager.GetEntities<TranscodingRuleEntity>(
                   StoreId,
                   entities.First().ParentId.QueryForThisParentId(),
                   Detail.Full).First();
                Assert.That(roundTripEntity.DsdCodeEntity, Is.Not.Null);
                Assert.AreEqual(roundTripEntity.DsdCodeEntity.ObjectId, newSDMXCode);
                Assert.AreEqual(roundTripEntity.LocalCodes[0].ObjectId, newLocalCode);
            }
            finally
            {
                if (entities != null)
                {
                    foreach (var entityId in entities.Select(x => x.EntityId))
                    {
                        this._persistManager.Delete<TranscodingRuleEntity>(StoreId, entityId);
                    }
                }
            }
        }

        private IDataStructureObject CreateDsdIfNecessary()
        {
            var result = GetMutableArtefats<IDataStructureMutableObject>(StoreId, new StructureReferenceImpl("TEST", "TEST_DSD", "1.0", SdmxStructureEnumType.Dsd));
            IDataStructureObject dataStructureObject = null;
            if (result == null || result.Count() == 0)
            {
                IMutableObjects mutableObjects = new MutableObjectsImpl();
                IConceptSchemeMutableObject conceptScheme = new ConceptSchemeMutableCore();
                SetupMaintainable(conceptScheme, "TEST_CS");

                AddConcept(conceptScheme, "FREQ");
                AddConcept(conceptScheme, DimensionObject.TimeDimensionFixedId);
                AddConcept(conceptScheme, PrimaryMeasure.FixedId);
                AddConcept(conceptScheme, "MEASURE_2");

                mutableObjects.AddConceptScheme(conceptScheme);

                ICodelistMutableObject cl = new CodelistMutableCore();
                SetupMaintainable(cl, "CL_FREQ");

                var c1 = new CodeMutableCore() { Id = "A" };
                c1.AddName("en", "Annual");
                cl.AddItem(c1);
                var c2 = new CodeMutableCore() { Id = "M" };
                c2.AddName("en", "Monthly");
                cl.AddItem(c2);

                mutableObjects.AddCodelist(cl);

                IDataStructureMutableObject dsd = new DataStructureMutableCore();
                SetupMaintainable(dsd, "TEST_DSD");
                dsd.AddDimension(
                    new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "FREQ"),
                    new StructureReferenceImpl("TEST", "CL_FREQ", "1.0", SdmxStructureEnumType.CodeList));

                IDimensionMutableObject timeDimension = new DimensionMutableCore();
                timeDimension.ConceptRef = new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, DimensionObject.TimeDimensionFixedId);
                timeDimension.TimeDimension = true;
                dsd.AddDimension(timeDimension);

                IMeasureMutableObject measure1 = new MeasureMutableCore();
                measure1.ConceptRef = new StructureReferenceImpl(
                        "TEST",
                        "TEST_CS",
                        "1.0",
                        SdmxStructureEnumType.Concept,
                        "TEST_CS");
                var structureReference = new StructureReferenceImpl("TEST_AGENCY", "CL_FREQ", "1.0", SdmxStructureEnumType.CodeList);
                measure1.Representation = new RepresentationMutableCore() { Representation = structureReference };

                dsd.AddMeasure(measure1);

                IMeasureMutableObject measure2 = new MeasureMutableCore();
                measure2.ConceptRef = new StructureReferenceImpl(
                        "TEST",
                        "TEST_CS",
                        "1.0",
                        SdmxStructureEnumType.Concept,
                        "MEASURE_2");

                dsd.AddMeasure(measure2);

                mutableObjects.AddDataStructure(dsd);

                dataStructureObject = dsd.ImmutableInstance;
                var dataflow = new DataflowMutableCore(dataStructureObject);
                mutableObjects.AddDataflow(dataflow);
                var artefactImportStatuses = new List<ArtefactImportStatus>();
                var structurePersistenceManager =
                    new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(
                        _connectionStringSettings,
                        artefactImportStatuses);

                structurePersistenceManager.SaveStructures(mutableObjects.ImmutableObjects);
                Assert.That(
                    artefactImportStatuses.All(status => status.ImportMessage.Status != ImportMessageStatus.Error),
                    string.Join("\n", artefactImportStatuses.Select(status => status.ImportMessage.Message)));
            }
            else
            {
                dataStructureObject = result.First().ImmutableInstance;
            }
            return dataStructureObject;
        }

        private void SetupMaintainable(IMaintainableMutableObject maintainable, string id)
        {
            maintainable.Version = "1.0";
            maintainable.AgencyId = "TEST";
            maintainable.Id = id;
            maintainable.AddName("en", $"Test {id}");
            maintainable.FinalStructure = TertiaryBool.ParseBoolean(true);
        }
        private void AddConcept(IConceptSchemeMutableObject cs, string id)
        {
            var c = new ConceptMutableCore() { Id = id };
            c.AddName("en", "Name of $id");
            cs.AddItem(c);
        }
        private class DataSetColumnNameComparer : IEqualityComparer<DataSetColumnEntity>
      {
        public bool Equals(DataSetColumnEntity x, DataSetColumnEntity y)
        {
          return string.Equals(x?.Name, y?.Name, StringComparison.OrdinalIgnoreCase);
        }

        public int GetHashCode([DisallowNull] DataSetColumnEntity obj)
        {
          return obj.Name.GetHashCode();
        }
      }
    }
}
