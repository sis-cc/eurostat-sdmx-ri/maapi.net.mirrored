﻿// -----------------------------------------------------------------------
// <copyright file="MappingStoreTest.cs" company="EUROSTAT">
//   Date Created : 2017-04-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System.IO;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Manager;

    using NUnit.Framework;

    public class MappingStoreTest
    {
        private readonly IMappingStoreManager mappingStoreManager;

        private Container _container;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public MappingStoreTest()
        {
            _container =
                new Container(
                    rules =>
                    rules.With(FactoryMethod.ConstructorWithResolvableArguments)
                        .WithoutThrowOnRegisteringDisposableTransient());
            var assemblies = new[] { typeof(IDatabaseProviderManager).Assembly, typeof(DatabaseManager).Assembly };
            _container.RegisterMany(assemblies
               ,
                type => !typeof(IEntity).IsAssignableFrom(type));
            this.mappingStoreManager = this._container.Resolve<IMappingStoreManager>();
        }

        [Test]
        public void ShouldReturnIds()
        {
            var retrieveStoreIds = this.mappingStoreManager.RetrieveStoreIds();
            Assert.That(retrieveStoreIds, Is.Not.Empty);
        }
    }
}