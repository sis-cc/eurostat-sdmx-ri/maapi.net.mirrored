// -----------------------------------------------------------------------
// <copyright file="TestLocalDataWriter.cs" company="EUROSTAT">
//   Date Created : 2019-11-11
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Engine.Streaming;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    class TestLocalDataWriter
    {
        [Test]
        public void WriteWithoutErrors()
        {
            FileInfo testOutput = new FileInfo("WriteWithoutErrors.json");
            using (Stream stream = testOutput.Create())
            using (ILocalDataWriter dataWriter = new LocalDataWriter(stream))
            {
                dataWriter.StartMessage(new[] { "COLUMN1", "COLUMN2" }, 0, 100);
                dataWriter.WriteProperty("totalRows", 15);
                for (int i = 0; i < 15; i++)
                {
                    dataWriter.WriteRow("A" + i, i.ToString(CultureInfo.InvariantCulture));
                }
                stream.Flush();
            }

            using (JsonReader reader = new JsonTextReader(testOutput.OpenText()))
            {
                var j = JObject.Load(reader);
                Assert.That(j, Is.Not.Null);
                Assert.That(j["columns"].HasValues);
                string[] columns = j["columns"].Values<string>().ToArray();
                Assert.That(columns, Has.Length.EqualTo(2));
                Assert.That(columns[0], Is.EqualTo("COLUMN1"));
                Assert.That(columns[1], Is.EqualTo("COLUMN2"));
                Assert.That(j["totalRows"].HasValues, Is.False);
                Assert.That(j["totalRows"].Value<int>(), Is.EqualTo(15));
                Assert.That(j["start"].HasValues, Is.False);
                Assert.That(j["start"].Value<int>(), Is.EqualTo(0));
                Assert.That(j["end"].HasValues, Is.False);
                Assert.That(j["end"].Value<int>(), Is.EqualTo(100));

                Assert.That(j["rows"].HasValues);
                JArray[] rows = j["rows"].Values<JArray>().ToArray();
                Assert.That(rows, Has.Length.EqualTo(15));
                for (int i = 0; i < rows.Length; i++)
                {
                    Assert.That(rows[i][0].Value<string>(), Is.EqualTo("A" + i));
                    Assert.That(rows[i][1].Value<int>(), Is.EqualTo(i));
                }
            }
        }

         [Test]
        public void WriteWithErrors()
        {
            FileInfo testOutput = new FileInfo("WriteWithoutErrors.json");
            using (Stream stream = testOutput.Create())
            using (ILocalDataWriter dataWriter = new LocalDataWriter(stream))
            {
                dataWriter.StartMessage(new[] { "COLUMN1", "COLUMN2" }, 0, 100);
                dataWriter.WriteProperty("totalRows", 15);
                for (int i = 0; i < 15; i++)
                {
                    dataWriter.WriteRow("A" + i, i.ToString(CultureInfo.InvariantCulture));
                }
                dataWriter.WriteErrors(new[] { "Error X at A1", "Error XY at column 2" });
                stream.Flush();
            }

            using (JsonReader reader = new JsonTextReader(testOutput.OpenText()))
            {
                var j = JObject.Load(reader);
                Assert.That(j, Is.Not.Null);
                Assert.That(j["columns"].HasValues);
                string[] columns = j["columns"].Values<string>().ToArray();
                Assert.That(columns, Has.Length.EqualTo(2));
                Assert.That(columns[0], Is.EqualTo("COLUMN1"));
                Assert.That(columns[1], Is.EqualTo("COLUMN2"));
                Assert.That(j["totalRows"].HasValues, Is.False);
                Assert.That(j["totalRows"].Value<int>(), Is.EqualTo(15));
                Assert.That(j["start"].HasValues, Is.False);
                Assert.That(j["start"].Value<int>(), Is.EqualTo(0));
                Assert.That(j["end"].HasValues, Is.False);
                Assert.That(j["end"].Value<int>(), Is.EqualTo(100));

                Assert.That(j["rows"].HasValues);
                JArray[] rows = j["rows"].Values<JArray>().ToArray();
                Assert.That(rows, Has.Length.EqualTo(15));
                for (int i = 0; i < rows.Length; i++)
                {
                    Assert.That(rows[i][0].Value<string>(), Is.EqualTo("A" + i));
                    Assert.That(rows[i][1].Value<int>(), Is.EqualTo(i));
                }
                
                Assert.That(j["errors"].HasValues);
                string[] errors = j["errors"].Values<string>().ToArray();
                Assert.That(errors, Has.Length.EqualTo(2));
                Assert.That(errors[0], Is.EqualTo("Error X at A1"));
                Assert.That(errors[1], Is.EqualTo("Error XY at column 2"));
            }
        }

        [Test]
        public void WriteLocalCodes()
        {
            FileInfo testOutput = new FileInfo("WriteWithoutErrors.json");
            DataSetColumnEntity col1 = new DataSetColumnEntity() { EntityId = "1", Name = "COLUMN1" };
            DataSetColumnEntity col2 = new DataSetColumnEntity() { EntityId = "2", Name = "COLUMN2" };
            using (Stream stream = testOutput.Create())
            using (ILocalDataWriter dataWriter = new LocalDataWriter(stream))
            {
                dataWriter.StartMessage(new[] { col1, col2 }, 0, 100);
                dataWriter.WriteProperty("totalRows", 15);
                for (int i = 0; i < 15; i++)
                {
                    LocalCodeEntity code1 = new LocalCodeEntity() { ParentId = "1", EntityId = (i*2).ToString(CultureInfo.InvariantCulture), ObjectId = "A" + i };
                    LocalCodeEntity code2 = new LocalCodeEntity() { ParentId = "2", EntityId = ((i*2) + 1).ToString(CultureInfo.InvariantCulture), ObjectId = i.ToString(CultureInfo.InvariantCulture) };
                    dataWriter.WriteRow(code1, code2);
                }
                stream.Flush();
            }

            using (JsonReader reader = new JsonTextReader(testOutput.OpenText()))
            {
                var j = JObject.Load(reader);
                Assert.That(j, Is.Not.Null);
                Assert.That(j["columns"].HasValues);
                JObject[] columns = j["columns"].Values<JObject>().ToArray();
                Assert.That(columns, Has.Length.EqualTo(2));
                Assert.That(columns[0]["name"].Value<string>(), Is.EqualTo("COLUMN1"));
                Assert.That(columns[1]["name"].Value<string>(), Is.EqualTo("COLUMN2"));
                Assert.That(j["totalRows"].HasValues, Is.False);
                Assert.That(j["totalRows"].Value<int>(), Is.EqualTo(15));
                Assert.That(j["start"].HasValues, Is.False);
                Assert.That(j["start"].Value<int>(), Is.EqualTo(0));
                Assert.That(j["end"].HasValues, Is.False);
                Assert.That(j["end"].Value<int>(), Is.EqualTo(100));

                Assert.That(j["rows"].HasValues);
                JArray[] rows = j["rows"].Values<JArray>().ToArray();
                Assert.That(rows, Has.Length.EqualTo(15));
                for (int i = 0; i < rows.Length; i++)
                {
                    Assert.That(rows[i][0]["id"].Value<string>(), Is.EqualTo("A" + i));
                    Assert.That(rows[i][1]["id"].Value<string>(), Is.EqualTo(""+ i));
                }
            }
        }

         [Test]
        public void WriteLocalCodesWithLocalCodeWriter()
        {
            FileInfo testOutput = new FileInfo("WriteWithoutErrors.json");
            DataSetColumnEntity col1 = new DataSetColumnEntity() { EntityId = "1", Name = "COLUMN1" };
            DataSetColumnEntity col2 = new DataSetColumnEntity() { EntityId = "2", Name = "COLUMN2" };

            var dataSetcolumns = new[] { col1, col2 };

            var descriptionPerCol = new Dictionary<string, Dictionary<string, string>>(StringComparer.Ordinal);
            descriptionPerCol.Add(col1.Name, new Dictionary<string, string> { { "A1", "Description for A1" }, { "A4", "A4 desc" } });

            using (Stream stream = testOutput.Create())
            using (ILocalDataWriter writer = new LocalDataWriter(stream))
            using (ILocalDataWriter dataWriter = new LocalCodeWriter(writer, dataSetcolumns, descriptionPerCol))
            {
                dataWriter.StartMessage( new[] { "COLUMN1", "COLUMN2" }, 0, 100);
                dataWriter.WriteProperty("totalRows", 15);
                for (int i = 0; i < 15; i++)
                {
                    LocalCodeEntity code1 = new LocalCodeEntity() { ParentId = "1", EntityId = (i*2).ToString(CultureInfo.InvariantCulture), ObjectId = "A" + i };
                    LocalCodeEntity code2 = new LocalCodeEntity() { ParentId = "2", EntityId = ((i*2) + 1).ToString(CultureInfo.InvariantCulture), ObjectId = i.ToString(CultureInfo.InvariantCulture) };
                    dataWriter.WriteRow("A" + i, i.ToString());
                }
                stream.Flush();
            }

            using (JsonReader reader = new JsonTextReader(testOutput.OpenText()))
            {
                var j = JObject.Load(reader);
                Assert.That(j, Is.Not.Null);
                Assert.That(j["columns"].HasValues);
                JObject[] columns = j["columns"].Values<JObject>().ToArray();
                Assert.That(columns, Has.Length.EqualTo(2));
                Assert.That(columns[0]["name"].Value<string>(), Is.EqualTo("COLUMN1"));
                Assert.That(columns[1]["name"].Value<string>(), Is.EqualTo("COLUMN2"));
                Assert.That(j["totalRows"].HasValues, Is.False);
                Assert.That(j["totalRows"].Value<int>(), Is.EqualTo(15));
                Assert.That(j["start"].HasValues, Is.False);
                Assert.That(j["start"].Value<int>(), Is.EqualTo(0));
                Assert.That(j["end"].HasValues, Is.False);
                Assert.That(j["end"].Value<int>(), Is.EqualTo(100));

                Assert.That(j["rows"].HasValues);
                JArray[] rows = j["rows"].Values<JArray>().ToArray();
                Assert.That(rows, Has.Length.EqualTo(15));
                for (int i = 0; i < rows.Length; i++)
                {
                    Assert.That(rows[i][0]["id"].Value<string>(), Is.EqualTo("A" + i));
                    Assert.That(rows[i][1]["id"].Value<string>(), Is.EqualTo(""+ i));
                    if (i == 1)
                    {
                        Assert.That(rows[i][0]["name"].Value<string>(), Is.EqualTo("Description for A1"));
                    }
                    else if (i == 4)
                    {
                        Assert.That(rows[i][0]["name"].Value<string>(), Is.EqualTo("A4 desc"));
                    }
                    else
                    {
                        Assert.That(rows[i][0]["name"], Is.Null);
                    }
                }
            }
        }
    }
}
