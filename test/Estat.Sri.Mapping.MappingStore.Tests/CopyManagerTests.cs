using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Manager.Persist;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    class CopyManagerTests : BaseTestClassWithContainer
    {
        [Test]
        public void ShouldCopyEntitiesFromOneDbToAnother()
        {
            IEntityRetrieverManager entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            IEntityPersistenceManager entityPersistenceManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            IConfigurationStoreManager configurationStoreManager = this.IoCContainer.Resolve<IConfigurationStoreManager>();
            var commonSdmxObjectRetrievalFactory = this.IoCContainer.Resolve<ICommonSdmxObjectRetrievalFactory>();
            var copyManager = new CopyManager(new CopyFactory(entityPersistenceManager, entityRetrieverManager, new DatabaseManager(configurationStoreManager),
                commonSdmxObjectRetrievalFactory));
            var actionResult = copyManager.Copy("sqlserver", "targetSqlServer", EntityType.DdbConnectionSettings, new EntityQuery());
            Assert.That(actionResult.Status, Is.EqualTo(StatusType.Success));
        }

        [Test]
        public void ShouldCopyEntitiesWithParentsToTheSameDb()
        {
            IEntityRetrieverManager entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            IEntityPersistenceManager entityPersistenceManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            IConfigurationStoreManager configurationStoreManager = this.IoCContainer.Resolve<IConfigurationStoreManager>();
            var commonSdmxObjectRetrievalFactory = this.IoCContainer.Resolve<ICommonSdmxObjectRetrievalFactory>();
            var copyManager = new CopyManager(new CopyFactory(entityPersistenceManager, entityRetrieverManager, new DatabaseManager(configurationStoreManager),
                commonSdmxObjectRetrievalFactory));
            var actionResult = copyManager.Copy("sqlserver", EntityType.DataSet, new EntityQuery(), true);
            Assert.That(actionResult.Status, Is.EqualTo(StatusType.Success));
        }


        [Test]
        public void ShouldCopyEverythingIncludingSDMX()
        {
            IEntityRetrieverManager entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            IEntityPersistenceManager entityPersistenceManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            IConfigurationStoreManager configurationStoreManager = this.IoCContainer.Resolve<IConfigurationStoreManager>();
            var commonSdmxObjectRetrievalFactory = this.IoCContainer.Resolve<ICommonSdmxObjectRetrievalFactory>();
            var copyManager = new CopyManager(new CopyFactory(entityPersistenceManager, entityRetrieverManager, new DatabaseManager(configurationStoreManager), commonSdmxObjectRetrievalFactory));

            // and the we retrieve
            var actionResult = copyManager.Copy( "sqlserver", "targetSqlServer");
            Assert.That(actionResult.Status, Is.EqualTo(StatusType.Warning));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTestClassWithContainer"/> class.
        /// </summary>
        public CopyManagerTests()
            : base("sqlServer")
        {
        }


    }
}
