// -----------------------------------------------------------------------
// <copyright file="SQLScriptLocationManagerTest.cs" company="EUROSTAT">
//   Date Created : 2017-03-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Estat.Sri.Mapping.MappingStore.Engine;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Config;

    using NUnit.Framework;

    public class SQLScriptLocationManagerTest
    {
        /// <summary>
        /// The location manager
        /// </summary>
        private readonly ISqlScriptProviderFactory _providerEngine = new SqlScriptProviderFactory();

        [TestCaseSource(nameof(GenerateVersions))]
        public int TestGetAvailableVersions(Version startVersion, Version endVersion)
        {
            var availableVersions = this._providerEngine.GetEngine(MappingStoreDefaultConstants.SqlServerName).GetAvailableVersions(startVersion,
                endVersion).ToArray();
            Assert.That(availableVersions, Has.All.GreaterThan(startVersion));
            Assert.That(availableVersions, Has.All.LessThanOrEqualTo(endVersion));
            Assert.That(availableVersions, Is.Ordered);

            return availableVersions.Length;
        }

        [TestCase(MappingStoreDefaultConstants.SqlServerName)]
        [TestCase(MappingStoreDefaultConstants.OracleName)]
        [TestCase(MappingStoreDefaultConstants.MySqlName)]
        public void TestGetInitScript(string provider)
        {
            ISqlScriptProviderEngine sqlScriptProviderEngine = _providerEngine.GetEngine(provider);
            Assert.That(sqlScriptProviderEngine, Is.Not.Null);
            string[] sqlStatements = sqlScriptProviderEngine.GetSqlStatements().ToArray();
            Assert.That(sqlStatements.Length, Is.GreaterThan(283));
        }

        static IEnumerable GenerateVersions()
        {
            yield return new TestCaseData(new Version(3, 0), new Version(4, 1)).Returns(5);
            yield return new TestCaseData(new Version(5, 0), new Version(5, 1)).Returns(1);
        }
    }
}