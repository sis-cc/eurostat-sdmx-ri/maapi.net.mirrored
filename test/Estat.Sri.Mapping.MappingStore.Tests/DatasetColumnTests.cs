﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Config;
using NSubstitute;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    public class DatasetColumnTests
    {
        [TestCase("odp")]
        [TestCase("mysql")]
        [TestCase("sqlserver")]
        public void ShouldUpdateADataset(string mappingStoreIdentifier)
        {
            var patch = new PatchRequest
            {
                new PatchDocument("replace", nameof(DataSetColumnEntity.Name), "MyDSColumn"),
                new PatchDocument("replace", nameof(DataSetColumnEntity.Description), "replace test"),
                new PatchDocument("replace", nameof(DataSetColumnEntity.LastRetrieval),DateTime.Now),
            };
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(mappingStoreIdentifier);
            entityPersistenceEngine.Update(patch, EntityType.DataSetColumn, "987");
        }
    }
}
