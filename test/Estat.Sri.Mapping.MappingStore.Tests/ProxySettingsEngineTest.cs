// -----------------------------------------------------------------------
// <copyright file="ProxySettingsEngineTest.cs" company="EUROSTAT">
//   Date Created : 2021-06-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System.Configuration;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Engine;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class ProxySettingsEngineTest : BaseTestClassWithContainer
    {
        private IProxySettingsEngine _proxySettingsEngine;

        public ProxySettingsEngineTest(string storeId) : base(storeId)
        {
            var connectionStringSettings = new ConnectionStringRetriever().GetConnectionStringSettings(storeId);
            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { connectionStringSettings });

            _proxySettingsEngine = new ProxySettingsEngine(configurationStoreManager);
        }

        [Test]
        public void ShouldUpdateProxySettings()
        {
            var proxySettings = new ProxySettings(true, true, "TestUrl", "admin", "admin");
            _proxySettingsEngine.Update(StoreId, proxySettings);
            ProxySettings readedSettings =_proxySettingsEngine.Get(StoreId);

            Assert.IsNotNull(readedSettings);
            Assert.That(readedSettings.EnableProxy, Is.EqualTo(proxySettings.EnableProxy));
            Assert.That(readedSettings.Authentication, Is.EqualTo(proxySettings.Authentication));
            Assert.That(readedSettings.URL, Is.EqualTo(proxySettings.URL));
            Assert.That(readedSettings.Username, Is.EqualTo(proxySettings.Username));
            Assert.That(readedSettings.Password, Is.EqualTo(proxySettings.Password));
        }

        [Test]
        public void ShouldUpdateProxySettings2()
        {
            var proxySettings = new ProxySettings(true, false, null, null, null);
            _proxySettingsEngine.Update(StoreId, proxySettings);
            ProxySettings readedSettings = _proxySettingsEngine.Get(StoreId);

            Assert.IsNotNull(readedSettings);
            Assert.That(readedSettings.EnableProxy, Is.EqualTo(proxySettings.EnableProxy));
            Assert.That(readedSettings.Authentication, Is.EqualTo(proxySettings.Authentication));
            Assert.That(readedSettings.URL, Is.EqualTo(proxySettings.URL));
            Assert.That(readedSettings.Username, Is.EqualTo(proxySettings.Username));
            Assert.That(readedSettings.Password, Is.EqualTo(proxySettings.Password));
        }
    }
}
