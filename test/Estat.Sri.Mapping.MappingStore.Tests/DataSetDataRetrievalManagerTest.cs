// -----------------------------------------------------------------------
// <copyright file="DataSetDataRetrievalManagerTest.cs" company="EUROSTAT">
//   Date Created : 2017-05-08
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Manager;

    using NUnit.Framework;

    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class DataSetDataRetrievalManagerTest : BaseTestClassWithContainer
    {

        private IDataSetDataRetrievalManager _dataSetDataRetrievalManager;

        private IEntityRetrieverManager _entityPersistenceManager;

        private IEntityRetrieverEngine<DatasetEntity> _dataSetEntityRetriever;

        private IEntityRetrieverEngine<DataSetColumnEntity> _dataSetColumnRetriever;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public DataSetDataRetrievalManagerTest(string storeId) : base(storeId)
        {
           
            this._dataSetDataRetrievalManager = this.IoCContainer.Resolve<IDataSetDataRetrievalManager>();
            this._entityPersistenceManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            this._dataSetEntityRetriever = this._entityPersistenceManager.GetRetrieverEngine<DatasetEntity>(storeId);
            this._dataSetColumnRetriever = this._entityPersistenceManager.GetRetrieverEngine<DataSetColumnEntity>(storeId);
            
        }

        [Test]
        public void ShouldGetAvailableData()
        {
            var datasetEntities = this._dataSetEntityRetriever.GetEntities(new EntityQuery(), Detail.Full);
            foreach (var datasetEntity in datasetEntities)
            {
                LocalCodeResults localCodeResults;
                try
                {
                    localCodeResults = this._dataSetDataRetrievalManager.GetAvailableData(datasetEntity, new HashSet<string>(), 0, 10);
               }
                catch (Exception e)
                {
                    Trace.WriteLine(e);
                    Trace.WriteLine(datasetEntity.Name);
                    throw;
                }

                Assert.That(localCodeResults, Is.Not.Null, datasetEntity.Name);
                Assert.That(localCodeResults.LocalCodes, Is.Not.Empty, datasetEntity.Name);
                var uneven = localCodeResults.LocalCodes.FirstOrDefault(list => list.Count != localCodeResults.Columns.Count)?.Count;
                if (uneven.HasValue)
                {
                    Trace.WriteLine(uneven);
                }

                Assert.That(localCodeResults.LocalCodes.All(list => list.Count == localCodeResults.Columns.Count), $"{datasetEntity.Name} {localCodeResults.Columns.Count} != {uneven}");
                Assert.That(localCodeResults.TotalRowCount, Is.GreaterThanOrEqualTo(localCodeResults.Count), $"{datasetEntity.Name} {localCodeResults.TotalRowCount} >= {localCodeResults.Columns.Count}");
            }
        }
    }
}