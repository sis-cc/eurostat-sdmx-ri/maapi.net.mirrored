// -----------------------------------------------------------------------
// <copyright file="BaseTestClassWithContainer.cs" company="EUROSTAT">
//   Date Created : 2017-07-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using DryIoc;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Engine.Streaming;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Plugin.Editor.QueryEditor.Engine;
    using Estat.Sri.Plugin.Editor.QueryEditor.Factory;
    using Estat.Sri.SdmxParseBase.Engine;
    using Estat.Sri.Utils.Helper;
    using Example;
    using log4net;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// The base test class with container.
    /// </summary>
    public abstract class BaseTestClassWithContainer
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(BaseTestClassWithContainer));

        /// <summary>
        /// The container
        /// </summary>
        private readonly Container _container;

        private readonly IMappingStoreManager _mappingStoreManager;

        protected readonly IConfigurationStoreManager connectionStringManager;
        private readonly IEntityPersistenceManager _persistManager;

        private readonly IEntityRetrieverManager _retrieveManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTestClassWithContainer"/> class.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        protected BaseTestClassWithContainer(string storeId)
        {
            try
            {
                StoreId = storeId;
                _container =
                    new Container(
                        rules =>
                        rules.With(FactoryMethod.ConstructorWithResolvableArguments)
                            .WithoutThrowOnRegisteringDisposableTransient());
                Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
                string[] folders = { "Estat.Sri.Plugin.SqlServer", "Estat.Sri.Plugin.Oracle", "Estat.Sri.Plugin.Mysql" };
                List<FileInfo> plugins = new List<FileInfo>();
                foreach (var folder in folders)
                {
                    plugins.Add(new FileInfo(string.Format("../../../../../src/{0}/bin/Debug/netstandard2.0/{0}.dll", folder)));
                    plugins.Add(new FileInfo(string.Format("../../../../../src/{0}/bin/Debug/netstandard2.0/{0}.pdb", folder)));
                }

                var assemblies = new List<Assembly>();
                foreach (var fileInfo in plugins)
                {
                    var destPath = Path.Combine(TestContext.CurrentContext.WorkDirectory, fileInfo.Name);
                    if (!File.Exists(destPath) || File.GetLastWriteTime(destPath) < fileInfo.LastWriteTime)
                    {
                        fileInfo.CopyTo(destPath, true);
                    }

                }
                foreach (var fileInfo in plugins)
                {
                    var destPath = Path.Combine(TestContext.CurrentContext.WorkDirectory, fileInfo.Name);
                    if (fileInfo.Extension.EndsWith("dll", StringComparison.OrdinalIgnoreCase))
                    {
                        assemblies.Add(Assembly.LoadFile(destPath));
                    }
                }

                assemblies.AddRange(new[] { typeof(IDatabaseProviderManager).Assembly, typeof(DatabaseManager).Assembly });
                _container.RegisterMany(assemblies
                    ,
                    type => !typeof(IEntity).IsAssignableFrom(type));
                _container.Unregister<IEntityAuthorizationManager>();
                _container.Register<IEntityAuthorizationManager, MockEntityAuthorizationManager>();
                MappingStoreIoc.Register<RetrievalEngineContainerFactory>("MappingStoreRetrieversFactory");
                _container.Register<IStructureParsingManager, StructureParsingManager>(
                    made: Made.Of(() => new StructureParsingManager()));
                _container.Register<IStructureWriterFactory, SdmxStructureWriterFactory>();
                _container.Register<IStructureWriterManager, StructureWriterManager>();
                _container.Register<IReadableDataLocationFactory, ReadableDataLocationFactory>();
                _mappingStoreManager = _container.Resolve<IMappingStoreManager>();
                connectionStringManager = _container.Resolve<IConfigurationStoreManager>();
                MappingStoreIoc.Container.RegisterMany(
                new[] { typeof(IEntityRetrieverManager).Assembly, typeof(EntityPeristenceFactory).Assembly },
                type => !typeof(IEntity).IsAssignableFrom(type),
                reuse: Reuse.Singleton,
                made: FactoryMethod.ConstructorWithResolvableArguments,
                setup: Setup.With(allowDisposableTransient: true),
                ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);
                MappingStoreIoc.Container.Register<IStructureParsingManager, StructureParsingManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep, made: FactoryMethod.ConstructorWithResolvableArguments);
                _container.Register<ICommonSdmxObjectRetrievalFactory, CommonSdmxObjectRetrievalFactory>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
                _container.Register<IDataSetEditorFactory, QueryEditorFactory>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
                //_container.Register<IEntityRetrieverFactory, DatasetRetrieverFactory>(made:Made.Of(() => CreateService(Arg.Of<IDataSetEditorManager>())));

                _persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
                _retrieveManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
                IoCContainer.Register<IRetrievalEngineContainerFactory, RetrievalEngineContainerFactory>();

            }
            catch (Exception e)
            {
                _log.Error(e);

                Console.WriteLine(e);
                throw;
            }
        }

        private static DatasetRetrieverFactory CreateService(IDataSetEditorManager dataSetEditorManager)
        {
            return new DatasetRetrieverFactory(Arg.Of<DatabaseManager>(), new Lazy<IDataSetEditorManager>(() => dataSetEditorManager));
        }

        /// <summary>
        /// Gets or sets the store identifier.
        /// </summary>
        /// <value>
        /// The store identifier.
        /// </value>
        public string StoreId { get; set; }

        /// <summary>
        /// Gets the containers
        /// </summary>
        /// <value>
        /// </value>
        public Container IoCContainer
        {
            get
            {
                return _container;
            }
        }

        protected ConnectionStringSettings GetConnectionStringSettings()
        {
            
                            var storeId = StoreId;
            var msdbSettings = GetConnectionStringSettings(storeId);
            return msdbSettings;
        }

        protected ConnectionStringSettings GetConnectionStringSettings(string storeId)
        {
            var msdbSettings =
                connectionStringManager.GetSettings<ConnectionStringSettings>()
                    .FirstOrDefault(settings => settings.Name.Equals(storeId));
            return msdbSettings;
        }

        protected IEnumerable<T> GetMutableArtefats<T>(string storeId, IStructureReference structureReference) where T : IMaintainableMutableObject
        {
            var database = new Database(GetConnectionStringSettings(storeId));
            var structureQuery = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.Null).SetStructureIdentification(structureReference).Build();
            return new RetrievalEngineContainer(database).GetEngine(structureReference.MaintainableStructureEnumType)
                .Retrieve(structureQuery).Cast<T>();
            
        }

        protected void InitializeMappingStore(string storeId)
        {
            var engineByStoreId = this._mappingStoreManager.GetEngineByStoreId(storeId);
            var actionResult = engineByStoreId.Initialize(new DatabaseIdentificationOptions() { StoreId = storeId});
            Assert.That(actionResult.Status, Is.EqualTo(StatusType.Success), string.Join(", ", actionResult.Messages));
        }

        protected IConnectionEntity AddADdbConnection()
        {
            // The input connection setting, a Connection String pointing to a Dissemination database
            var name = Guid.NewGuid().ToString();
            var ddbConnectionString = new System.Configuration.ConnectionStringSettings(name, "Server=localhost; Database=H2; User ID=testuser; Password=123;", "MySqlConnector");


            // Below we start interacting with the Mapping Assistant API
            // The goals are :
            // * Add the ddbConnectionString to the Mapping Store Database
            // * Retrieve it

            // The Connection settings are special, in the sense that we need the corresponding Database Provide plugin assistance to 
            // convert them to various forms and to be able to store them
            // First we need to get the Database Provider specific engine using the dbProviderManager and the ddbConnectionString ProviderName
            var databaseProvider = _container.Resolve<IDatabaseProviderManager>();
            IDatabaseProviderEngine dbPluginEngine = databaseProvider.GetEngineByProvider(ddbConnectionString.ProviderName);

            // Then we use the engine from above to convert the input ddbConnectionString to IConnectionEntity
            IConnectionEntity ddbSettings = dbPluginEngine.SettingsBuilder.CreateConnectionEntity(ddbConnectionString);
            ddbSettings.StoreId = StoreId;
            IEntityPersistenceManager persistenceManager = _container.Resolve<IEntityPersistenceManager>();
            DeleteByName(ddbSettings);
            return persistenceManager.Add(ddbSettings);
        }
        private void DeleteByName<T>(T entity) where T : ISimpleNameableEntity
        {
            IEntityPersistenceManager persistenceManager = _container.Resolve<IEntityPersistenceManager>();
            IEntityRetrieverManager retrieveManager = _container.Resolve<IEntityRetrieverManager>();
            var entities = retrieveManager.GetEntities<T>(this.StoreId,
                new EntityQuery() { ObjectId = new Criteria<string>(OperatorType.Exact, entity.Name) }, Detail.IdOnly);
            foreach (var simpleNameableEntity in entities)
            {
                persistenceManager.Delete<T>(StoreId, simpleNameableEntity.EntityId);
            }
        }

        protected DatasetEntity AddDataSet(IConnectionEntity entity, IMaintainableRefObject dataStructureRef, int skipComponentCount, string datasetName = null, DatasetPropertyEntity datasetProperty = null)
        {
            var dataset = new DatasetEntity();
            dataset.Name = datasetName ?? "Source Dataset";
            dataset.Description = "A Description";
            dataset.ParentId = entity.EntityId;
            dataset.StoreId = entity.StoreId;
            dataset.EditorType = "Test";
            dataset.Query = "select * from H2TEST";
            dataset.OrderByClause = "ORDER BY [TEST_COLUMN]";
            dataset.DatasetPropertyEntity = datasetProperty;

            IEntityPersistenceManager persistenceManager = _container.Resolve<IEntityPersistenceManager>();
            var addedDataSet = persistenceManager.Add(dataset);

            // add columns
            var dataStructureObject = GetMutableArtefats<IDataStructureMutableObject>(entity.StoreId, new StructureReferenceImpl( dataStructureRef, SdmxStructureEnumType.Dsd)).First().ImmutableInstance;
            List<DataSetColumnEntity> columnEntities = new List<DataSetColumnEntity>();
            foreach (var source in dataStructureObject.Components.Skip(skipComponentCount))
            {
                DataSetColumnEntity column = new DataSetColumnEntity();
                column.ParentId = addedDataSet.EntityId;
                column.StoreId = addedDataSet.StoreId;
                column.Name = string.Format(CultureInfo.InvariantCulture, "H2TEST_COLUMN_{0}", source.Id);
                column.Description = "Workaround";
                columnEntities.Add(column);
            }

            var first = dataStructureObject.GetDimensions().Skip(skipComponentCount).FirstOrDefault(dimension => dimension.HasCodedRepresentation());
            if (first != null)
            {
                DataSetColumnEntity columnExtra = new DataSetColumnEntity();
                columnExtra.ParentId = addedDataSet.EntityId;
                columnExtra.StoreId = addedDataSet.StoreId;
                columnExtra.Description = "Workaround";
                columnExtra.Name = string.Format(CultureInfo.InvariantCulture, "H2TEST_COLUMN_{0}2", first.Id);
                columnEntities.Add(columnExtra);
            }

            DataSetColumnEntity yearColumn = new DataSetColumnEntity();
            yearColumn.ParentId = addedDataSet.EntityId;
            yearColumn.StoreId = addedDataSet.StoreId;
            yearColumn.Description = "Workaround";
            yearColumn.Name = string.Format(CultureInfo.InvariantCulture, "H2TEST_COLUMN_{0}2", DimensionObject.TimeDimensionFixedId);
            columnEntities.Add(yearColumn);

            DataSetColumnEntity lastUpColumn = new DataSetColumnEntity
            {
                ParentId = addedDataSet.EntityId,
                StoreId = addedDataSet.StoreId,
                Name = string.Format(CultureInfo.InvariantCulture, "{0}_COL", ComponentMappingType.LastUpdated.AsMappingStoreType())
            };

            columnEntities.Add(lastUpColumn);
            DataSetColumnEntity validToColumn = new DataSetColumnEntity
            {
                ParentId = addedDataSet.EntityId,
                StoreId = addedDataSet.StoreId,
                Name = string.Format(CultureInfo.InvariantCulture, "{0}_COL", ComponentMappingType.ValidTo.AsMappingStoreType())
            };

            columnEntities.Add(validToColumn);
            DataSetColumnEntity updateStatusColumn = new DataSetColumnEntity
            {
                ParentId = addedDataSet.EntityId,
                StoreId = addedDataSet.StoreId,
                Name = string.Format(CultureInfo.InvariantCulture, "{0}_COL", ComponentMappingType.UpdatedStatus.AsMappingStoreType())
            };

            columnEntities.Add(updateStatusColumn);
            persistenceManager.AddEntities(columnEntities);
            return addedDataSet;
        }



        protected void SaveToMappingStoreAndJson(FileInfo source, FileInfo target)
        {
            using (Stream stream = source.OpenRead())
            using (Stream outputStream = target.Create())
            using (EntityStreamingReader reader = new EntityStreamingReader(this.StoreId, stream, EntityType.None))
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                _persistManager.AddEntities(this.StoreId, reader, writer, EntityType.None);
            }
        }

        protected IEnumerable<T> ReadEntities<T>(FileInfo source, EntityType type) where T : IEntity
        {
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader(this.StoreId, stream, type))
            {
                while (reader.MoveToNextMessage())
                {
                    while (reader.MoveToNextEntityTypeSection())
                    {
                        if (reader.CurrentEntityType == type)
                        {
                            while (reader.MoveToNextEntity())
                            {
                                yield return (T)reader.CurrentEntity.Entity;
                            }
                        }
                    }
                }
            }
        }
    }
}