﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Manager;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System.Configuration;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    [TestFixture]
    public class CommandBuilderTests
    {
        private readonly Database _database;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public CommandBuilderTests()
        {
            _database = new Database(new ConnectionStringSettings("test", "Initial Catalog=.;Data Source=master", "System.Data.SqlClient"));
        }

        [Test]
        public void ShouldConstructAConcatenatedStringOfColumns()
        {
            var commandBuilder = new CommandBuilder(_database, new DatabaseMapperManager(DatabaseInformationType.DdbConnectionSettings));
            var columnList = commandBuilder.CreateColumnList(new List<string> {"password","dbType","port"});
            Assert.That(columnList,Is.EqualTo("DB_PASSWORD,DB_TYPE,DB_PORT"));
        }


        [Test]
        public void ShouldConstructAConcatenatedStringOfParameters()
        {
            var commandBuilder = new CommandBuilder(_database, new DatabaseMapperManager(DatabaseInformationType.DdbConnectionSettings));
            var parameterList = commandBuilder.CreateParameterList(new List<string> { "password", "dbType", "port" });
            Assert.That(parameterList, Is.EqualTo("@DB_PASSWORD,@DB_TYPE,@DB_PORT"));
        }


        [Test]
        public void ShouldConstructAConcatenatedStringOfColumnsWithParametersWithCommaSeparator()
        {
            var commandBuilder = new CommandBuilder(_database, new DatabaseMapperManager(DatabaseInformationType.DdbConnectionSettings));
            var parameterList = commandBuilder.CreateColumnsWithParameters(new List<string> { "password", "dbType", "port" }, SqlSeparator.Comma);
            Assert.That(parameterList, Is.EqualTo("DB_PASSWORD=@DB_PASSWORD,DB_TYPE=@DB_TYPE,DB_PORT=@DB_PORT"));
        }

        [Test]
        public void ShouldConstructAConcatenatedStringOfColumnsWithParametersWithANDSeparator()
        {
            var commandBuilder = new CommandBuilder(_database, new DatabaseMapperManager(DatabaseInformationType.DdbConnectionSettings));
            var parameterList = commandBuilder.CreateColumnsWithParameters(new List<string> { "password", "dbType", "port" }, SqlSeparator.And);
            Assert.That(parameterList, Is.EqualTo("DB_PASSWORD=@DB_PASSWORD AND DB_TYPE=@DB_TYPE AND DB_PORT=@DB_PORT"));
        }
    }
}
