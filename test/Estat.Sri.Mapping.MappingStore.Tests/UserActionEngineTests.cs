﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Model;
using NUnit.Framework;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    class UserActionEngineTests
    {
        
        
    }


    [TestFixture]
    public class UserActionPathAndValuesTests: BaseTestClassWithContainer
    {
        private readonly IEntityRetrieverManager _entityRetrieverManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public UserActionPathAndValuesTests():base("sqlServer")
        {
            this._entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
        }

        [Test]
        public void ShouldGeneratePathAndValuesForConnection()
        {
            var entityId = "1";
            var name = "someName";
            IEntity entity = new DdbConnectionEntity()
            {
                EntityId = entityId,
                Name = name
            };
            var userAction = UserAction.Insert;

            var pathAndValues = this.CreateSUT().CreateFor(entity,userAction);

            object value =string.Empty;
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.EntityId), out value) && value.ToString() == entityId);
            Assert.That(pathAndValues.TryGetValue(nameof(SimpleNameableEntity.Name), out value) && value.ToString() == name);
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.EntityType), out value) && value.ToString() == EntityType.DdbConnectionSettings.ToString());
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.OperationType), out value) && value.ToString() == userAction.ToString());
        }

        [Test]
        public void ShouldGeneratePathAndValuesForDataset()
        {
            var entityId = "1";
            var name = "someName";
            IEntity entity = new DatasetEntity()
            {
                EntityId = entityId,
                Name = name
            };
            var userAction = UserAction.Insert;

            var pathAndValues = this.CreateSUT().CreateFor(entity, userAction);

            object value = string.Empty;
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.EntityId), out value) && value.ToString() == entityId);
            Assert.That(pathAndValues.TryGetValue(nameof(SimpleNameableEntity.Name), out value) && value.ToString() == name);
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.EntityType), out value) && value.ToString() == EntityType.DataSet.ToString());
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.OperationType), out value) && value.ToString() == userAction.ToString());
        }

        [Test]
        public void ShouldGeneratePathAndValuesForMappingSet()
        {
            var entityId = "1";
            var name = "someName";
            IEntity entity = new MappingSetEntity()
            {
                EntityId = entityId,
                Name = name
            };
            var userAction = UserAction.Insert;

            var pathAndValues = this.CreateSUT().CreateFor(entity, userAction);

            object value = string.Empty;
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.EntityId), out value) && value.ToString() == entityId);
            Assert.That(pathAndValues.TryGetValue(nameof(SimpleNameableEntity.Name), out value) && value.ToString() == name);
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.EntityType), out value) && value.ToString() == EntityType.MappingSet.ToString());
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.OperationType), out value) && value.ToString() == userAction.ToString());
        }


        [Test]
        public void ShouldGeneratePathAndValuesForHeader()
        {
            var entityId = "1";
            var parentId = "3";
            IEntity entity = new HeaderEntity()
            {
                EntityId = entityId,
                ParentId = parentId
            };
            var userAction = UserAction.Insert;

            var pathAndValues = this.CreateSUT().CreateFor(entity, userAction);

            object value = string.Empty;
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.EntityId), out value) && value.ToString() == entityId);
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.ParentId), out value) && value.ToString() == parentId);
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.EntityType), out value) && value.ToString() == EntityType.Header.ToString());
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.OperationType), out value) && value.ToString() == userAction.ToString());
        }


        [Test]
        public void ShouldGeneratePathAndValuesForUser()
        {
            var entityId = "1";
            var parentId = "3";
            IEntity entity = new UserEntity()
            {
                EntityId = entityId,
                ParentId = parentId
            };
            var userAction = UserAction.Insert;

            var pathAndValues = this.CreateSUT().CreateFor(entity, userAction);

            object value = string.Empty;
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.EntityId), out value) && value.ToString() == entityId);
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.EntityType), out value) && value.ToString() == EntityType.User.ToString());
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.OperationType), out value) && value.ToString() == userAction.ToString());
        }

        [Test]
        public void ShouldGeneratePathAndValuesForRegistry()
        {
            var entityId = "1";
            var parentId = "3";
            IEntity entity = new RegistryEntity()
            {
                EntityId = entityId,
                ParentId = parentId
            };
            var userAction = UserAction.Insert;

            var pathAndValues = this.CreateSUT().CreateFor(entity, userAction);

            object value = string.Empty;
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.EntityId), out value) && value.ToString() == entityId);
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.EntityType), out value) && value.ToString() == EntityType.Registry.ToString());
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.OperationType), out value) && value.ToString() == userAction.ToString());
        }

        private UserActionPathAndValues CreateSUT()
        {
            return new UserActionPathAndValues(this._entityRetrieverManager, "sqlserver");
        }

        [Test]
        public void ShouldGeneratePathAndValuesForDatasetColumn()
        {
            var entityId = "1";
            var name = "parentName";
            string parentId = "2";
            IEntity entity = new DataSetColumnEntity()
            {
                EntityId = entityId,
                ParentId = parentId
            };
            var userAction = UserAction.Insert;

            var pathAndValues = this.CreateSUT().CreateFor(entity, userAction);

            object value = string.Empty;
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.EntityId), out value) && value.ToString() == parentId);
            Assert.That(pathAndValues.TryGetValue(nameof(SimpleNameableEntity.Name), out value) && value.ToString() == "DS_6_161_267");
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.EntityType), out value) && value.ToString() == EntityType.DataSet.ToString());
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.OperationType), out value) && value.ToString() == UserAction.Update.ToString());
        }


        [Test]
        public void ShouldGeneratePathAndValuesForLocalCode()
        {
            var entityId = "1";
            var name = "parentName";
            string parentId = "2";
            IEntity entity = new LocalCodeEntity()
            {
                EntityId = entityId,
                ParentId = parentId
            };
            var userAction = UserAction.Insert;

            var pathAndValues = this.CreateSUT().CreateFor(entity, userAction);

            object value = string.Empty;
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.EntityId), out value) && value.ToString() == "1");
            Assert.That(pathAndValues.TryGetValue(nameof(SimpleNameableEntity.Name), out value) && value.ToString() == "DS_6_157_80");
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.EntityType), out value) && value.ToString() == EntityType.DataSet.ToString());
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.OperationType), out value) && value.ToString() == UserAction.Update.ToString());
        }


        [Test]
        public void ShouldGeneratePathAndValuesForDescSource()
        {
            var entityId = "1";
            var name = "parentName";
            string parentId = "2";
            IEntity entity = new ColumnDescriptionSourceEntity()
            {
                EntityId = entityId,
                ParentId = parentId
            };
            var userAction = UserAction.Insert;

            var pathAndValues = this.CreateSUT().CreateFor(entity, userAction);

            object value = string.Empty;
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.EntityId), out value) && value.ToString() == "1");
            Assert.That(pathAndValues.TryGetValue(nameof(SimpleNameableEntity.Name), out value) && value.ToString() == "DS_6_157_80");
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.EntityType), out value) && value.ToString() == EntityType.DataSet.ToString());
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.OperationType), out value) && value.ToString() == UserAction.Update.ToString());
        }


        [Test]
        public void ShouldGeneratePathAndValuesForComponentMapping()
        {
            var entityId = "1";
            var name = "parentName";
            string parentId = "2";
            IEntity entity = new ComponentMappingEntity()
            {
                EntityId = entityId,
                ParentId = parentId
            };
            var userAction = UserAction.Insert;

            var pathAndValues = this.CreateSUT().CreateFor(entity, userAction);

            object value = string.Empty;
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.EntityId), out value) && value.ToString() == "1");
            Assert.That(pathAndValues.TryGetValue(nameof(SimpleNameableEntity.Name), out value) && value.ToString() == "MS_6_161_267");
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.EntityType), out value) && value.ToString() == EntityType.MappingSet.ToString());
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.OperationType), out value) && value.ToString() == UserAction.Update.ToString());
        }

        [Test]
        public void ShouldGeneratePathAndValuesForTranscoding()
        {
            var entityId = "1";
            var name = "parentName";
            string parentId = "2";
            IEntity entity = new TranscodingEntity()
            {
                EntityId = entityId,
                ParentId = parentId
            };
            var userAction = UserAction.Insert;

            var pathAndValues = this.CreateSUT().CreateFor(entity, userAction);

            object value = string.Empty;
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.EntityId), out value) && value.ToString() == "1");
            Assert.That(pathAndValues.TryGetValue(nameof(SimpleNameableEntity.Name), out value) && value.ToString() == "MS_6_157_80");
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.EntityType), out value) && value.ToString() == EntityType.MappingSet.ToString());
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.OperationType), out value) && value.ToString() == UserAction.Update.ToString());
        }


        [Test]
        public void ShouldGeneratePathAndValuesForTranscodingRule()
        {
            var entityId = "1";
            var name = "parentName";
            string parentId = "2";
            IEntity entity = new TranscodingRuleEntity()
            {
                EntityId = entityId,
                ParentId = parentId
            };
            var userAction = UserAction.Insert;

            var pathAndValues = this.CreateSUT().CreateFor(entity, userAction);

            object value = string.Empty;
            Assert.That(pathAndValues.TryGetValue(nameof(Entity.EntityId), out value) && value.ToString() == "1");
            Assert.That(pathAndValues.TryGetValue(nameof(SimpleNameableEntity.Name), out value) && value.ToString() == "MS_6_157_80");
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.EntityType), out value) && value.ToString() == EntityType.MappingSet.ToString());
            Assert.That(pathAndValues.TryGetValue(nameof(UserActionEntity.OperationType), out value) && value.ToString() == UserAction.Update.ToString());
        }
    }
}
