using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Config;
using NSubstitute;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class DdbConnectionSettingsTest
    {
        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        private readonly ConnectionStringSettings _connectionStringSettings;

        public DdbConnectionSettingsTest(string name)
        {
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(name);
        }

        [Test]
        public void GetAllDdbConnectionSetting()
        {
            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DdbConnectioSettingsRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<DdbConnectionEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.AnyValue, null));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
            //Assert.That(result.First().Name, Is.Not.Null);
        }

        [Test]
        public void GetADdbConnectionSetting()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DdbConnectioSettingsRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<DdbConnectionEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.Exact, "101"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
            //Assert.That(result.First().Name, Is.Not.Null);
        }


        [Test]
        public void GetADdbConnectionSettingBasedOnAdditionalCriteria()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DdbConnectioSettingsRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<DdbConnectionEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>()
            {
                { "DatabaseType",new Criteria<string>(OperatorType.Exact,"SqlServer" )}
            });
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
            //Assert.That(result.First().Name, Is.EqualTo("CONN_DDB_B_TEST_JANUARY2014"));
        }

        [Test]
        public void GetADdbConnectionSettingBasedOnName()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DdbConnectioSettingsRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<DdbConnectionEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>()
            {
                { "Name",new Criteria<string>(OperatorType.Exact,"CONN_DDB_B_TEST_JANUARY2014" )}
            });
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            //Assert.That(result, Is.Not.Empty);
            //Assert.That(result.First().Name, Is.EqualTo("CONN_DDB_B_TEST_JANUARY2014"));
        }

    }
}