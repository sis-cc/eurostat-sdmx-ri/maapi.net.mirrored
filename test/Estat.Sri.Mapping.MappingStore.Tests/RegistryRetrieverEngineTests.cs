// -----------------------------------------------------------------------
// <copyright file="RegistryRetrieverEngineTests.cs" company="EUROSTAT">
//   Date Created : 2017-06-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Extension;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Utils;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("msdb_scratch.sqlserver")]
    [TestFixture("msdb_scratch.oracle")]
    public class RegistryRetrieverEngineTests : BaseTestClassWithContainer
    {
        private readonly IEntityPersistenceManager _persistManager;

        private readonly IEntityRetrieverManager _retrieveManager;



        public RegistryRetrieverEngineTests(string name) : base(name)
        {
            _persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            _retrieveManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
        }

        [OneTimeSetUp]
        public void Setup()
        {
            InitializeMappingStore(this.StoreId);
        }


        [Test]
        public void GetARegistrySetting()
        {
            var entities = _retrieveManager.GetEntities<RegistryEntity>(this.StoreId, EntityQuery.Empty, Detail.Full).ToArray();
            Assert.IsNotNull(entities);
            Assert.AreEqual(entities.Length, 6);
            Assert.IsFalse(entities.Any(r => string.IsNullOrEmpty(r.Technology)));
            Assert.IsFalse(entities.Any(r => string.IsNullOrEmpty(r.Name)));
            Assert.IsFalse(entities.Any(r => string.IsNullOrEmpty(r.Url)));
        }


        [Test]
        public void InsertARegistrySetting()
        {

            List<RegistryEntity> entitiesToAdd = new List<RegistryEntity>()
            {
                new()
                {
                    Url = "http://localhost/rest2",
                    UserName = "testName",
                    Password = "testPassword",
                    Name = "RestV2 server",
                    Technology = "RestV20",
                    StoreId = this.StoreId
                },
                new()
                {
                    Url = "http://localhost/rest",
                    Name = "RestV1 server",
                    Technology = "REst",
                    StoreId = this.StoreId

                },
                new()
                {
                    Url = "http://localhost/soap20",
                    Name = "SOAP 2.0 server",
                    Technology = "SoapV20",
                    StoreId = this.StoreId

                },
                new()
                {
                    Url = "http://localhost/soap21",
                    Name = "SOAP 2.1 server",
                    Technology = "SoapV21",
                    Proxy = true,
                    StoreId = this.StoreId

                }

            };
            var addedEntities = _persistManager.AddEntities(entitiesToAdd).ToList();
            Assert.IsNotNull(addedEntities);
            Assert.IsTrue(addedEntities.Any());
            Assert.AreEqual(entitiesToAdd.Count, addedEntities.Count);
            Assert.IsFalse(addedEntities.Any(r => string.IsNullOrEmpty(r.EntityId)));
            // Oracle doesn't return in the same order that were added
            foreach (var entity in addedEntities)
            {
                var retrieved = _retrieveManager.GetEntities<RegistryEntity>(this.StoreId, entity.QueryForThisEntityId(), Detail.Full).FirstOrDefault();
                _persistManager.Delete<RegistryEntity>(this.StoreId, entity.EntityId);
                Assert.IsNotNull(retrieved);
                Assert.AreEqual(entity.EntityId, retrieved.EntityId);
                Assert.AreEqual(entity.Technology, retrieved.Technology);
                Assert.AreEqual(entity.IsPublic, retrieved.IsPublic);
                Assert.AreEqual(entity.Name, retrieved.Name);
                Assert.AreEqual(entity.Url, retrieved.Url);
                Assert.AreEqual(entity.Proxy, retrieved.Proxy);
                Assert.AreEqual(entity.UserName, retrieved.UserName);
                Assert.AreEqual(entity.Password, retrieved.Password);
            }
        }


//      [Test]
        public void DeleteARegistrySetting()
        {
            //var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            //configurationStoreManager.GetSettings<ConnectionStringSettings>()
            //    .Returns(new[] { this._connectionStringSettings });
            //var databaseManager = new DatabaseManager(configurationStoreManager);
            //var registryRetrieverEngine = new RegistryPersistenceEngine(databaseManager, this._connectionStringSettings.Name, new CommandsFromUpdateInfo(),new UpdateInfoFactoryManager());
            //registryRetrieverEngine.Delete("2",EntityType.Registry);
        }


        [TestCase(@"[{""op"":""replace"",""path"":""/name"",""value"":""SDMX Global Registry""},{""op"":""replace"",""path"":""/description"",""value"":null},{""op"":""replace"",""path"":""/url"",""value"":""https://registry.sdmx.org/ws/public/sdmxapi/rest/""},{""op"":""replace"",""path"":""/technology"",""value"":""Rest""},{""op"":""replace"",""path"":""/username"",""value"":null},{""op"":""replace"",""path"":""/password"",""value"":""""},{""op"":""replace"",""path"":""/useProxy"",""value"":false},{""op"":""replace"",""path"":""/isPublic"",""value"":true},{""op"":""replace"",""path"":""/upgrades"",""value"":true}]", "2")]
        public void UpdateARegistrySetting(string json, string entityId)
        {
            PatchRequest patchRequest = PatchRequestParser.ParsePatchFromText(json);
            _persistManager.GetEngine<RegistryEntity>(this.StoreId).Update(patchRequest, EntityType.Registry, entityId);
            
            //var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            //configurationStoreManager.GetSettings<ConnectionStringSettings>()
            //    .Returns(new[] { this._connectionStringSettings });
            //var databaseManager = new DatabaseManager(configurationStoreManager);
            //var registryRetrieverEngine = new RegistryPersistenceEngine(databaseManager, this._connectionStringSettings.Name, new CommandsFromUpdateInfo(), new UpdateInfoFactoryManager());
            //registryRetrieverEngine.Update(new PatchRequest()
            //{
            //    new PatchDocument("replace","/url","some updated string")
            //}, EntityType.Registry, "1");
        }
    }
}