// -----------------------------------------------------------------------
// <copyright file="ContextWithTransactionTest.cs" company="EUROSTAT">
//   Date Created : 2017-09-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Linq;

    using Dapper;

    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.Utils;
    using log4net;

    using NUnit.Framework;

    [TestFixture("msdb_scratch.oracle")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.sqlserver")]
    public class ContextWithTransactionTest
    {
        private readonly Database _database;

        public ContextWithTransactionTest(string connectionName)
        {
            IConfigurationStoreManager connectionStringManager = new ConfigurationStoreManager(new AppConfigStoreFactory());
            var msdbSettings =
                connectionStringManager.GetSettings<ConnectionStringSettings>()
                    .FirstOrDefault(settings => settings.Name.Equals(connectionName));
            _database = new Database(msdbSettings);
        }

        [Test]
        public void ConnectionWithTransactionShouldDispose()
        {
            DbConnection connection;
            using (var context = new ContextWithTransaction(_database))
            {
                connection = context.Connection;
                context.Complete();
            }

            Assert.That(connection.State.HasFlag(ConnectionState.Closed));
        }

        [Test]
        public void ConnectionWithTransactionNested()
        {
            _database.ExecuteNonQuery("DELETE from ARTEFACT WHERE ID='TEST_DELETE_ME' and AGENCY='TEST'", null);
            using (var context = new ContextWithTransaction(_database))
            {
                InsertDummyRecord(context.Connection);

                // HACK needed by MYSQL/MARIA DB
                context.Connection.Close();
                using (var nested = new ContextWithTransaction(_database))
                {
                    try
                    {
                        InsertDummyRecord(nested.Connection);
                        nested.Complete();
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine(e);
                        LogManager.GetLogger(this.GetType()).Error(e);
                        throw;
                    }
                }

                context.Complete();
            }
            var enumerable = GetDummyRecords();
            Assert.That(enumerable.Count(), Is.EqualTo(2));
        }
        [Test]
        public void ConnectionWithTransactionNestedRollback()
        {
            _database.ExecuteNonQuery("DELETE from ARTEFACT WHERE ID='TEST_DELETE_ME' and AGENCY='TEST'", null);
            try
            {
                using (var context = new ContextWithTransaction(_database))
                {
                    InsertDummyRecord(context.Connection);

                    // HACK needed by MYSQL/MARIA DB
                    context.Connection.Close();
                    using (var nested = new ContextWithTransaction(_database))
                    {
                        InsertDummyRecord(nested.Connection);
                        throw new ApplicationException();
                    }
                }
            }
            catch (ApplicationException)
            {
            }

            var enumerable = GetDummyRecords();
            Assert.That(enumerable.Count(), Is.EqualTo(0));
        }
        [Test]
        public void ConnectionWithTransactionShouldRollback()
        {
            _database.ExecuteNonQuery("DELETE from ARTEFACT WHERE ID='TEST_DELETE_ME' and AGENCY='TEST'", null);
            using (var context = new ContextWithTransaction(_database))
            {
                var connection = context.Connection;
                InsertDummyRecord(connection);
            }

            var enumerable = GetDummyRecords();
            Assert.That(enumerable.Count(), Is.EqualTo(0));
        }

        private IEnumerable<dynamic> GetDummyRecords()
        {
            var enumerable = _database.Query<dynamic>("SELECT * from ARTEFACT WHERE ID='TEST_DELETE_ME' and AGENCY='TEST'");
            return enumerable;
        }

        [Test]
        public void ConnectionWithTransactionShouldCommit()
        {
            _database.ExecuteNonQuery("DELETE from ARTEFACT WHERE ID='TEST_DELETE_ME' and AGENCY='TEST'", null);
            using (var context = new ContextWithTransaction(_database))
            {
                var connection = context.Connection;
                InsertDummyRecord(connection);
                context.Complete();
            }

            var enumerable = GetDummyRecords();
            Assert.That(enumerable.Count(), Is.EqualTo(1));
            _database.ExecuteNonQuery("DELETE from ARTEFACT WHERE ID='TEST_DELETE_ME' and AGENCY='TEST'", null);
        }

        private static void InsertDummyRecord(DbConnection connection)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("p_id", "TEST_DELETE_ME");
            p.Add("p_version", "1.0");
            p.Add("p_agency", "TEST");
            p.Add("p_valid_from", new DateTime(2013, 2, 3));
            p.Add("p_valid_to", new DateTime(2019, 2, 3));
            p.Add("p_is_final", 1);
            p.Add("p_uri", string.Empty);
            p.Add("p_last_modified", DateTime.UtcNow);
            p.Add("p_pk", dbType: DbType.Int64, direction: ParameterDirection.Output);
            connection.Execute("INSERT_ARTEFACT", p, commandType: CommandType.StoredProcedure);
        }
    }
}