using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStore.Store.Engine;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using NSubstitute;
using Estat.Sri.Mapping.Api.Manager;
using Org.Sdmxsource.Sdmx.Util.Date;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Mapping.MappingStore.Factory;
using System.Configuration;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Estat.Sri.Mapping.MappingStore.Engine.Cloning;
using DryIoc;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using System.Security.Cryptography;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;
using DataSetColumnEntity = Estat.Sri.Mapping.Api.Model.DataSetColumnEntity;
using MappingSetEntity = Estat.Sri.Mapping.Api.Model.MappingSetEntity;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    public class DataStructureOnTheFlyEngineTests : BaseTestClassWithContainer
    {
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        private ConnectionStringSettings _connectionStringSettings;
        private Database db;
        private EntityRetrieverManager retrieverManager;

        public DataStructureOnTheFlyEngineTests() : base("sqlserver")
        {
        }

        [Test]
        public void CreateADSDOnTheFy()
        {
            var storeId = "sqlserver";
            var dataStructureOnTheFlyEngine = GetEngine(storeId);
            var retrieval = this.IoCContainer.Resolve<ICommonSdmxObjectRetrievalFactory>();
            var retriever = retrieval.GetManager(db.ConnectionStringSettings);
            ICommonStructureQuery commonQuery = CommonStructureQueryCore.Builder
            .NewQuery(CommonStructureQueryType.Other, null)
                .SetMaintainableIds("SDMX_Q_PERIODS")
                .SetMaintainableTarget(SdmxStructureEnumType.CodeList)
                .Build();
            var codelist =  retriever.GetMaintainables(commonQuery).Codelists.FirstOrDefault();
            if(codelist != null && !codelist.IsFinal.IsTrue ) {
                var mutable = codelist.MutableInstance;
                mutable.FinalStructure = TertiaryBool.ParseBoolean(true);
                var mappingStoreManager = new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(db.ConnectionStringSettings, new List<ArtefactImportStatus>());
                mappingStoreManager.SaveStructure(mutable.ImmutableInstance);
            }

            
            var transcodings = new Dictionary<string,string>();
            transcodings.Add("Q1", "T1");
            transcodings.Add("Q2", "T2");
            transcodings.Add("Q3", "T3");
            transcodings.Add("Q4", "T4");

            List<TemplateMapping> mappings = new List<TemplateMapping>()
            {
                new TemplateMapping()
                {
                    ColumnName = "ESA_MAITAT_101_ACCOUNTING_ENTR",
                    ColumnDescription = "The frequency",
                    ConceptId = 1,
                    ComponentId = "FREQ",
                    ComponentType = "Dimension",
                    Transcodings = transcodings,
                    ItemSchemeId = new StructureReferenceImpl("MA","SDMX_Q_PERIODS","1.0",SdmxStructureEnumType.CodeList)
                },
                new TemplateMapping()
                {
                    ColumnName = "ESA_MAI_TOT_ESTAT_101_ACTIVITY",
                    ColumnDescription = "The time",
                    ConceptId = 2,
                    ComponentId = "TIME_PERIOD",
                    ComponentType = "TimeDimension",
                    //Transcodings = new Dictionary<string, string>(){{"const","const" } }
                },

                 new TemplateMapping()
                {
                    ColumnName = "ESA_MAIOT_ESTAT_101_ADJUSTMENT",
                    ColumnDescription = "Value",
                    ConceptId = 2,
                    ComponentId = "OBS_VALUE",
                    ComponentType = "PrimaryMeasure",
                },

            };

            var datasetName = "DS_NA_ESTAT_MAIN_T0101A";
            var dataset = retrieverManager.GetEntities<DatasetEntity>(storeId, EntityQuery.Empty, Api.Constant.Detail.IdAndName).Where(x=>x.Name == datasetName).FirstOrDefault();
            var dsdOnTheFly = new DSDOnTheFlyEntity()
            {
                DsdId = new StructureReferenceImpl()
                {
                    AgencyId = "ESTAT",
                    MaintainableId = "TEST",
                    Version = "2.0"
                },
                Mappings = mappings,
                DsdNames = new Dictionary<string, string>() { { "en", "en" }, { "it", "it" } },
                Dataset = new SimpleNameableEntity(Api.Constant.EntityType.DataSet) { Name = datasetName, EntityId = dataset?.EntityId }
            };
            dataStructureOnTheFlyEngine.Add(storeId,dsdOnTheFly);
        }


        public DataStructureOnTheFlyEngine GetEngine(string storeId)
        {
            this._connectionStringSettings = ConfigurationManager.ConnectionStrings[storeId];
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });

            var databaseManager = new DatabaseManager(configurationStoreManager);

            db = databaseManager.GetDatabase(storeId);
          
            retrieverManager = new EntityRetrieverManager(new DatasetColumnRetrieverFactory(databaseManager), new DatasetRetrieverFactory(databaseManager));
            var datasetSetColumnRetriver = (DefaultsEntityRetrieverEngine<DataSetColumnEntity>)retrieverManager.GetRetrieverEngine<DataSetColumnEntity>(this._connectionStringSettings.Name);
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);

            MappingSetPersistenceEngine mappingSetPersistenceEngine = (MappingSetPersistenceEngine)entityPeristenceFactory.GetEngine<MappingSetEntity>(storeId);
            ComponentMappingPersistenceEngine componentMappingPersistenceEngine = (ComponentMappingPersistenceEngine)entityPeristenceFactory.GetEngine<ComponentMappingEntity>(storeId);
            TranscodingRuleEntityPersistenceEngine transcodingRuleEntityPersistenceEngine = (TranscodingRuleEntityPersistenceEngine)new TranscodingRulePersistFactory(databaseManager).GetEngine<TranscodingRuleEntity>(storeId);
            var mappingSetRetriever = new MappingSetEntityRetriever(db, storeId);
            var componentMappingRetriever = new ComponentMappingRetrieverEngine(db);
            var transcodingRuleRetriever = new TranscodingRuleRetrieverEngine(db);
            var entityCloneHelper = new EntityCloneHelper(this.IoCContainer.Resolve<IEntityRetrieverManager>(), this.IoCContainer.Resolve<IEntityPersistenceManager>(), storeId);
            return new DataStructureOnTheFlyEngine(databaseManager, datasetSetColumnRetriver, mappingSetPersistenceEngine, componentMappingPersistenceEngine, transcodingRuleEntityPersistenceEngine
                , mappingSetRetriever, componentMappingRetriever, transcodingRuleRetriever, entityCloneHelper, this.IoCContainer.Resolve<ICommonSdmxObjectRetrievalFactory>(), retrieverManager, this.IoCContainer.Resolve<IEntityPersistenceManager>());
        }

        [Test]
        public void GetADSDOnTheFly()
        {
            var storeId = "sqlserver";
            var dataStructureOnTheFlyEngine = GetEngine(storeId);
            var dsdOnTheFly = dataStructureOnTheFlyEngine.GetByAnnotation(storeId, new StructureReferenceImpl()
            {
                AgencyId = "ESTAT",
                MaintainableId = "TEST",
                Version = "2.0"
            });

            Assert.IsNotNull(dsdOnTheFly);
        }


        [Test]
        public void DeleteADSDOnTheFly()
        {
            var storeId = "sqlserver";
            var dataStructureOnTheFlyEngine = GetEngine(storeId);
            dataStructureOnTheFlyEngine.Delete(storeId, new StructureReferenceImpl()
            {
                AgencyId = "ESTAT",
                MaintainableId = "TEST",
                Version = "2.0"
            });

        }
    }
}
