// -----------------------------------------------------------------------
// <copyright file="TranscodingRulePersistTest.cs" company="EUROSTAT">
//   Date Created : 2017-04-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using NSubstitute;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;

    /// <summary>
    /// The transcoding rule persist test.
    /// </summary>
    [TestFixture("msdb_scratch.oracle")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.sqlserver")]
    public class TranscodingRulePersistTest : BaseTestClassWithContainer
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(TranscodingRulePersistTest));

        /// <summary>
        /// The connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The retriever manager
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;

        /// <summary>
        /// The persist manager
        /// </summary>
        private readonly IEntityPersistenceManager _persistManager;

        /// <summary>
        /// The retrieval manager
        /// </summary>
        private readonly IRetrievalEngineContainer _retrievalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="TranscodingRulePersistTest"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public TranscodingRulePersistTest(string connectionName) : base(connectionName)
        {
          
            this._retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            this._persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
              _connectionStringSettings = GetConnectionStringSettings();
            _retrievalManager = new RetrievalEngineContainer(new Database(_connectionStringSettings));
             ReBuildMappingStore();
        }

        private void ReBuildMappingStore()
        {
            try
            {
                InitializeMappingStore(this.StoreId);
                var dataflow = BuildStructuralMetadata();
                var connection = AddADdbConnection();
                var dataset = AddDataSet(connection, dataflow.DataStructureRef, 0);
                AddSourceMappingSet(dataflow, dataset.EntityId);
            }
            catch (Exception e)
            {
                _log.Error(e);
                Console.WriteLine(e);
                throw;
            }
        }

        [Test]
        public void ShouldDeleteFromParent()
        {
            var engine = _retrieverManager.GetRetrieverEngine<ComponentMappingEntity>(this.StoreId);
            var mappingEntity = engine.GetEntities(new EntityQuery(), Detail.Full).FirstOrDefault(entity => "CODED".Equals(entity.Component.ObjectId));

            Assert.That(mappingEntity, Is.Not.Null);
            TranscodingRuleEntity transcodingRuleEntity = new TranscodingRuleEntity();
            transcodingRuleEntity.DsdCodeEntity  = new IdentifiableEntity() {ObjectId = "C1"};
            transcodingRuleEntity.LocalCodes.Add(new LocalCodeEntity { ObjectId = "CODED_TS1", ParentId = mappingEntity.GetColumns().First().Name });
            transcodingRuleEntity.ParentId = mappingEntity.EntityId;
            transcodingRuleEntity.StoreId = this.StoreId;
            var addedTranscodingEntity = _persistManager.Add(transcodingRuleEntity);
            Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
            var retrievedEntity = _retrieverManager.GetEntities<TranscodingRuleEntity>(StoreId, addedTranscodingEntity.QueryForThisEntityId(), Detail.Full).First();

            Assert.That(retrievedEntity.EntityId, Is.EqualTo(addedTranscodingEntity.EntityId));

            IEntityPersistenceEngine<TranscodingRuleEntity> rulePersist = _persistManager.GetEngine<TranscodingRuleEntity>(this.StoreId);
            rulePersist.DeleteChildren(mappingEntity.EntityId, EntityType.Mapping);
            CollectionAssert.IsEmpty(_retrieverManager.GetEntities<TranscodingRuleEntity>(StoreId, addedTranscodingEntity.QueryForThisEntityId(), Detail.Full));

        }

        [Test]
        public void ShouldAddTranscodingRule()
        {
            var engine = _retrieverManager.GetRetrieverEngine<ComponentMappingEntity>(this.StoreId);
            var mappingEntity = engine.GetEntities(new EntityQuery(), Detail.Full).FirstOrDefault(entity => "CODED".Equals(entity.Component.ObjectId));

            Assert.That(mappingEntity, Is.Not.Null);
            TranscodingRuleEntity transcodingRuleEntity = new TranscodingRuleEntity();
            transcodingRuleEntity.DsdCodeEntity  = new IdentifiableEntity() {ObjectId = "C1"};
            transcodingRuleEntity.LocalCodes.Add(new LocalCodeEntity { ObjectId = "CODED_TS1", ParentId = mappingEntity.GetColumns().First().Name });
            transcodingRuleEntity.ParentId = mappingEntity.EntityId;
            transcodingRuleEntity.StoreId = this.StoreId;
            var addedTranscodingEntity = _persistManager.Add(transcodingRuleEntity);
            Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
            var retrievedEntity = _retrieverManager.GetEntities<TranscodingRuleEntity>(StoreId, addedTranscodingEntity.QueryForThisEntityId(), Detail.Full).First();

            Assert.That(retrievedEntity.EntityId, Is.EqualTo(addedTranscodingEntity.EntityId));
            Assert.That(retrievedEntity.ParentId, Is.EqualTo(addedTranscodingEntity.ParentId));
            Assert.That(retrievedEntity.UncodedValue, Is.EqualTo(addedTranscodingEntity.UncodedValue));
            Assert.That(retrievedEntity.DsdCodeEntity, Is.Not.Null);
            Assert.That(retrievedEntity.DsdCodeEntity.EntityId, Is.EqualTo(retrievedEntity.DsdCodeEntity.EntityId));
            Assert.That(retrievedEntity.LocalCodes, Is.Not.Null.And.Not.Empty);
            Assert.That(retrievedEntity.LocalCodes.Count, Is.EqualTo(transcodingRuleEntity.LocalCodes.Count));
            Assert.That(retrievedEntity.LocalCodes.First().ObjectId, Is.EqualTo(transcodingRuleEntity.LocalCodes.First().ObjectId));
            Assert.That(retrievedEntity.LocalCodes.First().ParentId, Is.EqualTo(transcodingRuleEntity.LocalCodes.First().ParentId));

        }

        [Test]
        public void ShouldNotAddInvalidTranscodingRule()
        {
            var engine = _retrieverManager.GetRetrieverEngine<ComponentMappingEntity>(this.StoreId);
            var mappingEntity = engine.GetEntities(new EntityQuery(), Detail.Full).FirstOrDefault(entity => "CODED".Equals(entity.Component.ObjectId));

            Assert.That(mappingEntity, Is.Not.Null);
            TranscodingRuleEntity transcodingRuleEntity = new TranscodingRuleEntity();
            transcodingRuleEntity.DsdCodeEntity  = new IdentifiableEntity() { ObjectId = null};
            transcodingRuleEntity.LocalCodes.Add(new LocalCodeEntity { ObjectId = "CODED_TS1", ParentId = mappingEntity.GetColumns().First().Name });
            transcodingRuleEntity.ParentId = mappingEntity.EntityId;
            transcodingRuleEntity.StoreId = this.StoreId;
            var addedTranscodingEntity = _persistManager.Add(transcodingRuleEntity);
            Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
            CollectionAssert.IsEmpty(_retrieverManager.GetEntities<TranscodingRuleEntity>(StoreId, addedTranscodingEntity.QueryForThisEntityId(), Detail.Full));
        }

        [Test]
        public void ShouldAddTranscodingRuleWithUncodedValue()
        {
            var engine = _retrieverManager.GetRetrieverEngine<ComponentMappingEntity>(this.StoreId);
            var mappingEntity = engine.GetEntities(new EntityQuery(), Detail.Full).FirstOrDefault(entity => "UNCODED".Equals(entity.Component.ObjectId));

            Assert.That(mappingEntity, Is.Not.Null);
            TranscodingRuleEntity transcodingRuleEntity = new TranscodingRuleEntity();
            transcodingRuleEntity.LocalCodes.Add(new LocalCodeEntity { ObjectId = "UNCODED_TST1", ParentId = mappingEntity.GetColumns().First().Name });
            transcodingRuleEntity.ParentId = mappingEntity.EntityId;
            transcodingRuleEntity.StoreId = this.StoreId;
            transcodingRuleEntity.DsdCodeEntity = new IdentifiableEntity { ObjectId = "Test Uncoded value" };
            var addedTranscodingEntity = _persistManager.Add(transcodingRuleEntity);
            Assert.That(addedTranscodingEntity.EntityId, Is.Not.Null.And.Not.Empty);
            var retrievedEntity = _retrieverManager.GetEntities<TranscodingRuleEntity>(StoreId, addedTranscodingEntity.QueryForThisEntityId(), Detail.Full).First();

            Assert.That(retrievedEntity.EntityId, Is.EqualTo(addedTranscodingEntity.EntityId));
            Assert.That(retrievedEntity.ParentId, Is.EqualTo(addedTranscodingEntity.ParentId));
            Assert.That(retrievedEntity.DsdCodeEntity.ObjectId, Is.EqualTo(addedTranscodingEntity.DsdCodeEntity.ObjectId));
            Assert.That(retrievedEntity.LocalCodes, Is.Not.Null.And.Not.Empty);
            Assert.That(retrievedEntity.LocalCodes.Count, Is.EqualTo(transcodingRuleEntity.LocalCodes.Count));
            Assert.That(retrievedEntity.LocalCodes.First().ObjectId, Is.EqualTo(transcodingRuleEntity.LocalCodes.First().ObjectId));
            Assert.That(retrievedEntity.LocalCodes.First().ParentId, Is.EqualTo(transcodingRuleEntity.LocalCodes.First().ParentId));

        }

        private IDataflowObject BuildStructuralMetadata()
        {
            IMutableObjects mutableObjects = new MutableObjectsImpl();
            IConceptSchemeMutableObject conceptScheme = new ConceptSchemeMutableCore();
            SetupMaintainable(conceptScheme, "TEST_CS");

            AddConcept(conceptScheme, "CODED");
            AddConcept(conceptScheme, "UNCODED");
            AddConcept(conceptScheme, PrimaryMeasure.FixedId);

            mutableObjects.AddConceptScheme(conceptScheme);

            ICodelistMutableObject cl = new CodelistMutableCore();
            SetupMaintainable(cl, "TEST_CL");

            var c1 = new CodeMutableCore() { Id = "C1" };
            c1.AddName("en", "c1 code");
            cl.AddItem(c1);
            var c2 = new CodeMutableCore() { Id = "C2" };
            c2.AddName("en", "c2 code");
            cl.AddItem(c2);

            mutableObjects.AddCodelist(cl);

            IDataStructureMutableObject dsd = new DataStructureMutableCore();
            SetupMaintainable(dsd, "TEST_DSD");
            dsd.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "CODED"),
                new StructureReferenceImpl("TEST", "TEST_CL", "1.0", SdmxStructureEnumType.CodeList));
            dsd.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "UNCODED"),
                null);
            dsd.AddPrimaryMeasure(
                new StructureReferenceImpl(
                    "TEST",
                    "TEST_CS",
                    "1.0",
                    SdmxStructureEnumType.Concept,
                    PrimaryMeasure.FixedId));
            mutableObjects.AddDataStructure(dsd);

            var dataflow = new DataflowMutableCore(dsd.ImmutableInstance);
            mutableObjects.AddDataflow(dataflow);
            var artefactImportStatuses = new List<ArtefactImportStatus>();
            var structurePersistenceManager =
                new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(
                    _connectionStringSettings,
                    artefactImportStatuses);

            structurePersistenceManager.SaveStructures(mutableObjects.ImmutableObjects);
            Assert.That(
                artefactImportStatuses.All(status => status.ImportMessage.Status != ImportMessageStatus.Error),
                string.Join("\n", artefactImportStatuses.Select(status => status.ImportMessage.Message)));

            return dataflow.ImmutableInstance;
        }

        private void SetupMaintainable(IMaintainableMutableObject maintainable, string id)
        {
            maintainable.Version = "1.0";
            maintainable.AgencyId = "TEST";
            maintainable.Id = id;
            maintainable.AddName("en", $"Test {id}");
            maintainable.FinalStructure = TertiaryBool.ParseBoolean(true);
        }

        private void AddConcept(IConceptSchemeMutableObject cs, string id)
        {
            var c = new ConceptMutableCore() {Id = id};
            c.AddName("en", $"Name of $id");
            cs.AddItem(c);
        }

        private MappingSetEntity AddSourceMappingSet(IDataflowObject dataflowObject, string datasetEntityId)
        {
            MappingSetEntity mappingSetEntity = new MappingSetEntity();
            mappingSetEntity.StoreId = StoreId;
            mappingSetEntity.DataSetId = datasetEntityId;
            mappingSetEntity.Name = "Test Mapping Set";
            mappingSetEntity.ParentId = dataflowObject.Urn.ToString();
            mappingSetEntity.Description = "a description";

            var addedMappingSet = _persistManager.Add(mappingSetEntity);

            var datasetColumns = _retrieverManager.GetEntities<DataSetColumnEntity>(
                StoreId,
                datasetEntityId.QueryForThisParentId(),
                Detail.Full).ToArray();

            ICommonStructureQuery commonStructureQuery = CommonStructureQueryCore
                .Builder
                .NewQuery(CommonStructureQueryType.Other, null)
                .SetStructureIdentification(dataflowObject.DataStructureRef)
                .SetReferences(StructureReferenceDetailEnumType.Children)
                .Build();

            IEnumerable<IDataStructureMutableObject> dsds = _retrievalManager.DSDRetrievalEngine.Retrieve(commonStructureQuery);
            IMutableObjects mutableObjects = new MutableObjectsImpl();
            
            mutableObjects.DataStructures.AddAll(dsds);

            mutableObjects.Codelists.AddAll(_retrievalManager.CodeListRetrievalEngine.RetrieveAsChildren(commonStructureQuery));

            ISdmxObjects dsdAndDepds = mutableObjects.ImmutableObjects;

            var dsd = dsdAndDepds.DataStructures.First();

            foreach (var component in dsd.Components)
            {
                ComponentMappingEntity componentMapping = new ComponentMappingEntity();
                componentMapping.StoreId = StoreId;
                componentMapping.ParentId = addedMappingSet.EntityId;
                componentMapping.Type = ComponentMappingType.Normal.AsMappingStoreType();

                ICodelistObject codelist = null;
                if (component.HasCodedRepresentation())
                {
                    codelist = dsdAndDepds.GetCodelists(component.Representation.Representation).First();
                }

                var dataAttribute = component as IAttributeObject;
                var dimension = component as IDimension;
                var dataSetColumnEntities =
                    datasetColumns.Where(entity => entity.Name.Contains("_" + component.Id)).ToArray();
                if (dimension == null || !dimension.TimeDimension)
                {
                    // no transcoding yet
                    dataSetColumnEntities =
                        dataSetColumnEntities.Where(entity => entity.Name.EndsWith(component.Id)).Take(1).ToArray();
                }

                componentMapping.SetColumns(dataSetColumnEntities);

                componentMapping.Component = new Component { ObjectId = component.Id };

                if (dataSetColumnEntities.Length == 0)
                {
                    if (dataAttribute == null || dataAttribute.Mandatory)
                    {
                        componentMapping.ConstantValues.Add( codelist != null ? codelist.Items.First().Id : "Test value");
                    }
                }

                var addedComponentEntity = _persistManager.Add(componentMapping);
            }

            return mappingSetEntity;
        }
    }
}