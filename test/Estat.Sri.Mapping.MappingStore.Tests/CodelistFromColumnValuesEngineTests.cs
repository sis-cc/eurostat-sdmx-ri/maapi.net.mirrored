﻿using Estat.Sri.Mapping.Api.Manager;
using DryIoc;
using NUnit.Framework;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Manager;
using System.Configuration;
using NSubstitute;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    public class CodelistFromColumnValuesEngineTests: BaseTestClassWithContainer
    {
        private IDataSetDataRetrievalManager _dataSetDataRetrievalManager;
        private IEntityRetrieverManager _retrieverManager;
        private ConnectionStringSettings _connectionStringSettings;

        public CodelistFromColumnValuesEngineTests() : base("sqlserver")
        {
            this._dataSetDataRetrievalManager = this.IoCContainer.Resolve<IDataSetDataRetrievalManager>();
            this._retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
        }

        [Test]
        public void CreateACodelistFromDbValues()
        {
            this._connectionStringSettings = ConfigurationManager.ConnectionStrings["sqlserver"];
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });

            var databaseManager = new DatabaseManager(configurationStoreManager);


            var codelistEngine = new CodelistFromColumnValuesEngine(_dataSetDataRetrievalManager, _retrieverManager,configurationStoreManager);
            var codelist = new CodelistMutableCore()
            {
                AgencyId = "TestAgency",
                Id = "TEST",
                Version = "3.0",
            };
            codelist.AddName("en", "Test codelist");
            codelist.AddDescription("en", "This is a test codelist");
            var codelistId = codelistEngine.Create("sqlserver", "6", "ITTER107", codelist);
            Assert.That(codelistId, Is.Not.EqualTo(-1));
        }
    }
}
