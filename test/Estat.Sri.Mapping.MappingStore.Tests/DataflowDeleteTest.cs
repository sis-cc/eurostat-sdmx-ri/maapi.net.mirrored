// -----------------------------------------------------------------------
// <copyright file="DataflowUpgradeTest.cs" company="EUROSTAT">
//   Date Created : 2017-07-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using MappingSetEntity = Api.Model.MappingSetEntity;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using NSubstitute;
    using Estat.Sri.Utils.Config;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;

    [TestFixture("msdb_scratch.sqlserver")]
    [TestFixture("msdb_scratch.oracle")]
    [TestFixture("msdb_scratch.mariadb")]
    public class DataflowDeleteTest : BaseTestClassWithContainer
    {
        private readonly IEntityPersistenceManager _persistenceManager;

        private readonly IEntityRetrieverManager _entityRetriever;


        private readonly IStructureParsingManager _structureParsingManager;

        private readonly IStructureWriterManager _structureWritingManager;

        private readonly ConnectionStringSettings _connectionStringSettings;

        private readonly ISdmxObjectRetrievalManager _retrievalManager;

        private IDataflowUpgradeManager _dataflowUpgrade;

        private readonly RetrievalEngineContainer _container;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataflowUpgradeTest"/> class.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        public DataflowDeleteTest(string storeId) : base(storeId)
        {
            try
            {
                _persistenceManager = IoCContainer.Resolve<IEntityPersistenceManager>();
                _entityRetriever = IoCContainer.Resolve<IEntityRetrieverManager>();
                _structureParsingManager = IoCContainer.Resolve<IStructureParsingManager>();
                _structureWritingManager = IoCContainer.Resolve<IStructureWriterManager>();
                _dataflowUpgrade = IoCContainer.Resolve<IDataflowUpgradeManager>();
                _connectionStringSettings = GetConnectionStringSettings();
                _retrievalManager = new MappingStoreSdmxObjectRetrievalManager(_connectionStringSettings);
                _container = new RetrievalEngineContainer(new Database(_connectionStringSettings));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [TestCase("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.0)", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.1)")]
        public void ShouldDeleteMappingSetAndDatasetForDataflow(string sourceUrn, string targetUrn)
        {
            var structureSetsTemp = new DirectoryInfo(Path.Combine(Path.GetTempPath(), "structureSets"));
            InitializeMappingStore(this.StoreId);

            var dataflow = ImportStructuralMetadata(structureSetsTemp, sourceUrn, targetUrn);
            var connection = AddADdbConnection();
            var dataset = AddDataSet(connection, dataflow.DataStructureRef, 1);
            var mappingSet = AddMappingSet(dataflow.Urn.ToString(), dataset.EntityId);


            IList<ArtefactImportStatus> artefactImportStatuses = new List<ArtefactImportStatus>();
            var manager = new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(_connectionStringSettings, artefactImportStatuses);
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            sdmxObjects.AddDataflow(dataflow);
            ConfigurationProvider.AutoDeleteMappingSets = true;
            manager.DeleteStructures(sdmxObjects);

            var structureQueryFull = CreateQuery(dataflow.AsReference, SdmxStructureEnumType.Dataflow, ComplexStructureQueryDetailEnumType.Full);
            var dbDataflow = _container.DataflowRetrievalEngine.Retrieve(structureQueryFull).FirstOrDefault();
            Assert.That(dbDataflow, Is.Null);

            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this.StoreId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new MappingSetFactory(databaseManager),new DatasetRetrieverFactory(databaseManager));

            var datasetEngine = retrieverManager.GetRetrieverEngine<DatasetEntity>(connectionStringSettings.Name);
            IEntityQuery datasetQuery = new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, dataset.EntityId.ToString()) };
            var datasets = datasetEngine.GetEntities(datasetQuery, Detail.Full).ToArray();
            Assert.IsFalse(datasets.Any());

            var mappingSetEngine = retrieverManager.GetRetrieverEngine<MappingSetEntity>(connectionStringSettings.Name);
            IEntityQuery query = new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, mappingSet.EntityId.ToString()) };
            var mappingSetEntities = mappingSetEngine.GetEntities(query, Detail.Full).ToArray();
            Assert.IsFalse(mappingSetEntities.Any());
        }

        private MappingSetEntity AddMappingSet(string sourceUrn, string datasetEntityId)
        {
            MappingSetEntity mappingSetEntity = new MappingSetEntity();
            mappingSetEntity.StoreId = StoreId;
            mappingSetEntity.DataSetId = datasetEntityId;
            mappingSetEntity.Name = "Test Mapping Set";
            mappingSetEntity.ParentId = sourceUrn;
            mappingSetEntity.Description = "a description";

            var addedMappingSet = _persistenceManager.Add(mappingSetEntity);
            return addedMappingSet;
        }

        private ICommonStructureQuery CreateQuery(IStructureReference identification, SdmxStructureEnumType target, ComplexStructureQueryDetailEnumType detail)
        {
            return CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                    .SetMaintainableTarget(SdmxStructureType.GetFromEnum(target))
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(detail))
                    .SetStructureIdentification(identification)
                    .Build();
        }

        private IDataflowObject ImportStructuralMetadata(DirectoryInfo structureSetsTemp, string sourceUrn, string targetUrn)
        {
            var artefactImportStatuses = new List<ArtefactImportStatus>();

            // TODO introduce an adapter and add a CTOR that uses Database 
            var structurePersistenceManager = new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(_connectionStringSettings, artefactImportStatuses);

            var sdmxObjects = GetSdmxObjects(_structureParsingManager);

            SaveStructureSets(sdmxObjects, _structureWritingManager, structureSetsTemp);
            var sourceDataflow = GetDataflow(sdmxObjects, sourceUrn);
            GetDataflow(sdmxObjects, targetUrn);

            structurePersistenceManager.SaveStructures(sdmxObjects);
            Assert.That(artefactImportStatuses.All(status => status.ImportMessage.Status != ImportMessageStatus.Error), string.Join("\n", artefactImportStatuses.Select(status => status.ImportMessage.Message)));
            return sourceDataflow;
        }


        /// <summary>
        /// Gets the dataflow.
        /// </summary>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        /// <param name="urn">The urn.</param>
        private IDataflowObject GetDataflow(ISdmxObjects sdmxObjects, string urn)
        {
            if (UrnUtil.GetIdentifiableType(urn).EnumType == SdmxStructureEnumType.Dataflow)
            {
                return sdmxObjects.GetDataflows(new StructureReferenceImpl(urn)).First();
            }

            if (UrnUtil.GetIdentifiableType(urn).EnumType == SdmxStructureEnumType.Dsd)
            {
                var dsd = sdmxObjects.GetDataStructures(new StructureReferenceImpl(urn)).First();
                var dataflowObjectCore = new DataflowObjectCore(new DataflowMutableCore(dsd));
                sdmxObjects.AddDataflow(dataflowObjectCore);
                return dataflowObjectCore;
            }

            Assert.Fail("input file issue. No Dataflow/Dsd related to '" + urn + "' found");
            return null;
        }

        private static void SaveStructureSets(ISdmxObjects sdmxObjects, IStructureWriterManager structureWritingManager, DirectoryInfo structureSetsTemp)
        {
            var structureSetObjects = sdmxObjects.StructureSets;
            Assert.That(structureSetObjects, Is.Not.Empty);

            if (!structureSetsTemp.Exists)
            {
                structureSetsTemp.Create();
            }

            FileInfo structureSetTempFile = new FileInfo(Path.Combine(structureSetsTemp.FullName, "structureSet.xml"));

            ISdmxObjects structureSetContainer = new SdmxObjectsImpl(new HeaderImpl("TEST", "ZZ9"), structureSetObjects);
            using (var structureSetOutputStream = structureSetTempFile.Create())
            {
                structureWritingManager.WriteStructures(
                    structureSetContainer,
                    new SdmxStructureFormat(
                        StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument)),
                    structureSetOutputStream);
            }

            // delete structure sets from old container
            foreach (var structureSetObject in structureSetObjects)
            {
                sdmxObjects.RemoveStructureSet(structureSetObject);
            }
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <param name="structureParsingManager">The structure parsing manager.</param>
        private static ISdmxObjects GetSdmxObjects(IStructureParsingManager structureParsingManager)
        {
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            var files = Directory.GetFiles("tests", "*.xml").Select(s => new FileInfo(s));
            foreach (var file in files)
            {
                sdmxObjects.Merge(file.GetSdmxObjects(structureParsingManager));
            }
            return sdmxObjects;
        }
    }
}