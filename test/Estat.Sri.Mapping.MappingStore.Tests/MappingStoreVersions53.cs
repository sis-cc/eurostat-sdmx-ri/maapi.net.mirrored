﻿using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Factory;

using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Plugin.Editor.QueryEditor.Factory;
using Estat.Sri.Plugin.Editor.CustomQueryEditor.Factory;

using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Builder;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using Estat.Sri.Mapping.MappingStore.Manager;
using NSubstitute;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Helpers;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("sqlserverMAStore53")]
    class MappingStoreVersions53 : BaseTestClassWithContainer
    {
        private readonly ConnectionStringSettings _connectionStringSettings;

        private IConfigurationStoreManager _configurationStoreManager;

        private IMappingStoreManager _mappingStoreManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTestClassWithContainer"/> class.
        /// </summary>
        public MappingStoreVersions53(string storeId)
            : base(storeId)
        {
            _connectionStringSettings = GetConnectionStringSettings();
            this._configurationStoreManager = this.IoCContainer.Resolve<IConfigurationStoreManager>();
            this._mappingStoreManager = this.IoCContainer.Resolve<IMappingStoreManager>();
        }

        [Test]
        public void TestComponentMappingVersion53()
        {
            Assert.That(MappingStoreVersionHelper.IsVersion53(_mappingStoreManager, StoreId));

            var databaseManager = new DatabaseManager(this._configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new ComponentMappingRetrieverFactory(databaseManager, this._mappingStoreManager));
            var engine = retrieverManager.GetRetrieverEngine<ComponentMappingEntity>(this._connectionStringSettings.Name);
        }

        [Test]
        public void TestTranscodingRuleEngineMappingVersion53()
        {
            Assert.That(MappingStoreVersionHelper.IsVersion53(_mappingStoreManager, StoreId));

            var databaseManager = new DatabaseManager(this._configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new TranscodingRuleRetrieverFactory(databaseManager, this._mappingStoreManager));
            var engine = retrieverManager.GetRetrieverEngine<TranscodingRuleEntity>(this._connectionStringSettings.Name);
        }
    }
}
