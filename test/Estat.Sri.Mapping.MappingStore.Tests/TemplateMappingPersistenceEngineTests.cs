using DryIoc;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Factory;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    class TemplateMappingPersistenceEngineTests: BaseTestClassWithContainer
    {
        /// <summary>
        /// The connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The retriever manager
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;

        /// <summary>
        /// The persist manager
        /// </summary>
        private readonly IEntityPersistenceManager _persistManager;

        private readonly EntityPeristenceFactory _entityPeristenceFactory;

        public TemplateMappingPersistenceEngineTests() : base("sqlserver")
        {

            this._retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            this._persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            this._entityPeristenceFactory = this.IoCContainer.Resolve<EntityPeristenceFactory>();
            _connectionStringSettings = GetConnectionStringSettings();
            //_retrievalManager = new MappingStoreSdmxObjectRetrievalManager(_connectionStringSettings);
            //ReBuildMappingStore();
        }

        [Test]
        public void ShouldAddATemplateMapping()
        {
            //var engine = new TemplateMappingPersistenceEngine();

            var timeTranscodingEntity = new TimeTranscodingEntity()
            {
                Frequency = "Q",
                Period = new PeriodTimeTranscoding()
                {
                    Start = 1,
                    Length = 4
                },
                Year = new TimeTranscoding()
                {
                    Start = 2,
                    Length = 3
                }

            };
            var templateMapping = new TemplateMapping()
            {
                ColumnDescription = "testDesc",
                ColumnName = "REF_AREA",
                ComponentId = "1",
                ComponentType = "Dimension",
                ConceptId = 19496,
                ItemSchemeId = new StructureReferenceImpl("MA", "SDMX_Q_PERIODS", "1.0.0", SdmxStructureEnumType.CodeList),
                Transcodings = new Dictionary<string, string>()
                {
                    { "Quarterly","Q" },
                    { "Testign","T"}
                },
                TimeTranscoding = new List<TimeTranscodingEntity>()
                {
                    timeTranscodingEntity
                },
                Name = "test",
                Description = "test",
            };
            templateMapping.StoreId = "sqlserver";
            this._persistManager.Add(templateMapping);
        }

        [Test]
        public void ShouldUpdateATemplateMapping ()
        {

            var patchRequest = new PatchRequest()
            {
                new PatchDocument("replace","/ColumnName","myName2Update"),
                new PatchDocument("remove","/transcodings/Monthly"),
                new PatchDocument("remove","/transcodings/Annually"),
                new PatchDocument("add","/transcodings","{\"Monthly\":\"M1\",\"Annually\":\"A1\"}"),
                new PatchDocument("replace","/transcodings/Quarterly","Q2"),
                new PatchDocument("replace","/transcodings/Testign","T2"),
            };
            var engine = this._entityPeristenceFactory.GetEngine<TemplateMapping>("sqlserver");
            engine.Update(patchRequest, Api.Constant.EntityType.TemplateMapping, "10837");
        }
    }
}
