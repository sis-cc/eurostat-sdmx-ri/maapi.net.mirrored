﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using System.Configuration;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.MappingStoreRetrieval.Config;
using Dapper;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    class DSDontheflyTests
    {
        public IDataStructureObject BuildDataStructure()
        {
            IDataStructureMutableObject dsd = new DataStructureMutableCore();
            dsd.AgencyId = "SDMXSOURCE";
            dsd.Id = "WDI";
            dsd.AddName("en", "World Development Indicators");

            dsd.AddDimension(CreateConceptReference("COUNTRY"), CreateCodelistReference("CL_COUNTRY"));
            dsd.AddDimension(CreateConceptReference("INDICATOR"), CreateCodelistReference("CL_INDICATOR"));
            IDimensionMutableObject timeDim = dsd.AddDimension(CreateConceptReference("TIME"), null);
            timeDim.TimeDimension = true;
            dsd.AddPrimaryMeasure(CreateConceptReference("OBS_VALUE"));

            return dsd.ImmutableInstance;
        }

        /// <summary>
        /// The create codelist reference.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IStructureReference"/>.
        /// </returns>
        private static IStructureReference CreateCodelistReference(string id)
        {
            return new StructureReferenceImpl("SDMXSOURCE", id, "1.0", SdmxStructureEnumType.CodeList);
        }

        /// <summary>
        /// The create concept reference.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IStructureReference"/>.
        /// </returns>
        private static IStructureReference CreateConceptReference(string id)
        {
            return new StructureReferenceImpl("SDMXSOURCE", "CONCEPTS", "1.0", SdmxStructureEnumType.Concept, id);
        }

    }
}
