using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NSubstitute;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class DataSourceRetrieverTests : BaseTestClassWithContainer
    {
        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        private readonly ConnectionStringSettings _connectionStringSettings;
        private string _connectionName;
        private IEntityPersistenceManager _persistManager;
        private DataSourceEntity _dataSourceEntity;
        private MappingStoreRetrievalManager _mappingStoreRetrivalManager;
        private Database _database;
        private string _parentId;

        public DataSourceRetrieverTests(string name): base (name)
        {
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(name);
            this._connectionName = name;
            this._persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this._connectionName);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
           this._database = databaseManager.GetDatabase(this._connectionName);
            this._parentId = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:157_80(1.0)";

        }

        [SetUp]
        public void InsertADataSource()
        {
            _dataSourceEntity = new DataSourceEntity()
            {
                DataUrl = "http://url_to_data_or_rest_or_soap",
                WSDLUrl = "http://url_to_wsdl_or_null",
                WADLUrl = "http://url_to_wadl_or_null",
                IsWs = true,
                IsRest = false,
                IsSimple = false,
                ParentId = this._parentId,
                StoreId = this._connectionName
            };

            //insert a DataSource
            var entity = _persistManager.Add(_dataSourceEntity);
        }

        [TearDown]
        public void DeleteADataSource()
        {
            _persistManager.Delete<DataSourceEntity>(_connectionName, _dataSourceEntity.EntityId);
        }


        [Test]
        public void GetAllDataSources()
        {
            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DataSourceRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<DataSourceEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.AnyValue, null));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.That(result.Count, Is.AtLeast(1));
        }

        [Test]
        public void GetADataSource()
        {

            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DataSourceRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<DataSourceEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.Exact, _dataSourceEntity.EntityId));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(result.First().DataUrl, Is.EqualTo("http://url_to_data_or_rest_or_soap"));
            Assert.That(result.First().ParentId, Is.EqualTo(_parentId));
        }


        [Test]
        public void GetADataSourceBasedOnParentId()
        {
            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DataSourceRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<DataSourceEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.ParentId.Returns(new Criteria<string>(OperatorType.Exact,_dataSourceEntity.ParentId));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public void GetADataSourceBasedOnParentIdExclusion()
        {
            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DataSourceRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<DataSourceEntity>(this._connectionStringSettings.Name);
            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.ParentId.Returns(new Criteria<string>(OperatorType.Not | OperatorType.Contains, "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:157_80(9.9)"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.That(result.Count, Is.AtLeast(1));
        }
    }
}