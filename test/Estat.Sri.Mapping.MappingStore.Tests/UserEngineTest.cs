using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Config;
using NSubstitute;
using NUnit.Framework;
using DryIoc;
using Estat.Sri.Mapping.MappingStore.Engine;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class UserEngineTest : BaseTestClassWithContainer
    {
        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        private readonly ConnectionStringSettings _connectionStringSettings;

        private readonly IMappingStoreManager _mappingStoreManager;

        private readonly string _storeId;

        public UserEngineTest(string name) : base(name)
        {
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(name);
            this._mappingStoreManager = this.IoCContainer.Resolve<IMappingStoreManager>();
            this._storeId = StoreId;
        }

        [Test]
        public void GetAllUsers()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            var databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new UserRetrieverFactory(databaseManager));
            var engine = retrieverManager.GetRetrieverEngine<UserEntity>(this._connectionStringSettings.Name);
            var query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.AnyValue, null));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.That(result, Is.Not.Empty);
        }
        

        [Test]
        public void ShouldInsertUser()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this._storeId);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
               .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<UserEntity>(this._storeId);


            UserEntity userEntity = new UserEntity();
            userEntity.UserName = "user2";
            userEntity.EditedBy = "admin";
            userEntity.EditDate = System.DateTime.Today;
            userEntity.Dataspace = "testDataspace";
            userEntity.IsGroup = true;
            userEntity.AddPermission("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30.31", "read_write");
            userEntity.AddPermission("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).6.157", "read_write");
            userEntity.AddPermission("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30", "read_write");
            userEntity.AddPermission("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:HC06(1.0)", "read_write");
            userEntity.AddPermission("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30.264D", "read_write");

            var existingUserQuery = new EntityQuery();
            existingUserQuery.AddAdditionalCriteria(nameof(UserEntity.UserName), new Criteria<string>(Api.Constant.OperatorType.Exact, userEntity.UserName));

            entityPersistenceEngine.Add(userEntity);
        }
    }
}