using System;
using System.Collections.Generic;
using System.Text;
using DryIoc;
using Estat.Sri.Mapping.Api.Manager;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{

    public class ConnectionSettingsBuilderTests : BaseTestClassWithContainer
    {
        private readonly IDatabaseProviderManager _dbProviderManager;
        public ConnectionSettingsBuilderTests() :base("SqlServer")
        {
            _dbProviderManager = IoCContainer.Resolve<IDatabaseProviderManager>();
        }

        [TestCase("oracle_connectiondesc", "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=MyHost)(PORT=MyPort))(CONNECT_DATA=(SERVICE_NAME=MyOracleSID)));User ID=MASTORE;Password=123", "Oracle.ManagedDataAccess.Client", "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=MyHost)(PORT=MyPort))(CONNECT_DATA=(SERVICE_NAME=MyOracleSID)))")]
        [TestCase("sqlserver", "Data Source=sodi-test;Initial Catalog=madb-latest;User Id=mauser;Password=123", "System.Data.SqlClient", "jdbc:sqlserver://sodi-test;databaseName=madb-latest")]
        [TestCase("sqlserverwithport", "Data Source=sodi-test,1433;Initial Catalog=madb-latest;User Id=mauser;Password=123", "System.Data.SqlClient", "jdbc:sqlserver://sodi-test:1433;databaseName=madb-latest")]
        [TestCase("mariadbwithport", "server=localhost;user id=mauser;password=123;database=mastore;default command timeout=120", "MySql.Data.MySqlClient", "jdbc:mysql://localhost:3306/mastore")]
        [TestCase("mariadb", "server=localhost:3307;user id=mauser;password=123;database=mastore;default command timeout=120", "MySql.Data.MySqlClient", "jdbc:mysql://localhost:3307/mastore")]
        [TestCase("oracle", "Data Source=test-ddb-oracle:1521/xe;user id=mauser;password=123;database=MASTORE;default command timeout=120", "Oracle.ManagedDataAccess.Client", "jdbc:oracle:thin:@test-ddb-oracle:1521/xe")]
        public void TestCreateJdbcString(string name, string connectionString, string providerName,string expected)
        {
            var ddbConnectionString = new System.Configuration.ConnectionStringSettings(name, connectionString, providerName);
            var dbPluginEngine = _dbProviderManager.GetEngineByProvider(ddbConnectionString.ProviderName);
            var ddbSettings = dbPluginEngine.SettingsBuilder.CreateConnectionEntity(ddbConnectionString);
            var connection = dbPluginEngine.SettingsBuilder.CreateDdbConnectionEntity(ddbSettings);
            Assert.That(connection.JdbcConnString, Is.EqualTo(expected));
        }
  }
}
