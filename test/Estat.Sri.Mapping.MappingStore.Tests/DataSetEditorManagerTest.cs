using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Factory;

using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Plugin.Editor.QueryEditor.Factory;
using Estat.Sri.Plugin.Editor.CustomQueryEditor.Factory;

using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Builder;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.Plugin.Editor.QueryEditor;
using Newtonsoft.Json.Linq;
using NSubstitute;
using Estat.Sri.Mapping.Api.Engine.Streaming;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    class DataSetEditorManagerTest : BaseTestClassWithContainer
    {

        [Test]
        public void GenerateSqlQueryTest()
        {
            IEntityRetrieverManager _retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            IDatabaseProviderManager _databaseProviderManager = this.IoCContainer.Resolve<IDatabaseProviderManager>();
            IBuilder<Database, DdbConnectionEntity> _databaseBuilder = this.IoCContainer.Resolve<IBuilder<Database, DdbConnectionEntity>>();

            var factory = new QueryEditorFactory(_databaseBuilder, _retrieverManager);

            var engine = factory.GetEngine("Estat.Ma.Ui.QueryEditors.MdiQueryEditorUC, Estat.Ma.Ui.QueryEditors");         

            var entityRetrieverEngine = _retrieverManager.GetRetrieverEngine<DatasetEntity>(this.StoreId);
            var datasetEntity =
                entityRetrieverEngine.GetEntities(
                    new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, "20") },
                    Detail.Full).FirstOrDefault();

            datasetEntity.StoreId = this.StoreId;

            var generatedSqlQuery = engine.GenerateSqlQuery(datasetEntity);

            Assert.AreEqual(generatedSqlQuery, datasetEntity.Query);
        }

        [Test]
        public void GenerateSqlQueryWithManagerTest()
        {
            IEntityRetrieverManager _retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            IDatabaseProviderManager _databaseProviderManager = this.IoCContainer.Resolve<IDatabaseProviderManager>();
            IBuilder<Database, DdbConnectionEntity> _databaseBuilder = this.IoCContainer.Resolve<IBuilder<Database, DdbConnectionEntity>>();

            IDataSetEditorFactory[] factories = new IDataSetEditorFactory[2];
            factories[0] = new QueryEditorFactory(_databaseBuilder, _retrieverManager);
            factories[1] = new CustomQueryEditorFactory();

            DataSetEditorManager dataSetEditorManager = new DataSetEditorManager(factories);

            var entityRetrieverEngine = _retrieverManager.GetRetrieverEngine<DatasetEntity>(this.StoreId);
            var datasetEntity =
                entityRetrieverEngine.GetEntities(
                    new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, "22") },
                    Detail.Full).FirstOrDefault();

            datasetEntity.StoreId = this.StoreId;
            datasetEntity.EditorType = "Estat.Ma.Ui.QueryEditors.MdiQueryEditorUC, Estat.Ma.Ui.QueryEditors";

            var generatedSqlQuery = dataSetEditorManager.GenerateSqlQuery(datasetEntity);

            Assert.That(generatedSqlQuery == datasetEntity.Query);
        }
        [Test]
        public void GetColumns()
        {
            IEntityRetrieverManager _retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            FileInfo output = new FileInfo("test-write-columns.json");
            using(var stream  = output.Create())
            using (var writer = new EntityStreamingWriter(stream))
            {
                _retrieverManager.WriteEntities(this.StoreId, writer, EntityType.DataSetColumn, EntityQuery.Empty, Detail.Full);

            }
        }

        [Test]
        public void GetAvailablePlugins()
        {
            IEntityRetrieverManager _retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            IDatabaseProviderManager _databaseProviderManager = this.IoCContainer.Resolve<IDatabaseProviderManager>();
            IBuilder<Database, DdbConnectionEntity> _databaseBuilder = this.IoCContainer.Resolve<IBuilder<Database, DdbConnectionEntity>>();

            List<string> avalableDBs = new List<string>()
            {
                "PCAxis",
                "SqlServer",
                "MySQL",
                "Oracle",
                "Odbc"
            };
            
            IDataSetEditorFactory[] factories = new IDataSetEditorFactory[2];
            factories[0] = new QueryEditorFactory(_databaseBuilder, _retrieverManager);
            factories[1] = new CustomQueryEditorFactory();


            DataSetEditorManager dataSetEditorManager = new DataSetEditorManager(factories);

            // for CustomQueryEditorFactory
            var queryEditorPlugins = dataSetEditorManager.GetSupportedDdbPlugins("Estat.Ma.Ui.QueryEditors.Custom.CustomQueryEditor, Estat.Ma.Ui.QueryEditors.Custom");

            foreach(var db in queryEditorPlugins)
            {
                Assert.That(avalableDBs.Contains(db));
            }


            // for QueryEditorFactory
            queryEditorPlugins = dataSetEditorManager.GetSupportedDdbPlugins("Estat.Ma.Ui.QueryEditors.MdiQueryEditorUC, Estat.Ma.Ui.QueryEditors");

            foreach (var db in queryEditorPlugins)
            {
                Assert.That(avalableDBs.Contains(db));
            }
        }

        [Test]
        public void GetEditorType()
        {
            IEntityRetrieverManager _retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            IBuilder<Database, DdbConnectionEntity> _databaseBuilder = this.IoCContainer.Resolve<IBuilder<Database, DdbConnectionEntity>>();

            QueryEditorFactory queryEditorFactory = new QueryEditorFactory(_databaseBuilder, _retrieverManager);
            CustomQueryEditorFactory customQueryEditorFactory = new CustomQueryEditorFactory();

            Assert.That(queryEditorFactory.EditorType,Is.EqualTo("Estat.Ma.Ui.QueryEditors.MdiQueryEditorUC, Estat.Ma.Ui.QueryEditors"));
            Assert.That(queryEditorFactory.Name, Is.EqualTo("Query Editor"));



            Assert.That(customQueryEditorFactory.EditorType, Is.EqualTo("Estat.Ma.Ui.QueryEditors.Custom.CustomQueryEditor, Estat.Ma.Ui.QueryEditors.Custom"));
            Assert.That(customQueryEditorFactory.Name, Is.EqualTo("Custom Query"));
        }


        [TestCase("MultipleTables.txt")]
        [TestCase("CrossJoin.txt")]
        [TestCase("Union.txt")]
        [TestCase("ColumnMerging.txt")]
        [TestCase("Constraints.txt")]
        [TestCase("RelationsAndTableConstraints.txt")]
        public void TestReadWriteXMLQuery(string fileName)
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings("sqlserver");
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>("sqlserver");


            var jsonValue = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "tests\\" + fileName));
            var patch = new PatchRequest
            {
                new PatchDocument("replace", "JSONQuery", jsonValue),
            };

            entityPersistenceEngine.Update(patch,EntityType.DataSet,"20");

            IEntityRetrieverManager _retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();

            var entityRetrieverEngine = _retrieverManager.GetRetrieverEngine<DatasetEntity>(this.StoreId);
            var datasetEntity =
                entityRetrieverEngine.GetEntities(
                    new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, "20") },
                    Detail.Full).FirstOrDefault();

            datasetEntity.StoreId = this.StoreId;

            QueryMetadataReaderWriterBase queryReaderWriter = new QueryMetadataReaderWriterBase();
            var queryMetadata = queryReaderWriter.Read(datasetEntity.JSONQuery);
            var generatedJson = queryReaderWriter.Write(queryMetadata);

            Assert.AreEqual(generatedJson, datasetEntity.JSONQuery);

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTestClassWithContainer"/> class.
        /// </summary>
        public DataSetEditorManagerTest()
            : base("sqlserver")
        {
        }
    }
}
