using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DryIoc;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("sqlserver")] // not used
    internal class TestThatConnectionEntityContainsAllInfo : BaseTestClassWithContainer
    {
        public TestThatConnectionEntityContainsAllInfo(string storeId) : base(storeId)
        {
        }

        [TestCase("oracle_easy_connect", "Data Source=localhost/xe;User ID=MASTORE;Password=123", "Oracle.ManagedDataAccess.Client", "MASTORE")]
        [TestCase("oracle_tnalias", "Data Source=myserver;User ID=MASTORE;Password=123", "Oracle.ManagedDataAccess.Client", "MASTORE" )]
        [TestCase("oracle_connectiondesc", "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=MyHost)(PORT=MyPort))(CONNECT_DATA=(SERVICE_NAME=MyOracleSID)));User ID=MASTORE;Password=123", "Oracle.ManagedDataAccess.Client", "MASTORE" )]
        [TestCase("sqlserver", "Data Source=sodi-test,1433;Initial Catalog=madb-latest;User Id=mauser;Password=123", "System.Data.SqlClient", "madb-latest" )]
        [TestCase("mariadbwithport", "server=localhost:3307;user id=mauser;password=123;database=mastore;default command timeout=120", "MySql.Data.MySqlClient", "mastore")]
        [TestCase("mariadb", "server=localhost;user id=mauser;password=123;database=MASTORE;default command timeout=120", "MySql.Data.MySqlClient", "MASTORE" )]
        public void EnsureThatDbNameIsSetCorrectly(string name, string connectionString, string providerName, string expectedDbName)
        {

            // First we get the DB PRovider specific manager
            IDatabaseProviderManager dbProviderManager = this.IoCContainer.Resolve<IDatabaseProviderManager>();

            // The input connection setting, a Connection String pointing to a Dissemination database (or Mapping Store)
            var ddbConnectionString = new System.Configuration.ConnectionStringSettings(name, connectionString, providerName);

            // Below we start interacting with the Mapping Assistant API
            // The goals are :
            // * Add the ddbConnectionString to the Mapping Store Database
            // * Retrieve it

            // The Connection settings are special, in the sense that we need the corresponding Database Provide plugin assistance to 
            // convert them to various forms and to be able to store them
            // First we need to get the Database Provider specific engine using the dbProviderManager and the ddbConnectionString ProviderName
            IDatabaseProviderEngine dbPluginEngine = dbProviderManager.GetEngineByProvider(ddbConnectionString.ProviderName);

            // Then we use the engine from above to convert the input ddbConnectionString to IConnectionEntity
            IConnectionEntity ddbSettings = dbPluginEngine.SettingsBuilder.CreateConnectionEntity(ddbConnectionString);

            Assert.That(ddbSettings, Is.Not.Null);
            Assert.That(ddbSettings.DbName, Is.EqualTo(expectedDbName));
        }
        [TestCase("sqlserver", "Data Source=sodi-test,1433;Initial Catalog=madb-latest;User Id=mauser;Password=123", "System.Data.SqlClient", "sodi-test")]
        [TestCase("mariadbwithport", "server=localhost:3307;user id=mauser;password=123;database=mastore;default command timeout=120", "MySql.Data.MySqlClient", "localhost")]
        [TestCase("mariadb", "server=localhost;user id=mauser;password=123;database=MASTORE;default command timeout=120", "MySql.Data.MySqlClient", "localhost")]
        public void EnsureThatServerIsSetCorrectly(string name, string connectionString, string providerName, string expectedServerName
            )
        {

            // First we get the DB PRovider specific manager
            IDatabaseProviderManager dbProviderManager = this.IoCContainer.Resolve<IDatabaseProviderManager>();

            // The input connection setting, a Connection String pointing to a Dissemination database (or Mapping Store)
            var ddbConnectionString = new System.Configuration.ConnectionStringSettings(name, connectionString, providerName);

            // Below we start interacting with the Mapping Assistant API
            // The goals are :
            // * Add the ddbConnectionString to the Mapping Store Database
            // * Retrieve it

            // The Connection settings are special, in the sense that we need the corresponding Database Provide plugin assistance to 
            // convert them to various forms and to be able to store them
            // First we need to get the Database Provider specific engine using the dbProviderManager and the ddbConnectionString ProviderName
            IDatabaseProviderEngine dbPluginEngine = dbProviderManager.GetEngineByProvider(ddbConnectionString.ProviderName);

            // Then we use the engine from above to convert the input ddbConnectionString to IConnectionEntity
            IConnectionEntity ddbSettings = dbPluginEngine.SettingsBuilder.CreateConnectionEntity(ddbConnectionString);

            Assert.That(ddbSettings, Is.Not.Null);
            Assert.That(ddbSettings.Settings.ContainsKey("server"), Is.True);
            Assert.That(ddbSettings.Settings["server"].Value, Is.EqualTo(expectedServerName));
        }
    }
}
