// -----------------------------------------------------------------------
// <copyright file="MockEntityAuthorizationManager.cs" company="EUROSTAT">
//   Date Created : 2022-11-11
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections.Generic;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// Mock authorisation to use in tests where user authorization is not of interest.
    /// </summary>
    internal class MockEntityAuthorizationManager : IEntityAuthorizationManager
    {
        public bool CanAccess(string mappingStoreId, string entityId, EntityType entityType, AccessType accessType)
        {
            return true;
        }

        public bool CanAccess(IEntity entity, AccessType accessType)
        {
            return true;
        }

        public IEnumerable<TPermissionEntity> CanAccess<TPermissionEntity>(IEnumerable<TPermissionEntity> entities, AccessType accessType) where TPermissionEntity : IEntity
        {
            throw new NotImplementedException();
        }
    }
}
