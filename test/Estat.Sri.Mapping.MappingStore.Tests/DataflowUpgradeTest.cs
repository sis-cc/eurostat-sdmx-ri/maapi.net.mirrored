// -----------------------------------------------------------------------
// <copyright file="DataflowUpgradeTest.cs" company="EUROSTAT">
//   Date Created : 2017-07-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture("msdb_scratch.sqlserver")]
    [TestFixture("msdb_scratch.oracle")]
    [TestFixture("msdb_scratch.mariadb")]
    public class DataflowUpgradeTest : BaseTestClassWithContainer
    {
        private readonly IEntityPersistenceManager _persistenceManager;

        private readonly IEntityRetrieverManager _entityRetriever;

       
        private readonly IStructureParsingManager _structureParsingManager;

        private readonly IStructureWriterManager _structureWritingManager;

        private readonly ConnectionStringSettings _connectionStringSettings;

        private readonly ICommonSdmxObjectRetrievalManager _retrievalManager;

        private IDataflowUpgradeManager _dataflowUpgrade;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataflowUpgradeTest"/> class.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        public DataflowUpgradeTest(string storeId) : base(storeId)
        {
            try
            {
                _persistenceManager = IoCContainer.Resolve<IEntityPersistenceManager>();
                _entityRetriever = IoCContainer.Resolve<IEntityRetrieverManager>();
                _structureParsingManager = IoCContainer.Resolve<IStructureParsingManager>();
                _structureWritingManager = IoCContainer.Resolve<IStructureWriterManager>();
                _dataflowUpgrade = IoCContainer.Resolve<IDataflowUpgradeManager>();
                _connectionStringSettings = GetConnectionStringSettings();
                var  commonSdmxObjectRetrievalFactory = this.IoCContainer.Resolve<ICommonSdmxObjectRetrievalFactory>();
                _retrievalManager = commonSdmxObjectRetrievalFactory.GetManager(_connectionStringSettings);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [TestCase("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.0)", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.1)")]
        public void UpgradeDataflow(string sourceUrn, string targetUrn)
        {
            var structureSetsTemp = new DirectoryInfo(Path.Combine(Path.GetTempPath(), "structureSets"));
            var sourceMappingSet = BuildMappingStore(sourceUrn, targetUrn, structureSetsTemp);
            var structureSets = structureSetsTemp.GetFiles("*.xml");
            var newMappingSet = _dataflowUpgrade.Upgrade(StoreId, structureSets, new Uri(sourceUrn), new Uri(targetUrn));
            Assert.That(newMappingSet, Is.Not.Null);
            var targetDataflow = newMappingSet.ParentId;
            var resultUrn = new StructureReferenceImpl(targetDataflow);
            Assert.That(resultUrn.TargetReference.EnumType, Is.EqualTo(SdmxStructureEnumType.Dataflow));

            var newMappings = this._entityRetriever.GetEntities<ComponentMappingEntity>(StoreId, newMappingSet.QueryForThisParentId(), Detail.Full).ToArray();
            var oldMappings = this._entityRetriever.GetEntities<ComponentMappingEntity>(StoreId, sourceMappingSet.QueryForThisParentId(), Detail.Full).ToArray();

            Assert.That(newMappings, Is.Not.Empty);
            
            // Note this is not always the case
            Assert.That(newMappings.Length, Is.GreaterThan(1));
            foreach (var mapping in newMappings)
            {
                if (ComponentMappingType.Normal.AsMappingStoreType().Equals(mapping.Type))
                {
                    Assert.That(mapping.Component, Is.Not.Null);
                    Assert.That(mapping.Component.ObjectId, Is.Not.Null.And.Not.Empty);
                    //Assert.That(mapping.Component.GetEntityId(), Is.GreaterThan(0));
                }

                if (!string.IsNullOrWhiteSpace(mapping.ConstantValue))
                {
                    Assert.That(mapping.GetColumns(), Is.Null.Or.Empty);
                }
                else
                {
                    Assert.That(mapping.GetColumns(), Is.Not.Null.And.Not.Empty);
                    if (mapping.GetColumns().Count > 1)
                    {
                        Assert.That(mapping.GetColumns().All(entity => entity.GetParentId() > 0));
                        Assert.That(mapping.GetColumns().All(entity => !string.IsNullOrWhiteSpace(entity.Name)));
                        if (!DimensionObject.TimeDimensionFixedId.Equals(mapping.Component.ObjectId))
                        {
                            var rules =
                                this._entityRetriever.GetEntities<TranscodingRuleEntity>(
                                    StoreId,
                                    mapping.TranscodingId.QueryForThisParentId(),
                                    Detail.Full).ToArray();
                            Assert.That(rules, Is.Not.Null.And.Not.Empty, mapping.Component.ObjectId);
                            foreach (var rule in rules)
                            {
                                Assert.That(rule.LocalCodes.Count, Is.EqualTo(mapping.GetColumns().Count));
                                Assert.That(rule.DsdCodeEntity, Is.Not.Null);
                            }
                        }
                    }

                    if (DimensionObject.TimeDimensionFixedId.Equals(mapping.Component.ObjectId))
                    {
                        if (mapping.HasTranscoding())
                        {
                            var transcoding = this._entityRetriever.GetEntities<TranscodingEntity>(StoreId, mapping.QueryForThisParentId(), Detail.Full).First();
                            Assert.That(transcoding.TimeTranscoding, Is.Not.Null.And.Not.Empty);
                        }
                    }
                }
            }
        }

        private MappingSetEntity BuildMappingStore(string sourceUrn, string targetUrn, DirectoryInfo structureSetsTemp)
        {
            InitializeMappingStore(this.StoreId);

            var dataflow = ImportStructuralMetadata(structureSetsTemp, sourceUrn, targetUrn);
            var connection = AddADdbConnection();
            var dataset = AddDataSet(connection, dataflow.DataStructureRef, 1);
            return AddSourceMappingSet(dataflow.Urn.ToString(), dataset.EntityId);

            // Registry query for dataflow agency/id ?reference=structureset
        }

        private MappingSetEntity AddSourceMappingSet(string sourceUrn, string datasetEntityId)
        {
            MappingSetEntity mappingSetEntity = new MappingSetEntity();
            mappingSetEntity.StoreId = StoreId;
            mappingSetEntity.DataSetId = datasetEntityId;
            mappingSetEntity.Name = "Test Mapping Set";
            mappingSetEntity.ParentId = sourceUrn;
            mappingSetEntity.Description = "a description";

            var addedMappingSet = _persistenceManager.Add(mappingSetEntity);

            var datasetColumns = _entityRetriever.GetEntities<DataSetColumnEntity>(
                StoreId,
                datasetEntityId.QueryForThisParentId(),
                Detail.Full).ToArray();
            ICommonStructureQuery commonQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, null)
                .SetStructureIdentification(new StructureReferenceImpl(sourceUrn))
                .SetMaintainableTarget(SdmxStructureEnumType.Dataflow)
                .Build();
            
            var dataflowObject = _retrievalManager.GetMaintainables(commonQuery).Dataflows.FirstOrDefault();
            Assert.That(dataflowObject, Is.Not.Null, sourceUrn);
            commonQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, null)
                .SetStructureIdentification(dataflowObject.DataStructureRef)
                .SetMaintainableTarget(SdmxStructureEnumType.Dsd)
                .SetReferences(StructureReferenceDetailEnumType.Children)
                .Build();
            var dsdAndDepds = _retrievalManager.GetMaintainables(
                commonQuery);

            var dsd = dsdAndDepds.DataStructures.First();

            foreach (var component in dsd.Components)
            {
                ComponentMappingEntity componentMapping = new ComponentMappingEntity();
                componentMapping.StoreId = StoreId;
                componentMapping.ParentId = addedMappingSet.EntityId;
                componentMapping.Type = ComponentMappingType.Normal.AsMappingStoreType();

                ICodelistObject codelist = null;
                if (component.HasCodedRepresentation())
                {
                    codelist = dsdAndDepds.GetCodelists(component.Representation.Representation).First();
                }

                var dataAttribute = component as IAttributeObject;
                var dimension = component as IDimension;
                var dataSetColumnEntities = datasetColumns.Where(entity => entity.Name.Contains("_" + component.Id)).ToArray();
                if ((dimension == null || !dimension.TimeDimension))
                {
                    // no transcoding yet
                    dataSetColumnEntities = dataSetColumnEntities.Where(entity => entity.Name.EndsWith(component.Id)).Take(1).ToArray();
                }

                componentMapping.SetColumns(dataSetColumnEntities);

                componentMapping.Component = new Component { ObjectId = component.Id };
                
                if (dataSetColumnEntities.Length == 0)
                {
                    if (dataAttribute == null || dataAttribute.Mandatory)
                    {
                        componentMapping.ConstantValue = codelist != null ? codelist.Items.First().Id : "Test value";
                    }
                }

                var addedComponentEntity = _persistenceManager.Add(componentMapping);

                if (dataSetColumnEntities.Length > 1)
                {
                    TimeDimensionMappingEntity transcodingEntity = new TimeDimensionMappingEntity();
                    transcodingEntity.ParentId = mappingSetEntity.EntityId;
                    transcodingEntity.StoreId = addedComponentEntity.StoreId;
                    if (dimension != null && dimension.TimeDimension)
                    {
                        TimeTranscodingEntity timeTranscodingEntity = new TimeTranscodingEntity();
                        timeTranscodingEntity.Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.QuarterOfYear).FrequencyCode;
                        timeTranscodingEntity.Period = new PeriodTimeTranscoding();
                        timeTranscodingEntity.Period.Column = dataSetColumnEntities[1];
                        timeTranscodingEntity.Period.AddRule("Q1", "T1");
                        timeTranscodingEntity.Period.AddRule("Q2", "T2");
                        timeTranscodingEntity.Period.Start = 1;
                        timeTranscodingEntity.Period.Length = 1;

                        timeTranscodingEntity.Year = new TimeTranscoding();
                        timeTranscodingEntity.Year.Column = dataSetColumnEntities[0];
                        timeTranscodingEntity.Year.Start = 0;
                        timeTranscodingEntity.Year.Length = 0;

                        transcodingEntity.Transcoding = TimeTranscodingConversionHelper.Convert(new[] { timeTranscodingEntity },"FREQ");
                        
                         _persistenceManager.Add(transcodingEntity);
                    }
                    else if (codelist != null)
                    {
                        // TODO test script
                        var addedTranscoding = _persistenceManager.Add(transcodingEntity);
                        List<TranscodingRuleEntity> rules = new List<TranscodingRuleEntity>();
                        foreach (var item in codelist.Items)
                        {
                            TranscodingRuleEntity transcodingRule = new TranscodingRuleEntity();
                            transcodingRule.ParentId = addedTranscoding.EntityId;
                            transcodingRule.StoreId = addedTranscoding.StoreId;

                            transcodingRule.DsdCodeEntity = new IdentifiableEntity(EntityType.Sdmx)
                                                                {
                                                                    ObjectId = item.Id
                                                                };
                            transcodingRule.LocalCodes = new List<LocalCodeEntity>();
                            for (int index = 0; index < dataSetColumnEntities.Length; index++)
                            {
                                var dataSetColumnEntity = dataSetColumnEntities[index];
                                var localCodeEntity = new LocalCodeEntity
                                                          {
                                                              ObjectId =
                                                                  string.Format(
                                                                      CultureInfo.InvariantCulture,
                                                                      "{0}_{1}_{2}",
                                                                      dataSetColumnEntity.Name,
                                                                      item.Id,
                                                                      index),
                                                              ParentId = dataSetColumnEntity.EntityId
                                                          };
                                transcodingRule.LocalCodes.Add(localCodeEntity);
                            }

                            rules.Add(transcodingRule);
                        }

                        _persistenceManager.AddEntities(rules);
                    }
                    else
                    {
                        // TODO test script
                        var addedTranscoding = _persistenceManager.Add(transcodingEntity);
                        List<TranscodingRuleEntity> rules = new List<TranscodingRuleEntity>();
                        var uncodedValues = new string[] { "uncoded 1", "uncoded 2" };
                        foreach (var uncodedValue in uncodedValues)
                        {

                            TranscodingRuleEntity transcodingRule = new TranscodingRuleEntity();
                            transcodingRule.ParentId = addedTranscoding.EntityId;
                            transcodingRule.StoreId = addedTranscoding.StoreId;
                            transcodingRule.UncodedValue = uncodedValue;
                            transcodingRule.LocalCodes = new List<LocalCodeEntity>();
                            for (int index = 0; index < dataSetColumnEntities.Length; index++)
                            {
                                var dataSetColumnEntity = dataSetColumnEntities[index];
                                var localCodeEntity = new LocalCodeEntity
                                                          {
                                                              ObjectId =
                                                                  string.Format(
                                                                      CultureInfo.InvariantCulture,
                                                                      "{0}_{1}_{2}",
                                                                      dataSetColumnEntity.Name,
                                                                      uncodedValue,
                                                                      index),
                                                              ParentId = dataSetColumnEntity.EntityId
                                                          };
                                transcodingRule.LocalCodes.Add(localCodeEntity);
                            }

                            rules.Add(transcodingRule);
                        }
                        _persistenceManager.AddEntities(rules);
                    }
                }
            }

            return mappingSetEntity;
        }

        private IDataflowObject ImportStructuralMetadata(DirectoryInfo structureSetsTemp, string sourceUrn, string targetUrn)
        {
            var artefactImportStatuses = new List<ArtefactImportStatus>();

            // TODO introduce an adapter and add a CTOR that uses Database 
            var structurePersistenceManager = new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(_connectionStringSettings, artefactImportStatuses);

            var sdmxObjects = GetSdmxObjects(_structureParsingManager);

            SaveStructureSets(sdmxObjects, _structureWritingManager, structureSetsTemp);
            var sourceDataflow = GetDataflow(sdmxObjects, sourceUrn);
            GetDataflow(sdmxObjects, targetUrn);

            structurePersistenceManager.SaveStructures(sdmxObjects);
            Assert.That(artefactImportStatuses.All(status => status.ImportMessage.Status != ImportMessageStatus.Error), string.Join("\n", artefactImportStatuses.Select(status => status.ImportMessage.Message)));
            return sourceDataflow;
        }


        /// <summary>
        /// Gets the dataflow.
        /// </summary>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        /// <param name="urn">The urn.</param>
        private IDataflowObject GetDataflow(ISdmxObjects sdmxObjects, string urn)
        {
            if (UrnUtil.GetIdentifiableType(urn).EnumType == SdmxStructureEnumType.Dataflow)
            {
                return sdmxObjects.GetDataflows(new StructureReferenceImpl(urn)).First();
            }

            if (UrnUtil.GetIdentifiableType(urn).EnumType == SdmxStructureEnumType.Dsd)
            {
                var dsd = sdmxObjects.GetDataStructures(new StructureReferenceImpl(urn)).First();
                var dataflowObjectCore = new DataflowObjectCore(new DataflowMutableCore(dsd));
                sdmxObjects.AddDataflow(dataflowObjectCore);
                return dataflowObjectCore;
            }

            Assert.Fail("input file issue. No Dataflow/Dsd related to '" + urn + "' found");
            return null;
        }

        private static void SaveStructureSets(ISdmxObjects sdmxObjects, IStructureWriterManager structureWritingManager, DirectoryInfo structureSetsTemp)
        {
            var structureSetObjects = sdmxObjects.StructureSets;
            Assert.That(structureSetObjects, Is.Not.Empty);

            if (!structureSetsTemp.Exists)
            {
                structureSetsTemp.Create();
            }

            FileInfo structureSetTempFile = new FileInfo(Path.Combine(structureSetsTemp.FullName, "structureSet.xml"));

            ISdmxObjects structureSetContainer = new SdmxObjectsImpl(new HeaderImpl("TEST", "ZZ9"), structureSetObjects);
            using (var structureSetOutputStream = structureSetTempFile.Create())
            {
                structureWritingManager.WriteStructures(
                    structureSetContainer,
                    new SdmxStructureFormat(
                        StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument)),
                    structureSetOutputStream);
            }

            // delete structure sets from old container
            foreach (var structureSetObject in structureSetObjects)
            {
                sdmxObjects.RemoveStructureSet(structureSetObject);
            }
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <param name="structureParsingManager">The structure parsing manager.</param>
        private static ISdmxObjects GetSdmxObjects(IStructureParsingManager structureParsingManager)
        {
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            var files = Directory.GetFiles("tests", "*.xml").Select(s => new FileInfo(s));
            foreach (var file in files)
            {
                sdmxObjects.Merge(file.GetSdmxObjects(structureParsingManager));
            }
            return sdmxObjects;
        }
    }
}