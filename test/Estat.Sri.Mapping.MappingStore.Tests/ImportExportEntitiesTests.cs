using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Factory;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NSubstitute;
using NUnit.Framework;
using DryIoc;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture]
    public class ImportExportEntitiesTests : BaseTestClassWithContainer
    {
        private readonly Database _database;
        private readonly EntitiesByType _entities;
        private readonly string _connectionsFilePath = Path.Combine(Environment.CurrentDirectory, "connections.xml");
        private readonly string _datasetFilePath = Path.Combine(Environment.CurrentDirectory, "datasets.xml");
        private readonly string _mappingSetsFilePath = Path.Combine(Environment.CurrentDirectory, "mappingSets.xml");
        private readonly IMappingStoreManager _mappingStoreManager;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public ImportExportEntitiesTests() : base("sqlserver")
        {
            this._database = this.Get().GetDatabase("msdb_scratch.sqlserver");
            _mappingStoreManager = this.IoCContainer.Resolve<IMappingStoreManager>();
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["MappingStoreRetrieversFactory"]);
            
            this._entities = new EntitiesByType();
        }

        [Test]
        public void ShouldWriteAndReadDatabaseConnections()
        {
            var engine = new DdbConnectionSettingsRetrieverEngine(this._database);
            var ddbConnectionEntities = engine.GetEntities(new EntityQuery(), Detail.Full).ToArray();
           
            _entities.AddRange(ddbConnectionEntities.Cast<IEntity>().ToList());

            using (var stream = new FileStream(this._connectionsFilePath, FileMode.Create))
            {
                using (var xmlEntityWriterEngine = new XmlEntityWriterEngine(stream, Encoding.UTF8,new DdbConnectionImportExportEntity()))
                {
                    xmlEntityWriterEngine.Write(this._entities);
                }
            }

            //var xDocument = XDocument.Load(this._connectionsFilePath);
            //xDocument.Descendants().Where(e => e.IsEmpty || string.IsNullOrWhiteSpace(e.Value)).Remove();
            EntitiesByType entitiesByType;
            using (var stream = new FileStream(this._connectionsFilePath, FileMode.Open))
            {
                entitiesByType = null;
                using (var xmlEntityReaderEngine = new XmlEntityReaderEngine(stream, Encoding.UTF8, new DdbConnectionImportExportEntity()))
                {
                    entitiesByType = xmlEntityReaderEngine.Parse();
                }
            }

            Assert.That(entitiesByType.GetEntities(EntityType.DdbConnectionSettings).Count, Is.EqualTo(ddbConnectionEntities.Length));
           
        }

        [Test]
        public void ShouldWriteAndReadDatasets()
        {
            //,,new MappingSetImportExport(transcodingEngine, transcodingRuleEngine, componentMappingEngine)
            var datasetEngine = new DatasetRetrieverEngine(this._database, new PermissionRetriever(_database), null);
            var datasets = datasetEngine.GetEntities(new EntityQuery(), Detail.Full).ToArray();
            this._entities.AddRange(datasets.Cast<IEntity>().ToList());
            using (var stream = new FileStream(this._datasetFilePath, FileMode.Create))
            {
                using (var xmlEntityWriterEngine = new XmlEntityWriterEngine(stream, Encoding.UTF8, new DataSetImportExportEntity()))
                {
                    xmlEntityWriterEngine.Write(this._entities);
                }
            }

            //var xDocument = XDocument.Load(this._datasetFilePath);
            //xDocument.Descendants().Where(e => e.IsEmpty || string.IsNullOrWhiteSpace(e.Value)).Remove();
            EntitiesByType entitiesByType;
            using (var stream = new FileStream(this._datasetFilePath, FileMode.Open))
            {
                entitiesByType = null;
                using (var xmlEntityReaderEngine = new XmlEntityReaderEngine(stream, Encoding.UTF8, new DataSetImportExportEntity()))
                {
                    entitiesByType = xmlEntityReaderEngine.Parse();
                }
            }

            Assert.That(entitiesByType.GetEntities(EntityType.DataSet).Count,Is.EqualTo(datasets.Count()));
        }

        [Test]
        public void ShouldWriteAndReadMappingSets()
        {
            var componentMappingEngine = new ComponentMappingRetrieverEngine(this._database);
            var componentMappingEntities = componentMappingEngine.GetEntities(new EntityQuery() , Detail.Full).ToArray();
            var mappingSetEntityRetriever = new MappingSetEntityRetriever(this._database, StoreId);
            var mappingSetEntities = mappingSetEntityRetriever.GetEntities(new EntityQuery(), Detail.Full).ToArray();
            var transcodingEngine = new TranscodingRetrieverEngine(this._database, _mappingStoreManager, "sqlserver");
            var transcodingEntities = transcodingEngine.GetEntities(new EntityQuery(), Detail.Full).ToArray();
            var transcodingRuleEngine = new TranscodingRuleRetrieverEngine(this._database);
            var transcodingRuleEntities = transcodingRuleEngine.GetEntities(new EntityQuery(), Detail.Full).ToArray();
            this._entities.AddRange(mappingSetEntities.Cast<IEntity>().ToList());
            this._entities.AddRange(componentMappingEntities.Cast<IEntity>().ToList());
            this._entities.AddRange(transcodingEntities.Cast<IEntity>().ToList());
            this._entities.AddRange(transcodingRuleEntities.Cast<IEntity>().ToList());
            using (var stream = new FileStream(this._mappingSetsFilePath, FileMode.Create))
            {
                using (var xmlEntityWriterEngine = new XmlEntityWriterEngine(stream, Encoding.UTF8, new MappingSetImportExport()))
                {
                    xmlEntityWriterEngine.Write(this._entities);
                }
            }

            EntitiesByType entitiesByType;
            using (var stream = new FileStream(this._mappingSetsFilePath, FileMode.Open))
            {
                entitiesByType = null;
                using (var xmlEntityReaderEngine = new XmlEntityReaderEngine(stream, Encoding.UTF8, new MappingSetImportExport()))
                {
                    entitiesByType = xmlEntityReaderEngine.Parse();
                }
            }

            var nonEmptyCount = mappingSetEntities.Count(entity => componentMappingEntities.Any(mappingEntity => mappingEntity.ParentId.Equals(entity.EntityId)));
            Assert.That(entitiesByType.GetEntities(EntityType.MappingSet).Count, Is.EqualTo(nonEmptyCount));
            var mappingEntities = entitiesByType.GetEntities<ComponentMappingEntity>().ToArray();
            var componentMappingEntity = mappingEntities.FirstOrDefault(entity => string.IsNullOrWhiteSpace(entity.ConstantValue) && (entity.GetColumns() == null || entity.GetColumns().Count == 0)); Assert.That(componentMappingEntity, Is.Null, $"Component Entity {componentMappingEntity?.Component?.ObjectId} entityId {componentMappingEntity?.EntityId} {componentMappingEntity?.ParentId}");
            var importedComponentMappings = mappingEntities.GroupBy(entity => entity.ParentId);
            var original = componentMappingEntities.Where(entity => entity.Component != null).GroupBy(entity => entity.ParentId);


            Assert.That(importedComponentMappings.Count(), Is.EqualTo(original.Count()));
            
            Assert.That(entitiesByType.GetEntities<TranscodingEntity>().Count(entity => entity.HasTimeTrascoding()), Is.EqualTo(transcodingEntities.Count(entity => entity.HasTimeTrascoding())));
        }


        [TearDown]
        public void TearDown()
        {
            //if (File.Exists(this._connectionsFilePath))
            //{
            //    File.Delete(this._connectionsFilePath);
            //}

            //if (File.Exists(this._datasetFilePath))
            //{
            //    File.Delete(this._datasetFilePath);
            //}

            //if (File.Exists(this._mappingSetsFilePath))
            //{
            //    File.Delete(this._mappingSetsFilePath);
            //}
        }


        [Test]
        public void ImportExportComparer()
        {
            IImportExportEntity[] helpers = {new DataSetImportExportEntity(), new MappingSetImportExport(), new DdbConnectionImportExportEntity() };
            Array.Sort(helpers, new ImportExportComparer());
            Assert.That(helpers[0],Is.TypeOf<DdbConnectionImportExportEntity>());
        }

        public DatabaseManager Get()
        {
            var _connectionStringManager = new ConfigurationStoreManager(new AppConfigStoreFactory());
            var _connectionStringSettings = _connectionStringManager.GetSettings<ConnectionStringSettings>()
                   .FirstOrDefault(settings => settings.Name.Equals("msdb_scratch.sqlserver"));
            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { _connectionStringSettings });
            return new DatabaseManager(configurationStoreManager);
        }
    }
}
