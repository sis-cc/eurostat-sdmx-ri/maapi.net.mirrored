// -----------------------------------------------------------------------
// <copyright file="TimePreFormattedPersistenceTests.cs" company="EUROSTAT">
//   Date Created : 2021-05-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Configuration;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using NSubstitute;
    using NUnit.Framework;
    using System.Linq;
    using Estat.Sri.Mapping.MappingStore.Model;
    using DryIoc;
    using Estat.Sri.Mapping.Api.Constant;
    using Dapper;
    using System.Data;
    using Estat.Sri.Mapping.MappingStore.Builder;
    using System.Globalization;
    using Estat.Sri.MappingStore.Store.Extension;

    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class TimePreFormattedPersistenceTests : BaseTestClassWithContainer
    {
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();
        private readonly ConnectionStringSettings _connectionStringSettings;
        private readonly Database _database;
        private readonly IEntityRetrieverManager _retrieverManager;
        private readonly IEntityPersistenceManager _persistManager;

        private readonly MappingSetEntity _mappingSetEntity;
        private readonly string _outputColumnId;
        private readonly string _fromColumnId;
        private readonly string _toColumnId;
        private readonly string _updateColumnId;

        public TimePreFormattedPersistenceTests(string storeId) : base(storeId)
        {
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(storeId);
            IConfigurationStoreManager configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            this._database = new Database(_connectionStringSettings);

            this._retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            this._persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();

            // use this mappingset entity
            this._mappingSetEntity = new MappingSetEntity()
            {
                EntityId = null,
                Name = "test_1538",
                StoreId = StoreId,
                Description = "",
                DataSetId = "20053",
                ParentId = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:NAMAIN_IDC_N(1.9)"
            };

            // use these dataset columns 
            WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder(_database);
            var criteria = new Criteria<long>(OperatorType.Exact, 20053);
            WhereClauseParameters whereClauseParameters = whereClauseBuilder.Build(criteria, "DS_ID");
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select COL_ID from DATASET_COLUMN where {0}", whereClauseParameters.WhereClause);
            var valuePairs = whereClauseParameters.Parameters.ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);

            var datasetColIds = this._database.Query<long>(sqlQuery, param).ToArray();
            if (datasetColIds.Count() > 3)
            {
                this._outputColumnId = datasetColIds[0].ToString();
                this._fromColumnId = datasetColIds[1].ToString();
                this._toColumnId = datasetColIds[2].ToString();
                this._updateColumnId = datasetColIds[3].ToString();
            }
        }

        [Test, Order(1)]
        public void ShouldInsertMappingSetWithTimePreFormatted()
        {
            // set up entity to insert
            var mappingSet = this._mappingSetEntity;
            mappingSet.TimePreFormattedEntity = new TimePreFormattedEntity()
            {
                EntityId = null,
                OutputColumn = new DataSetColumnEntity() { EntityId = _outputColumnId },
                FromColumn = new DataSetColumnEntity() { EntityId = _fromColumnId },
                ToColumn = new DataSetColumnEntity() { EntityId = _toColumnId }
            };
            // insert mapping set
            var writtenEntity = this._persistManager.Add(mappingSet);

            // read time pre formatted and assert results
            var readedTimePreFormatted = this.RetrieveTimePreFormatted(writtenEntity.EntityId);

            Assert.IsNotNull(readedTimePreFormatted);
            Assert.That(readedTimePreFormatted.MappingSetId.ToString(), Is.EqualTo(writtenEntity.EntityId));
            Assert.That(readedTimePreFormatted.OutputColumnId.ToString(), Is.EqualTo(_outputColumnId));
            Assert.That(readedTimePreFormatted.FromColumnId.ToString(), Is.EqualTo(_fromColumnId));
            Assert.That(readedTimePreFormatted.ToColumnId.ToString(), Is.EqualTo(_toColumnId));
        }

        [Test, Order(2)]
        public void ShouldRetrieveMappingSetWithTimePreFormatted()
        {
            // retrieve inserted mapping set id
            var existingEntityId = this.RetrieveMappingSetEntityId(this._mappingSetEntity.Name);

            // retrieve entity
            var mappingSetRetriever = _retrieverManager.GetRetrieverEngine<MappingSetEntity>(StoreId);
           var readedMappingSetEntity = mappingSetRetriever.GetEntities(
                    new EntityQuery { EntityId = new Criteria<string>(OperatorType.Exact, existingEntityId) },
                    Detail.Full).SingleOrDefault();
            var readedTimePreFormatted = readedMappingSetEntity.TimePreFormattedEntity;

            // assert results
            Assert.That(readedTimePreFormatted.EntityId, Is.EqualTo(existingEntityId));
            Assert.That(readedTimePreFormatted.OutputColumn.EntityId, Is.EqualTo(_outputColumnId));
            Assert.That(readedTimePreFormatted.FromColumn.EntityId, Is.EqualTo(_fromColumnId));
            Assert.That(readedTimePreFormatted.ToColumn.EntityId, Is.EqualTo(_toColumnId));

            Assert.IsNotNull(readedTimePreFormatted.OutputColumn.Name);
            Assert.IsNotNull(readedTimePreFormatted.FromColumn.Name);
            Assert.IsNotNull(readedTimePreFormatted.ToColumn.Name);

            Assert.IsNotNull(readedTimePreFormatted.OutputColumn.Description);
            Assert.IsNotNull(readedTimePreFormatted.FromColumn.Description);
            Assert.IsNotNull(readedTimePreFormatted.ToColumn.Description);
        }

        [Test, Order(3)]
        public void ShouldUpdateTimePreFormattedOfMappingSet()
        {
            // retrieve inserted mapping set id
            var existingEntityId = this.RetrieveMappingSetEntityId(this._mappingSetEntity.Name);

            // set up updated entity
            var mappingSet = this._mappingSetEntity;
            mappingSet.EntityId = existingEntityId;
            mappingSet.TimePreFormattedEntity = new TimePreFormattedEntity()
            {
                EntityId = existingEntityId,
                OutputColumn = new DataSetColumnEntity() { EntityId = _updateColumnId}, // this is updated
                FromColumn = new DataSetColumnEntity() { EntityId = _fromColumnId },
                ToColumn = new DataSetColumnEntity() { EntityId = _toColumnId }
            };
            // update entity
            this._persistManager.Add(mappingSet);

            // read time pre formatted and assert results
            var readedTimePreFormatted = this.RetrieveTimePreFormatted(existingEntityId);
            Assert.That(readedTimePreFormatted.OutputColumnId.ToString(), Is.EqualTo(_updateColumnId));
        }

        [Test, Order(4)]
        public void ShouldDeleteTimePreFormattedOfMappingSet()
        {
            // retrieve inserted mapping set id
            var existingEntityId = this.RetrieveMappingSetEntityId(this._mappingSetEntity.Name);

            // set up entity
            var mappingSet = this._mappingSetEntity;
            mappingSet.EntityId = existingEntityId;
            mappingSet.TimePreFormattedEntity = new TimePreFormattedEntity();
            // update mapping set with no time pre formatted
            this._persistManager.Add(mappingSet);

            // retrieve time pre formatted and assert results
            var readedTimePreFormatted = this.RetrieveTimePreFormatted(existingEntityId);
            Assert.IsNull(readedTimePreFormatted);
        }

        [Test, Order(5)]
        public void ShouldPutTimePreFormattedToMappingSet()
        {
            // retrieve inserted mapping set id
            var existingEntityId = this.RetrieveMappingSetEntityId(this._mappingSetEntity.Name);

            // set up updated entity
            var mappingSet = this._mappingSetEntity;
            mappingSet.EntityId = existingEntityId;
            mappingSet.TimePreFormattedEntity = new TimePreFormattedEntity()
            {
                EntityId = existingEntityId,
                OutputColumn = new DataSetColumnEntity() { EntityId = _outputColumnId },
                FromColumn = new DataSetColumnEntity() { EntityId = _fromColumnId },
                ToColumn = new DataSetColumnEntity() { EntityId = _toColumnId }
            };
            // update entity
            this._persistManager.Add(mappingSet);

            // read time pre formatted and assert results
            var readedTimePreFormatted = this.RetrieveTimePreFormatted(existingEntityId);

            Assert.IsNotNull(readedTimePreFormatted);
            Assert.That(readedTimePreFormatted.MappingSetId.ToString(), Is.EqualTo(existingEntityId));
            Assert.That(readedTimePreFormatted.OutputColumnId.ToString(), Is.EqualTo(_outputColumnId));
            Assert.That(readedTimePreFormatted.FromColumnId.ToString(), Is.EqualTo(_fromColumnId));
            Assert.That(readedTimePreFormatted.ToColumnId.ToString(), Is.EqualTo(_toColumnId));
        }

        [Test, Order(6)]
        public void ShouldDeleteMappingSetAlongTimePreFormatted()
        {
            // retrieve inserted mapping set id
            var existingEntityId = this.RetrieveMappingSetEntityId(this._mappingSetEntity.Name);

            // delete entity
            this._persistManager.Delete<MappingSetEntity>(StoreId, existingEntityId);

            // try to retrieve deleted entities & assert results
            var mappingSetEntityRetriever = _retrieverManager.GetRetrieverEngine<MappingSetEntity>(StoreId);
            var readedEntity = mappingSetEntityRetriever.GetEntities(
                    new EntityQuery { EntityId = new Criteria<string>(OperatorType.Exact, existingEntityId) },
                    Detail.Full).FirstOrDefault();
            var readedTimePreFormatted = this.RetrieveTimePreFormatted(existingEntityId);

            Assert.IsNull(readedEntity);
            Assert.IsNull(readedTimePreFormatted);
        }

        private MATimePreFormattedEntity RetrieveTimePreFormatted(string id)
        {
            WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder(_database);
            var criteria = new Criteria<string>(OperatorType.Exact, id);
            WhereClauseParameters whereClauseParameters = whereClauseBuilder.Build(criteria, "MAP_SET_ID");
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, 
                "select MAP_SET_ID as MappingSetId, OUTPUT_COLUMN as OutputColumnId, FROM_COLUMN as FromColumnId, TO_COLUMN as ToColumnId " +
                "from TIME_PRE_FORMATED where {0}", whereClauseParameters.WhereClause);
            var valuePairs = whereClauseParameters.Parameters.ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);

            return this._database.Query<MATimePreFormattedEntity>(sqlQuery, param).SingleOrDefault();
        }

        private string RetrieveMappingSetEntityId(string name)
        {
            var nameParam = _database.CreateInParameter("ID", DbType.AnsiString, name);
            var mapSetId = _database.ExecuteScalarFormat("select MAP_SET_ID from MAPPING_SET where ID = {0}", nameParam);
            return mapSetId.ToString();
        }
    }
}
