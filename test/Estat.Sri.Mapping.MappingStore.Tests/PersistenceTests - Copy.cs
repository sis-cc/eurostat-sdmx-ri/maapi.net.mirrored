using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dapper;
using DryIoc;
using Estat.Sri.Mapping.Api.Builder;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Engine.Streaming;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Config;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NSubstitute;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{

    public class PersistenceTests1
    {
        //[TestCase("sqlserver")]
        //public void ShouldInsertConnection(string mappingStoreIdentifier)
        //{

        //    var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
        //    var connectionStringHelper = new ConnectionStringRetriever();
        //    var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
        //    configurationStoreManager.GetSettings<ConnectionStringSettings>()
        //        .Returns(new[] { connectionStringSettings });
        //    var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
        //    var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.DdbConnectionSettings);

        //    var jsonString = @"{""entities"": { ""ddbconnections"": { 
        //                                                            """" : {
        //                                                                ""permissions"": {
        //                                                                    ""urn: sdmx: org.sdmx.infomodel.categoryscheme.Category = ESTAT:IT_445_3(1.0).2.T11.2222.T143"": ""read_write""
        //                                                                    },
        //                                                                ""type"": ""SqlServer"",
        //                                                                ""name"": ""test123tere45"",
        //                                                       ""subtype"": null,
        //                                                                ""properties"": {
        //                                                                            ""server"": { ""value"" : ""localhost""},
        //                                                                            ""database"": { ""value"" : ""mastore""},
        //                                                                            ""user id"": { ""value"" : ""mauser""},
        //                                                                            ""password"": { ""value"" : ""123""},
        //                                                                            ""port"": { ""value"" : ""1433""}
        //                                                                                }
        //                                                                     }
        //                                                            }
        //                                       }
        //                        }";


        //    JObject json = JObject.Parse(jsonString);
        //    var input = new EntityStreamingReader(mappingStoreIdentifier, json);
        //    var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
        //    while (input.MoveToNextMessage())
        //    {
        //        while (input.MoveToNextEntityTypeSection())
        //        {
        //            entityPersistenceEngine.Add(input, output);
        //        }
        //    }
        //}

        [TestCase("sqlserver")]
        public void ShouldInsertDataset(string mappingStoreIdentifier)
        {

            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { connectionStringSettings });
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.DataSet);

            var jsonString = @"{""entities"": { ""datasets"": { """" : {
                                                                        ""query"": ""select sts_sdmx_data1.FREQ as sts_sdmx_data1_FREQ, sts_sdmx_data1.REF_AREA as sts_sdmx_data1_REF_AREA, sts_sdmx_data1.ADJUSTMENT as sts_sdmx_data1_ADJUSTMENT, sts_sdmx_data1.STS_INDICATOR as sts_sdmx_data1_STS_INDICATOR, sts_sdmx_data1.STS_ACTIVITY as sts_sdmx_data1_STS_ACTIVITY, sts_sdmx_data1.STS_INSTITUTION as sts_sdmx_data1_STS_INSTITUTION, sts_sdmx_data1.STS_BASE_YEAR as sts_sdmx_data1_STS_BASE_YEAR, sts_sdmx_data1.TIME_PERIOD as sts_sdmx_data1_TIME_PERIOD, sts_sdmx_data1.OBS_VALUE as sts_sdmx_data1_OBS_VALUE, sts_sdmx_data1.OBS_CONF as sts_sdmx_data1_OBS_CONF, sts_sdmx_data1.TIME_FORMAT as sts_sdmx_data1_TIME_FORMAT from sts_sdmx_data sts_sdmx_data1"", 
                                                                        ""name"": ""mariaDatasethello"",
                                                                        ""parentId"": ""2"",
                                                                        ""editorType"":""org.estat.ma.gui.userControls.dataset.QueryEditor.CustomQueryEditor, MappingAssistant""
                                }}}}";

            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(jsonString);
            writer.Flush();
            stream.Position = 0;
            var input = new EntityStreamingReader(mappingStoreIdentifier, stream,EntityType.None);
            var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
            while (input.MoveToNextMessage())
            {
                while (input.MoveToNextEntityTypeSection())
                {
                    entityPersistenceEngine.Add(input, output);
                }
            }
        }

        //[TestCase("sqlserver")]
        //public void ShouldInsertDatasetColumn(string mappingStoreIdentifier)
        //{

        //    var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
        //    var connectionStringHelper = new ConnectionStringRetriever();
        //    var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
        //    configurationStoreManager.GetSettings<ConnectionStringSettings>()
        //        .Returns(new[] { connectionStringSettings });
        //    var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
        //    var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.DataSetColumn);

        //    var jsonString = @"{""entities"": { ""datasetColumn"": { 
        //                                                        ""-0"" : {
        //                                                            ""name"": ""column1"",
        //                                                            ""parentId"": ""20056""
        //                                                                 },
        //                                                        ""-1"" : {
        //                                                            ""name"": ""column2"",
        //                                                            ""parentId"": ""20056""
        //                                                                 },
        //                                                        ""-2"" : {
        //                                                            ""name"": ""column3"",
        //                                                            ""parentId"": ""20056""
        //                                                                 }
        //                                                        }
        //                                   }
        //                    }";


        //    JObject json = JObject.Parse(jsonString);
        //    var input = new EntityStreamingReader(mappingStoreIdentifier, json);
        //    var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
        //    while (input.MoveToNextMessage())
        //    {
        //        while (input.MoveToNextEntityTypeSection())
        //        {
        //            entityPersistenceEngine.Add(input, output);
        //        }
        //    }
        //}

        //[TestCase("sqlserver")]
        //public void ShouldInsertMappingSet(string mappingStoreIdentifier)
        //{

        //    var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
        //    var connectionStringHelper = new ConnectionStringRetriever();
        //    var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
        //    configurationStoreManager.GetSettings<ConnectionStringSettings>()
        //        .Returns(new[] { connectionStringSettings });
        //    var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
        //    var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.MappingSet);

        //    var jsonString = @"{""entities"":{ ""mappingsets"":
        //                                        {"""":
        //                                            {
        //                                                ""name"":""A122"",
        //                                                ""parentId"":""urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:HC06_ORA_DESC(1.0)"",
        //                                                ""description"":"""",
        //                                                ""entityId"":"""",
        //                                                ""dataset"":{
        //                                                            ""entityId"":""22""
        //                                                            }
        //                                            }}}}";

        //    JObject json = JObject.Parse(jsonString);
        //    var input = new EntityStreamingReader(mappingStoreIdentifier, json);
        //    var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
        //    while (input.MoveToNextMessage())
        //    {
        //        while (input.MoveToNextEntityTypeSection())
        //        {
        //            entityPersistenceEngine.Add(input, output);
        //        }
        //    }
        //}

        //[TestCase("sqlserver")]
        //public void ShouldInsertMapping(string mappingStoreIdentifier)
        //{

        //    var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
        //    var connectionStringHelper = new ConnectionStringRetriever();
        //    var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
        //    configurationStoreManager.GetSettings<ConnectionStringSettings>()
        //        .Returns(new[] { connectionStringSettings });
        //    var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
        //    var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.Mapping);

        //    var jsonString = @"{""entities"":{ ""genericmappings"":{ 
        //                                                    """":{ 
        //                                                            ""type"":""A"",
        //                                                            ""entityId"":"""",
        //                                                            ""parentId"":""30065"",
        //                                                            ""component"":{ 
        //                                                                            ""entityId"": ""10417"",
        //                                                                            ""id"":""GEO""
        //                                                                          },
        //                                                            ""constantValue"":null,
        //                                                            ""dataset_column"":[{
        //                                                                                 ""entityId"":""285"",
        //                                                                                 ""name"":""STS_SDMX_DATA1_ADJUSTMENT"",
        //                                                                                 ""parentId"":""22""
        //                                                                                }]
        //                                                          }
        //                                                    }}}";

        //  JObject json = JObject.Parse(jsonString);
        //    var input = new EntityStreamingReader(mappingStoreIdentifier, json);
        //    var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
        //    while (input.MoveToNextMessage())
        //    {
        //        while (input.MoveToNextEntityTypeSection())
        //        {
        //            entityPersistenceEngine.Add(input, output);
        //        }
        //    }
        //}
        //[TestCase("sqlserver")]
        //public void ShouldInsertTranscoding(string mappingStoreIdentifier)
        //{

        //    var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
        //    var connectionStringHelper = new ConnectionStringRetriever();
        //    var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
        //    configurationStoreManager.GetSettings<ConnectionStringSettings>()
        //        .Returns(new[] { connectionStringSettings });
        //    var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
        //    var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.Transcoding);
        //    var jsonString = @"{""entities"":{ 
        //                                           ""transcodings"":{ 
        //                                                    """":{ 
        //                                                            ""entityId"":"""",
        //                                                            ""name"":"""",                                                                    
        //                                                            ""description"":"""",
        //                                                            ""details"":{},
        //                                                            ""parentId"":""86"",
        //                                                            ""links"":[],
        //                                                            ""expresssion"":""null"",
        //                                                            ""script"":[],
        //                                                            ""time_transcoding"":{ }
        //                                                          }
        //                                                    }}}";

        //    JObject json = JObject.Parse(jsonString);
        //    var input = new EntityStreamingReader(mappingStoreIdentifier, json);
        //    var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
        //    while (input.MoveToNextMessage())
        //    {
        //        while (input.MoveToNextEntityTypeSection())
        //        {
        //            entityPersistenceEngine.Add(input, output);
        //        }
        //    }
        //}
        //[TestCase("sqlserver")]
        //public void ShouldInsertTranscodingRule(string mappingStoreIdentifier)
        //{

        //    var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
        //    var connectionStringHelper = new ConnectionStringRetriever();
        //    var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
        //    configurationStoreManager.GetSettings<ConnectionStringSettings>()
        //        .Returns(new[] { connectionStringSettings });
        //    var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
        //    var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.TranscodingRule);
        //   var jsonString = @"{""entities"":{ 
        //                                           ""transcodingRules"":[ 
        //                                                                    { 
        //                                                                        ""entityId"":"""",
        //                                                                        ""name"":"""",                                                                    
        //                                                                        ""description"":"""",
        //                                                                        ""details"":{},
        //                                                                        ""parentId"":""10005"",
        //                                                                        ""links"":[],
        //                                                                        ""code"":{
        //                                                                                    ""entityId"":null,
        //                                                                                    ""name"":null,
        //                                                                                    ""description"":null,
        //                                                                                    ""details"":{ },
        //                                                                                    ""parentId"":null,
        //                                                                                    ""links"":[],
        //                                                                                    ""id"":""B""
        //                                                                                },
        //                                                                        ""localCodes"":[
        //                                                                                            {
        //                                                                                                ""entityId"":"""",
        //                                                                                                ""name"":null,
        //                                                                                                ""description"":null,
        //                                                                                                ""details"":{},
        //                                                                                                ""parentId"":""17"",
        //                                                                                                ""links"":[],
        //                                                                                                ""id"":""2000""
        //                                                                                            }
        //                                                                                        ],
        //                                                                        ""uncodedValue"":null
        //                                                                    }
        //                                                                ]
        //                                    }
        //                    }";

        //    JObject json = JObject.Parse(jsonString);
        //    var input = new EntityStreamingReader(mappingStoreIdentifier, json);
        //    var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
        //    while (input.MoveToNextMessage())
        //    {
        //        while (input.MoveToNextEntityTypeSection())
        //        {
        //            entityPersistenceEngine.Add(input, output);
        //        }
        //    }
        //}

        //[TestCase("sqlserver")]
        //public void ShouldInsertTimeTranscoding(string mappingStoreIdentifier)
        //{

        //    var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
        //    var connectionStringHelper = new ConnectionStringRetriever();
        //    var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
        //    configurationStoreManager.GetSettings<ConnectionStringSettings>()
        //        .Returns(new[] { connectionStringSettings });
        //    var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
        //    var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.Transcoding);
        //    //var jsonString = @"{
        //    //                     ""entities"":
        //    //                      {""transcodings"":
        //    //                       {"""":
        //    //                        {
        //    //                         ""entityId"":"""",
        //    //                         ""parentId"":""83"",
        //    //                         ""timeTranscoding"":
        //    //                           {
        //    //                            ""A"":{""year"":{""column"":{""entityId"":""21"",""name"":""TIME_PERIOD""},""start"":0,""length"":4}},
        //    //                            ""Q"":{""year"":{""column"":{""entityId"":""21"",""name"":""TIME_PERIOD""},""start"":0,""length"":4},""period"":{""column"":{""entityId"":""21"",""name"":""TIME_PERIOD""},""start"":4,""length"":6,""rules"":[{""code"":{""id"":""Q1""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200501""}]},{""code"":{""id"":""Q2""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200502""}]},{""code"":{""id"":""Q3""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200503""}]},{""code"":{""id"":""Q4""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200504""}]}]}
        //    //                            },
        //    //                            ""M"":{""year"":{""column"":{""entityId"":""21"",""name"":""TIME_PERIOD""},""start"":0,""length"":4},""period"":{""column"":{""entityId"":""21"",""name"":""TIME_PERIOD""},""start"":4,""length"":6,""rules"":[{""code"":{""id"":""01""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200501""}]},{""code"":{""id"":""02""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200502""}]},{""code"":{""id"":""03""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200503""}]},{""code"":{""id"":""04""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200504""}]},{""code"":{""id"":""05""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200505""}]},{""code"":{""id"":""06""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200506""}]},{""code"":{""id"":""07""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200507""}]},{""code"":{""id"":""08""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200508""}]},{""code"":{""id"":""09""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200509""}]},{""code"":{""id"":""10""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200510""}]},{""code"":{""id"":""11""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200511""}]},{""code"":{""id"":""12""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200512""}]}]}
        //    //                            }
        //    //                           }
        //    //                        }
        //    //                       }
        //    //                      }
        //    //                    }";
        //    var jsonString = @"{
        //                         ""entities"":
        //                          {""transcodings"":
        //                           {"""":
        //                            {
        //                             ""entityId"":"""",
        //                             ""parentId"":""83"",
        //                             ""timeTranscoding"":
        //                               {
        //                                ""Q"":{""year"":{""column"":{""entityId"":""21"",""name"":""TIME_PERIOD""},""start"":0,""length"":4},""period"":{""column"":{""entityId"":""21"",""name"":""TIME_PERIOD""},""start"":4,""length"":6,""rules"":[{""code"":{""id"":""Q1""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200501""}]},{""code"":{""id"":""Q2""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200502""}]},{""code"":{""id"":""Q3""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200503""}]},{""code"":{""id"":""Q4""},""uncodedValue"":null,""localCodes"":[{""parentId"":""21"",""id"":""200504""}]}]}}
        //                               }
        //                            }
        //                           }
        //                          }
        //                        }";
        //    JObject json = JObject.Parse(jsonString);
        //    var input = new EntityStreamingReader(mappingStoreIdentifier, json);
        //    var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
        //    while (input.MoveToNextMessage())
        //    {
        //        while (input.MoveToNextEntityTypeSection())
        //        {
        //            entityPersistenceEngine.Add(input, output);
        //        }
        //    }
        //}
        //[TestCase("sqlserver")]
        //public void ShouldInsertHeaderTemplate(string mappingStoreIdentifier)
        //{

        //    var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
        //    var connectionStringHelper = new ConnectionStringRetriever();
        //    var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
        //    configurationStoreManager.GetSettings<ConnectionStringSettings>()
        //        .Returns(new[] { connectionStringSettings });
        //    var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
        //    var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.Header);

        //    var jsonString = @"{
        //         ""entities"":{
        //          ""headerTemplates"":{
        //              """":{
        //               ""name"":""Stefanidou Maria"",
        //               ""source"":""adfda"",
        //               ""entityId"":null,
        //               ""description"":null,
        //               ""details"":{},
        //               ""parentId"":""urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_M(2.0)"",
        //               ""links"":[],
        //               ""dataSetAgencyId"":""dfd"",
        //                                    ""test"":""false"",

        //               ""sender"":{
        //	                ""name"":{
        //		                ""en"":""dd""
        //	                },
        //	                ""id"":""adfa"",
        //	                ""contacts"":[{""name"":{""en"":""dd""},""department"":{""en"":""dep""},""role"":{""en"":""sa""},""email"":[""dfadfd""],""additionalProperties"":[]}],
        //	                ""entityId"":0,
        //	                ""links"":[],
        //	                ""description"":null
        //               },

        //               ""receiver"":[{
        //		                ""name"":{""en"":""dd""},
        //		                ""id"":""adfa"",
        //		                ""contacts"":[
        //					                {""name"":{
        //							                ""en"":""dd""
        //							                },
        //					                ""department"":{""en"":""ffdfd""},
        //					                ""role"":{""en"":""sa""},
        //					                ""email"":[""dfadfd""],
        //					                ""additionalProperties"":[]}],
        //					                ""entityId"":0,
        //					                ""links"":[],
        //					                ""description"":null
        //		                }]
        //               }
        //              }
        //            }
        //        }";
        //    JObject json = JObject.Parse(jsonString);
        //    var input = new EntityStreamingReader(mappingStoreIdentifier, json);
        //    var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
        //    while (input.MoveToNextMessage())
        //    {
        //        while (input.MoveToNextEntityTypeSection())
        //        {
        //            entityPersistenceEngine.Add(input, output);
        //        }
        //    }
        //}
        //[TestCase("sqlserver")]
        //public void ShouldInsertRegistry(string mappingStoreIdentifier)
        //{

        //    var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
        //    var connectionStringHelper = new ConnectionStringRetriever();
        //    var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
        //    configurationStoreManager.GetSettings<ConnectionStringSettings>()
        //        .Returns(new[] { connectionStringSettings });
        //    var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
        //    var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.Registry);

        //    var jsonString = @"{""entities"":{
        //                       ""registry"":{
        //                          """":{
        //                           ""name"":""nameReg1"",
        //                           ""description"":""descReg"",
        //                           ""url"":""urlReg"",
        //                           ""details"":{""public"":true,""upgrades"":true},
        //                           ""technology"":""SoapV20"",
        //                           ""username"":""userName"",
        //                           ""password"":""pass""
        //                           }
        //                          }
        //                       }
        //                    }";

        //    MemoryStream stream = new MemoryStream();
        //    StreamWriter writer = new StreamWriter(stream);
        //    writer.Write(jsonString);
        //    writer.Flush();
        //    stream.Position = 0;
        //    var input = new EntityStreamingReader(mappingStoreIdentifier, stream);
        //    var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
        //    while (input.MoveToNextMessage())
        //    {
        //        while (input.MoveToNextEntityTypeSection())
        //        {
        //            entityPersistenceEngine.Add(input, output);
        //        }
        //    }
        //}
        //[TestCase("sqlserver")]
        //public void ShouldInsertRegistry(string mappingStoreIdentifier)
        //{

        //    var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
        //    var connectionStringHelper = new ConnectionStringRetriever();
        //    var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
        //    configurationStoreManager.GetSettings<ConnectionStringSettings>()
        //        .Returns(new[] { connectionStringSettings });
        //    var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
        //    var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.Registry);

        //    var jsonString = @"{""entities"":{
        //                       ""registry"":{
        //                          """":{
        //                           ""name"":""nameReg2"",
        //                           ""description"":""descReg"",
        //                           ""url"":""urlReg"",
        //                           ""details"":{""public"":true,""upgrades"":true},
        //                           ""technology"":""SoapV20"",
        //                           ""username"":""userName"",
        //                           ""password"":""pass""
        //                           }
        //                          }
        //                       }
        //                    }";

        //    JObject json = JObject.Parse(jsonString);
        //    var input = new EntityStreamingReader(mappingStoreIdentifier, json);
        //    var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
        //    while (input.MoveToNextMessage())
        //    {
        //        while (input.MoveToNextEntityTypeSection())
        //        {
        //            entityPersistenceEngine.Add(input, output);
        //        }
        //    }
        //}
        //[TestCase("sqlserver")]
        //public void ShouldInsertUser(string mappingStoreIdentifier)
        //{

        //    var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
        //    var connectionStringHelper = new ConnectionStringRetriever();
        //    var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
        //    configurationStoreManager.GetSettings<ConnectionStringSettings>()
        //        .Returns(new[] { connectionStringSettings });
        //    var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
        //    var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.User);

        //    var jsonString = @"{""newUser"":{
        //               ""defaultStoreId"":""MappingStoreServer"",
        //               ""accessRules"":[""AdminRole""],
        //                                    ""storeIds"":[""MappingStoreServer""]
        //                       }
        //                    }";

        //    JObject json = JObject.Parse(jsonString);
        //    var input = new EntityStreamingReader(mappingStoreIdentifier, json);
        //    var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
        //    while (input.MoveToNextMessage())
        //    {
        //        while (input.MoveToNextEntityTypeSection())
        //        {
        //            entityPersistenceEngine.Add(input, output);
        //        }
        //    }
        //}
        //[TestCase("sqlserver")]
        //public void ShouldInsertTemplateMapping(string mappingStoreIdentifier)
        //{

        //    var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
        //    var connectionStringHelper = new ConnectionStringRetriever();
        //    var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(mappingStoreIdentifier);
        //    configurationStoreManager.GetSettings<ConnectionStringSettings>()
        //        .Returns(new[] { connectionStringSettings });
        //    var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
        //    var entityPersistenceEngine = entityPeristenceFactory.GetEngine(mappingStoreIdentifier, EntityType.TemplateMapping);

        //    var jsonString = @"{""entities"": {
        //                                        ""templateMapping"": { 
        //                                                            """" : {
        //                                                                    ""columnName"": ""REF_AREA"", 
        //                                                                    ""columnDesc"": ""testDesc"", 
        //                                                                    ""componentId"": ""1"", 
        //                                                                    ""componentType"": ""Dimnension"", 
        //                                                                    ""conId"": ""19496"", 
        //                                                                    ""itemSchemeId"": ""2"", 

        //                            }}}}";

        //    MemoryStream stream = new MemoryStream();
        //    StreamWriter writer = new StreamWriter(stream);
        //    writer.Write(jsonString);
        //    writer.Flush();
        //    stream.Position = 0;
        //    var input = new EntityStreamingReader(mappingStoreIdentifier, stream);
        //    var output = new EntityStreamingWriter(mappingStoreIdentifier, new MemoryStream(), null, null);
        //    while (input.MoveToNextMessage())
        //    {
        //        while (input.MoveToNextEntityTypeSection())
        //        {
        //            entityPersistenceEngine.Add(input, output);
        //        }
        //    }
        //}
    }
}
