﻿// -----------------------------------------------------------------------
// <copyright file="DataProviderTest.cs" company="EUROSTAT">
//   Date Created : 2017-04-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.MappingStoreRetrieval.Config;

    using NUnit.Framework;

    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class DataProviderTest : BaseTestClassWithContainer
    {
        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();
        private readonly IDatabaseProviderManager _manager;

        private readonly ConnectionStringSettings _connectionStringSettings;

        public DataProviderTest(string connectionName) : base(connectionName)
        {
           
            _manager = IoCContainer.Resolve<IDatabaseProviderManager>();
            _connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(connectionName);
        }

        [Test]
        public void TestPlugin()
        {
            var databaseProviderEngine = this._manager.GetEngineByProvider(this._connectionStringSettings.ProviderName);
            Assert.That(databaseProviderEngine, Is.Not.Null);
            Assert.That(
                databaseProviderEngine.ProviderName,
                Is.EqualTo(this._connectionStringSettings.ProviderName));
        }

        [Test]
        public void TestBrowserTables()
        {
            var engine = this._manager.GetEngineByProvider(this._connectionStringSettings.ProviderName);
            var databaseSchemaBrowser = engine.Browser;
            Assert.That(databaseSchemaBrowser, Is.Not.Null);

            var databaseObjects = databaseSchemaBrowser.GetDatabaseObjects(this._connectionStringSettings).ToArray();
            Assert.That(databaseObjects, Is.Not.Null);
            Assert.That(databaseObjects.Length, Is.GreaterThan(0), "No Database object retrieved");

            var tables = databaseObjects.Where(o => o.ObjectType == DatabaseObjectType.Table).ToArray();

            Assert.That(tables.Length, Is.GreaterThan(0), "No tables found");
        }

        [Test]
        public void TestBrowserTablesFields()
        {
            var engine = this._manager.GetEngineByProvider(this._connectionStringSettings.ProviderName);
            var databaseSchemaBrowser = engine.Browser;
            Assert.That(databaseSchemaBrowser, Is.Not.Null);

            var databaseObjects = databaseSchemaBrowser.GetDatabaseObjects(this._connectionStringSettings).ToArray();
            Assert.That(databaseObjects, Is.Not.Null);
            Assert.That(databaseObjects.Length, Is.GreaterThan(0), "No Database object retrieved");

            var tables = databaseObjects.Where(o => o.ObjectType == DatabaseObjectType.Table).ToArray();

            Assert.That(tables.Length, Is.GreaterThan(0), "No tables found");

            var table = tables.First();

            var fieldInfos = databaseSchemaBrowser.GetSchema(this._connectionStringSettings, table);
            Assert.That(fieldInfos, Is.Not.Null.And.Not.Empty);
        }

        [Test]
        public void TestBrowserViews()
        {
            var engine = this._manager.GetEngineByProvider(this._connectionStringSettings.ProviderName);
            var databaseSchemaBrowser = engine.Browser;
            Assert.That(databaseSchemaBrowser, Is.Not.Null);
            var databaseObjects = databaseSchemaBrowser.GetDatabaseObjects(this._connectionStringSettings).ToArray();
            Assert.That(databaseObjects, Is.Not.Null);
            Assert.That(databaseObjects.Length, Is.GreaterThan(0), "No Database object retrieved");

            var views = databaseObjects.Where(o => o.ObjectType == DatabaseObjectType.View).ToArray();
            Assert.That(views.Length, Is.GreaterThan(0), "No views found");
        }


        [Test]
        public void TestBrowserViewsFields()
        {
            var engine = this._manager.GetEngineByProvider(this._connectionStringSettings.ProviderName);
            var databaseSchemaBrowser = engine.Browser;
            Assert.That(databaseSchemaBrowser, Is.Not.Null);
            var databaseObjects = databaseSchemaBrowser.GetDatabaseObjects(this._connectionStringSettings).ToArray();
            Assert.That(databaseObjects, Is.Not.Null);
            Assert.That(databaseObjects.Length, Is.GreaterThan(0), "No Database object retrieved");

            var views = databaseObjects.Where(o => o.ObjectType == DatabaseObjectType.View).ToArray();
            Assert.That(views.Length, Is.GreaterThan(0), "No views found");
            var view = views.First();

            var fieldInfos = databaseSchemaBrowser.GetSchema(this._connectionStringSettings, view);
            Assert.That(fieldInfos, Is.Not.Null.Or.Not.Empty);
        }

        [Test]
        public void TestConnectionBuilder()
        {
            var engine = this._manager.GetEngineByProvider(this._connectionStringSettings.ProviderName);
            var builder = engine.SettingsBuilder;
            var connectionEntity = builder.CreateConnectionEntity(this._connectionStringSettings);
            Assert.That(connectionEntity, Is.Not.Null);
            Assert.That(connectionEntity.DatabaseVendorType, Is.EqualTo(engine.Name));
            var roundTrip = builder.CreateConnectionString(connectionEntity);
            Assert.That(roundTrip.ProviderName, Is.EqualTo(this._connectionStringSettings.ProviderName).IgnoreCase);
            var roundTripSplit = new HashSet<string>(roundTrip.ConnectionString.Split(';'), StringComparer.OrdinalIgnoreCase);
            var originalSplit = this._connectionStringSettings.ConnectionString.Split(';');
            Assert.That(roundTripSplit.Count, Is.EqualTo(originalSplit.Length));
            roundTripSplit.IntersectWith(originalSplit);
            Assert.That(roundTripSplit.Count, Is.EqualTo(originalSplit.Length));

        }

        [Test]
        public void TestConnectionBuilderEmpty()
        {
            var engine = this._manager.GetEngineByProvider(this._connectionStringSettings.ProviderName);
            var builder = engine.SettingsBuilder;
            var connectionEntity = builder.CreateConnectionEntity();
            Assert.That(connectionEntity, Is.Not.Null);
            Assert.That(connectionEntity.DatabaseVendorType, Is.EqualTo(engine.Name));
            Assert.That(connectionEntity.Settings, Is.Not.Null.And.Not.Empty);

        }
    }
}