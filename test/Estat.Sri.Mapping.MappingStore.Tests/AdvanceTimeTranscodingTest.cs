// -----------------------------------------------------------------------
// <copyright file="AdvanceTimeTranscodingTest - Copy.cs" company="EUROSTAT">
//   Date Created : 2018-4-17
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStore.Store.Model;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;
    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture("msdb_scratch.oracle")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.sqlserver")]
    internal class AdvanceTimeTranscodingTest : BaseTestClassWithContainer
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ComponentMappingManagerTest));

        /// <summary>
        /// The connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The entity persistence manager
        /// </summary>
        private readonly IEntityPersistenceManager _entityPersistenceManager;

        /// <summary>
        /// The entity retriever manager
        /// </summary>
        private readonly IEntityRetrieverManager _entityRetrieverManager;

        private IDataflowObject _dataflowObject;

        private IDataStructureObject _dataStructureObject;
        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMappingEndToEndTest"/> class.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        public AdvanceTimeTranscodingTest(string storeId)
            : base(storeId)
        {
            this._entityPersistenceManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            this._entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            _connectionStringSettings = GetConnectionStringSettings();
            ReBuildMappingStore();
        }

        [Test]
        public void ShouldInsertAdvanceTimeTranscodingWithDuration()
        {
            var mappingSetEntities = this._entityRetrieverManager.GetEntities<MappingSetEntity>(this.StoreId, new EntityQuery(), Detail.Full).First();
            var datasetcolumns = this._entityRetrieverManager.GetEntities<DataSetColumnEntity>(this.StoreId,
               new DatasetEntity() {EntityId = mappingSetEntities.DataSetId}.QueryForThisParentId(), Detail.Full).ToList();
     
            // get time dimension commponent mapping
            var componentMappingEntities = this._entityRetrieverManager.GetEntities<TimeDimensionMappingEntity>(this.StoreId, mappingSetEntities.QueryForThisParentId(), Detail.Full);
            var timeDimensionMapping = componentMappingEntities.Single();

            // add time dimension transcoding (advanced)
             // add duration mapping
             var durationMappingEntity = new DurationMappingEntity
             {
                 ConstantValue = "P1Y"
             };
            TimeTranscodingAdvancedEntity advancedEntity = new TimeTranscodingAdvancedEntity();
            advancedEntity.CriteriaColumn = datasetcolumns.First();
            TimeFormatConfiguration timeFormatConfiguration = AddStartDurationDateTime(durationMappingEntity, datasetcolumns);
            advancedEntity.TimeFormatConfigurations.Add("A", timeFormatConfiguration);
            TimeDimensionMappingEntity mappingEntity = new TimeDimensionMappingEntity();
            mappingEntity.Transcoding = advancedEntity;
            mappingEntity.TimeMappingType = TimeDimensionMappingType.TranscodingWithCriteriaColumn;
            mappingEntity.EntityId = mappingSetEntities.EntityId;
            mappingEntity.ParentId = mappingSetEntities.EntityId;
            mappingEntity.StoreId = StoreId;
            var returnValue = this._entityPersistenceManager.Add(mappingEntity);

            var roundTripTimeMapping = this._entityRetrieverManager.GetEntities<TimeDimensionMappingEntity>(
                this.StoreId,
                returnValue.QueryForThisEntityId(),
                Detail.Full).FirstOrDefault();

            Assert.That(roundTripTimeMapping, Is.Not.Null);
            Assert.That(roundTripTimeMapping.Transcoding, Is.Not.Null);
            Assert.That(roundTripTimeMapping.TimeMappingType, Is.EqualTo(mappingEntity.TimeMappingType));
            Assert.That(roundTripTimeMapping.Transcoding.CriteriaColumn, Is.Not.Null);
            Assert.That(roundTripTimeMapping.Transcoding.CriteriaColumn.Name, Is.EqualTo(advancedEntity.CriteriaColumn.Name));
            var timeFormatConfigurations = roundTripTimeMapping.Transcoding.TimeFormatConfigurations;
            Assert.That(timeFormatConfigurations, Is.Not.Empty);
            Assert.That(timeFormatConfigurations.Count, Is.EqualTo(1));
            Assert.That(timeFormatConfigurations.ContainsKey("A"));
            var formatConfiguration = timeFormatConfigurations.First().Value;
            Assert.That(formatConfiguration.StartConfig, Is.Not.Null);
            Assert.That(formatConfiguration.EndConfig, Is.Null);
            Assert.That(formatConfiguration.DurationMap, Is.Not.Null);
            Assert.That(formatConfiguration.DurationMap.MappingType, Is.EqualTo(ComponentMappingType.Duration));
            Assert.That(formatConfiguration.DurationMap.ConstantValue, Is.EqualTo(durationMappingEntity.ConstantValue));
            Assert.That(formatConfiguration.OutputFormat, Is.EqualTo(timeFormatConfiguration.OutputFormat));
            Assert.That(formatConfiguration.StartConfig.Format, Is.EqualTo(timeFormatConfiguration.StartConfig.Format));
            Assert.That(formatConfiguration.StartConfig.DateColumn.EntityId, Is.EqualTo(timeFormatConfiguration.StartConfig.DateColumn.EntityId));
            Assert.That(formatConfiguration.StartConfig.Year, Is.Null);
            Assert.That(formatConfiguration.StartConfig.Period, Is.Null);

            this._entityPersistenceManager.Delete<TimeDimensionMappingEntity>(StoreId, roundTripTimeMapping.EntityId);
        }

        [Test]
        public void ShouldInsertAdvanceTimeTranscodingWithDurationEndDate()
        {
            var mappingSetEntities = this._entityRetrieverManager.GetEntities<MappingSetEntity>(this.StoreId, new EntityQuery(), Detail.IdOnly).First();
            
            var datasetcolumns = this._entityRetrieverManager.GetEntities<DataSetColumnEntity>(this.StoreId,
               new DatasetEntity() {EntityId = mappingSetEntities.DataSetId}.QueryForThisParentId(), Detail.Full).ToList();

            // get time dimension commponent mapping
            var componentMappingEntities = this._entityRetrieverManager.GetEntities<TimeDimensionMappingEntity>(this.StoreId, mappingSetEntities.QueryForThisParentId(), Detail.Full);
            var timeDimensionMapping = componentMappingEntities.Single();

            // add time dimension transcoding (advanced)
            TimeTranscodingAdvancedEntity advancedEntity = new TimeTranscodingAdvancedEntity();
               var durationMappingEntity = new DurationMappingEntity()
                        {
                            ConstantValue = "P1Y"
                        };
               advancedEntity.CriteriaColumn = datasetcolumns.First();
            TimeFormatConfiguration withEnd = AddEndDateMonthlyWithDuration(durationMappingEntity, datasetcolumns);
            advancedEntity.TimeFormatConfigurations.Add("WITH_END", withEnd);
            var withStart = AddStartDurationDateTime(durationMappingEntity, datasetcolumns);
            advancedEntity.TimeFormatConfigurations.Add("WITH_START", withStart);

            var transcodingEntity = new TimeDimensionMappingEntity();
            transcodingEntity.Transcoding = advancedEntity;
            transcodingEntity.ParentId = timeDimensionMapping.EntityId;
            transcodingEntity.StoreId = StoreId;
            var returnValue = this._entityPersistenceManager.Add(transcodingEntity);

            var roundtripTranscoding = this._entityRetrieverManager.GetEntities<TimeDimensionMappingEntity>(
                this.StoreId,
                returnValue.QueryForThisEntityId(),
                Detail.Full).FirstOrDefault();

            Assert.That(roundtripTranscoding, Is.Not.Null);
            Assert.That(roundtripTranscoding.Transcoding, Is.Not.Null);
            Assert.That(roundtripTranscoding.TimeMappingType, Is.EqualTo(TimeDimensionMappingType.TranscodingWithCriteriaColumn));
            Assert.That(roundtripTranscoding.Transcoding.CriteriaColumn, Is.Not.Null);
            Assert.That(roundtripTranscoding.Transcoding.CriteriaColumn.EntityId, Is.Not.Null);
            var timeFormatConfigurations = roundtripTranscoding.Transcoding.TimeFormatConfigurations;
            Assert.That(timeFormatConfigurations, Is.Not.Empty);
            Assert.That(timeFormatConfigurations.Count, Is.EqualTo(2));
            Assert.That(timeFormatConfigurations.ContainsKey("WITH_START"));
            Assert.That(timeFormatConfigurations.ContainsKey("WITH_END"));
            var formatConfiguration = timeFormatConfigurations["WITH_START"];
            Assert.That(formatConfiguration.StartConfig, Is.Not.Null);
            Assert.That(formatConfiguration.EndConfig, Is.Null);
            Assert.That(formatConfiguration.DurationMap, Is.Not.Null);
            Assert.That(formatConfiguration.DurationMap.MappingType, Is.EqualTo(durationMappingEntity.MappingType));
            Assert.That(formatConfiguration.DurationMap.ConstantValue, Is.EqualTo(durationMappingEntity.ConstantValue));
            Assert.That(formatConfiguration.OutputFormat, Is.EqualTo(withStart.OutputFormat));
            Assert.That(formatConfiguration.StartConfig.Format, Is.EqualTo(withStart.StartConfig.Format));
            Assert.That(formatConfiguration.StartConfig.DateColumn.Name, Is.EqualTo(withStart.StartConfig.DateColumn.Name));
            Assert.That(formatConfiguration.StartConfig.Year, Is.Null);
            Assert.That(formatConfiguration.StartConfig.Period, Is.Null);

            formatConfiguration = timeFormatConfigurations["WITH_END"];
            Assert.That(formatConfiguration.StartConfig, Is.Null);
            Assert.That(formatConfiguration.EndConfig, Is.Not.Null);
            Assert.That(formatConfiguration.DurationMap, Is.Not.Null);
            Assert.That(formatConfiguration.DurationMap.MappingType, Is.EqualTo(durationMappingEntity.MappingType));
            Assert.That(formatConfiguration.DurationMap.ConstantValue, Is.EqualTo(durationMappingEntity.ConstantValue));
            Assert.That(formatConfiguration.OutputFormat, Is.EqualTo(withEnd.OutputFormat));
            Assert.That(formatConfiguration.EndConfig.Format, Is.EqualTo(withEnd.EndConfig.Format));
            Assert.That(formatConfiguration.EndConfig.DateColumn, Is.Null);
            Assert.That(formatConfiguration.EndConfig.Year, Is.Not.Null);
            Assert.That(formatConfiguration.EndConfig.Year.Column, Is.Not.Null);
            Assert.That(formatConfiguration.EndConfig.Year.Column.EntityId, Is.EqualTo(withEnd.EndConfig.Year.Column.EntityId));
            Assert.That(formatConfiguration.EndConfig.Year.Length, Is.EqualTo(withEnd.EndConfig.Year.Length));
            Assert.That(formatConfiguration.EndConfig.Year.Start, Is.EqualTo(withEnd.EndConfig.Year.Start));
            Assert.That(formatConfiguration.EndConfig.Period, Is.Not.Null);
            Assert.That(formatConfiguration.EndConfig.Period.Column, Is.Not.Null);
            Assert.That(formatConfiguration.EndConfig.Period.Length, Is.EqualTo(withEnd.EndConfig.Period.Length));
            Assert.That(formatConfiguration.EndConfig.Period.Start, Is.EqualTo(withEnd.EndConfig.Period.Start));
            Assert.That(formatConfiguration.EndConfig.Period.Rules, Is.Not.Null.And.Not.Empty);
            Assert.That(formatConfiguration.EndConfig.Period.Rules.Count, Is.EqualTo(12));
            Assert.That(formatConfiguration.EndConfig.Period.Rules, Has.All.Not.Null);
            Assert.That(formatConfiguration.EndConfig.Period.Rules[0].SdmxPeriodCode, Is.EqualTo("01"));
            Assert.That(formatConfiguration.EndConfig.Period.Rules[0].LocalPeriodCode.ToUpperInvariant(), Is.EqualTo("JAN"));
            Assert.That(formatConfiguration.EndConfig.Period.Rules[11].SdmxPeriodCode, Is.EqualTo("12"));
            Assert.That(formatConfiguration.EndConfig.Period.Rules[11].LocalPeriodCode.ToUpperInvariant(), Is.EqualTo("DEC"));

            this._entityPersistenceManager.Delete<TimeDimensionMappingEntity>(StoreId, roundtripTranscoding.EntityId);
        }

        private static TimeFormatConfiguration AddEndDateMonthlyWithDuration(DurationMappingEntity roundTripEntity, IList<DataSetColumnEntity> dataSetColumnEntities)
        {
            var timeFormatConfiguration = new TimeFormatConfiguration();
            timeFormatConfiguration.OutputFormat = TimeFormat.TimeRange;
            var endConfig = new TimeParticleConfiguration();
            endConfig.Year = new TimeRelatedColumnInfo() { Column = dataSetColumnEntities[1], Start = 0, Length = 4 };
            endConfig.Period = new PeriodTimeRelatedColumnInfo() { Column = dataSetColumnEntities[1], Start = 5, Length = 3 };
            var durationMap = roundTripEntity;
            timeFormatConfiguration.SetConfiguration(durationMap, endConfig);
            for (int i = 1; i < 13; i++)
            {
                string monthName = CultureInfo.InvariantCulture.DateTimeFormat.GetAbbreviatedMonthName(i);

                timeFormatConfiguration.EndConfig.AddRule(TimeFormatEnumType.Month, i, monthName);
            }

            timeFormatConfiguration.EndConfig.Format = DisseminationDatabaseTimeFormat.Monthly;
            return timeFormatConfiguration;
        }

        private static TimeFormatConfiguration AddStartDurationDateTime(DurationMappingEntity roundTripEntity, IList<DataSetColumnEntity> dataSetColumnEntities)
        {
            var timeFormatConfiguration = new TimeFormatConfiguration();
            timeFormatConfiguration.OutputFormat = TimeFormat.TimeRange;
            var startConfig = TimeParticleConfiguration.CreateDateFormat(dataSetColumnEntities[1]);
            var durationMap = roundTripEntity;
            timeFormatConfiguration.SetConfiguration(startConfig, durationMap);
            return timeFormatConfiguration;
        }

        private static TimeFormatConfiguration AddStartEndDateIsoWithoutDuration(IList<DataSetColumnEntity> dataSetColumnEntities)
        {
            var timeFormatConfiguration = new TimeFormatConfiguration();
            timeFormatConfiguration.OutputFormat = TimeFormat.TimeRange;
            var startConfig = new TimeParticleConfiguration();
            startConfig.Year = new TimeRelatedColumnInfo() { Column = dataSetColumnEntities[0] };
            startConfig.Period = new PeriodTimeRelatedColumnInfo() { Column = dataSetColumnEntities[0] };
            startConfig.Format = DisseminationDatabaseTimeFormat.Iso8601Compatible;
            var endConfig = new TimeParticleConfiguration();
            endConfig.Year = new TimeRelatedColumnInfo() { Column = dataSetColumnEntities[1] };
            endConfig.Period = new PeriodTimeRelatedColumnInfo() { Column = dataSetColumnEntities[1] };
            endConfig.Format = DisseminationDatabaseTimeFormat.Iso8601Compatible;
            timeFormatConfiguration.SetConfiguration(startConfig, endConfig);
            return timeFormatConfiguration;
        }

        private void AddConcept(IConceptSchemeMutableObject cs, string id)
        {
            var c = new ConceptMutableCore() { Id = id };
            c.AddName("en", "Name of $id");
            cs.AddItem(c);
        }

        private MappingSetEntity AddSourceMappingSet(IDataflowObject dataflowObject, string datasetEntityId)
        {
            var sourceUrn = dataflowObject.Urn.ToString();
            MappingSetEntity mappingSetEntity = new MappingSetEntity();
            mappingSetEntity.StoreId = StoreId;
            mappingSetEntity.DataSetId = datasetEntityId;
            mappingSetEntity.Name = "Test Mapping Set";
            mappingSetEntity.ParentId = sourceUrn;
            mappingSetEntity.Description = "a description";

            var addedMappingSet = _entityPersistenceManager.Add(mappingSetEntity);

            var datasetColumns = _entityRetrieverManager.GetEntities<DataSetColumnEntity>(
                StoreId,
                datasetEntityId.QueryForThisParentId(),
                Detail.Full).ToArray();

            Assert.That(dataflowObject, Is.Not.Null, sourceUrn);

            var dsd = GetMutableArtefats<IDataStructureMutableObject>(StoreId, dataflowObject.DataStructureRef).First().ImmutableInstance;

            foreach (var component in dsd.Components)
            {
                ComponentMappingEntity componentMapping = new ComponentMappingEntity();
                componentMapping.StoreId = StoreId;
                componentMapping.ParentId = addedMappingSet.EntityId;
                componentMapping.Type = ComponentMappingType.Normal.AsMappingStoreType();

                ICodelistObject codelist = null;
                if (component.HasCodedRepresentation())
                {
                    codelist = GetMutableArtefats<ICodelistMutableObject>(StoreId, component.Representation.Representation).First().ImmutableInstance;
                }

                var dataAttribute = component as IAttributeObject;
                var dimension = component as IDimension;
                var dataSetColumnEntities = datasetColumns.Where(entity => entity.Name.Contains("_" + component.Id)).ToArray();
                if ((dimension == null || !dimension.TimeDimension))
                {
                    // no transcoding yet
                    dataSetColumnEntities = dataSetColumnEntities.Where(entity => entity.Name.EndsWith(component.Id)).Take(1).ToArray();
                }

                componentMapping.SetColumns(dataSetColumnEntities);

                componentMapping.Component = new Component { ObjectId = component.Id };

                if (dataSetColumnEntities.Length == 0)
                {
                    if (dataAttribute == null || dataAttribute.Mandatory)
                    {
                        componentMapping.ConstantValue = codelist != null ? codelist.Items.First().Id : "Test value";
                    }
                }

                var addedComponentEntity = _entityPersistenceManager.Add(componentMapping);

                if (dataSetColumnEntities.Length > 1)
                {
                    TranscodingEntity transcodingEntity = new TranscodingEntity();
                    transcodingEntity.ParentId = addedComponentEntity.EntityId;
                    transcodingEntity.StoreId = addedComponentEntity.StoreId;
                    if (dimension != null && dimension.TimeDimension)
                    {
                    }
                    else if (codelist != null)
                    {
                        // TODO test script
                        var addedTranscoding = _entityPersistenceManager.Add(transcodingEntity);
                        List<TranscodingRuleEntity> rules = new List<TranscodingRuleEntity>();
                        foreach (var item in codelist.Items)
                        {
                            TranscodingRuleEntity transcodingRule = new TranscodingRuleEntity();
                            transcodingRule.ParentId = addedTranscoding.EntityId;
                            transcodingRule.StoreId = addedTranscoding.StoreId;

                            transcodingRule.DsdCodeEntity = new IdentifiableEntity(EntityType.Sdmx)
                            {
                                ObjectId = item.Id
                            };
                            transcodingRule.LocalCodes = new List<LocalCodeEntity>();
                            for (int index = 0; index < dataSetColumnEntities.Length; index++)
                            {
                                var dataSetColumnEntity = dataSetColumnEntities[index];
                                var localCodeEntity = new LocalCodeEntity
                                {
                                    ObjectId =
                                                                  string.Format(
                                                                      CultureInfo.InvariantCulture,
                                                                      "{0}_{1}_{2}",
                                                                      dataSetColumnEntity.Name,
                                                                      item.Id,
                                                                      index),
                                    ParentId = dataSetColumnEntity.EntityId
                                };
                                transcodingRule.LocalCodes.Add(localCodeEntity);
                            }

                            rules.Add(transcodingRule);
                        }

                        _entityPersistenceManager.AddEntities(rules);
                    }
                    else
                    {
                        // TODO test script
                        var addedTranscoding = _entityPersistenceManager.Add(transcodingEntity);
                        List<TranscodingRuleEntity> rules = new List<TranscodingRuleEntity>();
                        var uncodedValues = new string[] { "uncoded 1", "uncoded 2" };
                        foreach (var uncodedValue in uncodedValues)
                        {
                            TranscodingRuleEntity transcodingRule = new TranscodingRuleEntity();
                            transcodingRule.ParentId = addedTranscoding.EntityId;
                            transcodingRule.StoreId = addedTranscoding.StoreId;
                            transcodingRule.UncodedValue = uncodedValue;
                            transcodingRule.LocalCodes = new List<LocalCodeEntity>();
                            for (int index = 0; index < dataSetColumnEntities.Length; index++)
                            {
                                var dataSetColumnEntity = dataSetColumnEntities[index];
                                var localCodeEntity = new LocalCodeEntity
                                {
                                    ObjectId =
                                                                  string.Format(
                                                                      CultureInfo.InvariantCulture,
                                                                      "{0}_{1}_{2}",
                                                                      dataSetColumnEntity.Name,
                                                                      uncodedValue,
                                                                      index),
                                    ParentId = dataSetColumnEntity.EntityId
                                };
                                transcodingRule.LocalCodes.Add(localCodeEntity);
                            }

                            rules.Add(transcodingRule);
                        }
                        _entityPersistenceManager.AddEntities(rules);
                    }
                }
            }

            return mappingSetEntity;
        }

        private IDataflowObject BuildStructuralMetadata()
        {
            IMutableObjects mutableObjects = new MutableObjectsImpl();
            IConceptSchemeMutableObject conceptScheme = new ConceptSchemeMutableCore();
            SetupMaintainable(conceptScheme, "TEST_CS");

            AddConcept(conceptScheme, "FREQ");
            AddConcept(conceptScheme, DimensionObject.TimeDimensionFixedId);
            AddConcept(conceptScheme, PrimaryMeasure.FixedId);

            mutableObjects.AddConceptScheme(conceptScheme);

            ICodelistMutableObject cl = new CodelistMutableCore();
            SetupMaintainable(cl, "CL_FREQ");

            var c1 = new CodeMutableCore() { Id = "A" };
            c1.AddName("en", "Annual");
            cl.AddItem(c1);
            var c2 = new CodeMutableCore() { Id = "M" };
            c2.AddName("en", "Monthly");
            cl.AddItem(c2);

            mutableObjects.AddCodelist(cl);

            IDataStructureMutableObject dsd = new DataStructureMutableCore();
            SetupMaintainable(dsd, "TEST_DSD");
            dsd.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "FREQ"),
                new StructureReferenceImpl("TEST", "CL_FREQ", "1.0", SdmxStructureEnumType.CodeList));

            IDimensionMutableObject timeDimension = new DimensionMutableCore();
            timeDimension.ConceptRef = new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, DimensionObject.TimeDimensionFixedId);
            timeDimension.TimeDimension = true;
            dsd.AddDimension(timeDimension);

            dsd.AddPrimaryMeasure(
                new StructureReferenceImpl(
                    "TEST",
                    "TEST_CS",
                    "1.0",
                    SdmxStructureEnumType.Concept,
                    PrimaryMeasure.FixedId));
            mutableObjects.AddDataStructure(dsd);

            _dataStructureObject = dsd.ImmutableInstance;
            var dataflow = new DataflowMutableCore(_dataStructureObject);
            mutableObjects.AddDataflow(dataflow);
            var artefactImportStatuses = new List<ArtefactImportStatus>();
            var structurePersistenceManager =
                new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(
                    _connectionStringSettings,
                    artefactImportStatuses);

            structurePersistenceManager.SaveStructures(mutableObjects.ImmutableObjects);
            Assert.That(
                artefactImportStatuses.All(status => status.ImportMessage.Status != ImportMessageStatus.Error),
                string.Join("\n", artefactImportStatuses.Select(status => status.ImportMessage.Message)));

            return dataflow.ImmutableInstance;
        }

        private void ReBuildMappingStore()
        {
            try
            {
                InitializeMappingStore(this.StoreId);
                _dataflowObject = BuildStructuralMetadata();
                var connection = AddADdbConnection();
                var dataset = AddDataSet(connection, _dataflowObject.DataStructureRef, 0);
                AddSourceMappingSet(_dataflowObject, dataset.EntityId);
            }
            catch (Exception e)
            {
                _log.Error(e);
                Console.WriteLine(e);
                throw;
            }
        }
        private void SetupMaintainable(IMaintainableMutableObject maintainable, string id)
        {
            maintainable.Version = "1.0";
            maintainable.AgencyId = "TEST";
            maintainable.Id = id;
            maintainable.AddName("en", $"Test {id}");
            maintainable.FinalStructure = TertiaryBool.ParseBoolean(true);
        }
    }
}