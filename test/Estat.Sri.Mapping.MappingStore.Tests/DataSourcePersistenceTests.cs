using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NSubstitute;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class DataSourcePersistenceTests : BaseTestClassWithContainer
    {
        private readonly IEntityPersistenceManager _persistManager;
        private readonly IEntityRetrieverEngine<DataSourceEntity> _dataDourceRetrieverEngine;
        private readonly MappingStoreRetrievalManager _mappingStoreRetrivalManager;
        private readonly string _connectionName;

        public DataSourcePersistenceTests(string name) : base(name)
        {
            this._connectionName = name;
            this._persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            var connectionStringHelper = new ConnectionStringRetriever();
            var connectionStringSettings = connectionStringHelper.GetConnectionStringSettings(this._connectionName);
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { connectionStringSettings });
            DatabaseManager databaseManager = new DatabaseManager(configurationStoreManager);
            var retrieverManager = new EntityRetrieverManager(new DataSourceRetrieverFactory(databaseManager));
            this._dataDourceRetrieverEngine = retrieverManager.GetRetrieverEngine<DataSourceEntity>(this._connectionName);
            this._mappingStoreRetrivalManager = new MappingStoreRetrievalManager(new RetrievalEngineContainer(databaseManager.GetDatabase(this._connectionName)));
        }

        [Test]
        public void PerformCRUDOperations()
        {
            var initialDataUrl = "http://url_to_data_or_rest_or_soap";
            var newDataUrl = "http://new_url_to_data_or_rest_or_soap";
            var initialParentId = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:157_80(1.0)";
            var newParentId = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=TEST_AGENCY:NA_MAIN_GROUP(1.9)";
            DataSourceEntity dataSourceEntity = new DataSourceEntity()
            {
                DataUrl = initialDataUrl,
                WSDLUrl = "http://url_to_wsdl_or_null",
                WADLUrl = "http://url_to_wadl_or_null",
                IsWs = true,
                IsRest = false,
                IsSimple = false,
                ParentId = initialParentId,
                StoreId = this._connectionName
            };

            //insert a DataSource
            var entity = _persistManager.Add(dataSourceEntity);

            IEntityQuery query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.Exact, entity.EntityId.ToString()));
            var result = this._dataDourceRetrieverEngine.GetEntities(query, Detail.Full);

            Assert.That(result.First().ParentId, Is.EqualTo(initialParentId));

            //dataSourceEntity.ParentId = newParentId;
            //_persistManager.Add(dataSourceEntity);
            //query.EntityId.Returns(new Criteria<string>(OperatorType.Exact, entity.EntityId.ToString()));
            //result = this._dataDourceRetrieverEngine.GetEntities(query, Detail.Full);
            //Assert.That(result.First().ParentId, Is.EqualTo(newParentId));

            dataSourceEntity.DataUrl = newDataUrl;
            dataSourceEntity.ParentId = newParentId;
            _persistManager.Update(dataSourceEntity);
            result = this._dataDourceRetrieverEngine.GetEntities(query, Detail.Full);
            Assert.That(result.First().DataUrl, Is.EqualTo(newDataUrl));
            //Assert.That(result.First().ParentId, Is.EqualTo(newParentId));

            _persistManager.Delete<DataSourceEntity>(_connectionName, entity.EntityId);
        }
    }
}