// -----------------------------------------------------------------------
// <copyright file="TranscodingRuleIntegrationTests.cs" company="EUROSTAT">
//   Date Created : 2022-11-11
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Tests.Madb7
{
    using System.IO;
    using DryIoc;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Engine.Streaming;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Builder;
    using System;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Model.Error;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.XmlHelper;
    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sdmxsource.Extension.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Estat.Sri.Mapping.Api.Extension;
    using FluentAssertions;

    /// <summary>
    /// Tests the component mappings and transcoding rules in the msdb 7.0 schema.
    /// </summary>
    [TestFixture("msdb_scratch.sqlserver")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.oracle")]
    public class UpdateDataflowWithMappingSetIT : BaseTestClassWithContainer
    {
        #region test setup

        private readonly IReadableDataLocationFactory _readableDataLocationFactory = new ReadableDataLocationFactory();
        private readonly IStructureParsingManager _parsingManager = new StructureParsingManager();
        private readonly IEntityPersistenceManager _entityPersistenceManager;
        private readonly IRetrievalEngineContainer _sdmxRetriever;
        private readonly IEntityAuthorizationManager _entityAuthorizationManager;
        private readonly IEntityRetrieverManager _entityRetrieverManager;
        private readonly SdmxAuthorizationScope _authorizationScope;
        private readonly IStructureSubmitterEngine _submitterEngine;

        private long _mappingSetPK;
        private long datasetPK;

        public UpdateDataflowWithMappingSetIT(string storeId)
            : base(storeId)
        {
            _entityAuthorizationManager = IoCContainer.Resolve<IEntityAuthorizationManager>();
            _entityRetrieverManager = IoCContainer.Resolve<IEntityRetrieverManager>();
            _authorizationScope = new SdmxAuthorizationScope(null);
            var submitFactory = new StructureSubmitMappingStoreFactory((storeId) => GetConnectionStringSettings());
            _submitterEngine = submitFactory.GetEngine(storeId);
            _entityPersistenceManager = IoCContainer.Resolve<IEntityPersistenceManager>();
            _sdmxRetriever = IoCContainer.Resolve<IRetrievalEngineContainerFactory>().GetRetrievalEngineContainer(new MappingStoreRetrieval.Manager.Database(GetConnectionStringSettings()));
        }

        [OneTimeSetUp]
        public void SetUp()
        {
            InitializeMappingStore(StoreId);

            // add structures
            string structuresFile = @"tests/test-sdmxv3.0.0-measures-array-ESTAT+STS+2.0-DF-DSD-full.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = _submitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // add mapping entities
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(connectionStringManager), null);
            var ddbConnectionEntity = new DdbConnectionEntity()
            {
                Name = "STS 2.0 connection",
                Description = "DDB connection for new ESTAT.STS.2.0 datastructure",
                DbType = "sqlserver",
                DbUser = "ddb",
                Password = "123",
                AdoConnString = "Data Source=test-ddb-mssql;Initial Catalog=DDB_B_TEST_JANUARY2014;Integrated Security=False;User ID=ddb;Password=123"
            };
            long ddbConnectionPK = entityPeristenceFactory.GetEngine<DdbConnectionEntity>(StoreId).Add(ddbConnectionEntity);
            Assert.NotNull(ddbConnectionPK);

            var datasetEntity = new DatasetEntity()
            {
                Name = "STS 2.0 dataset",
                Description = "Dataset for new ESTAT.STS.2.0 datastructure",
                Query =
                    "select [ESA_MAIN_TOT_ESTAT_101_FREQ], [ESA_MAIOT_ESTAT_101_REF_PERIOD], [ESA_MAIN_TOT_ESTAT_101_STO], [ESA_MAI_TOT_ESTAT_101_ACTIVITY]," +
                    " [ESA_MAIT_ESTAT_101_CONF_STATUS], [ESA_MAITOT_ESTAT_101_OBS_VALUE], [ESA_MAIOT_ESTAT_101_OBS_STATUS]" +
                    " from ESA_MAIN_TOT_ESTAT_10",
                EditorType = "org.estat.ma.gui.userControls.dataset.QueryEditor.CustomQueryEditor, MappingAssistant",
                ParentId = ddbConnectionPK.ToString()
            };
            datasetPK = entityPeristenceFactory.GetEngine<DatasetEntity>(StoreId).Add(datasetEntity);
            Assert.NotNull(datasetPK);

            string dataflowUrn = importResults
                .First(r => r.StructureReference.MaintainableStructureEnumType == SdmxStructureEnumType.Dataflow)
                .StructureReference.MaintainableUrn.AbsoluteUri;
            var mappingSetEntity = new MappingSetEntity()
            {
                Name = "STS 2.0 mapping set",
                Description = "Mapping set for new ESTAT.STS.2.0 datastructure",
                DataSetId = datasetPK.ToString(),
                ParentId = dataflowUrn
            };
            _mappingSetPK = entityPeristenceFactory.GetEngine<MappingSetEntity>(StoreId).Add(mappingSetEntity);
            Assert.NotNull(_mappingSetPK);
        }

        private ISdmxObjects ReadStructures(string filepath, SdmxSchemaEnumType sdmxSchema)
        {
            FileInfo fileInfo = new FileInfo(filepath);
            using (IReadableDataLocation rdl = _readableDataLocationFactory.GetReadableDataLocation(fileInfo))
            {
                IStructureWorkspace workspace = _parsingManager.ParseStructures(rdl);

                ISdmxObjects structureBeans = workspace.GetStructureObjects(false);

                XMLParser.ValidateXml(rdl, sdmxSchema);

                return structureBeans;
            }
        }

        #endregion

        [Test]
        public void UpdateDataflow()
        {
            IDataflowMutableObject dataflow = new DataflowMutableCore();
            dataflow.Id = "DF_TEST_UPDATE";
            dataflow.AgencyId = "TEST";
            dataflow.Version = "1.0.0-draft";
            dataflow.AddName("en", "Test 123");

            ICommonStructureQuery query = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV3StructureDocument).SetMaintainableTarget(SdmxStructureEnumType.Dsd).SetVersionRequests(new VersionRequestCore("~")).Build();
            var dsd = _sdmxRetriever.DSDRetrievalEngine.Retrieve(query).First().ImmutableInstance;
            dataflow.DataStructureRef = dsd.AsReference;
            ISdmxObjects objects = new SdmxObjectsImpl();
            IDataflowObject dataflowImmutable = dataflow.ImmutableInstance;
            objects.AddDataflow(dataflowImmutable);
            objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Replace);
            _submitterEngine.SubmitStructures(objects);


          var mappingSetEntity = new MappingSetEntity()
            {
                Name = "Test deleteOther",
                Description = "Mapping set for new ESTAT.STS.2.0 datastructure",
                DataSetId = datasetPK.ToString(),
                ParentId = dataflowImmutable.Urn.ToString(),
                StoreId = StoreId
            };
            MappingSetEntity mappingSetEntity1 = _entityPersistenceManager.Add(mappingSetEntity);
            Assert.That(mappingSetEntity1, Is.Not.Null);

            var result = _entityRetrieverManager.GetEntities<MappingSetEntity>(StoreId, mappingSetEntity1.QueryForThisEntityId(), Detail.Full).SingleOrDefault();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.ParentId, Is.Not.Null.And.Not.Empty);

            objects.RemoveDataflow(dataflowImmutable);
            dataflow.AddAnnotation(new AnnotationMutableCore() { Type = "Test" });
            objects.AddDataflow(dataflow.ImmutableInstance);
            _submitterEngine.SubmitStructures(objects);

            result = _entityRetrieverManager.GetEntities<MappingSetEntity>(StoreId, mappingSetEntity1.QueryForThisEntityId(), Detail.Full).SingleOrDefault();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.ParentId, Is.Not.Null.And.Not.Empty);

            // clean up
            _entityPersistenceManager.Delete<MappingSetEntity>(StoreId, mappingSetEntity1.EntityId);
            objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Delete);
            _submitterEngine.SubmitStructures(objects);
        }
        [Test]
        public void DeleteDataflowWithMappingSet()
        {
            IDataflowMutableObject dataflow = new DataflowMutableCore();
            dataflow.Id = "DF_TEST_UPDATE";
            dataflow.AgencyId = "TEST";
            dataflow.Version = "1.0.0-draft";
            dataflow.AddName("en", "Test 123");

            ICommonStructureQuery query = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV3StructureDocument).SetMaintainableTarget(SdmxStructureEnumType.Dsd).SetVersionRequests(new VersionRequestCore("~")).Build();
            var dsd = _sdmxRetriever.DSDRetrievalEngine.Retrieve(query).First().ImmutableInstance;
            dataflow.DataStructureRef = dsd.AsReference;
            ISdmxObjects objects = new SdmxObjectsImpl();
            IDataflowObject dataflowImmutable = dataflow.ImmutableInstance;
            objects.AddDataflow(dataflowImmutable);
            objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Replace);
            _submitterEngine.SubmitStructures(objects);


          var mappingSetEntity = new MappingSetEntity()
            {
                Name = "Test DeleteDataflowWithMappingSet",
                Description = "Mapping set for new ESTAT.STS.2.0 datastructure",
                DataSetId = datasetPK.ToString(),
                ParentId = dataflowImmutable.Urn.ToString(),
                StoreId = StoreId
            };
            MappingSetEntity mappingSetEntity1 = _entityPersistenceManager.Add(mappingSetEntity);
            Assert.That(mappingSetEntity1, Is.Not.Null);

            var result = _entityRetrieverManager.GetEntities<MappingSetEntity>(StoreId, mappingSetEntity1.QueryForThisEntityId(), Detail.Full).SingleOrDefault();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.ParentId, Is.Not.Null.And.Not.Empty);
            
            objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Delete);
            var deleteResult = _submitterEngine.SubmitStructures(objects);
            Assert.That(deleteResult, Has.Count.EqualTo(1));
            Assert.That(deleteResult[0].Status, Is.EqualTo(ResponseStatus.Failure));
            
            result = _entityRetrieverManager.GetEntities<MappingSetEntity>(StoreId, mappingSetEntity1.QueryForThisEntityId(), Detail.Full).SingleOrDefault();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.ParentId, Is.Not.Null.And.Not.Empty);
            // clean up
            _entityPersistenceManager.Delete<MappingSetEntity>(StoreId, mappingSetEntity1.EntityId);
            objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Delete);
            _submitterEngine.SubmitStructures(objects);

        }

      [Test]
        public void DeleteDataflowWithDifferentVersion()
        {
            IDataflowMutableObject dataflow = new DataflowMutableCore();
            dataflow.Id = "DF_TEST_UPDATE";
            dataflow.AgencyId = "TEST";
            dataflow.Version = "1.0.0-draft";
            dataflow.AddName("en", "Test 123");

            ICommonStructureQuery query = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV3StructureDocument).SetMaintainableTarget(SdmxStructureEnumType.Dsd).SetVersionRequests(new VersionRequestCore("~")).Build();
            var dsd = _sdmxRetriever.DSDRetrievalEngine.Retrieve(query).First().ImmutableInstance;
            dataflow.DataStructureRef = dsd.AsReference;
            ISdmxObjects objects = new SdmxObjectsImpl();
            IDataflowObject dataflowImmutable = dataflow.ImmutableInstance;
            objects.AddDataflow(dataflowImmutable);
            objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Replace);
            _submitterEngine.SubmitStructures(objects);


          var mappingSetEntity = new MappingSetEntity()
            {
                Name = "Test DeleteDataflowWithDifferentVersion ",
                Description = "Mapping set for new ESTAT.STS.2.0 datastructure",
                DataSetId = datasetPK.ToString(),
                ParentId = dataflowImmutable.Urn.ToString(),
                StoreId = StoreId
            };
            MappingSetEntity mappingSetEntity1 = _entityPersistenceManager.Add(mappingSetEntity);
            Assert.That(mappingSetEntity1, Is.Not.Null);

            var result = _entityRetrieverManager.GetEntities<MappingSetEntity>(StoreId, mappingSetEntity1.QueryForThisEntityId(), Detail.Full).SingleOrDefault();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.ParentId, Is.Not.Null.And.Not.Empty);
            
            objects.RemoveDataflow(dataflowImmutable);
            objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Replace);
            dataflow.Version = "1.3.0-draft";
            objects.AddDataflow(dataflow.ImmutableInstance);
            _submitterEngine.SubmitStructures(objects);

            objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Delete);
            var deleteResult = _submitterEngine.SubmitStructures(objects);
            Assert.That(deleteResult, Has.Count.EqualTo(1));
            Assert.That(deleteResult[0].Status, Is.EqualTo(ResponseStatus.Success), "Messages {0}",
                string.Join(". ", deleteResult[0].Messages.SelectMany(m => m.Text).Select(t => t.Value)));
            
            result = _entityRetrieverManager.GetEntities<MappingSetEntity>(StoreId, mappingSetEntity1.QueryForThisEntityId(), Detail.Full).SingleOrDefault();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.ParentId, Is.Not.Null.And.Not.Empty);
            // clean up
            _entityPersistenceManager.Delete<MappingSetEntity>(StoreId, mappingSetEntity1.EntityId);
            objects.AddDataflow(dataflowImmutable);
            objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Delete);
            _submitterEngine.SubmitStructures(objects);

        }


        #region private methods

        #endregion
    }
}
