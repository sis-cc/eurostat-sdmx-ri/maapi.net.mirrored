using System.Linq;
using DryIoc;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using NUnit.Framework;
using Estat.Sri.Mapping.Api.Extension;
using Estat.Sri.Mapping.Api.Constant;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Estat.Sdmxsource.Extension.Engine;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using Estat.Sri.Mapping.Api.Engine;
using HeaderImpl = Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header.HeaderImpl;

namespace Estat.Sri.Mapping.MappingStore.Tests.Madb7
{
    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]

    internal class DataRetrieverCallTests  : BaseTestClassWithContainer
    {
        /// <summary>
        /// The persist manager
        /// </summary>
        private readonly IEntityPersistenceManager _persistManager;

        private readonly IEntityRetrieverManager _retrieveManager;
        private readonly IComponentMappingManager _componentMappingManager;
        private readonly IRetrievalEngineContainer _container;

        public DataRetrieverCallTests(string storeId) : base(storeId)
        {
            this._persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            this._retrieveManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            this._componentMappingManager = this.IoCContainer.Resolve<IComponentMappingManager>();
            this._container = new RetrievalEngineContainer(new MappingStoreRetrieval.Manager.Database(GetConnectionStringSettings()));
        }

        [Test]
        public void MappingSetEntityTestFromParent()
        {

            var dataflowUrn = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_A(1.0)";
            IEntityQuery entityQuery = dataflowUrn.QueryForThisParentId();

            //retrieve
            var result = _retrieveManager.GetEntities<MappingSetEntity>(this.StoreId, entityQuery, Detail.Full);
            Assert.IsTrue(result.Any());
            Assert.That(result.First().ParentId, Is.EqualTo(dataflowUrn));
            Assert.That(result.First().ValidFrom, Is.Null);
            Assert.That(result.First().ValidTo, Is.Null);
        }

        [Test]
        public void ComponentMappingManagerTest()
        {
            IDataflowObject dataflow = GetDataflow("SSTSCONS_PROD_A", "1.0", "ESTAT");
            IDataStructureObject dsd = GetDsd(dataflow);
            IComponentMappingContainer componentMappingContainer = _componentMappingManager.GetBuilders(this.StoreId, dataflow, dsd);
            Assert.NotNull(componentMappingContainer);
            Assert.NotNull(componentMappingContainer.DisseminationConnection);
            Assert.NotNull(componentMappingContainer.TimeDimensionMapping);
            Assert.NotNull(componentMappingContainer.TimeDimensionBuilder);
            Assert.NotNull(componentMappingContainer.ComponentMappingBuilders);
            CollectionAssert.IsNotEmpty(componentMappingContainer.ComponentMappingBuilders);
            CollectionAssert.IsNotEmpty(componentMappingContainer.ComponentMappings);
        }
        [Test]
        public void ComponentMappingManagerLatestPreductionTest()
        {
            using (var context = new DataflowFilterContext(MappingStoreRetrieval.Constants.DataflowFilter.Production))
            {
                IDataflowObject dataflow = GetDataflow("SSTSCONS_PROD_A", null, "ESTAT");
                IDataStructureObject dsd = GetDsd(dataflow);
                IComponentMappingContainer componentMappingContainer = _componentMappingManager.GetBuilders(this.StoreId, dataflow, dsd);
                Assert.NotNull(componentMappingContainer);
                Assert.NotNull(componentMappingContainer.DisseminationConnection);
                Assert.NotNull(componentMappingContainer.TimeDimensionMapping);
                Assert.NotNull(componentMappingContainer.TimeDimensionBuilder);
                Assert.NotNull(componentMappingContainer.ComponentMappingBuilders);
                CollectionAssert.IsNotEmpty(componentMappingContainer.ComponentMappingBuilders);
                CollectionAssert.IsNotEmpty(componentMappingContainer.ComponentMappings);
            }
        }
         [Test]
        public void ComponentMappingManagerUpdateStatus()
        {
            IDataflowObject dataflow = GetDataflow("STSCONS_UPDATE_STATUS_CONS", null, "ESTAT");
            IDataStructureObject dsd = GetDsd(dataflow);
            IComponentMappingContainer componentMappingContainer = _componentMappingManager.GetBuilders(this.StoreId, dataflow, dsd);
            Assert.NotNull(componentMappingContainer);
            Assert.NotNull(componentMappingContainer.DisseminationConnection);
            Assert.NotNull(componentMappingContainer.TimeDimensionMapping);
            Assert.NotNull(componentMappingContainer.TimeDimensionBuilder);
            Assert.NotNull(componentMappingContainer.UpdateStatusMapping);
            Assert.NotNull(componentMappingContainer.UpdateStatusMappingBuilder);
            Assert.NotNull(componentMappingContainer.ComponentMappingBuilders);
            CollectionAssert.IsNotEmpty(componentMappingContainer.ComponentMappingBuilders);
            CollectionAssert.IsNotEmpty(componentMappingContainer.ComponentMappings);
        }


        [Test]
        public void ComponentMappingManagerTestMultiColumnTranscoding()
        {
            // ESTAT,SSTSIND_PROD_M
            IDataflowObject dataflow = GetDataflow("SSTSIND_PROD_M", "2.0", "ESTAT");
            IDataStructureObject dsd = GetDsd(dataflow);
            IComponentMappingContainer componentMappingContainer = _componentMappingManager.GetBuilders(this.StoreId, dataflow, dsd);
            Assert.NotNull(componentMappingContainer);
            Assert.NotNull(componentMappingContainer.DisseminationConnection);
            Assert.NotNull(componentMappingContainer.TimeDimensionMapping);
            Assert.NotNull(componentMappingContainer.TimeDimensionBuilder);
            Assert.NotNull(componentMappingContainer.ComponentMappingBuilders);
            CollectionAssert.IsNotEmpty(componentMappingContainer.ComponentMappingBuilders);
            CollectionAssert.IsNotEmpty(componentMappingContainer.ComponentMappings);
        }

        [Test]
        public void ComponentMappingManagerTestTimeMapping()
        {
            // ESTAT,SSTSIND_PROD_M
            IDataflowObject dataflow = GetDataflow("SSTSCONS_PROD_QT", "2.0", "ESTAT");
            IDataStructureObject dsd = GetDsd(dataflow);
            IComponentMappingContainer componentMappingContainer = _componentMappingManager.GetBuilders(this.StoreId, dataflow, dsd);
            Assert.NotNull(componentMappingContainer);
            Assert.NotNull(componentMappingContainer.DisseminationConnection);
            Assert.NotNull(componentMappingContainer.TimeDimensionMapping);
            Assert.NotNull(componentMappingContainer.TimeDimensionBuilder);
            Assert.NotNull(componentMappingContainer.TimeDimensionMapping);
            Assert.NotNull(componentMappingContainer.TimeDimensionMapping.Transcoding);
            Assert.NotNull(componentMappingContainer.ComponentMappingBuilders);
            CollectionAssert.IsNotEmpty(componentMappingContainer.ComponentMappingBuilders);
            CollectionAssert.IsNotEmpty(componentMappingContainer.ComponentMappings);
            CollectionAssert.IsNotEmpty(componentMappingContainer.TimeDimensionMapping.Transcoding.TimeFormatConfigurations);
            Assert.NotNull(componentMappingContainer.TimeDimensionMapping.Transcoding.TimeFormatConfigurations["Q"].StartConfig);
            Assert.NotNull(componentMappingContainer.TimeDimensionMapping.Transcoding.TimeFormatConfigurations["Q"].StartConfig.Period);
            Assert.That(componentMappingContainer.TimeDimensionMapping.Transcoding.TimeFormatConfigurations["Q"].StartConfig.Period.Length.HasValue);
            Assert.NotZero(componentMappingContainer.TimeDimensionMapping.Transcoding.TimeFormatConfigurations["Q"].StartConfig.Period.Length.Value);
        }

        [Test]
        public void ComponentMappingManagerTestLastUpdated()
        {
            // ESTAT,SSTSIND_PROD_M
            IDataflowObject dataflow = GetDataflow("NAMAIN_IDC_N", null, "ESTAT");
            IDataStructureObject dsd = GetDsd(dataflow);
            IComponentMappingContainer componentMappingContainer = _componentMappingManager.GetBuilders(this.StoreId, dataflow, dsd);
            Assert.NotNull(componentMappingContainer);
            Assert.NotNull(componentMappingContainer.DisseminationConnection);
            Assert.NotNull(componentMappingContainer.TimeDimensionMapping);
            Assert.NotNull(componentMappingContainer.TimeDimensionBuilder);
            Assert.NotNull(componentMappingContainer.ComponentMappingBuilders);
            Assert.NotNull(componentMappingContainer.LastUpdateMapping);
            Assert.NotNull(componentMappingContainer.LastUpdateMappingBuilder);
            Assert.NotNull(componentMappingContainer.ValidToMapping);
            Assert.NotNull(componentMappingContainer.ValidToMappingBuilder);
            CollectionAssert.IsNotEmpty(componentMappingContainer.ComponentMappingBuilders);
            CollectionAssert.IsNotEmpty(componentMappingContainer.ComponentMappings);
        }

        [Test]
        public void GetHeaderForDataflowTest()
        {
            IDataflowObject dataflow = GetDataflow("SSTSCONS_PROD_A", "1.0", "ESTAT");
            HeaderEntity headerEntity = new HeaderEntity();
            headerEntity.SdmxHeader = new HeaderImpl("IREF0000001", "ZZ9");
            headerEntity.SdmxHeader.Test = true;
            headerEntity.SdmxHeader.AddSource(new TextTypeWrapperImpl("en", "test source", null));
            headerEntity.ParentId = dataflow.Urn.ToString();
            headerEntity.StoreId = this.StoreId;
            HeaderEntity headerEntity1 = _persistManager.Add(headerEntity);

            Assert.NotNull(headerEntity1);
            IHeaderRetrieverEngine headerRetrieverEngine = _container.HeaderRetrieverEngine;
            IHeader header = headerRetrieverEngine.GetHeader(dataflow, null, null);
            Assert.IsNotNull(header);

        }

        private IDataStructureObject GetDsd(IDataflowObject dataflow)
        {
            var dsdRef = dataflow.DataStructureRef;
            var builder = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.REST, StructureOutputFormatEnumType.SdmxV21StructureDocument);
            builder.SetAgencyIds(dsdRef.AgencyId);
            builder.SetMaintainableIds(dsdRef.MaintainableId);
            builder.SetVersionRequests(new VersionRequestCore(dsdRef.Version));
            builder.SetMaintainableTarget(SdmxStructureEnumType.Dsd);
            var query = builder.Build();

            IDataStructureObject dsd = (IDataStructureObject)_container.GetEngine(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd)).Retrieve(query).First().ImmutableInstance;
            return dsd;
        }

        private IDataflowObject GetDataflow(string maintainableIds, string version, string agencyIds)
        {
            var builder = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.REST, StructureOutputFormatEnumType.SdmxV21StructureDocument);
            if (agencyIds != null)
            {
                builder.SetAgencyIds(agencyIds);
            }
            builder.SetMaintainableIds(maintainableIds);
            builder.SetVersionRequests(new VersionRequestCore(version));
            builder.SetMaintainableTarget(SdmxStructureEnumType.Dataflow);
            var query = builder.Build();
            IDataflowObject dataflow = (IDataflowObject)_container.GetEngine(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)).Retrieve(query).First().ImmutableInstance;
            return dataflow;
        }
    }
}
