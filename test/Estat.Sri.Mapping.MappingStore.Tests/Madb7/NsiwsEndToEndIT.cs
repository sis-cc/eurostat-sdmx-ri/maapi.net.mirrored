using System;
using System.IO;
using System.Linq;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Extension;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Estat.Sri.Mapping.Api.Utils;

namespace Estat.Sri.Mapping.MappingStore.Tests.Madb7
{
    [TestFixture("msdb_scratch.sqlserver")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.oracle")]
    internal class NsiwsEndToEndIT : BaseTestClassWithContainer
    {
        private readonly IEntityPersistenceManager _persistManager;

        private readonly IEntityRetrieverManager _retrieveManager;

        public NsiwsEndToEndIT(string storeId) : base(storeId)
        {
            this.InitializeMappingStore(storeId);
            _persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            _retrieveManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
        }


        [TestCase("tests/end-to-end/nsiws-new.json")]
        public void ShouldCreateNsiws(string jsonFile)
        {
            FileInfo source = new FileInfo(jsonFile);
            FileInfo response = new FileInfo(jsonFile + ".response.json");
            SaveToMappingStoreAndJson(source, response);

            var expectedEntities = ReadEntities<NsiwsEntity>(source, EntityType.Nsiws).ToDictionary(k => k.Name);

            var mappingStoreEntities = _retrieveManager.GetEntities<NsiwsEntity>(StoreId, EntityQuery.Empty, Detail.Full).ToDictionary(k => k.Name);

            Assert.AreEqual(expectedEntities.Count, mappingStoreEntities.Count);
            CollectionAssert.AreEquivalent(expectedEntities.Keys, mappingStoreEntities.Keys);

            foreach (var expected in expectedEntities)
            {
                var result = mappingStoreEntities[expected.Key];
                // first we delete since we don't need it in the db anymore
                _persistManager.Delete<NsiwsEntity>(StoreId, result.EntityId);
                Assert.AreEqual(expected.Value.Name, result.Name);
                if (!string.IsNullOrEmpty(expected.Value.Description))
                {
                    Assert.AreEqual(expected.Value.Description, result.Description);
                }
                else
                {
                    Assert.That(result.Description, Is.Null.Or.Empty);
                }
                Assert.AreEqual(expected.Value.Url, result.Url);
                Assert.AreEqual(expected.Value.Technology, result.Technology);
                if (!string.IsNullOrEmpty(expected.Value.UserName))
                {
                    Assert.AreEqual(expected.Value.UserName, result.UserName);
                }
                else
                {
                    Assert.That(result.UserName, Is.Null.Or.Empty);
                }

                Assert.AreEqual(expected.Value.Proxy, result.Proxy);
            }
        }
        [TestCase("tests/end-to-end/nsiws-new.json")]
        public void ShouldDeleteNsiws(string jsonFile)
        {
            FileInfo source = new FileInfo(jsonFile);
            FileInfo response = new FileInfo(jsonFile + ".response.json");
            SaveToMappingStoreAndJson(source, response);

            var expectedEntities = ReadEntities<NsiwsEntity>(source, EntityType.Nsiws).ToDictionary(k => k.Name);

            var mappingStoreEntities = _retrieveManager.GetEntities<NsiwsEntity>(StoreId, EntityQuery.Empty, Detail.Full).ToDictionary(k => k.Name);

            Assert.AreEqual(expectedEntities.Count, mappingStoreEntities.Count);
            CollectionAssert.AreEquivalent(expectedEntities.Keys, mappingStoreEntities.Keys);

            foreach (var expected in expectedEntities)
            {
                var result = mappingStoreEntities[expected.Key];
                // first we delete since we don't need it in the db anymore
                _persistManager.Delete<NsiwsEntity>(StoreId, result.EntityId);
                Assert.AreEqual(expected.Value.Name, result.Name);
                Assert.Throws<SdmxNoResultsException> ( () => _retrieveManager.GetEntities<NsiwsEntity>(StoreId, expected.Key.QueryForThisEntityId(), Detail.Full) );
            }
        }

        [TestCase("tests/end-to-end/nsiws-new.json", "tests/end-to-end/nsiws-patch.json")]
        public void ShouldUpdateNsiWS(string jsonFile, string patchFile)
        {
            // change parent id of nsiws
            FileInfo source = new FileInfo(jsonFile);

            FileInfo response = new FileInfo(jsonFile + ".response.json");
            SaveToMappingStoreAndJson(source, response);

            var expectedEntities = ReadEntities<NsiwsEntity>(source, EntityType.Nsiws).ToList();

            // This test expects one and only one nsiws
            Assume.That(expectedEntities, Has.Count.EqualTo(1));

            var expected = expectedEntities.First();

            var nsiwsFromMappingStore = _retrieveManager.GetEntities<NsiwsEntity>(StoreId, EntityQuery.Empty, Detail.Full).FirstOrDefault(x => x.Name.Equals(expected.Name, StringComparison.Ordinal));
            Assume.That(nsiwsFromMappingStore, Is.Not.Null);


            // environment set
            PatchRequest patchRequest = PatchRequestParser.ParsePatch(patchFile);
            var dataSetEngine = _persistManager.GetEngine<NsiwsEntity>(StoreId);
            dataSetEngine.Update(patchRequest, EntityType.Nsiws, nsiwsFromMappingStore.EntityId);
            var updatedNsiWsFromMappingStore = _retrieveManager.GetEntities<NsiwsEntity>(StoreId, nsiwsFromMappingStore.QueryForThisEntityId(), Detail.Full).FirstOrDefault();
            // Delete the nsiws, it should delete also the nsiws columns
            _persistManager.Delete<NsiwsEntity>(StoreId, nsiwsFromMappingStore.EntityId);

            Assert.NotNull(updatedNsiWsFromMappingStore);

            Assert.AreEqual(updatedNsiWsFromMappingStore.Name, patchRequest.FirstOrDefault(p => "/name".Equals(p.Path, StringComparison.OrdinalIgnoreCase))?.Value?.ToString());
        }
    }
}
