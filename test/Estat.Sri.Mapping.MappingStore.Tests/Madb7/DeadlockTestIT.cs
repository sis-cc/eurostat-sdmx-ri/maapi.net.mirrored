using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine.Streaming;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStore.Store;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Util.Extension;
using System.Text.RegularExpressions;
using Estat.Sri.Mapping.Api.Extension;
using Newtonsoft.Json.Linq;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.BouncyCastle.Asn1.Mozilla;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Castle.Core.Logging;
using NSubstitute.Extensions;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Estat.Sri.MappingStoreRetrieval.Factory;

namespace Estat.Sri.Mapping.MappingStore.Tests.Madb7
{
    [TestFixture("msdb_scratch.sqlserver")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.oracle")]

    internal class DeadlockTestIT : BaseTestClassWithContainer
    {
        private readonly IEntityPersistenceManager _persistManager;

        private readonly IEntityRetrieverManager _retrieveManager;
        private readonly IRetrievalEngineContainer _retrieverFactory;
        private readonly ISdmxObjects _sdmxObjects;

        public DeadlockTestIT(string storeId) : base(storeId)
        {
            this.InitializeMappingStore(storeId);
            _persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            _retrieveManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            IStructureParsingManager structureParsingManager = this.IoCContainer.Resolve<IStructureParsingManager>();
            ISdmxObjects sdmxObjects = new FileInfo("tests/end-to-end/SSTSCONS_PROD_M_TUTORIAL.xml").GetSdmxObjects(structureParsingManager);
            sdmxObjects.Merge(new FileInfo("tests/end-to-end/CENSUSHUB_2021.xml").GetSdmxObjects(structureParsingManager));
            sdmxObjects.Merge(new FileInfo("tests/end-to-end/CENSUSHUB_Q_XS1.xml").GetSdmxObjects(structureParsingManager));
            var artefactImportStatuses = new List<ArtefactImportStatus>();
            var structurePersistenceManager =
                new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(
                    this.GetConnectionStringSettings(),
                    artefactImportStatuses);
            _sdmxObjects = sdmxObjects;
            structurePersistenceManager.SaveStructures(sdmxObjects);
            Assert.That(
                artefactImportStatuses.All(status => status.ImportMessage.Status != ImportMessageStatus.Error),
                string.Join("\n", artefactImportStatuses.Select(status => status.ImportMessage.Message)));
            // custom SQL to test performance

            Database db = new Database(this.GetConnectionStringSettings());
            //db.ExecuteNonQuery("CREATE INDEX TEST_CHILDREN_IDX on DATASET_COLUMN(DS_ID)",null);
            //db.ExecuteNonQuery("CREATE INDEX TEST_DTS_IDX on N_DATASET(CONNECTION_ID)", null);
            //db.ExecuteNonQuery("CREATE INDEX TEST_MS_DTS_IDX on N_MAPPING_SET(SOURCE_DS)", null);
            //db.ExecuteNonQuery("CREATE INDEX TEST_M_MS_IDX on N_COMPONENT_MAPPING(STR_MAP_SET_ID)", null);


            this._retrieverFactory = this.IoCContainer.Resolve<IRetrievalEngineContainerFactory>().GetRetrievalEngineContainer(db);
        }

        [Parallelizable(ParallelScope.All)]
        [TestCase("tests/end-to-end/new-ddb-connection.json")]
        [TestCase("tests/end-to-end/new-ddb-connection-p1.json")]
        [TestCase("tests/end-to-end/new-ddb-connection-p2.json")]
        [TestCase("tests/end-to-end/new-ddb-connection-p3.json")]
        [TestCase("tests/end-to-end/new-ddb-connection-p4.json")]
        [TestCase("tests/end-to-end/new-ddb-connection-with-permissions.json")]
        public void CreateADDBConnection(string jsonFile)
        {
            FileInfo source = new FileInfo(jsonFile);
            FileInfo response = new FileInfo(jsonFile + "CreateADDBConnection" + ".response.json");
            SaveToMappingStoreAndJson(source, response);

            var expectedEntities =ReadEntities<IConnectionEntity>(source, EntityType.DdbConnectionSettings).ToDictionary(k => k.Name);

            var mappingStoreEntities = _retrieveManager.GetEntities<IConnectionEntity>(StoreId, EntityQuery.Empty, Detail.Full).Where(d => expectedEntities.ContainsKey(d.Name)).ToDictionary(k => k.Name);

            Assert.AreEqual(expectedEntities.Count, mappingStoreEntities.Count);
            CollectionAssert.AreEquivalent(expectedEntities.Keys, mappingStoreEntities.Keys);

            foreach(var expected in expectedEntities)
            {
                var result = mappingStoreEntities[expected.Key];
                // first we delete since we don't need it in the db anymore
                _persistManager.Delete<IConnectionEntity>(StoreId, result.EntityId);
                Assert.AreEqual(expected.Value.DatabaseVendorType, result.DatabaseVendorType);
                Assert.NotNull(result.DbName);
                Assert.AreEqual(expected.Value.Description, result.Description);
                CollectionAssert.AreEquivalent(expected.Value.Permissions.Keys, result.Permissions.Keys);

                // When returning from mapping store we get all settings from driver but from maweb only the settings that are modified
                var keysUsed = new HashSet<string>(result.Settings.Keys);
                keysUsed.IntersectWith(expected.Value.Settings.Keys);
                foreach(var key in keysUsed)
                {
                    Assert.AreEqual(expected.Value.Settings[key].Value, result.Settings[key].Value, "Not equal for {0}", key);
                }
            }
        }

        [Parallelizable(ParallelScope.All)]
        [TestCase("tests/end-to-end/dataset.json")]
        [TestCase("tests/end-to-end/dataset-p1.json")]
        [TestCase("tests/end-to-end/dataset-p2.json")]
        [TestCase("tests/end-to-end/dataset-p3.json")]
        [TestCase("tests/end-to-end/dataset-p4.json")]
        [TestCase("tests/end-to-end/dataset-p5.json")]
        public void CreateDataSet(string datasetFile)
        {
            var uuid = Guid.NewGuid().ToString();
            var ddbConnection = AddADdbConnection();
            // change parent id of dataset
            FileInfo source_with_parent = FixParentId(datasetFile, ddbConnection, uuid);
            FileInfo source = FixNameId(source_with_parent, uuid);

            FileInfo response = new FileInfo(datasetFile + Guid.NewGuid().ToString() + ".response.json");
            SaveToMappingStoreAndJson(source, response);

            var expectedEntities =ReadEntities<DatasetEntity>(source, EntityType.DataSet).ToDictionary(k => k.Name);
            
            var mappingStoreEntities  = _retrieveManager.GetEntities<DatasetEntity>(StoreId,EntityQuery.Empty , Detail.Full).Where(d => expectedEntities.ContainsKey(d.Name)).ToDictionary(k => k.Name);

            Assert.AreEqual(expectedEntities.Count, mappingStoreEntities.Count);
            CollectionAssert.AreEquivalent(expectedEntities.Keys, mappingStoreEntities.Keys);

            foreach (var expected in expectedEntities)
            {
                var result = mappingStoreEntities[expected.Key];
                // first we delete since we don't need it in the db anymore
                _persistManager.Delete<DatasetEntity>(StoreId, result.EntityId);
                Assert.AreEqual(expected.Value.ParentId, result.ParentId);
                Assert.AreEqual(expected.Value.Description, result.Description);
                Assert.AreEqual(expected.Value.Query, result.Query);
                Assert.AreEqual(expected.Value.JSONQuery, result.JSONQuery);
                CollectionAssert.AreEquivalent(expected.Value.Permissions.Keys, result.Permissions.Keys);
            }
            _persistManager.Delete<IConnectionEntity>(StoreId, ddbConnection.EntityId);
        }

        [Parallelizable(ParallelScope.All)]
        [TestCase("tests/end-to-end/dataset-p1.json", "tests/end-to-end/datasetColumns.json")]
        [TestCase("tests/end-to-end/dataset-p2.json", "tests/end-to-end/datasetColumns.json")]
        [TestCase("tests/end-to-end/dataset-p3.json", "tests/end-to-end/datasetColumns.json")]
        [TestCase("tests/end-to-end/dataset-p4.json", "tests/end-to-end/datasetColumns.json")]
        [TestCase("tests/end-to-end/dataset-p5.json", "tests/end-to-end/datasetColumns.json")]
        [TestCase("tests/end-to-end/dataset.json", "tests/end-to-end/datasetColumns.json")]
        public void CreateDataSetWithColumns(string datasetFile, string datasetColumnFile)
        {
            var uuid = Guid.NewGuid().ToString();
            var ddbConnection = AddADdbConnection();
            // change parent id of dataset
            FileInfo source_with_parent = FixParentId(datasetFile, ddbConnection, uuid);
            FileInfo source = FixNameId(source_with_parent, uuid);

            FileInfo response = new FileInfo(datasetFile + uuid + ".response.json");
            SaveToMappingStoreAndJson(source, response);

            var expectedEntities =ReadEntities<DatasetEntity>(source, EntityType.DataSet).ToList();
            
            // This test expects one and only one dataset
            Assume.That(expectedEntities, Has.Count.EqualTo(1));

            var expected = expectedEntities.First();

            var datasetFromMappingStore  = _retrieveManager.GetEntities<DatasetEntity>(StoreId, EntityQuery.Empty, Detail.Full).FirstOrDefault(x => x.Name.Equals(expected.Name, StringComparison.Ordinal));
            Assert.NotNull(datasetFromMappingStore);

            FileInfo sourceColumns = FixParentId(datasetColumnFile, datasetFromMappingStore, uuid);
            FileInfo responseColumns = new FileInfo(datasetColumnFile + uuid + ".response.json");
            SaveToMappingStoreAndJson(sourceColumns, responseColumns);


            var expectedColumns = ReadEntities<DataSetColumnEntity>(sourceColumns, EntityType.DataSetColumn).ToDictionary(k => k.Name);
            var mappingStoreColumns = _retrieveManager.GetEntities<DataSetColumnEntity>(StoreId, datasetFromMappingStore.QueryForThisParentId(), Detail.Full).ToDictionary(k => k.Name);

            // Delete the dataset, it should delete also the dataset columns
            _persistManager.Delete<DatasetEntity>(StoreId, datasetFromMappingStore.EntityId);
            _persistManager.Delete<IConnectionEntity>(StoreId, ddbConnection.EntityId);

            Assert.AreEqual(expectedColumns.Count, mappingStoreColumns.Count);
            CollectionAssert.AreEquivalent(expectedColumns.Keys, mappingStoreColumns.Keys);
        }

        [Parallelizable(ParallelScope.All)]
        [TestCase("tests/end-to-end/dataset-p1.json", "tests/end-to-end/datasetColumns.json", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:CENSUSHUB_Q_XS1(1.0)", null, null)]
        [TestCase("tests/end-to-end/dataset-p2.json", "tests/end-to-end/datasetColumns.json", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:CENSUSHUB_Q_XS1(1.0)", "2034-01-01", "2034-10-01")]
        [TestCase("tests/end-to-end/dataset-p3.json", "tests/end-to-end/datasetColumns.json", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_M_TUTORIAL(2.0)", null, null)]
        [TestCase("tests/end-to-end/dataset-p4.json", "tests/end-to-end/datasetColumns.json", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_M_TUTORIAL(2.0)", "2034-01-01", "2034-10-01")]
        [TestCase("tests/end-to-end/dataset-p5.json", "tests/end-to-end/datasetColumns.json", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_M_TUTORIAL(2.0)", "2035-01-01", null)]
        [TestCase("tests/end-to-end/dataset.json", "tests/end-to-end/datasetColumns.json", "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:CENSUSHUB_Q_XS1(1.0)", "2035-01-01", null)]
        public void CreateMappingSets(string datasetFile, string datasetColumnFile, string dataflowUrn, string validFrom, string validTo)
        {
            var uuid = Guid.NewGuid().ToString();
            var ddbConnection = AddADdbConnection();
            // change parent id of dataset
            FileInfo source_with_parent = FixParentId(datasetFile, ddbConnection, uuid);
            FileInfo source = FixNameId(source_with_parent, uuid);

            FileInfo response = new FileInfo(datasetFile + uuid + ".response.json");
            SaveToMappingStoreAndJson(source, response);

            var expectedEntities = ReadEntities<DatasetEntity>(source, EntityType.DataSet).ToList();

            // This test expects one and only one dataset
            Assume.That(expectedEntities, Has.Count.EqualTo(1));

            var expected = expectedEntities.First();

            var datasetFromMappingStore = _retrieveManager.GetEntities<DatasetEntity>(StoreId, EntityQuery.Empty, Detail.Full).FirstOrDefault(x => x.Name.Equals(expected.Name, StringComparison.Ordinal));
            Assert.NotNull(datasetFromMappingStore);

            FileInfo sourceColumns = FixParentId(datasetColumnFile, datasetFromMappingStore, uuid);
            FileInfo responseColumns = new FileInfo(datasetColumnFile + uuid + ".response.json");
            SaveToMappingStoreAndJson(sourceColumns, responseColumns);

            MappingSetEntity mappingSetEntity = new MappingSetEntity();
            mappingSetEntity.Name = uuid;
            mappingSetEntity.ParentId = dataflowUrn;
            mappingSetEntity.DataSetId = datasetFromMappingStore.EntityId;
            mappingSetEntity.ValidFrom = validFrom == null ? null : DateTime.Parse(validFrom);
            mappingSetEntity.ValidTo = validTo == null ? null : DateTime.Parse(validTo);
            mappingSetEntity.StoreId = this.StoreId;

            MappingSetEntity mappingSetEntityFromMappingStore = _persistManager.Add(mappingSetEntity);

            Assert.AreEqual(mappingSetEntity.Name, mappingSetEntityFromMappingStore.Name);
            Assert.AreEqual(mappingSetEntity.ParentId, mappingSetEntityFromMappingStore.ParentId);
            Assert.AreEqual(mappingSetEntity.DataSetId, mappingSetEntityFromMappingStore.DataSetId);
            Assert.AreEqual(mappingSetEntity.ValidFrom, mappingSetEntityFromMappingStore.ValidFrom);
            Assert.AreEqual(mappingSetEntity.ValidTo, mappingSetEntityFromMappingStore.ValidTo);
            ICommonStructureQuery query = CommonStructureQueryCore.Builder
                .NewQuery(Org.Sdmxsource.Sdmx.Api.Constants.CommonStructureQueryType.Other, Org.Sdmxsource.Sdmx.Api.Constants.StructureOutputFormatEnumType.SdmxV21StructureDocument)
                .SetStructureIdentification(new StructureReferenceImpl(dataflowUrn))
                .Build();
            var dsd = _retrieverFactory.DSDRetrievalEngine.RetrieveAsChildren(query).FirstOrDefault();
            Assume.That(dsd, Is.Not.Null);

            // create dummy mappings

            var mappingStoreColumns = _retrieveManager.GetEntities<DataSetColumnEntity>(StoreId, datasetFromMappingStore.QueryForThisParentId(), Detail.Full).ToArray();
            int columnIndex = CreateMappingsAndSave(mappingSetEntityFromMappingStore.EntityId, mappingStoreColumns, dsd.Dimensions);
            columnIndex = CreateMappingsAndSave(mappingSetEntityFromMappingStore.EntityId, mappingStoreColumns, dsd.Attributes, columnIndex);
            if (dsd.PrimaryMeasure != null)
            {
                columnIndex = CreateMappingsAndSave(mappingSetEntityFromMappingStore.EntityId, mappingStoreColumns, new[] { dsd.PrimaryMeasure }, columnIndex);
            }
            else
            {
                columnIndex = CreateMappingsAndSave(mappingSetEntityFromMappingStore.EntityId, mappingStoreColumns, dsd.Measures, columnIndex);
            }

            _persistManager.Delete <MappingSetEntity>(StoreId, mappingSetEntityFromMappingStore.EntityId);
            // Delete the dataset, it should delete also the dataset columns
            _persistManager.Delete<DatasetEntity>(StoreId, datasetFromMappingStore.EntityId);
            _persistManager.Delete<IConnectionEntity>(StoreId, ddbConnection.EntityId);
        }

        private int CreateMappingsAndSave<T>(string parentId, DataSetColumnEntity[] mappingStoreColumns, IList<T> dimensions, int columnIndex = 0) where T: IComponentMutableObject
        {
            if (dimensions is null)
            {
                return columnIndex;
            }

            foreach (var component in dimensions)
            {
                if (component.StructureType.EnumType == Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureEnumType.TimeDimension)
                {
                    TimeDimensionMappingEntity timeMappingEntity = new TimeDimensionMappingEntity();
                    timeMappingEntity.ParentId = parentId;
                    timeMappingEntity.PreFormatted = new TimePreFormattedEntity();
                    timeMappingEntity.PreFormatted.OutputColumn = mappingStoreColumns[1];
                    timeMappingEntity.PreFormatted.FromColumn = mappingStoreColumns[2];
                    timeMappingEntity.PreFormatted.ToColumn = mappingStoreColumns[3];
                    timeMappingEntity.AutoDetectTypeIfNotSet();
                    timeMappingEntity.StoreId = this.StoreId;
                    _persistManager.Add(timeMappingEntity);
                }
                else
                {
                    ComponentMappingEntity mappingEntity = new ComponentMappingEntity();
                    mappingEntity.ParentId = parentId;
                    mappingEntity.Component = new Component() { ObjectId = component.Id };
                    mappingEntity.AddColumn(mappingStoreColumns[columnIndex++]);
                    mappingEntity.StoreId = this.StoreId;
                    _persistManager.Add(mappingEntity);
                }
                if (columnIndex >= mappingStoreColumns.Length)
                {
                    columnIndex = 0;
                }
            }

            return columnIndex;
        }

        private FileInfo FixNameId(FileInfo source_with_parent, string uuid)
        {
            FileInfo originalWithFixedParent = new FileInfo(source_with_parent.Name + ".parent.json");
            using (var writer = originalWithFixedParent.CreateText())
            using( var source = source_with_parent.OpenText())
            {
                writer.WriteLine(
                    Regex.Replace(source.ReadToEnd(), "(\"name\"):\\s*\"\\w+\"", $"$1: \"{uuid}\""));
                writer.Flush();
            }
            return originalWithFixedParent;
        }

        private static FileInfo FixParentId(string datasetFile, IEntity parentEntity, string uuid)
        {
            FileInfo original = new FileInfo(datasetFile);
            if (!parentEntity.EntityId.Equals("14", StringComparison.Ordinal))
            {
                FileInfo originalWithFixedParent = new FileInfo(datasetFile + uuid + ".parent.json");
                using (var writer = originalWithFixedParent.CreateText())
                using( var source = original.OpenText())
                {
                    writer.WriteLine(
                        Regex.Replace(source.ReadToEnd(), "(\"parentId\"):\\s*\"\\d+\"", $"$1: \"{parentEntity.EntityId}\""));
                    writer.Flush();
                }
                return originalWithFixedParent;
            }
            else
            {
                return original;
            }
        }

        private IEnumerable<T> RetrieverEntties<T>(FileInfo output, EntityType type, IEntityQuery entityQuery) where T : IEntity
        {
            // This retrieval doesn't work because the json written cannot be parsed
            using (Stream stream = output.Create())
            using (var writer = new EntityStreamingWriter(stream))
            {
                _retrieveManager.WriteEntities(StoreId, writer, type, entityQuery, Detail.Full);
            }

            return ReadEntities<T>(output, type);
        }
        private void DeleteByName<T>(T entity) where T : ISimpleNameableEntity
        {
            var entities = _retrieveManager.GetEntities<T>(this.StoreId,
                new EntityQuery() { ObjectId = new Criteria<string>(OperatorType.Exact, entity.Name) }, Detail.IdOnly);
            foreach (var simpleNameableEntity in entities)
            {
                _persistManager.Delete<T>(StoreId, simpleNameableEntity.EntityId);
            }
        }
    }
}
