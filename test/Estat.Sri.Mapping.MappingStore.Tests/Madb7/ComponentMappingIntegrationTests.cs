// -----------------------------------------------------------------------
// <copyright file="TranscodingRuleIntegrationTests.cs" company="EUROSTAT">
//   Date Created : 2022-11-11
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Tests.Madb7
{
    using System.IO;
    using DryIoc;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Engine.Streaming;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Builder;
    using System;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Model.Error;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.XmlHelper;
    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sdmxsource.Extension.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// Tests the component mappings and transcoding rules in the msdb 7.0 schema.
    /// </summary>
    [TestFixture("msdb_scratch.sqlserver")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.oracle")]
    public class ComponentMappingIntegrationTests : BaseTestClassWithContainer
    {
        #region test setup

        private readonly IReadableDataLocationFactory _readableDataLocationFactory = new ReadableDataLocationFactory();
        private readonly IStructureParsingManager _parsingManager = new StructureParsingManager();
        private readonly IEntityPersistenceManager _entityPersistenceManager;
        private readonly IEntityAuthorizationManager _entityAuthorizationManager;
        private readonly IEntityRetrieverManager _entityRetrieverManager;
        private readonly SdmxAuthorizationScope _authorizationScope;
        private readonly IStructureSubmitterEngine _submitterEngine;
        private readonly EntityMapper _entityMapper = new EntityMapper();

        private long _mappingSetPK;
        private const string _writeToFile = @"tests/msdb7_componentMapping-out.json";

        public ComponentMappingIntegrationTests(string storeId)
            : base(storeId)
        {
            _entityAuthorizationManager = IoCContainer.Resolve<IEntityAuthorizationManager>();
            _entityRetrieverManager = IoCContainer.Resolve<IEntityRetrieverManager>();
            _authorizationScope = new SdmxAuthorizationScope(null);
            var submitFactory = new StructureSubmitMappingStoreFactory((storeId) => GetConnectionStringSettings());
            _submitterEngine = submitFactory.GetEngine(storeId);
            _entityPersistenceManager = IoCContainer.Resolve<IEntityPersistenceManager>();
        }

        private IEntityStreamingReader GetStreamingReader(Stream stream, EntityType entityType)
        {
            return new AuthEntityStreamingReaderDecorator(new EntityStreamingReader(StoreId, stream, entityType),
                _entityAuthorizationManager, _authorizationScope);
        }

        private IEntityStreamingWriter GetStreamingWriter(Stream stream)
        {
            return new EntityStreamingWriter(StoreId, stream,
                (entity, writer, sid) => { }, // don't write links
                (entity, writer) => { }); // don't write details
        }

        [OneTimeSetUp]
        public void SetUp()
        {
            InitializeMappingStore(StoreId);

            // add structures
            string structuresFile = @"tests/test-sdmxv3.0.0-measures-array-ESTAT+STS+2.0-DF-DSD-full.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = _submitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // add mapping entities
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(connectionStringManager), null);
            var ddbConnectionEntity = new DdbConnectionEntity()
            {
                Name = "STS 2.0 connection",
                Description = "DDB connection for new ESTAT.STS.2.0 datastructure",
                DbType = "sqlserver",
                DbUser = "ddb",
                Password = "123",
                AdoConnString = "Data Source=test-ddb-mssql;Initial Catalog=DDB_B_TEST_JANUARY2014;Integrated Security=False;User ID=ddb;Password=123"
            };
            long ddbConnectionPK = entityPeristenceFactory.GetEngine<DdbConnectionEntity>(StoreId).Add(ddbConnectionEntity);
            Assert.NotNull(ddbConnectionPK);

            var datasetEntity = new DatasetEntity()
            {
                Name = "STS 2.0 dataset",
                Description = "Dataset for new ESTAT.STS.2.0 datastructure",
                Query =
                    "select [ESA_MAIN_TOT_ESTAT_101_FREQ], [ESA_MAIOT_ESTAT_101_REF_PERIOD], [ESA_MAIN_TOT_ESTAT_101_STO], [ESA_MAI_TOT_ESTAT_101_ACTIVITY]," +
                    " [ESA_MAIT_ESTAT_101_CONF_STATUS], [ESA_MAITOT_ESTAT_101_OBS_VALUE], [ESA_MAIOT_ESTAT_101_OBS_STATUS]" +
                    " from ESA_MAIN_TOT_ESTAT_10",
                EditorType = "org.estat.ma.gui.userControls.dataset.QueryEditor.CustomQueryEditor, MappingAssistant",
                ParentId = ddbConnectionPK.ToString()
            };
            long datasetPK = entityPeristenceFactory.GetEngine<DatasetEntity>(StoreId).Add(datasetEntity);
            Assert.NotNull(datasetPK);

            string dataflowUrn = importResults
                .First(r => r.StructureReference.MaintainableStructureEnumType == SdmxStructureEnumType.Dataflow)
                .StructureReference.MaintainableUrn.AbsoluteUri;
            var mappingSetEntity = new MappingSetEntity()
            {
                Name = "STS 2.0 mapping set",
                Description = "Mapping set for new ESTAT.STS.2.0 datastructure",
                DataSetId = datasetPK.ToString(),
                ParentId = dataflowUrn
            };
            _mappingSetPK = entityPeristenceFactory.GetEngine<MappingSetEntity>(StoreId).Add(mappingSetEntity);
            Assert.NotNull(_mappingSetPK);
        }

        private ISdmxObjects ReadStructures(string filepath, SdmxSchemaEnumType sdmxSchema)
        {
            FileInfo fileInfo = new FileInfo(filepath);
            using (IReadableDataLocation rdl = _readableDataLocationFactory.GetReadableDataLocation(fileInfo))
            {
                IStructureWorkspace workspace = _parsingManager.ParseStructures(rdl);

                ISdmxObjects structureBeans = workspace.GetStructureObjects(false);

                XMLParser.ValidateXml(rdl, sdmxSchema);

                return structureBeans;
            }
        }

        #endregion

        [Test]
        public void TestInsertRetrieveDelete_mapping_1to1()
        {
            string mappingFile = "tests/ComponentMappings/ComponentMapping1to1.json";
            string readFromFile = SetParentId(mappingFile, _mappingSetPK.ToString());

            // add entities and parse returned json to validate results
            AddEntities(readFromFile, _writeToFile, EntityType.Mapping);
            var mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            var mapping = mappings.Single();
            string entityId = mapping.EntityId;
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(1));
            Assert.AreEqual("TEST_COL_SINGLE", mapping.GetColumns().First().Name);

            // retrieve and validate mapping entity
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            mapping = mappings.Single();
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(1));
            Assert.AreEqual("TEST_COL_SINGLE", mapping.GetColumns().First().Name);

            // delete mapping entity
            var mappingEngine = _entityPersistenceManager.GetEngine<ComponentMappingEntity>(StoreId);
            mappingEngine.Delete(entityId, EntityType.Mapping);
            // try to retrieve it
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(0));
        }

        [Test]
        public void TestInsertRetrieveDelete_mapping_Nto1()
        {
            string mappingFile = "tests/ComponentMappings/ComponentMappingNto1.json";
            string readFromFile = SetParentId(mappingFile, _mappingSetPK.ToString());

            // add entities and parse returned json to validate results
            AddEntities(readFromFile, _writeToFile, EntityType.Mapping);
            var mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            var mapping = mappings.Single();
            string entityId = mapping.EntityId;
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(2));
            Assert.AreEqual("TEST_COL1", mapping.GetColumns()[0].Name);
            Assert.AreEqual("TEST_COL2", mapping.GetColumns()[1].Name);

            // retrieve and validate mapping entity
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            mapping = mappings.Single();
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(2));
            Assert.AreEqual("TEST_COL1", mapping.GetColumns()[0].Name);
            Assert.AreEqual("TEST_COL2", mapping.GetColumns()[1].Name);

            // delete mapping entity
            var mappingEngine = _entityPersistenceManager.GetEngine<ComponentMappingEntity>(StoreId);
            mappingEngine.Delete(entityId, EntityType.Mapping);
            // try to retrieve it
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(0));
        }

        [TestCase("tests/ComponentMappings/TranscodingNtoArrayValues.json", 0)]
        [TestCase("tests/ComponentMappings/TranscodingNto1andNtoArray.json", 1)]
        [TestCase("tests/ComponentMappings/TranscodingNtoArrayandNto1.json", 0)]
        public void TestInsertRetrieveDelete_mapping_Nto1_withTranscodingArrayValues(string transcodingFile, int expectedRules)
        {
            // we don't support transcoding NtoArray (N dataset columns to one SDMX component with array values)
            // So if there is a request with two transcodings, one valid, one NtoArray - invalid,
            // the application will accept the valid only if it is before the invalid.
            // The exception will stop the import of the following entities.

            string mappingFile = "tests/ComponentMappings/ComponentMappingNto1.json";

            // add mapping
            string readFromMappingFile = SetParentId(mappingFile, _mappingSetPK.ToString());
            AddEntities(readFromMappingFile, _writeToFile, EntityType.Mapping);
            var mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            var mapping = mappings.Single();
            string entityId = mapping.EntityId;

            // try add transcoding
            // should fail because we don't support Nto1 mapping with array transcoding yet
            string readFromTranscodingFile = SetParentId(transcodingFile, entityId);
            Assert.Throws<NotImplementedException>(() => AddEntities(readFromTranscodingFile, _writeToFile, EntityType.TranscodingRule));

            // retrieve and validate mapping entity
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            mapping = mappings.Single();
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(2));
            Assert.AreEqual("TEST_COL1", mapping.GetColumns()[0].Name);
            Assert.AreEqual("TEST_COL2", mapping.GetColumns()[1].Name);
            // should not retrieve any transcodings
            RetrieveEntity(_writeToFile, EntityType.TranscodingRule, null);
            var rules = ParseJsonEntities<TranscodingRuleEntity>(_writeToFile, EntityType.TranscodingRule);
            Assert.AreEqual(expectedRules, rules.Count());

            // delete mapping entity
            var mappingEngine = _entityPersistenceManager.GetEngine<ComponentMappingEntity>(StoreId);
            mappingEngine.Delete(entityId, EntityType.Mapping);
            // try to retrieve it
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(0));
        }

        [Test]
        public void TestInsertRetrieveDelete_mapping_Nto1_withTranscoding_Nto1()
        {
            string mappingFile = "tests/ComponentMappings/ComponentMappingNto1.json";
            string transcodingFile = "tests/ComponentMappings/TranscodingNto1.json";

            // add mapping
            string readFromMappingFile = SetParentId(mappingFile, _mappingSetPK.ToString());
            AddEntities(readFromMappingFile, _writeToFile, EntityType.Mapping);
            var mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            var mapping = mappings.Single();
            string entityId = mapping.EntityId;

            // add transcoding
            // should accept Nto1 mapping for a single code
            string readFromTranscodingFile = SetParentId(transcodingFile, entityId);
            AddEntities(readFromTranscodingFile, _writeToFile, EntityType.TranscodingRule);
            var rules = ParseJsonEntities<TranscodingRuleEntity>(_writeToFile, EntityType.TranscodingRule).ToList();
            Assert.That(rules.Count, Is.EqualTo(2));
            // rule 1
            CollectionAssert.IsEmpty(rules[0].DsdCodeEntities);
            Assert.AreEqual("K_SDMX_CODE_1", rules[0].DsdCodeEntity.ObjectId);
            Assert.That(rules[0].LocalCodes.Count, Is.EqualTo(2));
            Assert.AreEqual("K_LOCAL_CODE_11", rules[0].LocalCodes[0].ObjectId);
            Assert.AreEqual("TEST_COL1", rules[0].LocalCodes[0].ParentId);
            Assert.AreEqual("K_LOCAL_CODE_12", rules[0].LocalCodes[1].ObjectId);
            Assert.AreEqual("TEST_COL2", rules[0].LocalCodes[1].ParentId);
            // rule 2
            CollectionAssert.IsEmpty(rules[1].DsdCodeEntities);
            Assert.AreEqual("K_SDMX_CODE_2", rules[1].DsdCodeEntity.ObjectId);
            Assert.That(rules[1].LocalCodes.Count, Is.EqualTo(2));
            Assert.AreEqual("K_LOCAL_CODE_21", rules[1].LocalCodes[0].ObjectId);
            Assert.AreEqual("TEST_COL1", rules[1].LocalCodes[0].ParentId);
            Assert.AreEqual("K_LOCAL_CODE_22", rules[1].LocalCodes[1].ObjectId);
            Assert.AreEqual("TEST_COL2", rules[1].LocalCodes[1].ParentId);

            // retrieve and validate mapping entity
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            mapping = mappings.Single();
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(2));
            Assert.AreEqual("TEST_COL1", mapping.GetColumns()[0].Name);
            Assert.AreEqual("TEST_COL2", mapping.GetColumns()[1].Name);
            // retrieve and validate transcodings
            foreach (var rule in rules)
            {
                RetrieveEntity(_writeToFile, EntityType.TranscodingRule, rule.EntityId);
                var retrievedRules = ParseJsonEntities<TranscodingRuleEntity>(_writeToFile, EntityType.TranscodingRule);
                Assert.That(retrievedRules.Count(), Is.EqualTo(1));
                var retrievedRule = retrievedRules.Single();
                Assert.AreEqual(1, retrievedRule.DsdCodeEntities.Count);
                Assert.AreEqual(rule.DsdCodeEntity.ObjectId, retrievedRule.DsdCodeEntities.Single().ObjectId);
                Assert.AreEqual(rule.LocalCodes.Count, retrievedRule.LocalCodes.Count);
            }

            // delete mapping entity (should cascade delete transcodings)
            var mappingEngine = _entityPersistenceManager.GetEngine<ComponentMappingEntity>(StoreId);
            mappingEngine.Delete(entityId, EntityType.Mapping);
            // try to retrieve mapping & transcoding
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(0));
            foreach (var rule in rules)
            {
                RetrieveEntity(_writeToFile, EntityType.TranscodingRule, rule.EntityId);
                var retrievedRules = ParseJsonEntities<TranscodingRuleEntity>(_writeToFile, EntityType.TranscodingRule).ToList();
                Assert.That(retrievedRules.Count, Is.EqualTo(0));
            }
        }
       [Test]
        public void TestInsertRetrieveDelete_mappingset()
        {
            string mappingFile = "tests/ComponentMappings/ComponentMapping1to1.json";
            string transcodingFile = "tests/ComponentMappings/Transcoding1to1.json";

            var mappingSetQuery = new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, _mappingSetPK.ToString()) };
            MappingSetEntity mappingSetEntity = _entityRetrieverManager.GetEntities<MappingSetEntity>(StoreId, mappingSetQuery , Detail.Full).First();

            mappingSetEntity.Name = "new name";
            mappingSetEntity.EntityId = null;
            var newMappingSetPK = _entityPersistenceManager.Add(mappingSetEntity);
            // add mapping
            string readFromMappingFile = SetParentId(mappingFile, newMappingSetPK.EntityId);
            AddEntities(readFromMappingFile, _writeToFile, EntityType.Mapping);
            var mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            var mapping = mappings.Single();
            string entityId = mapping.EntityId;

            // add transcoding
            string readFromTranscodingFile = SetParentId(transcodingFile, entityId);
            AddEntities(readFromTranscodingFile, _writeToFile, EntityType.TranscodingRule);

            // delete mapping set (should cascade delete transcodings)
            _entityPersistenceManager.Delete<MappingSetEntity>(StoreId, newMappingSetPK.EntityId);

        }

        [Test]
        public void TestInsertRetrieveDelete_mapping_1to1_withTranscoding_1to1()
        {
            string mappingFile = "tests/ComponentMappings/ComponentMapping1to1.json";
            string transcodingFile = "tests/ComponentMappings/Transcoding1to1.json";

            // add mapping
            string readFromMappingFile = SetParentId(mappingFile, _mappingSetPK.ToString());
            AddEntities(readFromMappingFile, _writeToFile, EntityType.Mapping);
            var mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            var mapping = mappings.Single();
            string entityId = mapping.EntityId;

            // add transcoding
            string readFromTranscodingFile = SetParentId(transcodingFile, entityId);
            AddEntities(readFromTranscodingFile, _writeToFile, EntityType.TranscodingRule);
            var rules = ParseJsonEntities<TranscodingRuleEntity>(_writeToFile, EntityType.TranscodingRule).ToList();
            Assert.That(rules.Count, Is.EqualTo(2));
            CollectionAssert.IsEmpty(rules[0].DsdCodeEntities);
            CollectionAssert.IsEmpty(rules[1].DsdCodeEntities);
            Assert.AreEqual("TEST_SDMX_CODE1", rules[0].DsdCodeEntity.ObjectId);
            Assert.AreEqual("TEST_SDMX_CODE2", rules[1].DsdCodeEntity.ObjectId);
            Assert.That(rules[0].LocalCodes.Count, Is.EqualTo(1));
            Assert.That(rules[1].LocalCodes.Count, Is.EqualTo(1));
            Assert.AreEqual("K_LOCAL_CODE_1", rules[0].LocalCodes.Single().ObjectId);
            Assert.AreEqual("TEST_COL_SINGLE", rules[0].LocalCodes.Single().ParentId);
            Assert.AreEqual("K_LOCAL_CODE_2", rules[1].LocalCodes.Single().ObjectId);
            Assert.AreEqual("TEST_COL_SINGLE", rules[1].LocalCodes.Single().ParentId);

            // retrieve and validate mapping entity
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            mapping = mappings.Single();
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(1));
            Assert.AreEqual("TEST_COL_SINGLE", mapping.GetColumns()[0].Name);
            // retrieve and validate transcodings
            foreach (var rule in rules)
            {
                RetrieveEntity(_writeToFile, EntityType.TranscodingRule, rule.EntityId);
                var retrievedRules = ParseJsonEntities<TranscodingRuleEntity>(_writeToFile, EntityType.TranscodingRule);
                Assert.That(retrievedRules.Count(), Is.EqualTo(1));
                var retrievedRule = retrievedRules.Single();
//                Assert.AreEqual(1, retrievedRule.DsdCodeEntities.Count);
                Assert.AreEqual(rule.DsdCodeEntity.ObjectId, retrievedRule.DsdCodeEntity.ObjectId);
                Assert.AreEqual(rule.LocalCodes.Count, retrievedRule.LocalCodes.Count);
                Assert.AreEqual(rule.LocalCodes.Single().ObjectId, retrievedRule.LocalCodes.Single().ObjectId);
            }

            // delete mapping entity (should cascade delete transcodings)
            var mappingEngine = _entityPersistenceManager.GetEngine<ComponentMappingEntity>(StoreId);
            mappingEngine.Delete(entityId, EntityType.Mapping);
            // try to retrieve mapping & transcoding
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(0));
            foreach (var rule in rules)
            {
                RetrieveEntity(_writeToFile, EntityType.TranscodingRule, rule.EntityId);
                var retrievedRules = ParseJsonEntities<TranscodingRuleEntity>(_writeToFile, EntityType.TranscodingRule).ToList();
                Assert.That(retrievedRules.Count, Is.EqualTo(0));
            }
        }

        [Test]
        public void TestInsertRetrieveDelete_mapping_1to1_withTranscodingArrayValues()
        {
            string mappingFile = "tests/ComponentMappings/ComponentMapping1to1.json";
            string transcodingFile = "tests/ComponentMappings/Transcoding1toArrayValues.json";

            // add mapping
            string readFromMappingFile = SetParentId(mappingFile, _mappingSetPK.ToString());
            AddEntities(readFromMappingFile, _writeToFile, EntityType.Mapping);
            var mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            var mapping = mappings.Single();
            string entityId = mapping.EntityId;

            // add transcoding
            string readFromTranscodingFile = SetParentId(transcodingFile, entityId);
            AddEntities(readFromTranscodingFile, _writeToFile, EntityType.TranscodingRule);
            var rules = ParseJsonEntities<TranscodingRuleEntity>(_writeToFile, EntityType.TranscodingRule).ToList();
            Assert.That(rules.Count, Is.EqualTo(2));
            Assert.IsNull(rules[0].DsdCodeEntity);
            Assert.IsNull(rules[1].DsdCodeEntity);
            Assert.AreEqual("K_SDMX_CODE_11", rules[0].DsdCodeEntities[0].ObjectId);
            Assert.AreEqual("K_SDMX_CODE_12", rules[0].DsdCodeEntities[1].ObjectId);
            Assert.AreEqual("K_SDMX_CODE_21", rules[1].DsdCodeEntities[0].ObjectId);
            Assert.AreEqual("K_SDMX_CODE_22", rules[1].DsdCodeEntities[1].ObjectId);
            Assert.That(rules[0].LocalCodes.Count, Is.EqualTo(1));
            Assert.That(rules[1].LocalCodes.Count, Is.EqualTo(1));
            Assert.AreEqual("K_LOCAL_CODE_1", rules[0].LocalCodes.Single().ObjectId);
            Assert.AreEqual("TEST_COL_SINGLE", rules[0].LocalCodes.Single().ParentId);
            Assert.AreEqual("K_LOCAL_CODE_2", rules[1].LocalCodes.Single().ObjectId);
            Assert.AreEqual("TEST_COL_SINGLE", rules[1].LocalCodes.Single().ParentId);

            // retrieve and validate mapping entity
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            mapping = mappings.Single();
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(1));
            Assert.AreEqual("TEST_COL_SINGLE", mapping.GetColumns()[0].Name);
            // retrieve and validate transcodings
            foreach (var rule in rules)
            {
                RetrieveEntity(_writeToFile, EntityType.TranscodingRule, rule.EntityId);
                var retrievedRules0 = ParseJsonEntities<TranscodingRuleEntity>(_writeToFile, EntityType.TranscodingRule);
                Assert.That(retrievedRules0.Count(), Is.EqualTo(1));
                var retrievedRule = retrievedRules0.Single();
                Assert.AreEqual(rule.DsdCodeEntities.Count, retrievedRule.DsdCodeEntities.Count);
                Assert.AreEqual(rule.LocalCodes.Count, retrievedRule.LocalCodes.Count);
            }

            // try delete a transcoding first
            var transcodingEngine = _entityPersistenceManager.GetEngine<TranscodingRuleEntity>(StoreId);
            transcodingEngine.Delete(rules[0].EntityId, EntityType.TranscodingRule);
            RetrieveEntity(_writeToFile, EntityType.TranscodingRule, rules[0].EntityId);
            var retrievedRules = ParseJsonEntities<TranscodingRuleEntity>(_writeToFile, EntityType.TranscodingRule).ToList();
            Assert.That(retrievedRules.Count(), Is.EqualTo(0));
            // then delete mapping entity (should cascade delete remaining transcodings)
            var mappingEngine = _entityPersistenceManager.GetEngine<ComponentMappingEntity>(StoreId);
            mappingEngine.Delete(entityId, EntityType.Mapping);
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(0));
            // make sure the 2nd rule is deleted
            RetrieveEntity(_writeToFile, EntityType.TranscodingRule, rules[1].EntityId);
            retrievedRules = ParseJsonEntities<TranscodingRuleEntity>(_writeToFile, EntityType.TranscodingRule).ToList();
            Assert.That(retrievedRules.Count, Is.EqualTo(0));
        }

        [TestCase("tests/ComponentMappings/ComponentMappingConstValue.json")]
        [TestCase("tests/ComponentMappings/ComponentMappingConstValue2.json")]
        [TestCase("tests/ComponentMappings/ComponentMappingConstValue3.json")]
        public void TestImportRetrieveDelete_mapping_constValue(string mappingFile)
        {
            // add entities and parse returned json to validate results
            string readFromFile = SetParentId(mappingFile, _mappingSetPK.ToString());
            AddEntities(readFromFile, _writeToFile, EntityType.Mapping);
            var mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            var mapping = mappings.Single();
            string entityId = mapping.EntityId;
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(0));

            // retrieve and validate mapping entity
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            mapping = mappings.Single();
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(0));
            Assert.That(mapping.ConstantValues.Count, Is.EqualTo(1));
            Assert.AreEqual("TEST_CONST", mapping.ConstantValues.Single());//it always returns the array property regardless if it is one or more
            Assert.IsNull(mapping.DefaultValue);

            // delete mapping entity
            var mappingEngine = _entityPersistenceManager.GetEngine<ComponentMappingEntity>(StoreId);
            mappingEngine.Delete(entityId, EntityType.Mapping);
            // try to retrieve it
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(0));
        }

        [TestCase("tests/ComponentMappings/ComponentMappingConstValueWithCol.json")]
        [TestCase("tests/ComponentMappings/ComponentMappingConstArrayValueWithCol.json")]
        public void TestImportRetrieveDelete_mapping_constValueWithCol(string mappingFile)
        {
            string readFromFile = SetParentId(mappingFile, _mappingSetPK.ToString());
            // constant value requires no dataset columns
            Assert.Throws<ValidationException>(() => AddEntities(readFromFile, _writeToFile, EntityType.Mapping));
        }

        [TestCase("tests/ComponentMappings/ComponentMappingDefaultValue.json")]
        [TestCase("tests/ComponentMappings/ComponentMappingDefaultValue2.json")]
        [TestCase("tests/ComponentMappings/ComponentMappingDefaultValue3.json")]
        public void TestImportRetrieveDelete_mapping_defaultValue(string mappingFile)
        {
            string readFromFile = SetParentId(mappingFile, _mappingSetPK.ToString());

            // add entities and parse returned json to validate results
            AddEntities(readFromFile, _writeToFile, EntityType.Mapping);
            var mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            var mapping = mappings.Single();
            string entityId = mapping.EntityId;
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(1));

            // retrieve and validate mapping entity
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            mapping = mappings.Single();
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(1));
            Assert.That(mapping.DefaultValues.Count, Is.EqualTo(1));
            Assert.AreEqual("TEST_COL_SINGLE", mapping.GetColumns().Single().Name);
            Assert.AreEqual("TEST_DEFAULT", mapping.DefaultValues.Single());//it always returns the array property regardless if it is one or more
            Assert.IsNull(mapping.ConstantValue);

            // delete mapping entity
            var mappingEngine = _entityPersistenceManager.GetEngine<ComponentMappingEntity>(StoreId);
            mappingEngine.Delete(entityId, EntityType.Mapping);
            // try to retrieve it
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(0));
        }

        [TestCase("tests/ComponentMappings/ComponentMappingDefaultValueNoCol.json")]
        [TestCase("tests/ComponentMappings/ComponentMappingDefaultArrayValueNoCol.json")]
        public void TestImportRetrieveDelete_mapping_defaultValueNoCol(string mappingFile)
        {
            string readFromFile = SetParentId(mappingFile, _mappingSetPK.ToString());
            // default value requires a dataset column
            Assert.Throws<ValidationException>(() => AddEntities(readFromFile, _writeToFile, EntityType.Mapping));
        }

        [TestCase("tests/ComponentMappings/ComponentMappingDefaultArrayValues.json")]
        [TestCase("tests/ComponentMappings/ComponentMappingDefaultArrayValues2.json")]
        public void TestImportRetrieveDelete_mapping_defaultArrayValues(string mappingFile)
        {
            string readFromFile = SetParentId(mappingFile, _mappingSetPK.ToString());

            // add entities and parse returned json to validate results
            AddEntities(readFromFile, _writeToFile, EntityType.Mapping);
            var mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            var mapping = mappings.Single();
            string entityId = mapping.EntityId;
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(1));
            Assert.That(mapping.DefaultValues.Count, Is.EqualTo(2));

            // retrieve and validate mapping entity
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            mapping = mappings.Single();
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(1));
            Assert.AreEqual("TEST_DEFAULT_ARRAY1", mapping.DefaultValues.ToList()[0]);
            Assert.AreEqual("TEST_DEFAULT_ARRAY2", mapping.DefaultValues.ToList()[1]);
            Assert.IsNotNull(mapping.DefaultValue);//backward compatibility
            Assert.IsNull(mapping.ConstantValue);

            // delete mapping entity
            var mappingEngine = _entityPersistenceManager.GetEngine<ComponentMappingEntity>(StoreId);
            mappingEngine.Delete(entityId, EntityType.Mapping);
            // try to retrieve it
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(0));
        }

        [TestCase("tests/ComponentMappings/ComponentMappingConstArrayValues.json")]
        [TestCase("tests/ComponentMappings/ComponentMappingConstArrayValues2.json")]
        public void TestImportRetrieveDelete_mapping_constantArrayValues(string mappingFile)
        {
            string readFromFile = SetParentId(mappingFile, _mappingSetPK.ToString());

            // add entities and parse returned json to validate results
            AddEntities(readFromFile, _writeToFile, EntityType.Mapping);
            var mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            var mapping = mappings.Single();
            string entityId = mapping.EntityId;
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(0));
            Assert.That(mapping.ConstantValues.Count, Is.EqualTo(2));

            // retrieve and validate mapping entity
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(1));
            mapping = mappings.Single();
            Assert.That(mapping.GetColumns().Count, Is.EqualTo(0));
            Assert.AreEqual("TEST_CONST_ARRAY1", mapping.ConstantValues.ToList()[0]);
            Assert.AreEqual("TEST_CONST_ARRAY2", mapping.ConstantValues.ToList()[1]);
            Assert.IsNull(mapping.DefaultValue);
            Assert.IsNotNull(mapping.ConstantValue);//backward compatibility

            // delete mapping entity
            var mappingEngine = _entityPersistenceManager.GetEngine<ComponentMappingEntity>(StoreId);
            mappingEngine.Delete(entityId, EntityType.Mapping);
            // try to retrieve it
            RetrieveEntity(_writeToFile, EntityType.Mapping, entityId);
            mappings = ParseJsonEntities<ComponentMappingEntity>(_writeToFile, EntityType.Mapping);
            Assert.That(mappings.Count(), Is.EqualTo(0));
        }

        #region private methods

        /// <summary>
        /// Reads entities from <paramref name="readFromFile"/> and imports in msdb.
        /// Writes the response in <paramref name="writeToFile"/>.
        /// </summary>
        /// <param name="readFromFile"></param>
        /// <param name="writeToFile"></param>
        /// <param name="entityType"></param>
        private void AddEntities(string readFromFile, string writeToFile, EntityType entityType)
        {
            using (var readfromStream = new FileStream(readFromFile, FileMode.Open, FileAccess.Read))
            using (var writeToStream = new FileStream(writeToFile, FileMode.Create, FileAccess.ReadWrite))
            {
                using (var reader = GetStreamingReader(readfromStream, entityType))
                using (var writer = GetStreamingWriter(writeToStream))
                {
                    _entityPersistenceManager.AddEntities(StoreId, reader, writer, entityType);
                }
            }
        }

        private void RetrieveEntity(string writeToFile, EntityType entityType, string entityId)
        {
            using (var writeToStream = new FileStream(writeToFile, FileMode.Create, FileAccess.ReadWrite))
            using (var writer = GetStreamingWriter(writeToStream))
            {
                var entityQuery = new EntityQuery();
                if (!string.IsNullOrEmpty(entityId))
                {
                    entityQuery.EntityId = new Criteria<string>(OperatorType.Exact, entityId);
                }
                _entityRetrieverManager.WriteEntities(StoreId, writer, entityType, entityQuery, Detail.Full);
            }
        }

        private IEnumerable<T> ParseJsonEntities<T>(string fileName, EntityType entityType) where T : IEntity
        {
            FileInfo file = new FileInfo(fileName);
            using (var reader = new JsonTextReader(file.OpenText()))
            {
                while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                {
                    if (reader.Depth == 1)
                    {
                        var entity = _entityMapper.CastToObject("nothing", entityType, reader);
                        yield return (T)entity;
                    }
                }
            }
        }

        /// <summary>
        /// Will return a copy of the json, with the 'placeHolder' replaced by the actual mapping id.
        /// </summary>
        /// <param name="fileName">The json file to copy.</param>
        /// <param name="parentId"></param>
        /// <returns>the new temp json file name.</returns>
        private string SetParentId(string fileName, string parentId)
        {
            string tempFile = Path.GetTempFileName();
            FileInfo file = new FileInfo(fileName);
            using (var reader = new JsonTextReader(file.OpenText()))
            {
                var json = JObject.Load(reader);
                string jsonString = json.ToString().Replace("placeHolder", parentId, StringComparison.Ordinal);
                File.WriteAllText(tempFile, jsonString);
            }
            return tempFile;
        }

        #endregion
    }
}
