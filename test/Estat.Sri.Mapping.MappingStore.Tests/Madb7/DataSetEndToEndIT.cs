using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine.Streaming;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStore.Store;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Util.Extension;
using System.Text.RegularExpressions;
using Estat.Sri.Mapping.Api.Extension;
using Newtonsoft.Json.Linq;
using Estat.Sri.Mapping.Api.Utils;

namespace Estat.Sri.Mapping.MappingStore.Tests.Madb7
{
    [TestFixture("msdb_scratch.sqlserver")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.oracle")]

    internal class DataSetEndToEndIT : BaseTestClassWithContainer
    {
        private readonly IEntityPersistenceManager _persistManager;

        private readonly IEntityRetrieverManager _retrieveManager;

        public DataSetEndToEndIT(string storeId) : base(storeId)
        {
            this.InitializeMappingStore(storeId);
            _persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            _retrieveManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            IStructureParsingManager structureParsingManager = this.IoCContainer.Resolve<IStructureParsingManager>();
            ISdmxObjects sdmxObjects = new FileInfo("tests/end-to-end/SSTSCONS_PROD_M_TUTORIAL.xml").GetSdmxObjects(structureParsingManager);
            sdmxObjects.Merge(new FileInfo("tests/end-to-end/CENSUSHUB_2021.xml").GetSdmxObjects(structureParsingManager));
            sdmxObjects.Merge(new FileInfo("tests/end-to-end/CENSUSHUB_Q_XS1.xml").GetSdmxObjects(structureParsingManager));
            var artefactImportStatuses = new List<ArtefactImportStatus>();
            var structurePersistenceManager =
                new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(
                    this.GetConnectionStringSettings(),
                    artefactImportStatuses);

            structurePersistenceManager.SaveStructures(sdmxObjects);
            Assert.That(
                artefactImportStatuses.All(status => status.ImportMessage.Status != ImportMessageStatus.Error),
                string.Join("\n", artefactImportStatuses.Select(status => status.ImportMessage.Message)));

        }

        [TestCase("tests/end-to-end/new-ddb-connection.json")]
        [TestCase("tests/end-to-end/new-ddb-connection-with-permissions.json")]
        public void CreateADDBConnection(string jsonFile)
        {
            FileInfo source = new FileInfo(jsonFile);
            FileInfo response = new FileInfo(jsonFile + ".response.json");
            SaveToMappingStoreAndJson(source, response);

            var expectedEntities =ReadEntities<IConnectionEntity>(source, EntityType.DdbConnectionSettings).ToDictionary(k => k.Name);
            
            var mappingStoreEntities  = _retrieveManager.GetEntities<IConnectionEntity>(StoreId, EntityQuery.Empty, Detail.Full).ToDictionary(k => k.Name);

            Assert.AreEqual(expectedEntities.Count, mappingStoreEntities.Count);
            CollectionAssert.AreEquivalent(expectedEntities.Keys, mappingStoreEntities.Keys);

            foreach(var expected in expectedEntities)
            {
                var result = mappingStoreEntities[expected.Key];
                // first we delete since we don't need it in the db anymore
                _persistManager.Delete<IConnectionEntity>(StoreId, result.EntityId);
                Assert.AreEqual(expected.Value.DatabaseVendorType, result.DatabaseVendorType);
                Assert.NotNull(result.DbName);
                Assert.AreEqual(expected.Value.Description, result.Description);
                CollectionAssert.AreEquivalent(expected.Value.Permissions.Keys, result.Permissions.Keys);

                // When returning from mapping store we get all settings from driver but from maweb only the settings that are modified
                var keysUsed = new HashSet<string>(result.Settings.Keys);
                keysUsed.IntersectWith(expected.Value.Settings.Keys);
                foreach(var key in keysUsed)
                {
                    Assert.AreEqual(expected.Value.Settings[key].Value, result.Settings[key].Value, "Not equal for {0}", key);
                }
            }
        }

        [TestCase("tests/end-to-end/dataset.json")]
        public void CreateDataSet(string datasetFile)
        {
            var ddbConnection = AddADdbConnection();
            // change parent id of dataset
            FileInfo source = FixParentId(datasetFile, ddbConnection);

            FileInfo response = new FileInfo(datasetFile + ".response.json");
            SaveToMappingStoreAndJson(source, response);

            var expectedEntities =ReadEntities<DatasetEntity>(source, EntityType.DataSet).ToDictionary(k => k.Name);
            
            var mappingStoreEntities  = _retrieveManager.GetEntities<DatasetEntity>(StoreId, EntityQuery.Empty, Detail.Full).ToDictionary(k => k.Name);

            Assert.AreEqual(expectedEntities.Count, mappingStoreEntities.Count);
            CollectionAssert.AreEquivalent(expectedEntities.Keys, mappingStoreEntities.Keys);

            foreach (var expected in expectedEntities)
            {
                var result = mappingStoreEntities[expected.Key];
                // first we delete since we don't need it in the db anymore
                _persistManager.Delete<DatasetEntity>(StoreId, result.EntityId);
                Assert.AreEqual(expected.Value.ParentId, result.ParentId);
                Assert.AreEqual(expected.Value.Description, result.Description);
                Assert.AreEqual(expected.Value.Query, result.Query);
                Assert.AreEqual(expected.Value.JSONQuery, result.JSONQuery);
                CollectionAssert.AreEquivalent(expected.Value.Permissions.Keys, result.Permissions.Keys);
            }
            _persistManager.Delete<IConnectionEntity>(StoreId, ddbConnection.EntityId);
        }

        [TestCase("tests/end-to-end/dataset.json", "tests/end-to-end/datasetColumns.json")]
        public void CreateDataSetWithColumns(string datasetFile, string datasetColumnFile)
        {
            var ddbConnection = AddADdbConnection();
            // change parent id of dataset
            FileInfo source = FixParentId(datasetFile, ddbConnection);

            FileInfo response = new FileInfo(datasetFile + ".response.json");
            SaveToMappingStoreAndJson(source, response);

            var expectedEntities =ReadEntities<DatasetEntity>(source, EntityType.DataSet).ToList();
            
            // This test expects one and only one dataset
            Assume.That(expectedEntities, Has.Count.EqualTo(1));

            var expected = expectedEntities.First();

            var datasetFromMappingStore  = _retrieveManager.GetEntities<DatasetEntity>(StoreId, EntityQuery.Empty, Detail.Full).FirstOrDefault(x => x.Name.Equals(expected.Name, StringComparison.Ordinal));
            Assert.NotNull(datasetFromMappingStore);


            FileInfo sourceColumns = FixParentId(datasetColumnFile, datasetFromMappingStore);
            FileInfo responseColumns = new FileInfo(datasetColumnFile + ".response.json");
            SaveToMappingStoreAndJson(sourceColumns, responseColumns);


            var expectedColumns = ReadEntities<DataSetColumnEntity>(sourceColumns, EntityType.DataSetColumn).ToDictionary(k => k.Name);
            var mappingStoreColumns = _retrieveManager.GetEntities<DataSetColumnEntity>(StoreId, datasetFromMappingStore.QueryForThisParentId(), Detail.Full).ToDictionary(k => k.Name);

            // Delete the dataset, it should delete also the dataset columns
            _persistManager.Delete<DatasetEntity>(StoreId, datasetFromMappingStore.EntityId);
            _persistManager.Delete<IConnectionEntity>(StoreId, ddbConnection.EntityId);

            Assert.AreEqual(expectedColumns.Count, mappingStoreColumns.Count);
            CollectionAssert.AreEquivalent(expectedColumns.Keys, mappingStoreColumns.Keys);
        }
        [TestCase("tests/end-to-end/dataset.json", "tests/end-to-end/datasetColumns.json", "tests/end-to-end/datasetPatch.json")]
        public void ShouldUpdateADatasetWithPermissions(string datasetFile, string datasetColumnFile, string patchFile)
        {
            var ddbConnection = AddADdbConnection();
            // change parent id of dataset
            FileInfo source = FixParentId(datasetFile, ddbConnection);

            FileInfo response = new FileInfo(datasetFile + ".response.json");
            SaveToMappingStoreAndJson(source, response);

            var expectedEntities = ReadEntities<DatasetEntity>(source, EntityType.DataSet).ToList();

            // This test expects one and only one dataset
            Assume.That(expectedEntities, Has.Count.EqualTo(1));

            var expected = expectedEntities.First();

            var datasetFromMappingStore = _retrieveManager.GetEntities<DatasetEntity>(StoreId, EntityQuery.Empty, Detail.Full).FirstOrDefault(x => x.Name.Equals(expected.Name, StringComparison.Ordinal));
            Assume.That(datasetFromMappingStore, Is.Not.Null);

            FileInfo sourceColumns = FixParentId(datasetColumnFile, datasetFromMappingStore);
            FileInfo responseColumns = new FileInfo(datasetColumnFile + ".response.json");
            SaveToMappingStoreAndJson(sourceColumns, responseColumns);

            // environment set

            PatchRequest patchRequest = PatchRequestParser.ParsePatch(patchFile);
            var dataSetEngine = _persistManager.GetEngine<DatasetEntity>(StoreId);
            dataSetEngine.Update(patchRequest, EntityType.DataSet, datasetFromMappingStore.EntityId);
            var updatedDatasetFromMappingStore = _retrieveManager.GetEntities<DatasetEntity>(StoreId, datasetFromMappingStore.QueryForThisEntityId(), Detail.Full).FirstOrDefault();
            // Delete the dataset, it should delete also the dataset columns
            _persistManager.Delete<DatasetEntity>(StoreId, datasetFromMappingStore.EntityId);
            _persistManager.Delete<IConnectionEntity>(StoreId, ddbConnection.EntityId);

            Assert.NotNull(updatedDatasetFromMappingStore);

            Assert.AreEqual(updatedDatasetFromMappingStore.Name, patchRequest.FirstOrDefault(p => "/name".Equals(p.Path, StringComparison.OrdinalIgnoreCase))?.Value?.ToString());
            var patchPermissions = patchRequest
                .Where(p => "/permissions".Equals(p.Path, StringComparison.OrdinalIgnoreCase))
                .Select(v => v.Value)
                .FirstOrDefault() as JObject;

            var patchPermUrns = patchPermissions.Children().Cast<JProperty>().Select(p => p.Name).ToArray();

            CollectionAssert.AreEquivalent(updatedDatasetFromMappingStore.Permissions.Keys, patchPermUrns);
        }

        private static FileInfo FixParentId(string datasetFile, IEntity parentEntity)
        {
            FileInfo original = new FileInfo(datasetFile);
            if (!parentEntity.EntityId.Equals("14", StringComparison.Ordinal))
            {
                FileInfo originalWithFixedParent = new FileInfo(datasetFile + ".parent.json");
                using (var writer = originalWithFixedParent.CreateText())
                {
                    writer.WriteLine(
                        Regex.Replace(original.OpenText().ReadToEnd(), "(\"parentId\"):\\s*\"\\d+\"", $"$1: \"{parentEntity.EntityId}\""));
                    writer.Flush();
                }
                return originalWithFixedParent;
            }
            else
            {
                return original;
            }
        }

        private IEnumerable<T> RetrieverEntties<T>(FileInfo output, EntityType type, IEntityQuery entityQuery) where T : IEntity
        {
            // This retrieval doesn't work because the json written cannot be parsed
            using (Stream stream = output.Create())
            using (var writer = new EntityStreamingWriter(stream))
            {
                _retrieveManager.WriteEntities(StoreId, writer, type, entityQuery, Detail.Full);
            }

            return ReadEntities<T>(output, type);
        }
        private void DeleteByName<T>(T entity) where T : ISimpleNameableEntity
        {
            var entities = _retrieveManager.GetEntities<T>(this.StoreId,
                new EntityQuery() { ObjectId = new Criteria<string>(OperatorType.Exact, entity.Name) }, Detail.IdOnly);
            foreach (var simpleNameableEntity in entities)
            {
                _persistManager.Delete<T>(StoreId, simpleNameableEntity.EntityId);
            }
        }
    }
}
