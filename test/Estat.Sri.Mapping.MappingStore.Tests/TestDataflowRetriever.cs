using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using DryIoc;
using Estat.Sdmxsource.Extension.Constant;
using Estat.Sdmxsource.Extension.Model.Error;
using Estat.Sri.Mapping.MappingStore.Tests;
using Estat.Sri.MappingStoreRetrieval.Engine;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using log4net;
using log4net.Config;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
/// <summary>
/// Test unit for testing retrieval of DSD
/// </summary>
[TestFixture("sqlserver")]
[TestFixture("odp")]
[TestFixture("mysql")]
public class TestDsdRetreival : BaseTestClassWithContainer
{
    private ICommonSdmxObjectRetrievalManager _retrievalManager;

    public TestDsdRetreival(string storeId)
        : base(storeId)
    {
        var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
        XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
        var connectionStringSettings = GetConnectionStringSettings();
        var commonSdmxObjectRetrievalFactory = this.IoCContainer.Resolve<ICommonSdmxObjectRetrievalFactory>();
        _retrievalManager = commonSdmxObjectRetrievalFactory.GetManager(connectionStringSettings);
    }

    [Test]
    public void TestRetrievingDataflowsWithReferenceChildren()
    {
        Stopwatch stopwatch = Stopwatch.StartNew();
        ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
            .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.Null)
            .SetMaintainableTarget(SdmxStructureEnumType.Dataflow)
            .SetReferences(StructureReferenceDetailEnumType.Children)
            .Build();

        var commonQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, null)
                .SetMaintainableTarget(SdmxStructureEnumType.Dataflow)
                .SetReferences(StructureReferenceDetailEnumType.Descendants)
                .Build();
        var dataflowsAndDepds = _retrievalManager.GetMaintainables(commonQuery);


        Assert.IsNotNull(dataflowsAndDepds);
        Assert.IsNotEmpty(dataflowsAndDepds.Dataflows);
        Assert.IsNotEmpty(dataflowsAndDepds.DataStructures);
        Assert.IsNotEmpty(dataflowsAndDepds.Codelists);
        Assert.IsNotEmpty(dataflowsAndDepds.ConceptSchemes);

        stopwatch.Stop();
        var seconds = stopwatch.Elapsed.TotalSeconds;
        Console.Write("Retrieve time :" + stopwatch.Elapsed);

        Assert.That(seconds, Is.LessThan(30));
    }
}