﻿// -----------------------------------------------------------------------
// <copyright file="TestInitilization.cs" company="EUROSTAT">
//   Date Created : 2022-08-22
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests;
  [TestFixture("msdb_scratch.oracle")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.sqlserver")]
public class TestInit : BaseTestClassWithContainer
{
    public TestInit(string storeId) : base(storeId)
    {
    }

    [Test]
    public void ShouldInitializeDatabase()
    {
        var mappingStoreManager = IoCContainer.Resolve<IMappingStoreManager>();
        var mappingStoreEngine = mappingStoreManager.GetEngineByStoreId(StoreId);
        Assert.That(mappingStoreEngine, Is.Not.Null);
        var actionResult = mappingStoreEngine.Initialize(new DatabaseIdentificationOptions() {StoreId = this.StoreId});
        Assert.That(actionResult.Status, Is.EqualTo(StatusType.Success), string.Join("==============\n", actionResult.Messages));
    }

    [Test]
    public void ShouldUpgradeDatabase()
    {
         var mappingStoreManager = IoCContainer.Resolve<IMappingStoreManager>();
        var mappingStoreEngine = mappingStoreManager.GetEngineByStoreId(StoreId);
        Assert.That(mappingStoreEngine, Is.Not.Null);
        var actionResult = mappingStoreEngine.Upgrade(new DatabaseIdentificationOptions() {StoreId = this.StoreId}, Version.Parse("7.0"), false);
        Assert.That(actionResult.Status, Is.EqualTo(StatusType.Success), string.Join("==============\n", actionResult.Messages));
    }
}