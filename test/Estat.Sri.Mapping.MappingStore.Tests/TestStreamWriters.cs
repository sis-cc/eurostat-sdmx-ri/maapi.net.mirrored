// -----------------------------------------------------------------------
// <copyright file="TestStreamWriters.cs" company="EUROSTAT">
//   Date Created : 2019-12-16
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Engine.Streaming;
using Estat.Sri.Mapping.Api.Extension;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Model.AdvancedTime;
using Estat.Sri.Mapping.Api.Utils;
using Estat.Sri.Mapping.MappingStore.Extension;
using FluentAssertions;
using FluentAssertions.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    class TestStreamWriters
    {
        [Test]
        public void DdbConnectionEntityTest()
        {
            FileInfo outputFile = new FileInfo("ddbConectionEntity-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/ddbConectionEntity.json");
            ConnectionEntity connectionEntity = new ConnectionEntity();
            connectionEntity.Name = "NA_MAIN";
            connectionEntity.DatabaseVendorType = "MySQL";
            connectionEntity.SubType = string.Empty;
            connectionEntity.Permissions = new Dictionary<string, string>();
            connectionEntity.Settings.Add("database", new ConnectionParameterEntity() { Value = "mastore", Advanced = false, Required = true });
            connectionEntity.Settings.Add("server", new ConnectionParameterEntity() { Value = "localhost", Advanced = false, Required = true });
            connectionEntity.Settings.Add("user id", new ConnectionParameterEntity() { Value = "sdmxuser", Advanced = false, Required = true });
            connectionEntity.Settings.Add("password", new ConnectionParameterEntity() { Value = "sdmxpass", Advanced = false, Required = true, DataType = ParameterType.Password });
            connectionEntity.Settings.Add("port", new ConnectionParameterEntity() { Value = 3306, Advanced = false, Required = true, DataType = ParameterType.Number, DefaultValue = 3306 });


            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.DdbConnectionSettings);
                writer.Write(connectionEntity);
            }

            Compare(outputFile, expectedResult);
        }

        [Test]
        public void DataSourceEntityTest()
        {
            FileInfo outputFile = new FileInfo("dataSource-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/dataSource.json");
            var dataSourceEntity = new DataSourceEntity()
            {
                DataUrl = "http://url_to_data_or_rest_or_soap",
                WSDLUrl = "http://url_to_wsdl_or_null",
                WADLUrl = "http://url_to_wadl_or_null",
                IsWs = true,
                IsRest = false,
                IsSimple = false,
            };

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.DataSource);
                writer.Write(dataSourceEntity);
            }

            Compare(outputFile, expectedResult);
        }

        [Test]
        public void HeaderTemplateTest()
        {
             FileInfo outputFile = new FileInfo("headerTemplateEntity-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/headerTemplateEntity.json");
            HeaderEntity headerEntity = new HeaderEntity();
            headerEntity.EntityId = "1";
            headerEntity.ParentId = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:61_69(1.0)";

            Dictionary<string, string> additionalProperties = new Dictionary<string, string> { { "DataSetAgency", "Test Agency" } };

            IList<ITextTypeWrapper> names = new List<ITextTypeWrapper>();
            names.Add(new TextTypeWrapperImpl("en", "Romania INE", null));
            IList<IContact> contacts = new List<IContact>();
            var senderContact = new ContactCore();
            senderContact.Name.Add(new TextTypeWrapperImpl("en", "Smith", senderContact));
            senderContact.Departments.Add(new TextTypeWrapperImpl("en", "CENSUS", senderContact));
            senderContact.Role.Add(new TextTypeWrapperImpl("en", "Admin", senderContact));
            senderContact.Email.Add("test@somewhere.org");
            
            contacts.Add(senderContact);
            IParty sender = new PartyCore(names, "RO1", contacts, null);

            IList<ITextTypeWrapper> receiverName = new List<ITextTypeWrapper>();
            receiverName.Add(new TextTypeWrapperImpl("en", "Ministry of Truth", null));

            IList<IContact> receiverContacts = new List<IContact>();
            var receiverContact = new ContactCore();
            receiverContact.Name.Add(new TextTypeWrapperImpl("en", "someone", null));
            receiverContact.Role.Add(new TextTypeWrapperImpl("en", "aRole", null));
            receiverContact.Email.Add("someone@example.com");
            receiverContacts.Add(receiverContact);
            
            IParty receiver = new PartyCore(receiverName, "ZZ9", receiverContacts, null);
            headerEntity.SdmxHeader = new HeaderImpl(additionalProperties, null, null, null, null, null, null, null, null, null, null, null, null, new IParty[0], sender, false);
            headerEntity.SdmxHeader.AddName(new TextTypeWrapperImpl("en", "header name", null));
            headerEntity.SdmxHeader.AddReciever(receiver);

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.Header);
                writer.Write(headerEntity);
            }

            Compare(outputFile, expectedResult);
        }
        [Test]
        public void HeaderTemplateLocaleTest()
        {
             FileInfo outputFile = new FileInfo("headerTemplateMultipleNamesRolesAndDeparmentsEntity-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/headerTemplateMultipleNamesRolesAndDeparmentsEntity.json");
            HeaderEntity headerEntity = new HeaderEntity();
            headerEntity.EntityId = "1";
            headerEntity.ParentId = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:61_69(1.0)";

            Dictionary<string, string> additionalProperties = new Dictionary<string, string> { { "DataSetAgency", "Test Agency" } };

            IList<ITextTypeWrapper> names = new List<ITextTypeWrapper>();
            names.Add(new TextTypeWrapperImpl("en", "Romania INE", null));
            IList<IContact> contacts = new List<IContact>();
            var senderContact = new ContactCore();
            senderContact.Name.Add(new TextTypeWrapperImpl("en", "Smith", senderContact));
            senderContact.Departments.Add(new TextTypeWrapperImpl("en", "CENSUS", senderContact));
            senderContact.Role.Add(new TextTypeWrapperImpl("en", "Admin", senderContact));
            senderContact.Email.Add("test@somewhere.org");
            
            contacts.Add(senderContact);
            IParty sender = new PartyCore(names, "RO1", contacts, null);

            IList<ITextTypeWrapper> receiverName = new List<ITextTypeWrapper>();
            receiverName.Add(new TextTypeWrapperImpl("en", "Ministry of Truth", null));

            IList<IContact> receiverContacts = new List<IContact>();
            var receiverContact = new ContactCore();
            receiverContact.Name.Add(new TextTypeWrapperImpl("en", "someone", null));
            receiverContact.Name.Add(new TextTypeWrapperImpl("el", "onoma1", null));
            receiverContact.Departments.Add(new TextTypeWrapperImpl("en", "contactDepartment", null));
            receiverContact.Departments.Add(new TextTypeWrapperImpl("el", "tmima1", null));
            receiverContact.Role.Add(new TextTypeWrapperImpl("en", "aRole", null));
            receiverContact.Role.Add(new TextTypeWrapperImpl("de", "bRole", null));
            receiverContact.Email.Add("someone@example.com");
            receiverContacts.Add(receiverContact);
            
            IParty receiver = new PartyCore(receiverName, "ZZ9", receiverContacts, null);
            headerEntity.SdmxHeader = new HeaderImpl(additionalProperties, null, null, null, null, null, null, null, null, null, null, null, null, new IParty[0], sender, false);
            headerEntity.SdmxHeader.AddName(new TextTypeWrapperImpl("en", "header name", null));
            headerEntity.SdmxHeader.AddReciever(receiver);

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.Header);
                writer.Write(headerEntity);
            }

            Compare(outputFile, expectedResult);
        }

        [Test]
        public void AdvancedTimeTranscodingTest()
        {
            string mappingSetEntityId = "8";
            string timeMappingEntityId = "9";
            string timeTranscodingEntityId = "6";
            string durationTranscodingEntity = "11";
            string datasetEntityId = "2";
            // columns
            int columnIdx = 1;
            DataSetColumnEntity criteriaColumn= new DataSetColumnEntity() { Name = "CRITERIAL_COLUMN", EntityId = $"{columnIdx++}", ParentId = datasetEntityId };
            DataSetColumnEntity durationColumnNonCompliant = new DataSetColumnEntity() { ParentId = datasetEntityId, EntityId = $"{columnIdx++}", Name = "DURATION_COLUMN" };
            DataSetColumnEntity durationCompliantColumn = new DataSetColumnEntity() { ParentId = datasetEntityId, EntityId = $"{columnIdx++}", Name = "DURATION_COLUMN_C" };
            DataSetColumnEntity yearColumn = new DataSetColumnEntity() { Name = "YEAR", EntityId = $"{columnIdx++}", ParentId = datasetEntityId };
            DataSetColumnEntity periodMonthColumn = new DataSetColumnEntity() { Name = "MONTH", EntityId = $"{columnIdx++}", ParentId = datasetEntityId };
            DataSetColumnEntity yearQuarterColumn = new DataSetColumnEntity() { Name = "YEAR_QUARTER", EntityId = $"{columnIdx++}", ParentId = datasetEntityId };
            DataSetColumnEntity isoRangeStartEndColumn = new DataSetColumnEntity() { Name = "START_END", EntityId = $"{columnIdx++}", ParentId = datasetEntityId };
            DataSetColumnEntity isoRangeEndDurColumn = new DataSetColumnEntity() { Name = "END_DUR", EntityId = $"{columnIdx++}", ParentId = datasetEntityId };
            DataSetColumnEntity isoDateColumn = new DataSetColumnEntity() { Name = "ISO_DATE", EntityId = $"{columnIdx++}", ParentId = datasetEntityId };
            DataSetColumnEntity gregorianMonthColumn = new DataSetColumnEntity() { Name = "YEAR_MONTH", EntityId = $"{columnIdx++}", ParentId = datasetEntityId };
            DataSetColumnEntity dateTimeColumn = new DataSetColumnEntity() { Name = "DATE_COL", EntityId = $"{columnIdx++}", ParentId = datasetEntityId };

            FileInfo outputFile = new FileInfo("transcoding-adv-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/transcoding-advanced.json");
            var timeDimensionMappingEntity = new TimeDimensionMappingEntity();
            timeDimensionMappingEntity.EntityId = timeTranscodingEntityId;
            timeDimensionMappingEntity.ParentId = timeMappingEntityId;
            timeDimensionMappingEntity.Transcoding = new TimeTranscodingAdvancedEntity();
            timeDimensionMappingEntity.Transcoding.CriteriaColumn = criteriaColumn;

            // duration maps
            int durationMapsIdx = 10;
            var durationMapNonCompliant = new DurationMappingEntity()
            {
                Column = durationCompliantColumn, 
                EntityId = durationMapsIdx.ToString(CultureInfo.InvariantCulture),
                ParentId = mappingSetEntityId
            };
            durationMapNonCompliant.TranscodingRules["P1Y"] = "1y";
            durationMapNonCompliant.TranscodingRules["P1M"] = "1m";

            durationMapsIdx++;
            var durationMapCompliant = new DurationMappingEntity()
            {
                Column = durationCompliantColumn,
                EntityId = durationMapsIdx.ToString(CultureInfo.InvariantCulture),
                ParentId = mappingSetEntityId
            };
            durationMapsIdx++;
            var durationMapConstant = new DurationMappingEntity()
            {
                ConstantValue = "P1Y",
                EntityId = durationMapsIdx.ToString(CultureInfo.InvariantCulture),
                ParentId = mappingSetEntityId
            };

            // non compliant start with non compliant duration
            {
                TimeFormatConfiguration rangedDuration1 = new TimeFormatConfiguration();
                rangedDuration1.CriteriaValue = "start_nc_a_duration_nc";
                rangedDuration1.OutputFormat = TimeFormat.TimeRange;

                TimeParticleConfiguration startNoCompliant = new TimeParticleConfiguration() { Format = DisseminationDatabaseTimeFormat.Annual, Year = new TimeRelatedColumnInfo() { Start = 0, Length = 4, Column = yearColumn } };
                rangedDuration1.SetConfiguration(startNoCompliant, durationMapNonCompliant);
                timeDimensionMappingEntity.Transcoding.Add(rangedDuration1);
            }
            {
                TimeFormatConfiguration rangedDuration2 = new TimeFormatConfiguration();
                rangedDuration2.CriteriaValue = "start_nc_m_duration_nc";
                rangedDuration2.OutputFormat = TimeFormat.TimeRange;
                PeriodTimeRelatedColumnInfo periodTimeRelatedColumnInfo = new PeriodTimeRelatedColumnInfo() { Start = 0, Length = 3, Column = periodMonthColumn };
                periodTimeRelatedColumnInfo.AddRule("01", "JAN");
                periodTimeRelatedColumnInfo.AddRule("02", "FEB");
                periodTimeRelatedColumnInfo.AddRule("03", "MAR");
                periodTimeRelatedColumnInfo.AddRule("06", "JUN");
                periodTimeRelatedColumnInfo.AddRule("12", "DEC");
                TimeParticleConfiguration startNoCompliant = new TimeParticleConfiguration() { 
                    Format = DisseminationDatabaseTimeFormat.Monthly, 
                    Year = new TimeRelatedColumnInfo() { Start = 0, Length = 4, Column = yearColumn }, 
                    Period = periodTimeRelatedColumnInfo
                };
                rangedDuration2.SetConfiguration(startNoCompliant, durationMapNonCompliant);
                timeDimensionMappingEntity.Transcoding.Add(rangedDuration2);
            }
            {
                TimeFormatConfiguration rangedDuration2 = new TimeFormatConfiguration();
                rangedDuration2.CriteriaValue = "end_nc_m_duration_nc";
                rangedDuration2.OutputFormat = TimeFormat.TimeRange;
                PeriodTimeRelatedColumnInfo periodTimeRelatedColumnInfo = new PeriodTimeRelatedColumnInfo() { Start = 0, Length = 3, Column = periodMonthColumn };
                periodTimeRelatedColumnInfo.AddRule("01", "JAN");
                periodTimeRelatedColumnInfo.AddRule("02", "FEB");
                periodTimeRelatedColumnInfo.AddRule("03", "MAR");
                periodTimeRelatedColumnInfo.AddRule("06", "JUN");
                periodTimeRelatedColumnInfo.AddRule("12", "DEC");
                TimeParticleConfiguration endNonCompliant = new TimeParticleConfiguration() { 
                    Format = DisseminationDatabaseTimeFormat.Monthly, 
                    Year = new TimeRelatedColumnInfo() { Start = 0, Length = 4, Column = yearColumn }, 
                    Period = periodTimeRelatedColumnInfo
                };
                rangedDuration2.SetConfiguration(durationMapNonCompliant, endNonCompliant);
                timeDimensionMappingEntity.Transcoding.Add(rangedDuration2);
            }
            {
                TimeFormatConfiguration rangedDuration2 = new TimeFormatConfiguration();
                rangedDuration2.CriteriaValue = "end_nc_m_duration_c";
                rangedDuration2.OutputFormat = TimeFormat.TimeRange;
                PeriodTimeRelatedColumnInfo periodTimeRelatedColumnInfo = new PeriodTimeRelatedColumnInfo() { Start = 0, Length = 3, Column = periodMonthColumn };
                periodTimeRelatedColumnInfo.AddRule("01", "JAN");
                periodTimeRelatedColumnInfo.AddRule("02", "FEB");
                periodTimeRelatedColumnInfo.AddRule("03", "MAR");
                periodTimeRelatedColumnInfo.AddRule("06", "JUN");
                periodTimeRelatedColumnInfo.AddRule("12", "DEC");
                TimeParticleConfiguration endNonCompliant = new TimeParticleConfiguration()
                {
                    Format = DisseminationDatabaseTimeFormat.Monthly,
                    Year = new TimeRelatedColumnInfo() { Start = 0, Length = 4, Column = yearColumn },
                    Period = periodTimeRelatedColumnInfo
                };
                rangedDuration2.SetConfiguration(durationMapCompliant, endNonCompliant);
                timeDimensionMappingEntity.Transcoding.Add(rangedDuration2);
            }
            {
                TimeFormatConfiguration rangedDuration2 = new TimeFormatConfiguration();
                rangedDuration2.CriteriaValue = "end_c_duration_c";
                rangedDuration2.OutputFormat = TimeFormat.TimeRange;
                TimeParticleConfiguration endIsoConfig = new TimeParticleConfiguration()
                {
                    Format = DisseminationDatabaseTimeFormat.Iso8601Compatible,
                    DateColumn = isoDateColumn
                };
                rangedDuration2.SetConfiguration(durationMapCompliant, endIsoConfig);
                timeDimensionMappingEntity.Transcoding.Add(rangedDuration2);
            }
            {
                TimeFormatConfiguration rangedDuration2 = new TimeFormatConfiguration();
                rangedDuration2.CriteriaValue = "start_c_duration_cons";
                rangedDuration2.OutputFormat = TimeFormat.TimeRange;
                TimeParticleConfiguration startDateTime = new TimeParticleConfiguration()
                {
                    Format = DisseminationDatabaseTimeFormat.TimestampOrDate,
                    DateColumn = dateTimeColumn
                };
                rangedDuration2.SetConfiguration(startDateTime, durationMapConstant);
                timeDimensionMappingEntity.Transcoding.Add(rangedDuration2);
            }
            {
                TimeFormatConfiguration rangedDuration2 = new TimeFormatConfiguration();
                rangedDuration2.CriteriaValue = "start_end_2_col";
                rangedDuration2.OutputFormat = TimeFormat.TimeRange;
                TimeParticleConfiguration startDateTime = new TimeParticleConfiguration()
                {
                    Format = DisseminationDatabaseTimeFormat.TimestampOrDate,
                    DateColumn = dateTimeColumn
                };
                TimeParticleConfiguration endIsoConfig = new TimeParticleConfiguration()
                {
                    Format = DisseminationDatabaseTimeFormat.Iso8601Compatible,
                    DateColumn = isoDateColumn
                };
 
                rangedDuration2.SetConfiguration(startDateTime, endIsoConfig);
                timeDimensionMappingEntity.Transcoding.Add(rangedDuration2);
            }
            {
                TimeFormatConfiguration rangedDuration2 = new TimeFormatConfiguration();
                rangedDuration2.CriteriaValue = "start_nc_end_2_col_non_compl";
                rangedDuration2.OutputFormat = TimeFormat.TimeRange;
                PeriodTimeRelatedColumnInfo quarter = new PeriodTimeRelatedColumnInfo() { Start = 5, Length = 2, Column = yearQuarterColumn };
                quarter.AddRule("Q1", "T1");
                quarter.AddRule("Q2", "T2");
                quarter.AddRule("Q3", "T3");
                quarter.AddRule("Q4", "T4");
                TimeParticleConfiguration startDateTime = new TimeParticleConfiguration()
                {
                    Format = DisseminationDatabaseTimeFormat.Quarterly,
                    Year = new TimeRelatedColumnInfo() { Start = 0, Length = 4, Column = yearQuarterColumn },
                    Period = quarter
                };
                PeriodTimeRelatedColumnInfo periodTimeRelatedColumnInfo = new PeriodTimeRelatedColumnInfo() { Start = 0, Length = 3, Column = periodMonthColumn };
                periodTimeRelatedColumnInfo.AddRule("01", "JAN");
                periodTimeRelatedColumnInfo.AddRule("02", "FEB");
                periodTimeRelatedColumnInfo.AddRule("03", "MAR");
                periodTimeRelatedColumnInfo.AddRule("06", "JUN");
                periodTimeRelatedColumnInfo.AddRule("06", "JUL");
                periodTimeRelatedColumnInfo.AddRule("12", "DEC");
                TimeParticleConfiguration endNonCompliant = new TimeParticleConfiguration()
                {
                    Format = DisseminationDatabaseTimeFormat.Monthly,
                    Year = new TimeRelatedColumnInfo() { Start = 0, Length = 4, Column = yearColumn },
                    Period = periodTimeRelatedColumnInfo
                };

                rangedDuration2.SetConfiguration(startDateTime, endNonCompliant);
                timeDimensionMappingEntity.Transcoding.Add(rangedDuration2);
            }
            {
                TimeFormatConfiguration rangedDuration2 = new TimeFormatConfiguration();
                rangedDuration2.CriteriaValue = "start_end_2_col_non_compl";
                rangedDuration2.OutputFormat = TimeFormat.TimeRange;
                TimeParticleConfiguration startDateTime = new TimeParticleConfiguration()
                {
                    Format = DisseminationDatabaseTimeFormat.TimestampOrDate,
                    DateColumn = dateTimeColumn
                };
                PeriodTimeRelatedColumnInfo periodTimeRelatedColumnInfo = new PeriodTimeRelatedColumnInfo() { Start = 0, Length = 3, Column = periodMonthColumn };
                periodTimeRelatedColumnInfo.AddRule("01", "JAN");
                periodTimeRelatedColumnInfo.AddRule("02", "FEB");
                periodTimeRelatedColumnInfo.AddRule("03", "MAR");
                periodTimeRelatedColumnInfo.AddRule("06", "JUN");
                periodTimeRelatedColumnInfo.AddRule("12", "DEC");
                TimeParticleConfiguration endNonCompliant = new TimeParticleConfiguration()
                {
                    Format = DisseminationDatabaseTimeFormat.Monthly,
                    Year = new TimeRelatedColumnInfo() { Start = 0, Length = 4, Column = yearColumn },
                    Period = periodTimeRelatedColumnInfo
                };

                rangedDuration2.SetConfiguration(startDateTime, endNonCompliant);
                timeDimensionMappingEntity.Transcoding.Add(rangedDuration2);
            }
            {
                TimeFormatConfiguration rangedDuration2 = new TimeFormatConfiguration();
                rangedDuration2.CriteriaValue = "start_end_1_col";
                rangedDuration2.OutputFormat = TimeFormat.TimeRange;
                rangedDuration2.SetConfiguration(isoRangeStartEndColumn);
                timeDimensionMappingEntity.Transcoding.Add(rangedDuration2);
            }
            {
                TimeFormatConfiguration rangedDuration2 = new TimeFormatConfiguration();
                rangedDuration2.CriteriaValue = "end_dur_1_col";
                rangedDuration2.OutputFormat = TimeFormat.TimeRange;
                rangedDuration2.SetConfiguration(isoRangeEndDurColumn);
                timeDimensionMappingEntity.Transcoding.Add(rangedDuration2);
            }
            {
                TimeFormatConfiguration standardTime = new TimeFormatConfiguration();
                standardTime.CriteriaValue = "reporting_year";
                standardTime.OutputFormat = TimeFormat.ReportingYear;
                TimeParticleConfiguration standard = new TimeParticleConfiguration() { Format = DisseminationDatabaseTimeFormat.Annual, Year = new TimeRelatedColumnInfo() { Column = yearColumn, Length = 4, Start = 0 } };
                standardTime.SetConfiguration(standard);
                timeDimensionMappingEntity.Transcoding.Add(standardTime);
            }
            {
                TimeFormatConfiguration standardTime = new TimeFormatConfiguration();
                standardTime.CriteriaValue = "reporting_quarter";
                standardTime.OutputFormat = TimeFormat.QuarterOfYear;
                TimeParticleConfiguration standard = new TimeParticleConfiguration() { Format = DisseminationDatabaseTimeFormat.Quarterly, Year = new TimeRelatedColumnInfo() { Column = yearQuarterColumn, Length = 4, Start = 0 } };
                standard.Period = new PeriodTimeRelatedColumnInfo() { Start = 5, Length = 1, Column = yearQuarterColumn };
                standard.Period.AddRule("Q1", "1");
                standard.Period.AddRule("Q2", "2");
                standard.Period.AddRule("Q3", "3");
                standard.Period.AddRule("Q4", "4");

                standardTime.SetConfiguration(standard);
                timeDimensionMappingEntity.Transcoding.Add(standardTime);
            }
            {
                TimeFormatConfiguration standardTime = new TimeFormatConfiguration();
                standardTime.CriteriaValue = "reporting_month";
                standardTime.OutputFormat = TimeFormat.QuarterOfYear;
                TimeParticleConfiguration standard = new TimeParticleConfiguration() { Format = DisseminationDatabaseTimeFormat.Iso8601Compatible,  DateColumn = gregorianMonthColumn };
                standardTime.SetConfiguration(standard);
                timeDimensionMappingEntity.Transcoding.Add(standardTime);
            }
            {
                TimeFormatConfiguration standardTime = new TimeFormatConfiguration();
                standardTime.CriteriaValue = "date";
                standardTime.OutputFormat = TimeFormat.Date;
                TimeParticleConfiguration standard = new TimeParticleConfiguration() { Format = DisseminationDatabaseTimeFormat.TimestampOrDate, DateColumn = dateTimeColumn };
                standardTime.SetConfiguration(standard);
                timeDimensionMappingEntity.Transcoding.Add(standardTime);
            }
            {
                TimeFormatConfiguration standardTime = new TimeFormatConfiguration();
                standardTime.CriteriaValue = "yearMonth";
                standardTime.OutputFormat = TimeFormat.Month;
                TimeParticleConfiguration standard = new TimeParticleConfiguration() { Format = DisseminationDatabaseTimeFormat.Quarterly, Year = new TimeRelatedColumnInfo() { Column = yearColumn, Length = 4, Start = 0 } };
                standard.Period = new PeriodTimeRelatedColumnInfo() { Start = 0, Length = 3, Column = periodMonthColumn };
                standard.Period.AddRule("01", "JAN");
                standard.Period.AddRule("02", "FEB");
                standard.Period.AddRule("03", "MAR");
                standard.Period.AddRule("06", "JUN");
                standard.Period.AddRule("12", "DEC");

                standardTime.SetConfiguration(standard);
                timeDimensionMappingEntity.Transcoding.Add(standardTime);
            }

            timeDimensionMappingEntity.AutoDetectTypeIfNotSet();

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.TimeMapping);
                writer.Write(timeDimensionMappingEntity);
            }

            Compare(outputFile, expectedResult);
        }

        [Test]
        public void TranscodingTest()
        {
            FileInfo outputFile = new FileInfo("transcoding-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/transcodingEntity.json");
            TimeDimensionMappingEntity entity = new TimeDimensionMappingEntity();
            entity.EntityId = "6";
            entity.ParentId = "6";
            TimeTranscoding yearConfig = new TimeTranscoding() { Column = GetTimePeriodColumn(), Start = 0, Length = 4 };
            TimeTranscodingEntity annual = new TimeTranscodingEntity() { Frequency = "A", Year = yearConfig };
            PeriodTimeTranscoding periodTimeTranscoding = new PeriodTimeTranscoding()
            {
                Column = GetTimePeriodColumn(),
                Start = 4,
                Length = 2
            };
            periodTimeTranscoding.AddRule("Q1", "01");
            periodTimeTranscoding.AddRule("Q2", "02");
            periodTimeTranscoding.AddRule("Q3", "03");
            periodTimeTranscoding.AddRule("Q4", "04");
            TimeTranscodingEntity quarter = new TimeTranscodingEntity() { Frequency = "Q", Year = yearConfig, Period = periodTimeTranscoding };
            
            var oldStyle = new List<TimeTranscodingEntity>();
            oldStyle.Add(annual);
            oldStyle.Add(quarter);
            entity.Transcoding = TimeTranscodingConversionHelper.Convert(oldStyle, "FREQ");

            entity.AutoDetectTypeIfNotSet();
            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.TimeMapping);
                writer.Write(entity);
            }

            Compare(outputFile, expectedResult);
        }

        [Test]
        public void LocalCodeTest()
        {
            FileInfo outputFile = new FileInfo("localCodeEntity-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/localCodeEntity.json");
            LocalCodeEntity localCodeEntity = new LocalCodeEntity();
            /*
              "id": "0010",
              "parentId": "1",
              "entityId": "19553"
             */
            localCodeEntity.EntityId = "19553";
            localCodeEntity.ParentId = "1";
            localCodeEntity.ObjectId = "0010";

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.LocalCode);
                writer.Write(localCodeEntity);
            }

            Compare(outputFile, expectedResult);
        }

        [Test]
        public void MappingSetTest()
        {
            FileInfo outputFile = new FileInfo("mappingSetEntity-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/mappingSetEntity.json");
            MappingSetEntity mappingSetEntity = new MappingSetEntity();
            mappingSetEntity.EntityId = "2";
            mappingSetEntity.DataSetId = "2";
            mappingSetEntity.ParentId = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:161_267(1.0)";
            mappingSetEntity.Name = "MS_6_161_267";
            mappingSetEntity.Description = "MS_6_161_267";
            mappingSetEntity.ValidFrom = DateTime.Parse("2025-01-13T14:30:00");
            mappingSetEntity.IsMetadata = true;

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.MappingSet);
                writer.Write(mappingSetEntity);
            }

            Compare(outputFile, expectedResult);
        }
           [Test]
        public void WriteTimeMappingTest()
        {
            FileInfo outputFile = new FileInfo("timeMappingEntity-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/timeMappingEntity.json");
            TimeDimensionMappingEntity mapping = new TimeDimensionMappingEntity();
            mapping.EntityId = "377";
            mapping.ParentId = "377";
            mapping.SimpleMappingColumn = "TIME_COL";
            mapping.AutoDetectTypeIfNotSet();

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.TimeMapping);
                writer.Write(mapping);
            }

            Compare(outputFile, expectedResult);
        }
        [Test]
        public void WriteTimeMappingPreFormattedTest()
        {
            FileInfo outputFile = new FileInfo("timeMappingEntity-preformatted-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/timeMappingEntity-preformatted.json");
            TimeDimensionMappingEntity mapping = new TimeDimensionMappingEntity();
            mapping.EntityId = "377";
            mapping.ParentId = "377";
            mapping.PreFormatted = new TimePreFormattedEntity();
            mapping.PreFormatted.OutputColumn = new DataSetColumnEntity() { Name = "TIME_COL" };
            mapping.PreFormatted.FromColumn = new DataSetColumnEntity() { Name = "TIME_FROM" };
            mapping.PreFormatted.ToColumn = new DataSetColumnEntity() { Name = "TIME_TO" };
            mapping.AutoDetectTypeIfNotSet();

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.TimeMapping);
                writer.Write(mapping);
            }

            Compare(outputFile, expectedResult);
        }
        [Test]
        public void MappingTest()
        {
            FileInfo outputFile = new FileInfo("mappingEntity-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/mappingEntity.json");
            ComponentMappingEntity mapping = new ComponentMappingEntity();
            mapping.EntityId = "377";
            mapping.ParentId = "30";
            mapping.Component = new Component
            {
                ObjectId = "OBS_STATUS",
                EntityId = "10638"
            };
            mapping.AddColumn(new DataSetColumnEntity() { Description = "Description of column OBS_STATUS", Name = "sts_sdmx_transQ1_OBS_STATUS", EntityId = "644", ParentId = "36", IsMapped = ColumnMappingStatus.Yes });
            mapping.Type = "A";
            mapping.IsMetadata = true;

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.Mapping);
                writer.Write(mapping);
            }

            Compare(outputFile, expectedResult);
        }

        [Test]
        public void LastUpdatedEntityTest()
        {
            FileInfo outputFile = new FileInfo("lastupdated-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/lastupdatedentity.json");
            var mapping = new LastUpdatedEntity
            {
                EntityId = "377",
                ParentId = "377",
            };
            mapping.Column = new DataSetColumnEntity() { Description = "Description of column OBS_STATUS", Name = "sts_sdmx_transQ1_OBS_STATUS", EntityId = "644", ParentId = "36", IsMapped = ColumnMappingStatus.Yes };

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.LastUpdated);
                writer.Write(mapping);
            }

            Compare(outputFile, expectedResult);
        }
        [Test]
        public void LastUpdatedEntityTestConstant()
        {
            FileInfo outputFile = new FileInfo("lastupdated-constant-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/lastupdatedentity-constant.json");
            var mapping = new LastUpdatedEntity
            {
                EntityId = "377",
                ParentId = "377",
            };
            mapping.ConstantValue = "2021-04-20T14:30:00";

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.LastUpdated);
                writer.Write(mapping);
            }

            Compare(outputFile, expectedResult);
        }
        [Test]
        public void ValidToEntityTest()
        {
            FileInfo outputFile = new FileInfo("validtomapping-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/validtomappingentity.json");
            var mapping = new ValidToMappingEntity
            {
                EntityId = "377",
                ParentId = "377",
            };
            mapping.Column = new DataSetColumnEntity() { Description = "Description of column OBS_STATUS", Name = "sts_sdmx_transQ1_OBS_STATUS", EntityId = "644", ParentId = "36", IsMapped = ColumnMappingStatus.Yes };

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.ValidToMapping);
                writer.Write(mapping);
            }

            Compare(outputFile, expectedResult);
        }
        [Test]
        public void ValidToEntityTestConstant()
        {
            FileInfo outputFile = new FileInfo("validtomapping-constant-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/validtomappingentity-constant.json");
            var mapping = new ValidToMappingEntity
            {
                EntityId = "377",
                ParentId = "377",
            };
            mapping.ConstantValue = "2021-04-20T14:30:00";

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.ValidToMapping);
                writer.Write(mapping);
            }

            Compare(outputFile, expectedResult);
        }
        [Test]
        public void UpdatedStatusEntityTest()
        {
            FileInfo outputFile = new FileInfo("updatestatus-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/updatestatusentity.json");
            var mapping = new UpdateStatusEntity
            {
                EntityId = "377",
                ParentId = "377",
            };
            mapping.Column = new DataSetColumnEntity() { Description = "Description of column OBS_STATUS", Name = "sts_sdmx_transQ1_OBS_STATUS", EntityId = "644", ParentId = "36", IsMapped = ColumnMappingStatus.Yes };

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.UpdateStatusMapping);
                writer.Write(mapping);
            }

            Compare(outputFile, expectedResult);
        }
        [Test]
        public void UpdatedStatusEntityTestTranscoding()
        {
            FileInfo outputFile = new FileInfo("updatestatus-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/updatestatusentity-transcoding.json");
            var mapping = new UpdateStatusEntity
            {
                EntityId = "377",
                ParentId = "377",
            };
            mapping.Column = new DataSetColumnEntity() { Description = "Description of column OBS_STATUS", Name = "sts_sdmx_transQ1_OBS_STATUS", EntityId = "644", ParentId = "36", IsMapped = ColumnMappingStatus.Yes };
            mapping.Add(ObservationActionEnumType.Added, "insert");
            mapping.Add(ObservationActionEnumType.Updated, "replace");
            mapping.Add(ObservationActionEnumType.Deleted, "removed");

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.UpdateStatusMapping);
                writer.Write(mapping);
            }

            Compare(outputFile, expectedResult);
        }

        [Test]
        public void UpdatedStatusEntityTestConstant()
        {
            FileInfo outputFile = new FileInfo("updatestatus-constant-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/updatestatusentity-constant.json");
            var mapping = new UpdateStatusEntity
            {
                EntityId = "377",
                ParentId = "377",
            };
            mapping.Constant = ObservationAction.GetFromEnum(ObservationActionEnumType.Updated);
            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.UpdateStatusMapping);
                writer.Write(mapping);
            }

            Compare(outputFile, expectedResult);
        }

        [Test]
        public void TranscodingRuleTest()
        {
            FileInfo outputFile = new FileInfo("transcoding-rule-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/transcoding-rule.json");
            var rules = new List<TranscodingRuleEntity>();
            var rule1 = new TranscodingRuleEntity();
            rule1.EntityId = "2";
            rule1.ParentId = "2";
            rule1.DsdCodeEntity = new IdentifiableEntity() { ObjectId = "I" };
            rule1.LocalCodes = new List<LocalCodeEntity>() { new LocalCodeEntity() { ObjectId = "U" } };
            rules.Add(rule1);
            var rule2 = new TranscodingRuleEntity();
            rule2.EntityId = "3";
            rule2.ParentId = "2";
            rule2.DsdCodeEntity = new IdentifiableEntity() { ObjectId = "C" };
            rule2.LocalCodes = new List<LocalCodeEntity>() { new LocalCodeEntity() { ObjectId = "W" } };
            rules.Add(rule2);
                

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.TranscodingRule);
                writer.Write(rules);
            }

            Compare(outputFile, expectedResult);
        }

         [Test]
        public void RegistryTest()
        {
            FileInfo outputFile = new FileInfo("registry-writer.json");
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/registryEntity.json");
            RegistryEntity registryEntity = new RegistryEntity();
            registryEntity.EntityId = "1";
            registryEntity.Name = "New Registry Connection";
            registryEntity.Description = "mmm";
            registryEntity.Technology = "Rest";
            registryEntity.Url = "rrr";
            registryEntity.UserName = "eee";

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.Registry);
                writer.Write(registryEntity);
            }

            Compare(outputFile, expectedResult);
        }
        [Test]
        public void UserActionTest()
        {
            FileInfo outputFile = new FileInfo("useractionEntity-writer.json");
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/userActionEntity.json");
            UserActionEntity userAction = new UserActionEntity();
            userAction.EntityId = "1";
            userAction.EntityType = nameof(EntityType.DataSet);
            userAction.EntityName = "typea_1201";
            userAction.ActionWhen = new DateTime(2019, 11, 20, 10, 39, 36);
            userAction.UserName = "admin";
            userAction.OperationType = "Insert";

            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.UserAction);
                writer.Write(userAction);
            }

            Compare(outputFile, expectedResult);
        }
        [Test]
        public void UserTest()
        {
            FileInfo outputFile = new FileInfo("user-writer.json");
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/userEntity.json");
            UserEntity userEntity = new UserEntity();
            userEntity.EntityId = "2";
            userEntity.UserName = "user1";
            userEntity.Roles = new List<string>();
            userEntity.Roles.Add("CanReadData");
            userEntity.AddPermission("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30.31", "read_write");
            userEntity.AddPermission("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).6.157", "read_write");
            userEntity.AddPermission("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30", "read_write");
            userEntity.AddPermission("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:HC06(1.0)", "read_write");
            userEntity.AddPermission("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30.264D", "read_write");
            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.User);
                writer.Write(userEntity);
            }

            Compare(outputFile, expectedResult);
        }
        
        [Test]
        public void Dataset()
        {
            FileInfo outputFile = new FileInfo("datasetEntity-writer.json");
            // Note 
            FileInfo expectedResult = new FileInfo("tests/jsonWriter/datasetEntity.json");
            DatasetEntity dataset = new DatasetEntity();
            dataset.EntityId = "20052";
            dataset.ParentId = "4";
            dataset.Name = "xs measures";
            dataset.EditorType = "Estat.Ma.Ui.QueryEditors.MdiQueryEditorUC, Estat.Ma.Ui.QueryEditors";
            dataset.JSONQuery = @"{""Dataset"":{""ColumnInstances"":{""ColumnInstance"":[{""ID"":""CENSUS_TABLE_TYPE_B1_AGE"",""Type"":""nvarchar"",""Name"":""AGE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_CAS"",""Type"":""nvarchar"",""Name"":""CAS"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_GEO"",""Type"":""nvarchar"",""Name"":""GEO"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_MALE"",""Type"":""int"",""Name"":""MALE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_FEMALE"",""Type"":""int"",""Name"":""FEMALE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_TOTAL"",""Type"":""int"",""Name"":""TOTAL"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""}]},""TableInstances"":{""TableInstance"":[{""ID"":""CENSUS_TABLE_TYPE_B1"",""Name"":""CENSUS_TABLE_TYPE_B"",""XCoordinate"":""10"",""YCoordinate"":""10"",""ColumnInstanceRefs"":{""ColumnInstanceRef"":[{""ID"":""CENSUS_TABLE_TYPE_B1_AGE"",""Index"":""0""},{""ID"":""CENSUS_TABLE_TYPE_B1_CAS"",""Index"":""1""},{""ID"":""CENSUS_TABLE_TYPE_B1_GEO"",""Index"":""2""},{""ID"":""CENSUS_TABLE_TYPE_B1_MALE"",""Index"":""3""},{""ID"":""CENSUS_TABLE_TYPE_B1_FEMALE"",""Index"":""4""},{""ID"":""CENSUS_TABLE_TYPE_B1_TOTAL"",""Index"":""5""}]}}]},""Relations"":{""Relation"":[]}}}";
            dataset.Query = "select CENSUS_TABLE_TYPE_B1.AGE as CENSUS_TABLE_TYPE_B1_AGE, CENSUS_TABLE_TYPE_B1.CAS as CENSUS_TABLE_TYPE_B1_CAS, CENSUS_TABLE_TYPE_B1.GEO as CENSUS_TABLE_TYPE_B1_GEO, CENSUS_TABLE_TYPE_B1.MALE as CENSUS_TABLE_TYPE_B1_MALE, CENSUS_TABLE_TYPE_B1.FEMALE as CENSUS_TABLE_TYPE_B1_FEMALE, CENSUS_TABLE_TYPE_B1.TOTAL as CENSUS_TABLE_TYPE_B1_TOTAL\r\n from CENSUS_TABLE_TYPE_B CENSUS_TABLE_TYPE_B1\r\n";
            dataset.OrderByClause = "ORDER BY CENSUS_TABLE_TYPE_B1.AGE\r\n";
            dataset.Permissions = new Dictionary<string, string>();
            // TODO maybe always print description ?
            dataset.Description = "A dataset description";
            using (Stream outputStream = outputFile.Create())
            using (IEntityStreamingWriter writer = new EntityStreamingWriter(outputStream))
            {
                writer.StartMessage();
                writer.StartSection(EntityType.DataSet);
                writer.Write(dataset);
            }

            Compare(outputFile, expectedResult);
        }
        private static DataSetColumnEntity GetTimePeriodColumn()
        {
            return new DataSetColumnEntity() { EntityId = "21", Name = "TIME_PERIOD", IsMapped = ColumnMappingStatus.YesAndTranscoded };
        }

        private static void Compare(FileInfo outputFile, FileInfo expectedResult)
        {
            using (JsonReader resultReader = new JsonTextReader(outputFile.OpenText()))
            using (JsonReader expectedReader = new JsonTextReader(expectedResult.OpenText()))
            {
                var result = JToken.Load(resultReader);
                var expected = JToken.Load(expectedReader);
                result.Should().BeEquivalentTo(expected);
            }
        }
    }
}
