﻿using DryIoc;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Manager;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using Estat.Sri.Mapping.Api.Engine;

    [TestFixture]
    public class TestImportExternalConfigFile : BaseTestClassWithContainer
    {
        public TestImportExternalConfigFile() : base("sqlserver") { }

        [Test]
        public void TestExternalConnectionStrings()
        {
            AppConfigStore config = new AppConfigStore(ConfigurationManager.AppSettings["configLocation"]);

            var conf2 = config.GetSettings();
            var conf = config.GetSettings().Where(x=>x.Name == "TestName").FirstOrDefault();

            Assert.That(conf, Is.Not.Null);
            Assert.That(conf.ConnectionString, Is.EqualTo("TestConnectionString"));
            Assert.That(conf.ProviderName, Is.EqualTo("TestProviderName"));
        }

        [Test]
        public void TestSavingExternalConnectionStrings()
        {
            AppConfigStore config = new AppConfigStore(ConfigurationManager.AppSettings["configLocation"]);

            config.SaveSettings(ConfigurationManager.ConnectionStrings["sqlserver"]);
            config.SaveSettings(ConfigurationManager.ConnectionStrings["odp"]);
            config.SaveSettings(ConfigurationManager.ConnectionStrings["mysql"]);
            var connectionStringsAfterSave = config.GetSettings();

            var sqlserver = connectionStringsAfterSave.Where(x => x.Name == "sqlserver").FirstOrDefault();

            Assert.That(sqlserver, Is.Not.Null);
            Assert.That(sqlserver.ConnectionString, Is.EqualTo(ConfigurationManager.ConnectionStrings["sqlserver"].ConnectionString));
            Assert.That(sqlserver.ProviderName, Is.EqualTo(ConfigurationManager.ConnectionStrings["sqlserver"].ProviderName));

            var odp = connectionStringsAfterSave.Where(x => x.Name == "odp").FirstOrDefault();

            Assert.That(odp, Is.Not.Null);
            Assert.That(odp.ConnectionString, Is.EqualTo(ConfigurationManager.ConnectionStrings["odp"].ConnectionString));
            Assert.That(odp.ProviderName, Is.EqualTo(ConfigurationManager.ConnectionStrings["odp"].ProviderName));

            var mysql = connectionStringsAfterSave.Where(x => x.Name == "mysql").FirstOrDefault();

            Assert.That(mysql, Is.Not.Null);
            Assert.That(mysql.ConnectionString, Is.EqualTo(ConfigurationManager.ConnectionStrings["mysql"].ConnectionString));
            Assert.That(mysql.ProviderName, Is.EqualTo(ConfigurationManager.ConnectionStrings["mysql"].ProviderName));

        }

        [Test]
        public void TestDeletingExternalConnectionStrings()
        {
            AppConfigStore config = new AppConfigStore(ConfigurationManager.AppSettings["configLocation"]);

            config.SaveSettings(ConfigurationManager.ConnectionStrings["mysql"]);
            var connectionStringsAfterSave = config.GetSettings();

            var mysql = connectionStringsAfterSave.Where(x => x.Name == "mysql").FirstOrDefault();

            Assert.That(mysql, Is.Not.Null);
            Assert.That(mysql.ConnectionString, Is.EqualTo(ConfigurationManager.ConnectionStrings["mysql"].ConnectionString));
            Assert.That(mysql.ProviderName, Is.EqualTo(ConfigurationManager.ConnectionStrings["mysql"].ProviderName));

            config.DeleteSettings("mysql");

            var connectionStringsAfterDelete = config.GetSettings();

            Assert.That(connectionStringsAfterDelete.Where(x=>x.Name == "mysql").FirstOrDefault(), Is.Null);
        }
    }
}
