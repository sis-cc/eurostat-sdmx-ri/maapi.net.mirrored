using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Config;
using NSubstitute;
using NUnit.Framework;
using DryIoc;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class TranscodingEngineTest : BaseTestClassWithContainer
    {
        /// <summary>
        /// The connection string helper
        /// </summary>
        private readonly ConnectionStringRetriever _connectionStringHelper = new ConnectionStringRetriever();

        private readonly ConnectionStringSettings _connectionStringSettings;

        private readonly IMappingStoreManager _mappingStoreManager;

        private readonly string _storeId;

        public TranscodingEngineTest(string name) : base(name)
        {
            this._connectionStringSettings = this._connectionStringHelper.GetConnectionStringSettings(name);
            this._mappingStoreManager = this.IoCContainer.Resolve<IMappingStoreManager>();
            this._storeId = StoreId;
        }

        [Test]
        public void GetATranscoding()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            var databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new TranscodingRetrieverFactory(databaseManager, this._mappingStoreManager));
            var engine = retrieverManager.GetRetrieverEngine<TranscodingEntity>(this._connectionStringSettings.Name);
            var query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.Exact, "2"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.That(result, Is.Not.Empty);
        }

        [Test]
        public void GetATranscodingBasedOnParentIdCriteria()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            var databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new TranscodingRetrieverFactory(databaseManager, this._mappingStoreManager));
            var engine = retrieverManager.GetRetrieverEngine<TranscodingEntity>(this._connectionStringSettings.Name);
            var query = Substitute.For<IEntityQuery>();
            query.ParentId.Returns(new Criteria<string>(OperatorType.Exact, "2"));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.That(result, Is.Not.Empty);
            Assert.That(result.Count(), Is.EqualTo(1));
        }

        [Test]
        public void GetAllTranscodings()
        {
            var configurationStoreManager = Substitute.For<IConfigurationStoreManager>();
            configurationStoreManager.GetSettings<ConnectionStringSettings>()
                .Returns(new[] { this._connectionStringSettings });
            var databaseManager = new DatabaseManager(configurationStoreManager);
            IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new TranscodingRetrieverFactory(databaseManager, this._mappingStoreManager));
            var engine = retrieverManager.GetRetrieverEngine<TranscodingEntity>(this._connectionStringSettings.Name);
            var query = Substitute.For<IEntityQuery>();
            query.AdditionalCriteria.Returns(new Dictionary<string, ICriteria<string>>());
            query.EntityId.Returns(new Criteria<string>(OperatorType.AnyValue, null));
            var result = engine.GetEntities(query, Detail.Full).ToArray();
            Assert.That(result, Is.Not.Empty);
        }
    }
}