namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Config;

    using NSubstitute;

    using NUnit.Framework;

    [TestFixture("msdb_scratch.oracle")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.sqlserver")]
    class ComponentMappingEndToEndTest : BaseTestClassWithContainer
    {
        /// <summary>
        /// The entity persistence manager
        /// </summary>
        private readonly IEntityPersistenceManager _entityPersistenceManager;

        /// <summary>
        /// The entity retriever manager
        /// </summary>
        private readonly IEntityRetrieverManager _entityRetrieverManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMappingEndToEndTest"/> class.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        public ComponentMappingEndToEndTest(string storeId)
            : base(storeId)
        {
            this._entityPersistenceManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            this._entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
        }

        [Test]
        public void ShouldInsertComponentMappingWithComponentConstant()
        {
            var mappingSetEntities = this._entityRetrieverManager.GetEntities<MappingSetEntity>(this.StoreId ,new EntityQuery(), Detail.IdOnly).First();

            var entity = new ComponentMappingEntity()
                             {
                                 ParentId = mappingSetEntities.EntityId,
                                 Type = "test",
                                 ConstantValue = "constant",
                                 Component = new Component() { EntityId = "1"},
                                 StoreId = this.StoreId
                             };

            var entityWithEntityId = this._entityPersistenceManager.Add(entity);
            Assert.That(entityWithEntityId.EntityId, Is.Not.Null.Or.Empty);

            var roundTripEntity = this._entityRetrieverManager.GetEntities<ComponentMappingEntity>(
                StoreId,
                entityWithEntityId.EntityId.QueryForThisEntityId(),
                Detail.Full).Single();
            Assert.That(roundTripEntity.Component, Is.Not.Null);
            Assert.That(roundTripEntity.ConstantValue, Is.EqualTo(entity.ConstantValue));
            Assert.That(roundTripEntity.EntityId, Is.EqualTo(entityWithEntityId.EntityId));
            Assert.That(roundTripEntity.Type, Is.EqualTo(entityWithEntityId.Type));
            Assert.That(roundTripEntity.Component.EntityId, Is.EqualTo(entity.Component.EntityId));
            this._entityPersistenceManager.Delete<ComponentMappingEntity>(StoreId, roundTripEntity.EntityId);
        }

        [Test]
        public void ShouldInsertComponentMappingWithComponentConstantAndNoEntityId()
        {
            var mappingSetEntities = this._entityRetrieverManager.GetEntities<MappingSetEntity>(this.StoreId, new EntityQuery(), Detail.IdOnly).First();

            var entity = new ComponentMappingEntity()
            {
                ParentId = mappingSetEntities.EntityId,
                Type = "test",
                ConstantValue = "constant",
                Component = new Component() { EntityId = "1" , ObjectId = "REF_AREA" },
                StoreId = this.StoreId
            };

            var entityWithEntityId = this._entityPersistenceManager.Add(entity);
            Assert.That(entityWithEntityId.EntityId, Is.Not.Null.Or.Empty);

            var roundTripEntity = this._entityRetrieverManager.GetEntities<ComponentMappingEntity>(
                StoreId,
                entityWithEntityId.EntityId.QueryForThisEntityId(),
                Detail.Full).Single();
            Assert.That(roundTripEntity.Component, Is.Not.Null);
            Assert.That(roundTripEntity.ConstantValue, Is.EqualTo(entity.ConstantValue));
            Assert.That(roundTripEntity.EntityId, Is.EqualTo(entityWithEntityId.EntityId));
            Assert.That(roundTripEntity.Type, Is.EqualTo(entityWithEntityId.Type));
            Assert.That(roundTripEntity.Component.EntityId, Is.EqualTo(entity.Component.EntityId));
            this._entityPersistenceManager.Delete<ComponentMappingEntity>(StoreId, roundTripEntity.EntityId);
        }

        [Test]
        public void ShouldInsertComponentMappingWithColumns()
        {
            var mappingSetEntities = this._entityRetrieverManager.GetEntities<MappingSetEntity>(this.StoreId, new EntityQuery(), Detail.Full).First(setEntity => setEntity.DataSetId != null);
            var datasetColumns = this._entityRetrieverManager.GetEntities<DataSetColumnEntity>(
                this.StoreId,
                mappingSetEntities.DataSetId.QueryForThisParentId(),
                Detail.IdAndName);

            var entity = new ComponentMappingEntity()
            {
                ParentId = mappingSetEntities.EntityId,
                Type = "test",
                Component = new Component() { EntityId = "1",ObjectId= "REF_AREA" },
                
                StoreId = this.StoreId
            };
            entity.SetColumns(datasetColumns.ToArray());
            string entityId = null;
            try
            {
                var entityWithEntityId = this._entityPersistenceManager.Add(entity);
                entityId = entityWithEntityId.EntityId;
                Assert.That(entityId, Is.Not.Null.Or.Empty);

                var roundTripEntity = this._entityRetrieverManager.GetEntities<ComponentMappingEntity>(
                    StoreId,
                    entityId.QueryForThisEntityId(),
                    Detail.Full).Single();
                Assert.That(roundTripEntity.Component, Is.Not.Null);
                Assert.That(roundTripEntity.EntityId, Is.EqualTo(entityId));
                Assert.That(roundTripEntity.GetColumns(), Is.Not.Null);
                Assert.That(roundTripEntity.GetColumns(), Is.Not.Empty);
                Assert.That(roundTripEntity.Component.EntityId, Is.EqualTo(entity.Component.EntityId));
            }
            finally
            {
                if (entityId != null)
                {
                    this._entityPersistenceManager.Delete<ComponentMappingEntity>(StoreId, entityId);
                }
            }
        }

        [Test]
        public void ShouldInsertComponentMappingWithColumnsAndDefaultValue()
        {
            var mappingSetEntities = this._entityRetrieverManager.GetEntities<MappingSetEntity>(this.StoreId, new EntityQuery(), Detail.Full).First(setEntity => setEntity.DataSetId != null);
            var datasetColumns = this._entityRetrieverManager.GetEntities<DataSetColumnEntity>(
                this.StoreId,
                mappingSetEntities.DataSetId.QueryForThisParentId(),
                Detail.IdAndName);

            var entity = new ComponentMappingEntity()
            {
                ParentId = mappingSetEntities.EntityId,
                Type = "test",
                Component = new Component() { EntityId = "1" },
                DefaultValue = "TEST123",
                StoreId = this.StoreId
            };
            entity.SetColumns(datasetColumns.ToArray());
            string entityId = null;
            try
            {
                var entityWithEntityId = this._entityPersistenceManager.Add(entity);
                entityId = entityWithEntityId.EntityId;
                Assert.That(entityId, Is.Not.Null.Or.Empty);

                var roundTripEntity = this._entityRetrieverManager.GetEntities<ComponentMappingEntity>(
                    StoreId,
                    entityId.QueryForThisEntityId(),
                    Detail.Full).Single();
                Assert.That(roundTripEntity.Component, Is.Not.Null);
                Assert.That(roundTripEntity.EntityId, Is.EqualTo(entityId));
                Assert.That(roundTripEntity.GetColumns(), Is.Not.Null);
                Assert.That(roundTripEntity.GetColumns(), Is.Not.Empty);
                Assert.That(roundTripEntity.DefaultValue, Is.Not.Null.And.Not.Empty);
                Assert.That(roundTripEntity.DefaultValue, Is.EqualTo(entity.DefaultValue));
                Assert.That(roundTripEntity.Component.EntityId, Is.EqualTo(entity.Component.EntityId));
            }
            finally
            {
                if (entityId != null)
                {
                    this._entityPersistenceManager.Delete<ComponentMappingEntity>(StoreId, entityId);
                }
            }
        }
    }
}