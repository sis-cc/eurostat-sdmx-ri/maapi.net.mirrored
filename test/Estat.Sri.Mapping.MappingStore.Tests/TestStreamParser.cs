// -----------------------------------------------------------------------
// <copyright file="TestStreamParser.cs" company="EUROSTAT">
//   Date Created : 2020-1-16
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Model.AdvancedTime;
using Estat.Sri.Mapping.Api.Utils;
using Estat.Sri.SdmxXmlConstants;
using Newtonsoft.Json;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Sri.Mapping.MappingStore.Tests
{

    class TestStreamParser
    {

        [Test]
        public void ParseWithReaderWithEntityTypeNoneTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/multipleEntityTypes.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.None))
            {
               
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.DataSet));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as DatasetEntity;
                Assert.That(entity.ParentId, Is.EqualTo("1"));
                Assert.That(entity.Name, Is.EqualTo("mydataset"));
                Assert.That(entity.Description, Is.EqualTo("MyDescription"));
                Assert.That(entity.Query, Is.EqualTo("test query"));
                Assert.That(entity.JSONQuery, Is.EqualTo("{\r\n  \"value\": \"extra data\"\r\n}"));

                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as DatasetEntity;
                Assert.That(entity.ParentId, Is.EqualTo("2"));
                Assert.That(entity.Name, Is.EqualTo("test2"));
                Assert.That(entity.Description, Is.EqualTo("MyDescription"));
                Assert.That(entity.Query, Is.EqualTo("test query"));
                Assert.That(entity.JSONQuery, Is.EqualTo("{\r\n  \"value\": \"extra data\"\r\n}"));

                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.DdbConnectionSettings));
                Assert.That(reader.MoveToNextEntity());
                var ddbEntity = reader.CurrentEntity.Entity as IConnectionEntity;
                Assert.That(ddbEntity.EntityId, Is.Null);
                Assert.That(ddbEntity.Name, Is.EqualTo("NA_MAIN"));
                Assert.That(ddbEntity.DatabaseVendorType, Is.EqualTo("MySQL"));
                Assert.That(ddbEntity.Settings, Has.Count.EqualTo(5));
                Assert.That(ddbEntity.Settings["database"].Value, Is.EqualTo("mastore"));
                Assert.That(ddbEntity.Settings["server"].Value, Is.EqualTo("localhost"));
                Assert.That(ddbEntity.Settings["server"].Value, Is.EqualTo("localhost"));
                Assert.That(ddbEntity.Settings["user id"].Value, Is.EqualTo("sdmxuser"));
                Assert.That(ddbEntity.Settings["password"].Value, Is.EqualTo("sdmxpass"));
                // TODO the port might be stored as a string
                Assert.That(ddbEntity.Settings["port"].Value, Is.EqualTo(3306));

                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [Test]
        public void WrongTypeOfEntityTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/multipleEntityTypes.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.DdbConnectionSettings))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.DdbConnectionSettings));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as IConnectionEntity;
                Assert.That(entity.EntityId, Is.Null);
                Assert.That(entity.Name, Is.EqualTo("NA_MAIN"));
                Assert.That(entity.DatabaseVendorType, Is.EqualTo("MySQL"));
                Assert.That(entity.Settings, Has.Count.EqualTo(5));
                Assert.That(entity.Settings["database"].Value, Is.EqualTo("mastore"));
                Assert.That(entity.Settings["server"].Value, Is.EqualTo("localhost"));
                Assert.That(entity.Settings["server"].Value, Is.EqualTo("localhost"));
                Assert.That(entity.Settings["user id"].Value, Is.EqualTo("sdmxuser"));
                Assert.That(entity.Settings["password"].Value, Is.EqualTo("sdmxpass"));
                // TODO the port might be stored as a string
                Assert.That(entity.Settings["port"].Value, Is.EqualTo(3306));

                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }


        [Test]
        public void DdbConnectionEntityParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/ddbConectionEntity.json");
            using(Stream stream = source.OpenRead())
            using(EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.DdbConnectionSettings))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.DdbConnectionSettings));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as IConnectionEntity;
                Assert.That(entity.EntityId, Is.Null);
                Assert.That(entity.Name, Is.EqualTo("NA_MAIN"));
                Assert.That(entity.DatabaseVendorType, Is.EqualTo("MySQL"));
                Assert.That(entity.Settings, Has.Count.EqualTo(5));
                Assert.That(entity.Settings["database"].Value, Is.EqualTo("mastore"));
                Assert.That(entity.Settings["server"].Value, Is.EqualTo("localhost"));
                Assert.That(entity.Settings["server"].Value, Is.EqualTo("localhost"));
                Assert.That(entity.Settings["user id"].Value, Is.EqualTo("sdmxuser"));
                Assert.That(entity.Settings["password"].Value, Is.EqualTo("sdmxpass"));
                // TODO the port might be stored as a string
                Assert.That(entity.Settings["port"].Value, Is.EqualTo(3306));

                // No more entities
                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as IConnectionEntity;
                Assert.That(entity.EntityId, Is.EqualTo("1"));
                Assert.That(entity.Name, Is.EqualTo("CONN_DDB_B_TEST_JANUARY2014"));
                Assert.That(entity.DatabaseVendorType, Is.EqualTo("SqlServer"));
                Assert.That(entity.Settings, Has.Count.EqualTo(30));
                Assert.That(entity.Settings["database"].Value, Is.EqualTo("DDB_B_TEST_JANUARY2014"));
                Assert.That(entity.Settings["server"].Value, Is.EqualTo("localhost"));
                Assert.That(entity.Settings["user id"].Value, Is.EqualTo("ddb"));
                Assert.That(entity.Settings["password"].Value, Is.EqualTo("123"));
                Assert.That(entity.Settings["port"].Value, Is.EqualTo(1433));
                Assert.That(entity.Settings["port"].AllowedSubTypes.Count, Is.EqualTo(2));
                Assert.That(entity.Settings["port"].AllowedValues.Count, Is.EqualTo(2));

                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [Test]
        public void ColumDescriptionParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/columnDescription.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.DescSource))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.DescSource));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as ColumnDescriptionSourceEntity;
                Assert.That(entity.EntityId, Is.EqualTo("2"));
                Assert.That(entity.ParentId, Is.EqualTo("10994"));
                Assert.That(entity.DescriptionTable, Is.EqualTo("LOCALISED_STRING"));
                Assert.That(entity.RelatedField, Is.EqualTo("ITEM_ID"));
                Assert.That(entity.DescriptionField, Is.EqualTo("TEXT"));


                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as ColumnDescriptionSourceEntity;
                Assert.That(entity.EntityId, Is.EqualTo("3"));
                Assert.That(entity.ParentId, Is.EqualTo("23"));
                Assert.That(entity.DescriptionTable, Is.EqualTo("my table"));
                Assert.That(entity.RelatedField, Is.EqualTo("my related field"));
                Assert.That(entity.DescriptionField, Is.EqualTo("my description field"));

                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as ColumnDescriptionSourceEntity;
                Assert.That(entity.EntityId, Is.EqualTo("10003"));
                Assert.That(entity.ParentId, Is.EqualTo("4"));
                Assert.That(entity.DescriptionTable, Is.EqualTo("DimCLADDETTI"));
                Assert.That(entity.RelatedField, Is.EqualTo("IDMember"));
                Assert.That(entity.DescriptionField, Is.EqualTo("Name_E"));

                // No more entities
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [Test]
        public void DataSourceParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/dataSource.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.DataSource))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.DataSource));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as DataSourceEntity;
                Assert.That(entity.DataUrl, Is.EqualTo("http://url_to_data_or_rest_or_soap"));
                Assert.That(entity.WADLUrl, Is.EqualTo("http://url_to_wadl_or_null"));
                Assert.That(entity.WSDLUrl, Is.EqualTo("http://url_to_wsdl_or_null"));
                Assert.That(entity.IsSimple, Is.False);
                Assert.That(entity.IsRest, Is.False);
                Assert.That(entity.IsWs, Is.True);

                // No more entities
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }
        [Test]
        public void TemplateMappingParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/templateMapping.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.TemplateMapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.TemplateMapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as TemplateMapping;
                Assert.That(entity.EntityId, Is.EqualTo("23034"));
                Assert.That(entity.ColumnName, Is.EqualTo("REF_AREA"));
                Assert.That(entity.ComponentId, Is.EqualTo("1"));
                Assert.That(entity.ComponentType, Is.EqualTo("Dimension"));
                Assert.That(entity.ConceptUrn, Is.EqualTo("test_concept_urn"));
                Assert.That(entity.ItemSchemeId, Is.Null);
                Assert.That(entity.Transcodings.Count, Is.EqualTo(2));
                Assert.That(entity.Transcodings.First().Key, Is.EqualTo("Quarterly"));
                Assert.That(entity.Transcodings.First().Value, Is.EqualTo("Q"));
                Assert.That(entity.TimeTranscoding.First().Frequency, Is.EqualTo("Q"));



                // No more entities
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [Test]
        public void DdbConnectionEntityParseTest2()
        {
            FileInfo source = new FileInfo("tests/jsonParser/ddbconnection2.json");
            Assume.That(source.Exists);
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.DdbConnectionSettings))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.DdbConnectionSettings));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as IConnectionEntity;
                Assert.That(entity.EntityId, Is.EqualTo("1"));
                Assert.That(entity.Name, Is.EqualTo("NA_MAIN"));
                Assert.That(entity.DatabaseVendorType, Is.EqualTo("SqlServer"));
                Assert.That(entity.Settings, Has.Count.EqualTo(5));
                Assert.That(entity.Settings["database"].Value, Is.EqualTo("NA_DDB"));
                Assert.That(entity.Settings["server"].Value, Is.EqualTo("ddb_host"));
                Assert.That(entity.Settings["user id"].Value, Is.EqualTo("ddb"));
                Assert.That(entity.Settings["password"].Value, Is.EqualTo("123"));
                // TODO the port might be stored as a string
                Assert.That(entity.Settings["port"].Value, Is.EqualTo("1433"));

                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }
        [Test]
        public void DatasetEntityParseTest()
        {
             FileInfo source = new FileInfo("tests/jsonParser/datasetEntity.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.DataSet))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.DataSet));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as DatasetEntity;
                Assert.That(entity.EntityId, Is.EqualTo("20052"));
                Assert.That(entity.ParentId, Is.EqualTo("4"));
                Assert.That(entity.EditorType, Is.EqualTo("Estat.Ma.Ui.QueryEditors.MdiQueryEditorUC, Estat.Ma.Ui.QueryEditors"));
                Assert.That(entity.Name, Is.EqualTo("xs measures"));
                Assert.That(entity.Description, Is.EqualTo("A dataset description"));
                Assert.That(entity.JSONQuery, Is.Not.Null);
                var jsonQuery = @"{""Dataset"":{""ColumnInstances"":{""ColumnInstance"":[{""ID"":""CENSUS_TABLE_TYPE_B1_AGE"",""Type"":""nvarchar"",""Name"":""AGE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_CAS"",""Type"":""nvarchar"",""Name"":""CAS"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_GEO"",""Type"":""nvarchar"",""Name"":""GEO"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_MALE"",""Type"":""int"",""Name"":""MALE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_FEMALE"",""Type"":""int"",""Name"":""FEMALE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_TOTAL"",""Type"":""int"",""Name"":""TOTAL"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""}]},""TableInstances"":{""TableInstance"":[{""ID"":""CENSUS_TABLE_TYPE_B1"",""Name"":""CENSUS_TABLE_TYPE_B"",""XCoordinate"":""10"",""YCoordinate"":""10"",""ColumnInstanceRefs"":{""ColumnInstanceRef"":[{""ID"":""CENSUS_TABLE_TYPE_B1_AGE"",""Index"":""0""},{""ID"":""CENSUS_TABLE_TYPE_B1_CAS"",""Index"":""1""},{""ID"":""CENSUS_TABLE_TYPE_B1_GEO"",""Index"":""2""},{""ID"":""CENSUS_TABLE_TYPE_B1_MALE"",""Index"":""3""},{""ID"":""CENSUS_TABLE_TYPE_B1_FEMALE"",""Index"":""4""},{""ID"":""CENSUS_TABLE_TYPE_B1_TOTAL"",""Index"":""5""}]}}]},""Relations"":{""Relation"":[]}}}";
                Assert.That(Regex.Replace(entity.JSONQuery, @"\s+", "", RegexOptions.CultureInvariant), Is.EqualTo(jsonQuery));
                var sql = "select CENSUS_TABLE_TYPE_B1.AGE as CENSUS_TABLE_TYPE_B1_AGE, CENSUS_TABLE_TYPE_B1.CAS as CENSUS_TABLE_TYPE_B1_CAS, CENSUS_TABLE_TYPE_B1.GEO as CENSUS_TABLE_TYPE_B1_GEO, CENSUS_TABLE_TYPE_B1.MALE as CENSUS_TABLE_TYPE_B1_MALE, CENSUS_TABLE_TYPE_B1.FEMALE as CENSUS_TABLE_TYPE_B1_FEMALE, CENSUS_TABLE_TYPE_B1.TOTAL as CENSUS_TABLE_TYPE_B1_TOTAL\r\n from CENSUS_TABLE_TYPE_B CENSUS_TABLE_TYPE_B1\r\n";
                Assert.That(entity.Query, Is.EqualTo(sql));
                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }
        [Test]
        public void DatasetEntityWithExtraDataAsStringParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/datasetEntityExtraDataAsString.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.DataSet))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.DataSet));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as DatasetEntity;
                Assert.That(entity.EntityId, Is.EqualTo("20052"));
                Assert.That(entity.ParentId, Is.EqualTo("4"));
                Assert.That(entity.EditorType, Is.EqualTo("Estat.Ma.Ui.QueryEditors.MdiQueryEditorUC, Estat.Ma.Ui.QueryEditors"));
                Assert.That(entity.Name, Is.EqualTo("xs measures"));
                Assert.That(entity.Description, Is.EqualTo("A dataset description"));
                Assert.That(entity.JSONQuery, Is.Not.Null);
                var jsonQuery = @"{""Dataset"":{""ColumnInstances"":{""ColumnInstance"":[{""ID"":""CENSUS_TABLE_TYPE_B1_AGE"",""Type"":""nvarchar"",""Name"":""AGE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_CAS"",""Type"":""nvarchar"",""Name"":""CAS"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_GEO"",""Type"":""nvarchar"",""Name"":""GEO"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_MALE"",""Type"":""int"",""Name"":""MALE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_FEMALE"",""Type"":""int"",""Name"":""FEMALE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_TOTAL"",""Type"":""int"",""Name"":""TOTAL"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""}]},""TableInstances"":{""TableInstance"":[{""ID"":""CENSUS_TABLE_TYPE_B1"",""Name"":""CENSUS_TABLE_TYPE_B"",""XCoordinate"":""10"",""YCoordinate"":""10"",""ColumnInstanceRefs"":{""ColumnInstanceRef"":[{""ID"":""CENSUS_TABLE_TYPE_B1_AGE"",""Index"":""0""},{""ID"":""CENSUS_TABLE_TYPE_B1_CAS"",""Index"":""1""},{""ID"":""CENSUS_TABLE_TYPE_B1_GEO"",""Index"":""2""},{""ID"":""CENSUS_TABLE_TYPE_B1_MALE"",""Index"":""3""},{""ID"":""CENSUS_TABLE_TYPE_B1_FEMALE"",""Index"":""4""},{""ID"":""CENSUS_TABLE_TYPE_B1_TOTAL"",""Index"":""5""}]}}]},""Relations"":{""Relation"":[]}}}";
                Assert.That(Regex.Replace(entity.JSONQuery, @"\s+", "", RegexOptions.CultureInvariant), Is.EqualTo(jsonQuery));
                var sql = "select CENSUS_TABLE_TYPE_B1.AGE as CENSUS_TABLE_TYPE_B1_AGE, CENSUS_TABLE_TYPE_B1.CAS as CENSUS_TABLE_TYPE_B1_CAS, CENSUS_TABLE_TYPE_B1.GEO as CENSUS_TABLE_TYPE_B1_GEO, CENSUS_TABLE_TYPE_B1.MALE as CENSUS_TABLE_TYPE_B1_MALE, CENSUS_TABLE_TYPE_B1.FEMALE as CENSUS_TABLE_TYPE_B1_FEMALE, CENSUS_TABLE_TYPE_B1.TOTAL as CENSUS_TABLE_TYPE_B1_TOTAL\r\n from CENSUS_TABLE_TYPE_B CENSUS_TABLE_TYPE_B1\r\n";
                Assert.That(entity.Query, Is.EqualTo(sql));
                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }
        [Test]
        public void DatasetEntityStandaloneParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/datasetEntityStandalone.json");
            DatasetEntityReader datasetEntityReader = new DatasetEntityReader(string.Empty);
            using (var streamReader = source.OpenText())
            using (JsonReader reader = new JsonTextReader(streamReader))
            {
                var entity = datasetEntityReader.ParseEntity(reader);
                Assert.That(entity.EntityId, Is.EqualTo("20052"));
                Assert.That(entity.ParentId, Is.EqualTo("4"));
                Assert.That(entity.EditorType, Is.EqualTo("Estat.Ma.Ui.QueryEditors.MdiQueryEditorUC, Estat.Ma.Ui.QueryEditors"));
                Assert.That(entity.Name, Is.EqualTo("xs measures"));
                Assert.That(entity.Description, Is.EqualTo("A dataset description"));
                Assert.That(entity.JSONQuery, Is.Not.Null);
                var jsonQuery = @"{""Dataset"":{""ColumnInstances"":{""ColumnInstance"":[{""ID"":""CENSUS_TABLE_TYPE_B1_AGE"",""Type"":""nvarchar"",""Name"":""AGE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_CAS"",""Type"":""nvarchar"",""Name"":""CAS"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_GEO"",""Type"":""nvarchar"",""Name"":""GEO"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_MALE"",""Type"":""int"",""Name"":""MALE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_FEMALE"",""Type"":""int"",""Name"":""FEMALE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_TOTAL"",""Type"":""int"",""Name"":""TOTAL"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""}]},""TableInstances"":{""TableInstance"":[{""ID"":""CENSUS_TABLE_TYPE_B1"",""Name"":""CENSUS_TABLE_TYPE_B"",""XCoordinate"":""10"",""YCoordinate"":""10"",""ColumnInstanceRefs"":{""ColumnInstanceRef"":[{""ID"":""CENSUS_TABLE_TYPE_B1_AGE"",""Index"":""0""},{""ID"":""CENSUS_TABLE_TYPE_B1_CAS"",""Index"":""1""},{""ID"":""CENSUS_TABLE_TYPE_B1_GEO"",""Index"":""2""},{""ID"":""CENSUS_TABLE_TYPE_B1_MALE"",""Index"":""3""},{""ID"":""CENSUS_TABLE_TYPE_B1_FEMALE"",""Index"":""4""},{""ID"":""CENSUS_TABLE_TYPE_B1_TOTAL"",""Index"":""5""}]}}]},""Relations"":{""Relation"":[]}}}";
                Assert.That(Regex.Replace(entity.JSONQuery, @"\s+", "", RegexOptions.CultureInvariant), Is.EqualTo(jsonQuery));
                var sql = "select CENSUS_TABLE_TYPE_B1.AGE as CENSUS_TABLE_TYPE_B1_AGE, CENSUS_TABLE_TYPE_B1.CAS as CENSUS_TABLE_TYPE_B1_CAS, CENSUS_TABLE_TYPE_B1.GEO as CENSUS_TABLE_TYPE_B1_GEO, CENSUS_TABLE_TYPE_B1.MALE as CENSUS_TABLE_TYPE_B1_MALE, CENSUS_TABLE_TYPE_B1.FEMALE as CENSUS_TABLE_TYPE_B1_FEMALE, CENSUS_TABLE_TYPE_B1.TOTAL as CENSUS_TABLE_TYPE_B1_TOTAL\r\n from CENSUS_TABLE_TYPE_B CENSUS_TABLE_TYPE_B1\r\n";
                Assert.That(entity.Query, Is.EqualTo(sql));
                // No more entities
            }
        }
        [Test]
        public void DatasetEntityWithPermissionsParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/datasetEntityWithPermissions.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.DataSet))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.DataSet));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as DatasetEntity;
                Assert.That(entity.EntityId, Is.EqualTo("20052"));
                Assert.That(entity.ParentId, Is.EqualTo("4"));
                Assert.That(entity.EditorType, Is.EqualTo("Estat.Ma.Ui.QueryEditors.MdiQueryEditorUC, Estat.Ma.Ui.QueryEditors"));
                Assert.That(entity.Name, Is.EqualTo("xs measures"));
                Assert.That(entity.Description, Is.EqualTo("A dataset description"));
                Assert.That(entity.JSONQuery, Is.Not.Null);
                var jsonQuery = @"{""Dataset"":{""ColumnInstances"":{""ColumnInstance"":[{""ID"":""CENSUS_TABLE_TYPE_B1_AGE"",""Type"":""nvarchar"",""Name"":""AGE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_CAS"",""Type"":""nvarchar"",""Name"":""CAS"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_GEO"",""Type"":""nvarchar"",""Name"":""GEO"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_MALE"",""Type"":""int"",""Name"":""MALE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_FEMALE"",""Type"":""int"",""Name"":""FEMALE"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""},{""ID"":""CENSUS_TABLE_TYPE_B1_TOTAL"",""Type"":""int"",""Name"":""TOTAL"",""IsPrimaryKey"":""False"",""IsReturned"":""True"",""IsFixedValue"":""False""}]},""TableInstances"":{""TableInstance"":[{""ID"":""CENSUS_TABLE_TYPE_B1"",""Name"":""CENSUS_TABLE_TYPE_B"",""XCoordinate"":""10"",""YCoordinate"":""10"",""ColumnInstanceRefs"":{""ColumnInstanceRef"":[{""ID"":""CENSUS_TABLE_TYPE_B1_AGE"",""Index"":""0""},{""ID"":""CENSUS_TABLE_TYPE_B1_CAS"",""Index"":""1""},{""ID"":""CENSUS_TABLE_TYPE_B1_GEO"",""Index"":""2""},{""ID"":""CENSUS_TABLE_TYPE_B1_MALE"",""Index"":""3""},{""ID"":""CENSUS_TABLE_TYPE_B1_FEMALE"",""Index"":""4""},{""ID"":""CENSUS_TABLE_TYPE_B1_TOTAL"",""Index"":""5""}]}}]},""Relations"":{""Relation"":[]}}}";
                Assert.That(Regex.Replace(entity.JSONQuery, @"\s+", "", RegexOptions.CultureInvariant), Is.EqualTo(jsonQuery));
                var sql = "select CENSUS_TABLE_TYPE_B1.AGE as CENSUS_TABLE_TYPE_B1_AGE, CENSUS_TABLE_TYPE_B1.CAS as CENSUS_TABLE_TYPE_B1_CAS, CENSUS_TABLE_TYPE_B1.GEO as CENSUS_TABLE_TYPE_B1_GEO, CENSUS_TABLE_TYPE_B1.MALE as CENSUS_TABLE_TYPE_B1_MALE, CENSUS_TABLE_TYPE_B1.FEMALE as CENSUS_TABLE_TYPE_B1_FEMALE, CENSUS_TABLE_TYPE_B1.TOTAL as CENSUS_TABLE_TYPE_B1_TOTAL\r\n from CENSUS_TABLE_TYPE_B CENSUS_TABLE_TYPE_B1\r\n";
                Assert.That(entity.Query, Is.EqualTo(sql));
                Assert.That(entity.Permissions, Is.Not.Null);
                Assert.That(entity.Permissions.Count, Is.EqualTo(5));
                Assert.That(entity.Permissions.ElementAt(0).Key, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30.31"));
                Assert.That(entity.Permissions.ElementAt(0).Value, Is.EqualTo("read_write"));
                Assert.That(entity.Permissions.ElementAt(1).Key, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).6.157"));
                Assert.That(entity.Permissions.ElementAt(1).Value, Is.EqualTo("read_write"));
                Assert.That(entity.Permissions.ElementAt(2).Key, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30"));
                Assert.That(entity.Permissions.ElementAt(2).Value, Is.EqualTo("read_write"));
                Assert.That(entity.Permissions.ElementAt(3).Key, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:HC06(1.0)"));
                Assert.That(entity.Permissions.ElementAt(3).Value, Is.EqualTo("read_write"));
                Assert.That(entity.Permissions.ElementAt(4).Key, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30.264D"));
                Assert.That(entity.Permissions.ElementAt(4).Value, Is.EqualTo("read_write"));
                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [Test]
        public void DatasetColumnEntityParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/datasetColumn.json");
            using(Stream stream = source.OpenRead())
            using(EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.DataSetColumn))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.DataSetColumn));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as DataSetColumnEntity;
                Assert.That(entity.EntityId, Is.Null);
                Assert.That(entity.ParentId, Is.EqualTo("36"));

                Assert.That(entity.Name, Is.EqualTo("sts_sdmx_transQ1_OBS_STATUS"));
                Assert.That(entity.Description, Is.EqualTo("Description of column OBS_STATUS"));
                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as DataSetColumnEntity;
                Assert.That(entity.EntityId, Is.Null);
                Assert.That(entity.ParentId, Is.EqualTo("36"));

                Assert.That(entity.Name, Is.EqualTo("sts_sdmx_transQ1_OBS_CONF"));
                Assert.That(entity.Description, Is.EqualTo("Description of column OBS_CONF"));

                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [Test]
        public void HeaderTemplateEntityParseTestIstat()
        {
            FileInfo source = new FileInfo("tests/jsonParser/header_from_istat.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.Header))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.Header));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as HeaderEntity;
                Assert.That(entity.EntityId, Is.Null);
                Assert.That(entity.ParentId, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:HEADERTEST(1.0)"));
                Assert.That(entity.SdmxHeader.Source, Is.Empty);
                Assert.That(entity.SdmxHeader.Test, Is.True);
                Assert.That(entity.SdmxHeader.AdditionalAttribtues.ContainsKey(nameof(ElementNameTable.DataSetAgency)));
                Assert.That(entity.SdmxHeader.GetAdditionalAttribtue(nameof(ElementNameTable.DataSetAgency)), Is.Empty.Or.Null);
                Assert.That(entity.SdmxHeader.Sender, Is.Not.Null);
                Assert.That(entity.SdmxHeader.Sender.Id, Is.EqualTo("sender1"));
                Assert.That(entity.SdmxHeader.Sender.Name.Count, Is.EqualTo(0));
                Assert.That(entity.SdmxHeader.Sender.Contacts.Count, Is.EqualTo(0));

                Assert.That(entity.SdmxHeader.Receiver[0], Is.Not.Null);
                Assert.That(entity.SdmxHeader.Receiver[0].Id, Is.EqualTo("reciver2"));
                Assert.That(entity.SdmxHeader.Receiver[0].Name.Count, Is.EqualTo(1));
                Assert.That(entity.SdmxHeader.Receiver[0].Name[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Receiver[0].Name[0].Value, Is.EqualTo("reccName3"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts.Count, Is.EqualTo(0));

                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }


        [Test]
        public void HeaderTemplateEntityParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/headerTemplateEntity.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.Header))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.Header));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as HeaderEntity;
                Assert.That(entity.EntityId, Is.EqualTo("1"));
                Assert.That(entity.ParentId, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:61_69(1.0)"));
                Assert.That(entity.SdmxHeader.Source, Is.Empty);
                Assert.That(entity.SdmxHeader.Name, Is.Not.Empty);
                Assert.That(entity.SdmxHeader.Name[0].Value, Is.EqualTo("header name"));
                Assert.That(entity.SdmxHeader.Test, Is.False);
                Assert.That(entity.SdmxHeader.AdditionalAttribtues.ContainsKey(nameof(ElementNameTable.DataSetAgency)));
                Assert.That(entity.SdmxHeader.GetAdditionalAttribtue(nameof(ElementNameTable.DataSetAgency)), Is.EqualTo("Test Agency"));
                Assert.That(entity.SdmxHeader.Sender, Is.Not.Null);
                Assert.That(entity.SdmxHeader.Sender.Id, Is.EqualTo("RO1"));
                Assert.That(entity.SdmxHeader.Sender.Name.Count, Is.EqualTo(1));
                Assert.That(entity.SdmxHeader.Sender.Name[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Sender.Name[0].Value, Is.EqualTo("Romania INE"));
                Assert.That(entity.SdmxHeader.Sender.Contacts.Count, Is.EqualTo(1));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Name.Count, Is.EqualTo(1));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Name[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Name[0].Value, Is.EqualTo("Smith"));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Departments[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Departments[0].Value, Is.EqualTo("CENSUS"));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Role[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Role[0].Value, Is.EqualTo("Admin"));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Telephone, Is.Empty);
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Fax, Is.Empty);
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Uri, Is.Empty);
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].X400, Is.Empty);
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Email[0], Is.EqualTo("test@somewhere.org"));

                Assert.That(entity.SdmxHeader.Receiver[0], Is.Not.Null);
                Assert.That(entity.SdmxHeader.Receiver[0].Id, Is.EqualTo("ZZ9"));
                Assert.That(entity.SdmxHeader.Receiver[0].Name.Count, Is.EqualTo(1));
                Assert.That(entity.SdmxHeader.Receiver[0].Name[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Receiver[0].Name[0].Value, Is.EqualTo("Ministry of Truth"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts.Count, Is.EqualTo(1));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Name.Count, Is.EqualTo(1));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Name[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Name[0].Value, Is.EqualTo("someone"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Role[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Role[0].Value, Is.EqualTo("aRole"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Telephone, Is.Empty);
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Fax, Is.Empty);
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Uri, Is.Empty);
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].X400, Is.Empty);
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Email[0], Is.EqualTo("someone@example.com"));

                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }
        [Test]
        public void HeaderTemplateEntityParseLocaleTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/headerTemplateMultipleNamesRolesAndDeparmentsEntity.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.Header))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.Header));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as HeaderEntity;
                Assert.That(entity.EntityId, Is.EqualTo("1"));
                Assert.That(entity.ParentId, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:61_69(1.0)"));
                Assert.That(entity.SdmxHeader.Source, Is.Empty);
                Assert.That(entity.SdmxHeader.Name, Is.Not.Empty);
                Assert.That(entity.SdmxHeader.Name[0].Value, Is.EqualTo("header name"));
                Assert.That(entity.SdmxHeader.Test, Is.False);
                Assert.That(entity.SdmxHeader.AdditionalAttribtues.ContainsKey(nameof(ElementNameTable.DataSetAgency)));
                Assert.That(entity.SdmxHeader.GetAdditionalAttribtue(nameof(ElementNameTable.DataSetAgency)), Is.EqualTo("Test Agency"));
                Assert.That(entity.SdmxHeader.Sender, Is.Not.Null);
                Assert.That(entity.SdmxHeader.Sender.Id, Is.EqualTo("RO1"));
                Assert.That(entity.SdmxHeader.Sender.Name.Count, Is.EqualTo(1));
                Assert.That(entity.SdmxHeader.Sender.Name[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Sender.Name[0].Value, Is.EqualTo("Romania INE"));
                Assert.That(entity.SdmxHeader.Sender.Contacts.Count, Is.EqualTo(1));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Name.Count, Is.EqualTo(1));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Name[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Name[0].Value, Is.EqualTo("Smith"));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Departments[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Departments[0].Value, Is.EqualTo("CENSUS"));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Role[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Role[0].Value, Is.EqualTo("Admin"));
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Telephone, Is.Empty);
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Fax, Is.Empty);
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Uri, Is.Empty);
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].X400, Is.Empty);
                Assert.That(entity.SdmxHeader.Sender.Contacts[0].Email[0], Is.EqualTo("test@somewhere.org"));

                Assert.That(entity.SdmxHeader.Receiver[0], Is.Not.Null);
                Assert.That(entity.SdmxHeader.Receiver[0].Id, Is.EqualTo("ZZ9"));
                Assert.That(entity.SdmxHeader.Receiver[0].Name.Count, Is.EqualTo(1));
                Assert.That(entity.SdmxHeader.Receiver[0].Name[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Receiver[0].Name[0].Value, Is.EqualTo("Ministry of Truth"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts.Count, Is.EqualTo(1));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Name.Count, Is.EqualTo(2));
                // TODO names might not come in the same order, fix test in that case
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Name[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Name[0].Value, Is.EqualTo("someone"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Name[1].Locale, Is.EqualTo("el"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Name[1].Value, Is.EqualTo("onoma1"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Role[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Role[0].Value, Is.EqualTo("aRole"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Role[1].Locale, Is.EqualTo("de"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Role[1].Value, Is.EqualTo("bRole"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Departments[0].Locale, Is.EqualTo("en"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Departments[0].Value, Is.EqualTo("contactDepartment"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Departments[1].Locale, Is.EqualTo("el"));
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Departments[1].Value, Is.EqualTo("tmima1"));
 
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Telephone, Is.Empty);
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Fax, Is.Empty);
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Uri, Is.Empty);
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].X400, Is.Empty);
                Assert.That(entity.SdmxHeader.Receiver[0].Contacts[0].Email[0], Is.EqualTo("someone@example.com"));

                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [Test]
        public void LastUpdateMappingReadJsonTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/lastupdatedentity.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.LastUpdated))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.LastUpdated));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as LastUpdatedEntity;
                Assert.That(entity.EntityId, Is.EqualTo("377"));
                Assert.That(entity.ParentId, Is.EqualTo("377"));
                Assert.That(entity.ConstantValue, Is.Null);
                Assert.That(entity.GetColumns().Count, Is.EqualTo(1));
                Assert.That(entity.GetColumns().First().Name, Is.EqualTo("sts_sdmx_transQ1_OBS_STATUS"));


                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }
        [Test]
        public void LastUpdateMappingWithConstantReadJsonTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/lastupdatedentity-constant.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.LastUpdated))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.LastUpdated));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as LastUpdatedEntity;
                Assert.That(entity.EntityId, Is.EqualTo("377"));
                Assert.That(entity.ParentId, Is.EqualTo("377"));
                Assert.That(entity.ConstantValue, Is.Not.Null);
                Assert.That(entity.ConstantValue, Is.EqualTo("2021-04-20T14:30:00Z"));
                Assert.That(entity.GetColumns().Count, Is.EqualTo(0));


                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }
   [Test]
        public void ValidToMappingReadJsonTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/validtomappingentity.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.ValidToMapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.ValidToMapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as ValidToMappingEntity;
                Assert.That(entity.EntityId, Is.EqualTo("377"));
                Assert.That(entity.ParentId, Is.EqualTo("377"));
                Assert.That(entity.ConstantValue, Is.Null);
                Assert.That(entity.GetColumns().Count, Is.EqualTo(1));
                Assert.That(entity.GetColumns().First().Name, Is.EqualTo("sts_sdmx_transQ1_OBS_STATUS"));


                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }
        [Test]
        public void ValidToMappingWithConstantReadJsonTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/validtomappingentity-constant.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.ValidToMapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.ValidToMapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as ValidToMappingEntity;
                Assert.That(entity.EntityId, Is.EqualTo("377"));
                Assert.That(entity.ParentId, Is.EqualTo("377"));
                Assert.That(entity.ConstantValue, Is.Not.Null);
                Assert.That(entity.ConstantValue, Is.EqualTo("2021-04-20T14:30:00Z"));
                Assert.That(entity.GetColumns().Count, Is.EqualTo(0));


                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }
         [Test]
        public void UpdateStatusMappingReadJsonTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/updatestatusentity.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.UpdateStatusMapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.UpdateStatusMapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as UpdateStatusEntity;
                Assert.That(entity.EntityId, Is.EqualTo("377"));
                Assert.That(entity.ParentId, Is.EqualTo("377"));
                Assert.That(entity.ConstantValue, Is.Null);
                Assert.That(entity.GetColumns().Count, Is.EqualTo(1));
                Assert.That(entity.GetColumns().First().Name, Is.EqualTo("sts_sdmx_transQ1_OBS_STATUS"));


                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }
       [Test]
        public void UpdateStatusMappingWTranscodingReadJsonTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/updatestatusentity-transcoding.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.UpdateStatusMapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.UpdateStatusMapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as UpdateStatusEntity;
                Assert.That(entity.EntityId, Is.EqualTo("377"));
                Assert.That(entity.ParentId, Is.EqualTo("377"));
                Assert.That(entity.ConstantValue, Is.Null);
                Assert.That(entity.GetColumns().Count, Is.EqualTo(1));
                Assert.That(entity.GetColumns().First().Name, Is.EqualTo("sts_sdmx_transQ1_OBS_STATUS"));
                Assert.That(entity.Transcoding, Is.Not.Empty);
                Assert.That(entity.HasTranscoding());
                Assert.That(entity.Transcoding.Count, Is.EqualTo(3));
                Assert.That(entity.Transcoding["insert"], Is.EqualTo(ObservationActionEnumType.Added));
                Assert.That(entity.Transcoding["replace"], Is.EqualTo(ObservationActionEnumType.Updated));
                Assert.That(entity.Transcoding["removed"], Is.EqualTo(ObservationActionEnumType.Deleted));

                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [Test]
        public void UpdateStatusMappingWithConstantReadJsonTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/updatestatusentity-constant.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.UpdateStatusMapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.UpdateStatusMapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as UpdateStatusEntity;
                Assert.That(entity.EntityId, Is.EqualTo("377"));
                Assert.That(entity.ParentId, Is.EqualTo("377"));
                Assert.That(entity.ConstantValue, Is.Not.Null);
                Assert.That(entity.Constant.EnumType, Is.EqualTo(ObservationActionEnumType.Updated));
                Assert.That(entity.GetColumns().Count, Is.EqualTo(0));


                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [Test]
        public void MappingEntityParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/mappingEntity.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.Mapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.Mapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as ComponentMappingEntity;
                Assert.That(entity.EntityId, Is.EqualTo("377"));
                Assert.That(entity.ParentId, Is.EqualTo("30"));
                Assert.That(entity.Type, Is.EqualTo("A"));
                Assert.That(entity.DefaultValue, Is.Null);
                Assert.That(entity.ConstantValue, Is.Null);
                Assert.That(entity.IsMetadata, Is.EqualTo(false));
                Assert.That(entity.Component, Is.Not.Null);
                Assert.That(entity.Component.EntityId, Is.EqualTo("10638"));
                Assert.That(entity.Component.ObjectId, Is.EqualTo("OBS_STATUS"));
                Assert.That(entity.GetColumns().Count, Is.EqualTo(1));
                Assert.That(entity.GetColumns().First().Name, Is.EqualTo("sts_sdmx_transQ1_OBS_STATUS"));
                Assert.That(entity.GetColumns().First().EntityId, Is.EqualTo("644"));
                Assert.That(entity.GetColumns().First().ParentId, Is.EqualTo("36"));


                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as ComponentMappingEntity;
                Assert.That(entity.EntityId, Is.Null);
                Assert.That(entity.ParentId, Is.EqualTo("30"));
                Assert.That(entity.Type, Is.EqualTo("A"));
                Assert.That(entity.DefaultValue, Is.EqualTo("C"));
                Assert.That(entity.ConstantValue, Is.Null);
                Assert.That(entity.IsMetadata, Is.EqualTo(false));
                Assert.That(entity.Component, Is.Not.Null);
                Assert.That(entity.Component.EntityId, Is.EqualTo("10639"));
                Assert.That(entity.Component.ObjectId, Is.EqualTo("OBS_CONF"));
                Assert.That(entity.GetColumns().Count, Is.EqualTo(2));
                Assert.That(entity.GetColumns()[0].Name, Is.EqualTo("sts_sdmx_transQ1_OBS_CONF1"));
                Assert.That(entity.GetColumns()[0].EntityId, Is.EqualTo("645"));
                Assert.That(entity.GetColumns()[0].ParentId, Is.EqualTo("36"));
                Assert.That(entity.GetColumns()[1].Name, Is.EqualTo("sts_sdmx_transQ1_OBS_CONF2"));
                Assert.That(entity.GetColumns()[1].EntityId, Is.EqualTo("646"));
                Assert.That(entity.GetColumns()[1].ParentId, Is.EqualTo("36"));


                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as ComponentMappingEntity;
                Assert.That(entity.EntityId, Is.Null);
                Assert.That(entity.ParentId, Is.EqualTo("30"));
                Assert.That(entity.Type, Is.EqualTo("A"));
                Assert.That(entity.DefaultValue, Is.Null);
                Assert.That(entity.ConstantValue, Is.EqualTo("A comment"));
                Assert.That(entity.IsMetadata, Is.EqualTo(true));
                Assert.That(entity.Component, Is.Not.Null);
                Assert.That(entity.Component.EntityId, Is.EqualTo("10639"));
                Assert.That(entity.Component.ObjectId, Is.EqualTo("OBS_COM"));
                Assert.That(entity.GetColumns().Count, Is.EqualTo(0));

                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }
        [Test]
        public void MultipleMappingsTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/multiple-mappings.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.Mapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.Mapping));
                int count = 0;
                while (reader.MoveToNextEntity())
                {
                    count++;
                    var entity = reader.CurrentEntity.Entity as ComponentMappingEntity;
                    Assert.That(entity.EntityId, Is.Null);
                    Assert.That(entity.ParentId, Is.EqualTo("4"), "Component {0}", entity?.Component?.ObjectId ?? entity.Type);
                    if (entity.Type.Equals("A"))
                    {
                        Assert.That(entity.Component, Is.Not.Null);
                    }
                    else
                    {
                        Assert.That(entity.Component, Is.Null);
                    }

                    if (string.IsNullOrWhiteSpace(entity.ConstantValue))
                    {
                        Assert.That(entity.GetColumns(), Is.Not.Null.And.Not.Empty);
                    }
                    else
                    {
                        Assert.That(entity.GetColumns(), Is.Null.Or.Empty);
                    }
                }
                Assert.That(count, Is.EqualTo(25));

            }
        }
        [Test]
        public void MappingSetEntityParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/mappingSetEntity.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.MappingSet))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.MappingSet));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as MappingSetEntity;
                Assert.That(entity.EntityId, Is.EqualTo("2"));
                Assert.That(entity.ParentId, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:161_267(1.0)"));
                Assert.That(entity.Name, Is.EqualTo("MS_6_161_267"));
                Assert.That(entity.Description, Is.EqualTo("MS_6_161_267"));
                Assert.That(entity.DataSetId, Is.EqualTo("2"));
                Assert.That(entity.ValidFrom, Is.EqualTo(DateTime.Parse("2025-01-13T14:30:00")));
                Assert.That(entity.ValidTo, Is.EqualTo(null));
                Assert.That(entity.IsMetadata, Is.EqualTo(true));

                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [Test]
        public void RegistryEntityParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/registryEntity.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.Registry))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.Registry));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as RegistryEntity;
                Assert.That(entity.EntityId, Is.EqualTo("1"));
                Assert.That(entity.Name, Is.EqualTo("New Registry Connection"));
                Assert.That(entity.Description, Is.EqualTo("mmm"));
                Assert.That(entity.Technology, Is.EqualTo("Rest"));
                Assert.That(entity.Url, Is.EqualTo("http://example.com/sdmx/rest/v1/"));
                Assert.That(entity.UserName, Is.EqualTo("eee"));
                Assert.That(entity.Proxy, Is.True);
                Assert.That(entity.IsPublic, Is.True);
                Assert.That(entity.Upgrades, Is.True);

                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }


        [Test]
        public void TranscodingRuleParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/transcodingRules.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.TranscodingRule))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.TranscodingRule));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as TranscodingRuleEntity;
                Assert.That(entity.ParentId, Is.EqualTo("5"));
                Assert.That(entity.DsdCodeEntity.ObjectId, Is.EqualTo("N"));
                Assert.That(entity.LocalCodes.Count, Is.EqualTo(1));
                Assert.That(entity.LocalCodes.First().ParentId, Is.EqualTo("30"));
                Assert.That(entity.LocalCodes.First().ObjectId, Is.EqualTo("NOT_ADJ"));

                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as TranscodingRuleEntity;
                Assert.That(entity.ParentId, Is.EqualTo("5"));
                Assert.That(entity.DsdCodeEntity.ObjectId, Is.EqualTo("C"));
                Assert.That(entity.LocalCodes.Count, Is.EqualTo(1));
                Assert.That(entity.LocalCodes.First().ParentId, Is.EqualTo("30"));
                Assert.That(entity.LocalCodes.First().ObjectId, Is.EqualTo("NC"));

                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as TranscodingRuleEntity;
                Assert.That(entity.ParentId, Is.EqualTo("5"));
                Assert.That(entity.DsdCodeEntity.ObjectId, Is.EqualTo("T"));
                Assert.That(entity.LocalCodes.Count, Is.EqualTo(1));
                Assert.That(entity.LocalCodes.First().ParentId, Is.EqualTo("30"));
                Assert.That(entity.LocalCodes.First().ObjectId, Is.EqualTo("T"));
                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }
        [Test]
        public void TranscodingRuleParseTest2()
        {
            FileInfo source = new FileInfo("tests/jsonParser/transcoding-rules-from-maweb.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.TranscodingRule))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.TranscodingRule));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as TranscodingRuleEntity;
                Assert.That(entity.ParentId, Is.EqualTo("320"));
                Assert.That(entity.DsdCodeEntity.ObjectId, Is.EqualTo("N19500"));
                Assert.That(entity.LocalCodes.Count, Is.EqualTo(1));
                Assert.That(entity.LocalCodes.First().ParentId, Is.EqualTo("sts_sdmx_transQ1_STS_ACTIVITY"));
                Assert.That(entity.LocalCodes.First().ObjectId, Is.EqualTo("NS0020"));

                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as TranscodingRuleEntity;
                Assert.That(entity.ParentId, Is.EqualTo("320"));
                Assert.That(entity.DsdCodeEntity.ObjectId, Is.EqualTo("NS0030"));
                Assert.That(entity.LocalCodes.Count, Is.EqualTo(1));
                Assert.That(entity.LocalCodes.First().ParentId, Is.EqualTo("sts_sdmx_transQ1_STS_ACTIVITY"));
                Assert.That(entity.LocalCodes.First().ObjectId, Is.EqualTo("NS0030"));
                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [Test]
        public void UserEntityParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/userEntity.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.User))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.User));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as UserEntity;
                Assert.That(entity.EntityId, Is.EqualTo("2"));
                Assert.That(entity.UserName, Is.EqualTo("user1"));
                Assert.That(entity.Permissions.Count, Is.EqualTo(5));
                Assert.That(entity.Permissions.ElementAt(0).Key, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30.31"));
                Assert.That(entity.Permissions.ElementAt(0).Value, Is.EqualTo("read_write"));
                Assert.That(entity.Permissions.ElementAt(1).Key, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).6.157"));
                Assert.That(entity.Permissions.ElementAt(1).Value, Is.EqualTo("read_write"));
                Assert.That(entity.Permissions.ElementAt(2).Key, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30"));
                Assert.That(entity.Permissions.ElementAt(2).Value, Is.EqualTo("read_write"));
                Assert.That(entity.Permissions.ElementAt(3).Key, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:HC06(1.0)"));
                Assert.That(entity.Permissions.ElementAt(3).Value, Is.EqualTo("read_write"));
                Assert.That(entity.Permissions.ElementAt(4).Key, Is.EqualTo("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:DDB(1.0).30.264D"));
                Assert.That(entity.Permissions.ElementAt(4).Value, Is.EqualTo("read_write"));


                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [Test]
        public void TranscodingEntityParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/transcodingEntity.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.TimeMapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.TimeMapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as TimeDimensionMappingEntity;
                Assert.That(entity, Is.Not.Null);
                Assert.That(entity.EntityId, Is.EqualTo("6"));
                Assert.That(entity.ParentId, Is.EqualTo("6"));
                Assert.That(entity.ConstantValue, Is.Null);
                Assert.That(entity.DefaultValue, Is.Null);
                Assert.That(entity.SimpleMappingColumn, Is.Null);
                Assert.That(entity.HasTranscoding(), Is.True);
                Assert.That(entity.TimeMappingType, Is.EqualTo(TimeDimensionMappingType.TranscodingWithFrequencyDimension));
                Assert.That(entity.Transcoding,Is.Not.Null);
                var timeTranscoding = TimeTranscodingConversionHelper.Convert(entity.Transcoding);
                Assert.That(timeTranscoding[0].Frequency, Is.EqualTo("A"));
                Assert.That(timeTranscoding[0].Year,Is.Not.Null);
                Assert.That(timeTranscoding[0].Year.Column,Is.Not.Null);
                Assert.That(timeTranscoding[0].Year.Column.EntityId,Is.EqualTo("21"));
                Assert.That(timeTranscoding[0].Year.Column.Name,Is.EqualTo("TIME_PERIOD1"));
                Assert.That(timeTranscoding[0].Year.Length,Is.EqualTo(4));
                Assert.That(timeTranscoding[0].Year.Start,Is.EqualTo(0));
                Assert.That(timeTranscoding[1].Frequency, Is.EqualTo("Q"));
                Assert.That(timeTranscoding[1].Year, Is.Not.Null);
                Assert.That(timeTranscoding[1].Year.Column, Is.Not.Null);
                Assert.That(timeTranscoding[1].Year.Column.EntityId, Is.EqualTo("21"));
                Assert.That(timeTranscoding[1].Year.Column.Name, Is.EqualTo("TIME_PERIOD"));
                Assert.That(timeTranscoding[1].Year.Length, Is.EqualTo(4));
                Assert.That(timeTranscoding[1].Year.Start, Is.EqualTo(0));
                Assert.That(timeTranscoding[1].Period, Is.Not.Null);
                Assert.That(timeTranscoding[1].Period.Column, Is.Not.Null);
                Assert.That(timeTranscoding[1].Period.Column.EntityId, Is.EqualTo("21"));
                Assert.That(timeTranscoding[1].Period.Column.Name, Is.EqualTo("TIME_PERIOD"));
                Assert.That(timeTranscoding[1].Period.Length, Is.EqualTo(6));
                Assert.That(timeTranscoding[1].Period.Start, Is.EqualTo(4));
                Assert.That(timeTranscoding[1].Period.Rules.Count, Is.EqualTo(4));
                Assert.That(timeTranscoding[1].Period.Rules[0].DsdCodeEntity.ObjectId, Is.EqualTo("Q1"));
                Assert.That(timeTranscoding[1].Period.Rules[0].LocalCodes[0].ObjectId, Is.EqualTo("200501"));
                Assert.That(timeTranscoding[1].Period.Rules[0].LocalCodes[0].ParentId, Is.EqualTo("21"));

                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as TimeDimensionMappingEntity;
                Assert.That(entity, Is.Not.Null);
                Assert.That(entity.EntityId, Is.EqualTo("11"));
                Assert.That(entity.ParentId, Is.EqualTo("11"));
                Assert.That(entity.ConstantValue, Is.Null);
                Assert.That(entity.DefaultValue, Is.Null);
                Assert.That(entity.SimpleMappingColumn, Is.EqualTo("COMPLIANT_TIME_PERIOD"));
                Assert.That(entity.TimeMappingType, Is.EqualTo(TimeDimensionMappingType.MappingWithColumn));
                Assert.That(entity.HasTranscoding(), Is.False);
                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as TimeDimensionMappingEntity;
                Assert.That(entity, Is.Not.Null);
                Assert.That(entity.EntityId, Is.EqualTo("28"));
                Assert.That(entity.ParentId, Is.EqualTo("28"));
                Assert.That(entity.ConstantValue, Is.Null);
                Assert.That(entity.DefaultValue, Is.Null);
                Assert.That(entity.SimpleMappingColumn, Is.Null);
                Assert.That(entity.HasTranscoding(), Is.True);
                Assert.That(entity.Transcoding, Is.Not.Null);
                Assert.That(entity.TimeMappingType, Is.EqualTo(TimeDimensionMappingType.TranscodingWithFrequencyDimension));
                timeTranscoding = TimeTranscodingConversionHelper.Convert(entity.Transcoding);
                Assert.That(timeTranscoding[0].Year, Is.Not.Null);
                Assert.That(timeTranscoding[0].Frequency, Is.EqualTo("M"));
                Assert.That(timeTranscoding[0].Year.Column, Is.Not.Null);
                Assert.That(timeTranscoding[0].Year.Column.EntityId, Is.EqualTo("34"));
                Assert.That(timeTranscoding[0].Year.Column.Name, Is.EqualTo("TIME_PERIOD"));
                Assert.That(timeTranscoding[0].Year.Length, Is.EqualTo(4));
                Assert.That(timeTranscoding[0].Year.Start, Is.EqualTo(0));
                Assert.That(timeTranscoding[0].Period, Is.Not.Null);
                Assert.That(timeTranscoding[0].Period.Column, Is.Not.Null);
                Assert.That(timeTranscoding[0].Period.Column.EntityId, Is.EqualTo("34"));
                Assert.That(timeTranscoding[0].Period.Column.Name, Is.EqualTo("TIME_PERIOD"));
                Assert.That(timeTranscoding[0].Period.Length, Is.EqualTo(-1));
                Assert.That(timeTranscoding[0].Period.Start, Is.EqualTo(5));
                Assert.That(timeTranscoding[0].Period.Rules.Count, Is.EqualTo(12));
                Assert.That(timeTranscoding[0].Period.Rules.All(x => !string.IsNullOrWhiteSpace(x.DsdCodeEntity?.ObjectId)));
                Assert.That(timeTranscoding[0].Period.Rules.All(x => x.LocalCodes?.Count == 1));
                Assert.That(timeTranscoding[0].Period.Rules.SelectMany(x => x.LocalCodes).All(x => !string.IsNullOrWhiteSpace(x.ObjectId)));

            }
        }

        [Test]
        public void TranscodingEntityWithAdvancedTimeTranscodingParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/advancedTimeTranscoding.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.TimeMapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.TimeMapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as TimeDimensionMappingEntity;
                Assert.NotNull(entity);
                Assert.NotNull(entity.Transcoding);
                var criteriaColumn = entity.Transcoding.CriteriaColumn;
                Assert.That(criteriaColumn, Is.Not.Null);
                Assert.That(criteriaColumn.Name, Is.EqualTo("ATECO_2007"));

                IDictionary<string, TimeFormatConfiguration> configs = entity.Transcoding.TimeFormatConfigurations;
                Assert.AreEqual(16, configs.Count);
                TimeFormatConfiguration current = null;
                TimeParticleConfiguration currentParticle = null;

                // mostly auto generated
                Assert.That(configs, Contains.Key("date"));
                current = configs["date"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("date"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.Date));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.TimestampOrDate));
                Assert.That(currentParticle.DateColumn, Is.Not.Null);
                Assert.That(currentParticle.DateColumn.Name, Is.EqualTo("ITTER107"));
                Assert.That(configs, Contains.Key("end_c_duration_c"));
                current = configs["end_c_duration_c"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("end_c_duration_c"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.TimeRange));
                Assert.That(current.EndConfig, Is.Not.Null);
                currentParticle = current.EndConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Iso8601Compatible));
                Assert.That(currentParticle.DateColumn, Is.Not.Null);
                Assert.That(currentParticle.DateColumn.Name, Is.EqualTo("CL_IMPORTO1"));
                Assert.That(current.DurationMap, Is.Not.Null);
                Assert.That(configs, Contains.Key("end_dur_1_col"));
                current = configs["end_dur_1_col"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("end_dur_1_col"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.TimeRange));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Iso8601Compatible));
                Assert.That(currentParticle.DateColumn, Is.Not.Null);
                Assert.That(currentParticle.DateColumn.Name, Is.EqualTo("ATECO_2007"));
                Assert.That(configs, Contains.Key("end_nc_m_duration_c"));
                current = configs["end_nc_m_duration_c"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("end_nc_m_duration_c"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.TimeRange));
                Assert.That(current.EndConfig, Is.Not.Null);
                currentParticle = current.EndConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Monthly));
                Assert.That(currentParticle.Year, Is.Not.Null);
                Assert.That(currentParticle.Year.Column, Is.Not.Null);
                Assert.That(currentParticle.Year.Start, Is.EqualTo(0));
                Assert.That(currentParticle.Period, Is.Not.Null);
                Assert.That(currentParticle.Period.Column, Is.Not.Null);
                Assert.That(currentParticle.Period.Start, Is.EqualTo(0));
                Assert.That(current.DurationMap, Is.Not.Null);
                Assert.That(configs, Contains.Key("end_nc_m_duration_nc"));
                current = configs["end_nc_m_duration_nc"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("end_nc_m_duration_nc"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.TimeRange));
                Assert.That(current.EndConfig, Is.Not.Null);
                currentParticle = current.EndConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Monthly));
                Assert.That(currentParticle.Year, Is.Not.Null);
                Assert.That(currentParticle.Year.Column, Is.Not.Null);
                Assert.That(currentParticle.Year.Start, Is.EqualTo(0));
                Assert.That(currentParticle.Period, Is.Not.Null);
                Assert.That(currentParticle.Period.Column, Is.Not.Null);
                Assert.That(currentParticle.Period.Start, Is.EqualTo(0));
                Assert.That(current.DurationMap, Is.Not.Null);
                Assert.That(configs, Contains.Key("reporting_month"));
                current = configs["reporting_month"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("reporting_month"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.QuarterOfYear));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Iso8601Compatible));
                Assert.That(currentParticle.DateColumn, Is.Not.Null);
                Assert.That(currentParticle.DateColumn.Name, Is.EqualTo("AREEAMB"));
                Assert.That(configs, Contains.Key("reporting_quarter"));
                current = configs["reporting_quarter"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("reporting_quarter"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.QuarterOfYear));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Quarterly));
                Assert.That(currentParticle.Year, Is.Not.Null);
                Assert.That(currentParticle.Year.Column, Is.Not.Null);
                Assert.That(currentParticle.Year.Start, Is.EqualTo(0));
                Assert.That(currentParticle.Period, Is.Not.Null);
                Assert.That(currentParticle.Period.Column, Is.Not.Null);
                Assert.That(currentParticle.Period.Start, Is.EqualTo(5));
                Assert.That(configs, Contains.Key("reporting_year"));
                current = configs["reporting_year"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("reporting_year"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.ReportingYear));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Annual));
                Assert.That(currentParticle.Year, Is.Not.Null);
                Assert.That(currentParticle.Year.Column, Is.Not.Null);
                Assert.That(currentParticle.Year.Start, Is.EqualTo(0));
                Assert.That(configs, Contains.Key("start_c_duration_cons"));
                current = configs["start_c_duration_cons"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("start_c_duration_cons"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.TimeRange));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.TimestampOrDate));
                Assert.That(currentParticle.DateColumn, Is.Not.Null);
                Assert.That(currentParticle.DateColumn.Name, Is.EqualTo("DATE_COL"));
                Assert.That(current.DurationMap, Is.Not.Null);
                Assert.That(configs, Contains.Key("start_end_1_col"));
                current = configs["start_end_1_col"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("start_end_1_col"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.TimeRange));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Iso8601Compatible));
                Assert.That(currentParticle.DateColumn, Is.Not.Null);
                Assert.That(currentParticle.DateColumn.Name, Is.EqualTo("FLAGS"));
                Assert.That(configs, Contains.Key("start_end_2_col"));
                current = configs["start_end_2_col"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("start_end_2_col"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.TimeRange));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.TimestampOrDate));
                Assert.That(currentParticle.DateColumn, Is.Not.Null);
                Assert.That(currentParticle.DateColumn.Name, Is.EqualTo("ITTER107"));
                Assert.That(current.EndConfig, Is.Not.Null);
                currentParticle = current.EndConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Iso8601Compatible));
                Assert.That(currentParticle.DateColumn, Is.Not.Null);
                Assert.That(currentParticle.DateColumn.Name, Is.EqualTo("CL_IMPORTO1"));
                Assert.That(configs, Contains.Key("start_end_2_col_non_compl"));
                current = configs["start_end_2_col_non_compl"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("start_end_2_col_non_compl"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.TimeRange));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.TimestampOrDate));
                Assert.That(currentParticle.DateColumn, Is.Not.Null);
                Assert.That(currentParticle.DateColumn.Name, Is.EqualTo("ITTER107"));
                Assert.That(current.EndConfig, Is.Not.Null);
                currentParticle = current.EndConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Monthly));
                Assert.That(currentParticle.Year, Is.Not.Null);
                Assert.That(currentParticle.Year.Column, Is.Not.Null);
                Assert.That(currentParticle.Year.Start, Is.EqualTo(0));
                Assert.That(currentParticle.Period, Is.Not.Null);
                Assert.That(currentParticle.Period.Column, Is.Not.Null);
                Assert.That(currentParticle.Period.Start, Is.EqualTo(0));
                Assert.That(configs, Contains.Key("start_nc_a_duration_nc"));
                current = configs["start_nc_a_duration_nc"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("start_nc_a_duration_nc"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.TimeRange));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Annual));
                Assert.That(currentParticle.Year, Is.Not.Null);
                Assert.That(currentParticle.Year.Column, Is.Not.Null);
                Assert.That(currentParticle.Year.Start, Is.EqualTo(0));
                Assert.That(current.DurationMap, Is.Not.Null);
                Assert.That(configs, Contains.Key("start_nc_end_2_col_non_compl"));
                current = configs["start_nc_end_2_col_non_compl"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("start_nc_end_2_col_non_compl"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.TimeRange));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Quarterly));
                Assert.That(currentParticle.Year, Is.Not.Null);
                Assert.That(currentParticle.Year.Column, Is.Not.Null);
                Assert.That(currentParticle.Year.Start, Is.EqualTo(0));
                Assert.That(currentParticle.Period, Is.Not.Null);
                Assert.That(currentParticle.Period.Column, Is.Not.Null);
                Assert.That(currentParticle.Period.Start, Is.EqualTo(5));
                Assert.That(current.EndConfig, Is.Not.Null);
                currentParticle = current.EndConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Monthly));
                Assert.That(currentParticle.Year, Is.Not.Null);
                Assert.That(currentParticle.Year.Column, Is.Not.Null);
                Assert.That(currentParticle.Year.Start, Is.EqualTo(0));
                Assert.That(currentParticle.Period, Is.Not.Null);
                Assert.That(currentParticle.Period.Column, Is.Not.Null);
                Assert.That(currentParticle.Period.Start, Is.EqualTo(0));
                Assert.That(configs, Contains.Key("start_nc_m_duration_nc"));
                current = configs["start_nc_m_duration_nc"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("start_nc_m_duration_nc"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.TimeRange));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Monthly));
                Assert.That(currentParticle.Year, Is.Not.Null);
                Assert.That(currentParticle.Year.Column, Is.Not.Null);
                Assert.That(currentParticle.Year.Start, Is.EqualTo(0));
                Assert.That(currentParticle.Period, Is.Not.Null);
                Assert.That(currentParticle.Period.Column, Is.Not.Null);
                Assert.That(currentParticle.Period.Start, Is.EqualTo(0));
                Assert.That(current.DurationMap, Is.Not.Null);
                Assert.That(configs, Contains.Key("yearMonth"));
                current = configs["yearMonth"];
                Assert.That(current.HasParticleConfiguration);
                Assert.That(current.CriteriaValue, Is.EqualTo("yearMonth"));
                Assert.That(current.OutputFormat, Is.EqualTo(TimeFormat.Month));
                Assert.That(current.StartConfig, Is.Not.Null);
                currentParticle = current.StartConfig;
                Assert.That(currentParticle.Format, Is.EqualTo(DisseminationDatabaseTimeFormat.Quarterly));
                Assert.That(currentParticle.Year, Is.Not.Null);
                Assert.That(currentParticle.Year.Column, Is.Not.Null);
                Assert.That(currentParticle.Year.Column.Name, Is.EqualTo("CLADDETTI"));
                Assert.That(currentParticle.Year.Start, Is.EqualTo(0));
                Assert.That(currentParticle.Year.Length, Is.EqualTo(4));
                Assert.That(currentParticle.Period, Is.Not.Null);
                Assert.That(currentParticle.Period.Column, Is.Not.Null);
                Assert.That(currentParticle.Period.Column.Name, Is.EqualTo("TIME_PERIOD"));
                Assert.That(currentParticle.Period.Start, Is.EqualTo(0));
                Assert.That(currentParticle.Period.Length, Is.EqualTo(3));
                Assert.That(currentParticle.Period.Rules, Has.Count.EqualTo(5));
                Assert.That(currentParticle.Period.Rules[0].SdmxPeriodCode, Is.EqualTo("01"));
                Assert.That(currentParticle.Period.Rules[0].LocalPeriodCode, Is.EqualTo("JAN"));
                Assert.That(currentParticle.Period.Rules[4].SdmxPeriodCode, Is.EqualTo("12"));
                Assert.That(currentParticle.Period.Rules[4].LocalPeriodCode, Is.EqualTo("DEC"));

            }
        }
        [Test]
        public void TranscodingWithDateTimeEntityParseTestWithNullYearPeriod()
        {
            FileInfo source = new FileInfo("tests/jsonParser/transcodingTimeDateTimeYearPeriodNull.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.TimeMapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.TimeMapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as TimeDimensionMappingEntity;
                Assert.NotNull(entity);
                Assert.That(entity.EntityId, Is.Empty);
                Assert.That(entity.ParentId, Is.EqualTo("184"));
                Assert.That(entity.HasTranscoding());
                var timeTranscodingEntities = entity.Transcoding.AsOldTimeTranscoding();
                Assert.That(timeTranscodingEntities, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Frequency, Is.EqualTo("A"));
                Assert.That(timeTranscodingEntities[0].Year, Is.Null);
                Assert.That(timeTranscodingEntities[0].Period, Is.Null);
                Assert.That(timeTranscodingEntities[0].IsDateTime, Is.True);
                Assert.That(timeTranscodingEntities[0].DateColumn, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].DateColumn.EntityId, Is.EqualTo("160"));
                Assert.That(timeTranscodingEntities[0].DateColumn.Name, Is.EqualTo("STS_SDMX_DATA1_TIME_PERIOD"));
                Assert.That(!reader.MoveToNextEntity());
            }
        }

        [Test]
        public void TranscodingWithDateTimeEntityParseTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/transcodingTimeDateTime.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.TimeMapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.TimeMapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as TimeDimensionMappingEntity;
                Assert.NotNull(entity);
                Assert.That(entity.EntityId, Is.EqualTo("6"));
                Assert.That(entity.ParentId, Is.EqualTo("9"));
                Assert.That(entity.HasTranscoding());
                var timeTranscodingEntities = entity.Transcoding.AsOldTimeTranscoding();
                Assert.That(timeTranscodingEntities, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Frequency, Is.EqualTo("A"));
                Assert.That(timeTranscodingEntities[0].Year, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Year.Column, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Year.Column.EntityId, Is.EqualTo("21"));
                Assert.That(timeTranscodingEntities[0].Year.Column.Name, Is.EqualTo("TIME_PERIOD1"));
                Assert.That(timeTranscodingEntities[0].Year.Length, Is.EqualTo(4));
                Assert.That(timeTranscodingEntities[0].Year.Start, Is.EqualTo(0));
                Assert.That(timeTranscodingEntities[1].Frequency, Is.EqualTo("M"));
                Assert.That(timeTranscodingEntities[1].Year, Is.Null);
                Assert.That(timeTranscodingEntities[1].Period, Is.Null);
                Assert.That(timeTranscodingEntities[1].IsDateTime, Is.True);
                Assert.That(timeTranscodingEntities[1].DateColumn, Is.Not.Null);
                Assert.That(timeTranscodingEntities[1].DateColumn.EntityId, Is.EqualTo("669"));
                Assert.That(timeTranscodingEntities[1].DateColumn.Name, Is.EqualTo("sts_sdmx_transQ1_DATEC"));
                Assert.That(timeTranscodingEntities[2].Frequency, Is.EqualTo("Q"));
                Assert.That(timeTranscodingEntities[2].Year, Is.Not.Null);
                Assert.That(timeTranscodingEntities[2].Year.Column, Is.Not.Null);
                Assert.That(timeTranscodingEntities[2].Year.Column.EntityId, Is.EqualTo("21"));
                Assert.That(timeTranscodingEntities[2].Year.Column.Name, Is.EqualTo("TIME_PERIOD"));
                Assert.That(timeTranscodingEntities[2].Year.Length, Is.EqualTo(4));
                Assert.That(timeTranscodingEntities[2].Year.Start, Is.EqualTo(0));
                Assert.That(timeTranscodingEntities[2].Period, Is.Not.Null);
                Assert.That(timeTranscodingEntities[2].Period.Column, Is.Not.Null);
                Assert.That(timeTranscodingEntities[2].Period.Column.EntityId, Is.EqualTo("21"));
                Assert.That(timeTranscodingEntities[2].Period.Column.Name, Is.EqualTo("TIME_PERIOD"));
                Assert.That(timeTranscodingEntities[2].Period.Length, Is.EqualTo(6));
                Assert.That(timeTranscodingEntities[2].Period.Start, Is.EqualTo(4));
                Assert.That(timeTranscodingEntities[2].Period.Rules.Count, Is.EqualTo(4));
                Assert.That(timeTranscodingEntities[2].Period.Rules[0].DsdCodeEntity.ObjectId, Is.EqualTo("Q1"));
                Assert.That(timeTranscodingEntities[2].Period.Rules[0].LocalCodes[0].ObjectId, Is.EqualTo("200501"));
                Assert.That(timeTranscodingEntities[2].Period.Rules[0].LocalCodes[0].ParentId, Is.EqualTo("21"));

                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as TimeDimensionMappingEntity;
                Assert.NotNull(entity);
                Assert.That(entity.HasTranscoding(), Is.False);
                Assert.That(entity.EntityId, Is.EqualTo("11"));
                Assert.That(entity.ParentId, Is.EqualTo("10"));
                Assert.That(entity.SimpleMappingColumn, Is.EqualTo("DATE_TIME_COL"));
                Assert.That(reader.MoveToNextEntity());
                
                entity = reader.CurrentEntity.Entity as TimeDimensionMappingEntity;
                Assert.NotNull(entity);
                Assert.That(entity.EntityId, Is.EqualTo("28"));
                Assert.That(entity.ParentId, Is.EqualTo("43"));
                Assert.That(entity.HasTranscoding());
                timeTranscodingEntities = entity.Transcoding.AsOldTimeTranscoding();
                Assert.That(timeTranscodingEntities, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Year, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Frequency, Is.EqualTo("M"));
                Assert.That(timeTranscodingEntities[0].Year.Column, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Year.Column.Name, Is.EqualTo("TIME_PERIOD"));
                Assert.That(timeTranscodingEntities[0].Year.Length, Is.EqualTo(4));
                Assert.That(timeTranscodingEntities[0].Year.Start, Is.EqualTo(0));
                Assert.That(timeTranscodingEntities[0].Period, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Period.Column, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Period.Column.Name, Is.EqualTo("TIME_PERIOD"));
                Assert.That(timeTranscodingEntities[0].Period.Length, Is.EqualTo(-1));
                Assert.That(timeTranscodingEntities[0].Period.Start, Is.EqualTo(5));
                Assert.That(timeTranscodingEntities[0].Period.Rules.Count, Is.EqualTo(12));
                Assert.That(timeTranscodingEntities[0].Period.Rules.All(x => !string.IsNullOrWhiteSpace(x.DsdCodeEntity?.ObjectId)));
                Assert.That(timeTranscodingEntities[0].Period.Rules.All(x => x.LocalCodes?.Count == 1));
                Assert.That(timeTranscodingEntities[0].Period.Rules.SelectMany(x => x.LocalCodes).All(x => !string.IsNullOrWhiteSpace(x.ObjectId)));

            }
        }
        [Test]
        public void SimpleTimeMappingWithColumn()
        {
            FileInfo source = new FileInfo("tests/jsonParser/timemapping-simple.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.TimeMapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.TimeMapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as TimeDimensionMappingEntity;
                Assert.NotNull(entity);
                Assert.That(entity.EntityId, Is.EqualTo("7"));
                //Assert.That(entity.ParentId, Is.EqualTo("9"));
                Assert.That(entity.SimpleMappingColumn, Is.Not.Null.And.Not.Empty);
                Assert.That(!entity.HasTranscoding());
                Assert.That(!reader.MoveToNextEntity());
           }
        }

        [Test]
        public void TranscodingEntityParseTestMultipleFrequencies()
        {
            FileInfo source = new FileInfo("tests/jsonParser/transcoding-multiple-frequencies.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.TimeMapping))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.TimeMapping));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as TimeDimensionMappingEntity;
                Assert.That(entity.EntityId, Is.EqualTo("20234"));
                Assert.That(entity.ParentId, Is.EqualTo("20234"));
                Assert.That(entity.ConstantValue, Is.Null);
                Assert.That(entity.PreFormatted, Is.Null);
                var timeTranscodingEntities = entity.Transcoding.AsOldTimeTranscoding();
                Assert.That(timeTranscodingEntities, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Frequency, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Frequency, Is.EqualTo("A"));
                Assert.That(timeTranscodingEntities[0].Year, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Year.Column, Is.Not.Null);
                Assert.That(timeTranscodingEntities[0].Year.Column.EntityId, Is.EqualTo("481"));
                Assert.That(timeTranscodingEntities[0].Year.Column.Name, Is.EqualTo("sts_sdmx_transQ1_YEAR_1COL"));
                Assert.That(timeTranscodingEntities[0].Year.Length, Is.EqualTo(4));
                Assert.That(timeTranscodingEntities[0].Year.Start, Is.EqualTo(0));
                Assert.That(timeTranscodingEntities[1].Year, Is.Not.Null);
                Assert.That(timeTranscodingEntities[1].Frequency, Is.EqualTo("M"));
                Assert.That(timeTranscodingEntities[1].Year.Column, Is.Not.Null);
                Assert.That(timeTranscodingEntities[1].Year.Column.EntityId, Is.EqualTo("481"));
                Assert.That(timeTranscodingEntities[1].Year.Column.Name, Is.EqualTo("sts_sdmx_transQ1_YEAR_1COL"));
                Assert.That(timeTranscodingEntities[1].Year.Length, Is.EqualTo(4));
                Assert.That(timeTranscodingEntities[1].Year.Start, Is.EqualTo(0));
                Assert.That(timeTranscodingEntities[1].Period, Is.Not.Null);
                Assert.That(timeTranscodingEntities[1].Period.Column, Is.Not.Null);
                Assert.That(timeTranscodingEntities[1].Period.Column.EntityId, Is.EqualTo("478"));
                Assert.That(timeTranscodingEntities[1].Period.Column.Name, Is.EqualTo("sts_sdmx_transQ1_MONTH"));
                Assert.That(timeTranscodingEntities[1].Period.Length, Is.EqualTo(-1));
                Assert.That(timeTranscodingEntities[1].Period.Start, Is.EqualTo(0));
                Assert.That(timeTranscodingEntities[1].Period.Rules.Count, Is.EqualTo(12));
                Assert.That(timeTranscodingEntities[1].Period.Rules.All(x => !string.IsNullOrWhiteSpace(x.DsdCodeEntity?.ObjectId)));
                Assert.That(timeTranscodingEntities[1].Period.Rules.All(x => x.LocalCodes?.Count == 1));
                Assert.That(timeTranscodingEntities[1].Period.Rules.SelectMany(x => x.LocalCodes).All(x => !string.IsNullOrWhiteSpace(x.ObjectId)));
                Assert.That(timeTranscodingEntities[2].Frequency, Is.EqualTo("Q"));
                Assert.That(timeTranscodingEntities[2].Period.Rules.Count, Is.EqualTo(4));
                Assert.That(timeTranscodingEntities[2].Year, Is.Not.Null);
                Assert.That(timeTranscodingEntities[2].Year.Column, Is.Not.Null);
                Assert.That(timeTranscodingEntities[2].Year.Column.EntityId, Is.EqualTo("452"));
                Assert.That(timeTranscodingEntities[2].Year.Column.Name, Is.EqualTo("sts_sdmx_transQ1_TIME_PERIOD"));
                Assert.That(timeTranscodingEntities[2].Year.Length, Is.EqualTo(4));
                Assert.That(timeTranscodingEntities[2].Year.Start, Is.EqualTo(0));
                Assert.That(timeTranscodingEntities[2].Period, Is.Not.Null);
                Assert.That(timeTranscodingEntities[2].Period.Column, Is.Not.Null);
                Assert.That(timeTranscodingEntities[2].Period.Column.EntityId, Is.EqualTo("452"));
                Assert.That(timeTranscodingEntities[2].Period.Column.Name, Is.EqualTo("sts_sdmx_transQ1_TIME_PERIOD"));
                Assert.That(timeTranscodingEntities[2].Period.Length, Is.EqualTo(-1));
                Assert.That(timeTranscodingEntities[2].Period.Start, Is.EqualTo(4));
                Assert.That(timeTranscodingEntities[2].Period.Rules.All(x => !string.IsNullOrWhiteSpace(x.DsdCodeEntity?.ObjectId)));
                Assert.That(timeTranscodingEntities[2].Period.Rules.All(x => x.LocalCodes?.Count == 1));
                Assert.That(timeTranscodingEntities[2].Period.Rules.SelectMany(x => x.LocalCodes).All(x => !string.IsNullOrWhiteSpace(x.ObjectId)));


                Assert.That(!reader.MoveToNextEntity());
            }
        }

        [Test]
        public void SkipUnknownPropertiesTest()
        {
            FileInfo source = new FileInfo("tests/jsonParser/entitiesWithExtraProperties.json");
            using (Stream stream = source.OpenRead())
            using (EntityStreamingReader reader = new EntityStreamingReader("MappingStoreServer", stream, EntityType.DataSet))
            {
                Assert.That(reader.MoveToNextMessage());
                Assert.That(reader.MoveToNextEntityTypeSection());
                Assert.That(reader.CurrentEntityType, Is.EqualTo(EntityType.DataSet));
                Assert.That(reader.MoveToNextEntity());
                var entity = reader.CurrentEntity.Entity as DatasetEntity;
                Assert.That(entity.ParentId, Is.EqualTo("1"));
                Assert.That(entity.Name, Is.EqualTo("mydataset"));
                Assert.That(entity.Description, Is.EqualTo("MyDescription"));
                Assert.That(entity.Query, Is.EqualTo("test query"));
                Assert.That(entity.JSONQuery, Is.EqualTo("{\r\n  \"value\": \"extra data\"\r\n}"));

                Assert.That(reader.MoveToNextEntity());
                entity = reader.CurrentEntity.Entity as DatasetEntity;
                Assert.That(entity.ParentId, Is.EqualTo("2"));
                Assert.That(entity.Name, Is.EqualTo("test2"));
                Assert.That(entity.Description, Is.EqualTo("MyDescription"));
                Assert.That(entity.Query, Is.EqualTo("test query"));
                Assert.That(entity.JSONQuery, Is.EqualTo("{\r\n  \"value\": \"extra data\"\r\n}"));


                // No more entities
                Assert.That(!reader.MoveToNextEntity());
                Assert.That(!reader.MoveToNextEntityTypeSection());
                Assert.That(!reader.MoveToNextMessage());
            }
        }

        [TestCase]
        public void TestStoreSpecialCase()
        {
            MappingStoreConnectionEntity entity = null;
            MappingStoreConnectionReader parser = new MappingStoreConnectionReader();
            FileInfo source = new FileInfo("tests/jsonParser/store.json");
            using (var reader = source.OpenText())
            using (var jsonTextReader = new JsonTextReader(reader))
            {
                entity = parser.ParseEntity(jsonTextReader);
            }

            Assert.That(entity.Name, Is.EqualTo("MappingStoreServer"));
            Assert.That(entity.DatabaseVendorType, Is.EqualTo("SqlServer"));
            Assert.That(entity.StoreType, Is.EqualTo("msdb"));
            Assert.That(entity.DbName, Is.EqualTo("MASTORE"));
            Assert.That(entity.Settings["database"].Value, Is.EqualTo("MASTORE"));
            Assert.That(entity.Settings["server"].Value, Is.EqualTo("sqlserverhost"));
            Assert.That(entity.Settings["userid"].Value, Is.EqualTo("mauser"));
            Assert.That(entity.Settings["password"].Value, Is.EqualTo("123"));
            // TODO the port might be stored as a string
            Assert.That(entity.Settings["port"].Value, Is.EqualTo("1433"));
        }
        [TestCase]
        public void TestStoreFromMaweb()
        {
            MappingStoreConnectionEntity entity = null;
            MappingStoreConnectionReader parser = new MappingStoreConnectionReader();
            FileInfo source = new FileInfo("tests/jsonParser/store-from-maweb.json");
            using (var reader = source.OpenText())
            using (var jsonTextReader = new JsonTextReader(reader))
            {
                entity = parser.ParseEntity(jsonTextReader);
            }

            Assert.That(entity.Name, Is.EqualTo("MappingStoreServer"));
            Assert.That(entity.DatabaseVendorType, Is.EqualTo("SqlServer"));
            Assert.That(entity.StoreType, Is.EqualTo("msdb"));
            Assert.That(entity.DbName, Is.EqualTo("MASTORE"));
            Assert.That(entity.Settings["database"].Value, Is.EqualTo("MASTORE"));
            Assert.That(entity.Settings["server"].Value, Is.EqualTo("sqlserverhost"));
            Assert.That(entity.Settings["user ID"].Value, Is.EqualTo("mauser"));
            Assert.That(entity.Settings["password"].Value, Is.EqualTo("123"));
            // TODO the port might be stored as a string
            Assert.That(entity.Settings["port"].Value, Is.EqualTo("1433"));
        }
    }


}
