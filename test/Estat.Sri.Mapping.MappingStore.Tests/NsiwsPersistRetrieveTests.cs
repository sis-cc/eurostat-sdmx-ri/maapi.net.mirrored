// -----------------------------------------------------------------------
// <copyright file="NsiwsPersistRetrieveTests.cs" company="EUROSTAT">
//   Date Created : 2017-06-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Extension;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using NUnit.Framework;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("msdb_scratch.sqlserver")]
    [TestFixture("msdb_scratch.oracle")]
    public class NsiwsPersistRetrieveTests : BaseTestClassWithContainer
    {
        private readonly IEntityPersistenceManager _persistManager;

        private readonly IEntityRetrieverManager _retrieveManager;



        public NsiwsPersistRetrieveTests(string name) : base(name)
        {
            _persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            _retrieveManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
        }

        [OneTimeSetUp]
        public void Setup()
        {
            InitializeMappingStore(this.StoreId);
        }


        [Test]
        public void InsertANsiWsSetting()
        {

            List<NsiwsEntity> entitiesToAdd = new List<NsiwsEntity>()
            {
                new()
                {
                    Url = "http://localhost/rest2",
                    UserName = "testName",
                    Password = "testPassword",
                    Name = "RestV2 server",
                    Technology = "RestV20",
                    StoreId = this.StoreId
                },
                new()
                {
                    Url = "http://localhost/rest",
                    Name = "RestV1 server",
                    Technology = "REst",
                    StoreId = this.StoreId

                },
                new()
                {
                    Url = "http://localhost/soap20",
                    Name = "SOAP 2.0 server",
                    Technology = "SoapV20",
                    StoreId = this.StoreId

                },
                new()
                {
                    Url = "http://localhost/soap21",
                    Name = "SOAP 2.1 server",
                    Technology = "SoapV21",
                    Proxy = true,
                    StoreId = this.StoreId

                }

            };
            var addedEntities = _persistManager.AddEntities(entitiesToAdd).ToList();
            Assert.IsNotNull(addedEntities);
            Assert.IsTrue(addedEntities.Any());
            Assert.AreEqual(entitiesToAdd.Count, addedEntities.Count);
            Assert.IsFalse(addedEntities.Any(r => string.IsNullOrEmpty(r.EntityId)));
            // Oracle doesn't return in the same order that were added
            foreach (var entity in addedEntities)
            {
                var retrieved = _retrieveManager.GetEntities<NsiwsEntity>(this.StoreId, entity.QueryForThisEntityId(), Detail.Full).FirstOrDefault();
                _persistManager.Delete<NsiwsEntity>(this.StoreId, entity.EntityId);
                Assert.IsNotNull(retrieved);
                Assert.AreEqual(entity.EntityId, retrieved.EntityId);
                Assert.AreEqual(entity.Technology.ToUpperInvariant(), retrieved.Technology.ToUpperInvariant());
                Assert.AreEqual(entity.Name, retrieved.Name);
                Assert.AreEqual(entity.Url, retrieved.Url);
                Assert.AreEqual(entity.Proxy, retrieved.Proxy);
                Assert.AreEqual(entity.UserName, retrieved.UserName);
                Assert.AreEqual(entity.Password, retrieved.Password);
            }
        }
    }
}
