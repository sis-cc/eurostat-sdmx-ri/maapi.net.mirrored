using System;
using System.Linq;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using DataSetColumnEntity = Estat.Sri.Mapping.Api.Model.DataSetColumnEntity;
using MappingSetEntity = Estat.Sri.Mapping.Api.Model.MappingSetEntity;
using TimeTranscodingEntity = Estat.Sri.Mapping.Api.Model.TimeTranscodingEntity;
using TranscodingEntity = Estat.Sri.Mapping.Api.Model.TranscodingEntity;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    [TestFixture("sqlserver")]
    class TestUpdateEntity: BaseTestClassWithContainer
    {
        private string _storeId;
        private string _somestring;

        public TestUpdateEntity(string storeId)
            : base(storeId)
        {
            this._storeId = storeId;
            this._somestring = "somestring";
        }

        [Test]
        public void TestMappingSetEntity()
        {
            var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            var mappingSetEntity = entityRetrieverManager.GetEntities<MappingSetEntity>(this._storeId,new EntityQuery()
            {
                EntityId = new Criteria<string>(OperatorType.Exact,"1")
            },Detail.Full).First();


            var dataSetId = "2";
            mappingSetEntity.DataSetId = dataSetId;
            var someDescription = "Some description 2";
            mappingSetEntity.Description = someDescription;
            var someName = "Some Name 3";
            mappingSetEntity.Name = someName;
            mappingSetEntity.ObjectId = "ObjectId 1";
            var manager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            manager.Update(mappingSetEntity);

            var updatedMappingSetEntity = entityRetrieverManager.GetEntities<MappingSetEntity>("sqlserver", new EntityQuery()
            {
                EntityId = new Criteria<string>(OperatorType.Exact, "1")
            }, Detail.Full).First();

            Assert.That(updatedMappingSetEntity.Description,Is.EqualTo(someDescription));
            Assert.That(updatedMappingSetEntity.DataSetId, Is.EqualTo(dataSetId));
            Assert.That(updatedMappingSetEntity.Name,Is.EqualTo(someName));
        }

        [Test]
        public void TestDDBconnectionEntity()
        {
            var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            var ddbConnectionEntity = entityRetrieverManager.GetEntities<DdbConnectionEntity>(this._storeId, new EntityQuery()
            {
                EntityId = new Criteria<string>(OperatorType.Exact, "74")
            }, Detail.Full).First();

           
            ddbConnectionEntity.AdoConnString = this._somestring;
            ddbConnectionEntity.DbType = this._somestring;
            ddbConnectionEntity.DbUser = this._somestring;
            ddbConnectionEntity.JdbcConnString = this._somestring;
            ddbConnectionEntity.Password = this._somestring;

            var manager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            manager.Update(ddbConnectionEntity);
        }

        [Test]
        public void TestDataSetColumn()
        {
            var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            var dataSetColumnEntity = entityRetrieverManager.GetEntities<DataSetColumnEntity>(this._storeId, new EntityQuery()
            {
                EntityId = new Criteria<string>(OperatorType.Exact, "3")
            }, Detail.Full).First();

            dataSetColumnEntity.LastRetrieval = DateTime.Now;
            dataSetColumnEntity.Name = this._somestring;
            dataSetColumnEntity.Description = this._somestring;

            var manager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            manager.Update(dataSetColumnEntity);
        }

        [Test]
        public void TestLocalCode()
        {
            var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            var localCodeEntity = entityRetrieverManager.GetEntities<LocalCodeEntity>(this._storeId, new EntityQuery()
            {
                EntityId = new Criteria<string>(OperatorType.Exact, "19553")
            }, Detail.Full).First();

            localCodeEntity.ObjectId = this._somestring;

            var manager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            manager.Update(localCodeEntity);
        }


        [Test]
        public void TestRegistry()
        {
            var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            var registryEntity = entityRetrieverManager.GetEntities<RegistryEntity>(this._storeId, new EntityQuery()
            {
                EntityId = new Criteria<string>(OperatorType.Exact, "1")
            }, Detail.Full).First();

            registryEntity.UserName = this._somestring;
            registryEntity.Password = this._somestring;
            registryEntity.Url = this._somestring;
            registryEntity.Description = this._somestring;
            registryEntity.Technology = this._somestring;

            var manager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            manager.Update(registryEntity);
        }

        [Test]
        public void TestDescSource()
        {
            var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            var descriptionSourceEntity = entityRetrieverManager.GetEntities<ColumnDescriptionSourceEntity>(this._storeId, new EntityQuery()
            {
                EntityId = new Criteria<string>(OperatorType.Exact, "7")
            }, Detail.Full).First();

            descriptionSourceEntity.DescriptionField = this._somestring;
            descriptionSourceEntity.DescriptionTable = this._somestring;
            descriptionSourceEntity.RelatedField = this._somestring;
            descriptionSourceEntity.ParentId = "1";

            var manager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            manager.Update(descriptionSourceEntity);
        }

        [Test]
        public void TestComponentMappingEntity()
        {
            var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            var mappingEntity = entityRetrieverManager.GetEntities<ComponentMappingEntity>(this._storeId, new EntityQuery()
            {
                EntityId = new Criteria<string>(OperatorType.Exact, "20589")
            }, Detail.Full).First();

            mappingEntity.ConstantValue = this._somestring;
            mappingEntity.DefaultValue = this._somestring;
            mappingEntity.Type = this._somestring;
            mappingEntity.Component = new Component()
            {
                EntityId = "3",
                ObjectId = "FREQ"
            };

            var dataSetColumnEntities = entityRetrieverManager.GetEntities<DataSetColumnEntity>(this._storeId, new EntityQuery()
            {
                EntityId = new Criteria<string>(OperatorType.GreaterThan, "3")
            }, Detail.Full);
            mappingEntity.ReplaceColumn("1",dataSetColumnEntities.ToList().Find(x=>x.EntityId == "4"));
            mappingEntity.AddColumn(dataSetColumnEntities.ToList().Find(x => x.EntityId == "5"));
            mappingEntity.RemoveColumn("3");

            var manager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            manager.Update(mappingEntity);
        }

        [Test]
        public void TestTranscoding()
        {
            var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            var transcodingEntity = entityRetrieverManager.GetEntities<TranscodingEntity>(this._storeId, new EntityQuery()
            {
                EntityId = new Criteria<string>(OperatorType.Exact, "10233")
            }, Detail.Full).First();


            transcodingEntity.Script[0].ScriptContent = this._somestring;
            var manager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            manager.Update(transcodingEntity);
        }

        [Test]
        public void TestHeader()
        {
            var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            var transcodingEntity = entityRetrieverManager.GetEntities<HeaderEntity>(this._storeId, new EntityQuery()
            {
                EntityId = new Criteria<string>(OperatorType.Exact, "642")
            }, Detail.Full).First();


            transcodingEntity.SdmxHeader.Sender =new PartyCore(null, this._somestring, null,null); 
            var manager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            manager.Update(transcodingEntity);
        }

        [Test]
        public void TestTemplateMapping()
        {
            var manager = this.IoCContainer.Resolve<IEntityPersistenceManager>();

            var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            var templateMapping = entityRetrieverManager.GetEntities<TemplateMapping>(
                this._storeId, new EntityQuery()
                {
                    EntityId = new Criteria<string>(OperatorType.Exact, "10828")
                }, Detail.Full).First();

            var timeTranscodingEntity = 
                new TimeTranscodingEntity()
                {
                    DateColumn = new DataSetColumnEntity()
                    {
                        LastRetrieval = DateTime.Today
                    },
                    Frequency = "M",
                    EntityId = "5"
                };
            templateMapping.TimeTranscoding.Add(timeTranscodingEntity);
            
        

            //transcodingEntity.Transcodings.Add(this._somestring,this._somestring);
            manager.Update(templateMapping);
        }


        [Test]
        public void TestUserEntity()
        {
            var manager = this.IoCContainer.Resolve<IEntityPersistenceManager>();

            var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            var userEntity = entityRetrieverManager.GetEntities<UserEntity>(
                this._storeId, new EntityQuery()
                {
                    EntityId = new Criteria<string>(OperatorType.Exact, "1")
                }, Detail.Full).First();

            userEntity.RemovePermission("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=IT1:157_80(1.0)");
            manager.Update(userEntity);
        }
    }
}
