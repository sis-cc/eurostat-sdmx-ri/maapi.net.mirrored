// -----------------------------------------------------------------------
// <copyright file="CopyManagerFromScratchTests.cs" company="EUROSTAT">
//   Date Created : 2017-09-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture("msdb_scratch.oracle")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.sqlserver")]
    public class CopyManagerFromScratchTests : BaseTestClassWithContainer
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(CopyManagerFromScratchTests));

        /// <summary>
        /// The connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The retriever manager
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;

        /// <summary>
        /// The persist manager
        /// </summary>
        private readonly IEntityPersistenceManager _persistManager;

        /// <summary>
        /// The retrieval manager
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _retrievalManager;

        /// <summary>
        /// The copy manager
        /// </summary>
        private readonly ICopyManager _copyManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="CopyManagerFromScratchTests"/> class.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        public CopyManagerFromScratchTests(string storeId)
            : base(storeId)
        {
            this._retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            this._persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            this._copyManager = this.IoCContainer.Resolve<ICopyManager>();

            _connectionStringSettings = GetConnectionStringSettings();
            _retrievalManager = new MappingStoreSdmxObjectRetrievalManager(_connectionStringSettings);
        }

        [SetUp]
        public void Setup()
        {
            ReBuildMappingStore();
        }

        [TestCase("msdb_scratch.oracle")]
        [TestCase("msdb_scratch.mariadb")]
        [TestCase("msdb_scratch.sqlserver")]
        public void CopyToAnotherMappingStore(string targetStoreId)
        {
            if (string.Equals(this.StoreId, targetStoreId))
            {
                Assert.Ignore("Skipping source equals target");
            }

            InitializeMappingStore(targetStoreId);
            var result = this._copyManager.Copy(this.StoreId, targetStoreId);
            Assert.That(result.Status, Is.EqualTo(StatusType.Success));
            CompareEntities<IConnectionEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<DatasetEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<DataSetColumnEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<LocalCodeEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<MappingSetEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<ComponentMappingEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<TranscodingEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<TranscodingRuleEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<HeaderEntity>(targetStoreId, EntityQuery.Empty);
            var targetSettings = GetConnectionStringSettings(targetStoreId);
            Assert.That(targetSettings, Is.Not.Null);
            var t = new MappingStoreSdmxObjectRetrievalManager(targetSettings);
            var dataflowObjects = t.GetDataflowObjects(new MaintainableRefObjectImpl(), false, true);
            var targetDataflows = _retrievalManager.GetMaintainableObjects<IDataflowObject>();
            Assert.That(dataflowObjects, Is.Not.Empty);
            Assert.That(dataflowObjects.Count, Is.EqualTo(targetDataflows.Count));
        }

        [Test]
        public void CopyDatasetWithParent()
        {
            var ddbConnectionCount = _retrieverManager.GetEntities<DdbConnectionEntity>(StoreId, EntityQuery.Empty, Detail.IdOnly).Count();
            var datasetCount = _retrieverManager.GetEntities<DatasetEntity>(StoreId, EntityQuery.Empty, Detail.IdOnly).Count();
            var result = this._copyManager.Copy(this.StoreId, EntityType.DataSet, "1".QueryForThisEntityId(), true);
            Assert.That(result.Status, Is.EqualTo(StatusType.Success));
            var newDdbConnectionCount = _retrieverManager.GetEntities<DdbConnectionEntity>(StoreId, EntityQuery.Empty, Detail.IdOnly).Count();
            var newDatasetCount = _retrieverManager.GetEntities<DatasetEntity>(StoreId, EntityQuery.Empty, Detail.IdOnly).Count();
            Assert.That(newDdbConnectionCount, Is.EqualTo(ddbConnectionCount + 1));
            Assert.That(newDatasetCount, Is.EqualTo(datasetCount + 1));
        }

        [Test]
        public void CopyDatasetWithoutParent()
        {
            var ddbConnectionCount = _retrieverManager.GetEntities<DdbConnectionEntity>(StoreId, EntityQuery.Empty, Detail.IdOnly).Count();
            var datasetCount = _retrieverManager.GetEntities<DatasetEntity>(StoreId, EntityQuery.Empty, Detail.IdOnly).Count();
            var result = this._copyManager.Copy(this.StoreId, EntityType.DataSet, "1".QueryForThisEntityId(), false);
            Assert.That(result.Status, Is.EqualTo(StatusType.Success));
            var newDdbConnectionCount = _retrieverManager.GetEntities<DdbConnectionEntity>(StoreId, EntityQuery.Empty, Detail.IdOnly).Count();
            var newDatasetCount = _retrieverManager.GetEntities<DatasetEntity>(StoreId, EntityQuery.Empty, Detail.IdOnly).Count();
            Assert.That(newDdbConnectionCount, Is.EqualTo(ddbConnectionCount));
            Assert.That(newDatasetCount, Is.EqualTo(datasetCount + 1));
        }

        [Test]
        public void CopySimple([Values(EntityType.Registry, EntityType.DdbConnectionSettings)] EntityType entityType)
        {
            var result = this._copyManager.Copy(this.StoreId, entityType, EntityQuery.Empty, false);
            Assert.That(result.Status, Is.EqualTo(StatusType.Success));
            var sourceEntities = this._retrieverManager.GetRetrieverEngine(StoreId, entityType).GetEntities(EntityQuery.Empty, Detail.Full);
            var newSourceEntities = _retrieverManager.GetRetrieverEngine(StoreId, entityType).GetEntities(EntityQuery.Empty, Detail.Full);
            Assert.That(newSourceEntities.Count(), Is.EqualTo(sourceEntities .Count()));
        }

        [TestCase("msdb_scratch.oracle")]
        [TestCase("msdb_scratch.mariadb")]
        [TestCase("msdb_scratch.sqlserver")]
        public void CopyDataSetToAnotherMappingStore(string targetStoreId)
        {
            if (string.Equals(this.StoreId, targetStoreId))
            {
                Assert.Ignore("Skipping source equals target");
            }

            InitializeMappingStore(targetStoreId);
            var queryForThisEntityId = "1".QueryForThisEntityId();
            var result = this._copyManager.Copy(this.StoreId, targetStoreId, EntityType.DataSet, queryForThisEntityId);
            Assert.That(result.Status, Is.EqualTo(StatusType.Success));
            var entityQuery = EntityQuery.Empty;
            CompareEntities<IConnectionEntity>(targetStoreId, entityQuery);
            CompareEntities<DatasetEntity>(targetStoreId, queryForThisEntityId);
            CompareEntities<DataSetColumnEntity>(targetStoreId, entityQuery);
            CompareEntities<LocalCodeEntity>(targetStoreId, entityQuery);
        }

        [TestCase("msdb_scratch.oracle")]
        [TestCase("msdb_scratch.mariadb")]
        [TestCase("msdb_scratch.sqlserver")]
        public void CopyDataSetToAnotherMappingStoreX2(string targetStoreId)
        {
            if (string.Equals(this.StoreId, targetStoreId))
            {
                Assert.Ignore("Skipping source equals target");
            }

            InitializeMappingStore(targetStoreId);
            var queryForThisEntityId = "1".QueryForThisEntityId();
            var result = this._copyManager.Copy(this.StoreId, targetStoreId, EntityType.DataSet, queryForThisEntityId);
            Assert.That(result.Status, Is.EqualTo(StatusType.Success));
            CompareEntities<IConnectionEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<DatasetEntity>(targetStoreId, queryForThisEntityId);
            CompareEntities<DataSetColumnEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<LocalCodeEntity>(targetStoreId, EntityQuery.Empty);
            result = this._copyManager.Copy(this.StoreId, targetStoreId, EntityType.DataSet, queryForThisEntityId);
            Assert.That(result.Status, Is.EqualTo(StatusType.Success));
            var entities = _retrieverManager.GetEntities<DatasetEntity>(targetStoreId, EntityQuery.Empty, Detail.IdOnly);
            Assert.That(entities.Count(), Is.EqualTo(2));
        }

        /// <summary>
        /// The copy simple to another mapping store.
        /// </summary>
        /// <param name="targetStoreId">
        /// The target store id.
        /// </param>
        /// <param name="entityType">
        /// The entity type.
        /// </param>
        [Test]
        public void CopySimpleToAnotherMappingStore([Values("msdb_scratch.oracle", "msdb_scratch.mariadb", "msdb_scratch.sqlserver")]string targetStoreId, [Values(EntityType.Header, EntityType.Registry, EntityType.DdbConnectionSettings)] EntityType entityType)
        {
            if (string.Equals(this.StoreId, targetStoreId))
            {
                Assert.Ignore("Skipping source equals target");
            }

            InitializeMappingStore(targetStoreId);
            var result = this._copyManager.Copy(this.StoreId, targetStoreId, entityType, EntityQuery.Empty);
            Assert.That(result.Status, Is.EqualTo(StatusType.Success));
            var sourceEntities = _retrieverManager.GetRetrieverEngine(StoreId, entityType).GetEntities(EntityQuery.Empty, Detail.Full);
            var targetEntities = _retrieverManager.GetRetrieverEngine(targetStoreId, entityType).GetEntities(EntityQuery.Empty, Detail.Full);
            Assert.That(sourceEntities.Count(), Is.EqualTo(targetEntities.Count()));
       }

        [TestCase("msdb_scratch.oracle")]
        [TestCase("msdb_scratch.mariadb")]
        [TestCase("msdb_scratch.sqlserver")]
        public void CopyMappingSetToAnotherMappingStore(string targetStoreId)
        {
            if (string.Equals(this.StoreId, targetStoreId))
            {
                Assert.Ignore("Skipping source equals target");
            }

            InitializeMappingStore(targetStoreId);
            var result = this._copyManager.Copy(this.StoreId, targetStoreId, EntityType.MappingSet, EntityQuery.Empty);
            Assert.That(result.Status, Is.EqualTo(StatusType.Success), string.Join('\n', result.Messages));
            CompareEntities<IConnectionEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<DatasetEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<DataSetColumnEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<LocalCodeEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<MappingSetEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<ComponentMappingEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<TranscodingEntity>(targetStoreId, EntityQuery.Empty);
            CompareEntities<TranscodingRuleEntity>(targetStoreId, EntityQuery.Empty);
            var targetSettings = GetConnectionStringSettings(targetStoreId);
            Assert.That(targetSettings, Is.Not.Null);
            var t = new MappingStoreSdmxObjectRetrievalManager(targetSettings);
            var dataflowObjects = t.GetDataflowObjects(new MaintainableRefObjectImpl(), false, true);
            var targetDataflows = _retrievalManager.GetMaintainableObjects<IDataflowObject>();
            Assert.That(dataflowObjects, Is.Not.Empty);
            Assert.That(dataflowObjects.Count, Is.EqualTo(targetDataflows.Count));
        }

        [TestCase("msdb_scratch.oracle")]
        [TestCase("msdb_scratch.mariadb")]
        [TestCase("msdb_scratch.sqlserver")]
        public void CopyMappingSetToAnotherMappingStoreX2(string targetStoreId)
        {
            if (string.Equals(this.StoreId, targetStoreId))
            {
                Assert.Ignore("Skipping source equals target");
            }

            InitializeMappingStore(targetStoreId);
            var result = this._copyManager.Copy(this.StoreId, targetStoreId, EntityType.MappingSet, EntityQuery.Empty);
            Assert.That(result.Status, Is.EqualTo(StatusType.Success));
            result = this._copyManager.Copy(this.StoreId, targetStoreId, EntityType.MappingSet, EntityQuery.Empty);
            Assert.That(result.Status, Is.EqualTo(StatusType.Warning));
        }



        private void CompareEntities<TEntity>(string targetStoreId, IEntityQuery entityQuery) where TEntity : class, IEntity
        {
            var sourceEntities = this._retrieverManager.GetEntities<TEntity>(this.StoreId, entityQuery, Detail.Full).ToArray();
            var expectecCount = sourceEntities.Length;
            var targetEntities = this._retrieverManager.GetEntities<TEntity>(targetStoreId, EntityQuery.Empty, Detail.Full).ToArray();
            var actualCount = targetEntities.Length;
            Assert.That(actualCount, Is.EqualTo(expectecCount));
            // TODO entity specific checks
        }


        private void ReBuildMappingStore()
        {
            try
            {
                InitializeMappingStore(this.StoreId);
                var dataflow = BuildStructuralMetadata();
                var connection = AddADdbConnection();
                var dataset = AddDataSet(connection, dataflow.DataStructureRef, 0);
                AddSourceMappingSet(dataflow, dataset.EntityId);
            }
            catch (Exception e)
            {
                _log.Error(e);
                Console.WriteLine(e);
                throw;
            }
        }


        private MappingSetEntity AddSourceMappingSet(IDataflowObject dataflowObject, string datasetEntityId)
        {
            MappingSetEntity mappingSetEntity = new MappingSetEntity();
            mappingSetEntity.StoreId = StoreId;
            mappingSetEntity.DataSetId = datasetEntityId;
            mappingSetEntity.Name = "Test Mapping Set for " + dataflowObject.Id;
            mappingSetEntity.ParentId = dataflowObject.Urn.ToString();
            mappingSetEntity.Description = "a description";

            var addedMappingSet = _persistManager.Add(mappingSetEntity);

            var datasetColumns =
                _retrieverManager.GetEntities<DataSetColumnEntity>(
                    StoreId,
                    datasetEntityId.QueryForThisParentId(),
                    Detail.Full).ToArray();

            var dsdAndDepds = _retrievalManager.GetSdmxObjects(
                dataflowObject.DataStructureRef,
                ResolveCrossReferences.ResolveExcludeAgencies);

            var dsd = dsdAndDepds.DataStructures.First();

            // this test needs a time dimension
            Assert.That(dsd.TimeDimension, Is.Not.Null);

            var component = dsd.TimeDimension;
            ComponentMappingEntity componentMapping = new ComponentMappingEntity();
            componentMapping.StoreId = StoreId;
            componentMapping.ParentId = addedMappingSet.EntityId;
            componentMapping.Type = ComponentMappingType.Normal.AsMappingStoreType();

            var dataSetColumnEntities =
                datasetColumns.Where(entity => entity.Name.Contains("_" + component.Id)).ToArray();
            componentMapping.SetColumns(dataSetColumnEntities);

            componentMapping.Component = new Component { ObjectId = component.Id };

            var addedComponentEntity = _persistManager.Add(componentMapping);

            return mappingSetEntity;
        }

        private void SetupMaintainable(IMaintainableMutableObject maintainable, string id)
        {
            maintainable.Version = "1.0";
            maintainable.AgencyId = "TEST";
            maintainable.Id = id;
            maintainable.AddName("en", $"Test {id}");
            maintainable.FinalStructure = TertiaryBool.ParseBoolean(true);
        }

        private void AddConcept(IConceptSchemeMutableObject cs, string id)
        {
            var c = new ConceptMutableCore() { Id = id };
            c.AddName("en", $"Name of $id");
            cs.AddItem(c);
        }


        private IDataflowObject BuildStructuralMetadata()
        {
            IMutableObjects mutableObjects = new MutableObjectsImpl();
            IConceptSchemeMutableObject conceptScheme = new ConceptSchemeMutableCore();
            SetupMaintainable(conceptScheme, "TEST_CS");

            AddConcept(conceptScheme, "FREQ");
            AddConcept(conceptScheme, DimensionObject.TimeDimensionFixedId);
            AddConcept(conceptScheme, PrimaryMeasure.FixedId);

            mutableObjects.AddConceptScheme(conceptScheme);

            ICodelistMutableObject cl = new CodelistMutableCore();
            SetupMaintainable(cl, "CL_FREQ");

            var c1 = new CodeMutableCore() { Id = "A" };
            c1.AddName("en", "Annual");
            cl.AddItem(c1);
            var c2 = new CodeMutableCore() { Id = "M" };
            c2.AddName("en", "Monthly");
            cl.AddItem(c2);

            mutableObjects.AddCodelist(cl);

            IDataStructureMutableObject dsd = new DataStructureMutableCore();
            SetupMaintainable(dsd, "TEST_DSD");
            dsd.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "FREQ"),
                new StructureReferenceImpl("TEST", "CL_FREQ", "1.0", SdmxStructureEnumType.CodeList));

            IDimensionMutableObject timeDimension = new DimensionMutableCore();
            timeDimension.ConceptRef = new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, DimensionObject.TimeDimensionFixedId);
            timeDimension.TimeDimension = true;
            dsd.AddDimension(timeDimension);

            dsd.AddPrimaryMeasure(
                new StructureReferenceImpl(
                    "TEST",
                    "TEST_CS",
                    "1.0",
                    SdmxStructureEnumType.Concept,
                    PrimaryMeasure.FixedId));
            mutableObjects.AddDataStructure(dsd);

            var dataflow = new DataflowMutableCore(dsd.ImmutableInstance);
            mutableObjects.AddDataflow(dataflow);
            var artefactImportStatuses = new List<ArtefactImportStatus>();
            var structurePersistenceManager =
                new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(
                    _connectionStringSettings,
                    artefactImportStatuses);

            structurePersistenceManager.SaveStructures(mutableObjects.ImmutableObjects);
            Assert.That(
                artefactImportStatuses.All(status => status.ImportMessage.Status != ImportMessageStatus.Error),
                string.Join("\n", artefactImportStatuses.Select(status => status.ImportMessage.Message)));

            return dataflow.ImmutableInstance;
        }
    }
}