// -----------------------------------------------------------------------
// <copyright file="DataSetDataRetrievalManagerTest.cs" company="EUROSTAT">
//   Date Created : 2017-05-08
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Manager;

    using NUnit.Framework;

    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class DdbDataRetrievalManagerTest : BaseTestClassWithContainer
    {
        private IDataSetDataRetrievalManager _dataSetDataRetrievalManager;
        private IEntityRetrieverEngine<DdbConnectionEntity> _ddbConnectionSettingsRetriever;

        private IEntityRetrieverEngine<DataSetColumnEntity> _dataSetColumnRetriever;

        private ILocalCodeRetrieverManager _localRetrievalManager;

        private IDdbManager _ddbManager;



      
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public DdbDataRetrievalManagerTest(string storeId) : base(storeId) 
        {
           this._dataSetDataRetrievalManager = this.IoCContainer.Resolve<IDataSetDataRetrievalManager>();
           var entityRetrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            this._ddbConnectionSettingsRetriever = entityRetrieverManager.GetRetrieverEngine<DdbConnectionEntity>(storeId);
            this._dataSetColumnRetriever = entityRetrieverManager.GetRetrieverEngine<DataSetColumnEntity>(storeId);
            this._localRetrievalManager = this.IoCContainer.Resolve<ILocalCodeRetrieverManager>();
            this._ddbManager = this.IoCContainer.Resolve<IDdbManager>();

        }

        [Test]
        public void ShouldGetAvailableLocalData()
        {
            var entities = this._ddbConnectionSettingsRetriever.GetEntities(new EntityQuery(), Detail.Full);
            foreach (var ddbConnectionEntity in entities.Where(d => string.Equals("sqlserver", d.DbType, StringComparison.OrdinalIgnoreCase)))
            {
                var databaseObjects = this._ddbManager.GetDatabaseObjects(ddbConnectionEntity.StoreId, ddbConnectionEntity.EntityId).FirstOrDefault();
                if (databaseObjects == null)
                {
                    continue;
                }

                QueryResult localCodeResults;
                try
                {
                    localCodeResults = this._localRetrievalManager.GetLocalData(new LocalDataQuery() {DisseminationDatabaseSettingEntityId = ddbConnectionEntity.EntityId, Count = 10, MappingStoreId = ddbConnectionEntity.StoreId, Table = databaseObjects.Name});
               }
                catch (Exception e)
                {
                    Trace.WriteLine(e);
                    Trace.WriteLine(ddbConnectionEntity.Name);
                    throw;
                }

                Assert.That(localCodeResults, Is.Not.Null, ddbConnectionEntity.Name);
                Assert.That(localCodeResults.Rows, Is.Not.Empty, ddbConnectionEntity.Name);
                var uneven = localCodeResults.Rows.FirstOrDefault(list => list.Length != localCodeResults.Columns.Count)?.Length;
                if (uneven.HasValue)
                {
                    Trace.WriteLine(uneven);
                }

                Assert.That(localCodeResults.Rows.All(list => list.Length == localCodeResults.Columns.Count), $"{ddbConnectionEntity.Name} {localCodeResults.Columns.Count} != {uneven}");
                Assert.That(localCodeResults.TotalRows, Is.GreaterThanOrEqualTo(localCodeResults.Count), $"{ddbConnectionEntity.Name} {localCodeResults.TotalRows} >= {localCodeResults.Columns.Count}");
                Assert.AreEqual(localCodeResults.Rows.Count(), localCodeResults.Rows.Distinct().Count());
            }
        }
    }
}