// -----------------------------------------------------------------------
// <copyright file="DatasetPropertyPersistTest.cs" company="EUROSTAT">
//   Date Created : 2021-11-11
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Data;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Helper;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Tests
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Estat.Sri.MappingStore.Store.Extension;

    /// <summary>
    /// The dataset property persist test.
    /// </summary>
    [TestFixture("msdb_scratch.oracle")]
    [TestFixture("msdb_scratch.mariadb")]
    [TestFixture("msdb_scratch.sqlserver")]
    public class DatasetPropertyPersistTest : BaseTestClassWithContainer
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DatasetPropertyPersistTest));

        /// <summary>
        /// The connection string settings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The retriever manager
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;

        /// <summary>
        /// The persist manager
        /// </summary>
        private readonly IEntityPersistenceManager _persistManager;

        /// <summary>
        /// The retrieval manager
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _retrievalManager;

        private readonly IConnectionEntity _dataDbConnection;
        private readonly IDataflowObject _dataflow;
        private readonly Database _database;
        private readonly DatasetPropertyHelper _datasetPropertyHelper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatasetPropertyPersistTest"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public DatasetPropertyPersistTest(string connectionName) : base(connectionName)
        {
            _retrieverManager = this.IoCContainer.Resolve<IEntityRetrieverManager>();
            _persistManager = this.IoCContainer.Resolve<IEntityPersistenceManager>();
            _connectionStringSettings = GetConnectionStringSettings();
            _retrievalManager = new MappingStoreSdmxObjectRetrievalManager(_connectionStringSettings);
            _database = new Database(_connectionStringSettings);
            _datasetPropertyHelper = new DatasetPropertyHelper(_database);

            try
            {
                InitializeMappingStore(this.StoreId);

                _dataflow = BuildStructuralMetadata();
                _dataDbConnection = AddADdbConnection();
            }
            catch (Exception e)
            {
                _log.Error(e);
                Console.WriteLine(e);
                throw;
            }
        }


        [Test, Order(1)]
        public void ShouldInsertDataSetWithDatasetProperty()
        {
            var datasetName = "TEST DATASET 11";

            // set up entity to insert
            var datasetEntity = new DatasetEntity();
            datasetEntity.Name = datasetName;
            datasetEntity.Description = "A Description";
            datasetEntity.ParentId = _dataDbConnection.EntityId;
            datasetEntity.StoreId = _dataDbConnection.StoreId;
            datasetEntity.EditorType = "Test";
            datasetEntity.Query = "select * from H2TEST";
            datasetEntity.OrderByClause = "ORDER BY [TEST_COLUMN]";
            datasetEntity.DatasetPropertyEntity = new DatasetPropertyEntity()
            {
                CrossApplyColumn = "SID",
                CrossApplySupported = true,
                OrderByTimePeriodAsc = "SID ASC, PERIOD_START ASC, PERIOD_END DESC",
                OrderByTimePeriodDesc = "SID DESC, PERIOD_START DESC, PERIOD_END ASC"
            };

            // insert dataset
            var writtenEntity = this._persistManager.Add(datasetEntity);

            // read dataset property and assert results
            var readDatasetProperties = this.RetrieveDatasetProperties(writtenEntity.EntityId);

            Assert.IsNotNull(readDatasetProperties);
            Assert.That(readDatasetProperties.Count > 0);
            Assert.That(readDatasetProperties[DatasetPropertyType.OrderByTimePeriodAsc].PropertyValue,
                Is.EqualTo(datasetEntity.DatasetPropertyEntity.OrderByTimePeriodAsc));
            Assert.That(readDatasetProperties[DatasetPropertyType.OrderByTimePeriodDesc].PropertyValue,
                Is.EqualTo(datasetEntity.DatasetPropertyEntity.OrderByTimePeriodDesc));
            Assert.That(readDatasetProperties[DatasetPropertyType.CrossApplySupported].PropertyValue,
                Is.EqualTo(datasetEntity.DatasetPropertyEntity.CrossApplySupported.ToString()));
            Assert.That(readDatasetProperties[DatasetPropertyType.CrossApplyColumn].PropertyValue,
                Is.EqualTo(datasetEntity.DatasetPropertyEntity.CrossApplyColumn));
        }

        [Test, Order(2)]
        public void ShouldRetrieveDataSetWithDataSetProperty()
        {
            var datasetName = "TEST DATASET 11";

            var existingEntityId = this.RetrieveDataSetEntityId(datasetName);

            var datasetRetriever = _retrieverManager.GetRetrieverEngine<DatasetEntity>(StoreId);
            var readDatasetEntity = datasetRetriever.GetEntities(
                     new EntityQuery { EntityId = new Criteria<string>(OperatorType.Exact, existingEntityId) },
                     Detail.Full).SingleOrDefault();
            var readDatasetProperty = readDatasetEntity.DatasetPropertyEntity;

            // assert results
            Assert.That(readDatasetProperty.EntityId, Is.EqualTo(readDatasetEntity.EntityId));
            Assert.That(readDatasetProperty.OrderByTimePeriodAsc, Is.EqualTo("SID ASC, PERIOD_START ASC, PERIOD_END DESC"));
            Assert.That(readDatasetProperty.OrderByTimePeriodDesc, Is.EqualTo("SID DESC, PERIOD_START DESC, PERIOD_END ASC"));
            Assert.That(readDatasetProperty.CrossApplySupported, Is.EqualTo(true));
            Assert.That(readDatasetProperty.CrossApplyColumn, Is.EqualTo("SID"));
        }

        [Test, Order(3)]
        public void ShouldDeleteDataSetAlongDatasetProperty()
        {
            var datasetName = "TEST DATASET 11";

            var existingEntityId = this.RetrieveDataSetEntityId(datasetName);

            // delete entity
            this._persistManager.Delete<DatasetEntity>(StoreId, existingEntityId);

            // try to retrieve deleted entities & assert results
            var datasetEntityRetriever = _retrieverManager.GetRetrieverEngine<DatasetEntity>(StoreId);
            var readEntity = datasetEntityRetriever.GetEntities(
                new EntityQuery { EntityId = new Criteria<string>(OperatorType.Exact, existingEntityId) },
                Detail.Full).FirstOrDefault();

            var readDatasetProperties = this.RetrieveDatasetProperties(existingEntityId);

            Assert.IsNull(readEntity);
            Assert.IsFalse(readDatasetProperties.Any());
        }

        private Dictionary<DatasetPropertyType?, MADatasetPropertyEntity> RetrieveDatasetProperties(string datasetId)
        {
            WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder(_database);
            var criteria = new Criteria<long>(OperatorType.Exact, datasetId.AsMappingStoreEntityId());
            WhereClauseParameters whereClauseParameters = whereClauseBuilder.Build(criteria, "DATASET_ID");
            var sqlQuery = $"select DATASET_ID as DatasetId, PROPERTY_TYPE_ENUM as PropertyTypeId, PROPERTY_VALUE as PropertyValue from DATASET_PROPERTY where DATASET_ID = {datasetId}";

            return _database.Query<MADatasetPropertyEntity>(sqlQuery).ToDictionary( kvp => _datasetPropertyHelper.GetDatasetPropertyTypeFromID(kvp.PropertyTypeId));
        }

        private string RetrieveDataSetEntityId(string datasetName)
        {
            var nameParam = _database.CreateInParameter("ID", DbType.AnsiString, datasetName);
            var mapSetId = _database.ExecuteScalarFormat("SELECT DS_ID FROM DATASET WHERE NAME = {0}", nameParam);
            return mapSetId.ToString();
        }

        private static DatasetEntity GetDataset(IEntityRetrieverEngine<DatasetEntity> engine, string datasetId)
        {
            var datasetEntity = engine.GetEntities(new EntityQuery(), Detail.Full).FirstOrDefault(e => e.EntityId == datasetId);
            return datasetEntity;
        }

        private static DatasetPropertyEntity GetDatasetProperty(IEntityRetrieverEngine<DatasetPropertyEntity> engine,
            string datasetId)
        {
            var query = new EntityQuery() {EntityId = new Criteria<string>(OperatorType.Exact, datasetId)};
            
            var datasetPropertyEntityEntity = engine.GetEntities(query, Detail.Full).FirstOrDefault();
            
            return datasetPropertyEntityEntity;
        }


        private IDataflowObject BuildStructuralMetadata()
        {
            IMutableObjects mutableObjects = new MutableObjectsImpl();
            IConceptSchemeMutableObject conceptScheme = new ConceptSchemeMutableCore();
            SetupMaintainable(conceptScheme, "TEST_CS");

            AddConcept(conceptScheme, "FREQ");
            AddConcept(conceptScheme, DimensionObject.TimeDimensionFixedId);
            AddConcept(conceptScheme, PrimaryMeasure.FixedId);

            mutableObjects.AddConceptScheme(conceptScheme);

            ICodelistMutableObject cl = new CodelistMutableCore();
            SetupMaintainable(cl, "CL_FREQ");

            var c1 = new CodeMutableCore() { Id = "A" };
            c1.AddName("en", "Annual");
            cl.AddItem(c1);
            var c2 = new CodeMutableCore() { Id = "M" };
            c2.AddName("en", "Monthly");
            cl.AddItem(c2);

            mutableObjects.AddCodelist(cl);

            IDataStructureMutableObject dsd = new DataStructureMutableCore();
            SetupMaintainable(dsd, "TEST_DSD");
            dsd.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "FREQ"),
                new StructureReferenceImpl("TEST", "CL_FREQ", "1.0", SdmxStructureEnumType.CodeList));
            
            IDimensionMutableObject timeDimension = new DimensionMutableCore();
            timeDimension.ConceptRef = new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, DimensionObject.TimeDimensionFixedId);
            timeDimension.TimeDimension = true;
            dsd.AddDimension(timeDimension);

            dsd.AddPrimaryMeasure(
                new StructureReferenceImpl(
                    "TEST",
                    "TEST_CS",
                    "1.0",
                    SdmxStructureEnumType.Concept,
                    PrimaryMeasure.FixedId));
            mutableObjects.AddDataStructure(dsd);

            var dataflow = new DataflowMutableCore(dsd.ImmutableInstance);
            mutableObjects.AddDataflow(dataflow);
            var artefactImportStatuses = new List<ArtefactImportStatus>();
            var structurePersistenceManager =
                new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(
                    _connectionStringSettings,
                    artefactImportStatuses);

            structurePersistenceManager.SaveStructures(mutableObjects.ImmutableObjects);
            Assert.That(
                artefactImportStatuses.All(status => status.ImportMessage.Status != ImportMessageStatus.Error),
                string.Join("\n", artefactImportStatuses.Select(status => status.ImportMessage.Message)));

            return dataflow.ImmutableInstance;
        }

        private void SetupMaintainable(IMaintainableMutableObject maintainable, string id)
        {
            maintainable.Version = "1.0";
            maintainable.AgencyId = "TEST";
            maintainable.Id = id;
            maintainable.AddName("en", $"Test {id}");
            maintainable.FinalStructure = TertiaryBool.ParseBoolean(true);
        }

        private void AddConcept(IConceptSchemeMutableObject cs, string id)
        {
            var c = new ConceptMutableCore() {Id = id};
            c.AddName("en", $"Name of $id");
            cs.AddItem(c);
        }

        //private MappingSetEntity AddSourceMappingSet(IDataflowObject dataflowObject, string datasetEntityId)
        //{
        //    MappingSetEntity mappingSetEntity = new MappingSetEntity();
        //    mappingSetEntity.StoreId = StoreId;
        //    mappingSetEntity.DataSetId = datasetEntityId;
        //    mappingSetEntity.Name = "Test Mapping Set for " + dataflowObject.Id;
        //    mappingSetEntity.ParentId = dataflowObject.Urn.ToString();
        //    mappingSetEntity.Description = "a description";

        //    var addedMappingSet = _persistManager.Add(mappingSetEntity);

        //    addedMappingSet.TimePreFormattedEntity = new TimePreFormattedEntity()
        //    {
        //        EntityId = addedMappingSet.EntityId,
        //        OutputColumn = new DataSetColumnEntity() { EntityId = "H2TEST_COLUMN_FREQ" },
        //        FromColumn = new DataSetColumnEntity() { EntityId = "H2TEST_COLUMN_TIME_PERIOD" },
        //        ToColumn = new DataSetColumnEntity() { EntityId = "H2TEST_COLUMN_TIME_PERIOD2" }
        //    };
           
        //    _persistManager.Add(addedMappingSet);

        //    var datasetColumns =
        //        _retrieverManager.GetEntities<DataSetColumnEntity>(
        //            StoreId,
        //            datasetEntityId.QueryForThisParentId(),
        //            Detail.Full).ToArray();

        //    var dsdAndDepds = _retrievalManager.GetSdmxObjects(
        //        dataflowObject.DataStructureRef,
        //        ResolveCrossReferences.ResolveExcludeAgencies);

        //    var dsd = dsdAndDepds.DataStructures.First();

        //    // this test needs a time dimension
        //    Assert.That(dsd.TimeDimension, Is.Not.Null);

        //    var component = dsd.TimeDimension;
        //    ComponentMappingEntity componentMapping = new ComponentMappingEntity();
        //    componentMapping.StoreId = StoreId;
        //    componentMapping.ParentId = addedMappingSet.EntityId;
        //    componentMapping.Type = ComponentMappingType.Normal.AsMappingStoreType();

        //    var dataSetColumnEntities =
        //        datasetColumns.Where(entity => entity.Name.Contains("_" + component.Id)).ToArray();
        //    componentMapping.SetColumns(dataSetColumnEntities);

        //    componentMapping.Component = new Component { ObjectId = component.Id };

        //    var addedComponentEntity = _persistManager.Add(componentMapping);

        //    return mappingSetEntity;
        //}

    }
}