using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Utils.Builder;
using NUnit.Framework;

namespace Estat.Sri.Utils.Tests
{
    class TestSqlWhereBuilder
    {
        [Test]
        public void Test()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["sqlserver"];
            var database = new Database(connectionString);
            SqlWhereBuilder sqlWhereBuilder = new SqlWhereBuilder(database);
            sqlWhereBuilder.Add("A.ID = {0}", "id1", System.Data.DbType.AnsiString, "STS");
            sqlWhereBuilder.Add("A.AGENCY = {0}", "agency1", System.Data.DbType.AnsiString, "ESTAT");
            sqlWhereBuilder.Add("A.VERSION1 = {0}", "version1_1", System.Data.DbType.Int64, 1);
            sqlWhereBuilder.Add("A.VERSION2 = {0}", "version2_1", System.Data.DbType.Int64, 1);
            sqlWhereBuilder.Merge(SqlWhereBuilder.And);
            sqlWhereBuilder.Add("A.ID = {0}", "id2", System.Data.DbType.AnsiString, "STS2");
            sqlWhereBuilder.Add("A.AGENCY = {0}", "agency2", System.Data.DbType.AnsiString, "ESTAT");
            sqlWhereBuilder.Merge(SqlWhereBuilder.And);
            var query = sqlWhereBuilder.Build(SqlWhereBuilder.Or);
            Assert.That(query, Is.EqualTo("((A.ID = @id1 AND A.AGENCY = @agency1 AND A.VERSION1 = @version1_1 AND A.VERSION2 = @version2_1) OR (A.ID = @id2 AND A.AGENCY = @agency2))"));

        }
    }
}
