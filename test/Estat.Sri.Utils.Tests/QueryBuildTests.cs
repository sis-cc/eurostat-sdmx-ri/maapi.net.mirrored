using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using Estat.Sri.MappingStoreRetrieval.Constants;
using Estat.Sri.MappingStoreRetrieval.Manager;
using NUnit.Framework;

namespace Estat.Sri.Utils.Tests
{
    [TestFixture]
    class QueryBuildTests
    {
        [Test]
        public void TestConstraintQuery()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["sqlserver"];
            var database = new Database(connectionString);
            var inParameter = database.CreateInParameter(ParameterNameConstants.IdParameter, DbType.Int64, "1");
            database.GetSqlStringCommandFormat(ContentConstraintConstant.SqlConsInfo,inParameter);
        }
        
    }
}
