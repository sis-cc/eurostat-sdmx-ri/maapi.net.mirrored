// -----------------------------------------------------------------------
// <copyright file="SecurityManagerTests.cs" company="EUROSTAT">
//   Date Created : 2018-3-12
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using NUnit.Framework;

namespace Estat.Sri.Utils.Tests
{
    using System.Configuration;
    using System.Linq;
    using DryIoc;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Security;
    using Estat.Sri.Security.Engine;
    using Estat.Sri.Security.Exceptions;
    using Estat.Sri.Security.Factory;
    using Estat.Sri.Security.Manager;
    using Estat.Sri.Security.Model;

    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    public class SecurityManagerTests:TestWithContainer
    {
        private readonly ISecurityPersistenceEngine _persistenceEngine;
        private readonly ISecurityRetrieverEngine _retrieveEngine;
        private readonly ISecuritySchemaEngine _schemaEngine;
        private readonly Database _authDb;
        private readonly PrincipalRetriever _principalRetriever;

        public SecurityManagerTests(string name):base(name)
        {
            var securityManager = new SecurityManager(_container.Resolve<ISecurityStoreFactory>());
            var connectionString = ConfigurationManager.ConnectionStrings[name];
            _authDb = new Database(connectionString);
            var store = new AuthDbSecurityStore(_authDb);
            _schemaEngine = securityManager.GetSchemaEngine(store);
            _retrieveEngine = securityManager.GetRetrieverEngine(store);
            _persistenceEngine = securityManager.GetPersistenceEngine(store);
            _principalRetriever = new PrincipalRetriever();
        }

        [SetUp]
        public void InitDb()
        {
            _schemaEngine.DeleteStore();
            _schemaEngine.Initialize();
        }

        [Test]
        public void ShouldAddMappingStore()
        {
            var toadd = new MappingStoreInfo() {Name = "test_server"};
            _persistenceEngine.Add(toadd);
            Assert.That(_retrieveEngine.Exists(toadd));
            Assert.That(!_retrieveEngine.IsUsed(toadd));
            var fromBd = _retrieveEngine.GetMappingStores();
            Assert.That(fromBd.Any(x => x.Name.Equals(toadd.Name)));
        }

        [Test]
        public void ShouldNotAddMappingStoreTwoTimes()
        {
            var toadd = new MappingStoreInfo() {Name = "test_server"};
            _persistenceEngine.Add(toadd);
            Assert.That(_retrieveEngine.Exists(toadd));
            Assert.That(!_retrieveEngine.IsUsed(toadd));
            var fromBd = _retrieveEngine.GetMappingStores();
            Assert.That(fromBd.Any(x => x.Name.Equals(toadd.Name)));
            Assert.That(() => _persistenceEngine.Add(toadd), Throws.TypeOf<ResourceAlreadyExistsException>());
        }

        [Test]
        public void ShouldAddAndDeleteMappingStore()
        {
            var toadd = new MappingStoreInfo() {Name = "test_server"};
            _persistenceEngine.Add(toadd);
            Assert.That(_retrieveEngine.Exists(toadd));
            _persistenceEngine.Delete(toadd);
            Assert.That(!_retrieveEngine.Exists(toadd));
        }

        [Test]
        public void ShouldAddAccessRule()
        {
            var toadd = new AccessRule() {Name = "CanDoMagic"};
            _persistenceEngine.Add(toadd);
            Assert.That(_retrieveEngine.Exists(toadd));
            Assert.That(!_retrieveEngine.IsUsed(toadd));
            var fromBd = _retrieveEngine.GetAccessRules();
            Assert.That(fromBd.Any(x => x.Name.Equals(toadd.Name)));
        }

        [Test]
        public void ShouldNotAddAccessRuleTwoTimes()
        {
            var toadd = new AccessRule() {Name = "CanDoMagic"};
            _persistenceEngine.Add(toadd);
            Assert.That(_retrieveEngine.Exists(toadd));
            Assert.That(!_retrieveEngine.IsUsed(toadd));
            var fromBd = _retrieveEngine.GetAccessRules();
            Assert.That(fromBd.Any(x => x.Name.Equals(toadd.Name)));
            Assert.That(() => _persistenceEngine.Add(toadd), Throws.TypeOf<ResourceAlreadyExistsException>());
        }

        [Test]
        public void ShouldAddAndDeleteAccessRule()
        {
            var toadd = new AccessRule() {Name = "CanDoMagic"};
            _persistenceEngine.Add(toadd);
            Assert.That(_retrieveEngine.Exists(toadd));
            _persistenceEngine.Delete(toadd);
            Assert.That(!_retrieveEngine.Exists(toadd));
        }

        [Test]
        public void ShouldUpdateAndDeleteAccessRule()
        {
            // Note combining several tests for perfomance reasons
            var toadd = new AccessRule() {Name = "CanDoMagic"};
            _persistenceEngine.Add(toadd);
            Assert.That(_retrieveEngine.Exists(toadd));

            var impliedRule1 = new AccessRule() { Name = "Implied1" };
            var impliedRule2 = new AccessRule() { Name = "Implied2" };
            
           Assert.That(() => _persistenceEngine.Update(toadd, new []{impliedRule1, impliedRule2}), Throws.TypeOf<SecurityResourceNotFoundException>());

            _persistenceEngine.Add(impliedRule1);
            _persistenceEngine.Add(impliedRule2);
            _persistenceEngine.Update(toadd, new[] { impliedRule1, impliedRule2 });

            var storedImplied = _retrieveEngine.GetFlatImpliedAccessRules(toadd);

            Assert.That(storedImplied.Count, Is.EqualTo(3));
            Assert.That(storedImplied.Any(x => x.Name.Equals(impliedRule1.Name)));
            Assert.That(storedImplied.Any(x => x.Name.Equals(impliedRule2.Name)));

            _persistenceEngine.Update(toadd, new[] { impliedRule1 });

            storedImplied = _retrieveEngine.GetFlatImpliedAccessRules(toadd);

            Assert.That(storedImplied.Count, Is.EqualTo(2));
            Assert.That(storedImplied.Any(x => x.Name.Equals(impliedRule1.Name)));
            Assert.That(!storedImplied.Any(x => x.Name.Equals(impliedRule2.Name)));

            _persistenceEngine.Delete(impliedRule1);

            _persistenceEngine.Delete(toadd);
            Assert.That(!_retrieveEngine.Exists(toadd));
        }

        [Test]
        public void ShouldNotAddUserWithBogusAccessRule()
        {
            var toadd1 = new MappingStoreInfo() {Name = "test_server"};
            _persistenceEngine.Add(toadd1);
            Assert.That(_retrieveEngine.Exists(toadd1));
            Assert.That(!_retrieveEngine.IsUsed(toadd1));

            var toadd = new User() {Name = "test_user"};
            toadd.AccessRules.Add(
                new AccessRule()
                {
                    Name = "DOES_NT_EXIST"
                });
            toadd.DefaultMappingStore = toadd1;
            toadd.MappingStores.Add(toadd1);
            Assert.Throws<SecurityResourceNotFoundException>(() => _persistenceEngine.Add(toadd, "123"));
        }

        [Test]
        public void ShouldNotAddUserWithBogusMappingStore()
        {
            var toadd1 = new MappingStoreInfo() {Name = "test_server"};

            var toadd = new User() {Name = "test_user"};
            toadd.AccessRules.Add(
                new AccessRule()
                {
                    Name = "WsUserRole"
                });
            toadd.DefaultMappingStore = toadd1;
            toadd.MappingStores.Add(toadd1);
            Assert.Throws<SecurityResourceNotFoundException>(() => _persistenceEngine.Add(toadd, "123"));
        }

        [Test]
        public void ShouldAddUserWithMappingStoreAndDelete()
        {
            var toadd1 = new MappingStoreInfo() {Name = "test_server"};
            _persistenceEngine.Add(toadd1);
            Assert.That(_retrieveEngine.Exists(toadd1));
            Assert.That(!_retrieveEngine.IsUsed(toadd1));

            var toadd = new User() {Name = "test_user"};
            toadd.AccessRules.Add(
                new AccessRule()
                {
                    Name = "WsUserRole"
                });
            toadd.DefaultMappingStore = toadd1;
            toadd.MappingStores.Add(toadd1);
            _persistenceEngine.Add(toadd, "123");

            var fromDb = _retrieveEngine.GetUser(toadd.Name);
            Assert.That(fromDb, Is.Not.Null);
            Assert.That(fromDb.Name, Is.EqualTo(toadd.Name));
            Assert.That(fromDb.DefaultMappingStore?.Name, Is.EqualTo(toadd.DefaultMappingStore.Name));
            Assert.That(fromDb.AccessRules.Count, Is.EqualTo(toadd.AccessRules.Count));
            foreach (var toaddAccessRule in toadd.AccessRules)
            {
                Assert.That(fromDb.AccessRules.Any(x => x.Name.Equals(toaddAccessRule.Name)));
                Assert.That(_retrieveEngine.IsUsed(toaddAccessRule));
            }

            Assert.That(fromDb.MappingStores.Count, Is.EqualTo(toadd.MappingStores.Count));
            foreach (var mappingStoreInfo in toadd.MappingStores)
            {
                Assert.That(fromDb.MappingStores.Any(x => x.Name.Equals(mappingStoreInfo.Name)));
                Assert.That(_retrieveEngine.IsUsed(mappingStoreInfo));
            }

            _persistenceEngine.Delete(toadd);
            foreach (var mappingStoreInfo in toadd.MappingStores)
            {
                Assert.That(!_retrieveEngine.IsUsed(mappingStoreInfo));
            }
        }

        [Test]
        public void ShouldUpdateUserWithMappingStoreAndDelete()
        {
            var toadd1 = new MappingStoreInfo() {Name = "test_server"};
            _persistenceEngine.Add(toadd1);
            Assert.That(_retrieveEngine.Exists(toadd1));
            Assert.That(!_retrieveEngine.IsUsed(toadd1));

            var toadd = new User() {Name = "test_user"};
            toadd.AccessRules.Add(
                new AccessRule()
                {
                    Name = "WsUserRole"
                });
            _persistenceEngine.Add(toadd, "123");

            var fromDb = _retrieveEngine.GetUser(toadd.Name);
            Assert.That(fromDb, Is.Not.Null);
            Assert.That(fromDb.Name, Is.EqualTo(toadd.Name));
            Assert.That(fromDb.DefaultMappingStore, Is.Null);
            Assert.That(fromDb.MappingStores, Is.Empty);

            toadd.DefaultMappingStore = toadd1;
            toadd.MappingStores.Add(toadd1);
            _persistenceEngine.Update(toadd);

            fromDb = _retrieveEngine.GetUser(toadd.Name);
            Assert.That(fromDb, Is.Not.Null);
            Assert.That(fromDb.Name, Is.EqualTo(toadd.Name));
            Assert.That(fromDb.DefaultMappingStore, Is.Not.Null);
            Assert.That(fromDb.DefaultMappingStore.Name, Is.EqualTo(toadd.DefaultMappingStore.Name));
            Assert.That(fromDb.MappingStores, Is.Not.Empty);
            Assert.That(fromDb.MappingStores.Count, Is.EqualTo(toadd.MappingStores.Count));

            toadd.AccessRules.Add(new AccessRule() {Name = "StructureImporterRole"});
            _persistenceEngine.Update(toadd);
            fromDb = _retrieveEngine.GetUser(toadd.Name);
            Assert.That(fromDb.AccessRules, Is.Not.Empty);
            Assert.That(fromDb.AccessRules.Count, Is.EqualTo(toadd.AccessRules.Count));
            
            toadd.AccessRules.Clear();
            toadd.AccessRules.Add(new AccessRule() { Name = "DOES_NT_EXIST" });
            Assert.That(() => _persistenceEngine.Update(toadd), Throws.TypeOf<SecurityResourceNotFoundException>());
            toadd.AccessRules.Clear();
            toadd.AccessRules.Add(new AccessRule() { Name = "AdminRole" });
            _persistenceEngine.Update(toadd);
            fromDb = _retrieveEngine.GetUser(toadd.Name);
            Assert.That(fromDb.AccessRules, Is.Not.Empty);
            Assert.That(fromDb.AccessRules.Count, Is.EqualTo(toadd.AccessRules.Count));

            _persistenceEngine.Delete(toadd);
            foreach (var mappingStoreInfo in toadd.MappingStores)
            {
                Assert.That(!_retrieveEngine.IsUsed(mappingStoreInfo));
            }
        }

        [Test]
        public void ShouldNotAddUserTwoTimes()
        {
            var toadd = new User() {Name = "test_user"};
            toadd.AccessRules.Add(
                new AccessRule()
                {
                    Name = "WsUserRole"
                });
            _persistenceEngine.Add(toadd, "123");

            var fromDb = _retrieveEngine.GetUser(toadd.Name);
            Assert.That(fromDb, Is.Not.Null);
          
            Assert.That(() => _persistenceEngine.Add(toadd, "567"), Throws.TypeOf<ResourceAlreadyExistsException>());
        }

        [Test]
        public void ShouldAddUserWithoutMappingStore()
        {
            var toadd = new User() {Name = "test_user"};
            toadd.AccessRules.Add(
                new AccessRule()
                {
                    Name = "WsUserRole"
                });
            _persistenceEngine.Add(toadd, "123");

            var fromDb = _retrieveEngine.GetUser(toadd.Name);
            Assert.That(fromDb, Is.Not.Null);
            Assert.That(fromDb.Name, Is.EqualTo(toadd.Name));
            Assert.That(fromDb.AccessRules.Count, Is.EqualTo(toadd.AccessRules.Count));
            foreach (var toaddAccessRule in toadd.AccessRules)
            {
                Assert.That(fromDb.AccessRules.Any(x => x.Name.Equals(toaddAccessRule.Name)));
                Assert.That(_retrieveEngine.IsUsed(toaddAccessRule));
            }
        }

        [Test]
        public void ShouldAddUserWithoutMappingStoreAndDelete()
        {
            var toadd = new User() {Name = "test_user"};
            toadd.AccessRules.Add(
                new AccessRule()
                {
                    Name = "WsUserRole"
                });
            _persistenceEngine.Add(toadd, "123");

            var fromDb = _retrieveEngine.GetUser(toadd.Name);
            Assert.That(fromDb, Is.Not.Null);

            _persistenceEngine.Delete(toadd);
            var fromDb2 = _retrieveEngine.GetUser(toadd.Name);
            Assert.That(fromDb2, Is.Null);
        }

        [Test, Ignore("Need to verify if the test is still valid")]
        public void ShouldRetrieveUserWithImpliedRules()
        {
            var toadd = new User() {Name = "hello"};
            toadd.AccessRules.Add(
                new AccessRule()
                {
                    Name = "AdminRole"
                });
            _persistenceEngine.Add(toadd, "123");

            var fromDb = _retrieveEngine.GetUser(toadd.Name);
            Assert.That(fromDb, Is.Not.Null);
            Assert.That(fromDb.AccessRules, Is.Not.Empty);
            Assert.That(fromDb.AccessRules, Has.Count.GreaterThan(1));
            Assert.That(fromDb.AccessRules, Has.Exactly(1).Matches<AccessRule>(t=> t.Name.Equals("CanImportStructures")));
        }

        [Test]
        public void ShouldRetrievePrincipalWithImpliedRules()
        {
            var toadd = new User() {Name = "hello"};
            toadd.AccessRules.Add(
                new AccessRule()
                {
                    Name = "AdminRole"
                });
            _persistenceEngine.Add(toadd, "123");

            var fromDb= _principalRetriever.GetPrincipal(_authDb, "hello", "123", null); 
            Assert.That(fromDb.IsInRole("CanImportStructures"));
            Assert.That(fromDb.IsInRole("AdminRole"));
        }
    }
}