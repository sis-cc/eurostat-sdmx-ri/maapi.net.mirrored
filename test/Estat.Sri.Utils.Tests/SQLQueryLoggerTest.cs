// -----------------------------------------------------------------------
// <copyright file="SQLQueryLoggerTest.cs" company="EUROSTAT">
//   Date Created : 2018-8-9
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

using Estat.Sri.Utils.Helper;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Repository;
using NUnit.Framework;

namespace Estat.Sri.Utils.Tests
{
    class SQLQueryLoggerTest
    {
        private const string SqlQuery = "select * from table";

        private readonly ILoggerRepository _logRepository;
        public SQLQueryLoggerTest()
        {
            _logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(_logRepository, new FileInfo("log4net.config"));
        }

        [Test]
        public void ShouldRecordInLog()
        {
            using (var sqlL = new SqlQueryLogger(SqlQuery))
            {
                // do stuff
            }

            string file = GetLogFile(_logRepository);
            var line = File.ReadLines(file).FirstOrDefault();
            Assert.That(line, Is.Not.Null);
            Assert.That(line, Is.SupersetOf($";{SqlQuery};"));
        }

        [Test]
        public void ShouldRecordInLogCustomPath()
        {
            var dict = new Dictionary<string, object>() {{"test", "123"}};
            using (var sqlL = new SqlQueryLogger(SqlQuery, dict , "test123", "file.cs", 14))
            {
                // do stuff
            }

            string file = GetLogFile(_logRepository);
            var line = File.ReadLines(file).FirstOrDefault(x => x.Contains(";test123;"));
            Assert.That(line, Is.Not.Null);
            Assert.That(line, Is.SupersetOf(";test123;"));
            Assert.That(line, Is.SupersetOf(";file.cs;14"));
        }

        private static string GetLogFile(ILoggerRepository repository)
        {
            // TODO FIX ME 
            IAppender[] appender = repository.GetAppenders();
            var rootAppender = appender
                                 .OfType<FileAppender>()
                                 .FirstOrDefault(fa => fa.Name == "SQLQueryLoger");
            string file = rootAppender.File;
            FileAssert.Exists(file);
            return file;
        }

        [Test]
        public void ShouldRecordInLogMultipleThreads()
        {
            for(int i=0; i < 10; i++)
            {
                new Thread(InThreadTest1).Start();
                new Thread(InThreadTest2).Start();
                new Thread(InThreadTest3).Start();
            }

            string file = GetLogFile(_logRepository);
            var lines = File.ReadLines(file).Where(x => x.Contains(";InThreadTest")).ToArray();
            Assert.That(lines, Is.Not.Empty);
            foreach(var line in lines)
            {
                if (line.Contains(nameof(InThreadTest1)))
                {
                    Assert.That(line, Is.SupersetOf("--- 1"));
                }
                if (line.Contains(nameof(InThreadTest2)))
                {
                    Assert.That(line, Is.SupersetOf("--- 2"));
                }

                if (line.Contains(nameof(InThreadTest3)))
                {
                    Assert.That(line, Is.SupersetOf("--- 3"));
                }
            }
        }

        private void InThreadTest1()
        {
            using (var sqlL = new SqlQueryLogger(SqlQuery + "--- 1"))
            {
                // do stuff
            }
        }
        private void InThreadTest2()
        {
            using (var sqlL = new SqlQueryLogger(SqlQuery + "--- 2"))
            {
                // do stuff
            }
        }
        private void InThreadTest3()
        {
            using (var sqlL = new SqlQueryLogger(SqlQuery + "--- 3"))
            {
                // do stuff
            }
        }
    }
}
