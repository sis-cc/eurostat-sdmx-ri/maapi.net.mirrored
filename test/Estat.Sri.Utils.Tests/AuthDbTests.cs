// -----------------------------------------------------------------------
// <copyright file="InitializeDatabaseTests.cs" company="EUROSTAT">
//   Date Created : 2018-3-11
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Security;
using Estat.Sri.Security.Factory;
using Estat.Sri.Security.Manager;
using Estat.Sri.Security.Model;
using NUnit.Framework;
using DryIoc;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Engine;

namespace Estat.Sri.Utils.Tests
{
    [TestFixture("odp")]
    [TestFixture("sqlserver")]
    [TestFixture("mysql")]
    class AuthDbTests : TestWithContainer
    {
        private readonly string _sid;
        private readonly ISecurityStoreFactory _authdbfactory;
        private readonly SecurityManager _securityManager;
        private readonly Database _database;

        public AuthDbTests(string name):base(name)
        {
            _sid = name;
            _authdbfactory = _container.Resolve<ISecurityStoreFactory>();
            _securityManager = new SecurityManager(_authdbfactory);
            var connectionString = ConfigurationManager.ConnectionStrings[name];
            _database = new Database(connectionString);
        }

        [Test]
        public void ShouldInitializeDatabase()
        {
            var store = new AuthDbSecurityStore(_database);
            var engine = _securityManager.GetSchemaEngine(store);
            var retriever = _securityManager.GetRetrieverEngine(store);
            Assert.That(engine, Is.Not.Null);

            engine.DeleteStore();
            engine.Initialize();
            var version = engine.RetrieveVersion();
            Assert.That(version.Version.ToString(2), Is.EqualTo("1.0"));

            var defaultUser = retriever.GetUser("admin");
            Assert.That(defaultUser, Is.Not.Null);
            Assert.That(defaultUser.Name, Is.Not.Null);
            Assert.That(defaultUser.AccessRules, Is.Not.Null);
            Assert.That(defaultUser.AccessRules.Count, Is.EqualTo(1));
            Assert.That(defaultUser.AccessRules[0].Name, Is.EqualTo("AdminRole"));

            var accessRules = retriever.GetAccessRules();
            Assert.That(accessRules.Count, Is.GreaterThan(1));
            Assert.That(accessRules.Any(x => x.Name.Equals("AdminRole")));

            var impliedRules = retriever.GetImpliedAccessRules();
            Assert.That(impliedRules, Is.Not.Empty);
            Assert.That(!impliedRules.Any(x => x.Any(y => x.Key.Equals(y))));
            var adminImplied = retriever.GetFlatImpliedAccessRules(defaultUser.AccessRules[0]);
            Assert.That(adminImplied, Is.Not.Empty);
            Assert.That(adminImplied.Count, Is.GreaterThan(1));
        }

        [Test]
        public void ShouldReturn200IfConnectionIsCorrectAsAnAdmin()
        {
            var authdbSchema = _authdbfactory.GetSchemaEngine(new AuthDbSecurityStore(_database.ConnectionStringSettings));
            var connectionEntity = CreateConnection("localhost");
            var result = authdbSchema.TestConnection(connectionEntity, true);
            Assert.That(result.ErrorCode, Is.EqualTo("200"));
        }

        [Test]
        public void ShouldReturn503IfConnectionIsNotCorrect()
        {
            var authdbSchema = _authdbfactory.GetSchemaEngine(new AuthDbSecurityStore(_database.ConnectionStringSettings));
            var connectionEntity = CreateConnection("fake");
            var result = authdbSchema.TestConnection(connectionEntity, true);
            Assert.That(result.ErrorCode, Is.EqualTo("503"));
        }

        [Test]
        public void ShouldReturn503IfConnectionIsNotCorrectWhenStoring()
        {
            var authdbSchema = _authdbfactory.GetSchemaEngine(new AuthDbSecurityStore(_database.ConnectionStringSettings));
            var connectionEntity = CreateConnection("fake");
            var result = authdbSchema.StoreConnection(connectionEntity, true);
            Assert.That(result.ErrorCode, Is.EqualTo("503"));
        }

        [Test]
        public void ShouldReturn200IfConnectionIsCorrectAndConnectionIsNotConfiguredAsANonAdmin()
        {
            var connectionStringSettings = new ConnectionStringSettings()
            {
                Name = "authdb",
                ProviderName = _database.ProviderName
            };
            RemoveConnectionStringIfExists("authdb");
            var authdbSchema = _authdbfactory.GetSchemaEngine(new AuthDbSecurityStore(connectionStringSettings));
            var connectionEntity = CreateConnection("localhost");
            connectionEntity.Name = "authdb";
            var result = authdbSchema.TestConnection(connectionEntity, false);
            Assert.That(result.ErrorCode, Is.EqualTo("200"));
        }

        [Test]
        public void ShouldReturn403IfConnectionIsCorrectAndConnectionIsAlreadyConfiguredAsANonAdmin()
        {
            var authdbSchema = _authdbfactory.GetSchemaEngine(new AuthDbSecurityStore(_database.ConnectionStringSettings));
            var connectionEntity = CreateConnection("localhost");
            connectionEntity.Name = _database.Name;
            var result = authdbSchema.TestConnection(connectionEntity, false);
            Assert.That(result.ErrorCode, Is.EqualTo("403"));
        }

        [Test]
        public void ShouldReturn403ConnectionIsAlreadyConfiguredAsANonAdminWhenStoring()
        {
            var authdbSchema = _authdbfactory.GetSchemaEngine(new AuthDbSecurityStore(_database.ConnectionStringSettings));
            var connectionEntity = CreateConnection("localhost");
            connectionEntity.Name = _database.Name;
            var result = authdbSchema.StoreConnection(connectionEntity, false);
            Assert.That(result.ErrorCode, Is.EqualTo("403"));
        }

        [Test]
        public void ShouldReturn201ConnectionIsNotConfiguredAsANonAdminWhenStoring()
        {
            var connectionStringSettings = new ConnectionStringSettings()
            {
                Name = "fake",
                ProviderName = _database.ProviderName
            };
            var authdbSchema = _authdbfactory.GetSchemaEngine(new AuthDbSecurityStore(connectionStringSettings));
            var connectionEntity = CreateConnection("localhost");
            connectionEntity.Name = "fake";
            RemoveConnectionStringIfExists("authdb");
            var result = authdbSchema.StoreConnection(connectionEntity, false);
            Assert.That(result.ErrorCode, Is.EqualTo("201"));
        }


        
        [Test]
        public void ShouldReturn201AsAdminWhenStoring()
        {
            var connectionStringSettings = new ConnectionStringSettings()
            {
                Name = "fake",
                ProviderName = _database.ProviderName
            };
            var authdbSchema = _authdbfactory.GetSchemaEngine(new AuthDbSecurityStore(connectionStringSettings));
            var connectionEntity = CreateConnection("localhost");
            connectionEntity.Name = "fake";
            
            var result = authdbSchema.StoreConnection(connectionEntity, true);
            Assert.That(result.ErrorCode, Is.EqualTo("201"));
        }

        [Test]
        public void ShouldReturnCorrectStatusWhenGettingVersion()
        {
            var connectionStringSettings = new ConnectionStringSettings()
            {
                Name = "authdb",
                ProviderName = _database.ProviderName
            };
            AuthDbVersionAndStatus version = null;
            var authdbSchema = _authdbfactory.GetSchemaEngine(new AuthDbSecurityStore(connectionStringSettings));
            RemoveConnectionStringIfExists("authdb");
            
            //connection not set
            version = authdbSchema.RetrieveVersion();
            Assert.That(version.StatusAndMessage.Status, Is.EqualTo(AuthDbStatus.NotConfigured));

            //connection set directly through engine 
            var connectionEntity = CreateConnection("fakehost");
            connectionEntity.Name = "authdb";
            SaveConnectionThroughManager(connectionEntity);
            authdbSchema = _authdbfactory.GetSchemaEngine(new AuthDbSecurityStore(connectionStringSettings.Name));
            version = authdbSchema.RetrieveVersion();
            Assert.That(version.StatusAndMessage.Status, Is.EqualTo(AuthDbStatus.NotAccessible));

            //delete store and set correct connection
            
            connectionEntity = CreateConnection("localhost");
            connectionEntity.Name = "authdb";
            authdbSchema.StoreConnection(connectionEntity, true);
            authdbSchema = _authdbfactory.GetSchemaEngine(new AuthDbSecurityStore(connectionStringSettings.Name));
            authdbSchema.DeleteStore();
            version = authdbSchema.RetrieveVersion();
            Assert.That(version.StatusAndMessage.Status, Is.EqualTo(AuthDbStatus.ConfiguredAccessibleNotInitialized));

            //Initialize the schema
            authdbSchema.DeleteStore();
            authdbSchema.Initialize();
            authdbSchema = _authdbfactory.GetSchemaEngine(new AuthDbSecurityStore(connectionStringSettings.Name));
            version = authdbSchema.RetrieveVersion();
            Assert.That(version.StatusAndMessage.Status, Is.EqualTo(AuthDbStatus.ConfiguredAccessibleInitialized));
            Assert.That(version.Version.ToString(2), Is.EqualTo("1.0"));
        }

        public ConnectionEntity CreateConnection(string server)
        {
            var connectionEntity = new ConnectionEntity()
            {
                StoreId = "authdb",
            };
            if (_sid == "sqlserver")
            {
                connectionEntity.DatabaseVendorType = "SqlServer";
                connectionEntity.AddSetting("server", ParameterType.String, server);
                connectionEntity.AddSetting("database", ParameterType.String, "authdb_empty");
                connectionEntity.AddSetting("port", ParameterType.String, "1433");
                connectionEntity.AddSetting("user id", ParameterType.String, "mauser");
                connectionEntity.AddSetting("password", ParameterType.String, "123");
            }
            if (_sid == "odp")
            {
                connectionEntity.DatabaseVendorType = "Oracle";
                connectionEntity.AddSetting("server", ParameterType.String,server + "/xe");
                connectionEntity.AddSetting("port", ParameterType.String, "1521");
                connectionEntity.AddSetting("user id", ParameterType.String, "authdb_empty");
                connectionEntity.AddSetting("password", ParameterType.String, "123");
            }
            if (_sid == "mysql")
            {
                connectionEntity.DatabaseVendorType = "MySQL";
                connectionEntity.AddSetting("server", ParameterType.String, server);
                connectionEntity.AddSetting("port", ParameterType.String, "3306");
                connectionEntity.AddSetting("database", ParameterType.String, "authdb_empty");
                connectionEntity.AddSetting("user id", ParameterType.String, "sdmxuser");
                connectionEntity.AddSetting("password", ParameterType.String, "sdmxpass");
            }
            return connectionEntity;
        }

        public void RemoveConnectionStringIfExists(string name)
        {
            var databaseOptions = new DatabaseIdentificationOptions() { StoreId = name };
            var mappingStoreManager = _container.Resolve<IMappingStoreManager>();
            if (mappingStoreManager.GetEngineByStoreType(null).Retrieve(new DatabaseIdentificationOptions() { StoreId = name }) != null)
            {
                mappingStoreManager.GetEngineByStoreType(null).Delete(databaseOptions);
            }
        }

        public void SaveConnectionThroughManager(ConnectionEntity connectionEntity)
        {
            var mappingStoreManager = _container.Resolve<IMappingStoreManager>();
            mappingStoreManager.GetEngineByStoreType(null).Save(connectionEntity);
        }
    }
}
