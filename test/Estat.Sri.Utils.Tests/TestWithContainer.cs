// -----------------------------------------------------------------------
// <copyright file="InitializeDatabaseTests.cs" company="EUROSTAT">
//   Date Created : 2018-3-11
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DryIoc;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Security;
using Estat.Sri.Security.Factory;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;

namespace Estat.Sri.Utils.Tests
{
    public class TestWithContainer
    {
        protected readonly Container _container;

        public TestWithContainer(string sid)
        {
            _container =
                    new Container(
                        rules =>
                        rules.With(FactoryMethod.ConstructorWithResolvableArguments)
                            .WithoutThrowOnRegisteringDisposableTransient());
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
            string[] folders = { "Estat.Sri.Plugin.SqlServer", "Estat.Sri.Plugin.Oracle", "Estat.Sri.Plugin.Mysql" };
            List<FileInfo> plugins = new List<FileInfo>();
            foreach (var folder in folders)
            {
                plugins.Add(new FileInfo(string.Format("../../../../../src/{0}/bin/Debug/netstandard2.0/{0}.dll", folder)));
                plugins.Add(new FileInfo(string.Format("../../../../../src/{0}/bin/Debug/netstandard2.0/{0}.pdb", folder)));
            }

            var assemblies = new List<Assembly>();
            foreach (var fileInfo in plugins)
            {
                var destPath = Path.Combine(TestContext.CurrentContext.WorkDirectory, fileInfo.Name);
                if (!File.Exists(destPath) || File.GetLastWriteTime(destPath) < fileInfo.LastWriteTime)
                {
                    fileInfo.CopyTo(destPath, true);
                }

            }
            foreach (var fileInfo in plugins)
            {
                var destPath = Path.Combine(TestContext.CurrentContext.WorkDirectory, fileInfo.Name);
                if (fileInfo.Extension.EndsWith("dll", StringComparison.OrdinalIgnoreCase))
                {
                    assemblies.Add(Assembly.LoadFile(destPath));
                }
            }
            assemblies.AddRange(new[] { typeof(IDatabaseProviderManager).Assembly, typeof(DatabaseManager).Assembly });
            foreach (var assembly in assemblies)
            {
                var list = new List<Assembly>()
                    {
                        assembly
                    };
                _container.RegisterMany(list, type => !typeof(IEntity).IsAssignableFrom(type));
            }
            _container.Register<IStructureParsingManager, StructureParsingManager>(
                    made: Made.Of(() => new StructureParsingManager()));
            _container.Register<ISecurityStoreFactory, AuthDbSecurityFactory>();
        }
    }
}