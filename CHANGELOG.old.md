## MappingStoreRetrieval_v7.3.1
_2017-07-03_
---

The following bugs have been corrected:
- SDMXRI-530: Single quote in local code used in transcoding generates an error
- SDMXRI-466: Fix MSD retrieval when we have nested metadata attributes


## MappingStoreRetrieval_v7.1.0
_2017-04-25_
---
The following new features added:
- SDMXRI-466: MSD/MDF persist/retrieve
- SDMXRI-82: Maria BD / Linux / misc. enhancement
- SDMXRI-471: Implementation of Data Registration functionality
- SDMXRI-487: Modify ISTAT retriever code to be compatible future SDMX RI proof
- SDMXRI-465: ISTAT Review & Merge retrievers from ISTAT





