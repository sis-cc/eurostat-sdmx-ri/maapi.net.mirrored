#!groovy

pipeline {
  agent any
   options {
     buildDiscarder(logRotator(artifactDaysToKeepStr: '10', artifactNumToKeepStr: '5', daysToKeepStr: '10', numToKeepStr: '5')) 
  }
  parameters {
    string(description: 'date package was build (leave blank for current date)', name: 'buildDate')
    string(description: 'Create tag with the specific version and force version. It overrides the version_inc', name: 'forceVersion')
    choice(name: 'version_inc', choices: ['No', 'Major', 'Minor', 'Patch'], description: 'Select whether to increment version, note that unless there is a change it will remain stable. Selecting a value other than No it will update the RI dependencies and update the version (if needed)')
    choice(name: 'pre_release', choices: ['No', 'alpha', 'beta', 'rc'], description: 'Select whether to to add a version suffix (pre-release) and which')
    choice(name: 'build_config', choices: ['Release', 'Debug'], description: 'Select build configuration')
    booleanParam(name: 'build_archive', defaultValue: false, description: 'Check to create the zip archive to be send to CIRCA')
  }
  environment {
    PROJ='Estat.Sri.Mapping.sln'
    CONFIG="${params.build_config}"
  }
  tools {
    dotnetsdk 'dotnet6'
    nodejs "Latest"
  }
  stages {
    stage ('Clean') {
      steps {
        dotnetClean(project: env.PROJ, configuration: env.CONFIG)
      }
    }
    stage ('Get tools') {
      steps {
        dir ('tools') {
          sh 'chmod +x *.sh'
        }
      }
    }
    stage ('Build nuget.config') {
      when {
        expression {return env.NUGET_REPO }
      }
      steps {
        sh './tools/create-nuget-config.sh "$NUGET_REPO"'
      }
    }
    stage('Apply workaround') {
      steps {
        // workaround this test references SR but SR might not have been built yet!
        sh '''
        dotnet sln remove test/Estat.Sri.Mapping.MappingStore.Tests/Estat.Sri.Mapping.MappingStore.Tests.csproj
        '''
      }
    }
 
    stage ('Increment version') {
      when {
        anyOf {
          not { 
            equals(actual: params.version_inc, expected: 'No')
          }
          expression {return params.forceVersion }
        }
      }
      steps {
         withCredentials([gitUsernamePassword(credentialsId: 'citnet_user_with_token', gitToolName: 'Default')]) {
             sh 'git fetch'
             sh 'git checkout $BRANCH_NAME'
         }
       
        script {
          env.OLD_COMMIT=sh(returnStdout: true, script: 'git rev-parse --verify HEAD')
          sh "./tools/update-deps.sh '${params.version_inc}' "
          if (params.forceVersion) {
            env.NEW_VERSION=params.forceVersion
          } else {
            env.NEW_VERSION=sh(returnStdout: true, script: "./tools/get-version.sh '${params.version_inc}' '${params.pre_release}.${currentBuild.number}'").trim()
          }
          sh './tools/update-versions.sh $NEW_VERSION'
          env.NEW_COMMIT=sh(returnStdout: true, script: 'git rev-parse --verify HEAD')
        }
        // withCredentials([usernamePassword(credentialsId: 'citnet_user_with_token', gitToolName: 'Default')]) {
      }
    }

    stage ('Restore') {
      steps {
        dotnetRestore(project: env.PROJ)
      }
    }
    stage('Build') {
      steps {
        dotnetBuild(project: env.PROJ, configuration: env.CONFIG, noRestore: true)
      }
    }
    stage('Revert workaround') {
      steps {
        // revert workaround
        sh '''
        git checkout -- *.sln
        '''
      }
    }
 
    stage ('Push changes') {
      when {
        allOf {
          expression { return env.OLD_COMMIT }
          expression { return env.NEW_COMMIT }
          expression { return env.NEW_VERSION }
        }
      }
      steps {
        withCredentials([gitUsernamePassword(credentialsId: 'citnet_user_with_token', gitToolName: 'Default')]) {
          sh './tools/tag_n_push.sh "$OLD_COMMIT" "$NEW_COMMIT" "$NEW_VERSION"'
        }
      }
    }
    /*
    stage('Test') {
      steps {
        // Maybe Todo produce JUNIT <PackageReference Include="JunitXml.TestLogger" Version="3.0.110" />
        // TODO tests will fail on Linux because of hardcoded path separator \ instead of /
        // dotnetTest(project: env.PROJ, continueOnError:true, noBuild:true, unstableIfErrors: true)
      }
    }*/
    stage('Push nuget packages') {
      steps {
        dotnetNuGetPush (root: "src/*/bin/${params.build_config}/*.nupkg", noServiceEndpoint: true, source: env.NUGET_REPO)
        archiveArtifacts(artifacts: "src/*/bin/${params.build_config}/*.nupkg", onlyIfSuccessful: true)
      }
    }
    stage('build archive') {
      when {
        expression { return params.build_archive }
      }
      steps {
        dir('workdir') {
          deleteDir()
        }
        // TODO copy dependencies to lib
        sh './tools/create_archive.sh workdir workdir/package'
        archiveArtifacts(artifacts: 'workdir/src.tar.gz', onlyIfSuccessful: true)
        dir('workdir') {
          dir('package') {
            zip(zipFile:"../tool.zip")
          }
          archiveArtifacts(artifacts: "tool.zip", onlyIfSuccessful: true)
        }
      }
    }
  }
}
