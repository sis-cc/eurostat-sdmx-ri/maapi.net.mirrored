#!/bin/bash

set -u 
set -e
#set -x


if [[ $# < 1 || $# > 3 ]]; then
    echo "usage:"
    echo "$0 (major|minor|patch|current) <pre-release> <metadata>"
    echo "pre-release is optional and understands a special value 'No' to disable it"
    echo "metadata is optional"
    echo "Examples"
    echo "$0 minor alpha e2f20240a22d"
    exit -1
fi

# mandatory parameters
if [[ $# > 0 && "$1" != current* && "$1" != No*  ]]; then
  limitUpdate="-a $1"
else
  limitUpdate=""
  ver=$(sed -n -e 's#^\s*<VersionPrefix>\([0-9.]\+\)</VersionPrefix>\s*$#\1#p' Directory.Build.props)
  versionSuffix=$(sed -n -e 's#^\s*<VersionSuffix>\([^<]\+\)</VersionSuffix>\s*$#\1#p' Directory.Build.props)
  if [[ -n "$versionSuffix" ]]; then
    echo -n "$ver-$versionSuffix"
  else
    echo -n "$ver"
  fi
  exit 0
fi

if [[ $# > 1 && "$2" != No* ]]; then
  prerelease="-p $2"
else
  prerelease=""
fi
if [[ $# > 2 && -n "$3" ]]; then
  metadata="-b $3"
else
  metadata=""
fi

export PATH="$PATH:$HOME/.dotnet/tools"

if [[ ! -f /tmp/.dotnet-minver-cli-installed ]]; then
  if dotnet tool list -g | grep -q minver-cli; then 
    echo "OK minver-cli installed"
  else 
    dotnet tool install --global minver-cli >&2
  fi
  touch /tmp/.dotnet-minver-cli-installed 
fi


ver=$(minver $limitUpdate $metadata $prerelease -t rel-)
if [[ $ver == 0.0.* ]]; then
  echo "Error invalid version: $ver" >&2
  exit -2
fi
if [[ -z $prerelease ]]; then
  ver=${ver%%-*}
fi
echo -n $ver
