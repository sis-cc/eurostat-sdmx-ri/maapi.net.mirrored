# Mapping Assistant API (.NET)

## Synopsis

A API for accessing and altering information in a SDMX RI Mapping Store.

## Code Example

1. For simplicity Register the assemblies with a IoC e.g. DryIoc

Note this is not mandatory.

```c#
var _container =
                new Container(
                    rules =>
                    rules.With(FactoryMethod.ConstructorWithResolvableArguments)
                        .WithoutThrowOnRegisteringDisposableTransient());
            var assemblies = new[] { typeof(IDatabaseProviderManager).Assembly, typeof(DatabaseManager).Assembly };
_container.RegisterMany(assemblies,type => !typeof(IEntity).IsAssignableFrom(type));
```

2. Resolve the retriever and persistence manager

```c#
var retrieverManager = container.Resolve<IEntityRetrieverManager>();
var persistManager = container.Resolve<IEntityPersistenceManager>();
```

3. Once you have the two manager objects you can :

3.1. Retrieve entities by creating a EntityQuery :

```c#
var entityQuery = new EntityQuery {EntityId = new Criteria<string>(OperatorType.Exact, "2")};
var datasetRetrieverEngine = entityRetrieverManager.GetRetrieverEngine<DatasetEntity>(mappingStoreIdentifier);
var entities = datasetRetrieverEngine.GetEntities(entityQuery, Detail.Full)
```

3.2. Insert entities:

```c#
var entity = new DatasetEntity
      {
      Name = "some name",
      Query = "some query",
          ParentId = "3",
      Description = "some description",
      XmlQuery = "some xml query",
      EditorType = "some editor type"
      };
var datasetEngine = persistManager.GetEngine<DatasetEntity>(mappingStoreIdentifier,EntityType.DataSet);
      datasetEngine.Add(entity)
```

3.3. Delete entities:

```c#
      var datasetEngine = persistManager.GetEngine<DatasetEntity>(mappingStoreIdentifier,EntityType.DataSet);
      datasetEngine.Delete("71",EntityType.DataSet);
```

3.4. Update entities:

```c#
var patch = new PatchRequest()
      {
        new PatchDocument("replace", "/name", "myName")
      };
var datasetEngine = persistManager.GetEngine<DatasetEntity>(mappingStoreIdentifier,EntityType.DataSet);
datasetEngine.Update(patch, EntityType.DataSet,"70");
```

## Motivation

Migrate from a monolithic the SDMX RI Mapping Assistant application to a library and a web service. This is the library which can be re-used and extended.

## API Reference

TODO

## Installation

Use nuget to locate and install the packages

## Tests

To run tests open the solution with Visual Studio 2015 Update 3. 
You will need to install a NUnit 3.x Test Adapter which can be found in the Visual Studio Market Place (Tools -> Extensions and Updates)
Currently many tests depend on a existing MSDB, this will be fixed in the near future. Please configure the App.Config file under 

```sh
tests/Estat.Sri.Mapping.MappingStore.Tests
```

## Contributors

Tools needed:

* Git for Windows
* Visual Studio 2015 Update 3 Community Edition or better
  * Or alternative Build Tools 2015 or better

### How to build (Without Visual Studio)

```bat
msbuild.exe build.proj
```

### How to build nuget packages

```bat
msbuild.exe build.proj /t:Package
```

The packages will be stored in the build folder

### How to clone

Please note that submodules are used

```sh
git clone --recurse-submodules https://webgate.ec.europa.eu/CITnet/stash/scm/sdmxri/maapi.net.git
```

## License

EUPL v1.1
