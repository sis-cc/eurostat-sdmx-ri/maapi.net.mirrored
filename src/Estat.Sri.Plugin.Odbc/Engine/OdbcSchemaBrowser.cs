﻿// -----------------------------------------------------------------------
// <copyright file="OdbcSchemaBrowser.cs" company="EUROSTAT">
//   Date Created : 2017-07-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.Odbc.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Data.Odbc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    /// <summary>
    /// The ODBC schema browser.
    /// </summary>
    public class OdbcSchemaBrowser : IDatabaseSchemaBrowser
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(OdbcSchemaBrowser));

        /// <summary>
        /// Gets the database objects. E.g. Tables or Views
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The list of <see cref="DatabaseObject" />
        /// </returns>
        public IEnumerable<IDatabaseObject> GetDatabaseObjects(ConnectionStringSettings settings)
        {
            var database = new Database(settings);
            List<IDatabaseObject> databaseObjects = new List<IDatabaseObject>();
            var tables = GetDataTables(database, OdbcMetaDataCollectionNames.Tables);
            var tableList = GetDatabaseObjects(tables);
            databaseObjects.AddRange(tableList);

            var views = GetDataTables(database, OdbcMetaDataCollectionNames.Views);
            var viewList = GetDatabaseObjects(views);

            databaseObjects.AddRange(viewList);

            return databaseObjects;
        }

        /// <summary>
        /// Gets the fields of a database object.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="databaseObject">The database object.</param>
        /// <returns>
        /// The list of <see cref="IFieldInfo" />
        /// </returns>
        public IEnumerable<IFieldInfo> GetSchema(ConnectionStringSettings settings, IDatabaseObject databaseObject)
        {
            if (databaseObject == null)
            {
                throw new ArgumentNullException(nameof(databaseObject));
            }

            var database = new Database(settings);
            var columns = GetDataTables(database, OdbcMetaDataCollectionNames.Columns, null, null, databaseObject.Name);
            var name = columns.Columns["COLUMN_NAME"];
            if (name == null)
            {
                throw new ResourceNotFoundException("This ODBC driver doesn't support querying for columns");
            }

            List<IFieldInfo> fieldInfos = new List<IFieldInfo>();
            var type = columns.Columns["DATA_TYPE"] ?? columns.Columns["DATATYPE"] ?? columns.Columns["TYPE_NAME"];
            foreach (DataRow column in columns.Rows)
            {
                var field = new FieldInfo();
                field.Name = column[name]?.ToString();
                if (!string.IsNullOrWhiteSpace(field.Name))
                {
                    if (type != null)
                    {
                        field.DataType = column[type]?.ToString();
                    }

                    fieldInfos.Add(field);
                }
            }

            return fieldInfos;
        }

        /// <summary>
        /// Creates a new connection.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>The <see cref="DbConnection"/></returns>
        public DbConnection CreateConnection(ConnectionStringSettings settings)
        {
            return new Database(settings).CreateConnection();
        }

        /// <summary>
        /// Gets the type of the object.
        /// </summary>
        /// <param name="tableType">Type of the table.</param>
        /// <param name="table">The table.</param>
        /// <param name="defaultType">The default type.</param>
        /// <returns>
        /// The <see cref="DatabaseObjectType" />.
        /// </returns>
        private static DatabaseObjectType GetObjectType(DataColumn tableType, DataRow table, DatabaseObjectType defaultType)
        {
            if (tableType != null)
            {
                var type = table[tableType]?.ToString();
                if ("VIEW".Equals(type, StringComparison.OrdinalIgnoreCase))
                {
                    return DatabaseObjectType.View;
                }

                if ("TABLE".Equals(type, StringComparison.OrdinalIgnoreCase))
                {
                    return DatabaseObjectType.Table;
                }
            }

            return defaultType;
        }

        /// <summary>
        /// Gets the database objects.
        /// </summary>
        /// <param name="tables">The tables.</param>
        /// <exception cref="ResourceNotFoundException">This ODBC driver does not support querying for Tables/Views</exception>
        private static List<DatabaseObject> GetDatabaseObjects(DataTable tables)
        {
            var tableType = tables.Columns["TABLE_TYPE"];
            var tableName = tables.Columns["TABLE_NAME"];
            if (tableName == null)
            {
                throw new ResourceNotFoundException("This ODBC driver does not support querying for Tables/Views");
            }

            var tableList = new List<DatabaseObject>();
            foreach (DataRow table in tables.Rows)
            {
                var name = table[tableName]?.ToString();
                if (!string.IsNullOrWhiteSpace(name))
                {
                    var objectType = GetObjectType(tableType, table, DatabaseObjectType.Table);

                    var databaseObject = new DatabaseObject() { Name = name, ObjectType = objectType };
                    tableList.Add(databaseObject);
                }
            }

            return tableList;
        }

        /// <summary>
        /// Gets the data tables.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="constraints">The constraints.</param>
        /// <returns>
        /// The data table
        /// </returns>
        private DataTable GetDataTables(Database database, string collectionName, params string[] constraints)
        {
            try
            {
                using (var connection = database.CreateConnection())
                {
                    connection.Open();
                    if (constraints != null && constraints.Length > 0)
                    {
                        return connection.GetSchema(collectionName, constraints);
                    }

                    return connection.GetSchema(collectionName);
                }
            }
            catch (Exception ex)
            {
                //// TODO move to GUI MessageBox.Show(string.Format("Could not retrieve table schema information. Exception: {0}", ex.Message));
                _log.Error("Could not retrieve table schema information.", ex);
                throw;
            }
        }
    }
}