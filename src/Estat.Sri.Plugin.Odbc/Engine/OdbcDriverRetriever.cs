// -----------------------------------------------------------------------
// <copyright file="OdbcDriverRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-07-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.Odbc.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Security;
    using System.Threading;

    using log4net;

    using Microsoft.Win32;

    /// <summary>
    /// ODBC Driver retriever
    /// </summary>
    internal class OdbcDriverRetriever
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(OdbcDriverRetriever));

        /// <summary>
        /// The lazy
        /// </summary>
        private readonly Lazy<IList<string>> _lazy;

        /// <summary>
        /// Initializes a new instance of the <see cref="OdbcDriverRetriever"/> class.
        /// </summary>
        public OdbcDriverRetriever()
        {
            _lazy = new Lazy<IList<string>>(QueryDrivers, LazyThreadSafetyMode.ExecutionAndPublication);
        }

        /// <summary>
        /// Tries the query drivers.
        /// </summary>
        /// <returns>The set of driver names</returns>
        public ISet<string> TryQueryDrivers()
        {
            try
            {
                return new HashSet<string>(_lazy.Value);
            }
            catch (SecurityException e)
            {
                _log.Error(e);
            }
            catch (UnauthorizedAccessException e)
            {
                _log.Error(e);
            }

            return new HashSet<string>();
        }

        /// <summary>
        /// Queries the drivers from the Registry.
        /// </summary>
        /// <returns>The list of drivers; otherwise an empty list</returns>
        private static IList<string> QueryDrivers()
        {
            // TODO FIXME no support for Registry and compatibility package brings a lot of dependencies
            /*
            using (var odbcDrivers = Registry.LocalMachine.OpenSubKey("SOFTWARE\\ODBC\\ODBCINST.INI\\ODBC Drivers"))
            {
                if (odbcDrivers != null)
                {
                    return odbcDrivers.GetValueNames();
                }
            }
            */
            return new string[0];
        }
    }
}