// -----------------------------------------------------------------------
// <copyright file="OdbcSettingsBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-07-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Utils.Helper;
using Estat.Sri.Utils.Manager;

namespace Estat.Sri.Plugin.Odbc.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Common;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Plugin.Odbc.Engine;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// The ODBC settings builder.
    /// </summary>
    internal class OdbcSettingsBuilder : IConnectionSettingsBuilder
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(OdbcSettingsBuilder));
        /// <summary>
        /// The data source name keyword.
        /// </summary>
        private const string DataSourceNameKeyword = "DSN";

        /// <summary>
        /// The driver keyword.
        /// </summary>
        private const string DriverKeyword = "Driver";

        /// <summary>
        /// The server keyword.
        /// </summary>
        private const string ServerKeyword = "server";

        /// <summary>
        /// The database keyword.
        /// </summary>
        private const string DatabaseKeyword = "database";

        /// <summary>
        /// The user id keyword.
        /// </summary>
        private const string UserIdKeyword = "uid";

        /// <summary>
        /// The password keyword.
        /// </summary>
        private const string PasswordKeyword = "pwd";

        /// <summary>
        /// The _driver retriever.
        /// </summary>
        private readonly OdbcDriverRetriever _driverRetriever;

        /// <summary>
        /// The _provider name.
        /// </summary>
        private readonly string _providerName;

        /// <summary>
        /// Initializes a new instance of the <see cref="OdbcSettingsBuilder"/> class.
        /// </summary>
        /// <param name="driverRetriever">
        /// The driver retriever.
        /// </param>
        /// <param name="providerName">
        /// The provider Name.
        /// </param>
        public OdbcSettingsBuilder(OdbcDriverRetriever driverRetriever, string providerName)
        {
            _driverRetriever = driverRetriever;
            _providerName = providerName;
        }

        /// <summary>
        /// Creates the connection entity.
        /// </summary>
        /// <returns>The connection entity</returns>
        public IConnectionEntity CreateConnectionEntity()
        {
            return CreateConnectionEntity((ConnectionStringSettings)null);
        }

        /// <summary>
        /// Creates the connection entity from the specified <see cref="ConnectionStringSettings"/>
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// The <see cref="IConnectionEntity"/>
        /// </returns>
        public IConnectionEntity CreateConnectionEntity(ConnectionStringSettings settings)
        {
            IConnectionEntity connectionEntity = new ConnectionEntity();
            var builder = GetConnectionStringBuilder(_providerName);
            var existingSettings = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            if (settings != null)
            {
                try
                {
                    builder.ConnectionString = settings.ConnectionString;
                }
                catch (Exception e)
                {
                    _logger.Error($"Trying to set connection string {settings.ConnectionString} failed", e);
                }
                connectionEntity.Name = settings.Name;
                existingSettings.UnionWith(builder.Cast<KeyValuePair<string, object>>().Select(pair => pair.Key));
            }

            connectionEntity.SubTypeList.Add(DataSourceNameKeyword);
            connectionEntity.SubTypeList.Add(DriverKeyword);

            var dsnValue = GetValue(builder, DataSourceNameKeyword);
            var driverValue = GetValue(builder, DriverKeyword);
            if (!string.IsNullOrWhiteSpace(dsnValue as string))
            {
                connectionEntity.SubType = DataSourceNameKeyword;
                connectionEntity.AddSetting(DataSourceNameKeyword, ParameterType.String, dsnValue)
                    .AllowedSubTypes.Add(DataSourceNameKeyword);
                connectionEntity.DbName = dsnValue.ToString();
            }
            else if (!string.IsNullOrWhiteSpace(driverValue as string))
            {
                connectionEntity.SubType = DriverKeyword;
                connectionEntity.DbName = driverValue.ToString();
                var driverSetting = connectionEntity.AddSetting(DriverKeyword, ParameterType.Enumeration, driverValue);
                driverSetting.AllowedValues.AddAll(_driverRetriever.TryQueryDrivers());
                driverSetting.AllowedSubTypes.Add(DriverKeyword);

                CopyValue(builder, connectionEntity, ServerKeyword, subType: DriverKeyword);
                CopyValue(builder, connectionEntity, DatabaseKeyword, subType: DriverKeyword);
            }

            CopyValue(builder, connectionEntity, UserIdKeyword);
            CopyValue(builder, connectionEntity, PasswordKeyword, ParameterType.Password);

            return connectionEntity;
        }

        /// <summary>
        /// Creates the connection string.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <returns>
        /// The <see cref="ConnectionStringSettings"/>
        /// </returns>
        public ConnectionStringSettings CreateConnectionString(IConnectionEntity connection)
        {
            var builder = GetConnectionStringBuilder(_providerName);
            var connectionParameterEntities =
                connection.Settings.Where(
                    pair => !pair.Value.ForSpecificSubType || pair.Value.AllowedSubTypes.Contains(connection.SubType));

            foreach (var connectionParameterEntity in connectionParameterEntities)
            {
                if (!connectionParameterEntity.Value.IsItDefault())
                {
                    builder.Add(connectionParameterEntity.Key, connectionParameterEntity.Value.Value);
                }
            }

            return new ConnectionStringSettings(connection.Name, builder.ConnectionString, _providerName);
        }

        /// <summary>
        /// Creates the connection string.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <returns>
        /// The <see cref="ConnectionStringSettings"/>
        /// </returns>
        public ConnectionStringSettings CreateConnectionString(DdbConnectionEntity connection)
        {
            var providerName = _providerName;

            return new ConnectionStringSettings(connection.Name, connection.AdoConnString, providerName);
        }

        /// <summary>
        /// Creates the DDB connection entity.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <returns>
        /// The <see cref="DdbConnectionEntity"/>
        /// </returns>
        public DdbConnectionEntity CreateDdbConnectionEntity(IConnectionEntity connection)
        {
            if (connection == null)
            {
                throw new ArgumentNullException(nameof(connection));
            }

            var connectionEntity = new DdbConnectionEntity();
            connectionEntity.Name = connection.Name;
            connectionEntity.EntityId = connection.EntityId;
            connectionEntity.StoreId = connection.StoreId;
            connectionEntity.DbType = connection.DatabaseVendorType;
            connectionEntity.AdoConnString = CreateConnectionString(connection).ConnectionString;
            //connectionEntity.DbName = connection.DbName
            //                          ?? connection.GetValue<string>(DataSourceNameKeyword)
            //                          ?? connection.GetValue<string>(DriverKeyword);
            connectionEntity.DbUser = connection.GetValue<string>(UserIdKeyword);
            connectionEntity.Password = connection.GetValue<string>(PasswordKeyword);
            //connectionEntity.Server = connection.GetValue<string>(ServerKeyword);

            return connectionEntity;
        }

        /// <summary>
        /// Creates the connection entity.
        /// </summary>
        /// <param name="ddbConnection">
        /// The DDB connection.
        /// </param>
        /// <returns>
        /// The <see cref="IConnectionEntity"/>
        /// </returns>
        public IConnectionEntity CreateConnectionEntity(DdbConnectionEntity ddbConnection)
        {
            if (ddbConnection == null)
            {
                throw new ArgumentNullException(nameof(ddbConnection));
            }

            if (string.IsNullOrEmpty(ddbConnection.Name))
            {
                throw new SdmxException("Ddb connection name can not be empty", SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError));
            }

            var providerName = DatabaseType.GetProviderName(ddbConnection.DbType);
            var connectionEntity =
                CreateConnectionEntity(
                    new ConnectionStringSettings(ddbConnection.Name, ddbConnection.AdoConnString, providerName));
            connectionEntity.EntityId = ddbConnection.EntityId;
            connectionEntity.Name = ddbConnection.Name;
            connectionEntity.Description = ddbConnection.Description;
            connectionEntity.StoreId = ddbConnection.StoreId;
            return connectionEntity;
        }

        /// <summary>
        /// Gets the connection string builder.
        /// </summary>
        /// <param name="providerName">
        /// Name of the provider.
        /// </param>
        /// <returns>
        /// The <see cref="DbConnectionStringBuilder"/>
        /// </returns>
        private static DbConnectionStringBuilder GetConnectionStringBuilder(string providerName)
        {
            var factory = DbProviderFactories.GetFactory(providerName);
            var builder = factory.CreateConnectionStringBuilder();
            return builder;
        }

        /// <summary>
        /// Copies the value.
        /// </summary>
        /// <param name="builder">
        /// The builder.
        /// </param>
        /// <param name="connectionEntity">
        /// The connection entity.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="parameterType">
        /// Type of the parameter.
        /// </param>
        /// <param name="subType">
        /// Type of the sub.
        /// </param>
        /// <returns>
        /// The <see cref="IConnectionParameterEntity"/>.
        /// </returns>
        private static IConnectionParameterEntity CopyValue(
            DbConnectionStringBuilder builder,
            IConnectionEntity connectionEntity,
            string key,
            ParameterType parameterType = ParameterType.String,
            string subType = null)
        {
            var value = GetValue(builder, key);
            var connectionParameterEntity = connectionEntity.AddSetting(key, parameterType, value);
            if (!string.IsNullOrWhiteSpace(subType))
            {
                connectionParameterEntity.AllowedSubTypes.Add(subType);
            }

            return connectionParameterEntity;
        }

        /// <summary>
        /// The get value.
        /// </summary>
        /// <param name="builder">
        /// The builder.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private static object GetValue(DbConnectionStringBuilder builder, string key)
        {
            object value;
            if (builder.TryGetValue(key, out value))
            {
                return value;
            }

            return null;
        }
    }
}