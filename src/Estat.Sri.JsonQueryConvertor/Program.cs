// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="EUROSTAT">
//   Date Created : 2018-07-26
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using DryIoc;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Factory;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.Plugin.Editor.QueryEditor;

namespace Estat.Sri.JsonQueryConvertor
{
    internal class Program
    {
        private static void Main(string[] args)
        {

            Thread.Sleep(TimeSpan.FromSeconds(30));

            if (args.Length == 0 || args[0].ToLower(CultureInfo.InvariantCulture).IndexOf("help", System.StringComparison.Ordinal) >= 0)
            {
                DisplayHelp();
                return;
            }
            var container =
                new Container(
                    rules =>
                        rules.With(FactoryMethod.ConstructorWithResolvableArguments).WithoutThrowOnRegisteringDisposableTransient());


            var argumentsValues = GetArgValues(args);
            if (argumentsValues == null)
            {
                DisplayHelp();
                return;
            }
            var datasetEntityStoreId = argumentsValues.Item1;
            var entityName = argumentsValues.Item2;


            container.Register<IRetrievalEngineContainerFactory, RetrievalEngineContainerFactory>();
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["RetrieverFactory"]);
            var configurationStoreManager = new ConfigurationStoreManager(new AppConfigStoreFactory());
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);
            var entityPersistenceEngine = entityPeristenceFactory.GetEngine<Entity>(datasetEntityStoreId);
            IEntityRetrieverManager entityRetrieverManager = new EntityRetrieverManager(new DatasetRetrieverFactory(new DatabaseManager(configurationStoreManager)));

            var entityRetrieverEngine = entityRetrieverManager.GetRetrieverEngine<DatasetEntity>(datasetEntityStoreId);
            var datasetEntities =
                entityRetrieverEngine.GetEntities(
                    new EntityQuery(),
                    Detail.Full).ToList();

            var queryReaderWriter = new QueryMetadataReaderWriterBase();
            var sb = new StringBuilder();
            var nrOfParsedDatasets = 0;
            foreach (var datasetEntity in datasetEntities)
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(entityName))
                    {
                        if (datasetEntity.Name != entityName)
                        {
                            continue;
                        }
                    }
                    datasetEntity.StoreId = datasetEntityStoreId;
                    var queryMetadata = queryReaderWriter.Read(datasetEntity.JSONQuery);
                    var generatedJson = queryReaderWriter.Write(queryMetadata);
                    var patch = new PatchRequest
                    {
                        new PatchDocument("replace", "JSONQuery", generatedJson)
                    };

                    entityPersistenceEngine.Update(patch, EntityType.DataSet, datasetEntity.EntityId);
                    nrOfParsedDatasets++;
                }
                catch (Exception)
                {
                    sb.Append(datasetEntity.Name + ",");
                }
            }

            Console.WriteLine("Converted " + nrOfParsedDatasets + " datasets to JSON format");
            if (!string.IsNullOrWhiteSpace(sb.ToString()))
            {
                Console.WriteLine(@"Failed to convert datasets : " + sb.ToString().TrimEnd(','));
            }
            
            Console.ReadLine();
        }

        private static Tuple<string,string> GetArgValues(string[] args)
        {
            var dbserverValue = string.Empty;
            var datasetName = string.Empty;
            try
            {
                var dbServerArgument = args.FirstOrDefault(item => item.ToLower(CultureInfo.InvariantCulture).IndexOf("dbserver", System.StringComparison.Ordinal) > 0);
                if (!string.IsNullOrEmpty(dbServerArgument))
                {
                    var indexOf = Array.IndexOf(args, dbServerArgument);
                    dbserverValue = args[indexOf + 1];
                }
                
                var datasetNameArgument = args.FirstOrDefault(item => item.ToLower(CultureInfo.InvariantCulture).IndexOf("datasetname", System.StringComparison.Ordinal) > 0);
                if (!string.IsNullOrEmpty(datasetNameArgument))
                {
                    var indexOf = Array.IndexOf(args, datasetNameArgument);
                    datasetName = args[indexOf + 1];
                }
                
            }
            catch (Exception)
            {
                DisplayHelp();
                return null;
            }

            return new Tuple<string, string>(dbserverValue,datasetName);
        }

        private static void DisplayHelp()
        {
            Console.WriteLine("Available parameters");
            Console.WriteLine("-dbserver" + "\t" + "Id of the database server");
            Console.WriteLine("-datasetName" + "\t" + "Name of the dataset that you want to convert to JSON.\n\t\tIf not defined all datasets will be converted");
        }
    }
}