﻿// -----------------------------------------------------------------------
// <copyright file="CommonTargetObject.cs" company="EUROSTAT">
//   Date Created : 2017-04-14
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Model
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// A common model for <see cref="IConstraintContentTarget"/>, <see cref="IDataSetTarget"/>, <see cref="IIdentifiableTarget"/>, <see cref="IKeyDescriptorValuesTarget"/> and <see cref="IReportPeriodTarget"/>
    /// </summary>
    public class CommonTargetObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommonTargetObject"/> class.
        /// </summary>
        /// <param name="identifiable">The identifiable.</param>
        /// <exception cref="System.ArgumentNullException">identifiable is null</exception>
        public CommonTargetObject(IIdentifiableObject identifiable)
        {
            if (identifiable == null)
            {
                throw new ArgumentNullException("identifiable");
            }

            this.Id = identifiable.Id;
            this.Annotations = identifiable.Annotations;
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public string Id { get; private set; }

        /// <summary>
        /// Gets or sets the text format.
        /// </summary>
        /// <value>
        /// The text format.
        /// </value>
        public ITextFormat TextFormat { get; set; }

        /// <summary>
        /// Gets or sets the type of the object.
        /// </summary>
        /// <value>
        /// The type of the object.
        /// </value>
        public string ObjectType { get; set; }

        /// <summary>
        /// Gets or sets the item scheme.
        /// </summary>
        /// <value>
        /// The item scheme.
        /// </value>
        public IStructureReference ItemScheme { get; set; }

        /// <summary>
        /// Gets the annotations.
        /// </summary>
        /// <value>The annotations.</value>
        public IList<IAnnotation> Annotations { get; private set; }
    }
}
