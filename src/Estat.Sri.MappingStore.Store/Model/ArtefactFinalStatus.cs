// -----------------------------------------------------------------------
// <copyright file="ArtefactFinalStatus.cs" company="EUROSTAT">
//   Date Created : 2013-04-05
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using Org.Sdmxsource.Util;

namespace Estat.Sri.MappingStore.Store.Model
{
    /// <summary>
    ///     The artefact final status.
    /// </summary>
    public class ArtefactFinalStatus
    {
        /// <summary>
        ///     The _empty <see cref="ArtefactFinalStatus" />
        /// </summary>
        private static readonly ArtefactFinalStatus _empty;

        /// <summary>
        ///     The version extension (if any)
        /// </summary>
        private readonly string _versionExtension;

        /// <summary>
        ///     The _is final.
        /// </summary>
        private readonly bool _isFinal;

        /// <summary>
        ///     The _primary key.
        /// </summary>
        private readonly long _primaryKey;

        private readonly long _artefactBaseKey;

        /// <summary>
        ///     Initializes static members of the <see cref="ArtefactFinalStatus" /> class.
        /// </summary>
        static ArtefactFinalStatus()
        {
            _empty = new ArtefactFinalStatus(-1, null, -1);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ArtefactFinalStatus" /> class.
        /// </summary>
        /// <param name="primaryKey">
        ///     The primary Key.
        /// </param>
        /// <param name="versionExtension">
        ///     The version extension.
        /// </param>
        /// <param name="artefactBaseKey">the ARTEFACT_BASE ART_BASE_ID</param>
        public ArtefactFinalStatus(long primaryKey, string versionExtension, long artefactBaseKey)
        {
            this._primaryKey = primaryKey;
            _versionExtension = versionExtension;
            this._isFinal = !ObjectUtil.ValidString(versionExtension);
            _artefactBaseKey = artefactBaseKey;
        }

        /// <summary>
        ///     Gets the empty <see cref="ArtefactFinalStatus" />.
        /// </summary>
        public static ArtefactFinalStatus Empty
        {
            get
            {
                return _empty;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether it is empty.
        /// </summary>
        /// <value>
        ///     <c>true</c> if [is empty]; otherwise, <c>false</c>.
        /// </value>
        public bool IsEmpty
        {
            get
            {
                return ReferenceEquals(_empty, this);
            }
        }

        /// <summary>
        ///     Gets the version extension (if any)
        /// </summary>
        public string VersionExtension => _versionExtension;

        /// <summary>
        ///     Gets a value indicating whether is final.
        /// </summary>
        public bool IsFinal
        {
            get
            {
                return this._isFinal;
            }
        }

        /// <summary>
        ///     Gets the primary key.
        /// </summary>
        public long PrimaryKey
        {
            get
            {
                return this._primaryKey;
            }
        }

        /// <summary>
        ///     Gets the ARTEFACT_BASE primary key
        /// </summary>
        public long ArtefactBaseKey => _artefactBaseKey;
    }
}