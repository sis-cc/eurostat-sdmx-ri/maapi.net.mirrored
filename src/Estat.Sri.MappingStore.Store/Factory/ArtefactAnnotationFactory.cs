﻿// -----------------------------------------------------------------------
// <copyright file="ArtefactAnnotationFactory.cs" company="EUROSTAT">
//   Date Created : 2017-11-09
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Factory
{
    using Estat.Sri.MappingStore.Store.Engine;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The artefact annotation factory.
    /// </summary>
    public class ArtefactAnnotationFactory : IAnnotationInsertFactory
    {
        /// <summary>
        /// The insert artefact annotation
        /// </summary>
        private readonly InsertArtefactAnnotation _insertArtefactAnnotation;

        /// <summary>
        /// The artefact annotation insert engine
        /// </summary>
        private readonly ArtefactAnnotationInsertEngine _artefactAnnotationInsertEngine;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArtefactAnnotationFactory"/> class.
        /// </summary>
        public ArtefactAnnotationFactory()
        {
            _insertArtefactAnnotation = new InsertArtefactAnnotation();
            _artefactAnnotationInsertEngine = new ArtefactAnnotationInsertEngine(new AnnotationInsertEngine());
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="structureType">
        /// Type of the structure.
        /// </param>
        /// <returns>
        /// The <see cref="IAnnotationInsertEngine"/>.
        /// </returns>
        public IAnnotationInsertEngine GetEngine(SdmxStructureType structureType)
        {
            if (structureType.HasArtefactTableAsParent())
            {
                return _artefactAnnotationInsertEngine;
            }

            return null;
        }

        /// <summary>
        /// Gets the procedure.
        /// </summary>
        /// <param name="structureType">The structure type.</param>
        /// <returns>
        /// The <see cref="AnnotationProcedureBase" />.
        /// </returns>
        public AnnotationProcedureBase GetProcedure(SdmxStructureType structureType)
        {
            if (structureType.HasArtefactTableAsParent())
            {
                return _insertArtefactAnnotation;
            }

            return null;
        }
    }
}