﻿// -----------------------------------------------------------------------
// <copyright file="ComponentAnnotationFactory.cs" company="EUROSTAT">
//   Date Created : 2017-11-09
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Factory
{
    using Estat.Sri.MappingStore.Store.Engine;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The component annotation factory.
    /// </summary>
    public class ComponentAnnotationFactory : IAnnotationInsertFactory
    {
        /// <summary>
        /// The annotation insert engine
        /// </summary>
        private readonly AnnotationInsertEngine _annotationInsertEngine;

        /// <summary>
        /// The component annotation insert engine
        /// </summary>
        private readonly ComponentAnnotationInsertEngine _componentAnnotationInsertEngine;

        /// <summary>
        /// The insert component annotation
        /// </summary>
        private readonly InsertComponentAnnotation _insertComponentAnnotation;

        /// <summary>
        /// The insert group annotation
        /// </summary>
        private readonly InsertGroupAnnotation _insertGroupAnnotation;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentAnnotationFactory"/> class.
        /// </summary>
        public ComponentAnnotationFactory()
        {
            _annotationInsertEngine = new AnnotationInsertEngine();
            _componentAnnotationInsertEngine = new ComponentAnnotationInsertEngine(_annotationInsertEngine);
            _insertComponentAnnotation = new InsertComponentAnnotation();
            _insertGroupAnnotation = new InsertGroupAnnotation();
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="structureType">
        /// Type of the structure.
        /// </param>
        /// <returns>
        /// The <see cref="IAnnotationInsertEngine"/>.
        /// </returns>
        public IAnnotationInsertEngine GetEngine(SdmxStructureType structureType)
        {
            if (structureType.HasComponentCommonTableAsParent())
            {
                return _componentAnnotationInsertEngine;
            }

            if (structureType.EnumType == SdmxStructureEnumType.Group)
            {
                return _annotationInsertEngine;
            }

            return null;
        }

        /// <summary>
        /// Gets the procedure.
        /// </summary>
        /// <param name="structureType">The structure type.</param>
        /// <returns>
        /// The <see cref="AnnotationProcedureBase" />.
        /// </returns>
        public AnnotationProcedureBase GetProcedure(SdmxStructureType structureType)
        {
            if (structureType.HasComponentCommonTableAsParent())
            {
                return _insertComponentAnnotation;
            }

            if (structureType.EnumType == SdmxStructureEnumType.Group)
            {
                return _insertGroupAnnotation;
            }

            return null;
        }
    }
}