﻿// -----------------------------------------------------------------------
// <copyright file="StructureSubmitMappingStoreFactory.cs" company="EUROSTAT">
//   Date Created : 2017-11-17
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Engine;
using Estat.Sdmxsource.Extension.Factory;
using Estat.Sri.MappingStore.Store.Engine;
using System;
using System.Configuration;

namespace Estat.Sri.MappingStore.Store.Factory
{
    /// <summary>
    /// The factory for creating a <see cref="IStructureSubmitterEngine"/> implementation for Mapping Store DB
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Factory.IStructureSubmitFactory" />
    public class StructureSubmitMappingStoreFactory : IStructureSubmitFactory
    {
        /// <summary>
        /// The connection string builder
        /// </summary>
        private readonly Func<string, ConnectionStringSettings> _connectionStringBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureSubmitMappingStoreFactory"/> class.
        /// </summary>
        /// <param name="connectionStringBuilder">The connection string builder.</param>
        /// <exception cref="ArgumentNullException">connectionStringBuilder</exception>
        public StructureSubmitMappingStoreFactory(Func<string, ConnectionStringSettings> connectionStringBuilder)
        {
            if (connectionStringBuilder == null)
            {
                throw new ArgumentNullException(nameof(connectionStringBuilder));
            }

            _connectionStringBuilder = connectionStringBuilder;
        }

        /// <summary>
        /// Gets the engine for the specific store identified by <paramref name="storeId" />.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sdmxsource.Extension.Engine.IStructureSubmitterEngine" /> if <paramref name="storeId" /> is supported; otherwise null.
        /// </returns>
        public IStructureSubmitterEngine GetEngine(string storeId)
        {
            // we shouldn't thow an exception here, just return null if we cannot handle it
            if (storeId == null)
            {
                return null;
            }

            var connectionStringSetting = _connectionStringBuilder(storeId);
            if (connectionStringSetting == null)
            {
                return null;
            }

            return new StructureSubmitterMappingStore(connectionStringSetting);
        }
    }
}
