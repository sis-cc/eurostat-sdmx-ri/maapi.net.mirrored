// -----------------------------------------------------------------------
// <copyright file="ArtefactImportStatusExtension.cs" company="EUROSTAT">
//   Date Created : 2017-11-17
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Constant;
using Estat.Sdmxsource.Extension.Model.Error;
using Estat.Sri.MappingStore.Store.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.Sdmx.MappingStore.Store.Helper;
using Estat.Sri.Sdmx.MappingStore.Store.Properties;

namespace Estat.Sri.MappingStore.Store.Extension
{
    /// <summary>
    /// This class contains extension methods related to <see cref="ArtefactImportStatus"/>
    /// </summary>
    public static class ArtefactImportStatusExtension
    {
        /// <summary>
        /// Converts the specified <paramref name="importStatus"/> to <see cref="IResponseWithStatusObject"/>.
        /// </summary>
        /// <param name="importStatus">The import status.</param>
        /// <param name="action">The action.</param>
        /// <returns>The  <see cref="IResponseWithStatusObject"/>.</returns>
        public static IResponseWithStatusObject Convert(this IImportMessage importStatus, DatasetAction action)
        {
            var message = importStatus.Message;
            var status = importStatus.Status;
            var textList = new List<ITextTypeWrapper>();
            var messageList = new List<IMessageObject>();

            if (status == ImportMessageStatus.Success)
            {
                message = CreateSubmitStructureMessage( string.Empty, importStatus.ActualAction.ToActionString(), importStatus.StructureReference.GetAsHumanReadableString(), message, string.Empty);
                // start conversion
                var text = new TextTypeWrapperImpl("en", message, null);
                
                var messageObject = new MessageObject(new ITextTypeWrapper[] { text },  importStatus.ActualAction.ToSuccessCode());
                messageList.Add(messageObject);
            }
            else
            {
                var exception = importStatus.Exception;
                SdmxErrorCode sdmxErrorCode = null;
                if (exception != null)
                {
                    if (exception is SdmxException sdmxException)
                    {
                        sdmxErrorCode = sdmxException.SdmxErrorCode;
                        var conflictException = sdmxException as SdmxConflictException;
                        if (conflictException?.StructureReference != null)
                        {
                            message = conflictException.StructureReference.Aggregate(message, (current, structureReference) => current + (Environment.NewLine + "- " + structureReference.GetAsHumanReadableString()));
                        }
                    }
                    else
                    {
                        // TODO there is somewhere a method that converts normal exceptions to the corresponding SdmxException
                        sdmxErrorCode = SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError);
                    }
                }
                else if (status == ImportMessageStatus.Warning)
                {
                    sdmxErrorCode = SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError);
                }

                if (sdmxErrorCode == null)
                {
                    sdmxErrorCode = SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError);
                }

                // start conversion
                var text = new TextTypeWrapperImpl("en", message, null);
                var messageObject = new MessageObject(new ITextTypeWrapper[] { text }, sdmxErrorCode);
                messageList.Add(messageObject);
            }

            return new ResponseWithStatusObject(messageList, status.AsResponseStatus(), importStatus.StructureReference, action);
        }

        private static string CreateSubmitStructureMessage(string negation, string actionString, string structureString, string reason, string reasonString)
        {
            return Regex.Replace(string.Format(Resources.SubmitStructureMessage, negation,
                actionString, structureString,
                reason, reasonString), @"\s+", " ").Trim(' ').TrimEnd(':');
        }

        private static string GetActionString(DatasetAction action)
        {
            switch (action.EnumType)
            {
                case DatasetActionEnumType.Append:
                    return "Inserted";
                case DatasetActionEnumType.Delete:
                    return "Deleted";
                case DatasetActionEnumType.Replace:
                    return "Updated";
            }

            return String.Empty;
        }

        /// <summary>
        /// To the success code.
        /// </summary>
        /// <param name="datasetAction">The dataset action.</param>
        /// <returns>The <see cref="System.Net.HttpStatusCode"/>.</returns>
        public static HttpStatusCode ToSuccessCode(this DatasetAction datasetAction)
        {
            if (datasetAction != null)
            {
                switch (datasetAction.EnumType)
                {
                    case DatasetActionEnumType.Append:
                        return HttpStatusCode.Created;
                    case DatasetActionEnumType.Delete:
                    case DatasetActionEnumType.Replace:
                        return HttpStatusCode.OK;
                }
            }

            return HttpStatusCode.OK;
        }

        /// <summary>
        /// Gets the correpsonding <see cref="ResponseStatus"/>.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>The <see cref="ResponseStatus"/></returns>
        /// <exception cref="ArgumentOutOfRangeException">Invalid status</exception>
        public static ResponseStatus AsResponseStatus(this ImportMessageStatus status)
        {
            switch(status)
            {
                case ImportMessageStatus.Error:
                    return ResponseStatus.Failure;
                case ImportMessageStatus.Success:
                    return ResponseStatus.Success;
                case ImportMessageStatus.Warning:
                    return ResponseStatus.Warning;
                default:
                    throw new ArgumentOutOfRangeException(nameof(status), "Invalid status");
            }
        }

        /// <summary>
        /// Builds the updated status.
        /// </summary>
        /// <param name="maintainableObject">The maintainable object.</param>
        /// <param name="finalStatus">The final status.</param>
        /// <param name="status">The status.</param>
        /// <param name="updatedRecords">The updated records.</param>
        /// <returns>
        /// The <see cref="ArtefactImportStatus" />.
        /// </returns>
        public static ArtefactImportStatus BuildUpdatedStatus(
            this IMaintainableObject maintainableObject,
            ArtefactFinalStatus finalStatus,
            ArtefactImportStatus status,
            int updatedRecords)
        {
            //updatedRecords is the number of NonFinalAttributes and Identifiable composites updated
            //status is null if no operation was made on the artefact table specific
            var structureReference = maintainableObject.AsReference;
            ImportMessage importMessage = null;
            if (status == null)
            {
                if (updatedRecords == 0)
                {
                    importMessage = new ImportMessage(
                        ImportMessageStatus.Warning,
                        structureReference,
                        CreateSubmitStructureMessage(
                            "Not",
                            "Updated",
                            structureReference.GetAsHumanReadableString(),
                            "Reason",
                            "no differences were found in these artefact properties: " + GetArtefactPropertiesMessage(finalStatus.IsFinal)));
                }
                else
                {
                    importMessage = new ImportMessage(
                        ImportMessageStatus.Success,
                        structureReference,
                        "Updated differences found in any of these artefact properties: " + GetArtefactPropertiesMessage(finalStatus.IsFinal),
                        DatasetActionEnumType.Replace);
                }
            }
            else
            {
                switch (status.ImportMessage.Status)
                {
                    case ImportMessageStatus.Success:
                        var message = updatedRecords == 0 ? string.Empty : "Updated differences found in any of these artefact properties: " + GetArtefactPropertiesMessage(finalStatus.IsFinal);
                        importMessage = new ImportMessage(
                            ImportMessageStatus.Success,
                            structureReference,
                            message,
                            DatasetActionEnumType.Replace);
                        break;
                    case ImportMessageStatus.Warning:
                        if (updatedRecords > 0)
                        {
                            importMessage = new ImportMessage(
                                status.ImportMessage.Status,
                                structureReference,
                                CreateSubmitStructureMessage(string.Empty, "Updated",structureReference.GetAsHumanReadableString(), "Warning", status.ImportMessage.Message),
                                DatasetActionEnumType.Replace);
                        }
                        else
                        {
                            importMessage = new ImportMessage(
                                status.ImportMessage.Status,
                                structureReference,
                                CreateSubmitStructureMessage("Not", "Updated", structureReference.GetAsHumanReadableString(), "Warning", status.ImportMessage.Message)
                                );
                        }

                        break;
                    case ImportMessageStatus.Error:
                        if (updatedRecords > 0)
                        {
                            importMessage = new ImportMessage(
                                status.ImportMessage.Status,
                                structureReference,
                                CreateSubmitStructureMessage(string.Empty, "Updated", structureReference.GetAsHumanReadableString(), "Error", status.ImportMessage.Message),
                                status.ImportMessage.Exception);
                        }
                        else
                        {
                            importMessage = new ImportMessage(
                                status.ImportMessage.Status,
                                structureReference,
                                CreateSubmitStructureMessage("Not", "Updated", structureReference.GetAsHumanReadableString(), "Error", status.ImportMessage.Message),
                                status.ImportMessage.Exception);
                        }

                        break;
                }

            }
            return new ArtefactImportStatus(finalStatus.PrimaryKey, importMessage);
        }

        private static string GetArtefactPropertiesMessage(bool isFinal)
        {
            return (ConfigManager.Config.InsertNewItems || !isFinal ? "new items, " : string.Empty) +
                   (!isFinal ? "non-referenced removed items, " : string.Empty) +
                   "names, descriptions, annotations, validity, uri." +
                   (isFinal ? " Other content changes are not allowed for final artefacts." : string.Empty);
        }
    }
}
