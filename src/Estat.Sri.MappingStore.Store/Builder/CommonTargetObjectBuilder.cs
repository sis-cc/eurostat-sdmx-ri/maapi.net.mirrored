// -----------------------------------------------------------------------
// <copyright file="CommonTargetObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-04-14
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Builder
{
    using Estat.Sri.MappingStore.Store.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    /// The <see cref="CommonTargetObject"/> builder from <see cref="IConstraintContentTarget"/>, <see cref="IDataSetTarget"/>, <see cref="IIdentifiableTarget"/>, <see cref="IKeyDescriptorValuesTarget"/> and <see cref="IReportPeriodTarget"/>.
    /// </summary>
    public class CommonTargetObjectBuilder : ICommonTargetObjectBuilder<IConstraintContentTarget>, ICommonTargetObjectBuilder<IDataSetTarget>, ICommonTargetObjectBuilder<IIdentifiableTarget>, ICommonTargetObjectBuilder<IKeyDescriptorValuesTarget>, ICommonTargetObjectBuilder<IReportPeriodTarget>
    {
        /// <summary>
        /// Builds an object of type <see cref="CommonTargetObject"/>
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from
        ///             </param>
        /// <returns>
        /// Object of type <see cref="CommonTargetObject"/>
        /// </returns>
        public CommonTargetObject Build(IConstraintContentTarget buildFrom)
        {
            ITextFormatMutableObject textFormat = new TextFormatMutableCore();
            textFormat.TextType = buildFrom.TextType;
            return BuildCommonTargetObject(buildFrom, textFormat);
        }

        /// <summary>
        /// Builds an object of type <see cref="CommonTargetObject"/>
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from
        ///             </param>
        /// <returns>
        /// Object of type <see cref="CommonTargetObject"/>
        /// </returns>
        public CommonTargetObject Build(IDataSetTarget buildFrom)
        {
            ITextFormatMutableObject textFormat = new TextFormatMutableCore();
            textFormat.TextType = buildFrom.TextType;
            return BuildCommonTargetObject(buildFrom, textFormat);
        }

        /// <summary>
        /// Builds an object of type <see cref="CommonTargetObject"/>
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from
        ///             </param>
        /// <returns>
        /// Object of type <see cref="CommonTargetObject"/>
        /// </returns>
        public CommonTargetObject Build(IIdentifiableTarget buildFrom)
        {
            ITextFormat textFormat = null;
            IStructureReference structureReference = null;
            if (buildFrom.Representation != null)
            {
                structureReference = buildFrom.Representation.Representation;
                textFormat = buildFrom.Representation.TextFormat;
            }

            return new CommonTargetObject(buildFrom) { ObjectType = GetObjectType(buildFrom), TextFormat = textFormat, ItemScheme = structureReference };
        }

        /// <summary>
        /// Builds an object of type <see cref="CommonTargetObject"/>
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from
        ///             </param>
        /// <returns>
        /// Object of type <see cref="CommonTargetObject"/>
        /// </returns>
        public CommonTargetObject Build(IKeyDescriptorValuesTarget buildFrom)
        {
            var textType = buildFrom.TextType;
            ITextFormatMutableObject textFormat = new TextFormatMutableCore();
            textFormat.TextType = textType;
            return BuildCommonTargetObject(buildFrom, textFormat);
        }

        /// <summary>
        /// Builds an object of type <see cref="CommonTargetObject"/>
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from
        ///             </param>
        /// <returns>
        /// Object of type <see cref="CommonTargetObject"/>
        /// </returns>
        public CommonTargetObject Build(IReportPeriodTarget buildFrom)
        {
            ITextFormatMutableObject textFormat = new TextFormatMutableCore();
            textFormat.TextType = buildFrom.TextType;
            textFormat.StartTime = buildFrom.StartTime;
            textFormat.EndTime = buildFrom.EndTime;
            return BuildCommonTargetObject(buildFrom, textFormat);
        }

        /// <summary>
        /// Builds the common target object.
        /// </summary>
        /// <param name="buildFrom">The target object.</param>
        /// <param name="textFormat">The text format.</param>
        /// <returns>The <see cref="CommonTargetObject"/></returns>
        private static CommonTargetObject BuildCommonTargetObject(IIdentifiableObject buildFrom, ITextFormatMutableObject textFormat)
        {
            // The ID is fixed
            return new CommonTargetObject(buildFrom) { TextFormat = new TextFormatObjectCore(textFormat, buildFrom), ObjectType = GetObjectType(buildFrom) };
        }

        /// <summary>
        /// Gets the type of the object.
        /// </summary>
        /// <param name="identifiableTarget">The identifiable target.</param>
        /// <returns>The object type</returns>
        private static string GetObjectType(IIdentifiableTarget identifiableTarget)
        {
            return identifiableTarget.ReferencedStructureType.UrnClass;
        }

        /// <summary>
        /// Gets the type of the object.
        /// </summary>
        /// <param name="identifiableObject">The identifiable object.</param>
        /// <returns>The object type</returns>
        private static string GetObjectType(IIdentifiableObject identifiableObject)
        {
            return identifiableObject.StructureType.UrnClass;
        }
    }
}