using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Engine;
using Estat.Sri.MappingStoreRetrieval.Manager;
using log4net;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Util.Objects;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class ItemSchemeItemsComparisonEngine<T, TItem> where T : IItemSchemeObject<TItem> where TItem : IItemObject
    {

        private static readonly ILog _log = LogManager.GetLogger(typeof(ItemSchemeItemsComparisonEngine<T, TItem>));

        private Database _database;

        public ItemSchemeItemsComparisonEngine(Database database)
        {
            this._database = database;
        }

        public ItemsComparisonResult<TItem> CompareToDb(T itemScheme, ArtefactFinalStatus finalStatus)
        {
            if (itemScheme == null)
            {
                throw new ArgumentNullException(nameof(itemScheme));
            }

            if (finalStatus == null)
            {
                throw new ArgumentException(nameof(finalStatus));
            }
            var dbItemsMap = new Dictionary<string, List<DatabaseItem>>();
            var itemStructureType = itemScheme.GetChildItemStructureType();

            //TODO check if we can refactor category scheme scenario to use IdentifiableMapRetrieverEngine after implementation of task https://jira.intrasoft-intl.com/browse/SDMX-617
            if (itemScheme.StructureType.EnumType == SdmxStructureEnumType.CategoryScheme)
            {
                MaintainableRefRetrieverEngine maintainableRefRetrieverEngine = new MaintainableRefRetrieverEngine(_database);
                var dbSystemIdToItemUrnMap = maintainableRefRetrieverEngine.RetrievesUrnMapForSpecificArtefact(itemStructureType, finalStatus.PrimaryKey);
                dbItemsMap = GetItemsByIdFromUrnMap(dbSystemIdToItemUrnMap);
                var requestProvideditemsMap = GetItemsByIdFromRequest(itemScheme, itemStructureType);

                //items that are only in the request and not in database
                List<TItem> itemsNotInDb = GetItemsNotInDatabase(dbItemsMap, requestProvideditemsMap);

                //items that are only in the database and not in the request
                List<DatabaseItem> itemsNotInRequest = GetItemsNotInRequest(dbItemsMap, requestProvideditemsMap);

                //Get the rest items that are both in the request and in the database
                List<ItemBothInRequestAndInDb<TItem>> itemsBothInRequestAndInDb = GetItemsBothInRequestAndInDb(dbItemsMap, requestProvideditemsMap);

                return new ItemsComparisonResult<TItem>(itemsNotInDb, itemsNotInRequest, itemsBothInRequestAndInDb, dbSystemIdToItemUrnMap);
            }
            else
            {
                var identifiableMapRetrieverEngine = new IdentifiableMapRetrieverEngine("ID", itemStructureType);
                IdentifiableMap identifiableMap = identifiableMapRetrieverEngine.Retrieve(_database,finalStatus.PrimaryKey, null, itemStructureType);
                dbItemsMap = GetItemsByIdFromIdMap(identifiableMap.Dictionary);
                var requestProvideditemsMap = GetItemsByIdFromRequest(itemScheme, itemStructureType);

                //items that are only in the request and not in database
                List<TItem> itemsNotInDb = GetItemsNotInDatabase(dbItemsMap, requestProvideditemsMap);

                //items that are only in the database and not in the request
                List<DatabaseItem> itemsNotInRequest = GetItemsNotInRequest(dbItemsMap, requestProvideditemsMap);

                //Get the rest items that are both in the request and in the database
                List<ItemBothInRequestAndInDb<TItem>> itemsBothInRequestAndInDb = GetItemsBothInRequestAndInDb(dbItemsMap, requestProvideditemsMap);

                return new ItemsComparisonResult<TItem>(itemsNotInDb, itemsNotInRequest, itemsBothInRequestAndInDb, null);
            }
        }

        private Dictionary<string, List<TItem>> GetItemsByIdFromRequest(T itemScheme, SdmxStructureEnumType itemStructureType)
        {

           var idToitemsMap = new Dictionary<string, List<TItem>>();
            foreach (var identifiable in itemScheme.IdentifiableComposites)
            {
                if (identifiable.StructureType.EnumType == itemStructureType)
                {
                    TItem ItemBean = (TItem)identifiable;
                    String id = ItemBean.Id;
                    idToitemsMap.TryGetValue(id,out var items);
                    if (items == null)
                    {
                        items = new List<TItem>();
                        idToitemsMap.Add(id, items);
                    }
                    items.Add(ItemBean);
                }
            }

            return idToitemsMap;
        }

        private Dictionary<string, List<DatabaseItem>> GetItemsByIdFromIdMap(Dictionary<string, long> dbSystemIdToItemUrnMap)
        {

            var idToItemsMap = new Dictionary<String, List<DatabaseItem>>();
            foreach (var systemIdToItemEntry in dbSystemIdToItemUrnMap)
            {

                long systemId = systemIdToItemEntry.Value;
                //String urn = systemIdToItemEntry.getValue();
                string id = systemIdToItemEntry.Key;
                DatabaseItem databaseItem = new DatabaseItem(id, systemId, null);

                idToItemsMap.TryGetValue(id,out var items);
                if (items == null)
                {
                    items = new List<DatabaseItem>();
                    idToItemsMap.Add(id, items);
                }
                items.Add(databaseItem);
            }

            return idToItemsMap;
        }

        private Dictionary<string, List<DatabaseItem>> GetItemsByIdFromUrnMap(IDictionary<long, string> dbSystemIdToItemUrnMap)
        {
            var idToItemsMap = new Dictionary<string, List<DatabaseItem>>();
            foreach (var systemIdToItemEntry in dbSystemIdToItemUrnMap)
            {

                long systemId = systemIdToItemEntry.Key;
                string urn = systemIdToItemEntry.Value;
                string id = GetIdFromUrn(urn);
                DatabaseItem databaseItem = new DatabaseItem(id, systemId, urn);

                idToItemsMap.TryGetValue(id,out var items);
                if (items == null)
                {
                    items = new List<DatabaseItem>();
                    idToItemsMap.Add(id, items);
                }
                items.Add(databaseItem);
            }

            return idToItemsMap;
        }



        /**
         * Gets items with ids that are included in the request and are not included in database
         *
         * @param dbitemsMap
         * @param requestProvideditemsMap
         * @return
         */
        private List<TItem> GetItemsNotInDatabase(Dictionary<string, List<DatabaseItem>> dbitemsMap, Dictionary<string, List<TItem>> requestProvideditemsMap)
        {

            List<TItem> items = new List<TItem>();

            foreach (var itemEntry in requestProvideditemsMap)
            {
                if (!dbitemsMap.ContainsKey(itemEntry.Key))
                {
                    items.AddAll(itemEntry.Value);
                }
            }

            return items;
        }

        private List<DatabaseItem> GetItemsNotInRequest(Dictionary<string, List<DatabaseItem>> dbItemsMap, Dictionary<string, List<TItem>> requestProvideditemsMap)
        {

            List<DatabaseItem> dbitems = new List<DatabaseItem>();

            foreach (var dbItemEntry in dbItemsMap)
            {
                if (!requestProvideditemsMap.ContainsKey(dbItemEntry.Key))
                {
                    dbitems.AddAll(dbItemEntry.Value);
                }
            }

            return dbitems;
        }

        private List<ItemBothInRequestAndInDb<TItem>> GetItemsBothInRequestAndInDb(Dictionary<string, List<DatabaseItem>> dbItemsMap, Dictionary<string, List<TItem>> requestProvidedItemsMap)
        {
            List<ItemBothInRequestAndInDb<TItem>> commonItems = new List<ItemBothInRequestAndInDb<TItem>>();

            foreach (var item in requestProvidedItemsMap)
            {
                dbItemsMap.TryGetValue(item.Key,out var dbItems);
                if (dbItems != null)
                {
                    commonItems.Add(new ItemBothInRequestAndInDb<TItem>(item.Key, item.Value, dbItems));
                }
            }

            return commonItems;
        }

        private string GetIdFromUrn(string urn)
        {
            string[] ids = UrnUtil.GetUrnComponents(urn);
            //the last token of the urn is the id
            return ids[ids.Length - 1];
        }

    }
}
