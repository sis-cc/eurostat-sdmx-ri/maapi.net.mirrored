using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Engine;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    class IdentifiablesComparisonEngine<T> where T:IIdentifiableObject
    {

    private Database _database;

    private ArtefactFinalStatus _maintainableFinalStatus;

    private SdmxStructureEnumType _identifiableStructureType;

    public IdentifiablesComparisonEngine(Database database, ArtefactFinalStatus maintainableFinalStatus, SdmxStructureEnumType identifiableStructureType)
        {
            if(database == null)
            {
                throw new ArgumentNullException(nameof(database));
            }
            this._database = database;
            this._maintainableFinalStatus = maintainableFinalStatus;
            this._identifiableStructureType = identifiableStructureType;
        }

        public IdentifiableComparisonResult<T> CompareToDb(IList<T> objectsInRequest, IList<T> objectsInDatabase)
        {
            Dictionary<string, long> sysIds = GetSysIds();

            var identifiableBothInRequestAndInDbs = new List<IdentifiableBothInRequestAndInDb<T>>();
            var beansNotInRequest = new List<DatabaseIdentifiable<T>>();
            foreach (T identifiable in objectsInDatabase)
            {
                DatabaseIdentifiable<T> databaseIdentifiable = new DatabaseIdentifiable<T> { Identifiable = identifiable, SysId = sysIds[identifiable.Id] };
                T matchingRequestBean = objectsInRequest.FirstOrDefault(x=>x.Id == identifiable.Id);
                if (matchingRequestBean == null)
                {
                    beansNotInRequest.Add(databaseIdentifiable);
                }
                else
                {
                    identifiableBothInRequestAndInDbs.Add(new IdentifiableBothInRequestAndInDb<T> { Id = identifiable.Id, Identifiable = matchingRequestBean, DatabaseIdentifiable = databaseIdentifiable });
                }
            }

            var beansNotInDb = new List<T>();
            foreach (T requestBean in objectsInRequest)
            {
                T matchingDbBean = objectsInDatabase.FirstOrDefault(x=>x.Id == requestBean.Id);
                if (matchingDbBean == null)
                {
                    beansNotInDb.Add(requestBean);
                }
            }

            return new IdentifiableComparisonResult<T> { IdentifiablesNotInDb = beansNotInDb, IdentifiablesNotInRequest = beansNotInRequest, IdentifiablesBothInRequestAndInDb = identifiableBothInRequestAndInDbs, IdentifiableIdToSysId = sysIds };
        }

        private Dictionary<string, long> GetSysIds()
        {

            var identifiableMapRetriever = new IdentifiableMapRetrieverEngine("ID",
                    SdmxStructureType.GetFromEnum(_identifiableStructureType));

            IdentifiableMap identifiablesInDatabaseBySysIdMap = identifiableMapRetriever.Retrieve(_database,_maintainableFinalStatus.PrimaryKey, null, _identifiableStructureType);

            return identifiablesInDatabaseBySysIdMap.Dictionary;
        }
    }
}
