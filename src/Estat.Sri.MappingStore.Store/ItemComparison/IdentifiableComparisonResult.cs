using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    class IdentifiableComparisonResult<T> where T:IIdentifiableObject
    {
        public List<T> IdentifiablesNotInDb { get; set; }

        public List<DatabaseIdentifiable<T>> IdentifiablesNotInRequest { get; set; }

        public List<IdentifiableBothInRequestAndInDb<T>> IdentifiablesBothInRequestAndInDb
        { get; set; }
        public Dictionary<string, long> IdentifiableIdToSysId { get; set; }
    }
}
