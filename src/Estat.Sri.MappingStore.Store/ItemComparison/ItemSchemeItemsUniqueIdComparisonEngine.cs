using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    /// <summary>
    ///   <br />
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class ItemSchemeItemsUniqueIdComparisonEngine<T, TItem> where T : IItemSchemeObject<TItem> where TItem : IItemObject
    {

    private ItemSchemeItemsComparisonEngine<T, TItem> comparisonEngine;

    /// <summary>Initializes a new instance of the <see cref="ItemSchemeItemsUniqueIdComparisonEngine{T, TItem}" /> class.</summary>
    /// <param name="database">The database.</param>
    public ItemSchemeItemsUniqueIdComparisonEngine(Database database)
    {
        this.comparisonEngine = new ItemSchemeItemsComparisonEngine<T,TItem>(database);
    }

        /// <summary>Compares to database.</summary>
        /// <param name="itemSchemeBean">The item scheme bean.</param>
        /// <param name="finalStatus">The final status.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        public ItemsUniqueIdComparisonResult<TItem> CompareToDb(T itemSchemeBean, ArtefactFinalStatus finalStatus)
    {

        ItemsComparisonResult<TItem> comparisonResult = comparisonEngine.CompareToDb(itemSchemeBean, finalStatus);

        return new ItemsUniqueIdComparisonResult<TItem>(comparisonResult.ItemsNotInDb,
                comparisonResult.ItemsNotInRequest,
                GetItemsBothInRequestAndInDb(comparisonResult.ItemsBothInRequestAndInDb),
                comparisonResult.DbSystemIdToItemUrnMap);
    }

    private List<ItemUniqueIdBothInRequestAndInDb<TItem>> GetItemsBothInRequestAndInDb(List<ItemBothInRequestAndInDb<TItem>> commonItems)
    {
        List<ItemUniqueIdBothInRequestAndInDb<TItem>> results = new List<ItemUniqueIdBothInRequestAndInDb<TItem>>();
        foreach (ItemBothInRequestAndInDb<TItem> commonItem in commonItems)
        {
            results.Add(new ItemUniqueIdBothInRequestAndInDb<TItem>(commonItem.Id, commonItem.ItemsInRequest[0], commonItem.ItemsInDatabase[0]));
        }

        return results;
    }
}
}
