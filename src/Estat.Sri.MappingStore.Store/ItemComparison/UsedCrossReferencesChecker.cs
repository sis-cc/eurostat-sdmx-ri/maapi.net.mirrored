using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    class UsedCrossReferencesChecker
    {
        public static List<IStructureReference> GetUsedCrossreferences(IMaintainableObject maintainable, IList<IMaintainableMutableObject> crossReferencingObjects)
        {

            var structureReferences = new List<IStructureReference>();
            foreach (var crossReferencingObject in crossReferencingObjects)
            {
                var crossReferencingImmutableObject = crossReferencingObject.ImmutableInstance;
                var crossReferences = crossReferencingImmutableObject.CrossReferences;
                foreach (var crossReference in crossReferences)
                {
                    if (crossReference.MaintainableStructureEnumType == maintainable.StructureType && crossReference.HasChildReference())
                    {
                        if (crossReference.MaintainableReference
                                .Equals(maintainable.AsReference.MaintainableReference))
                        {
                            structureReferences.Add(crossReference);
                        }
                    }
                }

                if (crossReferencingObject.StructureType == SdmxStructureType.GetFromEnum(SdmxStructureEnumType.StructureSet))
                {
                    var dsd = (IDataStructureObject)maintainable;
                    var structureReference = (IStructureSetMutableObject)crossReferencingObject;
                    foreach (var structureMap in structureReference.StructureMapList)
                    {
                        if (structureMap.SourceRef.Equals(dsd.AsReference))
                        {
                            foreach (var components in structureMap.Components)
                            {
                                structureReferences.Add(dsd.GetComponent(components.MapConceptRef).AsReference);
                            }
                        }
                        else if (structureMap.TargetRef.Equals(dsd.AsReference))
                        {
                            foreach (var components in structureMap.Components)
                            {
                                structureReferences.Add(dsd.GetComponent(components.MapTargetConceptRef).AsReference);
                            }
                        }
                    }
                }
            }
            return structureReferences;
        }
    }
}
