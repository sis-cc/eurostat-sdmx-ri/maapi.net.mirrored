using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    public class DatabaseIdentifiable<T> where T: IIdentifiableObject
    {
        public T Identifiable { get; set; }
        public long SysId { get; set; }
    }
}
