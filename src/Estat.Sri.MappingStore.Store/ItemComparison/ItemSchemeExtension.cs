using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    /// <summary>
    ///   <br />
    /// </summary>
    public static class ItemSchemeExtension 
    {
        /// <summary>Gets the type of the child item structure.</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="itemScheme">The item scheme.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        /// <exception cref="SdmxException">could not find child structure type for structure type " + itemSchemeStructureType</exception>
        public static SdmxStructureType GetChildItemStructureType<T>(this IItemSchemeObject<T> itemScheme) where T: IItemObject
        {
            SdmxStructureType itemSchemeStructureType = itemScheme.StructureType;

            foreach (var structureType in SdmxStructureType.Values)
            {
                if (structureType.ParentStructureType == itemSchemeStructureType)
                {
                    return structureType;
                }
            }

            throw new SdmxException("could not find child structure type for structure type " + itemSchemeStructureType);
        }
    }
}
