using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    public class ItemsUniqueIdComparisonResult<TItem> : ItemsComparisonResultBase<TItem, List<ItemUniqueIdBothInRequestAndInDb<TItem>>> where TItem:IItemObject {

        public ItemsUniqueIdComparisonResult(List<TItem> itemsNotInDb, List<DatabaseItem> itemsNotInRequest, List<ItemUniqueIdBothInRequestAndInDb<TItem>> itemsBothInRequestAndInDb, IDictionary<long, string> dbSystemIdToItemUrnMap)
        : base(itemsNotInDb, itemsNotInRequest, itemsBothInRequestAndInDb, dbSystemIdToItemUrnMap)
        {
        }
    }
}
