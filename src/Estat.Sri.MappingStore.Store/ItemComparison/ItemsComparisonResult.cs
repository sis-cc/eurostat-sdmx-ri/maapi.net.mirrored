using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    public class ItemsComparisonResult<TItem> : ItemsComparisonResultBase<TItem, List<ItemBothInRequestAndInDb<TItem>>> where TItem: IItemObject {

        public ItemsComparisonResult(List<TItem> itemsNotInDb, List<DatabaseItem> itemsNotInRequest, List<ItemBothInRequestAndInDb<TItem>> itemsBothInRequestAndInDb, IDictionary<long, string> dbSystemIdToItemUrnMap)
        :base(itemsNotInDb, itemsNotInRequest, itemsBothInRequestAndInDb, dbSystemIdToItemUrnMap)
        {
        }
    }
}
