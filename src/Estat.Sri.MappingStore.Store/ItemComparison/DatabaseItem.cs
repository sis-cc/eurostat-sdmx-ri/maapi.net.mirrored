using System;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    /// <summary>
    /// Represents a database item
    /// </summary>
    public class DatabaseItem
    {
        /// <summary>Initializes a new instance of the <see cref="DatabaseItem" /> class.</summary>
        /// <param name="id">The identifier.</param>
        /// <param name="sysId">The system identifier.</param>
        /// <param name="urn">The urn.</param>
        public DatabaseItem(string id, long sysId, string urn)
        {
            this.Id = id;
            this.SysId = sysId;
            this.Urn = urn;
        }

        /// <summary>Gets or sets the identifier.</summary>
        /// <value>The identifier.</value>
        public string Id { get; set; }

        /// <summary>Gets or sets the system identifier.</summary>
        /// <value>The system identifier.</value>
        public long SysId { get; set; }

        /// <summary>Gets or sets the urn.</summary>
        /// <value>The urn.</value>
        public string Urn { get; set; }
    }
}
