using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    public class ItemsComparisonResultBase<TItem, TCommon> where TItem : IItemObject
    {

        //items that are only in the request and not in database
        private List<TItem> itemsNotInDb;

        //items that are only in the database and not in the request
        private List<DatabaseItem> itemsNotInRequest;

        //rest items that are both in the request and in the database

        private TCommon itemsBothInRequestAndInDb;

        //map with system ids of the items that are in database as keys and their urns as values

        private IDictionary<long, string> dbSystemIdToItemUrnMap;

        public ItemsComparisonResultBase(List<TItem> itemsNotInDb, List<DatabaseItem> itemsNotInRequest, TCommon itemsBothInRequestAndInDb, IDictionary<long, string> dbSystemIdToItemUrnMap)
        {
            this.ItemsNotInDb = itemsNotInDb;
            this.ItemsNotInRequest = itemsNotInRequest;
            this.ItemsBothInRequestAndInDb = itemsBothInRequestAndInDb;
            this.DbSystemIdToItemUrnMap = dbSystemIdToItemUrnMap;
        }

        public List<TItem> ItemsNotInDb { get; set; }

        public List<DatabaseItem> ItemsNotInRequest { get; set; }

        public TCommon ItemsBothInRequestAndInDb { get; set; }
       
        public IDictionary<long, string> DbSystemIdToItemUrnMap { get; set; }
    }
}
