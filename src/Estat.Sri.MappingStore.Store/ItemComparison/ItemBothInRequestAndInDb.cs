using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    /// <summary>
    /// Helper class for holding all items that are found with the same ID in the user request (stored in itemsInRequest) and those already existing in database with the same ID (stored in itemsInDatabase)
    ///   <br />
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class ItemBothInRequestAndInDb<TItem> where TItem : IItemObject
    {
        /// <summary>Initializes a new instance of the <see cref="ItemBothInRequestAndInDb{TItem}" /> class.</summary>
        /// <param name="id">The identifier.</param>
        /// <param name="itemsInRequest">The items in request.</param>
        /// <param name="itemsInDatabase">The items in database.</param>
        public ItemBothInRequestAndInDb(string id, List<TItem> itemsInRequest, List<DatabaseItem> itemsInDatabase)
        {
            this.Id = id;
            this.ItemsInRequest = itemsInRequest;
            this.ItemsInDatabase = itemsInDatabase;
        }

        /// <summary>Gets or sets the identifier.</summary>
        /// <value>The identifier.</value>
        public string Id { get; set; }

        /// <summary>Gets or sets the items in request.</summary>
        /// <value>The items in request.</value>
        public List<TItem> ItemsInRequest { get; set; }

        /// <summary>Gets or sets the items in database.</summary>
        /// <value>The items in database.</value>
        public List<DatabaseItem> ItemsInDatabase { get; set; }
    }
}
