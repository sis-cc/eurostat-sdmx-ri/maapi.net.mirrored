using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    public class ItemUniqueIdBothInRequestAndInDb<TItem> where TItem : IItemObject
    {

    public ItemUniqueIdBothInRequestAndInDb(string id, TItem itemInRequest, DatabaseItem itemInDatabase)
        {
            this.Id = id;
            this.ItemInRequest = itemInRequest;
            this.ItemInDatabase = itemInDatabase;
        }

        public string Id { get; set; }

        public TItem ItemInRequest { get; set; }

        public DatabaseItem ItemInDatabase { get; set; }

    }
}
