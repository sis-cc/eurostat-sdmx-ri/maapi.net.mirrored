using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Sdmx.MappingStore.Store.ItemComparison
{
    class IdentifiableBothInRequestAndInDb<T> where T:IIdentifiableObject
    {
        public string Id { get; set; }
        public T Identifiable { get; set; }
        public DatabaseIdentifiable<T> DatabaseIdentifiable { get; set; }
    }
}
