﻿// -----------------------------------------------------------------------
// <copyright file="AnnotationInsertManager.cs" company="EUROSTAT">
//   Date Created : 2017-11-09
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Manager
{
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// The annotation insert manager.
    /// </summary>
    internal class AnnotationInsertManager
    {
        /// <summary>
        /// The factories
        /// </summary>
        private readonly IList<IAnnotationInsertFactory> _factories;

        /// <summary>
        /// Initializes a new instance of the <see cref="AnnotationInsertManager"/> class.
        /// </summary>
        /// <param name="factories">The factories.</param>
        public AnnotationInsertManager(params IAnnotationInsertFactory[] factories)
        {
            _factories = factories.ToArray();
        }

        /// <summary>
        /// Inserts the specified structure type.
        /// </summary>
        /// <param name="structureType">Type of the structure.</param>
        /// <param name="database">The database.</param>
        /// <param name="annotatablePrimaryKey">The <see cref="IAnnotableObject"/>. primary key.</param>
        /// <param name="annotations">The annotations.</param>
        public void Insert(SdmxStructureType structureType, Database database, long annotatablePrimaryKey, IList<IAnnotation> annotations)
        {
            var engine = _factories.Select(factory => factory.GetEngine(structureType)).FirstOrDefault(e => e != null);
            if (engine != null)
            {
                AnnotationProcedureBase annotationProcedureBase = _factories.Select(factory => factory.GetProcedure(structureType)).First(e => e != null);
                engine.Insert(database, annotatablePrimaryKey, annotationProcedureBase, annotations);
            }
        }

        /// <summary>
        /// Inserts the specified structure type.
        /// </summary>
        /// <param name="structureType">Type of the structure.</param>
        /// <param name="database">The database.</param>
        /// <param name="annotations">The annotations grouped by each item primary key.</param>
        public void Insert(SdmxStructureType structureType, Database database, IList<KeyValuePair<long, IList<IAnnotation>>> annotations)
        {
            var engine = _factories.Select(factory => factory.GetEngine(structureType)).FirstOrDefault(e => e != null);
            if (engine != null)
            {
                AnnotationProcedureBase annotationProcedureBase =
               _factories.Select(factory => factory.GetProcedure(structureType)).First(e => e != null);
                engine.Insert(database, annotationProcedureBase, annotations);
            }
        }
    }
}