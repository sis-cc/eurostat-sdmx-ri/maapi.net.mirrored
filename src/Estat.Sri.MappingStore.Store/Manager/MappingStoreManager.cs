// -----------------------------------------------------------------------
// <copyright file="MappingStoreManager.cs" company="EUROSTAT">
//   Date Created : 2013-04-29
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;

    using Estat.Sri.MappingStore.Store.Engine;
    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Sdmx.MappingStore.Store.Engine;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Persist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The mapping store manager.
    /// </summary>
    public class MappingStoreManager : IStructurePersistenceManager
    {
        private static readonly object SyncObject = new object();

        /// <summary>
        ///     The _artefact import statuses.
        /// </summary>
        private readonly IList<ArtefactImportStatus> _artefactImportStatuses;

        /// <summary>
        ///     The _connection string settings.
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        ///     The _factories.
        /// </summary>
        private readonly IEngineFactories _factories;

        /// <summary>
        ///     The _measure dimension representation engine
        /// </summary>
        private readonly MeasureDimensionRepresentationEngine _measureDimensionRepresentationEngine;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MappingStoreManager" /> class.
        /// </summary>
        /// <param name="connectionStringSettings">
        ///     The connection string settings.
        /// </param>
        /// <param name="artefactImportStatuses">
        ///     The artefact import statuses.
        /// </param>
        public MappingStoreManager(ConnectionStringSettings connectionStringSettings, IList<ArtefactImportStatus> artefactImportStatuses)
            : this(connectionStringSettings, null, artefactImportStatuses)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MappingStoreManager" /> class.
        /// </summary>
        /// <param name="connectionStringSettings">
        ///     The connection string settings.
        /// </param>
        /// <param name="factories">
        ///     The factories.
        /// </param>
        /// <param name="artefactImportStatuses">
        ///     The artefact import statuses.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="connectionStringSettings" /> is null
        ///     -or-
        ///     <paramref name="artefactImportStatuses" /> is null
        /// </exception>
        public MappingStoreManager(ConnectionStringSettings connectionStringSettings, IEngineFactories factories, IList<ArtefactImportStatus> artefactImportStatuses)
        {
            if (connectionStringSettings == null)
            {
                throw new ArgumentNullException("connectionStringSettings");
            }

            if (artefactImportStatuses == null)
            {
                throw new ArgumentNullException("artefactImportStatuses");
            }

            this._connectionStringSettings = connectionStringSettings;
            this._artefactImportStatuses = artefactImportStatuses;
            this._factories = factories ?? new EngineFactories();
            Database database = DatabasePool.GetDatabase(connectionStringSettings);
            this._measureDimensionRepresentationEngine = new MeasureDimensionRepresentationEngine(database);
        }

        /// <summary>
        ///     Deletes the maintainable structures in the supplied objects
        /// </summary>
        /// <param name="maintainable">
        ///     The maintainable to delete
        /// </param>
        public void DeleteStructure(IMaintainableObject maintainable)
        {
            ISdmxObjects objects = new SdmxObjectsImpl(maintainable);
            objects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Delete);
            this.DeleteStructures(objects);
        }

        /// <summary>
        ///     Deletes the maintainable structures in the supplied sdmxObjects
        /// </summary>
        /// <param name="sdmxObjects">
        ///     SDMX objects
        /// </param>
        public void DeleteStructures(ISdmxObjects sdmxObjects)
        {
            if (sdmxObjects == null)
            {
                throw new ArgumentNullException(nameof(sdmxObjects));
            }
            
            var action = sdmxObjects.Action;
            if (action == null)
            {
                action = DatasetAction.GetFromEnum(DatasetActionEnumType.Delete);
            }

            if (action.EnumType.IsOneOf(DatasetActionEnumType.Append, DatasetActionEnumType.Replace))
            {
                throw new SdmxSemmanticException("Action set to Append/Replace but Delete was requested");
            }

            lock (SyncObject)
            {
                this.DeleteMaintainable(sdmxObjects.Categorisations);
                this.DeleteMaintainable(sdmxObjects.StructureSets);
                this.DeleteMaintainable(sdmxObjects.ContentConstraintObjects);
                this.DeleteMaintainable(sdmxObjects.Metadataflows);
                this.DeleteMaintainable(sdmxObjects.MetadataStructures);
                this.DeleteMaintainable(sdmxObjects.AgenciesSchemes);
                this.DeleteMaintainable(sdmxObjects.ProvisionAgreements);
                this.DeleteMaintainable(sdmxObjects.DataProviderSchemes);
                this.DeleteMaintainable(sdmxObjects.DataConsumerSchemes);
                this.DeleteMaintainable(sdmxObjects.OrganisationUnitSchemes);
                this.DeleteMaintainable(sdmxObjects.HierarchicalCodelists);
                this.DeleteMaintainable(sdmxObjects.Dataflows);

                this.DeleteMaintainable(sdmxObjects.CategorySchemes);
                this.DeleteMaintainable(sdmxObjects.DataStructures);
                this.DeleteMaintainable(sdmxObjects.ConceptSchemes);
                this.DeleteMaintainable(sdmxObjects.Codelists);
            }
        }

        /// <summary>
        ///     Saves the maintainable
        /// </summary>
        /// <param name="maintainable">
        ///     The <see cref="IMaintainableObject" />
        /// </param>
        public void SaveStructure(IMaintainableObject maintainable)
        {
            ISdmxObjects objects = new SdmxObjectsImpl(maintainable);
            this.SaveStructures(objects);
        }

        /// <summary>
        ///     Saves the maintainable structures in the supplied sdmxObjects
        /// </summary>
        /// <param name="sdmxObjects">
        ///     SDMX objects
        /// </param>
        public void SaveStructures(ISdmxObjects sdmxObjects)
        {
            if (sdmxObjects == null)
            {
                throw new ArgumentNullException(nameof(sdmxObjects));
            }

            var action = sdmxObjects.Action;
            if (action == null)
            {
                action = DatasetAction.GetFromEnum(DatasetActionEnumType.Information);
            }

            if (action.EnumType == DatasetActionEnumType.Delete)
            {
                throw new SdmxSemmanticException("Action set to Delete but Append/Replace was requested");
            }


            lock (SyncObject)
            {
                using (new StructureCacheContext(new StructureCache()))
                {
                    this.InsertMaintainable(sdmxObjects.Codelists, action);
                    this.InsertMaintainable(sdmxObjects.ConceptSchemes, action);
                    this.InsertMaintainable(sdmxObjects.DataStructures, action);
                    this.InsertMaintainable(sdmxObjects.Dataflows, action);
                    this.InsertMaintainable(sdmxObjects.CategorySchemes, action);
                    this.InsertMaintainable(sdmxObjects.AgenciesSchemes, action);
                    this.InsertMaintainable(sdmxObjects.DataProviderSchemes, action);
                    this.InsertMaintainable(sdmxObjects.DataConsumerSchemes, action);
                    this.InsertMaintainable(sdmxObjects.OrganisationUnitSchemes, action);
                    this.InsertMaintainable(sdmxObjects.MetadataStructures, action);
                    this.InsertMaintainable(sdmxObjects.Metadataflows, action);
                    this.InsertMaintainable(sdmxObjects.Categorisations, action);
                    this.InsertMaintainable(sdmxObjects.HierarchicalCodelists, action);
                    this.InsertMaintainable(sdmxObjects.StructureSets, action);
                    this.InsertMaintainable(sdmxObjects.ContentConstraintObjects, action);
                    this.InsertMaintainable(sdmxObjects.ProvisionAgreements, action);
                    this.InsertMaintainable(sdmxObjects.Registrations, action);

                    // HACK. Handle SDMX v2.1 Measure Dimensions until proper support is added.
                    // We create a dummy codelist for all SDMX v2.1 Measure dimensions.
                    this._measureDimensionRepresentationEngine.CreateDummyCodelistForAll();
                }
            }
        }

        /// <summary>
        ///     Delete the specified <paramref name="maintainables" />
        /// </summary>
        /// <param name="maintainables">
        ///     The maintainable.
        /// </param>
        /// <typeparam name="T">
        ///     The <see cref="IMaintainableObject" /> based type.
        /// </typeparam>
        private void DeleteMaintainable<T>(IEnumerable<T> maintainables) where T : IMaintainableObject
        {
            var maintainableDeleteMethod = this._factories.GetMaintainableDeleteMethod<T>(this._connectionStringSettings);
            var importStatus = maintainableDeleteMethod(maintainables);
            this._artefactImportStatuses.AddAll(importStatus);
        }

        /// <summary>
        /// Insert the specified <paramref name="maintainables" />.
        /// </summary>
        /// <typeparam name="T">The <see cref="IMaintainableObject" /> based type.</typeparam>
        /// <param name="maintainables">The maintainable artefacts.</param>
        /// <param name="action">The action.</param>
        private void InsertMaintainable<T>(IEnumerable<T> maintainables, DatasetAction action) where T : IMaintainableObject
        {
            this._artefactImportStatuses.AddAll(this._factories.GetMaintainableImportMethod<T>(this._connectionStringSettings, action)(maintainables));
        }
    }
}