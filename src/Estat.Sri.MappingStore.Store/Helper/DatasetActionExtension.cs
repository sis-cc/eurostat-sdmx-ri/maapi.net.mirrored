// -----------------------------------------------------------------------
// <copyright file="DatasetActionExtension.cs" company="EUROSTAT">
//   Date Created : 2019-08-30
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Constants;
using static System.String;

namespace Estat.Sri.Sdmx.MappingStore.Store.Helper
{
    /// <summary>
    /// dataset action extension
    /// </summary>
    public static class DatasetActionExtension
    {
        /// <summary>
        /// Converts to actionstring.
        /// </summary>
        /// <param name="datasetAction">The dataset action.</param>
        /// <returns></returns>
        public static string ToActionString(this DatasetAction datasetAction)
        {
            if (datasetAction == null)
            {
                return Empty;
            }

            switch (datasetAction.EnumType)
            {
                case DatasetActionEnumType.Append:
                    return "Created";
                case DatasetActionEnumType.Delete:
                    return "Deleted";
                case DatasetActionEnumType.Replace:
                    return "Updated";
            }

            return Empty;
        }
    }
}