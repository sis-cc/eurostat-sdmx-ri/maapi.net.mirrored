// -----------------------------------------------------------------------
// <copyright file="ReferenceImportEngine.cs" company="EUROSTAT">
//   Date Created : 2022-06-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStore.Store.Engine;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Util;
    using Estat.Sri.Sdmx.MappingStore.Store.Engine;

    /// <summary>
    /// The engine for importing reference artefacts
    /// </summary>
    public class ReferenceImportEngine
    {
        private readonly Database _mappingStore;

        private readonly Lazy<TextFormatTypesQueryEngine> _textFormatTypesQueryEngine;

        private readonly StoredProcedures _storedProcedures = new StoredProcedures();

        /// <summary>
        /// Initializes a new instance of the engine.
        /// </summary>
        /// <param name="mappingStore">The mapping store database instance.</param>
        public ReferenceImportEngine(Database mappingStore)
        {
            if (mappingStore == null)
            {
                throw new ArgumentNullException(nameof(mappingStore));
            }
            this._mappingStore = mappingStore;
            this._textFormatTypesQueryEngine = new Lazy<TextFormatTypesQueryEngine>(() =>
            {
                return new TextFormatTypesQueryEngine(mappingStore);
            });
        }

        /// <summary>
        /// Inserts the references to other artefacts for a given artefact.
        /// </summary>
        /// <param name="state">The transaction</param>
        /// <param name="artefactPrimaryKey">The PK of the given artefact.</param>
        /// <param name="references">The refernces to other artefacts.</param>
        public void InsertReferences(DbTransactionState state, long artefactPrimaryKey, IList<ReferenceToOtherArtefact> references)
        {
            if (!references.Any())
            {
                return;
            }

            IList<ReferenceWithPrimaryKey> referenceWithPrimaryKeys = InsertReferenceSources(state, artefactPrimaryKey, references);
            RetrieveTargetArtefactBaseId(state, referenceWithPrimaryKeys);

            var procedure = _storedProcedures.InsertStructureReference;
            using (DbCommand command = procedure.CreateCommand(state))
            {
                foreach (ReferenceWithPrimaryKey referenceWithPrimaryKey in referenceWithPrimaryKeys)
                {
                    command.Parameters.Clear();

                    procedure.CreateSourceIdParameter(command).Value = referenceWithPrimaryKey.PrimaryKey;
                    procedure.CreateTargetIdParameter(command).Value = referenceWithPrimaryKey.TargetArtefactPk;

                    IVersionRequest versionRequest = new VersionRequestCore(referenceWithPrimaryKey.Reference.Target.Version);
                    procedure.CreateMajorVersionParameter(command).Value = versionRequest.Major ?? -1;
                    procedure.CreateMinorVersionParameter(command).Value = versionRequest.Minor ?? -1;
                    procedure.CreatePatchVersionParameter(command).Value = versionRequest.Patch ?? -1;
                    DbParameter versionExtParam = procedure.CreateVersionExtensionParameter(command);
                    if (versionRequest.Extension != null)
                    {
                        versionExtParam.Value = versionRequest.Extension;
                    }
                    int wildcardPos = versionRequest.WildCard == null ? 0 : (int)versionRequest.WildCard.WildcardPosition + 1;
                    procedure.CreateWildcardPositionParameter(command).Value = wildcardPos;

                    DbParameter childIdParam = procedure.CreateChildIdParameter(command);
                    if (ObjectUtil.ValidString(referenceWithPrimaryKey.Reference.TargetSubStructurePath))
                    {
                        childIdParam.Value = referenceWithPrimaryKey.Reference.TargetSubStructurePath;
                    }

                    procedure.CreateOutputParameter(command);
                    command.ExecuteNonQuery();
                }
            }
        }

        public class ReferenceWithPrimaryKey
        {
            internal readonly ReferenceToOtherArtefact Reference;
            internal readonly long PrimaryKey;
            internal long TargetArtefactPk;

            public ReferenceWithPrimaryKey(ReferenceToOtherArtefact reference, long primaryKey)
            {
                this.Reference = reference;
                this.PrimaryKey = primaryKey;
            }

            public void SetTargetArtefactPk(long targetArtefactPk)
            {
                this.TargetArtefactPk = targetArtefactPk;
            }

            public ReferenceToOtherArtefact GetReference()
            {
                return Reference;
            }
        }
        /// <summary>
        /// Inserts the <see cref="ITextFormat"/>s for a given artefact.
        /// </summary>
        /// <param name="state">The transaction</param>
        /// <param name="artefactPrimaryKey">The PK of the given artefact.</param>
        /// <param name="textFormats">The text formats.</param>
        /// <exception cref="SdmxException">If insert fails.</exception>
        public void InsertTextFormats(DbTransactionState state, long artefactPrimaryKey, IList<ITextFormat> textFormats)
        {
            if (!textFormats.Any())
            {
                return;
            }
            IList<ReferenceToOtherArtefact> references = textFormats
                .Select(textFormatBean => new ReferenceToOtherArtefact(textFormatBean, RefTypes.Format)).ToList();
            List<ReferenceWithPrimaryKey> referenceWithPrimaryKeys = InsertReferenceSources(state, artefactPrimaryKey, references);

            try
            {
                _textFormatTypesQueryEngine.Value.InsertTextFormats(state,
                    referenceWithPrimaryKeys.Select(r => new KeyValuePair<long, ITextFormat>(r.PrimaryKey, r.Reference.TextFormat)));
            }
            catch (Exception e)
            {
                throw new SdmxException("Error occurred while inserting text format", e);
            }
        }

        private void RetrieveTargetArtefactBaseId(DbTransactionState state, IList<ReferenceWithPrimaryKey> referenceWithPrimaryKeys)
        {
            foreach (ReferenceWithPrimaryKey referenceWithPrimaryKey in referenceWithPrimaryKeys)
            {
               ReferenceWithPrimaryKey refWithPrimaryKey = StructureCacheContext.GetStructureCacheFromThreadLocal().GetCachedReferenceWithPrimaryKey(state.Database, referenceWithPrimaryKey);
               if(refWithPrimaryKey != null)
               {
                   referenceWithPrimaryKey.SetTargetArtefactPk(refWithPrimaryKey.TargetArtefactPk);
               }
            }
        }

        private List<ReferenceWithPrimaryKey> InsertReferenceSources(DbTransactionState state, long artefactPrimaryKey, IList<ReferenceToOtherArtefact> references)
        {
            List<ReferenceWithPrimaryKey> referenceWithPrimaryKeys = new List<ReferenceWithPrimaryKey>();
            var procedure = _storedProcedures.InsertReferenceSource;
            using (DbCommand command = procedure.CreateCommand(state))
            {
                foreach (ReferenceToOtherArtefact reference in references)
                {
                    command.Parameters.Clear();

                    procedure.CreateSourceIdParameter(command).Value = artefactPrimaryKey;

                    DbParameter sourceChildParam = procedure.CreateSourceChildParameter(command);
                    if (ObjectUtil.ValidString(reference.SubStructureFullPath))
                    {
                        sourceChildParam.Value = reference.SubStructureFullPath;
                    }

                    DbParameter referenceTypeParam = procedure.CreateReferenceTypeParameter(command);
                    if (reference.ReferenceType != null)
                    {
                        referenceTypeParam.Value = reference.ReferenceType;
                    }

                    DbParameter pOutPk = procedure.CreateOutputParameter(command);

                    command.ExecuteNonQuery();
                    long refSource = (long)pOutPk.Value;
                    referenceWithPrimaryKeys.Add(new ReferenceWithPrimaryKey(reference, refSource));
                }
            }
            return referenceWithPrimaryKeys;
        }
    }
}
