// -----------------------------------------------------------------------
// <copyright file="LocalisedStringInsertEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-05
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Data;
    using System.Data.Common;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// The engine for inserting localised strings
    /// Compatible to MSDB 7.0
    /// </summary>
    public class LocalisedStringInsertEngine
    {
        // TODO switch from call INSERT_LOCALISED_STRING to following for possible performance reasons
        // insert into LOCALISED_STRING_ITEM
        // insert into LOCALISED_STRING_ART

        /// <summary>
        ///     The _insert localized string
        /// </summary>
        private static readonly InsertLocalisedString _insertLocalisedString;

        /// <summary>
        ///     The SP for inserting localised-string-other.
        /// </summary>
        private static readonly InsertLocalisedStringOther _insertLocalisedStringOther;

        /// <summary>
        ///     Initializes static members of the <see cref="LocalisedStringInsertEngine" /> class.
        /// </summary>
        static LocalisedStringInsertEngine()
        {
            var SPs = new StoredProcedures();
            _insertLocalisedString = SPs.InsertLocalisedString;
            _insertLocalisedStringOther = SPs.InsertLocalisedStringOther;
        }

        /// <summary>
        ///     Insert a record with the values from <paramref name="maintainable" /> to <c>LOCALISED_STRING</c> for an artefact
        ///     with the specified <paramref name="artefactPrimaryKey" />
        /// </summary>
        /// <param name="artefactPrimaryKey">The artifact primary key.</param>
        /// <param name="maintainable">The maintainable.</param>
        /// <param name="database">The database.</param>
        public void InsertForArtefact(long artefactPrimaryKey, IMaintainableObject maintainable, Database database)
        {
            using (var dbCommand = _insertLocalisedString.CreateCommand(database))
            {
                _insertLocalisedString.CreateArtIdParameter(dbCommand).Value = artefactPrimaryKey;
                _insertLocalisedString.CreateItemIdParameter(dbCommand);
                InsertCommon(maintainable, dbCommand);
            }
        }

        /// <summary>
        ///     Insert a record with the values from <paramref name="maintainable" /> to <c>LOCALISED_STRING</c> for an item with
        ///     the specified <paramref name="itemPrimaryKey" />
        /// </summary>
        /// <param name="itemPrimaryKey">The item primary key.</param>
        /// <param name="maintainable">The nameable Object.</param>
        /// <param name="database">The database.</param>
        public void InsertForItem(long itemPrimaryKey, IItemObject maintainable, Database database)
        {
            using (var dbCommand = _insertLocalisedString.CreateCommand(database))
            {
                _insertLocalisedString.CreateItemIdParameter(dbCommand).Value = itemPrimaryKey;
                _insertLocalisedString.CreateArtIdParameter(dbCommand);
                InsertCommon(maintainable, dbCommand);
            }
        }

        /// <summary>
        /// Inserts the localised strings for <paramref name="nameableObject"/>.
        /// </summary>
        /// <param name="parentPrimaryKey">the primary key</param>
        /// <param name="nameableObject">the object</param>
        /// <param name="database">the database</param>
        public void Insert(long parentPrimaryKey, INameableObject nameableObject, Database database)
        {
            if (nameableObject.StructureType.IsMaintainable)
            {
                InsertForArtefact(parentPrimaryKey, (IMaintainableObject)nameableObject, database);
            }
            else if (nameableObject is IItemObject item)
            {
                InsertForItem(parentPrimaryKey, item, database);
            }
            else
            {
                InsertForOther(parentPrimaryKey, nameableObject, database);
            }
        }

        /// <summary>
        /// Inserts the localised strings of the <paramref name="nameableObject"/> into LOCALISED_STRING_OTHER table.
        /// </summary>
        /// <param name="parentPrimaryKey">The primary key</param>
        /// <param name="nameableObject">The object to insert the localised strings for.</param>
        /// <param name="database">The database.</param>
        public void InsertForOther(long parentPrimaryKey, INameableObject nameableObject, Database database)
        {
            foreach (ITextTypeWrapper name in nameableObject.Names)
            {
                InsertSingleOther(database, parentPrimaryKey, name.Value, name.Locale, true);

            }
            foreach (ITextTypeWrapper description in nameableObject.Descriptions)
            {
                InsertSingleOther(database, parentPrimaryKey, description.Value, description.Locale, false);
            }
        }

        private int InsertSingleOther(Database database, long parentPrimaryKey, string value, string locale, bool isName)
        {
            using (DbCommand command = _insertLocalisedStringOther.CreateCommandWithDefaults(database))
            {
                _insertLocalisedStringOther.CreateOtherIdParameter(command).Value = parentPrimaryKey;
                _insertLocalisedStringOther.CreateTextParameter(command).Value = value;
                _insertLocalisedStringOther.CreateIsNameParameter(command).Value = isName.ToDbValue();
                _insertLocalisedStringOther.CreateLanguageParameter(command).Value = locale;
                _insertLocalisedStringOther.CreateOutputParameter(command);

                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        ///     Returns the normalized language.
        /// </summary>
        /// <param name="text">
        ///     The text.
        /// </param>
        /// <returns>
        ///     The normalized language.
        /// </returns>
        private static string GetLanguage(ITextTypeWrapper text)
        {
            return string.IsNullOrEmpty(text.Locale) ? "en" : text.Locale.ToLowerInvariant();
        }

        /// <summary>
        ///     Insert a record with the values from <paramref name="nameableObject" /> to <c>LOCALISED_STRING</c>
        /// </summary>
        /// <param name="nameableObject">
        ///     The nameable object.
        /// </param>
        /// <param name="dbCommand">
        ///     The DB command.
        /// </param>
        private static void InsertCommon(INameableObject nameableObject, DbCommand dbCommand)
        {
            _insertLocalisedString.CreateOutputParameter(dbCommand);
            var typeParameter = _insertLocalisedString.CreateTypeParameter(dbCommand);
            var languageParameter = _insertLocalisedString.CreateLanguageParameter(dbCommand);

            var textParameter = _insertLocalisedString.CreateTextParameter(dbCommand);
            typeParameter.Value = LocalisedStringType.Name;
            foreach (var name in nameableObject.Names)
            {
                InsertCommon(name, dbCommand, languageParameter, textParameter);
            }

            typeParameter.Value = LocalisedStringType.Desc;
            foreach (var description in nameableObject.Descriptions)
            {
                InsertCommon(description, dbCommand, languageParameter, textParameter);
            }
        }

        /// <summary>
        ///     Insert a record with the values from <paramref name="text" /> to <c>LOCALISED_STRING</c>
        /// </summary>
        /// <param name="text">
        ///     The text.
        /// </param>
        /// <param name="dbCommand">
        ///     The DB command.
        /// </param>
        /// <param name="languageParameter">
        ///     The language Parameter.
        /// </param>
        /// <param name="textParameter">
        ///     The text Parameter.
        /// </param>
        private static void InsertCommon(ITextTypeWrapper text, IDbCommand dbCommand, IDataParameter languageParameter, IDataParameter textParameter)
        {
            languageParameter.Value = GetLanguage(text);
            textParameter.Value = text.Value;

            dbCommand.ExecuteNonQueryAndLog();
        }
    }
}
