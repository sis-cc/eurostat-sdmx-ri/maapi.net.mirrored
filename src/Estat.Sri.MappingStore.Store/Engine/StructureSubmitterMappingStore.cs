// -----------------------------------------------------------------------
// <copyright file="StructureSubmitterMappingStore.cs" company="EUROSTAT">
//   Date Created : 2017-11-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Model.Error;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Manager;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    /// The SDMX RI implementation of <see cref="IStructureSubmitterEngine"/>
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Engine.IStructureSubmitterEngine" />
    class StructureSubmitterMappingStore : IStructureSubmitterEngine
    {
        /// <summary>
        /// The connection strings
        /// </summary>
        private readonly ConnectionStringSettings _connectionStrings;

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureSubmitterMappingStore"/> class.
        /// </summary>
        /// <param name="connectionStrings">The connection strings.</param>
        /// <exception cref="ArgumentNullException">connectionStrings</exception>
        public StructureSubmitterMappingStore(ConnectionStringSettings connectionStrings)
        {
            if (connectionStrings == null)
            {
                throw new ArgumentNullException(nameof(connectionStrings));
            }

            _connectionStrings = connectionStrings;
        }

        /// <summary>
        /// Submits the <paramref name="sdmxObjects" /> to a store specific to the implementation. The action is determined by the <paramref name="sdmxObjects" /><see cref="P:Org.Sdmxsource.Sdmx.Api.Model.Objects.ISdmxObjects.Action" />
        /// </summary>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        /// <returns>
        /// The list of <see cref="T:Estat.Sdmxsource.Extension.Model.Error.IResponseWithStatusObject" />
        /// </returns>
        public IList<IResponseWithStatusObject> SubmitStructures(ISdmxObjects sdmxObjects)
        {
            // we don't want the filters when we delete or add or replace.
            using (new DataflowFilterContext(MappingStoreRetrieval.Constants.DataflowFilter.Any))
            {
                var list = new List<ArtefactImportStatus>();
                var persistManager = new MappingStoreManager(_connectionStrings, list);
                DatasetAction action = sdmxObjects.Action;
                if (action == null)
                {
                    action = DatasetAction.GetFromEnum(DatasetActionEnumType.Information);
                    var modifiedOjects = new SdmxObjectsImpl();
                    modifiedOjects.Merge(sdmxObjects);
                    modifiedOjects.Action = action;
                    sdmxObjects = modifiedOjects;
                }

                switch (action.EnumType)
                {
                    case DatasetActionEnumType.Information:
                    case DatasetActionEnumType.Append:
                    case DatasetActionEnumType.Replace:
                        persistManager.SaveStructures(sdmxObjects);
                        break;
                    case DatasetActionEnumType.Delete:
                        persistManager.DeleteStructures(sdmxObjects);
                        break;
                }

                var responses = list.Select(a => a.ImportMessage.Convert(action));

                return responses.ToArray();
            }
        }
    }
}
