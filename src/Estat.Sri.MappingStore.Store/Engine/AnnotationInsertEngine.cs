// -----------------------------------------------------------------------
// <copyright file="AnnotationInsertEngine.cs" company="EUROSTAT">
//   Date Created : 2014-10-13
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     The class responsible for adding records to <c>ANNOTATION</c> and related tables.
    /// </summary>
    public class AnnotationInsertEngine : IAnnotationInsertEngine
    {
        /// <summary>
        ///     The _insert annotation for artefacts
        /// </summary>
        private readonly InsertAnnotationText _annotationText;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotationInsertEngine" /> class.
        /// </summary>
        public AnnotationInsertEngine()
        {
            this._annotationText = new StoredProcedures().InsertAnnotationText;
        }

        /// <summary>
        ///     Insert a record with the values from <paramref name="annotations" /> to <paramref name="annotationProcedureBase" />
        ///     for an artifact with the specified
        ///     <paramref name="annotatablePrimaryKey" />
        /// </summary>
        /// <param name="state">The mapping store connection and transaction state</param>
        /// <param name="annotatablePrimaryKey">The artifact primary key.</param>
        /// <param name="annotationProcedureBase">The annotation procedure base.</param>
        /// <param name="annotations">The annotations.</param>
        public virtual void Insert(
            DbTransactionState state,
            long annotatablePrimaryKey,
            AnnotationProcedureBase annotationProcedureBase,
            IList<IAnnotation> annotations)
        {
            this.Insert(state.Database, annotatablePrimaryKey, annotationProcedureBase, annotations);
        }

        /// <summary>
        /// Insert a record with the values from <paramref name="annotations" /> to <paramref name="annotationProcedureBase" />
        /// for an artifact with the specified
        /// <paramref name="annotatablePrimaryKey" />
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="annotatablePrimaryKey">The artifact primary key.</param>
        /// <param name="annotationProcedureBase">The annotation procedure base.</param>
        /// <param name="annotations">The annotations.</param>
        public void Insert(Database database, long annotatablePrimaryKey, AnnotationProcedureBase annotationProcedureBase, IList<IAnnotation> annotations)
        {
            var count = annotations.Count;
            if (count == 0)
            {
                return;
            }

            var sysIdToAnnotation = new KeyValuePair<long, IAnnotation>[count];
            using (var command = annotationProcedureBase.CreateCommandWithDefaults(database))
            {
                annotationProcedureBase.CreateParentIdParameter(command, annotatablePrimaryKey);
                var outputParameter = annotationProcedureBase.CreateOutputParameter(command);
                for (int i = 0; i < count; i++)
                {
                    var annotation = annotations[i];
                    annotationProcedureBase.CreateIdParameter(command, annotation.Id);
                    annotationProcedureBase.CreateTitleParameter(command, annotation.Title);
                    annotationProcedureBase.CreateTypeParameter(command, annotation.Type);
                    annotationProcedureBase.CreateUriParameter(command, annotation.Uri != null ? annotation.Uri.ToString() : null);
                    command.ExecuteNonQueryAndLog();
                    if (annotation.Text.Count > 0)
                    {
                        sysIdToAnnotation[i] = new KeyValuePair<long, IAnnotation>((long)outputParameter.Value, annotation);
                    }
                }
            }

            using (var command = this._annotationText.CreateCommandWithDefaults(database))
            {
                for (int i = 0; i < sysIdToAnnotation.Length; i++)
                {
                    var keyValuePair = sysIdToAnnotation[i];
                    if (!keyValuePair.IsDefault())
                    {
                        this._annotationText.CreateAnnIdParameter(command).Value = keyValuePair.Key;
                        foreach (var textTypeWrapper in keyValuePair.Value.Text)
                        {
                            this._annotationText.CreateLanguageParameter(command).Value = GetLanguage(textTypeWrapper);
                            this._annotationText.CreateTextParameter(command).Value = textTypeWrapper.Value;
                            command.ExecuteNonQueryAndLog();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///  Insert a record with the values from <paramref name="annotations" />, which key is the <see cref="IAnnotableObject"/> primary key, to <paramref name="annotationProcedureBase" />
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="annotationProcedureBase">The annotation procedure base.</param>
        /// <param name="annotations">The annotations.</param>
        public void Insert(Database database, AnnotationProcedureBase annotationProcedureBase, IList<KeyValuePair<long, IList<IAnnotation>>> annotations)
        {
            var count = annotations.Count;
            if (count == 0)
            {
                return;
            }

            var sysIdToAnnotation = new List<KeyValuePair<long, IAnnotation>>();
            using (var command = annotationProcedureBase.CreateCommandWithDefaults(database))
            {
                var parentId = annotationProcedureBase.CreateParentIdParameter(command, -1);
                var id = annotationProcedureBase.CreateIdParameter(command, null);
                var title = annotationProcedureBase.CreateTitleParameter(command, null);
                var type = annotationProcedureBase.CreateTypeParameter(command, null);
                var uri = annotationProcedureBase.CreateUriParameter(command, null);
                    var outputParameter = annotationProcedureBase.CreateOutputParameter(command);
                for (int i = 0; i < count; i++)
                {
                    var keyValuePair = annotations[i];
                    parentId.Value = keyValuePair.Key;
                    foreach (var annotation in keyValuePair.Value)
                    {
                        id.Value = (object)annotation.Id ?? DBNull.Value;
                        title.Value = (object)annotation.Title ?? DBNull.Value;
                        type.Value = (object)annotation.Type ?? DBNull.Value;
                        uri.Value = annotation.Uri != null ? (object)annotation.Uri.ToString() : DBNull.Value;
                        command.ExecuteNonQueryAndLog();
                        if (annotation.Text.Count > 0)
                        {
                            sysIdToAnnotation.Add(new KeyValuePair<long, IAnnotation>(
                                (long)outputParameter.Value,
                                annotation));
                        }
                    }
                }
            }

            using (var command = this._annotationText.CreateCommandWithDefaults(database))
            {
                for (int i = 0; i < sysIdToAnnotation.Count; i++)
                {
                    var keyValuePair = sysIdToAnnotation[i];
                    if (!keyValuePair.IsDefault())
                    {
                        this._annotationText.CreateAnnIdParameter(command).Value = keyValuePair.Key;
                        foreach (var textTypeWrapper in keyValuePair.Value.Text)
                        {
                            this._annotationText.CreateLanguageParameter(command).Value = GetLanguage(textTypeWrapper);
                            this._annotationText.CreateTextParameter(command).Value = textTypeWrapper.Value;
                            command.ExecuteNonQueryAndLog();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Returns the normalized language.
        /// </summary>
        /// <param name="text">
        ///     The text.
        /// </param>
        /// <returns>
        ///     The normalized language.
        /// </returns>
        private static string GetLanguage(ITextTypeWrapper text)
        {
            return string.IsNullOrEmpty(text.Locale) ? "en" : text.Locale.ToLowerInvariant();
        }
    }
}