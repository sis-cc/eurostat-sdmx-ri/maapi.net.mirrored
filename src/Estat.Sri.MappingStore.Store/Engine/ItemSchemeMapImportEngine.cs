// -----------------------------------------------------------------------
// <copyright file="ItemSchemeMapImportEngine.cs" company="EUROSTAT">
//   Date Created : 2014-10-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Data.Common;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;

    /// <summary>
    /// The engine for importing an item scheme map.
    /// Compatible to MSDB 7.0
    /// </summary>
    /// <typeparam name="TSchemeMap">The type of scheme map.</typeparam>
    public abstract class ItemSchemeMapImportEngine<TSchemeMap> : SchemeMapBaseEngine<TSchemeMap>
        where TSchemeMap : IItemSchemeMapObject
    {
        private readonly InsertStructureSetCommonMap2Procedure _mapProcedure = new InsertStructureSetCommonMap2Procedure();

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="mappingStore">The mapping store database.</param>
        public ItemSchemeMapImportEngine(Database mappingStore)
            : base(mappingStore)
        {
        }

        protected override void WriteItemMaps(DbTransactionState state, TSchemeMap schemaMap, long primaryKey)
        {
            using (DbCommand command = _mapProcedure.CreateCommandWithDefaults(state.Database))
            {
                foreach (IItemMap itemMapBean in schemaMap.Items)
                {
                    _mapProcedure.CreateParentIdParameter(command).Value = primaryKey;
                    _mapProcedure.CreateSourceIdParameter(command).Value = itemMapBean.SourceId;
                    _mapProcedure.CreateTargetIdParameter(command).Value = itemMapBean.TargetId;

                    command.ExecuteNonQueryAndLog();
                }
            }
        }
    }
}