using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.MappingStore.Store.Engine;
using System.Threading;

namespace Estat.Sri.Sdmx.MappingStore.Store.Engine
{
    public class StructureCacheContext : IDisposable
    {
        private static ThreadLocal<StructureCache> structureCacheThreadLocal = new ThreadLocal<StructureCache>();

        public StructureCacheContext(StructureCache structureCache)
        {
            structureCacheThreadLocal.Value = structureCache;
        }

        public static StructureCache GetStructureCacheFromThreadLocal()
        {
            return structureCacheThreadLocal.Value;
        }

        public void Dispose()
        {
            structureCacheThreadLocal.Value = null;
        }
    }
}
