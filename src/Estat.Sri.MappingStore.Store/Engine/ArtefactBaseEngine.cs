// -----------------------------------------------------------------------
// <copyright file="ArtefactBaseEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-05
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Engine.Update;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Estat.Sri.Sdmx.MappingStore.Store.Engine;
    using Estat.Sri.Sdmx.MappingStore.Store.Properties;
    using Estat.Sri.Utils.Config;
    using Estat.Sri.Utils.Model;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// An abstract class that can be used to persist SDMX artefacts.
    /// </summary>
    public abstract class ArtefactBaseEngine
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ArtefactBaseEngine));

        /// <summary>
        /// The mapping store database
        /// </summary>
        protected readonly Database database;

        protected readonly IRetrievalEngineContainer retrievalEngineContainer;

        private readonly LocalisedStringInsertEngine _localisedStringInsertEngine;

        private readonly IAnnotationInsertEngine _annotationInsertEngine;

        private readonly InsertArtefactAnnotation _insertArtefactAnnotation;

        private readonly UpdateArtefact _updateEngine = new UpdateArtefact();


        /// <summary>
        /// Initializes a new instance of the abstract class
        /// Can only be used when initializing a derived class.
        /// </summary>
        /// <param name="mappingStore">The instance of the mastore database.</param>
        protected ArtefactBaseEngine(Database mappingStore)
        {
            this.database = mappingStore;
            retrievalEngineContainer = new RetrievalEngineContainer(mappingStore);
            _localisedStringInsertEngine = new LocalisedStringInsertEngine();
            _annotationInsertEngine = new ArtefactAnnotationInsertEngine(new AnnotationInsertEngine());
            _insertArtefactAnnotation = new InsertArtefactAnnotation();           
        }


        #region Stuff to override

        /// <summary>
        /// Should be overriden by each separate engine that needs to delete its children
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="primaryKey">The primary key.</param>
        protected virtual void DeleteChildStructures(DbTransactionState state, long primaryKey)
        {
        }

        /// <summary>
        ///     Insert the specified <paramref name="artefact" /> to MAPPING STORE
        /// </summary>
        /// <param name="state">
        ///     The Mapping Store connection and transaction state
        /// </param>
        /// <param name="artefact">
        ///     The artefact.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        protected abstract ArtefactImportStatus InsertArtefact(DbTransactionState state, IMaintainableObject artefact);

        /// <summary>
        /// Override this method to have the <paramref name="maintainable"/> checked before being inserted or replaced.
        /// Should throw some exception if the prerequisites are not met. 
        /// </summary>
        /// <param name="maintainable">The SDMX maintainable to check.</param>
        protected virtual void CheckPrerequisites(IMaintainableObject maintainable)
        {
        }

        /// <summary>
        /// Override this method to be executed before the deletion of the N_ARTEFACT table.
        /// </summary>
        /// <param name="state">The transaction</param>
        /// <param name="primaryKey">The PK of the artefact to be deleted.</param>
        /// <param name="artefact"></param>
        protected virtual void BeforeArtefactDeletion(DbTransactionState state, long primaryKey, IMaintainableObject artefact)
        {
        }

        /// <summary>
        /// Deletes and inserts provided artefact, unless it is used by another artefact
        /// </summary>
        /// <param name="state">The transaction</param>
        /// <param name="artefact">The artefact to delete.</param>
        /// <param name="finalStatus">The artefact's final status.</param>
        /// <returns></returns>
        protected virtual List<ArtefactImportStatus> DeleteInsert(DbTransactionState state, IMaintainableObject artefact, ArtefactFinalStatus finalStatus)
        {
            _log.Debug(string.Format("Will delete artefact {0}.", artefact.Urn));
            Delete(state, finalStatus.PrimaryKey,artefact);
            return new List<ArtefactImportStatus>()
            {
                this.InsertArtefact(state, artefact)
            };
        }

        /// <summary>
        /// Should be overriden by other specific engines that need to perform additional actions during replacement of a final artefact.
        /// </summary>
        /// <param name="state">The transaction</param>
        /// <param name="artefact">The artefact being replaced.</param>
        /// <param name="finalStatus">The artefact's final status.</param>
        /// <returns><c>null</c></returns>
        protected virtual ArtefactImportStatus HandleFinalArtefactReplace(
            DbTransactionState state, IMaintainableObject artefact, ArtefactFinalStatus finalStatus)
        {
            return null;
        }

        /// <summary>
        /// Should be overriden by other specific engines that need to perform additional actions during replacement of a non-final artefact.
        /// </summary>
        /// <param name="state">The transaction</param>
        /// <param name="artefact">The artefact being replaced.</param>
        /// <param name="finalStatus">The artefact's final status.</param>
        /// <param name="crossReferencingObjects"></param>
        /// <returns><c>null</c></returns>
        protected virtual ArtefactImportStatus HandleNonFinalArtefactReplace(
            DbTransactionState state, IMaintainableObject artefact, ArtefactFinalStatus finalStatus,
            IList<IMaintainableMutableObject> crossReferencingObjects)
        {
            return null;
        }

        #endregion

        /// <summary>
        ///     Delete an ARTEFACT with the specified <paramref name="primaryKey" /> value
        /// </summary>
        /// <param name="state">
        ///     The Mapping Store <see cref="DbTransactionState" />
        /// </param>
        /// <param name="primaryKey">
        ///     The primary key value.
        /// </param>
        /// <param name="artefact"></param>
        /// <returns>
        ///     The number of records deleted.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="state"/> is <see langword="null" />.</exception>
        public int Delete(DbTransactionState state, long primaryKey, IMaintainableObject artefact)
        {
            if (state == null)
            {
                throw new ArgumentNullException(nameof(state));
            }

            this.DeleteChildStructures(state, primaryKey);
            _log.DebugFormat(CultureInfo.InvariantCulture, "Deleting artefact record with primary key (ART_ID) = {0}", primaryKey);

            List<string> deletionQueries = new List<string>();

            deletionQueries.AddRange(new string[]
            {
                "DELETE FROM ANNOTATION WHERE ANN_ID in (select ANN_ID from ARTEFACT_ANNOTATION WHERE ART_ID = {0})",
                "DELETE FROM STRUCTURE_REF WHERE REF_SRC_ID in (select REF_SRC_ID from REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {0})",
                "DELETE FROM TEXT_FORMAT WHERE REF_SRC_ID in (select REF_SRC_ID from REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {0})",
                "DELETE FROM REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {0}",
                "DELETE FROM LOCALISED_STRING_ART WHERE ART_ID = {0}"
            });

            foreach (string query in deletionQueries)
            {
                ExecuteArtefactUpdateOrDeleteQuery(state, query, primaryKey);
            }

            // TODO SDMXRI-1839 The old code wont work on MADB7, at least for dataflow.
            // a suggestion for a better sql query would be the one below but that could still leave garbage
            //  delete from N_ARTEFACT where ART_ID in (select ART_ID from REF_CHILDREN where SOURCE_ARTEFACT = ? )
            //  and ART_BASE_ID in (select ART_BASE_ID from ARTEFACT_BASE where ARTEFACT_TYPE = 'Categorisation')
            // -be careful of recursive loop, don't call this when deleting categorisation
            // -As said above, we need to not leave garbage so best appoarch would be to find parent references of the artefact that we want to delete, and get only those that are categorisations and delete them with delete engine

            //delete artefact categorisations
            if (artefact != null && artefact.StructureType.EnumType != SdmxStructureEnumType.Categorisation)
            {
                List<long> categorisations = GetCategorisationsForArtefact(state, primaryKey);
                // TODO Avoid recursion in libraries
                categorisations.ForEach(x => Delete(state, x, null));
            }

            if (artefact != null) 
            {
                BeforeArtefactDeletion(state, primaryKey, artefact);
            }

            ExecuteArtefactUpdateOrDeleteQuery(state, "DELETE FROM ENTITY_SDMX_REF WHERE " +
                // ART_BASE_ID is common for all artefacts with the same id, agency and type
                " TARGET_ARTEFACT IN (SELECT ART_BASE_ID FROM N_ARTEFACT WHERE ART_ID = {0})"
                // But we don't want to delete ENTITY_SDMX_REF records for artefacts used by other versions of the artefact
                + " AND TARGET_ARTEFACT NOT IN (SELECT ART_BASE_ID FROM N_ARTEFACT WHERE ART_ID != {0})"
                , primaryKey);
            var artefactDeleted = ExecuteArtefactUpdateOrDeleteQuery(state, "DELETE FROM N_ARTEFACT WHERE ART_ID = {0}", primaryKey);
            ExecuteArtefactUpdateOrDeleteQuery(state, "DELETE FROM ARTEFACT_BASE WHERE ART_BASE_ID NOT IN (SELECT ART_BASE_ID FROM N_ARTEFACT WHERE ART_ID != {0})", primaryKey);

            // we need the artefacts that were deleted. Only from N_ARTEFACT
            return artefactDeleted;
        }

        /// <summary>
        /// Executes an update or delete query.
        /// </summary>
        /// <param name="state">The transaction</param>
        /// <param name="query">The query to execute.</param>
        /// <param name="primaryKey">The primary key to use in the query.</param>
        /// <returns>The number of affected records.</returns>
        protected int ExecuteArtefactUpdateOrDeleteQuery(DbTransactionState state, string query, long primaryKey)
        {
            return state.UsingLogger().ExecuteNonQueryFormat(query, state.Database.CreateInParameter("artId", DbType.Int64, primaryKey));
        }

        /// <summary>
        /// Delete the specified <paramref name="objects"/> from Mapping Store if they exist.
        /// </summary>
        /// <param name="objects">
        /// The objects.
        /// </param>
        /// <typeparam name="T">
        /// The type of maintainable object
        /// </typeparam>
        /// <returns>
        /// The <see cref="IList{ArtefactImportStatus}"/>.
        /// </returns>
        protected IList<ArtefactImportStatus> DeleteObjects<T>(IEnumerable<T> objects) where T : IMaintainableObject
        {
            List<ArtefactImportStatus> importStatusList = new List<ArtefactImportStatus>();
            foreach (var maintainableObject in objects)
            {
                using (var scope = new SdmxAuthorizationScope(Mapping.Api.Constant.Authorisation.Optional))
                {
                    if (scope.IsEnabled && !scope.CurrentSdmxAuthorisation.CanDelete(maintainableObject))
                    {
                        ImportMessage importMessage = maintainableObject.AsReference.GetErrorMessage(new SdmxUnauthorisedException("Not authorized to delete artefact"), DatasetActionEnumType.Delete);
                        importStatusList.Add(new ArtefactImportStatus(-1, importMessage));
                        continue;
                    }
                }

                using (DbTransactionState state = DbTransactionState.Create(this.database))
                {
                    ArtefactImportStatus importStatus = null;
                    try
                    {
                        IStructureReference structureReference = maintainableObject.AsReference;
                        var status = GetFinalStatus(state.Database, structureReference);
                        bool exists = status != null && status.PrimaryKey > 0;
                        if (exists)
                        {
                            EnsureNotUsed(state, maintainableObject, status, DatasetActionEnumType.Delete);
                            _log.DebugFormat(
                                CultureInfo.InvariantCulture,
                                "Deleting artefact record {0}.",
                                structureReference.GetAsHumanReadableString());
                            this.Delete(state, status.PrimaryKey, maintainableObject);
                            importStatus = new ArtefactImportStatus(
                                status.PrimaryKey,
                                new ImportMessage(
                                    ImportMessageStatus.Success,
                                    maintainableObject.AsReference,
                                    "Deleted artefact", DatasetActionEnumType.Delete));
                        }
                        else
                        {
                            // TODO return status
                            var errorMessage = "because the artefact doesn't exist";
                            _log.Warn(errorMessage);
                            throw new SdmxNoResultsException(errorMessage);
                        }

                        state.Commit();
                    }
                    catch (SdmxException e)
                    {
                        state.RollBack();
                        _log.Warn(maintainableObject.Urn, e);
                        importStatus = new ArtefactImportStatus(-1, maintainableObject.AsReference.GetErrorMessage(e, DatasetActionEnumType.Delete));
                    }
                    catch (Exception e)
                    {
                        state.RollBack();
                        _log.Warn(maintainableObject.Urn, e);
                        throw;
                    }

                    if (importStatus != null)
                    {
                        importStatusList.Add(importStatus);
                    }
                }
            }

            return importStatusList;
        }

        /// <summary>
        /// Ensures the not used.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="artefact">
        /// The artefact.
        /// </param>
        /// <param name="finalStatus">
        /// The final status.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <exception cref="SdmxConflictException">
        /// Cannot replace artefact. Non-Final artefact is used by other artefacts
        /// </exception>
        protected void EnsureNotUsed(DbTransactionState state, IMaintainableObject artefact, ArtefactFinalStatus finalStatus, DatasetActionEnumType action)
        {
            IList<string> crossReferences;
            bool isUsed = IsUsed(state, artefact.AsReference, finalStatus, out crossReferences, action, out var crossReferencingObjects);

            // How to handle Non-Final artefacts. Updating a Codelist is easy but updating a DSD or MSD 
            if (isUsed)
            {
                var conflictMessage = "because it is being used by :" + crossReferences.Aggregate((i, j) => i + "," + j).TrimEnd(',');
                if (action == DatasetActionEnumType.Replace)
                {
                    throw new SdmxConflictException(conflictMessage);
                }

                if (action == DatasetActionEnumType.Delete)
                {
                    throw new SdmxConflictException(conflictMessage);
                }
            }
        }

        /// <summary>
        /// Determines whether the specified <paramref name="artefact"/> is used.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="artefact">The artefact.</param>
        /// <param name="finalStatus">The final status.</param>
        /// <param name="crossReferences">The cross reference strings.</param>
        /// <param name="crossReferencingObjects"></param>
        /// <returns><c>true</c> if the specified <paramref name="artefact"/> is used; otherwise, <c>false</c>.</returns>
        private bool IsUsed(DbTransactionState state, IStructureReference artefact, ArtefactFinalStatus finalStatus, out IList<string> crossReferences, DatasetActionEnumType action, out IList<IStructureReference> crossReferencingObjects)
        {
            // TODO Those parameters are NOT MANDATORY. Using REST because OTHER doesn't set auto computed fields like hasSpecificMaintainanableIds
            // TODO the hasSpecific* fields MUST work the same regardless of COMMON_STRUCTURE_QUERY_TYPE value
            // TODO build a builder that takes a structure reference and builds a CommonStructureQuery
            // TODO also some information like maintainable target is MANDATORY
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV1StructureDocument))
                .SetAgencyIds(artefact.AgencyId)
                .SetMaintainableIds(artefact.MaintainableId)
                .SetVersionRequests(new VersionRequestCore(artefact.Version))
                .SetMaintainableTarget(artefact.MaintainableStructureEnumType)
                .Build();

            // We don't need to get the whole sdmx mutable beans here, we just need to see if they are used.
            ArtefactCommandBuilder commandBuilder = new ArtefactCommandBuilder(state.Database);
            crossReferencingObjects = retrievalEngineContainer.GetEngine(artefact.MaintainableStructureEnumType)
                    .RetrieveResolvedParents(structureQuery).ToList();
            crossReferences = crossReferencingObjects.Select(x => x.GetAsHumanReadableString()).ToList();

            foreach (var crossReferencingBean in crossReferencingObjects)
            {
                //in case of replace, we don't want to delete the dataflow because that causes the categorisation to get deleted as well
                //in case of delete, we want to be able to delete the dataflow even if it belongs in a category (linked by a categorisation)
                if (crossReferencingBean.MaintainableStructureEnumType.EnumType != SdmxStructureEnumType.Categorisation)
                {
                    return true;
                }
                else
                {
                    if (action != DatasetActionEnumType.Delete)
                    {
                        return true;
                    }
                }
            }
            // Dataflow has also a link to Mapping Set
            if (artefact.TargetReference.EnumType == SdmxStructureEnumType.Dataflow && !ConfigurationProvider.AutoDeleteMappingSets)
            {
                var mappingSetResult = GetMappingSetId(state, finalStatus);
                if (mappingSetResult.Count > 0)
                {
                    crossReferences.AddAll(mappingSetResult);
                    return true;
                }
            }

            return false;
        }

        private static IList<string> GetMappingSetId(DbTransactionState state, ArtefactFinalStatus finalStatus)
        {
            // Note SQL Query is different than Java because ENTITY_SDMX_REF.TARGET_ARTEFACT points to ART_BASE_ID !
            // and it returns only the OBJECT_ID as this is what is used
            // and it is a list because we might have multiple mappings sets per dataflow
            return state.Database.QuerySimple<string>("select eb.OBJECT_ID" +
                " from ENTITY_REF_CHILDREN erc" +
                " inner join ENTITY_BASE eb on erc.ENTITY_ID = eb.ENTITY_ID" +
                " where eb.ENTITY_TYPE = 'MappingSet'" +
                " and ART_ID = {0} ", new Int64Parameter(finalStatus.PrimaryKey)).ToArray();
        }

        /// <summary>
        /// Insert or replace the list of maintainable.
        /// </summary>
        /// <typeparam name="T">The <see cref="IMaintainableObject" /> based interface</typeparam>
        /// <param name="maintainables">The list of maintainable.</param>
        /// <param name="action">The action.</param>
        /// <returns>
        /// The <see cref="IEnumerable{ArtefactImportStatus}" />.
        /// </returns>
        protected IEnumerable<ArtefactImportStatus> ReplaceOrInsert<T>(IEnumerable<T> maintainables, DatasetAction action) where T : IMaintainableObject
        {
            List<ArtefactImportStatus> statuses = new List<ArtefactImportStatus>();
            foreach (var artefact in maintainables)
            {
                using (DbTransactionState state = DbTransactionState.Create(this.database))
                {
                    try
                    {
                        statuses.AddRange(this.ReplaceOrInsert(state, artefact, action));
                        state.Commit(); 
                    }
                    catch (MappingStoreException e)
                    {
                        _log.Error(artefact.Urn.ToString(), e);
                        state.RollBack();
                        statuses.Add(new ArtefactImportStatus(-1, artefact.AsReference.GetErrorMessage(e, action)));
                    }
                    catch (DbException e)
                    {
                        _log.Error(artefact.Urn.ToString(), e);
                        state.RollBack();
                        statuses.Add(new ArtefactImportStatus(-1, artefact.AsReference.GetErrorMessage(e, action)));
                    }
                    catch (SdmxException e)
                    {
                        _log.Error(artefact.Urn.ToString(), e);
                        state.RollBack();
                        statuses.Add(new ArtefactImportStatus(-1, artefact.AsReference.GetErrorMessage(e, action)));
                    }
                }
            }
            return statuses;
        }

        /// <summary>
        ///     Replace or insert the specified <paramref name="artefact" /> to MAPPING STORE
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="artefact">
        ///     The artefact.
        /// </param>
        /// <param name="action">
        ///     The action
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="state" /> is null
        ///     -or-
        ///     <paramref name="artefact" /> is null
        /// </exception>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        protected List<ArtefactImportStatus> ReplaceOrInsert(DbTransactionState state, IMaintainableObject artefact, DatasetAction action)
        {
            if (action.EnumType == DatasetActionEnumType.Delete)
            {
                throw new SdmxSemmanticException("Delete action used when Delete/Update was expected");
            }

            if (state == null)
            {
                throw new ArgumentNullException("state");
            }

            if (artefact == null)
            {
                throw new ArgumentNullException("artefact");
            }

            var structureReference = artefact.AsReference;
            _log.DebugFormat(CultureInfo.InvariantCulture, "Replacing or Insert artefact = {0}", structureReference);
            CheckPrerequisites(artefact);
            var finalStatus = GetFinalStatus(state.Database, structureReference);

            var exists = finalStatus != null && finalStatus.PrimaryKey > 0;
            if (!exists)
            {
                using (var scope = new SdmxAuthorizationScope(Authorisation.Optional))
                {
                    if (scope.IsEnabled && !scope.CurrentSdmxAuthorisation.CanInsert(artefact))
                    {
                        return new List<ArtefactImportStatus>()
                        {
                            new ArtefactImportStatus(-1, artefact.AsReference.GetErrorMessage(
                                new SdmxUnauthorisedException("Not authorized to insert artefact"), DatasetActionEnumType.Append))
                        };
                    }
                }
                return new List<ArtefactImportStatus>()
                {
                    this.InsertArtefact(state, artefact)
                };
            }

            using (var scope = new SdmxAuthorizationScope(Authorisation.Optional))
            {
                if (scope.IsEnabled && !scope.CurrentSdmxAuthorisation.CanUpdate(artefact))
                {
                    return new List<ArtefactImportStatus>()
                    {
                        new ArtefactImportStatus(-1, artefact.AsReference.GetErrorMessage(
                            new SdmxUnauthorisedException("Not authorized to update artefact"), DatasetActionEnumType.Replace))
                    };
                }
            }

            return ReplaceArtefact(state, artefact, finalStatus);
        }

        /// <summary>
        /// Replaces the artefact.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="artefact">The artefact.</param>
        /// <param name="finalStatus">The final status.</param>
        /// <returns>The <see cref="ArtefactImportStatus"/></returns>
        protected List<ArtefactImportStatus> ReplaceArtefact(DbTransactionState state, IMaintainableObject artefact, ArtefactFinalStatus finalStatus)
        {
            IList<IMaintainableMutableObject> crossReferencingObjects = new List<IMaintainableMutableObject>();
            bool isUsed = IsUsed(state, artefact.AsReference, finalStatus,
                out IList<string> crossReferences, DatasetActionEnumType.Replace, out var crossStructureReferences);
            // TODO Low priority, unless somthing breaks.
            // populate cross Referencing beans from crossStructureReferences
            if (!finalStatus.IsFinal)
            {
                // Dataflow is used by many entities like MappingSet, DataSource , Header template , permissions
                if (!isUsed && artefact.StructureType.EnumType != SdmxStructureEnumType.Dataflow)
                {
                    return DeleteInsert(state, artefact, finalStatus);
                }
                else
                {
                    return UpdateNonFinalInUse(state, artefact, finalStatus, crossReferences, crossReferencingObjects);
                }
            }
            return new List<ArtefactImportStatus>
            {
                UpdateArtefact(state, artefact, finalStatus, crossReferencingObjects)
            };
        }

        /// <summary>
        /// Updates a non-final artefact that is in use.
        /// </summary>
        /// <param name="state">The madb transaction.</param>
        /// <param name="artefact">The artefact to update</param>
        /// <param name="finalStatus"></param>
        /// <param name="crossReferences">The other artefacts that reference the artefact to be updated.</param>
        /// <param name="crossReferencingObjects"></param>
        /// <returns></returns>
        protected virtual List<ArtefactImportStatus> UpdateNonFinalInUse(DbTransactionState state, IMaintainableObject artefact, ArtefactFinalStatus finalStatus,
            IList<string> crossReferences, IList<IMaintainableMutableObject> crossReferencingObjects)
        {
            var updateStatus = UpdateArtefact(state, artefact, finalStatus, crossReferencingObjects);
            string message;
            if (crossReferences.Count > 0)
            {
                var structuresMessage = crossReferences.Aggregate((i, j) => i + "," + j).TrimEnd(',');
                message = "Other content changes cannot be done until the following dependent artefacts have been deleted: " + structuresMessage;
            }
            else
            {
                message = string.Empty;
            }

            var updateMessage = updateStatus.ImportMessage.Message + Environment.NewLine + message;
            return new List<ArtefactImportStatus>
            {
                new ArtefactImportStatus(finalStatus.PrimaryKey,
                    new ImportMessage(updateStatus.ImportMessage.Status, artefact.AsReference, updateMessage, updateStatus.ImportMessage.ActualAction))
            };
        }

        private ArtefactImportStatus UpdateArtefact(DbTransactionState state, IMaintainableObject artefact, ArtefactFinalStatus finalStatus, IList<IMaintainableMutableObject> crossReferencingObjects)
        {
            var updatedRecords = _updateEngine.UpdatedNonFinalAttributes(artefact, state.Database, finalStatus.PrimaryKey);
            ArtefactImportStatus status = null;
            if (!finalStatus.IsFinal)
            {
                status = this.HandleNonFinalArtefactReplace(state, artefact, finalStatus, crossReferencingObjects);
            }
            else
            {
                status = this.HandleFinalArtefactReplace(state, artefact, finalStatus);
            }

            updatedRecords += _updateEngine.UpdateIdentifiableComposites(
                artefact,
                state,
                finalStatus);

            if (artefact.IsFinal.IsTrue && !finalStatus.IsFinal)
            {
                switch (artefact.StructureType.ToEnumType())
                {

                    case SdmxStructureEnumType.Dataflow:
                    case SdmxStructureEnumType.CodeList:
                    case SdmxStructureEnumType.CategoryScheme:
                    case SdmxStructureEnumType.ConceptScheme:
                    case SdmxStructureEnumType.Dsd:
                        updatedRecords += this._updateEngine.UpdateFinalFlag(artefact, state);
                        break;
                    default:
                        break;
                }
            }

            if (updatedRecords > 0 || (status != null && status.ImportMessage.Status == ImportMessageStatus.Success))
            {
                _updateEngine.UpdateLastModified(state.Database, finalStatus.PrimaryKey);
            }

            return artefact.BuildUpdatedStatus(finalStatus, status, updatedRecords);
        }

        /// <summary>
        /// Returns the final status (id and is final value) of the specified <paramref name="reference" />; otherwise it
        /// returns null
        /// </summary>
        /// <param name="database"></param>
        /// <param name="reference">The structure reference.</param>
        /// <returns>
        /// The <see cref="ArtefactFinalStatus" /> of the specified <paramref name="reference" />; otherwise it returns null.
        /// </returns>
        public ArtefactFinalStatus GetFinalStatus(Database database, IStructureReference reference)
        {
            StructureCache cache = StructureCacheContext.GetStructureCacheFromThreadLocal() != null ? 
                                            StructureCacheContext.GetStructureCacheFromThreadLocal() : 
                                            new StructureCache();
            return cache.GetArtefactFinalStatusOfSpecificStructure(database, reference);
        }

        /// <summary>
        ///  Returns the final status of referenced child
        /// </summary>
        /// <param name="database"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        public ArtefactFinalStatus GetFinalStatusOfReferenceOfChild(Database database, IStructureReference reference)
        {
            StructureCache cache = StructureCacheContext.GetStructureCacheFromThreadLocal() != null ?
                                            StructureCacheContext.GetStructureCacheFromThreadLocal() :
                                            new StructureCache();
            return cache.GetArtefactFinalStatus(database, reference);
        }

        /// <summary>
        ///     Run common artefact import command.
        /// </summary>
        /// <param name="artefact">
        ///     The artefact.
        /// </param>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="artefactStoredProcedure">
        ///     The artefact stored procedure.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        protected ArtefactImportStatus RunArtefactCommand(IMaintainableObject artefact, DbCommand command, ArtefactProcedurebase artefactStoredProcedure)
        {
            DbParameter versionParameter = artefactStoredProcedure.CreateVersionParameter(command);
            if (artefact.Version != null)
            {
                versionParameter.Value = artefact.Version;
            }

            DbParameter agencyParameter = artefactStoredProcedure.CreateAgencyParameter(command);
            if (artefact.AgencyId != null)
            {
                agencyParameter.Value = artefact.AgencyId;
            }

            DbParameter validFromParameter = artefactStoredProcedure.CreateValidFromParameter(command);
            if (artefact.StartDate != null)
            {
                validFromParameter.Value = artefact.StartDate.Date;
            }

            DbParameter validToParameter = artefactStoredProcedure.CreateValidToParameter(command);
            if (artefact.EndDate != null)
            {
                validToParameter.Value = artefact.EndDate.Date;
            }

            DbParameter uriParameter = artefactStoredProcedure.CreateUriParameter(command);
            if (artefact.Uri != null)
            {
                uriParameter.Value = artefact.Uri.ToString();
            }

            DbParameter isFinalParameter = artefactStoredProcedure.CreateIsFinalParameter(command);
            isFinalParameter.Value = artefact.IsFinal.IsTrue ? 1 : 0;

            DbParameter lastModifiedParameter = artefactStoredProcedure.CreateLastModifiedParameter(command);
            lastModifiedParameter.Value = DateTime.UtcNow;

            if (artefactStoredProcedure.SupportsStub())
            {
                DbParameter isStubParameter = artefactStoredProcedure.CreateIsStubParameter(command);
                isStubParameter.Value = artefact.IsExternalReference.IsTrue ? 1 : 0;

                DbParameter serviceUrlParameter = artefactStoredProcedure.CreateServiceUrlParameter(command);
                if (artefact.ServiceUrl != null)
                {
                    serviceUrlParameter.Value = artefact.ServiceUrl.ToString();
                }

                DbParameter structureUrlParameter = artefactStoredProcedure.CreateStructureUrlParameter(command);
                if (artefact.StructureUrl != null)
                {
                    structureUrlParameter.Value = artefact.StructureUrl.ToString();
                }

                DbParameter structureTypeParameter = artefactStoredProcedure.CreateArtefactTypeParameter(command);
                if (artefact is IContentConstraintObject @object && artefact.IsExternalReference.IsTrue)
                {
                    if (@object.IsDefiningActualDataPresent)
                    {
                        structureTypeParameter.Value = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ActualConstraint).UrnClass;
                    }
                    else
                    {
                        structureTypeParameter.Value = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AllowedConstraint).UrnClass;
                    }
                }
                else
                {
                    structureTypeParameter.Value = artefact.StructureType.UrnClass;
                }
            }

            DbParameter sdmxSchemaParameter = artefactStoredProcedure.CreateSdmxSchemaParameter(command);
            sdmxSchemaParameter.Value = 0;

            var artID = this.RunNameableArtefactCommand(artefact, command, artefactStoredProcedure);

            var structureReference = artefact.AsReference;
            var importMessage = new ImportMessage(ImportMessageStatus.Success, structureReference, string.Format(Resources.SuccessInsertFormat2, structureReference.GetAsHumanReadableString(), Environment.NewLine), DatasetActionEnumType.Append);

            return new ArtefactImportStatus(artID, importMessage);
        }

        /// <summary>
        ///     Run common artefact import command.
        /// </summary>
        /// <param name="artefact">
        ///     The artefact.
        /// </param>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="artefactStoredProcedure">
        ///     The artefact stored procedure.
        /// </param>
        /// <returns>
        ///     The artefact id
        /// </returns>
        protected long RunNameableArtefactCommand(IMaintainableObject artefact, DbCommand command, ArtefactProcedurebase artefactStoredProcedure)
        {
            var artID = this.RunIdentifiableArterfactCommand(artefact, command, artefactStoredProcedure);

            _localisedStringInsertEngine.InsertForArtefact(artID, artefact, new Database(this.database, command.Transaction));

            return artID;
        }

        /// <summary>
        ///     Run common artefact import command.
        /// </summary>
        /// <param name="artefact">
        ///     The artefact.
        /// </param>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="artefactStoredProcedure">
        ///     The artefact stored procedure.
        /// </param>
        /// <returns>
        ///     The artefact id.
        /// </returns>
        protected long RunIdentifiableArterfactCommand(IIdentifiableObject artefact, DbCommand command, ArtefactProcedurebase artefactStoredProcedure)
        {
            DbParameter idParameter = artefactStoredProcedure.CreateIdParameter(command);
            idParameter.Value = artefact.Id ?? (object)DBNull.Value;

            DbParameter outputParameter = artefactStoredProcedure.CreateOutputParameter(command);

            command.ExecuteNonQueryAndLog();

            var artID = (long)outputParameter.Value;

            _annotationInsertEngine.Insert(
                new DbTransactionState(command.Transaction, this.database),
                artID,
                _insertArtefactAnnotation,
                artefact.Annotations);

            return artID;
        }

        private List<long> GetCategorisationsForArtefact(DbTransactionState state, long artefactId)
        {
            List<long> result = new List<long>();
            string query = "SELECT rs.SOURCE_ARTEFACT" +
                " FROM REFERENCE_SOURCE rs" +
                " INNER JOIN N_ARTEFACT na ON (na.ART_ID = rs.SOURCE_ARTEFACT)" +
                " INNER JOIN ARTEFACT_BASE ab ON (ab.ART_BASE_ID = na.ART_BASE_ID)" +
                " WHERE REF_SRC_ID IN (SELECT REF_SRC_ID FROM structure_ref WHERE TARGET_ARTEFACT IN (SELECT ART_BASE_ID FROM N_ARTEFACT WHERE ART_ID = {0}))" +
                " AND ab.ARTEFACT_TYPE = {1}";
            var artIdParam = state.Database.CreateInParameter("artId", DbType.Int64, artefactId);
            var structureTypeParam = state.Database.CreateInParameter("artType", DbType.AnsiString, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation).StructureType);
            try
            {
                using (var cmd = state.UsingLogger().Database.GetSqlStringCommandFormat(query, artIdParam, structureTypeParam))
                using (var reader = state.Database.ExecuteReader(cmd))
                {
                    var pos = reader.GetOrdinal("SOURCE_ARTEFACT");
                    while (reader.Read())
                    {
                        result.Add(reader.GetInt64(pos));
                    }
                }
            }
            catch (DbException e)
            {
                throw new SdmxException(e, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), "Error while retrieving the categorisations for artefact");
            }
            return result;
        }
    }
}
