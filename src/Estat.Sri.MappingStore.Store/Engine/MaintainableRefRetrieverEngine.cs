// -----------------------------------------------------------------------
// <copyright file="MaintainableRefRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2013-07-11
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    using Dapper;

    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.Sdmx.MappingStore.Store.Properties;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The maintainable ref retriever engine.
    /// </summary>
    public class MaintainableRefRetrieverEngine
    {
        /// <summary>
        ///     The SQL query for from PK format. 1 parameter the Primary Key value
        /// </summary>
        private const string SqlQueryFromPk = "SELECT A.AGENCY as AgencyId, A.ID as MaintainableId, A.VERSION FROM ARTEFACT_VIEW A WHERE A.ART_ID = {0}";

        /// <summary>
        /// The artefact query format
        /// </summary>
        private const string ArtefactQueryFormat = "SELECT A.ART_ID as SYSID, A.AGENCY, A.ID, A.VERSION FROM ARTEFACT_VIEW A WHERE A.ART_ID in (SELECT {0} FROM {1})";

        /// <summary>
        ///     The SQL query template.7 parameter: ID, AGENCY, VERSION1, VERSION2, VERSION3, {primary key field}, {table name}
        /// </summary>
        private const string SqlQueryFromRef = "SELECT AB.ART_BASE_ID FROM ARTEFACT A INNER JOIN ARTEFACT_BASE AB on AB.ART_BASE_ID = A.ART_BASE_ID " +
                                               "WHERE ({0} is null OR A.ID = {0}) AND ({1} is null OR A.AGENCY = {1}) and ({2} is NULL OR " +
                                               "A.Version1 ={2} AND A.Version2 = {3} AND A.Version3 = {4}) AND A.ART_ID in (SELECT {5} FROM {6} )";

        /// <summary>
        /// The ITEM with parent query format
        /// </summary>
        private const string ItemWithParentFormat = "SELECT I.ITEM_ID as SYSID, A.AGENCY, A.ID, A.VERSION, I.ID as IID, T.{3} as PARENTITEM_ID FROM ARTEFACT_VIEW A INNER JOIN {1} T ON A.ART_ID = T.{2} INNER JOIN ITEM I ON I.ITEM_ID = T.{0} ";

        /// <summary>
        /// The ITEM IDs of user query format
        /// </summary>
        private const string ItemIdsOfUserFormat = "SELECT I.ITEM_ID as SYSID FROM ARTEFACT_VIEW A INNER JOIN {1} T ON A.ART_ID = T.{2} INNER JOIN ITEM I ON I.ITEM_ID = T.{0}";

        /// <summary>
        /// The ITEM query format
        /// </summary>
        private const string ItemQueryFormat = "SELECT I.ITEM_ID as SYSID, A.AGENCY, A.ID, A.VERSION, I.ID as IID FROM ARTEFACT_VIEW A INNER JOIN {1} T ON A.ART_ID = T.{2} INNER JOIN ITEM I ON I.ITEM_ID = T.{0}";

        /// <summary>
        /// The ITEM query format. Parameters :
        /// 0: Child table Primary Key field name
        /// 1: Child table name
        /// 2: Child table foreign key field name
        /// 3: Artefact ID parameter name
        /// 4: Artefact Agency ID parameter name
        /// 5: Version 1
        /// 6: Version 2
        /// 7: Version 3
        /// 8: Item ID parameter name
        /// </summary>
        private const string ItemQueryFromRefFormat = "SELECT A.ART_BASE_ID as SYSID FROM ARTEFACT A INNER JOIN ITEM I ON A.ART_ID = I.PARENT_ITEM_SCHEME WHERE ({0} is null OR A.ID = {0}) AND ({1} is null OR A.AGENCY = {1}) AND ({2} is NULL OR " +
                                                      "A.VERSION1 ={2} AND A.VERSION2 = {3} AND A.VERSION3 = {4}) AND I.ID = {5}";

        /// <summary>
        ///     The _item table information builder
        /// </summary>
        private readonly ItemTableInfoBuilder _itemTableInfoBuilder = new ItemTableInfoBuilder();

        /// <summary>
        ///     The _mapping store database.
        /// </summary>
        private readonly Database _mappingStoreDatabase;

        /// <summary>
        ///     The _mutable retrieval manager.
        /// </summary>
        private readonly ISdmxMutableObjectRetrievalManager _mutableRetrievalManager;

        /// <summary>
        ///     The table info builder.
        /// </summary>
        private readonly TableInfoBuilder _tableInfoBuilder = new TableInfoBuilder();

        /// <summary>
        /// Initializes a new instance of the <see cref="MaintainableRefRetrieverEngine"/> class.
        /// </summary>
        /// <param name="mappingStoreDatabase">The mapping store database.</param>
        /// <param name="mutableRetrievalManager">The mutable retrieval manager.</param>
        public MaintainableRefRetrieverEngine(Database mappingStoreDatabase, ISdmxMutableObjectRetrievalManager mutableRetrievalManager)
        {
            this._mappingStoreDatabase = mappingStoreDatabase;
            this._mutableRetrievalManager = mutableRetrievalManager;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MaintainableRefRetrieverEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDatabase">
        ///     The mapping store database.
        /// </param>
        public MaintainableRefRetrieverEngine(Database mappingStoreDatabase)
        {
            this._mappingStoreDatabase = mappingStoreDatabase;
            this._mutableRetrievalManager = new MappingStoreRetrievalManager(this._mappingStoreDatabase);
        }

        /// <summary>
        ///     Returns a <see cref="IMaintainableRefObject" /> populate by the contents of the record in <c>ARTEFACT_VIEW</c> for
        ///     <c>ART_ID = </c> <paramref name="primaryKeyValue" />
        /// </summary>
        /// <param name="primaryKeyValue">
        ///     The primary key value.
        /// </param>
        /// <returns>
        ///     The <see cref="IMaintainableRefObject" />; otherwise null.
        /// </returns>
        public IMaintainableRefObject Retrieve(long primaryKeyValue)
        {
            string parameterName = this._mappingStoreDatabase.BuildParameterName("pk");
            var query = string.Format(CultureInfo.InvariantCulture, SqlQueryFromPk, parameterName);
            using (var connection = this._mappingStoreDatabase.CreateConnection())
            {
                connection.Open();
                return connection.Query<MaintainableRefObjectImpl>(query, new { pk = primaryKeyValue }).FirstOrDefault();
            }
        }

        /// <summary>
        ///     Retrieve the primary key from mapping store for the given <paramref name="structureReference" />
        /// </summary>
        /// <param name="structureReference">
        ///     The structure reference.
        /// </param>
        /// <returns>
        ///     The primary key value; otherwise <c>-1</c>.
        /// </returns>
        public long Retrieve(IStructureReference structureReference)
        {
            var maintainableRef = structureReference.MaintainableReference;
            var splitVersion = maintainableRef.SplitVersion(3);
            var parameters = new List<DbParameter>();
            string query;
            if (!structureReference.HasChildReference())
            {
                var tableInfo = this._tableInfoBuilder.Build(structureReference.MaintainableStructureEnumType.EnumType);

                query = string.Format(CultureInfo.InvariantCulture, SqlQueryFromRef, this.BuildParam("ID"), this.BuildParam("AGENCY"), this.BuildParam("VERSION1"), this.BuildParam("VERSION2"), this.BuildParam("VERSION3"), tableInfo.PrimaryKey, tableInfo.Table);
            }
            else
            {
                query = string.Format(CultureInfo.InvariantCulture, ItemQueryFromRefFormat,this.BuildParam("ID"), this.BuildParam("AGENCY"), this.BuildParam("VERSION1"), this.BuildParam("VERSION2"), this.BuildParam("VERSION3"), this.BuildParam("itemId"));
                parameters.Add(this._mappingStoreDatabase.CreateInParameter("itemId", DbType.AnsiString, (object)structureReference.IdentifiableIds.LastOrDefault() ?? DBNull.Value));
            }

            parameters.Add(this._mappingStoreDatabase.CreateInParameter("ID", DbType.AnsiString, maintainableRef.HasMaintainableId() ? (object)maintainableRef.MaintainableId : DBNull.Value));
            parameters.Add(this._mappingStoreDatabase.CreateInParameter("AGENCY", DbType.AnsiString, maintainableRef.HasAgencyId() ? (object)maintainableRef.AgencyId : DBNull.Value));

            // TODO fix that after the conclusion of MAT-579
            parameters.Add(this._mappingStoreDatabase.CreateInParameter("VERSION1", DbType.Int64, splitVersion[0].ToDbValue()));
            parameters.Add(this._mappingStoreDatabase.CreateInParameter("VERSION2", DbType.Int64, splitVersion[1].ToDbValue(0)));
            parameters.Add(this._mappingStoreDatabase.CreateInParameter("VERSION3", DbType.Int64, splitVersion[2] == null ? -1 : splitVersion[2].ToDbValue()));

             var executeScalar = this._mappingStoreDatabase.UsingLogger().ExecuteScalar(query, parameters);

            return executeScalar is long ? (long)executeScalar : -1;
        }

        /// <summary>
        ///     Retrieve the primary key from mapping store for the given period code list.
        /// </summary>
        /// <param name="timeFormat">
        ///     The time format.
        /// </param>
        /// <returns>
        ///     The  primary key.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     Value in <paramref name="timeFormat" /> not supported.
        /// </exception>
        public ICodelistMutableObject RetrievePeriodCodelist(TimeFormat timeFormat)
        {
            PeriodObject periodObject;
            if (PeriodCodelist.PeriodCodelistIdMap.TryGetValue(timeFormat.FrequencyCode, out periodObject))
            {
                var maintainableRef = new MaintainableRefObjectImpl(PeriodCodelist.Agency, periodObject.Id, PeriodCodelist.Version);
                return this._mutableRetrievalManager.GetMutableCodelist(maintainableRef, false, false);
            }

            throw new ArgumentOutOfRangeException("timeFormat", timeFormat, Resources.ErrorNotSupported);
        }

        /// <summary>
        ///     Retrieve the primary key from mapping store for the given period code list.
        /// </summary>
        /// <param name="timeFormat">
        ///     The time format.
        /// </param>
        /// <returns>
        ///     The  primary key.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     Value in <paramref name="timeFormat" /> not supported.
        /// </exception>
        public long RetrievePeriodCodelistId(TimeFormat timeFormat)
        {
            PeriodObject periodObject;
            if (PeriodCodelist.PeriodCodelistIdMap.TryGetValue(timeFormat.FrequencyCode, out periodObject))
            {
                return this.Retrieve(new StructureReferenceImpl(PeriodCodelist.Agency, periodObject.Id, PeriodCodelist.Version, SdmxStructureEnumType.CodeList));
            }

            throw new ArgumentOutOfRangeException("timeFormat", timeFormat, Resources.ErrorNotSupported);
        }

        /// <summary>
        /// Retrieves the urn map.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// The map between System ID to URN
        /// </returns>
        /// <exception cref="System.ArgumentException">sdmxStructure is not a Dataflow or a Category</exception>
        public IDictionary<long, string> RetrievesUrnMapForUser(SdmxStructureType sdmxStructure, long userId)
        {
            if (!sdmxStructure.IsOneOf(SdmxStructureEnumType.Category, SdmxStructureEnumType.Dataflow))
            {
                throw new ArgumentException(string.Format("Structure type : {0} not supported as user permission. Currently only dataflow and category based permissions are supported", sdmxStructure), "sdmxStructure");
            }

            var parameterName = this._mappingStoreDatabase.BuildParameterName("userId");
            if (sdmxStructure.EnumType == SdmxStructureEnumType.Dataflow)
            {
                TableInfo tableInfo = this._tableInfoBuilder.Build(sdmxStructure);
                var dataflowQuery = string.Format(CultureInfo.InvariantCulture, ArtefactQueryFormat, tableInfo.PrimaryKey, tableInfo.Table);
                // TODO DATAFLOW_USER table/view doesn't exist in msdb 7.0
                dataflowQuery = string.Format(CultureInfo.InvariantCulture, "{0} AND A.ART_ID IN (SELECT du.DF_ID FROM DATAFLOW_USER du WHERE du.USER_ID = {1} )", dataflowQuery, parameterName);
                return this.BuildMaintainableUrnMap(sdmxStructure, dataflowQuery, new { userId });
            }

            // category
            return GetFilteredCategories(sdmxStructure, userId, "=", parameterName);
        }

        /// <summary>
        /// Retrieves the urn map.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// The map between System ID to URN
        /// </returns>
        /// <exception cref="System.ArgumentException">sdmxStructure is not a Dataflow or a Category</exception>
        public IDictionary<long, string> RetrievesUrnMapForUser(SdmxStructureType sdmxStructure, string userId)
        {
            if (!sdmxStructure.IsOneOf(SdmxStructureEnumType.Category, SdmxStructureEnumType.Dataflow))
            {
                throw new ArgumentException(string.Format("Structure type : {0} not supported as user permission. Currently only dataflow and category based permissions are supported", sdmxStructure), "sdmxStructure");
            }

            var parameterName = this._mappingStoreDatabase.BuildParameterName("userId");
            var mauserQuery = string.Format(CultureInfo.InvariantCulture, "(SELECT USER_ID FROM MA_USER mu WHERE mu.USERNAME = {0})", parameterName);
            if (sdmxStructure.EnumType == SdmxStructureEnumType.Dataflow)
            {
                TableInfo tableInfo = this._tableInfoBuilder.Build(sdmxStructure);
                var dataflowQuery = string.Format(CultureInfo.InvariantCulture, ArtefactQueryFormat, tableInfo.PrimaryKey, tableInfo.Table);
                dataflowQuery = string.Format(CultureInfo.InvariantCulture, "{0} AND A.ART_ID IN (SELECT du.DF_ID FROM DATAFLOW_USER du WHERE du.USER_ID in {1} )", dataflowQuery, mauserQuery);
                return this.BuildMaintainableUrnMap(sdmxStructure, dataflowQuery, new { userId });
            }

            // category
            return GetFilteredCategories(sdmxStructure, userId, "in", mauserQuery);
        }

        /// <summary>
        /// Retrieves the urn map.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <param name="artefactId">The artefact identifier.</param>
        /// <returns>
        /// The map between System ID to URN
        /// </returns>
        /// <exception cref="NotImplementedException"></exception>
        public IDictionary<long, string> RetrievesUrnMapForSpecificArtefact(SdmxStructureType sdmxStructure, long artefactId)
        {
            var parameterName = this._mappingStoreDatabase.BuildParameterName("artefactId");
            if (sdmxStructure.IsMaintainable)
            {
                TableInfo tableInfo = this._tableInfoBuilder.Build(sdmxStructure);
                var dataflowQuery = string.Format(CultureInfo.InvariantCulture, ArtefactQueryFormat, tableInfo.PrimaryKey, tableInfo.Table);
                dataflowQuery = string.Format(CultureInfo.InvariantCulture, "{0} AND A.ART_ID = {1}", dataflowQuery, parameterName);
                return this.BuildMaintainableUrnMap(sdmxStructure, dataflowQuery, new { artefactId });
            }

            // category
            var itemTableInfo = this._itemTableInfoBuilder.Build(sdmxStructure);
            if (itemTableInfo == null)
            {
                throw new NotImplementedException(sdmxStructure.ToString());
            }

            var query = string.Format(CultureInfo.InvariantCulture, ItemWithParentFormat, itemTableInfo.PrimaryKey, itemTableInfo.Table, itemTableInfo.ForeignKey, itemTableInfo.ParentItem);
            query = string.Format(CultureInfo.InvariantCulture, "{0} WHERE T.{1} = {2} ", query, itemTableInfo.ForeignKey, parameterName);
            return this.RetrieveItemUrnMapWithParent(sdmxStructure, query, new { artefactId });
        }

        /// <summary>
        ///     Retrieves the urn map.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <returns>
        ///     The map between System ID to URN
        /// </returns>
        public IDictionary<long, string> RetrievesUrnMap(SdmxStructureType sdmxStructure)
        {
            if (sdmxStructure.IsMaintainable)
            {
                return this.BuildMaintainableUrnMap(sdmxStructure);
            }

            var itemTableInfo = this._itemTableInfoBuilder.Build(sdmxStructure);
            if (itemTableInfo == null)
            {
                throw new NotImplementedException(sdmxStructure.ToString());
            }

            string query;
            if (!string.IsNullOrEmpty(itemTableInfo.ParentItem) && sdmxStructure.EnumType.IsOneOf(SdmxStructureEnumType.Category, SdmxStructureEnumType.Agency))
            {
                query = string.Format(CultureInfo.InvariantCulture, ItemWithParentFormat, itemTableInfo.PrimaryKey, itemTableInfo.Table, itemTableInfo.ForeignKey, itemTableInfo.ParentItem);
                return this.RetrieveItemUrnMapWithParent(sdmxStructure, query);
            }

            query = string.Format(CultureInfo.InvariantCulture, ItemQueryFormat, itemTableInfo.PrimaryKey, itemTableInfo.Table, itemTableInfo.ForeignKey);
            return this.RetrieveItemUrnMap(sdmxStructure, query);
        }

        /// <summary>
        ///     Retrieves the urn map.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <returns>
        ///     The map between System ID to URN
        /// </returns>
        /// <exception cref="NotImplementedException">Unsupported artefact</exception>
        public IDictionary<string, long> RetrievesUrnToSysIdMap(SdmxStructureType sdmxStructure)
        {
            if (sdmxStructure.IsMaintainable)
            {
                return this.RetrievesUrnToSysIdMapWhenMaintainable(sdmxStructure);
            }

            return this.RetrievesUrnToSysIdMapWhenNotMaintable(sdmxStructure);
        }

        /// <summary>
        ///     Get filtered categories for the user.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="condition">The condition.</param>
        /// <param name="filter">The filter.</param>
        /// <returns>
        ///     The map between System ID to URN
        /// </returns>
        private IDictionary<long, string> GetFilteredCategories(SdmxStructureType sdmxStructure, object userId, string condition, string filter)
        {
            var itemTableInfo = this._itemTableInfoBuilder.Build(sdmxStructure);
            if (itemTableInfo == null)
            {
                throw new NotImplementedException(sdmxStructure.ToString());
            }

            var query = string.Format(CultureInfo.InvariantCulture, ItemWithParentFormat, itemTableInfo.PrimaryKey, itemTableInfo.Table, itemTableInfo.ForeignKey, itemTableInfo.ParentItem);
            var allItemUrnMaps = this.RetrieveItemUrnMapWithParent(sdmxStructure, query);

            query = string.Format(CultureInfo.InvariantCulture, ItemIdsOfUserFormat, itemTableInfo.PrimaryKey, itemTableInfo.Table, itemTableInfo.ForeignKey);
            query = string.Format(CultureInfo.InvariantCulture, "{0} WHERE T.{1} IN (SELECT cu.CAT_ID FROM CATEGORY_USER cu WHERE cu.USER_ID {2} {3} )", query, itemTableInfo.PrimaryKey, condition, filter);
            var userItemIds = this.RetrieveItemIdsForUser(query, userId);

            return allItemUrnMaps.Where(x => userItemIds.Contains(x.Key)).ToDictionary(x => x.Key, x => x.Value);
        }

        /// <summary>
        /// Builds the maintainable query.
        /// </summary>
        /// <param name="tableInfo">The table information.</param>
        /// <returns>The SQL Query.</returns>
        private static string BuildMaintainableQuery(TableInfo tableInfo)
        {
            return string.Format(CultureInfo.InvariantCulture, ArtefactQueryFormat, tableInfo.PrimaryKey, tableInfo.Table);
        }

        /// <summary>
        /// Builds the no maintainable query.
        /// </summary>
        /// <param name="itemTableInfo">The item table information.</param>
        /// <returns>The SQL Query.</returns>
        private static string BuildNoMaintainableQuery(ItemTableInfo itemTableInfo)
        {
            return string.Format(CultureInfo.InvariantCulture, ItemQueryFormat, itemTableInfo.PrimaryKey, itemTableInfo.Table, itemTableInfo.ForeignKey);
        }

        /// <summary>
        /// Gets the retrieve item URN map with parent.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <param name="enumerable">The enumerable.</param>
        /// <returns>The Primary Key to URN map</returns>
        private static IDictionary<long, string> GetRetrieveItemUrnMapWithParent(SdmxStructureType sdmxStructure, dynamic[] enumerable)
        {
            var objects = enumerable.ToLookup(o => new MaintainableRefObjectImpl(o.AGENCY, o.ID, o.VERSION));

            IDictionary<long, string> dictionary = new Dictionary<long, string>();
            foreach (var kv in objects)
            {
                var childParentMap = kv.Where(o1 => o1.PARENTITEM_ID != null).ToDictionary(o => (long)o.SYSID, o => (string)o.PARENTITEM_ID);
                foreach (var o in kv)
                {
                    Uri generateUrn = null;
                    if(string.IsNullOrEmpty(o.PARENTITEM_ID))
                    {
                        generateUrn = sdmxStructure.GenerateUrn(o.AGENCY, o.ID, o.VERSION,o.IID);
                    }
                    else
                    {
                        var fullPathOfParent = ((string)o.PARENTITEM_ID).Split('.').ToList();
                        fullPathOfParent.Add(o.IID);
                        generateUrn = sdmxStructure.GenerateUrn(o.AGENCY, o.ID, o.VERSION, fullPathOfParent.ToArray());
                    }
                    long systemId = o.SYSID;
                    dictionary.Add(systemId, generateUrn.ToString());
                }
            }

            return dictionary;
        }

        /// <summary>
        /// Builds the parameter name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>The parameter name.</returns>
        private string BuildParam(string name)
        {
            return this._mappingStoreDatabase.BuildParameterName(name);
        }

        /// <summary>
        /// Retrieves the item urn map.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <param name="query">The query.</param>
        /// <returns>The item URN map</returns>
        private IDictionary<long, string> RetrieveItemUrnMap(SdmxStructureType sdmxStructure, string query)
        {
            IDictionary<long, string> dictionary = new Dictionary<long, string>();
            var enumerable = this._mappingStoreDatabase.Query<dynamic>(query).ToArray();

            foreach (var o in enumerable)
            {
                Uri generateUrn = sdmxStructure.GenerateUrn(o.AGENCY, o.ID, o.VERSION, o.IID);
                long systemId = o.SYSID;
                var result = new KeyValuePair<long, string>(systemId, generateUrn.ToString());
                dictionary.Add(result);
            }

            return dictionary;
        }

        /// <summary>
        /// Retrieves the item urn map with parent.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <param name="query">The query.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// The Primary Key to URN map
        /// </returns>
        private IDictionary<long, string> RetrieveItemUrnMapWithParent(SdmxStructureType sdmxStructure, string query, object parameters = null)
        {
            var enumerable = this._mappingStoreDatabase.Query(query, parameters).ToArray();
            return GetRetrieveItemUrnMapWithParent(sdmxStructure, enumerable);
        }

        private IEnumerable<long> RetrieveItemIdsForUser(string query, object userId)
        {
            return this._mappingStoreDatabase.Query(query, new { userId }).Select(x => (long)x.SYSID);
        }

        /// <summary>
        /// Retrieves the urn to system identifier map when maintainable.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <returns>The <see cref="IDictionary{String, Int64}" />.</returns>
        private IDictionary<string, long> RetrievesUrnToSysIdMapWhenMaintainable(SdmxStructureType sdmxStructure)
        {
            var query = this.BuildMaintainableQuery(sdmxStructure);
            IDictionary<string, long> dictionary = new Dictionary<string, long>(StringComparer.Ordinal);
            foreach (var result in
                this._mappingStoreDatabase.Query<dynamic>(query).Select(o => new KeyValuePair<string, long>(sdmxStructure.GenerateUrn(o.AGENCY, o.ID, o.VERSION).ToString(), o.SYSID)))
            {
                Debug.Assert(!dictionary.ContainsKey(result.Key), "Already got key : {0}", result.Key);
                dictionary.Add(result);
            }

            return dictionary;
        }

        /// <summary>
        /// Builds the maintainable urn map.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// The map between primary key and URN
        /// </returns>
        private IDictionary<long, string> BuildMaintainableUrnMap(SdmxStructureType sdmxStructure, object parameters = null)
        {
            TableInfo tableInfo = this._tableInfoBuilder.Build(sdmxStructure);
            var query = string.Format(CultureInfo.InvariantCulture, ArtefactQueryFormat, tableInfo.PrimaryKey, tableInfo.Table);
            return this.BuildMaintainableUrnMap(sdmxStructure, query, parameters);
        }

        /// <summary>
        /// Builds the maintainable urn map.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <param name="query">The query.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// The map between primary key and URN
        /// </returns>
        private IDictionary<long, string> BuildMaintainableUrnMap(SdmxStructureType sdmxStructure, string query, object parameters = null)
        {
            IDictionary<long, string> dictionary = new Dictionary<long, string>();
            foreach (dynamic o in this._mappingStoreDatabase.Query(query, parameters))
            {
                long sysid = o.SYSID;

                Uri generateUrn = sdmxStructure.GenerateUrn(o.AGENCY, o.ID, o.VERSION);
                var result = new KeyValuePair<long, string>(sysid, generateUrn.ToString());
                dictionary.Add(result);
            }

            return dictionary;
        }

        /// <summary>
        /// Builds the maintainable query.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <returns>The SQL Query.</returns>
        private string BuildMaintainableQuery(SdmxStructureType sdmxStructure)
        {
            TableInfo tableInfo = this._tableInfoBuilder.Build(sdmxStructure);
            var query = BuildMaintainableQuery(tableInfo);
            return query;
        }

        /// <summary>
        /// Retrieves the urn to system identifier map when not maintainable.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <returns>The <see cref="IDictionary{String, Int64}" />.</returns>
        /// <exception cref="NotImplementedException">Unsupported artefact</exception>
        private IDictionary<string, long> RetrievesUrnToSysIdMapWhenNotMaintable(SdmxStructureType sdmxStructure)
        {
            var query = this.BuildNoMaintainableQuery(sdmxStructure);
            IDictionary<string, long> dictionary = new Dictionary<string, long>(StringComparer.Ordinal);
            foreach (var result in
                this._mappingStoreDatabase.Query<dynamic>(query).Select(o => new KeyValuePair<string, long>(sdmxStructure.GenerateUrn(o.AGENCY, o.ID, o.VERSION, o.IID).ToString(), o.SYSID)))
            {
                dictionary.Add(result);
            }

            return dictionary;
        }

        /// <summary>
        /// Builds the no maintainable query.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <returns>The SQL Query.</returns>
        /// <exception cref="NotImplementedException">Unsupported artefact</exception>
        private string BuildNoMaintainableQuery(SdmxStructureType sdmxStructure)
        {
            var itemTableInfo = this._itemTableInfoBuilder.Build(sdmxStructure);
            if (itemTableInfo == null)
            {
                throw new NotImplementedException(sdmxStructure.ToString());
            }

            var query = BuildNoMaintainableQuery(itemTableInfo);
            return query;
        }
    }
}