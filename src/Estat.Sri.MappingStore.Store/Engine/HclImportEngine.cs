// -----------------------------------------------------------------------
// <copyright file="HclImportEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-24
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Estat.Sri.Sdmx.MappingStore.Store.Engine;
    using Estat.Sri.Sdmx.MappingStore.Store.Properties;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The HCL import engine.
    /// </summary>
    public class HclImportEngine : ArtefactImportEngine<IHierarchicalCodelistObject>
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(HclImportEngine));

        /// <summary>
        ///     The _stored procedures
        /// </summary>
        private static readonly StoredProcedures _storedProcedures;

        /// <summary>
        ///     Initializes static members of the <see cref="HclImportEngine" /> class.
        /// </summary>
        static HclImportEngine()
        {
            _storedProcedures = new StoredProcedures();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="HclImportEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        public HclImportEngine(Database database)
            : base(database)
        {
        }

        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        public override ArtefactImportStatus Insert(DbTransactionState state, IHierarchicalCodelistObject maintainable)
        {
            foreach (var codelistRef in maintainable.CodelistRef)
            {
                ICrossReference codelistReference = codelistRef.CodelistReference;

                /* Those are handled now in ArtefactImportEngine.Validate */
                //ItemSchemeFinalStatus itemSchemeFinalStatus = codelistCache.GetStructure(state, codelistReference);
                //var codelistMaintainableReference = codelistReference.MaintainableReference;
                //if (itemSchemeFinalStatus.FinalStatus.PrimaryKey <= 0)
                //{
                //    string message = string.Format(Resources.ErrorHclUsesCodelistNotExistFormat4, maintainable.Id, codelistMaintainableReference.MaintainableId, codelistMaintainableReference.Version, codelistMaintainableReference.AgencyId);
                //    return new ArtefactImportStatus(-1, new ImportMessage(ImportMessageStatus.Error, codelistReference, message));
                //}

                //if (maintainable.IsFinal.IsTrue && !itemSchemeFinalStatus.FinalStatus.IsFinal)
                //{
                //    string message = string.Format(Resources.ErrorHclUsesCodelistNotFinalFormat4, maintainable.Id, codelistMaintainableReference.MaintainableId, codelistMaintainableReference.Version, codelistMaintainableReference.AgencyId);
                //    return new ArtefactImportStatus(-1, new ImportMessage(ImportMessageStatus.Error, codelistReference, message));
                //}
            }

            _log.DebugFormat(CultureInfo.InvariantCulture, "Importing artefact {0}", maintainable.Urn);
            var artefactStatus = this.InsertArtefactInternal(state, maintainable);
            foreach (KeyValuePair<long, IHierarchy> hierarchy in
                this.InsertHierarchies(state, maintainable.Hierarchies, artefactStatus.PrimaryKeyValue))
            {
                var levelIds = this.InsertLevels(state, hierarchy.Value.Level, hierarchy.Key, artefactStatus.PrimaryKeyValue);
                this.InsertCodeReferences(state, hierarchy.Value.HierarchicalCodeObjects, hierarchy.Key, artefactStatus.PrimaryKeyValue, levelIds);
            }

            return artefactStatus;
        }

        /// <summary>
        ///     Deletes the child items.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="primaryKey">The primary key.</param>
        protected override void DeleteChildStructures(DbTransactionState state, long primaryKey)
        {
            state.Database.UsingLogger().ExecuteNonQueryFormat("DELETE FROM ANNOTATION WHERE ANN_ID IN (SELECT DISTINCT ANN_ID FROM OTHER_ANNOTATION WHERE OTH_ID IN (SELECT OTH_ID FROM OTHER_NAMEABLE WHERE PARENT_ARTEFACT = {0}))", primaryKey);
            state.Database.UsingLogger().ExecuteNonQueryFormat("DELETE FROM LOCALISED_STRING_OTHER WHERE OTH_ID IN (SELECT OTH_ID FROM OTHER_NAMEABLE WHERE PARENT_ARTEFACT = {0})", primaryKey);
            state.Database.UsingLogger().ExecuteNonQueryFormat("DELETE FROM ANNOTATION WHERE ANN_ID IN (SELECT DISTINCT ANN_ID FROM OTHER_ANNOTATION WHERE OTH_ID IN (SELECT OTH_ID FROM OTHER_NAMEABLE WHERE PARENT_ARTEFACT = {0}))", primaryKey);
            state.Database.UsingLogger().ExecuteNonQueryFormat("DELETE FROM HCL_CODE WHERE HCODE_ID IN (SELECT OTH_ID FROM OTHER_NAMEABLE WHERE PARENT_ARTEFACT = {0})", primaryKey);
            state.Database.UsingLogger().ExecuteNonQueryFormat("DELETE FROM HLEVEL WHERE LEVEL_ID IN (SELECT OTH_ID FROM OTHER_NAMEABLE WHERE PARENT_ARTEFACT = {0})", primaryKey);

            state.Database.UsingLogger().ExecuteNonQueryFormat("delete from OTHER_NAMEABLE where PARENT_ARTEFACT = {0} and URN_CLASS = 'HierarchicalCode'", primaryKey);
            state.Database.UsingLogger().ExecuteNonQueryFormat("delete from OTHER_NAMEABLE where PARENT_ARTEFACT = {0} and URN_CLASS = 'Level'", primaryKey);
            state.Database.UsingLogger().ExecuteNonQueryFormat("delete from OTHER_NAMEABLE where PARENT_ARTEFACT = {0}", primaryKey);
        }

        /// <summary>
        /// Gets the references to Codelists.
        /// </summary>
        /// <param name="maintainable"></param>
        /// <returns></returns>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(IHierarchicalCodelistObject maintainable)
        {
            if (!ObjectUtil.ValidCollection(maintainable.CodelistRef))
            {
                return new List<ReferenceToOtherArtefact>();
            }
            // TODO probably it needs more than that
            // as hierarchical codes have direct references to code
            List<ReferenceToOtherArtefact> references = new List<ReferenceToOtherArtefact>();
            Stack<IHierarchicalCode> codeRefStack = new Stack<IHierarchicalCode>(maintainable.Hierarchies.SelectMany(h => h.HierarchicalCodeObjects));
            while(codeRefStack.Count > 0)
            {
                var currentCode = codeRefStack.Pop();
                references.Add(new ReferenceToOtherArtefact(currentCode.CodeReference, RefTypes.Code));
                foreach(var child in currentCode.CodeRefs)
                {
                    codeRefStack.Push(child);
                }
            }

            return references;
        }

        /// <summary>
        ///     Insert code references.
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="hierarchicalCodeObjects">
        ///     The hierarchical code objects.
        /// </param>
        /// <param name="hierarchyID">
        ///     The hierarchy id.
        /// </param>
        /// <param name="levelIds">
        ///     The level ids.
        /// </param>
        private void InsertCodeReferences(DbTransactionState state, IEnumerable<IHierarchicalCode> hierarchicalCodeObjects, long hierarchyID, long artefactId, IDictionary<string, long> levelIds)
        {
            HierarchicalCodeImportEngine hierarchicalCodeImportEngine = new HierarchicalCodeImportEngine(state.Database, hierarchyID, levelIds);
            var queue = new Queue<IHierarchicalCode>(hierarchicalCodeObjects);
            while (queue.Count > 0)
            {
                var hierarchicalCodelist = queue.Dequeue();
                long primaryKey = hierarchicalCodeImportEngine.Insert(state, hierarchicalCodelist, artefactId);
                foreach (var hierarchicalCode in hierarchicalCodelist.CodeRefs)
                {
                    queue.Enqueue(hierarchicalCode);
                }
            }
        }

        /// <summary>
        ///     The insert.
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="hierarchies">
        ///     The hierarchies.
        /// </param>
        /// <param name="parentArtefact">
        ///     The parent artefact.
        /// </param>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        private IEnumerable<KeyValuePair<long, IHierarchy>> InsertHierarchies(DbTransactionState state, IEnumerable<IHierarchy> hierarchies, long parentArtefact)
        {
            OtherNameableImportEngine hierarchy2x = new OtherNameableImportEngine(state.Database);
            foreach (IHierarchy item in hierarchies)
            {
                yield return new KeyValuePair<long, IHierarchy>(hierarchy2x.Insert(state, item, parentArtefact), item);
            }
        }

        /// <summary>
        ///     Insert levels to Mapping Store database.
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="rootLevel">
        ///     The root level.
        /// </param>
        /// <param name="parentHierarchyID">
        ///     The parent hierarchy id.
        /// </param>
        /// <returns>
        ///     The <see cref="IDictionary{String, Long}" /> with the level id as key and the mapping store level primary key as
        ///     value.
        /// </returns>
        private IDictionary<string, long> InsertLevels(DbTransactionState state, ILevelObject rootLevel, long parentHierarchyID, long artefactPrimaryKey)
        {
            HierarchicalLevelImportEngine levelImportEngine = new HierarchicalLevelImportEngine(database, parentHierarchyID);
            var levelIds = new Dictionary<string, long>(StringComparer.Ordinal);
            ILevelObject level = rootLevel;
            long parentLevel = 0;
            while (level != null)
            {
                var id = levelImportEngine.Insert(state, level, artefactPrimaryKey);
                levelIds.Add(level.Id, id);
                parentLevel = id;
                level = level.HasChild() ? level.ChildLevel : null;
            }

            return levelIds;
        }
    }
}