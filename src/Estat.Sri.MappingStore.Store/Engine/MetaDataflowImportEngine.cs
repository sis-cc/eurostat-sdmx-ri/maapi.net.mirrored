// -----------------------------------------------------------------------
// <copyright file="MetaDataflowImportEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Globalization;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;

    /// <summary>
    ///     The dataflow import engine.
    /// </summary>
    public class MetaDataflowImportEngine : ArtefactImportEngine<IMetadataFlow>
    {

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetaDataflowImportEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        public MetaDataflowImportEngine(Database database)
            : base(database)
        {
        }

        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        public override ArtefactImportStatus Insert(DbTransactionState state, IMetadataFlow maintainable)
        {
            return InsertArtefactInternal(state, maintainable);
        }

        /// <summary>
        /// Gets the reference to Metadata Structure
        /// </summary>
        /// <param name="maintainable"></param>
        /// <returns></returns>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(IMetadataFlow maintainable)
        {
            // TODO in SDMX 3.0.0 metadataflow has a list of targets
            return new List<ReferenceToOtherArtefact>() { new ReferenceToOtherArtefact(maintainable.MetadataStructureRef, RefTypes.Structure)};
        }
    }
}