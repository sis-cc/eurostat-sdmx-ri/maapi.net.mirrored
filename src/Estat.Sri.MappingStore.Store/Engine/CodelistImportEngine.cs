// -----------------------------------------------------------------------
// <copyright file="CodelistImportEngine.cs" company="EUROSTAT">
//   Date Created : 2022-06-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Collections.Generic;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.Sdmx.MappingStore.Store.Engine.Update;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// The engine for importing codelists.
    /// Compatible to MSDB 7.0
    /// </summary>
    public class CodelistImportEngine : ItemSchemeImportEngine<ICodelistObject, ICode>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CodelistImportEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        public CodelistImportEngine(Database database) 
            : base(database)
        {
        }

        /// <inheritdoc/>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(ICodelistObject maintainable)
        {
            IList<ReferenceToOtherArtefact> list = new List<ReferenceToOtherArtefact>();
            IList<ICodelistInheritanceRule> codelistExtensions = maintainable.CodelistExtensions;
            if (ObjectUtil.ValidCollection(codelistExtensions))
            {
                for (int i = 0; i < codelistExtensions.Count; i++)
                {
                    ICodelistInheritanceRule codelistExtension = codelistExtensions[i];

                    // HACK Store the order of codelist extension as unique id
                    list.Add(new ReferenceToOtherArtefact(codelistExtension.CodelistRef, RefTypes.CodelistExtensions, i.ToString(), null));
                }
            }

            // TODO Codelist Extensions are not yet supported, it will be done in a future Feature if ever!
            // It will need MADB updated
            return list;
        }

        protected override ArtefactImportStatus HandleNonFinalArtefactReplace(DbTransactionState state, IMaintainableObject maintainableObject, ArtefactFinalStatus finalStatus, IList<IMaintainableMutableObject> crossReferencingObjects)
        {
            ///TODO use UpdateCodelistEngine in the future, in order to delete the transcodings associated with this deleted code
            var updateItemSchemeEngine = new UpdateItemSchemeEngine<ICodelistObject, ICode>(state);
            return updateItemSchemeEngine.Update((ICodelistObject)maintainableObject, finalStatus, crossReferencingObjects);
        }
    }
}
