// -----------------------------------------------------------------------
// <copyright file="DataConsumerSchemeImportEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Collections.Generic;
    using System.Globalization;

    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Estat.Sri.Sdmx.MappingStore.Store.Engine.Update;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     The data provider scheme import engine.
    /// </summary>
    public class DataConsumerSchemeImportEngine : ItemSchemeImportEngine<IDataConsumerScheme, IDataConsumer>
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DataConsumerSchemeImportEngine));
        /// <summary>
        ///     Initializes a new instance of the <see cref="DataConsumerSchemeImportEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        public DataConsumerSchemeImportEngine(Database database)
            : base(database)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataConsumerSchemeImportEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        /// <param name="factory">
        ///     The <see cref="IItemImportEngine{T}" /> factory. Optional
        /// </param>
        public DataConsumerSchemeImportEngine(Database database, IItemImportFactory<IDataConsumer> factory)
            : base(database, factory)
        {
        }

        /// <summary>
        /// No references.
        /// </summary>
        /// <param name="maintainable"></param>
        /// <returns>An empty list</returns>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(IDataConsumerScheme maintainable)
        {
            return new List<ReferenceToOtherArtefact>();
        }
     }
}