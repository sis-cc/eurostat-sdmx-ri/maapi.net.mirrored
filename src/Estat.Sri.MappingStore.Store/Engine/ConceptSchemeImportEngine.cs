// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeImportEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sri.MappingStore.Store.Helper;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Estat.Sri.Sdmx.MappingStore.Store.Engine.Update;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     The concept scheme import engine.
    /// </summary>
    public class ConceptSchemeImportEngine : ItemSchemeImportEngine<IConceptSchemeObject, IConceptObject>
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ConceptSchemeImportEngine));

        /// <summary>
        ///     The _stored procedures
        /// </summary>
        private static readonly StoredProcedures _storedProcedures;

        /// <summary>
        ///     Initializes static members of the <see cref="ConceptSchemeImportEngine" /> class.
        /// </summary>
        static ConceptSchemeImportEngine()
        {
            _storedProcedures = new StoredProcedures();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeImportEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        public ConceptSchemeImportEngine(Database database)
            : base(database)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeImportEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        /// <param name="factory">
        ///     The <see cref="IItemImportEngine{T}" /> factory. Optional
        /// </param>
        public ConceptSchemeImportEngine(Database database, IItemImportFactory<IConceptObject> factory)
            : base(database, factory)
        {
        }

        /// <summary>
        /// References codelists
        /// </summary>
        /// <param name="maintainable"></param>
        /// <returns></returns>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(IConceptSchemeObject maintainable)
        {
            IList<ReferenceToOtherArtefact> referenceToOtherArtefacts = new List<ReferenceToOtherArtefact>();
            foreach (ICrossReference crossReference in maintainable.AllCrossReferences)
            {
                if (crossReference.TargetReference.IsMaintainable)
                {
                    referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(crossReference, RefTypes.Enumeration));
                }
            }
            return referenceToOtherArtefacts;
        }

        /// <summary>
        /// Gets the Text Formats of the items core representation
        /// </summary>
        /// <param name="maintainable"></param>
        /// <returns></returns>
        protected override IList<ITextFormat> GetTextFormats(IConceptSchemeObject maintainable)
        {
            return maintainable.Items
                .Where(i => i.CoreRepresentation?.TextFormat != null)
                .Select(i => i.CoreRepresentation.TextFormat)
                .ToList();
        }
    }
}