// -----------------------------------------------------------------------
// <copyright file="SchemeMapBaseEngine.cs" company="EUROSTAT">
//   Date Created : 2014-10-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Collections.Generic;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;

    /// <summary>
    ///     The Schema base engine.
    ///     Compatible to MSDB 7.0
    /// </summary>
    /// <typeparam name="TSchemaMap">The type of the schema map.</typeparam>
    public abstract class SchemeMapBaseEngine<TSchemaMap> : OtherNameableImportEngine
        where TSchemaMap : ISchemeMapObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SchemeMapBaseEngine{TSchemaMap}" /> class.
        /// </summary>
        protected SchemeMapBaseEngine(Database mappingStore)
            : base(mappingStore)
        {
        }

        /// <summary>
        ///     Inserts the specified item.
        /// </summary>
        /// <param name="state">The transaction.</param>
        /// <param name="schemaMaps">The schema maps.</param>
        /// <param name="structureSetId">The structure set identifier.</param>
        /// <returns>
        ///     The primary key values
        /// </returns>
        public IEnumerable<long> Insert(DbTransactionState state, IEnumerable<TSchemaMap> schemaMaps, long structureSetId)
        {
            var primaryKeys = new List<long>();
            foreach (var schemaMap in schemaMaps)
            {
                long primaryKey = this.Insert(state, schemaMap, structureSetId);
                primaryKeys.Add(primaryKey);
                this.WriteItemMaps(state, schemaMap, primaryKey);
            }

            return primaryKeys;
        }

        /// <summary>
        ///     Writes the item maps.
        /// </summary>
        /// <param name="state">The transaction.</param>
        /// <param name="schemaMap">The schema map.</param>
        /// <param name="primaryKey">The primary key.</param>
        protected abstract void WriteItemMaps(DbTransactionState state, TSchemaMap schemaMap, long primaryKey);
    }
}
