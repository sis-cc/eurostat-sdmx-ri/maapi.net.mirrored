// -----------------------------------------------------------------------
// <copyright file="DataflowImportEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;
    using DryIoc;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Estat.Sri.Sdmx.MappingStore.Store.Properties;
    using Estat.Sri.Utils.Config;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

    /// <summary>
    ///     The dataflow import engine.
    /// </summary>
    public class DataflowImportEngine : ArtefactImportEngine<IDataflowObject>
    {
        /// <summary>
        ///     The artefact stored procedure
        /// </summary>
        private static readonly InsertDataflow _artefactStoredProcedure;

        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DataflowImportEngine));
        private readonly IEntityRetrieverManager _entityRetrieverManager;
        private readonly IEntityPersistenceManager _entityPersistManager;

        /// <summary>
        ///     Initializes static members of the <see cref="DataflowImportEngine" /> class.
        /// </summary>
        static DataflowImportEngine()
        {
            _artefactStoredProcedure = new StoredProcedures().InsertDataflow;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataflowImportEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        public DataflowImportEngine(Database database)
            : base(database)
        {
            if (ConfigurationProvider.AutoDeleteMappingSets)
            {

                _entityRetrieverManager = MappingStoreIoc.Container.Resolve<IEntityRetrieverManager>();
                _entityPersistManager = MappingStoreIoc.Container.Resolve<IEntityPersistenceManager>();
            }
            else
            {
                _entityRetrieverManager = null;
                _entityPersistManager = null;
            }
        }

        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        public override ArtefactImportStatus Insert(DbTransactionState state, IDataflowObject maintainable)
        {
            var dsdStatus = GetFinalStatusOfReferenceOfChild(state.Database, maintainable.DataStructureRef);
            if (dsdStatus != null && dsdStatus.PrimaryKey > 0 && (dsdStatus.IsFinal || !maintainable.IsFinal.IsTrue))
            {
                _log.DebugFormat(CultureInfo.InvariantCulture, "Importing artefact {0}", maintainable.Urn);
                return this.InsertArtefactInternal(state, maintainable, _artefactStoredProcedure, null);
            }

            //// DSD issues
            var importMessage = BuildErrorMessage(maintainable, dsdStatus);

            return new ArtefactImportStatus(-1, importMessage);
        }

        /// <summary>
        /// Updates the non final artefact. Structure specific code goes here
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="maintainableObject">The maintainable object.</param>
        /// <param name="finalStatus">The final status.</param>
        /// <param name="crossReferencingObjects"></param>
        /// <returns>
        /// The <see cref="ArtefactImportStatus" />.
        /// </returns>
        protected override ArtefactImportStatus HandleNonFinalArtefactReplace(DbTransactionState state, IMaintainableObject maintainableObject, ArtefactFinalStatus finalStatus, IList<IMaintainableMutableObject> crossReferencingObjects)
        {
            var query = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                    .SetMaintainableTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow))
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                    .SetStructureIdentification(maintainableObject.AsReference)
                    .Build();
            var dataflow = new RetrievalEngineContainer(state.Database).DataflowRetrievalEngine.Retrieve(query).First();
            var dsdRefFromDb = dataflow.DataStructureRef;

            var dsdfromMaintainable = ((IDataflowObject)maintainableObject).DataStructureRef;

            if (!dsdfromMaintainable.Equals(dsdRefFromDb))
            {
                throw new SdmxConflictException("Reference DSD doesn't match");
            }

            return HandleFinalArtefactReplace(state, maintainableObject, finalStatus);
        }

        /// <summary>
        /// Updates the final.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="maintainableObject">The maintainable object.</param>
        /// <param name="finalStatus">The final status.</param>
        /// <returns>
        /// The <see cref="ArtefactImportStatus" />.
        /// </returns>
        protected override ArtefactImportStatus HandleFinalArtefactReplace(DbTransactionState state, IMaintainableObject maintainableObject, ArtefactFinalStatus finalStatus)
        {
            if (maintainableObject.StructureType.EnumType == SdmxStructureEnumType.Dataflow)
            {
                // the "Non" exists for backward compatibility
                var nonProductionAnnotation = maintainableObject.GetAnnotationsByType(nameof(CustomAnnotationType.NonProductionDataflow)).FirstOrDefault();
                if (nonProductionAnnotation != null)
                {
                    int toProduction = 0;
                    var text = nonProductionAnnotation.Text.FirstOrDefault()?.Value?.ToUpperInvariant();
                    switch (text)
                    {
                        case "FALSE":
                        case "0":
                            toProduction = 1;
                            break;
                    }

                    // TODO Validate mappings maybe in NSI WS
                    var primaryKey = state.Database.CreateInParameter(
                        nameof(ArtefactFinalStatus.PrimaryKey),
                        DbType.Int64,
                        finalStatus.PrimaryKey);
                    var production = state.Database.CreateInParameter(nameof(toProduction), DbType.Int32, toProduction);
                    var returnValue = state.UsingLogger().ExecuteNonQueryFormat("UPDATE DATAFLOW SET PRODUCTION={0} WHERE DF_ID={1} AND PRODUCTION != {0}", production, primaryKey);
                    if (returnValue > 0)
                    {
                        var message = string.Format(
                            CultureInfo.InvariantCulture,
                            "The dataflow was {0} production",
                            toProduction == 0 ? "removed from" : "put in");

                        return new ArtefactImportStatus(finalStatus.PrimaryKey, new ImportMessage(ImportMessageStatus.Success, maintainableObject.AsReference, message));
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            return null;
        }

        /// <inheritdoc/>
        protected override void BeforeArtefactDeletion(DbTransactionState state, long primaryKey, IMaintainableObject artefact)
        {
            // Get data source id from dataflow before deletion
            IList<long> dataSources = GetEntityIds(state, "SELECT DATA_SOURCE_ID AS ID FROM DATAFLOW WHERE DF_ID = {0}",primaryKey);

            string tempQuery = "DELETE FROM DATAFLOW WHERE DF_ID = {0}";
            ExecuteArtefactUpdateOrDeleteQuery(state, tempQuery, primaryKey);

            //delete from DATA_SOURCE table the entries that reference the dataflow
            foreach (long dsPrimaryKey in dataSources)
            {
                tempQuery = "DELETE FROM DATA_SOURCE WHERE DATA_SOURCE_ID = {0}";
                ExecuteArtefactUpdateOrDeleteQuery(state, tempQuery, dsPrimaryKey);
            }

            if (ConfigurationProvider.AutoDeleteMappingSets)
            {
                DeleteMappingSets(artefact as IDataflowObject, state);
            }
        }

        //old implementation
        private void DeleteMappingSets(IDataflowObject dataFlowObject, DbTransactionState state)
        {
            if (dataFlowObject == null)
            {
                throw new ArgumentNullException("dataFlowObject");
            }

            var storeId = state.Database.Name;
            if (string.IsNullOrEmpty(storeId))
            {
                throw new ArgumentNullException("storeId");
            }

            var entityQuery = new EntityQuery
            {
                EntityType = EntityType.MappingSet,
                ParentId = new Criteria<string>(OperatorType.Exact, dataFlowObject.Urn.ToString())
            };

            try
            {
                var mappingSetEntities = _entityRetrieverManager.GetEntities<MappingSetEntity>(storeId, entityQuery, Detail.Full);
                if (mappingSetEntities == null || !mappingSetEntities.Any())
                {
                    //No MappingSets 
                    _log.Debug($"There were no mappingSets found for the dataFlow:{dataFlowObject.Urn} .");
                    return;
                }

                var mappingSetEngine = _entityPersistManager.GetEngine<MappingSetEntity>(storeId);
                var timePreFormattedEngine = _entityPersistManager.GetEngine<TimeDimensionMappingEntity>(storeId);

                foreach (var mappingSet in mappingSetEntities)
                {
                    var preFormattedEntity = _entityRetrieverManager.GetEntities<TimeDimensionMappingEntity>(storeId, new EntityQuery()
                    {
                        EntityType = EntityType.TimePreFormatted,
                        ParentId = new Criteria<string>(OperatorType.Exact, mappingSet.EntityId)
                    },
                        Detail.IdOnly
                    )
                    .FirstOrDefault();

                    if (preFormattedEntity != null)
                    {
                        timePreFormattedEngine.Delete(preFormattedEntity.EntityId, EntityType.TimePreFormatted);
                    }

                    mappingSetEngine.Delete(mappingSet.EntityId, EntityType.MappingSet);

                    this.DeleteDataSet(mappingSet.DataSetId, state);
                }
            }
            catch (Exception e)
            {
                _log.Warn($"There was an error while trying to delete the mappingSets of the dataFlow:{dataFlowObject.Urn} .", e);
                throw;
            }
        }

        private void DeleteDataSet(string dataSetEntityId, DbTransactionState state)
        {
            if (dataSetEntityId == null)
            {
                throw new ArgumentNullException("dataSetEntityId");
            }

            var storeId = state.Database.Name;
            //Delete DataSetColumns
            var entityQuery = new EntityQuery
            {
                EntityType = EntityType.DataSetColumn,
                ParentId = new Criteria<string>(OperatorType.Exact, dataSetEntityId)
            };

            try
            {
                var dataSetColumnEntities = _entityRetrieverManager.GetEntities<DataSetColumnEntity>(storeId, entityQuery, Detail.Full);
                var dataSetColumnEngine = _entityPersistManager.GetEngine<DataSetColumnEntity>(storeId);

                foreach (var dataSetColumn in dataSetColumnEntities)
                {
                    dataSetColumnEngine.Delete(dataSetColumn.EntityId, EntityType.DataSetColumn);
                }

                //Delete DataSet
                _entityPersistManager.GetEngine<DatasetEntity>(storeId).Delete(dataSetEntityId, EntityType.DataSet);
            }
            catch (Exception e)
            {
                _log.Warn($"There was an error trying to delete the DataSet with ID:{dataSetEntityId} .", e);
                throw;
            }
        }

        /// <summary>
        /// Dataflow references a data structure.
        /// </summary>
        /// <param name="maintainable"></param>
        /// <returns>A single item list with the data structure reference.</returns>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(IDataflowObject maintainable)
        {
            return new List<ReferenceToOtherArtefact>()
            {
                new ReferenceToOtherArtefact(maintainable.DataStructureRef, RefTypes.Structure)
            };
        }

        private IList<long> GetEntityIds(DbTransactionState state,string queryUnformated, long primaryKey)
        {
            string query = string.Format(queryUnformated, primaryKey);
            using (DbCommand command = state.Database.CreateCommand(CommandType.Text, query))
            using (IDataReader dataReader = state.Database.ExecuteReader(command))
            {
                List<long> dataSources = new List<long>();
                while (dataReader.Read())
                {
                    long dsPrimaryKey = DataReaderHelper.GetInt64(dataReader, "ID");
                    if (dsPrimaryKey > 0)
                    {
                        dataSources.Add(dsPrimaryKey);
                    }
                }
                return dataSources;
            }
        }

        /// <summary>
        ///     Build the error message based on the specified <paramref name="dsdStatus" />.
        /// </summary>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <param name="dsdStatus">
        ///     The <c>DSD</c> status
        /// </param>
        /// <returns>
        ///     The <see cref="ImportMessage" />.
        /// </returns>
        private static ImportMessage BuildErrorMessage(IDataflowObject maintainable, ArtefactFinalStatus dsdStatus)
        {
            var maintainableRefObject = maintainable.DataStructureRef.MaintainableReference;
            var message = string.Format(CultureInfo.InvariantCulture, Resources.ErrorDataflowUsesDSDThatFormat4, maintainable.Id, maintainableRefObject.MaintainableId, maintainableRefObject.Version, maintainableRefObject.AgencyId);
            ImportMessage importMessage;
            var structureReference = maintainable.AsReference;

            if (dsdStatus == null || dsdStatus.PrimaryKey < 1)
            {
                var doesntExistMessage = string.Format(CultureInfo.InvariantCulture, Resources.ErrorDoesnotExistFormat1, message);
                _log.Error(doesntExistMessage);
                importMessage = new ImportMessage(ImportMessageStatus.Error, structureReference, doesntExistMessage);
            }
            else
            {
                var notFinalMessage = string.Format(CultureInfo.InvariantCulture, Resources.ErrorNotFinalFormat1, message);
                _log.Error(notFinalMessage);
                importMessage = new ImportMessage(ImportMessageStatus.Error, structureReference, notFinalMessage);
            }

            return importMessage;
        }
    }
}