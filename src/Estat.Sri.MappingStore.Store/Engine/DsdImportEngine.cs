// -----------------------------------------------------------------------
// <copyright file="DsdImportEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStore.Store.Engine.Update;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Estat.Sri.Sdmx.MappingStore.Store.Engine.Update;
    using Estat.Sri.SdmxParseBase.Engine;
    using log4net;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The DSD import engine.
    /// </summary>
    public class DsdImportEngine : ArtefactImportEngine<IDataStructureObject>
    {
        /// <summary>
        ///     The group id parameter.
        /// </summary>
        private const string GroupIdParameter = "groupId";

        /// <summary>
        ///     The id parameter.
        /// </summary>
        private const string IdParameter = "id";

        /// <summary>
        ///     The _stored procedures
        /// </summary>
        private readonly StoredProcedures _storedProcedures;

        /// <summary>
        ///     The _annotation insert engine
        /// </summary>
        private readonly IAnnotationInsertEngine _annotationInsertEngine;

        /// <summary>
        ///     The _insert component annotation
        /// </summary>
        private readonly InsertComponentAnnotation _insertComponentAnnotation;
        /// <summary>
        ///     The _artefact stored procedure
        /// </summary>
        private static readonly InsertDsd _artefactStoredProcedure;

        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DsdImportEngine));

        /// <summary>
        ///     The _component import.
        /// </summary>
        private readonly IIdentifiableImportEngine<IComponent> _componentImport;

        /// <summary>
        ///     The _group import.
        /// </summary>
        private readonly IIdentifiableImportEngine<IGroup> _groupImport;

        /// <summary>
        ///     Initializes static members of the <see cref="DsdImportEngine" /> class.
        /// </summary>
        static DsdImportEngine()
        {
            _artefactStoredProcedure = new StoredProcedures().InsertDsd;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DsdImportEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        public DsdImportEngine(Database database)
            : this(database, null, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DsdImportEngine" /> class.
        /// </summary>
        /// <param name="connectionStringSettings">
        ///     The mapping store database instance.
        /// </param>
        /// <param name="componentImport">
        ///     The component Import.
        /// </param>
        /// <param name="groupImport">
        ///     The group Import.
        /// </param>
        public DsdImportEngine(Database connectionStringSettings, IIdentifiableImportFactory<IComponent> componentImport,  IIdentifiableImportFactory<IGroup> groupImport)
            : base(connectionStringSettings)
        {
            var componentImportFactory = componentImport ?? new IdentifiableImportFactory<IComponent>();
            var groupImportFactory = groupImport ?? new IdentifiableImportFactory<IGroup>();
            this._componentImport = componentImportFactory.GetIdentifiableImport();
            this._groupImport = groupImportFactory.GetIdentifiableImport();
            _storedProcedures = new StoredProcedures();
            _annotationInsertEngine = new ComponentAnnotationInsertEngine(new AnnotationInsertEngine());
            _insertComponentAnnotation = _storedProcedures.InsertComponentAnnotation;
        }

        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        public override ArtefactImportStatus Insert(DbTransactionState state, IDataStructureObject maintainable)
        {
            _log.DebugFormat(CultureInfo.InvariantCulture, "Importing artefact {0}", maintainable.Urn);
            var artefactStoredProcedure = _artefactStoredProcedure;
            var artefactStatus = this.InsertArtefactInternal(state, maintainable, artefactStoredProcedure, null);

            if (maintainable.IdentifiableComposites.Any())
            {
                InsertComponents(state, artefactStatus.PrimaryKeyValue, maintainable);
            }
            return artefactStatus;
        }

        private void InsertComponents(DbTransactionState state, long dsdSysId, IDataStructureObject maintainable)
        {
            _log.Debug("|---- Insert Components...");

            ItemStatusCollection groups = InsertGroups(state, maintainable.Groups, dsdSysId);
            ItemStatusCollection components = InsertComponents(state, maintainable.GetAllComponents(), dsdSysId);
            if (maintainable.AttributeList != null)
            {
                ItemStatusCollection metadataAttributeUsage = InsertMetadataAttributeUsage(state, maintainable.AttributeList.MetadataAttributes, dsdSysId);
                components.AddAll(metadataAttributeUsage);
            }
            InsertInRelatedTables(state, maintainable, groups, components);
        }

        public ItemStatusCollection InsertMetadataAttributeUsage(DbTransactionState state, IEnumerable<IMetadataAttributeUsage> attributeUsages, long dsdSysId)
        {
            var itemStatusCollection = new ItemStatusCollection();
            foreach(var attributeUsage in attributeUsages)
            {
                long compId;
                var storedProcedure = _storedProcedures.InsertComponent;
                using (DbCommand command = storedProcedure.CreateCommand(state))
                {
                    DbParameter idParameter = storedProcedure.CreateIdParameter(command);
                    idParameter.Value = attributeUsage.MetadataAttributeReference;

                    DbParameter dsdIdParameter = storedProcedure.CreateDsdIdParameter(command);
                    dsdIdParameter.Value = dsdSysId;

                    DbParameter typeParameter = storedProcedure.CreateTypeParameter(command);
                    typeParameter.Value = SdmxComponentType.MetadataAttributeUsage;

                    DbParameter isFreqDimParameter = storedProcedure.CreateIsFreqDimParameter(command);

                    DbParameter isMeasureDimParameter = storedProcedure.CreateIsMeasureDimParameter(command);

                    DbParameter attAssLevelParameter = storedProcedure.CreateAttAssLevelParameter(command);
                    
                    DbParameter attStatusParameter = storedProcedure.CreateAttStatusParameter(command);

                    attAssLevelParameter.Value = attributeUsage.GetMappingStoreAssignmentLevel();
                    
                    DbParameter attIsTimeFormatParameter = storedProcedure.CreateAttIsTimeFormatParameter(command);

                    DbParameter xsMeasureCodeParameter = storedProcedure.CreateXsMeasureCodeParameter(command);

                    DbParameter minOccursParameter = storedProcedure.CreateMinOccursParameter(command);

                    DbParameter maxOccursParameter = storedProcedure.CreateMaxOccursParameter(command);

                    DbParameter outputParameter = storedProcedure.CreateOutputParameter(command);

                    DbParameter xsAttlevelDsParameter = storedProcedure.CreateXsAttlevelDsParameter(command);

                    DbParameter xsAttlevelGroupParameter = storedProcedure.CreateXsAttlevelGroupParameter(command);

                    DbParameter xsAttlevelSectionParameter = storedProcedure.CreateXsAttlevelSectionParameter(command);

                    DbParameter xsAttlevelObsParameter = storedProcedure.CreateXsAttlevelObsParameter(command);


                    command.ExecuteNonQueryAndLog();

                    compId = (long)outputParameter.Value;
                }

                _annotationInsertEngine.Insert(state, compId, _insertComponentAnnotation, attributeUsage.Annotations);
                itemStatusCollection.Add(new ItemStatus(attributeUsage.MetadataAttributeReference, compId));
            }

            return itemStatusCollection;
        }

        public ItemStatusCollection InsertGroups(DbTransactionState state, IList<IGroup> groupList, long dsdSysId)
        {
            return this._groupImport.Insert(state, groupList, dsdSysId);
        }

        public ItemStatusCollection InsertComponents(DbTransactionState state, IEnumerable<IComponent> componentList, long dsdSysId)
        {
            return this._componentImport.Insert(state, componentList, dsdSysId);
        }

        public void InsertComponentsReferences(DbTransactionState state, IEnumerable<IComponent> componentList, long dsdSysId)
        {
            //TODO SDMXRI-1919: this is also similar logic to GetReferenceToOtherArtefacts
            foreach (var component in componentList)
            {
                List<ReferenceToOtherArtefact> referenceToOtherArtefacts = new List<ReferenceToOtherArtefact>();

                referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(component.ConceptRef, RefTypes.ConceptIdentity));
                if (component.HasCodedRepresentation())
                {
                    referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(component.Representation.Representation, RefTypes.Enumeration));
                }

                if (component is IDimension dimension && ObjectUtil.ValidCollection(dimension.ConceptRole))
                {
                    referenceToOtherArtefacts.AddRange(ExtractRoles(dimension.ConceptRole));
                }
                else if (component is IAttributeObject attribute && ObjectUtil.ValidCollection(attribute.ConceptRoles))
                {
                    referenceToOtherArtefacts.AddRange(ExtractRoles(attribute.ConceptRoles));
                }
                else if (component is IMeasure measure && ObjectUtil.ValidCollection(measure.ConceptRoles))
                {
                    referenceToOtherArtefacts.AddRange(ExtractRoles(measure.ConceptRoles));
                }

                ReferenceImportEngine referenceImportEngine = new ReferenceImportEngine(state.Database);
                referenceImportEngine.InsertReferences(state, dsdSysId, referenceToOtherArtefacts);
                referenceImportEngine.InsertTextFormats(state, dsdSysId, GetTextFormats(component));
            }
        }

        public void InsertInRelatedTables(DbTransactionState state, IDataStructureObject maintainable, ItemStatusCollection groups, ItemStatusCollection components)
        {
            InsertDimensionGroup(state, maintainable, groups, components);
            InsertAttributeGroup(state, maintainable, groups, components);
            InsertAttributeDimensions(state, maintainable, components);
            InsertAttributeAttachmentMeasures(state, maintainable, components);
        }

        /// <summary>
        ///     Deletes the child items.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="primaryKey">The primary key.</param>
        protected override void DeleteChildStructures(DbTransactionState state, long primaryKey)
        {

            state.UsingLogger().ExecuteNonQueryFormat("DELETE FROM ANNOTATION WHERE ANN_ID IN (SELECT DISTINCT ANN_ID FROM COMPONENT_ANNOTATION WHERE COMP_ID IN (SELECT COMP_ID FROM N_COMPONENT WHERE DSD_ID = {0}))", state.Database.CreateInParameter("p_fk", DbType.Int64, primaryKey));
            state.UsingLogger().ExecuteNonQueryFormat("DELETE FROM ANNOTATION WHERE ANN_ID IN (SELECT DISTINCT ANN_ID FROM GROUP_ANNOTATION WHERE GR_ID IN (SELECT GR_ID FROM DSD_GROUP WHERE DSD_ID = {0}))", state.Database.CreateInParameter("p_fk", DbType.Int64, primaryKey));
            state.UsingLogger().ExecuteNonQueryFormat("DELETE FROM ATT_MEASURE WHERE ATT_COMP_ID IN (SELECT DISTINCT COMP_ID FROM N_COMPONENT WHERE DSD_ID = {0})", state.Database.CreateInParameter("p_fk", DbType.Int64, primaryKey));
            // Because oracle crashes with the following error, we cannot use the above way to delete records from COMPONENT_COMMON. Maybe in the future introduce PARENT_ARTEFACT to COMPONENT_COMMON
            // Oracle.ManagedDataAccess.Client.OracleException : ORA-01000: maximum open cursors exceeded
            Stack<long> componentids = new Stack<long>();
            state.UsingLogger().ExecuteReaderFormat("SELECT COMP_ID FROM N_COMPONENT WHERE DSD_ID = {0}", r =>
            {
                while (r.Read())
                {
                    componentids.Push(r.GetInt64(0));
                }
            }, state.Database.CreateInParameter("p_fk", DbType.Int64, primaryKey));
            DbHelper.BulkDelete(state.Database, "COMPONENT_COMMON", "COMP_ID", componentids);
        }

        protected override bool SkipFinalCheck(IDataStructureObject maintainable, ICrossReference crossReference)
        {
            // Exceptions (To be moved to overrided class in import engines?
            if (crossReference.TargetReference.EnumType == SdmxStructureEnumType.Concept && crossReference.MaintainableId.Equals("STANDALONE_CONCEPT_SCHEME"))
            {
                return true;
            }

            if (crossReference.ReferencedFrom.StructureType.EnumType == SdmxStructureEnumType.DataAttribute && AttributesCanReferenceNonFinal())
            {
                return true;
            }

            return false;
        }

        private static bool AttributesCanReferenceNonFinal()
        {
            var value = ConfigurationManager.AppSettings["datastructure.final.attributes.referenceNonFinal"];
            if (value == null)
            {
                //use old behaviour if the config does not have a setting
                return false;
            }

            bool boolValue;
            if (bool.TryParse(value, out boolValue))
            {
                return boolValue;
            }

            throw new ConfigurationErrorsException($"The setting 'datastructure.final.attributes.referenceNonFinal'  accepts only a Boolean value, true or false. The provided value '{value}' cannot be parsed");
        }

        /// <summary>
        ///     Inserts the attribute attachment measures. (SDMX V2.0 only)
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="dsd">The DSD.</param>
        /// <param name="components">The components.</param>
        private static void InsertAttributeAttachmentMeasures(DbTransactionState state, IDataStructureObject dsd, ItemStatusCollection components)
        {
            var parameterList = new List<DbParameter[]>();
            var cross = dsd as ICrossSectionalDataStructureObject;
            if (cross == null)
            {
                // it is not a SDXM v2.0 cross sectional DSD therefor there are no Measure dimensions or CrossSectional measures.
                // unless it is 3.0, but we don't have the sdmx schema so we need to rely on Measure
                foreach (var attributeObject in dsd.Attributes)
                {
                    ItemStatus attributeStatus;
                    if (components.TryGetValue(attributeObject.Id, out attributeStatus))
                    {
                        foreach (var measureRelationship in attributeObject.MeasureRelationships)
                        {
                            parameterList.AddRange(CreateParameterList(state, components, attributeStatus, measureRelationship));
                        }
                    }
                }
            }
            else
            {
                foreach (var attributeObject in cross.Attributes)
                {
                    ItemStatus attributeStatus;
                    if (components.TryGetValue(attributeObject.Id, out attributeStatus))
                    {
                        foreach (var crossSectionalMeasure in cross.GetAttachmentMeasures(attributeObject))
                        {
                            parameterList.AddRange(CreateParameterList(state, components, attributeStatus, crossSectionalMeasure.Id));
                        }
                    }
                }
            }

            state.UsingLogger().ExecuteNonQueryFormat("insert into ATT_MEASURE (ATT_COMP_ID, MEASURE_COMP_ID) VALUES ({0}, {1})", parameterList);
        }

        private static List<DbParameter[]> CreateParameterList(DbTransactionState state, ItemStatusCollection components, ItemStatus attributeStatus, string measureId)
        {
            var parameterList = new List<DbParameter[]>();
            ItemStatus measureStatus;
            if (components.TryGetValue(measureId, out measureStatus))
            {
                var parameters = new DbParameter[2];
                parameters[0] = state.Database.CreateInParameter(IdParameter, DbType.Int64, attributeStatus.SysID);
                parameters[1] = state.Database.CreateInParameter("measureId", DbType.Int64, measureStatus.SysID);
                parameterList.Add(parameters);
            }

            return parameterList;
        }

        /// <summary>
        ///     Insert attribute group.
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="dsd">
        ///     The DSD.
        /// </param>
        /// <param name="components">
        ///     The components.
        /// </param>
        public void InsertAttributeDimensions(DbTransactionState state, IDataStructureObject dsd, ItemStatusCollection components)
        {
            // If there are no attributes then we don't have attributes attached to groups
            if (dsd.AttributeList ==  null)
            {
                return;
            }

            var parameterList = new List<DbParameter[]>();
            foreach (var attributeObject in dsd.DimensionGroupAttributes)
            {
                ItemStatus attributeStatus;
                if (components.TryGetValue(attributeObject.Id, out attributeStatus))
                {
                    foreach (var dimensionReference in attributeObject.DimensionReferences)
                    {
                        ItemStatus dimensionStatus;
                        if (components.TryGetValue(dimensionReference, out dimensionStatus))
                        {
                            var parameters = new DbParameter[2];
                            parameters[0] = state.Database.CreateInParameter(IdParameter, DbType.Int64, attributeStatus.SysID);
                            parameters[1] = state.Database.CreateInParameter(GroupIdParameter, DbType.Int64, dimensionStatus.SysID);
                            parameterList.Add(parameters);
                        }
                    }
                }
            }

            if (dsd.AttributeList.MetadataAttributes != null)
            {
                foreach (var attributeObject in dsd.AttributeList.MetadataAttributes)
                {
                    ItemStatus attributeStatus;
                    if (components.TryGetValue(attributeObject.MetadataAttributeReference, out attributeStatus))
                    {
                        foreach (var dimensionReference in attributeObject.DimensionReferences)
                        {
                            ItemStatus dimensionStatus;
                            if (components.TryGetValue(dimensionReference, out dimensionStatus))
                            {
                                var parameters = new DbParameter[2];
                                parameters[0] = state.Database.CreateInParameter(IdParameter, DbType.Int64, attributeStatus.SysID);
                                parameters[1] = state.Database.CreateInParameter(GroupIdParameter, DbType.Int64, dimensionStatus.SysID);
                                parameterList.Add(parameters);
                            }
                        }
                    }
                }
            }

            state.UsingLogger().ExecuteNonQueryFormat("insert into ATTR_DIMS (ATTR_ID, DIM_ID) VALUES ({0}, {1})", parameterList);
        }

        /// <summary>
        ///     Insert attribute group.
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="dsd">
        ///     The DSD.
        /// </param>
        /// <param name="groups">
        ///     The groups.
        /// </param>
        /// <param name="components">
        ///     The components.
        /// </param>
        private static void InsertAttributeGroup(DbTransactionState state, IDataStructureObject dsd, ItemStatusCollection groups, ItemStatusCollection components)
        {
            var parameterList = new List<DbParameter[]>();
            foreach (var attributeObject in dsd.GroupAttributes)
            {
                ItemStatus dsdGroupStatus;
                if (attributeObject.AttachmentGroup != null && groups.TryGetValue(attributeObject.AttachmentGroup, out dsdGroupStatus))
                {
                    ItemStatus attributeStatus;
                    if (components.TryGetValue(attributeObject.Id, out attributeStatus))
                    {
                        var parameters = new DbParameter[2];
                        parameters[0] = state.Database.CreateInParameter(IdParameter, DbType.Int64, attributeStatus.SysID);
                        parameters[1] = state.Database.CreateInParameter(GroupIdParameter, DbType.Int64, dsdGroupStatus.SysID);
                        parameterList.Add(parameters);
                    }
                }
            }

            state.UsingLogger().ExecuteNonQueryFormat("insert into ATT_GROUP (COMP_ID, GR_ID) VALUES ({0}, {1})", parameterList);
        }

        /// <summary>
        ///     Insert dimension group.
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="dsd">
        ///     The DSD.
        /// </param>
        /// <param name="groups">
        ///     The groups.
        /// </param>
        /// <param name="components">
        ///     The components.
        /// </param>
        private static void InsertDimensionGroup(DbTransactionState state, IDataStructureObject dsd, ItemStatusCollection groups, ItemStatusCollection components)
        {
            var parameterList = new List<DbParameter[]>();
            foreach (var dsdGroup in dsd.Groups)
            {
                ItemStatus dsdGroupStatus;
                if (groups.TryGetValue(dsdGroup.Id, out dsdGroupStatus))
                {
                    foreach (var dimensionRef in dsdGroup.DimensionRefs)
                    {
                        ItemStatus dimensionStatus;
                        if (components.TryGetValue(dimensionRef, out dimensionStatus))
                        {
                            var parameters = new DbParameter[2];
                            parameters[0] = state.Database.CreateInParameter(IdParameter, DbType.Int64, dimensionStatus.SysID);
                            parameters[1] = state.Database.CreateInParameter(GroupIdParameter, DbType.Int64, dsdGroupStatus.SysID);
                            parameterList.Add(parameters);
                        }
                    }
                }
            }

            state.UsingLogger().ExecuteNonQueryFormat("insert into DIM_GROUP (COMP_ID, GR_ID) VALUES ({0}, {1})", parameterList);
        }

        /// <summary>
        /// Updates the Data Structure with the Cross References.
        /// </summary>
        /// <param name="state">The transaction</param>
        /// <param name="maintainableObject">The <see cref="IDataStructureObject"/> being replaced.</param>
        /// <param name="finalStatus">The <paramref name="maintainableObject"/>'s final status.</param>
        /// <param name="crossReferencingObjects">The Cross References.</param>
        /// <returns></returns>
        protected override ArtefactImportStatus HandleNonFinalArtefactReplace(DbTransactionState state, IMaintainableObject maintainableObject, ArtefactFinalStatus finalStatus,
            IList<IMaintainableMutableObject> crossReferencingObjects)
        {
            var updateDatastructureEngine = new UpdateDatastructureEngine(state, this);
            return updateDatastructureEngine.Update((IDataStructureObject)maintainableObject, finalStatus, crossReferencingObjects);
        }

        /// <summary>
        /// In this case the DSD is deleted and re-inserted.
        /// </summary>
        /// <remarks>
        /// Remove this method or call the base method, to update the existing records of the DSD and its references.
        /// This mechanism is not fully supported yet.
        /// <see cref="HandleNonFinalArtefactReplace(DbTransactionState, IMaintainableObject, ArtefactFinalStatus, IList{IMaintainableMutableObject})"/>
        /// </remarks>
        /// <param name="state"></param>
        /// <param name="artefact"></param>
        /// <param name="finalStatus"></param>
        /// <param name="crossReferences"></param>
        /// <param name="crossReferencingObjects"></param>
        /// <returns></returns>
        protected override List<ArtefactImportStatus> UpdateNonFinalInUse(DbTransactionState state, IMaintainableObject artefact, ArtefactFinalStatus finalStatus,
            IList<string> crossReferences, IList<IMaintainableMutableObject> crossReferencingObjects)
        {
            //return DeleteInsert(state, artefact, finalStatus); // use this to delete dsd and re-insert it
            return base.UpdateNonFinalInUse(state, artefact, finalStatus, crossReferences, crossReferencingObjects); // use this to try update the separate components
        }

        /// <inheritdoc/>
        protected override IList<ITextFormat> GetTextFormats(IDataStructureObject maintainable)
        {
            return maintainable.GetAllComponents()//TODO SDMXRI-1919: maybe replace with IComponent extension method?
                .Where(c => c.Representation != null && c.Representation.TextFormat != null)
                .Select(c => c.Representation.TextFormat).ToList();
        }

        public IList<ITextFormat> GetTextFormats(params IComponent[] components)
        {
            return components//TODO SDMXRI-1919: maybe replace with IComponent extension method?
                .Where(c => c.Representation != null && c.Representation.TextFormat != null)
                .Select(c => c.Representation.TextFormat).ToList();
        }

        /// <inheritdoc/>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(IDataStructureObject maintainable)
        {
            List<ReferenceToOtherArtefact> referenceToOtherArtefacts = new List<ReferenceToOtherArtefact>();
            if (maintainable.MetadataStructure != null)
            {
                referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(maintainable.MetadataStructure, RefTypes.Metadata));
            }

            IDimension crossMeasureDimension = null;
            // Workaround/Special case for SDMX 2.0 Cross Sectional DSD with Measure
            ICrossSectionalDataStructureObject crossDsd20 = maintainable as ICrossSectionalDataStructureObject;
            if (crossDsd20 != null)
            {
                crossMeasureDimension = crossDsd20.GetDimensions().FirstOrDefault(d => crossDsd20.IsMeasureDimension(d));
            }

            foreach (IComponent component in maintainable.Components)
            {
                if (component.HasCodedRepresentation())
                {
                    if (!component.Equals(crossMeasureDimension)) 
                    {
                        referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(component.Representation.Representation, RefTypes.Enumeration));
                    }
                    else 
                    {
                        // TODO add property to get the measure dim codelist without knowing the measure dim
                        // as it is already stored in the ICrossSectionalDataStructureObject
                        ICrossReference crossReference = crossDsd20.GetCodelistForMeasureDimension(crossMeasureDimension.Id);
                        if (crossReference != null)
                        {
                            referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(crossReference, RefTypes.Enumeration, component.Id, null));
                        }
                    }
                }

                referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(component.ConceptRef, RefTypes.ConceptIdentity));
            }
            foreach (IDimension dimension in maintainable.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension))
            {
                if (ObjectUtil.ValidCollection(dimension.ConceptRole))
                {
                    referenceToOtherArtefacts.AddRange(ExtractRoles(dimension.ConceptRole));
                }
            }
            foreach (IAttributeObject attributeBean in maintainable.Attributes)
            {
                if (ObjectUtil.ValidCollection(attributeBean.ConceptRoles))
                {
                    referenceToOtherArtefacts.AddRange(ExtractRoles(attributeBean.ConceptRoles));
                }
            }
            foreach (IMeasure measureBean in maintainable.Measures)
            {
                if (ObjectUtil.ValidCollection(measureBean.ConceptRoles))
                {
                    referenceToOtherArtefacts.AddRange(ExtractRoles(measureBean.ConceptRoles));
                }
            }
            return referenceToOtherArtefacts;
        }

        private IEnumerable<ReferenceToOtherArtefact> ExtractRoles(IList<ICrossReference> roles)
        {
            return roles
                .Select(crossReferenceBean => new ReferenceToOtherArtefact(crossReferenceBean, RefTypes.ConceptRoles));
        }
    }
}