// -----------------------------------------------------------------------
// <copyright file="ItemBase7Engine.cs" company="EUROSTAT">
//   Date Created : 2022-06-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using log4net;
    using System.Linq;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure;
    using Estat.Sri.Utils.Extensions;
    using Newtonsoft.Json.Linq;
    using Estat.Sri.Utils.Helper;
    using Newtonsoft.Json;
    using System.Resources;

    /// <summary>
    /// The insert engine for a generic item.
    /// </summary>
    /// <typeparam name="TItem">The item type.</typeparam>
    /// <typeparam name="TProc">The stored procedure type.</typeparam>
    public class ItemBaseEngine<TItem, TProc> : IItemImportEngine<TItem>
        where TItem : IItemObject 
        where TProc : ItemProcedureBase, new()
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ItemBaseEngine<TItem, TProc>));

        /// <summary>
        /// The SP for inserting the items
        /// </summary>
        private readonly TProc _itemProcedure;

        private readonly InsertItemJSONProcedure _jsonProcedure = new InsertItemJSONProcedure();

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="itemProcedure">The SP for the item.</param>
        public ItemBaseEngine(TProc itemProcedure)
        {
            _itemProcedure = itemProcedure;
        }

        #region interface implementations

        /// <inheritdoc/>
        public IEnumerable<long> Insert(DbTransactionState state, IEnumerable<TItem> items, long parentArtefact)
        {
            var json = GenerateJsonFromItems(items,parentArtefact);
            string remainingJson = json;
            
            do
            {
                var splitJson = JsonTruncateUtil.SplitForMSDB(remainingJson, state.Database.ProviderName);
                remainingJson = splitJson.Item2;
                InsertItemJSON(state, splitJson.Item1, parentArtefact);

            } while (remainingJson != null);
            
            InsertChildItems(state, items, parentArtefact);

            return new List<long>();
        }

        private string GenerateJsonFromItems(IEnumerable<TItem> batch,long parentid)
        {
            JArray jsonArray = new JArray();

            foreach (TItem item in batch)
            {
                JObject jsonObject = new JObject
                {
                    { "ID", item.Id },
                    { "VI", GetValueId(item) },
                    { "PIT", GetParentItem(item) },
                    { "PIS", parentid }
                };

                JArray names = GetNamesAsJson(item);
                if (names != null)
                {
                    jsonObject.Add("N", names);
                }

                JArray descriptions = GetDescriptionsAsJson(item);
                if (descriptions != null)
                {
                    jsonObject.Add("D", descriptions);
                }

                JArray annotations = GetAnnotationsAsJson(item);
                if (annotations != null)
                {
                    jsonObject.Add("A", annotations);
                }

                jsonArray.Add(jsonObject);
            }

            return JsonConvert.SerializeObject(jsonArray, Formatting.None);
        }

        protected JArray GetNamesAsJson(TItem item)
        {
            return ItemJsonFieldsUtils.TextTypeWrapperToJson(item.Names.ToList());
        }

        protected JArray GetDescriptionsAsJson(TItem item)
        {
            return ItemJsonFieldsUtils.TextTypeWrapperToJson(item.Descriptions.ToList());
        }

        protected JArray GetAnnotationsAsJson(TItem item)
        {
            return ItemJsonFieldsUtils.AnnotationToJson(item.Annotations.ToList());
        }

        /// <inheritdoc/>
        public IEnumerable<long> Insert(DbTransactionState state, IEnumerable<TItem> items, long parentArtefact, IDictionary<string, long> existingItems)
        {
            return Insert(state, items, parentArtefact);
        }

        /// <summary>
        /// Override when we have an actual items scheme with items that contain real hiearchy where the path uniquely identifies an item. i.e. only CategoryScheme/Category from the current 
        /// Warning the SdmxCore made all item schemes with parent e.g. codes and concepts hierarchical they are not, their ids are unique identifiers not the path
        /// Those implementations should avoid recursion as this is a library
        /// </summary>
        /// <param name="state">The current state</param>
        /// <param name="rootItems">the root items</param>
        /// <param name="parentArtefact">The primary key of the parent item scheme</param>
        protected virtual void InsertChildItems(DbTransactionState state, IEnumerable<TItem> rootItems, long parentArtefact)
        {
            // do nothing in all cases except CategoryScheme
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="state"></param>
        /// <param name="items"></param>
        /// <param name="parentArtefact"></param>
        /// <param name="parentItem"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IEnumerable<long> Insert(DbTransactionState state, IEnumerable<TItem> items, long parentArtefact, long parentItem)
        {
            throw new NotImplementedException();
        }

        #endregion

        public long InsertItem(DbTransactionState state, TItem item, long parentArtefact)
        {
            string names = GetNamesAsJson(item).ToString(Formatting.None);
          
            string descriptions = GetDescriptionsAsJson(item).ToString(Formatting.None);
          
            string annotations = GetAnnotationsAsJson(item).ToString(Formatting.None);
           
            using (DbCommand command = _itemProcedure.CreateCommandWithDefaults(state.Database))
            {
                DbParameter itemSchemeParameter = _itemProcedure.CreateSchemeIdParameter(command);
                itemSchemeParameter.Value = parentArtefact;
                DbParameter idPrameter = _itemProcedure.CreateIdParameter(command);
                idPrameter.Value = item.Id;
                _itemProcedure.CreateParameter(command, ItemProcedureBase.ParentItem, DbType.String, ParameterDirection.Input, GetParentItem(item));
                _itemProcedure.CreateParameter(command, ItemProcedureBase.ValueId, DbType.String, ParameterDirection.Input, GetValueId(item));
                _itemProcedure.CreateParameter(command, ItemProcedureBase.Names, DbType.String, ParameterDirection.Input, names);
                _itemProcedure.CreateParameter(command, ItemProcedureBase.Descriptions, DbType.String, ParameterDirection.Input, descriptions);
                _itemProcedure.CreateParameter(command, ItemProcedureBase.Annotations, DbType.String, ParameterDirection.Input, annotations);

                DbParameter outputParameter = _itemProcedure.CreateOutputParameter(command);
                command.ExecuteNonQueryAndLog();
                var itemId = (long)outputParameter.Value;


                return itemId;
            }
        }


        public long InsertItemJSON(DbTransactionState state,string json, long parentArtefact)
        {
            using (DbCommand command = _jsonProcedure.CreateCommand(state.Database))
            {
                var jsonParam = _jsonProcedure.CreateJSONParameter(command);
                jsonParam.Value = json;
               // DbParameter outputParameter = _itemProcedure.CreateOutputParameter(command);
                command.ExecuteNonQueryAndLog();
                //var itemId = (long)outputParameter.Value;
                return -1;
            }
        }

        /// <summary>
        /// The VALUE_ID of the item to insert.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected virtual string GetValueId(TItem item)
        {
            return null;
        }

        /// <summary>
        /// The PARENT_ITEM of the item to insert.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected virtual string GetParentItem(TItem item)
        {
            return null;
        }
    }
}
