// -----------------------------------------------------------------------
// <copyright file="ItemEngine7.cs" company="EUROSTAT">
//   Date Created : 2022-06-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// The prsistence engine for a generic item.
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class ItemEngine<TItem> : ItemBaseEngine<TItem, ItemProcedure>
        where TItem : IItemObject
    {
        /// <summary>
        /// Initializes a new instance of the class
        /// with the default SP.
        /// </summary>
        public ItemEngine()
            : base(new ItemProcedure())
        {
        }

        /// <summary>
        /// The query format for setting the parent item.
        /// </summary>
        protected const string QueryFormat = "UPDATE ITEM SET PARENT_ITEM = {0} WHERE ITEM_ID = {1}";
    }
}
