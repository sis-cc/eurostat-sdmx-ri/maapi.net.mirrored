// -----------------------------------------------------------------------
// <copyright file="HierarchicalCodeImportEngine.cs" company="EUROSTAT">
//   Date Created : 2023-3-8
//   Copyright (c) 2009, 2023 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.Common;
using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
using Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Util;

namespace Estat.Sri.Sdmx.MappingStore.Store.Engine
{
    internal class HierarchicalCodeImportEngine : OtherNameableImportEngine
    {
        private static readonly InsertHclCode insertHclCode = new InsertHclCode();
        private readonly long hierarchyId;
        private readonly IDictionary<string, long> levelIds;

        public HierarchicalCodeImportEngine(Database mappingStore, long hierarchyId, IDictionary<string, long> levelIds) : base(mappingStore)
        {
            this.hierarchyId = hierarchyId;
            this.levelIds = levelIds;
        }

        protected override InsertOtherNameable GetInsertProcedure() => insertHclCode;

        protected override string GetParentPath(IIdentifiableObject identifiableObject)
        {
            IHierarchicalCode hierarchicalCode = identifiableObject as IHierarchicalCode;
            if (hierarchicalCode != null)
            {
                return GetParentItem(hierarchicalCode);
            }

            return null;
        }

        protected override DateTime? GetValidFrom(IIdentifiableObject identifiableObject)
        {
            IHierarchicalCode hierarchicalCode = identifiableObject as IHierarchicalCode;
            if (hierarchicalCode != null)
            {
                return hierarchicalCode.ValidFrom?.Date;
            }

            return null;
        }

        protected override DateTime? GetValidTo(IIdentifiableObject identifiableObject)
        {
            IHierarchicalCode hierarchicalCode = identifiableObject as IHierarchicalCode;
            if (hierarchicalCode != null)
            {
                return hierarchicalCode.ValidTo?.Date;
            }

            return null;
        }

        protected override void SetupExtraParameter(IIdentifiableObject identifiableObject, DbCommand command)
        {
            IHierarchicalCode hierarchicalCode = identifiableObject as IHierarchicalCode;
            if (hierarchicalCode != null)
            {
                insertHclCode.CreateHIdParameter(command).Value = hierarchyId;
                DbParameter levelIdParameter = insertHclCode.CreateLevelIdParameter(command);
                ILevelObject levelObject = hierarchicalCode.GetLevel(false);
                long levelPrimaryKey;
                if (levelObject != null && levelIds.TryGetValue(levelObject.Id, out levelPrimaryKey))
                {
                    levelIdParameter.Value = levelPrimaryKey;
                }
            }
        }

        protected override void SetupUrnClass(IIdentifiableObject identifiableObject, InsertOtherNameable insertProcedure, DbCommand command) { }
        private string GetParentItem(IHierarchicalCode item)
        {
            string fullIdPath = item.GetFullIdPath(false);
            if (!ObjectUtil.ValidString(fullIdPath))
            {
                return null;
            }

            int i = fullIdPath.LastIndexOf('.');
            if (i > 0)
            {
                return fullIdPath.Substring(0, i);
            }

            return null;
        }

    }
}
