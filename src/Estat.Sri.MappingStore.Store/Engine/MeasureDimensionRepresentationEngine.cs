// -----------------------------------------------------------------------
// <copyright file="MeasureDimensionRepresentationEngine.cs" company="EUROSTAT">
//   Date Created : 2013-09-26
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using DryIoc.FastExpressionCompiler.LightExpression;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The measure dimension representation engine.
    ///     HACK: Creates a dummy codelist for all SDMX v2.1 DSD MeasureDimensions.
    /// </summary>
    public class MeasureDimensionRepresentationEngine
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(MeasureDimensionRepresentationEngine));
        /// <summary>
        ///     The SQL query for getting all measure dimensions without codelist.
        /// </summary>
        private const string GetAllMeasureDimensionsWithoutCodelist = @"select rc.SOURCE_ARTEFACT, 
    rc.SOURCE_CHILD_FULL_ID,  
    rc.AGENCY as CONCEPTSCHEME_AGENCY,
    rc.ID as CONCEPTSCHEME_ID, 
    rc.VERSION1 as CONCEPTSCHEME_VERSION1,  
    rc.VERSION2 as CONCEPTSCHEME_VERSION2,  
    rc.VERSION3 as CONCEPTSCHEME_VERSION3, 
    rc.EXTENSION  
from REF_CHILDREN rc 
inner join N_COMPONENT nc  on rc.SOURCE_ARTEFACT  = nc.DSD_ID and nc.IS_MEASURE_DIM  =1 and rc.SOURCE_CHILD_FULL_ID  = nc.ID
where rc.REF_TYPE  = 'enumeration' 
and rc.ARTEFACT_TYPE = 'ConceptScheme'
and not exists (select 1 from REF_CHILDREN rc2 
    where rc2.STR_REF_ID != rc.STR_REF_ID 
    and rc2.SOURCE_ARTEFACT = rc.SOURCE_ARTEFACT 
    and rc2.SOURCE_CHILD_FULL_ID = rc.SOURCE_CHILD_FULL_ID
    and rc2.ARTEFACT_TYPE = 'Codelist' 
    and rc2.REF_TYPE = rc.REF_TYPE)
";

        /// <summary>
        ///     The _database.
        /// </summary>
        private readonly Database _database;

        /// <summary>
        ///     The _import engine.
        /// </summary>
        private readonly CodelistImportEngine _importEngine;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureDimensionRepresentationEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        public MeasureDimensionRepresentationEngine(Database database)
        {
            this._database = database;
            this._importEngine = new CodelistImportEngine(database);
        }

        /// <summary>
        ///     Create a dummy codelist for all SDMX v2.1 DSD MeasureDimensions.
        /// </summary>
        public void CreateDummyCodelistForAll()
        {
            using (var state = DbTransactionState.Create(this._database))
            {
                var conceptSchemesPerMeasureDimension = new Dictionary<long, ReferenceToOtherArtefact>();

                state.UsingLogger().ExecuteReaderFormat(GetAllMeasureDimensionsWithoutCodelist, reader => PopulateConceptSchemePerMeasureDimension(reader, conceptSchemesPerMeasureDimension));

                this.ConvertConceptSchemes(state, conceptSchemesPerMeasureDimension);

                state.Commit();
            }
        }

        /// <summary>
        ///     Populates the concept scheme per measure dimension.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        /// <param name="conceptSchemesPerMeasureDimension">
        ///     The concept schemes per measure dimension.
        /// </param>
        private static void PopulateConceptSchemePerMeasureDimension(IDataReader reader, IDictionary<long, ReferenceToOtherArtefact> conceptSchemesPerMeasureDimension)
        {
            int dsdIdIdx = reader.GetOrdinal("SOURCE_ARTEFACT");
            int compIdIdx = reader.GetOrdinal("SOURCE_CHILD_FULL_ID");
            int agencyIdx = reader.GetOrdinal("CONCEPTSCHEME_AGENCY");
            int idIdx = reader.GetOrdinal("CONCEPTSCHEME_ID");
            int version1Idx = reader.GetOrdinal("CONCEPTSCHEME_VERSION1");
            int version2Idx = reader.GetOrdinal("CONCEPTSCHEME_VERSION2");
            int version3Idx = reader.GetOrdinal("CONCEPTSCHEME_VERSION3");

            while (reader.Read())
            {
                var dsdId = DataReaderHelper.GetInt64(reader, dsdIdIdx);
                var compId = DataReaderHelper.GetString(reader, compIdIdx);
                var agencyId = DataReaderHelper.GetString(reader, agencyIdx);
                var id = DataReaderHelper.GetString(reader, idIdx);
                var version1 = DataReaderHelper.GetInt64(reader, version1Idx);
                var version2 = DataReaderHelper.GetInt64(reader, version2Idx);
                var version3 = DataReaderHelper.GetInt64(reader, version3Idx);
                string version = string.Join(".", new long[] {version1, version2, version3}.TakeWhile(s => s > -1).Select(v => v.ToString(CultureInfo.InvariantCulture)));
                
                var conceptSchemeReference = new StructureReferenceImpl(agencyId, id, version, SdmxStructureEnumType.ConceptScheme);
                var referenceFromDsdToConceptScheme = new ReferenceToOtherArtefact(conceptSchemeReference, RefTypes.Enumeration, compId, null);
                conceptSchemesPerMeasureDimension.Add(dsdId, referenceFromDsdToConceptScheme);
            }
        }

        /// <summary>
        ///     Converts the concept schemes to code lists.
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="conceptSchemesPerMeasureDimension">
        ///     The concept schemes per measure dimension.
        /// </param>
        private void ConvertConceptSchemes(DbTransactionState state, IEnumerable<KeyValuePair<long, ReferenceToOtherArtefact>> conceptSchemesPerMeasureDimension)
        {
            var transactionalDatabase = state.Database;
            var mutableObjectRetrievalManager = new RetrievalEngineContainer(transactionalDatabase).ConceptSchemeRetrievalEngine;
            var codelistPrimaryKeyCache = new HashSet<IStructureReference>();
            ReferenceImportEngine referenceImportEngine = new ReferenceImportEngine(transactionalDatabase);
            foreach (var keyValuePair in conceptSchemesPerMeasureDimension)
            {
                // This is correct we create a reference to a codelist with the same agency/id/version as the concept scheme
                StructureReferenceImpl reference = new StructureReferenceImpl(keyValuePair.Value.Target, SdmxStructureEnumType.CodeList);
                if (!codelistPrimaryKeyCache.Contains(reference))
                {
                    ArtefactFinalStatus artefactFinalStatus = _importEngine.GetFinalStatus(state.Database, reference);

                    if (artefactFinalStatus == null || artefactFinalStatus.IsEmpty)
                    {
                        ICommonStructureQuery conceptSchemeQuery = CommonStructureQueryCore
                            .Builder
                            .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.Null)
                            .SetStructureIdentification(keyValuePair.Value.Target)
                            .Build();
                        var conceptScheme = mutableObjectRetrievalManager.Retrieve(conceptSchemeQuery).SingleOrDefault();
                        var codelist = conceptScheme.ConvertToCodelist();
                        var importStatus = this._importEngine.Insert(state, codelist.ImmutableInstance);
                        if (importStatus == null || importStatus.PrimaryKeyValue == -1 ) 
                        {
                            _log.ErrorFormat("Could not import fake codelist for 2.1 measure dimension with concept scheme enumeration : {0}", keyValuePair.Value.Target );
                            throw new SdmxInternalServerException("Could not import fake codelist for 2.1 measure dimension with concept scheme enumeration");
                        }
                    }

                    codelistPrimaryKeyCache.Add(reference);
                }

                ReferenceToOtherArtefact refToCodelist = new ReferenceToOtherArtefact(reference, RefTypes.Enumeration, keyValuePair.Value.SubStructureFullPath, null );
                referenceImportEngine.InsertReferences(state, keyValuePair.Key, new [] {refToCodelist});
            }
        }
    }
}