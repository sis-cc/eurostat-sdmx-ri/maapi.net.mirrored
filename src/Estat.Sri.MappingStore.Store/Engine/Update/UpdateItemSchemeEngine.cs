using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.MappingStore.Store;
using Estat.Sri.MappingStore.Store.Engine.Delete;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Sdmx.MappingStore.Store.ItemComparison;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Estat.Sri.Sdmx.MappingStore.Store.ItemComparison;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStore.Store.Engine.Update;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Engine;
using Estat.Sri.MappingStoreRetrieval.Extensions;
using Estat.Sri.MappingStoreRetrieval.Model;
using System.Drawing;

namespace Estat.Sri.Sdmx.MappingStore.Store.Engine.Update
{
    /// <summary>
    ///   <br />
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class UpdateItemSchemeEngine<T, TItem> where T : IItemSchemeObject<TItem> where TItem : IItemObject
    {

        private ItemSchemeItemsUniqueIdComparisonEngine<T, TItem> _itemSchemeItemsComparisonEngine;


        protected Database _database;
        private DbTransactionState _state;

        /// <summary>Initializes a new instance of the <see cref="UpdateItemSchemeEngine{T, TItem}" /> class.</summary>
        /// <param name="database">The database.</param>
        private UpdateItemSchemeEngine(Database database)
        {
            this._itemSchemeItemsComparisonEngine = new ItemSchemeItemsUniqueIdComparisonEngine<T, TItem>(database);
            this._database = database;
        }

        public UpdateItemSchemeEngine(DbTransactionState state) : this(state.Database)
        {
            this._state = state;
        }

        /// <summary>Updates the specified item scheme.</summary>
        /// <param name="itemScheme">The item scheme.</param>
        /// <param name="finalStatus">The final status.</param>
        /// <param name="crossReferencingObjects">The cross referencing objects.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        /// <exception cref="ArgumentNullException">itemScheme
        /// or
        /// finalStatus</exception>
        public ArtefactImportStatus Update(T itemScheme, ArtefactFinalStatus finalStatus,
                IList<IMaintainableMutableObject> crossReferencingObjects)
        {
            if (itemScheme == null)
            {
                throw new ArgumentNullException(nameof(itemScheme));
            }

            if (finalStatus == null)
            {
                throw new ArgumentNullException(nameof(finalStatus));
            }

            ItemsUniqueIdComparisonResult<TItem> comparisonResult = _itemSchemeItemsComparisonEngine.CompareToDb(itemScheme, finalStatus);

            // concepts that are used from DSDs, MSDs and ConceptRoles
            List<string> usedItemIds = new List<string>();
            foreach (var crossReference in UsedCrossReferencesChecker.GetUsedCrossreferences(itemScheme, crossReferencingObjects))
            {
                usedItemIds.Add(crossReference.FullId);
            }
            //concepts that are only in the database and not in the request, will need to be deleted
            var itemsToDelete = comparisonResult.ItemsNotInRequest.Where(x => !usedItemIds.Contains(x.Id)).ToList();
            var itemsNotDeleted = comparisonResult.ItemsNotInRequest.Where(x => usedItemIds.Contains(x.Id));

            BeforeDeleteItems(itemsToDelete, itemScheme);

            int count = 0;
            var itemStructureType = itemScheme.GetChildItemStructureType();
            if (itemsToDelete.Any())
            {
                count += DeleteItems(itemsToDelete);
            }

            if (itemsNotDeleted.Any())
            {
                string itemsNotChanged = itemsNotDeleted.Select(x=>x.Id).Aggregate((t,z)=> t +"," + z);
                new ArtefactImportStatus(finalStatus.PrimaryKey, new ImportMessage(ImportMessageStatus.Warning, itemScheme.AsReference, string.Format("Deleted {0} items except the following because they were used: {1}", itemsToDelete.Count(), itemsNotChanged),
                        DatasetActionEnumType.Append));
            }

             count += UpdatePropertiesThatCanChangeInFinalArtefacts(itemScheme, _state, finalStatus);

            if (count == 0)
            {
                return null;
            }

            return new ArtefactImportStatus(finalStatus.PrimaryKey, new ImportMessage(ImportMessageStatus.Success, itemScheme.AsReference, string.Format("ItemScheme {0} items modified", itemScheme.AsReference.GetAsHumanReadableString()),
                        DatasetActionEnumType.Append));
        }

        /// <summary>
        /// Update final item scheme items
        /// </summary>
        /// <param name="maintainableObject">The Item Scheme</param>
        /// <param name="state">The transaction state</param>
        /// <param name="finalStatus">The database item scheme status</param>
        /// <returns>Number of records updated</returns>
        public ArtefactImportStatus UpdateFinalItems(T maintainableObject, DbTransactionState state, ArtefactFinalStatus finalStatus)
        {
            int count = UpdatePropertiesThatCanChangeInFinalArtefacts(maintainableObject, state, finalStatus);
            if (count == 0)
            {
                return null;
            }

            return new ArtefactImportStatus(finalStatus.PrimaryKey, new ImportMessage(ImportMessageStatus.Success, maintainableObject.AsReference, string.Format("ItemScheme {0} items modified", maintainableObject.AsReference.GetAsHumanReadableString()),
                        DatasetActionEnumType.Append));

        }

        private static int UpdatePropertiesThatCanChangeInFinalArtefacts(T maintainableObject, DbTransactionState state, ArtefactFinalStatus finalStatus)
        {
            var count = 0;
            var updateNamebleItems = new UpdateNameableItems(state.Database);

            var componentType = SdmxStructureType.GetFromEnum(maintainableObject.GetChildItemStructureType());
            var identifiableRetriever = new IdentifiableMapRetrieverEngine("ID", componentType);
            // TODO FIXME performance Do we need a long to ID map or an ID to Long ?
            var identifiableObjects = maintainableObject.Items.Cast<IIdentifiableObject>().ToArray();

            count += updateNamebleItems.UpdateItems(identifiableObjects.Cast<INameableObject>(),
                finalStatus.PrimaryKey);


            if (componentType.IsItemSchemeItem() && (ConfigManager.Config.InsertNewItems || !finalStatus.IsFinal))
            {
                var map = identifiableRetriever.Retrieve(state.Database, finalStatus.PrimaryKey, "PARENT_ITEM", componentType);
                //insert new items
                var objectsToInsert = identifiableObjects.Where(item => !map.ContainsItemId(item.Id)).ToArray();

                if (objectsToInsert.Any())
                {
                    InsertNewItemsEngine.Insert(objectsToInsert, finalStatus.PrimaryKey, state);
                    count += objectsToInsert.Count();
                    //map can be modified by insert
                    map = identifiableRetriever.Retrieve(state.Database, finalStatus.PrimaryKey, "PARENT_ITEM", componentType);
                }
                count += UpdateParentItemsEngine.Update(map, identifiableObjects, state);
            }

            return count;
        }

        /// <summary>Befores the delete items.</summary>
        /// <param name="itemsToDelete">The items to delete.</param>
        /// <param name="itemScheme">The item scheme.</param>
        protected virtual void BeforeDeleteItems(List<DatabaseItem> itemsToDelete, T itemScheme)
        {
        }

        private int DeleteItems(List<DatabaseItem> itemsToDelete)
        {
            var itemsSysIds = itemsToDelete.Select(x => x.SysId).ToList();
            return DeleteItems(this._database, itemsSysIds);
        }

        public static int DeleteItems(Database database, IList<long> itemsSysIds)
        {
            if (itemsSysIds.Count == 0)
            {
                return 0;
            }

            var query = "DELETE FROM ITEM WHERE ITEM_ID IN ({0})";
            return DbHelper.ExecuteQuery(database, itemsSysIds, query);
        }
    }
}

