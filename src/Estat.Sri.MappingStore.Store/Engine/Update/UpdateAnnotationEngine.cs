// -----------------------------------------------------------------------
// <copyright file="UpdateAnnotationEngine.cs" company="EUROSTAT">
//   Date Created : 2017-11-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine.Update
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sri.MappingStore.Store.Manager;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// The update annotation engine.
    /// </summary>
    public class UpdateAnnotationEngine
    {
        /// <summary>
        /// The annotation insert manager
        /// </summary>
        private readonly AnnotationInsertManager _annotationInsertManager;

        /// <summary>
        /// The item table builder
        /// </summary>
        private readonly IBuilder<ItemTableInfo, SdmxStructureEnumType> _itemTableBuilder;

        /// <summary>
        /// The table information builder
        /// </summary>
        private readonly TableInfoBuilder _tableInfoBuilder = new TableInfoBuilder();

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateAnnotationEngine"/> class.
        /// </summary>
        public UpdateAnnotationEngine()
        {
            _itemTableBuilder = new IdentifiableTableInfoBuilder();
            _annotationInsertManager = new AnnotationInsertManager(
                new ItemAnnotationFactory(),
                new ComponentAnnotationFactory(),
                new ArtefactAnnotationFactory());
        }

        /// <summary>
        /// Updates the specified <paramref name="annotableObject"/>.
        /// </summary>
        /// <param name="database">
        /// The database.
        /// </param>
        /// <param name="annotableObject">
        /// The annotate-able object.
        /// </param>
        /// <param name="primaryKeyValue">
        /// The primary key value.
        /// </param>
        /// <returns>
        /// The number of records updated
        /// </returns>
        public int Update(Database database, IMaintainableObject annotableObject, long primaryKeyValue)
        {
            if (database == null)
            {
                throw new ArgumentNullException(nameof(database));
            }

            if (annotableObject == null)
            {
                throw new ArgumentNullException(nameof(annotableObject));
            }

            var retrieverEngine = new MaintainableAnnotationRetrieverEngine(database);
            var databaseAnnotation = retrieverEngine.RetrieveAnnotationsWithKey(primaryKeyValue);
            var differences = CheckForDifferences(databaseAnnotation, annotableObject.Annotations);
            var deletedAnnotations = DbHelper.BulkDelete(database, "ANNOTATION", "ANN_ID", new Stack<long>(databaseAnnotation.Select(x => x.Key)));
            _annotationInsertManager.Insert(
                    annotableObject.StructureType,
                    database,
                    primaryKeyValue,
                    annotableObject.Annotations);

            return differences;
        }

        private int CheckForDifferences(IList<KeyValuePair<long, IAnnotationMutableObject>> databaseAnnotationsWithKey, IList<IAnnotation> requestAnnotations)
        {
            var databaseAnnotations = databaseAnnotationsWithKey.Select(x => x.Value).ToList();
            //first we check the number of annotations
            if (databaseAnnotations.Count() != requestAnnotations.Count)
            {
                return 1;
            }

            //check request annotations
            foreach(var requestAnnotation in requestAnnotations)
            {
                if(databaseAnnotations.Find(x => AnnotationsAreEqual(x, requestAnnotation)) ==null)
                {
                    return 1;
                }   
            }

            //check db annotations
            foreach (var dbAnnotation in databaseAnnotations)
            {
                if (requestAnnotations.FirstOrDefault(x => AnnotationsAreEqual(dbAnnotation, x)) == null)
                {
                    return 1;
                }
            }

            return 0;
        }

        private bool AnnotationsAreEqual(IAnnotationMutableObject dbAnnotation, IAnnotation requestAnnotation)
        {
            //first we check without text and if that matches we check texts
            if (EqualsWithoutText(requestAnnotation, dbAnnotation))
            {
                if(requestAnnotation.Text.Count == dbAnnotation.Text.Count)
                {
                    foreach(var requestAnnotationText in requestAnnotation.Text)
                    {
                        if (dbAnnotation.Text.FirstOrDefault(x=>AnnotationTextIsEqual(x, requestAnnotationText))!=null){
                            continue;
                        }
                        else
                        {
                            //we cant find an equivalent annotation
                            return false;
                        }
                    }

                    //all texts are the same
                    return true;
                }
                else
                {
                    //different number of texts
                    return false;
                }
            }

            return false;
        }

        private bool AnnotationTextIsEqual(ITextTypeWrapperMutableObject x, ITextTypeWrapper y)
        {
            return x.Locale.Equals(y.Locale) && x.Value.Equals(y.Value);
        }

        /// <summary>
        /// Updates the child.
        /// </summary>
        /// <typeparam name="TIdentifiable">The type of the identifiable.</typeparam>
        /// <param name="database">The database.</param>
        /// <param name="annotableObject">The annotate-able object.</param>
        /// <param name="parentPrimaryKeyValue">The parent primary key value.</param>
        /// <param name="itemTableInfo">The item table information.</param>
        /// <param name="map"></param>
        /// <returns>
        /// The number of records updated
        /// </returns>
        public int UpdateChild<TIdentifiable>(
            Database database,
            IList<TIdentifiable> annotableObject,
            long parentPrimaryKeyValue,
            ItemTableInfo itemTableInfo, IDictionary<string, long> map) where TIdentifiable : IIdentifiableObject
        {
            var retrieverEngine = new IdentifiableAnnotationRetrieverEngine(
                database,
                itemTableInfo);
            var componentType = SdmxStructureType.GetFromEnum(itemTableInfo.StructureType);
            var idMap = annotableObject.ToDictionary(o => o.AsReference.FullId);

            var itemMap = new Dictionary<long, ItemVo>();
            foreach (var x in map)
            {
                TIdentifiable submittedAnnotation;
                if (idMap.TryGetValue(x.Key, out submittedAnnotation))
                {
                    var vo = new ItemVo
                             {
                                 ItemPrimaryKey = x.Value,
                                 SubmittedAnnotation = submittedAnnotation
                             };
                    itemMap.Add(vo.ItemPrimaryKey, vo);
                }
            }

            Action<IAnnotationMutableObject, long, long> action = (annotation, itemPrimaryKey, annotationPrimaryKey) =>
                {
                    ItemVo itemVo;
                    if (itemMap.TryGetValue(itemPrimaryKey, out itemVo))
                    {
                        itemVo.DatabaseAnnotations.Add(
                            new KeyValuePair<long, IAnnotationMutableObject>(annotationPrimaryKey, annotation));
                    }
                };

            retrieverEngine.RetrieveAnnotationsWithKey(parentPrimaryKeyValue, action);

            var count = 0;
            var reminderTotal = new List<KeyValuePair<long, IList<IAnnotation>>>();
            foreach (var itemVo in itemMap)
            {
                var reminder = new LinkedList<IAnnotation>(itemVo.Value.SubmittedAnnotation.Annotations);
                count += ApplyDifferences(database, itemVo.Value.DatabaseAnnotations, reminder);
                if (reminder.Count > 0)
                {
                    reminderTotal.Add(new KeyValuePair<long, IList<IAnnotation>>(itemVo.Key, reminder.ToArray()));
                }
            }

            if (reminderTotal.Count > 0)
            {
                this._annotationInsertManager.Insert(componentType, database, reminderTotal);
                count += reminderTotal.Count;
            }

            return count;
        }

        /// <summary>
        /// Applies the differences.
        /// </summary>
        /// <param name="database">
        /// The database.
        /// </param>
        /// <param name="databaseAnnotation">
        /// The database annotation.
        /// </param>
        /// <param name="reminder">
        /// The reminder.
        /// </param>
        /// <returns>
        /// The number of records updated
        /// </returns>
        private static int ApplyDifferences(
            Database database,
            IList<KeyValuePair<long, IAnnotationMutableObject>> databaseAnnotation,
            LinkedList<IAnnotation> reminder)
        {
            var count = 0;
            var thoseThatAlreadyExist =
                new List<KeyValuePair<IAnnotation, KeyValuePair<long, IAnnotationMutableObject>>>();
            var toDeleteFromDatabase = new List<long>();

            // annotations don't have any unique identifier in SDMX. So unless everything is the same it could be considered a different annotation
            var current = reminder.First;
            if (current != null)
            {
                BuildDifferences(databaseAnnotation, current, thoseThatAlreadyExist, reminder, toDeleteFromDatabase);

                count += ApplyTextDiff(database, thoseThatAlreadyExist);
            }
            else
            {
                // delete all
                toDeleteFromDatabase.AddRange(databaseAnnotation.Select(pair => pair.Key));
            }

            if (toDeleteFromDatabase.Count > 0)
            {
                count += DbHelper.BulkDelete(database, "ANNOTATION", "ANN_ID", new Stack<long>(toDeleteFromDatabase));
            }

            return count;
        }

        /// <summary>
        /// Builds the differences.
        /// </summary>
        /// <param name="databaseAnnotation">The database annotation.</param>
        /// <param name="current">The current.</param>
        /// <param name="thoseThatAlreadyExist">The those that already exist.</param>
        /// <param name="reminder">The reminder.</param>
        /// <param name="toDeleteFromDatabase">To delete from database.</param>
        private static void BuildDifferences(
            IList<KeyValuePair<long, IAnnotationMutableObject>> databaseAnnotation,
            LinkedListNode<IAnnotation> current,
            List<KeyValuePair<IAnnotation, KeyValuePair<long, IAnnotationMutableObject>>> thoseThatAlreadyExist,
            LinkedList<IAnnotation> reminder,
            List<long> toDeleteFromDatabase)
        {
            foreach (var keyValuePair in databaseAnnotation)
            {
                var found = false;
                while (current != null && !(found = EqualsWithoutText(current.Value, keyValuePair.Value)))
                {
                    current = current.Next;
                }

                if (current != null && found)
                {
                    thoseThatAlreadyExist.Add(
                        new KeyValuePair<IAnnotation, KeyValuePair<long, IAnnotationMutableObject>>(
                            current.Value,
                            keyValuePair));
                    reminder.Remove(current);
                }
                else
                {
                    toDeleteFromDatabase.Add(keyValuePair.Key);
                }

                // move back to first
                current = reminder.First;
            }
        }

        /// <summary>
        /// Applies the text difference.
        /// </summary>
        /// <param name="database">
        /// The database.
        /// </param>
        /// <param name="thoseThatAlreadyExist">
        /// The those that already exist.
        /// </param>
        /// <returns>
        /// The number of changes
        /// </returns>
        private static int ApplyTextDiff(
            Database database,
            List<KeyValuePair<IAnnotation, KeyValuePair<long, IAnnotationMutableObject>>> thoseThatAlreadyExist)
        {
            var count = 0;
            var localeToDelete = new List<string>();
            var localeToUpdate = new List<KeyValuePair<string, string>>();
            var localeToInsert = new List<KeyValuePair<string, string>>();

            // check and update text
            foreach (var pair in thoseThatAlreadyExist)
            {
                var sourceAnnotation = pair.Key;
                var primaryKey = pair.Value.Key;
                var targetAnnotation = pair.Value.Value;
                Diff(sourceAnnotation, targetAnnotation, localeToDelete, localeToUpdate, localeToInsert);

                // TODO I/U/D one set of statements per pair 
                count += DeleteText(database, localeToDelete, primaryKey);

                count += UpdateText(database, localeToUpdate, primaryKey);

                count += InsertText(database, localeToInsert, primaryKey);
            }

            return count;
        }

        /// <summary>
        /// Inserts the text.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="localeToInsert">The locale to insert.</param>
        /// <param name="primaryKey">The primary key.</param>
        /// <returns>
        /// The number of changes
        /// </returns>
        private static int InsertText(
            Database database,
            List<KeyValuePair<string, string>> localeToInsert,
            long primaryKey)
        {
            var count = 0;
            if (localeToInsert.Count > 0)
            {
                var insertAnnotationText = new InsertAnnotationText();
                var command = insertAnnotationText.CreateCommandWithDefaults(database);
                insertAnnotationText.CreateAnnIdParameter(command).Value = primaryKey;
                var text = insertAnnotationText.CreateTextParameter(command);
                var language = insertAnnotationText.CreateLanguageParameter(command);
                foreach (var keyValuePair in localeToInsert)
                {
                    text.Value = keyValuePair.Value;
                    language.Value = keyValuePair.Key;
                    count += command.ExecuteNonQueryAndLog();
                }
            }

            return count;
        }

        /// <summary>
        /// Deletes the text.
        /// </summary>
        /// <param name="database">
        /// The database.
        /// </param>
        /// <param name="localeToDelete">
        /// The locale to delete.
        /// </param>
        /// <param name="primaryKey">
        /// The primary key.
        /// </param>
        /// <returns>
        /// The number of changes
        /// </returns>
        private static int DeleteText(Database database, List<string> localeToDelete, long primaryKey)
        {
            var count = 0;
            if (localeToDelete.Count > 0)
            {
                count += DbHelper.BulkDeleteTwoKeys(
                    database,
                    "ANNOTATION_TEXT",
                    "LANGUAGE",
                    "ANN_ID",
                    primaryKey, DbType.Int64, new Stack<string>(localeToDelete), DbType.AnsiString);
            }

            return count;
        }

        /// <summary>
        /// Updates the text.
        /// </summary>
        /// <param name="database">
        /// The database.
        /// </param>
        /// <param name="localeToUpdate">
        /// The locale to update.
        /// </param>
        /// <param name="primaryKey">
        /// The primary key.
        /// </param>
        /// <returns>
        /// The number of changes
        /// </returns>
        private static int UpdateText(
            Database database,
            List<KeyValuePair<string, string>> localeToUpdate,
            long primaryKey)
        {
            var count = 0;
            if (localeToUpdate.Count > 0)
            {
                var annId = database.CreateInParameter(nameof(primaryKey), DbType.Int64, primaryKey);
                var language = database.CreateInParameter("p_lang", DbType.AnsiString);
                var text = database.CreateInParameter("p_text", DbType.String);
                var statement = string.Format(
                    CultureInfo.InvariantCulture,
                    "UPDATE ANNOTATION_TEXT SET TEXT = {0} WHERE ANN_ID = {1} AND LANGUAGE={2}",
                    text.ParameterName,
                    annId.ParameterName,
                    language.ParameterName);
                var cmd = database.CreateCommand(CommandType.Text, statement);
                cmd.Parameters.Add(text);
                cmd.Parameters.Add(annId);
                cmd.Parameters.Add(language);

                foreach (var locale in localeToUpdate)
                {
                    language.Value = locale.Key;
                    text.Value = locale.Value;
                    count += cmd.ExecuteNonQueryAndLog();
                }
            }

            return count;
        }

        /// <summary>
        /// Checks equality of <paramref name="annotation"/> and <paramref name="mutable"/> contents without checking the annotation text.
        /// </summary>
        /// <param name="annotation">
        /// The annotation.
        /// </param>
        /// <param name="mutable">
        /// The mutable.
        /// </param>
        /// <returns>
        /// <c>true</c> if <paramref name="annotation"/> <paramref name="mutable"/> have the same content; otherwise false
        /// </returns>
        private static bool EqualsWithoutText(IAnnotation annotation, IAnnotationMutableObject mutable)
        {
            if (annotation == null)
            {
                return mutable == null;
            }

            if (mutable == null)
            {
                return false;
            }

            if (!string.Equals(mutable.Id, annotation.Id))
            {
                return false;
            }

            if (!string.Equals(mutable.Title, annotation.Title))
            {
                return false;
            }

            if (!string.Equals(mutable.Type, annotation.Type))
            {
                return false;
            }

            if (!Equals(mutable.Uri, annotation.Uri))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The diff.
        /// </summary>
        /// <param name="annotation">
        /// The annotation.
        /// </param>
        /// <param name="mutable">
        /// The mutable.
        /// </param>
        /// <param name="localeToDelete">
        /// The locale to delete.
        /// </param>
        /// <param name="localeToUpdate">
        /// The locale to update.
        /// </param>
        /// <param name="localeToInsert">
        /// The locale to insert.
        /// </param>
        private static void Diff(
            IAnnotation annotation,
            IAnnotationMutableObject mutable,
            List<string> localeToDelete,
            List<KeyValuePair<string, string>> localeToUpdate,
            List<KeyValuePair<string, string>> localeToInsert)
        {
            var annotationText = annotation.Text.ToDictionary(
                wrapper => wrapper.Locale,
                wrapper => wrapper.Value,
                StringComparer.OrdinalIgnoreCase);

            var annotationMutableText = mutable.Text.ToDictionary(
                wrapper => wrapper.Locale,
                wrapper => wrapper.Value,
                StringComparer.OrdinalIgnoreCase);

            localeToDelete.AddRange(annotationMutableText.Keys.Except(annotationText.Keys));
            foreach (var text in annotationText)
            {
                string mutableValue;
                if (annotationMutableText.TryGetValue(text.Key, out mutableValue))
                {
                    if (!string.Equals(mutableValue, text.Value))
                    {
                        localeToUpdate.Add(text);
                    }
                }
                else
                {
                    localeToInsert.Add(text);
                }
            }
        }

        /// <summary>
        /// A value object for holding the information for an item
        /// </summary>
        private class ItemVo
        {
            /// <summary>
            /// Gets or sets the item primary key.
            /// </summary>
            /// <value>
            /// The item primary key.
            /// </value>
            public long ItemPrimaryKey { get; set; }

            /// <summary>
            /// Gets the database annotations.
            /// </summary>
            /// <value>
            /// The database annotations.
            /// </value>
            public IList<KeyValuePair<long, IAnnotationMutableObject>> DatabaseAnnotations { get; } =
                new List<KeyValuePair<long, IAnnotationMutableObject>>();

            /// <summary>
            /// Gets or sets the submitted annotation.
            /// </summary>
            /// <value>
            /// The submitted annotation.
            /// </value>
            public IIdentifiableObject SubmittedAnnotation { get; set; }
        }
    }
}