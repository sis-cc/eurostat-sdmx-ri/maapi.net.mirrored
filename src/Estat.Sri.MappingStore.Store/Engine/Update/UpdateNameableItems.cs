// -----------------------------------------------------------------------
// <copyright file="UpdateArtefact.cs" company="EUROSTAT">
//   Date Created : 2017-11-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.MappingStoreRetrieval.Engine;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.MappingStoreRetrieval.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Estat.Sri.MappingStoreRetrieval.Helper;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json.Linq;
using Estat.Sri.Utils.Helper;
using Newtonsoft.Json;
using Estat.Sri.SdmxParseBase.Engine;
using System.Reflection;
using System.Transactions;

namespace Estat.Sri.MappingStore.Store.Engine.Update
{
    internal class UpdateNameableItems
    {
        private Database _mappingStoreDatabase;

        public UpdateNameableItems(Database database)
        {
            this._mappingStoreDatabase = database;
        }

        /// <summary>
        /// Updates the item names and descriptions.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <param name="parentPrimaryKeyValue">The parent primary key value.</param>
        /// 
        /// <returns>
        /// The number of changes
        /// </returns>
        public int UpdateItems(
            IEnumerable<INameableObject> items,
            long parentPrimaryKeyValue)
        {
            var count = 0;
            var databaseNameable = this.RetrieveItemsLocalisation(parentPrimaryKeyValue);
            var itemsToUpdate = new Dictionary<long, (string, string, string)>();
            bool matched = true;
            foreach (var item in databaseNameable)
            {
                //we need to match by SDMX ID
                var match = items.Where(x => x.Id == item.Value.ItemId).FirstOrDefault();
                if (match != null)
                {
                    var descToUpdate = ItemJsonFieldsUtils.TextTypeWrapperToJson(match.Descriptions.ToList());
                    var nameToUpdate = ItemJsonFieldsUtils.TextTypeWrapperToJson(match.Names.ToList());
                    var annotationsToUpdate = ItemJsonFieldsUtils.AnnotationToJson(match.Annotations.ToList());
                    //compare JSONs of Nameable
                    matched = JToken.DeepEquals(nameToUpdate,item.Value.NamesJSON) &&
                         JToken.DeepEquals(descToUpdate, item.Value.DescriptionsJSON) &&
                         JToken.DeepEquals(annotationsToUpdate, item.Value.AnnotationsJSON);
                    if (!matched)
                    {
                        var nameStr = JsonConvert.SerializeObject(nameToUpdate, Formatting.None);
                        var descStr = JsonConvert.SerializeObject(descToUpdate, Formatting.None);
                        var annotationStr = JsonConvert.SerializeObject(annotationsToUpdate, Formatting.None);
                        itemsToUpdate.Add(item.Key, (nameStr, descStr, annotationStr));
                    }
                }
            }
            foreach (var itemToUpdate in itemsToUpdate)
            {
                UpdateNamesAndDescriptions(itemToUpdate);
            }
            count += itemsToUpdate.Count();
            return count;
        }

        private int UpdateNamesAndDescriptions(KeyValuePair<long, (string, string, string)> itemToUpdate)
        {
            return this._mappingStoreDatabase.UsingLogger().ExecuteNonQueryFormat(
                        "UPDATE ITEM SET ITEM_NAMES = {0},ITEM_DESCRIPTIONS = {1},ITEM_ANNOTATIONS = {2}  WHERE ITEM_ID = {3}",
                         this._mappingStoreDatabase.CreateInParameter("itemN", DbType.String, itemToUpdate.Value.Item1),
                         this._mappingStoreDatabase.CreateInParameter("itemD", DbType.String, itemToUpdate.Value.Item2),
                         this._mappingStoreDatabase.CreateInParameter("itemA", DbType.String, itemToUpdate.Value.Item3),
                         this._mappingStoreDatabase.CreateInParameter("itemId", DbType.Int64, itemToUpdate.Key));
        }

        /// <summary>
        /// Retrieves the list of <see cref="JSONNameble" /> for the specified nameable primary key value.
        /// </summary>
        /// <param name="itemSchemePrimaryKeyValue">The nameable primary key value.</param>
        /// <returns>
        /// The list of item id and <see cref="JSONNameble" /> pairs
        /// </returns>
        private Dictionary<long, JSONNameble> RetrieveItemsLocalisation(long itemSchemePrimaryKeyValue)
        {
            var query = string.Format(
                CultureInfo.InvariantCulture,
                 @"select i.ITEM_ID, i.ID, i.ITEM_NAMES, i.ITEM_DESCRIPTIONS, i.ITEM_ANNOTATIONS 
                from ITEM i  
                where i.PARENT_ITEM_SCHEME = {{0}}");
               
            var itemIdParameter = this._mappingStoreDatabase.CreateInParameter(
                "p_parentItemId",
                DbType.Int64,
                itemSchemePrimaryKeyValue);
            Dictionary<long, JSONNameble> map = new Dictionary<long, JSONNameble>();
            using (
                var command = this._mappingStoreDatabase.GetSqlStringCommandFormat(
                    query,
                    itemIdParameter))
            using (var reader = command.ExecuteReader())
            {
                int sysIdx = reader.GetOrdinal("ITEM_ID");
                int itemIdx = reader.GetOrdinal("ID");
                int dIdx = reader.GetOrdinal("ITEM_DESCRIPTIONS");
                int nIdx = reader.GetOrdinal("ITEM_NAMES");
                int aIdx = reader.GetOrdinal("ITEM_ANNOTATIONS");
                while (reader.Read())
                {
                    var sysId = DataReaderHelper.GetInt64(reader, sysIdx);

                    var namesAndDescriptions = new JSONNameble()
                    {
                        ItemId = DataReaderHelper.GetString(reader, itemIdx),
                        NamesJSON = GetAsJsonArray(reader, nIdx),
                        DescriptionsJSON = GetAsJsonArray(reader, dIdx),
                        AnnotationsJSON = GetAsJsonArray(reader, aIdx)
                    };
                    map.Add(sysId, namesAndDescriptions);
                }
            }

            return map;
        }

        private static JArray GetAsJsonArray(DbDataReader reader, int index)
        {
            if (reader.IsDBNull(index))
            {
                return new JArray();
            }

            var value = reader.GetString(index);
            if (string.IsNullOrWhiteSpace(value))
            {
                return new JArray();
            }

            return JArray.Parse(value);
        }
    }

    public class JSONNameble
    {
        public JArray NamesJSON { get; set; }
        public JArray DescriptionsJSON { get; set; }
        public JArray AnnotationsJSON { get; set; }
        public string ItemId { get; internal set; }
    }
}