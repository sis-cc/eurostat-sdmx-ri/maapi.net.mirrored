// -----------------------------------------------------------------------
// <copyright file="UpdateParentItemsEngine.cs" company="EUROSTAT">
//   Date Created : --
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at: 
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval;
using Estat.Sri.MappingStoreRetrieval.Engine;
using Estat.Sri.MappingStoreRetrieval.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

namespace Estat.Sri.MappingStore.Store.Engine.Update
{
    public class UpdateParentItemsEngine
    {
        public static int Update(IdentifiableMap map, IIdentifiableObject[] identifiableObjects, DbTransactionState state)
        {
            if (identifiableObjects.Length == 0 || !(identifiableObjects[0] is ICode))
            {
                return 0;
            }
            var itemsWithNewParents = identifiableObjects.Where(item => HasNewParent(item, map)).Cast<ICode>().ToList();
            foreach (var identifiableObject in itemsWithNewParents)
            {
                UpdateParent(state, map, identifiableObject);
            }

            return itemsWithNewParents.Count();
        }

        
        private static void UpdateParent(DbTransactionState state, IdentifiableMap map, ICode code)
        {
                state.UsingLogger().ExecuteNonQueryFormat("update ITEM set PARENT_ITEM = {0} where ITEM_ID = {1}",
                        state.Database.CreateInParameter("p_parent", DbType.AnsiString, (object)code.ParentCode ?? DBNull.Value),
                        state.Database.CreateInParameter("p_code", DbType.Int64, map.Dictionary[code.Id]));
        }

        private static bool HasNewParent(IIdentifiableObject item, IdentifiableMap identifiableMap)
        {
            if (item is ICode code)
            {
                return identifiableMap.HasParentItemId(code.Id, code.ParentCode);
            }

            return false;
        }
    }
}