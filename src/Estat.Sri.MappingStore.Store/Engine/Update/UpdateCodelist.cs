using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Estat.Sri.MappingStore.Store;
using Estat.Sri.MappingStore.Store.Engine.Delete;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Sdmx.MappingStore.Store.ItemComparison;
using log4net;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping;

namespace Estat.Sri.Sdmx.MappingStore.Store.Engine.Update
{
    public class UpdateCodelistEngine : UpdateItemSchemeEngine<ICodelistObject,ICode>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(UpdateCodelistEngine));

        
        private ItemSchemeItemsUniqueIdComparisonEngine<ICodelistObject, ICode> _itemSchemeItemsComparisonEngine;

	    public UpdateCodelistEngine(DbTransactionState state):base(state)
        {
        }

        protected override void BeforeDeleteItems(List<DatabaseItem> itemsToDelete, ICodelistObject codelist)
        {
            foreach (DatabaseItem codeToDelete in itemsToDelete)
            {
                DeleteTranscodingRules(codeToDelete, codelist);
            }
        }

        private int DeleteTranscodingRules(DatabaseItem codeToDelete, ICodelistObject codelist)
        {
            var commandText = "DELETE FROM TRANSCODING_RULE WHERE TR_RULE_ID in (" +
            "select tr.TR_RULE_ID from " +
            "TRANSCODING_RULE_DSD_CODE tr " +
            "inner join DSD_CODE c on c.LCD_ID = tr.CD_ID " +
            "inner join ITEM i on c.LCD_ID = i.ITEM_ID " +
            "inner join ARTEFACT_VIEW a on c.CL_ID = a.ART_ID " +
            "where " +
            "i.ID = {0} " +
            "and a.ID = {1} " +
            "and a.AGENCY = {2} " +
            "and a.VERSION = {3})";
            var parameters = new List<DbParameter>();
            parameters.Add(_database.CreateInParameter("codeId", DbType.String, codeToDelete.Id));
            parameters.Add(_database.CreateInParameter("codelistId", DbType.String, codelist.Id));
            parameters.Add(_database.CreateInParameter("codelistAgency", DbType.String, codelist.AgencyId));
            parameters.Add(_database.CreateInParameter("codelistVersion", DbType.String, codelist.Version));

            return this._database.ExecuteNonQueryFormat(commandText, parameters.ToArray());
        }
    }
}
