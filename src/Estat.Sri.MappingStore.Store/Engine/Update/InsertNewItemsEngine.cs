// -----------------------------------------------------------------------
// <copyright file="InsertNewItemsEngine.cs" company="EUROSTAT">
//   Date Created : 2018-10-05
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Estat.Sri.MappingStore.Store.Helper;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Engine;
using log4net;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;

namespace Estat.Sri.MappingStore.Store.Engine.Update
{
    /// <summary>
    /// InsertNewItemsEngine
    /// </summary>
    public static class InsertNewItemsEngine
    {
        /// <summary>
        /// The logger
        /// </summary>
        private static ILog _log = LogManager.GetLogger(typeof(InsertNewItemsEngine));

        /// <summary>
        /// Inserts the specified identifier map.
        /// </summary>
        /// <param name="identifiableMap">The identifier map.</param>
        /// <param name="objectsToInsert">The identifiable objects.</param>
        /// <param name="primaryKeyValue">The primary key value.</param>
        /// <param name="state"></param>
        /// <returns></returns>
        public static IEnumerable<long> Insert(IIdentifiableObject[] objectsToInsert, long primaryKeyValue, DbTransactionState state)
        {
            var type = objectsToInsert.First().GetType();

            if (typeof(ICode).IsAssignableFrom(type))
            {
                return Insert(state, objectsToInsert.Cast<ICode>(), primaryKeyValue);
            }

            if (typeof(IConceptObject).IsAssignableFrom(type))
            {
                return Insert(state, objectsToInsert.Cast<IConceptObject>(), primaryKeyValue);
            }

            if (typeof(ICategoryObject).IsAssignableFrom(type))
            {
                _log.Error("BUG: Tried to insert new category items using InsertNewItems instead of UpdateCategoryScheme");
                System.Diagnostics.Debug.Fail("BUG: Tried to insert new category items using InsertNewItems instead of UpdateCategoryScheme");
                // return Insert(state, objectsToInsert.Cast<ICategoryObject>(), primaryKeyValue, identifiableMap);
            }

            if (typeof(IAgency).IsAssignableFrom(type))
            {
                return Insert(state, objectsToInsert.Cast<IAgency>(), primaryKeyValue);
            }

            if (typeof(IOrganisationUnit).IsAssignableFrom(type))
            {
                return Insert(state, objectsToInsert.Cast<IOrganisationUnit>(), primaryKeyValue);
            }

            if (typeof(IDataProvider).IsAssignableFrom(type))
            {
                return Insert(state, objectsToInsert.Cast<IDataProvider>(), primaryKeyValue);
            }

            if (typeof(IDataConsumer).IsAssignableFrom(type))
            {
                return Insert(state, objectsToInsert.Cast<IDataConsumer>(), primaryKeyValue);
            }
            return new List<long>();
        }

        

        /// <summary>
        /// Inserts the specified state.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="state">The state.</param>
        /// <param name="items">The items.</param>
        /// <param name="parentArtefact">The parent artefact.</param>
        /// <param name="idMap"></param>
        /// <returns></returns>
        private static IEnumerable<long> Insert<T>(
            DbTransactionState state,
            IEnumerable<T> items,
            long parentArtefact) where T : IItemObject
        {
            var itemImportEngine = DefaultEngineHelper.GetItemEngine<T>();
            return itemImportEngine.Insert(state, items, parentArtefact);
        }
    }
}