// -----------------------------------------------------------------------
// <copyright file="UpdateLocalisedStringEngine.cs" company="EUROSTAT">
//   Date Created : 2014-04-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine.Update
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Sdmx.MappingStore.Store.Properties;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Util.Extensions;
    using DryIoc.ImTools;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;

    /// <summary>
    ///     Class that provides methods to update the localized string of a nameable or maintainable.
    /// </summary>
    public class UpdateLocalisedStringEngine
    {
        /// <summary>
        ///     The select localized string by foreign key
        /// </summary>
        private const string SelectLocalisedStringByForeignKey = "select ART_ID, TEXT, LANGUAGE, IS_NAME from LOCALISED_STRING_ART where IS_NAME={0} and ART_ID = {1}";

        /// <summary>
        ///     The update localized string statement. It take 2 parameters, 1. the text, 2. The ART_ID value.
        /// </summary>
        private const string UpdateLocalisedStringQueryArtect = "UPDATE LOCALISED_STRING_ART SET TEXT={0} where ART_ID = {1} and LANGUAGE = {2} and IS_NAME = {3}";

        /// <summary>
        ///     The _insert localized string
        /// </summary>
        private static readonly InsertLocalisedString _insertLocalisedString;

        /// <summary>
        ///     The mapping store database
        /// </summary>
        private readonly Database _mappingStoreDatabase;

        /// <summary>
        /// The item table builder
        /// </summary>
        private readonly IdentifiableTableInfoBuilder _itemTableBuilder;

        /// <summary>
        ///     Initializes static members of the <see cref="UpdateLocalisedStringEngine" /> class.
        /// </summary>
        static UpdateLocalisedStringEngine()
        {
            _insertLocalisedString = new StoredProcedures().InsertLocalisedString;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UpdateLocalisedStringEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDatabase">
        ///     The mapping store database.
        /// </param>
        public UpdateLocalisedStringEngine(Database mappingStoreDatabase)
        {
            this._mappingStoreDatabase = mappingStoreDatabase;
            _itemTableBuilder = new IdentifiableTableInfoBuilder();
        }

        /// <summary>
        /// Updates the specified <paramref name="mutable"/>.
        /// </summary>
        /// <param name="mutable">
        /// The nameable mutable object.
        /// </param>
        /// <param name="primaryKeyValue">
        /// The primary key value.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// <paramref name="mutable"/> is null
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// <paramref name="primaryKeyValue"/> is not valid
        /// </exception>
        /// <returns>
        /// The number of records updated
        /// </returns>
        public int Update(INameableMutableObject mutable, long primaryKeyValue)
        {
            bool isArtefact = IsArtefact(mutable);
            var count = this.Update(primaryKeyValue, LocalisedStringType.Name, isArtefact, mutable.Names);
            count += this.Update(primaryKeyValue, LocalisedStringType.Desc, isArtefact, mutable.Descriptions);
            return count;
        }

        /// <summary>
        /// Compares the names/descriptions of <paramref name="mutableLocalisation" /> against <paramref name="inDatabaseMap" /> and populates
        /// </summary>
        /// <param name="toUpdateInDatabase">To update in database.</param>
        /// <param name="toInsertInDatabase">To insert in database.</param>
        /// <param name="toDeleteInDatabase">To delete in database.</param>
        /// <param name="databaseLocalisation">The database localization.</param>
        /// <param name="mutableLocalisation">The names.</param>
        /// <param name="inDatabaseMap">The in database map.</param>
        private static void Compare(List<LocalisedStringVO> toUpdateInDatabase, List<KeyValuePair<long, ITextTypeWrapperMutableObject>> toInsertInDatabase, List<long> toDeleteInDatabase, ItemWithLocalisedString databaseLocalisation, IEnumerable<ITextTypeWrapperMutableObject> mutableLocalisation, IReadOnlyDictionary<string, LocalisedStringVO> inDatabaseMap,string type)
        {
            var inMutableMap = mutableLocalisation.ToDictionary(vo => vo.Locale);
            toUpdateInDatabase.AddRange(
                from d in inDatabaseMap
                join m in inMutableMap on d.Key equals m.Key
                where !d.Value.TextTypeWrapper.Value.Equals(m.Value.Value)
                select
                    new LocalisedStringVO
                    {
                        PrimaryKeyValue = d.Value.PrimaryKeyValue,
                        TextTypeWrapper = m.Value,
                        Type = type
                    });
            toInsertInDatabase.AddRange(
                from m in inMutableMap
                where !inDatabaseMap.ContainsKey(m.Key)
                select
                    new KeyValuePair<long, ITextTypeWrapperMutableObject>(
                    databaseLocalisation.ItemPrimaryKeyValue,
                    m.Value));
            toDeleteInDatabase.AddRange(
                from localisedStringVo in inDatabaseMap
                where !inMutableMap.ContainsKey(localisedStringVo.Key)
                select localisedStringVo.Value.PrimaryKeyValue);
        }

        /// <summary>
        ///     Determines whether the specified mutable is an <c>artefact</c>.
        /// </summary>
        /// <param name="mutable">
        ///     The mutable.
        /// </param>
        /// <returns>
        ///     true is the <c>LOCALISED_STRING.ART_ID</c> should be used; otherwise false.
        /// </returns>
        private static bool IsArtefact(IMutableObject mutable)
        {
            return mutable.StructureType.IsMaintainable || mutable.StructureType.IsOneOf(SdmxStructureEnumType.Hierarchy);
        }

        /// <summary>
        /// Inserts the specified in mutable new names.
        /// </summary>
        /// <param name="inMutableNewNames">The in mutable new names.</param>
        /// <param name="localisedType">Type of the localized.</param>
        /// <returns>The number of changes</returns>
        /// <exception cref="System.ArgumentNullException">inMutableNewNames is null</exception>
        private int Insert(IEnumerable<KeyValuePair<long, ITextTypeWrapperMutableObject>> inMutableNewNames, string localisedType)
        {
            if (inMutableNewNames == null)
            {
                throw new ArgumentNullException("inMutableNewNames");
            }

            var count = 0;
            var insertLocalisedString = _insertLocalisedString;
            using (var dbCommand = insertLocalisedString.CreateCommandWithDefaults(this._mappingStoreDatabase))
            {
                 var itemIdParam = insertLocalisedString.CreateItemIdParameter(dbCommand);
                
                var outputParam = insertLocalisedString.CreateOutputParameter(dbCommand);
                var typeParameter = insertLocalisedString.CreateTypeParameter(dbCommand);
                var languageParameter = insertLocalisedString.CreateLanguageParameter(dbCommand);

                var textParameter = insertLocalisedString.CreateTextParameter(dbCommand);
                typeParameter.Value = localisedType;
                foreach (var mutableObject in inMutableNewNames)
                {
                    if (!string.IsNullOrWhiteSpace(mutableObject.Value.Value))
                    {
                        itemIdParam.Value = mutableObject.Key;
                        languageParameter.Value = mutableObject.Value.Locale;
                        textParameter.Value = mutableObject.Value.Value;

                        // For UPDATE, INSERT, and DELETE statements, the return value is the number of rows affected by the command. For all other types of statements, the return value is -1.
                        dbCommand.ExecuteNonQueryAndLog();
                        if (outputParam.Value is long && ((long)outputParam.Value) > 0)
                        {
                            count++;
                        }
                    }
                }
            }

            return count;
        }

        /// <summary>
        /// Executes the update statement.
        /// </summary>
        /// <param name="inMutableUpdateNames">
        /// The in mutable update names.
        /// </param>
        /// <param name="statement">
        /// The statement.
        /// </param>
        /// <returns>
        /// The number of records updated
        /// </returns>
        private int ExecuteUpdateStatement(IEnumerable<LocalisedStringVO> inMutableUpdateNames, string statement)
        {
            DbParameter id = this._mappingStoreDatabase.CreateInParameter("p_lsid", DbType.Int64);
            DbParameter isName = this._mappingStoreDatabase.CreateInParameter("p_isName", DbType.Int16);
            DbParameter language = this._mappingStoreDatabase.CreateInParameter("p_language", DbType.String);
            var textParameter = this._mappingStoreDatabase.CreateInParameter("p_text", DbType.String);
            var total = 0;
            using (var cmd = this._mappingStoreDatabase.GetSqlStringCommandFormat(statement, textParameter, id,language, isName))
            {
                foreach (var name in inMutableUpdateNames)
                {
                    id.Value = name.PrimaryKeyValue;
                    isName.Value = name.Type == LocalisedStringType.Name ? 1 : 0;
                    language.Value = name.TextTypeWrapper.Locale;
                    textParameter.Value = name.TextTypeWrapper.Value;
                    var count = cmd.ExecuteNonQueryAndLog();
                    Debug.Assert(count <= 1, "Possible bug. More than 1 row affected at LOCALISED_STRING. Number of rows affected:" + count);
                    total += count;
                }
            }

            return total;
        }

        /// <summary>
        /// Inserts the specified text mutable object.
        /// </summary>
        /// <param name="textObject">
        /// The text mutable object.
        /// </param>
        /// <param name="isArtefact">
        /// The SDMX structure.
        /// </param>
        /// <param name="primaryKeyValue">
        /// The primary key value.
        /// </param>
        /// <param name="localisedType">
        /// A name or a description.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// <paramref name="textObject"/>
        ///     or
        ///     <paramref name="isArtefact"/>
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// <paramref name="isArtefact"/>
        ///     or <paramref name="primaryKeyValue"/>
        /// </exception>
        /// <returns>
        /// The number of records added
        /// </returns>
        private int Insert(IEnumerable<ITextTypeWrapperMutableObject> textObject, bool isArtefact, long primaryKeyValue, string localisedType)
        {
            if (textObject == null)
            {
                throw new ArgumentNullException("textObject");
            }

            if (primaryKeyValue < 1)
            {
                throw new ArgumentException(Resources.ExceptionInvalidPrimaryKey, "primaryKeyValue");
            }

            var insertLocalisedString = _insertLocalisedString;
            using (var dbCommand = insertLocalisedString.CreateCommand(this._mappingStoreDatabase))
            {
                if (isArtefact)
                {
                    insertLocalisedString.CreateArtIdParameter(dbCommand).Value = primaryKeyValue;
                    insertLocalisedString.CreateItemIdParameter(dbCommand);
                }
                else
                {
                    insertLocalisedString.CreateArtIdParameter(dbCommand);
                    insertLocalisedString.CreateItemIdParameter(dbCommand).Value = primaryKeyValue;
                }

                insertLocalisedString.CreateOutputParameter(dbCommand);
                var typeParameter = insertLocalisedString.CreateTypeParameter(dbCommand);
                var languageParameter = insertLocalisedString.CreateLanguageParameter(dbCommand);

                var textParameter = insertLocalisedString.CreateTextParameter(dbCommand);
                typeParameter.Value = localisedType;
                int count = 0;
                foreach (var mutableObject in textObject)
                {
                    if (!string.IsNullOrWhiteSpace(mutableObject.Value))
                    {
                        languageParameter.Value = mutableObject.Locale;
                        textParameter.Value = mutableObject.Value;
                        dbCommand.ExecuteNonQueryAndLog();
                        count++;
                    }
                }

                return count;
            }
        }

        /// <summary>
        ///     Retrieves the list of <see cref="LocalisedStringVO" /> for the specified nameable primary key value.
        /// </summary>
        /// <param name="nameablePrimaryKeyValue">
        ///     The nameable primary key value.
        /// </param>
        /// <param name="localisedType">
        ///     Type of the localized.
        /// </param>
        /// <param name="isArtefact">
        ///     Set to <c>true</c> if the foreign key is <c>ART_ID</c>.
        /// </param>
        /// <returns>
        ///     The list of <see cref="LocalisedStringVO" />
        /// </returns>
        private IEnumerable<LocalisedStringVO> Retrieve(long nameablePrimaryKeyValue, string localisedType, bool isArtefact)
        {
            var typeParameter = this._mappingStoreDatabase.CreateInParameter("p_type", DbType.Int32, localisedType.Equals(LocalisedStringType.Name, StringComparison.OrdinalIgnoreCase) ? 1 : 0);
            DbParameter artIdParameter = this._mappingStoreDatabase.CreateInParameter("p_artid", DbType.Int64, nameablePrimaryKeyValue);

            using (var command = this._mappingStoreDatabase.GetSqlStringCommandFormat(SelectLocalisedStringByForeignKey, typeParameter, artIdParameter)) 
            using (var reader = command.ExecuteReader())
            {
                int artIdIdx = reader.GetOrdinal("ART_ID");
                int textIdx = reader.GetOrdinal("TEXT");
                int langIdx = reader.GetOrdinal("LANGUAGE");
                int typeIdx = reader.GetOrdinal("IS_NAME");
                while (reader.Read())
                {
                    var lsdId = DataReaderHelper.GetInt64Nullable(reader, artIdIdx);
                    var text = DataReaderHelper.GetString(reader, textIdx);
                    var lang = DataReaderHelper.GetString(reader, langIdx);
                    var type = DataReaderHelper.GetBoolean(reader, typeIdx);
                    yield return new LocalisedStringVO { PrimaryKeyValue = lsdId.Value, TextTypeWrapper = new TextTypeWrapperMutableCore(lang, text), Type = localisedType };
                }
            }
        }

        

        /// <summary>
        /// Updates the specified primary key value.
        /// </summary>
        /// <param name="primaryKeyValue">
        /// The primary key value.
        /// </param>
        /// <param name="localisedType">
        /// Type of the localized.
        /// </param>
        /// <param name="isArtefact">
        /// Set to <c>true</c> if the foreign key is <c>ART_ID</c>.
        /// </param>
        /// <param name="names">
        /// The names.
        /// </param>
        /// <returns>
        /// The number of records
        /// </returns>
        private int Update(long primaryKeyValue, string localisedType, bool isArtefact, IEnumerable<ITextTypeWrapperMutableObject> names)
        {
            var inDatabaseMap = this.Retrieve(primaryKeyValue, localisedType, isArtefact).ToDictionary(vo => vo.TextTypeWrapper.Locale);
            var inMutableMap = names.ToDictionary(vo => vo.Locale);
            var inMutableUpdateNames = from d in inDatabaseMap join m in inMutableMap on d.Key equals m.Key where !d.Value.TextTypeWrapper.Value.Equals(m.Value.Value) select new LocalisedStringVO { PrimaryKeyValue = d.Value.PrimaryKeyValue, TextTypeWrapper = m.Value ,Type = d.Value.Type};
            var inMutableNewNames = from m in inMutableMap where !inDatabaseMap.ContainsKey(m.Key) select m.Value;
            var toDeleteInDatabase = from localisedStringVo in inDatabaseMap where !inMutableMap.ContainsKey(localisedStringVo.Key) select localisedStringVo.Key;
            var count = this.ExecuteUpdateStatement(inMutableUpdateNames, UpdateLocalisedStringQueryArtect);

            // the primary key in LOCALISED_STRING_ART is 3 columns (ART_ID, IS_NAME and LANGUAGE)
            var isName = LocalisedStringType.Name.Equals(localisedType) ? "1" : "0";
            count += DbHelper.BulkDeleteTwoKeys(this._mappingStoreDatabase,
                                                "LOCALISED_STRING_ART",
                                                "LANGUAGE",
                                                "ART_ID",
                                                primaryKeyValue,
                                                DbType.Int64,
                                                new Stack<string>(toDeleteInDatabase),
                                                DbType.AnsiString,
                                                "IS_NAME=" + isName);
            count += this.Insert(inMutableNewNames, isArtefact, primaryKeyValue, localisedType);

            return count;
        }

        /// <summary>
        /// The item with localized string.
        /// </summary>
        private class ItemWithLocalisedString
        {
            /// <summary>
            ///     Gets or sets the primary key value.
            /// </summary>
            /// <value>
            ///     The primary key value.
            /// </value>
            public long ItemPrimaryKeyValue { get; set; }

            /// <summary>
            /// Gets the localized string.
            /// </summary>
            /// <value>
            /// The localized string.
            /// </value>
            public Dictionary<string, LocalisedStringVO> Names { get; } = new Dictionary<string, LocalisedStringVO>(StringComparer.OrdinalIgnoreCase);

            /// <summary>
            /// Gets the descriptions.
            /// </summary>
            /// <value>The descriptions.</value>
            public Dictionary<string, LocalisedStringVO> Descriptions { get; } = new Dictionary<string, LocalisedStringVO>(StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        ///     The value object for the <c>LOCALISED_STRING</c>
        /// </summary>
        private class LocalisedStringVO
        {
            /// <summary>
            ///     Gets or sets the primary key value.
            /// </summary>
            /// <value>
            ///     The primary key value.
            /// </value>
            public long PrimaryKeyValue { get; set; }

            /// <summary>
            ///     Gets or sets the text type wrapper.
            /// </summary>
            /// <value>
            ///     The text type wrapper.
            /// </value>
            public ITextTypeWrapperMutableObject TextTypeWrapper { get; set; }
            /// <summary>
            /// Name or Description
            /// </summary>
            public string Type { get; set; }
        }
    }
}