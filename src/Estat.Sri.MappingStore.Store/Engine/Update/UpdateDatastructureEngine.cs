using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStore.Store;
using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Builder;
using Estat.Sri.MappingStoreRetrieval.Constants;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.MappingStoreRetrieval.Model;
using Estat.Sri.Sdmx.MappingStore.Store.ItemComparison;
using Estat.Sri.Utils.Extensions;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.Util.Objects;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Sdmx.MappingStore.Store.Engine.Update
{
    class UpdateDatastructureEngine
    {

        private DbTransactionState _state;

        private DsdImportEngine _dsdImportEngine;

        private readonly IRetrievalEngineContainer _retrievalEngineContainer;

        public UpdateDatastructureEngine(DbTransactionState state, DsdImportEngine dsdImportEngine)
        {
            if (state == null)
            {
                throw new ArgumentNullException(nameof(state));
            }
            this._state = state;
            this._dsdImportEngine = dsdImportEngine;
            _retrievalEngineContainer = new RetrievalEngineContainer(state.Database);
        }

        public ArtefactImportStatus Update(IDataStructureObject dsd, ArtefactFinalStatus finalStatus, IList<IMaintainableMutableObject> crossReferencingBeans)
        {
            if (dsd == null)
            {
                throw new ArgumentNullException(nameof(dsd));
            }

            if (dsd == finalStatus)
            {
                throw new ArgumentNullException(nameof(finalStatus));
            }

            //TODO SDMXRI-1919: maybe move related operations to ReferenceImportEngine, ComponentImportEngine, DsdGroupImportEngine ?

            var dsdInDb = GetDataStructureInDb(dsd);
            int count = 0; //counter for changes
           
            
            //Dimensions and Attributes are subclasses of components so getting components will include them
            IdentifiablesComparisonEngine<IComponent> componentsComparisonEngine = new IdentifiablesComparisonEngine<IComponent>(this._state.Database, finalStatus, SdmxStructureEnumType.Component);
            IdentifiableComparisonResult<IComponent> componentsComparisonResult = componentsComparisonEngine.CompareToDb(dsd.Components, dsdInDb.Components);
            

            //Compare groups
            IdentifiablesComparisonEngine<IGroup> groupsComparisonEngine = new IdentifiablesComparisonEngine<IGroup>(this._state.Database, finalStatus, SdmxStructureEnumType.Group);
            IdentifiableComparisonResult<IGroup> groupsComparisonResult = groupsComparisonEngine.CompareToDb(dsd.Groups, dsdInDb.Groups);

            //we start with to what we have in the db
            Dictionary<string, long> componentsSysIds = componentsComparisonResult.IdentifiableIdToSysId;
            Dictionary<string, long> groupSysIds = groupsComparisonResult.IdentifiableIdToSysId;

            var componentsNotInDb = componentsComparisonResult.IdentifiablesNotInDb;
            var groupsNotInDb = groupsComparisonResult.IdentifiablesNotInDb;

            var newComponents = this._dsdImportEngine.InsertComponents(this._state, componentsNotInDb, finalStatus.PrimaryKey);
            _dsdImportEngine.InsertComponentsReferences(_state, componentsNotInDb, finalStatus.PrimaryKey);
            count += newComponents.Count;
            var newGroups = this._dsdImportEngine.InsertGroups(this._state, groupsNotInDb, finalStatus.PrimaryKey);
            count += newGroups.Count;

            foreach (var status in newComponents)
            {
                componentsSysIds.Add(status.Id, status.SysID);
            }

            foreach (var status in newGroups)
            {
                groupSysIds.Add(status.Id, status.SysID);
            }

            var usedComponentIds = new List<string>();
            foreach (var crossReference in UsedCrossReferencesChecker.GetUsedCrossreferences(dsdInDb, crossReferencingBeans))
            {
                usedComponentIds.Add(crossReference.FullId);
            }

            var componentsToDelete = componentsComparisonResult.IdentifiablesNotInRequest.Where(x => !usedComponentIds.Contains(x.Identifiable.Id)).ToList();
            var notDeletedComponents = componentsComparisonResult.IdentifiablesNotInRequest.Where(x => usedComponentIds.Contains(x.Identifiable.Id)).ToList();
            var groupsToDelete = groupsComparisonResult.IdentifiablesNotInRequest;

            count += DeleteComponentsAndGroups(finalStatus.PrimaryKey, componentsToDelete, groupsToDelete);

            foreach (var component in componentsToDelete)
            {
                componentsSysIds.Remove(component.Identifiable.Id);
            }

            foreach (var group in groupsToDelete)
            {
                groupSysIds.Remove(group.Identifiable.Id);
            }

            var structureTypeChangedComponents = new List<IComponent>();

            count += UpdateExistingComponents(dsd, finalStatus.PrimaryKey, componentsComparisonResult.IdentifiablesBothInRequestAndInDb, structureTypeChangedComponents);

            HandleRelationshipTables(this._state, dsd, componentsSysIds, groupSysIds);

            if (structureTypeChangedComponents.Any() || notDeletedComponents.Any())
            {
                string structuresWithChangedType = "";
                if (structureTypeChangedComponents.Any())
                {
                    var componentsHumanReadableStrings = new List<string>();
                    foreach (var componentBean in structureTypeChangedComponents)
                    {
                        componentsHumanReadableStrings.Add(componentBean.AsReference.GetAsHumanReadableString());
                    }
                    structuresWithChangedType = "were not updated due to changed type: " + componentsHumanReadableStrings.Aggregate((x, y) => x + "," + y);
                }

                string notDeletedComponentsString = "";
                if (notDeletedComponents.Any())
                {
                    if (structureTypeChangedComponents.Any())
                    {
                        notDeletedComponentsString = "and the following ";
                    }
                    notDeletedComponentsString = notDeletedComponentsString + "were not deleted because they were used: " + notDeletedComponents.Select(x => x.Identifiable.Id).Aggregate((x, y) => x + "," + y);
                }

                return new ArtefactImportStatus(finalStatus.PrimaryKey, new ImportMessage(ImportMessageStatus.Warning, dsd.AsReference, string.Format("DataStructure {0} modified - the following {1} {2}", count, structuresWithChangedType, notDeletedComponents),
                            DatasetActionEnumType.Append));

            }

            if (dsd.AttributeList.MetadataAttributes.Any())
            {
                count += HandleMetadataAttributeUsage(dsd, finalStatus, componentsSysIds);
            }

            if (count == 0)
            {
                return null;
            }

            return new ArtefactImportStatus(finalStatus.PrimaryKey, new ImportMessage(ImportMessageStatus.Success, dsd.AsReference, "DataStructure {0} modified",
                                DatasetActionEnumType.Append));
        }

        private int HandleMetadataAttributeUsage(IDataStructureObject dsd, ArtefactFinalStatus finalStatus, Dictionary<string, long> identifiableIdToSysId)
        {
            // MetadataAttributeUsages are not Identifiable, in order to delete ones only in db, update others, and insert new ones,
            // we'd have to compare the entire bean. Instead we can delete all existing ones and insert.
            var deleted = DeleteMetadataAttributeUsages(finalStatus.PrimaryKey);
            var itemStatusCollection = this._dsdImportEngine.InsertMetadataAttributeUsage(this._state, dsd.AttributeList.MetadataAttributes, finalStatus.PrimaryKey);
            //we need to add the component sys ids to the list
            foreach (var component in dsd.DimensionList.Dimensions)
            {
                if (identifiableIdToSysId.ContainsKey(component.Id))
                {
                    itemStatusCollection.Add(new ItemStatus(component.Id, identifiableIdToSysId[component.Id]));
                }
                else
                {
                    throw new SdmxException($"Expecting to find dimension with id: '{component.Id}' for {dsd.Id}.");
                }
            }
            this._dsdImportEngine.InsertAttributeDimensions(this._state, dsd, itemStatusCollection);
            
            //we need to actually check if we modified something
            return 1;
        }

        private int DeleteMetadataAttributeUsages(long dsdSysId)
        {
            String query = "DELETE FROM ANNOTATION WHERE ANN_ID IN (" +
               "SELECT DISTINCT ANN_ID FROM COMPONENT_ANNOTATION WHERE COMP_ID IN (" +
               "SELECT COMP_ID FROM COMPONENT WHERE DSD_ID = {0} AND TYPE = {1}))";
            _state.UsingLogger().ExecuteNonQueryFormat(query,
            _state.Database.CreateInParameter(ParameterNameConstants.IdParameter, DbType.Int64, dsdSysId),
            _state.Database.CreateInParameter(ParameterNameConstants.ConceptIdParameter, DbType.AnsiString, SdmxComponentType.MetadataAttributeUsage));

            query = "DELETE FROM N_COMPONENT WHERE DSD_ID = {0} AND TYPE = {1}";

            return _state.UsingLogger().ExecuteNonQueryFormat(query,
            _state.Database.CreateInParameter(ParameterNameConstants.IdParameter, DbType.Int64, dsdSysId),
            _state.Database.CreateInParameter(ParameterNameConstants.ConceptIdParameter, DbType.AnsiString, SdmxComponentType.MetadataAttributeUsage));

        }

        public int DeleteComponentsAndGroups(long dsdSysId, List<DatabaseIdentifiable<IComponent>> componentsToDelete, List<DatabaseIdentifiable<IGroup>> groupsToDelete)
        {
            var count = 0;
            
            //perform the delete and update (before deleting component we need to delete its mappings)
            if (componentsToDelete.Any())
            {
                DeleteComponentMappings(dsdSysId, componentsToDelete);
                DeleteRelationships(componentsToDelete);
                DeleteComponentReferences(dsdSysId, componentsToDelete);
                count += DeleteComponents(componentsToDelete);
            }

            if (groupsToDelete.Any())
            {
                count += DeleteGroups(groupsToDelete);
            }

            return count;
        }

        private void DeleteRelationships(List<DatabaseIdentifiable<IComponent>> componentsToDelete)
        {
            //if the component is an attribute should we remove by ATTR_ID?
            var componentsSysIds = componentsToDelete.Select(x => x.SysId);
            DbHelper.BulkDelete(this._state.Database, "ATTR_DIMS", "DIM_ID", new Stack<long>(componentsSysIds));
            DbHelper.BulkDelete(this._state.Database, "ATT_MEASURE", "MEASURE_COMP_ID", new Stack<long>(componentsSysIds));
        }

        private void DeleteComponentReferences(long dsdSysId, List<DatabaseIdentifiable<IComponent>> componentsToDelete)
        {
            List<string> deletionQueries = new List<string>()
            {
                "DELETE FROM STRUCTURE_REF WHERE REF_SRC_ID in (select REF_SRC_ID from REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {0} and SOURCE_CHILD_FULL_ID = {1})",
                "DELETE FROM TEXT_FORMAT WHERE REF_SRC_ID in (select REF_SRC_ID from REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {0} and SOURCE_CHILD_FULL_ID = {1})",
                "DELETE FROM REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {0} and SOURCE_CHILD_FULL_ID = {1}",
            };
            foreach (var component in componentsToDelete)
            {
                foreach (string query in deletionQueries)
                {
                    _state.UsingLogger().ExecuteNonQueryFormat(query,
                        _state.Database.CreateInParameter(ParameterNameConstants.IdParameter, DbType.Int64, dsdSysId),
                        _state.Database.CreateInParameter(ParameterNameConstants.ConceptIdParameter, DbType.AnsiString, component.Identifiable.Id));
                }
            }
        }

        private int DeleteGroups(List<DatabaseIdentifiable<IGroup>> identifiablesNotInRequest)
        {
            //first we need to delete entries from the related tables DIM_GROUP and ATT_GROUP
            var sysIds = identifiablesNotInRequest.Select(x => x.SysId).ToList();

            string DIM_GROUP_DELETE_QUERY = "DELETE FROM DIM_GROUP WHERE GR_ID IN ({0})";
            DbHelper.ExecuteQuery(this._state.Database, sysIds, DIM_GROUP_DELETE_QUERY);

            string ATT_GROUP_DELETE_QUERY = "DELETE FROM ATT_GROUP WHERE GR_ID IN ({0})";
            DbHelper.ExecuteQuery(this._state.Database, sysIds, ATT_GROUP_DELETE_QUERY);

            //then delete the groups
            return DbHelper.BulkDelete(this._state.Database, "DSD_GROUP", "GR_ID", new Stack<long>(sysIds));
        }

        private void HandleRelationshipTables(DbTransactionState state, IDataStructureObject maintainable, Dictionary<string, long> componentsSysIds, Dictionary<string, long> groupSysIds)
        {
            DbHelper.BulkDelete(this._state.Database, "ATTR_DIMS", "ATTR_ID", new Stack<long>(componentsSysIds.Values));
            DbHelper.BulkDelete(this._state.Database, "ATT_MEASURE", "MEASURE_COMP_ID", new Stack<long>(componentsSysIds.Values));
            DbHelper.BulkDelete(this._state.Database, "DIM_GROUP", "GR_ID", new Stack<long>(groupSysIds.Values));
            DbHelper.BulkDelete(this._state.Database, "ATT_GROUP", "GR_ID", new Stack<long>(groupSysIds.Values));

            var groupsItemStatusCollection = new ItemStatusCollection(groupSysIds.Select(x => new ItemStatus(x.Key, x.Value)));
            var componentsItemStatusCollection = new ItemStatusCollection(componentsSysIds.Select(x => new ItemStatus(x.Key, x.Value)));

            this._dsdImportEngine.InsertInRelatedTables(this._state, maintainable, groupsItemStatusCollection, componentsItemStatusCollection);
        }
        private IDataStructureObject GetDataStructureInDb(IDataStructureObject dsd)
        {
            // retrieve dsd from db
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, null)
                .SetStructureIdentification(dsd.AsReference)
                .Build();
            var dsdInDb = _retrievalEngineContainer.DSDRetrievalEngine.Retrieve(structureQuery);

            if (dsdInDb == null || !dsdInDb.Any())
            {
                throw new SdmxNoResultsException($"Could nto find the DSD with urn: '{dsd.Urn}' in mapping store.");
            }
            else if (dsdInDb.Count() > 1)
            {
                throw new SdmxException($"Expecting to find one DSD in mapping store with urn: '{dsd.Urn}', but got {dsdInDb.Count()}.");
            }
            return dsdInDb.Single().ImmutableInstance;
        }

        private void DeleteComponentMappings(long dsdSysId, List<DatabaseIdentifiable<IComponent>> componentsToDelete)
        {
            string COMPONENT_MAPPING_DELETE_QUERY = "DELETE FROM N_COMPONENT_MAPPING WHERE MAP_ID IN (" +
                "select cm.MAP_ID from N_COMPONENT_MAPPING cm " +
                "inner join entity_ref_children erc on erc.ENTITY_ID = cm.STR_MAP_SET_ID " +
                "inner join REF_CHILDREN rc on rc.SOURCE_ARTEFACT = erc.ART_ID " +
                "where rc.ART_ID = {0} and erc.ENTITY_TYPE = {1} and cm.COMPONENT_ID = {2})";
            foreach (var component in componentsToDelete)
            {
                _state.UsingLogger().ExecuteNonQueryFormat(COMPONENT_MAPPING_DELETE_QUERY,
                    _state.Database.CreateInParameter(ParameterNameConstants.IdParameter, DbType.Int64, dsdSysId),
                    _state.Database.CreateInParameter("p_type", DbType.AnsiString, EntityType.MappingSet.GetDescription()),
                    _state.Database.CreateInParameter("p_comp_id", DbType.AnsiString, component.Identifiable.Id));
            }
        }

        private int DeleteComponents(List<DatabaseIdentifiable<IComponent>> componentsToDelete)
        {
            var tableInfoBuilder = new IdentifiableTableInfoBuilder();
            TableInfo componentInfo = tableInfoBuilder.Build(SdmxStructureEnumType.Component);
            var sysIds = componentsToDelete.Select(x => x.SysId);

            return DbHelper.BulkDelete(this._state.Database, componentInfo.Table, componentInfo.PrimaryKey, new Stack<long>(sysIds));
        }

        private int UpdateExistingComponents(IDataStructureObject dsd, long dsdSysId, List<IdentifiableBothInRequestAndInDb<IComponent>> componentsToUpdate, List<IComponent> structureTypeChangedComponents)
        {

            int count = 0;
            foreach (IdentifiableBothInRequestAndInDb<IComponent> componentBothInRequestAndDb in componentsToUpdate)
            {
                var componentInRequest = componentBothInRequestAndDb.Identifiable;
                var componentInDb = componentBothInRequestAndDb.DatabaseIdentifiable.Identifiable;
                long componentSysId = componentBothInRequestAndDb.DatabaseIdentifiable.SysId;

                var structureTypeChanged = CheckStructureTypeChanged(componentInRequest, componentInDb);
                if (structureTypeChanged)
                {
                    structureTypeChangedComponents.Add(componentInRequest);
                }

                count += UpdateRepresentation(dsd, componentInRequest, componentInDb, dsdSysId);
                count += UpdateConceptIdentity(componentInRequest, componentInDb, dsdSysId);
                count += UpdateConceptRoles(componentInRequest, componentInDb, dsdSysId, structureTypeChanged);
            }
            return count;
        }

        private int UpdateRepresentation(IDataStructureObject dsd, IComponent componentInRequest, IComponent componentInDb, long dsdSysId)
        {

            bool bothHaveNoCodeRepresentation = !(componentInRequest.HasCodedRepresentation() || componentInDb.HasCodedRepresentation());
            bool bothHaveTheSameCodeRepresentation = componentInRequest.HasCodedRepresentation() && componentInDb.HasCodedRepresentation()
                    && componentInDb.Representation.Representation.CreateMutableInstance().Equals(componentInRequest.Representation.Representation.CreateMutableInstance());

            if (bothHaveNoCodeRepresentation || bothHaveTheSameCodeRepresentation)
            {
                //they have same representation so do nothing
                return 0;
            }
            else
            {

                //remove transocding and rules (unless the rule is TIME_PERIOD)
                if (!componentInRequest.Id.Equals(DimensionObject.TimeDimensionFixedId))
                {
                    DeleteComponentTranscoding(dsdSysId, componentInDb.Id);
                }

                //update the db entry to match the request's representation
                if (componentInRequest.HasCodedRepresentation())
                {
                    //submitted component has coded representation, so update the database entry to use that
                    ICrossReference representation = componentInRequest.Representation.Representation;

                    return UpdateRepresentation(representation, dsdSysId, componentInDb.Id);
                }
                else
                {
                    //submitted component has no coded representation, so remove it from the database entry too
                    return RemoveRepresentation(dsdSysId, componentInDb.Id);
                }
            }

            //TODO IN THE FUTURE check if getRepresentation.getTextFormat has changed and update it (this is pretty complicated but we have existing classes somewhere that do this)
        }

        private int UpdateRepresentation(IStructureReference representation, long dsdSysId, string componentId)
        {
            string updateRepresentationQuery = 
                "UPDATE STRUCTURE_REF SET TARGET_ARTEFACT = " +
                "(SELECT cl.CL_ID FROM CODELIST cl INNER JOIN ARTEFACT_VIEW a ON cl.CL_ID = a.ART_ID WHERE a.ID = {0} AND a.AGENCY = {1} AND a.VERSION = {2}) " +
                "WHERE REF_SRC_ID = (SELECT REF_SRC_ID FROM REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {3} AND SOURCE_CHILD_FULL_ID = {4} AND REF_TYPE = {5})";
            var idParam = this._state.Database.CreateInParameter("id", DbType.String, representation.MaintainableId);
            var agencyParam = this._state.Database.CreateInParameter("agency", DbType.String, representation.AgencyId);
            var versionParam = this._state.Database.CreateInParameter("version", DbType.String, representation.Version);
            var dsdSysIdParam = this._state.Database.CreateInParameter("dsdSysId", DbType.Int64, dsdSysId);
            var compIdParam = this._state.Database.CreateInParameter("compId", DbType.String, componentId);
            var refTypeParam = this._state.Database.CreateInParameter("refType", DbType.String, RefTypes.Enumeration);

            return this._state.UsingLogger().ExecuteNonQueryFormat(updateRepresentationQuery, 
                idParam, agencyParam, versionParam, dsdSysIdParam, compIdParam, refTypeParam);
        }

        private int RemoveRepresentation(long dsdSysId, string componentId)
        {
            List<string> deletionQueries = new List<string>()
            {
                "DELETE FROM STRUCTURE_REF WHERE REF_SRC_ID in (select REF_SRC_ID from REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {0} AND SOURCE_CHILD_FULL_ID = {1} AND REF_TYPE = {2})",
                "DELETE FROM TEXT_FORMAT WHERE REF_SRC_ID in (select REF_SRC_ID from REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {0} AND SOURCE_CHILD_FULL_ID = {1} AND REF_TYPE = {2})",
                "DELETE FROM REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {0} and SOURCE_CHILD_FULL_ID = {1} AND REF_TYPE = {2}"
            };
            int count = 0;
            foreach (string deletionQuery in deletionQueries)
            {
                var dsdSysIdParam = this._state.Database.CreateInParameter("dsdSysId", DbType.Int64, dsdSysId);
                var compIdParam = this._state.Database.CreateInParameter("compId", DbType.String, componentId);
                var refTypeParam = this._state.Database.CreateInParameter("refType", DbType.String, RefTypes.Enumeration);
                count += this._state.UsingLogger().ExecuteNonQueryFormat(deletionQuery, 
                    dsdSysIdParam, compIdParam, refTypeParam);
            }
            return count;
        }

        private void DeleteComponentTranscoding(long dsdSysId, string componentId)
        {
            string componentTranscodingDeleteQuery =
                "DELETE FROM N_TRANSCODING_RULE WHERE MAP_ID IN " +
                "(SELECT cm.MAP_ID FROM N_COMPONENT_MAPPING cm " +
                "INNER JOIN ENTITY_REF_CHILDREN erc on erc.ENTITY_ID = cm.STR_MAP_SET_ID " +
                "INNER JOIN REF_CHILDREN rc on rc.SOURCE_ARTEFACT = erc.ART_ID " +
                "where rc.ART_ID = {0} and erc.ENTITY_TYPE = {1} and cm.COMPONENT_ID = {2})";

            var dsdSysIdParam = this._state.Database.CreateInParameter("dsdSysId", DbType.Int64, dsdSysId);
            var entityTypeParam = this._state.Database.CreateInParameter("entityType", DbType.String, EntityType.MappingSet.GetDescription());
            var compIdParam = this._state.Database.CreateInParameter("compId", DbType.String, componentId);

            this._state.UsingLogger().ExecuteNonQueryFormat(componentTranscodingDeleteQuery, 
                dsdSysIdParam, entityTypeParam, compIdParam);
        }

        private bool CheckStructureTypeChanged(IComponent componentInRequest, IComponent componentInDb)
        {
            if (componentInDb.StructureType != componentInRequest.StructureType)
            {

                // Although there should be an update, do not perform actual update.
                // That's because changing attribute to dimension will need other change too like e.g. ATTR_DIMS, so just report if type has changed
                return true;

            }
            else
            {
                return false;
            }
        }

        private int UpdateConceptIdentity(IComponent componentInRequest, IComponent componentInDb, long dsdSysId)
        {

            //if both have no concept identity do nothing
            if (componentInRequest.ConceptRef == null && componentInDb.ConceptRef == null)
            {
                return 0;
            }

            //if both have the same concept identity do nothing
            if (componentInRequest.ConceptRef.CreateMutableInstance().Equals(componentInDb.ConceptRef.CreateMutableInstance()))
            {
                return 0;
            }

            string updateConceptIdentityQuery = 
                "UPDATE STRUCTURE_REF SET TARGET_CHILD_FULL_ID = {0}, TARGET_ARTEFACT = " +
                "(SELECT cs.CON_SCH_ID FROM CONCEPT_SCHEME cs INNER JOIN ARTEFACT_VIEW a ON cs.CON_SCH_ID = a.ART_ID " +
                "WHERE a.ID = {1} AND a.AGENCY = {2} AND a.VERSION = {3}) " +
                "WHERE REF_SRC_ID = (SELECT REF_SRC_ID FROM REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {4} AND SOURCE_CHILD_FULL_ID = {5} AND REF_TYPE = {6})";

            var idParam = this._state.Database.CreateInParameter("id", DbType.String, componentInRequest.ConceptRef.FullId);
            var artefactIdParam = this._state.Database.CreateInParameter("artefactIdParam", DbType.String, componentInRequest.ConceptRef.MaintainableId);
            var agencyParam = this._state.Database.CreateInParameter("agency", DbType.String, componentInRequest.ConceptRef.AgencyId);
            var versionParam = this._state.Database.CreateInParameter("version", DbType.String, componentInRequest.ConceptRef.Version);
            var dsdSysIdParam = this._state.Database.CreateInParameter("dsdSysId", DbType.Int64, dsdSysId);
            var componentIdParam = this._state.Database.CreateInParameter("componentId", DbType.String, componentInDb.Id);
            var refTypeParam = this._state.Database.CreateInParameter("refType", DbType.String, RefTypes.ConceptIdentity);

            return this._state.UsingLogger().ExecuteNonQueryFormat(updateConceptIdentityQuery, 
                idParam, artefactIdParam, agencyParam, versionParam, dsdSysIdParam, componentIdParam, refTypeParam);
        }

        private int UpdateConceptRoles(IComponent componentInRequest, IComponent componentInDb, long dsdSysId, bool structureTypeChanged)
        {
            if (structureTypeChanged)
            {
                // do not update concept roles if structure type changed
                return 0;
            }

            var requestConceptRoles = new List<ICrossReference>();
            var dbConceptRoles = new List<ICrossReference>();

            if (componentInRequest.StructureType == SdmxStructureEnumType.Dimension)
            {
                var dimensionRequestBean = (IDimension)componentInRequest;
                requestConceptRoles = dimensionRequestBean.ConceptRole.ToList();

                var dimensionDbBean = (IDimension)componentInDb;
                dbConceptRoles = dimensionDbBean.ConceptRole.ToList();
            }
            else if (componentInRequest.StructureType == SdmxStructureEnumType.DataAttribute)
            {
                var attributeRequestBean = (IAttributeObject)componentInRequest;
                requestConceptRoles = attributeRequestBean.ConceptRoles.ToList();

                var attributeDbBean = (IAttributeObject)componentInDb;
                dbConceptRoles = attributeDbBean.ConceptRoles.ToList();
            }
            else
            {
                return 0;
            }
            //we need to remove the concept roles associated wit concept scheme COMPONENT_ROLES
            var builder = new Sdmxv2ConceptRoleBuilder();
            requestConceptRoles.RemoveAll(x => builder.Build(x) != ComponentRole.None);
            dbConceptRoles.RemoveAll(x => builder.Build(x) != ComponentRole.None);

            //There is a change if number of roles in the request vs the db changed, or if roles in the request and in the db are not completely same)
            bool sizeChanged = requestConceptRoles.Count() != dbConceptRoles.Count();

            //To check if there was a change check if count changed, or if .getConceptRoles() (convert and compare mutable StructureReferenceBean) lists are not completely same)
            if (!sizeChanged)
            {

                bool rolesChanged = false;
                List<IStructureReference> requestConceptRolesRefs = ConvertToStructureReferences(requestConceptRoles);
                List<IStructureReference> dbConceptRolesRefs = ConvertToStructureReferences(dbConceptRoles);

                foreach (var requestConceptRoleRef in requestConceptRolesRefs)
                {
                    if (!dbConceptRolesRefs.Contains(requestConceptRoleRef))
                    {
                        rolesChanged = true;
                        break;
                    }
                }

                if (!rolesChanged)
                {
                    return 0;
                }
            }

            DeleteInsertReferences(dsdSysId, componentInRequest, requestConceptRoles, RefTypes.ConceptRoles);
            //since we delete all and reinsert them we do not know how many had changed, so just return 1 (if in the future we need the exact count we can calculate it by comparing the lists)
            return 1;
        }

        private void DeleteInsertReferences(long dsdSysId, IComponent component, List<ICrossReference> references, string refType)
        {
            // If there is a change we just delete existing references and reinsert them for simplicity in order to avoid finding the exact changes
            List<string> deletionQueries = new List<string>()
            {
                "DELETE FROM STRUCTURE_REF WHERE REF_SRC_ID in (select REF_SRC_ID from REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {0} and REF_TYPE = {1} and SOURCE_CHILD_FULL_ID = {2})",
                "DELETE FROM TEXT_FORMAT WHERE REF_SRC_ID in (select REF_SRC_ID from REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {0} and REF_TYPE = {1} and SOURCE_CHILD_FULL_ID = {2})",
                "DELETE FROM REFERENCE_SOURCE WHERE SOURCE_ARTEFACT = {0} and REF_TYPE = {1} and SOURCE_CHILD_FULL_ID = {2}",
            };
            foreach (string query in deletionQueries)
            {
                _state.UsingLogger().ExecuteNonQueryFormat(query,
                    _state.Database.CreateInParameter(ParameterNameConstants.IdParameter, DbType.Int64, dsdSysId),
                    _state.Database.CreateInParameter("p_type", DbType.AnsiString, refType),
                    _state.Database.CreateInParameter(ParameterNameConstants.ConceptIdParameter, DbType.AnsiString, component.Id));
            }

            var referenceImportEngine = new ReferenceImportEngine(_state.Database);
            referenceImportEngine.InsertReferences(_state, dsdSysId,
                references.Select(crossReferenceBean => new ReferenceToOtherArtefact(crossReferenceBean, refType)).ToList());
            referenceImportEngine.InsertTextFormats(_state, dsdSysId, _dsdImportEngine.GetTextFormats(component));
        }

        private List<IStructureReference> ConvertToStructureReferences(List<ICrossReference> crossReferenceBeans)
        {
            List<IStructureReference> result = new List<IStructureReference>();
            foreach (var crossReferenceBean in crossReferenceBeans)
            {
                result.Add(crossReferenceBean.CreateMutableInstance());
            }

            return result;
        }
    }
}
