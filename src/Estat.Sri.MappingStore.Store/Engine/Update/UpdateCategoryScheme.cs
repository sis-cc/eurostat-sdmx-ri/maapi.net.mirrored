// -----------------------------------------------------------------------
// <copyright file="UpdateCategoryScheme.cs" company="EUROSTAT">
//   Date Created : 2017-11-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine.Update
{
    using Estat.Sri.MappingStore.Store.Engine.Delete;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.Sdmx.MappingStore.Store.Engine.Update;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    class UpdateCategoryScheme
    {
        /// <summary>
        /// The category import engine
        /// </summary>
        private readonly CategoryImportEngine _categoryImportEngine = new CategoryImportEngine();

        /// <summary>
        /// The item table builder
        /// </summary>
        private readonly ItemTableInfoBuilder _itemTableBuilder = new ItemTableInfoBuilder();

        /// <summary>
        /// The categorisation delete engine
        /// </summary>
        private readonly CategorisationDeleteEngine _categorisationDeleteEngine = new CategorisationDeleteEngine();


        /// Updates the final artefact. Structure specific code goes here
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="maintainableObject">The maintainable object.</param>
        /// <param name="finalStatus">The final status.</param>
        /// <returns>
        /// The <see cref="ArtefactImportStatus" />.
        /// </returns>
        public ArtefactImportStatus UpdateCategories(DbTransactionState state, IMaintainableObject maintainableObject, ArtefactFinalStatus finalStatus)
        {
            //// categories are used outside SDMX in authorization, so it is not just as simple as removing and adding them
            //// We need to diff what is in the DB and what is outside

            // get the categories from DB
            MaintainableRefRetrieverEngine maintainableRefRetrieverEngine = new MaintainableRefRetrieverEngine(state.Database);
            var dbLongToCategoryURNMap = maintainableRefRetrieverEngine.RetrievesUrnMapForSpecificArtefact(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category), finalStatus.PrimaryKey);
            var dictionary = dbLongToCategoryURNMap.ToDictionary(k => new Uri(k.Value), k => k.Key);

            var newOrMovedCategories = new List<ICategoryObject>();

            var removedOrMovedKeys = dictionary.ToDictionary(k => string.Join(".", new StructureReferenceImpl(k.Key).IdentifiableIds), k => k.Value, StringComparer.Ordinal);
            var dbCategoryIdToLong = new Dictionary<string, long>(removedOrMovedKeys, StringComparer.Ordinal);
            FirstPassDifferences(maintainableObject, dictionary, newOrMovedCategories, removedOrMovedKeys);

            var removedCategories = new HashSet<long>(removedOrMovedKeys.Values);
            var newCategories = new List<ICategoryObject>();
            var movedCategories = new List<KeyValuePair<long, long?>>();
            var movedToNewCategories = new List<KeyValuePair<long, Uri>>();

            // 2nd pass we analyse the differences
            SecondPassProcessDifferences(
                dbLongToCategoryURNMap,
                dictionary,
                newOrMovedCategories,
                removedOrMovedKeys,
                dbCategoryIdToLong,
                removedCategories,
                newCategories,
                movedCategories,
                movedToNewCategories);

            int count = 0;
            // 3rd pass apply the changes
            // first add the new ones
            // we group them according the number of parents
            count += AddNewCategories(state, finalStatus, dictionary, newCategories);

            foreach(var item in movedToNewCategories)
            {
                long parentPrimaryKey;
                if (dictionary.TryGetValue(item.Value, out parentPrimaryKey))
                {
                    movedCategories.Add(new KeyValuePair<long, long?>(item.Key, parentPrimaryKey));
                }
            }

            SdmxStructureType structureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category);
            var itemTableInfo = _itemTableBuilder.Build(structureType);
            var query = string.Format(CultureInfo.InvariantCulture, "UPDATE {0} SET {1} = {{0}} WHERE {2} = {{1}}", itemTableInfo.Table, itemTableInfo.ParentItem, itemTableInfo.PrimaryKey);
            DbParameter newParent = state.Database.CreateInParameter("newParent", DbType.String);
            DbParameter sysId = state.Database.CreateInParameter("sysId", DbType.Int64);
            using (var cmd = state.Database.GetSqlStringCommandFormat(query, newParent, sysId))
            {
                foreach(var moved in movedCategories)
                {
                    var parentId = GetParentId(dbCategoryIdToLong,movedToNewCategories,moved);
                    newParent.Value = parentId;
                    sysId.Value = moved.Key;
                    count += cmd.ExecuteNonQueryAndLog();
                }
            }

            long[] identifiableSysId = removedCategories.ToArray();
            // if there are categories to be deleted then delete categorisations related to the soon to be deleted categories
            count += _categorisationDeleteEngine.HandleCategorisations(state, removedCategories,GetMovedCategoriesFullId(dbCategoryIdToLong, movedToNewCategories, movedCategories),finalStatus, dbCategoryIdToLong);
            // Delete categories no longer present - last so we don't get conflicts from moved categories that still reference at database the removed categories
            count+=UpdateItemSchemeEngine<ICategorySchemeObject, ICategoryObject>.DeleteItems(state.Database, identifiableSysId);

            UpdateNameableItems updateNameableItems = new UpdateNameableItems(state.Database);
            ICategorySchemeObject categoryScheme = (ICategorySchemeObject)maintainableObject;
            var itemLevel = new Stack<IList<ICategoryObject>>();
            itemLevel.Push(categoryScheme.Items);
            while(itemLevel.Count > 0)
            {
                var current = itemLevel.Pop();
                count += updateNameableItems.UpdateItems(current, finalStatus.PrimaryKey);
                foreach(var item in current)
                {
                    itemLevel.Push(item.Items);
                }
            }
            if (count == 0)
            {
                return null;
            }

            return new ArtefactImportStatus(finalStatus.PrimaryKey, new ImportMessage(ImportMessageStatus.Success, maintainableObject.AsReference, "Category Scheme categories modified", DatasetActionEnumType.Replace));
        }

        private string GetParentId(Dictionary<string, long> dbCategoryIdToLong, List<KeyValuePair<long, Uri>> movedToNewCategories, KeyValuePair<long, long?> moved)
        {
            //moved to existing category
            var parentId = dbCategoryIdToLong.FirstOrDefault(x => x.Value == moved.Value.Value).Key;
            if (string.IsNullOrEmpty(parentId))
            {
                //moved to new category
                var newUri = movedToNewCategories.FirstOrDefault(x => x.Key == moved.Key).Value;
                var structureRefernce = new StructureReferenceImpl(newUri);
                parentId = structureRefernce.FullId;
            }
            return parentId;
        }

        private List<KeyValuePair<string, string>> GetMovedCategoriesFullId(Dictionary<string, long> oldFullIds, List<KeyValuePair<long, Uri>> movedToNewCategories, List<KeyValuePair<long, long?>> movedCategories)
        {
            List<KeyValuePair<string, string>> movedCategoriesFullId = new List<KeyValuePair<string, string>>();
            foreach (var movedCategory in movedCategories)
            {
                string newParent = GetParentId(oldFullIds, movedToNewCategories, movedCategory);
                string oldFullId = oldFullIds.First(x => x.Value == movedCategory.Key).Key;
                string newFullId = newParent + "." + oldFullId.Split('.').Last();
                movedCategoriesFullId.Add(new KeyValuePair<string, string>(newFullId, oldFullId));
            }

            return movedCategoriesFullId;
        }

        /// <summary>
        /// Seconds the pass process differences.
        /// </summary>
        /// <param name="dbLongToCategoryURNMap">The database long to category urn map.</param>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="newOrMovedCategories">The new or moved categories.</param>
        /// <param name="removedOrMovedKeys">The removed or moved keys.</param>
        /// <param name="dbCategoryIdToLong">The database category identifier to long.</param>
        /// <param name="removedCategories">The removed categories.</param>
        /// <param name="newCategories">The new categories.</param>
        /// <param name="movedCategories">The moved categories.</param>
        /// <param name="movedToNewCategories">The moved to new categories.</param>
        private static void SecondPassProcessDifferences(IDictionary<long, string> dbLongToCategoryURNMap, Dictionary<Uri, long> dictionary, List<ICategoryObject> newOrMovedCategories, Dictionary<string, long> removedOrMovedKeys, Dictionary<string, long> dbCategoryIdToLong, HashSet<long> removedCategories, List<ICategoryObject> newCategories, List<KeyValuePair<long, long?>> movedCategories, List<KeyValuePair<long, Uri>> movedToNewCategories)
        {
            foreach (var item in newOrMovedCategories)
            {
                long primaryKey;
                var parent = item.IdentifiableParent;
                if (GetSimilarPathId(removedOrMovedKeys, item,out primaryKey))
                {
                    // ok it is not removed
                    removedCategories.Remove(primaryKey);

                    // does it have a parent or it is moved to the root ?
                    if (parent != null && parent.StructureType.EnumType == SdmxStructureEnumType.Category)
                    {

                        // does the parent exist in the db ?
                        long parentCategoryPrimaryKey;

                        // A parent exists in the DB with exact path, meaning that the category just changed a parent ?
                        if (dictionary.TryGetValue(parent.Urn, out parentCategoryPrimaryKey))
                        {
                            movedCategories.Add(new KeyValuePair<long, long?>(primaryKey, parentCategoryPrimaryKey));
                        }
                        // parent doesn't exist with the same path, but maybe it exists
                        else if (dbCategoryIdToLong.TryGetValue(parent.Id, out parentCategoryPrimaryKey))
                        {
                            // maybe the direct parent hasn't changed at all, so we get the old URN
                            string urn;
                            if (dbLongToCategoryURNMap.TryGetValue(primaryKey, out urn))
                            {
                                var oldPath = new StructureReferenceImpl(urn).IdentifiableIds;
                                if (oldPath.Count == 1)
                                {
                                    // moved from root to parent
                                    movedCategories.Add(new KeyValuePair<long, long?>(primaryKey, parentCategoryPrimaryKey));
                                }
                                else if (oldPath.Count > 1)
                                {
                                    var oldDirectParentid = oldPath[oldPath.Count - 2];
                                    if (!string.Equals(oldDirectParentid, parent.Id))
                                    {
                                        // moved to another parent
                                        movedCategories.Add(new KeyValuePair<long, long?>(primaryKey, parentCategoryPrimaryKey));
                                    }
                                }
                            }
                            else
                            {
#if DEBUG
                                Debugger.Launch();
                                Debug.Fail("dbLongToCategoryURNMap doesn't contain primaryKey as key but removedOrMovedKeys does as value");
#endif
                            }
                        }
                        else
                        {
                            // to parent category that doesn't exist in db
                            movedToNewCategories.Add(new KeyValuePair<long, Uri>(primaryKey, parent.Urn));
                        }
                    }
                    else
                    {
                        // to root
                        movedCategories.Add(new KeyValuePair<long, long?>(primaryKey, null));
                    }
                }
                else
                {
                    newCategories.Add(item);
                }
            }
        }

        private static bool GetSimilarPathId(Dictionary<string, long> removedOrMovedKeys,ICategoryObject item,out long primaryKey)
        {
            foreach(var pair in removedOrMovedKeys)
            {
                var identifiableIds = pair.Key.Split('.');
                ///is this correct?
                if(identifiableIds.Last() == item.Id)
                {
                    primaryKey = pair.Value;
                    return true;
                }
            }

            primaryKey = 0;
            return false;
        }

        /// <summary>
        /// Firsts the pass differences.
        /// </summary>
        /// <param name="maintainableObject">The maintainable object.</param>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="newOrMovedCategories">The new or moved categories.</param>
        /// <param name="removedOrMovedKeys">The removed or moved keys.</param>
        private static void FirstPassDifferences(IMaintainableObject maintainableObject, Dictionary<Uri, long> dictionary, List<ICategoryObject> newOrMovedCategories, Dictionary<string, long> removedOrMovedKeys)
        {

            // get all categories from new object
            var categories = maintainableObject.IdentifiableComposites.Where(i => i.StructureType.EnumType == SdmxStructureEnumType.Category).Cast<ICategoryObject>();

            // do the diff first pass
            foreach (var item in categories)
            {
                var key = item.Urn;
                if (dictionary.ContainsKey(key))
                {
                    removedOrMovedKeys.Remove(item.AsReference.FullId);
                }
                else
                {
                    newOrMovedCategories.Add(item);
                }
            }
        }

        /// <summary>
        /// Adds the new categories.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="finalStatus">The final status.</param>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="newCategories">The new categories.</param>
        private int AddNewCategories(DbTransactionState state, ArtefactFinalStatus finalStatus, Dictionary<Uri, long> dictionary, List<ICategoryObject> newCategories)
        {
            int count = 0;
            var newCategoriesGrouped = newCategories.GroupBy(x => x.AsReference.IdentifiableIds.Count).OrderBy(g => g.Key);
            foreach (var item in newCategoriesGrouped)
            {
                if (item.Key == 1)
                {
                    foreach (var category in item)
                    {
                        var addedCategoryId = _categoryImportEngine.InsertItem(state, category, finalStatus.PrimaryKey);
                        dictionary.Add(category.Urn, addedCategoryId);
                        count++;
                    }
                }
                else
                {
                    foreach (var category in item)
                    {
                        long parentSysId;
                        if (dictionary.TryGetValue(category.IdentifiableParent.Urn, out parentSysId))
                        {
                            var addedCategoryId = _categoryImportEngine.InsertItem(state, category, finalStatus.PrimaryKey);
                            dictionary.Add(category.Urn, addedCategoryId);
                            count++;
                        }
                        else
                        {
#if DEBUG
                            Debugger.Launch();
#endif
                        }
                    }
                }
            }

            return count;
        }
    }
}
