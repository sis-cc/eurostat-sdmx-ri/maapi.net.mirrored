// -----------------------------------------------------------------------
// <copyright file="UpdateArtefact.cs" company="EUROSTAT">
//   Date Created : 2017-11-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Engine;
using Estat.Sri.MappingStoreRetrieval.Extensions;

namespace Estat.Sri.MappingStore.Store.Engine.Update
{
    using System;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;

    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using System.Collections.Generic;
    using System.Data.Common;

    /// <summary>
    /// The update artefact engine.
    /// </summary>
    public class UpdateArtefact
    {
        /// <summary>
        /// The inequality clause builder
        /// </summary>
        private readonly InequalityClauseBuilder _inequalityClauseBuilder;

        /// <summary>
        /// The update annotation engine
        /// </summary>
        private readonly UpdateAnnotationEngine _updateAnnotationEngine;

        private readonly IBuilder<ItemTableInfo, SdmxStructureEnumType> _tableBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateArtefact"/> class.
        /// </summary>
        public UpdateArtefact()
        {
            _inequalityClauseBuilder = new InequalityClauseBuilder();
            _updateAnnotationEngine = new UpdateAnnotationEngine();
            _tableBuilder = new IdentifiableTableInfoBuilder();
        }

        /// <summary>
        /// Updates the non final attributes of <paramref name="maintainableObject" /> which exists in the DB with primary key defined in <paramref name="primaryKeyValue" />.
        /// </summary>
        /// <param name="maintainableObject">The maintainable object.</param>
        /// <param name="mappingStoreDatabase">The mapping store database.</param>
        /// <param name="primaryKeyValue">The primary key value.</param>
        /// <returns>
        /// The number of records updated
        /// </returns>
        public int UpdatedNonFinalAttributes(IMaintainableObject maintainableObject, Database mappingStoreDatabase, long primaryKeyValue)
        {
            var artId = mappingStoreDatabase.CreateInParameter("artId", DbType.Int64, primaryKeyValue);
            var validFrom = this._inequalityClauseBuilder.Build(
                mappingStoreDatabase,
                "VALID_FROM",
                ArtefactProcedurebase.ValidFromType,
                maintainableObject.StartDate?.Date);
            var validTo = this._inequalityClauseBuilder.Build(
                mappingStoreDatabase,
                "VALID_TO",
                ArtefactProcedurebase.ValidToType,
                maintainableObject.EndDate?.Date);
            var uriParam = this._inequalityClauseBuilder.Build(
                mappingStoreDatabase,
                "URI",
                ArtefactProcedurebase.UriType,
                maintainableObject.Uri?.ToString());

            var whereClauses = string.Join(" or ", new[] { validFrom, validTo, uriParam }.Select(clause => clause.WhereClause));
            var updatedRecords =
                mappingStoreDatabase.UsingLogger().ExecuteNonQueryFormat(
                    // this works with the VIEW but should probably switch to table N_ARTEFACT
                    @"UPDATE ARTEFACT SET VALID_FROM={0}, VALID_TO={1}, URI={2} WHERE ART_ID = {3} AND (" + whereClauses + " )",
                    validFrom.Parameters[0],
                    validTo.Parameters[0],
                    uriParam.Parameters[0],
                    artId);
            Debug.Assert(updatedRecords <= 1, "Updated more than Artefact");

            var updateLocalisedString = new UpdateLocalisedStringEngine(mappingStoreDatabase);
            updatedRecords += updateLocalisedString.Update(maintainableObject.MutableInstance, primaryKeyValue);
            var updateAnnotations = this._updateAnnotationEngine;
            updatedRecords += updateAnnotations.Update(mappingStoreDatabase, maintainableObject, primaryKeyValue);
            return updatedRecords;
        }

        /// <summary>
        /// Updates the identifiable composites of the specified <paramref name="maintainableObject"/>.
        /// It cannot be used by ItemSchemes
        /// </summary>
        /// <param name="maintainableObject">The maintainable object.</param>
        /// <param name="state">The mapping store database.</param>
        /// <param name="finalStatus">The final status</param>
        /// <returns>The number of changes.</returns>
        public int UpdateIdentifiableComposites(IMaintainableObject maintainableObject, DbTransactionState state, ArtefactFinalStatus finalStatus)
        {
            var count = 0;
            var identifiableComposites = maintainableObject
                .IdentifiableComposites
                .Where(o => !o.StructureType.IsItemSchemeItem()) // ItemScheme have special code
                .GroupBy(o => _tableBuilder.Build(o.StructureType), comparer: TableInfoComparer.StructureTypeComparison);

            foreach (var composite in identifiableComposites)
            {
                ItemTableInfo itemTableInfo = composite.Key;

                // check if we support this table
                if (itemTableInfo != null)
                {
                    var componentType = SdmxStructureType.GetFromEnum(itemTableInfo.StructureType);
                    var identifiableObjects = composite.ToArray();
                    var identifiableRetriever = new IdentifiableMapRetrieverEngine("ID", componentType);
                    // TODO FIXME performance Do we need a long to ID map or an ID to Long ?
                    var map = identifiableRetriever.Retrieve(state.Database, finalStatus.PrimaryKey,itemTableInfo.ParentItem, itemTableInfo.StructureType);

                    var isNameAble = identifiableObjects.FirstOrDefault() is INameableObject;

                    // check if it is nameable
                    if (isNameAble)
                    {
                        // TODO update other nameables
                    }
                    // update annotations
                    count += _updateAnnotationEngine.UpdateChild(
                        state.Database,
                        identifiableObjects,
                        finalStatus.PrimaryKey,
                        itemTableInfo, map.Dictionary);
                }
            }

            return count;
        }

        /// <summary>
        /// Updates the last modified entries
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="primaryKey">The primary key.</param>
        public void UpdateLastModified(Database database, long primaryKey)
        {
            var artId2 = database.CreateInParameter("artId", DbType.Int64, primaryKey);
            var lastModified = database.CreateInParameter(
                ArtefactProcedurebase.LastModifiedParameterName,
                ArtefactProcedurebase.LastModifiedType,
                DateTime.UtcNow);
            var c = database.UsingLogger().ExecuteNonQueryFormat(
                "UPDATE ARTEFACT SET LAST_MODIFIED = {0} WHERE ART_ID = {1}",
                lastModified,
                artId2);
            // this works with the VIEW but should probably switch to table N_ARTEFACT
            Debug.Assert(c == 1, "Updating last modified didn't update 1 record", "Records updated : '{0}'", c);
        }

        internal int UpdateFinalFlag(IMaintainableObject maintainableObject, DbTransactionState state)
        {
            var commandText = "UPDATE N_ARTEFACT SET EXTENSION = {0} WHERE ART_ID IN (SELECT ART_ID FROM ARTEFACT WHERE ID = {1} AND AGENCY = {2} AND ARTEFACT_TYPE = {3} " +
                    "AND VERSION1 = {4} AND VERSION2 = {5})";
            var version = maintainableObject.AsReference.SplitVersion();
            var parameters = new List<DbParameter>();
            var extensionParam = state.Database.CreateInParameter("extension", DbType.String);
            if (maintainableObject.IsFinal.IsTrue)
            {
                extensionParam.Value = DBNull.Value;
            }
            else
            {
                extensionParam.Value = "draft";
            }
            parameters.Add(extensionParam);
            parameters.Add(state.Database.CreateInParameter("id", DbType.String, maintainableObject.Id));
            parameters.Add(state.Database.CreateInParameter("agency", DbType.String, maintainableObject.AgencyId));
            parameters.Add(state.Database.CreateInParameter("artefactType", DbType.String, maintainableObject.StructureType.UrnClass));
            parameters.Add(state.Database.CreateInParameter("version1", DbType.Int64, version[0]));
            parameters.Add(state.Database.CreateInParameter("version2", DbType.Int64, version[1]));

            if (version.Count > 2 && version[2] != null)
            {
                commandText += " AND VERSION3 = {6}";
                parameters.Add(state.Database.CreateInParameter("version3", DbType.Int64, version[2]));

            }
            return state.Database.ExecuteNonQueryFormat(commandText, parameters.ToArray());
        }
    }
}