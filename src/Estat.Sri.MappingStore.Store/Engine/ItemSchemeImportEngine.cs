// -----------------------------------------------------------------------
// <copyright file="ItemSchemeImportEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sri.MappingStore.Store.Helper;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Estat.Sri.Sdmx.MappingStore.Store.Engine;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    using Estat.Sri.Sdmx.MappingStore.Store.Engine.Update;
    /// <summary>
    /// The engine for importing an item scheme.
    /// Compatible to MSDB 7.0
    /// </summary>
    /// <typeparam name="T">The type of item scheme to import.</typeparam>
    /// <typeparam name="TItem">The type of items in the scheme.</typeparam>
    public abstract class ItemSchemeImportEngine<T, TItem> : ArtefactImportEngine<T>
        where T : IItemSchemeObject<TItem> where TItem : IItemObject
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ItemSchemeImportEngine<T, TItem>));

        /// <summary>
        ///     The _nameable import engine.
        /// </summary>
        private readonly IItemImportEngine<TItem> _itemImportEngine;

        private static readonly ItemSchemeProcedureBase DEFAULT_PROC = new ItemSchemeProcedureBase();

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemSchemeImportEngine{T,TItem}" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        protected ItemSchemeImportEngine(Database database)
            : this(database, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemSchemeImportEngine{T,TItem}" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        /// <param name="factory">
        ///     The <see cref="IItemImportEngine{T}" /> factory. Optional
        /// </param>
        protected ItemSchemeImportEngine(Database database, IItemImportFactory<TItem> factory)
            : base(database)
        {
            factory = factory ?? new ItemImportFactory<TItem>();
            this._itemImportEngine = factory.GetItemImport();
            if (this._itemImportEngine == null) 
            {
                throw new SdmxServiceUnavailableException("Cannot find an import factory for " + typeof(TItem)) ;
            }
        }

        /// <summary>
        ///     Gets the nameable import engine.
        /// </summary>
        protected IItemImportEngine<TItem> ItemImportEngine
        {
            get
            {
                return this._itemImportEngine;
            }
        }

        /// <summary>
        ///     Deletes the child items.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="primaryKey">The primary key.</param>
        protected override void DeleteChildStructures(DbTransactionState state, long primaryKey)
        {
            // delete item
            string deleteItemQuery = "DELETE FROM ITEM WHERE PARENT_ITEM_SCHEME = {0}";
            _log.DebugFormat(CultureInfo.InvariantCulture, "Executing delete query {0}", deleteItemQuery);
            var itemsDeleted = state.UsingLogger().ExecuteNonQueryFormat(deleteItemQuery, state.Database.CreateInParameter("p_fk", DbType.Int64, primaryKey));
            _log.DebugFormat(CultureInfo.InvariantCulture, "Item records deleted {0}", itemsDeleted);

            // SQL Server does not allow a CASCADE delete 
            // TODO do we still need this ?
            state.UsingLogger().ExecuteNonQueryFormat("DELETE FROM ITEM_SCHEME WHERE ITEM_SCHEME_ID = {0}", state.Database.CreateInParameter("artId", DbType.Int64, primaryKey));
        }

        /// <inheritdoc/>
        public override ArtefactImportStatus Insert(DbTransactionState state, T itemScheme)
        {
            _log.Info(string.Format("|-- Insert Item Scheme {0}", itemScheme.Urn));
            return InsertInternal(state, itemScheme, DEFAULT_PROC);
        }

        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <param name="artefactStoredProcedure">
        ///     The artefact Stored Procedure.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        protected ArtefactImportStatus InsertInternal(DbTransactionState state, T maintainable, ItemSchemeProcedureBase artefactStoredProcedure)
        {
            return this.InsertInternal(state, maintainable, artefactStoredProcedure, null);
        }
        


        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <param name="artefactStoredProcedure">
        ///     The artefact Stored Procedure.
        /// </param>
        /// <param name="setupCommand">
        ///     The setup Command.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        protected ArtefactImportStatus InsertInternal(
            DbTransactionState state, T maintainable, ItemSchemeProcedureBase artefactStoredProcedure, Action<DbCommand> setupCommand)
        {
            Action<DbCommand> itemSchemeSetup = command =>
            {
                artefactStoredProcedure.CreateIsPartialParameter(command).Value = maintainable.Partial ? 1 : 0;
                if (setupCommand != null)
                {
                    setupCommand(command);
                }
            };
            var artefactImportStatus = this.InsertArtefactInternal(state, maintainable, artefactStoredProcedure, itemSchemeSetup);
            if (artefactImportStatus == null) 
            {
                _log.ErrorFormat("Could not insert artefact {0} using {1}", maintainable, artefactStoredProcedure);
                throw new SdmxInternalServerException("Could not insert Item scheme ");
            }
            


            var itemIds = this.ItemImportEngine.Insert(state, maintainable.Items, artefactImportStatus.PrimaryKeyValue);

            // Add the maintainable and the items to cache
            StructureCache structureCache = StructureCacheContext.GetStructureCacheFromThreadLocal() != null
                ? StructureCacheContext.GetStructureCacheFromThreadLocal()
                : new StructureCache();
            //in order to get the final status we need to commit first
            
            if (maintainable.MutableInstance.StructureType == SdmxStructureEnumType.CodeList)
            {
                // For the moment, no need to cache the codes
                structureCache.AddReferenceWithItemsToCache(state.Database, maintainable.AsReference, null, null);
            }
            else
            {
                structureCache.AddReferenceWithItemsToCache(state.Database, maintainable.AsReference,
                    maintainable.Items.Cast<IItemObject>().ToList(), itemIds.ToList());
            }
            ValidationHelper.Validate(maintainable, itemIds);
            return artefactImportStatus;
        }
/// <inheritdoc/>

        protected override ArtefactImportStatus HandleFinalArtefactReplace(DbTransactionState state, IMaintainableObject artefact, ArtefactFinalStatus finalStatus)
        {
            var updateItemSchemeEngine = new UpdateItemSchemeEngine<T, TItem>(state);
            return updateItemSchemeEngine.UpdateFinalItems((T)artefact, state, finalStatus);
        }
/// <inheritdoc/>

        protected override ArtefactImportStatus HandleNonFinalArtefactReplace(DbTransactionState state, IMaintainableObject artefact, ArtefactFinalStatus finalStatus, IList<IMaintainableMutableObject> crossReferencingObjects)
        {
            var updateItemSchemeEngine = new UpdateItemSchemeEngine<T, TItem>(state);
            return updateItemSchemeEngine.Update((T)artefact,finalStatus,crossReferencingObjects);
        }
    }
}
