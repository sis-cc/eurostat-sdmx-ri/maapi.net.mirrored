// -----------------------------------------------------------------------
// <copyright file="OtherNameableImportEngine.cs" company="EUROSTAT">
//   Date Created : 2022-06-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Data.Common;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// The engine for importing other nameables
    /// </summary>
    public class OtherNameableImportEngine
    {
        private readonly Database _mappingStore;

        private readonly LocalisedStringInsertEngine _localisedStringInsertEngine;

        private readonly OtherAnnotationInsertEngine _otherAnnotationInsertEngine;

        private readonly InsertOtherNameable _insertProcedure;

        /// <summary>
        /// The mapping store database.
        /// </summary>
        protected Database MappingStore => _mappingStore;


        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="mappingStore">The mapping store database.</param>
        /// <exception cref="ArgumentNullException">For <paramref name="mappingStore"/>.</exception>
        public OtherNameableImportEngine(Database mappingStore)
        {
            if (mappingStore == null)
            {
                throw new ArgumentNullException(nameof(mappingStore));
            }
            this._mappingStore = mappingStore;
            _localisedStringInsertEngine = new LocalisedStringInsertEngine();
            _otherAnnotationInsertEngine = new OtherAnnotationInsertEngine();
            _insertProcedure = new InsertOtherNameable();
        }

        /// <summary>
        /// Inserts the given other nameable artefact.
        /// </summary>
        /// <param name="state">The transaction.</param>
        /// <param name="identifiableObject">The other nameable to insert.</param>
        /// <param name="artefactPrimaryKey">The PK of the parent artefact.</param>
        /// <returns></returns>
        public long Insert(DbTransactionState state, IIdentifiableObject identifiableObject, long artefactPrimaryKey)
        {
            long targetSysId;
            var insertProcedure = GetInsertProcedure();
            using (DbCommand command = insertProcedure.CreateCommand(state.Database))
            {
                //TODO should have a main compulsory way of setting parameter values.
                insertProcedure.CreateIdParameter(command).Value = identifiableObject.Id;
                insertProcedure.CreateValidFromParam(command).Value = GetValidFrom(identifiableObject) ?? (object)DBNull.Value;
                insertProcedure.CreateValidToParam(command).Value = GetValidTo(identifiableObject) ?? (object)DBNull.Value;
                insertProcedure.CreateVersionParam(command).Value = GetVersion(identifiableObject) ?? (object)DBNull.Value;
                insertProcedure.CreateParentIdParam(command).Value = artefactPrimaryKey;
                insertProcedure.CreateParentPathParam(command).Value = GetParentPath(identifiableObject) ?? (object)DBNull.Value;
                SetupUrnClass(identifiableObject, insertProcedure, command);
                SetupExtraParameter(identifiableObject, command);
                var outputParameter = insertProcedure.CreateOutputParameter(command);

                command.ExecuteNonQueryAndLog();

                // get the sys_id of the inserted metadata target
                targetSysId = (long)outputParameter.Value;
            }

            _otherAnnotationInsertEngine.Insert(state, targetSysId, identifiableObject.Annotations);
            return targetSysId;
        }

        /// <summary>
        /// Override this class when the store procedure doesn't have this parameter
        /// </summary>
        /// <param name="identifiableObject"></param>
        /// <param name="insertProcedure"></param>
        /// <param name="command"></param>
        protected virtual void SetupUrnClass(IIdentifiableObject identifiableObject, InsertOtherNameable insertProcedure, DbCommand command)
        {
            insertProcedure.CreateUrnClassParam(command).Value = identifiableObject.StructureType.UrnClass;
        }

        /// <summary>
        /// Inserts the given other nameable artefact.
        /// </summary>
        /// <param name="state">The transaction.</param>
        /// <param name="nameableObject">The other nameable to insert.</param>
        /// <param name="artefactPrimaryKey">The PK of the parent artefact.</param>
        /// <returns></returns>
        public long Insert(DbTransactionState state, INameableObject nameableObject, long artefactPrimaryKey)
        {
            long targetSysId = this.Insert(state, nameableObject as IIdentifiableObject, artefactPrimaryKey);
            _localisedStringInsertEngine.InsertForOther(targetSysId, nameableObject, state.Database);
            return targetSysId;
        }

        #region stuff to override

        /// <summary>
        /// Gets the store procedure for inserting the identifiable object
        /// Override in the subclass of this class if needed
        /// </summary>
        protected virtual InsertOtherNameable GetInsertProcedure() => _insertProcedure;

        /// <summary>
        /// Get the valid from the subclass of identifiable bean
        /// By default, this method returns null
        /// Override in the subclass of this class if needed
        /// </summary>
        /// <param name="identifiableObject">the SDMX identifiable object</param>
        /// <returns>the valid from date if implemented; otherwise null</returns>
        protected virtual DateTime? GetValidFrom(IIdentifiableObject identifiableObject)
        {
            return null;
        }

        /// <summary>
        /// Get the valid to the sub class of identifiable bean
        /// By default, this method returns null
        /// Override in the subclass of this class if needed
        /// </summary>
        /// <param name="identifiableObject">the SDMX identifiable object</param>
        /// <returns>the valid to date if implemented; otherwise null</returns>
        protected virtual DateTime? GetValidTo(IIdentifiableObject identifiableObject)
        {
            return null;
        }

        /// <summary>
        /// Get the parent path the subclass of identifiable bean
        /// By default, this method returns null
        /// Override in the subclass of this class if needed
        /// </summary>
        /// <param name="identifiableObject">the SDMX identifiable object</param>
        /// <returns>the parent path if implemented; otherwise null</returns>
        protected virtual string GetParentPath(IIdentifiableObject identifiableObject)
        {
            return null;
        }

        /// <summary>
        /// Get the version the subclass of identifiable bean
        /// By default, this method returns null
        /// Override in the subclass of this class if needed
        /// </summary>
        /// <param name="identifiableObject">the SDMX identifiable object</param>
        /// <returns>the version if implemented; otherwise null</returns>
        protected virtual string GetVersion(IIdentifiableObject identifiableObject)
        {
            return null;
        }
        /// <summary>
        /// Set extra parameters for Other nameables with dedicated table
        /// Override in the subclass of this class if needed
        /// </summary>
        /// <param name="identifiableObject">the SDMX identifiable object</param>
        /// <param name="command"></param>
        protected virtual void SetupExtraParameter(IIdentifiableObject identifiableObject, DbCommand command)
        {
        }

        #endregion
    }
}
