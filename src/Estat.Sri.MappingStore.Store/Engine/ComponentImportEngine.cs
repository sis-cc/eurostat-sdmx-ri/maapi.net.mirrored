// -----------------------------------------------------------------------
// <copyright file="ComponentImportEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Linq;

    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Helper;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.Sdmx.MappingStore.Store.Properties;
    using Estat.Sri.MappingStoreRetrieval;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     The component import engine.
    /// </summary>
    public class ComponentImportEngine : IIdentifiableImportEngine<IComponent>
    {
        /// <summary>
        ///     The true value in Mapping Store
        /// </summary>
        private const int TrueValue = 1;

        /// <summary>
        ///     The _annotation insert engine
        /// </summary>
        private readonly IAnnotationInsertEngine _annotationInsertEngine;

        /// <summary>
        ///     The _insert component annotation
        /// </summary>
        private readonly InsertComponentAnnotation _insertComponentAnnotation;

        /// <summary>
        ///     The _stored procedures
        /// </summary>
        private readonly StoredProcedures _storedProcedures;

        /// <summary>
        ///     The _enforceEmbargoTimeType configuration
        /// </summary>
        private static readonly bool _enforceEmbargoTimeType = ConfigManager.Config.EnforceEmbargoTimeType;

        /// <summary>
        ///     Initializes static members of the <see cref="ComponentImportEngine" /> class.
        /// </summary>
        public ComponentImportEngine()
        {
            _storedProcedures = new StoredProcedures();
            _annotationInsertEngine = new ComponentAnnotationInsertEngine(new AnnotationInsertEngine());
            _insertComponentAnnotation = _storedProcedures.InsertComponentAnnotation;
        }

        /// <summary>
        ///     Insert the specified <paramref name="items" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="items">
        ///     The items.
        /// </param>
        /// <param name="parentArtefact">
        ///     The primary key of the parent artefact.
        /// </param>
        /// <returns>
        ///     The <see cref="IEnumerable{Long}" />.
        /// </returns>
        public ItemStatusCollection Insert(DbTransactionState state, IEnumerable<IComponent> items, long parentArtefact)
        {
            var components = items as IComponent[] ?? items.ToArray();
            if (components.Length == 0)
            {
                return new ItemStatusCollection();
            }

            var returnValues = new ItemStatusCollection();

            var dsd = components[0].MaintainableParent as IDataStructureObject;
            if (dsd == null)
            {
                throw new ArgumentException(Resources.ExceptionCannotDetermineParent, "items");
            }

            foreach (var component in components)
            {
                returnValues.Add(new ItemStatus(component.Id, Insert(state, component, parentArtefact)));
            }

            return returnValues;
        }

        /// <summary>
        ///     Returns an error message with the specified <paramref name="format" /> for <paramref name="reference" />
        /// </summary>
        /// <param name="format">
        ///     The format.
        /// </param>
        /// <param name="reference">
        ///     The component.
        /// </param>
        /// <returns>
        ///     The error message.
        /// </returns>
        private static string GetError(string format, IStructureReference reference)
        {
            IMaintainableRefObject maintainableRefObject = reference.MaintainableReference;
            return reference.HasChildReference() ? string.Format(format, maintainableRefObject.MaintainableId, maintainableRefObject.AgencyId, maintainableRefObject.Version, reference.ChildReference.Id) : string.Format(format, maintainableRefObject.MaintainableId, maintainableRefObject.AgencyId, maintainableRefObject.Version);
        }

        /// <summary>
        ///     The insert.
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="component">
        ///     The component.
        /// </param>
        /// <param name="itemScheme">
        ///     The item scheme.
        /// </param>
        /// <param name="parentArtefact">
        ///     The parent artefact.
        /// </param>
        /// <returns>
        ///     The <see cref="long" />.
        /// </returns>
        /// <exception cref="MappingStoreException">
        ///     THere was a problem with the <paramref name="component" /> references.
        /// </exception>
        private long Insert(DbTransactionState state, IComponent component, long parentArtefact)
        {
            long compId;
            var attribute = component as IAttributeObject;
            var dimension = component as IDimension;
            var storedProcedure = _storedProcedures.InsertComponent;
            using (DbCommand command = storedProcedure.CreateCommand(state))
            {
                DbParameter idParameter = storedProcedure.CreateIdParameter(command);
                idParameter.Value = component.Id;

                SetDsd(parentArtefact, storedProcedure, command);

                SetComponentType(component, storedProcedure, command);

                DbParameter isFreqDimParameter = storedProcedure.CreateIsFreqDimParameter(command);

                DbParameter isMeasureDimParameter = storedProcedure.CreateIsMeasureDimParameter(command);

                DbParameter attAssLevelParameter = storedProcedure.CreateAttAssLevelParameter(command);

                DbParameter attStatusParameter = storedProcedure.CreateAttStatusParameter(command);

                DbParameter attIsTimeFormatParameter = storedProcedure.CreateAttIsTimeFormatParameter(command);

                DbParameter xsMeasureCodeParameter = storedProcedure.CreateXsMeasureCodeParameter(command);

                DbParameter minOccursParameter = storedProcedure.CreateMinOccursParameter(command);

                DbParameter maxOccursParameter = storedProcedure.CreateMaxOccursParameter(command);

                DbParameter outputParameter = storedProcedure.CreateOutputParameter(command);

                switch (component.StructureType.EnumType)
                {
                    case SdmxStructureEnumType.Dimension:
                    case SdmxStructureEnumType.MeasureDimension:
                        {
                            SetDimensionParameters(dimension, isFreqDimParameter, isMeasureDimParameter);
                        }

                        break;
                    case SdmxStructureEnumType.TimeDimension:
                        break;
                    case SdmxStructureEnumType.DataAttribute:
                        {
                            SetAttributeParameters(attribute, attAssLevelParameter, attStatusParameter, attIsTimeFormatParameter);
                        }

                        break;
                    case SdmxStructureEnumType.PrimaryMeasure:
                        break;
                    case SdmxStructureEnumType.CrossSectionalMeasure:
                        {
                            SetCrossSectionalMeasureParameters(component, xsMeasureCodeParameter);
                        }

                        break;
                }

                SetMinMaxOccurs(component, minOccursParameter, maxOccursParameter);

                SetCrossSectionalLevels(component, storedProcedure, command);

                command.ExecuteNonQueryAndLog();

                compId = (long)outputParameter.Value;

                if (component.Representation != null && component.Representation.TextFormat != null)
                {
                    ValidateTextFormat(component);
                }
            }

            _annotationInsertEngine.Insert(state, compId, _insertComponentAnnotation, component.Annotations);
            return compId;
        }

        private static void SetMinMaxOccurs(IComponent component, DbParameter minOccursParameter, DbParameter maxOccursParameter)
        {
            if (component.Representation != null && !(component is IDimension))
            {
                minOccursParameter.Value = (object)component.Representation.MinOccurs ?? DBNull.Value;
                if (component.Representation.MaxOccurs != null)
                {
                    if (component.Representation.MaxOccurs.IsUnbounded)
                    {
                        maxOccursParameter.Value = -1;
                    }
                    else
                    {
                        maxOccursParameter.Value = component.Representation.MaxOccurs.Occurrences;
                    }
                }
            }
        }

        /// <summary>
        ///     The insert attribute.
        /// </summary>
        /// <param name="attribute">
        ///     The attribute.
        /// </param>
        /// <param name="attAssLevelParameter">
        ///     The Attribute attachment level parameter.
        /// </param>
        /// <param name="attStatusParameter">
        ///     The Attribute assignment status parameter.
        /// </param>
        /// <param name="attIsTimeFormatParameter">
        ///     The Attribute is time format parameter.
        /// </param>
        private static void SetAttributeParameters(IAttributeObject attribute, IDataParameter attAssLevelParameter, IDataParameter attStatusParameter, IDataParameter attIsTimeFormatParameter)
        {
            if (attribute != null)
            {
                attAssLevelParameter.Value = attribute.GetMappingStoreAssignmentLevel();
                attStatusParameter.Value = attribute.Usage.ToString();
                if (attribute.TimeFormat)
                {
                    attIsTimeFormatParameter.Value = TrueValue;
                }
            }
        }

        /// <summary>
        ///     Set component type parameter.
        /// </summary>
        /// <param name="component">
        ///     The component.
        /// </param>
        /// <param name="storedProcedure">
        ///     The stored procedure.
        /// </param>
        /// <param name="command">
        ///     The command.
        /// </param>
        private static void SetComponentType(IComponent component, InsertComponent storedProcedure, DbCommand command)
        {
            DbParameter typeParameter = storedProcedure.CreateTypeParameter(command);
            typeParameter.Value = component.GetMappingStoreType();
        }

        /// <summary>
        ///     Fill cross sectional levels.
        /// </summary>
        /// <param name="component">
        ///     The component.
        /// </param>
        /// <param name="storedProcedure">
        ///     The stored Procedure.
        /// </param>
        /// <param name="command">
        ///     The command.
        /// </param>
        private static void SetCrossSectionalLevels(IComponent component, InsertComponent storedProcedure, DbCommand command)
        {
            DbParameter xsAttlevelDsParameter = storedProcedure.CreateXsAttlevelDsParameter(command);

            DbParameter xsAttlevelGroupParameter = storedProcedure.CreateXsAttlevelGroupParameter(command);

            DbParameter xsAttlevelSectionParameter = storedProcedure.CreateXsAttlevelSectionParameter(command);

            DbParameter xsAttlevelObsParameter = storedProcedure.CreateXsAttlevelObsParameter(command);
            var crossDsd = component.MaintainableParent as ICrossSectionalDataStructureObject;
            if (crossDsd != null)
            {
                if (crossDsd.GetCrossSectionalAttachDataSet(false, component.StructureType).Contains(component))
                {
                    xsAttlevelDsParameter.Value = TrueValue;
                }

                if (crossDsd.GetCrossSectionalAttachGroup(false, component.StructureType).Contains(component))
                {
                    xsAttlevelGroupParameter.Value = TrueValue;
                }

                if (crossDsd.GetCrossSectionalAttachSection(false, component.StructureType).Contains(component))
                {
                    xsAttlevelSectionParameter.Value = TrueValue;
                }

                if (crossDsd.GetCrossSectionalAttachObservation(component.StructureType).Contains(component))
                {
                    xsAttlevelObsParameter.Value = TrueValue;
                }
            }
        }

        /// <summary>
        ///     The insert cross sectional measure.
        /// </summary>
        /// <param name="component">
        ///     The component.
        /// </param>
        /// <param name="xsMeasureCodeParameter">
        ///     The CrossSectional measure code parameter.
        /// </param>
        private static void SetCrossSectionalMeasureParameters(IComponent component, IDataParameter xsMeasureCodeParameter)
        {
            var crossSectionalMeasureBean = component as ICrossSectionalMeasure;
            if (crossSectionalMeasureBean != null)
            {
                xsMeasureCodeParameter.Value = crossSectionalMeasureBean.Code;
            }
        }

        /// <summary>
        ///     Insert dimension.
        /// </summary>
        /// <param name="dimension">
        ///     The dimension.
        /// </param>
        /// <param name="isFreqDimParameter">
        ///     The is frequency dimension parameter.
        /// </param>
        /// <param name="isMeasureDimParameter">
        ///     The is measure dimension parameter.
        /// </param>
        private static void SetDimensionParameters(IDimension dimension, IDataParameter isFreqDimParameter, IDataParameter isMeasureDimParameter)
        {
            if (dimension != null)
            {
                if (dimension.FrequencyDimension)
                {
                    isFreqDimParameter.Value = TrueValue;
                }

                if (dimension.MeasureDimension)
                {
                    isMeasureDimParameter.Value = TrueValue;
                }
            }
        }

        /// <summary>
        ///     Set parent DataStructure definition.
        /// </summary>
        /// <param name="parentArtefact">
        ///     The parent artefact.
        /// </param>
        /// <param name="storedProcedure">
        ///     The stored procedure.
        /// </param>
        /// <param name="command">
        ///     The command.
        /// </param>
        private static void SetDsd(long parentArtefact, InsertComponent storedProcedure, DbCommand command)
        {
            DbParameter dsdIdParameter = storedProcedure.CreateDsdIdParameter(command);
            dsdIdParameter.Value = parentArtefact;
        }

        /// <summary>
        /// Validates the text format of the component.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <exception cref="SdmxSemmanticException">The specified <paramref name="component"/> is not valid.</exception>
        private static void ValidateTextFormat(IComponent component)
        {
            if (_enforceEmbargoTimeType &&
                component.StructureType == SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataAttribute) &&
                component.Id.Equals("EMBARGO_TIME", StringComparison.OrdinalIgnoreCase) &&
                component.Representation.TextFormat.TextType != TextType.GetFromEnum(TextEnumType.DateTime))
            {
                throw new SdmxSemmanticException("'EMBARGO_TIME' attribute must have a DateTime TextFormat. Cannot import DSD.");
            }
        }
    }
}