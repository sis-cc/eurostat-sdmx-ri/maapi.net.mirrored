// -----------------------------------------------------------------------
// <copyright file="CategorisationImportEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-29
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Collections.Generic;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    ///     The categorisation import engine.
    ///     Compatible to MSDB 7.0
    /// </summary>
    public class CategorisationImportEngine : ArtefactImportEngine<ICategorisationObject>
    {
        //TODO missing implementation for inserting a category that doesn't exist for future feature implementation
        //private readonly IItemImportEngine<ICategoryObject> _categoryImport;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorisationImportEngine" /> class.
        /// </summary>
        /// <param name="mappingStore">
        ///     The connection String Settings.
        /// </param>
        public CategorisationImportEngine(Database mappingStore)
            : base(mappingStore)
        {
        }

        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        public override ArtefactImportStatus Insert(DbTransactionState state, ICategorisationObject maintainable)
        {
            return this.InsertArtefactInternal(state, maintainable);
        }

        /// <summary>
        /// A categorization has reference to a dataflow and a category.
        /// </summary>
        /// <param name="maintainable">The categorization to get the references for.</param>
        /// <returns>The list of references.</returns>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(ICategorisationObject maintainable)
        {
            IList<ReferenceToOtherArtefact> referenceToOtherArtefacts = new List<ReferenceToOtherArtefact>();
            ICrossReference categoryReference = maintainable.CategoryReference;
            referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(categoryReference, RefTypes.Target));
            referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(maintainable.StructureReference, RefTypes.Source));
            return referenceToOtherArtefacts;
        }

        /// <summary>
        /// Validates that that no other Categorisation exists with the same reference to category and dataflow.
        /// </summary>
        /// <param name="database"></param>
        /// <param name="maintainable">The imported categorisation.</param>
        /// <param name="artefactImportStatus">The status of the import.</param>
        protected override void Validation(Database database, ICategorisationObject maintainable, ArtefactImportStatus artefactImportStatus)
        {
           // base.Validation(database, maintainable, artefactImportStatus);
            // TODO validate that no other Categorisation exists with the same reference to category and dataflow
        }
    }
}