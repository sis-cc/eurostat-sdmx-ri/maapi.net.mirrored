// -----------------------------------------------------------------------
// <copyright file="TargetObjectImportEngine.cs" company="EUROSTAT">
//   Date Created : 2017-04-14
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.MappingStore.Store.Builder;
    using Estat.Sri.MappingStore.Store.Helper;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// Class TargetObjectImportEngine.
    /// </summary>
    /// <typeparam name="TTargetObject">The type of the target object.</typeparam>
    /// <seealso cref="Estat.Sri.MappingStore.Store.Engine.IIdentifiableImportEngine{TTargetObject}" />
    public class TargetObjectImportEngine<TTargetObject> : IIdentifiableImportEngine<TTargetObject>
        where TTargetObject : IIdentifiableObject
    {
        /// <summary>
        /// The insert target object
        /// </summary>
        private readonly InsertTargetObject _insertTargetObject;

        /// <summary>
        /// The annotation insert engine
        /// </summary>
        private readonly IAnnotationInsertEngine _annotationInsertEngine;

        /// <summary>
        /// The common target object builder
        /// </summary>
        private readonly ICommonTargetObjectBuilder<TTargetObject> _commonTargetObjectBuilder;

        /// <summary>
        /// The insert component annotation
        /// </summary>
        private readonly InsertComponentAnnotation _insertComponentAnnotation;

        /// <summary>
        /// Initializes a new instance of the <see cref="TargetObjectImportEngine{TTargetObject}" /> class.
        /// </summary>
        /// <param name="insertTargetObject">The insert target object.</param>
        /// <param name="annotationInsertEngine">The annotation insert engine.</param>
        /// <param name="commonTargetObjectBuilder">The common target object builder.</param>
        /// <param name="insertComponentAnnotation">The insert component annotation.</param>
        public TargetObjectImportEngine(InsertTargetObject insertTargetObject, IAnnotationInsertEngine annotationInsertEngine, ICommonTargetObjectBuilder<TTargetObject> commonTargetObjectBuilder, InsertComponentAnnotation insertComponentAnnotation)
        {
            this._insertTargetObject = insertTargetObject;
            this._annotationInsertEngine = annotationInsertEngine;
            this._commonTargetObjectBuilder = commonTargetObjectBuilder;
            this._insertComponentAnnotation = insertComponentAnnotation;
        }

        /// <summary>
        /// Insert the specified <paramref name="items" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">The MAPPING STORE connection and transaction state</param>
        /// <param name="items">The items.</param>
        /// <param name="parentArtefact">The primary key of the parent artefact.</param>
        /// <returns>The <see cref="IEnumerable{ArtefactImportStatus}" />.</returns>
        /// <exception cref="MappingStoreException">
        /// Dependency not found or not final
        /// </exception>
        public ItemStatusCollection Insert(DbTransactionState state, IEnumerable<TTargetObject> items, long parentArtefact)
        {
            var targetObjectsItemScheme = this.BuildItemSchemeCache(state, items);

            var itemWithSysId = new List<KeyValuePair<long, CommonTargetObject>>();
            using (var command = this._insertTargetObject.CreateCommandWithDefaults(state.Database))
            {
                var idParameter = this._insertTargetObject.CreateIdParameter(command);
                var outputParameter = this._insertTargetObject.CreateOutputParameter(command);
                var typeParameter = this._insertTargetObject.CreateTypeParameter(command);
                this._insertTargetObject.CreateMdtIDParameter(command).Value = parentArtefact;

                foreach (var targetObject in targetObjectsItemScheme)
                {
                    var commonTargetObject = targetObject;
                    idParameter.Value = commonTargetObject.Id;
                    typeParameter.Value = commonTargetObject.ObjectType;

                    command.ExecuteNonQueryAndLog();

                    long sysId = (long)outputParameter.Value;
                    itemWithSysId.Add(new KeyValuePair<long, CommonTargetObject>(sysId, commonTargetObject));
                }
            }

            //var textFormats = itemWithSysId.Where(pair => pair.Value.TextFormat != null).Select(pair => new KeyValuePair<long, ITextFormat>(pair.Key, pair.Value.TextFormat)).ToArray();
            //if (textFormats.Length > 0)
            //{
            //    TextFormatTypesPool.GetTextFormatQuery(state,Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureEnumType.ConstraintContentTarget).InsertTextFormats(state, textFormats);
            //}

            foreach (var keyValuePair in itemWithSysId)
            {
                if (keyValuePair.Value.Annotations.Count > 0)
                {
                    this._annotationInsertEngine.Insert(state, keyValuePair.Key, this._insertComponentAnnotation, keyValuePair.Value.Annotations);
                }
            }

            return new ItemStatusCollection(itemWithSysId.Select(pair => new ItemStatus(pair.Value.Id, pair.Key)));
        }

        /// <summary>
        /// Builds the item scheme cache.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="items">The items.</param>
        /// <returns>The list of <see cref="CommonTargetObject"/> with Item Scheme Ids where it applies</returns>
        private IList<CommonTargetObject> BuildItemSchemeCache(DbTransactionState state, IEnumerable<TTargetObject> items)
        {
            IList<CommonTargetObject> targetObjectsItemScheme = new List<CommonTargetObject>();
            foreach (var targetObject in items)
            {
                var commonTargetObject = this._commonTargetObjectBuilder.Build(targetObject);
                targetObjectsItemScheme.Add(commonTargetObject);
            }

            return targetObjectsItemScheme;
        }
    }
}