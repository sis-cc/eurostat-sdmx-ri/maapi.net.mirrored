// -----------------------------------------------------------------------
// <copyright file="OtherAnnotationInsertEngine.cs" company="EUROSTAT">
//   Date Created : 2022-06-30
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Collections.Generic;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.MappingStore.Store.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure;

    /// <summary>
    /// The engine for inserting other annotations.
    /// </summary>
    public class OtherAnnotationInsertEngine
    {
        private readonly AnnotationInsertEngine _decorator = new AnnotationInsertEngine();

        /// <summary>
        /// Inserts the other annotations from a list of annotations
        /// </summary>
        /// <param name="state">The database state</param>
        /// <param name="annotatablePrimaryKey">The PK of the parent.</param>
        /// <param name="annotations">The list of annotations; will be filtered to insert only other types.</param>
        public void Insert(DbTransactionState state, long annotatablePrimaryKey, IList<IAnnotation> annotations)
        {
            IList<IAnnotation> annotationsWithoutCustomTypes = new List<IAnnotation>();

            foreach (IAnnotation annotation in annotations)
            {
                //if (!annotation.IsCustomAnnotation())
                //{
                //    annotationsWithoutCustomTypes.Add(annotation);
                //}
            }

            _decorator.Insert(state, annotatablePrimaryKey, new InsertOtherAnnotation(), annotationsWithoutCustomTypes);
        }
    }
}
