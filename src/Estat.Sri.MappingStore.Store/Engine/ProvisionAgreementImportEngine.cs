// -----------------------------------------------------------------------
// <copyright file="ProvisionAgreementImportEngine.cs" company="EUROSTAT">
//   Date Created : 2017-04-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Collections.Generic;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using MappingStoreRetrieval.Manager;
    using Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    /// <summary>
    /// The engien for importing <see cref="IProvisionAgreementObject"/>
    /// </summary>
    /// <seealso cref="ArtefactImportEngine{IProvisionAgreementObject}" />
    public class ProvisionAgreementImportEngine : ArtefactImportEngine<IProvisionAgreementObject>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ArtefactImportEngine{T}" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        public ProvisionAgreementImportEngine(Database database)
            : base(database)
        {
        }

        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        public override ArtefactImportStatus Insert(DbTransactionState state, IProvisionAgreementObject maintainable)
        {
            return this.InsertArtefactInternal(state, maintainable);
        }

        /// <summary>
        /// Deletes the child items.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="primaryKey">The primary key.</param>
        protected override void DeleteChildStructures(DbTransactionState state, long primaryKey)
        {
            // TODO the following SQL Queries need to delete instead the references
            ExecuteArtefactUpdateOrDeleteQuery(state, "DELETE FROM DATA_SOURCE WHERE DATA_SOURCE_ID " +
                "IN (SELECT DATA_SOURCE_ID FROM N_REGISTRATION WHERE ENTITY_ID = {0})", primaryKey);
            ExecuteArtefactUpdateOrDeleteQuery(state, "DELETE FROM N_REGISTRATION WHERE ENTITY_ID = {0}", primaryKey);
        }

        /// <summary>
        /// Refernces Data Provider & Structure Usage
        /// </summary>
        /// <param name="maintainable"></param>
        /// <returns></returns>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(IProvisionAgreementObject maintainable)
        {
            List<ReferenceToOtherArtefact> referenceToOtherArtefacts = new List<ReferenceToOtherArtefact>();
            referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(maintainable.DataproviderRef, RefTypes.DataProvider));
            referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(maintainable.StructureUseage, RefTypes.Dataflow));

            return referenceToOtherArtefacts;
        }
    }
}