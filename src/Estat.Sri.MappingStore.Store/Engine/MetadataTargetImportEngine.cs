// -----------------------------------------------------------------------
// <copyright file="MetadataTargetImportEngine.cs" company="EUROSTAT">
//   Date Created : 2017-04-13
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.MappingStore.Store.Builder;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;

    /// <summary>
    /// Class MetadataTargetImportEngine.
    /// </summary>
    /// <seealso cref="IIdentifiableImportEngine{IMetadataTarget}" />
    public class MetadataTargetImportEngine : IIdentifiableImportEngine<IMetadataTarget>
    {
        private readonly OtherNameableImportEngine _otherNameableImportEngine;
        /// <summary>
        /// The store procedure
        /// </summary>
        private readonly InsertMetadataTarget _storeProc;

        /// <summary>
        /// The _annotation insert engine
        /// </summary>
        private readonly IAnnotationInsertEngine _annotationInsertEngine;

        /// <summary>
        /// The _insert component annotation
        /// </summary>
        private readonly AnnotationProcedureBase _insertItemAnnotation;

        /// <summary>
        /// The constraint import engine
        /// </summary>
        private readonly IIdentifiableImportEngine<IConstraintContentTarget> _constraintImportEngine;
        
        /// <summary>
        /// The dataset target import engine
        /// </summary>
        private readonly IIdentifiableImportEngine<IDataSetTarget> _datasetTargetImportEngine;
        
        /// <summary>
        /// The key value target import engine
        /// </summary>
        private readonly IIdentifiableImportEngine<IKeyDescriptorValuesTarget> _keyValueTargetImportEngine;

        /// <summary>
        /// The identifiable import engine
        /// </summary>
        private readonly IIdentifiableImportEngine<IIdentifiableTarget> _identifiableImportEngine;

        /// <summary>
        /// The report period target import engine
        /// </summary>
        private readonly IIdentifiableImportEngine<IReportPeriodTarget> _reportPeriodTargetImportEngine;

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataTargetImportEngine" /> class.
        /// </summary>
        /// <param name="database"></param>
        /// <param name="storedProcedures">The stored procedures.</param>
        /// <param name="annotationInsertEngine">The annotation insert engine.</param>
        public MetadataTargetImportEngine(Database database, StoredProcedures storedProcedures, IAnnotationInsertEngine annotationInsertEngine)
        {
            this._annotationInsertEngine = annotationInsertEngine;
            this._insertItemAnnotation = storedProcedures.InsertItemAnnotation;
            this._storeProc = storedProcedures.InsertMetadataTarget;
            var insertTargetObject = storedProcedures.InsertTargetObject;
            this._otherNameableImportEngine = new OtherNameableImportEngine(database);
            var commonTargetObjectBuilder = new CommonTargetObjectBuilder();
            var insertComponentAnnotation = storedProcedures.InsertComponentAnnotation;
            this._constraintImportEngine = new TargetObjectImportEngine<IConstraintContentTarget>(insertTargetObject, this._annotationInsertEngine, commonTargetObjectBuilder, insertComponentAnnotation);
            this._datasetTargetImportEngine = new TargetObjectImportEngine<IDataSetTarget>(insertTargetObject, this._annotationInsertEngine, commonTargetObjectBuilder, insertComponentAnnotation);
            this._keyValueTargetImportEngine = new TargetObjectImportEngine<IKeyDescriptorValuesTarget>(insertTargetObject, this._annotationInsertEngine, commonTargetObjectBuilder, insertComponentAnnotation);
            this._identifiableImportEngine = new TargetObjectImportEngine<IIdentifiableTarget>(insertTargetObject, this._annotationInsertEngine, commonTargetObjectBuilder, insertComponentAnnotation);
            this._reportPeriodTargetImportEngine = new TargetObjectImportEngine<IReportPeriodTarget>(insertTargetObject, this._annotationInsertEngine, commonTargetObjectBuilder, insertComponentAnnotation);
        }

        /// <summary>
        /// Insert the specified <paramref name="items" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">The MAPPING STORE connection and transaction state</param>
        /// <param name="items">The items.</param>
        /// <param name="parentArtefact">The primary key of the parent artefact.</param>
        /// <returns>The <see cref="IEnumerable{ArtefactImportStatus}" />.</returns>
        public ItemStatusCollection Insert(DbTransactionState state, IEnumerable<IMetadataTarget> items, long parentArtefact)
        {
            var structures = items as IMetadataTarget[] ?? items.ToArray();
            if (structures.Length == 0)
            {
                return new ItemStatusCollection();
            }

            var itemList = new List<KeyValuePair<long, IMetadataTarget>>();
            foreach (var structure in structures)
            {
                long targetSysId = this._otherNameableImportEngine.Insert(state, structure, parentArtefact);
                itemList.Add(new KeyValuePair<long, IMetadataTarget>(targetSysId, structure));
            }

            foreach (var pair in itemList)
            {
                var metadataTarget = pair.Value;
                var sysId = pair.Key;

                if (metadataTarget.ConstraintContentTarget != null)
                {
                    this._constraintImportEngine.Insert(state, new[] { metadataTarget.ConstraintContentTarget }, sysId);
                }

                if (metadataTarget.IdentifiableTarget != null)
                {
                    this._identifiableImportEngine.Insert(state, metadataTarget.IdentifiableTarget, sysId);
                }

                if (metadataTarget.DataSetTarget != null)
                {
                    this._datasetTargetImportEngine.Insert(state, new[] { metadataTarget.DataSetTarget }, sysId);
                }

                if (metadataTarget.KeyDescriptorValuesTarget != null)
                {
                    this._keyValueTargetImportEngine.Insert(state, new[] { metadataTarget.KeyDescriptorValuesTarget }, sysId);
                }

                if (metadataTarget.ReportPeriodTarget != null)
                {
                    this._reportPeriodTargetImportEngine.Insert(state, new[] { metadataTarget.ReportPeriodTarget }, sysId);
                }

                if (metadataTarget.Annotations.Count > 0)
                {
                    this._annotationInsertEngine.Insert(state, sysId, this._insertItemAnnotation, metadataTarget.Annotations);
                }
            }

            return itemList.ToItemStatusCollection();
        }
    }
}