namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Linq;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Extension;
    using log4net;
    using MappingStoreRetrieval.Model.StoredProcedure;
    using Model;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// DataSourceImportEngine 
    /// </summary>
    public class DataSourceImportEngine
    {
        /// <summary>
        ///     The artefact stored procedure
        /// </summary>
        private static readonly InsertDataSource _insertDataSource;

        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof (DataflowImportEngine));

        /// <summary>
        ///     Initializes static members of the <see cref="DataflowImportEngine" /> class.
        /// </summary>
        static DataSourceImportEngine()
        {
            _insertDataSource = new StoredProcedures().InsertDataSource;
        }

        /// <summary>
        /// Inserts the specified state.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="dataSource">The data source.</param>
        /// <returns></returns>
        public long Insert(DbTransactionState state, IDataSource dataSource)
        {
            using (var command = _insertDataSource.CreateCommandWithDefaults(state))
            {
                var id = this.CreateCommandAndExecute(dataSource, command);
                return id;
            }
        }

        /// <summary>
        /// Inserts the specified state.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="dataSources">The data sources.</param>
        /// <returns></returns>
        public IEnumerable<long> Insert(DbTransactionState state, IEnumerable<IDataSource> dataSources)
        {
            using (var command = _insertDataSource.CreateCommandWithDefaults(state.Database))
            {
                var ids = dataSources.Select(dataSource => this.CreateCommandAndExecute(dataSource, command)).ToList();
                return ids;
            }
        }

        private  long CreateCommandAndExecute(IDataSource dataSource, DbCommand command)
        {
            if (dataSource.WsdlUrl != null)
            {
                _insertDataSource.CreateWsdlUrlParameter(command).Value = dataSource.WsdlUrl.ToString();
            }

            if (dataSource.WadlUrl != null)
            {
                _insertDataSource.CreateWadlUrlParameter(command).Value = dataSource.WadlUrl.ToString();
            }

            if (dataSource.DataUrl == null)
            {
                throw new SdmxSemmanticException("Data URL is mandatory");
            }

            _insertDataSource.CreateDataUrlParameter(command).Value = dataSource.DataUrl.ToString();
            _insertDataSource.CreateIsRestParameter(command).Value = dataSource.RESTDatasource.ToDbValue();
            _insertDataSource.CreateIsSimpleParameter(command).Value = dataSource.SimpleDatasource.ToDbValue();
            _insertDataSource.CreateIsWsParameter(command).Value = dataSource.WebServiceDatasource.ToDbValue();

            DbParameter outputParameter = _insertDataSource.CreateOutputParameter(command);
            command.ExecuteNonQueryAndLog();
            if (DBNull.Value.Equals(outputParameter.Value)) 
            {
                throw new SdmxInternalServerException(dataSource.ToString());
            }

            return (long)outputParameter.Value;
        }
    }
}