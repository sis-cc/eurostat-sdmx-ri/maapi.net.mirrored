// -----------------------------------------------------------------------
// <copyright file="RegistrationImportEngine.cs" company="EUROSTAT">
//   Date Created : 2017-11-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using Dapper;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.Sdmx.MappingStore.Store.Properties;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Estat.Sri.Sdmx.MappingStore.Store.Engine;

    class RegistrationImportEngine : IImportEngine<IRegistrationObject>, IDeleteEngine<IRegistrationObject>
    {
        /// <summary>
        ///     The artefact stored procedure
        /// </summary>
        private static readonly InsertRegistration _storedProc;

        /// <summary>
        /// The database
        /// </summary>
        private readonly Database _database;

        /// <summary>
        /// The data source import engine
        /// </summary>
        private readonly DataSourceImportEngine _dataSourceImportEngine;

        /// <summary>
        /// Initializes static members of the <see cref="RegistrationImportEngine"/> class.
        /// </summary>
        static RegistrationImportEngine()
        {
            _storedProc = new InsertRegistration();
        }

        public RegistrationImportEngine(Database database)
        {
            if (database == null)
            {
                throw new ArgumentNullException(nameof(database));
            }

            this._database = database;
            this._dataSourceImportEngine = new DataSourceImportEngine();
        }

        /// <summary>
        /// Delete the specified <paramref name="objects" /> from Mapping Store if they exist.
        /// </summary>
        /// <param name="objects">The objects.</param>
        /// <returns>The <see cref="T:System.Collections.Generic.IList`1" />.</returns>
        public IList<ArtefactImportStatus> Delete(IEnumerable<IRegistrationObject> objects)
        {
            // We need to sync with Java version which keeps adding new Registrations. 
            throw new SdmxNotImplementedException();
            /*            foreach (var artefact in objects)
                        {
                            using (DbTransactionState state = DbTransactionState.Create(this._database))
                            {
                                var artefactFinalStatus = ArtefactBaseEngine.GetFinalStatus(state, artefact.ProvisionAgreementRef);
                                var countObj = state.UsingLogger().ExecuteScalarFormat("SELECT DR_ID FROM N_REGISTRATION where ID = {0} and PA_ID = {1}", state.Database.CreateInParameter("rid", System.Data.DbType.AnsiString, artefact.Id), state.Database.CreateInParameter("paid", System.Data.DbType.Int64, artefactFinalStatus.PrimaryKey));
                                if (countObj is long && ((long)countObj) > 0)
                                {

                                }

                            }
                        }*/
        }

        /// <summary>
        /// Delete the specified <paramref name="artefact" /> from Mapping Store if they exist.
        /// </summary>
        /// <param name="state"></param>
        /// <param name="artefact">The artefact.</param>
        /// <param name="provisionAggrementRef"> The provisionAggrementRef</param>
        /// <returns>The <see cref="T:System.Collections.Generic.IList`1" />.</returns>
        public void DeleteIntenral(DbTransactionState state, IRegistrationObject artefact, ArtefactFinalStatus provisionAggrementRef)
        {
            var paIdParam = _database.BuildParameterName("PA_ID");
            var idParam = _database.BuildParameterName("ID");
            var dynamicParameters = new DynamicParameters();
            dynamicParameters.Add(paIdParam, provisionAggrementRef.PrimaryKey, DbType.Int64);
            dynamicParameters.Add(idParam, artefact.Id, DbType.AnsiString);

            var objects = _database.Query($"SELECT DR_ID, DATA_SOURCE_ID FROM N_REGISTRATION where PA_ID = {paIdParam} AND ID = {idParam}", dynamicParameters);

            foreach (var obj in objects)
            {
                long drId = obj.DR_ID;
                long dataSourceId = obj.DATA_SOURCE_ID;

                if (drId > 0)
                {
                    state.UsingLogger().ExecuteNonQueryFormat("DELETE FROM N_REGISTRATION WHERE DR_ID = {0}", state.Database.CreateInParameter("artId", DbType.Int64, drId));
                    if (dataSourceId > 0)
                    {
                        state.UsingLogger().ExecuteNonQueryFormat("DELETE FROM DATA_SOURCE WHERE DATA_SOURCE_ID = {0}",
                            state.Database.CreateInParameter("dsId", DbType.Int64, dataSourceId));
                    }
                }
            }
        }

        /// <summary>
        ///     Insert the specified <paramref name="maintainables" /> to the mapping store.
        /// </summary>
        /// <param name="maintainables">
        ///     The maintainable.
        /// </param>
        /// <param name="action">The action</param>
        /// <returns>
        ///     The <see cref="IEnumerable{ArtefactImportStatus}" />.
        /// </returns>
        public IEnumerable<ArtefactImportStatus> Insert(IEnumerable<IRegistrationObject> maintainables, DatasetActionEnumType action)
        {
            var results = new List<ArtefactImportStatus>();
            foreach (var artefact in maintainables)
            {
                try
                {
                    Insert(results, artefact);
                }
                catch (SdmxException ex)
                {
                    var status = new ArtefactImportStatus(-1, artefact.AsReference.GetErrorMessage(ex, action));
                    results.Add(status);
                }
            }

            return results;
        }

        /// <summary>
        /// Inserts the specified results.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <param name="artefact">The artefact.</param>
        /// <exception cref="SdmxNoResultsException">Not found:" + artefact.ProvisionAgreementRef.GetAsHumanReadableString()</exception>
        private void Insert(List<ArtefactImportStatus> results, IRegistrationObject artefact)
        {
            using (DbTransactionState state = DbTransactionState.Create(this._database))
            {
                var provisionAgreementKey = GetFinalStatus(state.Database, artefact.ProvisionAgreementRef);
                if (provisionAgreementKey == null || provisionAgreementKey.PrimaryKey < 1)
                {
                    throw new SdmxNoResultsException("Not found:" + artefact.ProvisionAgreementRef.GetAsHumanReadableString());
                }

                //Delete artefact if exists
                DeleteIntenral(state, artefact, provisionAgreementKey);

                var dataSourceKey = _dataSourceImportEngine.Insert(state, artefact.DataSource);
                var artefactImportStatus = InsertInternal(state, artefact, dataSourceKey, provisionAgreementKey.PrimaryKey);
                results.Add(artefactImportStatus);
                state.Commit();
            }
        }

        private ArtefactFinalStatus GetFinalStatus(Database state, ICrossReference reference)
        {
            StructureCache cache = StructureCacheContext.GetStructureCacheFromThreadLocal() != null ?
                                            StructureCacheContext.GetStructureCacheFromThreadLocal() :
                                            new StructureCache();
            return cache.GetArtefactFinalStatus(state, reference);
        }

        private ArtefactImportStatus InsertInternal(DbTransactionState state, IRegistrationObject registrationObject, long datasourceKey, long provisionAgreementKey)
        {
            using (var cmd = _storedProc.CreateCommandWithDefaults(state.Database))
            {
                var outputParameter = _storedProc.CreateOutputParameter(cmd);
                if (!string.IsNullOrWhiteSpace(registrationObject.Id))
                {
                    _storedProc.CreateIdParameter(cmd).Value = registrationObject.Id;
                }

                _storedProc.CreateValidFromParameter(cmd).Value = registrationObject.ValidFrom.ToDbValue();
                _storedProc.CreateValidToParameter(cmd).Value = registrationObject.ValidTo.ToDbValue();
                _storedProc.CreateLastUpdatedParameter(cmd).Value = registrationObject.LastUpdated.ToDbValue();
                _storedProc.CreateIndexTsParameter(cmd).Value = registrationObject.IndexTimeseries.ToDbValue();
                _storedProc.CreateIndexDsParameter(cmd).Value = registrationObject.IndexDataset.ToDbValue();
                _storedProc.CreateIndexAttributesParameter(cmd).Value = registrationObject.IndexAttribtues.ToDbValue();
                _storedProc.CreateIndexReportingPeriodParameter(cmd).Value = registrationObject.IndexReportingPeriod.ToDbValue();
                _storedProc.CreatePaIdParameter(cmd).Value = provisionAgreementKey;
                _storedProc.CreateDatasourceIdParameter(cmd).Value = datasourceKey;

                cmd.ExecuteNonQueryAndLog();

                var newlyCreatedKey = (long)outputParameter.Value;
                var structureReference = registrationObject.AsReference;
                return new ArtefactImportStatus(newlyCreatedKey, new ImportMessage(ImportMessageStatus.Success, structureReference, string.Format(Resources.SuccessInsertFormat2, structureReference.GetAsHumanReadableString(), Environment.NewLine), DatasetActionEnumType.Append));
            }
        }
    }
}
