// -----------------------------------------------------------------------
// <copyright file="CategorySchemeImportEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Collections.Generic;
    using System.Globalization;

    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using System.Linq;
    using System.Data;
    using Estat.Sri.MappingStore.Store.Engine.Update;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

    /// <summary>
    ///     The category scheme import engine.
    /// </summary>
    public class CategorySchemeImportEngine : ItemSchemeImportEngine<ICategorySchemeObject, ICategoryObject>
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(CategorySchemeImportEngine));

        /// <summary>
        ///     The _stored procedures.
        /// </summary>
        private static readonly StoredProcedures _storedProcedures;

        /// <summary>
        /// The update category scheme engine
        /// </summary>
        private readonly UpdateCategoryScheme _updateCategoryScheme = new UpdateCategoryScheme();
        private readonly CategorisationImportEngine _categorisationImportEngine;

        /// <summary>
        ///     Initializes static members of the <see cref="CategorySchemeImportEngine" /> class.
        /// </summary>
        static CategorySchemeImportEngine()
        {
            _storedProcedures = new StoredProcedures();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorySchemeImportEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        public CategorySchemeImportEngine(Database database)
            : base(database)
        {
            _categorisationImportEngine = new CategorisationImportEngine(database);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorySchemeImportEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        /// <param name="factory">
        ///     The <see cref="IItemImportEngine{T}" /> factory. Optional
        /// </param>
        public CategorySchemeImportEngine(Database database, IItemImportFactory<ICategoryObject> factory)
            : base(database, factory)
        {
            _categorisationImportEngine = new CategorisationImportEngine(database);
        }

        /// <summary>
        /// Updates the final artefact. Structure specific code goes here
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="maintainableObject">The maintainable object.</param>
        /// <param name="finalStatus">The final status.</param>
        /// <returns>
        /// The <see cref="ArtefactImportStatus" />.
        /// </returns>
        protected override ArtefactImportStatus HandleFinalArtefactReplace(DbTransactionState state, IMaintainableObject maintainableObject, ArtefactFinalStatus finalStatus)
        {
            return _updateCategoryScheme.UpdateCategories(state, maintainableObject, finalStatus);
        }

        /// <summary>
        /// Updates the non final artefact. Structure specific code goes here
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="maintainableObject">The maintainable object.</param>
        /// <param name="finalStatus">The final status.</param>
        /// <param name="crossReferencingObjects"></param>
        /// <returns>
        /// The <see cref="ArtefactImportStatus" />.
        /// </returns>
        protected override ArtefactImportStatus HandleNonFinalArtefactReplace(
            DbTransactionState state,
            IMaintainableObject maintainableObject,
            ArtefactFinalStatus finalStatus,
            IList<IMaintainableMutableObject> crossReferencingObjects)
        {
            return _updateCategoryScheme.UpdateCategories(state, maintainableObject, finalStatus);
        }

        /// <summary>
        /// Handles the no final replace.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="artefact">The artefact.</param>
        /// <param name="finalStatus">The final status.</param>
        /// <returns>
        /// The <see cref="ArtefactImportStatus" />
        /// </returns>
        protected override List<ArtefactImportStatus> DeleteInsert(DbTransactionState state, IMaintainableObject artefact, ArtefactFinalStatus finalStatus)
        {
            // get the categorisations
            var categorisations = GetCrossReferencingCategory(artefact.AsReference);               
            var statuses = new List<ArtefactImportStatus>();
            // delete category scheme + any categorisations
            var rowsAffected = this.Delete(state, finalStatus.PrimaryKey, artefact);

            if (rowsAffected > 0)
            {
                var deletedStatus = new ArtefactImportStatus(finalStatus.PrimaryKey,
                    new ImportMessage(ImportMessageStatus.Success, artefact.AsReference, "The artefact is non-final and needs to be re-created.",
                        DatasetActionEnumType.Delete));
                statuses.Add(deletedStatus);
            }

            var importStatus = this.Insert(state, (ICategorySchemeObject)artefact);
            foreach (var categorisation in categorisations)
            {
                _categorisationImportEngine.Insert(state, (ICategorisationObject)categorisation.ImmutableInstance);
            }
            statuses.Add(importStatus);
            return statuses;
        }

        /// <summary>
        /// // Neither Category Schemes or categories reference anything else
        /// </summary>
        /// <param name="maintainable"></param>
        /// <returns>An empty list</returns>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(ICategorySchemeObject maintainable)
        {
            return new List<ReferenceToOtherArtefact>();
        }

        /// <summary>
        /// GetCrossReferencingStructures
        /// </summary>
        /// <param name="structure"></param>
        /// <returns></returns>
        private List<IMaintainableMutableObject> GetCrossReferencingCategory(IStructureReference structure)
        {
            var categorisations = new List<IMaintainableMutableObject>();

            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
               .NewQuery(CommonStructureQueryType.Other, null)
               .SetStructureIdentification(structure)
               .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
               .Build();

            var _retrievalEng = retrievalEngineContainer.GetEngine(structure.MaintainableStructureEnumType);
            var structureReferenceList = _retrievalEng.RetrieveResolvedParents(structureQuery).ToList();

            foreach (var struRef in structureReferenceList)
            {
                if (struRef.MaintainableStructureEnumType == SdmxStructureEnumType.Categorisation)
                {
                    //getting the Categorisation
                    ICommonStructureQuery structureQueryC = CommonStructureQueryCore.Builder
                       .NewQuery(CommonStructureQueryType.Other, null)
                       .SetStructureIdentification(struRef)
                       .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                       .Build();

                    var maintainableObjects = retrievalEngineContainer.GetEngine(struRef.MaintainableStructureEnumType).Retrieve(structureQueryC);

                    foreach (var obj in maintainableObjects)
                    {
                        categorisations.Add(obj);
                    }

                }
            }

            return categorisations;
        }

    }
}