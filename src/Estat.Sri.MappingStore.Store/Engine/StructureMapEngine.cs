// -----------------------------------------------------------------------
// <copyright file="StructureMapEngine.cs" company="EUROSTAT">
//   Date Created : 2014-10-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;

    /// <summary>
    ///     The structure map engine.
    /// </summary>
    internal class StructureMapEngine : SchemeMapBaseEngine<IStructureMapObject>
    {
        private readonly InsertStructureSetCommonMap2Procedure _mapProcedure = new InsertStructureSetCommonMap2Procedure();

        public StructureMapEngine(Database mappingStore) 
            : base(mappingStore)
        {
        }

        /// <summary>
        ///     Writes the item maps.
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="schemaMap">
        ///     The schema map.
        /// </param>
        /// <param name="primaryKey">
        ///     The primary key.
        /// </param>
        protected override void WriteItemMaps(DbTransactionState state, IStructureMapObject schemaMap, long primaryKey)
        {
            using (var command = _mapProcedure.CreateCommandWithDefaults(state))
            {
                _mapProcedure.CreateParentIdParameter(command).Value = primaryKey;
                foreach (var componentMapObject in schemaMap.Components)
                {
                    _mapProcedure.CreateSourceIdParameter(command).Value = componentMapObject.MapConceptRef;
                    _mapProcedure.CreateTargetIdParameter(command).Value = componentMapObject.MapTargetConceptRef;
                    command.ExecuteNonQueryAndLog();
                }
            }
        }
    }
}