// -----------------------------------------------------------------------
// <copyright file="ReportStructureImportEngine.cs" company="EUROSTAT">
//   Date Created : 2017-04-13
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.Sdmx.MappingStore.Store.Properties;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// The <see cref="IReportStructure"/> import engine
    /// </summary>
    public class ReportStructureImportEngine : IIdentifiableImportEngine<IReportStructure>
    {
        /// <summary>
        /// The metadata attribute import engine
        /// </summary>
        private readonly MetadataAttributeImportEngine _metadataAttributesImportEngine;

        /// <summary>
        ///     The _annotation insert engine
        /// </summary>
        private readonly IAnnotationInsertEngine _annotationInsertEngine;

        /// <summary>
        /// The engine for importing Other Nameables
        /// </summary>
        private readonly OtherNameableImportEngine _otherNameableImportEngine;

        /// <summary>
        ///     The _insert component annotation
        /// </summary>
        private readonly AnnotationProcedureBase _insertItemAnnotation;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportStructureImportEngine" /> class.
        /// </summary>
        /// <param name="database">The mapping store database instance.</param>
        /// <param name="annotationInsertEngine">The annotation insert engine.</param>
        public ReportStructureImportEngine(Database database,
            IAnnotationInsertEngine annotationInsertEngine)
        {
            var storedProcedures = new StoredProcedures();
            this._annotationInsertEngine = annotationInsertEngine ?? new AnnotationInsertEngine();
            this._metadataAttributesImportEngine = new MetadataAttributeImportEngine(storedProcedures, this._annotationInsertEngine);
            this._otherNameableImportEngine = new OtherNameableImportEngine(database);
            this._insertItemAnnotation = storedProcedures.InsertItemAnnotation;
        }

        /// <summary>
        ///     Insert the specified <paramref name="items" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="items">
        ///     The items.
        /// </param>
        /// <param name="parentArtefact"> The primary key of the parent artefact.</param>
        /// <returns>
        ///     The <see cref="IEnumerable{ArtefactImportStatus}" />.
        /// </returns>
        public ItemStatusCollection Insert(DbTransactionState state, 
            IEnumerable<IReportStructure> items, long parentArtefact)
        {
            var structures = items as IReportStructure[] ?? items.ToArray();
            if (structures.Length == 0)
            {
                return new ItemStatusCollection();
            }

            var dsd = structures[0].MaintainableParent as IMetadataStructureDefinitionObject;
            if (dsd == null)
            {
                throw new ArgumentException(Resources.ExceptionCannotDetermineParent, "items");
            }

            var returnValues = new List<KeyValuePair<long, IReportStructure>>();
            foreach (var structure in structures)
            {
                long reportStructureSysId = this._otherNameableImportEngine.Insert(state, structure, parentArtefact);
                this._metadataAttributesImportEngine.Insert(state, structure.MetadataAttributes, reportStructureSysId, parentArtefact);
                this._annotationInsertEngine.Insert(state, reportStructureSysId, this._insertItemAnnotation, structure.Annotations);
                returnValues.Add(new KeyValuePair<long, IReportStructure>(reportStructureSysId, structure));
            }

            return returnValues.ToItemStatusCollection();
        }
    }
}