// -----------------------------------------------------------------------
// <copyright file="ArtefactImportEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Estat.Sri.MappingStore.Store.Extension;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Estat.Sri.Sdmx.MappingStore.Store.Engine;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// The engine for importing sdmx artefacts.
    /// Compatible to MSDB 7.0
    /// Should be derived from other classes, implementing the import of more specific types of artefacts.
    /// </summary>
    /// <typeparam name="T">The type of the maintainable to be imported.</typeparam>
    public abstract class ArtefactImportEngine<T> : ArtefactBaseEngine, IImportEngine<T>, IDeleteEngine<T>
        where T : IMaintainableObject
    {
        private readonly ReferenceImportEngine _referenceImportEngine;
        protected readonly RetrievalEngineContainer _container;
        private static readonly InsertArtefact artefactStoredProcedure = new StoredProcedures().InsertArtefact;

        /// <summary>
        /// Initializes a new instance of the abstract class.
        /// Can only be used in a derived class constructor.
        /// </summary>
        /// <param name="mappingStore">The mapping store database instance.</param>
        protected ArtefactImportEngine(Database mappingStore)
            : base(mappingStore)
        {
            _referenceImportEngine = new ReferenceImportEngine(mappingStore);
            _container = new RetrievalEngineContainer(mappingStore);
        }

        #region Interfaces implementations

        /// <summary>
        /// Insert the specified <paramref name="maintainables" /> to the mapping store.
        /// </summary>
        /// <param name="maintainables">The maintainable.</param>
        /// <param name="action">The action</param>
        /// <returns>
        /// The <see cref="IEnumerable{T}" />.
        /// </returns>
        public IEnumerable<ArtefactImportStatus> Insert(IEnumerable<T> maintainables, DatasetActionEnumType action)
        {
            return this.ReplaceOrInsert(maintainables, action).ToArray();
        }

        /// <summary>
        ///     Delete the specified <paramref name="objects" /> from Mapping Store if they exist.
        /// </summary>
        /// <param name="objects">
        ///     The objects.
        /// </param>
        public IList<ArtefactImportStatus> Delete(IEnumerable<T> objects)
        {
            return this.DeleteObjects(objects);
        }

        #endregion

        #region Stuff to override

        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        public abstract ArtefactImportStatus Insert(DbTransactionState state, T maintainable);

        /// <summary>
        /// Override this method to provide with the text formats to be inserted.
        /// </summary>
        /// <param name="maintainable">The maintainable to import the text formats for.</param>
        /// <returns>A list of the <see cref="ITextFormat"/>s to be inserted.</returns>
        protected virtual IList<ITextFormat> GetTextFormats(T maintainable)
        {
            return new List<ITextFormat>();
        }

        /// <summary>
        /// Gets the references of <paramref name="maintainable"/> to other artefacts.
        /// </summary>
        /// <param name="maintainable">The artefact to get the references for.</param>
        /// <returns>A list of the artefacts referenced.</returns>
        protected abstract IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(T maintainable);

        /// <summary>
        /// Validates an import.
        /// The method should throw an exception if validation fails, in order to rollback the transaction.
        /// </summary>
        /// <param name="database"></param>
        /// <param name="maintainable">The imported maintainable to validate.</param>
        /// <param name="artefactImportStatus"></param>
        /// <exception cref="SdmxConflictException">If validation fails.</exception>
        protected virtual void Validation(Database database, T maintainable, ArtefactImportStatus artefactImportStatus)
        {
            //TODO: what is artefactImportStatus needed for? Maybe run validation only if import status is success.
            StructureCache cache = StructureCacheContext.GetStructureCacheFromThreadLocal() != null ?
                                            StructureCacheContext.GetStructureCacheFromThreadLocal() :
                                            new StructureCache();
            ISet<ICrossReference> crossReferences = maintainable.CrossReferences;
            IList<ICrossReference> missing = new List<ICrossReference>();
            IList<ICrossReference> nonFinal = new List<ICrossReference>();
            foreach (ICrossReference crossReference in crossReferences)
            {
                if (crossReference.MaintainableStructureEnumType
                    .EnumType.IsOneOf(SdmxStructureEnumType.DataProviderScheme, 
                                      SdmxStructureEnumType.DataConsumerScheme,
                                      SdmxStructureEnumType.AgencyScheme,
                                      SdmxStructureEnumType.OrganisationScheme,
                                      SdmxStructureEnumType.OrganisationUnitScheme
                                      ))
                {
                    // organisation based schemes are always non-final
                    continue;
                }
                ItemSchemeFinalStatus itemSchemeFinalStatus = cache.GetItemSchemeFinalStatus(database, crossReference);
                if (itemSchemeFinalStatus == null ||
                    itemSchemeFinalStatus.FinalStatus == null ||
                    itemSchemeFinalStatus.FinalStatus == ArtefactFinalStatus.Empty)
                {
                    missing.Add(crossReference);
                }
                else if (!itemSchemeFinalStatus.FinalStatus.IsFinal && (maintainable.IsFinal != null && maintainable.IsFinal.IsTrue))
                {
                    if (!SkipFinalCheck(maintainable, crossReference))
                    {
                        nonFinal.Add(crossReference);
                    }
                }
                // TODO 3.0.0 check that if maintainable.Version is semver then the crossreference must be one.
            }
            if (missing.Any())
            {
                // TODO how do we handle missing references e.g. a concept is not found
                throw new SdmxConflictException("Missing references", missing.ToList<IStructureReference>());
            }
            if (nonFinal.Any())
            {
                throw new SdmxConflictException("Final cross reference ", nonFinal.ToList<IStructureReference>());
            }
        }

        /// <summary>
        /// Checks if we should skip final status check
        /// This can be overriden at concrete implementations
        /// </summary>
        /// <param name="maintainable">The maintainable </param>
        /// <param name="crossReference">the cross reference to check</param>
        /// <returns></returns>
        protected virtual bool SkipFinalCheck(T maintainable, ICrossReference crossReference)
        {
            return false;
        }

        #endregion

        /// <summary>
        ///     Insert the specified <paramref name="artefact" /> to MAPPING STORE
        /// </summary>
        /// <param name="state">
        ///     The Mapping Store connection and transaction state
        /// </param>
        /// <param name="artefact">
        ///     The artefact.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        protected override ArtefactImportStatus InsertArtefact(DbTransactionState state, IMaintainableObject artefact)
        {
            if (artefact.IsExternalReference.IsTrue)
            {
                return this.InsertArtefactInternal(state, (T)artefact, artefactStoredProcedure, null);
            }
            return this.Insert(state, (T)artefact);
        }

        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <param name="artefactStoredProcedure">
        ///     The artefact Stored Procedure.
        /// </param>
        /// <param name="setupCommand">
        ///     The setup Command. Use it to make additional changes to the command
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        protected ArtefactImportStatus InsertArtefactInternal(
            DbTransactionState state, T maintainable, ArtefactProcedurebase artefactStoredProcedure, Action<DbCommand> setupCommand)
        {
            ArtefactImportStatus artefactImportStatus;
            using (DbCommand command = artefactStoredProcedure.CreateCommandWithDefaults(state))
            {
                setupCommand?.Invoke(command);
                artefactImportStatus = this.RunArtefactCommand(maintainable, command, artefactStoredProcedure);
            }

            if (maintainable.IsExternalReference == null || !maintainable.IsExternalReference.IsTrue)
            {
                if (maintainable.StructureType != SdmxStructureEnumType.CodeList)
                { //for the moment, no need to validate the codelists
                  // validation needs to be before inserting references to see if a reference exists and if not throw a meaningful exception
                    Validation(state.Database, maintainable, artefactImportStatus);
                }
                InsertReferences(state, maintainable, artefactImportStatus);
            }

            return artefactImportStatus;
        }

        /// <summary>
        /// Insert the specified <paramref name="maintainable" /> with the default SP.
        /// </summary>
        /// <param name="state">The transaction to use.</param>
        /// <param name="maintainable">The artefact to insert.</param>
        /// <returns></returns>
        protected ArtefactImportStatus InsertArtefactInternal(DbTransactionState state, T maintainable)
        {
            return InsertArtefactInternal(state, maintainable, artefactStoredProcedure, null);
        }

        /// <summary>
        /// Inserts this artefact's references to other artefacts .
        /// </summary>
        /// <param name="state">The transaction</param>
        /// <param name="maintainable">the artefact to get the references for.</param>
        /// <param name="artefactImportStatus">The import status of the <paramref name="maintainable"/>.</param>
        /// <returns>The <paramref name="artefactImportStatus"/>.</returns>
        protected ArtefactImportStatus InsertReferences(DbTransactionState state, T maintainable, ArtefactImportStatus artefactImportStatus)
        {
            IList<ReferenceToOtherArtefact> referenceToOtherArtefacts = GetReferenceToOtherArtefacts(maintainable);
            _referenceImportEngine.InsertReferences(state, artefactImportStatus.PrimaryKeyValue, referenceToOtherArtefacts);
            _referenceImportEngine.InsertTextFormats(state, artefactImportStatus.PrimaryKeyValue, GetTextFormats(maintainable));
            return artefactImportStatus;
        }
    }
}
