// -----------------------------------------------------------------------
// <copyright file="StructureSetImportEngine.cs" company="EUROSTAT">
//   Date Created : 2014-10-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;

    /// <summary>
    ///     The DSD import engine.
    /// </summary>
    public class StructureSetImportEngine : ArtefactImportEngine<IStructureSetObject>
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(StructureSetImportEngine));

        /// <summary>
        ///     The _component import.
        /// </summary>
        private readonly CodelistMapImportEngine _codelistMapImport;

        /// <summary>
        ///     The _group import.
        /// </summary>
        private readonly StructureMapEngine _structureMapImportEngine;

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureSetImportEngine" /> class.
        /// </summary>
        /// <param name="connectionStringSettings">
        ///     The mapping store database instance.
        /// </param>
        public StructureSetImportEngine(Database connectionStringSettings)
            : base(connectionStringSettings)
        {
            this._codelistMapImport = new CodelistMapImportEngine(connectionStringSettings);
            this._structureMapImportEngine = new StructureMapEngine(connectionStringSettings);
        }

        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        public override ArtefactImportStatus Insert(DbTransactionState state, IStructureSetObject maintainable)
        {
            _log.DebugFormat(CultureInfo.InvariantCulture, "Importing artefact {0}", maintainable.Urn);
            
            var artefactStatus = this.InsertArtefactInternal(state, maintainable);

            List<long> added = new List<long>();
            added.AddRange(this._codelistMapImport.Insert(state, maintainable.CodelistMapList, artefactStatus.PrimaryKeyValue));
            added.AddRange(this._structureMapImportEngine.Insert(state, maintainable.StructureMapList, artefactStatus.PrimaryKeyValue));

            if (maintainable.ConceptSchemeMapList?.Count > 0)
            {
                if (added.Count == 0)
                {
                    return new ArtefactImportStatus(-1, new ImportMessage(ImportMessageStatus.Error, maintainable.AsReference, "Not implemented. ConceptSchemeMaps in StructureSets are not yet supported"));
                }
                return new ArtefactImportStatus(artefactStatus.PrimaryKeyValue, 
                    new ImportMessage(ImportMessageStatus.Warning, maintainable.AsReference, artefactStatus.ImportMessage.Message + "CodelistMap and/or StructureMap added. But not ConceptSchemeMaps. ConceptSchemeMap in StructureSets are not yet supported", DatasetActionEnumType.Append));
            }

            return artefactStatus;
        }

        /// <summary>
        ///     Deletes the child items.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="primaryKey">The primary key.</param>
        protected override void DeleteChildStructures(DbTransactionState state, long primaryKey)
        {
            string otherNameables = "SELECT OTH_ID FROM OTHER_NAMEABLE WHERE PARENT_ARTEFACT = {0} ";

            string tempQuery = "DELETE FROM ANNOTATION WHERE ANN_ID IN (SELECT DISTINCT ANN_ID FROM OTHER_ANNOTATION WHERE OTH_ID IN (" + otherNameables + "))";
            ExecuteArtefactUpdateOrDeleteQuery(state, tempQuery, primaryKey);

            tempQuery = "DELETE FROM OTHER_NAMEABLE WHERE PARENT_ARTEFACT = {0}";
            ExecuteArtefactUpdateOrDeleteQuery(state, tempQuery, primaryKey);
        }

        /// <inheritdoc/>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(IStructureSetObject maintainable)
        {
            IList<ReferenceToOtherArtefact> referenceToOtherArtefacts = new List<ReferenceToOtherArtefact>();
            foreach (ICodelistMapObject codelistMapBean in maintainable.CodelistMapList)
            {
                referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(codelistMapBean.TargetRef, RefTypes.Target));
                referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(codelistMapBean.SourceRef, RefTypes.Source));
            }

            foreach (IStructureMapObject structureMapBean in maintainable.StructureMapList)
            {
                referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(structureMapBean.TargetRef, RefTypes.Target));
                referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(structureMapBean.SourceRef, RefTypes.Source));
            }

            return referenceToOtherArtefacts;
        }
    }
}