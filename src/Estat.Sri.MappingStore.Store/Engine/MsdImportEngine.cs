// -----------------------------------------------------------------------
// <copyright file="MsdImportEngine.cs" company="EUROSTAT">
//   Date Created : 2017-04-13
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStore.Store.Engine.Update;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Import engine for SDMX MSD
    /// </summary>
    /// <seealso cref="ArtefactImportEngine{IMetadataStructureDefinitionObject}" />
    public class MsdImportEngine : ArtefactImportEngine<IMetadataStructureDefinitionObject>
    {
        /// <summary>
        /// The report structure import
        /// </summary>
        private readonly IIdentifiableImportEngine<IReportStructure> _reportStructureImport;

        /// <summary>
        /// The metadata target import
        /// </summary>
        private readonly IIdentifiableImportEngine<IMetadataTarget> _metadataTargetImport;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsdImportEngine"/> class.
        /// </summary>
        /// <param name="database">The mapping store database instance.</param>
        /// <param name="reportStructureImport">The report structure import.</param>
        /// <param name="metadataTargetImport">The metadata target import.</param>
        public MsdImportEngine(Database database, 
            IIdentifiableImportEngine<IReportStructure> reportStructureImport, IIdentifiableImportEngine<IMetadataTarget> metadataTargetImport)
            : base(database)
        {
            StoredProcedures storedProcedures = new StoredProcedures();
            var annotationInsertEngine = new AnnotationInsertEngine();
            this._reportStructureImport = reportStructureImport ?? new ReportStructureImportEngine(database, annotationInsertEngine);
            this._metadataTargetImport = metadataTargetImport ?? new MetadataTargetImportEngine(database, storedProcedures, annotationInsertEngine);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MsdImportEngine"/> class.
        /// </summary>
        /// <param name="database">The mapping store database instance.</param>
        public MsdImportEngine(Database database)
            : this(database, null, null)
        {
        }

        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        public override ArtefactImportStatus Insert(DbTransactionState state, IMetadataStructureDefinitionObject maintainable)
        {
            var artefactStatus = this.InsertArtefactInternal(state, maintainable);
            ItemStatusCollection targets = this._metadataTargetImport.Insert(state, maintainable.MetadataTargets, artefactStatus.PrimaryKeyValue);
            ItemStatusCollection reports = this._reportStructureImport.Insert(state, maintainable.ReportStructures, artefactStatus.PrimaryKeyValue);
            InsertReportStructureWithMetadataTargetLink(state, maintainable, targets, reports);
            return artefactStatus;
        }

        /// <summary>
        ///     Deletes the child items.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="primaryKey">The primary key.</param>
        protected override void DeleteChildStructures(DbTransactionState state, long primaryKey)
        {
            var metadataAttribute = "SELECT MA.MTD_ATTR_ID FROM METADATA_ATTRIBUTE MA WHERE MA.MSD_ID = {0} ";
            var targetObject = "SELECT MA.TARGET_OBJ_ID FROM TARGET_OBJECT MA INNER JOIN OTHER_NAMEABLE MT ON MT.OTH_ID = MA.MDT_ID " +
                "WHERE MT.PARENT_ARTEFACT = {0} ";
            var reportStructureAndMetadataTarget = "SELECT OTH_ID FROM OTHER_NAMEABLE WHERE PARENT_ARTEFACT = {0} ";
            
            ExecuteArtefactUpdateOrDeleteQuery(state,
                "DELETE FROM ANNOTATION WHERE ANN_ID IN (SELECT DISTINCT ANN_ID FROM COMPONENT_ANNOTATION " +
                "WHERE COMP_ID IN (" + metadataAttribute + "))", primaryKey);

            ExecuteArtefactUpdateOrDeleteQuery(state,
                "DELETE FROM ANNOTATION WHERE ANN_ID IN (SELECT DISTINCT ANN_ID FROM COMPONENT_ANNOTATION " +
                "WHERE COMP_ID IN (" + targetObject + "))", primaryKey);

            ExecuteArtefactUpdateOrDeleteQuery(state,
                "DELETE FROM ANNOTATION WHERE ANN_ID IN (SELECT DISTINCT ANN_ID FROM OTHER_ANNOTATION " +
                "WHERE OTH_ID IN (" + reportStructureAndMetadataTarget + "))", primaryKey);

            ExecuteArtefactUpdateOrDeleteQuery(state,
                "DELETE FROM COMPONENT_COMMON WHERE COMP_ID IN (" + metadataAttribute + ")", primaryKey);

            ExecuteArtefactUpdateOrDeleteQuery(state,
                "DELETE FROM COMPONENT_COMMON WHERE COMP_ID IN (" + targetObject + ")", primaryKey);

            ExecuteArtefactUpdateOrDeleteQuery(state,
                "DELETE FROM REPORT_STRUCTURE_META_TARGET WHERE RS_ID IN (" + reportStructureAndMetadataTarget + ")", primaryKey);

            ExecuteArtefactUpdateOrDeleteQuery(state, "DELETE FROM OTHER_NAMEABLE WHERE PARENT_ARTEFACT = {0}", primaryKey);
        }

        /// <summary>
        /// Inserts the report structure with metadata target link.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="maintainable">The maintainable.</param>
        /// <param name="targets">The targets.</param>
        /// <param name="reportStructures">The report structures.</param>
        private static void InsertReportStructureWithMetadataTargetLink(DbTransactionState state, IMetadataStructureDefinitionObject maintainable, ItemStatusCollection targets, ItemStatusCollection reportStructures)
        {
            foreach (var reportStructure in maintainable.ReportStructures)
            {
                if (!reportStructures.TryGetValue(reportStructure.Id, out ItemStatus reportStructureStatus))
                {
                    throw new MappingStoreException(string.Format("Report Structure with URN '{0}' couldn't be found in the database", reportStructure.Urn));
                }

                foreach (var targetMetadata in reportStructure.TargetMetadatas)
                {
                    if (!targets.TryGetValue(targetMetadata, out ItemStatus metadataTargetStatus))
                    {
                        throw new MappingStoreException(string.Format("Metadata Target with ID '{0}' couldn't be found in the database", targetMetadata));
                    }

                    var reportStructureParameter = state.Database.CreateInParameter("p_rs_id", DbType.Int64, reportStructureStatus.SysID);
                    var metadataTargetParameter = state.Database.CreateInParameter("p_mdt_id", DbType.Int64, metadataTargetStatus.SysID);
                    state.UsingLogger().ExecuteNonQueryFormat("INSERT INTO REPORT_STRUCTURE_META_TARGET (RS_ID, MDT_ID) VALUES ({0}, {1})", reportStructureParameter, metadataTargetParameter);
                }
            }
        }

        /// <inheritdoc/>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(IMetadataStructureDefinitionObject maintainable)
        {
            List<ReferenceToOtherArtefact> references = new List<ReferenceToOtherArtefact>();
            foreach (IMetadataTarget metadataTarget in maintainable.MetadataTargets)
            {
                if (ObjectUtil.ValidCollection(metadataTarget.IdentifiableTarget))
                {
                    foreach (IIdentifiableTarget identifiableTarget in metadataTarget.IdentifiableTarget)
                    {
                        if (identifiableTarget.HasCodedRepresentation())
                        {
                            ICrossReference enumerationReference = identifiableTarget.Representation.Representation;
                            string sourceSubStructure = string.Format("{0}.{1}", metadataTarget.Id, identifiableTarget.Id);
                            ReferenceToOtherArtefact reference = 
                                new ReferenceToOtherArtefact(enumerationReference, RefTypes.Enumeration, sourceSubStructure, null);
                            references.Add(reference);
                        }
                    }
                }
            }

            // With this code I hope I get all metadata attributes regardless on how nested they are
            // with "hope" I mean no sdmx source bugs
            List<IMetadataAttributeObject> metadataAttributes = GetMetadataAttributeObjects(maintainable);
            foreach (IMetadataAttributeObject metadataAttribute in metadataAttributes)
            {
                // code representation
                if (metadataAttribute.HasCodedRepresentation())
                {
                    references.Add(new ReferenceToOtherArtefact(metadataAttribute.Representation.Representation, RefTypes.Enumeration));
                }
                references.Add(new ReferenceToOtherArtefact(metadataAttribute.ConceptRef, RefTypes.ConceptIdentity));
            }

            return references;
        }

        /// <inheritdoc/>
        protected override IList<ITextFormat> GetTextFormats(IMetadataStructureDefinitionObject maintainable)
        {
            List<ITextFormat> textFormatObjects = new List<ITextFormat>();

            // Complicated SDMX 2.1 MSD
            // The following loop doesn't apply on SDMX 3.0.0
            foreach (IMetadataTarget metadataTarget in maintainable.MetadataTargets)
            {
                if (ObjectUtil.ValidCollection(metadataTarget.IdentifiableTarget))
                {
                    List<ITextFormat> identifiableTextFormat = metadataTarget.IdentifiableTarget
                            .Where(t => t.Representation != null && t.Representation.TextFormat != null)
                            .Select(t => t.Representation.TextFormat)
                            .ToList();
                    textFormatObjects.AddRange(identifiableTextFormat);
                }
                ConditionallyAddTextFormat(textFormatObjects, metadataTarget.ConstraintContentTarget, metadataTarget.ConstraintContentTarget?.TextType);
                ConditionallyAddTextFormat(textFormatObjects, metadataTarget.DataSetTarget, metadataTarget.DataSetTarget?.TextType);
                ConditionallyAddTextFormat(textFormatObjects, metadataTarget.KeyDescriptorValuesTarget, metadataTarget.KeyDescriptorValuesTarget?.TextType);
                IReportPeriodTarget reportPeriodTarget = metadataTarget.ReportPeriodTarget;
                if (reportPeriodTarget != null)
                {
                    TextType textType = reportPeriodTarget.TextType;
                    ITextFormatMutableObject textFormatMutableObject = new TextFormatMutableCore();
                    textFormatMutableObject.TextType = textType;
                    textFormatMutableObject.StartTime = reportPeriodTarget.StartTime;
                    textFormatMutableObject.EndTime = reportPeriodTarget.EndTime;
                    textFormatObjects.Add(new TextFormatObjectCore(textFormatMutableObject, reportPeriodTarget));
                }
            }

            textFormatObjects.AddRange(
                    GetMetadataAttributeObjects(maintainable)
                            .Where(a => a.Representation != null)
                            .Where(a => a.Representation.TextFormat != null)
                            .Select(a => a.Representation.TextFormat)
                            .ToList());
            return textFormatObjects;
        }

        private void ConditionallyAddTextFormat(List<ITextFormat> textFormats, IIdentifiableObject metadataTarget, TextType textType)
        {
            if (metadataTarget != null)
            {
                ITextFormat textFormat = GetTextFormatObject(metadataTarget, textType);
                textFormats.Add(textFormat);
            }
        }

        private ITextFormat GetTextFormatObject(IIdentifiableObject identifiableObject, TextType textType)
        {
            ITextFormatMutableObject textFormatMutableObject = new TextFormatMutableCore();
            textFormatMutableObject.TextType = textType;
            return new TextFormatObjectCore(textFormatMutableObject, identifiableObject);
        }

        private List<IMetadataAttributeObject> GetMetadataAttributeObjects(IMetadataStructureDefinitionObject maintainable)
        {
            return maintainable.IdentifiableComposites
                .Where(i => i is IMetadataAttributeObject)
                .Select(i => (IMetadataAttributeObject)i)
                .ToList();
        }
    }
}