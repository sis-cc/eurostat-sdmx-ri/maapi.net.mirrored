// -----------------------------------------------------------------------
// <copyright file="MetadataAttributeImportEngine.cs" company="EUROSTAT">
//   Date Created : 2017-04-13
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Linq;

    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Helper;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// Class MetadataAttributeImportEngine.
    /// </summary>
    public class MetadataAttributeImportEngine
    {
        /// <summary>
        /// The annotation insert engine
        /// </summary>
        private readonly IAnnotationInsertEngine _annotationInsertEngine;

        /// <summary>
        /// The store procedure
        /// </summary>
        private readonly InsertMetadataAttribute _storeProc;

        /// <summary>
        /// The insert component annotation
        /// </summary>
        private readonly InsertComponentAnnotation _insertComponentAnnotation;

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataAttributeImportEngine" /> class.
        /// </summary>
        /// <param name="storedProcedures">The stored procedures.</param>
        /// <param name="annotationInsertEngine">The annotation insert engine.</param>
        public MetadataAttributeImportEngine(StoredProcedures storedProcedures, IAnnotationInsertEngine annotationInsertEngine)
        {
            this._annotationInsertEngine = new ComponentAnnotationInsertEngine(annotationInsertEngine);
            this._storeProc = storedProcedures.InsertMetadataAttribute;
            this._insertComponentAnnotation = storedProcedures.InsertComponentAnnotation;
        }

        /// <summary>
        ///     Insert the specified <paramref name="items" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="items">
        ///     The items.
        /// </param>
        /// <param name="reportStructureSysId"> The report structure PK</param>
        /// <param name="parentArtefact">The metadata structure PK</param>
        /// <returns>
        ///     The <see cref="IEnumerable{ArtefactImportStatus}" />.
        /// </returns>
        public ItemStatusCollection Insert(DbTransactionState state, IEnumerable<IMetadataAttributeObject> items, long reportStructureSysId, long parentArtefact)
        {
            if (reportStructureSysId < 1) 
            {
                throw new SdmxInternalServerException("report structure id not set");
            }
            return this.InsertInternal(state, items.ToArray(), reportStructureSysId, parentArtefact);
        }

        /// <summary>
        /// Validates the concept scheme.
        /// </summary>
        /// <param name="conceptSchemeId">The concept scheme id.</param>
        /// <param name="reference">The reference.</param>
        /// <param name="parentIsFinal"></param>
        /// <returns>
        /// The <see cref="ItemStatus" />.
        /// </returns>
        /// <exception cref="MappingStoreException">The specified <paramref name="conceptSchemeId" /> is not valid.</exception>
        private static ItemStatus ValidateConceptScheme(ItemSchemeFinalStatus conceptSchemeId, IStructureReference reference, bool parentIsFinal)
        {
            if (conceptSchemeId.FinalStatus.PrimaryKey < 1)
            {
                throw new SdmxNoResultsException(string.Format("Dependency {0} is not available. Cannot import MSD.", reference));
            }

            if (parentIsFinal && !conceptSchemeId.FinalStatus.IsFinal)
            {
                throw new SdmxSemmanticException(string.Format("Dependency {0} is not final. Cannot import MSD.", reference));
            }

            ItemStatus conceptId;
            if (!conceptSchemeId.ItemIdMap.TryGetValue(reference.ChildReference.Id, out conceptId))
            {
                throw new SdmxNoResultsException(string.Format("Dependency {0} is not available. Cannot import MSD.", reference));
            }

            return conceptId;
        }

        /// <summary>
        /// Insert the specified <paramref name="items" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">The MAPPING STORE connection and transaction state</param>
        /// <param name="items">The items.</param>
        /// <param name="reportStructureSysId">The primary key of the parent artefact.</param>
        /// <param name="parentArtefact">The metadata structure PK</param>
        /// <returns>
        /// The <see cref="IEnumerable{ArtefactImportStatus}" />.
        /// </returns>
        private ItemStatusCollection InsertInternal(DbTransactionState state, IList<IMetadataAttributeObject> items, long reportStructureSysId, long parentArtefact)
        {
            var keyValuePairs = new List<KeyValuePair<long, IMetadataAttributeObject>>();
            foreach (var metadataAttributeObject in items)
            {
                var itemID = this.Insert(state, parentArtefact, reportStructureSysId, metadataAttributeObject);

                if (metadataAttributeObject.Annotations.Count > 0)
                {
                    this._annotationInsertEngine.Insert(state, reportStructureSysId, this._insertComponentAnnotation, metadataAttributeObject.Annotations);
                }
                if (metadataAttributeObject.MetadataAttributes.Count > 0)
                {
                    this.InsertInternal(state, metadataAttributeObject.MetadataAttributes, reportStructureSysId, parentArtefact);
                }

                keyValuePairs.Add(new KeyValuePair<long, IMetadataAttributeObject>(itemID, metadataAttributeObject));
            }

            return keyValuePairs.ToItemStatusCollection();
        }

        /// <summary>
        /// Insert the specified <paramref name="item" /> to mapping store
        /// </summary>
        /// <param name="state">The mapping store connection state.</param>
        /// <param name="msdId">The parent MSD id</param>
        /// <param name="reportStructureSysId">The parent artefact.</param>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The inserted record primary key. <c>METADATA_ATTRIBUTE.MA_ID</c>
        /// </returns>
        private long Insert(DbTransactionState state, long msdId, long reportStructureSysId, IMetadataAttributeObject item)
        {
            long itemID;
            var itemProcedure = this._storeProc;
            using (DbCommand command = itemProcedure.CreateCommandWithDefaults(state.Database))
            {
                DbParameter itemSchemeParameter = itemProcedure.CreateRsIDParameter(command);
                itemSchemeParameter.Value = reportStructureSysId;
                DbParameter parentPathParameter = itemProcedure.CreateParentPathParameter(command);
                if (item.IdentifiableParent is IMetadataAttributeObject parent)
                {
                    parentPathParameter.Value = parent.GetFullIdPath(false);
                }

                itemProcedure.CreateMaxOccursParameter(command).Value = item.MaxOccurs.ToDbValue();
                itemProcedure.CreateMinOccursParameter(command).Value = item.MinOccurs.ToDbValue();
                itemProcedure.CreateIsPresentationalParameter(command).Value = item.Presentational.IsTrue;
                itemProcedure.CreateMsdIdParameter(command).Value = msdId;

                DbParameter idParameter = itemProcedure.CreateIdParameter(command);
                idParameter.Value = item.Id;

                DbParameter outputParameter = itemProcedure.CreateOutputParameter(command);

                command.ExecuteNonQueryAndLog();
                itemID = (long)outputParameter.Value;
            }

            return itemID;
        }
    }
}