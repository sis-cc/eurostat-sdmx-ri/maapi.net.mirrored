// -----------------------------------------------------------------------
// <copyright file="StructureCache.cs" company="EUROSTAT">
//   Date Created : 2013-04-28
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------


namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.MappingStore.Store.Builder;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.Sdmx.MappingStore.Store.Properties;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using System.Data.Common;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using System.Collections.ObjectModel;
    using static Estat.Sri.MappingStore.Store.Engine.ReferenceImportEngine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using CsvHelper.Configuration;
    using System.Collections;

    /// <summary>
    ///     The structure cache.
    /// </summary>
    public class StructureCache
    {
        /// <summary>
        ///     The table info builder.
        /// </summary>
        private static readonly ItemTableInfoBuilder _tableInfoBuilder = new ItemTableInfoBuilder();

        /// <summary>
        ///     The _dictionary.
        /// </summary>
        private readonly IDictionary<IStructureReference, ItemSchemeFinalStatus> _dictionary = new Dictionary<IStructureReference, ItemSchemeFinalStatus>();
        private readonly IDictionary<string, ReferenceWithPrimaryKey> mapReferenceWithPrimaryKey= new Dictionary<string, ReferenceWithPrimaryKey>();

        /// <summary>
        ///     Gets the component map ids.
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="dsdReference">
        ///     The DSD reference.
        /// </param>
        /// <returns>
        ///     The component id to primary key value dictionary
        /// </returns>
        public static IDictionary<string, long> GetComponentMapIds(DbTransactionState state, IStructureReference dsdReference)
        {
            var database = state.Database;
            return GetComponentMapIds(dsdReference, database);
        }

        /// <summary>
        ///     Gets the component map ids.
        /// </summary>
        /// <param name="dsdReference">The DSD reference.</param>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     The component id to primary key value dictionary
        /// </returns>
        public static IDictionary<string, long> GetComponentMapIds(IStructureReference dsdReference, Database database)
        {
            IDictionary<string, long> map = new Dictionary<string, long>(StringComparer.Ordinal);

            var idParameter = database.CreateInParameter("p_id", DbType.AnsiString, dsdReference.MaintainableId);
            var agencyParameter = database.CreateInParameter("p_agency", DbType.AnsiString, dsdReference.AgencyId);
            var versionParameter = database.CreateInParameter("p_version", DbType.AnsiString, dsdReference.Version);

            string queryFormat;
            if (dsdReference.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.Dsd)
            {
                queryFormat =
                    "select c.COMP_ID, c.ID from COMPONENT c inner join ARTEFACT_VIEW a on a.ART_ID = c.DSD_ID where a.ID = {0} and a.AGENCY = {1} and a.VERSION = {2}";
            }
            else
            {
                queryFormat =
                    "select c.COMP_ID, c.ID from COMPONENT c inner join DATAFLOW d on d.DSD_ID = c.DSD_ID inner join ARTEFACT_VIEW a on a.ART_ID = d.DF_ID where a.ID = {0} and a.AGENCY = {1} and a.VERSION = {2}";
            }

            using (var command = database.GetSqlStringCommandFormat(queryFormat, idParameter, agencyParameter, versionParameter))
            using (var reader = database.ExecuteReader(command))
            {
                var sysIdOrdinal = reader.GetOrdinal("COMP_ID");
                var idOrdinal = reader.GetOrdinal("ID");

                while (reader.Read())
                {
                    map.Add(reader.GetString(idOrdinal), reader.GetInt64(sysIdOrdinal));
                }
            }

            return map;
        }

        /// <summary>
        ///     Returns the <c>ITEM.ITEM_ID</c> value from Mapping Store for <paramref name="artefactId" />
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="sdmxStructure">
        ///     The SDMX Structure.
        /// </param>
        /// <param name="artefactId">
        ///     The artefact primary key. <c>ARTEFACT.ART_ID</c>.
        /// </param>
        /// <returns>
        ///     The <c>ITEM.ITEM_ID</c> value from Mapping Store for <paramref name="artefactId" />
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="state" /> is null
        /// </exception>
        /// <exception cref="ArgumentException">
        ///     At <paramref name="sdmxStructure" />, unsupported structure
        /// </exception>
        public static ItemStatusCollection GetId(DbTransactionState state, SdmxStructureEnumType sdmxStructure, long artefactId)
        {
            return GetId(state.Database, sdmxStructure, artefactId);
        }

        /// <summary>
        ///     Returns the <c>ITEM.ITEM_ID</c> value from Mapping Store for <paramref name="artefactId" />
        /// </summary>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="sdmxStructure">
        ///     The SDMX Structure.
        /// </param>
        /// <param name="artefactId">
        ///     The artefact primary key. <c>ARTEFACT.ART_ID</c>.
        /// </param>
        /// <returns>
        ///     The <c>ITEM.ITEM_ID</c> value from Mapping Store for <paramref name="artefactId" />
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="state" /> is null
        /// </exception>
        /// <exception cref="ArgumentException">
        ///     At <paramref name="sdmxStructure" />, unsupported structure
        /// </exception>
        public static ItemStatusCollection GetId(Database state, SdmxStructureEnumType sdmxStructure, long artefactId)
        {
            if (state == null)
            {
                throw new ArgumentNullException("state");
            }

            var tableInfo = _tableInfoBuilder.Build(sdmxStructure);
            if (tableInfo == null)
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, Resources.ExceptionUnsupportedStructureReferenceFormat1, sdmxStructure), "sdmxStructure");
            }

            var itemIdQueryBuilder = new ItemIdQueryBuilder(state);

            var query = itemIdQueryBuilder.Build(tableInfo);
            var cmd = state.GetSqlStringCommandFormat(query, state.CreateInParameter("id", DbType.Int64, artefactId));
            var reader = state.ExecuteReader(cmd);
            var items = new List<ItemStatus>();
            while (reader.Read())
            {
                var id = reader.GetSafeString("ID");
                var sysId = reader.GetSafeInt64("SYSID");

                ItemStatus itemStatus = new ItemStatus(id, sysId);
                items.Add(itemStatus);
            }

            return new ItemStatusCollection(items);
        }

        /// <summary>
        /// Method to get artefact final status from structure reference. In case of a cross-reference, the version of the parent must be checked.
        /// In case the parent is non-final and the reference version has wildcard, we should search the database for non-final artefacts as well
        /// </summary>
        /// <param name="db"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        public ArtefactFinalStatus GetArtefactFinalStatus(Database db, IStructureReference reference)
        {
            bool isReferencedByFinal = MaintainableUtil<IMaintainableObject>.IsReferencedByFinal(reference);

            return GetArtefactFinalStatus(db, reference, isReferencedByFinal);
        }

        /// <summary>
        /// Method to get an item's final status from structure reference. In case of a cross-reference, the version of the parent must be checked.
        /// In case the parent is non-final and the reference version has wildcard, we should search the database for non-final artefacts as well
        /// The method searches just for the item scheme's final status to avoid searching all items individually.
        /// </summary>
        /// <param name="db"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        public ArtefactFinalStatus GetArtefactStatusOfItem(Database db, IStructureReference reference)
        {
            bool isReferencedByFinal = MaintainableUtil<IMaintainableObject>.IsReferencedByFinal(reference);

            return GetArtefactFinalStatus(db, 
                new StructureReferenceImpl(reference.AgencyId, reference.MaintainableId, reference.Version, reference.MaintainableStructureEnumType), 
                isReferencedByFinal);
        }

        /// <summary>
        ///  Method to get an item's final status from structure reference. In case of a cross-reference, the version of the parent must be checked.
        ///  In case the parent is non-final and the reference version has wildcard, we should search the database for non-final artefacts as well
        ///  The method searches just for the item scheme's final status to avoid searching all items individually.
        /// </summary>
        /// <param name="db"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        public ArtefactFinalStatus GetArtefactFinalStatusOfItem(Database db, IStructureReference reference, IStructureReference parentReference)
        {
            bool isReferencedByFinal = MaintainableUtil<IMaintainableObject>.IsReferencedByFinal(reference);
            return GetArtefactFinalStatus(db, parentReference, isReferencedByFinal);
        }

        /// <summary>
        ///  Method to get artefact final status from structure reference. Used when reference cannot have wildcard in version to avoid costly operations
        /// </summary>
        /// <param name="db"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        public ArtefactFinalStatus GetArtefactFinalStatusOfSpecificStructure(Database db, IStructureReference reference)
        {
            return GetArtefactFinalStatus(db, reference, null);
        }

        /// <summary>
        /// Retrieves the status of the artefact regarding the final property
        /// </summary>
        /// <param name="db">The database instance.</param>
        /// <param name="reference">The artefact to get the status for.</param>
        /// <param name="isReferencedByFinal"></param>
        /// <returns>A <see cref="ArtefactFinalStatus"/> with the status information.</returns>
        /// <exception cref="ArgumentNullException">For <paramref name="db"/> and <paramref name="reference"/>.</exception>
        public ArtefactFinalStatus GetArtefactFinalStatus(Database db, IStructureReference reference, bool? isReferencedByFinal)
        {
            if (db == null)
            {
                throw new ArgumentNullException(nameof(db));
            }

            if (reference == null)
            {
                throw new ArgumentNullException(nameof(reference));
            }

            if (_dictionary.ContainsKey(reference))
            {
                ItemSchemeFinalStatus itemSchemeFinalStatus = _dictionary[reference];
                if (itemSchemeFinalStatus == null)
                {
                    return null;
                }

                return itemSchemeFinalStatus.FinalStatus;
            }
         
            ArtefactCommandBuilder statementBuilder = new ArtefactCommandBuilder(db);
            switch (reference.MaintainableStructureEnumType.EnumType)
            {
                case SdmxStructureEnumType.ContentConstraint:
                    statementBuilder.WhereArtefactType(
                        SdmxStructureEnumType.ContentConstraint,
                        SdmxStructureEnumType.AllowedConstraint,
                        SdmxStructureEnumType.ActualConstraint);
                    break;
                case SdmxStructureEnumType.AllowedConstraint:
                case SdmxStructureEnumType.ActualConstraint:
                    statementBuilder.WhereArtefactType(SdmxStructureEnumType.ContentConstraint, reference.MaintainableStructureEnumType);
                    break;
                default:
                    break;
            }
            var query = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                .SetMaintainableIds(reference.MaintainableId)
                .SetAgencyIds(reference.AgencyId)
                .SetVersionRequests(VersionRequestCore.FromSdmxReference(reference.Version, isReferencedByFinal ?? true))
                .SetMaintainableTarget(reference.MaintainableStructureEnumType)
                .Build();

            using (DbCommand command = statementBuilder.BuildArtefactStatus(query))
            {
                using (IDataReader dataReader = db.ExecuteReader(command))
                {
                    if (dataReader.Read())
                    {
                        //art_id can never be null in database
                        long artId = DataReaderHelper.GetInt64(dataReader, 0);
                        // at columnIndex 1 we can get the ART_BASE_ID if needed
                        long artBaseId = dataReader.GetInt64(1);
                        string versionExtension = DataReaderHelper.GetString(dataReader, 2);
                        //is_final can be null in database
                        //if (dataReader.wasNull())
                        //{
                        //    versionExtension = null;
                        //}
                        bool isStub = DataReaderHelper.GetBoolean(dataReader, 3);
                        return new ArtefactFinalStatus(artId, versionExtension, artBaseId);
                    }
                }
            }

            // TODO Move this to ArtefactDAOUtils together with ArtefactPreparedStatementBuilder
            // return ArtefactDAOUtils.getArtefactFinalStatus(conn, bean, artefactType);
            return null;
        }

        public ItemSchemeFinalStatus GetItemSchemeFinalStatus(Database database, IStructureReference reference)
        {
            if (database == null)
            {
                throw new ArgumentNullException(nameof(database));
            }
            if (reference == null)
            {
                throw new ArgumentNullException(nameof(reference));
            }

            ItemSchemeFinalStatus returnObjet;
            IStructureReference maintainableReference = GetMaintainableReference(reference);

            // we cache maintainable requests
            if (this._dictionary.TryGetValue(maintainableReference, out returnObjet))
            {
                // ok we have it, but do we need to get it again?
                if (returnObjet == null || 
                    reference.TargetReference.IsMaintainable || 
                    !SdmxStructureTypeUtils.IsItemScheme(reference.TargetReference) 
                    || returnObjet.ItemIdMap != null)
                {
                    return returnObjet;
                }
            }

            if (!reference.TargetReference.IsMaintainable)
            {
                if (SdmxStructureTypeUtils.IsItemScheme(reference.TargetReference))
                {
                    var itemSchemeStatus = GetItems(database, reference);
                    _dictionary[maintainableReference] = itemSchemeStatus;
                    return itemSchemeStatus;
                }

                if (reference.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.Dsd)
                {
                    // TODO DSD Component
                    //  return GetDsdComponents(database, reference);
                }
            }

            ArtefactFinalStatus artefactFinalStatus = GetArtefactFinalStatus(database, reference);

            var nonItemSchemeWithItems = new ItemSchemeFinalStatus(artefactFinalStatus);
            _dictionary[maintainableReference] = nonItemSchemeWithItems;
            return nonItemSchemeWithItems;
        }


        /// <summary>
        /// Gets the structure.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="reference">The reference.</param>
        /// <returns>The <see cref="ItemStatusCollection" /></returns>
        public ItemSchemeFinalStatus GetStructure(Database state, IStructureReference reference)
        {
            if (state == null)
            {
                throw new ArgumentNullException(nameof(state));
            }

            if (reference == null)
            {
                throw new ArgumentNullException(nameof(reference));
            }

            ItemSchemeFinalStatus returnObjet;
            IStructureReference maintainableReference;
            if (reference.HasChildReference())
            {
                maintainableReference = new StructureReferenceImpl(reference.MaintainableReference, reference.MaintainableStructureEnumType);
            }
            else
            {
                maintainableReference = reference;
            }

            // we cache maintainable requests
            if (!this._dictionary.TryGetValue(maintainableReference, out returnObjet))
            {
                var artefactFinalStatus = GetArtefactFinalStatus(state, maintainableReference, MaintainableUtil<IMaintainableObject>.IsReferencedByFinal(reference));

                // we need to pass the original reference here in order to see if we should get the items
                var collection = GetStructure(state, reference, artefactFinalStatus?.PrimaryKey);

                returnObjet = new ItemSchemeFinalStatus(artefactFinalStatus ?? ArtefactFinalStatus.Empty, collection);
                this._dictionary.Add(maintainableReference, returnObjet);
            }
            else if (returnObjet.ItemIdMap == null && reference.HasChildReference())
            {
                var artefactFinalStatus = returnObjet.FinalStatus;
                // we need to pass the original reference here in order to see if we should get the items
                var collection = GetStructure(state, reference, artefactFinalStatus?.PrimaryKey);

                returnObjet = new ItemSchemeFinalStatus(artefactFinalStatus ?? ArtefactFinalStatus.Empty, collection);
                this._dictionary[maintainableReference] = returnObjet;
            }

            return returnObjet;
        }

        /// <summary>
        /// Get the maintainable reference used as a key
        /// </summary>
        /// <param name="reference">The reference</param>
        /// <returns>The maintainable reference to be used a a key in <see cref="_dictionary"/></returns>
        private static IStructureReference GetMaintainableReference(IStructureReference reference)
        {
            IStructureReference maintainableReference;
            if (reference.HasChildReference())
            {
                maintainableReference = new StructureReferenceImpl(reference.MaintainableReference, reference.MaintainableStructureEnumType);
            }
            else
            {
                maintainableReference = reference;
            }

            return maintainableReference;
        }

        /// <summary>
        ///     Gets the structure.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="reference">The reference.</param>
        /// <returns>The <see cref="ItemStatusCollection" /></returns>
        public ItemSchemeFinalStatus GetStructure(DbTransactionState state, IStructureReference reference)
        {
            return GetStructure(state.Database, reference);
        }

        /// <summary>
        /// Checks if the <paramref name="reference"/> exists in the <paramref name="database"/>.
        /// </summary>
        /// <param name="database">The mapping store.</param>
        /// <param name="reference">The reference to check.</param>
        /// <returns><c>true</c> or <c>false</c>.</returns>
        /// <exception cref="ArgumentNullException">For <paramref name="database"/> and <paramref name="reference"/>.</exception>
        public bool Exists(Database database, IStructureReference reference)
        {
            if (database == null)
            {
                throw new ArgumentNullException(nameof(database));
            }
            if (reference == null)
            {
                throw new ArgumentNullException(nameof(reference));
            }

            if (_dictionary.ContainsKey(reference))
            {
                return _dictionary[reference] != null;
            }
            if (reference.TargetReference.IsMaintainable)
            {
                StructureReferenceImpl parentReference = new StructureReferenceImpl(reference.AgencyId, reference.MaintainableId,
                   reference.Version, reference.MaintainableStructureEnumType);

                if (_dictionary.ContainsKey(parentReference))
                {
                    return _dictionary[parentReference] != null;
                }
                ArtefactFinalStatus artefactFinalStatus = GetArtefactFinalStatus(database, parentReference);
                _dictionary.Add(parentReference, new ItemSchemeFinalStatus(artefactFinalStatus, null));
                return artefactFinalStatus != null;
            }
            if (SdmxStructureTypeUtils.IsItemScheme(reference.TargetReference))
            {
                return ExistsItem(database, reference);
            }

            // TODO DSD Component
            return true;
        }

        private bool ExistsItem(Database database, IStructureReference reference)
        {
            IStructureReference parentReference = new StructureReferenceImpl(reference.AgencyId, reference.MaintainableId,
                    reference.Version, reference.MaintainableStructureEnumType);

            if (_dictionary.ContainsKey(parentReference))
            {
                ItemSchemeFinalStatus itemSchemeFinalStatus = _dictionary[parentReference];

                if (itemSchemeFinalStatus == null)
                {
                    return false;
                }

                List<IComplexIdentifiableReferenceObject> categories = null;
                List<IComplexIdentifiableReferenceObject> requestedItems = null;

                if (reference.TargetReference == SdmxStructureEnumType.Category)
                {
                    categories = new List<IComplexIdentifiableReferenceObject>
                    {
                        ComplexIdentifiableReferenceCore.CreateForRest(reference.FullId,SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category))
                    };
                }
                else
                {
                    requestedItems = new List<IComplexIdentifiableReferenceObject>
                    {
                        ComplexIdentifiableReferenceCore.CreateForRest(reference.FullId, reference.TargetReference)
                    };
                }

                return reference.TargetReference == SdmxStructureEnumType.Category
                    ? categories.Any(c => itemSchemeFinalStatus.ItemIdMap[c.Id.SearchParameter] != null)
                    : requestedItems.Any(c => itemSchemeFinalStatus.ItemIdMap[c.Id.SearchParameter] != null);
            }

            return FindParentReferenceWithItemsAndCacheIt(database, reference, parentReference);
        }

        private bool FindParentReferenceWithItemsAndCacheIt(Database database, IStructureReference reference, IStructureReference parentReference)
        {
            ArtefactFinalStatus artefactFinalStatus = GetArtefactFinalStatusOfItem(database, reference, parentReference);

            if (artefactFinalStatus == null)
            {
                _dictionary[reference] = null;
                return false;
            }

            List<IComplexIdentifiableReferenceObject> categories = null;
            List<IComplexIdentifiableReferenceObject> requestedItems = null;

            List<ItemStatus> items = new List<ItemStatus>();

            if (reference.TargetReference == SdmxStructureEnumType.Category)
            {
                categories = new List<IComplexIdentifiableReferenceObject>
                {
                    ComplexIdentifiableReferenceCore.CreateForRest(reference.FullId, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category))
                };
            }
            else
            {
                requestedItems = new List<IComplexIdentifiableReferenceObject>
                {
                    ComplexIdentifiableReferenceCore.CreateForRest(reference.FullId, reference.TargetReference)
                };
            }

            ItemCommandBuilder itemCommandBuilder = new ItemCommandBuilder(database);
            using (DbCommand command = itemCommandBuilder.WithoutLocalisedString().Build(artefactFinalStatus.PrimaryKey))
            {
                using (IDataReader reader = database.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        long sysId = DataReaderHelper.GetInt64(reader, "SYSID");
                        string id = DataReaderHelper.GetString(reader, "ID");
                        ItemStatus itemStatus = new ItemStatus(id, sysId);
                        items.Add(itemStatus);
                    }
                }
            }

            if (items.Count == 0)
            {
                _dictionary[parentReference] = null;
                return false;
            }

            ItemSchemeFinalStatus itemSchemeFinalStatus = new ItemSchemeFinalStatus(artefactFinalStatus, new ItemStatusCollection(items));
            _dictionary[parentReference] = itemSchemeFinalStatus;

            return reference.TargetReference == SdmxStructureEnumType.Category && categories != null
                ? categories.Any(c => itemSchemeFinalStatus.ItemIdMap[c.Id.SearchParameter] != null)
                : requestedItems != null && requestedItems.Any(c => itemSchemeFinalStatus.ItemIdMap[c.Id.SearchParameter] != null);
        }

        private ItemSchemeFinalStatus GetItems(Database database, IStructureReference reference)
        {
            ArtefactFinalStatus artefactFinalStatus = GetArtefactFinalStatus(database,
                new StructureReferenceImpl(reference.AgencyId, reference.MaintainableId, reference.Version, reference.MaintainableStructureEnumType));
            if (artefactFinalStatus == null)
            {
                return null;
            }

            ItemCommandBuilder itemCommandBuilder = new ItemCommandBuilder(database);

            IList<ItemStatus> items = new List<ItemStatus>();
            if (reference.TargetReference.EnumType == SdmxStructureEnumType.Category)
            {
                IList<IComplexIdentifiableReferenceObject> categories = new List<IComplexIdentifiableReferenceObject>()
                {
                    ComplexIdentifiableReferenceCore.CreateForRest(reference.FullId, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category))
                };
                itemCommandBuilder.WithNestedSpecificItems(categories);
            }
            else
            {
                IList<IComplexIdentifiableReferenceObject> requestedItems = new List<IComplexIdentifiableReferenceObject>()
                {
                    ComplexIdentifiableReferenceCore.CreateForRest(reference.FullId, reference.TargetReference)
                };
                // unless we change it to have all cross-referenced items we shouldn't get them one by one for performance reasons.
                // itemCommandBuilder.WithNotNestedSpecificItems(requestedItems);
            }

            // Here we should get either all items or all items cross reference in one SQL query not 
            // so we dont have to run 100s queries for the same item scheme
            using (DbCommand command = itemCommandBuilder.WithoutLocalisedString().Build(artefactFinalStatus.PrimaryKey))
            {
                using(IDataReader reader = database.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        long sysId = DataReaderHelper.GetInt64(reader, "SYSID");
                        string id = DataReaderHelper.GetString(reader, "ID");
                        ItemStatus itemStatus = new ItemStatus(id, sysId);
                        items.Add(itemStatus);
                    }
                }
            }

            if (!items.Any())
            {
                return null;
            }

            return new ItemSchemeFinalStatus(artefactFinalStatus, new ItemStatusCollection(items));
        }



        /// <summary>
        /// Gets the other identifiable ids.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <returns>
        /// The Item Status
        /// </returns>
        /// <exception cref="System.ArgumentNullException">childrenToParentTables is null or empty</exception>
        private IEnumerable<ItemStatus> GetOtherIdentifiableIds(Database database, long parentSysId, SdmxStructureType sdmxStructure)
        {
            if (sdmxStructure == null)
            {
                throw new ArgumentNullException(nameof(sdmxStructure));
            }

            var retriever = new IdentifiableMapRetrieverEngine("ID", sdmxStructure);
            var identifiableMap = retriever.Retrieve(
                database,
                parentSysId,
                sdmxStructure.EnumType == SdmxStructureEnumType.Category
                    ? _tableInfoBuilder.Build(SdmxStructureEnumType.Category).ParentItem
                    : string.Empty, sdmxStructure.EnumType);
            return identifiableMap.Dictionary.Select(pair => new ItemStatus(pair.Key, pair.Value));
        }

        /// <summary>
        /// Gets the structure.
        /// </summary>
        /// <param name="database">The state.</param>
        /// <param name="reference">The reference.</param>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <returns>
        /// The <see cref="ItemStatusCollection" />.
        /// </returns>
        private ItemStatusCollection GetStructure(
            Database database,
            IStructureReference reference,
            long? parentSysId)
        {
            ItemStatusCollection collection = null;
            if (parentSysId != null && reference.HasChildReference())
            {
                var items = GetOtherIdentifiableIds(
                               database,
                               parentSysId.Value,
                               reference.TargetReference);
                collection = new ItemStatusCollection(items);
            }
            return collection;
        }

        public void AddReferenceWithItemsToCache(Database database, IStructureReference reference, List<IItemObject> items, List<long> itemsIds)
        {
            // Get first the ArtefactFinalStatus of the reference
            ArtefactFinalStatus artefactFinalStatus = GetArtefactFinalStatus(database, reference, null);

            if (items == null)
            {
                _dictionary[reference] = new ItemSchemeFinalStatus(artefactFinalStatus, null);
                return;
            }

            // Cache the reference along with the items
            List<ItemStatus> itemStatusList = new List<ItemStatus>();

            for (int i = 0; i < items.Count; i++)
            {
                ItemStatus itemStatus = new ItemStatus(items[i].Id,-1);
                itemStatusList.Add(itemStatus);
            }

            ItemSchemeFinalStatus itemSchemeFinalStatus = new ItemSchemeFinalStatus(artefactFinalStatus, new ItemStatusCollection(itemStatusList));
            _dictionary[reference] = itemSchemeFinalStatus;
        }


        public ReferenceWithPrimaryKey GetCachedReferenceWithPrimaryKey(Database database, ReferenceWithPrimaryKey referenceWithPrimaryKey)
        {
            string key = referenceWithPrimaryKey.Reference.Target.MaintainableUrn.ToString();
            if (mapReferenceWithPrimaryKey.TryGetValue(key, out ReferenceWithPrimaryKey value))
            {
                return value;
            }
            else
            {
                using (DbCommand command = database.GetSqlStringCommandFormat(
                    "select ART_BASE_ID from ARTEFACT_BASE A where ID = {0} and AGENCY = {1} and ARTEFACT_TYPE = {2} "
                    , database.CreateInParameter("p_id", DbType.AnsiString, referenceWithPrimaryKey.Reference.Target.MaintainableId)
                    , database.CreateInParameter("p_agency", DbType.AnsiString, referenceWithPrimaryKey.Reference.Target.AgencyId)
                    , database.CreateInParameter("p_type", DbType.AnsiString, referenceWithPrimaryKey.Reference.Target.MaintainableStructureEnumType.UrnClass)
                    ))
                {
                    using (IDataReader dataReader = database.ExecuteReader(command))
                    {
                        if (dataReader.Read())
                        {
                            referenceWithPrimaryKey.SetTargetArtefactPk(dataReader.GetInt64(0));
                            mapReferenceWithPrimaryKey[key] = referenceWithPrimaryKey;
                            return referenceWithPrimaryKey;
                        }
                        else
                        {
                            throw new SdmxConflictException("Missing references", new[] { referenceWithPrimaryKey.Reference.Target });
                        }
                    }
                }
            }
        }
    }
}