// -----------------------------------------------------------------------
// <copyright file="ContentConstraintImportEngine.cs" company="EUROSTAT">
//   Date Created : 2014-10-13
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Exception;

namespace Estat.Sri.MappingStore.Store.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Estat.Sri.Sdmx.MappingStore.Store.Properties;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;

    /// <summary>
    ///     The content constraint import engine.
    /// </summary>
    public class ContentConstraintImportEngine : ArtefactImportEngine<IContentConstraintObject>
    {
        /// <summary>
        ///     The validate status engine
        /// </summary>
        private readonly ValidateStatusEngine _validateStatusEngine = new ValidateStatusEngine();

        /// <summary>
        /// The data source import engine
        /// </summary>
        private readonly DataSourceImportEngine _dataSourceImportEngine;


        /// <summary>
        ///     Initializes a new instance of the <see cref="ContentConstraintImportEngine" /> class.
        /// </summary>
        /// <param name="database">
        ///     The mapping store database instance.
        /// </param>
        public ContentConstraintImportEngine(Database database)
            : base(database)
        {
            _dataSourceImportEngine = new DataSourceImportEngine();
        }

        #region overrides

        /// <summary>
        ///     Insert the specified <paramref name="maintainable" /> to the mapping store with <paramref name="state" />
        /// </summary>
        /// <param name="state">
        ///     The MAPPING STORE connection and transaction state
        /// </param>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <returns>
        ///     The <see cref="ArtefactImportStatus" />.
        /// </returns>
        public override ArtefactImportStatus Insert(DbTransactionState state, IContentConstraintObject maintainable)
        {
            var insertContentConstraint = new InsertContentConstraintProcedure();

            var attachType = GetAttachType(maintainable);
            Action<DbCommand> setupCommand = command =>
            {
                insertContentConstraint.CreateActualDataParameter(command).Value =
                    maintainable.IsDefiningActualDataPresent.ToDbValue();

                if (maintainable.ReleaseCalendar != null)
                {
                    if (!string.IsNullOrWhiteSpace(maintainable.ReleaseCalendar.Offset))
                    {
                        insertContentConstraint.CreateOffsetParameter(command).Value =
                            maintainable.ReleaseCalendar.Offset;
                    }

                    if (!string.IsNullOrWhiteSpace(maintainable.ReleaseCalendar.Periodicity))
                    {
                        insertContentConstraint.CreatePeriodicityParameter(command).Value =
                            maintainable.ReleaseCalendar.Periodicity;
                    }

                    if (!string.IsNullOrWhiteSpace(maintainable.ReleaseCalendar.Tolerance))
                    {
                        insertContentConstraint.CreateToleranceParameter(command).Value =
                            maintainable.ReleaseCalendar.Tolerance;
                    }
                }

                insertContentConstraint.CreateAttachTypeParameter(command).Value = attachType.ToString("G");
                switch (attachType)
                {
                    case ContentConstraintAttachType.DataSet:
                    case ContentConstraintAttachType.MetadataSet:
                        insertContentConstraint.CreateDataSourceParameter(command);
                        insertContentConstraint.CreateSetIdParameter(command).Value =
                            maintainable.ConstraintAttachment.DataOrMetadataSetReference.SetId;
                        break;
                    case ContentConstraintAttachType.SimpleDataSource:
                        insertContentConstraint.CreateSetIdParameter(command);
                        var dataSource = maintainable.ConstraintAttachment.DataSources.First(x => x.SimpleDatasource);
                        insertContentConstraint.CreateDataSourceParameter(command).Value = dataSource.DataUrl.AbsoluteUri;
                        break;
                    case ContentConstraintAttachType.DataProvider:
                    default:
                        insertContentConstraint.CreateDataSourceParameter(command);
                        insertContentConstraint.CreateSetIdParameter(command);
                        break;
                }

                if (maintainable.ReferencePeriod != null)
                {
                    if (maintainable.ReferencePeriod.StartTime != null)
                    {
                        insertContentConstraint.CreateStartTimeParameter(command).Value = maintainable.ReferencePeriod.StartTime.Date;
                    }
                    if (maintainable.ReferencePeriod.EndTime != null)
                    {
                        insertContentConstraint.CreateEndTimeParameter(command).Value = maintainable.ReferencePeriod.EndTime.Date;
                    }

                }
            };

            var artefactStatus =
                this.InsertArtefactInternal(state, maintainable, insertContentConstraint, setupCommand);
            if (attachType.IsMaintainable())
            {
                this.InsertDataSources(state, maintainable, artefactStatus);
            }

            if (maintainable.IncludedCubeRegion != null)
            {
                InsertCubeRegion(state, maintainable.IncludedCubeRegion, artefactStatus, true);

                var cubeRegion = maintainable.IncludedCubeRegion as CubeRegionCore;
                if (cubeRegion != null && cubeRegion.Warnings.Any())
                {
                    var warningsMessage = artefactStatus.ImportMessage.Message.TrimEnd('.') + " with warnings:" + Environment.NewLine +  cubeRegion.Warnings.Aggregate((i, j) => i + "." + Environment.NewLine + j) + ".";
                    artefactStatus = new ArtefactImportStatus(artefactStatus.PrimaryKeyValue, new ImportMessage(ImportMessageStatus.Warning, maintainable.AsReference, warningsMessage, artefactStatus.ImportMessage.ActualAction));
                }
            }

            if (maintainable.ExcludedCubeRegion != null)
            {
                InsertCubeRegion(state, maintainable.ExcludedCubeRegion, artefactStatus, false);

                var cubeRegion = maintainable.IncludedCubeRegion as CubeRegionCore;
                if (cubeRegion != null && cubeRegion.Warnings.Any())
                {
                    var warningsMessage = artefactStatus.ImportMessage.Message.TrimEnd('.') + " with warnings:" + Environment.NewLine + cubeRegion.Warnings.Aggregate((i, j) => i + "." + Environment.NewLine + j) + ".";
                    artefactStatus = new ArtefactImportStatus(artefactStatus.PrimaryKeyValue, new ImportMessage(ImportMessageStatus.Warning, maintainable.AsReference, warningsMessage, artefactStatus.ImportMessage.ActualAction));
                }
            }

            return artefactStatus;
        }

        /// <inheritdoc/>
        protected override void CheckPrerequisites(IMaintainableObject maintainable)
        {
            var contentConstraintToInsert = maintainable as IContentConstraintObject;

            foreach (var structure in maintainable.CrossReferences)
            {
                var contentConstraints = GetCrossReferencingContentConstraint(structure);                  
                if (contentConstraints.Any())
                {
                    foreach (IContentConstraintMutableObject contentConstraint in contentConstraints)
                    {
                        if (!ShouldCheckContentConstraint(contentConstraintToInsert.MutableInstance, contentConstraint))
                        {
                            continue;
                        }

                        var commonDimensions = GetCommonDimensions(contentConstraint, contentConstraintToInsert);
                        if (commonDimensions.Any())
                        {
                            var overlappingDimensionsString = commonDimensions.Aggregate((i, j) => i + "," + j).TrimEnd(',');
                            throw new SdmxSemmanticException($"There is already a content constraint for dimension(s) {overlappingDimensionsString} artefact {structure.GetAsHumanReadableString()} ");
                        }
                    }
                }
            }
        }

        /// <inheritdoc/>
        protected override IList<ReferenceToOtherArtefact> GetReferenceToOtherArtefacts(IContentConstraintObject maintainable)
        {
            IList<ReferenceToOtherArtefact> referenceToOtherArtefacts = new List<ReferenceToOtherArtefact>();
            ContentConstraintAttachType contentConstraintattachType = GetAttachType(maintainable);
            switch (contentConstraintattachType)
            {
                case ContentConstraintAttachType.DataSet:
                case ContentConstraintAttachType.MetadataSet:
                    ICrossReference dataProviderRef = maintainable.ConstraintAttachment.DataOrMetadataSetReference.DataSetReference;
                    referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(dataProviderRef, RefTypes.DataProvider));
                    break;
                case ContentConstraintAttachType.DataProvider:

                    ISet<ICrossReference> crossReferences = maintainable.ConstraintAttachment.StructureReference;
                    foreach (ICrossReference crossReference in crossReferences)
                    {
                        referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(crossReference, RefTypes.DataProvider));
                    }
                    break;
                case ContentConstraintAttachType.Metadataflow:
                case ContentConstraintAttachType.Dataflow:
                case ContentConstraintAttachType.DataStructure:
                case ContentConstraintAttachType.ProvisionAgreement:
                case ContentConstraintAttachType.MetadataStructure:
                    foreach (ICrossReference crossReferenceBean in maintainable.ConstraintAttachment.StructureReference)
                    {
                        referenceToOtherArtefacts.Add(new ReferenceToOtherArtefact(crossReferenceBean, RefTypes.ConstraintAttachment));
                    }
                    break;
                default:
                    break;
            }
            return referenceToOtherArtefacts;
        }

        #endregion

        #region private methods


        private IEnumerable<string> GetCommonDimensions(IContentConstraintMutableObject contentConstraint, IContentConstraintObject contentConstraintToInsert)
        {
            var componentsFromDb = GetAllComponents(contentConstraint);
            var componentsToInsert = GetAllComponents(contentConstraintToInsert.MutableInstance);
            return componentsFromDb.Intersect(componentsToInsert);
        }

        private List<string> GetAllComponents(IContentConstraintMutableObject contentConstraint)
        {
            var allComponents = new List<string>();
            if (contentConstraint.ExcludedCubeRegion != null)
            {
                allComponents.AddRange(contentConstraint.ExcludedCubeRegion.KeyValues.Select(x=>x.Id));
                allComponents.AddRange(contentConstraint.ExcludedCubeRegion.AttributeValues.Select(x => x.Id));
            }
            if (contentConstraint.IncludedCubeRegion != null)
            {
                allComponents.AddRange(contentConstraint.IncludedCubeRegion.KeyValues.Select(x=>x.Id));
                allComponents.AddRange(contentConstraint.IncludedCubeRegion.AttributeValues.Select(x => x.Id));
            }

            return allComponents;
        }

        private List<IContentConstraintMutableObject> GetCrossReferencingContentConstraint(IStructureReference structure)
        {
            var contentConstraints = new List<IContentConstraintMutableObject>();

            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
               .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
               .SetStructureIdentification(structure)
               .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
               .Build();

            var _retrievalEng = retrievalEngineContainer.GetEngine(structure.MaintainableStructureEnumType);
            var structureReferenceList = _retrievalEng.RetrieveResolvedParents(structureQuery).ToList();

            foreach (var struRef in structureReferenceList)
            {
                if (struRef.MaintainableStructureEnumType == SdmxStructureEnumType.ContentConstraint) 
                {
                    //getting the Content Constraints
                    ICommonStructureQuery structureQueryCC = CommonStructureQueryCore.Builder
                       .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                       .SetStructureIdentification(struRef)
                       .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                       .Build();

                    var maintainableObjects = retrievalEngineContainer.GetEngine(struRef.MaintainableStructureEnumType).Retrieve(structureQueryCC);

                    foreach (var obj in maintainableObjects) {
                        contentConstraints.Add(obj.ImmutableInstance.MutableInstance as IContentConstraintMutableObject);
                    }

                }
            }

            return contentConstraints;
        }

        /// <summary>
        /// Checks if the two given content constraints should be checked for duplication
        /// </summary>
        /// <param name="maintainable">The existing cc</param>
        /// <param name="contentConstraint">The cc to insert</param>
        /// <returns><c>true</c> if should be checked.</returns>
        private bool ShouldCheckContentConstraint(IContentConstraintMutableObject maintainable, IContentConstraintMutableObject contentConstraint)
        {
            return contentConstraint.IsDefiningActualDataPresent == maintainable.IsDefiningActualDataPresent &&
              IsOverlapping(maintainable, contentConstraint) &&
			  (maintainable.AgencyId != contentConstraint.AgencyId || maintainable.Id != contentConstraint.Id || maintainable.Version != contentConstraint.Version);
        }

        /// <summary>
        /// Checks if the two given content constraints are overlapping for the same time period.
        /// </summary>
        /// <param name="maintainable">The existing content constraint.</param>
        /// <param name="contentConstraint">The cc to insert</param>
        /// <returns><c>true</c> if overlapping</returns>
        public bool IsOverlapping(IContentConstraintMutableObject maintainable, IContentConstraintMutableObject contentConstraint)
        {
            // supposing that a cc's start date is always before the end date
            if (maintainable.EndDate < contentConstraint.StartDate || maintainable.StartDate > contentConstraint.EndDate)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Inserts the data sources.
        /// </summary>
        /// <param name="state">The state.</param>.
        /// <param name="maintainable">The maintainable.</param>
        /// <param name="artefactStatus">The artefact status.</param>
        private void InsertDataSources(DbTransactionState state, IContentConstraintObject maintainable,
            ArtefactImportStatus artefactStatus)
        {
            if (maintainable.ConstraintAttachment == null || !maintainable.ConstraintAttachment.DataSources.Any())
            {
                return;
            }

            using (var command = state.Connection.CreateCommand())
            {
                command.Transaction = state.Transaction;
                foreach (var dataSource in maintainable.ConstraintAttachment.DataSources)
                {
                    var dataSourceId = _dataSourceImportEngine.Insert(state, dataSource);
                    var contentIdParam = state.Database.CreateInParameter("contentIdParam", DbType.Int64,
                        artefactStatus.PrimaryKeyValue);
                    var dataSourceIdParam =
                        state.Database.CreateInParameter("dataSourceIdParam", DbType.Int64, dataSourceId);

                    state.ExecuteNonQueryFormat("Insert into CONTENT_CONSTRAINT_SOURCE values ({0},{1})",
                        contentIdParam, dataSourceIdParam);
                }
            }
        }

        private ContentConstraintAttachType GetAttachType(IContentConstraintObject contentConstraintObject)
        {
            if (contentConstraintObject.ConstraintAttachment.DataOrMetadataSetReference != null)
            {
                if (contentConstraintObject.ConstraintAttachment.DataOrMetadataSetReference.IsDataSetReference)
                {
                    return ContentConstraintAttachType.DataSet;
                }

                return ContentConstraintAttachType.MetadataSet;
            }


            if (contentConstraintObject.ConstraintAttachment.StructureReference != null &&
                     contentConstraintObject.ConstraintAttachment.StructureReference.Count > 0)
            {
                var crossReference = contentConstraintObject.ConstraintAttachment.StructureReference.First();
                ContentConstraintAttachType attachType;
                if (Enum.TryParse(crossReference.TargetReference.UrnClass, true, out attachType))
                {
                    return attachType;
                }
            }


            if (contentConstraintObject.ConstraintAttachment.DataSources != null &&
                contentConstraintObject.ConstraintAttachment.DataSources.Count == 1)
            {
                return ContentConstraintAttachType.SimpleDataSource;
            }

            throw new SdmxSemmanticException("No attachment found");
        }

        /// <summary>
        ///     Inserts the cube region.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="cubeRegion">The cube region.</param>
        /// <param name="artefactStatus">The artefact status.</param>
        /// <param name="isInclude">if set to <c>true</c> [is include].</param>
        private static void InsertCubeRegion(DbTransactionState state, ICubeRegion cubeRegion,
            ArtefactImportStatus artefactStatus, bool isInclude)
        {
            var procedure = new InsertCubeRegionProcedure();
            long cubeRegionPrimaryKey;
            using (var command = procedure.CreateCommand(state))
            {
                procedure.CreateContentConstraintIdParameter(command, artefactStatus.PrimaryKeyValue);
                procedure.CreateIncludeParameter(command, isInclude);
                var outputParameter = procedure.CreateOutputParameter(command);
                command.ExecuteNonQueryAndLog();
                cubeRegionPrimaryKey = (long)outputParameter.Value;
            }

            // handle dimensions
            var keyValuesWithId = InsertKeyValues(state, isInclude, cubeRegionPrimaryKey, cubeRegion.KeyValues,
                SdmxComponentType.Dimension);

            InsertValues(state, isInclude, keyValuesWithId);

            // handle attributes
            var attrValuesWithId = InsertKeyValues(state, isInclude, cubeRegionPrimaryKey, cubeRegion.AttributeValues,
                SdmxComponentType.Attribute);

            InsertValues(state, isInclude, attrValuesWithId);
        }

        /// <summary>
        ///     Inserts the key values.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="isInclude">if set to <c>true</c> [is include].</param>
        /// <param name="cubeRegionPrimaryKey">The cube region primary key.</param>
        /// <param name="keyValuesCollection">The key values collection.</param>
        /// <param name="sdmxComponentType">Type of the SDMX component.</param>
        /// <returns>
        ///     The list of primary key, <see cref="IKeyValues" /> and <see cref="IComponent" />
        /// </returns>
        private static IEnumerable<Tuple<long, IKeyValues>> InsertKeyValues(DbTransactionState state, bool isInclude,
            long cubeRegionPrimaryKey, ICollection<IKeyValues> keyValuesCollection, SdmxComponentType sdmxComponentType)
        {
            var dimensionType = sdmxComponentType.ToString();
            var keyValuesWithId = new List<Tuple<long, IKeyValues>>(keyValuesCollection.Count);
            var keyValueProc = new InsertCubeRegionKeyValueProcedure();
            using (var command = keyValueProc.CreateCommandWithDefaults(state))
            {
                keyValueProc.CreateIncludeParameter(command, isInclude);
                keyValueProc.CreateCubeRegionIdParameter(command, cubeRegionPrimaryKey);
                foreach (var keyValues in keyValuesCollection)
                {
                    keyValueProc.CreateMemberIdParameter(command, keyValues.Id);
                    keyValueProc.CreateComponentTypeParameter(command, dimensionType);
                    if (keyValues.TimeRange != null) {
                        bool allValuesProvided = (keyValues.TimeRange.StartDate != null && keyValues.TimeRange.EndDate != null);
                        bool onlyStartValueProvided = (keyValues.TimeRange.StartDate != null && keyValues.TimeRange.EndDate == null);
                        bool onlyEndValueProvided = (keyValues.TimeRange.StartDate == null && keyValues.TimeRange.EndDate != null);
                        if (allValuesProvided) {
                            keyValueProc.CreateStartPeriodParameter(command).Value = keyValues.TimeRange.StartDate.DateInSdmxFormat;
                            keyValueProc.CreateEndPeriodParameter(command).Value = keyValues.TimeRange.EndDate.DateInSdmxFormat;
                            keyValueProc.CreateStartInclusiveParameter(command, keyValues.TimeRange.StartInclusive);
                            keyValueProc.CreateEndInclusiveParameter(command, keyValues.TimeRange.EndInclusive);
                        }
                        else if (onlyStartValueProvided) {
                            keyValueProc.CreateStartPeriodParameter(command).Value = keyValues.TimeRange.StartDate.DateInSdmxFormat;
                            keyValueProc.CreateStartInclusiveParameter(command, keyValues.TimeRange.StartInclusive);
                        }
                        else if (onlyEndValueProvided)
                        {
                            keyValueProc.CreateEndPeriodParameter(command).Value = keyValues.TimeRange.EndDate.DateInSdmxFormat;
                            keyValueProc.CreateEndInclusiveParameter(command, keyValues.TimeRange.EndInclusive);
                        }
                    }
                    else
                    {
                        keyValueProc.CreateEndInclusiveParameter(command, true);
                        keyValueProc.CreateStartInclusiveParameter(command, true);
                        keyValueProc.CreateEndPeriodParameter(command).Value = DBNull.Value;
                        keyValueProc.CreateStartPeriodParameter(command).Value = DBNull.Value;
                    }
                    var outputParameter = keyValueProc.CreateOutputParameter(command);
                    command.ExecuteNonQueryAndLog();
                    keyValuesWithId.Add(new Tuple<long, IKeyValues>((long)outputParameter.Value, keyValues));
                }
            }

            return keyValuesWithId;
        }

        /// <summary>
        ///     Inserts the values.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="isInclude">if set to <c>true</c> [is include].</param>
        /// <param name="keyValuesWithId">The key values with identifier.</param>
        private static void InsertValues(DbTransactionState state, bool isInclude,
            IEnumerable<Tuple<long, IKeyValues>> keyValuesWithId)
        {
            // Oracle doesn't support multiple values sets.
            var maxValueSets = state.Database.ProviderName.ToUpperInvariant().Contains("ORACLE") ? 1 : 1000;
            const string strCommand = @"INSERT INTO CUBE_REGION_VALUE 
                                (CUBE_REGION_KEY_VALUE_ID, MEMBER_VALUE, INCLUDE,CASCADE_VALUES)
                                VALUES";

            using (var command = state.Connection.CreateCommand())
            {
                var dbParameters = new List<DbParameter>();
                var strParametersList = new List<string>();
                var query = string.Empty;

                var i = 0;
                foreach (var (cubeRegionKeyValueId, cubeRegionKeyValue) in keyValuesWithId)
                {
                    foreach (var value in cubeRegionKeyValue.Values)
                    {
                        var includeParam = state.Database.CreateInParameter($"p_include_{i}", DbType.Int32, isInclude ? 1 : 0);
                        dbParameters.Add(includeParam);

                        var cubeRegionKeyValueIdParam = state.Database.CreateInParameter($"p_cube_region_key_value_id_{i}", DbType.Int64, cubeRegionKeyValueId);
                        dbParameters.Add(cubeRegionKeyValueIdParam);

                        var memberValueParam = state.Database.CreateInParameter($"p_member_value_{i}", DbType.AnsiString, value);
                        dbParameters.Add(memberValueParam);

                        var cascadeParam = state.Database.CreateInParameter($"p_cascade_values{i}", DbType.Int32, cubeRegionKeyValue.IsCascadeValue(value) ? 1 : 0);
                        dbParameters.Add(cascadeParam);

                        strParametersList.Add($"({cubeRegionKeyValueIdParam.ParameterName},{memberValueParam.ParameterName},{includeParam.ParameterName},{cascadeParam.ParameterName})");
                        i++;

                        //Avoid Sql limitation of 1000 row values in a INSERT statement."
                        //Avoid Sql limitation of 2100 DbParameters."
                        if (strParametersList.Count < maxValueSets && dbParameters.Count < (2100 - 3))
                        {
                            continue;
                        }

                        command.Transaction = state.Transaction;
                        query = strCommand + string.Join(",\n", strParametersList);
                        state.ExecuteNonQueryFormat(query, dbParameters.ToArray());

                        //Reset parameters
                        dbParameters = new List<DbParameter>();
                        strParametersList = new List<string>();
                        i = 0;
                    }
                }

                if (strParametersList.Count <= 0)
                {
                    return;
                }

                //Process any remaining values or when they are less than 1000
                command.Transaction = state.Transaction;
                query = strCommand + string.Join(",\n", strParametersList);
                state.ExecuteNonQueryFormat(query, dbParameters.ToArray());
            }
        }



        #endregion
    }
}
