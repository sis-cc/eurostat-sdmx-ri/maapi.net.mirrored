// -----------------------------------------------------------------------
// <copyright file="CategorisationDeleteEngine.cs" company="EUROSTAT">
//   Date Created : 2017-11-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using System;
using Estat.Sri.MappingStore.Store.Builder;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Builder;
using Estat.Sri.MappingStoreRetrieval.Extensions;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Estat.Sri.MappingStore.Store.Engine.Delete
{
    class CategorisationDeleteEngine
    {
        /// <summary>
        /// The artefact table
        /// </summary>
        private readonly TableInfo _artefactTable;
        private readonly TableInfo _annotationTable;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategorisationDeleteEngine"/> class.
        /// </summary>
        public CategorisationDeleteEngine()
        {
             var tableInfoBuilder = new TableInfoBuilder();
            var annotationTableBuilder = new AnnotationRelationInfoBuilder();
            var categorisation = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation);
            _artefactTable = categorisation.GetParentTable();
            _annotationTable = annotationTableBuilder.Build(categorisation);
        }

        /// <summary>
        /// Deletes the by categories.
        /// </summary>
        /// <param name="state">The database.</param>
        /// <param name="categoryId">The categories.</param>
        /// <returns>The number of records affected</returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="state"/> is <c>null</c>.</exception>
        public int DeleteByCategories(DbTransactionState state, long[] categories)
        {
            if (state == null)
            {
                throw new ArgumentNullException(nameof(state));
            }

            int count = 0;

            //var categoryParam = state.Database.CreateInParameter(nameof(categoryId), DbType.Int64, categoryId);
            //var categorisationId = state.Database.UsingLogger().ExecuteScalarFormat("SELECT SOURCE_ARTEFACT FROM REF_CHILDREN WHERE ART_ID = {0}", categoryParam) as long?;

            //if (categorisationId.HasValue)
            //{
            //    count += new CategorisationImportEngine(state.Database).Delete(state, categorisationId.Value);
            //}
            return count;
        }

        internal int HandleCategorisations(DbTransactionState state, HashSet<long> removedCategories, List<KeyValuePair<string, string>> movedCategories, ArtefactFinalStatus finalStatus, Dictionary<string, long> dbCategoryIdToLong)
        {
            int count = 0;
            
            List<Tuple<long, long,string>> dbDetails = new List<Tuple<long, long,string>>();
            var categoryParam = state.Database.CreateInParameter("categoryId", DbType.Int64, finalStatus.PrimaryKey);
            //first we need to see if there is a categorisation connected to the parent(categoryScheme) of the categories that are moved or removed
            using (var dbCommand = state.Database.GetSqlStringCommandFormat("SELECT STR_REF_ID,SOURCE_ARTEFACT,TARGET_CHILD_FULL_ID FROM REF_CHILDREN WHERE ART_ID = {0}",categoryParam))
            using (var dataReader = state.Database.ExecuteReader(dbCommand))
            {
                var categoryFullIdInDb = string.Empty;
                long categorisationId = -1;
                long id = -1;
                while (dataReader.Read())
                {
                    categoryFullIdInDb = DataReaderHelper.GetString(dataReader, "TARGET_CHILD_FULL_ID");
                    categorisationId = DataReaderHelper.GetInt64(dataReader, "SOURCE_ARTEFACT");
                    id = DataReaderHelper.GetInt64(dataReader, "STR_REF_ID");
                    dbDetails.Add(Tuple.Create(categorisationId, id, categoryFullIdInDb));

                }
            }

            foreach (var detail in dbDetails)
            {
                //if there is no connection then we are good
                if (!string.IsNullOrEmpty(detail.Item3))
                {
                    //if we have any categories removed then check if the categories removed are linked to the categorisation
                    if (removedCategories.Any())
                    {
                        foreach (var categoryDbId in removedCategories)
                        {
                            var removedFullId = dbCategoryIdToLong.First(x => x.Value == categoryDbId).Key;
                            //if there is a connection to a removed category we then need to delete the categorisation
                            if (removedFullId == detail.Item3)
                            {
                                count += new CategorisationImportEngine(state.Database).Delete(state, detail.Item1, null);
                            }
                        }
                    }

                    if (movedCategories.Any())
                    {
                        foreach (var categoryPair in movedCategories)
                        {
                            //if there is a connection to a removed category we then need to delete the categorisation
                            if (categoryPair.Value == detail.Item3)
                            {
                                var idParam = state.Database.CreateInParameter("id", DbType.Int64, detail.Item2);
                                var categoryFullPathParam = state.Database.CreateInParameter("categoryFullPath", DbType.String, categoryPair.Key);
                                count += state.UsingLogger().ExecuteNonQueryFormat("UPDATE STRUCTURE_REF SET TARGET_CHILD_FULL_ID = {0} WHERE STR_REF_ID = {1}", categoryFullPathParam, idParam);
                            }
                        }
                    }
                }
            }
            

            return count;
        }
    }
}
