// -----------------------------------------------------------------------
// <copyright file="GlobalSuppressions.cs" company="EUROSTAT">
//   Date Created : 2016-03-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member", Target = "Estat.Sri.MappingStore.Store.Engine.ArtefactBaseEngine.#RunIdentifiableArterfactCommand(Org.Sdmxsource.Sdmx.Api.Model.Objects.Base.IIdentifiableObject,System.Data.Common.DbCommand,Estat.Ma.Model.StoredProcedure.ArtefactProcedurebase)", Justification = "It is ok. It just wraps existing connection and transaction. TODO redesign so DbTransactionState is not needed.")]
[assembly: SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Scope = "member", Target = "Estat.Sri.MappingStore.Store.Helper.DefaultEngineHelper.#.cctor()", Justification = "It is ok. We want to map all supported SDMX artefacts to a the corresponding engine.")]
[assembly: SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member", Target = "Estat.Sri.MappingStore.Store.Helper.DefaultEngineHelper.#.cctor()", Justification = "It is OK. Lambdas increase the complexity")]
[assembly: SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Scope = "member", Target = "Estat.Sri.MappingStore.Store.Engine.MaintainableRefRetrieverEngine.#RetrievesUrnToSysIdMapWhenMaintainable(Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureType)", Justification = "It is OK.")]
[assembly: SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Scope = "member", Target = "Estat.Sri.MappingStore.Store.Engine.MaintainableRefRetrieverEngine.#RetrievesUrnToSysIdMapWhenNotMaintable(Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureType)", Justification = "IT is OK")]
[assembly: SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Scope = "member", Target = "Estat.Sri.MappingStore.Store.Engine.MaintainableRefRetrieverEngine.#RetrievesUrnToSysIdMap(Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureType)", Justification = "It is OK")]
[assembly: SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member", Target = "Estat.Sri.MappingStore.Store.Engine.MaintainableRefRetrieverEngine.#RetrievesUrnToSysIdMap(Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureType)", Justification = "It is OK.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member", Target = "Estat.Sri.MappingStore.Store.Engine.MaintainableRefRetrieverEngine.#RetrieveItemUrnMapWithParent(Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureType,System.String)")]
