// -----------------------------------------------------------------------
// <copyright file="KeysConnectionStringBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-07-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.MySql.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// The keys connection string builder. 
    /// This class is a workaround since <c>MySql Connector/.NET</c> <see cref="DbConnectionStringBuilder.Keys"/> doesn't return any values at least when <seealso cref="DbConnectionStringBuilder.ConnectionString"/> is not set.
    /// So it will try to get connection string keys either from driver from an internal field 
    /// or via the a hard-coded list from <c>MySql </c> Documentation
    /// </summary>
    internal class KeysConnectionStringBuilder
    {
        /// <summary>
        /// The hard coded keys from <c>MySql</c> Connector/.NET documentation. Not all values were taken
        /// </summary>
        /// <seealso href="https://dev.mysql.com/doc/connector-net/en/connector-net-connection-options.html"/>
        private static readonly string[] _hardCodedKeys =
            {
                "Allow Batch", "Allow User Variables", "Allow Zero Datetime",
                "Auto Enlist", "BlobAsUTF8ExcludePattern",
                "BlobAsUTF8IncludePattern", "Certificate File",
                "Certificate Password", "Certificate Store Location",
                "Certificate Thumbprint", "CharSet", "Check Parameters",
                "Command Interceptors", "Connect Timeout",
                "Default Command Timeout", "Default Table Cache Age",
                "enableSessionExpireCallback", "Encrypt",
                "Exception Interceptors", "Functions Return String", "Host",
                "Ignore Prepare", "Initial Catalog", "Interactive",
                "Integrated Security", "Keep Alive", "Logging", "Password",
                "Persist Security Info", "Pipe Name", "Port",
                "Procedure Cache Size", "Protocol", "Replication	",
                "Respect Binary Flags", "Shared Memory Name",
                "Sql Server Mode", "Table Cache", "Treat BLOBs as UTF8",
                "Treat Tiny As Boolean", "Use Affected Rows",
                "Use Procedure Bodies", "User Id", "Compress",
                "Use Usage Advisor", "Use Performance Monitor",
                "Server" , "Host" , "Data Source" , "DataSource",
                "Database","SSL Key","SSL Cert","SSL CA","SSL Mode"
            };

        /// <summary>
        /// Builds the specified connection string builder.
        /// </summary>
        /// <param name="connectionStringBuilder">The connection string builder.</param>
        /// <returns>The list of keys supported by the MySQL <see cref="DbConnectionStringBuilder"/></returns>
        /// <exception cref="System.ArgumentNullException">connectionStringBuilder is null</exception>
        public ICollection<string> Build(DbConnectionStringBuilder connectionStringBuilder)
        {
            if (connectionStringBuilder == null)
            {
                throw new ArgumentNullException(nameof(connectionStringBuilder));
            }

            // first we try to get the keys from the driver. 
            var driverKeys = connectionStringBuilder.Keys;
            if (driverKeys != null && driverKeys.Count > 0)
            {
                return driverKeys.Cast<string>().ToArray();
            }

            // if this fails, we try to get the keys from the internal field "values"
            var type = connectionStringBuilder.GetType();
            var fieldInfo = type.GetField("values", BindingFlags.NonPublic | BindingFlags.Instance);
            var dictionary = fieldInfo?.GetValue(connectionStringBuilder) as IDictionary<string, object>;
            if (dictionary != null)
            {
                return dictionary.Keys;
            }
            
            // If all fail, then return the hard-coded keys
            return _hardCodedKeys;
        }
    }
}