// -----------------------------------------------------------------------
// <copyright file="MySqlConnectionSettingsBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-04-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Utils.Helper;

namespace Estat.Sri.Plugin.MySql.Builder
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Common;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using log4net;
    using Mapping.MappingStore.Extension;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The MariaDB/MySQL connection settings builder.
    /// </summary>
    internal class MySqlConnectionSettingsBuilder : IConnectionSettingsBuilder
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(MySqlConnectionSettingsBuilder));
         /// <summary>
         /// The user identifier name
         /// </summary>
        private const string UserIdName = "UID";

        /// <summary>
        /// The database property name
        /// </summary>
        private const string DatabasePropertyName = "DATABASE";

        /// <summary>
        /// Recent versions of the drivers seem to 
        /// </summary>
        private const string InitialCatalog = "INITIAL CATALOG";

        /// <summary>
        /// The password name
        /// </summary>
        private const string PasswordName = "PWD";

        /// <summary>
        /// The server name
        /// </summary>
        private const string ServerName = "SERVER";
        private const string DataSource = "DATASOURCE";

        /// <summary>
        /// The port name
        /// </summary>
        private const string PortName = "PORT";

        /// <summary>
        /// The fail-over keys
        /// </summary>
        private static readonly ConcurrentDictionary<string, ICollection<string>> _failOverkeys = new ConcurrentDictionary<string, ICollection<string>>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// The build builder
        /// </summary>
        private readonly Func<string, DbConnectionStringBuilder> _buildBuilder;

        /// <summary>
        /// The provider name
        /// </summary>
        private readonly string _providerName;

        /// <summary>
        /// The keys connection string builder
        /// </summary>
        private readonly KeysConnectionStringBuilder _keysConnectionStringBuilder;

        /// <summary>
        /// The fail over key collection
        /// </summary>
        private readonly ICollection<string> _failOverKeyCollection;

        /// <summary>
        /// Initializes a new instance of the <see cref="MySqlConnectionSettingsBuilder" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="buildBuilder">The build builder.</param>
        public MySqlConnectionSettingsBuilder(string name, Func<string, DbConnectionStringBuilder> buildBuilder)
        {
            _buildBuilder = buildBuilder;
            Name = name;
            _providerName = DatabaseType.GetProviderName(Name);
            _keysConnectionStringBuilder = new KeysConnectionStringBuilder();
            _failOverKeyCollection = _failOverkeys.GetOrAdd(_providerName, s => _keysConnectionStringBuilder.Build(buildBuilder(s)));
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        private string Name { get; }

        /// <summary>
        /// Creates the connection entity.
        /// </summary>
        /// <returns>
        /// The connection entity
        /// </returns>
        public IConnectionEntity CreateConnectionEntity()
        {
            return CreateConnectionEntity((ConnectionStringSettings)null);
        }

        /// <summary>
        /// Creates the connection entity from the specified <see cref="T:System.Configuration.ConnectionStringSettings" />
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sri.Mapping.Api.Model.IConnectionEntity" />
        /// </returns>
        public IConnectionEntity CreateConnectionEntity(ConnectionStringSettings settings)
        {
            var builder = GetConnectionStringBuilder(_providerName);
            var connectionEntity = BuildConnectionEntity(builder, settings);

            return connectionEntity;
        }

        /// <summary>
        /// Creates the connection string.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>
        /// The <see cref="T:System.Configuration.ConnectionStringSettings" />
        /// </returns>
        public ConnectionStringSettings CreateConnectionString(IConnectionEntity connection)
        {
            var builder = GetConnectionStringBuilder(_providerName);
            if (builder == null)
            {
                throw new InvalidOperationException("Cannot build DbConnectionStringBuilder for provider: " + _providerName);
            }

            foreach (var connectionParameterEntity in connection.Settings)
            {
                if (!connectionParameterEntity.Value.IsItDefault())
                {
                    builder.Add(connectionParameterEntity.Key, connectionParameterEntity.Value.Value);
                }
            }

            return new ConnectionStringSettings(connection.Name, builder.ConnectionString, _providerName);
        }

        /// <summary>
        /// Creates the connection string.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>The <see cref="ConnectionStringSettings"/></returns>
        public ConnectionStringSettings CreateConnectionString(DdbConnectionEntity connection)
        {
            return new ConnectionStringSettings(connection.Name, connection.AdoConnString, _providerName);
        }

        /// <summary>
        /// Creates the DDB connection entity.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>The <see cref="DdbConnectionEntity"/></returns>
        public DdbConnectionEntity CreateDdbConnectionEntity(IConnectionEntity connection)
        {
            if (connection == null)
            {
                throw new ArgumentNullException(nameof(connection));
            }

            if (string.IsNullOrEmpty(connection.Name))
            {
                throw new SdmxException("Ddb connection name can not be empty", SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError));
            }

            DdbConnectionEntity connectionEntity = new DdbConnectionEntity();
            connectionEntity.Name = connection.Name;
            connectionEntity.EntityId = connection.EntityId;
            connectionEntity.DbType = connection.DatabaseVendorType;
            connectionEntity.AdoConnString = this.CreateConnectionString(connection).ConnectionString;
            var dbName = connection.DbName ?? connection.GetValue<string>(DatabasePropertyName) ?? connection.GetValue<string>(InitialCatalog);
            connectionEntity.DbUser = connection.GetValue<string>(UserIdName);
            connectionEntity.Password = connection.GetValue<string>(PasswordName);
            var server = connection.GetValue<string>(ServerName) ?? connection.GetValue<string>(DataSource);
            var port = connection.GetValueAsString(PortName, 3306);
            connectionEntity.JdbcConnString = JdbcConnectionStringHelper.GenerateJdbcString(connectionEntity.DbType, dbName, server, port);
            return connectionEntity;
        }

        /// <summary>
        /// Creates the connection entity.
        /// </summary>
        /// <param name="ddbConnection">The DDB connection.</param>
        /// <returns>The <see cref="IConnectionEntity"/></returns>
        public IConnectionEntity CreateConnectionEntity(DdbConnectionEntity ddbConnection)
        {
            if (ddbConnection == null)
            {
                throw new ArgumentNullException(nameof(ddbConnection));
            }

            if (string.IsNullOrEmpty(ddbConnection.Name))
            {
                throw new SdmxException("Ddb connection name can not be empty", SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError));
            }


            var providerName = DatabaseType.GetProviderName(ddbConnection.DbType);
            var connectionEntity = this.CreateConnectionEntity(
              new ConnectionStringSettings(ddbConnection.Name, ddbConnection.AdoConnString, providerName));
            connectionEntity.EntityId = ddbConnection.EntityId;
            connectionEntity.Name = ddbConnection.Name;
            connectionEntity.Description = ddbConnection.Description;
            connectionEntity.StoreId = ddbConnection.StoreId;
            return connectionEntity;
        }

        /// <summary>
        /// Gets the connection string builder.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <returns>The <see cref="DbConnectionStringBuilder"/></returns>
        private DbConnectionStringBuilder GetConnectionStringBuilder(string providerName)
        {
            return _buildBuilder(providerName);
        }

        /// <summary>
        /// Builds the connection entity.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="settings">The settings.</param>
        /// <returns>The <see cref="IConnectionEntity"/></returns>
        private IConnectionEntity BuildConnectionEntity(DbConnectionStringBuilder builder, ConnectionStringSettings settings)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            IConnectionEntity connectionEntity = new ConnectionEntity();
            connectionEntity.DatabaseVendorType = Name;
            var existingSettings = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            if (settings != null)
            {
                connectionEntity.Name = settings.Name;
                try
                {
                    builder.ConnectionString = settings.ConnectionString;
                }
                catch (Exception e)
                {
                    _logger.Error($"Trying to set connection string {settings.ConnectionString} failed", e);
                }
                existingSettings.UnionWith(builder.Cast<KeyValuePair<string, object>>().Select(pair => pair.Key));
            }

            foreach (string originalKey in _failOverKeyCollection)
            {
                var key = originalKey.ToUpperInvariant();

                object entry;
                if (!builder.TryGetValue(originalKey, out entry))
                {
                    entry = null;
                }

                var value = entry?.ToString().Trim();
                switch (key)
                {
                    case UserIdName:
                    case "USER ID":
                    case "USERID":
                        connectionEntity.AddSetting(originalKey, ParameterType.String, value, required: true);
                        break;
                    case DatabasePropertyName:
                    case InitialCatalog:
                        if (!connectionEntity.Settings.ContainsKey(DatabasePropertyName.ToLowerInvariant()) ||
                            connectionEntity.Settings[DatabasePropertyName.ToLowerInvariant()].Value == null ||
                            string.IsNullOrEmpty(connectionEntity.Settings[DatabasePropertyName.ToLowerInvariant()].Value.ToString()))
                        { connectionEntity.DbName = value;

                            // Try to avoid issues with MAWEB
                            connectionEntity.AddSetting(DatabasePropertyName.ToLowerInvariant(), ParameterType.String, value, required: true);
                        }
                        break;
                    case PasswordName:
                    case "PASSWORD":
                        connectionEntity.AddSetting(originalKey, ParameterType.Password, value, required: true);

                        break;
                    case ServerName:
                    case DataSource:
                    case "DATA SOURCE":
                    case "HOST":
                        //if server is already set,skip
                        if (!connectionEntity.Settings.ContainsKey(ServerName.ToLowerInvariant()) ||
                            connectionEntity.Settings[ServerName.ToLowerInvariant()].Value == null ||
                            string.IsNullOrEmpty(connectionEntity.Settings[ServerName.ToLowerInvariant()].Value.ToString())) {
                            if (value != null)
                            {
                                var serverValues = value.Split(':');
                                // Try to avoid issues with MAWEB
                                connectionEntity.AddSetting(ServerName.ToLowerInvariant(), ParameterType.String, serverValues.FirstOrDefault(), required: true);
                                if (serverValues.Count() > 1)
                                {
                                    if (int.TryParse(serverValues.LastOrDefault(), out var port))
                                    {
                                        connectionEntity.AddSetting(PortName, ParameterType.Number, port);
                                    }
                                }
                            }
                            else
                            {
                                connectionEntity.AddSetting(ServerName.ToLowerInvariant(), ParameterType.String, value, required: true);
                            }
                        }
                        break;
                    case PortName:
                        //the port is taken from the server value above or can be set separetly
                        if (value != null)
                        {
                            connectionEntity.AddSetting(PortName, ParameterType.Number, value);
                        }
                        break;

                    default:
                        connectionEntity.AddSetting(originalKey, value, existingSettings, true);
                        break;
                }
            }

            return connectionEntity;
        }
    }
}
