// -----------------------------------------------------------------------
// <copyright file="MySqlSchemaBrowser.cs" company="EUROSTAT">
//   Date Created : 2017-04-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.MySql.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Linq;

    using Dapper;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// The MySQL/MariaDB schema browser.
    /// </summary>
    public class MySqlSchemaBrowser : IDatabaseSchemaBrowser
    {
        private readonly Func<string, DbConnectionStringBuilder> _getConnectionStringBuilder;

        /// <summary>
        /// The base tables keywords
        /// </summary>
        private static readonly string[] _tablesKeywords = { "BASE_TABLE", "BASE TABLE" };

        /// <summary>
        /// The views keywords
        /// </summary>
        private static readonly string[] _viewsKeywords = { "VIEW" };

        /// <summary>
        /// Initializes a new instance of the <see cref="MySqlSchemaBrowser"/> class.
        /// </summary>
        /// <param name="getConnectionStringBuilder">The get connection string builder.</param>
        public MySqlSchemaBrowser(Func<string, DbConnectionStringBuilder> getConnectionStringBuilder)
        {
            _getConnectionStringBuilder = getConnectionStringBuilder;
        }

        /// <summary>
        /// Gets the database objects. E.g. Tables or Views
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The list of <see cref="T:Estat.Sri.Mapping.Api.Model.DatabaseObject" />
        /// </returns>
        /// <exception cref="System.ArgumentNullException">settings is null</exception>
        public IEnumerable<IDatabaseObject> GetDatabaseObjects(ConnectionStringSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            var database = new Database(settings);
            var databaseName = GetDatabaseName(settings);
            List<IDatabaseObject> databaseObjects = new List<IDatabaseObject>();
            using (var command = database.GetSqlStringCommandFormat("SELECT TABLE_NAME, TABLE_TYPE FROM INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA={0}", database.CreateInParameter("p1", DbType.String, databaseName)))
            using (var reader = database.ExecuteReader(command))
            {
                while (reader.Read())
                {
                    IDatabaseObject databaseObject = BuildDatabaseObject(reader);

                    databaseObjects.Add(databaseObject);
                }
            }

            return databaseObjects;
        }

        /// <summary>
        /// Gets the fields of a database object.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="databaseObject">The database object.</param>
        /// <returns>
        /// The list of <see cref="T:Estat.Sri.Mapping.Api.Model.IFieldInfo" />
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// settings
        /// or
        /// databaseObject
        /// </exception>
        public IEnumerable<IFieldInfo> GetSchema(ConnectionStringSettings settings, IDatabaseObject databaseObject)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            if (databaseObject == null)
            {
                throw new ArgumentNullException(nameof(databaseObject));
            }

            var database = new Database(settings);
            var databaseName = GetDatabaseName(settings);
            var dynamicParameters = new DynamicParameters();
            var tablename = databaseObject.Name;
            dynamicParameters.Add(nameof(tablename), tablename);
            dynamicParameters.Add(nameof(databaseName), databaseName);
            var tableParameterName = database.BuildParameterName(nameof(tablename));
            var schemaParameterName = database.BuildParameterName(nameof(databaseName));
            return database.Query<FieldInfo>($"SELECT COLUMN_NAME as {nameof(IFieldInfo.Name)}, DATA_TYPE as {nameof(IFieldInfo.DataType)} FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME={tableParameterName} and TABLE_SCHEMA={schemaParameterName}", dynamicParameters);
        }

        /// <summary>
        /// Creates a new connection.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>The <see cref="DbConnection"/></returns>
        public DbConnection CreateConnection(ConnectionStringSettings settings)
        {
            return new Database(settings).CreateConnection();
        }

        /// <summary>
        /// Builds the database object.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns>The <see cref="IDatabaseObject"/></returns>
        private static IDatabaseObject BuildDatabaseObject(IDataReader reader)
        {
            var tableType = DataReaderHelper.GetString(reader, 1);
            var isTable = _tablesKeywords.Contains(tableType, StringComparer.OrdinalIgnoreCase);
            var objectType = isTable
                                 ? DatabaseObjectType.Table
                                 : _viewsKeywords.Contains(tableType, StringComparer.OrdinalIgnoreCase)
                                       ? DatabaseObjectType.View
                                       : DatabaseObjectType.Unknown;
            return new DatabaseObject() { Name = DataReaderHelper.GetString(reader, 0), ObjectType = objectType };
        }

        /// <summary>
        /// Gets the connection string builder.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <returns>The <see cref="DbConnectionStringBuilder"/></returns>
        private DbConnectionStringBuilder GetConnectionStringBuilder(string providerName)
        {
            return _getConnectionStringBuilder(providerName);
        }

        /// <summary>
        /// Gets the name of the database.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>The database name</returns>
        private object GetDatabaseName(ConnectionStringSettings settings)
        {
            var builder = GetConnectionStringBuilder(settings.ProviderName);
            builder.ConnectionString = settings.ConnectionString;
            var databaseName = builder["database"];
            return databaseName;
        }
    }
}