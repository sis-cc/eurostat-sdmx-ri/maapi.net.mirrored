// -----------------------------------------------------------------------
// <copyright file="LocalCodeRetrieverManager.cs" company="EUROSTAT">
//   Date Created : 2017-04-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using log4net;

    /// <summary>
    /// The local code retriever manager.
    /// </summary>
    public class LocalCodeRetrieverManager : ILocalCodeRetrieverManager
    {
        /// <summary>
        /// The logger
        /// </summary>
        private readonly ILog _log = LogManager.GetLogger(typeof(LocalCodeRetrieverManager));

        /// <summary>
        /// The _database provider manager.
        /// </summary>
        private readonly IDatabaseProviderManager _databaseProviderManager;

        /// <summary>
        /// The factories
        /// </summary>
        private readonly IList<ILocalCodeRetrievalFactory> _factories;

        /// <summary>
        /// The _retriever manager.
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalCodeRetrieverManager" /> class.
        /// </summary>
        /// <param name="databaseProviderManager">The database provider manager.</param>
        /// <param name="retrieverManager">The retriever manager.</param>
        /// <param name="factories">The factories.</param>
        public LocalCodeRetrieverManager(
            IDatabaseProviderManager databaseProviderManager,
            IEntityRetrieverManager retrieverManager,
            params ILocalCodeRetrievalFactory[] factories)
        {
            _databaseProviderManager = databaseProviderManager;
            _retrieverManager = retrieverManager;
            _factories = factories.ToArray();
        }

        /// <summary>
        /// Gets the number of rows.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <returns>The number of rows</returns>
        public int GetCount(ILocalDataQuery dataQuery)
        {
            var ddbConnectionEntity = GetDdbConnectionSettings(
              dataQuery.MappingStoreId,
              dataQuery.DisseminationDatabaseSettingEntityId);
            var engine = GetEngine(ddbConnectionEntity);
            var connectionStringSettings = GetConnectionStringSettings(ddbConnectionEntity);

            var query = dataQuery.Query ?? dataQuery.Table;
            return engine.GetCount(connectionStringSettings, query);
        }

        /// <summary>
        /// Return a list of codes (may contain duplicates) from one or more columns in a table or a query
        /// </summary>
        /// <param name="dataQuery">
        /// The data query.
        /// </param>
        /// <returns>
        /// A list of codes
        /// </returns>
        public QueryResult GetLocalData(ILocalDataQuery dataQuery)
        {
            var ddbConnectionEntity = GetDdbConnectionSettings(
                dataQuery.MappingStoreId,
                dataQuery.DisseminationDatabaseSettingEntityId);
            var engine = GetEngine(ddbConnectionEntity);
            var connectionStringSettings = GetConnectionStringSettings(ddbConnectionEntity);

            var query = dataQuery.Query ?? dataQuery.Table;

            var dataSetColumnCollection = dataQuery.ColumnNames;

            var queryResult = new QueryResult();
            var count = engine.GetCount(connectionStringSettings, query);
            queryResult.TotalRows = count;
            if (count != 0 || dataSetColumnCollection.Count <= 0)
            {
                if (dataSetColumnCollection.Count == 1)
                {
                    // We need different behavior, remove empty values
                    HandleOneDataSetColumnSpecialCase(
                        dataQuery,
                        dataSetColumnCollection,
                        engine,
                        connectionStringSettings,
                        query,
                        queryResult);
                }
                else
                {
                    HandleMultipleColumns(
                        dataQuery,
                        engine,
                        connectionStringSettings,
                        query,
                        dataSetColumnCollection,
                        queryResult);
                }

                queryResult.Page = dataQuery.Page;
            }

            PopulateColumns(dataSetColumnCollection, queryResult);

            return queryResult;
        }

        /// <summary>
        /// Return a list of codes (may contain duplicates) from one or more columns in a table or a query
        /// </summary>
        /// <param name="dataQuery">
        /// The data query.
        /// </param>
        /// <returns>
        /// A list of codes
        /// </returns>
        public CodesByColumnsList GetAvailableCodes(ILocalDataQuery dataQuery)
        {
            var ddbConnectionEntity = GetDdbConnectionSettings(
                dataQuery.MappingStoreId,
                dataQuery.DisseminationDatabaseSettingEntityId);
            var engine = GetEngine(ddbConnectionEntity);
            var connectionStringSettings = GetConnectionStringSettings(ddbConnectionEntity);

            var transcodingCodesList = new CodesByColumnsList();
            var query = dataQuery.Query ?? dataQuery.Table;
            var dataSetColumnCollection = dataQuery.ColumnNames;

            foreach (var column in dataSetColumnCollection)
            {
                transcodingCodesList.Add(column, new List<string>());
            }

            var localCodeList = engine.GetLocalCodeList(
                connectionStringSettings,
                query,
                dataSetColumnCollection,
                dataQuery.Count,
                dataQuery.Page);

            if (dataSetColumnCollection.Count == 1)
            {
                // We need different behavior, remove empty values
                transcodingCodesList[dataSetColumnCollection[0]].AddRange(
                    localCodeList.Select(objects => objects[0])
                        .Skip(dataQuery.Page * dataQuery.Count)
                        .Where(objects => !Convert.IsDBNull(objects) && objects != null)
                        .Select(o => Convert.ToString(o, CultureInfo.InvariantCulture)));
                return transcodingCodesList;
            }

            foreach (var results in localCodeList)
            {
                for (var i = 0; i < dataSetColumnCollection.Count; i++)
                {
                    var column = dataSetColumnCollection[i];
                    var value = results[i];
                    transcodingCodesList[column].Add(!Convert.IsDBNull(value) ? Convert.ToString(value) : string.Empty);
                }
            }

            return transcodingCodesList;
        }

        /// <summary>
        /// Get codes for a set of columns from a table (or select query) from the local DB
        /// </summary>
        /// <param name="dataQuery">
        /// The data query.
        /// </param>
        /// <returns>
        /// A data table with n set of column name,column id and the codes as rows
        /// </returns>
        public LocalCodeResults GetValidCodeSet(ILocalDataQuery dataQuery)
        {
            var ddbConnectionEntity = GetDdbConnectionSettings(
                dataQuery.MappingStoreId,
                dataQuery.DisseminationDatabaseSettingEntityId);
            var engine = GetEngine(ddbConnectionEntity);
            var connectionStringSettings = GetConnectionStringSettings(ddbConnectionEntity);

            var query = dataQuery.Query ?? dataQuery.Table;

            var count = engine.GetCount(connectionStringSettings, query);
            LocalCodeResults rulesResults = new LocalCodeResults() { TotalRowCount = count };

            var columns = dataQuery.ColumnNames;
            rulesResults.MappingStoreId = dataQuery.MappingStoreId;
            rulesResults.RelatedEntities[ddbConnectionEntity.TypeOfEntity] = ddbConnectionEntity.EntityId;
            foreach (var column in columns)
            {
                rulesResults.Columns.Add(new DataSetColumnEntity() { Name = column, StoreId = dataQuery.MappingStoreId });
            }

            if (count == 0)
            {
                return rulesResults;
            }

            var localCodeList = engine.GetLocalCodeList(
              connectionStringSettings,
              query,
              dataQuery.ColumnNames,
              dataQuery.Count,
              dataQuery.Page);
            var errorMessages = rulesResults.ErrorMessages;
            var validator = GetValidator(columns, errorMessages);

            var values = NormalizeLocalCodeResults(validator, localCodeList, columns);
            foreach (var value in values)
            {
                rulesResults.AddCodes(value);
            }

            return rulesResults;
        }

        /// <summary>
        /// Gets the available ordered codes.
        /// </summary>
        /// <param name="dataQuery">
        ///     The data query.
        /// </param>
        /// <returns>
        /// A list of codes
        /// </returns>
        public QueryResult GetAvailableOrderedCodes(ILocalDataQuery dataQuery)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException(nameof(dataQuery));
            }

            if (dataQuery.ColumnNames.Count != 1)
            {
                throw new ArgumentException("It supports one column only");
            }

            var ddbConnectionEntity = GetDdbConnectionSettings(
            dataQuery.MappingStoreId,
            dataQuery.DisseminationDatabaseSettingEntityId);
            var engine = GetEngine(ddbConnectionEntity);
            var connectionStringSettings = GetConnectionStringSettings(ddbConnectionEntity);

            var query = dataQuery.Query ?? dataQuery.Table;

            var columnName = dataQuery.ColumnNames.First();
            var localCodes = engine.GetLocalCodeListOrdered(connectionStringSettings, query, columnName, dataQuery.Count, dataQuery.Page).Where(result => !Convert.IsDBNull(result) && result != null).Select(o => Convert.ToString(o, CultureInfo.InvariantCulture));
            var queryResult = new QueryResult();
            queryResult.Columns.Add(columnName);
            foreach (var localCode in localCodes.Where(s => !string.IsNullOrWhiteSpace(s)))
            {
                queryResult.Rows.Add(new[] { localCode });
            }

            return queryResult;
        }

        /// <summary>
        /// Populates the columns.
        /// </summary>
        /// <param name="dataSetColumnCollection">The data set column collection.</param>
        /// <param name="queryResult">The query result.</param>
        private static void PopulateColumns(IList<string> dataSetColumnCollection, QueryResult queryResult)
        {
            foreach (var column in dataSetColumnCollection)
            {
                queryResult.Columns.Add(column);
            }
        }

        /// <summary>
        /// Handles the multiple columns.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="engine">The engine.</param>
        /// <param name="connectionStringSettings">The connection string settings.</param>
        /// <param name="query">The query.</param>
        /// <param name="dataSetColumnCollection">The data set column collection.</param>
        /// <param name="queryResult">The query result.</param>
        private static void HandleMultipleColumns(
            ILocalDataQuery dataQuery,
            ILocalCodeRetrieval engine,
            ConnectionStringSettings connectionStringSettings,
            string query,
            IList<string> dataSetColumnCollection,
            QueryResult queryResult)
        {
            var localCodeList = engine.GetLocalCodeList(
                connectionStringSettings,
                query,
                dataSetColumnCollection,
                dataQuery.Count,
                dataQuery.Page);

            foreach (var results in localCodeList)
            {
                queryResult.Rows.Add(results.Select(o => !Convert.IsDBNull(o) ? Convert.ToString(o, CultureInfo.InvariantCulture) : string.Empty).ToArray());
            }
        }

        /// <summary>
        /// Handles the one data set column special case.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataSetColumnCollection">The data set column collection.</param>
        /// <param name="engine">The engine.</param>
        /// <param name="connectionStringSettings">The connection string settings.</param>
        /// <param name="query">The query.</param>
        /// <param name="queryResult">The query result.</param>
        private static void HandleOneDataSetColumnSpecialCase(
            ILocalDataQuery dataQuery,
            IList<string> dataSetColumnCollection,
            ILocalCodeRetrieval engine,
            ConnectionStringSettings connectionStringSettings,
            string query,
            QueryResult queryResult)
        {
            var dataSetColumn = dataSetColumnCollection[0];
            var codeList = engine.GetLocalCodeList(connectionStringSettings, query, dataSetColumn, dataQuery.Count, dataQuery.Page);
            var errorMessages = new List<string>();
            ILocalCodeValidator duplicateLocalCodeValidator = new DuplicateLocalCodeValidator(errorMessages);
            ILocalCodeValidator codeValidator = new LocalCodeValidator(errorMessages, 150);
            var codes =
                codeList.Where(objects => !Convert.IsDBNull(objects) && objects != null)
                    .Select(o => Convert.ToString(o, CultureInfo.InvariantCulture));
            foreach (var code in codes)
            {
                if (duplicateLocalCodeValidator.IsValidLocalCode(code, dataSetColumn)
                    && codeValidator.IsValidLocalCode(code, dataSetColumn))
                {
                    queryResult.Rows.Add(new[] { code });
                }
            }
        }

        /// <summary>
        /// Gets the validator.
        /// </summary>
        /// <param name="columns">The columns.</param>
        /// <param name="errorMessages">The error messages.</param>
        /// <returns>
        /// The validator
        /// </returns>
        private static ILocalCodeValidator GetValidator(IList<string> columns, IList<string> errorMessages)
        {
            if (columns.Count > 1)
            {
                /// return new LocalCodeValidator(errorMessages, 150);
                return new NoopValidator();
            }
            else
            {
                // TODO check why we need the duplicate local code validator for . 
                return new ComposedValidator(new DuplicateLocalCodeValidator(errorMessages), new LocalCodeValidator(errorMessages, 150));
            }
        }

        /// <summary>
        /// Normalizes the local code results.
        /// </summary>
        /// <param name="validator">The validator.</param>
        /// <param name="localCodeList">The local code list.</param>
        /// <param name="columns">The columns.</param>
        /// <returns>The <paramref name="localCodeList"/> validated and transformed to <see cref="LocalCodeEntity"/></returns>
        private static IEnumerable<List<LocalCodeEntity>> NormalizeLocalCodeResults(ILocalCodeValidator validator, IEnumerable<object[]> localCodeList, IList<string> columns)
        {
            foreach (var row in localCodeList)
            {
                List<LocalCodeEntity> codes = new List<LocalCodeEntity>();
                Debug.Assert(row.Length == columns.Count, $"row.Length {row.Length} == columns.Count {columns.Count}");

                // If there is one invalid code, meaning a code that we cannot support, then  
                bool invalid = false;
                for (int i = 0; i < columns.Count && !invalid; i++)
                {
                    var value = row[i];
                    invalid = !validator.IsValidLocalCode(value, columns[i]);
                    if (!invalid)
                    {
                        codes.Add(new LocalCodeEntity() { ObjectId = value.ToString() });
                    }
                }

                if (!invalid)
                {
                    yield return codes;
                }
            }
        }

        /// <summary>
        /// Gets the connection string settings.
        /// </summary>
        /// <param name="ddbConnectionEntity">
        /// The DDB connection entity.
        /// </param>
        /// <returns>
        /// The <see cref="ConnectionStringSettings"/>
        /// </returns>
        private ConnectionStringSettings GetConnectionStringSettings(DdbConnectionEntity ddbConnectionEntity)
        {
            return _databaseProviderManager.GetConnectionStringSettings(ddbConnectionEntity);
        }

        /// <summary>
        /// Gets the DDB connection settings.
        /// </summary>
        /// <param name="mappingStoreId">
        /// The mapping store identifier.
        /// </param>
        /// <param name="entityId">
        /// The entity identifier.
        /// </param>
        /// <returns>
        /// The <see cref="DdbConnectionEntity"/>.
        /// </returns>
        private DdbConnectionEntity GetDdbConnectionSettings(string mappingStoreId, string entityId)
        {
            var entityRetrieverEngine = _retrieverManager.GetRetrieverEngine<DdbConnectionEntity>(mappingStoreId);
            var ddbConnectionEntity =
                entityRetrieverEngine.GetEntities(
                    new EntityQuery { EntityId = new Criteria<string>(OperatorType.Exact, entityId) },
                    Detail.Full).FirstOrDefault();
            if (ddbConnectionEntity == null)
            {
                throw new ResourceNotFoundException("Could not locate requested DDB Connection Settings");
            }

            return ddbConnectionEntity;
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="ddbConnectionEntity">
        /// The DDB connection entity.
        /// </param>
        /// <returns>
        /// The <see cref="ILocalCodeRetrieval"/>
        /// </returns>
        private ILocalCodeRetrieval GetEngine(DdbConnectionEntity ddbConnectionEntity)
        {
            foreach (var factory in _factories)
            {
                var engine = factory.GetEngine(ddbConnectionEntity.DbType);
                if (engine != null)
                {
                    return engine;
                }
            }

            throw new NotImplementedException($"Support for {ddbConnectionEntity.DbType} not implemented");
        }

        /// <inherit />
        public void WriteLocalData(ILocalDataQuery dataQuery, ILocalDataWriter writer)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException(nameof(dataQuery));
            }

            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            _log.DebugFormat("Starting {0}", nameof(WriteLocalData));
            _log.DebugFormat("Getting DDB connection with entity ID from Mapping Store {1}", dataQuery.DisseminationDatabaseSettingEntityId, dataQuery.MappingStoreId);
            var ddbConnectionEntity = GetDdbConnectionSettings(
                dataQuery.MappingStoreId,
                dataQuery.DisseminationDatabaseSettingEntityId);
            if (ddbConnectionEntity == null)
            {
                throw new ResourceNotFoundException("Could not find the DDB connection with the specified entity id");
            }

            var engine = GetEngine(ddbConnectionEntity);
           
            var connectionStringSettings = GetConnectionStringSettings(ddbConnectionEntity);
          
            ILocalCodeValidator validator = null;
            IList<string> errorMessages = new List<string>();
            if (dataQuery.CheckForErrors)
            {
                // TODO check why old MA had the duplicate code validator
                // TODO make 150 a config option
                validator = new LocalCodeValidator(errorMessages, 150);            }

            engine.WriteLocalCodeList(connectionStringSettings, dataQuery, writer, validator);
            if (errorMessages.Count > 0)
            {
                writer.WriteErrors(errorMessages);
            }
        }

        /// <inheritdoc/> 
        public void ValidateValues(ILocalDataQuery dataQuery, ILocalCodeValidator validator)
        {
            if (dataQuery is null)
            {
                throw new ArgumentNullException(nameof(dataQuery));
            }

            if (validator is null)
            {
                throw new ArgumentNullException(nameof(validator));
            }

            _log.DebugFormat("Getting DDB connection with entity ID from Mapping Store {1}", dataQuery.DisseminationDatabaseSettingEntityId, dataQuery.MappingStoreId);
            var ddbConnectionEntity = GetDdbConnectionSettings(
                dataQuery.MappingStoreId,
                dataQuery.DisseminationDatabaseSettingEntityId);
            if (ddbConnectionEntity == null)
            {
                throw new ResourceNotFoundException("Could not find the DDB connection with the specified entity id");
            }

            var engine = GetEngine(ddbConnectionEntity);

            var connectionStringSettings = GetConnectionStringSettings(ddbConnectionEntity);
            engine.ValidatedValues(connectionStringSettings, dataQuery, validator);
        }
    }
}