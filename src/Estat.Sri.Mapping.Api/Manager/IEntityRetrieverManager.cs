// -----------------------------------------------------------------------
// <copyright file="IEntityRetrieverManager.cs" company="EUROSTAT">
//   Date Created : 2017-02-02
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// This is the entry point for retrieving entities 
    /// </summary>
    public interface IEntityRetrieverManager
    {
        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>
        /// The matching entities
        /// </returns>
        IEnumerable<TEntity> GetEntities<TEntity>(string storeId, IEntityQuery query, Detail detail) where TEntity : IEntity;

        /// <summary>
        /// Query entities of type <paramref name="entityType"/> on Mapping Store <paramref name="storeId"/> and write using <paramref name="writer"/>
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="writer">The writer</param>
        /// <param name="entityType">The entity type</param>
        /// <param name="query">The entity query</param>
        /// <param name="detail">The level of detail to retun</param>
        void WriteEntities(string storeId, IEntityStreamingWriter writer, EntityType entityType, IEntityQuery query, Detail detail);

        /// <summary>
        /// Gets the retriever engine.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>The <see cref="IEntityRetrieverEngine{TEntity}"/></returns>
        IEntityRetrieverEngine<TEntity> GetRetrieverEngine<TEntity>(string storeId) where TEntity : IEntity;

        /// <summary>
        /// Gets the retriever engine.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// The <see cref="IEntityRetrieverEngine{TEntity}" />
        /// <exception cref="System.ArgumentNullException">storeId is null</exception>
        IEntityRetrieverEngine<IEntity> GetRetrieverEngine(string storeId, EntityType entityType);
    }
}