// -----------------------------------------------------------------------
// <copyright file="EntityAuthorizationManager.cs" company="EUROSTAT">
//   Date Created : 2017-06-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;

    /// <summary>
    /// The entity authorization manager.
    /// </summary>
    public class EntityAuthorizationManager : IEntityAuthorizationManager
    {
        /// <summary>
        /// The factories
        /// </summary>
        private readonly IList<IEntityAuthorizationFactory> _factories;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityAuthorizationManager"/> class.
        /// </summary>
        /// <param name="factories">The factories.</param>
        public EntityAuthorizationManager(params IEntityAuthorizationFactory[] factories)
        {
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories));
            }

            if (factories.Length == 0)
            {
                throw new ArgumentException("No factories provided/injected", nameof(factories));
            }

            this._factories = factories;
        }

        /// <summary>
        /// Determines whether this instance can access the specified entity identifier.
        /// This is normally used by delete or update methods
        /// </summary>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity identifier; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">entityId is null</exception>
        public bool CanAccess(string mappingStoreId, string entityId, EntityType entityType, AccessType accessType)
        {
            if (entityId == null)
            {
                throw new ArgumentNullException(nameof(entityId));
            }

            if (SingleRequestScope.EnsureAuthorization)
            {
                return true;
            }

            if (!SingleRequestScope.ShouldAuthorize(entityType))
            {
                return true;
            }

            var engine = this._factories.Select(factory => factory.GetEngine(entityType)).First(authorizationEngine => authorizationEngine != null);
            return engine.CanAccess(mappingStoreId, entityId, accessType);
        }

        /// <summary>
        /// Determines whether this instance can access the specified entity.
        /// This is used when creating new artefacts
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccess(IEntity entity, AccessType accessType)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (SingleRequestScope.EnsureAuthorization)
            {
                return true;
            }

            if (!SingleRequestScope.ShouldAuthorize(entity.TypeOfEntity))
            {
                return true;
            }

            var engine = this._factories.Select(factory => factory.GetEngine(entity.TypeOfEntity)).First(authorizationEngine => authorizationEngine != null);
            return engine.CanAccess(entity, accessType);
        }

        /// <summary>
        /// Determines whether this instance can access the specified mapping store identifier.
        /// This is used when retrieving entities
        /// </summary>
        /// <typeparam name="TPermissionEntity">The type of the t permission entity.</typeparam>
        /// <param name="entities">The entities.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        /// The entities that it can access
        /// </returns>
        public IEnumerable<TPermissionEntity> CanAccess<TPermissionEntity>(IEnumerable<TPermissionEntity> entities, AccessType accessType) where TPermissionEntity : IEntity
        {
            var arrayOfEntities = entities.ToArray();
            var firstEntity = arrayOfEntities.FirstOrDefault();
            if (firstEntity == null)
            {
                return Enumerable.Empty<TPermissionEntity>();
            }

            if (SingleRequestScope.EnsureAuthorization)
            {
                return arrayOfEntities;
            }

            if (!SingleRequestScope.ShouldAuthorize(firstEntity.TypeOfEntity))
            {
                return arrayOfEntities;
            }

            var engine = this._factories.Select(factory => factory.GetEngine(firstEntity.TypeOfEntity)).First(authorizationEngine => authorizationEngine != null);
            return engine.CanAccess(arrayOfEntities, accessType);
        }
    }
}