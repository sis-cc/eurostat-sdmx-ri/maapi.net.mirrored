﻿// -----------------------------------------------------------------------
// <copyright file="IConfigurationStoreManager.cs" company="EUROSTAT">
//   Date Created : 2017-02-09
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The Configuration Store Manager interface.
    /// </summary>
    public interface IConfigurationStoreManager
    {
        /// <summary>
        /// Gets the settings from a default location.
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <returns>The settings</returns>
        IEnumerable<TSettings> GetSettings<TSettings>();

        /// <summary>
        /// Saves the settings.
        /// </summary>
        /// <typeparam name="TSettings">The type of the t settings.</typeparam>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The <see cref="IActionResult" />.
        /// </returns>
        IActionResult SaveSettings<TSettings>(TSettings settings);

        /// <summary>
        /// Deletes the settings.
        /// </summary>
        /// <typeparam name="TTettings">The type of the settings.</typeparam>
        /// <param name="name">The name.</param>
        /// <returns>
        /// The <see cref="IActionResult" />.
        /// </returns>
        IActionResult DeleteSettings<TTettings>(string name);
    }
}