﻿// -----------------------------------------------------------------------
// <copyright file="MappingStoreManager.cs" company="EUROSTAT">
//   Date Created : 2017-03-20
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;

    /// <summary>
    /// The mapping store manager.
    /// </summary>
    public class MappingStoreManager : IMappingStoreManager
    {
        /// <summary>
        /// The _factories.
        /// </summary>
        private readonly IList<IMappingStoreFactory> _factories;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingStoreManager"/> class.
        /// </summary>
        /// <param name="factories">
        /// The factories.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// No factories were provided
        /// </exception>
        public MappingStoreManager(params IMappingStoreFactory[] factories)
        {
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories));
            }

            _factories = factories.ToArray();
        }

        /// <summary>
        /// Retrieve the available mapping store ids.
        /// </summary>
        /// <returns>
        /// The available mapping ids
        /// </returns>
        public IEnumerable<string> RetrieveStoreIds()
        {
            return
                _factories.Select(factory => factory.GetEngine(null, null))
                    .SelectMany(engine => engine.AvailableStoreId);
        }

        /// <summary>
        /// Retrieve the store details.
        /// </summary>
        /// <returns>
        /// The store details
        public IEnumerable<StoreDetails> RetrieveStoresDetails()
        {
            return
                _factories.Select(factory => factory.GetEngine(null, null))
                    .SelectMany(engine => engine.GetStoresDetails());
        }

        /// <summary>
        /// The get store type.
        /// </summary>
        /// <param name="storeId">
        /// The store id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetStoreType(string storeId)
        {
            return GetEngineByStoreId(storeId)?.StoreType;
        }

        /// <summary>
        /// The get engine by store id.
        /// </summary>
        /// <param name="storeId">
        /// The store id.
        /// </param>
        /// <returns>
        /// The <see cref="IMappingStoreEngine"/>.
        /// </returns>
        public IMappingStoreEngine GetEngineByStoreId(string storeId)
        {
            return
                _factories.Select(factory => factory.GetEngine(null, storeId)).FirstOrDefault(engine => engine != null);
        }

        /// <summary>
        /// The get engine by store type.
        /// </summary>
        /// <param name="storeType">
        /// The store type.
        /// </param>
        /// <returns>
        /// The <see cref="IMappingStoreEngine"/>.
        /// </returns>
        public IMappingStoreEngine GetEngineByStoreType(string storeType)
        {
            return
                _factories.Select(factory => factory.GetEngine(storeType, null))
                    .FirstOrDefault(engine => engine != null);
        }
    }
}