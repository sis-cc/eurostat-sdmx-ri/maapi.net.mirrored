﻿// -----------------------------------------------------------------------
// <copyright file="IDatabaseProviderManager.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System.Collections.Generic;
    using System.Configuration;

    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// Provides DB Vendor specific <see cref="IDatabaseProviderEngine"/>
    /// </summary>
    public interface IDatabaseProviderManager
    {
        /// <summary>
        /// Gets the database provider types.
        /// </summary>
        /// <returns>The available types</returns>
        IEnumerable<string> GetDatabaseProviderTypes();

        /// <summary>
        /// Gets the database provider names
        /// </summary>
        /// <returns>The available provider names</returns>
        IEnumerable<string> GetDatabaseProviderNames();

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <returns>The DB Vendor specific <see cref="IDatabaseProviderEngine"/></returns>
        IDatabaseProviderEngine GetEngineByProvider(string providerName);

        /// <summary>
        /// Gets the type of the engine by.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The DB Vendor specific <see cref="IDatabaseProviderEngine"/></returns>
        IDatabaseProviderEngine GetEngineByType(string type);

        /// <summary>
        /// Gets the connection string settings.
        /// </summary>
        /// <param name="connectionEntity">The connection entity.</param>
        /// <returns>The connection string settings</returns>
        ConnectionStringSettings GetConnectionStringSettings(DdbConnectionEntity connectionEntity);
    }
}