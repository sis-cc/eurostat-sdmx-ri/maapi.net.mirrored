﻿// -----------------------------------------------------------------------
// <copyright file="ICopyManager.cs" company="EUROSTAT">
//   Date Created : 2017-09-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Manager
{
    using System;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The manager responsible for copying entities between mapping stores or even the same mapping store
    /// </summary>
    public interface ICopyManager
    {
        /// <summary>
        /// Copy everything (including SDMX) from the sourceStoreId to targetStoreId
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <returns>
        /// The result of the action
        /// </returns>
        /// <remarks>
        /// Cannot copy user if it exists
        /// Cannot copy header if the dataflow already has a header
        /// Cannot copy MappingSet if the dataflow already has a mapping set
        /// </remarks>
        IActionResult Copy(string sourceStoreId, string targetStoreId);

        /// <summary>
        /// Copy the selected entities from the sourceStoreId to targetStoreId
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="entityType">Type of the entity. The entity can be one following entities
        /// <see cref="EntityType.DdbConnectionSettings" />, <see cref="EntityType.DataSet" /> and <see cref="RegistryEntity" /><see cref="EntityType.MappingSet" />, <see cref="EntityType.User" /> and <see cref="EntityType.Header" /></param>
        /// <param name="entityQuery">The entity query.</param>
        /// <returns>
        /// The result of the action
        /// </returns>
        /// <remarks>
        /// Parents will be copied including SDMX dependencies for Mapping Sets. Some restrictions exist.
        /// Cannot copy user if it exists
        /// Cannot copy header if the dataflow already has a header
        /// Cannot copy MappingSet if the dataflow already has a mapping set
        /// </remarks>
        IActionResult Copy(string sourceStoreId, string targetStoreId, EntityType entityType, IEntityQuery entityQuery);

        /// <summary>
        /// Copy the selected entities within the same store id
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="entityType">Type of the entity. The entity can be one following entities <see cref="EntityType.DdbConnectionSettings" />, <see cref="EntityType.DataSet" /> and <see cref="RegistryEntity" /></param>
        /// <param name="entityQuery">The entity query.</param>
        /// <param name="copyParents">if set to <c>true</c> copy parents. Currently it only applies to <see cref="DatasetEntity" /></param>
        /// <returns>
        /// The result of the action
        /// </returns>
        IActionResult Copy(string storeId, EntityType entityType, IEntityQuery entityQuery, bool copyParents);

        /// <summary>
        /// Copies the header from the <paramref name="sourceDataflowUrn"/> to the <paramref name="targetDataflowUrn"/>.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="sourceDataflowUrn">The source dataflow urn.</param>
        /// <param name="targetDataflowUrn">The target dataflow urn.</param>
        /// <returns>The <see cref="IActionResult"/></returns>
        IActionResult CopyHeader(string storeId, Uri sourceDataflowUrn, Uri targetDataflowUrn);
    }
}