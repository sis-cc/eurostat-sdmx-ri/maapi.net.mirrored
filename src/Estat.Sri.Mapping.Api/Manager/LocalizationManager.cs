// -----------------------------------------------------------------------
// <copyright file="LocalizationManager.cs" company="EUROSTAT">
//   Date Created : 2017-05-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;

    public class LocalizationManager : ILocalizationManager
    {
        private readonly ILocalizationFactory[] _factories;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public LocalizationManager(params ILocalizationFactory[] factories)
        {
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories), "Must provide at least one factory");
            }

            if (factories.Length == 0)
            {
                throw new ArgumentException("Must provide at least one factory", nameof(factories));
            }

            this._factories = factories.ToArray();
        }

        public IList<string> GetLanguages(string mappingStoreId)
        {
            return GetEngine(mappingStoreId).GetLanguages();
        }

        private ILocalizationEngine GetEngine(string mappingStoreIdentifier)
        {
            foreach (var factory in this._factories)
            {
                var engine = factory.GetEngine(mappingStoreIdentifier);
                if (engine != null)
                {
                    return engine;
                }
            }

            throw new NotImplementedException(string.Format(CultureInfo.InvariantCulture, "Could not find an engine for store {0}", mappingStoreIdentifier));
        }
    }
}