﻿using Estat.Sri.Mapping.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Mapping.Api.Manager
{
    public interface IDataSetColumnGeneratorManager
    {
        // get the dataset columns given a storeId, the entityId of a DDB Connection Setting and a SQL Query (there is existing LocalDataQuery model which contains all that)

        IList<DataSetColumnEntity> GetDatasetColumns(LocalDataQuery localDataQuery);


        // get the dataset columns given a dataset. Note it will get the Dataset columns executing the Dataset.Query against the DDB . THis is just a wrapper to the first method

        IList<DataSetColumnEntity> GetDatasetColumns(string storeId, string datasetEntityId);
    }
}
