// -----------------------------------------------------------------------
// <copyright file="DdbManager.cs" company="EUROSTAT">
//   Date Created : 2017-05-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The DDB manager.
    /// </summary>
    public class DdbManager : IDdbManager
    {
        /// <summary>
        /// The database provider manager
        /// </summary>
        private readonly IDatabaseProviderManager _databaseProviderManager;

        /// <summary>
        /// The entity retriever manager
        /// </summary>
        private readonly IEntityRetrieverManager _entityRetrieverManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="DdbManager" /> class.
        /// </summary>
        /// <param name="databaseProviderManager">The database provider manager.</param>
        /// <param name="entityRetrieverManager">The entity retriever manager.</param>
        public DdbManager(IDatabaseProviderManager databaseProviderManager, IEntityRetrieverManager entityRetrieverManager)
        {
            _databaseProviderManager = databaseProviderManager;
            _entityRetrieverManager = entityRetrieverManager;
        }


        /// <summary>
        /// Gets the status of the connection of the DDB.
        /// </summary>
        /// <param name="ddbEntity">
        /// The mapping store identifier.
        /// </param>
        /// <returns>
        /// <see cref="ActionResults.Success"/> if it can connect; 
        /// otherwise if a DDB Connection Settings for the <paramref name="ddbEntity"/> doesn't exist a <see cref="ActionResults.NoResult"/>
        /// otherwise an <see cref="ActionResult"/> with <see cref="DbException"/> error message
        /// </returns>
        public IActionResult TestConnection(IConnectionEntity ddbEntity)
        {
            if (ddbEntity == null)
            {
                throw new ArgumentNullException(nameof(ddbEntity));
            }
            DdbConnectionEntity ddbConnectionEntity = new DdbConnectionEntity();
            ddbConnectionEntity.Name = ddbEntity.Name;
            ddbConnectionEntity.EntityId = ddbEntity.EntityId;
            ddbConnectionEntity.DbType = ddbEntity.DatabaseVendorType;
            var ddbEngine = this._databaseProviderManager.GetEngineByType(ddbConnectionEntity.DbType);
            var connectionStringSettings = ddbEngine.SettingsBuilder.CreateConnectionString(ddbEntity);

            try
            {
                // we need to be careful here as some DDB drivers such as PCAxis are very delicate on how the connection is opened. 
                using (var connection = ddbEngine.Browser.CreateConnection(connectionStringSettings))
                {
                    connection.Open();
                    return ActionResults.Success;
                }
            }
            catch (DbException e)
            {
                return new ActionResult("500", StatusType.Error, new List<Exception> { e }, e.Message);
            }
        }
        /// <summary>
        /// Gets the status of the connection of the DDB.
        /// </summary>
        /// <param name="mappingStoreId">
        /// The mapping store identifier.
        /// </param>
        /// <param name="entityId">
        /// The DDB settings entity identifier.
        /// </param>
        /// <returns>
        /// <see cref="ActionResults.Success"/> if it can connect; 
        /// otherwise if a DDB Connection Settings for the <paramref name="entityId"/> and <paramref name="mappingStoreId"/> doesn't exist a <see cref="ActionResults.NoResult"/>
        /// otherwise an <see cref="ActionResult"/> with <see cref="DbException"/> error message
        /// </returns>
        public IActionResult GetStatus(string mappingStoreId, string entityId)
        {
            if (mappingStoreId == null)
            {
                throw new ArgumentNullException(nameof(mappingStoreId));
            }

            if (entityId == null)
            {
                throw new ArgumentNullException(nameof(entityId));
            }

            var ddbConnectionEntity = GetDdbConnectionEntity(mappingStoreId, entityId);
            if (ddbConnectionEntity == null)
            {
                return ActionResults.NoResult;
            }

            var ddbEngine = this._databaseProviderManager.GetEngineByType(ddbConnectionEntity.DbType);
            var connectionStringSettings = ddbEngine.SettingsBuilder.CreateConnectionString(ddbConnectionEntity);

            try
            {
                // we need to be careful here as some DDB drivers such as PCAxis are very delicate on how the connection is opened. 
                using (var connection = ddbEngine.Browser.CreateConnection(connectionStringSettings))
                {
                    connection.Open();
                    return ActionResults.Success;
                }
            }
            catch (DbException e)
            {
                return new ActionResult("500", StatusType.Error,new List<Exception> {e}, e.Message);
            }
        }

        /// <summary>
        /// Gets the database objects.
        /// </summary>
        /// <param name="mappingStoreId">
        /// The mapping store identifier.
        /// </param>
        /// <param name="entityId">
        /// The DDB settings entity identifier.
        /// </param>
        /// <returns>
        /// The list of tables and/or views
        /// </returns>
        public IList<IDatabaseObject> GetDatabaseObjects(string mappingStoreId, string entityId)
        {
            if (mappingStoreId == null)
            {
                throw new ArgumentNullException(nameof(mappingStoreId));
            }

            if (entityId == null)
            {
                throw new ArgumentNullException(nameof(entityId));
            }

            var ddbConnectionEntity = GetDdbConnectionEntity(mappingStoreId, entityId);
            if (ddbConnectionEntity == null)
            {
                throw new ResourceNotFoundException($"Mapping Store Id : {mappingStoreId}, DDB Connection Settings Entity Id : {entityId}");
            }

            var ddbEngine = this._databaseProviderManager.GetEngineByType(ddbConnectionEntity.DbType);
            var connectionStringSettings = ddbEngine.SettingsBuilder.CreateConnectionString(ddbConnectionEntity);

            return ddbEngine.Browser.GetDatabaseObjects(connectionStringSettings).ToArray();
        }

        /// <summary>
        /// Gets the fields.
        /// </summary>
        /// <param name="mappingStoreId">
        /// The mapping store identifier.
        /// </param>
        /// <param name="entityId">
        /// The DDB settings entity identifier.
        /// </param>
        /// <param name="databaseObjectId">
        /// The database object identifier.
        /// </param>
        /// <returns>
        /// The list of fields of a specific database object (table or view)
        /// </returns>
        public IList<IFieldInfo> GetFields(string mappingStoreId, string entityId, string databaseObjectId)
        {
            if (mappingStoreId == null)
            {
                throw new ArgumentNullException(nameof(mappingStoreId));
            }

            if (entityId == null)
            {
                throw new ArgumentNullException(nameof(entityId));
            }

            var ddbConnectionEntity = GetDdbConnectionEntity(mappingStoreId, entityId);
            if (ddbConnectionEntity == null)
            {
                throw new ResourceNotFoundException($"Mapping Store Id : {mappingStoreId}, DDB Connection Settings Entity Id : {entityId}");
            }

            var ddbEngine = this._databaseProviderManager.GetEngineByType(ddbConnectionEntity.DbType);
            var connectionStringSettings = ddbEngine.SettingsBuilder.CreateConnectionString(ddbConnectionEntity);

            // if ObjectType is needed we do a UNION ALL 
            return ddbEngine.Browser.GetSchema(connectionStringSettings, new DatabaseObject() { Name = databaseObjectId }).ToArray();
        }

        /// <summary>
        /// Gets the DDB connection entity.
        /// </summary>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns>The <see cref="DdbConnectionEntity"/></returns>
        private DdbConnectionEntity GetDdbConnectionEntity(string mappingStoreId, string entityId)
        {
            var entityRetrieverEngine = this._entityRetrieverManager.GetRetrieverEngine<DdbConnectionEntity>(mappingStoreId);
            var ddbConnectionEntity =
                entityRetrieverEngine.GetEntities(
                    new EntityQuery()
                    {
                        EntityId = new Criteria<string>(OperatorType.Exact, entityId),
                        EntityType = EntityType.DdbConnectionSettings
                    },
                    Detail.Full).FirstOrDefault();
            return ddbConnectionEntity;
        }
    }
}