// -----------------------------------------------------------------------
// <copyright file="ILocalCodeRetrieverManager.cs" company="EUROSTAT">
//   Date Created : 2017-04-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System.Collections.Generic;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;

    /// <summary>
    /// The LocalCode Retriever Manager interface. This is responsible for retrieving information from DDB
    /// </summary>
    public interface ILocalCodeRetrieverManager
    {
        /// <summary>
        /// Return a list of codes (may contain duplicates) from one or more columns in a table or a query
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <returns>
        /// A list of codes
        /// </returns>
        QueryResult GetLocalData(ILocalDataQuery dataQuery);

        /// <summary>
        /// Get codes for a set of columns from a table (or select query) from the local DB
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <returns>
        /// A data table with n set of column name,column id and the codes as rows
        /// </returns>
        LocalCodeResults GetValidCodeSet(ILocalDataQuery dataQuery);

        /// <summary>
        /// Gets the available ordered codes.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <returns>
        /// A list of codes
        /// </returns>
        QueryResult GetAvailableOrderedCodes(ILocalDataQuery dataQuery);

        /// <summary>
        /// Validate values returned by <paramref name="dataQuery"/> using <paramref name="validator"/>
        /// </summary>
        /// <param name="dataQuery">The data query to get the data</param>
        /// <param name="validator">The validation code that checks each value</param>
        /// <returns></returns>
        void ValidateValues(ILocalDataQuery dataQuery, ILocalCodeValidator validator);

        /// <summary>
        /// Write local data from DDB for all or a set of specified columns
        /// </summary>
        void WriteLocalData(ILocalDataQuery dataQuery, ILocalDataWriter writer);
    }
}