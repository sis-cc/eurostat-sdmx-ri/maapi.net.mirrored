// -----------------------------------------------------------------------
// <copyright file="IDataSetEditorManager.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model;
using System.Collections.Generic;


namespace Estat.Sri.Mapping.Api.Manager
{
    public interface IDataSetEditorManager
    {
        // get the SQL String from the specified Dataset Entity. The Dataset entity doesn't need to exist in MSDB, Only its EditorType and XmlQuery are needed
        string GenerateSqlQuery(DatasetEntity datasetEntity);
        // This is part of this requirement but it is plugin specific, in Mapping Assistant 
        // some Dataset plugins support only specific type of DDB vendors. 
        IList<string> GetSupportedDdbPlugins(string editorType);

        IList<IDataSetEditorInfo> GetEditors();

        /// <summary>
        /// Deserialize the <see cref="DatasetEntity.JSONQuery"/> using the corresponding plugin
        /// </summary>
        /// <returns>The <see cref="DatasetEntity.JSONQuery"/> as an object; or null if it is not supported</returns>
        object DeserializeExtraData(DatasetEntity datasetEntity);

        /// <summary>
        /// Deserialize the <see cref="DatasetEntity.JSONQuery"/> using the corresponding plugin
        /// </summary>
        /// <returns>The <see cref="DatasetEntity.JSONQuery"/> as an object; or null if it is not supported</returns>
        object DeserializeExtraData(string editorType, string extraData);

        /// <summary>
        /// Migrate old extra data (<see cref="DatasetEntity.JSONQuery"/>) to a new format.
        /// If no migration is needed the plugin can return the same value as in <see cref="DatasetEntity.JSONQuery"/>
        /// </summary>
        /// <returns>The transformed <see cref="DatasetEntity.JSONQuery"/> if needed, the same value or null</returns>
        string Migrate(DatasetEntity datasetEntity);

        bool CanMigrate { get; }
    }
}
