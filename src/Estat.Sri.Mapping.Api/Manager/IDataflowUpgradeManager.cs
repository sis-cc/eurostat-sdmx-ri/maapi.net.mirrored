﻿// -----------------------------------------------------------------------
// <copyright file="IDataflowUpgradeManager.cs" company="EUROSTAT">
//   Date Created : 2017-07-14
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The DataflowUpgradeManager interface.
    /// </summary>
    public interface IDataflowUpgradeManager
    {
        /// <summary>
        /// Upgrades the specified store identifier.
        /// </summary>
        /// <param name="storeId">
        /// The store identifier.
        /// </param>
        /// <param name="structuralMetadata">
        /// The structural metadata. This can be null. In that case it will try to generate an on the fly StructureSet which will be used for the mapping
        /// </param>
        /// <param name="sourceDataflowUrn">
        /// The source dataflow urn.
        /// </param>
        /// <param name="targetDataflowUrn">
        /// The target dataflow urn.
        /// </param>
        /// <exception cref="ResourceNotFoundException">
        /// Mapping Store with ID <paramref name="storeId"/>
        /// </exception>
        /// <returns>
        /// The <see cref="MappingSetEntity"/>.
        /// </returns>
        MappingSetEntity Upgrade(
            string storeId,
            IEnumerable<FileInfo> structuralMetadata,
            Uri sourceDataflowUrn,
            Uri targetDataflowUrn);
    }
}