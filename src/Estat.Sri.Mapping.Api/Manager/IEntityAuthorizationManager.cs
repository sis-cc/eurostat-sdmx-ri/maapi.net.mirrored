﻿// -----------------------------------------------------------------------
// <copyright file="IEntityAuthorizationManager.cs" company="EUROSTAT">
//   Date Created : 2017-06-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The Entity Authorization Manager interface.
    /// </summary>
    public interface IEntityAuthorizationManager
    {
        /// <summary>
        /// Determines whether this instance can access the specified entity identifier.
        /// </summary>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity identifier; otherwise, <c>false</c>.
        /// </returns>
        bool CanAccess(string mappingStoreId, string entityId, EntityType entityType, AccessType accessType);

        /// <summary>
        /// Determines whether this instance can access the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity; otherwise, <c>false</c>.
        /// </returns>
        bool CanAccess(IEntity entity, AccessType accessType);

        /// <summary>
        /// Determines which instances can be accessed by the current principal
        /// </summary>
        /// <typeparam name="TPermissionEntity">The type of the t permission entity.</typeparam>
        /// <param name="entities">The entities.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        /// The entities that it can access
        /// </returns>
        IEnumerable<TPermissionEntity> CanAccess<TPermissionEntity>(IEnumerable<TPermissionEntity> entities, AccessType accessType) where TPermissionEntity : IEntity;
    }
}