﻿// -----------------------------------------------------------------------
// <copyright file="ComponentMappingValidationManager.cs" company="EUROSTAT">
//   Date Created : 2017-09-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    /// The component mapping validation manager.
    /// </summary>
    public class ComponentMappingValidationManager : IComponentMappingValidationManager
    {
        /// <summary>
        /// The factories
        /// </summary>
        private readonly IComponentMappingValidatorFactory[] _factories;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMappingValidationManager"/> class.
        /// </summary>
        /// <param name="factories">The factories.</param>
        /// <exception cref="System.ArgumentNullException">factories is null</exception>
        public ComponentMappingValidationManager(params IComponentMappingValidatorFactory[] factories)
        {
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories));
            }

            _factories = factories.ToArray();
        }

        /// <summary>
        /// Returns true if the specified <paramref name="componentMappings"/> is valid.
        /// </summary>
        /// <param name="componentMappings">The component mappings.</param>
        /// <param name="dataStructureObject">The data structure object.</param>
        /// <returns>
        ///   <c>true</c> if the specified component mappings is valid; otherwise, <c>false</c>.
        /// </returns>
        public IActionResult IsValid(IComponentMappingContainer componentMappings, IDataStructureObject dataStructureObject)
        {
            var results = new List<IActionResult>();
            foreach (var componentMappingValidatorFactory in _factories)
            {
                var result = componentMappingValidatorFactory.GetValidator()
                    .Validate(componentMappings, dataStructureObject);
                results.Add(result);
            }

            return results.Merge();
        }
    }
}