// -----------------------------------------------------------------------
// <copyright file="EntityRetrieverManager.cs" company="EUROSTAT">
//   Date Created : 2017-02-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;

    /// <summary>
    /// The default implementation of the <see cref="IEntityRetrieverManager"/>.
    /// This implementation can be used with any store
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Manager.IEntityRetrieverManager" />
    public class EntityRetrieverManager : IEntityRetrieverManager
    {
        /// <summary>
        /// The factories
        /// </summary>
        private readonly IList<IEntityRetrieverFactory> _factories;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityRetrieverManager"/> class.
        /// </summary>
        /// <param name="factories">The factories.</param>
        /// <exception cref="ArgumentNullException">factories - Must provide at least one factory</exception>
        /// <exception cref="ArgumentException">Must provide at least one factory - factories</exception>
        public EntityRetrieverManager(params IEntityRetrieverFactory[] factories)
        {
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories), "Must provide at least one factory");
            }

            if (factories.Length == 0)
            {
                throw new ArgumentException("Must provide at least one factory", nameof(factories));
            }

            this._factories = factories.ToArray();
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>
        /// The matching entities
        /// </returns>
        public IEnumerable<TEntity> GetEntities<TEntity>(string storeId, IEntityQuery query, Detail detail) where TEntity : IEntity
        {
            var engine = GetRetrieverEngine<TEntity>(storeId);
            var values = engine.GetEntities(query, detail);
            return values ?? Enumerable.Empty<TEntity>();
        }

        /// <summary>
        /// Gets the retriever engine.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>
        /// The <see cref="IEntityRetrieverEngine{TEntity}" />
        /// </returns>
        /// <exception cref="ArgumentNullException">storeId is null</exception>
        /// <exception cref="NotImplementedException">Could not find an engine</exception>
        public IEntityRetrieverEngine<TEntity> GetRetrieverEngine<TEntity>(string storeId) where TEntity : IEntity
        {
            if (storeId == null)
            {
                throw new ArgumentNullException(nameof(storeId));
            }

            foreach (var factory in this._factories)
            {
                IEntityRetrieverEngine<TEntity> engine = factory.GetRetrieverEngine<TEntity>(storeId);
                if (engine != null)
                {
                    return engine;
                }
            }

            throw new NotImplementedException(string.Format(CultureInfo.InvariantCulture, "Could not find an engine for type {0} and store {1}", typeof(TEntity), storeId));
        }

        /// <summary>
        /// Gets the retriever engine.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// The <see cref="IEntityRetrieverEngine{TEntity}" />
        /// <exception cref="System.ArgumentNullException">storeId is null</exception>
        public IEntityRetrieverEngine<IEntity> GetRetrieverEngine(string storeId, EntityType entityType)
        {
            if (storeId == null)
            {
                throw new ArgumentNullException(nameof(storeId));
            }

            foreach (var factory in this._factories)
            {
                var engine = factory.GetRetrieverEngine(storeId, entityType);
                if (engine != null)
                {
                    return engine;
                }
            }

            throw new NotImplementedException(string.Format(CultureInfo.InvariantCulture, "Could not find an engine for type {0} and store {1}", entityType,storeId));
        }

        public void WriteEntities(string storeId, IEntityStreamingWriter writer, EntityType entityType, IEntityQuery query, Detail detail)
        {
            IEntityRetrieverEngine<IEntity> engine = GetRetrieverEngine(storeId, entityType);
            writer.StartMessage();
            writer.StartSection(entityType);
            using (var notused = new AuthorizationCache(storeId))
            {
                engine.WriteEntities(query, detail, writer);
            }
        }
    }
}