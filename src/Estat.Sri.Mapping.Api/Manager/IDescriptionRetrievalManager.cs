// -----------------------------------------------------------------------
// <copyright file="IDescriptionRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2017-04-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// Gets the engine which allows us to get the descriptions of a dataset column values
    /// </summary>
    public interface IDescriptionRetrievalManager
    {
        /// <summary>
        /// Adds the description to <paramref name="localCodes"/>.
        /// </summary>
        /// <param name="datasetEntity">The dataset entity.</param>
        /// <param name="columnEntityId">The column entity identifier.</param>
        /// <param name="localCodes">The local codes.</param>
        /// <returns>The same instance of <paramref name="localCodes"/></returns>
        IList<LocalCodeEntity> AddDescription(
            DatasetEntity datasetEntity,
            string columnEntityId,
            IList<LocalCodeEntity> localCodes);

        /// <summary>
        /// Adds the description to the codes in the specified <paramref name="results"/>
        /// </summary>
        /// <param name="results">The results.</param>
        /// <returns>The same instance of <paramref name="results"/></returns>
        LocalCodeResults AddDescription(LocalCodeResults results);

        /// <summary>
        /// Gets a map with local code id and description from the database
        /// </summary>
        /// <param name="datasetEntity">The dataset</param>
        /// <param name="dataSetColumnEntity">The dataset column</param>
        /// <returns>The map </returns>
        Dictionary<string, string> GetMap(DatasetEntity datasetEntity, DataSetColumnEntity dataSetColumnEntity);
    }
}