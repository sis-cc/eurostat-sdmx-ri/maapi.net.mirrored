// -----------------------------------------------------------------------
// <copyright file="IComponentMappingManager.cs" company="EUROSTAT">
//   Date Created : 2017-09-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;

    /// <summary>
    /// This interface is responsible for retrieving the logic for mapping a component from/to local code.
    /// </summary>
    public interface IComponentMappingManager
    {
        /// <summary>
        /// Gets the builder for producing mappings between local and SDMX data.
        /// This returns the list <see cref="IComponentMappingBuilder" /> object for each component including Time Dimension and Last Update and Update Status in a mapping depending on the type of mapping e.g. 1
        /// DSD component - N local columns e.t.c.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="metadataStructure">The metadata structure.</param>
        /// <param name="isPit">The is pit flag. </param>
        /// <returns>
        /// The <see cref="IComponentMappingContainer" />.
        /// </returns>
        IComponentMappingContainer GetBuilders(string storeId, IDataflowObject dataflow, IDataStructureObject dataStructure, IMetadataStructureDefinitionObject metadataStructure, bool isPit);

        /// <summary>
        /// Gets the builder for producing mappings between local and SDMX data.
        /// This returns the list <see cref="IComponentMappingBuilder" /> object for each component including Time Dimension and Last Update and Update Status in a mapping depending on the type of mapping e.g. 1
        /// DSD component - N local columns e.t.c.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="dataStructure">The data structure.</param>
        /// <returns>
        /// The <see cref="IComponentMappingContainer" />.
        /// </returns>
        IComponentMappingContainer GetBuilders(string storeId, IDataflowObject dataflow, IDataStructureObject dataStructure);
    }
}