// -----------------------------------------------------------------------
// <copyright file="ComponentMappingManager.cs" company="EUROSTAT">
//   Date Created : 2017-09-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model.AdvancedTime;

namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Estat.Sdmxsource.Extension.Constant;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using System.ComponentModel;

    /// <summary>
    /// The component mapping manager.
    /// </summary>
    public class ComponentMappingManager : IComponentMappingManager
    {
        /// <summary>
        /// The _component mapping factories.
        /// </summary>
        private readonly IComponentMappingFactory[] _componentMappingFactories;

        /// <summary>
        /// The _entity retriever manager.
        /// </summary>
        private readonly IEntityRetrieverManager _entityRetrieverManager;

        /// <summary>
        /// The _last update mapping factories.
        /// </summary>
        private readonly ILastUpdateMappingFactory[] _lastUpdateMappingFactories;

        /// <summary>
        /// The _time dimension frequency factories.
        /// </summary>
        private readonly ITimeDimensionFrequencyFactory[] _timeDimensionFrequencyFactories;

        /// <summary>
        /// The _update status mapping factories.
        /// </summary>
        private readonly IUpdateStatusMappingFactory[] _updateStatusMappingFactories;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMappingManager"/> class.
        /// </summary>
        /// <param name="componentMappingFactories">The component mapping factories.</param>
        /// <param name="entityRetrieverManager">The entity retriever manager.</param>
        /// <param name="lastUpdateMappingFactories">The last update mapping factories.</param>
        /// <param name="timeDimensionFrequencyFactories">The time dimension frequency factories.</param>
        /// <param name="updateStatusMappingFactories">The update status mapping factories.</param>
        public ComponentMappingManager(IComponentMappingFactory[] componentMappingFactories, 
            IEntityRetrieverManager entityRetrieverManager, ILastUpdateMappingFactory[] lastUpdateMappingFactories, 
            ITimeDimensionFrequencyFactory[] timeDimensionFrequencyFactories, IUpdateStatusMappingFactory[] updateStatusMappingFactories)
        {
            _componentMappingFactories = componentMappingFactories;
            _entityRetrieverManager = entityRetrieverManager;
            _lastUpdateMappingFactories = lastUpdateMappingFactories;
            _timeDimensionFrequencyFactories = timeDimensionFrequencyFactories;
            _updateStatusMappingFactories = updateStatusMappingFactories;
        }

        /// <summary>
        /// Gets the builders.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="dataStructure">The data structure.</param>
        /// <returns></returns>
        public IComponentMappingContainer GetBuilders(
            string storeId,
            IDataflowObject dataflow,
            IDataStructureObject dataStructure)
        {
            return this.GetBuilders(storeId, dataflow, dataStructure, null, false);
        }

        /// <summary>
        /// Gets the builder for producing mappings between local and SDMX data.
        /// This returns the list <see cref="IComponentMappingContainer"/> object for each component including Time Dimension and Last Update and Update Status in a mapping depending on the type of mapping e.g. 1
        /// DSD component - N local columns e.t.c.
        /// </summary>
        /// <param name="storeId">
        /// The store identifier.
        /// </param>
        /// <param name="dataflow">
        /// The dataflow.
        /// </param>
        /// <param name="dataStructure">
        /// The data structure.
        /// </param>
        /// <param name="metadataStructure">
        /// The metadata structure.
        /// </param>
        /// <param name="isPit">
        /// The pit flag to identify if it is a point in time request.
        /// </param>
        /// <returns>
        /// The <see cref="IComponentMappingContainer"/>.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// dataflow
        /// or
        /// dataStructure
        /// </exception>
        public IComponentMappingContainer GetBuilders(
            string storeId,
            IDataflowObject dataflow,
            IDataStructureObject dataStructure,
            IMetadataStructureDefinitionObject metadataStructure,
            bool isPit)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            if (dataStructure == null)
            {
                throw new ArgumentNullException(nameof(dataStructure));
            }

            var dataflowUrn = dataflow.Urn;
            if (DataflowFilterContext.CurrentFilter == DataflowFilter.Production && dataflow.IsNonProduction())
            {
                return null;
            }

            var mappingSetEntityList =
                _entityRetrieverManager.GetEntities<MappingSetEntity>(
                    storeId,
                    dataflowUrn.ToString().QueryForThisParentId(),
                    Detail.Full).ToList();

            var container = new ComponentMappingContainer();

            var currentMappingSetEntity = GetCurrentMappingSetEntity(isPit, mappingSetEntityList, dataflowUrn, false);

            if (currentMappingSetEntity == null)
            {
                throw new ResourceNotFoundException($"Dataflow '{dataflowUrn}' doesn't contain a mapping set");
            }

            var componentMappings =
                _entityRetrieverManager.GetEntities<ComponentMappingEntity>(
                    storeId,
                    currentMappingSetEntity.QueryForThisParentId(),
                    Detail.Full).ToArray();

            var dataset = _entityRetrieverManager.GetEntities<DatasetEntity>(storeId, currentMappingSetEntity.DataSetId.QueryForThisEntityId(), Detail.Full).First();
            container.Dataset = dataset;

            var ddbConnection = _entityRetrieverManager.GetEntities<DdbConnectionEntity>(storeId, dataset.ParentId.QueryForThisEntityId(), Detail.Full).First();
            container.DisseminationConnection = ddbConnection;

            PopulateNormalMappings(componentMappings, dataflowUrn, dataStructure, container);

            // there can be be only one Time Dimension mapping per mapping set
            var timeMapping =
                            _entityRetrieverManager.GetEntities<TimeDimensionMappingEntity>(
                                storeId,
                                currentMappingSetEntity.QueryForThisParentId(),
                                Detail.Full).FirstOrDefault();
            PopulateTimeDimensionMappings(storeId, timeMapping, dataStructure, container);

            PopulateLastUpdateMapping(container, storeId, currentMappingSetEntity);

            PopulateUpdateStatusMapping(storeId, container, currentMappingSetEntity);

            PopulateValidToMapping(storeId, container, currentMappingSetEntity);

            if (metadataStructure != null)
            {
                var currentMetadataMappingSetEntity = GetCurrentMappingSetEntity(isPit, mappingSetEntityList, dataflowUrn, true);

                if (currentMetadataMappingSetEntity != null)
                {
                    var componentMetadataMappings =
                        _entityRetrieverManager.GetEntities<ComponentMappingEntity>(
                            storeId,
                            currentMetadataMappingSetEntity.QueryForThisParentId(),
                            Detail.Full).Where(x => x.IsMetadata).ToArray();

                    container.MetadataDataset = _entityRetrieverManager.GetEntities<DatasetEntity>(storeId,
                        currentMetadataMappingSetEntity.DataSetId.QueryForThisEntityId(), Detail.Full).First();

                    PopulateMetadataMappings(componentMetadataMappings, dataflowUrn, container);
                }
            }

            return container;
        }

        private void PopulateValidToMapping(string storeId, ComponentMappingContainer container, MappingSetEntity currentMappingSetEntity)
        {
            var validToMapping = _entityRetrieverManager.GetEntities<ValidToMappingEntity>(
                                storeId,
                                currentMappingSetEntity.QueryForThisParentId(),
                                Detail.Full).FirstOrDefault();
            container.ValidToMapping = validToMapping;
            if (validToMapping?.Column != null)
            {
                container.ValidToMappingBuilder = new ValidToMappingBuilder(validToMapping.GetColumns().First());
            }
        }

        /// <summary>
        /// Populates the update status mapping.
        /// </summary>
        /// <param name="storeId">
        /// The store identifier.
        /// </param>
        /// <param name="container">
        /// The container.
        /// </param>
        private void PopulateUpdateStatusMapping(
            string storeId,
            ComponentMappingContainer container, MappingSetEntity mappingSetEntity)
        {
            var updateStatusMapping = _entityRetrieverManager.GetEntities<UpdateStatusEntity>(storeId, mappingSetEntity.QueryForThisEntityId(), Detail.Full).FirstOrDefault();
            if (updateStatusMapping != null)
            {

                var builder =
                    _updateStatusMappingFactories.Select(factory => factory.GetBuilder(container.DisseminationConnection, updateStatusMapping))
                        .FirstOrDefault(mappingBuilder => mappingBuilder != null);
                container.SetUpdateStatus(updateStatusMapping, builder);
            }
        }

        /// <summary>
        /// Populates the last update mapping.
        /// </summary>
        /// <param name="componentMappings">
        /// The component mappings.
        /// </param>
        /// <param name="container">
        /// The container.
        /// </param>
        private void PopulateLastUpdateMapping(
            ComponentMappingContainer container,
            string storeId, MappingSetEntity currentMappingSetEntity)
        {
            var lastUpdated = _entityRetrieverManager.GetEntities<LastUpdatedEntity>(
                                storeId,
                                currentMappingSetEntity.QueryForThisParentId(),
                                Detail.Full).SingleOrDefault();
            if (lastUpdated != null)
            {
                var builder =
                    _lastUpdateMappingFactories.Select(factory => factory.GetBuilder(container.DisseminationConnection, lastUpdated))
                        .FirstOrDefault(mappingBuilder => mappingBuilder != null);
                container.SetLastUpdate(lastUpdated, builder);
            }
        }

        /// <summary>
        /// Populates the time dimension mappings.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="componentMappings">The component mappings.</param>
        /// <param name="timePreFormattedEntity">The time-pre-formatted entity.</param>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="container">The container.</param>
        private void PopulateTimeDimensionMappings(
            string storeId,
            TimeDimensionMappingEntity timeDimensionMapping,
            IDataStructureObject dataStructure,
            ComponentMappingContainer container)
        {
            if (timeDimensionMapping != null)
            {
                IComponentMappingBuilder freqComponentMappingBuilder = null;
                if (dataStructure.FrequencyDimension != null)
                {
                    container.ComponentMappingBuilders.TryGetValue(dataStructure.FrequencyDimension.Id, out freqComponentMappingBuilder);
                }

                // var durationMappings = .Where(entity => entity.IsDurationMapping()).ToDictionary(e => e.EntityId, StringComparer.Ordinal);

                SetTimeDimensionMapping(storeId, timeDimensionMapping,  container, container.DisseminationConnection.DbType, freqComponentMappingBuilder);
            }
        }

        /// <summary>
        /// Populates the normal mappings.
        /// </summary>
        /// <param name="componentMappings">
        /// The component mappings.
        /// </param>
        /// <param name="dataflowUrn">
        /// The dataflow urn.
        /// </param>
        /// <param name="dataStructure">The DSD to get component value types.</param>
        /// <param name="container">
        /// The container.
        /// </param>
        /// <exception cref="MissingInformationException">
        /// Cannot recognize the mapping for component
        /// </exception>
        private void PopulateNormalMappings(
            ComponentMappingEntity[] componentMappings,
            Uri dataflowUrn,
            IDataStructureObject dataStructure,
            ComponentMappingContainer container)
        {
            foreach (
                var componentMappingEntity in
                    componentMappings.Where(entity => entity.IsNormalMapping() && !entity.IsTimeDimensionMapping()))
            {
                // set the value type of the component
                var dsdComponent = dataStructure.GetComponent(componentMappingEntity.Component.ObjectId);
                if (dsdComponent != null)
                {
                    ComponentValueType componentValueType = dsdComponent.Representation != null && dsdComponent.Representation.MaxOccurs != null &&
                        (dsdComponent.Representation.MaxOccurs.IsUnbounded || dsdComponent.Representation.MaxOccurs.Occurrences > 1) ?
                        ComponentValueType.Array : ComponentValueType.Scalar;
                    componentMappingEntity.Component.ValueType = componentValueType;
                }

                TranscodingRuleMap transcodingRuleMap = null;
                if (componentMappingEntity.HasTranscoding())
                {
                    var rules = this._entityRetrieverManager.GetEntities<TranscodingRuleEntity>(
                        componentMappingEntity.StoreId,
                        componentMappingEntity.QueryForThisParentId(),
                        Detail.Full).ToArray();
                    transcodingRuleMap = new TranscodingRuleMap(rules, componentMappingEntity);
                }

                // We have a normal component mapping
                var builder =
                    _componentMappingFactories.Select(factory => factory.GetBuilder(componentMappingEntity, transcodingRuleMap, container.DisseminationConnection.DbType))
                        .FirstOrDefault(mappingBuilder => mappingBuilder != null);
                if (builder == null)
                {
                    throw new MissingInformationException(
                        $"Cannot recognize the mapping for component '{componentMappingEntity.Component?.ObjectId}' used by dataflow {dataflowUrn}");
                }

                container.Add(componentMappingEntity, builder);
            }
        }

        private void PopulateMetadataMappings(ComponentMappingEntity[] componentMappings, Uri dataflowUrn, ComponentMappingContainer container)
        {
            foreach (var componentMappingEntity in componentMappings.Where(entity => entity.IsMetadataMapping()))
            {
                // We have a metadata mapping
                var builder = _componentMappingFactories.Select(factory => factory.GetBuilder(componentMappingEntity, null, container.DisseminationConnection.DbType))
                        .FirstOrDefault(mappingBuilder => mappingBuilder != null);

                if (builder == null)
                {
                    throw new MissingInformationException($"Cannot recognize the mapping for metadata '{componentMappingEntity.MetadataAttributeSdmxId}' used by dataflow {dataflowUrn}");
                }

                container.AddMetadata(componentMappingEntity, builder);
            }
        }

        /// <summary>
        /// Populates the normal mappings.
        /// </summary>
        /// <param name="componentMappingEntity">The component mapping entity.</param>
        /// <param name="container">The container.</param>
        /// <returns>IComponentMappingBuilder.</returns>
        /// <exception cref="MissingInformationException">Cannot recognize the mapping for component</exception>
        private IComponentMappingBuilder GenerateDurationMappingBuilder(DurationMappingEntity componentMappingEntity, ComponentMappingContainer container)
        {
            {
                TranscodingRuleMap transcodingRuleMap = null;
                if (componentMappingEntity.HasTranscoding())
                {
                    transcodingRuleMap = new TranscodingRuleMap(componentMappingEntity);
                }

                // Duration mapping is not normal mapping. It doesn't have a component and type != A . Factories should have IsNormalOrDurationMapping()
                var builder =
                    _componentMappingFactories.Select(factory => factory.GetBuilder(componentMappingEntity.AsComponentMapping(), transcodingRuleMap, container.DisseminationConnection.DbType))
                        .FirstOrDefault(mappingBuilder => mappingBuilder != null);
                if (builder == null)
                {
                    throw new MissingInformationException($"Duration map without builder");
                }

                return builder;
            }
        }

        /// <summary>
        /// Sets the time dimension mapping.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="componentMappingEntity">The component mapping entity.</param>
        /// <param name="timePreFormattedEntity">The time-pre-formatted entity.</param>
        /// <param name="container">The container.</param>
        /// <param name="databaseType">Type of the database.</param>
        /// <param name="frequencyMapping">The frequency mapping.</param>
        /// <param name="durationMappings">The duration mappings.</param>
        private void SetTimeDimensionMapping(
            string storeId,
            TimeDimensionMappingEntity componentMappingEntity,
            ComponentMappingContainer container,
            string databaseType,
            IComponentMappingBuilder frequencyMapping
            )
        {
            TranscodingEntity transcodingEntity = null;
            TimeDimensionMappingSettings timeDimensionMappingSettings = null;

            if (componentMappingEntity.PreFormatted != null)
            {
                timeDimensionMappingSettings = new TimeDimensionMappingSettings(componentMappingEntity, componentMappingEntity.PreFormatted, databaseType, frequencyMapping);
            }
            else if (componentMappingEntity.HasTranscoding())
            {
                    if (componentMappingEntity.TimeMappingType == TimeDimensionMappingType.TranscodingWithCriteriaColumn)
                    {
                        var durationMapBuilders = componentMappingEntity
                            .Transcoding
                            .TimeFormatConfigurations
                            .Values
                            .Where(v => v.DurationMap != null)
                            .Select(v => v.DurationMap)
                            .ToDictionary(k => k.EntityId, x => GenerateDurationMappingBuilder(x, container), StringComparer.Ordinal);
                        timeDimensionMappingSettings = new TimeDimensionMappingSettings(componentMappingEntity, databaseType, durationMapBuilders);
                    }
            }

            if (timeDimensionMappingSettings == null)
            {
                timeDimensionMappingSettings = new TimeDimensionMappingSettings(
                    componentMappingEntity,
                    componentMappingEntity.PreFormatted,
                    databaseType,
                    frequencyMapping);
            }

            var builder =
                _timeDimensionFrequencyFactories.Select(
                    factory => factory.GetBuilder(timeDimensionMappingSettings))
                    .FirstOrDefault(mappingBuilder => mappingBuilder != null);
            container.SetTimeDimension(componentMappingEntity, builder);
        }

        private static MappingSetEntity GetCurrentMappingSetEntity(bool isPit, List<MappingSetEntity> mappingSetEntityList, Uri dataflowUrn, bool isMetadata)
        {
            var currentMappingSetEntity = GetMappingSet(mappingSetEntityList, DateTime.Now, isMetadata);

            // Point in time request
            if (isPit)
            {
                if (currentMappingSetEntity == null)
                {
                    // It can happen, that only a PIT version exists
                    currentMappingSetEntity = GetMappingSet(mappingSetEntityList, DateTime.MaxValue, isMetadata);
                }
                else
                {
                    if (!currentMappingSetEntity.ValidTo.HasValue)
                    {
                        // If there is no ValidTo set for the current, there cannot be a PIT version
                        throw new ResourceNotFoundException($"Dataflow '{dataflowUrn}' doesn't contain a PIT {(isMetadata ? "metadata" : string.Empty)} mapping set");
                    }

                    // Get the mapping set right after the current one
                    currentMappingSetEntity =
                        GetMappingSet(mappingSetEntityList, TryAddSecond(currentMappingSetEntity.ValidTo.Value), isMetadata);
                }
            }

            return currentMappingSetEntity;
        }

        private static MappingSetEntity GetMappingSet(List<MappingSetEntity> mappingSetEntityList, DateTime dateTime, bool isMetadata = false)
        {
            if (mappingSetEntityList == null)
            {
                throw new ArgumentNullException(nameof(mappingSetEntityList));
            }

            if (!mappingSetEntityList.Any())
            {
                return null;
            }

            mappingSetEntityList = mappingSetEntityList.FindAll(x => x.IsMetadata == isMetadata);

            return
                mappingSetEntityList.FirstOrDefault(x => !x.ValidFrom.HasValue && !x.ValidTo.HasValue) ??
                mappingSetEntityList.FirstOrDefault(x =>
                    (!x.ValidFrom.HasValue || x.ValidFrom.Value <= dateTime) &&
                    (!x.ValidTo.HasValue || x.ValidTo.Value > dateTime));
        }

        private static DateTime TryAddSecond(DateTime dateTime)
        {
            try
            {
                return dateTime.AddSeconds(1);
            }
            catch (ArgumentOutOfRangeException)
            {
                return DateTime.MaxValue;
            }
        }
    }
}