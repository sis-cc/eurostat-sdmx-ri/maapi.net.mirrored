﻿// -----------------------------------------------------------------------
// <copyright file="IMappingStoreManager.cs" company="EUROSTAT">
//   Date Created : 2017-02-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Engine;

    /// <summary>
    /// This manager class is responsible for retrieving the available Mapping Store engine and ids
    /// </summary>
    public interface IMappingStoreManager
    {
        /// <summary>
        /// Retrieve the available mapping store ids.
        /// </summary>
        /// <returns>
        /// The available mapping ids
        /// </returns>
        IEnumerable<string> RetrieveStoreIds();

        /// <summary>
        /// Retrieve the store details.
        /// </summary>
        /// <returns>
        /// The store details
        /// </returns>
        IEnumerable<StoreDetails> RetrieveStoresDetails();
        /// <summary>
        /// Gets the type of the store.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>The store type</returns>
        string GetStoreType(string storeId);

        /// <summary>
        /// Gets the engine by store identifier.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>The <see cref="IMappingStoreEngine"/></returns>
        IMappingStoreEngine GetEngineByStoreId(string storeId);

        /// <summary>
        /// Gets the type of the engine by store.
        /// </summary>
        /// <param name="storeType">Type of the store.</param>
        /// <returns>The <see cref="IMappingStoreEngine"/></returns>
        IMappingStoreEngine GetEngineByStoreType(string storeType);
    }
}