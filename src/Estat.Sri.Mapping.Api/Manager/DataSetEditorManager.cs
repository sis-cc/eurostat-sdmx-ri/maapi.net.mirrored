// -----------------------------------------------------------------------
// <copyright file="DataSetEditorManager.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Exceptions;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Manager
{ 
    public class DataSetEditorManager : IDataSetEditorManager
    {
        private readonly List<IDataSetEditorFactory> _factories;

        public DataSetEditorManager(params IDataSetEditorFactory[] factories)
        {
            _factories = factories.ToList();
        }

        public string GenerateSqlQuery(DatasetEntity datasetEntity)
        {
            foreach(var factory in _factories)
            {
                var engine = factory.GetEngine(datasetEntity.EditorType);

                if(engine != null)
                {
                    return engine.GenerateSqlQuery(datasetEntity);
                }
            }

            throw new ResourceNotFoundException($"Plugin " + datasetEntity.EditorType + " not found");
        }

        public IList<IDataSetEditorInfo> GetEditors()
        {
            return _factories.Select(x => x.EditorInfo).ToArray();
        }

        /// <inheritdoc />
        public object DeserializeExtraData(DatasetEntity datasetEntity)
        {
            if (datasetEntity == null)
            {
                throw new ArgumentNullException(nameof(datasetEntity));
            }

            return this.DeserializeExtraData(datasetEntity.EditorType, datasetEntity.JSONQuery);
        }

        /// <inheritdoc />
        public object DeserializeExtraData(string editorType, string extraData)
        {
            foreach(var factory in _factories)
            {
                var engine = factory.GetEngine(editorType);

                if(engine != null)
                {
                    return engine.DeserializeExtraData(extraData);
                }
            }

            return null;
        }

        public IList<string> GetSupportedDdbPlugins(string editorType)
        {
            var engine = _factories.Select(x => x.GetEngine(editorType)).FirstOrDefault(x => x != null);
            if (engine == null)
            {
                throw new ResourceNotFoundException($"Plugin "+ editorType + " not found");
            }

            return engine.GetSupportedDdbPlugins();
        }

        public string Migrate(DatasetEntity datasetEntity)
        {
            if (datasetEntity is null)
            {
                throw new ArgumentNullException(nameof(datasetEntity));
            }

            foreach (var factory in _factories)
            {
                var engine = factory.GetEngine(datasetEntity.EditorType);

                if (engine != null)
                {
                    return engine.Migrate(datasetEntity);
                }
            }

            return null;
        }

        public bool CanMigrate => _factories.Any();
    }
}
