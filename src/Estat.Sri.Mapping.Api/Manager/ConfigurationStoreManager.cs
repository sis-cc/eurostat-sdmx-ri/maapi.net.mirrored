﻿// -----------------------------------------------------------------------
// <copyright file="ConfigurationStoreManager.cs" company="EUROSTAT">
//   Date Created : 2017-03-20
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// Configuration store manager default implementation
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Manager.IConfigurationStoreManager" />
    public class ConfigurationStoreManager : IConfigurationStoreManager
    {
        /// <summary>
        /// The factories
        /// </summary>
        private readonly IList<IConfigurationStoreFactory> _factories;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationStoreManager"/> class.
        /// </summary>
        /// <param name="factories">
        /// The factories.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// factories are null
        /// </exception>
        public ConfigurationStoreManager(params IConfigurationStoreFactory[] factories)
        {
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories));
            }

            _factories = factories.ToArray();
        }

        /// <summary>
        /// Gets the settings from a default location.
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <returns>
        /// The settings
        /// </returns>
        /// <exception cref="System.NotImplementedException">Settings of the following type not found:" + typeof(TSettings)</exception>
        public IEnumerable<TSettings> GetSettings<TSettings>()
        {
            return _factories.Select(factory => factory.GetEngine<TSettings>())
                    .Where(engine => engine != null)
                    .SelectMany(engine => engine.GetSettings());
        }

        /// <summary>
        /// Saves the settings.
        /// </summary>
        /// <typeparam name="TSettings">The type of the t settings.</typeparam>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The <see cref="IActionResult" />.
        /// </returns>
        public IActionResult SaveSettings<TSettings>(TSettings settings)
        {
            this.GetFirstEngine<TSettings>().SaveSettings(settings);
            return ActionResults.Success;
        }

        /// <summary>
        /// Deletes the settings.
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <param name="name">The name.</param>
        /// <returns>
        /// The <see cref="IActionResult" />.
        /// </returns>
        public IActionResult DeleteSettings<TSettings>(string name)
        {
            // TODO check if it exists 
            this.GetFirstEngine<TSettings>().DeleteSettings(name);
            return ActionResults.Success;
        }

        /// <summary>
        /// Gets the first engine.
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <returns>The engine</returns>
        /// <exception cref="System.NotImplementedException">Settings of the following type not found</exception>
        private IConfigurationStoreEngine<TSettings> GetFirstEngine<TSettings>()
        {
            foreach (var factory in _factories)
            {
                var engine = factory.GetEngine<TSettings>();
                if (engine != null)
                {
                    return engine;
                }
            }

            throw new NotImplementedException("Settings of the following type not found:" + typeof(TSettings));
        }
    }
}