﻿// -----------------------------------------------------------------------
// <copyright file="DatabaseProviderManager.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The database provider manager.
    /// </summary>
    public class DatabaseProviderManager : IDatabaseProviderManager
    {
        /// <summary>
        /// The _factories.
        /// </summary>
        private readonly IList<IDatabaseProviderFactory> _factories;

        /// <summary>
        /// The provider names
        /// </summary>
        private readonly string[] _providerNames;

        /// <summary>
        /// The provider types
        /// </summary>
        private readonly string[] _providerTypes;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseProviderManager"/> class.
        /// </summary>
        /// <param name="factories">
        /// The factories.
        /// </param>
        public DatabaseProviderManager(params IDatabaseProviderFactory[] factories)
        {
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories));
            }

            _factories = factories.ToArray();
            _providerTypes = _factories.Select(databaseProviderFactory => databaseProviderFactory.ProviderType).ToArray();
            _providerNames = _factories.Select(databaseProviderFactory => databaseProviderFactory.ProviderName).ToArray();
        }

        /// <summary>
        /// Gets the database provider types.
        /// </summary>
        /// <returns>The available types</returns>
        public IEnumerable<string> GetDatabaseProviderTypes()
        {
            return _providerTypes;
        }

        /// <summary>
        /// Gets the database provider names
        /// </summary>
        /// <returns>The available provider names</returns>
        public IEnumerable<string> GetDatabaseProviderNames()
        {
            return _providerNames;
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="providerName">
        /// Name of the provider.
        /// </param>
        /// <returns>
        /// The DB Vendor specific <see cref="IDatabaseProviderEngine"/>
        /// </returns>
        /// <exception cref="System.NotImplementedException">
        /// No support for provider <paramref name="providerName"/>
        /// </exception>
        public IDatabaseProviderEngine GetEngineByProvider(string providerName)
        {
            foreach (var factory in _factories)
            {
                var engine = factory.GetEngineByProvider(providerName);
                if (engine != null)
                {
                    return engine;
                }
            }

            throw new NotImplementedException("No support for provider :" + providerName);
        }

        /// <summary>
        /// Gets the <see cref="IDatabaseProviderEngine"/> by type
        /// </summary>
        /// <param name="type">The database type.</param>
        /// <returns>
        /// The <see cref="IDatabaseProviderEngine" />.
        /// </returns>
        /// <exception cref="System.NotImplementedException">No support for <paramref name="type"/></exception>
        public IDatabaseProviderEngine GetEngineByType(string type)
        {
            foreach (var factory in _factories)
            {
                var engine = factory.GetEngineByType(type);
                if (engine != null)
                {
                    return engine;
                }
            }

            throw new NotImplementedException("No support for database of type :" + type);
        }

        /// <summary>
        /// Gets the connection string settings.
        /// </summary>
        /// <param name="connectionEntity">The connection entity.</param>
        /// <returns>The connection string settings</returns>
        public ConnectionStringSettings GetConnectionStringSettings(DdbConnectionEntity connectionEntity)
        {
            return GetEngineByType(connectionEntity.DbType).SettingsBuilder.CreateConnectionString(connectionEntity);
        }
    }
}