// -----------------------------------------------------------------------
// <copyright file="EntityPersistenceManager.cs" company="EUROSTAT">
//   Date Created : 2017-02-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using Estat.Sri.Mapping.Api.Constant;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using log4net;

    /// <summary>
    /// This manager is responsible for adding, deleting and updating entities 
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Manager.IEntityPersistenceManager" />
    public class EntityPersistenceManager : IEntityPersistenceManager
    {
        /// <summary>
        /// logger
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(EntityPersistenceManager)); 
        /// <summary>
        /// The factories
        /// </summary>
        private readonly IEntityPeristenceFactory[] _factories;

        /// <summary>
        /// The entity type builder
        /// </summary>
        private readonly IEntityTypeBuilder _entityTypeBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityPersistenceManager" /> class.
        /// </summary>
        /// <param name="entityTypeBuilder">The entity type builder.</param>
        /// <param name="factories">The factories.</param>
        /// <exception cref="System.ArgumentNullException">factories - Must provide at least one factory</exception>
        /// <exception cref="System.ArgumentException">Must provide at least one factory - factories</exception>
        public EntityPersistenceManager(IEntityTypeBuilder entityTypeBuilder, params IEntityPeristenceFactory[] factories)
        {
            if (entityTypeBuilder == null)
            {
                throw new ArgumentNullException(nameof(entityTypeBuilder));
            }

            _entityTypeBuilder = entityTypeBuilder;

            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories), "Must provide at least one factory");
            }

            if (factories.Length == 0)
            {
                throw new ArgumentException("Must provide at least one factory", nameof(factories));
            }

            this._factories = factories.ToArray();
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public TEntity Add<TEntity>(TEntity entity) where TEntity : IEntity
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (entity.StoreId == null)
            {
                throw new ArgumentException("StoreId not set", nameof(entity));
            }

            var entityPersistenceEngine = GetEngine<TEntity>(entity.StoreId);

            // TODO check first if an entity exists
            var entityId = entityPersistenceEngine.Add(entity);
            entity.EntityId = entityId.ToString(CultureInfo.InvariantCulture);
            return entity;
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entities">The entity.</param>
        /// <returns>The entity that were added with the entity ID</returns>
        public IEnumerable<TEntity> AddEntities<TEntity>(IEnumerable<TEntity> entities) where TEntity : IEntity
        {
            IList<TEntity> addedEntities = new List<TEntity>();

            foreach (var entity in entities)
            {
                // TODO use engine.AddEntities()
                var updatedEntity = this.Add(entity);
                addedEntities.Add(updatedEntity);
            }

            return addedEntities;
        }

        /// <inheritdoc />
        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        public void Update<TEntity>(TEntity entity) where TEntity : IEntity
        {
            var entityPersistenceEngine = this.GetEngine<TEntity>(entity.StoreId);
            if (entity is TranscodingEntity)
            {
                var updatePersistanceEngine = entityPersistenceEngine as IEntityPersistenceForUpdate<TranscodingEntity>;
                updatePersistanceEngine.Update(entity);
            }
            else if (entity is HeaderEntity)
            {
                var updatePersistanceEngine = entityPersistenceEngine as IEntityPersistenceForUpdate<HeaderEntity>;
                updatePersistanceEngine.Update(entity);
            }

            else if (entity is TemplateMapping)
            {
                var updatePersistanceEngine = entityPersistenceEngine as IEntityPersistenceForUpdate<TemplateMapping>;
                updatePersistanceEngine.Update(entity);
            }
            else if(entity is UserEntity)
            {
                var updatePersistanceEngine = entityPersistenceEngine as IEntityPersistenceForUpdate<UserEntity>;
                updatePersistanceEngine.Update(entity);
            }
            else
            {
                entityPersistenceEngine.Update(entity.PatchRequest, entity.TypeOfEntity, entity.EntityId);
            }
        }

        /// <summary>
        /// Deletes the specified entity identifier.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="entityId">The entity identifier.</param>
        public void Delete<TEntity>(string storeId, string entityId) where TEntity : IEntity
        {
            var engine = GetEngine<TEntity>(storeId);

            var entityType = this._entityTypeBuilder.Build<TEntity>();

            engine.Delete(entityId, entityType);
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="mappingStoreIdentifier">The mapping store identifier.</param>
        /// <returns>The <see cref="IEntityPersistenceEngine{TEntity}"/></returns>
        /// <exception cref="System.NotImplementedException">Could not find an appropriate engine</exception>
        public IEntityPersistenceEngine<TEntity> GetEngine<TEntity>(string mappingStoreIdentifier) where TEntity : IEntity
        {
            foreach (var factory in this._factories)
            {
                var engine = factory.GetEngine<TEntity>(mappingStoreIdentifier);
                if (engine != null)
                {
                    return engine;
                }
            }

            throw new NotImplementedException(string.Format(CultureInfo.InvariantCulture, "Could not find an engine for type {0} and store {1}", typeof(TEntity), mappingStoreIdentifier));
        }

        public void AddEntities(string storeId, IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityType entityType)
        {
            // TODO do we need to map the entity id's between parent and children ?
            // This could affect import/export 
            // In case this is needed we need to provide for each entity type a Map/Dictionary
            // between the XML/JSON entity id's and the actual entity id's
            using (var notused = new AuthorizationCache(storeId))
            while (input.MoveToNextMessage())
            {
                responseWriter?.StartMessage();

                while (input.MoveToNextEntityTypeSection())
                {
                    foreach (var factory in this._factories)
                    {
                        var engine = factory.GetEngine(storeId, input.CurrentEntityType);
                        if (engine != null)
                        {
                            responseWriter?.StartSection(input.CurrentEntityType);
                            var statuses = engine.Add(input, responseWriter);
                            responseWriter.Status = statuses.Any(x => x == StatusType.Warning) ? 403 : 201;
                        }
                    }
                }
            }
        }

        public void Import(string storeId, IEntityStreamingReader input, IEntityStreamingWriter responseWriter)
        {
            EntityIdMapping mapping = new EntityIdMapping();
            while (input.MoveToNextEntityTypeSection())
            {
                var engine = GetEngine(storeId, input.CurrentEntityType);
                if (engine != null)
                {
                    _log.DebugFormat("Starting import of {0}", input.CurrentEntityType);
                    engine.Import(input, responseWriter, mapping);
                    _log.DebugFormat("Done import of {0}", input.CurrentEntityType);
                }
            }
        }

        private IEntityPersistanceStreamEngine GetEngine(string storeId, EntityType currentEntityType)
        {
            foreach (var factory in this._factories)
            {
                var engine = factory.GetEngine(storeId, currentEntityType);
                if (engine != null)
                {
                    return engine;
                }
            }
            return null;
        }
    }


}
