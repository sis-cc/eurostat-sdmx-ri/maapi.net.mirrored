// -----------------------------------------------------------------------
// <copyright file="IDataSetDataRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2018-10-18
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;

    /// <summary>
    /// This manager is responsible for routing and retrieving local codes from Mapping Store or Dissemination database
    /// </summary>
    public interface IDataSetDataRetrievalManager
    {
        /// <summary>
        /// Gets the Cartesian data.
        /// </summary>
        /// <param name="datasetEntity">The dataset entity.</param>
        /// <param name="requestedColumns">The columns.</param>
        /// <param name="page">The page.</param>
        /// <param name="count">The count.</param>
        /// <returns>
        /// The <see cref="LocalCodeResults" />
        /// </returns>
        [Obsolete("Non-streaming version. Use the " + nameof(WriteAvailableData))]
        LocalCodeResults GetAvailableData(DatasetEntity datasetEntity, ISet<string> requestedColumns, int page, int count);

        /// <summary>
        /// Gets the local codes with description.
        /// </summary>
        /// <param name="columnEntity">The column entity.</param>
        /// <param name="page">The page.</param>
        /// <param name="count">The count.</param>
        /// <returns></returns>
        [Obsolete("Non-streaming version. Use the " + nameof(WriteAvailableData))]
        IList<LocalCodeEntity> GetLocalCodesWithDescription(DataSetColumnEntity columnEntity, int page, int count);

        /// <summary>
        /// Get the available data from Dataset. If all requested columns have optionally using cached codes from MSDB. For columns where description exists include it
        /// </summary>
        void WriteAvailableData(DatasetEntity datasetEntity, ISet<string> requestedColumns, int page, int count,
            ILocalDataWriter writer);

        /// <summary>
        /// Validate the values of the <paramref name="columnEntity"/> using the specified <paramref name="localCodeValidator"/>
        /// </summary>
        /// <param name="columnEntity">The column which values should be validated</param>
        /// <param name="localCodeValidator">The validation to check the column against. This is also responsible for recording the errors</param>
        void Validate(DataSetColumnEntity columnEntity, ILocalCodeValidator localCodeValidator);
    }
}