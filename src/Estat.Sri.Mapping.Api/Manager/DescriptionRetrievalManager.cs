// -----------------------------------------------------------------------
// <copyright file="DescriptionRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2017-05-08
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The description retrieval manager.
    /// </summary>
    public class DescriptionRetrievalManager : IDescriptionRetrievalManager
    {
        /// <summary>
        /// The _database provider manager.
        /// </summary>
        private readonly IDatabaseProviderManager _databaseProviderManager;

        /// <summary>
        /// The _factories.
        /// </summary>
        private readonly IList<IDescriptionRetrievalFactory> _factories;

        /// <summary>
        /// The _retriever manager.
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptionRetrievalManager"/> class.
        /// </summary>
        /// <param name="retrieverManager">
        /// The retriever manager.
        /// </param>
        /// <param name="databaseProviderManager">
        /// The database Provider Manager.
        /// </param>
        /// <param name="factories">
        /// The factories.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// factories is <c>null</c>
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// No factories were set - factories
        /// </exception>
        public DescriptionRetrievalManager(
            IEntityRetrieverManager retrieverManager,
            IDatabaseProviderManager databaseProviderManager,
            params IDescriptionRetrievalFactory[] factories)
        {
            if (retrieverManager == null)
            {
                throw new ArgumentNullException(nameof(retrieverManager));
            }

            _retrieverManager = retrieverManager;
            _databaseProviderManager = databaseProviderManager;
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories));
            }

            if (factories.Length == 0)
            {
                throw new ArgumentException("No factories were set", nameof(factories));
            }

            _factories = factories.ToArray();
        }

        /// <summary>
        /// Adds the description.
        /// </summary>
        /// <param name="datasetEntity">
        /// The dataset entity.
        /// </param>
        /// <param name="columnEntityId">
        /// The column entity identifier.
        /// </param>
        /// <param name="localCodes">
        /// The local codes.
        /// </param>
        /// <returns>
        /// The same instance of <paramref name="localCodes"/>
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// datasetEntity
        /// or
        /// columnEntityId
        /// or
        /// localCodes
        /// </exception>
        public IList<LocalCodeEntity> AddDescription(
            DatasetEntity datasetEntity,
            string columnEntityId,
            IList<LocalCodeEntity> localCodes)
        {
            if (datasetEntity == null)
            {
                throw new ArgumentNullException(nameof(datasetEntity));
            }

            if (columnEntityId == null)
            {
                throw new ArgumentNullException(nameof(columnEntityId));
            }

            if (localCodes == null)
            {
                throw new ArgumentNullException(nameof(localCodes));
            }

            var descSourceRetriever =
                _retrieverManager.GetRetrieverEngine<ColumnDescriptionSourceEntity>(datasetEntity.StoreId);

            var connectionEntityId = datasetEntity.ParentId;
            var engine = GetEngine(datasetEntity.StoreId, connectionEntityId);

            return AddDescription(columnEntityId, localCodes, descSourceRetriever, engine);
        }

        /// <summary>
        /// Adds the description to the codes in the specified <paramref name="results"/>
        /// </summary>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <returns>
        /// The same instance of <paramref name="results"/>
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// results is <c>null</c>
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// Cannot determine the DDB Connection ID - results
        /// </exception>
        public LocalCodeResults AddDescription(LocalCodeResults results)
        {
            if (results == null)
            {
                throw new ArgumentNullException(nameof(results));
            }

            if (results.Count == 0)
            {
                return results;
            }

            string ddbConnectionEntityId;
            if (!results.RelatedEntities.TryGetValue(EntityType.DdbConnectionSettings, out ddbConnectionEntityId))
            {
                throw new ArgumentException("Cannot determine the DDB Connection ID", nameof(results));
            }

            var descSourceRetriever =
                _retrieverManager.GetRetrieverEngine<ColumnDescriptionSourceEntity>(results.MappingStoreId);

            var engine = GetEngine(results.MappingStoreId, ddbConnectionEntityId);
            for (int i = 0, resultsColumnsCount = results.Columns.Count; i < resultsColumnsCount; i++)
            {
                var columns = results.Columns[i];
                var localCodes = results.LocalCodes.Select(list => list[i]).ToArray();

                AddDescription(columns.EntityId, localCodes, descSourceRetriever, engine);
            }

            return results;
        }

        /// <inherit />
        public Dictionary<string, string> GetMap(DatasetEntity datasetEntity, DataSetColumnEntity dataSetColumnEntity)
        {
            if (datasetEntity == null)
            {
                throw new ArgumentNullException(nameof(datasetEntity));
            }

            if (dataSetColumnEntity == null)
            {
                throw new ArgumentNullException(nameof(dataSetColumnEntity));
            }

            var descSourceRetriever =
                _retrieverManager.GetRetrieverEngine<ColumnDescriptionSourceEntity>(datasetEntity.StoreId);

            var connectionEntityId = datasetEntity.ParentId;
            var engine = GetEngine(datasetEntity.StoreId, connectionEntityId);
            var descSource =
             descSourceRetriever.GetEntities(
                 new EntityQuery { ParentId = new Criteria<string>(OperatorType.Exact, dataSetColumnEntity.ParentId) },
                 Detail.Full).FirstOrDefault();
            if (descSource == null)
            {
                return null;
            }

            return engine.GetMap(datasetEntity.ParentId, descSource);
        }

        /// <summary>
        /// Adds the description.
        /// </summary>
        /// <param name="columnEntityId">
        /// The column entity identifier.
        /// </param>
        /// <param name="localCodes">
        /// The local codes.
        /// </param>
        /// <param name="descSourceRetriever">
        /// The description source retriever.
        /// </param>
        /// <param name="engine">
        /// The engine.
        /// </param>
        /// <returns>
        /// The list of codes
        /// </returns>
        private static IList<LocalCodeEntity> AddDescription(
            string columnEntityId,
            IList<LocalCodeEntity> localCodes,
            IEntityRetrieverEngine<ColumnDescriptionSourceEntity> descSourceRetriever,
            IDescriptionRetrievalEngine engine)
        {
            var descSource =
                descSourceRetriever.GetEntities(
                    new EntityQuery { ParentId = new Criteria<string>(OperatorType.Exact, columnEntityId) },
                    Detail.Full).FirstOrDefault();
            if (descSource == null)
            {
                return localCodes;
            }

            engine.FillDescription(descSource, localCodes);
            return localCodes;
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="mappingStoreId">
        /// The mapping Store Id.
        /// </param>
        /// <param name="connectionEntityId">
        /// The connection entity identifier.
        /// </param>
        /// <returns>
        /// The <see cref="IDescriptionRetrievalEngine"/>.
        /// </returns>
        private IDescriptionRetrievalEngine GetEngine(string mappingStoreId, string connectionEntityId)
        {
            var ddbConnectionSettingsRetriever =
                _retrieverManager.GetRetrieverEngine<DdbConnectionEntity>(mappingStoreId);

            var connectionEntity =
                ddbConnectionSettingsRetriever.GetEntities(
                    new EntityQuery { EntityId = new Criteria<string>(OperatorType.Exact, connectionEntityId) },
                    Detail.Full).FirstOrDefault();

            if (connectionEntity == null)
            {
                throw new ResourceNotFoundException($"DDB Connection Entity with ID {connectionEntityId} not found");
            }

            var connectionStringSettings = _databaseProviderManager.GetConnectionStringSettings(connectionEntity);
            IDescriptionRetrievalEngine engine = null;
            foreach (var factory in _factories)
            {
                var descriptionRetrievalEngine = factory.GetEngine(connectionStringSettings);
                if (descriptionRetrievalEngine != null)
                {
                    engine = descriptionRetrievalEngine;
                }
            }

            if (engine == null)
            {
                throw new NotImplementedException(
                    $"Description retrieval Engine for DB Provider {connectionEntity.DbType} not implemented");
            }

            return engine;
        }
    }
}