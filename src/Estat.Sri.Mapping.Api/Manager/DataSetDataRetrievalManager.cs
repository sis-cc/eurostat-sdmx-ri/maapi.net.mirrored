// -----------------------------------------------------------------------
// <copyright file="DataSetDataRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2017-04-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Engine.Streaming;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

    /// <summary>
    /// The data set data retrieval manager.
    /// </summary>
    public class DataSetDataRetrievalManager : IDataSetDataRetrievalManager
    {
        /// <summary>
        /// The local code retriever manager
        /// </summary>
        private readonly ILocalCodeRetrieverManager _localCodeRetrieverManager;

        /// <summary>
        /// The description retrieval manager
        /// </summary>
        private readonly IDescriptionRetrievalManager _descriptionRetrievalManager;

        /// <summary>
        /// The _retriever manager.
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSetDataRetrievalManager" /> class.
        /// </summary>
        /// <param name="localCodeRetrieverManager">The local code retriever manager.</param>
        /// <param name="retrieverManager">The retriever manager.</param>
        /// <param name="descriptionRetrievalManager">The description retrieval manager.</param>
        public DataSetDataRetrievalManager(ILocalCodeRetrieverManager localCodeRetrieverManager, IEntityRetrieverManager retrieverManager, IDescriptionRetrievalManager descriptionRetrievalManager)
        {
            _localCodeRetrieverManager = localCodeRetrieverManager;
            _retrieverManager = retrieverManager;
            _descriptionRetrievalManager = descriptionRetrievalManager;
        }

        /// <summary>
        /// Gets the available codes with description.
        /// </summary>
        /// <param name="columnEntity">The column entity.</param>
        /// <param name="page">The page.</param>
        /// <param name="count">The count.</param>
        /// <returns>
        /// The list of codes with description
        /// </returns>
        /// <remarks>In old Mapping Assistant this is used to get a Show Local codes in MA</remarks>
        public IList<LocalCodeEntity> GetLocalCodesWithDescription(DataSetColumnEntity columnEntity, int page, int count)
        {
            if (columnEntity == null)
            {
                throw new ArgumentNullException(nameof(columnEntity));
            }

            var dataset = GetDataSet(columnEntity);

            var retrieverEngine = this._retrieverManager.GetRetrieverEngine<LocalCodeEntity>(columnEntity.StoreId);

            IList<LocalCodeEntity> localCodes;
            var cachedLocalCodes =
                retrieverEngine.GetEntities(
                    new EntityQuery() { ParentId = new Criteria<string>(OperatorType.Exact, columnEntity.EntityId), Limit = new Criteria<int>(OperatorType.Exact, count), Page = new Criteria<int>(OperatorType.Exact, page) }, Detail.Full).ToArray();
            if (cachedLocalCodes.Length > 0)
            {
                localCodes = cachedLocalCodes;
            }
            else
            {
                var localDataQuery = new LocalDataQuery()
                {
                    DisseminationDatabaseSettingEntityId = dataset.ParentId,
                    Query = dataset.Query
                };
                localDataQuery.ColumnNames.Add(columnEntity.Name);
                localDataQuery.MappingStoreId = columnEntity.StoreId;
                localDataQuery.Page = page;
                localDataQuery.Count = count;

                var columnCodesMap = this._localCodeRetrieverManager.GetLocalData(localDataQuery);

                var codes = columnCodesMap.Rows.Select(strings => strings[0]);
                localCodes = codes.Select(s => new LocalCodeEntity() { ObjectId = s, ParentId = columnEntity.EntityId, StoreId = columnEntity.StoreId }).ToArray();
            }

            FillDescription(dataset, columnEntity, localCodes);

            return this._descriptionRetrievalManager.AddDescription(dataset, columnEntity.EntityId, localCodes);
        }

        /// <summary>
        /// Gets the Cartesian data.
        /// </summary>
        /// <param name="datasetEntity">The dataset entity.</param>
        /// <param name="requestedColumns">The columns.</param>
        /// <param name="page">The page.</param>
        /// <param name="count">The count.</param>
        /// <returns>
        /// The <see cref="LocalCodeResults" />
        /// </returns>
        public LocalCodeResults GetAvailableData(DatasetEntity datasetEntity, ISet<string> requestedColumns, int page, int count)
        {
            var retrieverEngine = this._retrieverManager.GetRetrieverEngine<DataSetColumnEntity>(datasetEntity.StoreId);
            var datasetColumns = retrieverEngine.GetEntities(new EntityQuery() { ParentId = new Criteria<string>(OperatorType.Exact, datasetEntity.EntityId) }, Detail.Full).ToArray();

            if (requestedColumns != null && requestedColumns.Count > 0)
            {
                datasetColumns = datasetColumns.Where(entity => requestedColumns.Contains(entity.Name)).ToArray();
            }

            // TODO in old MA, it executes one SQL Query using the Mapping EntityID 
            //var localCodeRetriever = this._retrieverManager.GetRetrieverEngine<LocalCodeEntity>(datasetEntity.StoreId);

            //var cachedCodes = TryGetCachedCodes(datasetColumns, localCodeRetriever);

            LocalCodeResults validCodeSet = GetLocalCodeResults(datasetEntity, page, count, datasetColumns);

            //// old MA behavior, use cached codes only if all requested columns 
            //if (cachedCodes != null && datasetColumns.Length == cachedCodes.Count)
            //{
            //    validCodeSet = GetCachedLocalCodeResults(page, count, cachedCodes, datasetColumns);
            //}
            //else
            //{
            //    validCodeSet = GetLocalCodeResults(datasetEntity, page, count, datasetColumns);
            //}

            return this._descriptionRetrievalManager.AddDescription(validCodeSet);
        }

        /// <summary>
        /// Gets the cached codes.
        /// </summary>
        /// <param name="datasetColumns">The dataset columns.</param>
        /// <param name="localCodeRetriever">The local code retriever.</param>
        /// <returns></returns>
        private static List<IList<LocalCodeEntity>> TryGetCachedCodes(DataSetColumnEntity[] datasetColumns, IEntityRetrieverEngine<LocalCodeEntity> localCodeRetriever)
        {
            var cachedCodes = new List<IList<LocalCodeEntity>>();
            foreach (var datasetColumn in datasetColumns)
            {
                var entityQuery = new EntityQuery()
                                      {
                                          ParentId =
                                              new Criteria<string>(OperatorType.Exact, datasetColumn.EntityId)
                                      };
                var localCodeEntities = localCodeRetriever.GetEntities(entityQuery, Detail.Full).ToArray();
                if (localCodeEntities.Length > 0)
                {
                    cachedCodes.Add(localCodeEntities);
                }
                else
                {
                    return null;
                }
            }

            return cachedCodes;
        }

        /// <summary>
        /// Gets the cached local code results.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="count">The count.</param>
        /// <param name="cachedCodes">The cached codes.</param>
        /// <param name="datasetColumns"></param>
        /// <returns></returns>
        private static LocalCodeResults GetCachedLocalCodeResults(int page, int count, List<IList<LocalCodeEntity>> cachedCodes, DataSetColumnEntity[] datasetColumns)
        {
            var enumerable = cachedCodes.CartesianProduct().Skip(page * count);
            int totalRows = cachedCodes.Aggregate(1, (current, cachedCode) => current * cachedCode.Count);

            LocalCodeResults localCodeResults = new LocalCodeResults() { TotalRowCount = totalRows };

            foreach (var dataSetColumnEntity in datasetColumns)
            {
                localCodeResults.Columns.Add(dataSetColumnEntity);
            }

            foreach (var codes in enumerable)
            {
                localCodeResults.AddCodes(codes.ToArray());
            }

            localCodeResults.Page = page;
            return localCodeResults;
        }

        /// <summary>
        /// Gets the local code results from the DDB
        /// </summary>
        /// <param name="datasetEntity">The dataset entity.</param>
        /// <param name="page">The page.</param>
        /// <param name="count">The count.</param>
        /// <param name="datasetColumns">The dataset columns.</param>
        /// <returns>The <see cref="LocalCodeResults"/></returns>
        private LocalCodeResults GetLocalCodeResults(
            DatasetEntity datasetEntity,
            int page,
            int count,
            DataSetColumnEntity[] datasetColumns)
        {
            var localDataQuery = new LocalDataQuery()
                                     {
                                         DisseminationDatabaseSettingEntityId = datasetEntity.ParentId,
                                         Query = datasetEntity.Query,
                                         DatasetProperty = datasetEntity.DatasetPropertyEntity
                                     };
            foreach (var dataSetColumnEntity in datasetColumns)
            {
                localDataQuery.ColumnNames.Add(dataSetColumnEntity.Name);
            }

            localDataQuery.MappingStoreId = datasetEntity.StoreId;
            localDataQuery.Page = page;
            localDataQuery.Count = count;
            var validCodeSet = this._localCodeRetrieverManager.GetValidCodeSet(localDataQuery);
            for (int index = 0; index < datasetColumns.Length; index++)
            {
                var dataSetColumnEntity = datasetColumns[index];
                if (!string.Equals(dataSetColumnEntity.Name, validCodeSet.Columns[index].Name))
                {
                    throw new InvalidOperationException("Dataset column order mismatch");
                }

                validCodeSet.Columns[index] = dataSetColumnEntity;
            }

            validCodeSet.RelatedEntities[datasetEntity.TypeOfEntity] = datasetEntity.EntityId;
            foreach (var localCode in validCodeSet.LocalCodes)
            {
                for (int index = 0; index < datasetColumns.Length; index++)
                {
                    localCode[index].ParentId = datasetColumns[index].EntityId;
                }
            }

            return validCodeSet;
        }

        /// <summary>
        /// Fills the description.
        /// </summary>
        /// <param name="dataset">The dataset.</param>
        /// <param name="columnEntity">The column entity.</param>
        /// <param name="localCodes">The local codes.</param>
        private void FillDescription(DatasetEntity dataset, DataSetColumnEntity columnEntity, IList<LocalCodeEntity> localCodes)
        {
            this._descriptionRetrievalManager.AddDescription(dataset, columnEntity.EntityId, localCodes);
        }

        /// <summary>
        /// Gets the data set.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The <see cref="DatasetEntity"/></returns>
        /// <exception cref="ResourceNotFoundException">Could not locate requested Dataset</exception>
        private DatasetEntity GetDataSet(DataSetColumnEntity entity)
        {
            var entityRetrieverEngine = _retrieverManager.GetRetrieverEngine<DatasetEntity>(entity.StoreId);
            var datasetEntity =
                entityRetrieverEngine.GetEntities(
                    new EntityQuery { EntityId = new Criteria<string>(OperatorType.Exact, entity.ParentId) },
                    Detail.Full).FirstOrDefault();
            if (datasetEntity == null)
            {
                throw new ResourceNotFoundException("Could not locate requested Dataset");
            }

            return datasetEntity;
        }

        /// <inheritdoc/>
        public void WriteAvailableData(DatasetEntity datasetEntity, ISet<string> requestedColumns, int page, int count, ILocalDataWriter writer)
        {
            EntityQuery query = new EntityQuery() { ParentId = new Criteria<string>(OperatorType.Exact, datasetEntity.EntityId) };
            var datasetColumns = this._retrieverManager.GetEntities<DataSetColumnEntity>(datasetEntity.StoreId, query, Detail.Full).ToArray();


            if (requestedColumns != null && requestedColumns.Count > 0)
            {
                datasetColumns = datasetColumns.Where(entity => requestedColumns.Contains(entity.Name))
                    .GroupBy(x => x.Name).Select(group => group.FirstOrDefault()).ToArray();
            }

            var localDataQuery = new LocalDataQuery()
            {
                DisseminationDatabaseSettingEntityId = datasetEntity.ParentId,
                Query = datasetEntity.Query,
                DatasetProperty = datasetEntity.DatasetPropertyEntity
            };
            localDataQuery.MappingStoreId = datasetEntity.StoreId;
            localDataQuery.Page = page;
            localDataQuery.Count = count;
            localDataQuery.CheckForErrors = true;

            var descriptions = new Dictionary<string, Dictionary<string, string>>(StringComparer.Ordinal);
            for (int i = 0; i < datasetColumns.Length; i++)
            {
                DataSetColumnEntity dataSetColumnEntity = datasetColumns[i];
                localDataQuery.ColumnNames.Add(dataSetColumnEntity.Name);
                // We take the description sources as it could be very slow to execute the dataset.query joined with description tables
                Dictionary<string, string> codeMap = _descriptionRetrievalManager.GetMap(datasetEntity, dataSetColumnEntity);
                if (codeMap != null)
                {
                    descriptions[dataSetColumnEntity.Name] = codeMap;
                }
            }

            using (LocalCodeWriter localCodeWriter = new LocalCodeWriter(writer, datasetColumns, descriptions))
            {
                _localCodeRetrieverManager.WriteLocalData(localDataQuery, localCodeWriter);
            }
         }

        /// <inheritdoc/>
        public void Validate(DataSetColumnEntity columnEntity, ILocalCodeValidator validator)
        {
            if (columnEntity is null)
            {
                throw new ArgumentNullException(nameof(columnEntity));
            }

            DatasetEntity dataset = GetDataSet(columnEntity);
            var localDataQuery = new LocalDataQuery()
            {
                DisseminationDatabaseSettingEntityId = dataset.ParentId,
                Query = dataset.Query
            };

            localDataQuery.MappingStoreId = dataset.StoreId;
            localDataQuery.ColumnNames.Add(columnEntity.Name);

            this._localCodeRetrieverManager.ValidateValues(localDataQuery, validator);
        }
    }
}