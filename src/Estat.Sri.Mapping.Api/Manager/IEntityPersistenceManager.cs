// -----------------------------------------------------------------------
// <copyright file="IEntityPersistenceManager.cs" company="EUROSTAT">
//   Date Created : 2017-02-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Manager
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// This manager is responsible for adding, deleting and updating entities 
    /// </summary>
    public interface IEntityPersistenceManager
    {
        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        TEntity Add<TEntity>(TEntity entity) where TEntity : IEntity;

        /// <summary>
        /// Adds the specified entities.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entities">The entity.</param>
        /// <returns>The entity that were added with the entity ID</returns>
        IEnumerable<TEntity> AddEntities<TEntity>(IEnumerable<TEntity> entities) where TEntity : IEntity;

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        void Update<TEntity>(TEntity entity) where TEntity : IEntity;

        /// <summary>
        /// Deletes the specified entity identifier.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="entityId">The entity identifier.</param>
        void Delete<TEntity>(string storeId, string entityId) where TEntity : IEntity;

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="mappingStoreIdentifier">The mapping store identifier.</param>
        /// <returns>The <see cref="IEntityPersistenceEngine{TEntity}"/></returns>
        /// <exception cref="System.NotImplementedException">Could not find an appropriate engine</exception>
        IEntityPersistenceEngine<TEntity> GetEngine<TEntity>(string mappingStoreIdentifier) where TEntity : IEntity;

        /// <summary>
        /// Add or update entities read from <paramref name="input"/> to Mapping Store with <paramref name="storeId"/>
        /// Optionally if provided write the output to <paramref name="responseWriter"/>
        /// </summary>
        /// <param name="storeId">The mapping store id</param>
        /// <param name="input">The input to parse</param>
        /// <param name="responseWriter">An optional writer to write the entities that were added/updated</param>
        /// <param name="entityType"></param>
        void AddEntities(string storeId, IEntityStreamingReader input, IEntityStreamingWriter responseWriter,EntityType entityType);

        /// <summary>
        /// TODO should this be a different method or the same like AddEntities
        /// Add or update entities read from <paramref name="input"/> to Mapping Store with <paramref name="storeId"/>
        /// Optionally if provided write the output to <paramref name="responseWriter"/>
        /// </summary>
        /// <param name="storeId">The mapping store id</param>
        /// <param name="input">The input to parse</param>
        /// <param name="responseWriter">An optional writer to write the entities that were added/updated</param>
        void Import(string storeId, IEntityStreamingReader input, IEntityStreamingWriter responseWriter);
    }
}