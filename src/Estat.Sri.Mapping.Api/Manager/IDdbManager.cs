// -----------------------------------------------------------------------
// <copyright file="IDdbManager.cs" company="EUROSTAT">
//   Date Created : 2017-05-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Manager
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// This manager is responsible for testing and accessing a DDB given it Entity ID
    /// </summary>
    public interface IDdbManager
    {
        /// <summary>
        /// Gets the status of the connection of the DDB.
        /// </summary>
        /// <param name="ddbEntity">The mapping store identifier.</param>
        /// <returns>
        ///   <see cref="ActionResults.Success" /> if it can connect; otherwise an <see cref="ActionResult" /> with <see cref="StatusType.Error" />.
        /// </returns>
        IActionResult TestConnection(IConnectionEntity ddbEntity);

        /// <summary>
        /// Gets the status of the connection of the DDB.
        /// </summary>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        /// <param name="entityId">The DDB settings entity identifier.</param>
        /// <returns>
        ///   <see cref="ActionResults.Success" /> if it can connect; otherwise an <see cref="ActionResult" /> with <see cref="StatusType.Error" />.
        /// </returns>
        IActionResult GetStatus(string mappingStoreId, string entityId);

        /// <summary>
        /// Gets the database objects.
        /// </summary>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        /// <param name="entityId">The DDB settings entity identifier.</param>
        /// <returns>
        /// The list of tables and/or views
        /// </returns>
        IList<IDatabaseObject> GetDatabaseObjects(string mappingStoreId, string entityId);

        /// <summary>
        /// Gets the fields.
        /// </summary>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        /// <param name="entityId">The DDB settings entity identifier.</param>
        /// <param name="databaseObjectId">The database object identifier.</param>
        /// <returns>
        /// The list of fields of a specific database object (table or view)
        /// </returns>
        IList<IFieldInfo> GetFields(string mappingStoreId, string entityId, string databaseObjectId);
    }
}