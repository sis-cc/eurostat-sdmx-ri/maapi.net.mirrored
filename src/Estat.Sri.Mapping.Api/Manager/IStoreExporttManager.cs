﻿// -----------------------------------------------------------------------
// <copyright file="IStoreExporttManager.cs" company="EUROSTAT">
//   Date Created : 2017-08-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Text;

namespace Estat.Sri.Mapping.Api.Manager
{
    public interface IStoreExporttManager
    {
        /// <summary>
        /// Exports all entities in the Mapping Store (no SDMX) to the specified stream.
        /// </summary>
        /// <param name="stream">The stream the XML.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="sid">The sid.</param>
        void Export(Stream stream, Encoding encoding, string sid);
        /// <summary>
        /// Exports all entities in the Mapping Store and SDMX to the specified stream.
        /// </summary>
        /// <param name="stream">The stream to write the ZIP archive.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="sid">The sid.</param>
        void ExportWithSdmx(Stream stream, Encoding encoding, string sid);
    }
}