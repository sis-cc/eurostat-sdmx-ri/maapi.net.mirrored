// -----------------------------------------------------------------------
// <copyright file="TimePreFormattedEntityReader.cs" company="EUROSTAT">
//   Date Created : 2021-05-24
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;

namespace Estat.Sri.Mapping.Api.Engine
{
    /// <summary>
    /// The reader for time-pre-formatted entity.
    /// </summary>
    public class TimePreFormattedEntityReader : EntityReader<TimePreFormattedEntity>
    {
        private readonly DataSetColumnEntityReader _datasetColumnEntityReader = new DataSetColumnEntityReader();

        /// <summary>
        /// Initializes a new instance of the <see cref="TimePreFormattedEntity"/> class.
        /// </summary>
        public TimePreFormattedEntityReader()
        {
            _specificProperties.Add("outputColumn");
            _specificProperties.Add("fromColumn");
            _specificProperties.Add("toColumn");
        }

        /// <summary>
        /// Parses a json object to <see cref="TimePreFormattedEntity"/> class.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> class.</param>
        /// <param name="mappingSetId">The mapping set Id.</param>
        /// <returns>The <see cref="TimePreFormattedEntity"/> class with the values.</returns>
        public TimePreFormattedEntity ParseEntity(JsonReader reader, string mappingSetId)
        {
            var entity = new TimePreFormattedEntity();

            ExtractValueFor(reader, entity);
            entity.EntityId = mappingSetId;

            return entity;
        }

        /// <summary>
        /// Parses the specific properties.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> class.</param>
        /// <param name="entity">The <see cref="TimePreFormattedEntity"/> entity.</param>
        protected override void ExtractSpecificProperties(JsonReader reader, TimePreFormattedEntity entity)
        {
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".outputColumn"))
            {
                entity.OutputColumn = ExtractDatasetColumn(reader);
            }

            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".fromColumn"))
            {
                entity.FromColumn = ExtractDatasetColumn(reader);
            }

            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".toColumn"))
            {
                entity.ToColumn = ExtractDatasetColumn(reader);
            }
        }

        private DataSetColumnEntity ExtractDatasetColumn(JsonReader reader)
        {
            return _datasetColumnEntityReader.ParseEntity(reader);
        }
    }
}
