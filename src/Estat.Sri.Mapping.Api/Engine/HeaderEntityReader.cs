using System;
using System.Collections.Generic;
using System.Globalization;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.SdmxXmlConstants;
using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class HeaderEntityReader : NameableEntityReader<HeaderEntity>
    {
        public HeaderEntityReader()
        {
            _propertyNames.Add("dataSetAgencyId",(entity,value)=>entity.SdmxHeader.AdditionalAttribtues["DataSetAgency"]= value);
            _propertyNames.Add("structureType", (entity, value) => entity.SdmxHeader.AdditionalAttribtues[nameof(ElementNameTable.StructureUsage)] = value);
            _propertyNames.Add("dataSetId", (entity, value) => entity.SdmxHeader.AdditionalAttribtues[nameof(ElementNameTable.DataSetID)] = value);
            _propertyNames.Add("dataSetAction", (entity, value) => entity.SdmxHeader.AdditionalAttribtues[nameof(ElementNameTable.DataSetAction)] = value);
            _propertyNames.Add("source",AddSource);
            // we need to add name to SdmxHeader not Entity.Name
            _propertyNames.Remove("name");
            _propertyNames.Add("name",AddName);
            _propertyNames.Add("test",(entity,value)=>entity.SdmxHeader.Test = bool.Parse(value));
            _specificProperties.Add("receiver");
            _specificProperties.Add("sender");
        }

        private static void AddSource(HeaderEntity entity, string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                entity.SdmxHeader.AddSource(new TextTypeWrapperImpl("en",value,null));
            }
        }

        private static void AddName(HeaderEntity entity, string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                entity.SdmxHeader.AddName(new TextTypeWrapperImpl("en", value, null));
            }
        }

        public HeaderEntity ParseEntity(JsonReader reader)
        {
            HeaderEntity header = new HeaderEntity();
            header.SdmxHeader = new HeaderImpl("test", "test");
            ExtractValueFor(reader,header);
            return header;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, HeaderEntity entity)
        {
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("receiver"))
            {
                while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                {
                    if (reader.TokenType == JsonToken.StartObject)
                    { 
                        entity.SdmxHeader.AddReciever(ExtractParty(reader));
                    }
                }
            }

            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("sender"))
            {
                entity.SdmxHeader.Sender = ExtractParty(reader);
            }
        }

        private IParty ExtractParty(JsonReader reader)
        {
            IList<ITextTypeWrapper> name = null;
            string id = null;
            IList<IContact> contacts  = new List<IContact>();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.String && reader.Path.Contains("id"))
                {
                    id = reader.Value?.ToString();
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("name"))
                {
                    name = ExtractPartyNames(reader);
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("contacts"))
                {
                    while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                    {
                        if (reader.TokenType == JsonToken.StartObject)
                        {
                            contacts.Add(ExtractContact(reader));
                        }
                    }
                }
            }

            return new PartyCore(name,id,contacts,null);
        }

        private IContact ExtractContact(JsonReader reader)
        {
            var contact = new ContactCore();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("department"))
                {
                    contact.Departments = ExtractPartyNames(reader);
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("role"))
                {
                    contact.Role = ExtractPartyNames(reader);
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("name"))
                {
                    contact.Name = ExtractPartyNames(reader);
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("telephone"))
                {
                    while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                    {
                        if (reader.TokenType == JsonToken.String)
                        {
                            contact.Telephone.Add(reader.Value?.ToString());
                        }
                    }
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("fax"))
                {
                    while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                    {
                        if (reader.TokenType == JsonToken.String)
                        {
                            contact.Fax.Add(reader.Value?.ToString());
                        }
                    }
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("email"))
                {
                    while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                    {
                        if (reader.TokenType == JsonToken.String)
                        {
                            contact.Email.Add(reader.Value?.ToString());
                        }
                    }
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("uri"))
                {
                    while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                    {
                        if (reader.TokenType == JsonToken.String)
                        {
                            contact.Uri.Add(reader.Value?.ToString());
                        }
                    }
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("x400"))
                {
                    while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                    {
                        if (reader.TokenType == JsonToken.String)
                        {
                            contact.X400.Add(reader.Value?.ToString());
                        }
                    }
                }
            }

            return contact;
        }

        private IList<ITextTypeWrapper> ExtractPartyNames(JsonReader reader)
        {
            var names = new List<ITextTypeWrapper>();
            reader.Read();
            if(reader.TokenType == JsonToken.Null)
            {
                return names;
            }
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                var name = GetName(reader);
                if (!string.IsNullOrWhiteSpace(name.Item1))
                {
                    names.Add(new TextTypeWrapperImpl(name.Item1, name.Item2,null));
                }
            }
            return names;
        }

        private static Tuple<string, string> GetName(JsonReader reader)
        {
            var key = string.Empty;
            var value = string.Empty;

            if (reader.TokenType == JsonToken.PropertyName)
            {
                key = reader.Value?.ToString();
            }
            reader.Read();
            if (reader.TokenType == JsonToken.String)
                {
                    value = reader.Value?.ToString();
                }
            return new Tuple<string, string>(key, value);
        }
    }
}
