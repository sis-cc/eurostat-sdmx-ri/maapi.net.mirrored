using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class ColumnDescriptionSourceEntityReader : EntityReader<ColumnDescriptionSourceEntity>
    {
        public ColumnDescriptionSourceEntityReader()
        {
            _propertyNames.Add("table", (entity, value) => entity.DescriptionTable = value);
            _propertyNames.Add("relationField", (entity, value) => entity.RelatedField = value);
            _propertyNames.Add("descriptionField", (entity, value) => entity.DescriptionField = value);
        }

        public ColumnDescriptionSourceEntity ParseEntity(JsonReader reader)
        {
            ColumnDescriptionSourceEntity entity = new ColumnDescriptionSourceEntity();
            base.ExtractValueFor(reader, entity);
            return entity;
        }
    }
}
