// -----------------------------------------------------------------------
// <copyright file="IDescriptionRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2014-09-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Engine
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    ///     The DescriptionRetrievalEngine interface.
    /// </summary>
    public interface IDescriptionRetrievalEngine
    {
        /// <summary>
        ///     Fills the specified <paramref name="localCodelist" /> description.
        /// </summary>
        /// <param name="descriptionSource">The description source.</param>
        /// <param name="localCodelist">The local code list.</param>
        void FillDescription(ColumnDescriptionSourceEntity descriptionSource, IEnumerable<LocalCodeEntity> localCodelist);

        /// <summary>
        /// Gets a map of a code and its description
        /// </summary>
        /// <param name="ddbEntityId">The DDB connection id which will use to query</param>
        /// <param name="descSource">The entity containing the table and fields to quey</param>
        /// <returns>The dictionary where the key is the code id (*Not entity id*) and the value it's description </returns>
        Dictionary<string, string> GetMap(string ddbEntityId, ColumnDescriptionSourceEntity descSource);
    }
}