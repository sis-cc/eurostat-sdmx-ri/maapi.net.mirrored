// -----------------------------------------------------------------------
// <copyright file="AppConfigStore.cs" company="EUROSTAT">
//   Date Created : 2017-03-27
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.InteropServices;
    using Estat.Sri.Mapping.Api.Exceptions;

    using log4net;

    /// <summary>
    /// The APP CONFIG store.
    /// </summary>
    public class AppConfigStore : IConfigurationStoreEngine<ConnectionStringSettings>
    {
        /// <summary>
        ///     The protection provider
        /// </summary>
        private const string ProtectionProvider = "ConfiguredProtectedConfigurationProvider";

        /// <summary>
        ///     This should be "configProtectedData"
        /// </summary>
        private const string ProtectionProviderSectionName = "configProtectedData";

        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(AppConfigStore));

        /// <summary>
        /// The location
        /// </summary>
        private readonly string _location;

        private static readonly bool _useEncryption = UseEncryption();

        private IEnumerable<ConnectionStringSettings> _settings;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppConfigStore"/> class.
        /// </summary>
        public AppConfigStore()
        {
            this._location = ConfigurationManager.AppSettings["configLocation"];
            _log.Debug("AppConfigStore initialized");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppConfigStore"/> class.
        /// </summary>
        /// <param name="location">The location.</param>
        public AppConfigStore(string location)
        {
            this._location = location;
            _log.Debug("AppConfigStore initialized");

        }

        /// <summary>
        /// Gets the settings.
        /// </summary>
        /// <returns>
        /// The list of settings
        /// </returns>
        public IEnumerable<ConnectionStringSettings> GetSettings()
        {
            //if (this._settings != null)
            //{
            //    return this._settings;
            //}

            var config = this.GetConfiguration();
            if (config == null)
            {
                this._settings = Enumerable.Empty<ConnectionStringSettings>();

                return this._settings;
            }

            _log.DebugFormat(CultureInfo.InvariantCulture, "CONFIG: Loading configuration from: {0}", config.FilePath);

            ConnectionStringsSection configSection = config.ConnectionStrings;
            _log.DebugFormat(CultureInfo.InvariantCulture, "CONFIG: Found {0} Connection Strings", configSection.ConnectionStrings.Count);

            if (_useEncryption && configSection.SectionInformation.IsProtected)
            {
                configSection.SectionInformation.UnprotectSection();
            }

            this._settings = configSection.ConnectionStrings.Cast<ConnectionStringSettings>();
            return this._settings;
        }

        /// <summary>
        /// Saves the settings.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public void SaveSettings(ConnectionStringSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }
            this._settings = null;
            Configuration config = this.GetConfiguration();
            if (config == null)
            {
                throw new MissingInformationException("Cannot open configuration for editing");
            }

            var configFileName = config.FilePath;

            _log.InfoFormat("Loaded configuration from file {0}", config.FilePath);
            ConnectionStringsSection configSection = config.ConnectionStrings;

            if (_useEncryption && configSection.SectionInformation.IsProtected)
            {
                configSection.SectionInformation.UnprotectSection();
            }

            // just changing the connection string doesn't seem to work...
            configSection.ConnectionStrings.Remove(settings.Name);

            config.ConnectionStrings.ConnectionStrings.Add(settings);
            if (_useEncryption) 
            {
                CreateProtectProvider(config);

                configSection.SectionInformation.ProtectSection(ProtectionProvider);
            }

            _log.InfoFormat("Saving to configuration file {0}", configFileName);
            if (string.Equals(configFileName, config.FilePath))
            {
                config.Save(ConfigurationSaveMode.Modified);
            }
            else
            {
                config.SaveAs(configFileName, ConfigurationSaveMode.Modified);
            }

            _log.DebugFormat("Wrote configuration file: {0}", configFileName);

            ConfigurationManager.RefreshSection(config.ConnectionStrings.SectionInformation.Name);
            ConfigurationManager.RefreshSection("configuration");
        }

        /// <summary>
        /// Deletes the settings.
        /// </summary>
        /// <param name="name">The name.</param>
        public void DeleteSettings(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }
            this._settings = null;
            Configuration config = this.GetConfiguration();
            if (config == null)
            {
                throw new MissingInformationException("Cannot open configuration for editing");
            }

            var configFileName = config.FilePath;

            _log.InfoFormat("Loaded configuration from file {0}", config.FilePath);
            ConnectionStringsSection configSection = config.ConnectionStrings;

            if (_useEncryption && configSection.SectionInformation.IsProtected)
            {
                configSection.SectionInformation.UnprotectSection();
            }

            if (config.ConnectionStrings.ConnectionStrings[name] == null)
            {
                throw new NullReferenceException("The connection does not exist");
            }

            config.ConnectionStrings.ConnectionStrings.Remove(name);
            if (_useEncryption) 
            {
                CreateProtectProvider(config);

                configSection.SectionInformation.ProtectSection(ProtectionProvider);
            }

            _log.InfoFormat("Saving to configuration file {0}", configFileName);
            if (string.Equals(configFileName, config.FilePath))
            {
                config.Save(ConfigurationSaveMode.Modified);
            }
            else
            {
                config.SaveAs(configFileName, ConfigurationSaveMode.Modified);
            }

            _log.DebugFormat("Wrote configuration file: {0}", configFileName);
            ConfigurationManager.RefreshSection("configuration");
            ConfigurationManager.RefreshSection(config.ConnectionStrings.SectionInformation.Name);
        }

        /// <summary>
        ///     Gets or creates protected provider section
        /// </summary>
        /// <param name="config">The configuration</param>
        private static void CreateProtectProvider(Configuration config)
        {
            if (!_useEncryption)
            {
                return;
            }
            var protectedConfigurationSection = config.GetSection(ProtectionProviderSectionName) as ProtectedConfigurationSection;

            bool found = false;
            if (protectedConfigurationSection != null)
            {
                foreach (ProviderSettings provider in protectedConfigurationSection.Providers)
                {
                    if (provider.Name.Equals(ProtectionProvider))
                    {
                        found = true;
                        break;
                    }
                }
            }
            else
            {
                protectedConfigurationSection = new ProtectedConfigurationSection();
                config.Sections.Add(ProtectionProviderSectionName, protectedConfigurationSection);
            }

            if (!found)
            {
                protectedConfigurationSection.Providers.Add(new ProviderSettings(ProtectionProvider, typeof(DpapiProtectedConfigurationProvider).AssemblyQualifiedName));
            }
        }

        /// <summary>
        /// Gets the connection string settings.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>The <see cref="ConnectionStringSettings" /></returns>
        private static ConnectionStringSettings GetConnectionStringSettings(Configuration config, string storeId)
        {
            _log.DebugFormat("Reading configuration file: {0}", config.FilePath);
            ConnectionStringsSection configSection = config.ConnectionStrings;
            if (_useEncryption && configSection.SectionInformation.IsProtected)
            {
                configSection.SectionInformation.UnprotectSection();
            }

            ConnectionStringSettings connectionStringSetting = configSection.ConnectionStrings[storeId];
            return connectionStringSetting;
        }


        private Configuration GetConfiguration()
        {
            Configuration config;
            if (string.IsNullOrWhiteSpace(this._location))
            {
                ConfigurationManager.RefreshSection("ConnectionStrings");
                try
                {
                    config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                }
                catch (ArgumentException e)
                {
                    // running probably from a web service
                    _log.Error(e);
                    return null;
                }
            }
            else
            {
                ExeConfigurationFileMap configMap = new ExeConfigurationFileMap
                {
                    ExeConfigFilename = this._location
                };
                config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
            }
            return config;
        }
        private static bool UseEncryption()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return true;
            }

            _log.Warn("Encryption of connection strings is not currently supported on non-Windows platforms");
            return false;
        }
    }
}