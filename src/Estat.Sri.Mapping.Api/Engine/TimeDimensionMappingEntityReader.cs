using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Model.AdvancedTime;
using Estat.Sri.Mapping.Api.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class TimeDimensionMappingEntityReader : EntityReader<TimeDimensionMappingEntity>
    {
        private TimeTranscodingEntityReader _timeTranscodingEntityReader = new TimeTranscodingEntityReader();
        private DataSetColumnEntityReader _datasetColumnEntityReader = new DataSetColumnEntityReader();
        private TimePreFormattedEntityReader _timePreFormattedEntityReader = new TimePreFormattedEntityReader();
        public TimeDimensionMappingEntityReader()
        {
            _propertyNames.Add("type", (entity, value) =>
            {
                if (Enum.TryParse(value, true,
                        out TimeDimensionMappingType timeDimensionMappingType))
                {
                    entity.TimeMappingType = timeDimensionMappingType;
                }
            });
           _propertyNames.Add("defaultValue", (entity, value) => entity.DefaultValue = value);
           _propertyNames.Add("constantValue", (entity, value) => entity.ConstantValue = value);
           _propertyNames.Add("column", (entity, value) => entity.SimpleMappingColumn = value);
            _specificProperties.Add("timePreFormatted");
           _specificProperties.Add("timeTranscoding");
           _specificProperties.Add("transcodingAdvancedTime");
        }

        public TimeDimensionMappingEntity ParseEntity(JsonReader reader)
        {
            var entity = new TimeDimensionMappingEntity();
            ExtractValueFor(reader,entity);
            entity.AutoDetectTypeIfNotSet();
            return entity;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, TimeDimensionMappingEntity entity)
        {
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("timeTranscoding"))
            {
                var timeTranscoding = new List<TimeTranscodingEntity>();
                while (reader.Read() && HasTranscoding(reader))
                {
                    var timeTranscodingEntity = _timeTranscodingEntityReader.ParseEntity(reader);
                    timeTranscoding.Add(timeTranscodingEntity);
                }

                if (timeTranscoding.Count > 0)
                {
                    entity.Transcoding = TimeTranscodingConversionHelper.Convert(timeTranscoding, "FREQ");
                }
            } 
            else if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("transcodingAdvancedTime"))
            {
                    ReadAdvanced(entity,reader);
            }
            else  if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".timePreFormatted"))
            {
                entity.PreFormatted = _timePreFormattedEntityReader.ParseEntity(reader, entity.EntityId);
            }

        }

        private bool HasTranscoding(JsonReader reader)
        {
            if(reader.TokenType == JsonToken.StartObject)
            {
                reader.Read();
            }
            return reader.TokenType == JsonToken.PropertyName;
        }

        private void ReadAdvanced(TimeDimensionMappingEntity entity, JsonReader reader)
        {
            var transcodingAdvancedEntity = new TimeTranscodingAdvancedEntity();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("criteriaColumn"))
                {
                    transcodingAdvancedEntity.CriteriaColumn = _datasetColumnEntityReader.ParseEntity(reader);
                }
                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("isFrequencyBased"))
                {
                    reader.Read();
                    if (reader.Value is bool readerValue)
                    {
                        transcodingAdvancedEntity.IsFrequencyDimension = readerValue;
                    }
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("timeFormatConfigurations"))
                {
                    ExtractTimeFormatConfigurations(reader, transcodingAdvancedEntity);
                }
            }

            if (transcodingAdvancedEntity.CriteriaColumn != null || transcodingAdvancedEntity.IsFrequencyDimension
                || transcodingAdvancedEntity.TimeFormatConfigurations.Count > 0)
            {
                entity.Transcoding = transcodingAdvancedEntity;
            }
        }

        private void ExtractTimeFormatConfigurations(JsonReader reader, TimeTranscodingAdvancedEntity timeTranscodingAdvancedEntity)
        {
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                var timeFormatConfiguration = ExtractTimeFormatConfiguration(reader);
                timeTranscodingAdvancedEntity.Add(timeFormatConfiguration);
            } 
        }
        private DurationMappingEntity ExtractDurationMap(JsonReader reader)
        {
            DurationMappingEntity durationMappingEntity = new DurationMappingEntity();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("constantValue"))
                {
                    reader.Read();
                    durationMappingEntity.ConstantValue = reader.Value as string;
                }
                else if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("defaultValue"))
                {
                    reader.Read();
                    durationMappingEntity.DefaultValue = reader.Value as string;
                }
                else if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("column"))
                {
                   durationMappingEntity.Column= _datasetColumnEntityReader.ParseEntity(reader);
                }
                else if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("rule"))
                {
                    ExtractDurationMapRules(reader, durationMappingEntity.TranscodingRules);
                }
            }

            return durationMappingEntity;
        }
        private TimeFormatConfiguration ExtractTimeFormatConfiguration(JsonReader reader)
        {
            var timeFormatConfiguration = new TimeFormatConfiguration();
            TimeParticleConfiguration startConfig = null;
            TimeParticleConfiguration endConfig = null;
            DurationMappingEntity durationMap = null;
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("startConfig"))
                {
                    startConfig = ExtractTimeParticleConfiguration(reader);
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("endConfig"))
                {
                    endConfig = ExtractTimeParticleConfiguration(reader);
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("durationMap"))
                {
                    durationMap = ExtractDurationMap(reader);
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("outputFormat"))
                {
                    reader.Read();
                    var timeFormat = (TimeFormatEnumType)Enum.Parse(typeof(TimeFormatEnumType), reader.Value?.ToString());
                    timeFormatConfiguration.OutputFormat = TimeFormat.GetFromEnum(timeFormat);
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("criteriaValue"))
                {
                    reader.Read();
                    timeFormatConfiguration.CriteriaValue = reader.Value?.ToString();
                }
            }
            timeFormatConfiguration.SetConfiguration(durationMap, startConfig, endConfig);
            return timeFormatConfiguration;
        }

        private TimeParticleConfiguration ExtractTimeParticleConfiguration(JsonReader reader)
        {
            TimeParticleConfiguration timeParticleConfiguration = new TimeParticleConfiguration();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("dateColumn"))
                {
                    timeParticleConfiguration.DateColumn = _datasetColumnEntityReader.ParseEntity(reader);
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("year"))
                {
                    timeParticleConfiguration.Year = ExtractTimeParticleYear(reader);
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("period"))
                {
                    timeParticleConfiguration.Period = ExtractTimeParticlePeriod(reader);
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("format"))
                {
                    reader.Read();
                    timeParticleConfiguration.Format = (DisseminationDatabaseTimeFormat)Enum.Parse(typeof(DisseminationDatabaseTimeFormat),reader.Value?.ToString());
                }
            }
            return timeParticleConfiguration;
        }

        private PeriodTimeRelatedColumnInfo ExtractTimeParticlePeriod(JsonReader reader)
        {
            PeriodTimeRelatedColumnInfo periodTimeRelatedColumn = new PeriodTimeRelatedColumnInfo();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("period.start"))
                {
                    periodTimeRelatedColumn.Start = reader.ReadAsNullableInt();
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("period.length"))
                {
                    periodTimeRelatedColumn.Length = reader.ReadAsNullableInt();
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("column"))
                {
                    periodTimeRelatedColumn.Column = _datasetColumnEntityReader.ParseEntity(reader);
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("rule"))
                {
                    while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                    {
                        var periodCodeMap = ExtractPeriodCodeMap(reader);

                        periodTimeRelatedColumn.Rules.Add(periodCodeMap);
                    }
                }
             }
            return periodTimeRelatedColumn;
        }
        private void ExtractDurationMapRules(JsonReader reader, Dictionary<string, string> rules)
        {
            string key = null;
            string value = null;
            while (reader.Read() && reader.TokenType != JsonToken.EndArray)
            {
                switch (reader.TokenType)
                {
                    case JsonToken.PropertyName:
                        if (reader.Path.EndsWith(".sdmxCode"))
                        {
                            reader.Read();
                            key = reader.Value?.ToString();
                        }
                        else if (reader.Path.EndsWith(".localCode"))
                        {
                            reader.Read();
                            value = reader.Value?.ToString();
                        }
                        break;
                    case JsonToken.EndObject:
                        if (key != null)
                        {
                            rules[key] = value;
                            key = null;
                            value = null;
                        }
                        break;
                }
            }

        }
        private PeriodCodeMap ExtractPeriodCodeMap(JsonReader reader)
        {   PeriodCodeMap periodCodeMap = new PeriodCodeMap();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("sdmxPeriodCode"))
                {
                    reader.Read();
                    periodCodeMap.SdmxPeriodCode = reader.Value?.ToString();
                }
                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("localPeriodCode"))
                {
                    reader.Read();
                    periodCodeMap.LocalPeriodCode = reader.Value?.ToString();
                }
            }
            return periodCodeMap;
        }

        private TimeRelatedColumnInfo ExtractTimeParticleYear(JsonReader reader)
        {
            TimeRelatedColumnInfo timeRelatedColumnInfo = new TimeRelatedColumnInfo();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("year.start"))
                {
                    timeRelatedColumnInfo.Start = reader.ReadAsNullableInt();
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("year.length"))
                {
                    timeRelatedColumnInfo.Length = reader.ReadAsNullableInt();
                }
                if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith("column"))
                { 
                    timeRelatedColumnInfo.Column = _datasetColumnEntityReader.ParseEntity(reader);
                }
            }

            return timeRelatedColumnInfo;
        }

        
    }
}
