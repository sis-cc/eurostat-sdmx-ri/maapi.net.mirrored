using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class DataSourceEntityReader : EntityReader<DataSourceEntity>
    {
        public DataSourceEntityReader(){
            _propertyNames.Add("dataUrl", (entity, value) => entity.DataUrl = value);
            _propertyNames.Add("wsdlUrl", (entity, value) => entity.WSDLUrl = value);
            _propertyNames.Add("wadlUrl", (entity, value) => entity.WADLUrl = value);
            _propertyNames.Add("isRESTDatasource", (entity, value) => entity.IsRest = bool.Parse(value));
            _propertyNames.Add("isSimpleDatasource", (entity, value) => entity.IsSimple = bool.Parse(value));
            _propertyNames.Add("isWebServiceDatasource", (entity, value) => entity.IsWs = bool.Parse(value));
        }

        public DataSourceEntity ParseEntity(JsonReader reader)
        {
            DataSourceEntity entity = new DataSourceEntity();
            base.ExtractValueFor(reader, entity);
            return entity;
        }
    }
}
