﻿// -----------------------------------------------------------------------
// <copyright file="IDatabaseSchemaBrowser.cs" company="EUROSTAT">
//   Date Created : 2017-04-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Engine
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Common;

    using Estat.Sri.Mapping.Api.Model;

    public interface IDatabaseSchemaBrowser
    {
        /// <summary>
        /// Gets the database objects. E.g. Tables or Views
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The list of <see cref="DatabaseObject" />
        /// </returns>
        IEnumerable<IDatabaseObject> GetDatabaseObjects(ConnectionStringSettings settings);

        /// <summary>
        /// Gets the fields of a database object.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="databaseObject">The database object.</param>
        /// <returns>
        /// The list of <see cref="IFieldInfo" />
        /// </returns>
        IEnumerable<IFieldInfo> GetSchema(ConnectionStringSettings settings, IDatabaseObject databaseObject);

        /// <summary>
        /// Creates a new connection.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>The <see cref="DbConnection"/></returns>
        DbConnection CreateConnection(ConnectionStringSettings settings);
    }
}