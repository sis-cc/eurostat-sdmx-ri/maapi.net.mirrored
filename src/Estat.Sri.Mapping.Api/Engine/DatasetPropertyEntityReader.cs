using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class DatasetPropertyEntityReader : EntityReader<DatasetPropertyEntity>
    {
        private readonly string _suffix;

        public DatasetPropertyEntityReader()
        {
            _propertyNames.Add("orderByTimePeriodAsc", (entity, value) => entity.OrderByTimePeriodAsc = value);
            _propertyNames.Add("orderByTimePeriodDesc", (entity, value) => entity.OrderByTimePeriodDesc = value);
            _propertyNames.Add("crossApplyColumn", (entity, value) => entity.CrossApplyColumn = value);
            _propertyNames.Add("crossApplySupported", (entity, value) => entity.CrossApplySupported = bool.TryParse(value, out var boolValue) ? boolValue : (bool?) null);
            _suffix = ".";
        }

        public DatasetPropertyEntityReader(string suffix) : this()
        {
            _suffix = suffix;
        }

        public DatasetPropertyEntity ParseEntity(JsonReader reader, string dataSetId)
        {
            var entity = new DatasetPropertyEntity();

            ExtractValueFor(reader, entity,_suffix);
            entity.EntityId = dataSetId;

            return entity;
        }
    }
}
