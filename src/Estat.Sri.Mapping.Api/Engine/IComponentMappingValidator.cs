﻿// -----------------------------------------------------------------------
// <copyright file="IComponentMappingValidator.cs" company="EUROSTAT">
//   Date Created : 2017-09-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Engine
{
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    /// The ComponentMappingValidator interface.
    /// </summary>
    public interface IComponentMappingValidator
    {
        /// <summary>
        /// Validates the specified component mappings.
        /// </summary>
        /// <param name="componentMappings">The component mappings.</param>
        /// <param name="dataStructure">The data structure.</param>
        /// <returns>
        /// The <see cref="IActionResult" />.
        /// </returns>
        IActionResult Validate(IComponentMappingContainer componentMappings, IDataStructureObject dataStructure);
    }
}