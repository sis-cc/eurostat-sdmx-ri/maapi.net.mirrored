using Estat.Sri.Mapping.Api.Model;
using System.Collections.Generic;

namespace Estat.Sri.Mapping.Api.Engine
{
    public interface IDataSetEditorEngine
    {
        IList<string> GetSupportedDdbPlugins();

        string GenerateSqlQuery(DatasetEntity datasetEntity);

        /// <summary>
        /// Deserialize the <see cref="DatasetEntity.JSONQuery"/> using the corresponding plugin
        /// </summary>
        /// <returns>The <see cref="DatasetEntity.JSONQuery"/> as an object; or null if it is not supported</returns>
        object DeserializeExtraData(string extraData);

        /// <summary>
        /// Migrate old extra data (<see cref="DatasetEntity.JSONQuery"/>) to a new format.
        /// If no migration is needed the plugin can return the same value as in <see cref="DatasetEntity.JSONQuery"/>
        /// </summary>
        /// <returns>The transformed <see cref="DatasetEntity.JSONQuery"/> if needed, the same value or null</returns>
        string Migrate(DatasetEntity datasetEntity);
    }
}
