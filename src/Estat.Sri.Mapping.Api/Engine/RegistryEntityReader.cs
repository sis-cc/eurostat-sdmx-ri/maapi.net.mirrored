using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class RegistryEntityReader : NameableEntityReader<RegistryEntity>
    {
        public RegistryEntityReader()
        {
            _propertyNames.Add("technology", (entity, value) => entity.Technology = value);
            _propertyNames.Add("url", (entity, value) => entity.Url = value);
            _propertyNames.Add("username", (entity, value) => entity.UserName = value);
            _propertyNames.Add("password", (entity, value) => entity.Password = value);
            _propertyNames.Add("isPublic", (entity, value) => entity.IsPublic = bool.TryParse(value, out bool isPublic) && isPublic);
            _propertyNames.Add("useProxy", (entity, value) => entity.Proxy = bool.TryParse(value, out bool proxy) && proxy);
            _propertyNames.Add("upgrades", (entity, value) => entity.Upgrades = bool.TryParse(value, out bool upgrades) && upgrades);
            _specificProperties.Add("details");
        }

        public RegistryEntity ParseEntity(JsonReader reader)
        {
            var entity = new RegistryEntity();
            ExtractValueFor(reader,entity);
            return entity;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, RegistryEntity entity)
        {
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.Boolean && reader.Path.Contains("proxy"))
                {
                    var value = reader.Value?.ToString();
                    if (bool.TryParse(value, out var result))
                    {
                        entity.Proxy = result;
                    }
                }

                if (reader.TokenType == JsonToken.Boolean && reader.Path.Contains("upgrades"))
                {
                    var value = reader.Value?.ToString();
                    if (bool.TryParse(value, out var result))
                    {
                        entity.Upgrades = result;
                    }
                }

                if (reader.TokenType == JsonToken.Boolean && reader.Path.Contains("public"))
                {
                    var value = reader.Value?.ToString();
                    if (bool.TryParse(value, out var result))
                    {
                        entity.IsPublic = result;
                    }
                }
            }
        }
    }
}
