using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class TemplateMappingReader : EntityReader<TemplateMapping>
    {
        private TimeTranscodingEntityReader timeTranscodingEntityReader = new TimeTranscodingEntityReader();
        private string _suffix;

        public TemplateMappingReader()
        {
            _propertyNames.Add("column_name", (entity, value) => entity.ColumnName = value);
            _propertyNames.Add("column_description", (entity, value) => entity.ColumnDescription = value);
            _propertyNames.Add("component_id", (entity, value) => entity.ComponentId = value);
            _propertyNames.Add("component_type", (entity, value) => entity.ComponentType = value);
            _propertyNames.Add("concept_urn", (entity, value) => entity.ConceptUrn = value);
            _propertyNames.Add("item_scheme_id", (entity, value) => entity.ItemSchemeId = new StructureReferenceImpl(value));
            _specificProperties.Add("transcodings");
            _specificProperties.Add("timeTranscodings");
            _suffix = ".";
        }

        public TemplateMapping ParseEntity(JsonReader reader)
        {
            TemplateMapping entity = new TemplateMapping();
            entity.Transcodings = new Dictionary<string, string>();
            entity.TimeTranscoding = new List<TimeTranscodingEntity>();
            ExtractValueFor(reader, entity);
            return entity;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, TemplateMapping entity)
        {
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(_suffix + "transcodings"))
            {
                while(reader.Read() && reader.TokenType != JsonToken.EndObject)
                {
                    if(reader.TokenType == JsonToken.PropertyName)
                    {
                        entity.Transcodings.Add(reader.Value.ToString(), reader.ReadAsString());
                    }
                }
            }

            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(_suffix + "timeTranscodings"))
            {
                while (reader.Read() && HasTranscoding(reader))
                {
                    var timeTranscodingEntity = this.timeTranscodingEntityReader.ParseEntity(reader);
                    if (timeTranscodingEntity != null)
                    { 
                        entity.TimeTranscoding.Add(timeTranscodingEntity); 
                    }
                }
            }
        }

        private bool HasTranscoding(JsonReader reader)
        {
            if (reader.TokenType == JsonToken.StartObject)
            {
                reader.Read();
            }
            return reader.TokenType == JsonToken.PropertyName;
        }
    }
}
