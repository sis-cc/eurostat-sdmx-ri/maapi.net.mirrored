using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;

namespace Estat.Sri.Mapping.Api.Engine
{
    [Obsolete("For time transcoding replaced by TimeDimensionMapping Entity")]
    public class TranscodingEntityReader : EntityReader<TranscodingEntity>
    {
        private TimeTranscodingEntityReader _timeTranscodingEntityReader = new TimeTranscodingEntityReader();
        private DataSetColumnEntityReader _datasetColumnEntityReader = new DataSetColumnEntityReader();
        public TranscodingEntityReader()
        {
            _propertyNames.Add("expression",(entity,value)=>entity.Expression = value);
        }

        public TranscodingEntity ParseEntity(JsonReader reader)
        {
            var entity = new TranscodingEntity();
            ExtractValueFor(reader,entity);
            return entity;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, TranscodingEntity entity)
        {
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("timeTranscoding"))
            {
                entity.TimeTranscoding = new List<TimeTranscodingEntity>();
                while (reader.Read() && HasTranscoding(reader))
                {
                    var timeTranscodingEntity = _timeTranscodingEntityReader.ParseEntity(reader);
                    entity.TimeTranscoding.Add(timeTranscodingEntity);
                }
            }

        }

        private bool HasTranscoding(JsonReader reader)
        {
            if(reader.TokenType == JsonToken.StartObject)
            {
                reader.Read();
            }
            return reader.TokenType == JsonToken.PropertyName;
        }
    }
}
