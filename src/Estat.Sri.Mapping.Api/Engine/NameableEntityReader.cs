// -----------------------------------------------------------------------
// <copyright file="EntityReader.cs" company="EUROSTAT">
//   Date Created : --
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class NameableEntityReader<T>: EntityReader<T> where T : SimpleNameableEntity
    {
        public NameableEntityReader()
        {
            _propertyNames.Add("name", (entity, value) => entity.Name = value);
            _propertyNames.Add("description", (entity, value) => entity.Description = value);
        }
    }
}
