using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class DataSetColumnEntityReader :NameableEntityReader<DataSetColumnEntity>
    {
        private List<string> _propertyNames;

        public DataSetColumnEntity ParseEntity(JsonReader reader)
        {
            DataSetColumnEntity entity = new DataSetColumnEntity();

            ExtractValueFor(reader,entity);
            return entity;
        }
    }
}
