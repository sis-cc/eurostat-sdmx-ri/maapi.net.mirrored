// -----------------------------------------------------------------------
// <copyright file="CacheEntityAuthorizationEngine.cs" company="EUROSTAT">
//   Date Created : 2020-2-3
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Utils;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class CacheEntityAuthorizationEngine : IEntityAuthorizationEngine
    {
        private readonly IEntityAuthorizationEngine decoratedEngine;
        private readonly EntityType entityType;

        public CacheEntityAuthorizationEngine(IEntityAuthorizationEngine decoratedEngine, EntityType entityType)
        {
            if (decoratedEngine is null)
            {
                throw new ArgumentNullException(nameof(decoratedEngine));
            }

            this.decoratedEngine = decoratedEngine;
            this.entityType = entityType;
        }

        public bool CanAccess(string mappingStoreId, string entityId, AccessType accessType)
        {
            bool? cached = AuthorizationCache.CanAccess(entityType, mappingStoreId, entityId, accessType);
            if (cached.HasValue)
            {
                return cached.Value;
            }
            var result = decoratedEngine.CanAccess(mappingStoreId, entityId, accessType);
            AuthorizationCache.Add(entityType, mappingStoreId, entityId, accessType, result);
            return result;
        }

        public bool CanAccess(IEntity entity, AccessType accessType)
        {
            if (string.IsNullOrWhiteSpace(entity.StoreId))
            {
                entity.StoreId = AuthorizationCache.StoreId;
            }

            bool? cached = AuthorizationCache.CanAccess(entity.TypeOfEntity, entity.StoreId, entity.EntityId, accessType);
            if (cached.HasValue)
            {
                return cached.Value;
            }
            var result = decoratedEngine.CanAccess(entity, accessType);
            
            AuthorizationCache.Add(entity.TypeOfEntity, entity.StoreId, entity.EntityId, accessType, result);
            return result;
        }

        public IEnumerable<TPermissionEntity> CanAccess<TPermissionEntity>(IList<TPermissionEntity> arrayOfEntities, AccessType accessType) where TPermissionEntity : IEntity
        {
            return from permissionEntity in arrayOfEntities where CanAccess(permissionEntity, accessType) select permissionEntity;
        }
    }
}
