using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class TimeTranscodingEntityReader : EntityReader<TimeTranscodingEntity>
    {
        private DataSetColumnEntityReader _dataSetColumnEntityReader = new DataSetColumnEntityReader();
        private TranscodingRuleEntityReader _transcodingRuleEntityReader = new TranscodingRuleEntityReader();

        public TimeTranscodingEntity ParseEntity(JsonReader reader)
        {
            return ExtractTimeTranscodingEntity(reader);
        }

        protected TimeTranscodingEntity ExtractTimeTranscodingEntity(JsonReader reader)
        {
            var entity = new TimeTranscodingEntity();
            var frequency = reader.Value?.ToString();
            entity.Frequency = frequency;
            reader.Read();
            if (reader.TokenType == JsonToken.StartObject)
            {
                while (reader.Read() && reader.TokenType != JsonToken.EndObject)
                {
                    if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("is_date_time"))
                    {
                        entity.IsDateTime = reader.ReadAsBoolean().Value;
                    }

                    if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("isDateTime"))
                    {
                        entity.IsDateTime = reader.ReadAsBoolean().Value;
                    }

                    if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("year"))
                    {
                        entity.Year = ExtractTimeTranscoding(reader);
                    }
                    
                    if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("date_column"))
                    {
                        entity.DateColumn = _dataSetColumnEntityReader.ParseEntity(reader);
                    }

                    if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("dataSetColumn"))
                    {
                        entity.DateColumn = _dataSetColumnEntityReader.ParseEntity(reader);
                    }

                    if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("period"))
                    {
                        entity.Period = ExtractPeriod(reader);
                    }
                }
            }

            return entity;
        }

        private PeriodTimeTranscoding ExtractPeriod(JsonReader reader)
        {
            var periodTimeTranscoding = new PeriodTimeTranscoding();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.Null)
                {
                    return null;
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("rules"))
                {
                    periodTimeTranscoding.Rules = ExtractRules(reader);
                }

                FillTimeTranscoding(reader, periodTimeTranscoding);
            }

            return periodTimeTranscoding;
        }

        private List<TranscodingRuleEntity> ExtractRules(JsonReader reader)
        {
            var rules = new List<TranscodingRuleEntity>();
            while (reader.Read() && HasTranscodingRule(reader))
            {
                rules.Add(_transcodingRuleEntityReader.ParseEntity(reader));
            }
            return rules;
        }

        private bool HasTranscodingRule(JsonReader reader)
        {
            if(reader.TokenType == JsonToken.StartArray)
            {
                reader.Read();
            }
            return reader.TokenType == JsonToken.StartObject;
        }

        private TimeTranscoding ExtractTimeTranscoding(JsonReader reader)
        {
            var timeTranscoding = new TimeTranscoding();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.Null)
                {
                    return null;
                }

                FillTimeTranscoding(reader, timeTranscoding);
            }

            return timeTranscoding;
        }

        private void FillTimeTranscoding(JsonReader reader, TimeTranscoding timeTranscoding)
        {
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("column"))
            {
                timeTranscoding.Column = _dataSetColumnEntityReader.ParseEntity(reader);
            }

            if (reader.TokenType == JsonToken.Integer && reader.Path.Contains("length"))
            {
                timeTranscoding.Length = Convert.ToInt32((long)reader.Value);
            }

            if (reader.TokenType == JsonToken.Integer && reader.Path.Contains("start"))
            {
                timeTranscoding.Start = Convert.ToInt32((long)reader.Value);
            }
        }
    }
}
