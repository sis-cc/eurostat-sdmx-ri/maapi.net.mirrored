// -----------------------------------------------------------------------
// <copyright file="EntityReader.cs" company="EUROSTAT">
//   Date Created : --
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Util.Date;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class EntityReader<T> where T : Entity
    {
        protected readonly Dictionary<string, Action<T, string>> _propertyNames;
        protected readonly List<string> _specificProperties = new List<string>();

        /// <summary>
        /// base class for reading JSON entities
        /// </summary>
        public EntityReader()
        {
            _propertyNames = new Dictionary<string, Action<T, string>>
            {
                {"entityId", (entity, value) => entity.EntityId = value},
                {"parentId", (entity, value) => entity.ParentId = value},
            };
        }

        protected void ExtractValueFor(JsonReader reader, T entity, string suffix=".")
        {
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                ////if (reader.TokenType == JsonToken.StartObject)
                ////{
                ////    continue;
                ////}
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    int startPath = !string.IsNullOrEmpty(suffix) ? reader.Path.LastIndexOf(suffix) : 0;
                    string path = reader.Path.Substring(startPath);
                    var nodeName = _propertyNames.Keys.ToList().Find(property => path.Equals(suffix + property));
                    if (!string.IsNullOrWhiteSpace(nodeName))
                    {
                        reader.Read();
                        switch(reader.TokenType)
                        {
                            case JsonToken.String:
                            case JsonToken.Integer:
                            case JsonToken.Boolean:
                            case JsonToken.Float:
                                {
                                    var value = Convert.ToString(reader.Value, CultureInfo.InvariantCulture);
                                    _propertyNames[nodeName](entity, value);
                                }
                                break;
                            case JsonToken.Date:
                                {
                                    string value = null;
                                    if (reader.ValueType == typeof(DateTime))
                                    {
                                        var date = (DateTime)reader.Value;
                                        value = DateUtil.FormatDate(date);
                                    }
                                    else if (reader.ValueType == typeof(DateTimeOffset))
                                    {
                                        var date = (DateTimeOffset)reader.Value;
                                        value = DateUtil.FormatDate(date, Org.Sdmxsource.Sdmx.Api.Constants.TimeFormatEnumType.DateTime);
                                    }
                                    else
                                    {
                                        value = Convert.ToString(reader.Value, CultureInfo.InvariantCulture);
                                    }
                                    
                                    _propertyNames[nodeName](entity, value);
                                }
                                break;

                        }
                    }
                    else
                    {
                        var specificNodeName = _specificProperties.Find(property => path.Equals(suffix + property));
                        if (!string.IsNullOrWhiteSpace(specificNodeName))
                        {
                            ExtractSpecificProperties(reader, entity);
                        }
                        else
                        {
                            reader.Skip();
                        }
                    }
                    
                }
            }
        }

        protected virtual void ExtractSpecificProperties(JsonReader reader, T entity)
        {

        }
    }
}