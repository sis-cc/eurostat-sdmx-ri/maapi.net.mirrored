// -----------------------------------------------------------------------
// <copyright file="IEntityStreamingReader.cs" company="EUROSTAT">
//   Date Created : 2019-9-13
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Mapping.Api.Engine
{
    /// <summary>
    /// A streaming forward only streaming interface for parsing Entities
    /// </summary>
    public interface IEntityStreamingReader : IDisposable
    {
        /// <summary>
         /// Returns true if there is a JSON message (maybe we could have an identifier like "entities")
         /// </summary>
        bool MoveToNextMessage();

        /// <summary>
        /// Moves to the next section, must be called at least once
         /// </summary>
        bool MoveToNextEntityTypeSection();

        /// <summary>
        /// gets the current entity type, changes every time we all MoveToNextEntityTypeSection()
         /// </summary>
        EntityType CurrentEntityType { get; }

        /// <summary>
        /// Moves to the next entity, it has be called at least once
         /// </summary>
        bool MoveToNextEntity();

        /// <summary>
        /// Get the current entity
        /// </summary>
        EntityWithResult CurrentEntity {get;}

        /// <summary>
        /// A lazy enumeration a combination of MoveToNextEntity and CurrentEntity
        /// </summary>
        IList<IEntity> GetCurrentEntities();

        /// <summary>
        /// close and free resources
         /// </summary>
        void Close();
    }
}
