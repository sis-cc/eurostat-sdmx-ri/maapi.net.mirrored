// -----------------------------------------------------------------------
// <copyright file="ILocalCodeRetrieval.cs" company="EUROSTAT">
//   Date Created : 2014-09-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;

    /// <summary>
    ///     The local code list retrieval interface
    /// </summary>
    public interface ILocalCodeRetrieval
    {
        /// <summary>
        /// Gets the local codes list from the DDB.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="query">The query.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="page">The page.</param>
        /// <returns>The enumeration of local codes from the DDB</returns>
        IEnumerable<object> GetLocalCodeList(ConnectionStringSettings settings, string query, string columnName, int limit, int page);

        /// <summary>
        /// Gets the local codes list from the DDB.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="query">The query.</param>
        /// <returns>
        /// The number of records
        /// </returns>
        int GetCount(ConnectionStringSettings settings, string query);

        /// <summary>
        /// Gets the local code list for multiple columns from the DDB. If <paramref name="columns"/> is empty then it will also populate them.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="query">The query.</param>
        /// <param name="columns">The columns.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="page">The page.</param>
        /// <returns>
        /// The enumeration of local codes from the DDB
        /// </returns>
        IEnumerable<object[]> GetLocalCodeList(ConnectionStringSettings settings, string query, IList<string> columns, int limit, int page);

        /// <summary>
        /// Gets the local codes list from the DDB.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="query">The query.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="page"></param>
        /// <returns>
        /// The enumeration of local codes from the DDB
        /// </returns>
        IEnumerable<object> GetLocalCodeListOrdered(ConnectionStringSettings settings, string query, string columnName, int limit, int page);
        /// <summary>
        /// Write the local codes fro mthe DDB for a specific <paramref name="dataQuery"/>
        /// </summary>
        /// <param name="connectionStringSettings">The DDB connection string settings</param>
        /// <param name="dataQuery">The query</param>
        /// <param name="writer">The writer</param>
        /// <param name="validator">The validator to use for each value, optional</param>
        void WriteLocalCodeList(ConnectionStringSettings connectionStringSettings, ILocalDataQuery dataQuery, ILocalDataWriter writer, ILocalCodeValidator validator);

        /// <summary>
        /// Validate local codes from the DDB returned by <paramref name="dataQuery"/>
        /// </summary>
        /// <param name="connectionStringSettings">The DDB connection string settings</param>
        /// <param name="dataQuery">The query</param>
        /// <param name="validator">The validator to use for each value</param>
        void ValidatedValues(ConnectionStringSettings connectionStringSettings, ILocalDataQuery dataQuery, ILocalCodeValidator validator);
    }
}