// -----------------------------------------------------------------------
// <copyright file="IProxySettingsEngine.cs" company="EUROSTAT">
//   Date Created : --
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Engine
{
    /// <summary></summary>
    public interface IProxySettingsEngine
    {
        /// <summary>Gets this instance.</summary>
        /// <returns></returns>
        ProxySettings Get(string sid);

        /// <summary>Updates the specified settings.</summary>
        /// <param name="sid"></param>
        /// <param name="settings">The settings.</param>
        void Update(string sid,ProxySettings settings);

        /// <summary>Deletes this instance.</summary>
        void Delete(string sid);
    }
}