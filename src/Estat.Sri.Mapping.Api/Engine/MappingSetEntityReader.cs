using System;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Util.Date;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class MappingSetEntityReader : NameableEntityReader<MappingSetEntity>
    {
        public MappingSetEntityReader()
        {
            _specificProperties.Add("dataset");
            _specificProperties.Add("validFrom");
            _specificProperties.Add("validTo");
            _propertyNames.Add("isMetadata", (entity, value) => entity.IsMetadata = bool.Parse(value));
        }

        public MappingSetEntity ParseEntity(JsonReader reader)
        {
            var entity = new MappingSetEntity();

            ExtractValueFor(reader, entity);

            return entity;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, MappingSetEntity entity)
        {
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".dataset"))
            {
                while (reader.Read() && reader.TokenType != JsonToken.EndObject)
                {
                    if (reader.TokenType == JsonToken.String)
                    {
                        entity.DataSetId = reader.Value?.ToString();
                    }
                }
            }

         
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".validFrom"))
            {
                entity.ValidFrom = ParseDateTime(reader);
            }

            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".validTo"))
            {
                entity.ValidTo = ParseDateTime(reader);
            }
        }

        private static DateTime? ParseDateTime(JsonReader reader)
        {
            if (!reader.Read())
            {
                return null;
            }

            object value = reader.Value;

            if (value is null)
            {
                return null;
            }

            if (value is DateTimeOffset)
            {
                // Using UtcDateTime will modify the value
                return ((DateTimeOffset)value).DateTime;
            }

            if (value is DateTime)
            {
                return (DateTime)value;
            }

            return DateUtil.FormatDate(reader.Value);
        }
    }
}
