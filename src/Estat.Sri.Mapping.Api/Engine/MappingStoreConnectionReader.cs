using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class MappingStoreConnectionReader : NameableEntityReader<MappingStoreConnectionEntity>
    {
        public MappingStoreConnectionReader()
        {
            _propertyNames.Add("type", (entity, value) => entity.DatabaseVendorType = value);
            _propertyNames.Add("storeType", (entity, value) => entity.StoreType = value);
            _propertyNames.Add("subtype", (entity, value) => entity.SubType = value);
            _propertyNames.Add("dbName", (entity, value) => entity.DbName = value);
            _specificProperties.Add("properties");
        }

        public MappingStoreConnectionEntity ParseEntity(JsonReader reader)
        {
            var entity = new MappingStoreConnectionEntity();
            ExtractValueFor(reader, entity,string.Empty);
            return entity;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, MappingStoreConnectionEntity entity)
        {
            var connectionEntity = entity as MappingStoreConnectionEntity;
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Equals("properties"))
            {
                ExtractSettings(reader, connectionEntity);
                reader.Read();
            }
        }

        private void ExtractValueForSpecific(JsonReader reader, MappingStoreConnectionEntity entity)
        {
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var nodeName = _propertyNames.Keys.ToList().Find(property => reader.Path.Equals(property));
                    if (!string.IsNullOrWhiteSpace(nodeName))
                    {
                        reader.Read();
                        if (reader.TokenType == JsonToken.String)
                        {
                            var value = reader.Value?.ToString();
                            _propertyNames[nodeName](entity, value);
                        }
                    }
                    else
                    {
                        var specificNodeName = _specificProperties.Find(property => reader.Path.Equals(property));
                        if (!string.IsNullOrWhiteSpace(specificNodeName))
                        {
                            ExtractSpecificProperties(reader, entity);
                        }
                        else
                        {
                            reader.Skip();
                        }
                    }

                }
            }
        }


        private void ExtractSettings(JsonReader reader, ConnectionEntity connectionEntity)
        {
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var settingName = reader.Value?.ToString();
                    var value = ExtractSetting(reader);
                    connectionEntity.Settings.Add(settingName, value);
                }
            }
        }

        private IConnectionParameterEntity ExtractSetting(JsonReader reader)
        {
            var entity = new ConnectionParameterEntity();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.String || reader.TokenType == JsonToken.Integer)
                {
                    entity.Value = reader.Value;
                }
            }

            return entity;
        }
    }

}
