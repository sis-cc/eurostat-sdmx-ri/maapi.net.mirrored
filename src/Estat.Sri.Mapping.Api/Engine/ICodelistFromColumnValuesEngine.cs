﻿using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Mapping.Api.Engine
{
    /// <summary>
    /// ICodelistFromColumnValuesEngine
    /// </summary>
    public interface ICodelistFromColumnValuesEngine
    {
        /// <summary>
        /// Creates a codelist.
        /// </summary>
        /// <param name="sid">The sid.</param>
        /// <param name="datasetId">The dataset identifier.</param>
        /// <param name="column">The column.</param>
        /// <param name="codelist">The codelist.</param>
        /// <returns></returns>
        long Create(string sid, string datasetId, string column, ICodelistMutableObject codelist);
    }
}
