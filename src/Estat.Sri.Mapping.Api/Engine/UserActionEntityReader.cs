using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class UserActionEntityReader
    {
        public UserActionEntity ParseEntity(JsonReader reader)
        {
            UserActionEntity entity = new UserActionEntity();
            while (reader.Read())
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    if (reader.Path.Contains("name"))
                    {
                        JToken obj = JToken.Load(reader);
                        if (obj is JProperty value)
                        {
                            //entity.Name = value.Value.ToString();
                        }
                    }
                    if (reader.Path.Contains("entityId"))
                    {
                        JToken obj = JToken.Load(reader);
                        if (obj is JProperty value)
                        {
                            entity.EntityId = value.Value.ToString();
                        }
                    }
                    if (reader.Path.Contains("parentId"))
                    {
                        JToken obj = JToken.Load(reader);
                        if (obj is JProperty value)
                        {
                            entity.ParentId = value.Value.ToString();
                        }
                    }
                    if (reader.Path.Contains("description"))
                    {
                        JToken obj = JToken.Load(reader);
                        if (obj is JProperty value)
                        {
                            //entity.Description = value.Value.ToString();
                        }
                    }
                }
                if (reader.TokenType == JsonToken.StartObject)
                {
                    if (reader.Path.Contains("dataset"))
                    {
                        JObject properties = JObject.Load(reader);
                        foreach (var property in properties)
                        {
                            //entity.DataSetId = property.Value.ToString();
                        }
                    }
                }
            }

            return entity;
        }
    }
}
