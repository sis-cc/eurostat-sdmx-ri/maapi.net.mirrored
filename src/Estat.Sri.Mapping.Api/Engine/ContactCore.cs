using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Diff;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class ContactCore : IContact
    {
        private IList<ITextTypeWrapper> _name;
        private IList<ITextTypeWrapper> _departments;
        private IList<ITextTypeWrapper> _role;
        private IList<string> _email;
        private IList<string> _fax;
        private IList<string> _telephone;
        private IList<string> _uri;
        private IList<string> _x400;
        public ContactCore()
        {
            _name = new List<ITextTypeWrapper>();
            _departments = new List<ITextTypeWrapper>();
            _role = new List<ITextTypeWrapper>();
            _email = new List<string>();
            _fax = new List<string>();
            _telephone = new List<string>();
            _uri = new List<string>();
            _x400 = new List<string>();
        }
        public IList<ITextTypeWrapper> Departments
        {
            get { return _departments; }
            set { _departments = value; }
        }

        public IList<string> Email => _email;

        public IList<string> Fax => _fax;

        public string Id => throw new NotImplementedException();

        public IList<ITextTypeWrapper> Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public IList<ITextTypeWrapper> Role
        {
            get { return _role; }
            set { _role = value; }
        }

        public IList<string> Telephone => _telephone;

        public IList<string> Uri => _uri;

        public IList<string> X400 => _x400;

        public ISet<ISdmxObject> Composites => throw new NotImplementedException();

        public ISet<ICrossReference> CrossReferences => throw new NotImplementedException();

        public IList<ICrossReference> AllCrossReferences => throw new NotImplementedException();

        public ISdmxObject Parent => throw new NotImplementedException();

        public SdmxStructureType StructureType => throw new NotImplementedException();

        public string BeanKey => throw new NotImplementedException();

        public bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            throw new NotImplementedException();
        }

        public bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties, IDiffReport report)
        {
            throw new NotImplementedException();
        }

        public ISet<T> GetComposites<T>(Type type)
        {
            throw new NotImplementedException();
        }

        public T GetParent<T>(bool includeThisInSearch) where T : class
        {
            throw new NotImplementedException();
        }

        public IReadOnlyList<string> GetSupportedLanguages()
        {
            throw new NotImplementedException();
        }
    }
}
