using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class UserEntityReader : EntityReader<UserEntity>
    {
        private readonly PermissionEntityReader permissionEntityReader = new PermissionEntityReader();
        public UserEntityReader()
        {
            _propertyNames.Add("username",(entity,value) => entity.UserName = value);
            _specificProperties.Add("permissions");
        }

        public UserEntity ParseEntity(JsonReader reader)
        {
            var entity = new UserEntity();
            ExtractValueFor(reader,entity);
            return entity;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, UserEntity entity)
        {
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains("permissions"))
            {
                permissionEntityReader.ParsePermissions(reader, entity);
            }
        }

        private static Tuple<string,string> GetPermission(JsonReader reader)
        {
            var key = reader.Value?.ToString();
            reader.Read();
            var value = reader.Value?.ToString();
            return new Tuple<string, string>(key,value);
        }
    }
}
