// -----------------------------------------------------------------------
// <copyright file="ComponentMappingEntityReader.cs" company="EUROSTAT">
//   Date Created : 2019-12-18
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using static Dapper.SqlMapper;

namespace Estat.Sri.Mapping.Api.Engine
{
    internal class ComponentMappingEntityReader : NameableEntityReader<ComponentMappingEntity>
    {
        private readonly DataSetColumnEntityReader _datasetColumnEntityReader = new DataSetColumnEntityReader();

        public ComponentMappingEntityReader()
        {
            _propertyNames.Add("type", (entity, value) => entity.Type = value);
            _propertyNames.Add("defaultValue", (entity, value) => entity.DefaultValue = value);
            _propertyNames.Add("constantValue", (entity, value) => entity.ConstantValue = value);
            _propertyNames.Add("isMetadata", (entity, value) => entity.IsMetadata = bool.Parse(value));
            _specificProperties.Add("component");
            _specificProperties.Add("dataset_column");
            _specificProperties.Add("defaultValues");
            _specificProperties.Add("constantValues");
        }

        public ComponentMappingEntity ParseEntity(JsonReader reader)
        {
            var entity = new ComponentMappingEntity();

            ExtractValueFor(reader, entity);

            return entity;
        }

        

        private void TestFunction(Action<ComponentMappingEntity,string> func)
        {
            throw new NotImplementedException();
        }

        protected override void ExtractSpecificProperties(JsonReader reader, ComponentMappingEntity entity)
        {
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".component"))
            {
                ExtractComponent(reader, entity);
            }

            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".dataset_column"))
            {
                ExtractDatasetColumns(reader, entity);
            }

            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".constantValues"))
            {
                ExtractConstantOrDefaultValues(reader, entity.ConstantValues.Add);
            }

            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".defaultValues"))
            {
                ExtractConstantOrDefaultValues(reader, entity.DefaultValues.Add);
            }
        }

        private static void ExtractComponent(JsonReader reader, ComponentMappingEntity entity)
        {
            var entityId = string.Empty;
            var id = string.Empty;

            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.String && reader.Path.Contains(".entityId"))
                {
                    entityId = reader.Value?.ToString();
                }

                if (reader.TokenType == JsonToken.String && reader.Path.Contains(".id"))
                {
                    id = reader.Value?.ToString();
                }
            }


            entity.Component = new Component
            {
                EntityId = entityId,
                ObjectId = id
            };
        }

        private DataSetColumnEntity ExtractDatasetColumn(JsonReader reader)
        {
            return _datasetColumnEntityReader.ParseEntity(reader);
        }

        private void ExtractDatasetColumns(JsonReader reader, ComponentMappingEntity entity)
        {
            while (reader.Read() && reader.TokenType != JsonToken.EndArray)
            {
                if (reader.TokenType == JsonToken.StartObject)
                {
                    var column = ExtractDatasetColumn(reader);
                    entity.AddColumn(column);
                }
            }
        }

        private void ExtractConstantOrDefaultValues(JsonReader reader, Func<string, bool> appendAction)
        {
            while (reader.Read() && (reader.TokenType != JsonToken.EndArray && reader.TokenType != JsonToken.Null))
            {
                if (reader.TokenType == JsonToken.String)
                {
                    appendAction(reader.Value.ToString());
                }
            }
        }
    }

}