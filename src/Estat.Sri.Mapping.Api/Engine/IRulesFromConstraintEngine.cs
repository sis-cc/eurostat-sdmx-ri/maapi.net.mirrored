// -----------------------------------------------------------------------
// <copyright file="IRulesFromConstraintEngine.cs" company="EUROSTAT">
//   Date Created : --
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Engine
{
    /// <summary></summary>
    public interface IRulesFromConstraintEngine
    {
        /// <summary>Creates the rules.</summary>
        /// <param name="sid">The sid.</param>
        /// <param name="mappingSetId">The mapping set identifier.</param>
        /// <param name="agency">The agency.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="version">The version.</param>
        void CreateRules(string sid, string mappingSetId, string agency, string id, string version);
    }
}