using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Diff;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Util;

namespace Estat.Sri.Mapping.Api.Engine
{
    [Serializable]
    public class TextTypeWrapperImpl : ITextTypeWrapper
    {
        /// <summary>
        ///     The is html text.
        /// </summary>
        private readonly bool _isHtmlText;

        /// <summary>
        ///     The locale.
        /// </summary>
        private string _locale;

        /// <summary>
        ///     The valueren.
        /// </summary>
        private string _valueren;

        public TextTypeWrapperImpl(string locale0, string valueren, ISdmxObject parent)
        {
            this.Locale = locale0;
            this.Value = valueren;
            this.Validate();
        }

        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (string.IsNullOrWhiteSpace(this._locale))
            {
                // Default to english
                this._locale = CultureInfo.CreateSpecificCulture("en").TwoLetterISOLanguageName;
            }

            if (!this.ParseLocale(this._locale))
            {
                throw new SdmxSemmanticException("Illegal Locale: " + this._locale);
            }

            if (string.IsNullOrWhiteSpace(this._valueren))
            {
                throw new SdmxSemmanticException("Text Type can not have an empty string value");
            }
        }
        public virtual bool Html
        {
            get
            {
                return this._isHtmlText;
            }
        }

        /// <summary>
        ///     Gets or sets the locale.
        /// </summary>
        public string Locale
        {
            get
            {
                return this._locale;
            }

            set
            {
                if (!ObjectUtil.ValidString(this._locale))
                {
                    this._locale = "en";
                }

                // Bug fix, in XML Locale contains a '-' to be valid, in Java '_' is used
                this._locale = this._locale.Replace("_", "-");
                this._locale = value;
            }
        }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        public string Value
        {
            get
            {
                return this._valueren;
            }

            set
            {
                if (value != null)
                {
                    this._valueren = value.Trim();
                }
            }
        }

        private bool ParseLocale(string locale)
        {
            return LocaleConstants.ValidLocaleSet.Contains(locale);
        }

        public ISet<ISdmxObject> Composites => throw new NotImplementedException();

        public ISet<ICrossReference> CrossReferences => throw new NotImplementedException();

        public IList<ICrossReference> AllCrossReferences => throw new NotImplementedException();

        public ISdmxObject Parent => throw new NotImplementedException();

        public SdmxStructureType StructureType => throw new NotImplementedException();

        public string BeanKey => throw new NotImplementedException();

        public bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            throw new NotImplementedException();
        }

        public ISet<T> GetComposites<T>(Type type)
        {
            throw new NotImplementedException();
        }

        public T GetParent<T>(bool includeThisInSearch) where T : class
        {
            throw new NotImplementedException();
        }

        public IReadOnlyList<string> GetSupportedLanguages()
        {
            throw new NotImplementedException();
        }

        public bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties, IDiffReport report)
        {
            throw new NotImplementedException();
        }
    }
}
