// -----------------------------------------------------------------------
// <copyright file="AuthEntityStreamingWriterDecorator.cs" company="EUROSTAT">
//   Date Created : 2019-9-13
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Utils;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    /// <summary>
    /// Decorator for permissions authorization when writing
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Engine.IEntityStreamingWriter" />
    public class AuthEntityStreamingWriterDecorator : IEntityStreamingWriter
    {
        private readonly IEntityStreamingWriter decoratedEngine;
        private readonly IEntityAuthorizationManager authorizationManager;
        private readonly SdmxAuthorizationScope scope;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthEntityStreamingWriterDecorator"/> class.
        /// </summary>
        /// <param name="decoratedEngine">The decorated engine.</param>
        /// <param name="authorizationManager">The authorization manager.</param>
        public AuthEntityStreamingWriterDecorator(IEntityStreamingWriter decoratedEngine, IEntityAuthorizationManager authorizationManager, SdmxAuthorizationScope scope)
        {
            this.decoratedEngine = decoratedEngine;
            this.authorizationManager = authorizationManager;
            this.scope = scope;
        }

      

        public void Close()
        {
            decoratedEngine.Close();
        }

        public void Dispose()
        {
            decoratedEngine.Dispose();
            scope.Dispose();
        }

        public void StartMessage()
        {
            decoratedEngine.StartMessage();
        }

        public void StartSection(EntityType entityType)
        {
            decoratedEngine.StartSection(entityType);
        }

        public void Write(IEntity entity)
        {
            // we return only what the user can access
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(IEnumerable<IEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<DatasetEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IConnectionEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(DatasetEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(DatasetPropertyEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(DataSetColumnEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(LocalCodeEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(MappingSetEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(ColumnDescriptionSourceEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(ComponentMappingEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(TranscodingEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(TranscodingRuleEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(HeaderEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(RegistryEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(TimeDimensionMappingEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }
       public void Write(LastUpdatedEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

       public void Write(ValidToMappingEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }
        public void Write(UpdateStatusEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }


        public void Write(NsiwsEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(UserEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }
        public void Write(DataSourceEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }
        public void Write(TimePreFormattedEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }
        public void Write(IEnumerable<IConnectionEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<DataSetColumnEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<LocalCodeEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<MappingSetEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<ColumnDescriptionSourceEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<ComponentMappingEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<TranscodingEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<TranscodingRuleEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<HeaderEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<TimeDimensionMappingEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }
        public void Write(IEnumerable<LastUpdatedEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<ValidToMappingEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<UpdateStatusEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }


        public void Write(IEnumerable<RegistryEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<NsiwsEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<UserEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(IEnumerable<long> entityIds)
        {
            throw new System.NotImplementedException();
        }

        public void Write(TemplateMapping entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(IEnumerable<TemplateMapping> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void Write(UserActionEntity entity)
        {
            if (this.authorizationManager.CanAccess(entity, AccessType.Read))
            {
                decoratedEngine.Write(entity);
            }
        }

        public void Write(IEnumerable<UserActionEntity> entity)
        {
            decoratedEngine.Write(this.authorizationManager.CanAccess(entity, AccessType.Read));
        }

        public void WriteProperty(string name, string value)
        {
            decoratedEngine.WriteProperty(name,value);
        }

        public void WriteStatus(StatusType status,string message)
        {
            decoratedEngine.WriteStatus(status,message);
        }

        public int Status { 
            get 
            { 
                return decoratedEngine.Status; 
            } 
            set 
            { 
                decoratedEngine.Status = value; 
            } 
        }
    }
}
