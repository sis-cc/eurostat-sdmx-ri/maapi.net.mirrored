// -----------------------------------------------------------------------
// <copyright file="RegistryStreamWriter.cs" company="EUROSTAT">
//   Date Created : 2019-12-16
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    internal class RegistryStreamWriter : AbstractEntityStreamingWriter
    {
        private readonly CommonStreamWriter commonWriter;

        public RegistryStreamWriter(CommonStreamWriter commonWriter)
        {
            if (commonWriter == null)
            {
                throw new ArgumentNullException(nameof(commonWriter));
            }

            this.commonWriter = commonWriter;
        }

        public override void Write(RegistryEntity entity)
        {
            if (entity != null)
            {
                using (commonWriter.WriteStartObject())
                {
                    commonWriter.WriteEntityId(entity);
                    commonWriter.WriteSimpleNameable(entity);
                    commonWriter.WriteParentId(entity);
                    commonWriter.WriteDetailsLinks(entity);

                    commonWriter.TryWrite("technology", entity.Technology);
                    commonWriter.TryWrite("url", entity.Url);
                    commonWriter.TryWrite("username", entity.UserName);
                    // TODO placeholder names for isPublic and useProxy. Verify they are correct
                    commonWriter.WriteProperty("isPublic", entity.IsPublic);
                    commonWriter.WriteProperty("useProxy", entity.Proxy);
                    commonWriter.WriteProperty("upgrades", entity.Upgrades);
                }
            }
        }
    }
}