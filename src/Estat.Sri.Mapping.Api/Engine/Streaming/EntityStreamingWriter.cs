// -----------------------------------------------------------------------
// <copyright file="EntityStreamingWriter.cs" company="EUROSTAT">
//   Date Created : 2019-12-11
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Estat.Sri.Utils.Extensions;

// TODO move to Engine folder/namespace
namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    public class EntityStreamingWriter :  IEntityStreamingWriter
    {
        private readonly CommonStreamWriter commonWriter;
        private readonly TextWriter textWriter;
        private bool disposed = false;
        private bool _calledFromWriteIEntity = false;
        private readonly bool _closeWriter;
        private IDisposable _messageArray = null;
        private IEntityStreamingWriter _currentStreamingWriter;
        protected JsonWriter writer;

        private readonly Dictionary<EntityType, IEntityStreamingWriter> _streamWriters;
        public int Status { get; set; }
        /// <summary>
        /// Inti
        /// </summary>
        /// <param name="writer">The JSON writer. It is assumed this is disposed outside this class</param>
        public EntityStreamingWriter(JsonWriter writer)
        {
            this._closeWriter = false;
            this.writer = writer;
            this.writer.Formatting = Formatting.Indented;
            this.commonWriter = new CommonStreamWriter(writer, null, null, null);
            this._streamWriters = Populate(commonWriter);
        }
        public EntityStreamingWriter(Stream response): this(null, response, null, null)
        {

        }
        public EntityStreamingWriter(string sid, Stream response, Action<IEntity, JsonWriter, string> writeLinks, Action<IEntity, JsonWriter> writeDetails):
            base()
        {
            this._closeWriter = true;
            textWriter = new StreamWriter(response, new UTF8Encoding(false));
            this.writer = new JsonTextWriter(textWriter) { Formatting = Formatting.Indented };
            this.commonWriter = new CommonStreamWriter(writer, sid, writeLinks, writeDetails);
            this._streamWriters = Populate(commonWriter);
        }

        private Dictionary<EntityType, IEntityStreamingWriter> Populate(CommonStreamWriter commonStreamWriter)
        {
            var map = new Dictionary<EntityType, IEntityStreamingWriter>();
            var datasetStreamWriter = new DatasetStreamWriter(commonStreamWriter);
            
            // Dataset, columns, local codes and column description
            map.Add(EntityType.DataSet, datasetStreamWriter);
            map.Add(EntityType.DataSetColumn, datasetStreamWriter);
            map.Add(EntityType.DescSource, datasetStreamWriter);
            map.Add(EntityType.LocalCode, datasetStreamWriter);
            

            // DDB and Store
            var connectionStreamWriter = new ConnectionStreamWriter(commonStreamWriter);
            map.Add(EntityType.DdbConnectionSettings, connectionStreamWriter);
            map.Add(EntityType.MappingStore, connectionStreamWriter);

            // Header
            map.Add(EntityType.Header, new HeaderStreamWriter(commonStreamWriter));

            // Mapping Set and mappings
            MappingSetStreamingWriter mappingSetWriter = new MappingSetStreamingWriter(commonStreamWriter);
            map.Add(EntityType.Mapping, mappingSetWriter);
            map.Add(EntityType.MappingSet, mappingSetWriter);
            map.Add(EntityType.TimeMapping, mappingSetWriter);
            map.Add(EntityType.LastUpdated, mappingSetWriter);
            map.Add(EntityType.ValidToMapping, mappingSetWriter);
            map.Add(EntityType.UpdateStatusMapping, mappingSetWriter);
            map.Add(EntityType.TemplateMapping, mappingSetWriter);

            // Trascoding and rules
            TranscodingStreamWriter trancodingWriter = new TranscodingStreamWriter(commonStreamWriter);
            map.Add(EntityType.Transcoding, trancodingWriter);
            map.Add(EntityType.TranscodingRule, trancodingWriter);

            // Registry
            map.Add(EntityType.Registry, new RegistryStreamWriter(commonStreamWriter));

            //Nsiws
            map.Add(EntityType.Nsiws, new NsiwsStreamWriter(commonStreamWriter));

            // mapping store users
            map.Add(EntityType.User, new UserStreamWriter(commonStreamWriter));
            map.Add(EntityType.UserAction, new UserActionStreamWriter(commonStreamWriter));

            map.Add(EntityType.DataSource, new DataSourceStreamWriter(commonStreamWriter));

            return map;
        }

        public void WriteForExport(List<IEntity> entities)
        {
            if (entities.Any())
            {
                var entityType = entities.First().TypeOfEntity;
                writer.WritePropertyName(entityType.GetDescription());
                StartMessage();
                StartSection(entityType);
                Write(entities);
                writer.WriteEndArray();
            }
        }

        public void WriteRoot(string root)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(root);
            writer.WriteStartObject();
        }

        public void Write(IEnumerable<TimeDimensionMappingEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<LastUpdatedEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }

        public void Write(IEnumerable<ValidToMappingEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<UpdateStatusEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }

        public void Close()
        {
            Dispose(true);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool managed)
        {
            if (disposed)
            {
                return;
            }
            disposed = true;
            if (managed)
            {
                if (!_closeWriter)
                {
                    return;
                }

                if (writer != null)
                {
                    if (this._messageArray != null)
                    {
                        this._messageArray.Dispose();
                    }
                    writer.Flush();
                    writer.Close();
                }
                if (textWriter != null && (writer == null || !writer.CloseOutput))
                {
                    textWriter.Flush();
                    textWriter.Dispose();
                }
            }
        }

        /// <inherit/>
        public void StartMessage()
        {
            this._messageArray = commonWriter.WriteStartArray();
        }

        /// <inherit/>
        public void StartSection(EntityType entityType)
        {
            if (this._streamWriters.TryGetValue(entityType, out IEntityStreamingWriter writer))
            {
                this._currentStreamingWriter = writer;
            }
            else
            {
                throw new SdmxNotImplementedException("Writer for entity Type " + entityType);
            }
        }

        public void Write(IEnumerable<IConnectionEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<DatasetEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<DataSetColumnEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<LocalCodeEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<MappingSetEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<ColumnDescriptionSourceEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<ComponentMappingEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<TranscodingEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<TranscodingRuleEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<HeaderEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<RegistryEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }

        public void Write(IEnumerable<NsiwsEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<UserEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<TemplateMapping> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }

        public void Write(IEnumerable<IEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }

        public void Write(IEntity entity)
        {
          
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            // Note this is not thread safe, only trying to avoid being called by it self
            if (_calledFromWriteIEntity)
            {
                throw new InvalidOperationException("BUG loop detected in writer for entity of type"  + entity.TypeOfEntity);
            }
            _calledFromWriteIEntity = true;
            try
            {
                switch (entity.TypeOfEntity)
                {
                    case EntityType.DdbConnectionSettings:
                        if (entity is IConnectionEntity)
                        {
                            Write((IConnectionEntity)entity);
                        }
                        break;

                    case EntityType.DataSet:
                        Write((DatasetEntity)entity);
                        break;

                    case EntityType.DataSetProperty:
                        Write((DatasetPropertyEntity)entity);
                        break;

                    case EntityType.DataSetColumn:
                        Write((DataSetColumnEntity)entity);
                        break;

                    case EntityType.LocalCode:
                        Write((LocalCodeEntity)entity);
                        break;

                    case EntityType.MappingSet:
                        Write((MappingSetEntity)entity);
                        break;

                    case EntityType.Mapping:
                        Write((ComponentMappingEntity)entity);
                        break;
                    case EntityType.LastUpdated:
                        Write((LastUpdatedEntity)entity);
                        break;
                    case EntityType.UpdateStatusMapping:
                        Write((UpdateStatusEntity)entity);
                        break;
                    case EntityType.ValidToMapping: 
                        Write((ValidToMappingEntity)entity); 
                        break;
                        
                    case EntityType.Transcoding:
                        Write((TranscodingEntity)entity);
                        break;

                    case EntityType.TranscodingRule:
                        Write((TranscodingRuleEntity)entity);
                        break;

                    case EntityType.DescSource:
                        Write((ColumnDescriptionSourceEntity)entity);
                        break;

                    case EntityType.Header:
                        Write((HeaderEntity)entity);
                        break;

                    case EntityType.Registry:
                        Write((RegistryEntity)entity);
                        break;
                    case EntityType.User:
                        Write((UserEntity)entity);
                        break;
                    case EntityType.UserAction:
                        Write((UserActionEntity)entity);
                        break;
                    case EntityType.TemplateMapping:
                        Write((TemplateMapping)entity);
                        break;
                    case EntityType.DataSource:
                        Write((DataSourceEntity)entity);
                        break;
                    case EntityType.Nsiws:
                        Write((NsiwsEntity)entity);
                        break;

                    case EntityType.TimeMapping:
                        Write((TimeDimensionMappingEntity)entity);
                        break;
                }
            }
            finally 
            {
                _calledFromWriteIEntity = false;
            }
        }

        public void WriteProperty(string name, string value)
        {
            commonWriter.WriteProperty(name, value);
        }

        public void Write(IConnectionEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(DatasetEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(DatasetPropertyEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(DataSetColumnEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(LocalCodeEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        private IEntityStreamingWriter GetCurrentStreamingWriter()
        {
            if (_currentStreamingWriter == null)
            {
                throw new Exceptions.MissingInformationException("StartSection must be called first");
            }
            return _currentStreamingWriter;
        }

        public void Write(MappingSetEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(ColumnDescriptionSourceEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(ComponentMappingEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(TranscodingEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(TranscodingRuleEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(DataSourceEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(HeaderEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(RegistryEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(TimeDimensionMappingEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }
        public void Write(LastUpdatedEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }
        public void Write(ValidToMappingEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }
        public void Write(UpdateStatusEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(NsiwsEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(UserEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(TemplateMapping entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(UserActionEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(TimePreFormattedEntity entity)
        {
            GetCurrentStreamingWriter().Write(entity);
        }

        public void Write(IEnumerable<UserActionEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }

        public void WriteStatus(StatusType status, string message)
        {
            writer.WriteStartObject();
            writer.WritePropertyName("Status");
            writer.WriteValue(status.ToString());
            writer.WritePropertyName("Message");
            writer.WriteValue(message);
            writer.WriteEndObject();
        }
    }
}
