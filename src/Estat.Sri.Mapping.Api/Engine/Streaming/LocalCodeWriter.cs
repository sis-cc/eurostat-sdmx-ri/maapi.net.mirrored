// -----------------------------------------------------------------------
// <copyright file="LocalCodeWriter.cs" company="EUROSTAT">
//   Date Created : 2019-11-14
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    /// <summary>
    /// Wrapper that converts string values for dataset columns and local codes to entities 
    /// </summary>
    public class LocalCodeWriter : ILocalDataWriter
    {
        private readonly ILocalDataWriter _decoratedWriter;
        private readonly DataSetColumnEntity[] _datasetColumns;
        private readonly Dictionary<string, string>[] _descriptions;

        public LocalCodeWriter(ILocalDataWriter writer, DataSetColumnEntity[] datasetColumns, Dictionary<string, Dictionary<string, string>> descriptions)
        {
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            if (datasetColumns == null)
            {
                throw new ArgumentNullException(nameof(datasetColumns));
            }

            this._decoratedWriter = writer;
            this._datasetColumns = datasetColumns;

            // micro optimization to access by index instead of dictionary
            this._descriptions = new Dictionary<string, string>[datasetColumns.Length];
            for(int i=0; i < datasetColumns.Length; i++)
            {
                var d = datasetColumns[i].Name;
                if (descriptions.TryGetValue(d, out Dictionary<string,string> description))
                {
                    _descriptions[i] = description;
                }
                else
                {
                    _descriptions[i] = null;
                }
            }
        }

        public void Dispose()
        {
            // NO OP
        }

        public void SetDescriptions(string columnName, Dictionary<string, string> valueDescriptions)
        {
            _decoratedWriter.SetDescriptions(columnName, valueDescriptions);
        }

        public void StartMessage(IList<string> columns, int start, int end)
        { 
            if (columns.Count != this._datasetColumns.Length)
            {
                throw new ArgumentException("Expected number of columns to be equal to dataset columns provided to the constructor which is " + _datasetColumns.Length, nameof(columns));
            }

            for (int i=0;i < columns.Count; i++)
            {
                if (!columns[i].Equals(_datasetColumns[i].Name))
                {
                    throw new ArgumentException($"Expected order of columns to be equal to dataset columns but at position {i} at method provided '{columns[i]}' vs constuctor provided '{_datasetColumns[i].Name}'  " + _datasetColumns.Length, nameof(columns));
                }
            }

            _decoratedWriter.StartMessage(this._datasetColumns, start, end);
        }

        public void StartMessage(IList<DataSetColumnEntity> columns, int start, int end)
        {
            _decoratedWriter.StartMessage(columns, start, end);
        }

        public void WriteErrors(IList<string> errorMessages)
        {
            _decoratedWriter.WriteErrors(errorMessages);
        }

        public void WriteProperty(string name, object value)
        {
            _decoratedWriter.WriteProperty(name, value);
        }

        public void WriteRow(params string[] values)
        {
            if (values == null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            if (values.Length != _datasetColumns.Length)
            {
                throw new ArgumentException($"Expected number of values {values.Length} to be equal to dataset columns provided to the constructor which is " + _datasetColumns.Length, nameof(values));
            }

            var codes = new LocalCodeEntity[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                string value = values[i];
                LocalCodeEntity localCode = new LocalCodeEntity();
                localCode.ObjectId = value;
                localCode.ParentId = _datasetColumns[i].EntityId;
                if (_descriptions[i] != null && _descriptions[i].TryGetValue(value, out string description))
                {
                    // technically it is the name of the local code now
                    // in the past it was called description because SDMX v2.0 codes could only have description
                    localCode.Name = description;
                }

                codes[i] = localCode;
            }

            _decoratedWriter.WriteRow(codes);
        }

        public void WriteRow(params LocalCodeEntity[] values)
        {
            _decoratedWriter.WriteRow(values);
        }

        public void WriteStatus(StatusType status,string message)
        {
            _decoratedWriter.WriteStatus(status,message);
        }
    }
}
