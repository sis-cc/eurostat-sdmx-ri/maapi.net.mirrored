// -----------------------------------------------------------------------
// <copyright file="MappingSetStreamingWriter.cs" company="EUROSTAT">
//   Date Created : 2019-12-16
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Extension;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Utils;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Util;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    class MappingSetStreamingWriter : AbstractEntityStreamingWriter
    {
        private readonly CommonStreamWriter _commonWriter;
        private readonly AdvanceTimeTranscodingWriter _advanceTimeTranscodingWriter;
        private readonly TranscodingStreamWriter _transcodingStreamWriter;
        private readonly TimePreFormattedStreamWriter _timePreFormattedWriter;

        public MappingSetStreamingWriter(CommonStreamWriter commonWriter)
        {
            if (commonWriter == null)
            {
                throw new ArgumentNullException(nameof(commonWriter));
            }

            this._commonWriter = commonWriter;
            _transcodingStreamWriter = new TranscodingStreamWriter(commonWriter);
            _timePreFormattedWriter = new TimePreFormattedStreamWriter(commonWriter);
            _advanceTimeTranscodingWriter = new AdvanceTimeTranscodingWriter();
        }

        public override void Write(MappingSetEntity entity)
        {
            if (entity != null)
            {
                using (_commonWriter.WriteStartObject())
                {
                    _commonWriter.WriteEntityId(entity);
                    _commonWriter.WriteParentId(entity);
                    _commonWriter.WriteSimpleNameable(entity);
                    using (_commonWriter.WriteStartObject("dataset"))
                    {
                        _commonWriter.WriteProperty("entityId", entity.DataSetId);
                    }
                    _timePreFormattedWriter.Write(entity.TimePreFormattedEntity);
                    _commonWriter.WriteDetailsLinks(entity);
                    _commonWriter.WriteProperty("validFrom", entity.ValidFrom);
                    _commonWriter.WriteProperty("validTo", entity.ValidTo);
                    _commonWriter.WriteProperty("isMetadata", entity.IsMetadata);
                }
            }
        }

        public override void Write(TemplateMapping entity)
        {
            if (entity != null)
            {
                using (_commonWriter.WriteStartObject())
                {
                    _commonWriter.WriteEntityId(entity);
                    _commonWriter.WriteProperty("column_name",entity.ColumnName);
                    _commonWriter.WriteProperty("column_description",entity.ColumnDescription);
                    _commonWriter.WriteProperty("component_id",entity.ComponentId);
                    _commonWriter.WriteProperty("component_type",entity.ComponentType);
                    _commonWriter.WritePropertyObject("concept_urn",entity.ConceptUrn);
                    _commonWriter.WritePropertyObject("item_scheme_id",entity.ItemSchemeId?.MaintainableUrn?.ToString());
                    if (ObjectUtil.ValidCollection((ICollection)entity.Transcodings))
                    {
                        using (_commonWriter.WriteStartObject("transcodings"))
                        {
                            foreach (var item in entity.Transcodings)
                            {
                                _commonWriter.WriteProperty(item.Key, item.Value);
                            }
                        }
                    }

                    if (ObjectUtil.ValidCollection((ICollection)entity.TimeTranscoding))
                    {
                        using (_commonWriter.WriteStartObject("timeTranscodings"))
                        {
                            foreach (var item in entity.TimeTranscoding)
                            {
                                this._transcodingStreamWriter.WriteSimpleTimeTranscoding(entity.TimeTranscoding);
                            }
                        }
                    }

                    _commonWriter.WriteDetailsLinks(entity);
                }
            }
        }

        public override void Write(LastUpdatedEntity entity)
        {
            this.WriteValidity(entity);
        }
        public override void Write(ValidToMappingEntity entity)
        {
            this.WriteValidity(entity);
        }


        private void WriteValidity(ValidityBaseEntity entity)
        {
            if (entity != null)
            {
                using (_commonWriter.WriteStartObject())
                {
                    _commonWriter.WriteEntityId(entity);
                    _commonWriter.WriteProperty("type", entity.MappingType.AsMappingStoreType());
                    _commonWriter.WriteParentId(entity);
                    _commonWriter.WriteDetailsLinks(entity);
                    _commonWriter.WriteProperty("constantValue", entity.ConstantValue);
                    var columns = entity.GetColumns();
                    using (_commonWriter.WriteStartArray("dataset_column"))
                    {
                        if (ObjectUtil.ValidCollection(columns))
                        {

                            for (int i = 0; i < columns.Count; i++)
                            {
                                var column = columns[i];
                                // HACK
                                column.IsMapped = ColumnMappingStatus.Yes;
                                DatasetStreamWriter.WriteDataSetColumn(_commonWriter, column);
                            }

                        }
                    }
                }
            }
        }
        public override void Write(UpdateStatusEntity entity)
        {
                if (entity != null)
            {
                using (_commonWriter.WriteStartObject())
                {
                    _commonWriter.WriteEntityId(entity);
                    _commonWriter.WriteProperty("type", entity.MappingType.AsMappingStoreType());
                    _commonWriter.WriteParentId(entity);
                    _commonWriter.WriteDetailsLinks(entity);
                    _commonWriter.WriteProperty("constantValue", entity.ConstantValue);
                    WriteSpecialMappingColumn(entity);

                    if (entity.HasTranscoding())
                    {
                        using (_commonWriter.WriteStartArray("rule"))
                        {
                            foreach (var rule in entity.Transcoding)
                            {
                                using (_commonWriter.WriteStartObject())
                                {
                                    _commonWriter.WriteProperty("sdmxCode", ObservationAction.GetFromEnum(rule.Value).ObsAction);
                                    _commonWriter.WriteProperty("localCode", rule.Key);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void WriteSpecialMappingColumn(UpdateStatusEntity entity)
        {
            var columns = entity.GetColumns();
            using (_commonWriter.WriteStartArray("dataset_column"))
            {
                if (ObjectUtil.ValidCollection(columns))
                {

                    for (int i = 0; i < columns.Count; i++)
                    {
                        var column = columns[i];
                        // HACK
                        column.IsMapped = ColumnMappingStatus.Yes;
                        DatasetStreamWriter.WriteDataSetColumn(_commonWriter, column);
                    }

                }
            }
        }


        public override void Write(ComponentMappingEntity entity)
        {
            if (entity != null)
            {
                using (_commonWriter.WriteStartObject())
                {
                    _commonWriter.WriteEntityId(entity);
                    _commonWriter.WriteProperty("type", entity.Type);
                    _commonWriter.WriteParentId(entity);
                    _commonWriter.WriteDetailsLinks(entity);
                    _commonWriter.WriteProperty("constantValue", entity.ConstantValue);
                    _commonWriter.WriteProperty("defaultValue", entity.DefaultValue);
                    _commonWriter.WriteProperty("isMetadata", entity.IsMetadata);
                    using (_commonWriter.WriteStartArray("constantValues"))
                    {
                        foreach(string constValue in entity.ConstantValues)
                        {
                            _commonWriter.WriteValue(constValue);
                        }
                    }
                    using (_commonWriter.WriteStartArray("defaultValues"))
                    {
                        foreach (string defValue in entity.DefaultValues)
                        {
                            _commonWriter.WriteValue(defValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(entity.TranscodingId))
                    {
                        var transcoding = new TranscodingEntity();
                        transcoding.EntityId = entity.TranscodingId;
                        transcoding.ParentId = entity.EntityId;
                        transcoding.StoreId = entity.StoreId;
                        _commonWriter.WritePropertyName("transcoding");
                        this._transcodingStreamWriter.WriteTranscoding(transcoding);
                    }

                    Component component = entity.Component;
                    _commonWriter.WriteIdentifiable("component", component);
                    var columns = entity.GetColumns();
                    using (_commonWriter.WriteStartArray("dataset_column"))
                    {
                        if (ObjectUtil.ValidCollection(columns))
                        {

                            for (int i = 0; i < columns.Count; i++)
                            {
                                var column = columns[i];
                                // HACK
                                column.IsMapped = string.IsNullOrEmpty(entity.TranscodingId) ? ColumnMappingStatus.Yes : ColumnMappingStatus.YesAndTranscoded;
                                DatasetStreamWriter.WriteDataSetColumn(_commonWriter, column);
                            }

                        }
                    }
                }
            }
        }

        public override void Write(TimeDimensionMappingEntity entity)
        {
            if (entity == null)
            {
                return;
            }

            // The json should look like Component Mapping because code in MAWEB expects it to
            using (_commonWriter.WriteStartObject())
            {
                _commonWriter.WriteEntityId(entity);
                _commonWriter.WriteProperty("type", entity.TimeMappingType.ToString());
                _commonWriter.WriteParentId(entity);
                _commonWriter.WriteDetailsLinks(entity);
                _commonWriter.WriteProperty("constantValue", entity.ConstantValue);
                _commonWriter.WriteProperty("defaultValue", entity.DefaultValue);

                switch (entity.TimeMappingType)
                {
                    case TimeDimensionMappingType.MappingWithColumn:
                        _commonWriter.WriteProperty("column", entity.SimpleMappingColumn);
                        break;
                    case TimeDimensionMappingType.PreFormatted:

                        if (entity.PreFormatted != null)
                        {
       
                            _timePreFormattedWriter.Write(entity.PreFormatted);
                        }

                        break;
                    case TimeDimensionMappingType.TranscodingWithCriteriaColumn:
                    case TimeDimensionMappingType.TranscodingWithFrequencyDimension:
                        var timeTranscodingAdvancedEntity = entity.Transcoding;
                        if (timeTranscodingAdvancedEntity != null)
                        {

                            if (entity.TimeMappingType == TimeDimensionMappingType.TranscodingWithFrequencyDimension)
                            {
                                var timeTranscodingEntities =
                                    TimeTranscodingConversionHelper.Convert(timeTranscodingAdvancedEntity);
                                using (_commonWriter.WriteStartObject("timeTranscoding"))
                                {
                                    _transcodingStreamWriter.WriteSimpleTimeTranscoding(timeTranscodingEntities);
                                }
                            }
                            else
                            {
                                // advanced time transcoding
                                using (_commonWriter.WriteStartObject("transcodingAdvancedTime"))
                                {
                                    _advanceTimeTranscodingWriter.Write(_commonWriter, timeTranscodingAdvancedEntity);
                                }
                            }
                        }

                        break;
                    case TimeDimensionMappingType.MappingWithConstant:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                
                using (_commonWriter.WriteStartArray("dataset_column"))
                {
                    var datasetColumns = entity.GetColumns();
                    if (ObjectUtil.ValidCollection(datasetColumns))
                    {
                        for (int i = 0; i < datasetColumns.Count; i++)
                        {
                            var column = datasetColumns[i];
                            // HACK
                            column.IsMapped = entity.Transcoding == null ? ColumnMappingStatus.Yes  : ColumnMappingStatus.YesAndTranscoded;
                            column.IsTimePreFormatted = entity.PreFormatted != null;
                            DatasetStreamWriter.WriteDataSetColumn(_commonWriter, column);
                        }
                    }
                }
            }
        }
     
        public override void Write(DataSetColumnEntity entity)
        {
            if (entity != null)
            {
                DatasetStreamWriter.WriteDataSetColumn(_commonWriter, entity);
            }
        }
    }
}
