// -----------------------------------------------------------------------
// <copyright file="CommonStreamWriter.cs" company="EUROSTAT">
//   Date Created : 2019-12-13
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    class CommonStreamWriter 
    {
        private readonly Action<IEntity, JsonWriter, string> _writeLinks;
        private readonly Action<IEntity, JsonWriter> _writeDetails;
        public readonly JsonWriter writer;
        private readonly string sid;

        public CommonStreamWriter(JsonWriter writer, string sid, Action<IEntity, JsonWriter, string> writeLinks, Action<IEntity, JsonWriter> writeDetails)
        {
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            // sid is needed only for write details
            if (sid == null && _writeDetails != null)
            {
                throw new ArgumentNullException(nameof(sid));
            }

            this.writer = writer;
            this.sid = sid;
            _writeLinks = writeLinks;
            _writeDetails = writeDetails;
        }

        public void WriteProperty(string name, string value)
        {
            writer.WritePropertyName(name);
            writer.WriteValue(value);
        }

        public void WriteProperty(string name, bool? value)
        {
            writer.WritePropertyName(name);
            writer.WriteValue(value);
        }

        public void WriteProperty(string name, int? value)
        {
            writer.WritePropertyName(name);
            writer.WriteValue(value);
        }

        public void WriteProperty(string name, DateTime? value)
        {
            writer.WritePropertyName(name);
            writer.WriteValue(value);
        }

        public void WritePropertyObject(string name, object value)
        {
            writer.WritePropertyName(name);
            writer.WriteValue(value);
        }

        public void WritePropertyName(string name)
        {
            writer.WritePropertyName(name);
        }

        public void WriteDetailsLinks(IEntity entity)
        {
            string sid = this.sid;
            if (_writeDetails != null)
            {
                _writeDetails(entity, writer);
            }
            if (_writeLinks != null)
            {
                _writeLinks(entity, writer, sid);
            }
        }

        public void TryWriteOther<TValueType>(string propertyName, TValueType value)
        {
            if (value == null)
            {
                return;
            }

            writer.WritePropertyName(propertyName);
            writer.WriteValue(value);
        }
        public void TryWrite(string propertyName, string value)
        {
            writer.WritePropertyName(propertyName);
            writer.WriteValue(value);
        }
        public void WriteParentId(IEntity item)
        {
            TryWrite("parentId", item.ParentId);
        }
        public void WriteSimpleNameable(ISimpleNameableEntity entity)
        {
            TryWrite("name", entity.Name);
            TryWrite("description", entity.Description);
        }
        public void WriteEntityId(IEntity entity)
        {
            writer.WritePropertyName("entityId");
            writer.WriteValue(entity.EntityId);
        }
        public EndObjectDispose WriteStartObject(string propertyName)
        {
            writer.WritePropertyName(propertyName);
            return this.WriteStartObject();
        }

        public void WritePropertyRaw(string propertyName, string json)
        {
            writer.WritePropertyName(propertyName);
            if (string.IsNullOrWhiteSpace(json))
            {
                writer.WriteValue(string.Empty);
            }
            else
            {
                writer.WriteRawValue(json);
            }
        }
        public void Write(string propertyName, IEnumerable<string> values)
        {
            using (WriteStartArray(propertyName))
            {
                foreach (string value in values)
                {
                    writer.WriteValue(value);
                }
            }
        }
        public void Write(string propertyName, IList<ITextTypeWrapper> names)
        {
            using (WriteStartObject(propertyName))
            {
                foreach (var name in names)
                {
                    WriteProperty(name.Locale, name.Value);
                }
            }
        }
        public EndObjectDispose WriteStartObject()
        {
            writer.WriteStartObject();
            return new EndObjectDispose(writer,false);
        }

        public EndObjectDispose WriteStartArray(string propertyName)
        {
            writer.WritePropertyName(propertyName);
            return this.WriteStartArray();
        }

        public EndObjectDispose WriteStartArray()
        {
            writer.WriteStartArray();
            return new EndObjectDispose(writer,true);
        }
        public void WriteIdentifiable(string propertyName, IIdentifiableEntity component)
        {
            if (component == null)
            {
                return;
            }

            using (WriteStartObject(propertyName))
            {
                if (!string.IsNullOrEmpty(component.EntityId))
                {
                    WriteEntityId(component);
                }

                writer.WritePropertyName("id");
                writer.WriteValue(component.ObjectId);
            }
        }
        public void WritePermissions(IPermissionEntity entity)
        {
            if (entity.Permissions == null)
            {
                return;
            }
            using (WriteStartObject("permissions"))
            {
                foreach (var permission in entity.Permissions)
                {
                    writer.WritePropertyName(permission.Key);
                    writer.WriteValue(permission.Value);
                }
            }
        }
        public class EndObjectDispose : IDisposable
        {
            private readonly JsonWriter writer;
            private readonly bool isArray;
            private bool disposedValue = false; // To detect redundant calls

            public EndObjectDispose(JsonWriter writer, bool isArray)
            {
                if (writer == null)
                {
                    throw new ArgumentNullException(nameof(writer));
                }

                this.writer = writer;
                this.isArray = isArray;
            }


            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        if (writer.WriteState == WriteState.Array)
                        {
                            writer.WriteEndArray();
                        }
                        else
                        {
                            writer.WriteEndObject();
                        }
                    }
                    disposedValue = true;
                }
            }

         
            public void Dispose()
            {
                Dispose(true);
            }
        }

        internal void WriteValue(string value)
        {
            writer.WriteValue(value);
        }
    }
}
