// -----------------------------------------------------------------------
// <copyright file="AuthEntityStreamingReaderDecorator.cs" company="EUROSTAT">
//   Date Created : --
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Utils;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    /// <summary>
    /// Decorator for permissions authorization when reading
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Engine.IEntityStreamingReader" />
    public class AuthEntityStreamingReaderDecorator : IEntityStreamingReader
    {
        private readonly IEntityStreamingReader _decoratedEngine;
        private readonly IEntityAuthorizationManager _authorizationManager;
        private readonly SdmxAuthorizationScope scope;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthEntityStreamingReaderDecorator"/> class.
        /// </summary>
        /// <param name="decoratedEngine">The decorated engine.</param>
        /// <param name="authorizationManager">The authorization manager.</param>
        public AuthEntityStreamingReaderDecorator(IEntityStreamingReader decoratedEngine, IEntityAuthorizationManager authorizationManager, SdmxAuthorizationScope scope)
        {
            _decoratedEngine = decoratedEngine;
            _authorizationManager = authorizationManager;
            this.scope = scope;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _decoratedEngine.Dispose();
            scope.Dispose();
        }

        /// <summary>
        /// Returns true if there is a JSON message (maybe we could have aasdasn identifier like "entities")
        /// </summary>
        /// <returns></returns>
        public bool MoveToNextMessage()
        {
            return _decoratedEngine.MoveToNextMessage();
        }

        /// <summary>
        /// Moves to the next section, must be called at least once
        /// </summary>
        /// <returns></returns>
        public bool MoveToNextEntityTypeSection()
        {
            return _decoratedEngine.MoveToNextEntityTypeSection();
        }

        /// <summary>
        /// gets the current entity type, changes every time we all MoveToNextEntityTypeSection()
        /// </summary>
        public EntityType CurrentEntityType => _decoratedEngine.CurrentEntityType;

        /// <summary>
        /// Moves to the next entity, it has be called at least once
        /// </summary>
        /// <returns></returns>
        public bool MoveToNextEntity()
        {
            return _decoratedEngine.MoveToNextEntity();
        }

        /// <summary>
        /// Get the current entity
        /// </summary>
        public EntityWithResult CurrentEntity {
            get
            {
                var entity = _decoratedEngine.CurrentEntity;
                if (this._authorizationManager.CanAccess(entity.Entity, AccessType.Create))
                {
                    return entity;
                }
                else
                {
                    return new EntityWithResult()
                    {
                        Entity = entity.Entity,
                        Status = StatusType.Warning,
                        Message = $"entity with name {entity.Entity.EntityId} was not inserted because the user doesn't have the appropriate rights"
                    };
                }

            }
        }
        /// <summary>
        /// A lazy enumeration a combination of MoveToNextEntity and CurrentEntity
        /// </summary>
        /// <returns></returns>
        public IList<IEntity> GetCurrentEntities()
        {
            return _decoratedEngine.GetCurrentEntities();
        }

        /// <summary>
        /// close and free resources
        /// </summary>
        public void Close()
        {
            _decoratedEngine.Close();
        }
    }
}