// -----------------------------------------------------------------------
// <copyright file="AbstractEntityStreamingWriter.cs" company="EUROSTAT">
//   Date Created : 2019-12-13
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    /// <summary>
    /// An abstract class that does nothing, it should be used to extend only parts of the interface
    /// </summary>
    public abstract class AbstractEntityStreamingWriter : IEntityStreamingWriter
    {
        public int Status { get; set; }

        public virtual void Write(IEnumerable<TimeDimensionMappingEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Close()
        {
            this.Dispose();
        }

        public virtual void StartMessage()
        {
            throw new NotImplementedException();
        }

        public virtual void StartSection(EntityType entityType)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IConnectionEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(DatasetEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(DatasetPropertyEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(DataSetColumnEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(LocalCodeEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(MappingSetEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(ColumnDescriptionSourceEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(ComponentMappingEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(TranscodingEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(TranscodingRuleEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(HeaderEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(RegistryEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(TimeDimensionMappingEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(NsiwsEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(UserEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(DataSourceEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(TimePreFormattedEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<IEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<IConnectionEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<DatasetEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<DataSetColumnEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<LocalCodeEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<MappingSetEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<ColumnDescriptionSourceEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<ComponentMappingEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<TranscodingEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<TranscodingRuleEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<HeaderEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<RegistryEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<NsiwsEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<UserEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void WriteProperty(string name, string value)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(TemplateMapping entity)
        {
            throw new NotImplementedException();
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        public virtual void Write(IEnumerable<TemplateMapping> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(UserActionEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<UserActionEntity> entity)
        {
            throw new NotImplementedException();
        }

        public void WriteStatus(StatusType status,string message)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(LastUpdatedEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(ValidToMappingEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<LastUpdatedEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual  void Write(IEnumerable<ValidToMappingEntity> entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(UpdateStatusEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Write(IEnumerable<UpdateStatusEntity> entity)
        {
            throw new NotImplementedException();
        }
    }
}