// -----------------------------------------------------------------------
// <copyright file="DatasetPropertyStreamWriter.cs" company="EUROSTAT">
//   Date Created : 2021-11-11
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    internal class DatasetPropertyStreamWriter : AbstractEntityStreamingWriter
    {
        private readonly CommonStreamWriter commonWriter;

        /// <summary>
        /// Inti
        /// </summary>
        /// <param name="commonWriter">The JSON writer. It is assumed this is disposed outside this class</param>
        public DatasetPropertyStreamWriter(CommonStreamWriter commonWriter)
        {
            if (commonWriter == null)
            {
                throw new ArgumentNullException(nameof(commonWriter));
            }

            this.commonWriter = commonWriter;
        }

        public override void Write(DatasetPropertyEntity entity)
        {
            if (entity != null)
            {
                using (commonWriter.WriteStartObject("datasetProperty"))
                {
                    commonWriter.WriteProperty("orderByTimePeriodAsc", entity.OrderByTimePeriodAsc);
                    commonWriter.WriteProperty("orderByTimePeriodDesc", entity.OrderByTimePeriodDesc);
                    commonWriter.WriteProperty("crossApplyColumn", entity.CrossApplyColumn);
                    commonWriter.WriteProperty("crossApplySupported", entity.CrossApplySupported);
                }
            }
        }

        public override void Write(DataSetColumnEntity entity)
        {
            if (entity != null)
            {
                commonWriter.WriteEntityId(entity);
                commonWriter.WriteProperty("name", entity.Name);
            }
        }

    }
}