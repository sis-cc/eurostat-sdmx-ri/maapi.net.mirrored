// -----------------------------------------------------------------------
// <copyright file="AdvanceTimeTranscodingWriter.cs" company="EUROSTAT">
//   Date Created : 2019-12-11
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Model.AdvancedTime;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    class AdvanceTimeTranscodingWriter
    {
        /// <summary>
        /// Write advanced time transcoding if it is not null
        /// </summary>
        /// <param name="writer">The json writer</param>
        /// <param name="entity">The <see cref="TimeTranscodingAdvancedEntity"/></param>
        /// <example> JSON Schema
        /// <code>
        /// <![CDATA[
        /// "criteriaValue": {
        ///     "type": "string",
        ///     "description": "The critera value of the criteria column that is used to selected this particular configuration over the others"
        /// },
        /// "outputFormat": {
        ///     "$ref": "#/definitions/TimeFormat",
        ///     "description": "The Time Dimension output format to use for this configuration"
        /// },
        /// "startConfig": {
        ///     "$ref": "#/definitions/TimeParticleConfigurationObject",
        ///     "description": "The (start) period configuration"
        /// },
        /// "endConfig": {
        ///     "$ref": "#/definitions/TimeParticleConfigurationObject",
        ///     "description": "The end period configuration"
        /// },
        /// "durationMap": {
        ///     "$ref": "mapping.json#",
        ///     "description": "The duration mapping"
        /// }
        /// ]]>
        /// </code>
        /// </example>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// "TranscodingAdvancedTime": {
        ///    "type": "object",
        ///    "description": "Represents the Advanced Time Transcoding",
        ///    "properties": {
        ///        "criteriaColumn": {
        ///            "$ref": "dataset-column.json#"
        ///        },
        ///        "timeFormatConfigurations": {
        ///            "type": "object",
        ///            "additionalProperties": {
        ///                "$ref": "#/definitions/TimeFormatConfigurationObject"
        ///            }
        ///        }
        ///    }
        /// },
        /// ]]>
        /// </code>
        /// </example>
        public void Write(CommonStreamWriter writer, TimeTranscodingAdvancedEntity entity)
        {
            if (entity == null)
            {
                return;
            }
            var mappingSetWriter = new MappingSetStreamingWriter(writer);

            {
                WriteDataSetColumn(writer, "criteriaColumn", entity.CriteriaColumn);
                writer.WriteProperty("isFrequencyBased", entity.IsFrequencyDimension);
                using (writer.WriteStartObject("timeFormatConfigurations"))
                {
                    foreach (KeyValuePair<string, TimeFormatConfiguration> config in entity.TimeFormatConfigurations)
                    {
                        using (writer.WriteStartObject(config.Key))
                        {
                            writer.WriteProperty("criteriaValue", config.Value.CriteriaValue);
                            writer.WriteProperty("outputFormat", Convert(config.Value.OutputFormat));
                            if (config.Value.HasParticleConfiguration)
                            {
                                if (config.Value.StartConfig != null)
                                {
                                    using (writer.WriteStartObject("startConfig"))
                                    {
                                        WriteParticleConfiguration(writer, config.Value.StartConfig);
                                    }
                                }
                                if (config.Value.EndConfig != null)
                                {
                                    using (writer.WriteStartObject("endConfig"))
                                    {
                                        WriteParticleConfiguration(writer, config.Value.EndConfig);
                                    }
                                }

                                var durationMappingEntity = config.Value.DurationMap;
                                Write(writer, durationMappingEntity);
                            }
                        }
                    }
                }
            }
        }

        private void Write(CommonStreamWriter writer, DurationMappingEntity durationMappingEntity)
        {
            if (durationMappingEntity != null)
            {
                using (writer.WriteStartObject("durationMap"))
                {
                    writer.WriteProperty("entityId", durationMappingEntity.EntityId);
                    writer.WriteProperty("type", durationMappingEntity.MappingType.ToString("G").ToUpperInvariant());
                    writer.WriteProperty("parentId", durationMappingEntity.ParentId);
                    writer.WriteProperty("constantValue", durationMappingEntity.ConstantValue);
                    writer.WriteProperty("defaultValue", durationMappingEntity.DefaultValue);

                    // TODO kept for backwards compatibility
                    // if not needed we can remove
                    if (durationMappingEntity.HasTranscoding())
                    {
                        using (writer.WriteStartObject("transcoding"))
                        {
                            writer.WriteProperty("obsolete", "transcoding rules are in 'rules' property");
                        }
                    }
                    if (durationMappingEntity.Column != null)
                    {
                        // HACK do we need isMapped ?
                        var copy = durationMappingEntity.Column.CreateShallowCopy<DataSetColumnEntity>();
                        copy.IsMapped = !durationMappingEntity.HasTranscoding() ? ColumnMappingStatus.Yes : ColumnMappingStatus.YesAndTranscoded;
                        WriteDataSetColumn(writer, "dataset_column", copy);
                    }

                    if (durationMappingEntity.HasTranscoding())
                    {
                        using (writer.WriteStartArray("rule"))
                        {
                            foreach (var transcodingRule in durationMappingEntity.TranscodingRules)
                            {
                                using (writer.WriteStartObject())
                                {
                                    writer.WriteProperty("sdmxPeriodCode", transcodingRule.Key);
                                    writer.WriteProperty("localPeriodCode", transcodingRule.Value);
                                }
                                 
                            }
                        }
                    }
                }
            }
        }

        private void WriteDataSetColumn(CommonStreamWriter writer, string name, DataSetColumnEntity column)
        {
            writer.WritePropertyName(name);
            DatasetStreamWriter.WriteDataSetColumn(writer, column);
        }

        /// <summary>
        /// Writes a particle configuration if it is not null
        /// </summary>
        /// <param name="config">The <see cref="TimeParticleConfiguration"/></param>
        /// <example> The JSON Schema
        /// <code lang="js">
        /// <![CDATA[   "TimeParticleConfigurationObject": {
        ///    "type": "object",
        ///    "properties": {
        ///        "dateColumn": {
        ///            "$ref": "dataset-column.json#"
        ///         },
        ///         "year": {
        ///             "$ref": "#/definitions/TimeRelatedColumnInfoObject"
        ///         },
        ///         "period": {
        ///             "$ref": "#/definitions/PeriodTimeRelatedColumnInfoObject"
        ///         },
        ///         "format": {
        ///             "$ref": "#/definitions/DisseminationDatabaseTimeFormat"
        ///         }
        ///     }
        /// },]]>
        /// </code>
        /// </example>
        private void WriteParticleConfiguration(CommonStreamWriter writer, TimeParticleConfiguration config)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }

            writer.WriteProperty("format", Convert(config.Format));
            if (config.DateColumn != null)
            {
                WriteDataSetColumn(writer, "dateColumn", config.DateColumn);
            }
            if (config.Year != null)
            {
                using (writer.WriteStartObject("year"))
                {
                    Write(writer, config.Year);
                }
            }
            if (config.Period != null)
            {
                using(writer.WriteStartObject("period"))
                {
                    WritePeriod(writer, config.Period);
                }
            }
        }

        /// <summary>
        /// Write the period related information
        /// </summary>
        /// <param name="writer">The JSON Writer</param>
        /// <param name="period">The information</param>
        /// <example> The JSON Schema
        /// <code>
        /// <![CDATA[
        ///   "PeriodTimeRelatedColumnInfoObject": {
        ///             "allOf":[
        ///                 {
        ///                     "$ref": "#/definitions/TimeRelatedColumnInfoObject"
        ///                 },
        ///                 {
        ///                     "type":"object",
        ///                     "properties":{
        ///                         "rules":{
        ///                             "type":"array",
        ///                             "items":{
        ///                                 "$ref":"#/definitions/PeriodCodeMap"
        ///                             }
        ///                         }
        ///                     }
        ///                 }
        ///             ]
        ///         },
        /// ]]>
        /// </code>
        /// </example>
        private void WritePeriod(CommonStreamWriter writer, PeriodTimeRelatedColumnInfo period)
        {
            if (period == null)
            {
                return;
            }

            Write(writer, period);

            using(writer.WriteStartArray("rule"))
            {
                if (period.Rules != null)
                {
                    foreach(var rule in period.Rules)
                    {
                        using(writer.WriteStartObject())
                        {
                            writer.WriteProperty("sdmxPeriodCode", rule.SdmxPeriodCode);
                            writer.WriteProperty("localPeriodCode",rule.LocalPeriodCode);
                        }
                    }
                }

            }
        }


        /// <summary>
        /// Write the common/year related information
        /// </summary>
        /// <param name="writer">The JSON Writer</param>
        /// <param name="columnInfo">The information</param>
        /// <example> The JSON Schema
        /// <code>
        /// <![CDATA[
        ///  "TimeRelatedColumnInfoObject": {
        ///    "type":"object",
        ///    "properties":{
        ///        "column":{
        ///            "$ref": "dataset-column.json#"
        ///        },
        ///        "start":{
        ///            "type":"integer"
        ///        },
        ///        "length":{
        ///            "type":"integer"
        ///        }
        ///    }
        /// }
        /// ]]>
        /// </code>
        /// </example>
        private void Write(CommonStreamWriter writer, TimeRelatedColumnInfo columnInfo)
        {
            if (columnInfo == null)
            {
                return;
            }

            WriteDataSetColumn(writer, "column", columnInfo.Column);

            writer.WriteProperty("start", columnInfo.Start);
            writer.WriteProperty("length", columnInfo.Length);
        }

        private string Convert(DisseminationDatabaseTimeFormat format)
        {
            switch(format)
            {
                case DisseminationDatabaseTimeFormat.Annual: return "Annual";
                case DisseminationDatabaseTimeFormat.Monthly: return "Monthly";
                case DisseminationDatabaseTimeFormat.Weekly: return "Weekly";
                case DisseminationDatabaseTimeFormat.Quarterly: return "Quarterly";
                case DisseminationDatabaseTimeFormat.Semester: return "Semester";
                case DisseminationDatabaseTimeFormat.Trimester: return "Trimester";
                case DisseminationDatabaseTimeFormat.TimestampOrDate: return "TimestampOrDate";
                case DisseminationDatabaseTimeFormat.Iso8601Compatible: return "Iso8601Compatible";
            }

            // TODO maybe throw an exception but it could block editing a transcoding
            // On the other hand this value probably will be different than in Java
            return format.ToString();
        }

        private string Convert(TimeFormat outputFormat)
        {
            if (outputFormat == null)
            {
                return null;
            }

            switch(outputFormat.EnumType)
            {
                case TimeFormatEnumType.Year: return "Year";
                case TimeFormatEnumType.HalfOfYear: return "HalfOfYear";
                case TimeFormatEnumType.ThirdOfYear: return "ThirdOfYear";
                case TimeFormatEnumType.QuarterOfYear: return "QuarterOfYear";
                case TimeFormatEnumType.Month: return "Month";
                case TimeFormatEnumType.ReportingMonth: return "ReportingMonth";
                case TimeFormatEnumType.ReportingYear: return "ReportingYear";
                case TimeFormatEnumType.ReportingDay: return "ReportingDay";
                case TimeFormatEnumType.TimeRange: return "TimeRange";
                case TimeFormatEnumType.Week: return "Week";
                case TimeFormatEnumType.Date: return "Date";
                case TimeFormatEnumType.Hour: return "Hour";
                case TimeFormatEnumType.DateTime: return "DateTime";
            }

            // TODO maybe throw an exception here? but it will block editing a transcoding
            // On the other hand this value probably will be different than in Java
            return outputFormat.ReadableCode;
        }
    }
}
