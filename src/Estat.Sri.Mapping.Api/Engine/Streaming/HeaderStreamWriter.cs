// -----------------------------------------------------------------------
// <copyright file="HeaderStreamWriter.cs" company="EUROSTAT">
//   Date Created : 2019-12-16
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.SdmxXmlConstants;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    class HeaderStreamWriter : AbstractEntityStreamingWriter
    {
        private readonly CommonStreamWriter commonWriter;

        public HeaderStreamWriter(CommonStreamWriter commonWriter)
        {
            if (commonWriter == null)
            {
                throw new ArgumentNullException(nameof(commonWriter));
            }

            this.commonWriter = commonWriter;
        }

        public override void Write(HeaderEntity entity)
        {
            if (entity != null)
            {
                using (commonWriter.WriteStartObject())
                {
                    commonWriter.WriteEntityId(entity);
                    commonWriter.WriteParentId(entity);
                    IHeader sdmxHeader = entity.SdmxHeader;
                    if (sdmxHeader != null)
                    {
                        commonWriter.WriteProperty("dataSetAgencyId", sdmxHeader.GetAdditionalAttribtue("DataSetAgency"));
                        commonWriter.WriteProperty("name", sdmxHeader.Name.FirstOrDefault()?.Value ?? string.Empty);
                        commonWriter.WriteProperty("source", sdmxHeader.Source.FirstOrDefault()?.Value ?? string.Empty);
                        commonWriter.WriteProperty("test", sdmxHeader.Test);
                        commonWriter.WriteProperty("structureType", sdmxHeader.GetAdditionalAttribtue(nameof(ElementNameTable.StructureUsage)));
                        commonWriter.WriteProperty("dataSetId", sdmxHeader.GetAdditionalAttribtue(nameof(ElementNameTable.DataSetID)));
                        commonWriter.WriteProperty("dataSetAction", sdmxHeader.GetAdditionalAttribtue(nameof(ElementNameTable.DataSetAction)));
                        Write("sender", sdmxHeader.Sender);
                        Write("receiver", sdmxHeader.Receiver);
                    }
                }
            }
        }

        private void Write(string propertyName, IList<IParty> receiver)
        {
            using (commonWriter.WriteStartArray(propertyName))
            {
                foreach (IParty party in receiver)
                {
                    using(commonWriter.WriteStartObject())
                    {
                        Write(party);
                    }
                }
            }
        }

        public void Write(string propertyName, IParty buildFrom)
        {
            using (commonWriter.WriteStartObject(propertyName))
            {
                Write(buildFrom);
            }
        }

        private void Write(IParty buildFrom)
        {
            commonWriter.WriteProperty("id", buildFrom.Id);
            commonWriter.Write("name", buildFrom.Name);
            Write("contacts", buildFrom.Contacts);
            commonWriter.TryWrite("timeZone", buildFrom.TimeZone);
        }

        private void Write(string propertyName, IList<IContact> contacts)
        {
            using (commonWriter.WriteStartArray(propertyName))
            {
                foreach (IContact contact in contacts)
                {
                    Write(contact);
                }
            }
         }

        private void Write(IContact buildFrom)
        {
            using (commonWriter.WriteStartObject())
            {
                commonWriter.Write("department", buildFrom.Departments);
                commonWriter.Write("role", buildFrom.Role);
                commonWriter.Write("name", buildFrom.Name);
                commonWriter.Write("email", buildFrom.Email);
                commonWriter.Write("telephone", buildFrom.Telephone);
                commonWriter.Write("uri", buildFrom.Uri);
                commonWriter.Write("fax", buildFrom.Fax);
                commonWriter.Write("x400", buildFrom.X400);
            }
        }

    }
}
