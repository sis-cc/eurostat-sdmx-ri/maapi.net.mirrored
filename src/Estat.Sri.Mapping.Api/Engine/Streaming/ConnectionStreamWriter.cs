// -----------------------------------------------------------------------
// <copyright file="ConnectionStreamWriter.cs" company="EUROSTAT">
//   Date Created : 2019-12-16
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    class ConnectionStreamWriter : AbstractEntityStreamingWriter
    {
        private readonly CommonStreamWriter commonWriter;
        
        /// <summary>
        /// The list of properties that shouldn't be written in json
        /// so security reasons. E.g. not password
        /// This affects what MAWEB gets and the export functionality
        /// Maybe the list could be configurable
        /// </summary>
        private static readonly HashSet<string> _blacklist = new HashSet<string>(StringComparer.OrdinalIgnoreCase) { "password", "pwd" };

        public ConnectionStreamWriter(CommonStreamWriter commonWriter)
        {
            if (commonWriter == null)
            {
                throw new ArgumentNullException(nameof(commonWriter));
            }

            this.commonWriter = commonWriter;
        }

        private void WriteConnectionProperties(IConnectionEntity entity )
        {
            if (entity.Settings == null || entity.Settings.Count == 0)
            {
                return;
            }
            using (commonWriter.WriteStartObject("properties"))
            {
                foreach (var property in entity.Settings.Where(s => !_blacklist.Contains(s.Key)))
                {
                    using (commonWriter.WriteStartObject(property.Key))
                    {
                        commonWriter.WriteProperty("type", property.Value.DataType.ToString());
                        commonWriter.WritePropertyObject("value", property.Value.Value);
                        commonWriter.Write("allowedValues", property.Value.AllowedValues);
                        commonWriter.WritePropertyObject("defaultValue", property.Value.DefaultValue);
                        commonWriter.WriteProperty("required", property.Value.Required);
                        commonWriter.WriteProperty("advanced", property.Value.Advanced);
                        commonWriter.Write("allowedSubTypes", property.Value.AllowedSubTypes);
                    }
                }
            }
        }

        private void WriteSubTypes(IConnectionEntity entity)
        {
            if (entity.SubTypeList == null)
            {
                return;
            }
            commonWriter.Write("subTypeList", entity.SubTypeList);
        }

        public override void Write(IConnectionEntity entity)
        {
            if (entity != null)
            {
                using (commonWriter.WriteStartObject())
                {
                    if (entity is MappingStoreConnectionEntity mappingStore)
                    {
                        commonWriter.WriteProperty("storeType", mappingStore.StoreType);
                        commonWriter.WriteProperty("dbName", entity.DbName);
                        commonWriter.WriteProperty("status", mappingStore.Status.ToString().ToLowerInvariant());
                        commonWriter.WriteProperty("version", mappingStore.Version);
                        commonWriter.WriteProperty("availableUpgrade", mappingStore.AvailableUpgrade);
                    }
                    else
                    {
                        commonWriter.WriteEntityId(entity);
                        commonWriter.WritePermissions(entity);
                        commonWriter.WriteProperty("db_name", entity.DbName);
                    }
                    commonWriter.WriteSimpleNameable(entity);
                    commonWriter.WriteProperty("type", entity.DatabaseVendorType);
                    commonWriter.WriteProperty("subtype", entity.SubType);
                    WriteSubTypes(entity);
                    WriteConnectionProperties(entity);
                    commonWriter.WriteDetailsLinks(entity);
                }
            }
        }
    }
}
