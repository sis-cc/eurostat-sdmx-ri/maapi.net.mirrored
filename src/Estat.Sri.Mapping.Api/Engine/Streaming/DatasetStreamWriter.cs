// -----------------------------------------------------------------------
// <copyright file="DatasetStreamWriter.cs" company="EUROSTAT">
//   Date Created : 2019-12-11
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    internal class DatasetStreamWriter : AbstractEntityStreamingWriter
    {
        private const string YesAndTrascoded = "yes_and_transcoded";

        private readonly CommonStreamWriter writer;
        private readonly DatasetPropertyStreamWriter datasetPropertyStreamWriter;

        /// <summary>
        /// Inti
        /// </summary>
        /// <param name="writer">The JSON writer. It is assumed this is disposed outside this class</param>
        public DatasetStreamWriter(CommonStreamWriter writer)
        {
            this.writer = writer;

            this.datasetPropertyStreamWriter = new DatasetPropertyStreamWriter(writer);
        }

        public static void WriteDataSetColumn(CommonStreamWriter writer, DataSetColumnEntity column)
        {
            using (writer.WriteStartObject())
            {
                writer.WriteSimpleNameable(column);
                writer.WriteEntityId(column);
                writer.WriteDetailsLinks(column);
                writer.WriteParentId(column);
                writer.WritePropertyObject("last_Updated", column.LastRetrieval);
                writer.WritePropertyName("isMapped");
                switch (column.IsMapped)
                {
                    case ColumnMappingStatus.Yes:
                        writer.WriteValue("yes");
                        break;

                    case ColumnMappingStatus.YesAndTranscoded:
                        writer.WriteValue(YesAndTrascoded);
                        break;

                    default:
                        writer.WriteValue("no");
                        break;
                }
            }
        }

        public static void WriteLocalCode(CommonStreamWriter writer, LocalCodeEntity item)
        {
            using (writer.WriteStartObject())
            {
                // only print what is needed to reduce size
                if (!string.IsNullOrEmpty(item.EntityId))
                {
                    writer.WriteEntityId(item);
                }
                if (!string.IsNullOrEmpty(item.ParentId))
                {
                    writer.WriteParentId(item);
                }
                // Local code links/details not used by frontend and increase size
                //   writer.WriteDetailsLinks(item);
                writer.TryWrite("id", item.ObjectId);
                writer.WriteSimpleNameable(item);
            }
        }

        public override void Write(DatasetEntity entity)
        {
            if (entity != null)
            {
                using (writer.WriteStartObject())
                {
                    using (writer.WriteStartObject("connection"))
                    {
                        writer.WritePropertyName("entityId");
                        writer.WriteValue(entity.ParentId);
                    }
                    writer.WriteProperty("query", entity.Query);

                    writer.WritePropertyRaw("extraData", entity.JSONQuery);
                    writer.WriteProperty("editorType", entity.EditorType);
                    writer.WritePermissions(entity);

                    writer.WriteParentId(entity);
                    writer.WriteEntityId(entity);
                    writer.WriteSimpleNameable(entity);

                    datasetPropertyStreamWriter.Write(entity.DatasetPropertyEntity);

                    writer.WriteDetailsLinks(entity);
                }
            }
        }

        public override void Write(DataSetColumnEntity entity)
        {
            if (entity != null)
            {
                WriteDataSetColumn(this.writer, entity);
            }
        }

        public override void Write(LocalCodeEntity entity)
        {
            if (entity != null)
            {
                WriteLocalCode(writer, entity);
            }
        }
     
        public override void Write(ColumnDescriptionSourceEntity entity)
        {
            if (entity != null)
            {
                using (writer.WriteStartObject())
                {
                    writer.WriteEntityId(entity);
                    writer.WriteParentId(entity);
                    writer.WritePropertyName("table");
                    writer.WriteValue(entity.DescriptionTable);
                    writer.WritePropertyName("relationField");
                    writer.WriteValue(entity.RelatedField);
                    writer.WritePropertyName("descriptionField");
                    writer.WriteValue(entity.DescriptionField);
                    writer.WriteDetailsLinks(entity);
                }
            }
        }

        public override void Write(IEnumerable<DatasetEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }

        public override void Write(IEnumerable<DataSetColumnEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }

        public override void Write(IEnumerable<LocalCodeEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }

        public override void Write(IEnumerable<ColumnDescriptionSourceEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
    }
}