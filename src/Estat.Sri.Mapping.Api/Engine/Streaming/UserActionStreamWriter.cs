// -----------------------------------------------------------------------
// <copyright file="UserActionStreamWriter.cs" company="EUROSTAT">
//   Date Created : 2019-12-16
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Estat.Sri.Mapping.Api.Model;
using Org.Sdmxsource.Sdmx.Util.Date;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    class UserActionStreamWriter : AbstractEntityStreamingWriter
    {
        private readonly CommonStreamWriter commonWriter;

        public UserActionStreamWriter(CommonStreamWriter commonWriter)
        {
            if (commonWriter == null)
            {
                throw new ArgumentNullException(nameof(commonWriter));
            }

            this.commonWriter = commonWriter;
        }

        public override void Write(UserActionEntity entity)
        {
           if (entity == null)
            {
                return;
            }

           using(commonWriter.WriteStartObject())
            {
                commonWriter.WriteEntityId(entity);
                commonWriter.WriteDetailsLinks(entity);
                commonWriter.WriteProperty("type", entity.OperationType);
                commonWriter.WriteProperty("username", entity.UserName);
                commonWriter.WriteProperty("entity_type", entity.EntityType);
                commonWriter.WriteProperty("action_when", DateUtil.FormatDate(entity.ActionWhen));
                commonWriter.WriteProperty("entity_name", entity.EntityName);
            }
        }
    }
}
