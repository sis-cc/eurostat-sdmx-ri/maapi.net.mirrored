// -----------------------------------------------------------------------
// <copyright file="TranscodingStreamWriter.cs" company="EUROSTAT">
//   Date Created : 2019-12-16
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.SdmxParseBase.Engine;

namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    internal class TranscodingStreamWriter : AbstractEntityStreamingWriter
    {
        private readonly CommonStreamWriter commonWriter;
        private readonly AdvanceTimeTranscodingWriter advanceTimeTranscodingWriter;

        public TranscodingStreamWriter(CommonStreamWriter commonWriter)
        {
            if (commonWriter == null)
            {
                throw new ArgumentNullException(nameof(commonWriter));
            }

            this.commonWriter = commonWriter;
            this.advanceTimeTranscodingWriter = new AdvanceTimeTranscodingWriter();
        }

        public void WriteTranscodingRule(TranscodingRuleEntity entity)
        {
            using (commonWriter.WriteStartObject())
            {
                if (!string.IsNullOrEmpty(entity.EntityId))
                {
                    commonWriter.WriteEntityId(entity);
                }
                if (!string.IsNullOrEmpty(entity.ParentId))
                {
                    commonWriter.WriteParentId(entity);
                }
                commonWriter.WriteIdentifiable("code", entity.DsdCodeEntity);
                if (entity.DsdCodeEntities.Any())
                {
                    using (commonWriter.WriteStartArray("codes"))
                    {
                        foreach (var code in entity.DsdCodeEntities)
                        {
                            WriteCode(code);
                        }
                    }
                }
                commonWriter.TryWrite("uncodedValue", entity.UncodedValue);
                if (entity.LocalCodes != null)
                {
                    using (commonWriter.WriteStartArray("localCodes"))
                    {
                        foreach (var item in entity.LocalCodes)
                        {
                            DatasetStreamWriter.WriteLocalCode(commonWriter, item);
                        }
                    }
                }
            }
        }

        private void WriteCode(IIdentifiableEntity codeEntity)
        {
            using (commonWriter.WriteStartObject())
            {
                commonWriter.WritePropertyName("id");
                commonWriter.WriteValue(codeEntity.ObjectId);
            }
        }

        [Obsolete]
        public override void Write(TranscodingEntity entity)
        {
            if (entity != null)
            {
                WriteTranscoding(entity);
            }
        }

        public override void Write(TranscodingRuleEntity entity)
        {
            if (entity != null)
            {
                WriteTranscodingRule(entity);
            }
        }

        private void WriteTimeTranscoding(TimeTranscoding year)
        {
            
            if (year.Column != null)
            {
                commonWriter.WritePropertyName("column");
                DatasetStreamWriter.WriteDataSetColumn(commonWriter, year.Column);
            }
            commonWriter.TryWriteOther("start", year.Start);
            commonWriter.TryWriteOther("length", year.Length);
        }

        public void WriteTranscoding(TranscodingEntity entity)
        {
            using (commonWriter.WriteStartObject())
            {
                commonWriter.WriteEntityId(entity);
                commonWriter.WriteParentId(entity);

                commonWriter.WriteDetailsLinks(entity);

                if (entity.HasScript())
                {
                    using (commonWriter.WriteStartArray("script"))
                    {
                        for (int i = 0; i < entity.Script.Count; i++)
                        {
                            using (commonWriter.WriteStartObject())
                            {
                                var script = entity.Script[i];
                                commonWriter.WriteProperty("name", script.ScriptTile);
                                commonWriter.WriteProperty("content", script.ScriptContent);
                            }
                        }
                    }
                }
            }
        }

        public void WriteSimpleTimeTranscoding(IList<TimeTranscodingEntity> timeTranscodingEntities)
        {
            foreach (var timePerFreq in timeTranscodingEntities)
            {
                using (commonWriter.WriteStartObject(timePerFreq.Frequency))
                {
                    commonWriter.TryWriteOther("is_date_time", timePerFreq.IsDateTime);
                    if (timePerFreq.IsDateTime)
                    {
                        commonWriter.WritePropertyName("date_column");
                        DatasetStreamWriter.WriteDataSetColumn(commonWriter, timePerFreq.DateColumn);
                    }
                    else
                    {
                        TimeTranscoding year = timePerFreq.Year;
                        if (year != null)
                        {
                            using (commonWriter.WriteStartObject("year"))
                            {
                                WriteTimeTranscoding(year);
                            }
                        }
                        PeriodTimeTranscoding period = timePerFreq.Period;
                        if (period != null)
                        {
                            using (commonWriter.WriteStartObject("period"))
                            {
                                WriteTimeTranscoding(period);
                                if (period.Rules != null)
                                {
                                    using (commonWriter.WriteStartArray("rules"))
                                    {
                                        foreach (var rule in period.Rules)
                                        {
                                            WriteTranscodingRule(rule);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
