using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;

// FIXME move to engine namespace
// FIXME add license
namespace Estat.Sri.Mapping.Api.Engine.Streaming
{
    public class LocalDataWriter : ILocalDataWriter
    {
        private enum State
        {
            None, NotStarted, StartedMessage, WroteProperty, WroteARows, WroteErrorMessages, WroteDescriptions, Closed
        }
        private enum Condition
        {
            StartMessage, WriteProperty, WriteRow, WriteErrorMessage, WriteDescription, Close
        }

        private class Transition
        {
            public Transition(State fromState, State targetState, Action transitionAction, params Condition[] conditions)
            {
                if (conditions == null)
                {
                    throw new ArgumentNullException(nameof(conditions));
                }

                FromState = fromState;
                TargetState = targetState;
                TransitionAction = transitionAction;
                Conditions = new HashSet<Condition>(conditions);
            }

            public State FromState { get; }
            public ISet<Condition> Conditions { get; }
            public State TargetState { get; }
            public Action TransitionAction { get; }
        }

        private class StateMachine
        {
            private readonly ISet<State> _visited;
            private readonly IList<Transition> _transitions;
            public State Current { get; set; }
            public StateMachine(State start, IList<Transition> transitions)
            {
                _transitions = transitions;
                Current = start;
                _visited = new HashSet<State>();
            }

            public void Apply(ISet<Condition> conditions)
            {
                var last = Current;
                Current = GetNextState(conditions);
                if (last != Current && !_visited.Add(Current))
                {
                    throw new InvalidOperationException("The state " + Current + " already visited");
                }
            }

            public void Apply(params Condition[] conditions)
            {
                Apply(new HashSet<Condition>(conditions));

            }

            public State GetNextState(ISet<Condition> conditions)
            {
                foreach (Transition transition in _transitions)
                {
                    var currentMatches = transition.FromState.Equals(Current);
                    var conditionMatch = transition.Conditions.SetEquals(conditions);
                    if (currentMatches && conditionMatch)
                    {
                        transition.TransitionAction?.Invoke();
                        return transition.TargetState;
                    }
                }

                return State.None;
            }
        }

        private readonly TextWriter textWriter;
        private bool disposed = false;
        private readonly StateMachine _stateMachine;
        private readonly EntityStreamingWriter entityStreamingWriter;
        protected JsonWriter writer;

        public LocalDataWriter(Stream response)
        {
            textWriter = new StreamWriter(response, new UTF8Encoding(false));

            this.writer = new JsonTextWriter(textWriter) { Formatting = Formatting.Indented};

            Transition startMessageTransition = new Transition(State.NotStarted, State.StartedMessage, null, Condition.StartMessage);

            Transition writePropertyTransition = new Transition(State.StartedMessage, State.WroteProperty, null, Condition.WriteProperty);

            Transition writeRowTransition = new Transition(State.StartedMessage, State.WroteARows, StartRowsArray, Condition.WriteRow);
            Transition writeRowTransition2 = new Transition(State.WroteProperty, State.WroteARows, StartRowsArray, Condition.WriteRow);

            IList<Transition> transitions = new []{
                startMessageTransition, 
                writePropertyTransition, 
                writeRowTransition, 
                writeRowTransition2,
                new Transition(State.WroteARows, State.WroteARows, null, Condition.WriteRow),
                new Transition(State.WroteProperty, State.WroteProperty, null, Condition.WriteProperty),
                new Transition( State.WroteARows, State.WroteDescriptions, EndRowsArray, Condition.WriteDescription),
                new Transition( State.WroteARows, State.WroteErrorMessages, EndRowsArray, Condition.WriteErrorMessage),
                new Transition( State.WroteARows, State.Closed, EndRowsArray, Condition.Close),
                new Transition( State.WroteErrorMessages, State.Closed, null, Condition.Close),
                new Transition( State.WroteDescriptions, State.Closed, null, Condition.Close),
                new Transition( State.Closed, State.Closed, null, Condition.Close),
                new Transition( State.WroteErrorMessages, State.WroteDescriptions, null, Condition.WriteDescription),
                new Transition( State.WroteDescriptions, State.WroteErrorMessages, null, Condition.WriteErrorMessage),
             };

            entityStreamingWriter = new EntityStreamingWriter(this.writer);
            this._stateMachine = new StateMachine(State.NotStarted, transitions);
        }

        private void StartRowsArray()
        {
            writer.WritePropertyName("rows");
            writer.WriteStartArray();
        }
        private void EndRowsArray()
        {
            writer.WriteEndArray();
        }
        private void EndObject(JsonWriter writer)
        {
            writer.WriteEndObject();
        }

        private State CurrentState { get; set; }

        public void SetDescriptions(string columnName, Dictionary<string, string> valueDescriptions)
        {

        }

        private void TryTransition(Condition condition, State expected)
        {
            this._stateMachine.Apply(condition);
            if (this._stateMachine.Current != expected)
            {
                throw new InvalidOperationException("Invalid state");
            }

        }
        public void StartMessage(IList<string> columns, int start, int end)
        {
            TryTransition(Condition.StartMessage, State.StartedMessage);
            this.writer.WriteStartObject();
            WriteStartEnd(start, end);
            writer.WritePropertyName("columns");
            writer.WriteStartArray();
            foreach (var column in columns)
            {
                writer.WriteValue(column);
            }
            writer.WriteEndArray();
        }

        private void WriteStartEnd(int start, int end)
        {
            writer.WritePropertyName("start");
            writer.WriteValue(start);
            writer.WritePropertyName("end");
            writer.WriteValue(end);
        }

        public void WriteProperty(string name, object value)
        {
            TryTransition(Condition.WriteProperty, State.WroteProperty);

            writer.WritePropertyName(name);
            writer.WriteValue(value);
        }

        public void WriteRow(params string[] values)
        {
            TryTransition(Condition.WriteRow, State.WroteARows);
            writer.WriteStartArray();
            foreach (var value in values)
            {
                writer.WriteValue(value);
            }
            writer.WriteEndArray();
        }

        public void WriteRow(params object[] values)
        {

            TryTransition(Condition.WriteRow, State.WroteARows);
            writer.WriteStartArray();
            foreach (var value in values)
            {
                writer.WriteValue(value);
            }
            writer.WriteEndArray();
        }

        public void WriteRow(params LocalCodeEntity[] values)
        {

            TryTransition(Condition.WriteRow, State.WroteARows);
            writer.WriteStartArray();
            foreach (var value in values)
            {
                entityStreamingWriter.Write(value);
            }
            writer.WriteEndArray();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool managed)
        {
            if (this._stateMachine.Current == State.Closed)
            {
                return;
            }
            this._stateMachine.Apply(Condition.Close);
            
            if (managed)
            {
                if (writer != null)
                {
                    this.writer.WriteEndObject();
                    writer.Flush();
                    writer.Close();
                }
                if (textWriter != null && !writer.CloseOutput)
                {
                    textWriter.Flush();
                    textWriter.Close();
                }
            }
        }

        public void WriteErrors(IList<string> errorMessages)
        {
            TryTransition(Condition.WriteErrorMessage, State.WroteErrorMessages);
            writer.WritePropertyName("errors");
            writer.WriteStartArray();
            foreach(var error in errorMessages)
            {
                writer.WriteValue(error);
            }

            writer.WriteEndArray();

        }

        public void StartMessage(IList<DataSetColumnEntity> columns, int start, int end)
        {
            TryTransition(Condition.StartMessage, State.StartedMessage);
            this.writer.WriteStartObject();
            WriteStartEnd(start, end);
            writer.WritePropertyName("columns");
            entityStreamingWriter.StartSection(Constant.EntityType.DataSetColumn);
            writer.WriteStartArray();
            foreach (var column in columns)
            {
                entityStreamingWriter.Write(column);
            }
            writer.WriteEndArray();
        }

        public void WriteStatus(StatusType status,string message)
        {
            writer.WriteStartObject();
            writer.WritePropertyName("Status");
            writer.WriteValue(status.ToString());
            writer.WritePropertyName("Message");
            writer.WriteValue(message);
            writer.WriteEndObject();
        }
    }
}
