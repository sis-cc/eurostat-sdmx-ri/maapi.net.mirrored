using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class DdbConnectionEntityReader : NameableEntityReader<ConnectionEntity>
    {
        private readonly PermissionEntityReader permissionEntityReader = new PermissionEntityReader();
        private readonly string _suffix;
        public DdbConnectionEntityReader()
        {
            _propertyNames.Add("type", (entity, value) => entity.DatabaseVendorType = value);
            _propertyNames.Add("subtype", (entity, value) => entity.SubType = value);
            _propertyNames.Add("db_name", (entity, value) => entity.DbName = value);
            _specificProperties.Add("properties");
            _specificProperties.Add("permissions");
            _suffix = ".";
        }

        public IConnectionEntity ParseEntity(JsonReader reader)
        {
            var entity = new ConnectionEntity
            {
                Permissions = new Dictionary<string, string>()
            };
            ExtractValueFor(reader, entity);
            return entity;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, ConnectionEntity entity)
        {
            var connectionEntity = entity as ConnectionEntity;
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".properties"))
            {
                ExtractSettings(reader, connectionEntity);
            }

            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(_suffix + "permissions"))
            {
                permissionEntityReader.ParsePermissions(reader, entity);
            }
        }

        private void ExtractSettings(JsonReader reader, ConnectionEntity connectionEntity)
        {
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var settingName = reader.Value?.ToString();
                    var value = ExtractSetting(reader);
                    connectionEntity.Settings.Add(settingName, value);
                }
            }
        }

        private IConnectionParameterEntity ExtractSetting(JsonReader reader)
        {
            var entity = new ConnectionParameterEntity();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if(reader.TokenType == JsonToken.PropertyName && reader.Value.ToString().ToLower() == "type")
                {
                    entity.DataType = (ParameterType)Enum.Parse(typeof(ParameterType), reader.ReadAsString(), true);
                }
                if (reader.TokenType == JsonToken.PropertyName && reader.Value.ToString().ToLower() == "value")
                {
                    reader.Read();
                    entity.Value = reader.Value;
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Value.ToString().ToLower() == "allowedvalues")
                {
                    entity.AllowedValues.AddAll(ReadAllowedValues(reader));
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Value.ToString().ToLower() == "defaultvalue")
                {
                    reader.Read();
                    entity.DefaultValue = reader.Value;
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Value.ToString().ToLower() == "required")
                {
                    entity.Required = reader.ReadAsBoolean().Value;
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Value.ToString().ToLower() == "advanced")
                {
                    entity.Advanced = reader.ReadAsBoolean().Value;
                }

                if (reader.TokenType == JsonToken.PropertyName && reader.Value.ToString().ToLower() == "allowedsubtypes")
                {
                    entity.AllowedSubTypes.AddAll(ReadAllowedValues(reader));
                }
            }

            return entity;
        }

        private T ReadProperty<T>(JsonReader reader, string nodeName)
        {
                if (reader.TokenType == JsonToken.PropertyName && reader.Value.ToString().ToLower() == nodeName)
                {
                    reader.Read();
                    return (T)Convert.ChangeType(reader.Value, typeof(T));
                }
            return default;
        }

        private List<string> ReadAllowedValues(JsonReader reader)
        {
            List<string> allowedValues = new List<string>();
            
                while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                {
                    if (reader.TokenType == JsonToken.String)
                    {
                        allowedValues.Add(reader.Value?.ToString());
                    }
                };

            return allowedValues;
        }
    }

}
