using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class MultiQueryDatasetEditorEngine : IDataSetEditorEngine
    {
        public const string EditorType = "MultiQuery";
        public const string IncludeHistoryQuery = "IncludeHistory";

        private static HashSet<string> NonObservationActions = new HashSet<string>()
        {
            IncludeHistoryQuery, 
            ObservationActionEnumType.Active.ToString()
        };

        #region If ever needed to be supported in Mapping assistant
        
        public IList<string> GetSupportedDdbPlugins()
        {
            throw new NotImplementedException();
        }

        public string GenerateSqlQuery(DatasetEntity datasetEntity)
        {
            throw new NotImplementedException();
        }

        public object DeserializeExtraData(string extraData)
        {
            return null;
        }

        public string Migrate(DatasetEntity datasetEntity)
        {
            throw new NotImplementedException();
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="datasetEntity"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static string GenerateSqlQuery(DatasetEntity datasetEntity, string action)
        {
            var list = !string.IsNullOrEmpty(datasetEntity.JSONQuery)
                ? Deserialize(datasetEntity.JSONQuery)
                : throw new InvalidOperationException($"Dataset has missing {nameof(datasetEntity.JSONQuery)} object");

            return list.FirstOrDefault(x => x.Actions.Contains(action, StringComparer.InvariantCultureIgnoreCase))?.Query
                   ?? throw new InvalidOperationException($"No query for action [{action}] defined");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static List<DisseminationMultiQuery> Deserialize(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<List<DisseminationMultiQuery>>(json);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string Serialize(List<DisseminationMultiQuery> list)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(list);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="datasetEntity"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public static IEnumerable<ObservationAction> GetActions(DatasetEntity datasetEntity)
        {
            var list = !string.IsNullOrEmpty(datasetEntity.JSONQuery)
                ? Deserialize(datasetEntity.JSONQuery)
                : throw new InvalidOperationException($"Dataset has missing {nameof(datasetEntity.JSONQuery)} object");

            return list.SelectMany(x => x.Actions)
                .Except(NonObservationActions, StringComparer.InvariantCultureIgnoreCase)
                .Select(ObservationAction.ParseString);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DisseminationMultiQuery
    {
        public string[] Actions { get; set; }
        public string Query { get; set; }
    }
}
