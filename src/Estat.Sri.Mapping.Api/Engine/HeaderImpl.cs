using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Estat.Sri.Mapping.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Util.Random;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class HeaderImpl : IHeader
    {
        /// <summary>
        ///     The _additional attribtues.
        /// </summary>
        private readonly IDictionary<string, string> _additionalAttributes;

        /// <summary>
        ///     The _embargo date.
        /// </summary>
        private readonly DateTime? _embargoDate;

        /// <summary>
        ///     The _extracted.
        /// </summary>
        private readonly DateTime? _extracted;

        /// <summary>
        ///     The _name.
        /// </summary>
        private readonly IList<ITextTypeWrapper> _name;

        /// <summary>
        ///     The _receiver.
        /// </summary>
        private readonly IList<IParty> _receiver;

        /// <summary>
        ///     The _source.
        /// </summary>
        private readonly IList<ITextTypeWrapper> _source;

        /// <summary>
        ///     The _structure references.
        /// </summary>
        private readonly IList<IDatasetStructureReference> _structureReferences;

        /// <summary>
        ///     The _data provider reference.
        /// </summary>
        private IStructureReference _dataProviderReference;

        /// <summary>
        ///     The _dataset action.
        /// </summary>
        private DatasetAction _datasetAction;

        /// <summary>
        ///     The _dataset id.
        /// </summary>
        private string _datasetId;

        /// <summary>
        ///     The _id.
        /// </summary>
        private string _id;

        /// <summary>
        ///     The _prepared.
        /// </summary>
        private DateTime? _prepared;

        /// <summary>
        ///     The _reporting begin.
        /// </summary>
        private DateTime? _reportingBegin;

        /// <summary>
        ///     The _reporting end.
        /// </summary>
        private DateTime? _reportingEnd;

        /// <summary>
        ///     The _sender.
        /// </summary>
        private IParty _sender;

        /// <summary>
        ///     The _test.
        /// </summary>
        private bool _test;

        /// <summary>
        ///     Initializes a new instance of the <see cref="HeaderImpl" /> class.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="senderId">
        ///     The sender id.
        public HeaderImpl(
            IDictionary<string, string> additionalAttributes,
            IList<IDatasetStructureReference> structures,
            IStructureReference dataProviderReference,
            DatasetAction datasetAction,
            string id,
            string datasetId,
            DateTime? embargoDate,
            DateTime? extracted,
            DateTime? prepared,
            DateTime? reportingBegin,
            DateTime? reportingEnd,
            IList<ITextTypeWrapper> name,
            IList<ITextTypeWrapper> source,
            IList<IParty> receiver,
            IParty sender,
            bool test)
        {
            this._additionalAttributes = new Dictionary<string, string>();
            this._name = new List<ITextTypeWrapper>();
            this._source = new List<ITextTypeWrapper>();
            this._receiver = new List<IParty>();
            this._structureReferences = new List<IDatasetStructureReference>();
            if (additionalAttributes != null)
            {
                this._additionalAttributes = new Dictionary<string, string>(additionalAttributes);
            }

            if (structures != null)
            {
                this._structureReferences = new List<IDatasetStructureReference>(structures);
            }

            this._dataProviderReference = dataProviderReference;
            this._datasetAction = datasetAction;
            this._id = id;
            this._datasetId = datasetId;
            if (embargoDate != null)
            {
                this._embargoDate = embargoDate.Value;
            }

            if (extracted != null)
            {
                this._extracted = extracted.Value;
            }

            if (prepared != null)
            {
                this._prepared = prepared.Value;
            }

            if (reportingBegin != null)
            {
                this._reportingBegin = reportingBegin.Value;
            }

            if (reportingEnd != null)
            {
                this._reportingEnd = reportingEnd.Value;
            }

            if (name != null)
            {
                this._name = new List<ITextTypeWrapper>(name);
            }

            if (source != null)
            {
                this._source = new List<ITextTypeWrapper>(source);
            }

            if (receiver != null)
            {
                this._receiver = new List<IParty>(receiver);
            }

            this._sender = sender;
            this._test = test;
            this.Validate();
        }

        public HeaderImpl(string id, string senderId)
        {
            this._additionalAttributes = new Dictionary<string, string>();
            this._name = new List<ITextTypeWrapper>();
            this._source = new List<ITextTypeWrapper>();
            this._receiver = new List<IParty>();
            this._structureReferences = new List<IDatasetStructureReference>();

            if (id == null)
            {
                throw new ArgumentException("The ID may not be null");
            }

            if (char.IsDigit(id, 0))
            {
                throw new ArgumentException("An ID may not start with a digit!");
            }

            this._id = id;
            this._sender = new PartyCore(null, senderId, null, null);
            this._prepared = DateTime.Now;
        }

        public DatasetAction Action { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public IDictionary<string, string> AdditionalAttribtues => _additionalAttributes;

        public IStructureReference DataProviderReference { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string DatasetId { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public DateTime? EmbargoDate => throw new NotImplementedException();

        public DateTime? Extracted => throw new NotImplementedException();

        public string Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public IList<ITextTypeWrapper> Name => _name;

        public DateTime? Prepared => throw new NotImplementedException();

        /// <summary>
        ///     Gets the receiver.
        /// </summary>
        public virtual IList<IParty> Receiver => new List<IParty>(this._receiver);

        public DateTime? ReportingBegin { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public DateTime? ReportingEnd { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        /// <summary>
        ///     Gets or sets the sender.
        /// </summary>
        public virtual IParty Sender
        {
            get => this._sender;

            set
            {
                this._sender = value;
                this.Validate();
            }
        }
        public IList<ITextTypeWrapper> Source => _source;

        public IList<IDatasetStructureReference> Structures => throw new NotImplementedException();

        public bool Test
        {
            get { return _test; }
            set { _test = value; }
        }

        public void AddName(ITextTypeWrapper name)
        {
            this._name.Add(name);
        }

        public void AddReciever(IParty recevier)
        {
            this._receiver.Add(recevier);
        }

        public void AddSource(ITextTypeWrapper source)
        {
            this._source.Add(source);
        }

        public void AddStructure(IDatasetStructureReference datasetStructureReference)
        {
            this._structureReferences.Add(datasetStructureReference);
        }

        public string GetAdditionalAttribtue(string headerField)
        {
            return _additionalAttributes.FirstOrDefault(x => x.Key == headerField).Value;
        }

        public IDatasetStructureReference GetStructureById(string structureId)
        {
            throw new NotImplementedException();
        }

        public bool HasAdditionalAttribute(string headerField)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (string.IsNullOrWhiteSpace(this._id))
            {
                this._id = RandomUtil.GenerateRandomString();
            }

            if (this._prepared == null)
            {
                this._prepared = DateTime.Now;
            }

            if (this._sender == null)
            {
                if (string.IsNullOrWhiteSpace(this._id))
                {
                    throw new SdmxSemmanticException("Header missing sender");
                }
            }
        }
    }
}
