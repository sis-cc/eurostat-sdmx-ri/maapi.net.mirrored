﻿// -----------------------------------------------------------------------
// <copyright file="IDatabaseProviderEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Engine
{
    using Estat.Sri.Mapping.Api.Builder;

    /// <summary>
    /// The Database Provider specific Engine interface.
    /// This interface contains methods to get DB vendor specific information.
    /// </summary>
    public interface IDatabaseProviderEngine
    {
        /// <summary>
        ///     Gets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        string Name { get; }

        /// <summary>
        /// Gets the name of the provider.
        /// </summary>
        /// <value>
        /// The name of the provider.
        /// </value>
        string ProviderName { get; }

        /// <summary>
        ///     Gets a value indicating whether the parameters are read only.
        /// </summary>
        /// <value>
        ///     <c>true</c> if parameters are read only; otherwise, <c>false</c>.
        /// </value>
        bool ParametersAreReadOnly { get; }

        /// <summary>
        ///     Gets a value indicating whether to show the description association.
        /// </summary>
        /// <value>
        ///     <c>true</c> if it should show the description association; otherwise, <c>false</c>.
        /// </value>
        bool ShowDescriptionAssociation { get; }

        /// <summary>
        /// Gets the browser.
        /// </summary>
        /// <value>
        /// The browser.
        /// </value>
        IDatabaseSchemaBrowser Browser { get; }

        /// <summary>
        /// Gets the settings builder.
        /// </summary>
        /// <value>
        /// The settings builder.
        /// </value>
        IConnectionSettingsBuilder SettingsBuilder { get; }
    }
}