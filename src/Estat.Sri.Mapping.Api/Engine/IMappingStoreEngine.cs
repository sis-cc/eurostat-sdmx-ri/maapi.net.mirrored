// -----------------------------------------------------------------------
// <copyright file="IMappingStoreEngine.cs" company="EUROSTAT">
//   Date Created : 2017-02-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Engine
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The Mapping Store database
    /// </summary>
    public interface IMappingStoreEngine
    {
        /// <summary>
        /// Gets the available store identifier.
        /// </summary>
        /// <value>The available store identifier.</value>
        IEnumerable<string> AvailableStoreId { get; }

        /// <summary>
        /// Gets the type of the store.
        /// </summary>
        /// <value>The type of the store.</value>
        string StoreType { get; }

        /// <summary>
        /// Retrieves the version.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <returns>The <see cref="System.Version"/> of the store.</returns>
        Version RetrieveVersion(DatabaseIdentificationOptions databaseIdentificationOptions);

        /// <summary>
        /// Gets the available versions that the store with id <paramref name="storeId"/> can be upgraded to. 
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <returns>The list possible <see cref="System.Version"/> that the store can be upgrade.</returns>
        IEnumerable<Version> GetAvailableVersions(DatabaseIdentificationOptions databaseIdentificationOptions);

        /// <summary>
        /// Tests the connection.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        IActionResult TestConnection(DatabaseIdentificationOptions databaseIdentificationOptions);

        /// <summary>
        /// Initializes the specified store identifier.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        IActionResult Initialize(DatabaseIdentificationOptions databaseIdentificationOptions);

        /// <summary>
        /// Verifies the integrity.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="action">The action.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        IActionResult VerifyIntegrity(DatabaseIdentificationOptions storeId, VerifyStoreAction action);

        /// <summary>
        /// Upgrades the specified store identifier.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <param name="targetVersion">The target version.</param>
        /// <param name="shoudInitialize"></param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        IActionResult Upgrade(DatabaseIdentificationOptions databaseIdentificationOptions, Version targetVersion, bool shoudInitialize);

        /* TODO Move the following to a repository but it is not a true repository */

        /// <summary>
        /// Retrieves the specified store identifier.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        IConnectionEntity Retrieve(DatabaseIdentificationOptions databaseIdentificationOptions);

        /// <summary>
        /// Saves the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        IActionResult Save(IConnectionEntity entity);

        /// <summary>
        /// Deletes the specified store identifier.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        IActionResult Delete(DatabaseIdentificationOptions databaseIdentificationOptions);

        /// <summary>
        /// Deletes the specified store identifier.
        /// </summary>
        /// <param name="entity">The store identifier.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        IActionResult TestMappingStoreConnection(IConnectionEntity entity);

        /// <summary>
        /// Gets the stores details.
        /// </summary>
        IEnumerable<StoreDetails> GetStoresDetails();
    }
}