// -----------------------------------------------------------------------
// <copyright file="ValidityBaseReader.cs" company="EUROSTAT">
//   Date Created : 2022-9-14
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
namespace Estat.Sri.Mapping.Api.Engine
{
    public abstract class ValidityBaseReader<T> : EntityReader<T> where T : ValidityBaseEntity
    {

        private readonly DataSetColumnEntityReader _datasetColumnEntityReader = new DataSetColumnEntityReader();
        protected ValidityBaseReader()
        {
            _propertyNames.Add("constantValue", (entity, value) => entity.ConstantValue = value);
            _specificProperties.Add("dataset_column");
        }


        /// <summary>
        /// Create a entity, it usually just returns the object created with <c>new</c> e.g. <c>new LastUpdateEntity()</c>
        /// </summary>
        /// <returns>The new object</returns>
        protected abstract T CreateEntity();

        public T ParseEntity(JsonReader reader)
        {
            var entity = CreateEntity();
            ExtractValueFor(reader,entity);
            return entity;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, T entity)
        {
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".dataset_column"))
            {
                ExtractDatasetColumns(reader, entity);
            }
        }

        private DataSetColumnEntity ExtractDatasetColumn(JsonReader reader)
        {
            return _datasetColumnEntityReader.ParseEntity(reader);
        }

        private void ExtractDatasetColumns(JsonReader reader, T entity)
        {
            while (reader.Read() && reader.TokenType != JsonToken.EndArray)
            {
                if (reader.TokenType == JsonToken.StartObject)
                {
                    var column = ExtractDatasetColumn(reader);
                    entity.Column = column;
                }
            }
        }

    }
}
