// -----------------------------------------------------------------------
// <copyright file="UpdateStatusMappingReader.cs" company="EUROSTAT">
//   Date Created : 2022-9-14
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
namespace Estat.Sri.Mapping.Api.Engine
{
    public class UpdateStatusMappingReader : EntityReader<UpdateStatusEntity> 
    {

        private readonly DataSetColumnEntityReader _datasetColumnEntityReader = new DataSetColumnEntityReader();
        public UpdateStatusMappingReader()
        {
            _propertyNames.Add("constantValue", (entity, value) => entity.ConstantValue = value);
            _specificProperties.Add("dataset_column");
            _specificProperties.Add("rule");
        }


      
        public UpdateStatusEntity ParseEntity(JsonReader reader)
        {
            var entity = new UpdateStatusEntity();
            ExtractValueFor(reader,entity);
            return entity;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, UpdateStatusEntity entity)
        {
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".dataset_column"))
            {
                ExtractDatasetColumns(reader, entity);
            }
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(".rule"))
            {
                ExtractRules(reader, entity);
            }

        }

        private DataSetColumnEntity ExtractDatasetColumn(JsonReader reader)
        {
            return _datasetColumnEntityReader.ParseEntity(reader);
        }

        private void ExtractDatasetColumns(JsonReader reader, UpdateStatusEntity entity)
        {
            while (reader.Read() && reader.TokenType != JsonToken.EndArray)
            {
                if (reader.TokenType == JsonToken.StartObject)
                {
                    var column = ExtractDatasetColumn(reader);
                    entity.Column = column;
                }
            }
        }
        private void ExtractRules(JsonReader reader, UpdateStatusEntity entity)
        {
            string key = null;
            string value = null;
            while (reader.Read() && reader.TokenType != JsonToken.EndArray)
            {
                switch(reader.TokenType)
                {
                    case JsonToken.PropertyName:
                        if (reader.Path.EndsWith(".sdmxCode"))
                        {
                            reader.Read();
                            key = reader.Value?.ToString();
                        }
                        else if (reader.Path.EndsWith(".localCode"))
                        {
                            reader.Read();
                            value = reader.Value?.ToString();
                        }
                        break;
                    case JsonToken.EndObject:
                        if (key != null)
                        {
                            entity.Add(key, value);
                            key = null;
                            value = null;
                        }
                        break;
                }
            }

        }
    }
}
