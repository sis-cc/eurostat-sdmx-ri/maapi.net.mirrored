﻿// -----------------------------------------------------------------------
// <copyright file="IDataflowUpgrade.cs" company="EUROSTAT">
//   Date Created : 2017-07-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Engine
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The Dataflow Upgrade engine interface.
    /// </summary>
    public interface IDataflowUpgrade
    {
        /// <summary>
        /// Upgrades, in other words copy the mapping set, of the Dataflow identified by <paramref name="sourceDataflowUrn"/>  to dataflow <paramref name="targetDataflowUrn"/>
        /// </summary>
        /// <param name="sourceDataflowUrn">
        ///     The source dataflow urn.
        /// </param>
        /// <param name="targetDataflowUrn">
        ///     The target dataflow urn.
        /// </param>
        /// <param name="structureSets">
        /// The structure sets. If it is null, the tool will try to auto-generate the structure set
        /// </param>
        /// <returns>
        /// The <see cref="MappingSetEntity"/>.
        /// </returns>
        MappingSetEntity Upgrade(Uri sourceDataflowUrn, Uri targetDataflowUrn, IEnumerable<FileInfo> structureSets);
    }
}