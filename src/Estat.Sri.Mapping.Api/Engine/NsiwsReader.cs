using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class NsiwsEntityReader : NameableEntityReader<NsiwsEntity>
    {
        public NsiwsEntityReader()
        {
            _propertyNames.Add("technology", (entity, value) => entity.Technology = value);
            _propertyNames.Add("url", (entity, value) => entity.Url = value);
            _propertyNames.Add("username", (entity, value) => entity.UserName = value);
            _propertyNames.Add("password", (entity, value) => entity.Password = value);
            _propertyNames.Add("proxy", (entity, value) => entity.Proxy = bool.TryParse(value, out bool proxy) && proxy);
            _specificProperties.Add("details");
        }

        public NsiwsEntity ParseEntity(JsonReader reader)
        {
            var entity = new NsiwsEntity();
            ExtractValueFor(reader, entity);
            return entity;
        }
    }
}
