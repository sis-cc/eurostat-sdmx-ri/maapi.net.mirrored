using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Sri.Mapping.Api.Engine
{
    [Serializable]
    public class PartyCore: IParty
    {

        public virtual IList<IContact> Contacts
        {
            get
            {
                return new List<IContact>(this._contacts);
            }
        }
        /// <summary>
        ///     Gets the id.
        /// </summary>
        public virtual string Id
        {
            get
            {
                return this._id;
            }
        }

        /// <summary>
        ///     Gets the name.
        /// </summary>
        public virtual IList<ITextTypeWrapper> Name
        {
            get
            {
                return new List<ITextTypeWrapper>(this._name);
            }
        }

        /// <summary>
        ///     Gets the time zone.
        /// </summary>
        public virtual string TimeZone
        {
            get
            {
                return this._timeZone;
            }
        }
        /// <summary>
        ///     The _time zone regex
        /// </summary>
        private static readonly Regex _timeZoneRegex = new Regex(
            "^(\\+|\\-)(14:00|((0[0-9]|1[0-3]):[0-5][0-9]))$",
            RegexOptions.Compiled);

        /// <summary>
        ///     The _name.
        /// </summary>
        private readonly IList<ITextTypeWrapper> _name;

        /// <summary>
        ///     The contacts.
        /// </summary>
        private readonly IList<IContact> _contacts;

        /// <summary>
        ///     The id.
        /// </summary>
        private readonly string _id;

        /// <summary>
        ///     The time zone.
        /// </summary>
        private readonly string _timeZone;

        public PartyCore(IList<ITextTypeWrapper> name, string id, IList<IContact> contacts, string timeZone)
        {
            if (name != null)
            {
                this._name = new List<ITextTypeWrapper>(name);
            }

            this._id = id;
            if (contacts != null)
            {
                this._contacts = new List<IContact>(contacts);
            }

            this._timeZone = timeZone;
            this.Validate();
        }

        public PartyCore()
        {
            this._name = new List<ITextTypeWrapper>();
            this._contacts = new List<IContact>();
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(this._id))
            {
                throw new SdmxSemmanticException("Party missing mandatory id");
            }

            if (this._timeZone != null)
            {
                // Pattern idPattern = ILOG.J2CsMapping.Text.Pattern.Compile("(\\+|\\-)(14:00|((0[0-9]|1[0-3]):[0-5][0-9]))");
                if (!_timeZoneRegex.IsMatch(this._timeZone))
                {
                    throw new SdmxSemmanticException(
                        "Time zone '" + this._timeZone
                                      + "' is in an invalid format. please ensure the format matches the patttern (\\+|\\-)(14:00|((0[0-9]|1[0-3]):[0-5][0-9]) example +12:30");
                }
            }
        }

    }
}
