// -----------------------------------------------------------------------
// <copyright file="IConstraintsFromTranscodingsEngine.cs" company="EUROSTAT">
//   Date Created : 2019-03-04
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Sri.Mapping.Api.Engine
{
    /// <summary>
    ///   <para>Contraints from Transcodings</para>
    /// </summary>
    public interface IConstraintsFromTranscodingsEngine
    {
        /// <summary>Creates for.</summary>
        /// <param name="sid">The sid.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="actual"></param>
        /// <param name="componentId"></param>
        /// <param name="sdmxSchema"></param>
        bool CreateOrUpdateFor(string sid, string id, bool actual, string componentId = null,
            SdmxSchemaEnumType sdmxSchema = SdmxSchemaEnumType.VersionTwoPointOne);
    }
}