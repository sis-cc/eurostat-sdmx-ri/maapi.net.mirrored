using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class DatasetEntityReader : NameableEntityReader<DatasetEntity>
    {
        private readonly string _suffix;
        private readonly PermissionEntityReader permissionEntityReader = new PermissionEntityReader();
        private readonly DatasetPropertyEntityReader _datasetPropertyEntityReader = new DatasetPropertyEntityReader();

        public DatasetEntityReader()
        {
            _propertyNames.Add("query", (entity, value) => entity.Query = value);
            _propertyNames.Add("editorType", (entity, value) => entity.EditorType = value);
            _specificProperties.Add("extraData");
            _specificProperties.Add("permissions");
            _specificProperties.Add("datasetProperty");
            _suffix = ".";
        }

        public DatasetEntityReader(string suffix) : this()
        {
            _suffix = suffix;
        }

        public DatasetEntity ParseEntity(JsonReader reader)
        {
            var entity = new DatasetEntity();
            ExtractValueFor(reader, entity,_suffix);
            return entity;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, DatasetEntity entity)
        {
            var datasetEntity = entity as DatasetEntity;
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(_suffix + "extraData"))
            {
                reader.Read();
                if (reader.TokenType == JsonToken.StartObject)
                {
                    var jObject = JObject.Load(reader);
                    datasetEntity.JSONQuery = jObject.ToString();
                }
                else if (reader.TokenType == JsonToken.String)
                {
                    datasetEntity.JSONQuery = reader.Value?.ToString();
                }
            }

            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(_suffix + "permissions"))
            {
                permissionEntityReader.ParsePermissions(reader, entity);
            }


            if (reader.TokenType == JsonToken.PropertyName && reader.Path.Contains(_suffix + "datasetProperty"))
            {
                entity.DatasetPropertyEntity = _datasetPropertyEntityReader.ParseEntity(reader, entity.EntityId);
            }
        }
    }
}
