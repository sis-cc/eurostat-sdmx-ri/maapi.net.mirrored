// -----------------------------------------------------------------------
// <copyright file="IEntityStreamingWriter.cs" company="EUROSTAT">
//   Date Created : 2019-9-13
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Engine
{
    public interface IEntityStreamingWriter : IExceptionStreamingWriter, IDisposable
    {
        /// <summary>
        /// Start a message, the start object. It can be called only once per instance
        /// </summary>
        void StartMessage();

        /// <summary>
        /// Start an entity section. All entities written after this statement must be of type EntityType until a new StartSection is used
        /// This is to make clear we don't do any buffering and we mix different entity and expect it to work
        /// it should close the last called StartSection
        /// </summary>
        void StartSection(EntityType entityType);
        /// <summary>
        /// Write metadata that might be needed, eg. parentId and others.\
        /// </summary>
        /// <param name="name">The name of the property</param>
        /// <param name="value">The value</param>
        void WriteProperty(string name, string value);
        /// <summary>
        /// Write an entity, the entity type must conform to the last StartSection entity type
        /// </summary>
        void Write(IEntity entity);

        void Write(IConnectionEntity entity);
        void Write(DatasetEntity entity);
        void Write(DatasetPropertyEntity entity);
        void Write(DataSetColumnEntity entity);
        void Write(LocalCodeEntity entity);
        void Write(MappingSetEntity entity);
        void Write(ColumnDescriptionSourceEntity entity);
        void Write(ComponentMappingEntity entity);
        void Write(TranscodingEntity entity);
        void Write(TranscodingRuleEntity entity);
        void Write(HeaderEntity entity);
        void Write(RegistryEntity entity);
        void Write(UserEntity entity);
        void Write(TemplateMapping entity);
        void Write(UserActionEntity entity);
        void Write(DataSourceEntity entity);
        void Write(TimePreFormattedEntity entity);
        void Write(TimeDimensionMappingEntity entity);
        void Write(LastUpdatedEntity entity);
        void Write(ValidToMappingEntity entity);
        void Write(UpdateStatusEntity entity);
        void Write(NsiwsEntity entity);
        /// <summary>
        /// Write many entities
        /// </summary>
        void Write(IEnumerable<IEntity> entity);

        /// <summary>
        /// Write many entities
        /// </summary>
        void Write(IEnumerable<IConnectionEntity> entity);
        void Write(IEnumerable<DatasetEntity> entity);
        void Write(IEnumerable<DataSetColumnEntity> entity);
        void Write(IEnumerable<LocalCodeEntity> entity);
        void Write(IEnumerable<MappingSetEntity> entity);
        void Write(IEnumerable<ColumnDescriptionSourceEntity> entity);
        void Write(IEnumerable<ComponentMappingEntity> entity);
        void Write(IEnumerable<TranscodingEntity> entity);
        void Write(IEnumerable<TranscodingRuleEntity> entity);
        void Write(IEnumerable<HeaderEntity> entity);
        void Write(IEnumerable<RegistryEntity> entity);
        void Write(IEnumerable<UserEntity> entity);
        void Write(IEnumerable<TemplateMapping> entity);
        void Write(IEnumerable<UserActionEntity> entity);
        void Write(IEnumerable<NsiwsEntity> entity);
        void Write(IEnumerable<TimeDimensionMappingEntity> entity);
        void Write(IEnumerable<LastUpdatedEntity> entity);
        void Write(IEnumerable<ValidToMappingEntity> entity);
        void Write(IEnumerable<UpdateStatusEntity> entity);
        /// <summary>
        /// closes a message, closes sections calls Dispose
        /// </summary>
        void Close();

        int Status { get; set; }
    }
}
