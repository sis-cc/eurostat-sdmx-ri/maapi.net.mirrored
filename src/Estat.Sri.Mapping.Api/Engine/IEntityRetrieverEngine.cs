// -----------------------------------------------------------------------
// <copyright file="IEntityRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-02-02
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Engine
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The implementations of this interface are responsible for retrieving entities from a store
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public interface IEntityRetrieverEngine<out TEntity> where TEntity : IEntity
    {
        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        IEnumerable<TEntity> GetEntities(IEntityQuery query, Detail detail);

        /// <summary>
        /// Query the database and write the entities usign <paramref name="entityWriter"/>
        /// </summary>
        /// <param name="query">The query</param>
        /// <param name="detail">The output detail</param>
        /// <param name="entityWriter">The writer</param>
        /// 
        void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter);
    }
}