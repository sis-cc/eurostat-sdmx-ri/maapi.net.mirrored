// -----------------------------------------------------------------------
// <copyright file="ILocalDataWriter.cs" company="EUROSTAT">
//   Date Created : 2019-9-13
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Engine
{
    /// <summary>
    /// Local data writer interface. Local meaning data from the database without any mapping/transcoding
    /// </summary>
    public interface ILocalDataWriter : IExceptionStreamingWriter, IDisposable
    {
        /// <summary>
        /// Start a data message with the given <paramref name="columns"/>
        /// </summary>
        /// <param name="columns">The names of the columns</param>
        /// <param name="start">Paging start</param>
        /// <param name="end">Paging end</param>
        void StartMessage(IList<string> columns, int start, int end);

        /// <summary>
        /// Start a data message with the given <paramref name="columns"/>
        /// </summary>
        /// <param name="columns">The names of the columns</param>
        /// <param name="start">Paging start</param>
        /// <param name="end">Paging end</param>
        void StartMessage(IList<DataSetColumnEntity> columns, int start, int end);

        /// <summary>
        /// Write metadata that might be needed, eg. parentId and others.
        /// TODO check if this is needed
        /// </summary>
        /// <param name="name">The name of the property</param>
        /// <param name="value">The value</param>
        void WriteProperty(string name, object value);

        /// <summary>
        /// Set the descriptions of the local codes of the specified <paramref name="columnName"/>
        /// If this is already set, it should be replaced
        /// </summary>
        /// <param name="columnName">The column name</param>
        /// <param name="valueDescriptions">A map between the local code value and the descriptions</param>
        void SetDescriptions(string columnName, Dictionary<string, string> valueDescriptions);

        /// <summary>
        /// Write a data row
        /// </summary>
        /// <param name="values">The values</param>
        void WriteRow(params string[] values);

        /// <summary>
        /// Write a data row
        /// </summary>
        /// <param name="values">The values</param>
        void WriteRow(params LocalCodeEntity[] values);

        /// <summary>
        /// Write the error messages
        /// </summary>
        /// <param name="errorMessages">The error messages to write</param>
        void WriteErrors(IList<string> errorMessages);
    }
}
