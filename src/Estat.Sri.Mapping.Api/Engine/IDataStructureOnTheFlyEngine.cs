﻿using Estat.Sri.Mapping.Api.Model;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using System.Collections.Generic;

namespace Estat.Sri.Mapping.Api.Engine
{
    /// <summary>
    /// Interface for DataStructureOnTheFlyEngine
    /// </summary>
    public interface IDataStructureOnTheFlyEngine
    {
        /// <summary>
        /// Adds the specified sid.
        /// </summary>
        /// <param name="sid">The sid.</param>
        /// <param name="dSDOnTheFlyEntity">The d sd on the fly entity.</param>
        void Add(string sid,DSDOnTheFlyEntity dSDOnTheFlyEntity);
        /// <summary>
        /// Gets the by annotation.
        /// </summary>
        /// <param name="sid">The sid.</param>
        /// <param name="dsd">The DSD.</param>
        /// <returns></returns>
        DSDOnTheFlyEntity GetByAnnotation(string sid,StructureReferenceImpl dsd);

        /// <summary>
        /// Deletes the specified sid.
        /// </summary>
        /// <param name="sid">The sid.</param>
        /// <param name="dsd">The DSD.</param>
        void Delete(string sid, StructureReferenceImpl dsd);
    }
}
