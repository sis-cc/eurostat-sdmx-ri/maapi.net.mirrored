// -----------------------------------------------------------------------
// <copyright file="IEntityPersistanceStreamEngine.cs" company="EUROSTAT">
//   Date Created : 2019-11-12
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Engine
{
    /// <summary>
    /// Base persistance engine interface, contains only stream and non-generic methods
    /// </summary>
    public interface IEntityPersistanceStreamEngine
    {
        /// <summary>
        /// Deletes the specified entity identifier.
        /// </summary>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        void Delete(string entityId, EntityType entityType);

        /// <summary>
        /// Deletes the specified entity identifier.
        /// </summary>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="childrenEntityType">The entity type of children.</param>
        void DeleteChildren(string entityId, EntityType childrenEntityType);

        /// <summary>
        /// Add or update entities read from <paramref name="input"/> to Mapping Store 
        /// Optionally if provided write the output to <paramref name="responseWriter"/>
        /// </summary>
        /// <param name="input">The input to parse</param>
        /// <param name="responseWriter">An optional writer to write the entities that were added/updated</param>
        List<StatusType> Add(IEntityStreamingReader input, IEntityStreamingWriter responseWriter);

        /// <summary>
        /// Import entities read from <paramref name="input"/> to Mapping Store 
        /// Optionally if provided write the output to <paramref name="responseWriter"/>
        /// </summary>
        /// <param name="input">The input to parse</param>
        /// <param name="responseWriter">An optional writer to write the entities that were added/updated</param>
        /// <param name="mapping">The mapping between given entity/parent id and actual in MSDB</param>
        void Import(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping);
    }
}
