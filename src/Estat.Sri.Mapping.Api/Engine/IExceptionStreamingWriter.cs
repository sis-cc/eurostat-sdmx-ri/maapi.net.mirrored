// -----------------------------------------------------------------------
// <copyright file="IExceptionStreamingWriter.cs" company="EUROSTAT">
//   Date Created : --
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Engine
{
    /// <summary>
    /// Interface for writing an exception to a stream
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface IExceptionStreamingWriter 
    {
        /// <summary>
        /// Writes the exception.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="message">The exception.</param>
        void WriteStatus(StatusType status,string message);
    }
}