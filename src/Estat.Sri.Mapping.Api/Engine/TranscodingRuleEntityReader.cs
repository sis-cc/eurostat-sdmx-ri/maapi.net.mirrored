using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Mapping.Api.Engine
{
    public class TranscodingRuleEntityReader : EntityReader<TranscodingRuleEntity>
    {
        public TranscodingRuleEntityReader()
        {
            _propertyNames.Add("uncodedValue", (entity, value) => entity.UncodedValue = value);
            _specificProperties.Add("code");
            _specificProperties.Add("codes");
            _specificProperties.Add("localCodes");
        }

        public TranscodingRuleEntity ParseEntity(JsonReader reader)
        {
            var entity = new TranscodingRuleEntity();
            ExtractValueFor(reader,entity);
            return entity;
        }

        protected override void ExtractSpecificProperties(JsonReader reader, TranscodingRuleEntity entity)
        {
            if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith(".code"))
            {
                entity.DsdCodeEntity = ExtractCode(reader);
            }

            if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith(".codes"))
            {
                
                while (reader.Read() && (reader.TokenType != JsonToken.EndArray && reader.TokenType != JsonToken.Null))
                {
                    var code = ExtractCode(reader);
                    //if ObjectId is null we have an empty array
                    if (code.ObjectId != null)
                    {
                        entity.DsdCodeEntities.Add(code);
                    }
                    else
                    {
                        break;
                    }
                }
            }

            if (reader.TokenType == JsonToken.PropertyName && reader.Path.EndsWith(".localCodes"))
            {
                while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                {
                    var localCode = ExtractLocalCode(reader);
                    entity.LocalCodes.Add(localCode);
                }
            }
        }

        private LocalCodeEntity ExtractLocalCode(JsonReader reader)
        {
            var localCode = new LocalCodeEntity();  
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.String && reader.Path.Contains("id"))
                {
                    localCode.ObjectId = reader.Value?.ToString();
                }
                if (reader.TokenType == JsonToken.String && reader.Path.Contains("parentId"))
                {
                    localCode.ParentId = reader.Value?.ToString();
                }
            }

            return localCode;
        }

        private IIdentifiableEntity ExtractCode(JsonReader reader)
        {
            var code = new IdentifiableEntity();
            //check for EndArray because the codes can be empty
            //"code":{
            //"id":"00",
            //   "entityId":null
            //},
            //"codes":[
            //
            //
            //]
            while (reader.Read() && reader.TokenType != JsonToken.EndObject && reader.TokenType != JsonToken.EndArray)
            {
                // Must check the path first else any other property with null will break this
                // "code" could now be null with "codes" having the values
                if (reader.Path.EndsWith(".code") && reader.TokenType == JsonToken.Null)
                {
                    return null;
                }
 
                // we only care about .id
                if (code.ObjectId == null && reader.Path.EndsWith(".id"))
                {
                    if (reader.TokenType == JsonToken.PropertyName)
                    {
                        code.ObjectId = reader.ReadAsString();
                    }
                }
           }

            return code;
        }
    }
}
