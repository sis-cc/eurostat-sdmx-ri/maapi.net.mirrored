﻿// -----------------------------------------------------------------------
// <copyright file="DbReaderDatasetColumnBuilder.cs" company="EUROSTAT">
//   Date Created : 2014-10-23
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    ///     DataSet Column Name Exception
    /// </summary>
    public class DataSetColumnNameException : Exception
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DataSetColumnNameException" /> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public DataSetColumnNameException(string message)
            : base(message)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataSetColumnNameException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        public DataSetColumnNameException(string message, Exception ex)
            : base(message, ex)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataSetColumnNameException" /> class.
        /// </summary>
        public DataSetColumnNameException()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataSetColumnNameException" /> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization information.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected DataSetColumnNameException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }
    }
}