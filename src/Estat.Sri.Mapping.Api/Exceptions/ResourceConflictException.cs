﻿// -----------------------------------------------------------------------
// <copyright file="ResourceConflictException.cs" company="EUROSTAT">
//   Date Created : 2017-07-11
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The resource conflict exception.
    /// This exception should be used to report resource conflicts during Create, Delete, Update operations. 
    /// Such trying to delete an entity that it is used by other entities
    /// Or trying to add an entity that already exists
    /// </summary>
    public class ResourceConflictException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceConflictException"/> class.
        /// </summary>
        public ResourceConflictException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceConflictException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public ResourceConflictException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceConflictException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="innerException">
        /// The inner exception.
        /// </param>
        public ResourceConflictException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceConflictException"/> class.
        /// </summary>
        /// <param name="info">
        /// The info.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        protected ResourceConflictException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}