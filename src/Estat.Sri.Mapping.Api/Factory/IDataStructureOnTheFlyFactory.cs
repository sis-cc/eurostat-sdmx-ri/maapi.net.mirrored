﻿using Estat.Sri.Mapping.Api.Engine;

namespace Estat.Sri.Mapping.Api.Factory
{
    /// <summary>
    /// IDataStructureOnTheFlyFactory
    /// </summary>
    public interface IDataStructureOnTheFlyFactory
    {
        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="sid">The sid.</param>
        /// <returns></returns>
        IDataStructureOnTheFlyEngine GetEngine(string sid);
    }
}
