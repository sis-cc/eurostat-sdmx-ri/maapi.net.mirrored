using System;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Factory
{
    // TODO is this used ?
    public class EntityTypeFactory : IEntityTypeFactory
    {
        public EntityType ParseUrlPath(string path)
        {
            switch (path)
            {
                case "ddb":
                    return   EntityType.DdbConnectionSettings;
                case "dataset":
                    return EntityType.DataSet;
                case "column":
                    return EntityType.DataSetColumn;
                case "localCode":
                    return EntityType.LocalCode;
                case "mappingset":
                    return EntityType.MappingSet;
                case "column_description":
                    return EntityType.DescSource;
                case "mapping":
                    return EntityType.Mapping;
                case "transcoding":
                    return EntityType.Transcoding;
                case "transcodingRule":
                    return EntityType.TranscodingRule;
                case "header_template":
                    return EntityType.Header;
                case "registry":
                    return EntityType.Registry;
                case "user":
                    return EntityType.User;
                default:
                    throw new NotImplementedException($"string {path} could not be parsed");
            }
            
        }
    }
}