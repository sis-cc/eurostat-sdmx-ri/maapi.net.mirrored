using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Factory
{
    public interface IEntityTypeFactory
    {
        EntityType ParseUrlPath(string path);
    }
}