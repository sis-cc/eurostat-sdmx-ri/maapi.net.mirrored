// -----------------------------------------------------------------------
// <copyright file="ITimeDimensionMappingFactory.cs" company="EUROSTAT">
//   Date Created : 2017-09-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Factory
{
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;

    /// <summary>
    /// The TimeDimension Mapping Factory interface.
    /// </summary>
    public interface ITimeDimensionMappingFactory
    {
        /// <summary>
        /// Gets the builder for either no transcoding or simple transcoding.
        /// </summary>
        /// <param name="componentMapping">The component mapping.</param>
        /// <param name="timeTranscoding">The time transcoding.</param>
        /// <param name="databaseType">Type of the database.</param>
        /// <returns>
        /// The <see cref="ITimeDimensionMappingBuilder" />.
        /// </returns>
        ITimeDimensionMappingBuilder GetBuilder(TimeDimensionMappingEntity componentMapping, TimeTranscodingEntity timeTranscoding, string databaseType);

        /// <summary>
        /// Gets the builder for advanced mapping.
        /// </summary>
        /// <param name="criteriaColumn">The criteria column</param>
        /// <param name="timeTranscoding">The time transcoding.</param>
        /// <param name="durationMappingBuilder">ComponentMapping Entity id to <see cref="IComponentMappingBuilder"/> </param>
        /// <param name="databaseType">Type of the database.</param>
        /// <returns>The <see cref="ITimeDimensionMappingBuilder" />.</returns>
        ITimeDimensionMappingBuilder GetBuilder(
            DataSetColumnEntity criteriaColumn,
            TimeFormatConfiguration timeTranscoding,
            IComponentMappingBuilder durationMappingBuilder,
            string databaseType);
    }
}