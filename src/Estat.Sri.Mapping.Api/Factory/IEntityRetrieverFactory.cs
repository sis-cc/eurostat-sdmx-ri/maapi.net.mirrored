﻿// -----------------------------------------------------------------------
// <copyright file="IEntityRetrieverFactory.cs" company="EUROSTAT">
//   Date Created : 2017-02-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Factory
{
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The implementations of this interface are responsible for returning a <see cref="IEntityRetrieverEngine{TEntity}"/>.
    /// </summary>
    public interface IEntityRetrieverFactory
    {
        /// <summary>
        /// Get the retriever engine.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="mappingAssistantStoreId">The mapping assistant store identifier.</param>
        /// <returns>The <see cref="IEntityRetrieverEngine{TEntity}"/> if a matching is found; otherwise the <c>null</c>.</returns>
        IEntityRetrieverEngine<TEntity> GetRetrieverEngine<TEntity>(string mappingAssistantStoreId) where TEntity : IEntity;

        /// <summary>
        /// Gets the retriever engine.
        /// </summary>
        /// <param name="mappingAssistantStoreId">The mapping assistant store identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>The <see cref="IEntityRetrieverEngine{IEntity}"/></returns>
        IEntityRetrieverEngine<IEntity> GetRetrieverEngine(string mappingAssistantStoreId, EntityType entityType);
    }
}