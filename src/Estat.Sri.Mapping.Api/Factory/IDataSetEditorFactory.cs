﻿
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using System.Collections.Generic;

namespace Estat.Sri.Mapping.Api.Factory
{
    public interface IDataSetEditorFactory
    {
        IDataSetEditorEngine GetEngine(string editorType);

        IList<string> GetSupportedPlugins();

        IDataSetEditorInfo EditorInfo { get; }
    }
}
