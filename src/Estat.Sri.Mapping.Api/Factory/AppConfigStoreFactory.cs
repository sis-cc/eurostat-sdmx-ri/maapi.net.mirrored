﻿// -----------------------------------------------------------------------
// <copyright file="AppConfigStoreFactory.cs" company="EUROSTAT">
//   Date Created : 2017-03-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Factory
{
    using System.Configuration;

    using Estat.Sri.Mapping.Api.Engine;

    /// <summary>
    /// The App.config based <see cref="IConfigurationStoreFactory"/>
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Factory.IConfigurationStoreFactory" />
    public class AppConfigStoreFactory : IConfigurationStoreFactory
    {
        /// <summary>
        /// The application configuration store
        /// </summary>
        private readonly AppConfigStore _appConfigStore = new AppConfigStore();

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <param name="location">The location.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sri.Mapping.Api.Engine.IConfigurationStoreEngine`1" />
        /// </returns>
        public IConfigurationStoreEngine<TSettings> GetEngine<TSettings>(string location)
        {
            if (typeof(TSettings) == typeof(ConnectionStringSettings))
            {
                return (IConfigurationStoreEngine<TSettings>)new AppConfigStore(location);
            }

            return null;
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <returns>
        /// The <see cref="T:Estat.Sri.Mapping.Api.Engine.IConfigurationStoreEngine`1" />
        /// </returns>
        public IConfigurationStoreEngine<TSettings> GetEngine<TSettings>()
        {
            if (typeof(TSettings) == typeof(ConnectionStringSettings))
            {
                return (IConfigurationStoreEngine<TSettings>)_appConfigStore;
            }

            return null;
        }
    }
}