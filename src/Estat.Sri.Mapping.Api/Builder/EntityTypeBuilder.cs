// -----------------------------------------------------------------------
// <copyright file="EntityTypeBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-07-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;

namespace Estat.Sri.Mapping.Api.Builder
{
    using System;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// Returns the corresponding <see cref="EntityType"/> from the specified type
    /// </summary>
    public class EntityTypeBuilder : IEntityTypeBuilder
    {
        /// <summary>
        /// /// Builds the <see cref="EntityType"/> from the specified entity type
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>The <see cref="EntityType"/></returns>
        /// <exception cref="System.NotImplementedException">Entity not supported</exception>
        public EntityType Build(Type entityType)
        {
            if (typeof(DdbConnectionEntity) == entityType || typeof(IConnectionEntity).IsAssignableFrom(entityType))
            {
                return EntityType.DdbConnectionSettings;
            }

            if (typeof(DatasetEntity) == entityType)
            {
                return EntityType.DataSet;
            }

            if (typeof(DataSetColumnEntity) == entityType)
            {
                return EntityType.DataSetColumn;
            }

            if (typeof(LocalCodeEntity) == entityType)
            {
                return EntityType.LocalCode;
            }

            if (typeof(MappingSetEntity) == entityType)
            {
                return EntityType.MappingSet;
            }

            if (typeof(ComponentMappingEntity) == entityType)
            {
                return EntityType.Mapping;
            }

            if (typeof(TranscodingEntity) == entityType)
            {
                return EntityType.Transcoding;
            }

            if (typeof(TimeTranscodingEntity) == entityType)
            {
                return EntityType.TimeTranscoding;
            }

            if (typeof(TranscodingRuleEntity) == entityType)
            {
                return EntityType.TranscodingRule;
            }

            if (typeof(TranscodingScriptEntity) == entityType)
            {
                return EntityType.TranscodingScript;
            }

            if (typeof(ColumnDescriptionSourceEntity) == entityType)
            {
                return EntityType.DescSource;
            }

            if (typeof(DataSourceEntity) == entityType)
            {
                return EntityType.DataSource;
            }

            if (typeof(HeaderEntity) == entityType)
            {
                return EntityType.Header;
            }

            if (typeof(RegistryEntity) == entityType)
            {
                return EntityType.Registry;
            }

            if (typeof(UserEntity) == entityType)
            {
                return EntityType.User;
            }

            if (typeof(UserActionEntity) == entityType)
            {
                return EntityType.UserAction;
            }

            if (typeof(TemplateMapping) == entityType)
            {
                return EntityType.TemplateMapping;
            }

            if (typeof(TimePreFormattedEntity) == entityType)
            {
                return EntityType.TimePreFormatted;
            }

            if (typeof(DatasetPropertyEntity) == entityType)
            {
                return EntityType.DataSetProperty;
            }

            if (typeof(NsiwsEntity) == entityType)
            {
                return EntityType.Nsiws;
            }
            if (typeof(TimeDimensionMappingEntity) == entityType)
            {
                return EntityType.TimeMapping;
            }
            if (typeof(LastUpdatedEntity) == entityType)
            {
                return EntityType.LastUpdated;
            }
            if (typeof(ValidToMappingEntity) == entityType)
            {
                return EntityType.ValidToMapping;
            }
            if (typeof(UpdateStatusEntity) == entityType)
            {
                return EntityType.UpdateStatusMapping;
            }


            throw new NotImplementedException($"Entity type not supported");
        }

        public EntityType Build(string entityTypeWithPrefix)
        {
            //sync with java code
            var entityType = entityTypeWithPrefix.Split('.').Last();
            switch (entityType)
            {
                case "datasets"://OLD, replaced by "dataset"
                case "dataset":
                    return EntityType.DataSet;
                case "datasetColumn": //OLD, replaced by "column"
                case "column":
                    return EntityType.DataSetColumn;
                case "localCode":
                    return EntityType.LocalCode;
                case "ddbconnections"://OLD, replaced by "ddb"
                case "ddb":
                    return EntityType.DdbConnectionSettings;
                case "mappingsets"://OLD, replaced by "mappingset"
                case "mappingset":
                    return EntityType.MappingSet;
                case "mappings": //OLD, replaced by "mapping"
                case "mapping":
                    return EntityType.Mapping;
                case "lastupdatemappings":
                case "lastupdatemapping":
                case "lastUpdated":
                    return EntityType.LastUpdated;
                case "validtomappings":
                case "validtomapping":
                case "validTo":
                    return EntityType.ValidToMapping;
                case "updatestatusmappings":
                case "updatestatusmapping":
                case "updateStatus":
                    return EntityType.UpdateStatusMapping;
                case "genericmappings":
                    return EntityType.Mapping;
                case "transcodings": //OLD, replaced by transcoding
                case "transcoding":
                case "timemappings":
                case "timemapping":
                    return EntityType.TimeMapping;
                case "transcodingRules": //OLD, replaced by transcodingRule
                case "transcodingRule":
                    return EntityType.TranscodingRule;
                case "registry":
                    return EntityType.Registry;
                case "column_description":
                case "descriptionSource":
                    return EntityType.DescSource;
                case "entities.genericmappings":
                    return EntityType.Mapping;
                case "headerTemplates"://OLD, replaced by "header_template"
                case "header_template":
                    return EntityType.Header;
                case "users":
                case "user":
                    return EntityType.User;
                case "dataSource":
                    return EntityType.DataSource;
                case "nsiws":
                    return EntityType.Nsiws;
                case "timePreFormatted":
                    return EntityType.TimePreFormatted;
                case "templateMapping":
                    return EntityType.TemplateMapping;
            }


            throw new NotImplementedException($"Entity type not supported");
        }
        /// <summary>
        /// Builds the EntityType from the specified <typeparamref name="TEntity"/>
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>The <see cref="EntityType"/></returns>
        /// <exception cref="System.NotImplementedException">Entity not supported</exception>
        public EntityType Build<TEntity>() where TEntity : IEntity
        {
            return this.Build(typeof(TEntity));
        }
    }
}