﻿// -----------------------------------------------------------------------
// <copyright file="IEntityTypeBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-07-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Builder
{
    using System;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The EntityTypeBuilder interface.
    /// </summary>
    public interface IEntityTypeBuilder
    {
        /// <summary>
        /// Builds the <see cref="EntityType"/> from the specified entity type
        /// </summary>
        /// <param name="entityType">
        /// Type of the entity.
        /// </param>
        /// <returns>
        /// The <see cref="EntityType"/>
        /// </returns>
        /// <exception cref="System.NotImplementedException">
        /// Entity not supported
        /// </exception>
        EntityType Build(Type entityType);

        /// <summary>
        /// Builds the EntityType from the specified <typeparamref name="TEntity"/>
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>The <see cref="EntityType"/></returns>
        /// <exception cref="System.NotImplementedException">Entity not supported</exception>
        EntityType Build<TEntity>() where TEntity : IEntity;
    }
}