using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Mapping.Api.Builder
{
    public class EntityMapper
    {
        public IEntity CastToObject(string entityTypePath,EntityType entityType, JsonReader reader)
        {
            switch (entityType)
            {
                case EntityType.DdbConnectionSettings:
                    {
                        DdbConnectionEntityReader ddbEntityReader = new DdbConnectionEntityReader();
                        return ddbEntityReader.ParseEntity(reader);
                    }
                case EntityType.DataSet:
                    {
                        DatasetEntityReader datasetEntityReader = new DatasetEntityReader();
                        return datasetEntityReader.ParseEntity(reader);
                    }
                case EntityType.DataSetColumn:
                    {
                    DataSetColumnEntityReader datasetColumnEntityReader = new DataSetColumnEntityReader();
                    return datasetColumnEntityReader.ParseEntity(reader);
                    }
                case EntityType.LocalCode:
                    {
                        LocalCodeEntityReader localCodeEntityReader = new LocalCodeEntityReader();
                        return localCodeEntityReader.ParseEntity(reader);
                    }
                case EntityType.MappingSet:
                    {
                        MappingSetEntityReader mappingSetEntityReader = new MappingSetEntityReader();
                        return mappingSetEntityReader.ParseEntity(reader);
                    }
                case EntityType.Mapping:
                    {
                        ComponentMappingEntityReader mappingEntityReader = new ComponentMappingEntityReader();
                        return mappingEntityReader.ParseEntity(reader);
                    }
                case EntityType.TimeMapping:
                    {
                        var entityReader = new TimeDimensionMappingEntityReader();
                        return entityReader.ParseEntity(reader);
                    }
                case EntityType.LastUpdated:
                    {
                        var entityReader = new LastUpdatedReader();
                        return entityReader.ParseEntity(reader);
                    }
                case EntityType.ValidToMapping:
                    {
                        var entityReader = new ValidToMappingReader();
                        return entityReader.ParseEntity(reader);
                    }
                case EntityType.UpdateStatusMapping:
                    {
                        var entityReader = new UpdateStatusMappingReader();
                        return entityReader.ParseEntity(reader);
                    }

                case EntityType.TranscodingRule:
                    {
                        TranscodingRuleEntityReader transcodingRuleEntityReader = new TranscodingRuleEntityReader();
                        return transcodingRuleEntityReader.ParseEntity(reader);
                    }
                case EntityType.TimeTranscoding:
                    {
                        TimeTranscodingEntityReader timeTranscodingEntityReader = new TimeTranscodingEntityReader();
                        return timeTranscodingEntityReader.ParseEntity(reader);
                    }
                case EntityType.TranscodingScript:
                    {
                        TranscodingScriptEntityReader transcodingScriptEntityReader = new TranscodingScriptEntityReader();
                        return transcodingScriptEntityReader.ParseEntity(reader);
                    }
                case EntityType.DescSource:
                    {
                        ColumnDescriptionSourceEntityReader columnDescriptionSourceEntityReader = new ColumnDescriptionSourceEntityReader();
                        return columnDescriptionSourceEntityReader.ParseEntity(reader);
                    }
                case EntityType.Header:
                    {
                        HeaderEntityReader headerEntityReaderReader = new HeaderEntityReader();
                        return headerEntityReaderReader.ParseEntity(reader);
                    }
                case EntityType.Registry:
                    {
                        RegistryEntityReader registryEntityReader = new RegistryEntityReader();
                        return registryEntityReader.ParseEntity(reader);
                    }
                case EntityType.User:
                    {
                        UserEntityReader userEntityReader = new UserEntityReader();
                        return userEntityReader.ParseEntity(reader);
                    }
                case EntityType.UserAction:
                    {
                        UserActionEntityReader userActionEntityReader = new UserActionEntityReader();
                        return userActionEntityReader.ParseEntity(reader);
                    }
                case EntityType.TemplateMapping:
                    {
                        TemplateMappingReader templateMapping = new TemplateMappingReader();
                        return templateMapping.ParseEntity(reader);
                    }
                case EntityType.DataSource:
                    {
                        DataSourceEntityReader ddbEntityReader = new DataSourceEntityReader();
                        return ddbEntityReader.ParseEntity(reader);
                    }
                case EntityType.Nsiws:
                    {
                        NsiwsEntityReader nsiwsEntityReader = new NsiwsEntityReader();
                        return nsiwsEntityReader.ParseEntity(reader);
                    }
            }
            throw new NotImplementedException($"Entity type not supported");
        }
    }
}
