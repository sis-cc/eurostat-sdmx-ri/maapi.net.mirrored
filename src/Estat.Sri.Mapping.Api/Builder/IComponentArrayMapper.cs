// -----------------------------------------------------------------------
// <copyright file="IComponentArrayMapper.cs" company="EUROSTAT">
//   Date Created : 2023-01-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Builder
{
    using System.Collections.Generic;
    using System.Data;
    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// A component mapping builder for array components
    /// One instance for each component
    /// </summary>
    public interface IComponentArrayMapper : IComponentMappingBuilder
    {
        /// <summary>
        /// Maps the column(s) of the mapping to this component array value
        /// </summary>
        /// <param name="dataReader"></param>
        /// <returns></returns>
        string[] MapComponentArray(IDataReader dataReader);

        /// <summary>
        ///     Generates the SQL Where clause for the component used in this mapping
        ///     and the condition value from SDMX Query
        /// </summary>
        /// <param name="conditionValue">
        ///     string array with the conditional array value from the SDMX query
        /// </param>
        /// <param name="operatorValue">
        ///     string with the operator value from the SDMX query, "=" by default
        /// </param>
        /// <returns>
        ///     A SQL where clause for the columns of the mapping
        /// </returns>
        string GenerateComponentWhere(string[] conditionValue, OperatorType operatorValue);

        /// <summary>
        ///     Generates the SQL Where IN clause for the component used in this mapping
        ///     and the condition values from SDMX Query
        /// NOTE this should be used only when the operator is Equal (the only option in SOAP 2.0 and REST v1.x, the default in SOAP 2.1 and REST v2) for all values
        /// </summary>
        /// <param name="conditionValues">
        ///     the set of array conditional values for a specific component from the SDMX query
        /// </param>
        /// <param name="withNull">
        ///     whether to add NULL as a condition
        /// </param>
        /// <returns>
        ///     A SQL where clause for the columns of the mapping
        /// </returns>
        string GenerateComponentWhere(ISet<string[]> conditionValues, bool withNull = false);
    }
}
