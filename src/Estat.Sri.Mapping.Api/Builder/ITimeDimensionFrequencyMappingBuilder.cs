﻿// -----------------------------------------------------------------------
// <copyright file="ITimeDimensionMappingBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-09-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Builder
{
    using System.Data;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    /// This interface is responsible for mapping and optionally transcoding of the Time dimension based on the current Frequency code
    /// The Frequency codes should be a valid and supported SDMX frequency code, specifically one of <c>M, Q, A, S, W</c>.
    /// </summary>
    public interface ITimeDimensionFrequencyMappingBuilder
    {
        /// <summary>
        /// Maps the Time Dimension value (which extracted from <paramref name="reader"/> and normalized according to the <paramref name="frequencyValue"/>
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="frequencyValue">The frequency value.</param>
        /// <returns>The normalized time dimension value </returns>
        string MapComponent(IDataReader reader, string frequencyValue);

        /// <summary>
        ///     Generates the SQL Query where condition from the SDMX Query Time
        /// </summary>
        /// <param name="dateFrom">
        ///     The start time
        /// </param>
        /// <param name="dateTo">
        ///     The end time
        /// </param>
        /// <param name="frequencyValue">
        ///     The frequency value
        /// </param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>
        ///     The string containing SQL Query where condition
        /// </returns>
        string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, string frequencyValue, bool addIsNull = false);
    }
}