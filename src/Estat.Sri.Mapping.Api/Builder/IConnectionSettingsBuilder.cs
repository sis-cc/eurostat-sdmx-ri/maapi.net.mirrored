﻿// -----------------------------------------------------------------------
// <copyright file="IConnectionSettingsBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-04-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Builder
{
    using System.Configuration;

    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The Connection Settings Builder interface.
    /// Implement this interface to convert between various models holding connection settings
    /// </summary>
    public interface IConnectionSettingsBuilder
    {
        /// <summary>
        /// Creates the connection entity.
        /// </summary>
        /// <returns>The connection entity</returns>
        IConnectionEntity CreateConnectionEntity();

        /// <summary>
        /// Creates the connection entity from the specified <see cref="ConnectionStringSettings"/>
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>The <see cref="IConnectionEntity"/></returns>
        IConnectionEntity CreateConnectionEntity(ConnectionStringSettings settings);

        /// <summary>
        /// Creates the connection string.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>The <see cref="ConnectionStringSettings"/></returns>
        ConnectionStringSettings CreateConnectionString(IConnectionEntity connection);

        /// <summary>
        /// Creates the connection string.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>The <see cref="ConnectionStringSettings"/></returns>
        ConnectionStringSettings CreateConnectionString(DdbConnectionEntity connection);

        /// <summary>
        /// Creates the DDB connection entity.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>The <see cref="DdbConnectionEntity"/></returns>
        DdbConnectionEntity CreateDdbConnectionEntity(IConnectionEntity connection);

        /// <summary>
        /// Creates the connection entity.
        /// </summary>
        /// <param name="ddbConnection">The DDB connection.</param>
        /// <returns>The <see cref="IConnectionEntity"/></returns>
        IConnectionEntity CreateConnectionEntity(DdbConnectionEntity ddbConnection);
    }
}