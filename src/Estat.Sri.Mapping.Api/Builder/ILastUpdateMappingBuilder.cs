// -----------------------------------------------------------------------
// <copyright file="ILastUpdateMappingBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-09-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Model.Base;

namespace Estat.Sri.Mapping.Api.Builder
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    /// The LastUpdateMappingBuilder interface.
    /// </summary>
    public interface ILastUpdateMappingBuilder
    {
        /// <summary>
        /// Generates the where.
        /// </summary>
        /// <param name="timeRanges">The time ranges.</param>
        /// <returns>
        /// The <see cref="SqlClause" />.
        /// </returns>
        SqlClause GenerateWhere(IList<ITimeRange> timeRanges);

        /// <summary>
        /// Generates where statement for asOf querystring parameter
        /// </summary>
        /// <param name="validTo">ValidTo mapping</param>
        /// <param name="asOfDate">AsOfDate query parameter</param>
        /// <returns></returns>
        SqlClause AsOfWhere(ComponentMappingEntity validTo, ISdmxDate asOfDate);
    }
}