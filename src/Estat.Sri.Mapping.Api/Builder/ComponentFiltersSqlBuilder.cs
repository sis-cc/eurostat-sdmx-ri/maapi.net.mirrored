// -----------------------------------------------------------------------
// <copyright file="ComponentFiltersSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2022-10-24
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data.Common;
    using System.Linq;
    using System.Text;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Utils;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    /// Builds the sql query from a collection of <see cref="ComponentFilter"/>
    /// </summary>
    public class ComponentFiltersSqlBuilder
    {
        private readonly StringBuilder _queryBuilder = new StringBuilder();
        private readonly List<DbParameter> _dbParameters = new List<DbParameter>();

        /// <summary>
        /// Initializes a new instance of the class
        /// </summary>
        /// <param name="buildFrom">The component filters</param>
        /// <param name="mappingBuilders">the available component mappers</param>
        public ComponentFiltersSqlBuilder(IList<ComponentFilter> buildFrom, IDictionary<string, IComponentMappingBuilder> mappingBuilders)
        {
            Build(buildFrom, mappingBuilders);
        }

        /// <summary>
        /// Returns the query being built
        /// </summary>
        /// <returns></returns>
        public string GetSqlQuery()
        {
            return _queryBuilder.ToString();
        }

        /// <summary>
        /// The database parameters for the query.
        /// </summary>
        public IList<DbParameter> DbParameters => new ReadOnlyCollection<DbParameter>(_dbParameters);

        private void Build(IList<ComponentFilter> buildFrom, IDictionary<string, IComponentMappingBuilder> mappingBuilders)
        {
            bool appendAnd = false;
            foreach (var componentFilter in buildFrom)
            {
                // componentFilter holds all filters for a component

                // for now special case time range is handled in SelectionGroups
                // time range is not even mapped
                if (componentFilter.IsTimeRange)
                {
                    continue;
                }

                // try get the component mapping
                if (!mappingBuilders.TryGetValue(componentFilter.ComponentName, out IComponentMappingBuilder compInfo))
                {
                    throw new ArgumentException($"Component {componentFilter.ComponentName} is not mapped.");
                }

                if (appendAnd)
                {
                    _queryBuilder.Append(" AND ");
                    appendAnd = false;
                }
                _queryBuilder.Append("(");

                // iterate through the filters for a single component
                var iterator = componentFilter.StartIterator();
                while (iterator.MoveNext())
                {
                    // this loop navigates us to all filters in a component
                    var filter = iterator.Current;
                    OperatorType operatorType = RESTQueryOperator2OperatorTypeConverter.Convert(filter.OperatorType);

                    switch (filter.LogicalOperator)
                    {
                        case null:
                            break;
                        case 0:
                            _queryBuilder.Append(" AND ");
                            break;
                        case 1:
                            _queryBuilder.Append(" OR ");
                            break;
                        default:
                            throw new ArgumentException($"Value {filter.LogicalOperator} does not correspond to a logcal operator.");
                    }

                    _queryBuilder.Append("(");
                    // array components
                    if (filter.HasArrayValues
                        && compInfo is IComponentArrayMapper arrayMapper)
                    {
                        if (filter.ArrayValues.Count > 1) 
                        {
                            HashSet<string[]> arrayValues = new HashSet<string[]>();
                            foreach (var arrayValue in filter.ArrayValues)
                            {
                                arrayValues.Add(arrayValue.ToArray());
                            }
                            _queryBuilder.Append(arrayMapper.GenerateComponentWhere(arrayValues));
                        }
                        else
                        {
                            string[] arrayValue = filter.ArrayValues.Single().ToArray();
                            _queryBuilder.Append(arrayMapper.GenerateComponentWhere(arrayValue, operatorType));
                        }
                    }
                    // scalar components
                    else
                    {
                        if (filter.Values.Count > 1)
                        {
                            _queryBuilder.Append(compInfo.GenerateComponentWhere(new HashSet<string>(filter.Values)));
                        }
                        else
                        {
                            string value = filter.Values.Single();
                            _queryBuilder.Append(compInfo.GenerateComponentWhere(value, operatorType));
                        }
                    }
                    _queryBuilder.Append(")");

                    appendAnd = true;
                }

                _queryBuilder.Append(")");
            }
        }
    }
}
