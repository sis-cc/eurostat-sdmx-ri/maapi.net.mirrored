﻿// -----------------------------------------------------------------------
// <copyright file="ColumNameComparer.cs" company="EUROSTAT">
//   Date Created : 2022-08-18
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Utils
{
    /// <summary>
    /// Compare <see cref="DataSetColumnEntity"/> by their Name value
    /// </summary>
    public class ColumNameComparer : IEqualityComparer<DataSetColumnEntity>
    {
        /// <inheritdoc />
        public bool Equals(DataSetColumnEntity x, DataSetColumnEntity y)
        {
            if (x == null)
            {
                return y == null;
            }

            if (y == null)
            {
                return false;
            }

            return ReferenceEquals(x, y) || string.Equals(x.Name, y.Name);
        }

        /// <inheritdoc />
        public int GetHashCode(DataSetColumnEntity obj)
        {
            if (obj.Name == null)
            {
                return -1;
            }
            
            return obj.Name.GetHashCode();
        }
    }
}