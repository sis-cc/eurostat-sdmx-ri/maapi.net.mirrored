// -----------------------------------------------------------------------
// <copyright file="TimeTranscodingConversionHelper.cs" company="EUROSTAT">
//   Date Created : 2022-07-31
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Exceptions;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Model.AdvancedTime;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Mapping.Api.Utils
{
    /// <summary>
    /// Convert between simple and advance time transcoding entities
    /// </summary>
    public static class TimeTranscodingConversionHelper
    {
        /// <summary>
        /// Converts from Simple frequency based transcoding entities to Advance transcoding entities
        /// </summary>
        /// <param name="transcodingEntity">The list of simple transcoding entities</param>
        /// <param name="frequencyDimensionId">The frequency dimension ID. Although in 99% of the cases it is <c>FREQ</c>.
        /// The correct value must be retrieved from the <c>Id</c> of <see cref="IDataStructureObject.FrequencyDimension"/> </param>
        /// <returns>The advance time transcoding</returns>
        public static TimeTranscodingAdvancedEntity Convert(IList<TimeTranscodingEntity> transcodingEntity, string frequencyDimensionId)
        {
            if (transcodingEntity == null)
            {
                throw new ArgumentNullException(nameof(transcodingEntity));
            }

            if (transcodingEntity.Count == 0)
            {
                throw new ArgumentException("Value cannot be an empty collection.", nameof(transcodingEntity));
            }

            TimeTranscodingAdvancedEntity advancedEntity = new TimeTranscodingAdvancedEntity();
            advancedEntity.IsFrequencyDimension = true;
            advancedEntity.FrequencyDimension = frequencyDimensionId;
            foreach (var timeTranscodingEntity in transcodingEntity)
            {
                TimeFormatConfiguration timeFormatConfiguration = new TimeFormatConfiguration();
                timeFormatConfiguration.CriteriaValue = timeTranscodingEntity.Frequency;
                timeFormatConfiguration.OutputFormat =
                    TimeFormat.GetTimeFormatFromCodeId(timeTranscodingEntity
                        .Frequency); // Might not always work ?
                TimeParticleConfiguration particleConfig = new TimeParticleConfiguration();
                particleConfig.Format = GetDbFormatFromFreq(timeTranscodingEntity);
                if (timeTranscodingEntity.IsDateTime)
                {
                    particleConfig.DateColumn = timeTranscodingEntity.DateColumn;
                }
                else
                {
                    particleConfig.Year = new TimeRelatedColumnInfo();
                    particleConfig.Year.Column = timeTranscodingEntity.Year.Column;
                    particleConfig.Year.Length = timeTranscodingEntity.Year.Length;
                    particleConfig.Year.Start = timeTranscodingEntity.Year.Start;
                    if (timeTranscodingEntity.Period != null)
                    {
                        particleConfig.Period = new PeriodTimeRelatedColumnInfo();
                        particleConfig.Period.Column = timeTranscodingEntity.Period.Column;
                        particleConfig.Period.Length = timeTranscodingEntity.Period.Length;
                        particleConfig.Period.Start = timeTranscodingEntity.Period.Start;
                        particleConfig.Period.Rules.AddAll(timeTranscodingEntity.Period.Rules.Select(t =>
                            new PeriodCodeMap()
                            {
                                LocalPeriodCode = t.LocalCodes[0].Name ?? t.LocalCodes[0].ObjectId,
                                SdmxPeriodCode = t.UncodedValue ?? t.DsdCodeEntity.ObjectId
                            }));
                    }
                }

                timeFormatConfiguration.SetConfiguration(particleConfig);
                advancedEntity.TimeFormatConfigurations.Add(timeTranscodingEntity.Frequency,
                    timeFormatConfiguration);
            }

            return advancedEntity;
        }

        /// <summary>
        /// Convert from an advance time transcoding entity to a simple ones.
        /// One <see cref="TimeTranscodingEntity"/> is created for each frequency value
        /// </summary>
        /// <param name="advancedEntity">The advance time transcoding</param>
        /// <returns>A list of <see cref="TimeTranscodingEntity"/>, one for each criteria value </returns>
        /// <exception cref="ResourceConflictException">The transcoding is not based on frequency</exception>
        public static IList<TimeTranscodingEntity> Convert(TimeTranscodingAdvancedEntity advancedEntity)
        {
            if (!advancedEntity.IsFrequencyDimension)
            {
                throw new ResourceConflictException("");
            }
            List<TimeTranscodingEntity> timeTranscodingEntities = new List<TimeTranscodingEntity>();
            foreach (var config in advancedEntity.TimeFormatConfigurations.Values)
            {
                TimeTranscodingEntity transcodingEntity = new TimeTranscodingEntity();
                transcodingEntity.Frequency = config.CriteriaValue;
                timeTranscodingEntities.Add(transcodingEntity);
                if (config.StartConfig != null)
                {
                    if (config.StartConfig.DateColumn != null)
                    {
                        transcodingEntity.DateColumn = config.StartConfig.DateColumn;
                        transcodingEntity.IsDateTime = true;
                    }
                    else if (config.StartConfig.Year != null)
                    {
                        transcodingEntity.Year = new TimeTranscoding();
                        transcodingEntity.Year.Column = config.StartConfig.Year.Column;
                        transcodingEntity.Year.Start = config.StartConfig.Year.Start;
                        transcodingEntity.Year.Length = config.StartConfig.Year.Length;
                        if (config.StartConfig.Period != null)
                        {
                            transcodingEntity.Period = new PeriodTimeTranscoding();
                            transcodingEntity.Period.Column = config.StartConfig.Period.Column;
                            transcodingEntity.Period.Start = config.StartConfig.Period.Start;
                            transcodingEntity.Period.Length = config.StartConfig.Period.Length;
                            foreach (var periodCodeMap in config.StartConfig.Period.Rules)
                            {
                                transcodingEntity.Period.AddRule(periodCodeMap.SdmxPeriodCode, periodCodeMap.LocalPeriodCode);
                            }
                        }

                    }
                }
            }


            return timeTranscodingEntities;
        }

        private static DisseminationDatabaseTimeFormat GetDbFormatFromFreq(TimeTranscodingEntity timeTranscodingEntity)
        {
            if (timeTranscodingEntity.IsDateTime)
            {
                return DisseminationDatabaseTimeFormat.TimestampOrDate;
            }

            switch (timeTranscodingEntity.Frequency)
            {
                case "A":
                case "A1":
                    return DisseminationDatabaseTimeFormat.Annual;
                case "M":
                    return DisseminationDatabaseTimeFormat.Monthly;
                case "Q":
                    return DisseminationDatabaseTimeFormat.Quarterly;
                case "S":
                case "B":
                    return DisseminationDatabaseTimeFormat.Semester;
                case "T":
                    return DisseminationDatabaseTimeFormat.Trimester;
                case "W":
                    return DisseminationDatabaseTimeFormat.Weekly;
            }

            throw new SdmxNotImplementedException($"Frequency not supported : '{timeTranscodingEntity.Frequency}'");
        }
    }
}