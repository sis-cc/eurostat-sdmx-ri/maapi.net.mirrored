// -----------------------------------------------------------------------
// <copyright file="JsonReaderExtension.cs" company="EUROSTAT">
//   Date Created : 2014-09-17
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using System;
using Newtonsoft.Json;

namespace Estat.Sri.Mapping.Api.Engine
{
    /// <summary>
    /// JsonReader extension methods
    /// </summary>
    public static class JsonReaderExtension
    {
        /// <summary>
        /// Extract value as nullable int
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static int? ReadAsNullableInt(this JsonReader reader)
        {
            reader.Read();
            if(reader.Value == null)
            {
                return null;
            }
            else
            {
                return Convert.ToInt32(reader.Value);
            }
        }
    }
}
