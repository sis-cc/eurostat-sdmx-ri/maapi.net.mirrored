// -----------------------------------------------------------------------
// <copyright file="JsonReaderExtension.cs" company="EUROSTAT">
//   Date Created : 2014-09-17
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Estat.Sri.Mapping.Api.Model;
using System.Text;

namespace Estat.Sri.Mapping.Api.Utils
{
    public static class PatchRequestParser
    {
        public static PatchRequest ParsePatch(string patchFile)
        {
            FileInfo patchSource = new FileInfo(patchFile);
            using (var stream = patchSource.OpenRead())
            {
                JToken inputtoken = ParseJsonToken(stream);
                JArray array = (JArray)inputtoken;
                return ParsePatchDocument(array);
            }
        }
        public static PatchRequest ParsePatchFromText(string jsonText)
        {
            JArray array = JArray.Parse(jsonText);
            return ParsePatchDocument(array);
            PatchRequest patchRequest = new PatchRequest();
            foreach (JObject row in array)
            {
                var op = Parse(row, "op", (string)null);
                var path = Parse(row, "path", (string)null);
                var value = ParseDynamic(row, "value");
                var fromValue = Parse(row, "from", (string)null);
                // also validates if op and path are set
                var patch = new PatchDocument(op, path, value, fromValue);
                patchRequest.Add(patch);
            }
            return patchRequest;
        }


        public static PatchRequest ParsePatchDocument(JArray array)
        {
            PatchRequest patchRequest = new PatchRequest();
            foreach (JObject row in array)
            {
                var op = Parse(row, "op", (string)null);
                var path = Parse(row, "path", (string)null);
                var value = ParseDynamic(row, "value");
                var fromValue = Parse(row, "from", (string)null);
                // also validates if op and path are set
                var patch = new PatchDocument(op, path, value, fromValue);
                patchRequest.Add(patch);
            }
            return patchRequest;
        }

        public static JObject ParseJson(Stream request)
        {
            using (var reader = new StreamReader(request))
            using (var jsonTextReader = new JsonTextReader(reader))
            {
                return JObject.Load(jsonTextReader);
            }
        }

        public static JToken ParseJsonToken(Stream request)
        {
            using (var reader = new StreamReader(request))
            using (var jsonTextReader = new JsonTextReader(reader))
            {
                return JToken.Load(jsonTextReader);
            }
        }

        public static TValue Parse<TValue>(JObject jObject, string propertyName, TValue defaultValue)
        {
            if (jObject.TryGetValue(propertyName, out JToken token))
            {
                return token.Value<TValue>();
            }

            return defaultValue;
        }

        public static dynamic ParseDynamic(JObject jObject, string propertyName)
        {
            if (jObject.TryGetValue(propertyName, out JToken token))
            {
                switch (token.Type)
                {
                    case JTokenType.String:
                        return token.Value<string>();
                    case JTokenType.Boolean:
                        return token.Value<bool>();
                    case JTokenType.Integer:
                        return token.Value<int>();
                    case JTokenType.Float:
                        return token.Value<double>();
                    case JTokenType.Object:
                        return token.Value<JObject>();
                    case JTokenType.Null:
                        return null;
                    default:
                        return token.Value<object>();
                }
            }

            return null;
        }

    }
}
