// -----------------------------------------------------------------------
// <copyright file="DuplicateLocalCodeValidator.cs" company="EUROSTAT">
//   Date Created : 2017-05-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// The duplicate local code validator.
    /// </summary>
    public class DuplicateLocalCodeValidator : ILocalCodeValidator
    {
        /// <summary>
        ///     The _duplicate codes.
        /// </summary>
        private readonly Dictionary<string, HashSet<string>> _duplicateCodes = new Dictionary<string, HashSet<string>>(StringComparer.Ordinal);
        
        /// <summary>
        ///     The _error messages.
        /// </summary>
        private readonly IList<string> _errorMessages;

        /// <summary>
        /// Initializes a new instance of the <see cref="DuplicateLocalCodeValidator"/> class.
        /// </summary>
        /// <param name="errorMessages">The error messages.</param>
        public DuplicateLocalCodeValidator(IList<string> errorMessages)
        {
            _errorMessages = errorMessages;
        }

        /// <summary>
        /// Determines whether the specified <paramref name="value" /> is valid local code.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns>
        ///   <c>true</c> if it is valid; otherwise false.
        /// </returns>
        public bool IsValidLocalCode(object value, string columnName)
        {
            if (!Convert.IsDBNull(value))
            {
                var duplicateCodes = GetDuplicateCodes(columnName);
                var localCode = Convert.ToString(value, CultureInfo.InvariantCulture);

                if (!string.IsNullOrWhiteSpace(localCode))
                {
                    if (!duplicateCodes.Contains(localCode))
                    {
                        duplicateCodes.Add(localCode);
                    }
                    else
                    {
                        var format = $"Ignored code(s) '{localCode}' at Column {columnName} because it already exists";
                        this._errorMessages.Add(format);
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Gets the duplicate codes for the specified column.
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <returns>The duplicate codes</returns>
        private HashSet<string> GetDuplicateCodes(string columnName)
        {
            HashSet<string> visitedCodes;
            if (!this._duplicateCodes.TryGetValue(columnName, out visitedCodes))
            {
                visitedCodes = new HashSet<string>(StringComparer.Ordinal);
                this._duplicateCodes.Add(columnName, visitedCodes);
            }

            return visitedCodes;
        }
    }
}