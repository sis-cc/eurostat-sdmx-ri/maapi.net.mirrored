﻿// -----------------------------------------------------------------------
// <copyright file="LocalCodeValidator.cs" company="EUROSTAT">
//   Date Created : 2013-06-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    ///     The local code validator.
    /// </summary>
    public class LocalCodeValidator : ILocalCodeValidator
    {
        /// <summary>
        ///     The _error messages.
        /// </summary>
        private readonly IList<string> _errorMessages;

        /// <summary>
        ///     The _max length.
        /// </summary>
        private readonly int _maxLength;

        /// <summary>
        ///     Initializes a new instance of the <see cref="LocalCodeValidator" /> class.
        /// </summary>
        /// <param name="errorMessages">
        ///     The error Messages.
        /// </param>
        /// <param name="maxLength">
        ///     The max Length.
        /// </param>
        public LocalCodeValidator(IList<string> errorMessages, int maxLength)
        {
            this._errorMessages = errorMessages;
            this._maxLength = maxLength;
        }

        /// <summary>
        /// Determines whether the specified <paramref name="value" /> is valid local code.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns>
        ///   <c>true</c> if it is valid; otherwise false.
        /// </returns>
        public bool IsValidLocalCode(object value, string columnName)
        {
            bool invalid = false;
            if (!Convert.IsDBNull(value))
            {
                var localCode = Convert.ToString(value, CultureInfo.InvariantCulture);
                if (localCode.Length <= this._maxLength)
                {
                    if (string.IsNullOrWhiteSpace(localCode))
                    {
                        var format = $"Ignored code(s) '{localCode}' at Column {columnName} because it is empty.";
                        this._errorMessages.Add(format);
                        invalid = true;
                    }
                }
                else
                {
                    var format = string.Format("Ignored code(s) '{0}' at Column {2} because it exceeds the maximum length of {1} characters.", localCode, this._maxLength, columnName);
                    this._errorMessages.Add(format);
                    invalid = true;
                }
            }
            else
            {
                var format = $"Ignoring a code at column {columnName} because it has no value.";
                this._errorMessages.Add(format);
                invalid = true;
            }

            return !invalid;
        }
    }
}