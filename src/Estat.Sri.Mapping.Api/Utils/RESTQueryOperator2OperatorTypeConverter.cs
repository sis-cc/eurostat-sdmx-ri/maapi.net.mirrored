// -----------------------------------------------------------------------
// <copyright file="RESTQueryOperator2OperatorTypeConverter.cs" company="EUROSTAT">
//   Date Created : 2021-08-23
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Utils
{
    using System;
    using Estat.Sri.Mapping.Api.Constant;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Converts a <see cref="RESTQueryOperator"/> to <see cref="OperatorType"/>
    /// </summary>
    public static class RESTQueryOperator2OperatorTypeConverter
    {
        /// <summary>
        /// Converts a <see cref="RESTQueryOperator"/> to <see cref="OperatorType"/>
        /// </summary>
        /// <param name="operatorType">The operator value to convert</param>
        /// <returns>A operator value of type <see cref="OperatorType"/>.</returns>
        public static OperatorType Convert(RESTQueryOperator operatorType)
        {
            switch (operatorType.EnumType)
            {
                case RESTQueryOperatorEnumType.Contains:
                    return OperatorType.Contains;
                case RESTQueryOperatorEnumType.DoesNotContain:
                    return OperatorType.Contains | OperatorType.Not;
                case RESTQueryOperatorEnumType.EndsWith:
                    return OperatorType.EndsWith;
                case RESTQueryOperatorEnumType.Equals:
                    return OperatorType.Exact;
                case RESTQueryOperatorEnumType.GreaterThan:
                    return OperatorType.GreaterThan;
                case RESTQueryOperatorEnumType.GreaterThanOrEqualTo:
                    return OperatorType.GreaterThanOrEqual;
                case RESTQueryOperatorEnumType.LessThan:
                    return OperatorType.LessThan;
                case RESTQueryOperatorEnumType.LessThanOrEqualTo:
                    return OperatorType.LessThanOrEqual;
                case RESTQueryOperatorEnumType.NotEqualTo:
                    return OperatorType.Not | OperatorType.Exact;
                case RESTQueryOperatorEnumType.Null:
                    throw new ArgumentNullException(nameof(operatorType) + " is not set.");
                case RESTQueryOperatorEnumType.StartsWith:
                    return OperatorType.StartsWith;
                default:
                    return OperatorType.Exact;
            }
        }
    }
}
