using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Utils
{
    public static class EntityTypeExtension
    {
        // TODO what is the difference between this and CommonEntityExtension.GetParent ?
        public static EntityType ParentType(this EntityType entityType)
        {
            switch (entityType)
            {
                case EntityType.DdbConnectionSettings:
                    return EntityType.None;
                case EntityType.DataSet:
                    return EntityType.DdbConnectionSettings;
                case EntityType.DataSetColumn:
                    return EntityType.DataSet;
                case EntityType.MappingSet:
                    return EntityType.None;
                case EntityType.Mapping:
                case EntityType.TimeMapping:
                case EntityType.LastUpdated:
                case EntityType.UpdateStatusMapping:
                case EntityType.ValidToMapping:
                    return EntityType.MappingSet;
                case EntityType.Transcoding:
                    return EntityType.Mapping;
                case EntityType.TimeTranscoding:
                    return EntityType.TimeMapping;
                case EntityType.TranscodingRule:
                    return EntityType.Mapping;
                case EntityType.TranscodingScript:
                    return EntityType.Mapping;
                case EntityType.LocalCode:
                    return EntityType.DataSetColumn;
                case EntityType.DescSource:
                    return EntityType.DataSetColumn;
                case EntityType.Header:
                    return EntityType.None;
                case EntityType.TemplateMapping:
                    return EntityType.None;
                case EntityType.Registry:
                    return EntityType.None;
                default:
                    return EntityType.None;
            }
        }
    }
}
