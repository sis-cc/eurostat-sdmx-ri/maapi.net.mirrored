// -----------------------------------------------------------------------
// <copyright file="AuthorizationCache.cs" company="EUROSTAT">
//   Date Created : 2020-1-30
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Threading;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Utils
{
    public class AuthorizationCache : IDisposable
    {
        private static AsyncLocal<Dictionary<(EntityType, string, string, AccessType), bool>> _cache = new AsyncLocal<Dictionary<(EntityType, string, string, AccessType), bool>>{Value = new Dictionary<(EntityType, string, string, AccessType), bool>()};
        private static AsyncLocal<bool> _enabled = new AsyncLocal<bool>{ Value = false};
        private static AsyncLocal<string> _storeId = new AsyncLocal<string>();

        public AuthorizationCache(string storeId)
        {
            _enabled.Value = true;
            _storeId.Value = storeId;
            _cache.Value = new Dictionary<(EntityType, string, string, AccessType), bool>();
        }

        public static bool? CanAccess(EntityType entityType, string storeId, string entityId, AccessType accessType)
        {
            if (!_enabled.Value)
            {
                return null;
            }
 
            if (_cache.Value.TryGetValue((entityType, storeId, entityId, accessType), out bool result)) 
            {
                return result;
            }

            return null;
        }

        public static void Add(EntityType entityType, string storeId, string entityId, AccessType accessType, bool shouldAccess)
        {
            if (!_enabled.Value)
            {
                return;
            }
            _cache.Value[(entityType, storeId, entityId, accessType)] = shouldAccess;
        }

        public static string StoreId
        {
            get
            {
                if (!_enabled.Value)
                {
                    return null;
                }

                return _storeId.Value;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _enabled.Value = false;
                    _storeId.Value = null;
                    _cache.Value.Clear();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

    }
}
