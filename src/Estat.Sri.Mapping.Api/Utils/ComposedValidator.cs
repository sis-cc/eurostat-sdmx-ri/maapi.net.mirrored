// -----------------------------------------------------------------------
// <copyright file="ComposedValidator.cs" company="EUROSTAT">
//   Date Created : 2017-05-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Utils
{
    using System.Linq;

    /// <summary>
    /// The composed validator.
    /// </summary>
    internal class ComposedValidator : ILocalCodeValidator
    {
        /// <summary>
        /// The decorated validators
        /// </summary>
        private readonly ILocalCodeValidator[] _decoratedValidators;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComposedValidator"/> class.
        /// </summary>
        /// <param name="codeValidators">The code validators.</param>
        public ComposedValidator(params ILocalCodeValidator[] codeValidators)
        {
            this._decoratedValidators = codeValidators?.ToArray() ?? new ILocalCodeValidator[0];
        }

        /// <summary>
        /// Determines whether the specified <paramref name="value" /> is valid local code.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns>
        ///   <c>true</c> if it is valid; otherwise false.
        /// </returns>
        public bool IsValidLocalCode(object value, string columnName)
        {
            foreach (var decoratedValidator in _decoratedValidators)
            {
                if (!decoratedValidator.IsValidLocalCode(value, columnName))
                {
                    return false;
                }
            }

            return true;
        }
    }
}