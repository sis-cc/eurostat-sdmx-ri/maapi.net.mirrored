﻿// -----------------------------------------------------------------------
// <copyright file="SingleRequestScope.cs" company="EUROSTAT">
//   Date Created : 2017-07-27
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Utils
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// A class that can be used to by pass dataflow authorization
    /// </summary>
    public class SingleRequestScope : IDisposable
    {
        /// <summary>
        /// The should authorize
        /// </summary>
        [ThreadStatic]
        private static Stack<RequestScopeModel> _stack;

        /// <summary>
        /// Initializes a new instance of the <see cref="SingleRequestScope"/> class.
        /// </summary>
        public SingleRequestScope()
        {
            RequestScopeModel parentModel;
            if (!IsInContext())
            {
                _stack = new Stack<RequestScopeModel>();
                parentModel = new RequestScopeModel();
            }
            else
            {
                parentModel = _stack.Peek();
            }

            var currentRequest = new RequestScopeModel()
                                     {
                                         RecordOnlyRoot = parentModel.RecordOnlyRoot,
                                         AuthorizationDisabled = parentModel.AuthorizationDisabled,
                                         LastRecordedEntity = parentModel.LastRecordedEntity,
                                         AuthorizeOnlyRoot = parentModel.AuthorizeOnlyRoot
                                     };
            _stack.Push(currentRequest);
        }

        /// <summary>
        /// Gets a value indicating whether it should authorize.
        /// </summary>
        /// <value>
        ///   <c>true</c> if it should authorize; otherwise, <c>false</c>.
        /// </value>
        public static bool EnsureAuthorization
        {
            get
            {
                if (!IsInContext())
                {
                    return false;
                }

                var requestScopeModel = _stack.Peek();
                if (!requestScopeModel.AuthorizationDisabled)
                {
                   // requestScopeModel.AuthorizationDisabled = true;
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Should authorize the specified type.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>True if the specified type should be authorized</returns>
        public static bool ShouldAuthorize(EntityType entityType)
        {
            if (!IsInContext())
            {
                return true;
            }

            var requestScopeModel = _stack.Peek();
            if (requestScopeModel.AuthorizeOnlyRoot)
            {
                return entityType.IsRootEntity();
            }

            return true;
        }

        /// <summary>
        /// Authorizes the only root entities.
        /// </summary>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public void AuthorizeOnlyRootEntities(bool value)
        {
            if (!IsInContext())
            {
                return;
            }

            var requestScopeModel = _stack.Peek();
            requestScopeModel.AuthorizeOnlyRoot = value;
        }

        /// <summary>
        /// Setting this to true will disable authorization
        /// TODO change name
        /// </summary>
        /// <param name="value">Setting this to <c>true</c> to disable authorization, i.e. authorization has been done and we don't want to make costly authorization calls anymore.</param>
        public void TrySetAuthorizationState(bool value)
        {
            if (IsInContext())
            {
                _stack.Peek().AuthorizationDisabled = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether action is recorded.
        /// </summary>
        /// <value><c>true</c> if action is recorded; otherwise, <c>false</c>.</value>
        public bool RecordOnlyRoot
        {
            get
            {
                if (!IsInContext())
                {
                    return false;
                }

                return _stack.Peek().RecordOnlyRoot;
            }
            set
            {
                if (IsInContext())
                {
                    _stack.Peek().RecordOnlyRoot = value;
                }
            }
        }

        /// <summary>
        /// Ensures the action is recorded.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns><c>true</c> if the action for this item is already recorded, <c>false</c> otherwise.</returns>
        public static bool EnsureActionRecorded(IEntity entity)
        {
            if (entity == null)
            {
                return false;
            }

            if (!IsInContext())
            {
                return false;
            }

            var requestScopeModel = _stack.Peek();
            if (requestScopeModel.RecordOnlyRoot)
            {
                return !entity.TypeOfEntity.IsRootEntity();
            }

            if (requestScopeModel.LastRecordedEntity == null)
            {
                requestScopeModel.LastRecordedEntity = entity;
                return false;
            }

            if (!entity.TypeOfEntity.IsRootEntity() && entity.TypeOfEntity == requestScopeModel.LastRecordedEntity.TypeOfEntity && string.Equals(entity.ParentId, requestScopeModel.LastRecordedEntity.ParentId))
            {
                return true;
            }

            if (entity.TypeOfEntity.GetParentType() == requestScopeModel.LastRecordedEntity.TypeOfEntity && string.Equals(entity.ParentId, requestScopeModel.LastRecordedEntity.EntityId))
            {
                return true;
            }

            requestScopeModel.LastRecordedEntity = entity;
            return false;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
            if (managed)
            {
                if (_stack.Count > 0)
                {
                    _stack.Pop();
                }

                if (_stack.Count == 0)
                {
                    _stack = null;
                }
            }
        }

        /// <summary>
        /// Determines whether [is in context].
        /// </summary>
        /// <returns><c>true</c> if [is in context]; otherwise, <c>false</c>.</returns>
        private static bool IsInContext()
        {
            return _stack != null && _stack.Count > 0;
        }
    }
}