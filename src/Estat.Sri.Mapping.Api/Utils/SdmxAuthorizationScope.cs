// -----------------------------------------------------------------------
// <copyright file="SdmxAuthorizationScope.cs" company="EUROSTAT">
//   Date Created : 2020-9-9
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Threading;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Utils
{
    public class SdmxAuthorizationScope : IDisposable
    {
        private class SdmxAuthorizationInternalScope
        {
            private bool disposedValue;
            private static readonly AsyncLocal<ISdmxAuthorization> _currentSdmxAuthorization = new AsyncLocal<ISdmxAuthorization>();

            public static ISdmxAuthorization Current
            {
                get
                {
                    return _currentSdmxAuthorization.Value;
                }
                set
                {
                    _currentSdmxAuthorization.Value = value;
                }
            }

            public static bool Enabled
            {
                get
                {
                    return _currentSdmxAuthorization.Value != null;
                }
            }

            public static void ClearCurrent()
            {
                _currentSdmxAuthorization.Value = null;
            }


            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        _currentSdmxAuthorization.Value = null;
                    }

                    disposedValue = true;
                }
            }

            public void Dispose()
            {
                // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
                Dispose(disposing: true);
                GC.SuppressFinalize(this);
            }
        }

        private bool disposedValue;
        private bool shouldDispose;
        private ISdmxAuthorization previous = null;

        public SdmxAuthorizationScope(ISdmxAuthorization sdmxAuthorization)
        {
            SdmxAuthorizationInternalScope.Current = sdmxAuthorization;
            shouldDispose = true;
        }

        public SdmxAuthorizationScope(Authorisation authorization)
        {
            switch(authorization)
            {
                case Authorisation.Required:
                    FailWhenNoAuthorization();
                    break;
                case Authorisation.Skip:
                    previous = SdmxAuthorizationInternalScope.Current;
                    SdmxAuthorizationInternalScope.Current = null;
                    break;
                case Authorisation.Optional:
                default:
                    break;
            }
            shouldDispose = false;
        }

        // TODO make private
        public static void FailWhenNoAuthorization()
        {
            if (!Enabled)
            {
                throw new InvalidOperationException("Call only when SdmxAuthorizationScope.Enabled == true");
            }
        }

        // TODO make private
        public static ISdmxAuthorization Current
        {
            get
            {
                return SdmxAuthorizationInternalScope.Current;
            }
            set
            {
                SdmxAuthorizationInternalScope.Current = value;
            }
        }

        // TODO make private
        public static bool Enabled
        {
            get
            {
                return SdmxAuthorizationInternalScope.Enabled;
            }
        }

        // TODO make private
        public static void ClearCurrent()
        {
            SdmxAuthorizationInternalScope.ClearCurrent();
        }

        /// <summary>
        /// Gets a value indicating whether authorization is enabled.
        /// </summary>
        public bool IsEnabled => Enabled;

        public ISdmxAuthorization CurrentSdmxAuthorisation
        {
            get
            {
                FailWhenNoAuthorization();
                return Current;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (shouldDispose)
                    {
                        SdmxAuthorizationInternalScope.ClearCurrent();
                    }
                    else if (previous != null)
                    {
                        SdmxAuthorizationInternalScope.Current = previous;
                        previous = null;
                    }
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}