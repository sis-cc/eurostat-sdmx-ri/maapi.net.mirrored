using System.Collections.Generic;
using System.Text;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    public class Party : SimpleNameableEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public Party(): base(EntityType.Header)
        {
        }

        /// Initializes a new instance of the <see cref="Party" /> class.
        /// <param name="entityId">The entity ID.</param>
        /// <param name="name">Name.</param>
        /// <param name="contact">Contact.</param>
        public Party(string entityId , string name  , List<Contact> contact)
            : base(EntityType.Header)
        {
            this.EntityId = entityId;
            this.Name = name;
            this.Contact = contact;

        }

        public List<Contact> Contact { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Party {\n");
            sb.Append("  EntityId: ").Append(EntityId).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Contact: ").Append(this.Contact).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Party)obj);
        }

        /// <summary>
        /// Returns true if Party instances are equal
        /// </summary>
        /// <param name="other">Instance of Party to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Party other)
        {

            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return
                (
                    this.EntityId == other.EntityId ||
                    this.EntityId != null &&
                    this.EntityId.Equals(other.EntityId)
                ) &&
                (
                    this.Name == other.Name ||
                    this.Name != null &&
                    this.Name.Equals(other.Name)
                ) &&
                (
                    this.Contact == other.Contact ||
                    this.Contact != null &&
                    this.Contact.SequenceEqual(other.Contact)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                if (this.EntityId != null)
                    hash = hash * 59 + this.EntityId.GetHashCode();
                if (this.Name != null)
                    hash = hash * 59 + this.Name.GetHashCode();
                if (this.Contact != null)
                    hash = hash * 59 + this.Contact.GetHashCode();
                return hash;
            }
        }

        #region Operators

        public static bool operator ==(Party left, Party right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Party left, Party right)
        {
            return !Equals(left, right);
        }

        #endregion Operators

    }
}