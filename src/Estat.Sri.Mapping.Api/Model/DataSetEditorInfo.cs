﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Mapping.Api.Model
{
    public class DataSetEditorInfo : IDataSetEditorInfo
    {
        public DataSetEditorInfo(string editorType, string name)
        {
            EditorType = editorType;
            Name = name;
        }

        public string EditorType { get; set; }
        public string Name { get; set; }
    }
}
