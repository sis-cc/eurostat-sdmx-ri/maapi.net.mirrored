// -----------------------------------------------------------------------
// <copyright file="ProxySettings.cs" company="EUROSTAT">
//   Date Created : 2019-05-02
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Model
{
    /// <summary></summary>
    public class ProxySettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProxySettings"/> class.
        /// </summary>
        public ProxySettings()
        {
            
        }

        /// <summary>Initializes a new instance of the <see cref="ProxySettings"/> class.</summary>
        /// <param name="enableProxy">if set to <c>true</c> [enable proxy].</param>
        /// <param name="authentication">if set to <c>true</c> [authentication].</param>
        /// <param name="url">The URL.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        public ProxySettings(bool enableProxy, bool authentication, string url = null, string username = null,
            string password = null)
        {
            EnableProxy = enableProxy;
            Authentication = authentication;
            URL = url;
            Username = username;
            Password = password;
        }

        /// <summary>Gets or sets a value indicating whether this <see cref="ProxySettings"/> is authentication.</summary>
        /// <value>
        ///   <c>true</c> if authentication; otherwise, <c>false</c>.</value>
        public bool Authentication { get; set; }
        /// <summary>Gets or sets a value indicating whether [enable proxy].</summary>
        /// <value>
        ///   <c>true</c> if [enable proxy]; otherwise, <c>false</c>.</value>
        public bool EnableProxy { get; set; }
        /// <summary>Gets or sets the password.</summary>
        /// <value>The password.</value>
        public string Password { get; set; }
        /// <summary>Gets or sets the URL.</summary>
        /// <value>The URL.</value>
        public string URL { get; set; }
        /// <summary>Gets or sets the username.</summary>
        /// <value>The username.</value>
        public string Username { get; set; }
    }
}