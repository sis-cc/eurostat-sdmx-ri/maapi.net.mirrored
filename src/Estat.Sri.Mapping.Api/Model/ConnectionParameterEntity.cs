﻿// -----------------------------------------------------------------------
// <copyright file="ConnectionParameterEntity.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// The connection parameter entity.
    /// </summary>
    public class ConnectionParameterEntity : IConnectionParameterEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionParameterEntity"/> class.
        /// </summary>
        public ConnectionParameterEntity()
        {
            AllowedValues = new List<string>();
            Advanced = true;
            DataType = ParameterType.String;
            Required = false;
            AllowedSubTypes = new HashSet<string>();
        }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public ParameterType DataType { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public object Value { get; set; }

        /// <summary>
        /// Gets the allowed values.
        /// </summary>
        /// <value>
        /// The allowed values.
        /// </value>
        public IList<string> AllowedValues { get; }

        /// <summary>
        /// Gets or sets the default value.
        /// </summary>
        /// <value>
        /// The default value.
        /// </value>
        public object DefaultValue { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IConnectionParameterEntity" /> is required.
        /// </summary>
        /// <value>
        ///   <c>true</c> if required; otherwise, <c>false</c>.
        /// </value>
        public bool Required { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IConnectionParameterEntity" /> is advanced.
        /// </summary>
        /// <value>
        ///   <c>true</c> if advanced; otherwise, <c>false</c>.
        /// </value>
        public bool Advanced { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is for a specific sub type.
        /// </summary>
        /// <value>
        /// <c>true</c> if this setting is a for specific sub type; otherwise, <c>false</c>.
        /// </value>
        public bool ForSpecificSubType => this.AllowedSubTypes.Count == 0;

        /// <summary>
        /// Gets the allowed sub types.
        /// </summary>
        /// <value>
        /// The allowed sub types.
        /// </value>
        public ISet<string> AllowedSubTypes { get; }
    }
}