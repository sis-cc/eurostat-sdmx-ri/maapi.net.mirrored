// -----------------------------------------------------------------------
// <copyright file="ValidityBaseEntity.cs" company="EUROSTAT">
//   Date Created : 2017-09-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Org.Sdmxsource.Sdmx.Util.Date;

    /// <summary>
    /// The normalized Last Updated entity
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Model.Entity" />
    public abstract class ValidityBaseEntity : Entity, IMappingEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidityBaseEntity"/> class.
        /// </summary>
        protected ValidityBaseEntity(EntityType entityType) : base(entityType)
        {
        }

        /// <summary>
        /// Gets or sets the mapping constant
        /// </summary>
        /// <value>
        /// The constant.
        /// </value>
        public DateTime? Constant { get; set; }

        /// <summary>
        /// Gets or sets the column.
        /// </summary>
        /// <value>
        /// The column.
        /// </value>
        public DataSetColumnEntity Column { get; set; }

        /// <inheritdoc/>
        public string ConstantValue { get => Constant.HasValue ? DateUtil.FormatDateUTC(Constant.Value) : null; set => Constant = value ==null ? (DateTime?)null : DateUtil.FormatDate(value); }
        /// <inheritdoc/>
        public string DefaultValue { get => null; set => throw new NotImplementedException(); }

        /// <inheritdoc/>
        public abstract ComponentMappingType MappingType { get; }

        /// <inheritdoc/>
        public IList<DataSetColumnEntity> GetColumns()
        {
            if (Column != null)
            {
                return new[] { Column };
            }

            return new DataSetColumnEntity[0];
        }

        /// <inheritdoc/>
        public bool HasTranscoding()
        {
            return false;
        }
    }
}