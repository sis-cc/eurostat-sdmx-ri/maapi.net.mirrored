using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using Estat.Sri.Mapping.Api.Builder;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Factory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Util;

namespace Estat.Sri.Mapping.Api.Model
{
    public class EntityStreamingReader : IEntityStreamingReader
    {
        private readonly Action<IEntity, JsonWriter, string> _writeLinks;
        private readonly Action<IEntity, JsonWriter> _writeDetails;
        private readonly JsonReader reader;
        private readonly TextReader textReader;
        private bool disposed = false;
        private string sid = string.Empty;
        private string _firstLevel= string.Empty;
        private string _secondLevel = string.Empty;
        private EntityType currentEntityType;
        private IEntity _currentEntity;
        private readonly EntityTypeBuilder _entityBuilder;
        private readonly EntityMapper _entityMapper;
        public IList<IEntity> _currentEntities;
        private readonly EntityType _filteredEntity;

        public EntityStreamingReader(string sid, Stream request, EntityType entityType)
        {
            textReader = new StreamReader(request, new UTF8Encoding(false));

            this.reader = new JsonTextReader(textReader);
            reader.DateParseHandling = DateParseHandling.DateTimeOffset;
            this.sid = sid;

            _entityBuilder = new EntityTypeBuilder();
            _entityMapper = new EntityMapper();
            _filteredEntity = entityType;
        }

        public EntityType CurrentEntityType => this.currentEntityType;
        
        public EntityWithResult CurrentEntity => new EntityWithResult() { Entity = this._currentEntity, Status = StatusType.Success };

        public void Close()
        {
           if (reader != null)
                {
                    reader.Close();
                }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool managed)
        {
            if (disposed)
            {
                return;
            }
            disposed = true;
            if (managed)
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (textReader != null)
                {
                    textReader.Dispose();
                }
            }
        }

        public IList<IEntity> GetCurrentEntities()
        {
            return _currentEntities;
        }

        public bool MoveToNextEntity()
        {
            bool value = false;
            while (reader.Read() && reader.TokenType != JsonToken.EndArray)
            {
                if (reader.Depth == 3)
                {
                    SetCurrentEntity();
                    value = true;
                    break;
                }
            }
            return value;
        }

        private void SetCurrentEntity()
        {
            //while (reader.Read())
            //{
                _currentEntity = _entityMapper.CastToObject(_secondLevel, currentEntityType, reader);
            if (_currentEntity != null)
            {
                _currentEntity.StoreId = sid;
            }
            //}
        }

        public bool MoveToNextEntityTypeSection()
        {
            bool value = false;
            while (reader.Read())
            {
                if (reader.Value != null)
                {
                    if (reader.Depth == 2)
                    {
                        _secondLevel = reader.Value.ToString();
                        currentEntityType = _entityBuilder.Build(reader.Path);
                        if (_filteredEntity != EntityType.None && _filteredEntity != currentEntityType)
                        {
                            continue;
                        }
                        value = true;
                        break;
                    }
                }
            }

            return value;
        }

        public bool MoveToNextMessage()
        {
            bool value = false;
            while (reader.Read())
            {
                if (reader.Value != null)
                {
                    if(reader.Depth == 1)
                    {
                        _firstLevel = reader.Value.ToString();
                        value = true;
                        break;
                    }
                }
            }

            return value;
        }
    }
}
