using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    public class EntityWithResult
    {
        public IEntity Entity { get; set; }
        public StatusType Status { get; set; }
        public string Message { get; set; }
    }
}
