// -----------------------------------------------------------------------
// <copyright file="LastUpdatedEntity.cs" company="EUROSTAT">
//   Date Created : 2017-09-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Exceptions;

    /// <summary>
    /// The normalized Last Updated entity
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Model.Entity" />
    public class LastUpdatedEntity : ValidityBaseEntity    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LastUpdatedEntity"/> class.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">componentMapping is null</exception>
        /// <exception cref="MissingInformationException">Both Constant and Column are not set. Cannot map Last Update</exception>
        public LastUpdatedEntity() : base(EntityType.LastUpdated)
        {
        }

               /// <inheritdoc/>
        public override ComponentMappingType MappingType => ComponentMappingType.LastUpdated;
    }
}