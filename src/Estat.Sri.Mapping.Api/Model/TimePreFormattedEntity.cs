// -----------------------------------------------------------------------
// <copyright file="TimePreFormatted.cs" company="EUROSTAT">
//   Date Created : 2021-05-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// The time-pre-formatted entity.
    /// </summary>
    public class TimePreFormattedEntity : Entity
    {
        private DataSetColumnEntity _outputColumn;
        private DataSetColumnEntity _fromColumn;
        private DataSetColumnEntity _toColumn;

        /// <summary>
        /// Initializes a new instance of <see cref="TimePreFormattedEntity"/> class.
        /// </summary>
        public TimePreFormattedEntity()
            : base(EntityType.TimePreFormatted)
        {
        }

        /// <summary>
        /// Gets or sets the output column entity.
        /// </summary>
        /// <value>
        /// The <see cref="DataSetColumnEntity"/> for the output column.
        /// </value>
        public DataSetColumnEntity OutputColumn
        {
            get
            {
                return _outputColumn;
            }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._outputColumn, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.OutputColumn), value);
                }
                _outputColumn = value;
            }
        }

        /// <summary>
        /// Gets or sets the from column entity.
        /// </summary>
        /// <value>
        /// The <see cref="DataSetColumnEntity"/> for the from column.
        /// </value>
        public DataSetColumnEntity FromColumn
        {
            get
            {
                return _fromColumn;
            }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._fromColumn, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.FromColumn), value);
                }
                _fromColumn = value;
            }
        }

        /// <summary>
        /// Gets or sets the to column entity.
        /// </summary>
        /// <value>
        /// The <see cref="DataSetColumnEntity"/> for the to column.
        /// </value>
        public DataSetColumnEntity ToColumn
        {
            get
            {
                return _toColumn;
            }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._toColumn, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ToColumn), value);
                }
                _toColumn = value;
            }
        }
    }
}
