// -----------------------------------------------------------------------
// <copyright file="TranscodingEntity.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Model
{
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;

    /// <summary>
    /// The transcoding entity.
    /// </summary>
    public class TranscodingEntity : Entity
    {
        private string _expression;

        /// <summary>
        /// Initializes a new instance of the <see cref="TranscodingEntity"/> class. 
        /// </summary>
        public TranscodingEntity()
            : base(EntityType.Transcoding)
        {
        }

        /// <summary>
        /// Gets or sets the expression.
        /// </summary>
        /// <value>
        /// The expression.
        /// </value>
        public string Expression
        {
            get { return this._expression; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._expression, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.Expression), value);
                }
                this._expression = value;
            }
        }

        /// <summary>
        /// Gets or sets the script.
        /// </summary>
        public List<TranscodingScriptEntity> Script { get; set; }

        /// <summary>
        /// Gets or sets the time transcoding.
        /// </summary>
        public List<TimeTranscodingEntity> TimeTranscoding { get; set; }

        /// <summary>
        /// Gets or sets the advanced time transcoding.
        /// </summary>
        ///// TODO we can either have AdvancedTimeTranscoding or normal
        public TimeTranscodingAdvancedEntity AdvancedTimeTranscoding { get; set; }

        /// <summary>
        /// Gets the transcoding type.
        /// </summary>
        public TranscodingType TranscodingType
        {
            get
            {
                if (AdvancedTimeTranscoding != null)
                {
                    return TranscodingType.AdvancedTime;
                }

                if (HasTimeTrascoding())
                {
                    return TranscodingType.Time;
                }

                return TranscodingType.ComponentToLocalColumns;
            }
        }

        /// <summary>
        /// Determines whether <see cref="TimeTranscoding"/> is not null and not empty
        /// </summary>
        /// <returns>
        ///   <c>true</c> if there is one or more <see cref="TimeTranscoding"/>; otherwise, <c>false</c>.
        /// </returns>
        public bool HasTimeTrascoding()
        {
            return AdvancedTimeTranscoding == null && TimeTranscoding != null && TimeTranscoding.Count > 0;
        }

        /// <summary>
        /// Determines whether <see cref="Script"/> is not null and not empty
        /// </summary>
        /// <returns>
        ///   <c>true</c> if there is one or more <see cref="Script"/>; otherwise, <c>false</c>.
        /// </returns>
        public bool HasScript()
        {
            return Script != null && Script.Count > 0;
        }

        /// <summary>
        /// Creates a new shallow copy of this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>
        /// The <see cref="Entity"/> based instance. Note that <see cref="Entity.EntityId"/> and <see cref="Entity.ParentId"/> are set to null
        /// </returns>
        public override TEntity CreateDeepCopy<TEntity>()
        {
            var clone = base.CreateDeepCopy<TEntity>();
            var copy = AsSpecificEntity<TranscodingEntity>(clone);
            copy.Script = Script?.Select(entity => entity.CreateDeepCopy<TranscodingScriptEntity>()).ToList();
            copy.TimeTranscoding =
                TimeTranscoding?.Select(entity => entity.CreateDeepCopy<TimeTranscodingEntity>()).ToList();
            return clone;
        }
    }
}