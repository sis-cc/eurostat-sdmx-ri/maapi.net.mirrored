// -----------------------------------------------------------------------
// <copyright file="PatchDocument.cs" company="EUROSTAT">
//   Date Created : 2017-02-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace Estat.Sri.Mapping.Api.Model
{
    /// <summary>
    /// A JSONPatch document as defined by RFC 6902
    /// </summary>
    public class PatchDocument : IEquatable<PatchDocument>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PatchDocument" /> class.
        /// </summary>
        /// <param name="Op">The operation to be performed (required).</param>
        /// <param name="Path">A JSON-Pointer (required).</param>
        /// <param name="Value">The value to be used within the operations..</param>
        /// <param name="From">A string containing a JSON Pointer value..</param>
        public PatchDocument(string Op = null, string Path = null, Object Value = null, string From = null)
        {
            // to ensure "Op" is required (not null)
            if (Op == null)
            {
                throw new InvalidDataException("Op is a required property for PatchDocument and cannot be null");
            }
            this.Op = Op;

            // to ensure "Path" is required (not null)
            if (Path == null)
            {
                throw new InvalidDataException("Path is a required property for PatchDocument and cannot be null");
            }
            this.Path = Path;
            this.Value = Value;
            this.From = From;
        }

        /// <summary>
        /// A string containing a JSON Pointer value.
        /// </summary>
        /// <value>A string containing a JSON Pointer value.</value>
        public string From { get; set; }

        /// <summary>
        /// The operation to be performed
        /// </summary>
        /// <value>The operation to be performed</value>
        public string Op { get; set; }

        /// <summary>
        /// A JSON-Pointer
        /// </summary>
        /// <value>A JSON-Pointer</value>
        public string Path { get; set; }

        /// <summary>
        /// The value to be used within the operations.
        /// </summary>
        /// <value>The value to be used within the operations.</value>
        public Object Value { get; set; }

        public static bool operator ==(PatchDocument left, PatchDocument right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PatchDocument left, PatchDocument right)
        {
            return !Equals(left, right);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != GetType())
            {
                return false;
            }
            return Equals((PatchDocument) obj);
        }

        /// <summary>
        /// Returns true if PatchDocument instances are equal
        /// </summary>
        /// <param name="other">Instance of PatchDocument to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(PatchDocument other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return
                (
                    this.Op == other.Op ||
                    this.Op != null &&
                    this.Op.Equals(other.Op)
                    ) &&
                (
                    this.Path == other.Path ||
                    this.Path != null &&
                    this.Path.Equals(other.Path)
                    ) &&
                (
                    this.Value == other.Value ||
                    this.Value != null &&
                    this.Value.Equals(other.Value)
                    ) &&
                (
                    this.From == other.From ||
                    this.From != null &&
                    this.From.Equals(other.From)
                    );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                var hash = 41;

                // Suitable nullity checks etc, of course :)
                if (this.Op != null)
                {
                    hash = hash*59 + this.Op.GetHashCode();
                }
                if (this.Path != null)
                {
                    hash = hash*59 + this.Path.GetHashCode();
                }
                if (this.Value != null)
                {
                    hash = hash*59 + this.Value.GetHashCode();
                }
                if (this.From != null)
                {
                    hash = hash*59 + this.From.GetHashCode();
                }
                return hash;
            }
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class PatchDocument {\n");
            sb.Append("  Op: ").Append(Op).Append("\n");
            sb.Append("  Path: ").Append(Path).Append("\n");
            sb.Append("  Value: ").Append(Value).Append("\n");
            sb.Append("  From: ").Append(From).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }


        public bool HasPath(string expectedPath)
        {
            return Regex.IsMatch(this.Path, "\\b"+expectedPath+"\\b", RegexOptions.IgnoreCase);
        }
    }
}