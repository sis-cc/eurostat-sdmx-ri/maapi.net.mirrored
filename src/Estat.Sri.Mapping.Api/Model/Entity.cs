// -----------------------------------------------------------------------
// <copyright file="Entity.cs" company="EUROSTAT">
//   Date Created : 2017-02-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System;

    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// The base entity class
    /// </summary>
    public abstract class Entity : IEntity
    {
        private string _parentId;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        /// <param name="typeOfEntity">
        /// The type of entity.
        /// </param>
        protected Entity(EntityType typeOfEntity)
        {
            this.TypeOfEntity = typeOfEntity;
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// In some stores this could be a number but in order to support
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string EntityId { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Gets or sets the parent identifier.
        /// </summary>
        /// <value>
        /// The parent identifier.
        /// </value>
        public string ParentId {
            get { return _parentId; }
            set
            {
                //if (this.ShouldCreateUpdateDocument(this._parentId, value))
                //{
                //    this.PatchRequest.CreateUpdateDocument(nameof(this.ParentId), value);
                //}
                _parentId = value;
            }
        }
       

        /// <summary>
        /// Gets or sets the Mapping Assistant store identifier.
        /// </summary>
        /// <value>
        /// The store identifier.
        /// </value>
        public string StoreId { get; set; }

        /// <summary>
        /// Gets the type of entity.
        /// </summary>
        /// <value>
        /// The type of entity.
        /// </value>
        public EntityType TypeOfEntity { get; }

        /// <summary>
        /// Gets or sets the owner.
        /// </summary>
        /// <value>
        /// The owner.
        /// </value>
        public string Owner
        {
            get; set;
        }


        /// <summary>
        /// Creates a new shallow copy of this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>
        /// The <see cref="Entity"/> based instance. Note that <see cref="EntityId"/> and <see cref="ParentId"/> are set to null
        /// </returns>
        public TEntity CreateShallowCopy<TEntity>() where TEntity : IEntity
        {
            var memberwiseClone = (TEntity)this.MemberwiseClone();
            return memberwiseClone;
        }

        /// <summary>
        /// Creates a new shallow copy of this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>
        /// The <see cref="Entity"/> based instance. Note that <see cref="EntityId"/> and <see cref="ParentId"/> are set to null
        /// </returns>
        public virtual TEntity CreateDeepCopy<TEntity>() where TEntity : IEntity
        {
            var memberwiseClone = CreateShallowCopy<TEntity>();
            ValidateTypeParam<TEntity>();

            memberwiseClone.EntityId = null;
            memberwiseClone.ParentId = null;
            return memberwiseClone;
        }

        /// <summary>
        /// Returns it as a specific entity.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <exception cref="System.ArgumentException">
        /// Cannot convert provided type <typeparamref name="TEntity"/> to this instance
        /// </exception>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        protected TEntity AsSpecificEntity<TEntity>(IEntity entity) where TEntity : Entity
        {
            var specificEntity = entity as TEntity;
            if (specificEntity == null)
            {
                throw new ArgumentException($"Cannot convert provided type {typeof(TEntity)} to this {GetType()}");
            }

            return specificEntity;
        }

        /// <summary>
        /// Validates the type parameter.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <exception cref="System.ArgumentException">Cannot convert provided type <typeparamref name="TEntity"/> to this instance</exception>
        private void ValidateTypeParam<TEntity>() where TEntity : IEntity
        {
            if (!(this is TEntity))
            {
                throw new ArgumentException($"Cannot convert provided type {typeof(TEntity)} to this {GetType()}");
            }
        }

        /// <summary>
        /// Gets the patch request.
        /// </summary>
        /// <value>
        /// The patch request.
        /// </value>
        public PatchRequest PatchRequest { get; } = new PatchRequest();

        /// <summary>
        /// Shoulds the create update document.
        /// </summary>
        /// <param name="oldValue">The old value.</param>
        /// <param name="newValue">The new value.</param>
        /// <returns></returns>
        public bool ShouldCreateUpdateDocument(object oldValue, object newValue)
        {
            return oldValue != newValue;
        }
    }
}