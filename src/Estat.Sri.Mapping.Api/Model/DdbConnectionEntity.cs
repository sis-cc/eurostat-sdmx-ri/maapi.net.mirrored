using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    /// <summary>
    /// DdbConnectionEntity
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Model.Entity" />
    public class DdbConnectionEntity : SimpleNameableEntity
    {
        private string _adoConnString;
        private bool _isEncrypted;
        private string _dbType;
        private string _jdbcConnString;
        private string _password;
        private string _dbUser;
        private string _properties;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public DdbConnectionEntity()
            : base(EntityType.DdbConnectionSettings)
        {
        }

        /// <summary>
        /// Gets or sets the ADO connection string.
        /// </summary>
        /// <value>
        /// The ADO connection string.
        /// </value>
        public string AdoConnString
        {
            get { return this._adoConnString; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._adoConnString, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.AdoConnString), value);
                }
                this._adoConnString = value;
            }
        }
        /// <summary>
        /// Gets or sets the IsEncrypted.
        /// </summary>
        /// <value>
        /// The IsEncrypted.
        /// </value>
        public bool IsEncrypted
        {
            get { return this._isEncrypted; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._isEncrypted, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.IsEncrypted), value);
                }
                this._isEncrypted = value;
            }
        }
        /// <summary>
        /// Gets or sets the type of the database.
        /// </summary>
        /// <value>
        /// The type of the database.
        /// </value>
        public string DbType
        {
            get { return this._dbType; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._dbType, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.DbType), value);
                }
                this._dbType = value;
            }
        }
        /// <summary>
        /// Gets or sets the JDBC connection string.
        /// </summary>
        /// <value>
        /// The JDBC connection string.
        /// </value>
        public string JdbcConnString
        {
            get { return this._jdbcConnString; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._jdbcConnString, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.JdbcConnString), value);
                }
                this._jdbcConnString = value;
            }
        }
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password
        {
            get { return this._password; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._password, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.Password), value);
                }
                this._password = value;
            }
        }
        /// Gets or sets the database user.
        /// </summary>
        /// <value>
        /// The database user.
        /// </value>
        public string DbUser
        {
            get { return this._dbUser; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._dbUser, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.DbUser), value);
                }
                this._dbUser = value;
            }
        }


        /// <summary>Gets or sets the properties.</summary>
        /// <value>The properties.</value>
        public string Properties
        {
            get => this._properties;
            set
            {
                if (this.ShouldCreateUpdateDocument(this._properties, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.Properties), value);
                }
                this._properties = value;
            }
        }
    }
}