// -----------------------------------------------------------------------
// <copyright file="ComponentMappingContainer.cs" company="EUROSTAT">
//   Date Created : 2017-09-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

namespace Estat.Sri.Mapping.Api.Model
{
    using System;
    using System.Collections.Generic;
    using Estat.Sri.Mapping.Api.Builder;

    /// <summary>
    /// The component mapping container.
    /// </summary>
    public class ComponentMappingContainer : IComponentMappingContainer
    {
        /// <summary>
        /// The _component mapping builders.
        /// </summary>
        private readonly IDictionary<string, IComponentMappingBuilder> _componentMappingBuilders =
            new Dictionary<string, IComponentMappingBuilder>(StringComparer.Ordinal);

        /// <summary>
        /// The _component mappings.
        /// </summary>
        private readonly IDictionary<string, ComponentMappingEntity> _componentMappings =
            new Dictionary<string, ComponentMappingEntity>(StringComparer.Ordinal);

        /// <summary>
        /// The _last update mapping.
        /// </summary>
        private LastUpdatedEntity _lastUpdateMapping;

        /// <summary>
        /// The _last update mapping builder.
        /// </summary>
        private ILastUpdateMappingBuilder _lastUpdateMappingBuilder;

        /// <summary>
        /// The _time dimension builder.
        /// </summary>
        private ITimeDimensionFrequencyMappingBuilder _timeDimensionBuilder;

        /// <summary>
        /// The _time dimension mapping.
        /// </summary>
        private TimeDimensionMappingEntity _timeDimensionMapping;

        /// <summary>
        /// The _update status mapping.
        /// </summary>
        private UpdateStatusEntity _updateStatusMapping;

        /// <summary>
        /// The _update status mapping builder.
        /// </summary>
        private IUpdateStatusMappingBuilder _updateStatusMappingBuilder;

        
        /// <summary>
        /// Gets the component mapping builders.
        /// </summary>
        /// <value>
        /// The component mapping builders.
        /// </value>
        public IDictionary<string, IComponentMappingBuilder> ComponentMappingBuilders
        {
            get { return _componentMappingBuilders; }
        }

        /// <summary>
        /// Gets the time dimension builder.
        /// </summary>
        /// <value>
        /// The time dimension builder.
        /// </value>
        public ITimeDimensionFrequencyMappingBuilder TimeDimensionBuilder
        {
            get { return _timeDimensionBuilder; }
        }

        /// <summary>
        /// Gets the component mappings.
        /// </summary>
        /// <value>
        /// The component mappings.
        /// </value>
        public IDictionary<string, ComponentMappingEntity> ComponentMappings
        {
            get { return _componentMappings; }
        }

        /// <summary>
        /// Gets the time dimension mapping.
        /// </summary>
        /// <value>
        /// The time dimension mapping.
        /// </value>
        public TimeDimensionMappingEntity TimeDimensionMapping
        {
            get { return _timeDimensionMapping; }
        }

        /// <summary>
        /// Gets the last update mapping.
        /// </summary>
        /// <value>
        /// The last update mapping.
        /// </value>
        public LastUpdatedEntity LastUpdateMapping
        {
            get { return _lastUpdateMapping; }
        }

        /// <summary>
        /// Gets the update status mapping.
        /// </summary>
        /// <value>
        /// The update status mapping.
        /// </value>
        public UpdateStatusEntity UpdateStatusMapping
        {
            get { return _updateStatusMapping; }
        }

        /// <summary>
        /// Gets the last update mapping builder.
        /// </summary>
        /// <value>
        /// The last update mapping builder.
        /// </value>
        public ILastUpdateMappingBuilder LastUpdateMappingBuilder
        {
            get { return _lastUpdateMappingBuilder; }
        }

        /// <summary>
        /// Gets the update status mapping builder.
        /// </summary>
        /// <value>
        /// The update status mapping builder.
        /// </value>
        public IUpdateStatusMappingBuilder UpdateStatusMappingBuilder
        {
            get { return _updateStatusMappingBuilder; }
        }

        /// <summary>
        /// Gets the valid to mapping builder.
        /// </summary>
        /// <value>
        /// The valid to mapping builder.
        /// </value>
        public IValidToMappingBuilder ValidToMappingBuilder { get; set; }

        /// <summary>
        /// Gets the dataset.
        /// </summary>
        /// <value>
        /// The dataset.
        /// </value>
        public DatasetEntity Dataset { get; set; }

        /// <summary>
        /// Gets or sets the metadata dataset.
        /// </summary>
        /// <value>
        /// The metadata dataset.
        /// </value>
        public DatasetEntity MetadataDataset { get; set; }

        /// <summary>
        /// Gets the dissemination connection.
        /// </summary>
        /// <value>
        /// The dissemination connection.
        /// </value>
        public DdbConnectionEntity DisseminationConnection { get; set; }

        /// <summary>
        /// Gets or sets the valid to mapping.
        /// </summary>
        /// <value>
        /// The valid to mapping.
        /// </value>
        public ValidToMappingEntity ValidToMapping { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance has historic mappings.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has historic mappings; otherwise, <c>false</c>.
        /// </value>
        public bool HasHistoricMappings => LastUpdateMapping != null && ValidToMapping != null;

        /// <summary>
        /// Adds the specified component mapping entity.
        /// </summary>
        /// <param name="componentMappingEntity">
        /// The component mapping entity.
        /// </param>
        /// <param name="componentMappingBuilder">
        /// The component mapping builder.
        /// </param>
        public void Add(ComponentMappingEntity componentMappingEntity, IComponentMappingBuilder componentMappingBuilder)
        {
            _componentMappingBuilders.Add(componentMappingEntity.Component.ObjectId, componentMappingBuilder);
            _componentMappings.Add(componentMappingEntity.Component.ObjectId, componentMappingEntity);
        }

        /// <summary>
        /// Adds the specified metadata mapping entity.
        /// </summary>
        /// <param name="componentMappingEntity">
        /// The component mapping entity.
        /// </param>
        /// <param name="componentMappingBuilder">
        /// The component mapping builder.
        /// </param>
        public void AddMetadata(ComponentMappingEntity componentMappingEntity, IComponentMappingBuilder componentMappingBuilder)
        {
            _componentMappingBuilders.Add(componentMappingEntity.MetadataAttributeSdmxId, componentMappingBuilder);
            _componentMappings.Add(componentMappingEntity.MetadataAttributeSdmxId, componentMappingEntity);
        }

        /// <summary>
        /// Sets the time dimension.
        /// </summary>
        /// <param name="timeDimensionMappingEntity">
        /// The time dimension mapping entity.
        /// </param>
        /// <param name="timeDimensionFrequencyMappingBuilder">
        /// The time dimension frequency mapping builder.
        /// </param>
        public void SetTimeDimension(
            TimeDimensionMappingEntity timeDimensionMappingEntity,
            ITimeDimensionFrequencyMappingBuilder timeDimensionFrequencyMappingBuilder)
        {
            _timeDimensionBuilder = timeDimensionFrequencyMappingBuilder;
            _timeDimensionMapping = timeDimensionMappingEntity;
        }

        /// <summary>
        /// Sets the last update.
        /// </summary>
        /// <param name="lastUpdateMappingEntity">
        /// The last update mapping entity.
        /// </param>
        /// <param name="lastUpdateMappingBuilder">
        /// The last update mapping builder.
        /// </param>
        public void SetLastUpdate(
            LastUpdatedEntity lastUpdateMappingEntity,
            ILastUpdateMappingBuilder lastUpdateMappingBuilder)
        {
            _lastUpdateMappingBuilder = lastUpdateMappingBuilder;
            _lastUpdateMapping = lastUpdateMappingEntity;
        }

        /// <summary>
        /// Sets the update status.
        /// </summary>
        /// <param name="updateMappingEntity">
        /// The update mapping entity.
        /// </param>
        /// <param name="updateStatusMappingBuilder">
        /// The update status mapping builder.
        /// </param>
        public void SetUpdateStatus(
            UpdateStatusEntity updateMappingEntity,
            IUpdateStatusMappingBuilder updateStatusMappingBuilder)
        {
            _updateStatusMappingBuilder = updateStatusMappingBuilder;
            _updateStatusMapping = updateMappingEntity;
        }
    }
}