using System;
using System.Collections.Generic;
using System.Text;

namespace Estat.Sri.Mapping.Api.Model
{
    public class MappingStoreConnectionEntity : ConnectionEntity
    {
        public enum MsdbStatus
        {
            OK,
            NeedsUpgrade,
            ErrorsFound,
            NotInitialized
        }

        public MappingStoreConnectionEntity() : base(Constant.EntityType.MappingStore)
        {
        }

        public string StoreType { get; set; }
        public string DbName { get; set; }

        public MsdbStatus Status { get; set; }

        public string Version { get; set; }

        public string AvailableUpgrade { get; set; }
    }
}
