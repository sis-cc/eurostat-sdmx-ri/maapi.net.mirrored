// -----------------------------------------------------------------------
// <copyright file="DataSetColumnEntity.cs" company="EUROSTAT">
//   Date Created : 2017-03-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Newtonsoft.Json;

namespace Estat.Sri.Mapping.Api.Model
{
    public class DataSetColumnEntity : SimpleNameableEntity
    {
        private DateTime? _lastRetrieval;
        private ColumnMappingStatus _isMapped;
        private bool _isTimePreFormatted;
        


        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public DataSetColumnEntity()
            : base(EntityType.DataSetColumn)
        {
        }

        /// <summary>
        /// Gets or sets the mapping status of a dataset column. In other words if the dataset column is used in a mapping set and how.
        /// </summary>
        public ColumnMappingStatus IsMapped
        {
            get
            {
                return _isMapped;
            }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._isMapped, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.IsMapped), value);
                }

                _isMapped = value;
            }
        }

        /// <summary>
        /// Gets or sets the last retrieval.
        /// </summary>
        /// <value>
        /// The last retrieval.
        /// </value>
        public DateTime? LastRetrieval
        {
            get { return this._lastRetrieval; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._lastRetrieval, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.LastRetrieval), value);
                }
                this._lastRetrieval = value;
            }
        }

        /// <summary>
        /// Mediator property to help set <see cref="IsMapped"/> property.
        /// </summary>
        public bool IsTimePreFormatted
        {
            get { return this._isTimePreFormatted; }
            set
            {
                this._isTimePreFormatted = value;
                if (IsMapped == ColumnMappingStatus.No && value)
                {
                    IsMapped = ColumnMappingStatus.Yes;
                }
            }
        }
    }
}