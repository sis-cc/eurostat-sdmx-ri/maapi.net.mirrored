using System;

namespace Estat.Sri.Mapping.Api.Model
{
    /// <summary>
    /// UserActionEntity
    /// </summary>
    public class UserActionEntity:Entity
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Id { get; set; }
        /// <summary>
        /// Gets or sets the type of the entity.
        /// </summary>
        /// <value>
        /// The type of the entity.
        /// </value>
        public string EntityType { get; set; }

        /// <summary>
        /// Gets or sets the type of the operation.
        /// </summary>
        /// <value>
        /// The type of the operation.
        /// </value>
        public string OperationType { get; set; }
        /// <summary>
        /// Gets the action when.
        /// </summary>
        /// <value>
        /// The action when.
        /// </value>
        public DateTime ActionWhen { get; set; } 
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }
        
        /// <summary>
        /// Gets or sets the name of the entity.
        /// </summary>
        /// <value>
        /// The name of the entity.
        /// </value>
        public string EntityName { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public UserActionEntity()
            : base(Api.Constant.EntityType.UserAction)
        {
        }
    }
}