// -----------------------------------------------------------------------
// <copyright file="IMappingEntity.cs" company="EUROSTAT">
//   Date Created : 2022-08-03
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    public interface IMappingEntity
    {
        /// <summary>
        /// Gets or sets the constant.
        /// </summary>
        /// <value>
        /// The constant.
        /// </value>
        string ConstantValue { get; set; }

        /// <summary>
        /// Gets or sets the default value.
        /// </summary>
        /// <value>
        /// The default value.
        /// </value>
        string DefaultValue { get; set; }

        /// <summary>
        /// Gets or sets the type. TODO change to Enum
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        ComponentMappingType MappingType { get; }

        /// <summary>
        /// Gets a value indicating if transcoding is used
        /// </summary>
        /// <returns>True if transcoding is used; otherwise false</returns>
        bool HasTranscoding();
        
        /// <summary>
        /// Gets the list of columns used in the mapping
        /// </summary>
        /// <returns>The columns used</returns>
        IList<DataSetColumnEntity> GetColumns();
    }
}