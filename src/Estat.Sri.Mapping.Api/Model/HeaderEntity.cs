// -----------------------------------------------------------------------
// <copyright file="HeaderEntity.cs" company="EUROSTAT">
//   Date Created : 2017-05-02
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    using System;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;

    public class HeaderEntity:SimpleNameableEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public HeaderEntity()
            : base(EntityType.Header)
        {
        }

        /// <summary>
        /// The SdmxSource model that contains the header
        /// </summary>
        public IHeader SdmxHeader { get; set; }

        /// <summary>
        /// The DataSet Agency ID
        /// </summary>
        /// <value>The DataSet Agency ID</value>
        [Obsolete("Use SdmxHeader.GetAdditionalProperties()")]
        public string DataSetAgencyId { get; set; }

        /// <summary>
        /// The transmission name
        /// </summary>
        /// <value>The transmission name</value>
        [Obsolete("Use SdmxHeader. This is not supported by MSDB")]
        public string TransmissionName { get; set; }

        /// <summary>
        /// Gets or Sets Receiver
        /// </summary>
        [Optional]
        [Obsolete("Use SdmxHeader.")]
        public List<Party> Receiver { get; set; }

        /// <summary>
        /// Gets or Sets Sender
        /// </summary>
        [Optional]
        [Obsolete("Use SdmxHeader.")]
        public Party Sender { get; set; }

        /// <summary>
        /// Gets or Sets Source
        /// </summary>
        [Obsolete("Use SdmxHeader.")]
        public string Source { get; set; }

        /// <summary>
        /// Indicates whether the message is for test purposes or not. False for normal messages
        /// </summary>
        /// <value>Indicates whether the message is for test purposes or not. False for normal messages</value>
        [Obsolete("Use SdmxHeader.")]
        public bool? Test { get; set; }

        /// <summary>
        /// Gets or sets the dataflow identifier.
        /// </summary>
        /// <value>
        /// The dataflow identifier.
        /// </value>
        [Obsolete("Use ParentId.")]
        public string DataflowId { get; set; }
    }
}