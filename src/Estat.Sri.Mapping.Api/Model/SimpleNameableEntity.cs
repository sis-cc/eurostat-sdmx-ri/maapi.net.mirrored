// -----------------------------------------------------------------------
// <copyright file="NameableEntity.cs" company="EUROSTAT">
//   Date Created : 2017-04-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using Estat.Sri.Mapping.Api.Constant;

    /// <inheritdoc />
    /// <summary>
    /// The nameable entity.
    /// </summary>
    public class SimpleNameableEntity : IdentifiableEntity, ISimpleNameableEntity
    {
        private string _name;
        private string _description;

        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Estat.Sri.Mapping.Api.Model.SimpleNameableEntity" /> class.
        /// </summary>
        /// <param name="typeOfEntity">
        /// The type of entity.
        /// </param>
        public SimpleNameableEntity(EntityType typeOfEntity)
            : base(typeOfEntity)
        {
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return this._name; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._name, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.Name), value);
                }
                this._name = value;
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description
        {
            get { return this._description; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._description, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.Description), value);
                }
                this._description = value;
            }
        }
    }
}