﻿// -----------------------------------------------------------------------
// <copyright file="IIdentifiableEntity.cs" company="EUROSTAT">
//   Date Created : 2017-04-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    /// <summary>
    /// An entity that has a store independent identifier
    /// </summary>
    public interface IIdentifiableEntity : IEntity
    {
        /// <summary>
        /// Gets or sets the object identifier. This could store the SDMX id of a Component or the value of a local code.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        string ObjectId { get; set; }
    }
}