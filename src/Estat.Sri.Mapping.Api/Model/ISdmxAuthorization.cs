// -----------------------------------------------------------------------
// <copyright file="ISdmxAuthorization.cs" company="EUROSTAT">
//   Date Created : 2020-9-9
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace Estat.Sri.Mapping.Api.Model
{
    /// <summary>
    /// Glue interface between NSIWS and retriver/persist
    /// Probably there is a better way
    /// </summary>
    public interface ISdmxAuthorization
    {
        bool CanInsert(IMaintainableObject maintainableObject);
        bool CanUpdate(IMaintainableObject maintainableObject);
        bool CanDelete(IMaintainableObject maintainableObject);
        bool CanRead(IStructureReference structureReference);
        bool CanReadData(IStructureReference structureReference, bool isPit);

        ISet<IStructureReference> GetAllowedStructureReferences();
    }
}
