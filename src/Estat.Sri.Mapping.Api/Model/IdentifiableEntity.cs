// -----------------------------------------------------------------------
// <copyright file="IdentifiableEntity.cs" company="EUROSTAT">
//   Date Created : 2017-04-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// An entity with <see cref="ObjectId"/>
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Model.Entity" />
    /// <seealso cref="Estat.Sri.Mapping.Api.Model.IIdentifiableEntity" />
    public class IdentifiableEntity : Entity, IIdentifiableEntity
    {
        private string _objectId;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public IdentifiableEntity()
            : base(EntityType.Sdmx)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentifiableEntity"/> class.
        /// </summary>
        /// <param name="typeOfEntity">
        /// The type of entity.
        /// </param>
        public IdentifiableEntity(EntityType typeOfEntity)
            : base(typeOfEntity)
        {
        }

        /// <summary>
        /// Gets or sets the object id.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        public string ObjectId {
            get { return this._objectId; }
            set
            {
                if (!string.IsNullOrWhiteSpace(this._objectId) && this._objectId != value)
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ObjectId), value);
                }
                this._objectId = value;
            } 
        }
    }
}