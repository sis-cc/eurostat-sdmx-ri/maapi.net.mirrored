// -----------------------------------------------------------------------
// <copyright file="PatchRequest.cs" company="EUROSTAT">
//   Date Created : 2017-02-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Estat.Sri.Mapping.Api.Model
{
    public class PatchRequest : List<PatchDocument>, IEquatable<PatchRequest>
    {
        public static bool operator ==(PatchRequest left, PatchRequest right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PatchRequest left, PatchRequest right)
        {
            return !Equals(left, right);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != GetType())
            {
                return false;
            }
            return Equals((PatchRequest) obj);
        }

        /// <summary>
        /// Returns true if PatchRequest instances are equal
        /// </summary>
        /// <param name="other">Instance of PatchRequest to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(PatchRequest other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                var hash = 41;

                // Suitable nullity checks etc, of course :)
                return hash;
            }
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class PatchRequest {\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Creates the update document.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="value">The value.</param>
        public void CreateUpdateDocument(string path, object value)
        {
            var patchDocument = this.Find(item => item.Path == path);
            if (patchDocument == null)
            {
                this.Add(new PatchDocument("replace", path, value));
            }
            else
            {
                patchDocument.Value = value;
            }
        }

        /// <summary>
        /// Creates the update document for complex.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="currentValue">The current value.</param>
        /// <param name="newValue">The new value.</param>
        public void CreateUpdateDocumentForComplexIdentifiableEntity(string path, IEntity currentValue, IEntity newValue)
        {
            this.RemoveAll(item => item.Path == path);

            if (currentValue == null)
            {
                this.Add(new PatchDocument("add", path, newValue.EntityId));
                return;
            }
            if (newValue == null)
            {
                this.Add(new PatchDocument("remove", path + "/" + currentValue.EntityId));
                return;
            }

            if (currentValue.EntityId != newValue.EntityId)
            {
                this.Add(new PatchDocument("replace", path,newValue.EntityId));
            }
            
        }

        /// <summary>
        /// Creates the update document for list.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="currentValue">The current value.</param>
        /// <param name="newValue">The new value.</param>
        public void CreateUpdateDocumentForList(string path, IEntity currentValue, IEntity newValue)
        {
            if (currentValue == null)
            {
                this.Add(new PatchDocument("add", path, newValue.EntityId));
                return;
            }
            if (newValue == null)
            {
                this.Add(new PatchDocument("remove", path + "/" + currentValue.EntityId));
            }
        }

        public void CreateUpdateDocumentForList(string path, object currentValue, object newValue)
        {
            if (currentValue == null)
            {
                this.Add(new PatchDocument("add", path, newValue));
                return;
            }
            if (newValue == null)
            {
                this.Add(new PatchDocument("remove", path + "/" + currentValue,currentValue));
            }
        }
    }
}