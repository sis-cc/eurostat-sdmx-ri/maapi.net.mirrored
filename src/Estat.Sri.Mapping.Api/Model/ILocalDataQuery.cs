// -----------------------------------------------------------------------
// <copyright file="ILocalDataQuery.cs" company="EUROSTAT">
//   Date Created : 2017-04-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System.Collections.Generic;

    /// <summary>
    /// A query for querying local codes and data using either a table or a query.
    /// </summary>
    public interface ILocalDataQuery
    {
        /// <summary>
        /// Gets or sets the mapping store identifier.
        /// </summary>
        /// <value>
        /// The mapping store identifier.
        /// </value>
        string MappingStoreId { get; set; }

        /// <summary>
        /// Gets or sets the dissemination database settings.
        /// </summary>
        /// <value>
        /// The connection string settings.
        /// </value>
        string DisseminationDatabaseSettingEntityId { get; set; }

        /// <summary>
        /// Gets or sets the page.
        /// </summary>
        /// <value>
        /// The page.
        /// </value>
        int Page { get; set; }

        /// <summary>
        /// Gets or sets the query.
        /// </summary>
        /// <value>
        /// The query.
        /// </value>
        string Query { get; set; }

        /// <summary>
        /// Gets or sets the orderByClause.
        /// </summary>
        /// <value>
        /// 
        /// </value>
        DatasetPropertyEntity DatasetProperty { get; set; }

        /// <summary>
        /// Gets or sets the table.
        /// </summary>
        /// <value>
        /// The table.
        /// </value>
        string Table { get; set; }

        /// <summary>
        /// Gets the column names.
        /// </summary>
        /// <value>
        /// The column names.
        /// </value>
        IList<string> ColumnNames { get; }

        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        int Count { get; set; }

        /// <summary>
        /// Gets or sets a value indicating if we need an ordered query. This currently works only if there is one column.
        /// </summary>
        bool IsOrdered { get; set; }

        /// <summary>
        /// Check if there are null/empty values or greater than the allowed number of characters
        /// </summary>
        bool CheckForErrors { get; set; }
    }
}