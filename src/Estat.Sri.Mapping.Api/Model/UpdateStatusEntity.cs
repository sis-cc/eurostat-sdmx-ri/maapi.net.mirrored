// -----------------------------------------------------------------------
// <copyright file="UpdateStatusEntity.cs" company="EUROSTAT">
//   Date Created : 2017-09-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Exceptions;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The update status entity.
    /// </summary>
    public class UpdateStatusEntity : Entity, IMappingEntity
    {
        /// <summary>
        /// The transcoding
        /// </summary>
        private readonly Dictionary<string, ObservationActionEnumType> _transcoding = new Dictionary<string, ObservationActionEnumType>(StringComparer.OrdinalIgnoreCase);


        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateStatusEntity"/> class.
        /// </summary>
        /// <param name="componentMappingEntity">The component mapping entity.</param>
        /// <exception cref="System.ArgumentNullException">componentMappingEntity is null</exception>
        /// <exception cref="MissingInformationException">Both Constant and Column are not set. Cannot map Update Status</exception>
        [Obsolete]
        public UpdateStatusEntity(ComponentMappingEntity componentMappingEntity) : base(Api.Constant.EntityType.UpdateStatusMapping)
        {
            throw new NotImplementedException("Obsolete constructor");
        }

        public UpdateStatusEntity() : base(Api.Constant.EntityType.UpdateStatusMapping)
        {
        }


        /// <summary>
        ///     Gets or sets the mapping constant
        /// </summary>
        public ObservationAction Constant { get; set; }

        /// <summary>
        /// Gets or sets the column.
        /// </summary>
        /// <value>
        /// The column.
        /// </value>
        public DataSetColumnEntity Column { get; set; }

        public void Add(IEnumerable<TranscodingRuleEntity> rules)
        {
            foreach (var transcodingRuleEntity in rules)
            {
                Add(transcodingRuleEntity.DsdCodeEntity?.ObjectId ?? transcodingRuleEntity.UncodedValue, transcodingRuleEntity.LocalCodes.First().ObjectId);
            } 
        }

        /// <summary>
        /// Adds the specified SDMX action code.
        /// </summary>
        /// <param name="sdmxActionCode">The SDMX action code.</param>
        /// <param name="localCode">The local code.</param>
        public void Add(string sdmxActionCode, string localCode)
        {
            var obsAction = ObservationAction.ParseString(sdmxActionCode);
            this._transcoding.Add(localCode, obsAction);
        }
        /// <summary>
        /// Adds the specified SDMX action code.
        /// </summary>
        /// <param name="sdmxActionCode">The SDMX action code.</param>
        /// <param name="localCode">The local code.</param>
        public void Add(ObservationActionEnumType sdmxActionCode, string localCode)
        {
            this._transcoding.Add(localCode, sdmxActionCode);
        }


        /// <summary>
        /// Gets the action.
        /// </summary>
        /// <param name="localCode">
        /// The local code.
        /// </param>
        /// <returns>
        /// The <see cref="ObservationActionEnumType"/>.
        /// </returns>
        public ObservationActionEnumType GetAction(string localCode)
        {
            ObservationActionEnumType obsAction;
            if (this._transcoding.TryGetValue(localCode, out obsAction))
            {
                return obsAction;
            }

            return ObservationActionEnumType.Null;
        }

        public string InsertValue { get { return GetLocalAction(ObservationActionEnumType.Added);  } }
        public string UpdateValue { get { return GetLocalAction(ObservationActionEnumType.Updated); } }
        public string DeleteValue { get { return GetLocalAction(ObservationActionEnumType.Deleted); } }

        /// <summary>
        /// Gets the local code for action.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns>
        /// The local action.
        /// </returns>
        public string GetLocalAction(ObservationActionEnumType action)
        {
            return this.GetLocalAction(ObservationAction.GetFromEnum(action));
        }

        /// <summary>
        /// Gets the local code for action.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns>
        /// The local action.
        /// </returns>
        public string GetLocalAction(ObservationAction action)
        {
            return this._transcoding.Where(pair => pair.Value == action.EnumType)
                .Select(pair => pair.Key)
                .FirstOrDefault();
        }

        /// <summary>
        /// Gets the active local actions.
        /// </summary>
        /// <returns>The active actions (ADDED/UPDATED) that were mapped.</returns>
        public IEnumerable<string> GetActiveLocalActions()
        {
            return this._transcoding.Where(pair => pair.Value == ObservationActionEnumType.Updated || pair.Value == ObservationActionEnumType.Added).Select(pair => pair.Key);
        }

        public bool HasTranscoding()
        {
            return this._transcoding.Any();
        }

        public IList<DataSetColumnEntity> GetColumns()
        {
            if (Column == null)
            {
                return new DataSetColumnEntity[0];
            }

            return new[] { Column };
        }
/// <inheritdoc/>

        public string ConstantValue
        {
            get
            {
                if (Constant == null || Constant.EnumType == ObservationActionEnumType.Null)
                {
                    return null;
                }

                return Constant.ObsAction.ToUpperInvariant();
            }
            set
            {
                if (value == null)
                {
                    Constant = null;
                }

                Constant = ObservationAction.ParseString(value);
            }
        }
        public string DefaultValue { get; set; }

        public ComponentMappingType MappingType => ComponentMappingType.UpdatedStatus;

        public Dictionary<string, ObservationActionEnumType> Transcoding => _transcoding;
    }
}