// -----------------------------------------------------------------------
// <copyright file="TemplateMapping.cs" company="EUROSTAT">
//   Date Created : 2018-01-16
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace Estat.Sri.Mapping.Api.Model
{
    /// <inheritdoc />
    /// <summary>
    /// A representation of a mapping between a dataset column and a component
    /// </summary>
    public class TemplateMapping : SimpleNameableEntity
    {
        private string _columnDescription;
        private string _columnName;
        private string _componentId;
        private string _componentType;
        private long _connectionId;

        public TemplateMapping()
            : base(EntityType.TemplateMapping)
        {
        }

        /// <summary>
        /// Gets or sets the column description.
        /// </summary>
        /// <value>
        /// The column description.
        /// </value>
        public string ColumnDescription
        {
            get { return this._columnDescription; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._columnDescription, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ColumnDescription), value);
                }
                this._columnDescription = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the column.
        /// </summary>
        /// <value>
        /// The name of the column.
        /// </value>
        public string ColumnName
        {
            get { return this._columnName; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._columnName, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ColumnName), value);
                }
                this._columnName = value;
            }
        }

        /// <summary>
        /// Gets or sets the component identifier.
        /// </summary>
        /// <value>
        /// The component identifier.
        /// </value>
        public string ComponentId
        {
            get { return this._componentId; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._componentId, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ComponentId), value);
                }
                this._componentId = value;
            }
        }

        /// <summary>
        /// Gets or sets the type of the component.
        /// </summary>
        /// <value>
        /// The type of the component.
        /// </value>
        public string ComponentType
        {
            get { return this._componentType; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._componentType, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ComponentType), value);
                }
                this._componentType = value;
            }
        }

        /// <summary>
        /// Gets or sets the connection identifier.
        /// </summary>
        /// <value>
        /// The concept identifier.
        /// </value>
        public long ConceptId
        {
            get { return this._connectionId; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._connectionId, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ConceptId), value);
                }
                this._connectionId = value;
            }
        }

        /// <summary>
        /// Gets or sets the item scheme identifier.
        /// </summary>
        /// <value>
        /// The item scheme identifier.
        /// </value>
        public IStructureReference ItemSchemeId { get; set; }

        /// <summary>
        /// Gets or sets the time transcoding.
        /// </summary>
        /// <value>
        /// The time transcoding.
        /// </value>
        public List<TimeTranscodingEntity> TimeTranscoding { get; set; }

        /// <summary>
        /// Gets or sets the transcodings.
        /// </summary>
        /// <value>
        /// The transcodings.
        /// </value>
        public Dictionary<string, string> Transcodings { get; set; }
        public string ConceptUrn { get; set; }
    }
}