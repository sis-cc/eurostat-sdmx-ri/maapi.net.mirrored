// -----------------------------------------------------------------------
// <copyright file="TranscodingRuleMap.cs" company="EUROSTAT">
//   Date Created : 2017-09-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model.AdvancedTime;

namespace Estat.Sri.Mapping.Api.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// The transcoding rule map.
    /// </summary>
    public class TranscodingRuleMap
    {
        /// <summary>
        /// The SDMX code identifier to rules. A code can be used in one or more rules
        /// </summary>
        private readonly ILookup<string, TranscodingRuleEntity> _sdmxCodeIdToRule;

        /// <summary>
        /// The local codes to SDMX
        /// </summary>
        private readonly Dictionary<IList<string>, string> _localCodesToSdmx = new Dictionary<IList<string>, string>(StringCollectionComparer.Instance);

        /// <summary>
        /// The local code to SDMX
        /// </summary>
        private readonly Dictionary<string, string> _localCodeToSdmx = new Dictionary<string, string>(StringComparer.Ordinal);

        /// <summary>
        /// SDMX 3.0.0 supports array values, where a single local code can be transcoded to an array of sdmx codes.
        /// </summary>
        private readonly Dictionary<string, IList<string>> _localCodeToSdmxArray = new Dictionary<string, IList<string>>(StringComparer.Ordinal);

        /// <summary>
        /// Initializes a new instance of the <see cref="TranscodingRuleMap" /> class.
        /// </summary>
        /// <param name="rules">The rules.</param>
        /// <param name="mapping">The mapping.</param>
        public TranscodingRuleMap(IList<TranscodingRuleEntity> rules, ComponentMappingEntity mapping)
        {
            if (rules == null)
            {
                throw new ArgumentNullException(nameof(rules));
            }

            if (mapping == null)
            {
                throw new ArgumentNullException(nameof(mapping));
            }

            if (rules.Count == 0)
            {
                throw new ArgumentException("Argument is empty collection", nameof(rules));
            }

            OrderedColumns = mapping.GetColumns().ToArray();
            if (OrderedColumns.Count > 1)
            {
                // scalar type component (1) maps to N columns, transcoding N local codes to 1 sdmx code
                // so far mapping of 1 array component to N columns with transcoding is not supported.
                // When that changes, this code must also change
                Populate1ToNTranscodings(rules);
            }
            else if (OrderedColumns.Count == 1)
            {
                foreach (var rule in rules)
                {
                    if (mapping.Component.ValueType == Constant.ComponentValueType.Array)
                    {
                        // array type component (N) maps to 1 column, transcoding 1 local code to N sdmx array values
                        _localCodeToSdmxArray.Add(rule.LocalCodes[0].ObjectId, rule.DsdCodeEntities.Select(e => e.ObjectId).ToList());
                    }
                    else
                    {
                        // scalar type component(1) maps to 1 column, transcoding 1 local code to 1 sdmx code
                        var actualSdmxCodeOrValue = GetActualSdmxCodeOrValue(rule);
                        _localCodeToSdmx.Add(rule.LocalCodes[0].ObjectId, actualSdmxCodeOrValue);
                    }
                }               
            }

            _sdmxCodeIdToRule = rules.ToLookup(GetActualSdmxCodeOrValue, StringComparer.Ordinal);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TranscodingRuleMap"/> class.
        /// </summary>
        /// <param name="periodTranscoding">The period transcoding.</param>
        /// <exception cref="System.ArgumentNullException">periodTranscoding is null</exception>
        /// <exception cref="System.ArgumentException">Rules are null or empty collection - periodTranscoding</exception>
        public TranscodingRuleMap(PeriodTimeTranscoding periodTranscoding)
        {
            if (periodTranscoding == null)
            {
                throw new ArgumentNullException(nameof(periodTranscoding));
            }

            if (periodTranscoding.Rules == null || periodTranscoding.Rules.Count == 0)
            {
                throw new ArgumentException("Rules are null or empty collection", nameof(periodTranscoding));
            }

            var rules = periodTranscoding.Rules;

            OrderedColumns = new[] { periodTranscoding.Column };
            foreach (var rule in rules)
            {
                var actualSdmxCodeOrValue = GetActualSdmxCodeOrValue(rule);

                _localCodeToSdmx.Add(rule.LocalCodes[0].ObjectId, actualSdmxCodeOrValue);
            }

            _sdmxCodeIdToRule = rules.ToLookup(GetActualSdmxCodeOrValue, StringComparer.Ordinal);
            TimePeriodToLocalPeriod = rules.ToDictionary(GetActualSdmxCodeOrValue, v => v.LocalCodes.FirstOrDefault()?.ObjectId, StringComparer.Ordinal);
        }

        /// <summary>
        /// Initializes a new instance for Duration mapping
        /// </summary>
        /// <param name="componentMappingEntity"></param>
        public TranscodingRuleMap(DurationMappingEntity componentMappingEntity)
        {
            if (componentMappingEntity == null)
            {
                throw new ArgumentNullException(nameof(componentMappingEntity));
            }

            if (!componentMappingEntity.HasTranscoding())
            {
                throw new InvalidOperationException(
                    "Called Transcoding Rule map for Duration mapping without transcoding rules");
            }

            OrderedColumns = new[] { componentMappingEntity.Column };
            foreach (var keyValuePair in componentMappingEntity.TranscodingRules)
            {
                _localCodeToSdmx.Add(keyValuePair.Value, keyValuePair.Key);
            }

            _sdmxCodeIdToRule = componentMappingEntity
                .TranscodingRules
                .ToLookup(k => k.Key,
                    ToTranscodingRule);
        }

        private static TranscodingRuleEntity ToTranscodingRule(KeyValuePair<string, string> v)
        {
            var rule = new TranscodingRuleEntity() { UncodedValue = v.Key };
            rule.LocalCodes.Add(new LocalCodeEntity() { ObjectId = v.Value });
            return rule;
        }

        /// <summary>
        /// Gets the ordered columns.
        /// </summary>
        /// <value>The ordered columns.</value>
        public IList<DataSetColumnEntity> OrderedColumns { get;  }

        /// <summary>
        /// Gets the SDMX code to local code.
        /// </summary>
        /// <value>The SDMX code to local code.</value>
        public Dictionary<string, string> TimePeriodToLocalPeriod { get; }

        /// <summary>
        /// Gets the SDMX code (or un-coded value).
        /// </summary>
        /// <param name="localCodes">The local codes.</param>
        /// <returns>The SDMX Code (or un-coded value)</returns>
        public string GetSdmxCode(IList<string> localCodes)
        {
            if (_localCodesToSdmx.TryGetValue(localCodes, out string sdmxCodeId))
            {
                return sdmxCodeId;
            }

            return null;
        }

        /// <summary>
        /// Gets the SDMX code.
        /// </summary>
        /// <param name="localCode">The local code.</param>
        /// <returns>The SDMX Code (or un-coded value)</returns>
        public string GetSdmxCode(string localCode)
        {
            string sdmxCodeId;
            if (_localCodeToSdmx.TryGetValue(localCode, out sdmxCodeId))
            {
                return sdmxCodeId;
            }

            return null;
        }

        /// <summary>
        /// SDMX 3.0.0 support: Get the array of SDMX codes that corresponds to <paramref name="localCode"/>
        /// </summary>
        /// <param name="localCode"></param>
        /// <returns></returns>
        public IList<string> GetSdmxCodes(string localCode)
        {
            if (_localCodeToSdmxArray.TryGetValue(localCode, out IList<string> sdmxCodes))
            {
                return sdmxCodes;
            }
            return null;
        }

        /// <summary>
        /// Gets the local codes.
        /// </summary>
        /// <param name="sdmxValue">The SDMX value.</param>
        /// <returns>The list of local codes</returns>
        public IList<TranscodingRuleEntity> GetLocalCodes(string sdmxValue)
        {
            return _sdmxCodeIdToRule[sdmxValue].ToArray();
        }

        /// <summary>
        /// Gets the local code for an sdmx array value.
        /// Used in mapping 1 component to 1 column
        /// with 1 local value transcoded to N aray values
        /// </summary>
        /// <param name="sdmxArrayValue"></param>
        /// <returns></returns>
        public string GetLocalCode(string[] sdmxArrayValue)
        {
            foreach (var pair in _localCodeToSdmxArray)
            {
                if(pair.Value.SequenceEqual(sdmxArrayValue))
                {
                    return pair.Key;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the actual SDMX code or value.
        /// </summary>
        /// <param name="rule">The rule.</param>
        /// <returns>The SDMX Code id or un-coded value</returns>
        private static string GetActualSdmxCodeOrValue(TranscodingRuleEntity rule)
        {
            return rule.DsdCodeEntity?.ObjectId ?? rule.UncodedValue;
        }

        /// <summary>
        /// Populate the 1 code to N local code transcoding.
        /// </summary>
        /// <param name="rules">The rules.</param>
        private void Populate1ToNTranscodings(IList<TranscodingRuleEntity> rules)
        {
            foreach (var rule in rules)
            {
                string[] localCodeKey = new string[OrderedColumns.Count];
                var actualSdmxCodeOrValue = GetActualSdmxCodeOrValue(rule);

                for (int i = 0; i < this.OrderedColumns.Count; i++)
                {
                    var dataSetColumnEntity = this.OrderedColumns[i];
                    localCodeKey[i] =
                        rule.LocalCodes.First(entity => string.Equals(entity.ParentId, dataSetColumnEntity.Name)).ObjectId;
                }

                _localCodesToSdmx.Add(localCodeKey, actualSdmxCodeOrValue);
            }
        }
    }
}