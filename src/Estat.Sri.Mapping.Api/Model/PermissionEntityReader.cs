// -----------------------------------------------------------------------
// <copyright file="PermissionEntityReader.cs" company="EUROSTAT">
//   Date Created : 2020-1-20
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Estat.Sri.Mapping.Api.Model
{
    public class PermissionEntityReader
    {
        public void ParsePermissions(JsonReader reader, IPermissionEntity entity)
        {
            if (entity.Permissions == null)
            {
                entity.Permissions = new Dictionary<string, string>(StringComparer.Ordinal);
            }
            reader.Read();
            while (reader.Read() && reader.TokenType != JsonToken.EndObject)
            {
                var permission = GetPermission(reader);
                entity.Permissions.Add(permission.Item1, permission.Item2);
            }
        }

        private static Tuple<string, string> GetPermission(JsonReader reader)
        {
            var key = reader.Value?.ToString();
            reader.Read();
            var value = reader.Value?.ToString();
            return new Tuple<string, string>(key, value);
        }
    }
}
