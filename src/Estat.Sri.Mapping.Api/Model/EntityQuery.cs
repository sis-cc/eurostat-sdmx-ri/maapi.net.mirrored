﻿namespace Estat.Sri.Mapping.Api.Model
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Model.IEntityQuery" />
    public class EntityQuery : IEntityQuery
    {
        private readonly Dictionary<string, ICriteria<string>> _additionalCriteria = new Dictionary<string, ICriteria<string>>();

        /// <summary>
        /// Gets a new instance of an empty query.
        /// </summary>
        /// <value>
        /// The empty.
        /// </value>
        public static IEntityQuery Empty => new EntityQuery();

        public ICriteria<string> EntityId { get; set; }
        public ICriteria<string> ObjectId { get; set; }

        public ICriteria<string> ParentId { get; set; }
        public EntityType? ParentType { get; set; }

        public EntityType EntityType { get; set; }

        public ICriteria<int> Page { get; set; }

        public ICriteria<int> Limit { get; set; }

        public IReadOnlyDictionary<string, ICriteria<string>> AdditionalCriteria => new ReadOnlyDictionary<string, ICriteria<string>>(_additionalCriteria);

        public void AddAdditionalCriteria(string key, ICriteria<string> criteria)
        {
            this._additionalCriteria.Add(key, criteria);
        }

        
    }
}