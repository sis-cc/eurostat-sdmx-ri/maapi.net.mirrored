﻿// -----------------------------------------------------------------------
// <copyright file="StringCollectionComparer.cs" company="EUROSTAT">
//   Date Created : 2017-09-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The string collection comparer.
    /// </summary>
    public class StringCollectionComparer : IEqualityComparer<IList<string>>
    {
        /// <summary>
        ///     The _instance.
        /// </summary>
        private static readonly StringCollectionComparer _instance = new StringCollectionComparer();

        /// <summary>
        ///     Prevents a default instance of the <see cref="StringCollectionComparer" /> class from being created.
        /// </summary>
        private StringCollectionComparer()
        {
        }

        /// <summary>
        ///     Gets Instance.
        /// </summary>
        public static StringCollectionComparer Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     Checks if two collection, <paramref name="x" /> and <paramref name="y" /> are equal regarding their contents.
        /// </summary>
        /// <param name="x">
        ///     The x.
        /// </param>
        /// <param name="y">
        ///     The y.
        /// </param>
        /// <returns>
        ///     <c>true</c> if the collections are equal. Else <c>false</c>.
        /// </returns>
        public bool Equals(IList<string> x, IList<string> y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (x == null)
            {
                return false;
            }

            if (y == null)
            {
                throw new ArgumentNullException("y");
            }

            if (x.Count != y.Count)
            {
                return false;
            }

            for (int i = 0; i < x.Count; i++)
            {
                if (!string.Equals(x[i], y[i]))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     Gets the hash code from the collections contents
        /// </summary>
        /// <param name="obj">
        ///     The collection to get the hash code from
        /// </param>
        /// <returns>
        ///     The hash code
        /// </returns>
        public int GetHashCode(IList<string> obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (obj.Count == 1)
            {
                return obj[0].GetHashCode();
            }

            int hashCode = 10;
            for (int i = 0; i < obj.Count; i++)
            {
                string s = obj[i];
                hashCode = hashCode * 33 ^ s.GetHashCode();
            }

            return hashCode;
        }
    }
}