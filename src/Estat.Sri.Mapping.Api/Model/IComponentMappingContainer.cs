// -----------------------------------------------------------------------
// <copyright file="IComponentMappingContainer.cs" company="EUROSTAT">
//   Date Created : 2017-09-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Builder;

    /// <summary>
    /// A container for all mapping information. This can be used by tools like Data Retriever and Structure Retriever to map the components of a DSD
    /// </summary>
    public interface IComponentMappingContainer
    {
        /// <summary>
        /// Gets the component mapping builders.
        /// </summary>
        /// <value>
        /// The component mapping builders.
        /// </value>
        IDictionary<string, IComponentMappingBuilder> ComponentMappingBuilders { get; }

        /// <summary>
        /// Gets the time dimension builder.
        /// </summary>
        /// <value>
        /// The time dimension builder.
        /// </value>
        ITimeDimensionFrequencyMappingBuilder TimeDimensionBuilder { get; }

        /// <summary>
        /// Gets the component mappings.
        /// </summary>
        /// <value>
        /// The component mappings.
        /// </value>
        IDictionary<string, ComponentMappingEntity> ComponentMappings { get; }

        /// <summary>
        /// Gets the time dimension mapping.
        /// </summary>
        /// <value>
        /// The time dimension mapping.
        /// </value>
        TimeDimensionMappingEntity TimeDimensionMapping { get;  }

        /// <summary>
        /// Gets the last update mapping.
        /// </summary>
        /// <value>
        /// The last update mapping.
        /// </value>
        LastUpdatedEntity LastUpdateMapping { get; }

        /// <summary>
        /// Gets the update status mapping.
        /// </summary>
        /// <value>
        /// The update status mapping.
        /// </value>
        UpdateStatusEntity UpdateStatusMapping { get; }

        /// <summary>
        /// Gets the last update mapping builder.
        /// </summary>
        /// <value>
        /// The last update mapping builder.
        /// </value>
        ILastUpdateMappingBuilder LastUpdateMappingBuilder { get; }

        /// <summary>
        /// Gets the update status mapping builder.
        /// </summary>
        /// <value>
        /// The update status mapping builder.
        /// </value>
        IUpdateStatusMappingBuilder UpdateStatusMappingBuilder { get; }

        /// <summary>
        /// Gets the valid to mapping builder.
        /// </summary>
        /// <value>
        /// The valid to mapping builder.
        /// </value>
        IValidToMappingBuilder ValidToMappingBuilder { get; }

        /// <summary>
        /// Gets the dataset.
        /// </summary>
        /// <value>
        /// The dataset.
        /// </value>
        DatasetEntity Dataset { get; }

        /// <summary>
        /// Gets the metadata dataset.
        /// </summary>
        /// <value>
        /// The metadata dataset.
        /// </value>
        DatasetEntity MetadataDataset { get; }

        /// <summary>
        /// Gets the dissemination connection.
        /// </summary>
        /// <value>
        /// The dissemination connection.
        /// </value>
        DdbConnectionEntity DisseminationConnection { get; }

        /// <summary>
        /// Gets or sets the valid to mapping.
        /// </summary>
        /// <value>
        /// The valid to mapping.
        /// </value>
        ValidToMappingEntity ValidToMapping { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance has historic mappings.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has historic mappings; otherwise, <c>false</c>.
        /// </value>
        bool HasHistoricMappings { get; }
    }
}