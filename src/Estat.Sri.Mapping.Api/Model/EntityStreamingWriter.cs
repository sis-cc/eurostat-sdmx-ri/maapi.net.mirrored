// -----------------------------------------------------------------------
// <copyright file="EntityStreamingWriter.cs" company="EUROSTAT">
//   Date Created : 2019-12-11
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using Estat.Sri.Mapping.Api.Builder;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Factory;
using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Util;

// TODO move to Engine folder/namespace
namespace Estat.Sri.Mapping.Api.Model
{
    [Obsolete("Use impl from Engine.Streaming namespace")]
    public class EntityStreamingWriterOld 
    {
        private const string YesAndTrascoded = "yes_and_transcoded";

        private readonly Action<IEntity, JsonWriter, string> _writeLinks;
        private readonly Action<IEntity, JsonWriter> _writeDetails;
        private readonly JsonWriter writer;
        private readonly TextWriter textWriter;
        private bool disposed = false;
        private string sid = string.Empty;
        private bool _calledFromWriteIEntity = false;
        private readonly bool _closeWriter = true;

        /// <summary>
        /// Inti
        /// </summary>
        /// <param name="writer">The JSON writer. It is assumed this is disposed outside this class</param>
        public EntityStreamingWriterOld(JsonWriter writer)
        {
            this._closeWriter = false;
            this.writer = writer;
        }
        public EntityStreamingWriterOld(string sid, Stream response, Action<IEntity, JsonWriter, string> writeLinks, Action<IEntity, JsonWriter> writeDetails)
        {
            this._closeWriter = true;
            _writeLinks = writeLinks;
            _writeDetails = writeDetails;
            textWriter = new StreamWriter(response, new UTF8Encoding(false));
            this.writer = new JsonTextWriter(textWriter);
            this.sid = sid;
        }

        public void Close()
        {
            Dispose(true);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool managed)
        {
            if (disposed)
            {
                return;
            }
            disposed = true;
            if (managed)
            {
                if (!_closeWriter)
                {
                    return;
                }

                if (writer != null)
                {
                    this.writer.WriteEndArray();
                    writer.Flush();
                    writer.Close();
                }
                if (textWriter != null && !writer.CloseOutput)
                {
                    textWriter.Flush();
                    textWriter.Dispose();
                }
            }
        }

        public void StartMessage()
        {
            writer.WriteStartArray();
        }

        public void StartSection(EntityType entityType)
        {
            // throw new NotImplementedException();
        }

        public void WriteProperty(string name, string value)
        {
            writer.WritePropertyName(name);
            writer.WriteValue(value);
        }

        public void WriteProperty(string name, bool value)
        {
            writer.WritePropertyName(name);
            writer.WriteValue(value);
        }

        public void WriteProperty(string name, object value)
        {

            writer.WritePropertyName(name);
            if (value != null)
            {
                writer.WriteValue(value);
            }
            else
            {
                // TODO what is the difference with the above
                writer.WriteValue(value);
            }
        }

        private void WriteDetailsLinks(IEntity entity)
        {
            string sid = this.sid;
            if (_writeDetails != null)
            {
                _writeDetails(entity, writer);
            }
            if (_writeLinks != null)
            {
                _writeLinks(entity, writer, sid);
            }
        }

        private void TryWriteOther<TValueType>(string propertyName, TValueType value)
        {
            if (value == null)
            {
                return;
            }

            writer.WritePropertyName(propertyName);
            writer.WriteValue(value);
        }
        private void TryWrite(string propertyName, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return;
            }

            writer.WritePropertyName(propertyName);
            writer.WriteValue(value);
        }
        private void WriteParentId(IEntity item)
        {
            TryWrite("parentId", item.ParentId);
        }
        private void WriteSimpleNameable(ISimpleNameableEntity entity)
        {
            TryWrite("name", entity.Name);
            TryWrite("description", entity.Description);
        }
        private void WriteEntityId(IEntity entity)
        {
            writer.WritePropertyName("entityId");
            writer.WriteValue(entity.EntityId);
        }
        private void NWriteDataSetColumn(JsonWriter writer, DataSetColumnEntity column)
        {
            writer.WriteStartObject();
            WriteSimpleNameable(column);
            WriteEntityId(column);
            WriteDetailsLinks(column);
            WriteParentId(column);
            TryWriteOther("last_updated", column.LastRetrieval);
            writer.WritePropertyName("isMapped");
            switch (column.IsMapped)
            {
                case ColumnMappingStatus.Yes:
                    writer.WriteValue("yes");
                    break;
                case ColumnMappingStatus.YesAndTranscoded:
                    writer.WriteValue(YesAndTrascoded);
                    break;
                default:
                    writer.WriteValue("no");
                    break;
            }
            writer.WriteEndObject();
        }
        private void WriteIdentifiable(JsonWriter writer, string propertyName, IIdentifiableEntity component)
        {
            if (component == null)
            {
                return;
            }

            writer.WritePropertyName(propertyName);
            writer.WriteStartObject();
            WriteEntityId(component);

            writer.WritePropertyName("id");
            writer.WriteValue(component.ObjectId);
            writer.WriteEndObject();
        }
        private void WriteLocalCode(JsonWriter writer, LocalCodeEntity item)
        {
            writer.WriteStartObject();
            // only print what is needed to reduce size
            if (!string.IsNullOrEmpty(item.EntityId))
            {
                WriteEntityId(item);
            }
            if (!string.IsNullOrEmpty(item.ParentId))
            {
                WriteParentId(item);
            }
            WriteDetailsLinks(item);
            TryWrite("id", item.ObjectId);
            WriteSimpleNameable(item);
            writer.WriteEndObject();
        }
        private void WriteTranscodingRule(JsonWriter writer, TranscodingRuleEntity rule)
        {
            writer.WriteStartObject();
            WriteEntityId(rule);
            WriteIdentifiable(writer, "code", rule.DsdCodeEntity);
            TryWrite("uncodedValue", rule.UncodedValue);
            if (rule.LocalCodes != null)
            {
                writer.WritePropertyName("localCodes");
                writer.WriteStartArray();
                foreach (var item in rule.LocalCodes)
                {
                    WriteLocalCode(writer, item);
                }
                writer.WriteEndArray();
            }
            writer.WriteEndObject();
        }
        private void WriteTimeTranscoding(JsonWriter writer, TimeTranscoding year)
        {
            writer.WritePropertyName("column");
            NWriteDataSetColumn(writer, year.Column);
            TryWriteOther("start", year.Start);
            TryWriteOther("length", year.Length);
        }
        private void WriteTranscoding(JsonWriter writer, TranscodingEntity entity)
        {
            writer.WriteStartObject();
            {
                WriteEntityId(entity);
                WriteParentId(entity);

                writer.WritePropertyName("expression");
                writer.WriteValue(entity.Expression);
                WriteDetailsLinks(entity);

                if (entity.HasScript())
                {
                    writer.WritePropertyName("script");
                    writer.WriteStartArray();
                    for (int i = 0; i < entity.Script.Count; i++)
                    {
                        var script = entity.Script[i];
                        writer.WritePropertyName("name");
                        writer.WriteValue(script.ScriptTile);
                        writer.WritePropertyName("content");
                        writer.WriteValue(script.ScriptContent);
                    }
                    writer.WriteEndArray();
                }

                if (entity.TranscodingType == TranscodingType.Time)
                {
                    writer.WritePropertyName("timeTranscoding");
                    writer.WriteStartObject();
                    foreach (var timePerFreq in entity.TimeTranscoding)
                    {
                        writer.WritePropertyName(timePerFreq.Frequency);
                        writer.WriteStartObject();
                        TryWriteOther("is_date_time", timePerFreq.IsDateTime);
                        if (timePerFreq.IsDateTime)
                        {
                            writer.WritePropertyName("date_column");
                            NWriteDataSetColumn(writer, timePerFreq.DateColumn);
                        }
                        else
                        {
                            TimeTranscoding year = timePerFreq.Year;
                            if (year != null)
                            {
                                writer.WritePropertyName("year");
                                writer.WriteStartObject();
                                WriteTimeTranscoding(writer, year);
                                writer.WriteEndObject();
                            }
                            PeriodTimeTranscoding period = timePerFreq.Period;
                            if (period != null)
                            {
                                writer.WritePropertyName("period");
                                writer.WriteStartObject();
                                WriteTimeTranscoding(writer, period);
                                if (period.Rules != null)
                                {
                                    writer.WritePropertyName("rules");
                                    writer.WriteStartArray();
                                    foreach (var rule in period.Rules)
                                    {
                                        WriteTranscodingRule(writer, rule);
                                    }
                                    writer.WriteEndArray();
                                }
                                writer.WriteEndObject();

                            }
                        }
                        writer.WriteEndObject();
                    }
                    writer.WriteEndObject();
                }
                else if (entity.TranscodingType == TranscodingType.AdvancedTime)
                {
                    // TODO advance time transcoding
                }

            }
            writer.WriteEndObject();
        }

        private void WriteConnectionProperties(IConnectionEntity entity, JsonWriter writer)
        {
            if (entity.Settings == null || entity.Settings.Count == 0)
            {
                return;
            }
            writer.WritePropertyName("properties");
            writer.WriteStartObject();
            foreach (var property in entity.Settings)
            {
                writer.WritePropertyName(property.Key);
                writer.WriteStartObject();
                WriteProperty("type", property.Value.DataType);
                WriteProperty("links", null);
                WriteProperty("value", property.Value.Value);
                writer.WritePropertyName("allowedValues");
                writer.WriteStartArray();
                foreach (var value in property.Value.AllowedValues)
                {
                    writer.WriteValue(value);
                }
                writer.WriteEndArray();
                WriteProperty("defaultValue", property.Value.DefaultValue);
                WriteProperty("required", property.Value.Required);
                WriteProperty("advanced", property.Value.Advanced);
                writer.WritePropertyName("allowedSubTypes");
                writer.WriteStartArray();
                foreach (var value in property.Value.AllowedSubTypes)
                {
                    writer.WriteValue(value);
                }
                writer.WriteEndArray();
                writer.WriteEndObject();
            }
            writer.WriteEndObject();
        }

        private void WriteSubTypes(IConnectionEntity entity, JsonWriter writer)
        {
            if (entity.SubTypeList == null)
            {
                return;
            }
            writer.WritePropertyName("subTypeList");
            writer.WriteStartArray();
            foreach (var subType in entity.SubTypeList)
            {
                writer.WriteValue(subType);
            }
            writer.WriteEndArray();
        }

        private void WritePermissions(IPermissionEntity entity, JsonWriter writer)
        {
            if (entity.Permissions == null)
            {
                return;
            }
            writer.WritePropertyName("permissions");
            writer.WriteStartObject();
            foreach (var permission in entity.Permissions)
            {
                writer.WritePropertyName(permission.Key);
                writer.WriteValue(permission.Value);
            }
            writer.WriteEndObject();
        }
        public void Write(IConnectionEntity entity)
        {
            if (entity != null)
            {
                writer.WriteStartObject();
                WriteEntityId(entity);
                WritePermissions(entity, writer);
                WriteProperty("db_name", entity.DbName);

                writer.WritePropertyName("type");
                writer.WriteValue(entity.DatabaseVendorType);
                writer.WritePropertyName("subtype");
                writer.WriteValue(entity.SubType);
                WriteSubTypes(entity, writer);
                WriteConnectionProperties(entity, writer);
                WriteDetailsLinks(entity);
                writer.WritePropertyName("name");
                writer.WriteValue(entity.Name);
                WriteDetailsLinks(entity);
                writer.WritePropertyName("details");
                writer.WriteStartObject();
                writer.WriteEndObject();
                writer.WriteEndObject();
            }
        }
        public void Write(DatasetEntity entity)
        {
            if (entity != null)
            {
                writer.WriteStartObject();
                writer.WritePropertyName("connection");
                writer.WriteStartObject();
                {
                    writer.WritePropertyName("entityId");
                    writer.WriteValue(entity.EntityId);
                }
                writer.WriteEndObject();
                writer.WritePropertyName("query");
                writer.WriteValue(entity.Query);
                writer.WritePropertyName("extraData");
                // TODO Test this
                writer.WriteRawValue(entity.JSONQuery);
                writer.WritePropertyName("editorType");
                writer.WriteValue(entity.EditorType);
                WritePermissions(entity, writer);

                WriteParentId(entity);
                WriteEntityId(entity);
                WriteSimpleNameable(entity);

                WriteDetailsLinks(entity);

                writer.WriteEndObject();
            }
        }
        public void Write(DataSetColumnEntity entity)
        {
            if (entity != null)
            {
                writer.WriteStartObject();
                WriteSimpleNameable(entity);
                WriteEntityId(entity);
                WriteDetailsLinks(entity);
                WriteParentId(entity);
                TryWriteOther("last_updated", entity.LastRetrieval);
                writer.WritePropertyName("isMapped");
                switch (entity.IsMapped)
                {
                    case ColumnMappingStatus.Yes:
                        writer.WriteValue("yes");
                        break;
                    case ColumnMappingStatus.YesAndTranscoded:
                        writer.WriteValue(YesAndTrascoded);
                        break;
                    default:
                        writer.WriteValue("no");
                        break;
                }
                writer.WriteEndObject();
            }
        }
        public void Write(LocalCodeEntity entity)
        {
            if (entity != null)
            {
                WriteLocalCode(writer, entity);
            }
        }
        public void Write(MappingSetEntity entity)
        {
            if (entity != null)
            {
                writer.WriteStartObject();
                WriteEntityId(entity);
                WriteParentId(entity);
                WriteSimpleNameable(entity);
                writer.WritePropertyName("dataset");
                writer.WriteStartObject();
                writer.WritePropertyName("entityId");
                writer.WriteValue(entity.EntityId);
                writer.WriteEndObject();
                WriteDetailsLinks(entity);
                writer.WritePropertyName("name");
                writer.WriteValue(entity.Name);
                writer.WritePropertyName("description");
                writer.WriteValue(entity.Description);

                writer.WriteEndObject();
            }
        }
        public void Write(ColumnDescriptionSourceEntity entity)
        {
            if (entity != null)
            {
                writer.WriteStartObject();
                WriteEntityId(entity);
                WriteParentId(entity);
                writer.WritePropertyName("table");
                writer.WriteValue(entity.DescriptionTable);
                writer.WritePropertyName("relationField");
                writer.WriteValue(entity.RelatedField);
                writer.WritePropertyName("descriptionField");
                writer.WriteValue(entity.DescriptionField);
                WriteDetailsLinks(entity);

                writer.WriteEndObject();
            }
        }
        public void Write(ComponentMappingEntity entity)
        {
            if (entity != null)
            {
                writer.WriteStartObject();
                WriteEntityId(entity);
                writer.WritePropertyName("type");
                writer.WriteValue(entity.Type);

                WriteParentId(entity);
                WriteDetailsLinks(entity);
                TryWrite("constant_value", entity.ConstantValue);
                TryWrite("defaultValue", entity.DefaultValue);
                if (!string.IsNullOrEmpty(entity.TranscodingId))
                {
                    var transcoding = new TranscodingEntity();
                    transcoding.EntityId = entity.TranscodingId;
                    transcoding.ParentId = entity.EntityId;
                    transcoding.StoreId = entity.StoreId;
                    writer.WritePropertyName("transcoding");
                    WriteTranscoding(writer, transcoding);
                }

                Component component = entity.Component;
                {
                    WriteIdentifiable(writer, "component", component);
                }
                var columns = entity.GetColumns();
                if (ObjectUtil.ValidCollection(columns))
                {
                    writer.WritePropertyName("dataset_column");
                    writer.WriteStartArray();
                    for (int i = 0; i < columns.Count; i++)
                    {
                        var column = columns[i];
                        // HACK
                        column.IsMapped = string.IsNullOrEmpty(entity.TranscodingId) ? ColumnMappingStatus.Yes : ColumnMappingStatus.YesAndTranscoded;
                        NWriteDataSetColumn(writer, column);
                    }
                    writer.WriteEndArray();
                }
                writer.WriteEndObject();
            }
        }
        public void Write(TranscodingEntity entity)
        {
            if (entity != null)
            {
                WriteTranscoding(writer, entity);
            }
        }
        public void Write(TranscodingRuleEntity entity)
        {
            if (entity != null)
            {
                WriteTranscodingRule(writer, entity);
            }
        }
        public void Write(HeaderEntity entity)
        {
            if (entity != null)
            {
                writer.WriteStartObject();
                WriteEntityId(entity);
                WriteParentId(entity);
                IHeader sdmxHeader = entity.SdmxHeader;
                if (sdmxHeader != null)
                {
                    writer.WritePropertyName("dataSetAgencyId");
                    writer.WriteValue(sdmxHeader.GetAdditionalAttribtue("DataSetAgency"));
                    WriteProperty("name", sdmxHeader.Name.FirstOrDefault()?.Value);

                    WriteProperty("source", sdmxHeader.Source.FirstOrDefault()?.Value);
                    WriteProperty("test", sdmxHeader.Test);

                    Write("sender",sdmxHeader.Sender);
                    Write("receiver", sdmxHeader.Receiver);
                }
               
                writer.WriteEndObject();
            }
        }

        private void Write(string propertyName, IList<IParty> receiver)
        {
            writer.WritePropertyName(propertyName);
            writer.WriteStartArray();
            foreach(IParty party in receiver)
            {
                Write(party);
            }
            writer.WriteEndArray();
        }

        public void Write(string propertyName, IParty buildFrom)
        {
            writer.WritePropertyName(propertyName);
            writer.WriteStartObject();
            Write(buildFrom);
            writer.WriteEndObject();
        }

        private void Write(IParty buildFrom)
        {
            WriteProperty("id", buildFrom.Id);
            // TODO change to names
            Write("name", buildFrom.Name);
            Write("contacts", buildFrom.Contacts);
            WriteProperty("timeZone", buildFrom.TimeZone);
        }

        private void Write(string propertyName, IList<IContact> contacts)
        {
            writer.WritePropertyName(propertyName);
            writer.WriteStartArray();
            foreach(IContact contact in contacts)
            {
                Write(contact);
            }
            writer.WriteEndArray();
         }

        private void Write(IContact buildFrom)
        {
            writer.WriteStartObject();
            Write("departments",buildFrom.Departments);
            Write("role",buildFrom.Role);
            Write("name",buildFrom.Name);
            Write("email",buildFrom.Email);
            Write("telephone",buildFrom.Telephone);
            Write("uri",buildFrom.Uri);
            Write("fax",buildFrom.Fax);
            Write("x400",buildFrom.X400);
            writer.WriteEndObject();
        }

        private void Write(string propertyName, IEnumerable<string> values)
        {
            writer.WritePropertyName(propertyName);
            writer.WriteStartArray();
            foreach(string value in values)
            {
                writer.WriteValue(value);
            }

            writer.WriteEndArray();
        }
        private void Write(string propertyName, IList<ITextTypeWrapper> names)
        {
            writer.WritePropertyName(propertyName);
            writer.WriteStartObject();
            foreach(var name in names)
            {
                WriteProperty(name.Locale, name.Value);
            }

            writer.WriteEndObject();
        }

        public void Write(RegistryEntity entity)
        {
            if (entity != null)
            {
                writer.WriteStartObject();
                {
                    WriteEntityId(entity);
                    WriteSimpleNameable(entity);
                    WriteParentId(entity);
                    WriteDetailsLinks(entity);

                    TryWrite("technology", entity.Technology);
                    TryWrite("url", entity.Url);
                    TryWrite("username", entity.UserName);
                }
                writer.WriteEndObject();
            }
        }

        public void Write(NsiwsEntity entity)
        {
            if (entity != null)
            {
                writer.WriteStartObject();
                {
                    WriteEntityId(entity);
                    WriteSimpleNameable(entity);
                    WriteParentId(entity);
                    WriteDetailsLinks(entity);

                    TryWrite("technology", entity.Technology);
                    TryWrite("url", entity.Url);
                    TryWrite("username", entity.UserName);
                }
                writer.WriteEndObject();
            }
        }
        public void Write(UserEntity entity)
        {
            if (entity != null)
            {
                writer.WriteStartObject();
                WriteEntityId(entity);
                WriteProperty("username", entity.UserName);
                WritePermissions(entity, writer);
                writer.WriteEndObject();
            }
        }
        public void Write(TemplateMapping entity)
        {
            if (entity != null)
            {
                writer.WriteStartObject();
                WriteEntityId(entity);
                writer.WritePropertyName("columnName");
                writer.WriteValue(entity.ColumnName);
                writer.WritePropertyName("columnDescription");
                writer.WriteValue(entity.ColumnDescription);
                writer.WritePropertyName("componentId");
                writer.WriteValue(entity.ComponentId);
                writer.WritePropertyName("componentType");
                writer.WriteValue(entity.ComponentType);
                writer.WritePropertyName("transcodings");
                writer.WriteStartObject();
                foreach (var item in entity.Transcodings)
                {
                    writer.WritePropertyName(item.Key);
                    writer.WriteValue(item.Value);
                }
                writer.WriteEndObject();
                writer.WritePropertyName("timeTranscodings");
                writer.WriteStartObject();
                foreach (var item in entity.TimeTranscoding)
                {
                    WriteEntityId(item);
                }
                writer.WriteEndObject();
                WriteDetailsLinks(entity);
                writer.WriteEndObject();
            }
        }

        public void Write(IEnumerable<IConnectionEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<DatasetEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<DataSetColumnEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<LocalCodeEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<MappingSetEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<ColumnDescriptionSourceEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<ComponentMappingEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<TranscodingEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<TranscodingRuleEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<HeaderEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<RegistryEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }

        public void Write(IEnumerable<NsiwsEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<UserEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }
        public void Write(IEnumerable<TemplateMapping> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }

        public void Write(IEnumerable<IEntity> entity)
        {
            foreach (var ent in entity)
            {
                Write(ent);
            }
        }

        public void Write(IEntity entity)
        {
          
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            // Note this is not thread safe, only trying to avoid being called by it self
            if (_calledFromWriteIEntity)
            {
                throw new InvalidOperationException("BUG loop detected in writer for entity of type"  + entity.TypeOfEntity);
            }
            _calledFromWriteIEntity = true;
            try
            {
                switch (entity.TypeOfEntity)
                {
                    case EntityType.DdbConnectionSettings:
                        if (entity is IConnectionEntity)
                        {
                            Write((IConnectionEntity)entity);
                        }
                        break;

                    case EntityType.DataSet:
                        Write((DatasetEntity)entity);
                        break;

                    case EntityType.DataSetColumn:
                        Write((DataSetColumnEntity)entity);
                        break;

                    case EntityType.LocalCode:
                        Write((LocalCodeEntity)entity);
                        break;

                    case EntityType.MappingSet:
                        Write((MappingSetEntity)entity);
                        break;

                    case EntityType.Mapping:
                        Write((ComponentMappingEntity)entity);
                        break;

                    case EntityType.Transcoding:
                        Write((TranscodingEntity)entity);
                        break;

                    case EntityType.TranscodingRule:
                        Write((TranscodingRuleEntity)entity);
                        break;

                    case EntityType.DescSource:
                        Write((ColumnDescriptionSourceEntity)entity);
                        break;

                    case EntityType.Header:
                        Write((HeaderEntity)entity);
                        break;

                    case EntityType.Registry:
                        Write((RegistryEntity)entity);
                        break;
                    case EntityType.User:
                        Write((UserEntity)entity);
                        break;
                    case EntityType.UserAction:
                        Write((UserActionEntity)entity);
                        break;
                    case EntityType.TemplateMapping:
                        Write((TemplateMapping)entity);
                        break;
                    case EntityType.Nsiws:
                        Write((NsiwsEntity)entity);
                        break;
                }
            }
            finally 
            {
                _calledFromWriteIEntity = false;
            }
        }

        public void Write(UserActionEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Write(IEnumerable<UserActionEntity> entity)
        {
            throw new NotImplementedException();
        }

        public void WriteStatus(StatusType status,string message)
        {
            writer.WriteStartObject();
            writer.WritePropertyName("status");
            writer.WriteValue(nameof(status));
            writer.WritePropertyName("errorMessage");
            writer.WriteValue(message);
            writer.WriteEndObject();
        }
    }
}
