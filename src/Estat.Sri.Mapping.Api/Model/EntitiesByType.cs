﻿// -----------------------------------------------------------------------
// <copyright file="Entities.cs" company="EUROSTAT">
//   Date Created : 2017-07-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    [Serializable]
    public class EntitiesByType
    {
        private readonly Dictionary<EntityType, List<IEntity>> _entitiesByType = new Dictionary<EntityType, List<IEntity>>();

        public void Add(IEntity entity)
        {
            if (this._entitiesByType.ContainsKey(entity.TypeOfEntity))
            {
                this._entitiesByType[entity.TypeOfEntity].Add(entity);
            }
            else
            {
                this._entitiesByType.Add(entity.TypeOfEntity, new List<IEntity> {entity});
            }
        }

        public void AddRange(IList<IEntity> entities)
        {
            var groupedEntities = entities.GroupBy(x => x.TypeOfEntity);
            foreach (var groupedEntity in groupedEntities)
            {
                if (this._entitiesByType.ContainsKey(groupedEntity.Key))
                {
                    this._entitiesByType[groupedEntity.Key].AddRange(groupedEntity.ToList());
                }
                else
                {
                    this._entitiesByType.Add(groupedEntity.Key, groupedEntity.ToList());
                }
            }
        }

        public IList<IEntity> GetEntities(EntityType type)
        {
            return this._entitiesByType.ContainsKey(type) ? this._entitiesByType[type] : null;
        }

        public IEnumerable<TEntity> GetEntities<TEntity>() where TEntity : class, IEntity
        {
            return this._entitiesByType.SelectMany(pair => pair.Value).OfType<TEntity>();
        }

        public Dictionary<EntityType, List<IEntity>>.KeyCollection Keys => this._entitiesByType.Keys;
    }
}