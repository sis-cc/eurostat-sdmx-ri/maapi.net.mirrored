using System;

namespace Estat.Sri.Mapping.Api.Model
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class OptionalAttribute : Attribute
    {
    }
}