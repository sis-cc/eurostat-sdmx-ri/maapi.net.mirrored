﻿using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Mapping.Api.Model
{
    /// <summary>
    /// A representation of a DSDOnTheFly
    /// </summary>
    public class DSDOnTheFlyEntity
    {
        /// <summary>
        /// Gets or sets the DSD identifier.
        /// </summary>
        /// <value>
        /// The DSD identifier.
        /// </value>
        public StructureReferenceImpl DsdId { get; set; }
        /// <summary>
        /// Gets or sets the mappings.
        /// </summary>
        /// <value>
        /// The mappings.
        /// </value>
        public List<TemplateMapping> Mappings { get; set; }
        /// <summary>
        /// Gets or sets the DSD names.
        /// </summary>
        /// <value>
        /// The DSD names.
        /// </value>
        public Dictionary<string, string> DsdNames { get; set; }
        /// <summary>
        /// Gets or sets the name of the dataset.
        /// </summary>
        /// <value>
        /// The name of the dataset.
        /// </value>
        public SimpleNameableEntity Dataset { get; set; }
    }
}
