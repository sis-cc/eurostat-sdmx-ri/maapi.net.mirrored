﻿// -----------------------------------------------------------------------
// <copyright file="IConnectionEntity.cs" company="EUROSTAT">
//   Date Created : 2017-02-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System.Collections.Generic;

    /// <summary>
    /// The Connection Entity interface.
    /// </summary>
    public interface IConnectionEntity : IPermissionEntity, ISimpleNameableEntity
    {
        /// <summary>
        /// Gets or sets the name of the database.
        /// </summary>
        /// <value>
        /// The name of the database.
        /// </value>
        string DbName { get; set; }

        /// <summary>
        /// Gets or sets the type of the database.
        /// </summary>
        /// <value>
        /// The type of the database.
        /// </value>
        string DatabaseVendorType { get; set; }

        /// <summary>
        /// Gets the advanced settings.
        /// </summary>
        /// <value>
        /// The advanced settings.
        /// </value>
        IDictionary<string, IConnectionParameterEntity> Settings { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has sub type.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has sub type; otherwise, <c>false</c>.
        /// </value>
        bool HasSubType { get; set; }

        /// <summary>
        /// Gets or sets the type of the sub.
        /// </summary>
        /// <value>
        /// The type of the sub.
        /// </value>
        string SubType { get; set; }

        /// <summary>
        /// Gets the sub type list.
        /// </summary>
        /// <value>
        /// The sub type list.
        /// </value>
        IList<string> SubTypeList { get; }
    }
}