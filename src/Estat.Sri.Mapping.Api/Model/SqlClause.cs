﻿// -----------------------------------------------------------------------
// <copyright file="SqlClause.cs" company="EUROSTAT">
//   Date Created : 2016-11-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data.Common;

    /// <summary>
    /// SQL Clause holds a Clause format and the <see cref="DbParameter"/>
    /// </summary>
    public class SqlClause
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SqlClause"/> class.
        /// </summary>
        /// <param name="whereClause">The clause string.</param>
        /// <param name="parameters">The parameters.</param>
        public SqlClause(string whereClause, IList<DbParameter> parameters)
        {
            this.WhereClause = whereClause;
            this.Parameters = new ReadOnlyCollection<DbParameter>(parameters);
        }

        /// <summary>
        /// Gets the clause string.
        /// </summary>
        /// <value>
        /// The clause.
        /// </value>
        public string WhereClause { get; }

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        public IList<DbParameter> Parameters { get; }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return string.Format("Clause: {0}, Parameters: {1}", this.WhereClause, this.Parameters);
        }
    }
}