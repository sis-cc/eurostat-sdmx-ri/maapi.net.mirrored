﻿namespace Estat.Sri.Mapping.Api.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;

    public class ActionResult : IActionResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ActionResult"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="status">The status.</param>
        /// <param name="messages">The messages.</param>
        public ActionResult(string errorCode, StatusType status, params string[] messages)
            : this(errorCode, status, null, messages)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ActionResult" /> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="status">The status.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="messages">The messages.</param>
        public ActionResult(string errorCode, StatusType status,List<Exception> exceptions, params string[] messages)
        {
            this.ErrorCode = errorCode;
            this.Messages = messages?.ToArray() ?? new string[0];
            this.Status = status;
            this.Exceptions = exceptions;
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public StatusType Status { get; }

        /// <summary>
        /// Gets the error code.
        /// </summary>
        /// <value>
        /// The error code.
        /// </value>
        public string ErrorCode { get; }

        /// <summary>
        /// Gets the messages.
        /// </summary>
        /// <value>
        /// The messages.
        /// </value>
        public IList<string> Messages { get; }

        /// <summary>
        /// Gets the exceptions.
        /// </summary>
        /// <value>
        /// The exceptions.
        /// </value>
        public List<Exception> Exceptions { get; }

        /// <summary>
        /// Merges the specified <see cref="IActionResult"/> with this and provide an new instance of <see cref="IActionResult"/>. 
        /// Unless <paramref name="result"/> is null, in this case it returns the current instance. 
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns>The merged <see cref="IActionResult"/></returns>
        public IActionResult Merge(IActionResult result)
        {
            if (result == null)
            {
                return this;
            }

            var statusType = (StatusType)Math.Max((int)result.Status, (int)this.Status);
            return new ActionResult(this.ErrorCode, statusType, this.Exceptions.Union(result.Exceptions).ToList(),this.Messages.Union(result.Messages).ToArray());
        }
    }
}