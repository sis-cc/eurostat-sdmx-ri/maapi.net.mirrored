// -----------------------------------------------------------------------
// <copyright file="ColumnDescriptionSourceEntity.cs" company="EUROSTAT">
//   Date Created : 2017-04-07
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    /// <summary>
    /// DescSourceEntity
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Model.Entity" />
    public class ColumnDescriptionSourceEntity : Entity
    {
        private string _descriptionField;
        private string _descriptionTable;
        private string _returnField;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public ColumnDescriptionSourceEntity()
            : base(EntityType.DescSource)
        {
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string DescriptionField
        {
            get { return this._descriptionField; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._descriptionField, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.DescriptionField), value);
                }
                this._descriptionField = value;
            }
        }

        /// <summary>
        /// Gets or sets the table.
        /// </summary>
        /// <value>
        /// The table.
        /// </value>
        public string DescriptionTable
        {
            get { return this._descriptionTable; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._descriptionTable, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.DescriptionTable), value);
                }
                this._descriptionTable = value;
            }
        }

        /// <summary>
        /// Gets or sets the related field.
        /// </summary>
        /// <value>
        /// The related field.
        /// </value>
        public string RelatedField
        {
            get { return this._returnField; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._returnField, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.RelatedField), value);
                }
                this._returnField = value;
            }
        }
    }
}