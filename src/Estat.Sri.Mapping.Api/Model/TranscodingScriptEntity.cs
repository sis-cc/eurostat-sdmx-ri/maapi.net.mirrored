// -----------------------------------------------------------------------
// <copyright file="TranscodingScriptEntity.cs" company="EUROSTAT">
//   Date Created : 2017-03-20
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// The transcoding script entity.
    /// </summary>
    public class TranscodingScriptEntity : Entity
    {
        private string _name;
        private string _content;

        /// <summary>
        /// Initializes a new instance of the <see cref="TranscodingScriptEntity"/> class. 
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public TranscodingScriptEntity()
            : base(EntityType.TranscodingScript)
        {
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string ScriptTile
        {
            get { return this._name; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._name, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ScriptTile), value);
                }
                this._name = value;
            }
        }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        public string ScriptContent
        {
            get { return this._content; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._content, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ScriptContent), value);
                }
                this._content = value;
            }
        }
    }
}