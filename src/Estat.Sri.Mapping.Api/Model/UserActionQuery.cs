﻿using System;

namespace Estat.Sri.Mapping.Api.Model
{
    public class UserActionQuery
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}