using System;
using System.Collections.Generic;
using System.Text;

namespace Estat.Sri.Mapping.Api.Model
{
    /// <summary>
    /// Database identification options
    /// </summary>
    public class DatabaseIdentificationOptions
    {
        /// <summary>
        /// connection string
        /// </summary>
        public virtual string ConnectionString { get; set; }
        /// <summary>
        /// provider name
        /// </summary>
        public virtual string ProviderName { get; set; }
        /// <summary>
        /// store id
        /// </summary>
        public virtual string StoreId { get; set; }

        /// <summary>
        /// Gets an identification string based on connection string, provider name or store id
        /// </summary>
        /// <returns></returns>
        public string GetIdentification()
        {
            var identification = string.Empty;
            identification += string.IsNullOrWhiteSpace(ConnectionString) ? string.Empty : $"Connection string : {ConnectionString},";
            identification += string.IsNullOrWhiteSpace(ProviderName) ? string.Empty : $"Provider name : {ProviderName},";
            identification += string.IsNullOrWhiteSpace(StoreId) ? string.Empty : $"Store Id : {StoreId},";
            return identification.TrimEnd(',');
        }
    }
}
