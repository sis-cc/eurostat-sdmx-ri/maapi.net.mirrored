// -----------------------------------------------------------------------
// <copyright file="DatasetEntity.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    public class DatasetEntity : SimpleNameableEntity, IPermissionEntity
    {
        private string _query;
        private string _orderByClause;
        private string _JSONQuery;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public DatasetEntity() : base(EntityType.DataSet)
        {
        }


        /// <summary>
        /// Gets or sets the type of the editor.
        /// </summary>
        /// <value>
        /// The type of the editor.
        /// </value>
        public string EditorType { get; set; }
        

        /// <summary>
        /// Gets or sets the query.
        /// </summary>
        /// <value>
        /// The query.
        /// </value>
        public string Query
        {
            get => this._query;
            set
            {
                if (this.ShouldCreateUpdateDocument(this._query, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.Query), value);
                }
                this._query = value;
            }
        }

        /// <summary>
        /// Gets or sets the orderByClause. 
        /// </summary>
        /// <value>
        /// The query.
        /// </value>
        public string OrderByClause
        {
            get => this._orderByClause;
            set
            {
                if (this.ShouldCreateUpdateDocument(this._orderByClause, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.OrderByClause), value);
                }
                this._orderByClause = value;
            }
        }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        //public string UserId { get; set; }
        

        /// <summary>
        /// Gets or sets the XML query.
        /// </summary>
        /// <value>
        /// The XML query.
        /// </value>
        public string JSONQuery
        {
            get => this._JSONQuery;
            set
            {
                if (this.ShouldCreateUpdateDocument(this._JSONQuery, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.JSONQuery), value);
                }
                this._JSONQuery = value;
            }
        }


        /// <summary>
        /// Gets or sets the dataset property value.
        /// </summary>
        /// <value>
        /// The <see cref="DatasetPropertyEntity"/> class.
        /// </value>
        public DatasetPropertyEntity DatasetPropertyEntity { get; set; }


        /// <summary>
        /// Gets or sets the permissions.
        /// </summary>
        /// <value>
        /// The permissions.
        /// </value>
        [Optional]
        public Dictionary<string,string> Permissions { get; set; }
    }
}