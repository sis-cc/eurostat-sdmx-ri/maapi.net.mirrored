// -----------------------------------------------------------------------
// <copyright file="TranscodingRule.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Model
{
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;

    public class TranscodingRuleEntity : Entity
    {
        private IIdentifiableEntity _dsdCodeEntity;

        /// <summary>
        /// Initializes a new instance of the <see cref="TranscodingRuleEntity"/> class.
        /// </summary>
        public TranscodingRuleEntity()
            : base(EntityType.TranscodingRule)
        {
            this.LocalCodes = new List<LocalCodeEntity>();
        }

        /// <summary>
        /// Gets or sets the DSD code entity.
        /// </summary>
        /// <remarks>
        /// There is an overlapping with the <see cref="DsdCodeEntities"/> introduced in SDMX 3.0.0 
        /// to allow for DSD array mapping. 
        /// TODO: should consider unifying the functionality under <see cref="DsdCodeEntities"/> only.
        /// </remarks>
        /// <value>
        /// The DSD code entity.
        /// </value>
        public IIdentifiableEntity DsdCodeEntity
        {
            get 
            {
                if (_dsdCodeEntity == null && DsdCodeEntities.Count == 1)
                {
                    return DsdCodeEntities.Single();
                }
                else
                {
                    return _dsdCodeEntity;
                }
            } 
            set { this._dsdCodeEntity = value; }
        }

        /// <summary>
        /// SDMX 3.0 code with array values
        /// </summary>
        /// <remarks>
        /// Use the <see cref="DsdCodeEntity"/> when only one value exists.
        /// </remarks>
        public IList<IIdentifiableEntity> DsdCodeEntities { get; set; } = new List<IIdentifiableEntity>();

        /// <summary>
        /// Gets or sets the uncoded value.
        /// </summary>
        /// <value>
        /// The uncoded value.
        /// </value>
        public string UncodedValue { get; set; }

        /// <summary>
        /// Gets the local codes.
        /// </summary>
        /// <value>
        /// The local codes.
        /// </value>
        public IList<LocalCodeEntity> LocalCodes { get; set; }

        /// <summary>
        /// Adds the specified rules
        /// </summary>
        /// <param name="sdmxCodeId">The SDMX code identifier.</param>
        /// <param name="localId">The local identifier.</param>
        /// <param name="localcodes">The local codes.</param>
        public void Add(string sdmxCodeId, string localId, params string[] localcodes)
        {
            DsdCodeEntity = new IdentifiableEntity() { ObjectId = sdmxCodeId };
            LocalCodes.Add(new LocalCodeEntity() { ObjectId = localId });
            if (localcodes != null)
            {
                foreach (var localcode in localcodes)
                {
                    LocalCodes.Add(new LocalCodeEntity { ObjectId = localcode });
                }
            }
        }

        /// <summary>
        /// Creates a new shallow copy of this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>
        /// The <see cref="Entity"/> based instance. Note that <see cref="Entity.EntityId"/> and <see cref="Entity.ParentId"/> are set to null
        /// </returns>
        public override TEntity CreateDeepCopy<TEntity>()
        {
            var clone = base.CreateDeepCopy<TEntity>();
            var newRule = AsSpecificEntity<TranscodingRuleEntity>(clone);

            newRule.DsdCodeEntity = DsdCodeEntity?.CreateDeepCopy<IIdentifiableEntity>();
            newRule.DsdCodeEntities = DsdCodeEntities.Select(code => code.CreateDeepCopy<IIdentifiableEntity>()).ToList();
            newRule.LocalCodes = LocalCodes?.Select(entity => entity.CreateShallowCopy<LocalCodeEntity>()).ToList();
            return clone;
        }
    }
}