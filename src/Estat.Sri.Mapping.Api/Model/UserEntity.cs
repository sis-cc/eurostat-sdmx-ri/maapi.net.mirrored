// -----------------------------------------------------------------------
// <copyright file="UserEntity.cs" company="EUROSTAT">
//   Date Created : 2017-06-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Constant;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Mapping.Api.Model
{
    using System;
    using System.Collections.Generic;

    public class UserEntity : Entity, IPermissionEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public UserEntity()
            : base(EntityType.User)
        {
        }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        //// TODO this should be the EntityId
        public string UserName { get; set; }

        /// <summary>
        /// Gets the roles.
        /// </summary>
        /// <value>The roles.</value>
        public IList<string> Roles { get; set; }

        /// <summary>
        /// Gets the permissions.
        /// </summary>
        /// <value>The permissions.</value>
        public Dictionary<string, string>  Permissions { get; set; }

        /// <summary>
        /// Is Group
        /// </summary>
        public bool IsGroup { get; set; }
        /// <summary>
        /// The Dataspace
        /// </summary>
        public string Dataspace { get; set; }

        /// <summary>
        /// Edited by
        /// </summary>
        public string EditedBy { get; set; }

        /// <summary>
        /// Edit date
        /// </summary>
        public DateTime EditDate { get; set; }
        /// <summary>
        /// Adds the permission.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void AddPermission(string key, string value)
        {
            this.PatchRequest.CreateUpdateDocumentForList("permissions/" +key ,null, new JArray(){key});
            if (Permissions == null)
            {
                Permissions = new Dictionary<string, string>(StringComparer.Ordinal);
            }
            this.Permissions.Add(key,value);
        }

        public void RemovePermission(string key)
        {
            if (Permissions == null)
            {
                return;
            }
            this.PatchRequest.CreateUpdateDocumentForList("permissions/" + key, key, null);
          
            this.Permissions.Remove(key);
        }

        public void ClearPermissions()
        {
            if (Permissions == null)
            {
                return;
            }
            var permissionsKeys = this.Permissions.Keys;
            foreach (var permissionsKey in permissionsKeys)
            {
                this.RemovePermission(permissionsKey);
            }
        }
    }
}