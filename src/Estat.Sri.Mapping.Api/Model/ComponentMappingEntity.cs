// -----------------------------------------------------------------------
// <copyright file="ComponentMappingEntity.cs" company="EUROSTAT">
//   Date Created : 2017-03-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    using System;
    using System.Linq;

    /// <summary>
    /// ComponentMappingEntity
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Model.Entity" />
    public class ComponentMappingEntity : SimpleNameableEntity, IMappingEntity /* TODO This shouldn't be SimpleNameableEntity, just Entity */
    {
        private string _constantValue;
        private string _transcodingId;
        private string _type;
        private string _defaultValue;
        private Component _component;
        private IList<DataSetColumnEntity> _columns = new List<DataSetColumnEntity>();
        private bool _isMetadata;
        private string _metadataAttributeSdmxId;


        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public ComponentMappingEntity()
            : base(EntityType.Mapping)
        {
        }

        /// <summary>
        /// Gets or sets the column identifier.
        /// </summary>
        /// <returns>
        /// The column identifier.
        /// </returns>
        public IList<DataSetColumnEntity> GetColumns()
        {
            return this._columns;
        }

        /// <summary>
        /// Gets whether the component mappings has dataset columns set
        /// </summary>
        public bool HasColumns
        {
            get
            {
                return !(GetColumns() == null || GetColumns().Count == 0);
            }
        }

        /// <summary>
        /// Gets or sets the column identifier.
        /// </summary>
        /// <param name="value">
        /// The column identifier.
        /// </param>
        public void SetColumns(IList<DataSetColumnEntity> value)
        {
            foreach (var dataSetColumnEntity in value)
            {
                this.AddColumn(dataSetColumnEntity);    
            }
        }

        /// <summary>
        /// Adds the column.
        /// </summary>
        /// <param name="value">The value.</param>
        public void AddColumn(DataSetColumnEntity value)
        {
            var dataSetColumnEntity = this._columns.ToList().Find(item => item.EntityId == value.EntityId);
            this.PatchRequest.CreateUpdateDocumentForList("datasetColumn", dataSetColumnEntity,value);
            this._columns.Add(value);
        }

        public void RemoveColumn(string entityId)
        {
            var dataSetColumnEntity = this._columns.ToList().Find(item => item.EntityId == entityId);
            this.PatchRequest.CreateUpdateDocumentForList("datasetColumn", dataSetColumnEntity, null);
            this._columns.Remove(dataSetColumnEntity);
        }

        public void Clear()
        {
            var entities = this._columns.Select(x => x.EntityId);
            foreach (var entityId in entities)
            {
                this.RemoveColumn(entityId);
            }
        }

        public void ReplaceColumn(string entityId, DataSetColumnEntity value)
        {
            this.RemoveColumn(entityId);
            this.AddColumn(value);
        }
        /// <summary>
        /// Gets or sets the constant.
        /// </summary>
        /// <value>
        /// The constant.
        /// </value>
        [Obsolete("SDMX 3.0 allows multiple values; use ConstantValues property instead.")]
        public string ConstantValue
        {
            get 
            { 
                // backward compatibility 
                return this._constantValue ?? ConstantValues.FirstOrDefault(); 
            }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._constantValue, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ConstantValue), value);
                }
                this._constantValue = value;
            }
        }

        /// <summary>
        /// SDMX 3.0 allows for array of constant values.
        /// Use this property instead of <see cref="ConstantValue"/> for array of values.
        /// Use either this or the <see cref="ConstantValue"/> for single value.
        /// </summary>
        public ISet<string> ConstantValues { get; } = new HashSet<string>();

        /// <summary>
        /// Gets whether the component mapping has constant value set
        /// </summary>
        public bool HasConstantValue 
        { 
            get
            {
                return !string.IsNullOrWhiteSpace(ConstantValue) || ConstantValues.Any();
            } 
        }

        /// <summary>
        /// Gets or sets the default value.
        /// </summary>
        /// <value>
        /// The default value.
        /// </value>
        [Optional]
        [Obsolete("SDMX 3.0 allows multiple values; use DefaultValues property instead.")]
        public string DefaultValue
        {
            get 
            {
                // backward compatibility 
                return this._defaultValue ?? DefaultValues.FirstOrDefault(); 
            }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._defaultValue, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.DefaultValue), value);
                }
                this._defaultValue = value;
            }
        }

        /// <summary>
        /// SDMX 3.0 allows for array of constant values.
        /// Use this property instead of <see cref="DefaultValue"/> for array of values.
        /// Use either this or the <see cref="DefaultValue"/> for single value.
        /// </summary>
        public ISet<string> DefaultValues { get; } = new HashSet<string>();

        /// <summary>
        /// Gets whether the component mapping has default value set
        /// </summary>
        public bool HasDefaultValue
        {
            get
            {
                return !string.IsNullOrWhiteSpace(DefaultValue) || DefaultValues.Any();
            }
        }

        /// <summary>
        /// Gets or sets the transcoding identifier.
        /// </summary>
        /// <value>
        /// The transcoding identifier.
        /// </value>
        [Optional]
        public string TranscodingId
        {
            get { return this._transcodingId; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._transcodingId, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.TranscodingId), value);
                }
                this._transcodingId = value;
            }
        }
        /// <summary>
        /// Gets or sets the type. TODO change to Enum
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type
        {
            get { return this._type; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._type, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.Type), value);
                }
                this._type = value;
            }
        }

        /// <summary>
        /// Gets or sets the component identifier.
        /// </summary>
        /// <value>
        /// The component identifier.
        /// </value>
        [Optional]
        public Component Component
        {
            get { return this._component; }
            set
            {
                this.PatchRequest.CreateUpdateDocumentForComplexIdentifiableEntity(nameof(this.Component), this._component,value);
                this._component = value;
            }
        }

        /// <summary>
        /// Gets or sets the is metadata flag.
        /// </summary>
        /// <value>
        /// Is metadata flag.
        /// </value>
        public bool IsMetadata
        {
            get { return this._isMetadata; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._isMetadata, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.IsMetadata), value);
                }
                this._isMetadata = value;
            }
        }

        /// <summary>
        /// Gets or sets the metadata attribute SDMX ID
        /// </summary>
        [Optional]
        public string MetadataAttributeSdmxId
        {
            get { return IsMetadata ? Component?.ObjectId : null; }
            set
            {
                // this.PatchRequest.CreateUpdateDocumentForComplexIdentifiableEntity(nameof(this.Component), this._component, value);
                if (Component == null)
                {
                    Component = new Component();
                }
                Component.ObjectId = value;
                IsMetadata = !string.IsNullOrEmpty(value);

            }
        }

        public ComponentMappingType MappingType
        {
            get { return ComponentMappingType.Normal; }
        }

        public List<TranscodingScriptEntity> Script { get; set; }

        public bool HasTranscoding()
        {
            return !string.IsNullOrWhiteSpace(this.TranscodingId);
        }

        /// <summary>
        /// Creates a new shallow copy of this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>
        /// The <see cref="Entity"/> based instance. Note that <see cref="Entity.EntityId"/> and <see cref="Entity.ParentId"/> are set to null
        /// </returns>
        public override TEntity CreateDeepCopy<TEntity>()
        {
            var clone = base.CreateDeepCopy<TEntity>();
            var copy = clone as ComponentMappingEntity;
            if (copy != null)
            {
                copy._columns = _columns.Select(entity => entity.CreateShallowCopy<DataSetColumnEntity>()).ToList();
            }

            return clone;
        }
    }
}