﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Mapping.Api.Model
{
    /// <summary>
    /// DSDAnnotationEnum
    /// </summary>
    public enum DSDAnnotationEnum
    {
        /// <summary>
        /// The DSD on the fly
        /// </summary>
        DSDOnTheFly
    }
}
