// -----------------------------------------------------------------------
// <copyright file="NsiwsEntity.cs" company="EUROSTAT">
//   Date Created : 2021-08-10
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    /// <summary>
    /// NsiwsEntity
    /// </summary>
    public class NsiwsEntity:SimpleNameableEntity,IEndpointEntity
    {

        private string _url;
        private string _userName;
        private string _password;
        private string _technology;
        private bool _proxy;
        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        public string Url
        {
            get { return this._url; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._url, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.Url), value);
                }
                this._url = value;
            }
        }
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public string UserName
        {
            get { return this._userName; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._userName, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.UserName), value);
                }
                this._userName = value;
            }
        }
        /// <summary>
        /// Gets or sets the availability.
        /// </summary>
        /// <value>
        /// The availability.
        /// </value>
        public string Password
        {
            get { return this._password; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._password, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.Password), value);
                }
                this._password = value;
            }
        }

        /// <summary>
        /// Gets or sets the technology.
        /// </summary>
        /// <value>
        /// The technology.
        /// </value>
        public string Technology
        {
            get { return this._technology; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._technology, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.Technology), value);
                }
                this._technology = value;
            }
        }

        /// <summary>Gets or sets a value indicating whether this <see cref="NsiwsEntity"/> is proxy.</summary>
        /// <value>
        ///   <c>true</c> if proxy; otherwise, <c>false</c>.</value>
        public bool Proxy
        {
            get => this._proxy;
            set
            {
                if (this.ShouldCreateUpdateDocument(this._proxy, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.Proxy), value);
                }
                this._proxy = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public NsiwsEntity()
            : base(EntityType.Nsiws)
        {
        }
    }
}