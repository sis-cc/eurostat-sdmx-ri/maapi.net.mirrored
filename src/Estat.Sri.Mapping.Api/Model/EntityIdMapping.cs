// -----------------------------------------------------------------------
// <copyright file="EntityIdMapping.cs" company="EUROSTAT">
//   Date Created : 2019-9-13
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    public class EntityIdMapping
    {
        private readonly Dictionary<EntityType, Dictionary<string, string>> _entityIdMapByType = new Dictionary<EntityType, Dictionary<string, string>>();

        public void Add(EntityType entityType, string oldEntityId, string newEntityId)
        {
            if (!string.IsNullOrWhiteSpace(oldEntityId))
            {
                Dictionary<string, string> oldNewEntityMap;
                if (!_entityIdMapByType.TryGetValue(entityType, out oldNewEntityMap))
                {
                    oldNewEntityMap = new Dictionary<string, string>(StringComparer.Ordinal);
                    _entityIdMapByType.Add(entityType, oldNewEntityMap);
                }

                oldNewEntityMap.Add(oldEntityId, newEntityId);
            }
        }

        public string Get(EntityType entityType, string oldEntityId)
        {
            if (!string.IsNullOrWhiteSpace(oldEntityId))
            {
                Dictionary<string, string> oldNewEntityMap;
                if (_entityIdMapByType.TryGetValue(entityType, out oldNewEntityMap))
                {
                    if (oldNewEntityMap.TryGetValue(oldEntityId, out string newEntityId))
                    {
                        return newEntityId;
                    }
                }

            }
            return null;
        }
    }
}
