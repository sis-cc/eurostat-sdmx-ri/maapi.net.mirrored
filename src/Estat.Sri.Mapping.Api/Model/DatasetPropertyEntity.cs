// -----------------------------------------------------------------------
// <copyright file="DatasetPropertyEntity.cs" company="EUROSTAT">
//   Date Created : 2021-11-11
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    public class DatasetPropertyEntity : Entity
    {
        private string _orderByTimePeriodAsc;
        private string _orderByTimePeriodDesc;
        private string _crossApplyColumn;
        private bool? _crossApplySupported;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public DatasetPropertyEntity()
            : base(EntityType.DataSetProperty)
        {
        }


        /// <summary>
        /// Gets or sets the orderByTimePeriodAsc.
        /// </summary>
        /// <value>
        /// The comma separated list of order by columns optionally with sort order.
        /// </value>
        public string OrderByTimePeriodAsc
        {
            get => this._orderByTimePeriodAsc;
            set
            {
                if (this.ShouldCreateUpdateDocument(this._orderByTimePeriodAsc, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.OrderByTimePeriodAsc), value);
                }
                this._orderByTimePeriodAsc = value;
            }
        }


        /// <summary>
        /// Gets or sets the orderByTimePeriodDesc.
        /// </summary>
        /// <value>
        /// The comma separated list of order by columns optionally with sort order.
        /// </value>
        public string OrderByTimePeriodDesc
        {
            get => this._orderByTimePeriodDesc;
            set
            {
                if (this.ShouldCreateUpdateDocument(this._orderByTimePeriodDesc, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.OrderByTimePeriodDesc), value);
                }
                this._orderByTimePeriodDesc = value;
            }
        }


        /// <summary>
        /// Gets or sets the crossApplyColumn.
        /// </summary>
        /// <value>
        /// The name of column used at cross apply query
        /// </value>
        public string CrossApplyColumn
        {
            get => this._crossApplyColumn;
            set
            {
                if (this.ShouldCreateUpdateDocument(this._crossApplyColumn, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.CrossApplyColumn), value);
                }
                this._crossApplyColumn = value;
            }
        }


        /// <summary>
        /// Gets or sets the crossApplyColumn.
        /// </summary>
        /// <value>
        /// Flag indication.
        /// </value>
        public bool? CrossApplySupported
        {
            get => this._crossApplySupported;
            set
            {
                if (this.ShouldCreateUpdateDocument(this._crossApplySupported, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.CrossApplySupported), value);
                }
                this._crossApplySupported = value;
            }
        }
    }
}