﻿// -----------------------------------------------------------------------
// <copyright file="LocalCodeResults.cs" company="EUROSTAT">
//   Date Created : 2017-04-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// The local code results.
    /// </summary>
    public class LocalCodeResults : IPageableResults
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocalCodeResults"/> class.
        /// </summary>
        public LocalCodeResults()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalCodeResults"/> class.
        /// </summary>
        /// <param name="codes">The codes.</param>
        public LocalCodeResults(IList<IList<LocalCodeEntity>> codes)
        {
            this.LocalCodes = codes ?? new List<IList<LocalCodeEntity>>();
        }

        public string MappingStoreId { get; set; }

        /// <summary>
        /// Gets the related entities.
        /// </summary>
        /// <value>
        /// The related entities.
        /// </value>
        public IDictionary<EntityType, string> RelatedEntities { get; } = new Dictionary<EntityType, string>();

        /// <summary>
        /// Gets the dataset columns. EntityId and Name
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        public IList<DataSetColumnEntity> Columns { get; } = new List<DataSetColumnEntity>();

        /// <summary>
        /// Gets or sets the page.
        /// </summary>
        /// <value>
        /// The page.
        /// </value>
        public int Page { get; set; }

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public int Count => LocalCodes.Count;

        /// <summary>
        /// Gets or sets the total row count.
        /// </summary>
        /// <value>
        /// The total row count.
        /// </value>
        public int TotalRowCount { get; set; }

        /// <summary>
        /// Gets the Transcoding rules.
        /// </summary>
        /// <value>
        /// The rules.
        /// </value>
        public IList<IList<LocalCodeEntity>> LocalCodes { get; }

        /// <summary>
        /// Gets the error messages.
        /// </summary>
        /// <value>
        /// The error messages.
        /// </value>
        public IList<string> ErrorMessages { get; } = new List<string>();

        /// <summary>
        /// Adds the codes.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <exception cref="System.ArgumentException">Number of cells in row must much the number of column rows - row</exception>
        public void AddCodes(IList<LocalCodeEntity> row)
        {
            if (Columns.Count == 0)
            {
                throw new InvalidOperationException("No columns set yet");
            }

            if (row.Count != Columns.Count)
            {
                throw new ArgumentException("Number of cells in row must much the number of column rows", nameof(row));
            }

            this.LocalCodes.Add(row);
        }
    }
}