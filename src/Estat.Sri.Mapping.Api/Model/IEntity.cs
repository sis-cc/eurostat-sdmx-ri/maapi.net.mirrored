﻿// -----------------------------------------------------------------------
// <copyright file="IEntity.cs" company="EUROSTAT">
//   Date Created : 2017-02-02
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Model
{
    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// The base Entity interface. It contains common properties for all entities
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Gets or sets the Entity identifier. This could be the Primary Key Value from the database
        /// In some stores this could be a number but in order to support stores where this is not a number  
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        string EntityId { get; set; }

        /// <summary>
        /// Gets or sets the parent identifier. Again this represents the corresponding <see cref="EntityId"/> of the Parent
        /// </summary>
        /// <value>
        /// The parent identifier.
        /// </value>
        string ParentId { get; set; }

        /// <summary>
        /// Gets or sets the Mapping Assistant store identifier.
        /// </summary>
        /// <value>
        /// The store identifier.
        /// </value>
        string StoreId { get; set; }

        /// <summary>
        /// Gets the type of entity.
        /// </summary>
        /// <value>
        /// The type of entity.
        /// </value>
        EntityType TypeOfEntity { get; }

        /// <summary>
        /// Gets or sets the owner.
        /// </summary>
        /// <value>
        /// The owner.
        /// </value>
        string Owner { get; set; }

        /// <summary>
        /// Gets the patch request.
        /// </summary>
        /// <value>
        /// The patch request.
        /// </value>
        PatchRequest PatchRequest { get; }

        /// <summary>
        /// Creates a new shallow copy of this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>
        /// The <see cref="Entity"/> based instance. Note that <see cref="Entity.EntityId"/> and <see cref="Entity.ParentId"/> are set to null
        /// </returns>
        TEntity CreateDeepCopy<TEntity>() where TEntity : IEntity;
    }
}