// -----------------------------------------------------------------------
// <copyright file="ConnectionEntity.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// The connection entity.
    /// </summary>
    public class ConnectionEntity : SimpleNameableEntity, IConnectionEntity
    {
        private string _dbName;
        private string _databaseVendorType;
        private string _subType;

        protected ConnectionEntity(EntityType entityType) : base(entityType)
        {
            this.Settings = new Dictionary<string, IConnectionParameterEntity>(StringComparer.OrdinalIgnoreCase);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionEntity"/> class.
        /// </summary>
        public ConnectionEntity()
            : this(EntityType.DdbConnectionSettings)
        {
        }

        /// <summary>
        /// Gets or sets the name of the database.
        /// </summary>
        /// <value>
        /// The name of the database.
        /// </value>
        public string DbName
        {
            get { return this._dbName; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._dbName, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.DbName), value);
                }
                this._dbName = value;
            }
        }

        /// <summary>
        /// Gets or sets the type of the database.
        /// </summary>
        /// <value>
        /// The type of the database.
        /// </value>
        public string DatabaseVendorType
        {
            get { return this._databaseVendorType; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._databaseVendorType, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.DatabaseVendorType), value);
                }
                this._databaseVendorType = value;
            }
        }

        /// <summary>
        /// Gets the advanced settings.
        /// </summary>
        /// <value>
        /// The advanced settings.
        /// </value>
        public IDictionary<string, IConnectionParameterEntity> Settings { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has sub type.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has sub type; otherwise, <c>false</c>.
        /// </value>
        public bool HasSubType { get; set; } = false;

        /// <summary>
        /// Gets or sets the type of the sub.
        /// </summary>
        /// <value>
        /// The type of the sub.
        /// </value>
        public string SubType
        {
            get { return this._subType; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._subType, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.SubType), value);
                }
                this._subType = value;
            }
        }

        /// <summary>
        /// Gets the sub type list.
        /// </summary>
        /// <value>
        /// The sub type list.
        /// </value>
        public IList<string> SubTypeList { get; } = new List<string>();

        /// <summary>
        /// Gets or sets the permissions.
        /// </summary>
        /// <value>
        /// The permissions.
        /// </value>
        public Dictionary<string, string> Permissions { get; set; }
    }
}