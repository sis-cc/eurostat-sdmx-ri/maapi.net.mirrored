using System.Security.Cryptography.X509Certificates;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    using System;
    using System.Linq;

    public class TimeTranscodingEntity : Entity
    {
        private string _frequency;
        private bool _isDateTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public TimeTranscodingEntity()
            : base(EntityType.TimeTranscoding)
        {
        }

        /// <summary>
        /// Gets or sets the frequency.
        /// </summary>
        /// <value>
        /// The frequency.
        /// </value>
        public string Frequency
        {
            get { return this._frequency; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._frequency, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.Frequency), value);
                }
                this._frequency = value;
            }
        }

        /// <summary>
        /// Gets or sets the data set column.
        /// </summary>
        /// <value>
        /// The data set column.
        /// </value>
        public DataSetColumnEntity DateColumn { get; set; }

        public TimeTranscoding Year { get; set; }

        public PeriodTimeTranscoding Period { get; set; }

        public bool IsDateTime
        {
            get { return this._isDateTime; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._isDateTime, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.IsDateTime), value);
                }
                this._isDateTime = value;
            }
        }

        /// <summary>
        /// Creates a new shallow copy of this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>
        /// The <see cref="Entity"/> based instance. Note that <see cref="Entity.EntityId"/> and <see cref="Entity.ParentId"/> are set to null
        /// </returns>
        public override TEntity CreateDeepCopy<TEntity>()
        {
            var clone = base.CreateDeepCopy<TEntity>();
            var timeTranscoding = AsSpecificEntity<TimeTranscodingEntity>(clone);

            if (this.Period != null)
            {
                timeTranscoding.Period = new PeriodTimeTranscoding();
                timeTranscoding.Period.Start = Period.Start;
                timeTranscoding.Period.Length = Period.Length;
                timeTranscoding.Period.Column = Period.Column.CreateDeepCopy<DataSetColumnEntity>();
                timeTranscoding.Period.Rules =
                    Period.Rules.Select(entity => entity.CreateDeepCopy<TranscodingRuleEntity>()).ToList();
            }

            return clone;
        }
    }
}