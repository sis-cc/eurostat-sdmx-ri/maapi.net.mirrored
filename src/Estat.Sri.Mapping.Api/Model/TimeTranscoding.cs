namespace Estat.Sri.Mapping.Api.Model
{
    public class TimeTranscoding
    {
        public DataSetColumnEntity Column { get; set; }

        public int? Start { get; set; }

        public int? Length { get; set; }
    }
}