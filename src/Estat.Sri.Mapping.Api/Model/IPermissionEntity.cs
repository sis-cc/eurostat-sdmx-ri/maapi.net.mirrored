using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model
{
    public interface IPermissionEntity : IEntity
    {
        Dictionary<string, string> Permissions { get; set; }
    }
}