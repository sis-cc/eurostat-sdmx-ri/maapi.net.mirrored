﻿// -----------------------------------------------------------------------
// <copyright file="RequestScopeModel.cs" company="EUROSTAT">
//   Date Created : 2017-09-25
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    /// <summary>
    /// The request scope model.
    /// </summary>
    public class RequestScopeModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether [disable authorization].
        /// </summary>
        /// <value><c>true</c> if [disable authorization]; otherwise, <c>false</c>.</value>
        public bool AuthorizationDisabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [record only root].
        /// </summary>
        /// <value><c>true</c> if [record only root]; otherwise, <c>false</c>.</value>
        public bool RecordOnlyRoot { get; set; }

        /// <summary>
        /// Gets or sets the last recorded entity.
        /// </summary>
        /// <value>The last recorded entity.</value>
        public IEntity LastRecordedEntity { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it should authorize only root entities
        /// </summary>
        /// <value>
        ///   <c>true</c> if it should authorize only root entities; otherwise, <c>false</c>.
        /// </value>
        public bool AuthorizeOnlyRoot { get; set; }
    }
}