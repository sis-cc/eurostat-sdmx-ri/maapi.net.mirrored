// -----------------------------------------------------------------------
// <copyright file="ColumnDescriptionSourceEntity.cs" company="EUROSTAT">
//   Date Created : 2017-04-07
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Globalization;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Sri.Mapping.Api.Model
{
    public class DataSourceEntity : Entity
    {
        private string _dataUrl;
        private string _wsdlUrl;
        private string _wadlUrl;
        private bool _isRest;
        private bool _isSimple;
        private bool _isWs;

        public DataSourceEntity()
            : base(EntityType.DataSource)
        {
        }
        public string DataUrl { 
            get { return this._dataUrl; } 
            set { if (this.ShouldCreateUpdateDocument(this._dataUrl, value))
                    {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.DataUrl), value);
                    }
                _dataUrl = value;
                } 
        }
        public string WSDLUrl
        {
            get { return this._wsdlUrl; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._wsdlUrl, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.WSDLUrl), value);
                }
                _wsdlUrl = value;
            }
        }
        public string WADLUrl
        {
            get { return this._wadlUrl; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._wadlUrl, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.WADLUrl), value);
                }
                _wadlUrl = value;
            }
        }

        public bool IsSimple
        {
            get { return this._isSimple; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._isSimple, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.IsSimple), value);
                }
                _isSimple = value;
            }
        }
        public bool IsRest
        {
            get { return this._isRest; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._isRest, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.IsRest), value);
                }
                _isRest = value;
            }
        }
        public bool IsWs
        {
            get { return this._isWs; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._isWs, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.IsWs), value);
                }
                _isWs = value;
            }
        }
    }
}