// -----------------------------------------------------------------------
// <copyright file="TimeRelatedColumnInfo.cs" company="EUROSTAT">
//   Date Created : 2018-3-28
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Model.AdvancedTime
{
    /// <summary>
    /// A class to store the configuration of a Column
    /// </summary>
    /// <remarks>
    /// Note a class  <see cref="TimeTranscoding"/> exists but the name is not correct.
    /// </remarks>
    //// TODO to merge with after identifying if <c>TimeTranscoding</c> can be renamed without side affects
    public class TimeRelatedColumnInfo
    {
        /// <summary>
        /// Gets or sets the column.
        /// </summary>
        /// <value>The column.</value>
        public DataSetColumnEntity Column { get; set; }

        /// <summary>
        /// Gets or sets the start.
        /// </summary>
        /// <value>The start.</value>
        public int? Start { get; set; }

        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        /// <value>The length.</value>
        public int? Length { get; set; }
    }
}
