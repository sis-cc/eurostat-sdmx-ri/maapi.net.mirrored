// -----------------------------------------------------------------------
// <copyright file="PeriodCodeMap.cs" company="EUROSTAT">
//   Date Created : 2018-3-28
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Model.AdvancedTime
{
    /// <summary>
    /// It stores the mapping between a local code and SDMX time period
    /// </summary>
    public class PeriodCodeMap
    {
        /// <summary>
        /// Gets or sets the SDMX period code.
        /// </summary>
        //// TODO should this be an enum or a set of classes implementing the same interface ?
        public string SdmxPeriodCode { get; set; }

        /// <summary>
        /// Gets or sets the local period code.
        /// </summary>
        public string LocalPeriodCode { get; set; }
    }
}
