// -----------------------------------------------------------------------
// <copyright file="TimeParticleConfiguration.cs" company="EUROSTAT">
//   Date Created : 2018-4-13
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Model.AdvancedTime
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// The time particle configuration.
    /// </summary>
    //// TODO Abstract this class and make an sub-class for each DisseminationDatabaseTimeFormat specialization
    public class TimeParticleConfiguration
    {
        /// <summary>
        /// Gets or sets the date column.
        /// </summary>
        public DataSetColumnEntity DateColumn { get; set; }

        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        public TimeRelatedColumnInfo Year { get; set; }

        /// <summary>
        /// Gets or sets the period.
        /// </summary>
        public PeriodTimeRelatedColumnInfo Period { get; set; }

        /// <summary>
        /// Gets or sets the format.
        /// </summary>
        public DisseminationDatabaseTimeFormat Format { get; set; }

        /// <summary>
        /// Creates an instance with date format.
        /// </summary>
        /// <param name="dateColumn">The column entity.</param>
        /// <returns>TimeParticleConfiguration.</returns>
        /// <exception cref="System.ArgumentNullException">columnEntity</exception>
        public static TimeParticleConfiguration CreateDateFormat(DataSetColumnEntity dateColumn)
        {
            if (dateColumn == null)
            {
                throw new System.ArgumentNullException(nameof(dateColumn));
            }

            return new TimeParticleConfiguration() { DateColumn = dateColumn, Format = DisseminationDatabaseTimeFormat.TimestampOrDate };
        }

        /// <summary>
        /// Creates an instance with ISO 8601 format.
        /// </summary>
        /// <param name="iso8601CompatibleColumn">The iso8601 compatible column.</param>
        /// <returns>TimeParticleConfiguration.</returns>
        /// <exception cref="System.ArgumentNullException">columnEntity</exception>
        public static TimeParticleConfiguration CreateIsoFormat(DataSetColumnEntity iso8601CompatibleColumn)
        {
            if (iso8601CompatibleColumn == null)
            {
                throw new System.ArgumentNullException(nameof(iso8601CompatibleColumn));
            }

            return new TimeParticleConfiguration() { DateColumn = iso8601CompatibleColumn, Format = DisseminationDatabaseTimeFormat.Iso8601Compatible };
        }

        /// <summary>
        /// Creates an instance with annual periodicity
        /// </summary>
        /// <param name="yearColumn">The year column.</param>
        /// <param name="selectionStart">The selection start.</param>
        /// <param name="selectionLength">Length of the selection.</param>
        /// <returns>TimeParticleConfiguration.</returns>
        /// <exception cref="System.ArgumentNullException">yearColumn</exception>
        public static TimeParticleConfiguration CreateAnnual(DataSetColumnEntity yearColumn, int selectionStart, int selectionLength)
        {
            if (yearColumn == null)
            {
                throw new System.ArgumentNullException(nameof(yearColumn));
            }

            TimeRelatedColumnInfo timeRelatedColumnInfo = new TimeRelatedColumnInfo() { Column = yearColumn, Start = selectionStart, Length = selectionLength };
            return new TimeParticleConfiguration() { Year = timeRelatedColumnInfo, Format = DisseminationDatabaseTimeFormat.Annual };
        }

        /// <summary>
        /// Creates an instance with annual periodicity
        /// </summary>
        /// <param name="yearColumn">The year column.</param>
        /// <param name="selectionStart">The selection start.</param>
        /// <param name="selectionLength">Length of the selection.</param>
        /// <param name="databaseTimeFormat">The database time format.</param>
        /// <param name="periodConfiguration">The period configuration.</param>
        /// <returns>TimeParticleConfiguration.</returns>
        /// <exception cref="System.ArgumentNullException">yearColumn</exception>
        public static TimeParticleConfiguration CreateWithPeriod(DataSetColumnEntity yearColumn, int selectionStart, int selectionLength, DisseminationDatabaseTimeFormat databaseTimeFormat, PeriodTimeRelatedColumnInfo periodConfiguration)
        {
            if (yearColumn == null)
            {
                throw new ArgumentNullException(nameof(yearColumn));
            }

            if (periodConfiguration == null)
            {
                throw new ArgumentNullException(nameof(periodConfiguration));
            }

            if (databaseTimeFormat.IsOneOf(DisseminationDatabaseTimeFormat.Annual, DisseminationDatabaseTimeFormat.Iso8601Compatible, DisseminationDatabaseTimeFormat.TimestampOrDate))
            {
                throw new ArgumentException("Format doesn't have a period", nameof(databaseTimeFormat));
            }

            TimeRelatedColumnInfo timeRelatedColumnInfo = new TimeRelatedColumnInfo() { Column = yearColumn, Start = selectionStart, Length = selectionLength };

            return new TimeParticleConfiguration() { Year = timeRelatedColumnInfo, Format = databaseTimeFormat, Period = periodConfiguration };
        }

        /// <summary>
        /// Adds the rule to <see cref="Period"/>.
        /// </summary>
        /// <param name="timeFormat">The time format.</param>
        /// <param name="sdmxPeriod">The SDMX period.</param>
        /// <param name="localCode">The local code.</param>
        /// <exception cref="MissingInformationException">Period is not set</exception>
        public void AddRule(TimeFormatEnumType timeFormat, int sdmxPeriod, string localCode)
        {
            if (Period != null)
            {
                Period.AddRule(timeFormat, sdmxPeriod, localCode);
            }
            else
            {
                throw new MissingInformationException("Period is not set");
            }
        }

        public IEnumerable<DataSetColumnEntity> GetColumns()
        {
            if (DateColumn != null)
            {
                yield return DateColumn;
                yield break;
            }

            if (Year?.Column != null)
            {
                yield return Year.Column;
            }

            if (Period?.Column != null)
            {
                yield return Period.Column;
            }
        }
    }
}