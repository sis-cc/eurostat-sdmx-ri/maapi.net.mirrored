// -----------------------------------------------------------------------
// <copyright file="PeriodTimeRelatedColumnInfo.cs" company="EUROSTAT">
//   Date Created : 2018-3-28
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Model.AdvancedTime
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Util.Date;

    /// <summary>
    /// The period time related column info.
    /// </summary>
    public class PeriodTimeRelatedColumnInfo : TimeRelatedColumnInfo
    {
        private TimeFormatEnumType _lastTimeFormat;
        private IPeriodicity _periodicity;
        private string _periodicityFormat;

        /// <summary>
        /// Gets the rules.
        /// </summary>
        public IList<PeriodCodeMap> Rules { get; } = new List<PeriodCodeMap>();

        /// <summary>
        /// Add a rule with the specified <paramref name="period" /> and <paramref name="localCode" />
        /// </summary>
        /// <param name="timeFormat">The time format.</param>
        /// <param name="period">The period.</param>
        /// <param name="localCode">The local code.</param>
        /// <exception cref="ArgumentException">Period must be between 1 and time format specific maximum count - period</exception>
        public void AddRule(TimeFormatEnumType timeFormat, int period, string localCode)
        {
            ValidateAndSetup(timeFormat);

            if (period < 1 || period > _periodicity.PeriodCount)
            {
                throw new ArgumentException("Period must be between 1 and time format specific maximum count", nameof(period));
            }

            var transcodingRule = new PeriodCodeMap();
            transcodingRule.SdmxPeriodCode = period.ToString(_periodicityFormat, CultureInfo.InvariantCulture);

            transcodingRule.LocalPeriodCode = localCode;
            Rules.Add(transcodingRule);
        }

        /// <summary>
        /// Add a rule with the specified <paramref name="periodId"/> and <paramref name="localCode"/>
        /// </summary>
        /// <param name="periodId">
        /// The period id.
        /// </param>
        /// <param name="localCode">
        /// The local code.
        /// </param>
        public void AddRule(string periodId, string localCode)
        {
            var transcodingRule = new PeriodCodeMap();
            transcodingRule.SdmxPeriodCode = periodId;
            transcodingRule.LocalPeriodCode = localCode;
            Rules.Add(transcodingRule);
        }

        /// <summary>
        /// Validates the and setup.
        /// </summary>
        /// <param name="timeFormat">The time format.</param>
        /// <exception cref="ArgumentException">
        /// Time Format have at least one period - timeFormat
        /// or
        /// Time Format must remain the same through out the instance - timeFormat
        /// </exception>
        private void ValidateAndSetup(TimeFormatEnumType timeFormat)
        {
            if (_lastTimeFormat == TimeFormatEnumType.Null)
            {
                var periodicity = PeriodicityFactory.Create(timeFormat);
                if (periodicity.PeriodCount < 1)
                {
                    throw new ArgumentException("Time Format have at least one period", nameof(timeFormat));
                }
                _periodicityFormat = periodicity.Format;
                _periodicity = periodicity;
                _lastTimeFormat = timeFormat;
            }
            else if (_lastTimeFormat != timeFormat)
            {
                throw new ArgumentException("Time Format must remain the same through out the instance", nameof(timeFormat));
            }
        }
    }
}