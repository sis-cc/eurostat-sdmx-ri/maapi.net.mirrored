// -----------------------------------------------------------------------
// <copyright file="TimeFormatConfiguration.cs" company="EUROSTAT">
//   Date Created : 2018-4-13
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Model.AdvancedTime
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Exceptions;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The time format configuration.
    /// </summary>
    public class TimeFormatConfiguration
    {
        /// <summary>
        /// The start configuration
        /// </summary>
        private TimeParticleConfiguration _startConfig;

        /// <summary>
        /// The end configuration
        /// </summary>
        private TimeParticleConfiguration _endConfig;

        /// <summary>
        /// The duration map
        /// </summary>
        private DurationMappingEntity _durationMap;

        /// <summary>
        /// Gets or sets the criteria value.
        /// </summary>
        public string CriteriaValue { get; set; }

        /// <summary>
        /// Gets or sets the output format.
        /// </summary>
        public TimeFormat OutputFormat { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance has particle configuration.
        /// </summary>
        /// <value><c>true</c> if this instance has particle configuration; otherwise, <c>false</c>.</value>
        public bool HasParticleConfiguration
        {
            get
            {
                if (_startConfig != null)
                {
                    return true;
                }

                if (_endConfig != null && _durationMap != null)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the start configuration.
        /// </summary>
        /// <value>The start configuration.</value>
        public TimeParticleConfiguration StartConfig
        {
            get
            {
                return _startConfig;
            }
        }

        /// <summary>
        /// Gets the end configuration.
        /// </summary>
        /// <value>The end configuration.</value>
        public TimeParticleConfiguration EndConfig
        {
            get
            {
                return _endConfig;
            }
        }

        /// <summary>
        /// Gets the duration map.
        /// </summary>
        /// <value>The duration map.</value>
        public DurationMappingEntity DurationMap
        {
            get
            {
                return _durationMap;
            }
        }

        /// <summary>
        /// Sets the configuration for start/duration.
        /// </summary>
        /// <param name="startConfig">The start configuration.</param>
        /// <param name="durationMap">The duration map.</param>
        public void SetConfiguration(TimeParticleConfiguration startConfig, DurationMappingEntity durationMap)
        {
            Reset();
            _startConfig = startConfig;
            _durationMap = durationMap;
        }

        /// <summary>
        /// Sets the configuration for start/end.
        /// </summary>
        /// <param name="startConfig">The start configuration.</param>
        /// <param name="endConfig">The end configuration.</param>
        public void SetConfiguration(TimeParticleConfiguration startConfig, TimeParticleConfiguration endConfig)
        {
            Reset();
            _startConfig = startConfig;
            _endConfig = endConfig;
        }

        /// <summary>
        /// Sets the configuration for ISO 8601 start
        /// </summary>
        /// <param name="startConfig">The start configuration.</param>
        public void SetConfiguration(TimeParticleConfiguration startConfig)
        {
            Reset();
            _startConfig = startConfig;
        }

        /// <summary>
        /// Sets the configuration for ISO 8601 start
        /// </summary>
        /// <param name="isoTimeRangeColumn">The iso time range column.</param>
        public void SetConfiguration(DataSetColumnEntity isoTimeRangeColumn)
        {
            Reset();
            var startConfig = new TimeParticleConfiguration();
            startConfig.DateColumn = isoTimeRangeColumn;
            startConfig.Format = Constant.DisseminationDatabaseTimeFormat.Iso8601Compatible;
            _startConfig = startConfig;
        }

        /// <summary>
        /// Sets the configuration for duration/end.
        /// </summary>
        /// <param name="durationMap">The duration map.</param>
        /// <param name="endConfig">The end configuration.</param>
        public void SetConfiguration(DurationMappingEntity durationMap, TimeParticleConfiguration endConfig)
        {
            Reset();
            _durationMap = durationMap;
            _endConfig = endConfig;
        }

        /// <summary>
        /// Sets the configuration.
        /// </summary>
        /// <param name="durationMap">The duration map.</param>
        /// <param name="startConfig">The start configuration.</param>
        /// <param name="endConfig">The end configuration.</param>
        /// <returns>ITimeConfiguration.</returns>
        /// <exception cref="MissingInformationException">Specified configuration not supported</exception>
        public void SetConfiguration(DurationMappingEntity durationMap, TimeParticleConfiguration startConfig, TimeParticleConfiguration endConfig)
        {
            if (durationMap != null)
            {
                if (startConfig != null && endConfig == null)
                {
                    SetConfiguration(startConfig, durationMap);
                }
                else if (endConfig != null && startConfig == null)
                {
                    SetConfiguration(durationMap, endConfig);
                }
                else
                {
                    throw new MissingInformationException("Specified configuration with duration mapping not supported");
                }
            }
            else
            {
                if (startConfig != null && endConfig != null)
                {
                    SetConfiguration(startConfig, endConfig);
                }
                else if (startConfig != null)
                {
                    SetConfiguration(startConfig);
                }
                else
                {
                    throw new MissingInformationException("Specified configuration without duration mapping not supported");
                }
            }
        }

        public IEnumerable<DataSetColumnEntity> GetColumns()
        {
            if (StartConfig != null)
            {
                foreach (var column in StartConfig.GetColumns())
                {
                    yield return column;
                }
            }

            if (EndConfig != null)
            {
                foreach (var column in EndConfig.GetColumns())
                {
                    yield return column;
                }
            }

            if (DurationMap?.Column != null)
            {
                yield return DurationMap.Column;
            }
        }

        private void Reset()
        {
            _durationMap = null;
            _startConfig = null;
            _endConfig = null;
        }
    }
}