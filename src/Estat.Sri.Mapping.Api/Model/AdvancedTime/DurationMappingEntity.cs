// -----------------------------------------------------------------------
// <copyright file="DurationMappingEntity.cs" company="EUROSTAT">
//   Date Created : 2022-08-03
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.Api.Model.AdvancedTime
{
    /// <summary>
    /// The Duration mapping entity, used in advanced time transcoding to store any mapping related
    /// to ISO 8601 duration with constant or DDB columns
    /// </summary>
    public class DurationMappingEntity : Entity, IMappingEntity
    {
        /// <summary>
        /// Gets or set the constant value. <see cref="Column"/> must be <c>null</c>
        /// </summary>
        public string ConstantValue { get; set; }
        
        /// <summary>
        /// Gets or sets the default value. <see cref="Column"/> must not be <c>null</c>
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// Gets or set the column used for the Duration Mapping entity
        /// Only the name is important
        /// </summary>
        public DataSetColumnEntity Column { get; set; }

        /// <summary>
        /// Gets the Mapping Type
        /// </summary>
        public ComponentMappingType MappingType
        {
            get => ComponentMappingType.Duration;
        }

        /// <summary>
        /// Gets the transcoding for duration if any
        /// </summary>
        public Dictionary<string, string> TranscodingRules { get; } = new Dictionary<string, string>(StringComparer.Ordinal);
        
        /// <summary>
        /// Gets a value indicating if there is transcoding
        /// </summary>
        public bool HasTranscoding() => Column != null && TranscodingRules.Count > 0;

        public IList<DataSetColumnEntity> GetColumns()
        {
            if (this.Column != null)
            {
                return new List<DataSetColumnEntity>() { this.Column };
            }

            return new Collection<DataSetColumnEntity>();
        }
        

        public DurationMappingEntity() : base(EntityType.DurationMapping)
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ComponentMappingEntity AsComponentMapping()
        {
            ComponentMappingEntity componentMappingEntity = new ComponentMappingEntity();
            componentMappingEntity.EntityId = EntityId;
            // there is no parent id here
            componentMappingEntity.Type = ComponentMappingType.Duration.ToString("G");
            componentMappingEntity.ConstantValue = ConstantValue;
            componentMappingEntity.DefaultValue = DefaultValue;
            if (Column != null)
            {
                componentMappingEntity.AddColumn(Column);
            }

            if (HasTranscoding())
            {
                // there is no entity id here for transcoding rules
                componentMappingEntity.TranscodingId = EntityId;
            }

            return componentMappingEntity;
        }
    }
}