// -----------------------------------------------------------------------
// <copyright file="TimeTranscodingAdvancedEntity.cs" company="EUROSTAT">
//   Date Created : 2018-4-13
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Utils;

namespace Estat.Sri.Mapping.Api.Model.AdvancedTime
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Model for Time dimension Mapping. It includes both with or without Time Transcoding Or Preformated, this includes Time Ranges and selecting the output format regardless of the frequency columns
    /// </summary>
    public class TimeTranscodingAdvancedEntity
    {
        /// <summary>
        /// Gets or sets the criteria column.
        /// </summary>
        public DataSetColumnEntity CriteriaColumn { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether <see cref="CriteriaColumn"/> oe <see cref="FrequencyDimension"/>
        /// should be used
        /// </summary>
        public bool IsFrequencyDimension { get; set; }
        
        /// <summary>
        /// The Frequency component ID
        /// It must not be set unless <see cref="CriteriaColumn"/> is null
        /// </summary>
        public string FrequencyDimension { get; set; }

        /// <summary>
        /// Gets the time format configurations. The key are values of the <see cref="CriteriaColumn"/>
        /// </summary>
        public IDictionary<string, TimeFormatConfiguration> TimeFormatConfigurations { get; } = new Dictionary<string, TimeFormatConfiguration>(StringComparer.Ordinal);

        public void Add(TimeFormatConfiguration formatConfiguration)
        {
            if (formatConfiguration == null)
            {
                throw new ArgumentNullException(nameof(formatConfiguration));
            }

            if (formatConfiguration.CriteriaValue == null)
            {
                throw new ArgumentException("Criteria value is null ",nameof(formatConfiguration));
            }

            TimeFormatConfigurations.Add(formatConfiguration.CriteriaValue, formatConfiguration);
        }

        /// <summary>
        /// Convert this instance of <see cref="TimeTranscodingAdvancedEntity"/> to old simple time transcoding
        /// </summary>
        /// <returns>The old frequency based transcoding</returns>
        public IList<TimeTranscodingEntity> AsOldTimeTranscoding()
        {
            return TimeTranscodingConversionHelper.Convert(this);
        }
    }
}