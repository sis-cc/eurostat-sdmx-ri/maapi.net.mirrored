// -----------------------------------------------------------------------
// <copyright file="Contact.cs" company="EUROSTAT">
//   Date Created : 2017-03-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Estat.Sri.Mapping.Api.Model
{
    public class Contact
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Contact" /> class.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="department">Department.</param>
        /// <param name="role">Role.</param>
        /// <param name="email">Email.</param>
        /// <param name="additionalProperties">AdditionalProperties.</param>
        public Contact(string name = default(string), string department = default(string), string role = default(string), List<string> email = default(List<string>), List<string> additionalProperties = default(List<string>))
        {
            this.Name = name;
            this.Department = department;
            this.Role = role;
            this.Email = email;
            this.AdditionalProperties = additionalProperties;
        }

        /// <summary>
        /// Gets or Sets AdditionalProperties
        /// </summary>
        public List<string> AdditionalProperties { get; set; }

        /// <summary>
        /// Gets or Sets Department
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Gets or Sets Email
        /// </summary>
        public List<string> Email { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Role
        /// </summary>
        public string Role { get; set; }

        public static bool operator ==(Contact left, Contact right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Contact left, Contact right)
        {
            return !Equals(left, right);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != this.GetType())
            {
                return false;
            }
            return this.Equals((Contact) obj);
        }

        /// <summary>
        /// Returns true if Contact instances are equal
        /// </summary>
        /// <param name="other">Instance of Contact to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Contact other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return
                (
                    this.Name == other.Name ||
                    this.Name != null &&
                    this.Name.Equals(other.Name)
                    ) &&
                (
                    this.Department == other.Department ||
                    this.Department != null &&
                    this.Department.Equals(other.Department)
                    ) &&
                (
                    this.Role == other.Role ||
                    this.Role != null &&
                    this.Role.Equals(other.Role)
                    ) &&
                (
                    this.Email == other.Email ||
                    this.Email != null &&
                    this.Email.SequenceEqual(other.Email)
                    ) &&
                (
                    this.AdditionalProperties == other.AdditionalProperties ||
                    this.AdditionalProperties != null &&
                    this.AdditionalProperties.SequenceEqual(other.AdditionalProperties)
                    );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                var hash = 41;

                // Suitable nullity checks etc, of course :)
                if (this.Name != null)
                {
                    hash = hash*59 + this.Name.GetHashCode();
                }
                if (this.Department != null)
                {
                    hash = hash*59 + this.Department.GetHashCode();
                }
                if (this.Role != null)
                {
                    hash = hash*59 + this.Role.GetHashCode();
                }
                if (this.Email != null)
                {
                    hash = hash*59 + this.Email.GetHashCode();
                }
                if (this.AdditionalProperties != null)
                {
                    hash = hash*59 + this.AdditionalProperties.GetHashCode();
                }
                return hash;
            }
        }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Contact {\n");
            sb.Append("  Name: ").Append(this.Name).Append("\n");
            sb.Append("  Department: ").Append(this.Department).Append("\n");
            sb.Append("  Role: ").Append(this.Role).Append("\n");
            sb.Append("  Email: ").Append(this.Email).Append("\n");
            sb.Append("  AdditionalProperties: ").Append(this.AdditionalProperties).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
    }
}