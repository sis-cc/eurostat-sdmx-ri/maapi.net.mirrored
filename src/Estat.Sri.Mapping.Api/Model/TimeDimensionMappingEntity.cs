// -----------------------------------------------------------------------
// <copyright file="TimeDimensionMapping.cs" company="EUROSTAT">
//   Date Created : 2022-07-31
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Exceptions;
using Estat.Sri.Mapping.Api.Model.AdvancedTime;
using Estat.Sri.Mapping.Api.Utils;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

namespace Estat.Sri.Mapping.Api.Model
{

    /// <summary>
    /// Model for Time dimension Mapping. It includes both with or without Time Transcoding Or Preformated,
    /// this includes Time Ranges and selecting the output format regardless of the frequency columns
    /// </summary>
    public class TimeDimensionMappingEntity : Entity, IMappingEntity
    {
        private string _constantValue;
        private string _defaultValue;
        private string _simpleMappingColumn;

        /// <summary>
        /// 
        /// </summary>
        public TimeDimensionMappingEntity() : base(EntityType.TimeMapping)
        {
        }

        /// <summary>
        /// The Time dimension type;
        /// </summary>
        public TimeDimensionMappingType TimeMappingType { get; set; }

        /// <summary>
        /// Gets or sets the column Name used for 1 to 1 without transcoding or pre-formatted 
        /// </summary>
        public string SimpleMappingColumn
        {
            get => _simpleMappingColumn;
            set
            {
                if (this.ShouldCreateUpdateDocument(this._simpleMappingColumn, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.SimpleMappingColumn), value);
                }

                _simpleMappingColumn = value;
            }
        }

        /// <summary>
        /// Gets or sets the preformatted for 
        /// </summary>
        public TimePreFormattedEntity PreFormatted { get; set; }

        /// <summary>
        /// Time transcoding
        /// </summary>
        public TimeTranscodingAdvancedEntity Transcoding { get; set; }

        /// <summary>
        /// Set the advanced transcoding from simple time transcoding.
        /// This method exists for backwards compatibility with existing code
        /// It will first convert the <paramref name="oldTImeTranscoding"/> to <see cref="TimeTranscodingAdvancedEntity"/>
        /// and then set the <see cref="Transcoding"/>
        /// </summary>
        /// <param name="oldTImeTranscoding">The old simple time transcoding</param>
        /// <param name="frequencyDimensionId">The frequency dimension ID; in the vast majority of cases it is <c>FREQ</c>
        /// It can be taken from the <c>Id</c> of <see cref="IDataStructureObject.FrequencyDimension"/>
        /// </param>
        public void ConvertFromTranscoding(IList<TimeTranscodingEntity> oldTImeTranscoding, string frequencyDimensionId)
        {
            Transcoding = TimeTranscodingConversionHelper.Convert(oldTImeTranscoding, frequencyDimensionId);
        }

        /// <summary>
        /// Gets or sets the constant.
        /// </summary>
        /// <value>
        /// The constant.
        /// </value>
        public string ConstantValue
        {
            get => this._constantValue;
            set
            {
                if (this.ShouldCreateUpdateDocument(this._constantValue, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ConstantValue), value);
                }

                this._constantValue = value;
            }
        }

        /// <summary>
        /// Gets or sets the default value.
        /// </summary>
        /// <value>
        /// The default value.
        /// </value>
        [Optional]
        public string DefaultValue
        {
            get { return this._defaultValue; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._defaultValue, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.DefaultValue), value);
                }

                this._defaultValue = value;
            }
        }

        /// <summary>
        /// Set to normal for backwards compatibility, check if we need to have Time or something similar
        /// </summary>
        public ComponentMappingType MappingType => ComponentMappingType.Normal;


        /// <inheritdoc />
        public bool HasTranscoding()
        {
            return Transcoding != null;
        }

        public void AutoDetectTypeIfNotSet()
        {
            if (TimeMappingType != TimeDimensionMappingType.NotSet)
            {
                return;
            }

            if (Transcoding != null)
            {
                TimeMappingType = Transcoding.IsFrequencyDimension ? TimeDimensionMappingType.TranscodingWithFrequencyDimension : TimeDimensionMappingType.TranscodingWithCriteriaColumn;
            }
            else if (!string.IsNullOrWhiteSpace(ConstantValue))
            {
                TimeMappingType = TimeDimensionMappingType.MappingWithConstant;
            }
            else if (!string.IsNullOrWhiteSpace(SimpleMappingColumn))
            {
                TimeMappingType = TimeDimensionMappingType.MappingWithColumn;
            }
            else if (PreFormatted != null)
            {
                TimeMappingType = TimeDimensionMappingType.PreFormatted;
            }
            else
            {
                throw new ResourceConflictException("Cannot determine Time type for this Time Mapping");
            }
        }

        /// <summary>
        /// Readonly method that returns the columns used in this entity
        /// It only exists for cases where we want to have the full list of dataset column entities
        /// </summary>
        /// <returns>The list of dataset column entities used in this entity</returns>
        public IList<DataSetColumnEntity> GetColumns()
        {
            List<DataSetColumnEntity> datasetColumns = new List<DataSetColumnEntity>();
            switch (TimeMappingType)
            {
                case TimeDimensionMappingType.MappingWithColumn:
                    datasetColumns.Add(new DataSetColumnEntity() { Name = SimpleMappingColumn });
                    break;
                case TimeDimensionMappingType.PreFormatted:

                    if (PreFormatted != null)
                    {
                        datasetColumns.Add(PreFormatted.OutputColumn);
                        datasetColumns.Add(PreFormatted.FromColumn);
                        datasetColumns.Add(PreFormatted.ToColumn);
                    }

                    break;
                case TimeDimensionMappingType.TranscodingWithCriteriaColumn:
                case TimeDimensionMappingType.TranscodingWithFrequencyDimension:
                    var timeTranscodingAdvancedEntity = Transcoding;
                    if (timeTranscodingAdvancedEntity != null)
                    {
                        ExtractDatasetColumns(timeTranscodingAdvancedEntity, datasetColumns);
                    }

                    break;
                case TimeDimensionMappingType.MappingWithConstant:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(TimeMappingType.ToString());
            }

            IEqualityComparer<DataSetColumnEntity> comparer = new ColumNameComparer();
            return datasetColumns.Distinct(comparer).ToList();
        }

        private static void ExtractDatasetColumns(TimeTranscodingAdvancedEntity timeTranscodingAdvancedEntity,
            List<DataSetColumnEntity> datasetColumns)
        {
            // dataset columns stuff needed by maweb
            if (!timeTranscodingAdvancedEntity.IsFrequencyDimension)
            {
                datasetColumns.Add(timeTranscodingAdvancedEntity.CriteriaColumn);
            }

            foreach (var timeFormatConfiguration in timeTranscodingAdvancedEntity.TimeFormatConfigurations.Values)
            {
                ExtractDatasetColumns(timeFormatConfiguration.StartConfig, datasetColumns);
                ExtractDatasetColumns(timeFormatConfiguration.EndConfig, datasetColumns);
                // TODO retrieve duration map
                if (timeFormatConfiguration.DurationMap?.Column != null)
                {
                    datasetColumns.Add(timeFormatConfiguration.DurationMap.Column);
                }
            }
        }

        private static void ExtractDatasetColumns(TimeParticleConfiguration config,
            List<DataSetColumnEntity> datasetColumns)
        {
            if (config == null)
            {
                return;
            }

            if (config.Period != null)
            {
                datasetColumns.Add(config.Period.Column);
            }

            if (config.Year != null)
            {
                datasetColumns.Add(config.Year.Column);
            }
            else if (config.DateColumn != null)
            {
                datasetColumns.Add(config.DateColumn);
            }
        }
    }
}