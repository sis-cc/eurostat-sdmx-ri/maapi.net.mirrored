// -----------------------------------------------------------------------
// <copyright file="MappingSetEntity.cs" company="EUROSTAT">
//   Date Created : 2017-02-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System;

    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// The mapping set entity.
    /// </summary>
    public class MappingSetEntity : SimpleNameableEntity
    {
        private string _dataSetId;
        private DateTime? _validFrom;
        private DateTime? _validTo;
        private TimePreFormattedEntity _timePreFormattedEntity;
        private bool _isMetadata;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingSetEntity"/> class.
        /// </summary>
        public MappingSetEntity() : base(EntityType.MappingSet)
        {
            _timePreFormattedEntity = new TimePreFormattedEntity();
        }

        /// <summary>
        /// Gets or sets the data set identifier.
        /// </summary>
        /// <value>
        /// The data set identifier.
        /// </value>
        public string DataSetId
        {
            get { return this._dataSetId; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._dataSetId, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.DataSetId), value);
                }
                this._dataSetId = value;
            }
        }

        /// <summary>
        /// Gets or sets the valid from datetime.
        /// </summary>
        /// <value>
        /// The valid from datetime.
        /// </value>
        public DateTime? ValidFrom
        {
            get { return this._validFrom; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._validFrom, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ValidFrom), value);
                }
                this._validFrom = value;
            }
        }

        /// <summary>
        /// Gets or sets the valid to datetime.
        /// </summary>
        /// <value>
        /// The valid to datetime.
        /// </value>
        public DateTime? ValidTo
        {
            get { return this._validTo; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._validTo, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.ValidTo), value);
                }
                this._validTo = value;
            }
        }

        /// <summary>
        /// Gets or sets the time-pre-formatted value.
        /// </summary>
        /// <value>
        /// The <see cref="TimePreFormattedEntity"/> class.
        /// </value>
        [Obsolete("This has moved to TimeDimensionMappingEntity")]
        public TimePreFormattedEntity TimePreFormattedEntity
        {
            get { return this._timePreFormattedEntity; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._timePreFormattedEntity, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.TimePreFormattedEntity), value);
                }
                this._timePreFormattedEntity = value;
            }
        }

        /// <summary>
        /// Gets or sets the is metadata flag.
        /// </summary>
        /// <value>
        /// Is metadata flag.
        /// </value>
        public bool IsMetadata
        {
            get { return this._isMetadata; }
            set
            {
                if (this.ShouldCreateUpdateDocument(this._isMetadata, value))
                {
                    this.PatchRequest.CreateUpdateDocument(nameof(this.IsMetadata), value);
                }
                this._isMetadata = value;
            }
        }
    }
}