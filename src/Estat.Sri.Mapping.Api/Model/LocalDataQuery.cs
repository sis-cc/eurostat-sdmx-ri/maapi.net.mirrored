// -----------------------------------------------------------------------
// <copyright file="LocalDataQuery.cs" company="EUROSTAT">
//   Date Created : 2017-04-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using System.Collections.Generic;

    /// <summary>
    /// The local data query.
    /// </summary>
    public class LocalDataQuery : ILocalDataQuery
    {
        /// <summary>
        /// Gets or sets the mapping store identifier.
        /// </summary>
        /// <value>
        /// The mapping store identifier.
        /// </value>
        public string MappingStoreId { get; set; }

        /// <summary>
        /// Gets or sets the dissemination database settings.
        /// </summary>
        /// <value>
        /// The connection string settings.
        /// </value>
        public string DisseminationDatabaseSettingEntityId { get; set; }

        /// <summary>
        /// Gets or sets the page.
        /// </summary>
        /// <value>
        /// The page.
        /// </value>
        public int Page { get; set; }

        /// <summary>
        /// Gets or sets the query.
        /// </summary>
        /// <value>
        /// The query.
        /// </value>
        public string Query { get; set; }


        /// <summary>
        /// Gets or sets the dataset property.
        /// </summary>
        /// <value>
        /// The DatasetProperty object.
        /// </value>
        public DatasetPropertyEntity DatasetProperty { get; set; }

        /// <summary>
        /// Gets or sets the table.
        /// </summary>
        /// <value>
        /// The table.
        /// </value>
        public string Table { get; set; }

        /// <summary>
        /// Gets the column names.
        /// </summary>
        /// <value>
        /// The column names.
        /// </value>
        public IList<string> ColumnNames { get; } = new List<string>();

        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public int Count { get; set; }

        /// <inherit />
        public bool IsOrdered { get; set; }

        /// <inherit />
        public bool CheckForErrors { get; set; }
    }
}