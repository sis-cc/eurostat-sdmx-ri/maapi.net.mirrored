﻿// -----------------------------------------------------------------------
// <copyright file="Criteria.cs" company="EUROSTAT">
//   Date Created : 2017-02-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Model
{
    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// Class Criteria.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    /// <seealso cref="Estat.Sri.Mapping.Api.Model.ICriteria{TValue}" />
    public class Criteria<TValue> : ICriteria<TValue>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Criteria{TValue}"/> class.
        /// </summary>
        /// <param name="conditionOperator">The condition operator.</param>
        /// <param name="value">The value.</param>
        public Criteria(OperatorType conditionOperator, TValue value)
        {
            this.Operator = conditionOperator;
            this.Value = value;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public TValue Value { get; }

        /// <summary>
        /// Gets the operator.
        /// </summary>
        /// <value>
        /// The operator.
        /// </value>
        public OperatorType Operator { get; }
    }
}