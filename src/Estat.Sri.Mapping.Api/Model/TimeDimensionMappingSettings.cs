// -----------------------------------------------------------------------
// <copyright file="TimeDimensionMappingSettings.cs" company="EUROSTAT">
//   Date Created : 2018-5-14
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model.AdvancedTime;
using Estat.Sri.Mapping.Api.Utils;

namespace Estat.Sri.Mapping.Api.Model
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Builder;

    /// <summary>
    /// A model class used for passing information to <see cref="ITimeDimensionFrequencyMappingBuilder"/>
    /// </summary>
    public class TimeDimensionMappingSettings
    {
        public TimeDimensionMappingSettings(TimeDimensionMappingEntity componentMapping, string databaseType, IComponentMappingBuilder frequencyMapping)
            : this(componentMapping, null, databaseType, frequencyMapping)
        {
        }
        public TimeDimensionMappingSettings(TimeDimensionMappingEntity componentMapping, string databaseType, Dictionary<string, IComponentMappingBuilder> durationMappings)
        {
            ComponentMapping = componentMapping;
            DatabaseType = databaseType;
            DurationMappings = durationMappings;
        }
        public TimeDimensionMappingSettings(TimeDimensionMappingEntity componentMapping, TimePreFormattedEntity timePreFormattedEntity, string databaseType, IComponentMappingBuilder frequencyMapping)
        {
            ComponentMapping = componentMapping;
            TimePreFormattedEntity = timePreFormattedEntity;
            DatabaseType = databaseType;
            FrequencyMapping = frequencyMapping;
            if (componentMapping.HasTranscoding())
            {
                if (componentMapping.TimeMappingType == TimeDimensionMappingType.TranscodingWithCriteriaColumn)
                {
                    TranscodingEntity = componentMapping.Transcoding;
                }
                else if (componentMapping.TimeMappingType == TimeDimensionMappingType.TranscodingWithFrequencyDimension)
                {
                    TranscodingEntity = componentMapping.Transcoding;
                    SimpleTranscodingEntity = TimeTranscodingConversionHelper.Convert(TranscodingEntity);
                }
            }
        }

        public TimeDimensionMappingEntity ComponentMapping { get; private set; }

        public TimeTranscodingAdvancedEntity TranscodingEntity { get; private set; }
        [Obsolete("Replaced by TimeTranscodingAdvancedEntity. Slowly to move to TranscodingEntity")]
        public IList<TimeTranscodingEntity> SimpleTranscodingEntity { get; }

        public string DatabaseType { get; private set; }

        public IComponentMappingBuilder FrequencyMapping { get; private set; }

        public Dictionary<string, IComponentMappingBuilder> DurationMappings { get; private set; }

        public TimePreFormattedEntity TimePreFormattedEntity { get; private set; }
    }
}