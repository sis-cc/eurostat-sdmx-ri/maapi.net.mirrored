﻿// -----------------------------------------------------------------------
// <copyright file="ParameterType.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Constant
{
    /// <summary>
    /// The parameter type.
    /// </summary>
    public enum ParameterType
    {
        /// <summary>
        /// The string
        /// </summary>
        String,

        /// <summary>
        /// The number
        /// </summary>
        Number,

        /// <summary>
        /// The boolean
        /// </summary>
        Boolean,

        /// <summary>
        /// The password
        /// </summary>
        Password,

        /// <summary>
        /// The enumeration
        /// </summary>
        Enumeration
    }
}