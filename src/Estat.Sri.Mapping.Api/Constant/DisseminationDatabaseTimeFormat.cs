// -----------------------------------------------------------------------
// <copyright file="DisseminationDatabaseTimeFormat.cs" company="EUROSTAT">
//   Date Created : 2018-3-28
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Constant
{
    /// <summary>
    /// Enum Dissemination Database TimeFormat
    /// </summary>
    public enum DisseminationDatabaseTimeFormat
    {
        /// <summary>
        /// Time is annual
        /// </summary>
        Annual,
        
        /// <summary>
        /// Time is monthly
        /// </summary>
        Monthly,
        
        /// <summary>
        /// Time is weekly
        /// </summary>
        Weekly,
        
        /// <summary>
        /// Time is quarterly
        /// </summary>
        Quarterly,
        
        /// <summary>
        /// Time is semester
        /// </summary>
        Semester,
        
        /// <summary>
        /// Time is trimester
        /// </summary>
        Trimester,
        
        /// <summary>
        /// Time is timestamp or date
        /// </summary>
        TimestampOrDate,

        /// <summary>
        /// Time is ISO 8601 compatible
        /// </summary>
        Iso8601Compatible
    }
}
