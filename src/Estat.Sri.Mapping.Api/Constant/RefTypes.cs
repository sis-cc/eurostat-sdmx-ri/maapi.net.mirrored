// -----------------------------------------------------------------------
// <copyright file="ReferenceImportEngine.cs" company="EUROSTAT">
//   Date Created : 2022-06-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Constant
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class RefTypes
    {
        /// <summary>
        /// data, metadata, content constraint attachment (dataflow, metadataflow etc)
        /// </summary>
        public const string ConstraintAttachment = "constraintAttachment";

        /// <summary>
        /// component concept identity
        /// </summary>
        public const string ConceptIdentity = "conceptIdentity";

        /// <summary>
        /// dimension/attribute concept role
        /// </summary>
        public const string ConceptRoles = "conceptRoles";

        /// <summary>
        /// source used in SDMX 2.x Structure Map to Structure
        /// </summary>
        public const string StructureSource = "structure.source";

        /// <summary>
        /// target used in SDMX 2.x Structure Map to Structure
        /// </summary>
        public const string StructureTarget = "structure.target";

        /// <summary>
        /// source used in SDMX 2.x Codelist Map to Structure
        /// </summary>
        public const string CodelistSource = "codelist.source";

        /// <summary>
        /// target used in SDMX 2.x Codelist Map to Structure
        /// </summary>
        public const string CodelistTarget = "codelist.target";

        /// <summary>
        /// source used in many places like categorisation and structure mapping
        /// </summary>
        public const string Source = "source";

        /// <summary>
        /// target used in many places like categorisation and structure mapping
        /// </summary>
        public const string Target = "target";

        /// <summary>
        /// the reference to codelist extensions from a codelist
        /// </summary>
        public const string CodelistExtensions = "codelistExtensions";

        /// <summary>
        /// coded representation for components and concepts
        /// </summary>
        public const string Enumeration = "enumeration";

        /// <summary>
        /// From DSD to MSD reference
        /// </summary>
        public const string Metadata = "metadata";

        /// <summary>
        /// (meta)dataflow to (M)DSD reference
        /// </summary>
        public const string Structure = "structure";

        /// <summary>
        /// Reference from a Hierarchical Code to a Code
        /// </summary>
        public const string Code = "code";

        /// <summary>
        /// Used from Hierarchy Association to a Hierarchy association
        /// </summary>
        public const string LinkedHierarchy = "linkedHierarchy";

        /// <summary>
        /// from Hierarchy Association to an identifiable
        /// </summary>
        public const string LinkedObject = "linkedObject";

        /// <summary>
        /// Related to the above. E.g. if the linkedObject is a Dimension , the context can be a dataflow. The context within which the association is performed.
        /// </summary>
        public const string ContextObject = "contextObject";

        /// <summary>
        /// SDMX 2.x MSD or SDMX 3.0.0 metadataflow or metadata provision target`
        /// </summary>
        public const string Targets = "targets";

        /// <summary>
        /// provision agreement to dataflow reference
        /// </summary>
        public const string Dataflow = "dataflow";

        /// <summary>
        /// provision agreement to data provider reference
        /// </summary>
        public const string DataProvider = "dataprovider";

        /// <summary>
        /// SDMX 3.0.0 metadata provision agreement to metadatadataflow reference
        /// </summary>
        public const string Metadataflow = "metadataflow";

        /// <summary>
        /// SDMX 3.0.0 metadata provision agreement to metadata provider reference
        /// </summary>
        public const string MetadataProvider = "metadataProvider";

        /// <summary>
        /// SDMX 3.0.0 Structure Map to Reference Map reference
        /// </summary>
        public const string RepresentationMap = "representationMap";

        /// <summary>
        /// text format used in components and concepts
        /// </summary>
        public const string Format = "format";

        /// <summary>
        /// enumeration format used in components and concepts
        /// </summary>
        public const string EnumerationFormat = "enumerationFormat";

        /// <summary>
        /// other cases that is possible to identify the type reference from the context of the reference
        /// </summary>
        public const string Other = "other";
    }
}
