// -----------------------------------------------------------------------
// <copyright file="ColumnMappingStatus.cs" company="EUROSTAT">
//   Date Created : 2018-12-10
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.Api.Constant
{
    /// <summary>
    /// Enumerations of the <see cref="DataSetColumnEntity"/> mapped status
    /// </summary>
    public enum ColumnMappingStatus
    {
        /// <summary>
        /// No mapping. This is the default
        /// </summary>
        No = 0,

        /// <summary>
        /// The dataset column is mapped but not transcoded
        /// </summary>
        Yes = 1,

        /// <summary>
        /// The dataset column is mapped and transcoded
        /// </summary>
        YesAndTranscoded = 2
    }
}