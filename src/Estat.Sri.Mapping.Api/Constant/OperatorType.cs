// -----------------------------------------------------------------------
// <copyright file="OperatorType.cs" company="EUROSTAT">
//   Date Created : 2017-02-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Constant
{
    using System;

    /// <summary>
    /// The operator enumeration flags. Can be used together
    /// </summary>
    [Flags]
    public enum OperatorType
    {
        /// <summary>
        /// Return any value. needs to be used alone
        /// </summary>
        AnyValue = 0,

        /// <summary>
        /// Negative flag, must be used with other values. e.g. <c>Operator.Not | Operator.Contains</c> is not contains.
        /// </summary>
        Not = 1,
        
        /// <summary>
        /// Return entries that contain the value
        /// </summary>
        Contains = 2,

        /// <summary>
        /// The exact
        /// </summary>
        Exact = 4,

        /// <summary>
        /// The less than
        /// </summary>
        LessThan = 8,

        /// <summary>
        /// The greater than
        /// </summary>
        GreaterThan = 16,

        /// <summary>
        /// The less than and equal
        /// </summary>
        LessThanOrEqual = LessThan | Exact,

        /// <summary>
        /// The more than and equal
        /// </summary>
        GreaterThanOrEqual = GreaterThan | Exact,

        /// <summary>
        /// Ends with
        /// </summary>
        EndsWith = 32,

        /// <summary>
        /// Starts with
        /// </summary>
        StartsWith = 64,
        
        /// <summary>
        /// IS NULL
        /// </summary>
        Null = 128,

        /// <summary>
        /// Null Or Exact
        /// </summary>
        NullOrExact = Null | Exact
    }
}