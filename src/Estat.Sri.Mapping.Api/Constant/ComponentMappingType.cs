// -----------------------------------------------------------------------
// <copyright file="MappingType.cs" company="EUROSTAT">
//   Date Created : 2017-07-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Constant
{
    /// <summary>
    /// The component mapping type.
    /// </summary>
    public enum ComponentMappingType
    {
        /// <summary>
        /// The normal and default mapping type, that involves a SDMX Component either with a constant value or DDB Columns
        /// </summary>
        /// <remarks>This is the default mapping type used by </remarks>
        Normal = 0,

        /// <summary>
        /// The last updated
        /// </summary>
        LastUpdated,

        /// <summary>
        /// The updated status
        /// </summary>
        UpdatedStatus,

        /// <summary>
        /// Duration column mapping
        /// </summary>
        Duration,

        /// <summary>
        /// The other
        /// </summary>
        Other,
        ValidTo
    }
}