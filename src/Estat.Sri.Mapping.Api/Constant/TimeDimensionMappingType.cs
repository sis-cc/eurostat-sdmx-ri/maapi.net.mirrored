// -----------------------------------------------------------------------
// <copyright file="TimeDimensionMappingType.cs" company="EUROSTAT">
//   Date Created : 2022-08-01
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Constant
{
    /// <summary>
    /// Time Dimension Mapping types
    /// </summary>
    public enum TimeDimensionMappingType
    {
        /// <summary>
        /// When this is not set
        /// </summary>
        NotSet,
        
        /// <summary>
        /// One to One mapping between the Time Dimension and one column. It may have a default value
        /// </summary>
        MappingWithColumn,
        /// <summary>
        /// Mapping with Constant value
        /// </summary>
        MappingWithConstant,
        /// <summary>
        /// Mapping with 3 columns, one for output and two for criteria
        /// </summary>
        PreFormatted,
        /// <summary>
        /// Transcoding with Criteria column, meaning based on the value of the criteria column of each record different
        /// transcoding configuration will be used
        /// </summary>
        TranscodingWithCriteriaColumn,
        /// <summary>
        /// Transcoding with Frequency Dimension, meaning based on the value of the mapped Frequency dimension of each record different
        /// transcoding configuration will be used
        /// </summary>
        TranscodingWithFrequencyDimension
        
    }
}