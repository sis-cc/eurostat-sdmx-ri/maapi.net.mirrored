﻿// -----------------------------------------------------------------------
// <copyright file="ActionResults.cs" company="EUROSTAT">
//   Date Created : 2017-03-23
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Constant
{
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// This class provides a number of predefined action results.
    /// </summary>
    public static class ActionResults
    {
        /// <summary>
        /// Gets the success.
        /// </summary>
        /// <value>
        /// The success.
        /// </value>
        public static IActionResult Success { get; } = new ActionResult("200", StatusType.Success);

        /// <summary>
        /// Gets the no result.
        /// </summary>
        /// <value>
        /// The no result.
        /// </value>
        public static IActionResult NoResult { get; } = new ActionResult("404", StatusType.Error, "Not found");

        /// <summary>
        /// Gets the connection error.
        /// </summary>
        /// <value>
        /// The connection error.
        /// </value>
        public static IActionResult ConnectionError { get; } = new ActionResult("500", StatusType.Error, "Connection failure");
    }
}