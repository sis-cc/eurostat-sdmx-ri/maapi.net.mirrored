// -----------------------------------------------------------------------
// <copyright file="EntityType.cs" company="EUROSTAT">
//   Date Created : 2017-02-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.ComponentModel;

namespace Estat.Sri.Mapping.Api.Constant
{
    /// <summary>
    /// An enumeration with all entity types
    /// </summary>
    public enum EntityType
    {
        /// <summary>
        /// The DDB connection settings
        /// </summary>
        [Description("ddb")]
        DdbConnectionSettings,

        /// <summary>
        /// The data set
        /// </summary>
        [Description("dataset")]
        DataSet,

        /// <summary>
        /// The data set column
        /// </summary>
        [Description("column")]
        DataSetColumn,

        /// <summary>
        /// The local code
        /// </summary>
        [Description("localCode")]
        LocalCode,

        /// <summary>
        /// The mapping set
        /// </summary>
        [Description("mappingset")]
        MappingSet,

        /// <summary>
        /// The mapping
        /// </summary>
        [Description("mapping")]
        Mapping,
        
        /// <summary>
        /// The Time Dimension mapping (new in MADB7)
        /// </summary>
        [Description("timemapping")]
        TimeMapping,

        /// <summary>
        /// The mapping store
        /// </summary>
        MappingStore,

        /// <summary>
        /// The trans-coding
        /// </summary>
        [Description("transcoding")]
        Transcoding,

        /// <summary>
        /// The time trans-coding
        /// </summary>
        TimeTranscoding,

        /// <summary>
        /// The trans-coding rule
        /// </summary>
        [Description("transcodingRule")]
        TranscodingRule,

        /// <summary>
        /// The transcoding script
        /// </summary>
        TranscodingScript,

        /// <summary>
        /// The desc source
        /// </summary>
        [Description("column_description")]
        DescSource,

        /// <summary>
        /// The header
        /// </summary>
        [Description("header_template")]
        Header,

        /// <summary>
        /// For SDMX entities
        /// </summary>
        Sdmx,

        /// <summary>
        /// The registry
        /// </summary>
        [Description("registry")]
        Registry,
        /// <summary>
        /// The user
        /// </summary>
        [Description("user")]
        User,
        /// <summary>
        /// The user action
        /// </summary>
        [Description("userAction")]
        UserAction,
        /// <summary>
        /// Template mapping
        /// </summary>
        [Description("templateMapping")]
        TemplateMapping,
        /// <summary>
        /// Data source
        /// </summary>
        [Description("dataSource")]
        DataSource,
        /// <summary>
        /// Time-pre-formatted
        /// </summary>
        [Description("timePreFormatted")]
        TimePreFormatted,
        /// <summary>
        /// Dataset property
        /// </summary>
        [Description("datasetProperty")]
        DataSetProperty,
        /// <summary>
        /// The registry
        /// </summary>
        [Description("nsiws")]
        Nsiws,
        /// <summary>
        /// No type
        /// </summary>
        None,

        /// <summary>
        /// The duration mapping typw
        /// </summary>
        [Description("durationmapping")]
        DurationMapping,

         /// <summary>
        /// The Last updated mapping type
        /// </summary>       
        [Description("lastUpdated")]
        LastUpdated,

        /// <summary>
        /// Valid To mapping type
        /// </summary>
        [Description("validTo")]
        ValidToMapping,

        /// <summary>
        /// Update Status mapping type
        /// </summary>
        [Description("updateStatus")]
        UpdateStatusMapping
    }
}