// -----------------------------------------------------------------------
// <copyright file="CommonEntityExtension.cs" company="EUROSTAT">
//   Date Created : 2017-07-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Extension
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// Store independent extension methods
    /// </summary>
    public static class CommonEntityExtension
    {
        /// <summary>
        /// Sets the parent identifier to all entities
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entities">The entities.</param>
        /// <param name="parentId">The parent identifier.</param>
        /// <returns>The list of entities with parent id set</returns>
        public static IEnumerable<TEntity> WithParentId<TEntity>(this IEnumerable<TEntity> entities, string parentId) where TEntity : class, IEntity
        {
            foreach (var entity in entities)
            {
                entity.ParentId = parentId;
                yield return entity;
            }
        }

        /// <summary>
        /// Determines whether the <paramref name="entityType"/> is a root entity
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns><c>true</c> if the specified entity type is root entity; otherwise, <c>false</c>.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">entityType - null</exception>
        public static bool IsRootEntity(this EntityType entityType)
        {
            switch (entityType)
            {
                case EntityType.DdbConnectionSettings:
                case EntityType.DataSet:
                case EntityType.MappingSet:
                case EntityType.MappingStore:
                case EntityType.Header:
                case EntityType.Sdmx:
                case EntityType.Registry:
                case EntityType.User:
                    return true;
                case EntityType.DataSetColumn:
                case EntityType.LocalCode:
                case EntityType.Mapping:
                case EntityType.Transcoding:
                case EntityType.TimeMapping:
                case EntityType.LastUpdated:
                case EntityType.UpdateStatusMapping:
                case EntityType.ValidToMapping:
                case EntityType.TimeTranscoding:
                case EntityType.TranscodingRule:
                case EntityType.TranscodingScript:
                case EntityType.DescSource:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException(nameof(entityType), entityType, null);
            }
        }

        /// <summary>
        /// Gets the type of the parent.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>The parent <see cref="EntityType"/></returns>
        public static EntityType GetParentType(this EntityType entityType)
        {
            switch (entityType)
            {
                case EntityType.DataSet:
                    return EntityType.DdbConnectionSettings;
                case EntityType.DataSetColumn:
                    return EntityType.DataSet;
                case EntityType.LocalCode:
                    return EntityType.DataSetColumn;
                case EntityType.MappingSet:
                    return EntityType.Sdmx;
                case EntityType.Mapping:
                case EntityType.TimeMapping:
                case EntityType.LastUpdated:
                case EntityType.UpdateStatusMapping:
                case EntityType.ValidToMapping:
                    return EntityType.MappingSet;
                case EntityType.Transcoding:
                    return EntityType.Mapping;
                case EntityType.TimeTranscoding:
                    return EntityType.TimeMapping;
                case EntityType.TranscodingRule:
                    return EntityType.Mapping;
                case EntityType.TranscodingScript:
                    return EntityType.Mapping;
                case EntityType.DescSource:
                    return EntityType.DataSetColumn;
                case EntityType.Header:
                    return EntityType.Sdmx;
            }

            return EntityType.MappingStore;
        }
    }
}