﻿// -----------------------------------------------------------------------
// <copyright file="ActionResultExtension.cs" company="EUROSTAT">
//   Date Created : 2017-09-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Extension
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;


    /// <summary>
    /// A set of extension methods for <see cref="IActionResult"/>
    /// </summary>
    public static class ActionResultExtension
    {
        /// <summary>
        /// Merges the specified results.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <returns>The merged <see cref="IActionResult"/></returns>
        public static IActionResult Merge(this IList<IActionResult> results)
        {
            if (results == null)
            {
                throw new ArgumentNullException(nameof(results));
            }

            if (results.Count == 0)
            {
                return ActionResults.Success;
            }

            var allSuccess = results.All(result => result.Status == StatusType.Success);
            var hasError = !allSuccess && results.Any(result => result.Status == StatusType.Error);
            ActionResult rootResult;
                var messages = results.SelectMany(result => result.Messages).ToArray();
            if (allSuccess)
            {
                rootResult = new ActionResult("200", StatusType.Success, messages);
            }
            else if (hasError)
            {
                var errorCodes = results.Where(result => result.Status == StatusType.Error).Select(result => result.ErrorCode).Distinct().ToArray();

                rootResult = new ActionResult(errorCodes.FirstOrDefault(), StatusType.Error, messages);
            }
            else
            {
                var errorCodes = results.Where(result => result.Status == StatusType.Warning).Select(result => result.ErrorCode).Distinct().ToArray();
                rootResult = new ActionResult(errorCodes.FirstOrDefault(), StatusType.Warning, messages);
            }

            return rootResult;
        }
    }
}