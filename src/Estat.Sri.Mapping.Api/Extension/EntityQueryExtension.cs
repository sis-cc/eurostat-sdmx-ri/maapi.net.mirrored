﻿// -----------------------------------------------------------------------
// <copyright file="EntityQueryExtension.cs" company="EUROSTAT">
//   Date Created : 2017-07-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Extension
{
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// Extension methods related to <see cref="EntityQuery"/>
    /// </summary>
    public static class EntityQueryExtension
    {
        /// <summary>
        /// Queries for this entity identifier.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="IEntityQuery"/>.
        /// </returns>
        public static IEntityQuery QueryForThisEntityId(this string value)
        {
            return new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, value) };
        }

        /// <summary>
        /// Queries for this entity identifier.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="IEntityQuery"/>.
        /// </returns>
        public static IEntityQuery QueryForThisEntityId(this IEntity value)
        {
            return new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, value.EntityId) };
        }

        /// <summary>
        /// Queries for this parent identifier.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="IEntityQuery"/>.
        /// </returns>
        public static IEntityQuery QueryForThisParentId(this IEntity value)
        {
            return new EntityQuery() { ParentId = new Criteria<string>(OperatorType.Exact, value.EntityId) };
        }

        /// <summary>
        /// Queries for this parent identifier.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="IEntityQuery"/>.
        /// </returns>
        public static IEntityQuery QueryForThisParentId(this string value)
        {
            return new EntityQuery() { ParentId = new Criteria<string>(OperatorType.Exact, value) };
        }
    }
}