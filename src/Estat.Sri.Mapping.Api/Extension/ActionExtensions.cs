﻿// -----------------------------------------------------------------------
// <copyright file="ActionExtensions.cs" company="EUROSTAT">
//   Date Created : 2016-11-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Extensions
{
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Extension methods related to <see cref="ObservationAction"/>
    /// </summary>
    public static class ActionExtensions
    {
        /// <summary>
        /// Determines whether this instance is active.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns>
        ///   <c>true</c> if the specified action is active; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsActive(this ObservationAction action)
        {
            if (action == null)
            {
                return false;
            }

            return action.EnumType != ObservationActionEnumType.Deleted;
        }
    }
}