// -----------------------------------------------------------------------
// <copyright file="DisseminationDatabaseTimeFormatExtension.cs" company="EUROSTAT">
//   Date Created : 2018-5-11
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.Api.Extension
{
    using System;

    using Estat.Sri.Mapping.Api.Constant;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// A collection of extension methods for the <see cref="DisseminationDatabaseTimeFormat"/>
    /// </summary>
    public static class DisseminationDatabaseTimeFormatExtension
    {
        /// <summary>
        /// Get the corresponding <see cref="TimeFormat"/> for the specified <paramref name="disseminationDatabaseTimeFormat"/>
        /// </summary>
        /// <param name="disseminationDatabaseTimeFormat">The dissemination DB time format</param>
        /// <returns>The <see cref="TimeFormat"/></returns>
        public static TimeFormat GetTimeFormat(this DisseminationDatabaseTimeFormat disseminationDatabaseTimeFormat)
        {
            switch (disseminationDatabaseTimeFormat)
            {
                case DisseminationDatabaseTimeFormat.Annual:
                    return TimeFormat.Year;
                case DisseminationDatabaseTimeFormat.Monthly:
                    return TimeFormat.Month;
                case DisseminationDatabaseTimeFormat.Weekly:
                    return TimeFormat.Week;
                case DisseminationDatabaseTimeFormat.Quarterly:
                    return TimeFormat.QuarterOfYear;
                case DisseminationDatabaseTimeFormat.Semester:
                    return TimeFormat.HalfOfYear;
                case DisseminationDatabaseTimeFormat.Trimester:
                    return TimeFormat.ThirdOfYear;
                case DisseminationDatabaseTimeFormat.TimestampOrDate:
                case DisseminationDatabaseTimeFormat.Iso8601Compatible:
                    return TimeFormat.Date;
                default:
                    throw new ArgumentOutOfRangeException(nameof(disseminationDatabaseTimeFormat), disseminationDatabaseTimeFormat, null);
            }
        }
    }
}