﻿// -----------------------------------------------------------------------
// <copyright file="ParameterTypeExtension.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Extension
{
    using System;

    using Estat.Sri.Mapping.Api.Constant;

    public static class ParameterTypeExtension
    {
        public static ParameterType ToParameterType(this Type type)
        {
            if (type == null)
            {
                return ParameterType.String;
            }

            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Boolean:
                    return ParameterType.Boolean;
                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                    return ParameterType.Number;
                default:
                    return ParameterType.String;
            }

        }
    }
}