﻿// -----------------------------------------------------------------------
// <copyright file="DataExtension.cs" company="EUROSTAT">
//   Date Created : 2017-04-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Extension
{
    using System.Collections.Generic;
    using System.Linq;

    public static class DataExtension
    {
        /// <summary>
        /// Builds the Cartesian product.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sequences">The sequences.</param>
        /// <returns>The Cartesian product</returns>
        /// <remarks>Credit goes to <a href="http://stackoverflow.com/a/3098381">Here</a></remarks>
        public static IEnumerable<IEnumerable<T>> CartesianProduct<T>(this IEnumerable<IEnumerable<T>> sequences)
        {
            IEnumerable<IEnumerable<T>> emptyProduct = new[] { Enumerable.Empty<T>() };
            return sequences.Aggregate(
                emptyProduct,
                (accumulator, sequence) =>
                from accseq in accumulator from item in sequence select accseq.Concat(new[] { item }));
        }
    }
}