// -----------------------------------------------------------------------
// <copyright file="ComponentMappingExtension.cs" company="EUROSTAT">
//   Date Created : 2017-09-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Api.Extension
{
    using System;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;

    /// <exclude />
    public static class ComponentMappingExtension
    {
        /// <summary>
        /// The update status type. Value used in <c>COMPONENT_MAPPING.TYPE</c> TODO Migrate to ComponentMappingType
        /// </summary>
        private const string UpdateStatusType = "UP_STATUS";

        /// <summary>
        /// The last updated. Value used in <c>COMPONENT_MAPPING.TYPE</c> TODO Migrate to ComponentMappingType
        /// </summary>
        private const string LastUpdatedType = "LAST_UP";
        /// <summary>
        /// The valid to type
        /// </summary>
        private const string ValidToType = "VALID_TO";

        /// <summary>
        /// Determines whether [is normal mapping] [the specified entity].
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// <c>true</c> if [is normal mapping] [the specified entity]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNormalMapping(this ComponentMappingEntity entity)
        {
            // TODO Use ComponentType enum
            // always true?

            return "A".Equals(entity.Type) && entity.Component != null;
        }

        /// <summary>
        /// Determines whether [is normal mapping] [the specified entity].
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// <c>true</c> if [is normal mapping] [the specified entity]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNormalOrDurationOrMetadataMapping(this ComponentMappingEntity entity)
        {
            return entity.IsNormalMapping() || entity.IsDurationMapping() || entity.IsMetadataMapping();
        }

        /// <summary>
        /// Determines whether [is last update] [the specified entity].
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// <c>true</c> if [is last update] [the specified entity]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsLastUpdate(this ComponentMappingEntity entity)
        {
            // TODO Use ComponentType enum
            return LastUpdatedType.Equals(entity.Type) && entity.Component == null;
        }

        /// <summary>
        /// Determines whether 
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// <c>true</c> if [is update status] [the specified entity]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsUpdateStatus(this ComponentMappingEntity entity)
        {
            // TODO Use ComponentType enum
            return UpdateStatusType.Equals(entity.Type) && entity.Component == null;
        }

        /// <summary>
        /// Determines whether [is valid to].
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        ///   <c>true</c> if [is valid to] [the specified entity]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidTo(this ComponentMappingEntity entity)
        {
            // TODO Use ComponentType enum
            return ValidToType.Equals(entity.Type) && entity.Component == null;
        }
        /// <summary>
        /// Determines whether [is time dimension mapping] [the specified entity].
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// <c>true</c> if [is time dimension mapping] [the specified entity]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsTimeDimensionMapping(this ComponentMappingEntity entity)
        {
            return DimensionObject.TimeDimensionFixedId.Equals(entity.Component?.ObjectId);
        }

        /// <summary>
        /// Determines whether the specified <paramref name="entity"/> is a <see cref="ComponentMappingType.Duration"/>
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// <c>true</c> if the specified <paramref name="entity"/> is a <see cref="ComponentMappingType.Duration"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsDurationMapping(this ComponentMappingEntity entity)
        {
            return ComponentMappingType.Duration.ToString("G").Equals(entity?.Type, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Determines whether [is metadata mapping] [the specified entity].
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// <c>true</c> if [is metadata mapping] [the specified entity]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsMetadataMapping(this ComponentMappingEntity entity)
        {
            return "A".Equals(entity.Type) && !string.IsNullOrEmpty(entity.MetadataAttributeSdmxId);
        }
         /// <summary>
        /// Convert from <see cref="ComponentMappingType"/> to <c>MSDB</c> <c>COMPONENT_MAPPING.TYPE</c> field value  
        /// </summary>
        /// <param name="componentMappingType">Type of the component mapping.</param>
        /// <returns>The <c>MSDB</c> specific values</returns>
        /// <exception cref="System.NotSupportedException">Mapping store scheme doesn't support the specified <paramref name="componentMappingType"/></exception>
        public static string AsMappingStoreType(this ComponentMappingType componentMappingType)
        {
            switch (componentMappingType)
            {
                    case ComponentMappingType.Normal:
                    return "A";
                    case ComponentMappingType.LastUpdated:
                    return "LAST_UP";
                    case ComponentMappingType.UpdatedStatus:
                    return "UP_STATUS";
                    case ComponentMappingType.ValidTo:
                    return "VALID_TO";
                    case ComponentMappingType.Duration:
                        return "DURATION";
            }

            throw new NotSupportedException($"Mapping store scheme doesn't support the specified '${componentMappingType}'");
        }


    }
}