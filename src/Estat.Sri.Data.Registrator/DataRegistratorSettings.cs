// -----------------------------------------------------------------------
// <copyright file="DataRegistratorSettings.cs" company="EUROSTAT">
//   Date Created : 2017-11-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Data.Registrator
{
    using System;
    using System.Configuration;

    /// <summary>
    /// The DataRegistrator settings
    /// </summary>
    public class DataRegistratorSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataRegistratorSettings"/> class.
        /// </summary>
        public DataRegistratorSettings()
        {
            Username = ConfigurationManager.AppSettings["data.registrator.registry.http.username"];
            Password = ConfigurationManager.AppSettings["data.registrator.registry.http.password"];
            IsUseHttpAuthentication = TryGetBoolOrFalse("data.registrator.registry.http.useHttpAuthentication");
            RegistryWSDL = TryGetUriOrNull("data.registrator.registry.endpoint.v21.wsdl");
            RegistryEndpoint = TryGetUriOrNull("data.registrator.registry.endpoint.v21");
            IsRest = TryGetBoolOrFalse("data.registrator.registry.endpoint.isRest");
            // ProxySettings
            EnableProxy = TryGetBoolOrFalse("data.registrator.registry.useProxy");
            UseSystemProxy = TryGetBoolOrFalse("data.registrator.registry.useSystemProxy");
            ProxyHost = TryGetUriOrNull("data.registrator.registry.proxyHost");
            ProxyPort = Convert.ToInt32(ConfigurationManager.AppSettings["data.registrator.registry.proxyPort"]);
            ProxyUsername = ConfigurationManager.AppSettings["data.registrator.registry.proxyUsername"];
            ProxyPassword = ConfigurationManager.AppSettings["data.registrator.registry.proxyPassword"];
        }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        public string Username
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is use HTTP authentication.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is use HTTP authentication; otherwise, <c>false</c>.
        /// </value>
        public bool IsUseHttpAuthentication
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the registry WSDL.
        /// </summary>
        /// <value>
        /// The registry WSDL.
        /// </value>
        public Uri RegistryWSDL
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the registry endpoint.
        /// </summary>
        /// <value>
        /// The registry endpoint.
        /// </value>
        public Uri RegistryEndpoint
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the if the requeste is rest.
        /// </summary>
        /// <value>
        /// The request type is rest.
        /// </value>
        public bool IsRest
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the ProxyHost
        /// </summary>
        /// <value>
        /// The proxy address.
        /// </value>
        public Uri ProxyHost
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance use Proxy Server.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is use Proxy Server; otherwise, <c>false</c>.
        /// </value>
        public bool EnableProxy
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance use System Proxy Server.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is use System Proxy Server; otherwise, <c>false</c>.
        /// </value>
        public bool UseSystemProxy
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the ProxyPort
        /// </summary>
        /// <value>
        /// The proxy port.
        /// </value>
        public int ProxyPort
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the ProxyUserName
        /// </summary>
        /// <value>
        /// The proxy username.
        /// </value>
        public string ProxyUsername
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the ProxyPassword
        /// </summary>
        /// <value>
        /// The proxy password.
        /// </value>
        public string ProxyPassword
        {
            get; set;
        }

        /// <summary>
        /// Tries the get URI or null.
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <returns>System.Uri.</returns>
        /// <exception cref="ConfigurationErrorsException"></exception>
        private Uri TryGetUriOrNull(string setting)
        {
            var value = ConfigurationManager.AppSettings[setting];
            if (value == null)
            {
                return null;
            }

            Uri url;
            if (Uri.TryCreate(value, UriKind.Absolute, out url))
            {
                return url;
            }

            throw new ConfigurationErrorsException(string.Format("The setting {0} accepts only a absolute URL. The provided value '{1}' cannot be parsed", setting, value));
        }

        /// <summary>
        /// Tries the get bool or false.
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <returns>System.Boolean.</returns>
        private bool TryGetBoolOrFalse(string setting)
        {
            var value = ConfigurationManager.AppSettings[setting];
            if (value == null)
            {
                return false;
            }

            bool boolValue;
            if (bool.TryParse(value, out boolValue))
            {
                return boolValue;
            }

            throw new ConfigurationErrorsException(string.Format("The setting {0} accepts only a Boolean value, true or false. The provided value '{1}' cannot be parsed", setting, value));
        }
    }
}
