// -----------------------------------------------------------------------
// <copyright file="DataRegistrator.cs" company="EUROSTAT">
//   Date Created : 2017-11-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Data.Registrator
{
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using System;
    using System.Collections.Generic;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Engine.WebService;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Estat.Sdmxsource.Extension.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.SubmissionResponse;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;

    public class DataRegistrator : IDataRegistrator
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(DataRegistrator));

        private DataRegistratorSettings _settings;
        private readonly SubmitRegistrationEngine _submitRegistrationEngine;
        private readonly ISoapMessageSubmitterEngine _soapSubmitter;
        private readonly IStructureSubmitter _structureSubmitter;
        private readonly string _storeId;
        private readonly WsInfo wsInfo;

        public DataRegistrator(string storeId, SubmitRegistrationEngine submitRegistrationEngine, IStructureSubmitter structureSubmitter, DataRegistratorSettings settings)
        {
            if (submitRegistrationEngine == null)
            {
                throw new ArgumentNullException(nameof(submitRegistrationEngine));
            }

            if (structureSubmitter == null)
            {
                throw new ArgumentNullException(nameof(structureSubmitter));
            }

            this._settings = settings;
            _log.Debug("DataRegistrator initialized");
            _submitRegistrationEngine = submitRegistrationEngine;
            // TODO WsInfo from DataRegistrationSettings
            wsInfo = new WsInfo();
            wsInfo.EnableHTTPAuthentication = settings.IsUseHttpAuthentication;
            wsInfo.UserName = settings.Username;
            wsInfo.Password = settings.Password;
            wsInfo.EndPoint = settings.RegistryEndpoint.ToString();
            wsInfo.EnableProxy = settings.EnableProxy;
            wsInfo.ProxyServer = settings.ProxyHost?.ToString();
            wsInfo.ProxyServerPort = settings.ProxyPort;
            wsInfo.ProxyUserName = settings.ProxyUsername;
            wsInfo.ProxyPassword = settings.ProxyPassword;
            wsInfo.UseSystemProxy = settings.UseSystemProxy;

            if (!settings.IsRest)
            {
                wsInfo.Wsdl = settings.RegistryWSDL?.ToString();
                _soapSubmitter = new SoapMessageSubmitter(wsInfo, new SoapMessageWriterEngine());
            }

            _structureSubmitter = structureSubmitter;
            _storeId = storeId;
        }

        /// <summary>
        /// Sends the SDMX submit registration request containign the specified <paramref name="registrations" /> to a SDMX v2.1 compliant Registry and store the response in a local store
        /// </summary>
        /// <param name="registrations">The registrations.</param>
        /// <exception cref="ArgumentNullException">Null paramater: 'registrations'</exception>
        public void SendSubmitRegistrationsRequest(Dictionary<DatasetActionEnumType, IList<IRegistrationMutableObject>> registrations)
        {
            if (registrations == null)
            {
                throw new ArgumentNullException(nameof(registrations));
            }
            _log.Debug("> Before sending the SubmitStructureRegistrationRequest...");

            IList<ISubmitRegistrationResponse> responses = new List<ISubmitRegistrationResponse>();
            if (_settings.IsRest)
            {
                GlobalRegistryRestSubmitterEngine globalRegistryRestSubmitterEngine = new GlobalRegistryRestSubmitterEngine(wsInfo);
                foreach (var registration in registrations)
                {
                    IList<IRegistrationObject> registrationObjects = registration.Value.Select(x => x.ImmutableInstance).ToArray();
                    IList<ISubmitRegistrationResponse> response = _submitRegistrationEngine.Register(globalRegistryRestSubmitterEngine, registrationObjects, registration.Key);
                    responses.AddAll(response);
                }
            }
            else
            {
                SoapRegistryInterfaceRequestSubmitter soapRegistryInterfaceRequestSubmitter = new SoapRegistryInterfaceRequestSubmitter(_soapSubmitter);
                foreach (var registration in registrations)
                {
                    IList<IRegistrationObject> registrationObjects = registration.Value.Select(x => x.ImmutableInstance).ToArray();
                    IList<ISubmitRegistrationResponse> response = _submitRegistrationEngine.Register(soapRegistryInterfaceRequestSubmitter, registrationObjects, registration.Key);
                    responses.AddAll(response);
                }
            }

            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();

            foreach (var response in responses)
            {

                IRegistrationObject registration = response.Registration;
                if (!response.IsError)
                {
                    _log.Info("> The registration " + registration.Id + " will be inserted in mapping store.");
                    sdmxObjects.AddRegistration(registration);
                }
                else
                {
                    _log.Info("> The registration " + registration.Id + " will not be inserted in mapping store. (Error: " + response.ErrorList + ")");
                }
            }

            var saveResults = _structureSubmitter.SubmitStructures(this._storeId, sdmxObjects);
            foreach(var result in saveResults)
            {
                switch (result.Status)
                {
                    case Sdmxsource.Extension.Constant.ResponseStatus.Success:
                        _log.DebugFormat("Successfully added registration {0}", result.StructureReference);
                        break;
                    case Sdmxsource.Extension.Constant.ResponseStatus.Failure:
                    case Sdmxsource.Extension.Constant.ResponseStatus.Warning:
                        _log.WarnFormat("Error while adding registration {0}", result.StructureReference);
                        foreach (var message in result.Messages)
                        {
                            if (message.ErrorCode != null)
                            {
                                _log.WarnFormat("SDMX Error Code {0} and description {1}",message.ErrorCode.ClientErrorCode, message.ErrorCode.ErrorString);
                            }
                            foreach(var text in message.Text)
                            {
                                _log.WarnFormat("lang:{0}, Text: {1}", text.Locale, text.Value);
                            }
                        }

                        break;
                }
            }
        }

    }
}
