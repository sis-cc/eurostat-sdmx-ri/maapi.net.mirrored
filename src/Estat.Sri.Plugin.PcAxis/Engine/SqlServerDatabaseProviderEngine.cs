﻿// -----------------------------------------------------------------------
// <copyright file="PcAxisDatabaseProviderEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.PcAxis.Engine
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    /// <summary>
    /// The SQL server database provider engine.
    /// </summary>
    public class PcAxisDatabaseProviderEngine : IDatabaseProviderEngine
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(PcAxisDatabaseProviderEngine));

        /// <summary>
        /// The integrated security true values
        /// </summary>
        private static readonly string[] _integratedSecurityTrueValues = { "yes", "true", "sspi" };

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name => MappingStoreDefaultConstants.PcAxisName;

        /// <summary>
        /// Gets a value indicating whether the parameters are read only.
        /// </summary>
        /// <value>
        /// <c>true</c> if parameters are read only; otherwise, <c>false</c>.
        /// </value>
        public bool ParametersAreReadOnly => false;

        /// <summary>
        /// Gets a value indicating whether to show the description association.
        /// </summary>
        /// <value>
        /// <c>true</c> if it should show the description association; otherwise, <c>false</c>.
        /// </value>
        public bool ShowDescriptionAssociation => true;

        /// <summary>
        /// Creates the connection entity.
        /// </summary>
        /// <returns>
        /// The connection entity
        /// </returns>
        public IConnectionEntity CreateConnectionEntity()
        {
            return CreateConnectionEntity(null);
        }

        /// <summary>
        /// Creates the connection entity from the specified <see cref="T:System.Configuration.ConnectionStringSettings" />
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sri.Mapping.Api.Model.IConnectionEntity" />
        /// </returns>
        public IConnectionEntity CreateConnectionEntity(ConnectionStringSettings settings)
        {
            var providerName = DatabaseType.GetProviderName(Name);
            var factory = DbProviderFactories.GetFactory(providerName);
            var builder = factory.CreateConnectionStringBuilder();
            var connectionEntity = BuildConnectionEntity(builder, settings);

            return connectionEntity;
        }

        /// <summary>
        /// Creates the connection string.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>
        /// The <see cref="T:System.Configuration.ConnectionStringSettings" />
        /// </returns>
        public ConnectionStringSettings CreateConnectionString(IConnectionEntity connection)
        {
            var providerName = DatabaseType.GetProviderName(Name);
            
            var factory = DbProviderFactories.GetFactory(providerName);
            var builder = factory.CreateConnectionStringBuilder();
            if (builder == null)
            {
                throw new InvalidOperationException("Cannot build DbConnectionStringBuilder for provider: " + providerName );
            }

            foreach (var connectionParameterEntity in connection.AdvancedSettings)
            {
                builder.Add(connectionParameterEntity.Key, connectionParameterEntity.Value);
            }

            return new ConnectionStringSettings(connection.Name, builder.ConnectionString, providerName);
        }

        /// <summary>
        /// Gets the database objects. E.g. Tables or Views
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The list of <see cref="T:Estat.Sri.Mapping.Api.Model.DatabaseObject" />
        /// </returns>
        /// <exception cref="System.ArgumentNullException">settings is null</exception>
        public IEnumerable<IDatabaseObject> GetDatabaseObjects(ConnectionStringSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            var database = new Database(settings);
            List<IDatabaseObject> databaseObjects = new List<IDatabaseObject>();
            using (var command = database.GetSqlStringCommand("SELECT TABLE_NAME, TABLE_TYPE FROM INFORMATION_SCHEMA.TABLES"))
            using (var reader = database.ExecuteReader(command))
            {
                while (reader.Read())
                {
                    IDatabaseObject databaseObject = BuildDatabaseObject(reader);

                    databaseObjects.Add(databaseObject);
                }
            }

            return databaseObjects;
        }

        /// <summary>
        /// Gets the fields of a database object.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="databaseObject">The database object.</param>
        /// <returns>
        /// The list of <see cref="T:Estat.Sri.Mapping.Api.Model.IFieldInfo" />
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// settings
        /// or
        /// databaseObject
        /// </exception>
        public IEnumerable<IFieldInfo> GetSchema(ConnectionStringSettings settings, IDatabaseObject databaseObject)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }
            if (databaseObject == null)
            {
                throw new ArgumentNullException(nameof(databaseObject));
            }

            var database = new Database(settings);
            List<IFieldInfo> fields = new List<IFieldInfo>();
            using (var command = database.GetSqlStringCommand("SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS"))
            using (var reader = database.ExecuteReader(command))
            {
                while (reader.Read())
                {
                    IFieldInfo field = new FieldInfo() { Name = DataReaderHelper.GetString(reader, 0), DataType = DataReaderHelper.GetString(reader, 1) };
                    fields.Add(field);
                }
            }

            return fields;
        }

        /// <summary>
        /// Builds the database object.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns>The <see cref="IDatabaseObject"/></returns>
        private static IDatabaseObject BuildDatabaseObject(IDataReader reader)
        {
            var tableType = DataReaderHelper.GetString(reader, 1);
            var objectType = "BASE_TABLE".Equals(tableType, StringComparison.OrdinalIgnoreCase)
                                 ? DatabaseObjectType.Table
                                 : "VIEW".Equals(tableType, StringComparison.OrdinalIgnoreCase)
                                       ? DatabaseObjectType.View
                                       : DatabaseObjectType.Unknown;
            return new DatabaseObject() { Name = DataReaderHelper.GetString(reader, 0), ObjectType = objectType };
        }

        /// <summary>
        /// Builds the connection entity.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="settings">The settings.</param>
        /// <returns>The <see cref="IConnectionEntity"/></returns>
        private IConnectionEntity BuildConnectionEntity(DbConnectionStringBuilder builder, ConnectionStringSettings settings)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            IConnectionEntity connectionEntity = new ConnectionEntity();
            connectionEntity.DbType = Name;
            if (settings != null)
            {
                connectionEntity.Name = settings.Name;
                builder.ConnectionString = settings.ConnectionString;
            }

            foreach (DictionaryEntry entry in builder)
            {
                var key = entry.Key.ToString().ToUpperInvariant();
                var value = entry.Value?.ToString().Trim();
                switch (key)
                {
                    case "USER ID":
                        connectionEntity.DbUser = value;
                        AddOtherSetting(connectionEntity, "User ID", ParameterType.String, value, required: true);
                        break;
                    case "INITIAL CATALOG":
                    case "DATABASE":
                        connectionEntity.DbName = value;

                        AddOtherSetting(
                            connectionEntity,
                            "Initial Catalog",
                            ParameterType.String,
                            value,
                            required: true);

                        break;
                    case "PASSWORD":
                        connectionEntity.Password = value;
                        AddOtherSetting(connectionEntity, "Password", ParameterType.Password, value, required: true);

                        break;
                    case "DATA SOURCE":
                    case "SERVER":
                    case "ADDRESS":
                    case "NETWORK ADDRESS":
                        SetServer(value, connectionEntity);

                        AddOtherSetting(connectionEntity, "Data Source", ParameterType.String, value, required: true);
                        break;
                    case "INTEGRATED SECURITY":
                        bool isTrue = false;
                        if (!string.IsNullOrWhiteSpace(value))
                        {
                            isTrue = _integratedSecurityTrueValues.Contains(value, StringComparer.OrdinalIgnoreCase);
                        }

                        AddOtherSetting(
                            connectionEntity,
                            "Integrated Security",
                            ParameterType.Boolean,
                            isTrue,
                            required: true);

                        break;
                    default:
                        var type = entry.Value?.GetType();
                        AddOtherSetting(connectionEntity, entry.Key.ToString(), type.ToParameterType(), value);

                        break;
                }
            }

            return connectionEntity;
        }

        private static void SetServer(string value, IConnectionEntity connectionEntity)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                var splitValue = value.Split(',');
                if (splitValue.Length > 1)
                {
                    connectionEntity.Server = splitValue[0].Trim();
                    int port;
                    if (int.TryParse(splitValue[1], out port))
                    {
                        connectionEntity.Port = port;
                    }
                }
                else
                {
                    connectionEntity.Server = value;
                }
            }
        }

        private void AddOtherSetting(IConnectionEntity connectionEntity, string key, ParameterType type, object value, bool advancedSetting = false, bool required = false)
        {
            connectionEntity.AdvancedSettings[key] = new ConnectionParameterEntity()
                                                         {
                                                             Advanced = advancedSetting,
                                                             Type = type,
                                                             Value = value,
                                                             Required = required
                                                         };
        }
    }
}