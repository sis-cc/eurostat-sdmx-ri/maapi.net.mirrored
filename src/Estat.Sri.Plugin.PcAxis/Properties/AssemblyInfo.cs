﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("European Commision (Eurostat)")]
[assembly: AssemblyProduct("Estat.Sri.Plugin.PcAxis")]
[assembly: AssemblyTrademark("Copyright (c) 2017")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3cb24957-66a7-481e-a199-4679bafdb82d")]
[assembly: AssemblyVersion("1.14.2")]
[assembly: AssemblyInformationalVersion("1.14.2")]
[assembly: NeutralResourcesLanguage("en")]
[assembly: CLSCompliant(true)]
[assembly: AssemblyTitle("Estat.Sri.Mapping.Api")]
[assembly: AssemblyDescription("A common abstract API for accessing a SDMX RI Mapping Store")] 
