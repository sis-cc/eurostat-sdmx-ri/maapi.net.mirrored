﻿// -----------------------------------------------------------------------
// <copyright file="IColumnInstance.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using System.ComponentModel;

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System.ComponentModel;
    /// <summary>
    ///     The ColumnInstance interface.
    /// </summary>
    public interface IColumnInstance
    {
        /// <summary>
        ///     Gets or sets the fixed value.
        /// </summary>
        [Browsable(false)]
        string FixedValue { get; set; }

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        [Browsable(false)]
        string ID { get; set; }

        /// <summary>
        ///     Gets or sets the index.
        /// </summary>
        [Browsable(false)]
        int Index { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is fixed value.
        /// </summary>
        [Browsable(false)]
        bool IsFixedValue { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is primary key.
        /// </summary>
        [Browsable(false)]
        bool IsPrimaryKey { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is returned.
        /// </summary>
        bool IsReturned { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        ///     Gets the query metadata.
        /// </summary>
        [Browsable(false)]
        IQueryMetadata QueryMetadata { get; }

        /// <summary>
        ///     Gets the table instance.
        /// </summary>
        [Browsable(false)]
        ITableInstance TableInstance { get; }

        /// <summary>
        ///     Gets or sets the type.
        /// </summary>
        string Type { get; set; }

        /// <summary>
        ///     The is merged.
        /// </summary>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        bool IsMerged();
    }
}