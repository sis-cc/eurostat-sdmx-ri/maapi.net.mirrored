﻿// -----------------------------------------------------------------------
// <copyright file="QueryMetadata.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     The query metadata.
    /// </summary>
    public class QueryMetadata : IQueryMetadata
    {
        /// <summary>
        ///     The _ column instances.
        /// </summary>
        private readonly ColumnInstancesCollection _columnInstances;

        /// <summary>
        ///     The _ columns merging.
        /// </summary>
        private readonly ColumnsMergingCollection _columnsMerging;

        /// <summary>
        ///     The _ column table references.
        /// </summary>
        private readonly ColumnTableReferencesCollection _columnTableReferences;

        /// <summary>
        ///     The _ constraints.
        /// </summary>
        private readonly ConstraintsCollection _constraints;

        /// <summary>
        ///     Gets the cross-join relations.
        /// </summary>
        private readonly CrossJoinRelationsCollection _crossJoinRelations;

        /// <summary>
        ///     The _ relations.
        /// </summary>
        private readonly RelationsCollection _relations;

        /// <summary>
        ///     The _ table constraints.
        /// </summary>
        private readonly List<ConstraintsCollection> _tableConstraints = new List<ConstraintsCollection>();

        /// <summary>
        ///     The _ table instances.
        /// </summary>
        private readonly TableInstancesCollection _tableInstances;

        /// <summary>
        ///     The _ union relations.
        /// </summary>
        private readonly UnionRelationsCollection _unionRelations;

        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryMetadata" /> class.
        /// </summary>
        public QueryMetadata()
        {
            this._columnInstances = new ColumnInstancesCollection(this);
            this._tableInstances = new TableInstancesCollection(this);
            this._relations = new RelationsCollection(this);
            this._columnTableReferences = new ColumnTableReferencesCollection(this);
            this._columnsMerging = new ColumnsMergingCollection(this);
            this._constraints = new ConstraintsCollection(this);
            this._unionRelations = new UnionRelationsCollection(this);
            this._crossJoinRelations = new CrossJoinRelationsCollection(this);
        }

        /// <summary>
        ///     Gets the column instances.
        /// </summary>
        public ColumnInstancesCollection ColumnInstances
        {
            get
            {
                return this._columnInstances;
            }
        }

        /// <summary>
        ///     Gets the columns merging.
        /// </summary>
        public ColumnsMergingCollection ColumnsMerging
        {
            get
            {
                return this._columnsMerging;
            }
        }

        /// <summary>
        ///     Gets the column table references.
        /// </summary>
        public ColumnTableReferencesCollection ColumnTableReferences
        {
            get
            {
                return this._columnTableReferences;
            }
        }

        /// <summary>
        ///     Gets the constraints.
        /// </summary>
        public ConstraintsCollection Constraints
        {
            get
            {
                return this._constraints;
            }
        }

        /// <summary>
        ///     Gets the cross-join relations.
        /// </summary>
        public CrossJoinRelationsCollection CrossJoinRelations
        {
            get
            {
                return this._crossJoinRelations;
            }
        }

        /// <summary>
        ///     Gets the relations.
        /// </summary>
        public RelationsCollection Relations
        {
            get
            {
                return this._relations;
            }
        }

        /// <summary>
        ///     Gets the table constraints.
        /// </summary>
        public ICollection<ConstraintsCollection> TableConstraints
        {
            get
            {
                return this._tableConstraints;
            }
        }

        /// <summary>
        ///     Gets the table instances.
        /// </summary>
        public TableInstancesCollection TableInstances
        {
            get
            {
                return this._tableInstances;
            }
        }

        /// <summary>
        ///     Gets the union relations.
        /// </summary>
        public UnionRelationsCollection UnionRelations
        {
            get
            {
                return this._unionRelations;
            }
        }

        /// <summary>
        ///     Adds the column table reference.
        /// </summary>
        /// <returns>
        ///     The <see cref="ColumnTableReference" />
        /// </returns>
        public IColumnTableReference AddColumnTableReference()
        {
            return new ColumnTableReference(this);
        }

        /// <summary>
        ///     Adds the constraints.
        /// </summary>
        /// <returns>
        ///     The <see cref="AllowedValues" />
        /// </returns>
        public IAllowedValues AddConstraints()
        {
            var allowedValues = new AllowedValues(this);
            this.Constraints.Add(allowedValues);
            return allowedValues;
        }

        /// <summary>
        ///     Adds the relation.
        /// </summary>
        /// <returns>
        ///     The <see cref="Relation" />
        /// </returns>
        public IRelation AddRelation()
        {
            return new Relation(this);
        }

        /// <summary>
        ///     Adds the table instance.
        /// </summary>
        /// <returns>
        ///     The <see cref="TableInstance" />
        /// </returns>
        public ITableInstance AddTableInstance()
        {
            return new TableInstance(this);
        }

        /// <summary>
        ///     The clone.
        /// </summary>
        /// <returns>
        ///     The <see cref="Estat.Ma.Model.QueryEditor.QueryMetadata" />.
        /// </returns>
        public IQueryMetadata Clone()
        {
            var queryMetadata = new QueryMetadataReaderWriter();
            var s = queryMetadata.Write(this);
            return queryMetadata.Read(s);
        }

        /// <summary>
        ///     The has unconnected tables.
        /// </summary>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        public bool HasUnconnectedTables()
        {
            if (this.TableInstances.Count <= 1)
            {
                return false;
            }

            var visited = new Dictionary<string, object>(StringComparer.Ordinal);
            var stack = new Stack<ITableInstance>();
            var tableInstance = this._tableInstances[0];
            var relationCollections = new List<IRelationBase>();

            if (tableInstance.HasColumnTableRelation() || tableInstance.HasInnerJoinRelation())
            {
                foreach (var columnTableReference in this._columnTableReferences)
                {
                    relationCollections.Add(columnTableReference);
                }

                foreach (var relation in this._relations)
                {
                    relationCollections.Add(relation);
                }
            }
            else if (tableInstance.HasUnionRelation())
            {
                foreach (var relation in this._unionRelations)
                {
                    relationCollections.Add(relation);
                }
            }
            else if (tableInstance.HasCrossJoinRelation())
            {
                foreach (var relation in this._crossJoinRelations)
                {
                    relationCollections.Add(relation);
                }
            }

            stack.Push(tableInstance);

            while (stack.Count > 0)
            {
                var ti = stack.Pop();
                if (!visited.ContainsKey(ti.ID))
                {
                    visited.Add(ti.ID, null);
                    PushNextRelation(ti, stack, relationCollections);
                }
            }

            if (visited.Count < this._tableInstances.Count)
            {
                return true;
            }

            ///// OLD CODE commnent out after MAT-523 fix
            ////foreach (TableInstance ti in this.TableInstances)
            ////{
            ////    bool isConnected = ti.HasRelationOfAnyType();

            ////    if (!isConnected)
            ////    {
            ////        return true;
            ////    }
            ////}
            var retval = !this.AreAllUnionTablesConnected();

            return retval;
        }

        /// <summary>
        ///     The remove merged column.
        /// </summary>
        /// <param name="col">
        ///     The col.
        /// </param>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        public bool RemoveMergedColumn(ColumnInstancesCollection col)
        {
            if (this.ColumnsMerging.GeneratedMeasureColumnInstance != null && this.ColumnsMerging.GeneratedDimensionColumnInstance != null)
            {
                this.ColumnInstances.Remove(this.ColumnsMerging.GeneratedMeasureColumnInstance);
                this.ColumnsMerging.GeneratedMeasureColumnInstance.TableInstance.ColumnInstances.Remove(this.ColumnsMerging.GeneratedMeasureColumnInstance);
                this.ColumnInstances.Remove(this.ColumnsMerging.GeneratedDimensionColumnInstance);
                this.ColumnsMerging.GeneratedDimensionColumnInstance.TableInstance.ColumnInstances.Remove(this.ColumnsMerging.GeneratedDimensionColumnInstance);
                return true;
            }

            return false;
        }

        /// <summary>
        ///     Pushes the next relation.
        /// </summary>
        /// <typeparam name="T">
        ///     The relation type
        /// </typeparam>
        /// <param name="currentTable">
        ///     The current table.
        /// </param>
        /// <param name="stack">
        ///     The stack.
        /// </param>
        /// <param name="relations">
        ///     The relations.
        /// </param>
        private static void PushNextRelation<T>(ITableInstance currentTable, Stack<ITableInstance> stack, IEnumerable<T> relations) where T : IRelationBase
        {
            foreach (var relation in relations)
            {
                if (relation.TableInstanceFrom != null && relation.TableInstanceFrom.ID == currentTable.ID)
                {
                    if (relation.TableInstanceTo != null)
                    {
                        stack.Push(relation.TableInstanceTo);
                    }
                }
                else if (relation.TableInstanceTo != null && relation.TableInstanceTo.ID == currentTable.ID)
                {
                    if (relation.TableInstanceFrom != null)
                    {
                        stack.Push(relation.TableInstanceFrom);
                    }
                }
            }
        }

        /// <summary>
        ///     The are all union tables connected.
        /// </summary>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        private bool AreAllUnionTablesConnected()
        {
            ITableInstance start = null;
            ITableInstance end = null;
            foreach (var ti in this.TableInstances)
            {
                if (ti.HasOnlyUnionRelationFrom())
                {
                    if (start == null)
                    {
                        start = ti;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (ti.HasOnlyUnionRelationTo())
                {
                    if (end == null)
                    {
                        end = ti;
                    }
                    else
                    {
                        return false;
                    }
                }

                // else if (ti.HasUnionRelation()) {
                // //is a middle element in a multiple union relations 
                // }
            }

            return true;
        }
    }
}
