﻿// -----------------------------------------------------------------------
// <copyright file="TableRelationBase.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System;

    /// <summary>
    ///     The table relation base.
    /// </summary>
    public abstract class TableRelationBase : RelationBase
    {
        /// <summary>
        ///     The _ table instance from.
        /// </summary>
        private readonly ITableInstance _tableInstanceFrom;

        /// <summary>
        ///     The _ table instance to.
        /// </summary>
        private readonly ITableInstance _tableInstanceTo;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TableRelationBase" /> class.
        /// </summary>
        /// <param name="sourceTable">
        ///     The source Table.
        /// </param>
        /// <param name="destinationTable">
        ///     The destination Table.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="destinationTable" /> is null
        ///     -or-
        ///     <paramref name="sourceTable" /> is null
        /// </exception>
        protected TableRelationBase(ITableInstance sourceTable, ITableInstance destinationTable)
            : base(sourceTable)
        {
            if (sourceTable == null)
            {
                throw new ArgumentNullException("sourceTable");
            }

            if (destinationTable == null)
            {
                throw new ArgumentNullException("destinationTable");
            }

            this._tableInstanceFrom = sourceTable;
            this._tableInstanceTo = destinationTable;
        }

        /// <summary>
        ///     Gets or sets the table instance from.
        /// </summary>
        public override ITableInstance TableInstanceFrom
        {
            get
            {
                return this._tableInstanceFrom;
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        ///     Gets or sets the table instance to.
        /// </summary>
        public override ITableInstance TableInstanceTo
        {
            get
            {
                return this._tableInstanceTo;
            }

            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
