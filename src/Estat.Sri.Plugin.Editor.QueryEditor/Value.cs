﻿// -----------------------------------------------------------------------
// <copyright file="Value.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    /// <summary>
    ///     The value.
    /// </summary>
    public class Value
    {
        /// <summary>
        ///     The comparison operator used to compare against this value
        /// </summary>
        private string _operator;

        /// <summary>
        ///     The _ val.
        /// </summary>
        private string _val;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Value" /> class.
        /// </summary>
        public Value()
        {
            this._operator = Operators.Get(Operators.OperatorIndex.Equals);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Value" /> class.
        /// </summary>
        /// <param name="val">
        ///     The val.
        /// </param>
        /// <param name="op">
        ///     The op.
        /// </param>
        public Value(string val, string op)
        {
            this._val = val;
            this._operator = op;
        }

        /// <summary>
        /// Gets or sets the operator.
        /// </summary>
        /// <value>
        /// The operator.
        /// </value>
        public string Operator
        {
            get
            {
                return this._operator;
            }

            set
            {
                this._operator = value;
            }
        }

        /// <summary>
        ///     Gets or sets the val.
        /// </summary>
        public string Val
        {
            get
            {
                return this._val;
            }

            set
            {
                this._val = value;
            }
        }
    }
}
