using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    public static class JTokenExtension
    {
        public static List<JToken> GetChildren(this JToken jObject, string property)
        {
            var selectToken = jObject.SelectToken(property);
            if (selectToken is JArray)
            {
                return selectToken.ToList();
            }
            if (selectToken is JObject)
            {
                return new List<JToken>()
                {
                    selectToken
                };
            }
            return null;
        }
    }
}