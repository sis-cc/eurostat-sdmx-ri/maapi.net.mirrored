﻿// -----------------------------------------------------------------------
// <copyright file="FixedQueryPart.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System.Text;

    /// <summary>
    /// FixedQueryPart
    /// </summary>
    public class FixedQueryPart
    {
        /// <summary>
        /// Does the specified query metadata.
        /// </summary>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <param name="tableInstance">The ugly table.</param>
        /// <param name="columnsMergedTable">The columns merged table.</param>
        /// <param name="columnsCollection">The ugly columns collection.</param>
        public StringBuilder Build(IQueryMetadata queryMetadata, ITableInstance tableInstance, QueryGeneratorBase.CompositeTableInstance columnsMergedTable, ColumnsMergingCollection columnsCollection)
        {
            var fixedInnerSelectPart = new StringBuilder("select ");
            var selectStarted = false;
            foreach (var col in tableInstance.ColumnInstances)
            {
                // create columns of composite table for all columns but the ones to be merged
                if (!queryMetadata.ColumnsMerging.Contains(col))
                {
                    var compCol = columnsMergedTable.AddColumnInstance();
                    compCol.Name = col.Name;
                    compCol.ID = col.ID;
                    compCol.IsPrimaryKey = col.IsPrimaryKey;
                    compCol.IsReturned = col.IsReturned;
                    compCol.Type = col.Type;
                    compCol.Index = col.Index;

                    // compCol.IsFixedValue = col.IsFixedValue;
                    // compCol.FixedValue = col.FixedValue;

                    // map the constraints to the newly created column instances
                    foreach (var constraint in queryMetadata.Constraints)
                    {
                        if (constraint.ColumnInstance.ID == compCol.ID)
                        {
                            constraint.ColumnInstance = compCol;
                        }
                    }
                }

                if (!columnsCollection.Contains(col))
                {
                    if (!col.IsReturned)
                    {
                        continue;
                    }
                    if (selectStarted)
                    {
                        fixedInnerSelectPart.Append(", ");
                    }

                    if (col.IsFixedValue)
                    {
                        fixedInnerSelectPart.Append("'" + Utils.GetEscapedString(col.FixedValue) + "' as " + col.Name);
                    }
                    else
                    {
                        fixedInnerSelectPart.Append(col.Name);
                    }

                    selectStarted = true;
                }
            }

            return fixedInnerSelectPart;
        }
    }
}
