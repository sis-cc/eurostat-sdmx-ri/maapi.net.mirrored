﻿// -----------------------------------------------------------------------
// <copyright file="Relation.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    /// <summary>
    ///     The relation.
    /// </summary>
    public class Relation : RelationBase, IRelation
    {
        /// <summary>
        ///     The _ column mappings.
        /// </summary>
        private readonly ColumnMappingsCollection _columnMappings;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Relation" /> class.
        /// </summary>
        /// <param name="querymetadata">
        ///     The query metadata.
        /// </param>
        public Relation(IQueryMetadata querymetadata)
            : base(querymetadata)
        {
            this.QueryMetadata.Relations.DoAdd(this);

            this._columnMappings = new ColumnMappingsCollection(this);
        }

        /// <summary>
        ///     Gets the column mappings.
        /// </summary>
        public ColumnMappingsCollection ColumnMappings
        {
            get
            {
                return this._columnMappings;
            }
        }

        /// <summary>
        ///     Gets or sets the table instance from.
        /// </summary>
        public override ITableInstance TableInstanceFrom { get; set; }

        /// <summary>
        ///     Gets or sets the table instance to.
        /// </summary>
        public override ITableInstance TableInstanceTo { get; set; }

        /// <summary>
        ///     Add a new column mapping.
        /// </summary>
        /// <returns>
        ///     The <see cref="ColumnMapping" />
        /// </returns>
        public IColumnMapping AddColumnMapping()
        {
            return new ColumnMapping(this);
        }
    }
}
