// -----------------------------------------------------------------------
// <copyright file="JsonFormatExtractor.cs" company="EUROSTAT">
//   Date Created : 2018-07-19
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    public class JsonFormatExtractor : IFormatSpecificExtractor
    {
        private readonly IQueryMetadata _queryMetadata;
        private readonly Dictionary<string, ITableInstance> _tableInstanceIdDict;
        private readonly Dictionary<string, ITableInstance> _tableInstanceColumnIdDict;
        private readonly Dictionary<string, int> _columnInstanceSortIndexDict;
        private readonly Dictionary<string, IColumnInstance> _columnInstanceIdDict;

        public JsonFormatExtractor(IQueryMetadata queryMetadata, Dictionary<string, ITableInstance> tableInstanceIdDict, Dictionary<string, ITableInstance> tableInstanceColumnIdDict,
            Dictionary<string, int> columnInstanceSortIndexDict, Dictionary<string, IColumnInstance> columnInstanceIdDict)
        {
            if (queryMetadata == null)
            {
                throw new ArgumentNullException(nameof(queryMetadata));
            }

            if (tableInstanceIdDict == null)
            {
                throw new ArgumentNullException(nameof(tableInstanceIdDict));
            }

            if (tableInstanceColumnIdDict == null)
            {
                throw new ArgumentNullException(nameof(tableInstanceColumnIdDict));
            }

            if (columnInstanceSortIndexDict == null)
            {
                throw new ArgumentNullException(nameof(columnInstanceSortIndexDict));
            }

            if (columnInstanceIdDict == null)
            {
                throw new ArgumentNullException(nameof(columnInstanceIdDict));
            }

            this._queryMetadata = queryMetadata;
            this._tableInstanceIdDict = tableInstanceIdDict;
            this._tableInstanceColumnIdDict = tableInstanceColumnIdDict;
            this._columnInstanceSortIndexDict = columnInstanceSortIndexDict;
            this._columnInstanceIdDict = columnInstanceIdDict;
        }

        public IQueryMetadata Read(string value)
        {
            var jsonObject = JObject.Parse(value);

            // get columnsInstances of Dataset
            var tableInstancesNodes = jsonObject.SelectToken("Dataset.TableInstances");
            if (tableInstancesNodes != null)
            {
                this.HandleSingleTableInstanceNode(tableInstancesNodes, this._queryMetadata, this._tableInstanceIdDict, this._tableInstanceColumnIdDict, this._columnInstanceSortIndexDict);
            }

            // get columnsInstances of Dataset
            var columnInstancesNodes = jsonObject.SelectToken("Dataset.ColumnInstances");
            if (columnInstancesNodes != null)
            {
                // throw new Exception("Invalid QueryMetadata XML: number ColumnInstances != 1");
                foreach (var columnInstnanceNode in columnInstancesNodes.GetChildren("ColumnInstance"))
                {
                    // TODO check Node is an Element type

                    var columnInstance = this.ReadColumnInstance(columnInstnanceNode, this._tableInstanceColumnIdDict, this._columnInstanceSortIndexDict);

                    this._columnInstanceIdDict.Add(columnInstance.ID, columnInstance);

                    // Boolean.TryParse(columnInstanceEl.GetAttribute("IsPrimaryKey"), columnInstance.IsPrimaryKey);
                    // columnInstance.IsReturned = columnInstanceEl.GetAttribute("IsReturned");
                }
            }

            // get relations of Dataset
            var relationsNodes = jsonObject.SelectToken("Dataset.Relations");
            if (relationsNodes != null)
            {
                // throw new Exception("Invalid QueryMetadata XML: number Relations != 1");
                foreach (var relationNode in relationsNodes.GetChildren("Relation"))
                {
                    // TODO check Node is an Element type

                    var relation = this.CreateRelation(this._queryMetadata, relationNode, this._tableInstanceIdDict);

                    foreach (var columnMappingNode in relationNode.GetChildren("ColumnMapping"))
                    {
                        this.ReadColumnMapping(relation, this._columnInstanceIdDict, columnMappingNode);
                    }
                }
            }

            // get ColumnTableReferences of Dataset
            var columnTableReferencesNodes = jsonObject.SelectToken("Dataset.ColumnTableReferences");
            if (columnTableReferencesNodes != null)
            {
                // throw new Exception("Invalid QueryMetadata XML: number ColumnTableReferences != 1");
                foreach (var columnTableReferenceNode in columnTableReferencesNodes.GetChildren("ColumnTableReference"))
                {
                    // TODO check Node is an Element type

                    this.ReadColumnTableReference(this._queryMetadata, this._columnInstanceIdDict, columnTableReferenceNode, this._tableInstanceIdDict);
                }
            }

            // get ColumnsMerging of Dataset
            var columnsMergingNodes = jsonObject.SelectToken("Dataset.ColumnsMerging");
            if (columnsMergingNodes != null)
            {
                this.ReadColumnMerging(columnsMergingNodes, this._columnInstanceIdDict, this._queryMetadata);
            }

            // get Where details
            var whereDetailsNodes = jsonObject.SelectToken("Dataset.Constraints");
            if (whereDetailsNodes != null)
            {
                this.GetWhereDetailsWhenCountIsOne(whereDetailsNodes, this._queryMetadata, this._columnInstanceIdDict);
            }

            // get TableCostraints details
            var tableConstraintsNodes = jsonObject.SelectToken("Dataset.TableConstraints");
            if (tableConstraintsNodes != null)
            {
                this.GetTableConstraints(tableConstraintsNodes, this._tableInstanceIdDict, this._columnInstanceIdDict, this._queryMetadata);
            }

            // get Unions
            var unionNodes = jsonObject.SelectToken("Dataset.Unions");
            if (unionNodes != null)
            {
                foreach (var union in unionNodes.GetChildren("UnionRelation"))
                {
                    this.ReadUnion(union, this._tableInstanceIdDict);

                    // queryMetadata.UnionRelations.Add(urelation);
                }
            }

            // get Unions
            var crossJoinNodes = jsonObject.SelectToken("Dataset.CrossJoins");
            if (crossJoinNodes != null)
            {
                foreach (var crossJoin in crossJoinNodes.GetChildren("CrossRelation"))
                {
                    this.ReadCrossJoin(this._tableInstanceIdDict, crossJoin);

                    // queryMetadata.UnionRelations.Add(urelation);
                }
            }

            foreach (var ti in this._queryMetadata.TableInstances)
            {
                ti.ColumnInstances.Sort();
            }

            return this._queryMetadata;
        }

        /// <summary>
        ///     Creates the relation.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="relationNodeEl">
        ///     The relation node element.
        /// </param>
        /// <param name="tableInstanceIdDict">
        ///     The table instance id dictionary.
        /// </param>
        /// <returns>
        ///     The <see cref="IRelation" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML: Relation with TableInstanceRefFrom not found
        ///     or
        ///     Invalid QueryMetadata XML: Relation with TableInstanceRefTo not found
        /// </exception>
        protected virtual IRelation CreateRelation(IQueryMetadata queryMetadata, JToken relationNodeEl, IDictionary<string, ITableInstance> tableInstanceIdDict)
        {
            var relation = queryMetadata.AddRelation();
            string tableInstanceFromId = relationNodeEl.Value<string>("TableInstanceRefFrom");
            ITableInstance tableInstanceFrom;
            if (!tableInstanceIdDict.TryGetValue(tableInstanceFromId, out tableInstanceFrom))
            {
                throw new Exception("Invalid QueryMetadata XML: Relation with TableInstanceRefFrom not found ");
            }

            relation.TableInstanceFrom = tableInstanceFrom;
            string tableInstanceToId = relationNodeEl.Value<string>("TableInstanceRefTo");
            ITableInstance tableInstanceTo;
            if (!tableInstanceIdDict.TryGetValue(tableInstanceToId, out tableInstanceTo))
            {
                throw new Exception("Invalid QueryMetadata XML: Relation with TableInstanceRefTo not found ");
            }

            relation.TableInstanceTo = tableInstanceTo;
            return relation;
        }

        /// <summary>
        ///     Reads the allowed values.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="columnInstanceIdDict">
        ///     The column instance id dictionary.
        /// </param>
        /// <param name="allowedValuesNodeEl">
        ///     The allowed values node element.
        /// </param>
        /// <returns>
        ///     The <see cref="IAllowedValues" />
        /// </returns>
        protected virtual IAllowedValues ReadAllowedValues(IQueryMetadata queryMetadata, Dictionary<string, IColumnInstance> columnInstanceIdDict, JToken allowedValuesNodeEl)
        {
            var allowedValues = queryMetadata.AddConstraints();

            return this.ReadAllowedValues(columnInstanceIdDict, allowedValuesNodeEl, allowedValues);
        }

        /// <summary>
        ///     Reads the allowed values.
        /// </summary>
        /// <param name="tableInstance">
        ///     The table instance.
        /// </param>
        /// <param name="columnInstanceIdDict">
        ///     The column instance id dictionary.
        /// </param>
        /// <param name="allowedValuesNodeEl">
        ///     The allowed values node element.
        /// </param>
        /// <returns>
        ///     The <see cref="IAllowedValues" />
        /// </returns>
        protected virtual IAllowedValues ReadAllowedValues(ITableInstance tableInstance, Dictionary<string, IColumnInstance> columnInstanceIdDict, JToken allowedValuesNodeEl)
        {
            var allowedValues = tableInstance.AddConstraint();

            return this.ReadAllowedValues(columnInstanceIdDict, allowedValuesNodeEl, allowedValues);
        }

        /// <summary>
        ///     Reads the allowed values.
        /// </summary>
        /// <param name="columnInstanceIdDict">
        ///     The column instance id dictionary.
        /// </param>
        /// <param name="allowedValuesNodeEl">
        ///     The allowed values node element.
        /// </param>
        /// <param name="allowedValues">
        ///     The allowed values.
        /// </param>
        /// <returns>
        ///     The <see cref="IAllowedValues" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML:  AllowedValues with ColumnInstanceRef not found
        /// </exception>
        protected virtual IAllowedValues ReadAllowedValues(Dictionary<string, IColumnInstance> columnInstanceIdDict, JToken allowedValuesNodeEl, IAllowedValues allowedValues)
        {
            IColumnInstance columnInstance;
            if (!columnInstanceIdDict.TryGetValue(allowedValuesNodeEl.Value<string>("ColumnInstanceRef"), out columnInstance))
            {
                throw new Exception("Invalid QueryMetadata XML:  AllowedValues with ColumnInstanceRef not found ");
            }

            allowedValues.ColumnInstance = columnInstance;

            // get logicalOperator attribute, default is OR
            var logicalOperator = allowedValuesNodeEl.Value<string>("logicalOperator");
            if (!string.IsNullOrEmpty(logicalOperator) && Enum.IsDefined(typeof(LogicalOperators), logicalOperator))
            {
                allowedValues.LogicalOperator = (LogicalOperators)Enum.Parse(typeof(LogicalOperators), logicalOperator);
            }

            return allowedValues;
        }

        /// <summary>
        ///     Reads the column instance.
        /// </summary>
        /// <param name="columnInstanceEl">
        ///     The column instance element.
        /// </param>
        /// <param name="tableInstanceColumnIdDict">
        ///     The table instance column id dictionary.
        /// </param>
        /// <param name="columnInstanceSortIndexDict">
        ///     The column instance sort index dictionary.
        /// </param>
        /// <returns>
        ///     The <see cref="IColumnInstance" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid Query Metadata XML: ColumnInstance id not found
        /// </exception>
        protected virtual IColumnInstance ReadColumnInstance(JToken columnInstanceEl, Dictionary<string, ITableInstance> tableInstanceColumnIdDict, Dictionary<string, int> columnInstanceSortIndexDict)
        {
            // get table instance object given id
            var id = columnInstanceEl.Value<string>("ID");
            if (!tableInstanceColumnIdDict.ContainsKey(id))
            {
                throw new Exception("Invalid Query Metadata XML: ColumnInstance id '" + id + "' not found");
            }

            int index = columnInstanceSortIndexDict[id];
            ITableInstance tableInstance = tableInstanceColumnIdDict[id];
            var columnInstance = tableInstance.AddColumnInstance();
            columnInstance.ID = id;
            columnInstance.Index = index;
            columnInstance.Type = columnInstanceEl.Value<string>("Type");
            columnInstance.Name = columnInstanceEl.Value<string>("Name");
            if (!string.IsNullOrEmpty(columnInstanceEl.Value<string>("IsPrimaryKey")))
            {
                columnInstance.IsPrimaryKey = Convert.ToBoolean(columnInstanceEl.Value<string>("IsPrimaryKey"));
            }

            if (!string.IsNullOrEmpty(columnInstanceEl.Value<string>("IsReturned")))
            {
                columnInstance.IsReturned = Convert.ToBoolean(columnInstanceEl.Value<string>("IsReturned"));
            }

            if (!string.IsNullOrEmpty(columnInstanceEl.Value<string>("IsFixedValue")))
            {
                columnInstance.IsFixedValue = Convert.ToBoolean(columnInstanceEl.Value<string>("IsFixedValue"));
            }
            if (!string.IsNullOrEmpty(columnInstanceEl.Value<string>("FixedValue")))
            {
                columnInstance.FixedValue = columnInstanceEl.Value<string>("FixedValue");
            }
            
            return columnInstance;
        }

        /// <summary>
        ///     Reads the column mapping.
        /// </summary>
        /// <param name="relation">The relation.</param>
        /// <param name="columnInstanceIdDict">The column instance id dictionary.</param>
        /// <param name="columnMappingNodeEl">The column mapping node element.</param>
        /// <returns>
        ///     The <see cref="IColumnMapping" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefFrom not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefTo not found
        /// </exception>
        protected virtual IColumnMapping ReadColumnMapping(IRelation relation, Dictionary<string, IColumnInstance> columnInstanceIdDict, JToken columnMappingNodeEl)
        {
            var columnMapping = relation.AddColumnMapping();
            IColumnInstance columnInstanceFrom;
            if (!columnInstanceIdDict.TryGetValue(columnMappingNodeEl.Value<string>("ColumnInstanceRefFrom"), out columnInstanceFrom))
            {
                throw new Exception("Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefFrom not found ");
            }

            columnMapping.ColumnInstanceFrom = columnInstanceFrom;
            IColumnInstance columnInstanceTo;
            if (!columnInstanceIdDict.TryGetValue(columnMappingNodeEl.Value<string>("ColumnInstanceRefTo"), out columnInstanceTo))
            {
                throw new Exception("Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefTo not found ");
            }

            columnMapping.ColumnInstanceTo = columnInstanceTo;

            return columnMapping;
        }

        /// <summary>
        ///     Reads the column merging.
        /// </summary>
        /// <param name="columnsMergingNodeEl">The columns merging node element.</param>
        /// <param name="columnInstanceIdDict">The column instance id dictionary.</param>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <returns>
        ///     The <see cref="ColumnsMergingCollection" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        /// </exception>
        protected virtual ColumnsMergingCollection ReadColumnMerging(JToken columnsMergingNodeEl, Dictionary<string, IColumnInstance> columnInstanceIdDict, IQueryMetadata queryMetadata)
        {
            var generatedDimensionColumnInstanceRef = columnsMergingNodeEl.Value<string>("GeneratedDimensionColumnInstanceRef");
            var generatedMeasureColumnInstanceRef = columnsMergingNodeEl.Value<string>("GeneratedMeasureColumnInstanceRef");
            if (!string.IsNullOrEmpty(generatedDimensionColumnInstanceRef) && !string.IsNullOrEmpty(generatedMeasureColumnInstanceRef))
            {
                IColumnInstance generatedDimensionColumnInstance;
                if (!columnInstanceIdDict.TryGetValue(generatedDimensionColumnInstanceRef, out generatedDimensionColumnInstance))
                {
                    throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found ");
                }

                queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance = generatedDimensionColumnInstance;

                IColumnInstance generatedMeasureColumnInstance;
                if (!columnInstanceIdDict.TryGetValue(generatedMeasureColumnInstanceRef, out generatedMeasureColumnInstance))
                {
                    throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found ");
                }

                queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance = generatedMeasureColumnInstance;

                foreach (var columnMergingNode in columnsMergingNodeEl.GetChildren("ColumnInstanceRef"))
                {
                    IColumnInstance columnInstance;
                    if (!columnInstanceIdDict.TryGetValue(columnMergingNode.Value<string>("ID"), out columnInstance))
                    {
                        throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found ");
                    }

                    queryMetadata.ColumnsMerging.Add(columnInstance);
                }
            }

            return queryMetadata.ColumnsMerging;
        }

        /// <summary>
        ///     Reads the column table reference.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="columnInstanceIdDict">
        ///     The column instance id dictionary.
        /// </param>
        /// <param name="columnTableReferenceNodeEl">
        ///     The column table reference node element.
        /// </param>
        /// <param name="tableInstanceIdDict">
        ///     The table instance id dictionary.
        /// </param>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with TableInstanceRef not found
        /// </exception>
        protected virtual void ReadColumnTableReference(IQueryMetadata queryMetadata, Dictionary<string, IColumnInstance> columnInstanceIdDict, JToken columnTableReferenceNodeEl, Dictionary<string, ITableInstance> tableInstanceIdDict)
        {
            var columnTableReference = queryMetadata.AddColumnTableReference();
            IColumnInstance columnInstance;
            if (!columnInstanceIdDict.TryGetValue(columnTableReferenceNodeEl.Value<string>("ColumnInstanceRef"), out columnInstance))
            {
                throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found ");
            }

            columnTableReference.ColumnInstance = columnInstance;
            ITableInstance tableInstance;
            if (!tableInstanceIdDict.TryGetValue(columnTableReferenceNodeEl.Value<string>("TableInstanceRef"), out tableInstance))
            {
                throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with TableInstanceRef not found ");
            }

            columnTableReference.TableInstance = tableInstance;
        }

        /// <summary>
        ///     Reads the cross join.
        /// </summary>
        /// <param name="tableInstanceIdDict">
        ///     The table instance id dictionary.
        /// </param>
        /// <param name="crossJoinEl">
        ///     The cross join element.
        /// </param>
        protected virtual void ReadCrossJoin(Dictionary<string, ITableInstance> tableInstanceIdDict, JToken crossJoinEl)
        {
            var tinFrom = tableInstanceIdDict[crossJoinEl.Value<string>("TableInstanceRefFrom")];
            var tinTo = tableInstanceIdDict[crossJoinEl.Value<string>("TableInstanceRefTo")];

            tinFrom.AddCrossJoinRelation(tinTo);
        }

        /// <summary>
        ///     Reads the table instance.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="tableInstanceEl">
        ///     The table instance element.
        /// </param>
        /// <returns>
        ///     The <see cref="ITableInstance" />
        /// </returns>
        protected virtual ITableInstance ReadTableInstance(IQueryMetadata queryMetadata, JToken tableInstanceEl)
        {
            var tableInstance = queryMetadata.AddTableInstance();
            tableInstance.Name = tableInstanceEl.SelectToken("Name").Value<string>();
            tableInstance.ID = tableInstanceEl.SelectToken("ID").Value<string>();
            if (!string.IsNullOrEmpty(tableInstanceEl.SelectToken("XCoordinate").Value<string>()))
            {
                tableInstance.XCoordinate = Convert.ToInt32(tableInstanceEl.SelectToken("XCoordinate").Value<string>());
            }

            if (!string.IsNullOrEmpty(tableInstanceEl.SelectToken("YCoordinate").Value<string>()))
            {
                tableInstance.YCoordinate = Convert.ToInt32(tableInstanceEl.SelectToken("YCoordinate").Value<string>());
            }

            return tableInstance;
        }

        /// <summary>
        ///     Reads the union.
        /// </summary>
        /// <param name="union">
        ///     The union.
        /// </param>
        /// <param name="tableInstanceIdDict">
        ///     The table instance id dictionary.
        /// </param>
        protected virtual void ReadUnion(JToken union, Dictionary<string, ITableInstance> tableInstanceIdDict)
        {
            var tinFrom = tableInstanceIdDict[union.Value<string>("TableInstanceRefFrom")];
            var tinTo = tableInstanceIdDict[union.Value<string>("TableInstanceRefTo")];

            tinFrom.AddUnionRelation(tinTo);
        }

        /// <summary>
        ///     Reads the value.
        /// </summary>
        /// <param name="valueEl">
        ///     The value element.
        /// </param>
        /// <param name="allowedValues">
        ///     The allowed values.
        /// </param>
        protected virtual void ReadValue(JToken valueEl, IAllowedValues allowedValues)
        {
            // get the operator if it is set or default to "="
            var op = valueEl.Value<string>("operator");
            if (string.IsNullOrEmpty(op) || op.Trim().Length == 0)
            {
                // TODO check if the operator is valid
                op = Operators.Get(Operators.OperatorIndex.Equals);
            }

            allowedValues.Values.Add(new Value(valueEl.Value<string>("text"), op));
        }

        /// <summary>
        /// Gets the table constraints.
        /// </summary>
        /// <param name="tableConstraintsNodes">The table constraints nodes.</param>
        /// <param name="tableInstanceIdDict">The table instance identifier dictionary.</param>
        /// <param name="columnInstanceIdDict">The column instance identifier dictionary.</param>
        /// <param name="queryMetadata">The query metadata.</param>
        private void GetTableConstraints(JToken tableConstraintsNodes, Dictionary<string, ITableInstance> tableInstanceIdDict, Dictionary<string, IColumnInstance> columnInstanceIdDict, IQueryMetadata queryMetadata)
        {
            foreach (var tableConstraint in tableConstraintsNodes.GetChildren("TableConstraint"))
            {
                var tin = tableInstanceIdDict[tableConstraint.Value<string>("TableInstanceRef")];

                // ConstraintsCollection tableConstraints = new ConstraintsCollection(tin);
                foreach (var allowedValuesNode in tableConstraint.GetChildren("AllowedValues"))
                {
                    // TODO check Node is an Element type
                    if (allowedValuesNode != null)
                    {
                        var allowedValues = this.ReadAllowedValues(tin, columnInstanceIdDict, allowedValuesNode);

                        ReadAllowedValues(allowedValuesNode, allowedValues);
                    }
                }

                queryMetadata.TableConstraints.Add(tin.Constraints);
            }
        }

        private void ReadAllowedValues(JToken allowedValuesNode, IAllowedValues allowedValues)
        {
            foreach (var valueNode in allowedValuesNode.GetChildren("Value"))
            {
                if (valueNode != null)
                {
                    this.ReadValue(valueNode, allowedValues);
                }
            }
        }

        /// <summary>
        /// Gets the where details when count is one.
        /// </summary>
        /// <param name="whereDetailsNodes">The where details nodes.</param>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <param name="columnInstanceIdDict">The column instance identifier dictionary.</param>
        private void GetWhereDetailsWhenCountIsOne(JToken whereDetailsNodes, IQueryMetadata queryMetadata, Dictionary<string, IColumnInstance> columnInstanceIdDict)
        {
            foreach (var allowedValuesNode in whereDetailsNodes.GetChildren("AllowedValues"))
            {
                // TODO this code appears to be duplicated also QueryMetadataReaderWriterBase
                var allowedValues = this.ReadAllowedValues(queryMetadata, columnInstanceIdDict, allowedValuesNode);
                ReadAllowedValues(allowedValuesNode, allowedValues);
            }
        }

        /// <summary>
        /// Handles the single table instance node.
        /// </summary>
        /// <param name="tableInstances">The table instances nodes.</param>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <param name="tableInstanceIdDict">The table instance identifier dictionary.</param>
        /// <param name="tableInstanceColumnIdDict">The table instance column identifier dictionary.</param>
        /// <param name="columnInstanceSortIndexDict">The column instance sort index dictionary.</param>
        private void HandleSingleTableInstanceNode(JToken tableInstances, IQueryMetadata queryMetadata, Dictionary<string, ITableInstance> tableInstanceIdDict, Dictionary<string, ITableInstance> tableInstanceColumnIdDict, Dictionary<string, int> columnInstanceSortIndexDict)
        {
            // throw new Exception("Invalid QueryMetadata XML: number TableInstances != 1");
            foreach (var tableInstnanceNode in tableInstances.GetChildren("TableInstance"))
            {
                var tableInstance = this.ReadTableInstance(queryMetadata, tableInstnanceNode);

                tableInstanceIdDict.Add(tableInstance.ID, tableInstance);

                var i = 0;

                // get TableInstance's ColumnInstanceRefs
                foreach (var columnInstancRefNode in tableInstnanceNode.GetChildren("ColumnInstanceRefs.ColumnInstanceRef"))
                {
                    // TODO check Node is an Element type
                    var id = columnInstancRefNode.SelectToken("ID").Value<string>();
                    if (tableInstanceColumnIdDict.ContainsKey(id))
                    {
                        continue;
                    }
                    tableInstanceColumnIdDict.Add(id, tableInstance);
                    var index = i;
                    if (!string.IsNullOrEmpty(columnInstancRefNode.SelectToken("Index").Value<string>()))
                    {
                        index = int.Parse(columnInstancRefNode.SelectToken("Index").Value<string>());
                    }

                    columnInstanceSortIndexDict.Add(id, index);
                    i++;
                }
            }
        }
    }
}