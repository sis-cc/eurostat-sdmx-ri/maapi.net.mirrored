﻿// -----------------------------------------------------------------------
// <copyright file="ColumnInstance.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System;
    using System.ComponentModel;

    /// <summary>
    ///     The column instance.
    /// </summary>
    public class ColumnInstance : IColumnInstance
    {
        /// <summary>
        ///     The _ query metadata.
        /// </summary>
        private readonly IQueryMetadata _queryMetadata;

        /// <summary>
        ///     The _ table instance.
        /// </summary>
        private readonly ITableInstance _tableInstance;

        /// <summary>
        ///     The _ fixed value.
        /// </summary>
        private string _fixedValue = string.Empty;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ColumnInstance" /> class.
        /// </summary>
        /// <param name="ti">The table instance.</param>
        /// <exception cref="System.Exception">TableInstance does not belong to a QueryMetadata</exception>
        public ColumnInstance(ITableInstance ti)
        {
            this._tableInstance = ti;
            this._queryMetadata = ti.QueryMetadata;
            if (ti.QueryMetadata == null)
            {
                throw new Exception("TableInstance does not belong to a QueryMetadata");
            }

            this._queryMetadata.ColumnInstances.DoAdd(this);
            this._tableInstance.ColumnInstances.DoAdd(this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnInstance" /> class.
        /// </summary>
        /// <param name="ti">The table instance</param>
        /// <param name="mergeType">The merge type.</param>
        /// <exception cref="Exception">TableInstance does not belong to a QueryMetadata</exception>
        public ColumnInstance(ITableInstance ti, MergedColumnType mergeType)
        {
            this._tableInstance = ti;
            this._queryMetadata = ti.QueryMetadata;
            if (ti.QueryMetadata == null)
            {
                throw new Exception("TableInstance does not belong to a QueryMetadata");
            }

            this._queryMetadata.ColumnInstances.DoAdd(this);
            if (mergeType == MergedColumnType.IsMergedDimension)
            {
                this._queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance = this;
            }
            else if (mergeType == MergedColumnType.IsMergedMeasure)
            {
                this._queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance = this;
            }

            this._tableInstance.ColumnInstances.DoAdd(this);
        }

        /// <summary>
        ///     Gets or sets the fixed value.
        /// </summary>
        [Browsable(false)]
        public string FixedValue
        {
            get
            {
                return this._fixedValue;
            }

            set
            {
                this._fixedValue = value;
            }
        }

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        [Browsable(false)]
        public string ID { get; set; }

        /// <summary>
        ///     Gets or sets the index.
        /// </summary>
        [Browsable(false)]
        public int Index { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is fixed value.
        /// </summary>
        [Browsable(false)]
        public bool IsFixedValue { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is primary key.
        /// </summary>
        [Browsable(false)]
        public bool IsPrimaryKey { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is returned.
        /// </summary>
        public bool IsReturned { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Gets the query metadata.
        /// </summary>
        [Browsable(false)]
        public IQueryMetadata QueryMetadata
        {
            get
            {
                return this._queryMetadata;
            }
        }

        /// <summary>
        ///     Gets the table instance.
        /// </summary>
        [Browsable(false)]
        public ITableInstance TableInstance
        {
            get
            {
                return this._tableInstance;
            }
        }

        /// <summary>
        ///     Gets or sets the type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        ///     The is merged.
        /// </summary>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        public bool IsMerged()
        {
            return this._queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance == this || this._queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance == this;
        }

        // [Browsable(false)]

        // public bool IsUsedInMappingSet { get; set; }
    }
}
