// -----------------------------------------------------------------------
// <copyright file="QueryMetadataReaderWriterBase.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Runtime.Serialization.Json;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Util.Io;
using Org.Sdmxsource.Util.Xml;

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Xml;

    /// <summary>
    ///     The abstract class for <see cref="QueryMetadata" /> readers and writers.
    /// </summary>
    public class QueryMetadataReaderWriterBase : IQueryMetadataReaderWriter
    {
        /// <inheritdoc />
        /// <summary>
        ///     Reads the specified XML string.
        /// </summary>
        /// <param name="value">
        ///     The XML string.
        /// </param>
        /// <returns>
        ///     The <see cref="T:Estat.Sri.Plugin.Editor.QueryEditor.QueryMetadata" />
        /// </returns>
        /// <exception cref="T:System.Exception">
        ///     Invalid Query Metadata XML: ColumnInstance id ' + id + ' not found
        ///     or
        ///     Invalid QueryMetadata XML: Relation with TableInstanceRefFrom not found
        ///     or
        ///     Invalid QueryMetadata XML: Relation with TableInstanceRefTo not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefFrom not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefTo not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with TableInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML:  AllowedValues with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML:  AllowedValues with ColumnInstanceRef not found
        /// </exception>
        public IQueryMetadata Read(string value)
        {
            var isXml = XmlUtil.IsXML(new MemoryReadableLocation(Encoding.Unicode.GetBytes(value)));
            var queryMetadata = this.CreateQueryMetadata();
            var tableInstanceIdDict = new Dictionary<string, ITableInstance>();
            var columnInstanceIdDict = new Dictionary<string, IColumnInstance>();
            var tableInstanceColumnIdDict = new Dictionary<string, ITableInstance>();
            var columnInstanceSortIndexDict = new Dictionary<string, int>();
            var formatSpecificExtractor = isXml ? new XmlFormatExtractor(queryMetadata, tableInstanceIdDict, tableInstanceColumnIdDict, columnInstanceSortIndexDict, columnInstanceIdDict) : (IFormatSpecificExtractor)new JsonFormatExtractor(queryMetadata, tableInstanceIdDict, tableInstanceColumnIdDict, columnInstanceSortIndexDict, columnInstanceIdDict);
            return formatSpecificExtractor.Read(value);
        }

        /// <inheritdoc />
        /// <summary>
        /// Writes the specified query metadata.
        /// </summary>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <returns>
        /// The XML as a string.
        /// </returns>
        public string Write(IQueryMetadata queryMetadata)
        {
            var datasetJson = new JObject();
            

            // column instances
            var columnInstanceJson = new JObject();
            var columnInstanceArray = new JArray();
            foreach (var columnInstance in queryMetadata.ColumnInstances)
            {
                columnInstanceArray.Add(this.CreateColumnInstance(columnInstance));
            }
            columnInstanceJson["ColumnInstance"] = columnInstanceArray;

            if (columnInstanceJson.HasValues)
            {
                datasetJson["ColumnInstances"] = columnInstanceJson;
            }

            // table instances
            var tableInstanceJson = new JObject();
            var tableInstanceArray = new JArray();
            foreach (var tableInstance in queryMetadata.TableInstances)
            {
                tableInstanceArray.Add(this.WriteTableInstances(tableInstance));
            }
            tableInstanceJson["TableInstance"] = tableInstanceArray;
            if (tableInstanceJson.HasValues)
            {
                datasetJson["TableInstances"] = tableInstanceJson;
            }

            //return datasetJson.ToString();
            //relations
            var relationsJson = new JObject();
            var relationJson = new JArray();
            foreach (var relation in queryMetadata.Relations)
            {
                relationJson.Add(this.WriteRelations(relation));
            }
            relationsJson["Relation"] = relationJson;
            if (relationsJson.HasValues)
            {
                datasetJson["Relations"] = relationsJson;
            }

            // column table references
            var columnTableRederenceArray = new JArray();
            foreach (var columnTableReference in queryMetadata.ColumnTableReferences)
            {
                var columnTableReferenceJson = new JObject
                {
                    ["ColumnTableReference"] = this.WriteColumnTableReferences(columnTableReference)
                };
                columnInstanceArray.Add(columnTableReferenceJson);
            }

            if (columnTableRederenceArray.HasValues)
            {
                datasetJson["ColumnTableReferences"] = columnTableRederenceArray;
            }

            // ColumnsMerging
            var columnMergingJson = new JObject();
            if (queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance != null && queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance != null)
            {
                this.WriteColumnsMerging(columnMergingJson,queryMetadata);
                var columnInstanceRefArray = new JArray();
                foreach (var columnInstance in queryMetadata.ColumnsMerging)
                {
                    columnInstanceRefArray.Add(this.WriteColumnInstanceRef(columnInstance));
                }
                columnMergingJson["ColumnInstanceRef"] = columnInstanceRefArray;
                if (columnMergingJson.HasValues)
                {
                    datasetJson["ColumnsMerging"] = columnMergingJson;
                }
            }

            // Where Details
            
            var constraintsJson = new JObject();
            var allowedValuesArray = new JArray();
            foreach (var allowedValues in queryMetadata.Constraints)
            {
                
                var allowedValuesJson = new JObject();
                this.WriteAllowedValues(allowedValuesJson, allowedValues);
                var valuesArray = new JArray();
                foreach (var value in allowedValues.Values)
                {
                    valuesArray.Add(this.WriteValue(value));
                    
                }
                allowedValuesJson["Value"] = valuesArray;
                allowedValuesArray.Add(allowedValuesJson);
            }
            if (allowedValuesArray.HasValues)
            {
                constraintsJson["AllowedValues"] = allowedValuesArray;
            }
            if (constraintsJson.HasValues)
            {
                datasetJson["Constraints"] = constraintsJson;
            }

            // Table Constraints Details
            var tableContraintsArray = new JArray();
            foreach (var constraints in queryMetadata.TableConstraints)
            {
                tableContraintsArray.Add(this.GetTableConstraints(constraints));
            }

            if (tableContraintsArray.HasValues)
            {
                datasetJson["TableConstraints"] = new JObject()
                {
                    ["TableConstraint"] = tableContraintsArray
                };
            }

            // unions 
            var unionsArray = new JArray();
            
            foreach (var relation in queryMetadata.UnionRelations)
            {
                var unionJson = new JObject();
                this.WriteUnion(unionJson, relation);
                unionsArray.Add(unionJson);
            }

            if (unionsArray.HasValues)
            {
                datasetJson["Unions"] = new JObject()
                {
                    ["UnionRelation"] = unionsArray
                };
            }

            // cross-joins 
            
            var crossJoinsArray = new JArray();
            foreach (var relation in queryMetadata.CrossJoinRelations)
            {
                var crossJoinJson = new JObject();
                this.WriteCrossJoin(crossJoinJson, relation);
                crossJoinsArray.Add(crossJoinJson);
            }

            if (crossJoinsArray.HasValues)
            {
                datasetJson["CrossJoins"] = new JObject()
                {
                   ["CrossRelation"] = crossJoinsArray
                };
            }

            return new JObject()
            {
                ["Dataset"] = datasetJson
            }.ToString();
        }

        /// <summary>
        /// Creates the query metadata.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryMetadata" />
        /// </returns>
        protected virtual IQueryMetadata CreateQueryMetadata()
        {
            return new QueryMetadata();
        }

        /// <summary>
        ///     Creates the relation.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="relationNodeEl">
        ///     The relation node element.
        /// </param>
        /// <param name="tableInstanceIdDict">
        ///     The table instance id dictionary.
        /// </param>
        /// <returns>
        ///     The <see cref="IRelation" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML: Relation with TableInstanceRefFrom not found
        ///     or
        ///     Invalid QueryMetadata XML: Relation with TableInstanceRefTo not found
        /// </exception>
        protected virtual IRelation CreateRelation(IQueryMetadata queryMetadata, JToken relationNodeEl, IDictionary<string, ITableInstance> tableInstanceIdDict)
        {
            var relation = queryMetadata.AddRelation();
            string tableInstanceFromId = relationNodeEl.Value<string>("TableInstanceRefFrom");
            ITableInstance tableInstanceFrom;
            if (!tableInstanceIdDict.TryGetValue(tableInstanceFromId, out tableInstanceFrom))
            {
                throw new Exception("Invalid QueryMetadata XML: Relation with TableInstanceRefFrom not found ");
            }

            relation.TableInstanceFrom = tableInstanceFrom;
            string tableInstanceToId = relationNodeEl.Value<string>("TableInstanceRefTo");
            ITableInstance tableInstanceTo;
            if (!tableInstanceIdDict.TryGetValue(tableInstanceToId, out tableInstanceTo))
            {
                throw new Exception("Invalid QueryMetadata XML: Relation with TableInstanceRefTo not found ");
            }

            relation.TableInstanceTo = tableInstanceTo;
            return relation;
        }

        /// <summary>
        ///     Reads the allowed values.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="columnInstanceIdDict">
        ///     The column instance id dictionary.
        /// </param>
        /// <param name="allowedValuesNodeEl">
        ///     The allowed values node element.
        /// </param>
        /// <returns>
        ///     The <see cref="IAllowedValues" />
        /// </returns>
        protected virtual IAllowedValues ReadAllowedValues(IQueryMetadata queryMetadata, Dictionary<string, IColumnInstance> columnInstanceIdDict, JToken allowedValuesNodeEl)
        {
            var allowedValues = queryMetadata.AddConstraints();

            return this.ReadAllowedValues(columnInstanceIdDict, allowedValuesNodeEl, allowedValues);
        }

        /// <summary>
        ///     Reads the allowed values.
        /// </summary>
        /// <param name="tableInstance">
        ///     The table instance.
        /// </param>
        /// <param name="columnInstanceIdDict">
        ///     The column instance id dictionary.
        /// </param>
        /// <param name="allowedValuesNodeEl">
        ///     The allowed values node element.
        /// </param>
        /// <returns>
        ///     The <see cref="IAllowedValues" />
        /// </returns>
        protected virtual IAllowedValues ReadAllowedValues(ITableInstance tableInstance, Dictionary<string, IColumnInstance> columnInstanceIdDict, JToken allowedValuesNodeEl)
        {
            var allowedValues = tableInstance.AddConstraint();

            return this.ReadAllowedValues(columnInstanceIdDict, allowedValuesNodeEl, allowedValues);
        }

        /// <summary>
        ///     Reads the allowed values.
        /// </summary>
        /// <param name="columnInstanceIdDict">
        ///     The column instance id dictionary.
        /// </param>
        /// <param name="allowedValuesNodeEl">
        ///     The allowed values node element.
        /// </param>
        /// <param name="allowedValues">
        ///     The allowed values.
        /// </param>
        /// <returns>
        ///     The <see cref="IAllowedValues" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML:  AllowedValues with ColumnInstanceRef not found
        /// </exception>
        protected virtual IAllowedValues ReadAllowedValues(Dictionary<string, IColumnInstance> columnInstanceIdDict, JToken allowedValuesNodeEl, IAllowedValues allowedValues)
        {
            IColumnInstance columnInstance;
            if (!columnInstanceIdDict.TryGetValue(allowedValuesNodeEl.Value<string>("ColumnInstanceRef"), out columnInstance))
            {
                throw new Exception("Invalid QueryMetadata XML:  AllowedValues with ColumnInstanceRef not found ");
            }

            allowedValues.ColumnInstance = columnInstance;

            // get logicalOperator attribute, default is OR
            var logicalOperator = allowedValuesNodeEl.Value<string>("logicalOperator");
            if (!string.IsNullOrEmpty(logicalOperator) && Enum.IsDefined(typeof(LogicalOperators), logicalOperator))
            {
                allowedValues.LogicalOperator = (LogicalOperators)Enum.Parse(typeof(LogicalOperators), logicalOperator);
            }

            return allowedValues;
        }

        /// <summary>
        ///     Reads the column instance.
        /// </summary>
        /// <param name="columnInstanceEl">
        ///     The column instance element.
        /// </param>
        /// <param name="tableInstanceColumnIdDict">
        ///     The table instance column id dictionary.
        /// </param>
        /// <param name="columnInstanceSortIndexDict">
        ///     The column instance sort index dictionary.
        /// </param>
        /// <returns>
        ///     The <see cref="IColumnInstance" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid Query Metadata XML: ColumnInstance id not found
        /// </exception>
        protected virtual IColumnInstance ReadColumnInstance(JToken columnInstanceEl, Dictionary<string, ITableInstance> tableInstanceColumnIdDict, Dictionary<string, int> columnInstanceSortIndexDict)
        {
            // get table instance object given id
            var id = columnInstanceEl.Value<string>("ID");
            if (!tableInstanceColumnIdDict.ContainsKey(id))
            {
                throw new Exception("Invalid Query Metadata XML: ColumnInstance id '" + id + "' not found");
            }

            int index = columnInstanceSortIndexDict[id];
            ITableInstance tableInstance = tableInstanceColumnIdDict[id];
            var columnInstance = tableInstance.AddColumnInstance();
            columnInstance.ID = id;
            columnInstance.Index = index;
            columnInstance.Type = columnInstanceEl.Value<string>("Type");
            columnInstance.Name = columnInstanceEl.Value<string>("Name");
            if (!string.IsNullOrEmpty(columnInstanceEl.Value<string>("IsPrimaryKey")))
            {
                columnInstance.IsPrimaryKey = Convert.ToBoolean(columnInstanceEl.Value<string>("IsPrimaryKey"));
            }

            if (!string.IsNullOrEmpty(columnInstanceEl.Value<string>("IsReturned")))
            {
                columnInstance.IsReturned = Convert.ToBoolean(columnInstanceEl.Value<string>("IsReturned"));
            }

            if (!string.IsNullOrEmpty(columnInstanceEl.Value<string>("IsFixedValue")))
            {
                columnInstance.IsFixedValue = Convert.ToBoolean(columnInstanceEl.Value<string>("IsFixedValue"));
            }

            columnInstance.FixedValue = columnInstanceEl.Value<string>("FixedValue");
            return columnInstance;
        }

        /// <summary>
        ///     Reads the column mapping.
        /// </summary>
        /// <param name="relation">The relation.</param>
        /// <param name="columnInstanceIdDict">The column instance id dictionary.</param>
        /// <param name="columnMappingNodeEl">The column mapping node element.</param>
        /// <returns>
        ///     The <see cref="IColumnMapping" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefFrom not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefTo not found
        /// </exception>
        protected virtual IColumnMapping ReadColumnMapping(IRelation relation, Dictionary<string, IColumnInstance> columnInstanceIdDict, JToken columnMappingNodeEl)
        {
            var columnMapping = relation.AddColumnMapping();
            IColumnInstance columnInstanceFrom;
            if (!columnInstanceIdDict.TryGetValue(columnMappingNodeEl.Value<string>("ColumnInstanceRefFrom"), out columnInstanceFrom))
            {
                throw new Exception("Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefFrom not found ");
            }

            columnMapping.ColumnInstanceFrom = columnInstanceFrom;
            IColumnInstance columnInstanceTo;
            if (!columnInstanceIdDict.TryGetValue(columnMappingNodeEl.Value<string>("ColumnInstanceRefTo"), out columnInstanceTo))
            {
                throw new Exception("Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefTo not found ");
            }

            columnMapping.ColumnInstanceTo = columnInstanceTo;

            return columnMapping;
        }

        /// <summary>
        ///     Reads the column merging.
        /// </summary>
        /// <param name="columnsMergingNodeEl">The columns merging node element.</param>
        /// <param name="columnInstanceIdDict">The column instance id dictionary.</param>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <returns>
        ///     The <see cref="ColumnsMergingCollection" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        /// </exception>
        protected virtual ColumnsMergingCollection ReadColumnMerging(JToken columnsMergingNodeEl, Dictionary<string, IColumnInstance> columnInstanceIdDict, IQueryMetadata queryMetadata)
        {
            var generatedDimensionColumnInstanceRef = columnsMergingNodeEl.Value<string>("GeneratedDimensionColumnInstanceRef");
            var generatedMeasureColumnInstanceRef = columnsMergingNodeEl.Value<string>("GeneratedMeasureColumnInstanceRef");
            if (!string.IsNullOrEmpty(generatedDimensionColumnInstanceRef) && !string.IsNullOrEmpty(generatedMeasureColumnInstanceRef))
            {
                IColumnInstance generatedDimensionColumnInstance;
                if (!columnInstanceIdDict.TryGetValue(generatedDimensionColumnInstanceRef, out generatedDimensionColumnInstance))
                {
                    throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found ");
                }

                queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance = generatedDimensionColumnInstance;

                IColumnInstance generatedMeasureColumnInstance;
                if (!columnInstanceIdDict.TryGetValue(generatedMeasureColumnInstanceRef, out generatedMeasureColumnInstance))
                {
                    throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found ");
                }

                queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance = generatedMeasureColumnInstance;

                foreach (var columnMergingNode in columnsMergingNodeEl.Children())
                {

                    IColumnInstance columnInstance;
                    if (!columnInstanceIdDict.TryGetValue(columnMergingNode.Value<string>("ID"), out columnInstance))
                    {
                        throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found ");
                    }

                    queryMetadata.ColumnsMerging.Add(columnInstance);
                }
            }

            return queryMetadata.ColumnsMerging;
        }

        /// <summary>
        ///     Reads the column table reference.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="columnInstanceIdDict">
        ///     The column instance id dictionary.
        /// </param>
        /// <param name="columnTableReferenceNodeEl">
        ///     The column table reference node element.
        /// </param>
        /// <param name="tableInstanceIdDict">
        ///     The table instance id dictionary.
        /// </param>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with TableInstanceRef not found
        /// </exception>
        protected virtual void ReadColumnTableReference(IQueryMetadata queryMetadata, Dictionary<string, IColumnInstance> columnInstanceIdDict, JToken columnTableReferenceNodeEl, Dictionary<string, ITableInstance> tableInstanceIdDict)
        {
            var columnTableReference = queryMetadata.AddColumnTableReference();
            IColumnInstance columnInstance;
            if (!columnInstanceIdDict.TryGetValue(columnTableReferenceNodeEl.Value<string>("ColumnInstanceRef"), out columnInstance))
            {
                throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found ");
            }

            columnTableReference.ColumnInstance = columnInstance;
            ITableInstance tableInstance;
            if (!tableInstanceIdDict.TryGetValue(columnTableReferenceNodeEl.Value<string>("TableInstanceRef"), out tableInstance))
            {
                throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with TableInstanceRef not found ");
            }

            columnTableReference.TableInstance = tableInstance;
        }

        /// <summary>
        ///     Reads the cross join.
        /// </summary>
        /// <param name="tableInstanceIdDict">
        ///     The table instance id dictionary.
        /// </param>
        /// <param name="crossJoinEl">
        ///     The cross join element.
        /// </param>
        protected virtual void ReadCrossJoin(Dictionary<string, ITableInstance> tableInstanceIdDict, JToken crossJoinEl)
        {
            var tinFrom = tableInstanceIdDict[crossJoinEl.Value<string>("TableInstanceRefFrom")];
            var tinTo = tableInstanceIdDict[crossJoinEl.Value<string>("TableInstanceRefTo")];

            tinFrom.AddCrossJoinRelation(tinTo);
        }

        /// <summary>
        ///     Reads the table instance.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="tableInstanceEl">
        ///     The table instance element.
        /// </param>
        /// <returns>
        ///     The <see cref="ITableInstance" />
        /// </returns>
        protected virtual ITableInstance ReadTableInstance(IQueryMetadata queryMetadata, JToken tableInstanceEl)
        {
            var tableInstance = queryMetadata.AddTableInstance();
            tableInstance.Name = tableInstanceEl.SelectToken("Name").Value<string>();
            tableInstance.ID = tableInstanceEl.SelectToken("ID").Value<string>();
            if (!string.IsNullOrEmpty(tableInstanceEl.SelectToken("XCoordinate").Value<string>()))
            {
                tableInstance.XCoordinate = Convert.ToInt32(tableInstanceEl.SelectToken("XCoordinate").Value<string>());
            }

            if (!string.IsNullOrEmpty(tableInstanceEl.SelectToken("YCoordinate").Value<string>()))
            {
                tableInstance.YCoordinate = Convert.ToInt32(tableInstanceEl.SelectToken("YCoordinate").Value<string>());
            }

            return tableInstance;
        }

        /// <summary>
        ///     Reads the union.
        /// </summary>
        /// <param name="union">
        ///     The union.
        /// </param>
        /// <param name="tableInstanceIdDict">
        ///     The table instance id dictionary.
        /// </param>
        protected virtual void ReadUnion(JToken union, Dictionary<string, ITableInstance> tableInstanceIdDict)
        {

            var tinFrom = tableInstanceIdDict[union.Value<string>("TableInstanceRefFrom")];
            var tinTo = tableInstanceIdDict[union.Value<string>("TableInstanceRefTo")];

            tinFrom.AddUnionRelation(tinTo);
        }

        /// <summary>
        ///     Reads the value.
        /// </summary>
        /// <param name="valueEl">
        ///     The value element.
        /// </param>
        /// <param name="allowedValues">
        ///     The allowed values.
        /// </param>
        protected virtual void ReadValue(JToken valueEl, IAllowedValues allowedValues)
        {
            // get the operator if it is set or default to "="
            var op = valueEl.Value<string>("operator");
            if (string.IsNullOrEmpty(op) || op.Trim().Length == 0)
            {
                // TODO check if the operator is valid
                op = Operators.Get(Operators.OperatorIndex.Equals);
            }

            allowedValues.Values.Add(new Value(valueEl.Value<string>("text"), op));
        }

        /// <summary>
        ///     Writes the allowed values.
        /// </summary>
        /// <param name="allowedValuesNode">
        ///     The allowed values node.
        /// </param>
        /// <param name="allowedValues">
        ///     The allowed values.
        /// </param>
        protected virtual void WriteAllowedValues(JObject allowedValuesNode, IAllowedValues allowedValues)
        {
            allowedValuesNode["ColumnInstanceRef"] = allowedValues.ColumnInstance.ID;
            allowedValuesNode["logicalOperator"] = allowedValues.LogicalOperator.ToString();
        }

        /// <summary>
        ///     Writes the column instance.
        /// </summary>
        /// <param name="columnInstance">
        ///     The column instance.
        /// </param>
        protected virtual JObject CreateColumnInstance(IColumnInstance columnInstance)
        {
            var jObject = new JObject
            {
                { "ID", columnInstance.ID },
                { "Type", columnInstance.Type },
                { "Name", columnInstance.Name },
                { "IsPrimaryKey", columnInstance.IsPrimaryKey.ToString() },
                { "IsReturned", columnInstance.IsReturned.ToString() },
                { "IsFixedValue", columnInstance.IsFixedValue.ToString() },
            };
            if (!string.IsNullOrEmpty(columnInstance.FixedValue))
            {
                jObject["FixedValue"] = columnInstance.FixedValue;
            }
            return jObject;
        }

        /// <summary>
        ///     Writes the column instance ref.
        /// </summary>
        /// <param name="columnInstance">
        ///     The column instance.
        /// </param>
        /// <param name="columnInstanceNode">
        ///     The column instance node.
        /// </param>
        protected virtual JObject WriteColumnInstanceRef(IColumnInstance columnInstance)
        {
            return new JObject()
            {
                ["ID"] = columnInstance.ID
            };
        }

        /// <summary>
        ///     Writes the column instance ref.
        /// </summary>
        /// <param name="columnInstance">
        ///     The column instance.
        /// </param>
        /// <param name="i">
        ///     The i.
        /// </param>
        protected virtual JObject WriteColumnInstanceRef(IColumnInstance columnInstance, int i)
        {
            return new JObject()
            {
                {"ID", columnInstance.ID},
                {"Index", i.ToString(CultureInfo.InvariantCulture) }
            };
           
        }

        /// <summary>
        ///     Writes the column mapping.
        /// </summary>
        /// <param name="columnMapping">
        ///     The column mapping.
        /// </param>
        protected virtual JObject GetColumnMapping(IColumnMapping columnMapping)
        {
            return new JObject()
            {
                ["ColumnInstanceRefFrom"] = columnMapping.ColumnInstanceFrom.ID,
                ["ColumnInstanceRefTo"] = columnMapping.ColumnInstanceTo.ID
            };
        }

        /// <summary>
        ///     Writes the columns merging.
        /// </summary>
        /// <param name="columnMergingJson"></param>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        protected virtual void WriteColumnsMerging(JObject columnMergingJson, IQueryMetadata queryMetadata)
        {
            columnMergingJson["GeneratedMeasureColumnInstanceRef"] = queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance.ID;
            columnMergingJson["GeneratedDimensionColumnInstanceRef"] = queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance.ID;
        }

        /// <summary>
        ///     Writes the column table references.
        /// </summary>
        /// <param name="columnTableReference">
        ///     The column table reference.
        /// </param>
        protected virtual JObject WriteColumnTableReferences(IColumnTableReference columnTableReference)
        {
            return new JObject()
            {
                ["ColumnInstanceRef"] = columnTableReference.ColumnInstance.ID,
                ["TableInstanceRef"] = columnTableReference.TableInstance.ID
            };
        }

        /// <summary>
        ///     Writes the cross join.
        /// </summary>
        /// <param name="crossJoinJson"></param>
        /// <param name="relation">
        ///     The relation.
        /// </param>
        protected virtual void WriteCrossJoin(JObject crossJoinJson, ICrossJoinRelation relation)
        {
            crossJoinJson["TableInstanceRefFrom"] = relation.TableInstanceFrom.ID;
            crossJoinJson["TableInstanceRefTo"] = relation.TableInstanceTo.ID;
        }

        /// <summary>
        ///     Writes the relation.
        /// </summary>
        /// <param name="relationNode">
        ///     The relation node.
        /// </param>
        /// <param name="relation">
        ///     The relation.
        /// </param>
        protected virtual void WriteRelation(JObject relationNode, IRelation relation)
        {
            relationNode["TableInstanceRefFrom"] = relation.TableInstanceFrom.ID;
            relationNode["TableInstanceRefTo"] = relation.TableInstanceTo.ID;
        }

        /// <summary>
        ///     Writes the table attribute values.
        /// </summary>
        /// <param name="allowedValuesJson">
        ///     The allowed values node.
        /// </param>
        /// <param name="allowedValues">
        ///     The allowed values.
        /// </param>
        protected virtual void WriteTableAttributeValues(JObject allowedValuesJson, IAllowedValues allowedValues)
        {
            this.WriteAllowedValues(allowedValuesJson, allowedValues);
        }

        /// <summary>
        ///     Writes the table instance.
        /// </summary>
        /// <param name="tableInstanceNode">
        ///     The table instance node.
        /// </param>
        /// <param name="tableInstance">
        ///     The table instance.
        /// </param>
        protected virtual void WriteTableInstance(JObject tableInstanceNode, ITableInstance tableInstance)
        {
            tableInstanceNode.Add("ID", tableInstance.ID);
            tableInstanceNode.Add("Name", tableInstance.Name);
            tableInstanceNode.Add("XCoordinate", tableInstance.XCoordinate.ToString(CultureInfo.InvariantCulture));
            tableInstanceNode.Add("YCoordinate", tableInstance.YCoordinate.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        ///     Writes the union.
        /// </summary>
        /// <param name="unionJson">
        ///     The union relation node.
        /// </param>
        /// <param name="relation">
        ///     The relation.
        /// </param>
        protected virtual void WriteUnion(JObject unionJson, IUnionRelation relation)
        {
            unionJson["TableInstanceRefFrom"] = relation.TableInstanceFrom.ID;
            unionJson["TableInstanceRefTo"] = relation.TableInstanceTo.ID;
        }

        /// <summary>
        ///     Writes the value.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        protected virtual JObject WriteValue(Value value)
        {
            return new JObject()
            {
                ["operator"] = value.Operator,
                ["text"] = value.Val
            };
        }

        /// <summary>
        /// Gets the table constraints.
        /// </summary>
        /// <param name="tableConstraintsNodes">The table constraints nodes.</param>
        /// <param name="tableInstanceIdDict">The table instance identifier dictionary.</param>
        /// <param name="columnInstanceIdDict">The column instance identifier dictionary.</param>
        /// <param name="queryMetadata">The query metadata.</param>
        private void GetTableConstraints(JToken tableConstraintsNodes, Dictionary<string, ITableInstance> tableInstanceIdDict, Dictionary<string, IColumnInstance> columnInstanceIdDict, IQueryMetadata queryMetadata)
        {

            foreach (var tableConstraint in tableConstraintsNodes.GetChildren("TableConstraint"))
            {

                var tin = tableInstanceIdDict[tableConstraint.Value<string>("TableInstanceRef")];

                // ConstraintsCollection tableConstraints = new ConstraintsCollection(tin);
                foreach (var allowedValuesNode in tableConstraint.GetChildren("AllowedValues"))
                {
                    // TODO check Node is an Element type
                    if (allowedValuesNode != null)
                    {
                        var allowedValues = this.ReadAllowedValues(tin, columnInstanceIdDict, allowedValuesNode);

                        foreach (var valueNode in allowedValuesNode.GetChildren("Value"))
                        {
                            if (valueNode != null)
                            {
                                this.ReadValue(valueNode, allowedValues);
                            }
                        }
                    }
                }

                queryMetadata.TableConstraints.Add(tin.Constraints);
            }
        }

        /// <summary>
        /// Gets the where details when count is one.
        /// </summary>
        /// <param name="whereDetailsNodes">The where details nodes.</param>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <param name="columnInstanceIdDict">The column instance identifier dictionary.</param>
        private void GetWhereDetailsWhenCountIsOne(JToken whereDetailsNodes, IQueryMetadata queryMetadata, Dictionary<string, IColumnInstance> columnInstanceIdDict)
        {

            foreach (var allowedValuesNode in whereDetailsNodes.SelectToken("AllowedValues").Children())
            {
                var allowedValues = this.ReadAllowedValues(queryMetadata, columnInstanceIdDict, allowedValuesNode);
                foreach (var valueNode in allowedValuesNode.GetChildren("Value"))
                {
                    if (valueNode != null)
                    {
                        this.ReadValue(valueNode, allowedValues);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the single table instance node.
        /// </summary>
        /// <param name="tableInstances">The table instances nodes.</param>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <param name="tableInstanceIdDict">The table instance identifier dictionary.</param>
        /// <param name="tableInstanceColumnIdDict">The table instance column identifier dictionary.</param>
        /// <param name="columnInstanceSortIndexDict">The column instance sort index dictionary.</param>
        private void HandleSingleTableInstanceNode(JToken tableInstances, IQueryMetadata queryMetadata, Dictionary<string, ITableInstance> tableInstanceIdDict, Dictionary<string, ITableInstance> tableInstanceColumnIdDict, Dictionary<string, int> columnInstanceSortIndexDict)
        {
            // throw new Exception("Invalid QueryMetadata XML: number TableInstances != 1");
            foreach (var tableInstnanceNode in tableInstances.GetChildren("TableInstance"))
            {
                var tableInstance = this.ReadTableInstance(queryMetadata, tableInstnanceNode);

                tableInstanceIdDict.Add(tableInstance.ID, tableInstance);

                var i = 0;

                // get TableInstance's ColumnInstanceRefs
                foreach (var columnInstancRefNode in tableInstnanceNode.GetChildren("ColumnInstanceRefs.ColumnInstanceRef"))
                {
                    // TODO check Node is an Element type
                    var id = columnInstancRefNode.SelectToken("ID").Value<string>();
                    tableInstanceColumnIdDict.Add(id, tableInstance);
                    var index = i;
                    if (!string.IsNullOrEmpty(columnInstancRefNode.SelectToken("Index").Value<string>()))
                    {
                        index = int.Parse(columnInstancRefNode.SelectToken("Index").Value<string>());
                    }

                    columnInstanceSortIndexDict.Add(id, index);
                    i++;
                }
            }
        }

        /// <summary>
        /// Handles the table constraints.
        /// </summary>
        /// <param name="constraints">The constraints.</param>
        private JObject GetTableConstraints(ConstraintsCollection constraints)
        {
            var tableConstraintJson = new JObject
            {
                ["TableInstanceRef"] = constraints.TableInstance.ID
            };

            var allowedValuesArray = new JArray();
            foreach (var allowedValues in constraints)
            {
                var allowedValuesJson = new JObject();
               
                this.WriteTableAttributeValues(allowedValuesJson, allowedValues);
                var valuesArray = new JArray();
                foreach (var value in allowedValues.Values)
                {
                    valuesArray.Add(this.WriteValue(value));
                }

                allowedValuesJson["Value"] = valuesArray;
                allowedValuesArray.Add(allowedValuesJson);
            }

            tableConstraintJson["AllowedValues"] = allowedValuesArray;
            return tableConstraintJson;
        }

        /// <summary>
        /// Writes the relations.
        /// </summary>
        /// <param name="relation">The relation.</param>
        private JObject WriteRelations(IRelation relation)
        {
            var relationJson = new JObject();

            this.WriteRelation(relationJson, relation);
            var columnMappingArray = new JArray();
            foreach (var columnMapping in relation.ColumnMappings)
            {
                columnMappingArray.Add(this.GetColumnMapping(columnMapping));
            }
            relationJson["ColumnMapping"] = columnMappingArray;
            return relationJson;
        }

        /// <summary>
        /// Writes the table instances.
        /// </summary>
        /// <param name="tableInstance">The table instance.</param>
        private JObject WriteTableInstances(ITableInstance tableInstance)
        {
            var tableInstanceJson = new JObject();
            this.WriteTableInstance(tableInstanceJson, tableInstance);

            var i = 0;
            var columnInstanceRefArray = new JArray();
            foreach (var columnInstance in tableInstance.ColumnInstances)
            {
                columnInstanceRefArray.Add(this.WriteColumnInstanceRef(columnInstance, i));
                i++;
            }
            tableInstanceJson["ColumnInstanceRefs"] = new JObject
            {
                ["ColumnInstanceRef"] = columnInstanceRefArray
            };

            return tableInstanceJson;
        }
    }

    public static class XmlExtensions
    {
        public static string GetValueFromNode(this XmlNode xmlnode, string elementName)
        {
            var node = xmlnode.SelectSingleNode(elementName);
            if (node == null)
            {
                return string.Empty;
            }
            return node.FirstChild.Value;
        }
    }
}
