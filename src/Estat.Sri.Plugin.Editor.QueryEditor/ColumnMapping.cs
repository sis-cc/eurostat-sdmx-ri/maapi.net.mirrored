﻿// -----------------------------------------------------------------------
// <copyright file="ColumnMapping.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------


namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System;

    /// <summary>
    ///     The column mapping.
    /// </summary>
    public class ColumnMapping : IColumnMapping
    {
        /// <summary>
        ///     The _ query metadata.
        /// </summary>
        private readonly IQueryMetadata _queryMetadata;

        /// <summary>
        ///     The _ relation.
        /// </summary>
        private readonly IRelation _relation;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ColumnMapping" /> class.
        /// </summary>
        /// <param name="relation">The relation.</param>
        /// <exception cref="System.Exception">Relation does not belong to a QueryMetadata</exception>
        public ColumnMapping(IRelation relation)
        {
            this._relation = relation;
            this._relation.ColumnMappings.DoAdd(this);
            this._queryMetadata = relation.QueryMetadata;
            if (relation.QueryMetadata == null)
            {
                throw new Exception("Relation does not belong to a QueryMetadata");
            }
        }

        /// <summary>
        ///     Gets or sets the column instance from.
        /// </summary>
        public IColumnInstance ColumnInstanceFrom { get; set; }

        /// <summary>
        ///     Gets or sets the column instance to.
        /// </summary>
        public IColumnInstance ColumnInstanceTo { get; set; }

        /// <summary>
        ///     Gets the query metadata.
        /// </summary>
        public IQueryMetadata QueryMetadata
        {
            get
            {
                return this._queryMetadata;
            }
        }

        /// <summary>
        ///     Gets the relation.
        /// </summary>
        public IRelation Relation
        {
            get
            {
                return this._relation;
            }
        }
    }
}
