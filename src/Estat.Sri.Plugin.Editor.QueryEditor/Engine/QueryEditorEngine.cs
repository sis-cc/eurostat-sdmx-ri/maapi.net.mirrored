// -----------------------------------------------------------------------
// <copyright file="QueryEditorEngine.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Estat.Sri.Mapping.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Builder;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Mapping.Api.Constant;
using Newtonsoft.Json;

namespace Estat.Sri.Plugin.Editor.QueryEditor.Engine
{
    public class QueryEditorEngine : IDataSetEditorEngine
    {
        private readonly IBuilder<Database, DdbConnectionEntity> _databaseBuilder;
        private readonly IEntityRetrieverManager _entityRetreiverManager;

        public QueryEditorEngine(IBuilder<Database, DdbConnectionEntity> databaseBuilder, IEntityRetrieverManager entityRetrieverManager)
        {
            _databaseBuilder = databaseBuilder;
            _entityRetreiverManager = entityRetrieverManager;
        }


        public string GenerateSqlQuery(DatasetEntity datasetEntity)
        {
            QueryMetadataReaderWriterBase queryReaderWriter = new QueryMetadataReaderWriterBase();

            QueryGenerator queryGeneratorBase = new QueryGenerator();

            var entityRetreiveEngine = _entityRetreiverManager.GetRetrieverEngine<DdbConnectionEntity>(datasetEntity.StoreId);

            var ddbConnectionEntity =
               entityRetreiveEngine.GetEntities(
                   new EntityQuery { EntityId = new Criteria<string>(OperatorType.Exact, datasetEntity.ParentId) },
                   Detail.Full).FirstOrDefault();

            var database = _databaseBuilder.Build(ddbConnectionEntity);

            using (var connection = database.CreateConnection())
            {
                return queryGeneratorBase.GenerateQuery(queryReaderWriter.Read(datasetEntity.JSONQuery), connection);
            }
        }

        /// <inheritdoc />
        public object DeserializeExtraData(string extraData)
        {
            if (string.IsNullOrWhiteSpace(extraData))
            {
                return null;
            }

            char firstChar = GetFirstNoWhitespaceCharacter(extraData);
            // JSON
            if (firstChar == '{')
            {
                return JsonConvert.DeserializeObject(extraData);
            }

            // XML 
            QueryMetadataReaderWriterBase queryReaderWriter = new QueryMetadataReaderWriterBase();
            IQueryMetadata queryMetadata = queryReaderWriter.Read(extraData);
            string generatedJson = queryReaderWriter.Write(queryMetadata);
            return JsonConvert.DeserializeObject(generatedJson);
        }

        /// <inheritdoc/>
        public string Migrate(DatasetEntity datasetEntity)
        {
            var extraData = datasetEntity?.JSONQuery;
            if (string.IsNullOrWhiteSpace(extraData))
            {
                return extraData;
            }

            char firstChar = GetFirstNoWhitespaceCharacter(extraData);
            // JSON
            if (firstChar == '{')
            {
                return extraData;
            }

            // XML 
            QueryMetadataReaderWriterBase queryReaderWriter = new QueryMetadataReaderWriterBase();
            IQueryMetadata queryMetadata = queryReaderWriter.Read(extraData);
            return queryReaderWriter.Write(queryMetadata);
        }
        /// <inheritdoc />
        public IList<string> GetSupportedDdbPlugins()
        {
            return Properties.Settings.Default.SupportedProviders.Cast<string>().ToList();
        }

        private char GetFirstNoWhitespaceCharacter(string extraData)
        {
            return extraData.First(x => !char.IsWhiteSpace(x));
        }
    }
}
