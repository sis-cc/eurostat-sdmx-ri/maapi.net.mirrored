﻿// -----------------------------------------------------------------------
// <copyright file="IQueryMetadata.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System.Collections.Generic;

    /// <summary>
    ///     The QueryMetadata interface.
    /// </summary>
    public interface IQueryMetadata
    {
        /// <summary>
        ///     Gets the column instances.
        /// </summary>
        ColumnInstancesCollection ColumnInstances { get; }

        /// <summary>
        ///     Gets the columns merging.
        /// </summary>
        ColumnsMergingCollection ColumnsMerging { get; }

        /// <summary>
        ///     Gets the column table references.
        /// </summary>
        ColumnTableReferencesCollection ColumnTableReferences { get; }

        /// <summary>
        ///     Gets the constraints.
        /// </summary>
        ConstraintsCollection Constraints { get; }

        /// <summary>
        ///     Gets the cross-join relations.
        /// </summary>
        CrossJoinRelationsCollection CrossJoinRelations { get; }

        /// <summary>
        ///     Gets the relations.
        /// </summary>
        RelationsCollection Relations { get; }

        /// <summary>
        ///     Gets the table constraints.
        /// </summary>
        ICollection<ConstraintsCollection> TableConstraints { get; }

        /// <summary>
        ///     Gets the table instances.
        /// </summary>
        TableInstancesCollection TableInstances { get; }

        /// <summary>
        ///     Gets the union relations.
        /// </summary>
        UnionRelationsCollection UnionRelations { get; }

        /// <summary>
        ///     Adds the column table reference.
        /// </summary>
        /// <returns>
        ///     The <see cref="ColumnTableReference" />
        /// </returns>
        IColumnTableReference AddColumnTableReference();

        /// <summary>
        ///     Adds the constraints.
        /// </summary>
        /// <returns>
        ///     The <see cref="AllowedValues" />
        /// </returns>
        IAllowedValues AddConstraints();

        /// <summary>
        ///     Adds the relation.
        /// </summary>
        /// <returns>
        ///     The <see cref="Relation" />
        /// </returns>
        IRelation AddRelation();

        /// <summary>
        ///     Adds the table instance.
        /// </summary>
        /// <returns>
        ///     The <see cref="TableInstance" />
        /// </returns>
        ITableInstance AddTableInstance();

        /// <summary>
        ///     The clone.
        /// </summary>
        /// <returns>
        ///     The <see cref="Estat.Ma.Model.QueryEditor.QueryMetadata" />.
        /// </returns>
        IQueryMetadata Clone();

        /// <summary>
        ///     The has unconnected tables.
        /// </summary>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        bool HasUnconnectedTables();

        /// <summary>
        ///     The remove merged column.
        /// </summary>
        /// <param name="col">
        ///     The col.
        /// </param>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        bool RemoveMergedColumn(ColumnInstancesCollection col);
    }
}
