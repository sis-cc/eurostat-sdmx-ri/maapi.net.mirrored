// -----------------------------------------------------------------------
// <copyright file="ColumnTableReferencesCollection.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System.ComponentModel;

    /// <summary>
    ///     The column table references collection.
    /// </summary>
    public class ColumnTableReferencesCollection : BindingList<IColumnTableReference>
    {
        /// <summary>
        ///     The _ query metadata.
        /// </summary>
        private readonly IQueryMetadata _queryMetadata;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ColumnTableReferencesCollection" /> class.
        /// </summary>
        /// <param name="querymetadata">
        ///     The query metadata.
        /// </param>
        public ColumnTableReferencesCollection(IQueryMetadata querymetadata)
        {
            this._queryMetadata = querymetadata;
        }

        /// <summary>
        ///     Gets the query metadata.
        /// </summary>
        public IQueryMetadata QueryMetadata
        {
            get
            {
                return this._queryMetadata;
            }
        }

        /// <summary>
        ///     The contains column instance.
        /// </summary>
        /// <param name="ci">
        ///     The ci.
        /// </param>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        public bool ContainsColumnInstance(IColumnInstance ci)
        {
            foreach (var rel in this)
            {
                if (rel.ColumnInstance == ci)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     The contains table instance.
        /// </summary>
        /// <param name="ti">
        ///     The ti.
        /// </param>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        public bool ContainsTableInstance(ITableInstance ti)
        {
            foreach (var rel in this)
            {
                if (rel.TableInstance == ti)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     The do add.
        /// </summary>
        /// <param name="c">
        ///     The c.
        /// </param>
        public void DoAdd(IColumnTableReference c)
        {
            this.Add(c);
        }
    }
}