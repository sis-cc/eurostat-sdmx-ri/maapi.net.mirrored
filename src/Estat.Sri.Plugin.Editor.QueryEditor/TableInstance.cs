﻿// -----------------------------------------------------------------------
// <copyright file="TableInstance.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System.Collections.Generic;

    /// <summary>
    ///     The table instance.
    /// </summary>
    public class TableInstance : ITableInstance
    {
        /// <summary>
        ///     The _ column instances.
        /// </summary>
        private readonly ColumnInstancesCollection _columnInstances;

        /// <summary>
        ///     The _ constraints.
        /// </summary>
        private readonly ConstraintsCollection _constraints;

        /// <summary>
        ///     The _ query metadata.
        /// </summary>
        private readonly IQueryMetadata _queryMetadata;

        /// <summary>
        ///     The _ x coordinate.
        /// </summary>
        private int _xcoordinate;

        /// <summary>
        ///     The _ y coordinate.
        /// </summary>
        private int _ycoordinate;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TableInstance" /> class.
        /// </summary>
        /// <param name="querymetadata">
        ///     The <see cref="QueryMetadata" /> that this instance will belong to.
        /// </param>
        public TableInstance(IQueryMetadata querymetadata)
        {
            this._queryMetadata = querymetadata;
            this._columnInstances = new ColumnInstancesCollection(this);
            this._queryMetadata.TableInstances.DoAdd(this);
            this._constraints = new ConstraintsCollection(this);
        }

        /// <summary>
        ///     Gets the column instances.
        /// </summary>
        public ColumnInstancesCollection ColumnInstances
        {
            get
            {
                return this._columnInstances;
            }
        }

        /// <summary>
        ///     Gets the constraints.
        /// </summary>
        public ConstraintsCollection Constraints
        {
            get
            {
                return this._constraints;
            }
        }

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Gets the query metadata.
        /// </summary>
        public IQueryMetadata QueryMetadata
        {
            get
            {
                return this._queryMetadata;
            }
        }

        /// <summary>
        ///     Gets or sets the x coordinate.
        /// </summary>
        public int XCoordinate
        {
            get
            {
                return this._xcoordinate;
            }

            set
            {
                this._xcoordinate = value;
            }
        }

        /// <summary>
        ///     Gets or sets the y coordinate.
        /// </summary>
        public int YCoordinate
        {
            get
            {
                return this._ycoordinate;
            }

            set
            {
                this._ycoordinate = value;
            }
        }

        /// <summary>
        ///     Add a new column instance.
        /// </summary>
        /// <returns>
        ///     The <see cref="ColumnInstance" />
        /// </returns>
        public IColumnInstance AddColumnInstance()
        {
            return new ColumnInstance(this);
        }

        /// <summary>
        ///     Add a new column instance.
        /// </summary>
        /// <param name="mergedColumnType">
        ///     Type of the merged column.
        /// </param>
        /// <returns>
        ///     The <see cref="ColumnInstance" />
        /// </returns>
        public IColumnInstance AddColumnInstance(MergedColumnType mergedColumnType)
        {
            return new ColumnInstance(this, mergedColumnType);
        }

        /// <summary>
        ///     Add a new constraint.
        /// </summary>
        /// <returns>
        ///     The <see cref="AllowedValues" />
        /// </returns>
        public IAllowedValues AddConstraint()
        {
            var allowedValues = new AllowedValues(this);
            this.Constraints.Add(allowedValues);
            return allowedValues;
        }

        /// <summary>
        ///     Add a new cross join relation.
        /// </summary>
        /// <param name="destinationTableInstance">
        ///     The destination table instance.
        /// </param>
        /// <returns>
        ///     The <see cref="CrossJoinRelation" />
        /// </returns>
        public ICrossJoinRelation AddCrossJoinRelation(ITableInstance destinationTableInstance)
        {
            return new CrossJoinRelation(this, destinationTableInstance);
        }

        /// <summary>
        ///     Add a new fixed column instance.
        /// </summary>
        /// <param name="name">
        ///     The name.
        /// </param>
        /// <param name="fixedValue">
        ///     The fixed value.
        /// </param>
        /// <returns>
        ///     The <see cref="ColumnInstance" />
        /// </returns>
        public IColumnInstance AddFixedColumnInstance(string name, string fixedValue)
        {
            var col = this.AddColumnInstance();
            col.IsFixedValue = true;
            col.FixedValue = fixedValue;
            col.Type = "fixed";
            col.Name = name;
            return col;
        }

        /// <summary>
        ///     Add a new union relation.
        /// </summary>
        /// <param name="destinationTableInstance">
        ///     The destination table instance.
        /// </param>
        /// <returns>
        ///     The <see cref="UnionRelation" />
        /// </returns>
        public IUnionRelation AddUnionRelation(ITableInstance destinationTableInstance)
        {
            return new UnionRelation(this, destinationTableInstance);
        }

        /// <summary>
        ///     Returns the next table in the Union Relation that the table participates in (if it participates in a union
        ///     relation)
        /// </summary>
        /// <returns>
        ///     The <see cref="Estat.Ma.Model.QueryEditor.ITableInstance" />.
        /// </returns>
        public ITableInstance GetNextUnionTable()
        {
            foreach (var ur in this._queryMetadata.UnionRelations)
            {
                if (ur.TableInstanceFrom.ID == this.ID)
                {
                    return ur.TableInstanceTo;
                }
            }

            return null;
        }

        /// <summary>
        ///     Returns true if the table participates in an Inner Join  Relation
        /// </summary>
        /// <returns>
        ///     true if the table participates in a Inner-Join Relation; otherwise false
        /// </returns>
        public bool HasColumnTableRelation()
        {
            foreach (var ctr in this._queryMetadata.ColumnTableReferences)
            {
                if (ctr.TableInstance.ID == this.ID || ctr.ColumnInstance.TableInstance.ID == this.ID)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     Returns true if the table participates in a Cross-Join Relation
        /// </summary>
        /// <returns>
        ///     true if the table participates in a Cross-Join Relation; otherwise false
        /// </returns>
        public bool HasCrossJoinRelation()
        {
            foreach (var ur in this._queryMetadata.CrossJoinRelations)
            {
                if (ur.TableInstanceTo.ID == this.ID || ur.TableInstanceFrom.ID == this.ID)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     Returns true if the table participates in an Inner Join  Relation
        /// </summary>
        /// <returns>
        ///     true if the table participates in a Inner-Join Relation; otherwise false
        /// </returns>
        public bool HasInnerJoinRelation()
        {
            foreach (var relation in this._queryMetadata.Relations)
            {
                if (relation.TableInstanceTo.ID == this.ID || relation.TableInstanceFrom.ID == this.ID)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     Returns true only if the table participates in a Union Relation and NO OTHER Relations (i.e. ColumnTableReference,
        ///     Join Relation)
        ///     AND is the FIRST table in this Union Relation that it participates in
        /// </summary>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        public bool HasOnlyUnionRelationFrom()
        {
            if (this.HasInnerJoinRelation() || this.HasColumnTableRelation() || this.HasCrossJoinRelation())
            {
                return false;
            }

            var retval = false;
            foreach (var ur in this._queryMetadata.UnionRelations)
            {
                if (ur.TableInstanceTo.ID == this.ID)
                {
                    return false;
                }

                if (ur.TableInstanceFrom.ID == this.ID)
                {
                    retval = true;
                }
            }

            return retval;
        }

        /// <summary>
        ///     Returns true only if the table participates in a Union Relation and NO OTHER Relations (i.e. ColumnTableReference,
        ///     Join Relation)
        ///     AND is the LAST table in this Union Relation that it participates in
        /// </summary>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        public bool HasOnlyUnionRelationTo()
        {
            if (this.HasInnerJoinRelation() || this.HasColumnTableRelation() || this.HasCrossJoinRelation())
            {
                return false;
            }

            var retval = false;
            foreach (var ur in this._queryMetadata.UnionRelations)
            {
                if (ur.TableInstanceFrom.ID == this.ID)
                {
                    return false;
                }

                if (ur.TableInstanceTo.ID == this.ID)
                {
                    retval = true;
                }
            }

            return retval;
        }

        /// <summary>
        ///     The has relation of any type.
        /// </summary>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        public bool HasRelationOfAnyType()
        {
            return this.HasUnionRelation() || this.HasCrossJoinRelation() || this.HasInnerJoinRelation() || this.HasColumnTableRelation();
        }

        /// <summary>
        ///     Returns true if the table participates in a Union Relation
        /// </summary>
        /// <returns>
        ///     true if the table participates in a Union; otherwise false
        /// </returns>
        public bool HasUnionRelation()
        {
            foreach (var ur in this._queryMetadata.UnionRelations)
            {
                if (ur.TableInstanceTo.ID == this.ID || ur.TableInstanceFrom.ID == this.ID)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     The remove all references.
        /// </summary>
        public void RemoveAllReferences()
        {
            var ctrToRemove = new List<IColumnTableReference>();
            var relToRemove = new List<IRelation>();
            var constraintsToRemove = new List<IAllowedValues>();
            var unionsToRemove = new List<IUnionRelation>();
            var crossJoinToRemove = new List<ICrossJoinRelation>();
            var tableConstraintsToRemove = new List<ConstraintsCollection>();

            foreach (var conCol in this._queryMetadata.TableConstraints)
            {
                if (conCol.TableInstance == this)
                {
                    tableConstraintsToRemove.Add(conCol);
                }
            }

            foreach (var union in this._queryMetadata.UnionRelations)
            {
                if (union.TableInstanceFrom == this || union.TableInstanceTo == this)
                {
                    unionsToRemove.Add(union);
                }
            }

            foreach (var relation in this._queryMetadata.CrossJoinRelations)
            {
                if (relation.TableInstanceFrom == this || relation.TableInstanceTo == this)
                {
                    crossJoinToRemove.Add(relation);
                }
            }

            foreach (var ctr in this._queryMetadata.ColumnTableReferences)
            {
                if (ctr.TableInstance == this)
                {
                    ctrToRemove.Add(ctr);
                }
            }

            foreach (var ci in this.ColumnInstances)
            {
                this.HandleColumnInstances(ci, ctrToRemove, relToRemove, constraintsToRemove);
            }

            foreach (var ctr in ctrToRemove)
            {
                this._queryMetadata.ColumnTableReferences.Remove(ctr);
            }

            foreach (var rel in relToRemove)
            {
                this._queryMetadata.Relations.Remove(rel);
            }

            foreach (var constr in constraintsToRemove)
            {
                this._queryMetadata.Constraints.Remove(constr);
            }

            foreach (var union in unionsToRemove)
            {
                this._queryMetadata.UnionRelations.Remove(union);
            }

            foreach (var relation in crossJoinToRemove)
            {
                this._queryMetadata.CrossJoinRelations.Remove(relation);
            }

            foreach (var conCol in tableConstraintsToRemove)
            {
                this._queryMetadata.TableConstraints.Remove(conCol);
            }

            this._queryMetadata.TableInstances.Remove(this);
        }

        /// <summary>
        ///     The selected columns match columns of.
        /// </summary>
        /// <param name="to">
        ///     The to.
        /// </param>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        public bool SelectedColumnsMatchColumnsOf(ITableInstance to)
        {
            var columnInstances1 = new List<IColumnInstance>();
            var columnInstances2 = new List<IColumnInstance>();
            foreach (var col in to.ColumnInstances)
            {
                if (col.IsReturned)
                {
                    columnInstances1.Add(col);
                }
            }

            foreach (var col in this.ColumnInstances)
            {
                if (col.IsReturned)
                {
                    columnInstances2.Add(col);
                }
            }

            if (columnInstances1.Count != columnInstances2.Count)
            {
                return false;
            }

            for (var i = 0; i < columnInstances1.Count; i++)
            {
                if (columnInstances1[i].Type != columnInstances2[i].Type && columnInstances1[i].Type != "fixed" && columnInstances2[i].Type != "fixed")
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Handles the column instances.
        /// </summary>
        /// <param name="ci">The ci.</param>
        /// <param name="ctrToRemove">The CTR to remove.</param>
        /// <param name="relToRemove">The relative to remove.</param>
        /// <param name="constraintsToRemove">The constraints to remove.</param>
        private void HandleColumnInstances(IColumnInstance ci, List<IColumnTableReference> ctrToRemove, List<IRelation> relToRemove, List<IAllowedValues> constraintsToRemove)
        {
            this._queryMetadata.ColumnInstances.Remove(ci);
            foreach (var ctr in this._queryMetadata.ColumnTableReferences)
            {
                if (ctr.ColumnInstance == ci)
                {
                    ctrToRemove.Add(ctr);
                }
            }

            foreach (var rel in this._queryMetadata.Relations)
            {
                foreach (var cm in rel.ColumnMappings)
                {
                    if (cm.ColumnInstanceFrom == ci || cm.ColumnInstanceTo == ci)
                    {
                        relToRemove.Add(rel);
                    }
                }
            }

            foreach (var constraint in this._queryMetadata.Constraints)
            {
                if (constraint.ColumnInstance == ci)
                {
                    constraintsToRemove.Add(constraint);
                }
            }

            var mustRemoveColumnsMerging = false;
            foreach (var cim in this._queryMetadata.ColumnsMerging)
            {
                if (cim == ci)
                {
                    mustRemoveColumnsMerging = true;
                    break;
                }
            }

            if (mustRemoveColumnsMerging)
            {
                this._queryMetadata.ColumnInstances.Remove(this._queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance);
                this._queryMetadata.ColumnInstances.Remove(this._queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance);
                this._queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance = null;
                this._queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance = null;
                this._queryMetadata.ColumnsMerging.Clear();
            }
        }

        // [Browsable(false)]

        // public bool IsUsedInMappingSet { get; set; }
    }
}
