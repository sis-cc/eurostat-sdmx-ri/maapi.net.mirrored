namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    public interface IFormatSpecificExtractor
    {
        IQueryMetadata Read(string value);
    }
}