// -----------------------------------------------------------------------
// <copyright file="QueryGeneratorBase.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Data.Odbc;

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Text;

    /// <summary>
    ///     The base class for Query Generator.
    /// </summary>
    /// <remarks>Not thread safe.</remarks>
    public abstract class QueryGeneratorBase
    {
        /// <summary>
        ///     The from.
        /// </summary>
        private StringBuilder from;

        /// <summary>
        ///     The from started.
        /// </summary>
        private bool fromStarted;

        /// <summary>
        ///     The overall select started.
        /// </summary>
        private bool overallSelectStarted;

        /// <summary>
        ///     The query metadata.
        /// </summary>
        private IQueryMetadata queryMetadata;

        /// <summary>
        ///     The select.
        /// </summary>
        private StringBuilder select;

        /// <summary>
        ///     The visited tables.
        /// </summary>
        private List<ITableInstance> visitedTables;

        /// <summary>
        ///     The where.
        /// </summary>
        private StringBuilder where;

        /// <summary>
        ///     The where started.
        /// </summary>
        private bool whereStarted;

        /// <summary>
        ///     This is the top-level method for the generation of an SQL query that is equivalent of the query metadata.
        ///     It first calls the appropriate methods for the elimination of special storage scheme cases, like the ISTAT scheme
        ///     and then recursively creates the query.
        /// </summary>
        /// <param name="queryMetadata">
        ///     the query metadata for which the query will be created.
        /// </param>
        /// <param name="dissConn">
        ///     The dissemination Connection
        /// </param>
        /// <returns>
        ///     The System.String.
        /// </returns>
        public virtual string GenerateQuery(IQueryMetadata queryMetadata, DbConnection dissConn)
        {
            this.queryMetadata = queryMetadata;
            queryMetadata.Clone();
            // keep a ref to the generated column instance that will be removed
            queryMetadata.RemoveMergedColumn(null);

            queryMetadata = this.ProcessMasterTables(queryMetadata, dissConn);
            queryMetadata = this.ProcessColMergingTables(queryMetadata);

            string retSql;
            if (queryMetadata.UnionRelations.Count > 0)
            {
                var sb = this.WriteUnionQuery(queryMetadata);

                retSql = sb.ToString();
            }
            else
            {
                var firstTableToProcess = queryMetadata.TableInstances[0];
                retSql = this.GetSqlFrom(queryMetadata, firstTableToProcess, true);
            }

            return retSql;
        }

        /// <summary>
        ///     Gets the escaped string.
        /// </summary>
        /// <param name="strToEscape">
        ///     The string to escape.
        /// </param>
        /// <returns>
        ///     The escaped string.
        /// </returns>
        public virtual string GetEscapedString(string strToEscape)
        {
            return Utils.GetEscapedString(strToEscape);
        }

        /// <summary>
        ///     Build and populate <paramref name="sb" /> from the specified <paramref name="value" /> and
        ///     <paramref name="constraint" />
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <param name="sb">
        ///     The string buffer
        /// </param>
        /// <param name="constraint">
        ///     The constraint.
        /// </param>
        protected virtual void BuildConstraintForTable(Value value, StringBuilder sb, IAllowedValues constraint)
        {
            switch (value.Operator.ToUpperInvariant())
            {
                case "IN":
                case "NOT IN":
                    sb.AppendFormat("( {0}.{1} {2} ({3}) )", constraint.ColumnInstance.TableInstance.ID, constraint.ColumnInstance.Name, value.Operator, value.Val);
                    break;
                default:
                    sb.AppendFormat("( {0}.{1} {2} '{3}' )", constraint.ColumnInstance.TableInstance.ID, constraint.ColumnInstance.Name, value.Operator, this.GetEscapedString(value.Val));
                    break;
            }
        }

        /// <summary>
        ///     Writes the FROM clauses.
        /// </summary>
        /// <param name="currentTable">
        ///     The current table.
        /// </param>
        protected virtual void WriteFrom(ITableInstance currentTable)
        {
            if (this.fromStarted)
            {
                this.@from.Append(", ");
            }

            // is it a composite table? If yes, append its query, else its name
            var table = currentTable as CompositeTableInstance;
            if (table != null)
            {
                this.@from.Append(string.Format("{0}-- Composite table{0}", Environment.NewLine));
                this.@from.Append("(");
                this.@from.Append(table.Query);
                this.@from.Append(")");
            }
            else
            {
                this.@from.Append(currentTable.Name);
            }

            this.@from.Append(" ");
            this.@from.Append(currentTable.ID);
            //this.@from.Append(string.Format("{0}", Environment.NewLine));

            // this is to know that from has started, only the first time will actually change from false --> true
            this.fromStarted = true;
        }

        /// <summary>
        ///     Write the SELECT clauses.
        /// </summary>
        /// <param name="currentTable">
        ///     The current table.
        /// </param>
        protected virtual void WriteSelect(ITableInstance currentTable)
        {
            foreach (var columnInstance in currentTable.ColumnInstances)
            {
                if (columnInstance.IsReturned)
                {
                    // if select has started, then "and " is needed
                    if (this.overallSelectStarted)
                    {
                        this.@select.Append(", ");
                    }

                    if (columnInstance.IsFixedValue)
                    {
                        this.@select.Append("'");
                        this.@select.Append(this.GetEscapedString(columnInstance.FixedValue));
                        this.@select.Append("'");
                    }
                    else
                    {
                        this.@select.Append(columnInstance.TableInstance.ID);
                        this.@select.Append(".");
                        this.@select.Append(columnInstance.Name);
                    }

                    this.@select.Append(" as ");
                    this.@select.Append(columnInstance.ID);

                    // this is to know that select has started, only the first time will actually change from false --> true
                    this.overallSelectStarted = true;
                }
            }
        }

        /// <summary>
        ///     Write WHERE clauses.
        /// </summary>
        /// <param name="previousTable">
        ///     The previous table.
        /// </param>
        /// <param name="currentTable">
        ///     The current table.
        /// </param>
        protected virtual void WriteWhere(ITableInstance previousTable, ITableInstance currentTable)
        {
            foreach (var relation in this.queryMetadata.Relations)
            {
                if (previousTable != null && ((relation.TableInstanceFrom.Equals(previousTable) && relation.TableInstanceTo.Equals(currentTable)) || (relation.TableInstanceTo.Equals(previousTable) && relation.TableInstanceFrom.Equals(currentTable))))
                {
                    foreach (var columnMapping in relation.ColumnMappings)
                    {
                        if (this.whereStarted)
                        {
                            this.@where.Append("and ");
                        }

                        if (columnMapping.ColumnInstanceFrom.IsFixedValue)
                        {
                            this.@where.Append("'"); // the ID is used in the "as" alias of the table in the FROM
                            this.@where.Append(this.GetEscapedString(columnMapping.ColumnInstanceFrom.FixedValue));
                            this.@where.Append("'");
                        }
                        else
                        {
                            this.@where.Append(columnMapping.ColumnInstanceFrom.TableInstance.ID);

                            // the ID is used in the "as" alias of the table in the FROM
                            this.@where.Append(".");
                            this.@where.Append(columnMapping.ColumnInstanceFrom.Name);
                        }

                        this.@where.Append(" = ");

                        if (columnMapping.ColumnInstanceTo.IsFixedValue)
                        {
                            this.@where.Append("'"); // the ID is used in the "as" alias of the table in the FROM
                            this.@where.Append(this.GetEscapedString(columnMapping.ColumnInstanceTo.FixedValue));
                            this.@where.Append("'");
                        }
                        else
                        {
                            this.@where.Append(columnMapping.ColumnInstanceTo.TableInstance.ID);

                            // the ID is used in the "as" alias of the table in the FROM
                            this.@where.Append(".");
                            this.@where.Append(columnMapping.ColumnInstanceTo.Name);
                        }

                        this.@where.Append(" ");

                        // this is to know that from has started, only the first time will actually change from false --> true
                        this.whereStarted = true;
                    }
                }
            }

            foreach (var constraint in currentTable.Constraints)
            {
                if (constraint.ColumnInstance != null && constraint.Values.Count > 0)
                {
                    if (this.whereStarted)
                    {
                        this.@where.Append(" and ");
                    }
                    else
                    {
                        this.whereStarted = true;
                    }

                    object logicalOperator = string.Format(" {0} ", constraint.LogicalOperator);
                    object logicalOpSeparator = null;
                    this.@where.Append(" ( ");
                    foreach (var value in constraint.Values)
                    {
                        this.@where.Append(logicalOpSeparator);
                        logicalOpSeparator = logicalOperator;
                        this.BuildConstraintForTable(value, this.@where, constraint);
                    }

                    this.@where.Append(" ) ");
                }
            }
        }

        /// <summary>
        /// Handles the relations.
        /// </summary>
        /// <param name="rel">The relative.</param>
        /// <param name="masterTable">The master table.</param>
        /// <param name="slaveSampleTable">The slave sample table.</param>
        /// <param name="relToBeDeleted">The relative to be deleted.</param>
        /// <param name="compositeTable">The composite table.</param>
        private static void HandleRelations(IRelation rel, ITableInstance masterTable, ITableInstance slaveSampleTable, List<IRelation> relToBeDeleted, CompositeTableInstance compositeTable)
        {
            // subpart 1.1: discard relations between master and slave tables
            if ((rel.TableInstanceFrom.Equals(masterTable) && rel.TableInstanceTo.Equals(slaveSampleTable)) || (rel.TableInstanceFrom.Equals(slaveSampleTable) && rel.TableInstanceTo.Equals(masterTable)))
            {
                relToBeDeleted.Add(rel);
                return;
            }

            // subpart 1.2: modify relations where only master table was involved
            if (rel.TableInstanceFrom.Equals(masterTable))
            {
                rel.TableInstanceFrom = compositeTable;

                // iterate over column mappings, and replace "from" columns from master table with their corresponding composite column
                IterateOverColumnMappings(rel, compositeTable);
            }
            else if (rel.TableInstanceTo.Equals(masterTable))
            {
                rel.TableInstanceTo = compositeTable;

                // iterate over column mappings, and replace "to" columns from master table with their corresponding composite column
                foreach (var colMap in rel.ColumnMappings)
                {
                    foreach (var compColumn in compositeTable.ColumnInstances)
                    {
                        if (colMap.ColumnInstanceTo.ID.Equals(compColumn.ID))
                        {
                            colMap.ColumnInstanceTo = compColumn;
                        }
                    }
                }
            }

            // subpart 1.3: modify relations where only slave table was involved
            if (rel.TableInstanceFrom.Equals(slaveSampleTable))
            {
                rel.TableInstanceFrom = compositeTable;

                // iterate over column mappings, and replace "from" columns from slave table with their corresponding composite column
                foreach (var colMap in rel.ColumnMappings)
                {
                    foreach (var compColumn in compositeTable.ColumnInstances)
                    {
                        if (colMap.ColumnInstanceFrom.ID.Equals(compColumn.ID))
                        {
                            colMap.ColumnInstanceFrom = compColumn;
                        }
                    }
                }
            }
            else if (rel.TableInstanceTo.Equals(slaveSampleTable))
            {
                rel.TableInstanceTo = compositeTable;

                // iterate over column mappings, and replace "to" columns from slave table with their corresponding composite column
                foreach (var colMap in rel.ColumnMappings)
                {
                    foreach (var compColumn in compositeTable.ColumnInstances)
                    {
                        if (colMap.ColumnInstanceTo.ID.Equals(compColumn.ID))
                        {
                            colMap.ColumnInstanceTo = compColumn;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Iterates the over column mappings.
        /// </summary>
        /// <param name="rel">The relative.</param>
        /// <param name="compositeTable">The composite table.</param>
        private static void IterateOverColumnMappings(IRelation rel, CompositeTableInstance compositeTable)
        {
            foreach (var colMap in rel.ColumnMappings)
            {
                foreach (var compColumn in compositeTable.ColumnInstances)
                {
                    if (colMap.ColumnInstanceFrom.ID.Equals(compColumn.ID))
                    {
                        colMap.ColumnInstanceFrom = compColumn;
                    }
                }
            }
        }

        /// <summary>
        ///     Build and populate <paramref name="sb" /> from the specified <paramref name="value" /> and
        ///     <paramref name="constraint" />
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <param name="sb">
        ///     The string buffer
        /// </param>
        /// <param name="constraint">
        ///     The constraint.
        /// </param>
        private void BuildConstraint(Value value, StringBuilder sb, IAllowedValues constraint)
        {
            switch (value.Operator.ToUpperInvariant())
            {
                case "IN":
                case "NOT IN":
                    sb.AppendFormat("( {0} {1} ({2}) )", constraint.ColumnInstance.ID, value.Operator, value.Val);
                    break;
                default:
                    sb.AppendFormat("( {0} {1} '{2}' )", constraint.ColumnInstance.ID, value.Operator, this.GetEscapedString(value.Val));
                    break;
            }
        }

        /// <summary>
        ///     The fetch master slave column mappings.
        /// </summary>
        /// <param name="masterTable">
        ///     The master table.
        /// </param>
        /// <param name="slaveSampleTable">
        ///     The slave sample table.
        /// </param>
        /// <returns>
        ///     The column mappings
        /// </returns>
        private List<IColumnMapping> FetchMasterSlaveColumnMappings(ITableInstance masterTable, ITableInstance slaveSampleTable)
        {
            var masterSlaveColumnMappings = new List<IColumnMapping>();
            foreach (var rel in this.queryMetadata.Relations)
            {
                if ((rel.TableInstanceFrom == masterTable && rel.TableInstanceTo == slaveSampleTable) || (rel.TableInstanceFrom == slaveSampleTable && rel.TableInstanceTo == masterTable))
                {
                    foreach (var colMap in rel.ColumnMappings)
                    {
                        masterSlaveColumnMappings.Add(colMap);
                    }
                }
            }

            return masterSlaveColumnMappings;
        }

        /// <summary>
        ///     This method handles the query metadata containing
        /// </summary>
        /// <param name="queryMetadata">
        /// The query data
        /// </param>
        /// <returns>
        ///     The table instance.
        /// </returns>
        private CompositeTableInstance GenerateColMergingEquivalent(IQueryMetadata queryMetadata)
        {
            var uglyColumnsCollection = queryMetadata.ColumnsMerging;
            var generatedDimensionColumn = queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance;
            var generatedMeasureColumn = queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance;

            var uglyTable = queryMetadata.ColumnsMerging[0].TableInstance;
            var columnsMergedTable = new CompositeTableInstance(queryMetadata);
            columnsMergedTable.Name = "merged";
            columnsMergedTable.ID = "merged1";

            // INNER SELECT
            // first handle non-mergeable columns

            var fixedInnerSelectPart = new FixedQueryPart().Build(queryMetadata, uglyTable, columnsMergedTable, uglyColumnsCollection);

            // COMPOSITE QUERY
            var compositeQuery = new StringBuilder(string.Empty);
            compositeQuery.Append(fixedInnerSelectPart);
            compositeQuery.Append(", ");
            compositeQuery.Append(generatedDimensionColumn.Name);
            compositeQuery.Append(", ");
            compositeQuery.Append(generatedMeasureColumn.Name);
            compositeQuery.Append(string.Format("{0}-- Merging outer component {0}", Environment.NewLine));
            compositeQuery.Append(" from (");

            var unionStarted = false;
            foreach (var col in uglyColumnsCollection)
            {
                if (unionStarted)
                {
                    compositeQuery.Append("union all ");
                }

                compositeQuery.Append(string.Format("{0}(", Environment.NewLine));
                compositeQuery.Append(fixedInnerSelectPart);
                compositeQuery.Append(", '");
                compositeQuery.Append(col.Name);
                compositeQuery.Append("' as ");
                compositeQuery.Append(generatedDimensionColumn.Name);
                compositeQuery.Append(", ");
                if (col.IsFixedValue)
                {
                    compositeQuery.Append("'" + this.GetEscapedString(col.FixedValue) + "'");
                }
                else
                {
                    compositeQuery.Append(col.Name);
                }

                compositeQuery.Append(" as ");
                compositeQuery.Append(generatedMeasureColumn.Name);
                compositeQuery.Append(string.Format("{0}-- Merging component {0}", Environment.NewLine));
                compositeQuery.Append(" from ");
                var compositeTableInstance = uglyTable as CompositeTableInstance;
                if (compositeTableInstance != null)
                {
                    var compUgly = compositeTableInstance;
                    compositeQuery.Append(string.Format("{0}(", Environment.NewLine));
                    compositeQuery.Append(compUgly.Query);
                    compositeQuery.Append(string.Format(") merg_component{0}", Environment.NewLine));
                }
                else
                {
                    compositeQuery.Append(uglyTable.Name);
                }

                unionStarted = true;
                compositeQuery.Append(") ");
            }

            compositeQuery.Append(string.Format("{0}) ", Environment.NewLine));
            compositeQuery.Append(columnsMergedTable.Name);
            columnsMergedTable.Query = compositeQuery.ToString();

            // we need another two columns: one for merged dimension and one for the primary measure
            var mergedDimCol = columnsMergedTable.AddColumnInstance();
            mergedDimCol.Name = queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance.Name;
            mergedDimCol.ID = queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance.ID;
            mergedDimCol.IsPrimaryKey = queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance.IsPrimaryKey;
            mergedDimCol.IsReturned = queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance.IsReturned;
            mergedDimCol.Type = queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance.Type;
            mergedDimCol.Index = queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance.Index;

            var primaryMeasureCol = columnsMergedTable.AddColumnInstance();
            primaryMeasureCol.Name = queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance.Name;
            primaryMeasureCol.ID = queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance.ID;
            primaryMeasureCol.IsPrimaryKey = queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance.IsPrimaryKey;
            primaryMeasureCol.IsReturned = queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance.IsReturned;
            primaryMeasureCol.Type = queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance.Type;
            primaryMeasureCol.Index = queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance.Index;

            // map the constraints to the newly created column instances
            foreach (var constraint in queryMetadata.Constraints)
            {
                if (constraint.ColumnInstance.ID == primaryMeasureCol.ID)
                {
                    constraint.ColumnInstance = primaryMeasureCol;
                }

                if (constraint.ColumnInstance.ID == mergedDimCol.ID)
                {
                    constraint.ColumnInstance = mergedDimCol;
                }
            }

            columnsMergedTable.ColumnInstances.Sort();

            return columnsMergedTable;
        }

        /// <summary>
        /// This method generates the equivalent table of a master-table scheme in a new Query Metadata
        /// </summary>
        /// <param name="queryMetadata">the current query metadata</param>
        /// <param name="colTablRef">the column-table reference that represents the master-table scheme</param>
        /// <param name="cn">The database connection.</param>
        /// <returns>
        /// a new composite table containing the equivalent query, that has a column instance for every master and slave column
        /// instance (except the joined ones, where only one is returned with the same name as the slave one).
        /// </returns>
        private CompositeTableInstance GenerateMasterSlaveEquivalent(IQueryMetadata queryMetadata, IColumnTableReference colTablRef, DbConnection cn)
        {
            // STEP 1: 
            // * get the master table
            // * prepare query for all slave tables
            var masterTable = colTablRef.ColumnInstance.TableInstance;
            var slaveSampleTable = colTablRef.TableInstance;
            var masterSlaveColumnMappings = this.FetchMasterSlaveColumnMappings(masterTable, slaveSampleTable);

            var compositeTable = new CompositeTableInstance(queryMetadata);
            compositeTable.Name = masterTable.Name + "_" + slaveSampleTable.Name;
            compositeTable.ID = masterTable.ID + "_" + slaveSampleTable.ID;

            // i.e. for ISTAT:
            // select distinct n_dato from metadati
            var masterContentsQuery = new StringBuilder("select distinct ");
            if (colTablRef.ColumnInstance.IsFixedValue)
            {
                masterContentsQuery.Append("'" + this.GetEscapedString(colTablRef.ColumnInstance.FixedValue) + "' as " + colTablRef.ColumnInstance.Name);
            }
            else
            {
                masterContentsQuery.Append(colTablRef.ColumnInstance.Name);
            }

            masterContentsQuery.Append(string.Format("{0}-- Master outer outer from{0}", Environment.NewLine));
            masterContentsQuery.Append(" from ");
            masterContentsQuery.Append(masterTable.Name);

            // System.Console.WriteLine(string.Format("{0}{0}", Environment.NewLine) + masterContentsQuery.ToString() + string.Format("{0}{0}", Environment.NewLine));

            // STEP 2: 
            // * execute the query and build up the query for the composite table

            // OUTER SELECT part by iterating over both master and slave columns.
            // ie for ISTAT:
            // select cod, freq, u_mis, n_dato, dom, s_dom, s_s_dom, tipo, vs, ateco, anno, periodo, valore
            var compositeQuery = new StringBuilder("select ");
            var selectStarted = false;

            var masterAndSlaveColumns = new List<IColumnInstance>();
            masterAndSlaveColumns.AddRange(masterTable.ColumnInstances);
            masterAndSlaveColumns.AddRange(slaveSampleTable.ColumnInstances);

            // bool skipAppend = false;
            foreach (var col in masterAndSlaveColumns)
            {
                // skipAppend = false;
                //// do not return PK/FK columns of Master table: they will be returned as slave columns (they are joined)

                // foreach (ColumnMapping compMap in masterSlaveColumnMappings)
                // {
                // if (
                // col.TableInstance.Equals(masterTable) && // if this is a master column
                // (compMap.ColumnInstanceFrom.Equals(col) || compMap.ColumnInstanceTo.Equals(col)) // and is somehow related to a slave column
                // )
                // {
                // skipAppend = true;
                // break;
                // }
                // }
                // if(skipAppend) 
                // {
                // continue;   // do not append anything to the select
                // }
                if (selectStarted)
                {
                    compositeQuery.Append(", ");
                }

                // create new column for composite table
                var newCompColumn = compositeTable.AddColumnInstance();

                // aris (old: newCompColumn.Name = col.Name;)
                newCompColumn.Name = col.ID;
                newCompColumn.ID = col.ID;
                newCompColumn.IsPrimaryKey = col.IsPrimaryKey;
                newCompColumn.IsReturned = col.IsReturned;
                newCompColumn.Type = col.Type;
                newCompColumn.Index = col.Index;

                // newCompColumn.IsFixedValue = col.IsFixedValue;
                // newCompColumn.FixedValue = col.FixedValue;

                // map the constraints to the newly created column instances
                foreach (var constraint in queryMetadata.Constraints)
                {
                    if (constraint.ColumnInstance.ID == newCompColumn.ID)
                    {
                        constraint.ColumnInstance = newCompColumn;
                    }
                }

                // append to query
                // aris (old: compositeQuery.Append(col.Name);)
                compositeQuery.Append(col.ID);

                selectStarted = true; // to keep track whether a "," is needed
            }

            compositeTable.ColumnInstances.Sort();

            // at this stage, we have the select of both the outer query, and the inner ones.
            // String select = compositeQuerySelect.ToString();

            // OUTER FROM part
            compositeQuery.Append(string.Format("{0}-- Master outer from{0}", Environment.NewLine));
            compositeQuery.Append(" from (");

            // This is your data adapter that understands SQL databases:
            // OdbcDataAdapter da = new OdbcDataAdapter(masterContentsQuery.ToString(), cn);
            var comm = cn.CreateCommand();
            comm.CommandText = masterContentsQuery.ToString();

            try
            {
                cn.Open();

                // Fill the data table with select statement's query results:
                // int recordsAffected = da.Fill(dataTable);
                // if (recordsAffected > 0) 
                // {
                var dr = comm.ExecuteReader();
                var unionNeeded = false;
                while (dr.Read())
                {
                    // }
                    // foreach (DataRow masterTableRow in dataTable.Rows) {
                    var currentSlaveTable = dr.GetString(0); // masterTableRow[0].ToString();

                    if (unionNeeded)
                    {
                        compositeQuery.Append(" union all ");
                    }

                    // INNER SELECT
                    var compositeSelectStarted = false;
                    compositeQuery.Append(string.Format("{0}( select ", Environment.NewLine));
                    foreach (var col in masterTable.ColumnInstances)
                    {
                        // skipAppend = false;
                        //// do not return PK/FK columns of Master table: they will be returned as slave columns (they are joined)
                        // foreach (ColumnMapping compMap in masterSlaveColumnMappings)
                        // {
                        // if (compMap.ColumnInstanceFrom.Equals(col) || compMap.ColumnInstanceTo.Equals(col)) // and is somehow related to a slave column
                        // {
                        // skipAppend = true;
                        // break;
                        // }
                        // }
                        // if (skipAppend) {
                        // continue;   // do not append anything to the select
                        // }
                        if (compositeSelectStarted)
                        {
                            compositeQuery.Append(", ");
                        }

                        if (col.IsFixedValue)
                        {
                            compositeQuery.Append("'");
                            compositeQuery.Append(this.GetEscapedString(col.FixedValue));
                            compositeQuery.Append("'");
                        }
                        else
                        {
                            compositeQuery.Append(col.TableInstance.Name);
                            compositeQuery.Append(".");
                            compositeQuery.Append(col.Name);
                        }

                        // aris aaddition
                        compositeQuery.Append(" as ");
                        compositeQuery.Append(col.ID);

                        compositeSelectStarted = true; // to keep track whether a "," is needed
                    }

                    foreach (var col in slaveSampleTable.ColumnInstances)
                    {
                        //// do not return PK/FK columns of Master table: they will be returned as slave columns (they are joined)
                        // foreach (ColumnMapping compMap in masterSlaveColumnMappings)
                        // {
                        // if (compMap.ColumnInstanceFrom.Equals(col) || compMap.ColumnInstanceTo.Equals(col)) // and is somehow related to a slave column
                        // {
                        // skipAppend = true;
                        // break;
                        // }
                        // }
                        if (compositeSelectStarted)
                        {
                            compositeQuery.Append(", ");
                        }

                        compositeQuery.Append(currentSlaveTable);
                        compositeQuery.Append(".");
                        compositeQuery.Append(col.Name);

                        // aris addition
                        compositeQuery.Append(" as ");
                        compositeQuery.Append(col.ID);

                        compositeSelectStarted = true; // to keep track whether a "," is needed
                    }

                    // inner FROM
                    // i.e. ISTAT:
                    // from
                    compositeQuery.Append(string.Format("{0}-- Master component{0}", Environment.NewLine));
                    compositeQuery.Append(" from ");
                    compositeQuery.Append(masterTable.Name);
                    compositeQuery.Append(", ");
                    compositeQuery.Append(currentSlaveTable);

                    // inner WHERE
                    // i.e. ISTAT:
                    // where
                    compositeQuery.Append(" where ");

                    // add slave table name
                    if (colTablRef.ColumnInstance.IsFixedValue)
                    {
                        compositeQuery.Append("'");
                        compositeQuery.Append(this.GetEscapedString(colTablRef.ColumnInstance.FixedValue));
                        compositeQuery.Append("'");
                    }
                    else
                    {
                        compositeQuery.Append(masterTable.Name);
                        compositeQuery.Append(".");
                        compositeQuery.Append(colTablRef.ColumnInstance.Name);
                    }

                    compositeQuery.Append("='");
                    compositeQuery.Append(currentSlaveTable);
                    compositeQuery.Append("' ");

                    // where: put all the FK constraints
                    foreach (var colMap in masterSlaveColumnMappings)
                    {
                        compositeQuery.Append("and ");

                        if (colMap.ColumnInstanceFrom.TableInstance.Equals(slaveSampleTable))
                        {
                            // the relation uses the slave table. But its in fact now dynamic
                            compositeQuery.Append(currentSlaveTable);
                        }
                        else
                        {
                            compositeQuery.Append(masterTable.Name);
                        }

                        compositeQuery.Append(".");
                        compositeQuery.Append(colMap.ColumnInstanceFrom.Name);
                        compositeQuery.Append(" = ");
                        if (colMap.ColumnInstanceTo.TableInstance.Equals(slaveSampleTable))
                        {
                            // the relation uses the slave table. But its in fact now dynamic
                            compositeQuery.Append(currentSlaveTable);
                        }
                        else
                        {
                            compositeQuery.Append(masterTable.Name);
                        }

                        compositeQuery.Append(".");
                        compositeQuery.Append(colMap.ColumnInstanceTo.Name);
                        compositeQuery.Append(" ");
                    }

                    compositeQuery.Append(")");

                    // housekeeping
                    unionNeeded = true; // to keep track that "union all" is needed for next select
                }

                dr.Close();

                // cn.Close();
                dr.Dispose();

                // cn.Dispose();
                compositeQuery.Append(string.Format("{0}) totalunion", Environment.NewLine));
            }
            catch (DbException e)
            {
                var msg = " Message: " + e.Message + "\n";
                Console.WriteLine(msg);
            }
            finally
            {
                if (cn.State != ConnectionState.Closed)
                {
                    cn.Close();
                }
            }

            // finally put the generated query to the composite table, and return it
            compositeTable.Query = compositeQuery.ToString();
            return compositeTable;
        }

        /// <summary>
        ///     The get SQL from.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="firstTableToProcess">
        ///     The first table to process.
        /// </param>
        /// <param name="doProcessConstraints">
        ///     The do process constraints.
        /// </param>
        /// <returns>
        ///     The System.String.
        /// </returns>
        private string GetSqlFrom(IQueryMetadata queryMetadata, ITableInstance firstTableToProcess, bool doProcessConstraints)
        {
            // initialise variables
            this.@select = new StringBuilder("select ");
            this.@from = new StringBuilder(string.Format("{0} from ", Environment.NewLine));
            this.@where = new StringBuilder(string.Format("{0} where ", Environment.NewLine));
            this.overallSelectStarted = false;
            this.fromStarted = false;
            this.whereStarted = false;
            this.visitedTables = new List<ITableInstance>();

            // this will recursively process tables in the correct order
            if (queryMetadata.CrossJoinRelations.Count > 0)
            {
                this.ProcessTableCrossJoin(null, firstTableToProcess);
            }
            else
            {
                this.ProcessTable(null, firstTableToProcess);
            }

            if (doProcessConstraints)
            {
                this.ProcessConstraints(queryMetadata.Constraints);
            }

            // merge the three stringbuilders
            this.@select.Append(this.@from);
            if (this.whereStarted)
            {
                this.@select.Append(this.@where);
            }

            return this.@select.ToString();
        }

        /// <summary>
        /// The process col merging tables.
        /// </summary>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <returns>
        /// The query data.
        /// </returns>
        private IQueryMetadata ProcessColMergingTables(IQueryMetadata queryMetadata)
        {
            var uglyColumnsCollection = queryMetadata.ColumnsMerging;

            if (uglyColumnsCollection != null && uglyColumnsCollection.Count > 0)
            {
                // generate composite equivalent
                var compositeTable = this.GenerateColMergingEquivalent(queryMetadata);

                // HOUSEKEEPING in the query metadata
                // PART 1: modify relations
                var uglyTable = queryMetadata.ColumnsMerging[0].TableInstance;
                foreach (var rel in queryMetadata.Relations)
                {
                    // subpart 1.1: modify relations to former table
                    if (rel.TableInstanceFrom.Equals(uglyTable))
                    {
                        // iterate over column mappings, and replace "from" columns from ugly table with their corresponding composite column
                        foreach (var colMap in rel.ColumnMappings)
                        {
                            foreach (var compColumn in compositeTable.ColumnInstances)
                            {
                                if (colMap.ColumnInstanceFrom.Name.Equals(compColumn.Name))
                                {
                                    colMap.ColumnInstanceFrom = compColumn;
                                    break;
                                }
                            }
                        }

                        rel.TableInstanceFrom = compositeTable;
                    }
                    else if (rel.TableInstanceTo.Equals(uglyTable))
                    {
                        // iterate over column mappings, and replace "to" columns from ugly table with their corresponding composite column
                        foreach (var colMap in rel.ColumnMappings)
                        {
                            foreach (var compColumn in compositeTable.ColumnInstances)
                            {
                                if (colMap.ColumnInstanceTo.Name.Equals(compColumn.Name))
                                {
                                    colMap.ColumnInstanceTo = compColumn;
                                    break;
                                }
                            }
                        }

                        rel.TableInstanceTo = compositeTable;
                    }
                }

                // PART 2: remove ugly table columns and the ugly table
                foreach (var uglyCol in uglyTable.ColumnInstances)
                {
                    queryMetadata.ColumnInstances.Remove(uglyCol);
                }

                queryMetadata.TableInstances.Remove(uglyTable);

                // PART 3: remove column mergings
                while (queryMetadata.ColumnsMerging.Count > 0)
                {
                    queryMetadata.ColumnsMerging.Remove(queryMetadata.ColumnsMerging[0]);
                }
            }

            return queryMetadata;
        }

        /// <summary>
        ///     The process constraints.
        /// </summary>
        /// <param name="constraintsCollection">
        ///     The constraints collection.
        /// </param>
        private void ProcessConstraints(ConstraintsCollection constraintsCollection)
        {
            foreach (var constraint in constraintsCollection)
            {
                if (constraint.ColumnInstance != null && constraint.Values.Count > 0)
                {
                    if (this.whereStarted)
                    {
                        this.@where.Append(" and ");
                    }
                    else
                    {
                        this.whereStarted = true;
                    }

                    object logicalOperator = string.Format(" {0} ", constraint.LogicalOperator);
                    object logicalOpSeparator = null;
                    this.@where.Append(" ( ");
                    foreach (var value in constraint.Values)
                    {
                        this.@where.Append(logicalOpSeparator);
                        logicalOpSeparator = logicalOperator;
                        this.BuildConstraintForTable(value, this.@where, constraint);

                        // where.AppendFormat("( {0} {1} '{2}' )", constraint.ColumnInstance.Name, value.Operator, value.Val.Replace("'", "''"));
                    }

                    this.@where.Append(" ) ");

                    // where.Append(string.Format("{0}.{1} in (", constraint.ColumnInstance.TableInstance.ID, constraint.ColumnInstance.Name));
                    // bool isFirst = true;
                    // foreach (Value value in constraint.Values) {
                    // if (!isFirst)
                    // where.Append(",");
                    // isFirst = false;
                    // where.Append(string.Format("'{0}'", value.Val.Replace("'", "''")));
                    // }
                    // where.Append(")");
                }
            }
        }

        /// <summary>
        ///     This method eliminates master tables and their slave tables with a composite table
        /// </summary>
        /// <param name="queryMetadata">
        /// The query metadata
        /// </param>
        /// <param name="cn">
        ///     The database connection.
        /// </param>
        /// <returns>
        ///     The query data.
        /// </returns>
        private IQueryMetadata ProcessMasterTables(IQueryMetadata queryMetadata, DbConnection cn)
        {
            // Are there still exist master tables?
            if (queryMetadata.ColumnTableReferences.Count > 0)
            {
                // YES, at least one master tables exist. Let's get rid of one.
                // Get the first of master-slave relations that still exist
                var colTablRef = queryMetadata.ColumnTableReferences[0];
                var masterTable = colTablRef.ColumnInstance.TableInstance;
                var slaveSampleTable = colTablRef.TableInstance;

                var compositeTable = this.GenerateMasterSlaveEquivalent(queryMetadata, colTablRef, cn);

                // System.Console.WriteLine(compositeTable.Query);

                // HOUSEKEEPING in the query metadata
                // PART 1: modify relations
                var relToBeDeleted = new List<IRelation>();
                foreach (var rel in queryMetadata.Relations)
                {
                    HandleRelations(rel, masterTable, slaveSampleTable, relToBeDeleted, compositeTable);
                }

                // subpart 1.1: delete marked relations
                foreach (var delRelation in relToBeDeleted)
                {
                    queryMetadata.Relations.Remove(delRelation);
                }

                // PART 2: modify column mergingss (if they exist, they will be in the slave table)
                if (queryMetadata.ColumnsMerging != null && queryMetadata.ColumnsMerging.Count > 0)
                {
                    var uglyTable = queryMetadata.ColumnsMerging[0].TableInstance;
                    if (uglyTable.Equals(slaveSampleTable))
                    {
                        // cannot have the merged columns in master table, since they contain the promary measure.
                        var removeMergeColumns = new List<IColumnInstance>();
                        var addMergeColumns = new List<IColumnInstance>();
                        foreach (var oldColMerge in queryMetadata.ColumnsMerging)
                        {
                            foreach (var compColumn in compositeTable.ColumnInstances)
                            {
                                if (oldColMerge.ID.Equals(compColumn.ID))
                                {
                                    removeMergeColumns.Add(oldColMerge);
                                    addMergeColumns.Add(compColumn);
                                }
                            }
                        }

                        foreach (var oldColMerge in removeMergeColumns)
                        {
                            queryMetadata.ColumnsMerging.Remove(oldColMerge);
                        }

                        foreach (var newColMerge in addMergeColumns)
                        {
                            queryMetadata.ColumnsMerging.Add(newColMerge);
                        }
                    }
                }

                // PART 3: remove master and slave table columns and the tables
                foreach (var masterCol in masterTable.ColumnInstances)
                {
                    queryMetadata.ColumnInstances.Remove(masterCol);
                }

                foreach (var slaveCol in slaveSampleTable.ColumnInstances)
                {
                    queryMetadata.ColumnInstances.Remove(slaveCol);
                }

                queryMetadata.TableInstances.Remove(masterTable);
                queryMetadata.TableInstances.Remove(slaveSampleTable);

                // PART 4: remove column-table reference that was eliminated
                queryMetadata.ColumnTableReferences.Remove(queryMetadata.ColumnTableReferences[0]);

                // RECURSIVE CALL TO SELF:
                return this.ProcessMasterTables(queryMetadata, cn);
            }

            return queryMetadata;
        }

        /// <summary>
        ///     This method processes a table. It appends everything necessary in the select, from and where part of the query.
        /// </summary>
        /// <param name="previousTable">
        ///     The previous Table.
        /// </param>
        /// <param name="currentTable">
        ///     the table to be processed
        /// </param>
        private void ProcessTable(ITableInstance previousTable, ITableInstance currentTable)
        {
            if (this.visitedTables.Contains(currentTable))
            {
                return;
            }

            this.visitedTables.Add(currentTable);

            if (!this.visitedTables.Contains(previousTable))
            {
                this.visitedTables.Add(previousTable);
            }

            // SELECT
            this.WriteSelect(currentTable);

            // FROM
            this.WriteFrom(currentTable);

            // WHERE
            this.WriteWhere(previousTable, currentTable);

            // to all connected tables
            foreach (var relation in this.queryMetadata.Relations)
            {
                if (relation.TableInstanceFrom.Equals(currentTable) && !this.visitedTables.Contains(relation.TableInstanceTo))
                {
                    this.ProcessTable(currentTable, relation.TableInstanceTo);
                }
                else if (relation.TableInstanceTo.Equals(currentTable) && !this.visitedTables.Contains(relation.TableInstanceFrom))
                {
                    this.ProcessTable(currentTable, relation.TableInstanceFrom);
                }
            }
        }

        /// <summary>
        ///     This method processes a table. It appends everything necessary in the select, from and where part of the query.
        /// </summary>
        /// <param name="previousTable">
        ///     The previous Table.
        /// </param>
        /// <param name="currentTable">
        ///     the table to be processed
        /// </param>
        private void ProcessTableCrossJoin(ITableInstance previousTable, ITableInstance currentTable)
        {
            while (currentTable != null && !this.visitedTables.Contains(currentTable))
            {
                this.visitedTables.Add(currentTable);

                if (!this.visitedTables.Contains(previousTable))
                {
                    this.visitedTables.Add(previousTable);
                }

                // SELECT
                this.WriteSelect(currentTable);

                // FROM
                this.WriteFrom(currentTable);

                // WHERE
                this.WriteWhere(previousTable, currentTable);

                // to all connected tables
                foreach (var relation in this.queryMetadata.CrossJoinRelations)
                {
                    if (relation.TableInstanceFrom.Equals(currentTable) && !this.visitedTables.Contains(relation.TableInstanceTo))
                    {
                        previousTable = currentTable;
                        currentTable = relation.TableInstanceTo;
                    }
                    else if (relation.TableInstanceTo.Equals(currentTable) && !this.visitedTables.Contains(relation.TableInstanceFrom))
                    {
                        previousTable = currentTable;
                        currentTable = relation.TableInstanceFrom;
                    }
                }
            }
        }

        /// <summary>
        ///     Write union query.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <returns>
        ///     The <see cref="StringBuilder" />.
        /// </returns>
        private StringBuilder WriteUnionQuery(IQueryMetadata queryMetadata)
        {
            // Then we only have UnionRelations and no other relations (all other relations are forbidden from GUI)
            // so we pick the first table in the Union Relations and we start generating the query...
            var currentTable = queryMetadata.UnionRelations.GetFirstTableOfUnion();
            var sb = new StringBuilder();
            sb.Append(" select ");
            var i = 0;
            foreach (var col in currentTable.ColumnInstances)
            {
                if (col.IsReturned)
                {
                    if (i != 0)
                    {
                        sb.Append(", ");
                    }

                    sb.Append(col.ID);
                    i++;
                }
            }

            sb.Append(" from ( ");
            sb.Append(Environment.NewLine);

            i = 0;
            while (currentTable != null)
            {
                if (i != 0)
                {
                    sb.Append(Environment.NewLine);
                    sb.Append(" union all ");
                    sb.Append(Environment.NewLine);
                }

                sb.Append(this.GetSqlFrom(queryMetadata, currentTable, false));
                currentTable = currentTable.GetNextUnionTable();
                i++;
            }

            sb.Append(Environment.NewLine);
            sb.Append(" ) uniontable ");
            sb.Append(Environment.NewLine);

            if (queryMetadata.Constraints.Count > 0)
            {
                sb.Append(" where ");
                i = 0;
                foreach (var constraint in queryMetadata.Constraints)
                {
                    if (constraint.ColumnInstance != null && constraint.Values.Count > 0)
                    {
                        if (i != 0)
                        {
                            sb.Append(" and ");
                        }

                        object logicalOperator = string.Format(" {0} ", constraint.LogicalOperator);
                        object logicalOpSeparator = null;
                        sb.Append(" ( ");
                        foreach (var value in constraint.Values)
                        {
                            sb.Append(logicalOpSeparator);
                            logicalOpSeparator = logicalOperator;
                            this.BuildConstraint(value, sb, constraint);
                        }

                        sb.Append(" ) ");
                        i++;
                    }
                }
            }

            return sb;
        }

        /// <summary>
        ///     This is the equivalent of a table, but it does not refer to a real table, but rather a query.
        ///     This class is used to replace in the model cases of master-slave scheme, or columns merging scheme.
        /// </summary>
        public class CompositeTableInstance : TableInstance
        {
            /// <summary>
            ///     Initializes a new instance of the <see cref="CompositeTableInstance" /> class.
            /// </summary>
            /// <param name="queryMetadata">
            ///     The query metadata.
            /// </param>
            public CompositeTableInstance(IQueryMetadata queryMetadata)
                : base(queryMetadata)
            {
                // base(queryMetadata);
            }

            /// <summary>
            ///     Gets or sets the query.
            /// </summary>
            public string Query { get; set; }
        }
    }
}
