// -----------------------------------------------------------------------
// <copyright file="CrossJoinRelationsCollection.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System.ComponentModel;

    /// <summary>
    ///     The cross-join relations collection.
    /// </summary>
    public class CrossJoinRelationsCollection : BindingList<ICrossJoinRelation>
    {
        /// <summary>
        ///     The _ query metadata.
        /// </summary>
        private readonly IQueryMetadata _queryMetadata;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CrossJoinRelationsCollection" /> class.
        /// </summary>
        /// <param name="querymetadata">
        ///     The query metadata.
        /// </param>
        public CrossJoinRelationsCollection(IQueryMetadata querymetadata)
        {
            this._queryMetadata = querymetadata;
        }

        /// <summary>
        ///     Gets the query metadata.
        /// </summary>
        public IQueryMetadata QueryMetadata
        {
            get
            {
                return this._queryMetadata;
            }
        }

        /// <summary>
        ///     Checks if a relation exists between <paramref name="source" /> and <paramref name="destination" />
        /// </summary>
        /// <param name="source">
        ///     The source.
        /// </param>
        /// <param name="destination">
        ///     The destination.
        /// </param>
        /// <returns>
        ///     True  if a relation exists between <paramref name="source" /> and <paramref name="destination" />; otherwise false.
        /// </returns>
        public bool ExistsRelation(ITableInstance source, ITableInstance destination)
        {
            foreach (var item in this.Items)
            {
                if ((string.Equals(source.ID, item.TableInstanceFrom.ID) && string.Equals(destination.ID, item.TableInstanceTo.ID)) || (string.Equals(source.ID, item.TableInstanceTo.ID) && string.Equals(destination.ID, item.TableInstanceFrom.ID)))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     The do add.
        /// </summary>
        /// <param name="ur">
        ///     The ur.
        /// </param>
        public void DoAdd(ICrossJoinRelation ur)
        {
            this.Add(ur);
        }
    }
}