// -----------------------------------------------------------------------
// <copyright file="XmlFormatExtractor.cs" company="EUROSTAT">
//   Date Created : 2018-07-19
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    public class XmlFormatExtractor : IFormatSpecificExtractor
    {
        private readonly IQueryMetadata _queryMetadata;
        private readonly Dictionary<string, ITableInstance> _tableInstanceIdDict;
        private readonly Dictionary<string, ITableInstance> _tableInstanceColumnIdDict;
        private readonly Dictionary<string, int> _columnInstanceSortIndexDict;
        private readonly Dictionary<string, IColumnInstance> _columnInstanceIdDict;

        public XmlFormatExtractor(IQueryMetadata queryMetadata, Dictionary<string, ITableInstance> tableInstanceIdDict, Dictionary<string, ITableInstance> tableInstanceColumnIdDict,
            Dictionary<string, int> columnInstanceSortIndexDict, Dictionary<string, IColumnInstance> columnInstanceIdDict)
        {
            this._queryMetadata = queryMetadata;
            this._tableInstanceIdDict = tableInstanceIdDict;
            this._tableInstanceColumnIdDict = tableInstanceColumnIdDict;
            this._columnInstanceSortIndexDict = columnInstanceSortIndexDict;
            this._columnInstanceIdDict = columnInstanceIdDict;
        }

        public IQueryMetadata Read(string value)
        {
            var doc = new XmlDocument();
            doc.LoadXml(value);

            // get columnsInstances of Dataset
            var tableInstancesNodes = doc.DocumentElement.GetElementsByTagName("TableInstances");
            if (tableInstancesNodes.Count == 1)
            {
                this.HandleSingleTableInstanceNode(tableInstancesNodes, this._queryMetadata, this._tableInstanceIdDict, this._tableInstanceColumnIdDict, this._columnInstanceSortIndexDict);
            }

            // get columnsInstances of Dataset
            var columnInstancesNodes = doc.DocumentElement.GetElementsByTagName("ColumnInstances");
            if (columnInstancesNodes.Count == 1)
            {
                // throw new Exception("Invalid QueryMetadata XML: number ColumnInstances != 1");
                var columnInstancesNode = columnInstancesNodes.Item(0);
                foreach (XmlNode columnInstnanceNode in columnInstancesNode.ChildNodes)
                {
                    // TODO check Node is an Element type
                    var columnInstanceEl = (XmlElement)columnInstnanceNode;

                    var columnInstance = this.ReadColumnInstance(columnInstanceEl, this._tableInstanceColumnIdDict, this._columnInstanceSortIndexDict);

                    this._columnInstanceIdDict.Add(columnInstance.ID, columnInstance);

                    // Boolean.TryParse(columnInstanceEl.GetAttribute("IsPrimaryKey"), columnInstance.IsPrimaryKey);
                    // columnInstance.IsReturned = columnInstanceEl.GetAttribute("IsReturned");
                }
            }

            // get relations of Dataset
            var relationsNodes = doc.DocumentElement.GetElementsByTagName("Relations");
            if (relationsNodes.Count == 1)
            {
                // throw new Exception("Invalid QueryMetadata XML: number Relations != 1");
                var relationsNode = relationsNodes.Item(0);
                foreach (XmlNode relationNode in relationsNode.ChildNodes)
                {
                    // TODO check Node is an Element type
                    var relationNodeEl = (XmlElement)relationNode;

                    var relation = this.CreateRelation(this._queryMetadata, relationNodeEl, this._tableInstanceIdDict);

                    foreach (XmlNode columnMappingNode in relationNode.ChildNodes)
                    {
                        var columnMappingNodeEl = (XmlElement)columnMappingNode;

                        this.ReadColumnMapping(relation, this._columnInstanceIdDict, columnMappingNodeEl);
                    }
                }
            }

            // get ColumnTableReferences of Dataset
            var columnTableReferencesNodes = doc.DocumentElement.GetElementsByTagName("ColumnTableReferences");
            if (columnTableReferencesNodes.Count == 1)
            {
                // throw new Exception("Invalid QueryMetadata XML: number ColumnTableReferences != 1");
                var columnTableReferencesNode = columnTableReferencesNodes.Item(0);
                foreach (XmlNode columnTableReferenceNode in columnTableReferencesNode.ChildNodes)
                {
                    // TODO check Node is an Element type
                    var columnTableReferenceNodeEl = (XmlElement)columnTableReferenceNode;

                    this.ReadColumnTableReference(this._queryMetadata, this._columnInstanceIdDict, columnTableReferenceNodeEl, this._tableInstanceIdDict);
                }
            }

            // get ColumnsMerging of Dataset
            var columnsMergingNodes = doc.DocumentElement.GetElementsByTagName("ColumnsMerging");
            if (columnsMergingNodes.Count == 1)
            {
                var columnsMergingNode = columnsMergingNodes.Item(0);
                var columnsMergingNodeEl = (XmlElement)columnsMergingNode;

                this.ReadColumnMerging(columnsMergingNodeEl, this._columnInstanceIdDict, this._queryMetadata);
            }

// get Where details
            var whereDetailsNodes = doc.DocumentElement.GetElementsByTagName("Constraints");
            if (whereDetailsNodes.Count == 1)
            {
                this.GetWhereDetailsWhenCountIsOne(whereDetailsNodes, this._queryMetadata, this._columnInstanceIdDict);
            }

// get TableCostraints details
            var tableConstraintsNodes = doc.DocumentElement.GetElementsByTagName("TableConstraints");
            if (tableConstraintsNodes.Count == 1)
            {
                this.GetTableConstraints(tableConstraintsNodes, this._tableInstanceIdDict, this._columnInstanceIdDict, this._queryMetadata);
            }

// get Unions
            var unionNodes = doc.DocumentElement.GetElementsByTagName("Unions");
            if (unionNodes.Count == 1)
            {
                var unionNode = unionNodes.Item(0);
                var unionNodeEl = (XmlElement)unionNode;

                foreach (XmlNode union in unionNodeEl.ChildNodes)
                {
                    this.ReadUnion(union, this._tableInstanceIdDict);

                    // queryMetadata.UnionRelations.Add(urelation);
                }
            }

            // get Unions
            var crossJoinNodes = doc.DocumentElement.GetElementsByTagName("CrossJoins");
            if (crossJoinNodes.Count == 1)
            {
                var xjoinNode = crossJoinNodes.Item(0);
                var xjoinNodeEl = (XmlElement)xjoinNode;

                foreach (XmlNode crossJoin in xjoinNodeEl.ChildNodes)
                {
                    var crossJoinEl = (XmlElement)crossJoin;

                    this.ReadCrossJoin(this._tableInstanceIdDict, crossJoinEl);

                    // queryMetadata.UnionRelations.Add(urelation);
                }
            }

            foreach (var ti in this._queryMetadata.TableInstances)
            {
                ti.ColumnInstances.Sort();
            }

            return this._queryMetadata;
        }

        /// <summary>
        /// Writes the specified query metadata.
        /// </summary>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <returns>
        /// The XML as a string.
        /// </returns>
        public string Write(IQueryMetadata queryMetadata)
        {
            var xmlDoc = new XmlDocument();
            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
            var dataSetNode = xmlDoc.CreateElement("Dataset");
            xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);
            xmlDoc.AppendChild(dataSetNode);

            // column instances
            var columnInstancesNode = xmlDoc.CreateElement("ColumnInstances");
            dataSetNode.AppendChild(columnInstancesNode);
            foreach (var columnInstance in queryMetadata.ColumnInstances)
            {
                var columnInstanceNode = xmlDoc.CreateElement("ColumnInstance");
                columnInstancesNode.AppendChild(columnInstanceNode);

                this.WriteColumnInstance(columnInstanceNode, columnInstance);
            }

            // table instances
            var tableInstancesNode = xmlDoc.CreateElement("TableInstances");
            dataSetNode.AppendChild(tableInstancesNode);
            foreach (var tableInstance in queryMetadata.TableInstances)
            {
                this.WriteTableInstances(xmlDoc, tableInstancesNode, tableInstance);
            }

            // relations
            var relationsNode = xmlDoc.CreateElement("Relations");
            dataSetNode.AppendChild(relationsNode);
            foreach (var relation in queryMetadata.Relations)
            {
                this.WriteRelations(xmlDoc, relationsNode, relation);
            }

            // column table references
            var columnTableReferencesNode = xmlDoc.CreateElement("ColumnTableReferences");
            dataSetNode.AppendChild(columnTableReferencesNode);
            foreach (var columnTableReference in queryMetadata.ColumnTableReferences)
            {
                var columnTableReferenceNode = xmlDoc.CreateElement("ColumnTableReference");
                columnTableReferencesNode.AppendChild(columnTableReferenceNode);

                this.WriteColumnTableReferences(columnTableReferenceNode, columnTableReference);
            }

            // ColumnsMerging
            if (queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance != null && queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance != null)
            {
                var columnsMergingNode = xmlDoc.CreateElement("ColumnsMerging");
                dataSetNode.AppendChild(columnsMergingNode);

                this.WriteColumnsMerging(queryMetadata, columnsMergingNode);

                foreach (var columnInstance in queryMetadata.ColumnsMerging)
                {
                    var columnInstanceNode = xmlDoc.CreateElement("ColumnInstanceRef");
                    columnsMergingNode.AppendChild(columnInstanceNode);

                    this.WriteColumnInstanceRef(columnInstanceNode, columnInstance);
                }
            }

            // Where Details
            var whereDetailsNode = xmlDoc.CreateElement("Constraints");
            dataSetNode.AppendChild(whereDetailsNode);

            foreach (var allowedValues in queryMetadata.Constraints)
            {
                var allowedValuesNode = xmlDoc.CreateElement("AllowedValues");
                whereDetailsNode.AppendChild(allowedValuesNode);

                this.WriteAllowedValues(allowedValuesNode, allowedValues);
                foreach (var value in allowedValues.Values)
                {
                    var valueNode = xmlDoc.CreateElement("Value");
                    this.WriteValue(valueNode, value);
                    allowedValuesNode.AppendChild(valueNode);
                }
            }

            // Table Constraints Details
            var tableConstraintsNode = xmlDoc.CreateElement("TableConstraints");
            dataSetNode.AppendChild(tableConstraintsNode);
            foreach (var constraints in queryMetadata.TableConstraints)
            {
                this.HandleTableConstraints(xmlDoc, constraints, tableConstraintsNode);
            }

            // unions 
            var unionsNode = xmlDoc.CreateElement("Unions");
            dataSetNode.AppendChild(unionsNode);
            foreach (var relation in queryMetadata.UnionRelations)
            {
                var unionRelationNode = xmlDoc.CreateElement("UnionRelation");
                unionsNode.AppendChild(unionRelationNode);

                this.WriteUnion(unionRelationNode, relation);
            }

            // cross-joins 
            var crossNode = xmlDoc.CreateElement("CrossJoins");
            dataSetNode.AppendChild(crossNode);
            foreach (var relation in queryMetadata.CrossJoinRelations)
            {
                var element = xmlDoc.CreateElement("CrossRelation");
                crossNode.AppendChild(element);

                this.WriteCrossJoin(element, relation);
            }

            using (var stream = new StringWriter())
            {
                xmlDoc.Save(stream);
                return stream.ToString();
            }
        }

        /// <summary>
        /// Creates the query metadata.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryMetadata" />
        /// </returns>
        protected virtual IQueryMetadata CreateQueryMetadata()
        {
            return new QueryMetadata();
        }

        /// <summary>
        ///     Creates the relation.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="relationNodeEl">
        ///     The relation node element.
        /// </param>
        /// <param name="tableInstanceIdDict">
        ///     The table instance id dictionary.
        /// </param>
        /// <returns>
        ///     The <see cref="IRelation" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML: Relation with TableInstanceRefFrom not found
        ///     or
        ///     Invalid QueryMetadata XML: Relation with TableInstanceRefTo not found
        /// </exception>
        protected virtual IRelation CreateRelation(IQueryMetadata queryMetadata, XmlElement relationNodeEl, IDictionary<string, ITableInstance> tableInstanceIdDict)
        {
            var relation = queryMetadata.AddRelation();
            string tableInstanceFromId = relationNodeEl.GetAttribute("TableInstanceRefFrom");
            ITableInstance tableInstanceFrom;
            if (!tableInstanceIdDict.TryGetValue(tableInstanceFromId, out tableInstanceFrom))
            {
                throw new Exception("Invalid QueryMetadata XML: Relation with TableInstanceRefFrom not found ");
            }

            relation.TableInstanceFrom = tableInstanceFrom;
            string tableInstanceToId = relationNodeEl.GetAttribute("TableInstanceRefTo");
            ITableInstance tableInstanceTo;
            if (!tableInstanceIdDict.TryGetValue(tableInstanceToId, out tableInstanceTo))
            {
                throw new Exception("Invalid QueryMetadata XML: Relation with TableInstanceRefTo not found ");
            }

            relation.TableInstanceTo = tableInstanceTo;
            return relation;
        }

        /// <summary>
        ///     Reads the allowed values.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="columnInstanceIdDict">
        ///     The column instance id dictionary.
        /// </param>
        /// <param name="allowedValuesNodeEl">
        ///     The allowed values node element.
        /// </param>
        /// <returns>
        ///     The <see cref="IAllowedValues" />
        /// </returns>
        protected virtual IAllowedValues ReadAllowedValues(IQueryMetadata queryMetadata, Dictionary<string, IColumnInstance> columnInstanceIdDict, XmlElement allowedValuesNodeEl)
        {
            var allowedValues = queryMetadata.AddConstraints();

            return this.ReadAllowedValues(columnInstanceIdDict, allowedValuesNodeEl, allowedValues);
        }

        /// <summary>
        ///     Reads the allowed values.
        /// </summary>
        /// <param name="tableInstance">
        ///     The table instance.
        /// </param>
        /// <param name="columnInstanceIdDict">
        ///     The column instance id dictionary.
        /// </param>
        /// <param name="allowedValuesNodeEl">
        ///     The allowed values node element.
        /// </param>
        /// <returns>
        ///     The <see cref="IAllowedValues" />
        /// </returns>
        protected virtual IAllowedValues ReadAllowedValues(ITableInstance tableInstance, Dictionary<string, IColumnInstance> columnInstanceIdDict, XmlElement allowedValuesNodeEl)
        {
            var allowedValues = tableInstance.AddConstraint();

            return this.ReadAllowedValues(columnInstanceIdDict, allowedValuesNodeEl, allowedValues);
        }

        /// <summary>
        ///     Reads the allowed values.
        /// </summary>
        /// <param name="columnInstanceIdDict">
        ///     The column instance id dictionary.
        /// </param>
        /// <param name="allowedValuesNodeEl">
        ///     The allowed values node element.
        /// </param>
        /// <param name="allowedValues">
        ///     The allowed values.
        /// </param>
        /// <returns>
        ///     The <see cref="IAllowedValues" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML:  AllowedValues with ColumnInstanceRef not found
        /// </exception>
        protected virtual IAllowedValues ReadAllowedValues(Dictionary<string, IColumnInstance> columnInstanceIdDict, XmlElement allowedValuesNodeEl, IAllowedValues allowedValues)
        {
            IColumnInstance columnInstance;
            if (!columnInstanceIdDict.TryGetValue(allowedValuesNodeEl.GetAttribute("ColumnInstanceRef"), out columnInstance))
            {
                throw new Exception("Invalid QueryMetadata XML:  AllowedValues with ColumnInstanceRef not found ");
            }

            allowedValues.ColumnInstance = columnInstance;

            // get logicalOperator attribute, default is OR
            var logicalOperator = allowedValuesNodeEl.GetAttribute("logicalOperator");
            if (!string.IsNullOrEmpty(logicalOperator) && Enum.IsDefined(typeof(LogicalOperators), logicalOperator))
            {
                allowedValues.LogicalOperator = (LogicalOperators)Enum.Parse(typeof(LogicalOperators), logicalOperator);
            }

            return allowedValues;
        }

        /// <summary>
        ///     Reads the column instance.
        /// </summary>
        /// <param name="columnInstanceEl">
        ///     The column instance element.
        /// </param>
        /// <param name="tableInstanceColumnIdDict">
        ///     The table instance column id dictionary.
        /// </param>
        /// <param name="columnInstanceSortIndexDict">
        ///     The column instance sort index dictionary.
        /// </param>
        /// <returns>
        ///     The <see cref="IColumnInstance" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid Query Metadata XML: ColumnInstance id not found
        /// </exception>
        protected virtual IColumnInstance ReadColumnInstance(XmlElement columnInstanceEl, Dictionary<string, ITableInstance> tableInstanceColumnIdDict, Dictionary<string, int> columnInstanceSortIndexDict)
        {
            // get table instance object given id
            var id = columnInstanceEl.GetAttribute("ID");
            if (!tableInstanceColumnIdDict.ContainsKey(id))
            {
                throw new Exception("Invalid Query Metadata XML: ColumnInstance id '" + id + "' not found");
            }

            int index = columnInstanceSortIndexDict[id];
            ITableInstance tableInstance = tableInstanceColumnIdDict[id];
            var columnInstance = tableInstance.AddColumnInstance();
            columnInstance.ID = id;
            columnInstance.Index = index;
            columnInstance.Type = columnInstanceEl.GetAttribute("Type");
            columnInstance.Name = columnInstanceEl.GetAttribute("Name");
            if (!string.IsNullOrEmpty(columnInstanceEl.GetAttribute("IsPrimaryKey")))
            {
                columnInstance.IsPrimaryKey = Convert.ToBoolean(columnInstanceEl.GetAttribute("IsPrimaryKey"));
            }

            if (!string.IsNullOrEmpty(columnInstanceEl.GetAttribute("IsReturned")))
            {
                columnInstance.IsReturned = Convert.ToBoolean(columnInstanceEl.GetAttribute("IsReturned"));
            }

            if (!string.IsNullOrEmpty(columnInstanceEl.GetAttribute("IsFixedValue")))
            {
                columnInstance.IsFixedValue = Convert.ToBoolean(columnInstanceEl.GetAttribute("IsFixedValue"));
            }

            columnInstance.FixedValue = columnInstanceEl.GetAttribute("FixedValue");
            return columnInstance;
        }

        /// <summary>
        ///     Reads the column mapping.
        /// </summary>
        /// <param name="relation">The relation.</param>
        /// <param name="columnInstanceIdDict">The column instance id dictionary.</param>
        /// <param name="columnMappingNodeEl">The column mapping node element.</param>
        /// <returns>
        ///     The <see cref="IColumnMapping" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefFrom not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefTo not found
        /// </exception>
        protected virtual IColumnMapping ReadColumnMapping(IRelation relation, Dictionary<string, IColumnInstance> columnInstanceIdDict, XmlElement columnMappingNodeEl)
        {
            var columnMapping = relation.AddColumnMapping();
            IColumnInstance columnInstanceFrom;
            if (!columnInstanceIdDict.TryGetValue(columnMappingNodeEl.GetAttribute("ColumnInstanceRefFrom"), out columnInstanceFrom))
            {
                throw new Exception("Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefFrom not found ");
            }

            columnMapping.ColumnInstanceFrom = columnInstanceFrom;
            IColumnInstance columnInstanceTo;
            if (!columnInstanceIdDict.TryGetValue(columnMappingNodeEl.GetAttribute("ColumnInstanceRefTo"), out columnInstanceTo))
            {
                throw new Exception("Invalid QueryMetadata XML: ColumnMapping with ColumnInstanceRefTo not found ");
            }

            columnMapping.ColumnInstanceTo = columnInstanceTo;

            return columnMapping;
        }

        /// <summary>
        ///     Reads the column merging.
        /// </summary>
        /// <param name="columnsMergingNodeEl">The columns merging node element.</param>
        /// <param name="columnInstanceIdDict">The column instance id dictionary.</param>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <returns>
        ///     The <see cref="ColumnsMergingCollection" />
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        /// </exception>
        protected virtual ColumnsMergingCollection ReadColumnMerging(XmlElement columnsMergingNodeEl, Dictionary<string, IColumnInstance> columnInstanceIdDict, IQueryMetadata queryMetadata)
        {
            var generatedDimensionColumnInstanceRef = columnsMergingNodeEl.GetAttribute("GeneratedDimensionColumnInstanceRef");
            var generatedMeasureColumnInstanceRef = columnsMergingNodeEl.GetAttribute("GeneratedMeasureColumnInstanceRef");
            if (!string.IsNullOrEmpty(generatedDimensionColumnInstanceRef) && !string.IsNullOrEmpty(generatedMeasureColumnInstanceRef))
            {
                IColumnInstance generatedDimensionColumnInstance;
                if (!columnInstanceIdDict.TryGetValue(generatedDimensionColumnInstanceRef, out generatedDimensionColumnInstance))
                {
                    throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found ");
                }

                queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance = generatedDimensionColumnInstance;

                IColumnInstance generatedMeasureColumnInstance;
                if (!columnInstanceIdDict.TryGetValue(generatedMeasureColumnInstanceRef, out generatedMeasureColumnInstance))
                {
                    throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found ");
                }

                queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance = generatedMeasureColumnInstance;

                foreach (XmlNode columnMergingNode in columnsMergingNodeEl.ChildNodes)
                {
                    // TODO check Node is an Element type
                    var columnMergingNodeEl = (XmlElement)columnMergingNode;

                    IColumnInstance columnInstance;
                    if (!columnInstanceIdDict.TryGetValue(columnMergingNodeEl.GetAttribute("ID"), out columnInstance))
                    {
                        throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found ");
                    }

                    queryMetadata.ColumnsMerging.Add(columnInstance);
                }
            }

            return queryMetadata.ColumnsMerging;
        }

        /// <summary>
        ///     Reads the column table reference.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="columnInstanceIdDict">
        ///     The column instance id dictionary.
        /// </param>
        /// <param name="columnTableReferenceNodeEl">
        ///     The column table reference node element.
        /// </param>
        /// <param name="tableInstanceIdDict">
        ///     The table instance id dictionary.
        /// </param>
        /// <exception cref="System.Exception">
        ///     Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found
        ///     or
        ///     Invalid QueryMetadata XML: ColumnTableReference with TableInstanceRef not found
        /// </exception>
        protected virtual void ReadColumnTableReference(IQueryMetadata queryMetadata, Dictionary<string, IColumnInstance> columnInstanceIdDict, XmlElement columnTableReferenceNodeEl, Dictionary<string, ITableInstance> tableInstanceIdDict)
        {
            var columnTableReference = queryMetadata.AddColumnTableReference();
            IColumnInstance columnInstance;
            if (!columnInstanceIdDict.TryGetValue(columnTableReferenceNodeEl.GetAttribute("ColumnInstanceRef"), out columnInstance))
            {
                throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with ColumnInstanceRef not found ");
            }

            columnTableReference.ColumnInstance = columnInstance;
            ITableInstance tableInstance;
            if (!tableInstanceIdDict.TryGetValue(columnTableReferenceNodeEl.GetAttribute("TableInstanceRef"), out tableInstance))
            {
                throw new Exception("Invalid QueryMetadata XML: ColumnTableReference with TableInstanceRef not found ");
            }

            columnTableReference.TableInstance = tableInstance;
        }

        /// <summary>
        ///     Reads the cross join.
        /// </summary>
        /// <param name="tableInstanceIdDict">
        ///     The table instance id dictionary.
        /// </param>
        /// <param name="crossJoinEl">
        ///     The cross join element.
        /// </param>
        protected virtual void ReadCrossJoin(Dictionary<string, ITableInstance> tableInstanceIdDict, XmlElement crossJoinEl)
        {
            var tinFrom = tableInstanceIdDict[crossJoinEl.GetAttribute("TableInstanceRefFrom")];
            var tinTo = tableInstanceIdDict[crossJoinEl.GetAttribute("TableInstanceRefTo")];

            tinFrom.AddCrossJoinRelation(tinTo);
        }

        /// <summary>
        ///     Reads the table instance.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="tableInstanceEl">
        ///     The table instance element.
        /// </param>
        /// <returns>
        ///     The <see cref="ITableInstance" />
        /// </returns>
        protected virtual ITableInstance ReadTableInstance(IQueryMetadata queryMetadata, XmlElement tableInstanceEl)
        {
            var tableInstance = queryMetadata.AddTableInstance();
            tableInstance.Name = tableInstanceEl.GetAttribute("Name");
            tableInstance.ID = tableInstanceEl.GetAttribute("ID");
            if (!string.IsNullOrEmpty(tableInstanceEl.GetAttribute("XCoordinate")))
            {
                tableInstance.XCoordinate = Convert.ToInt32(tableInstanceEl.GetAttribute("XCoordinate"));
            }

            if (!string.IsNullOrEmpty(tableInstanceEl.GetAttribute("YCoordinate")))
            {
                tableInstance.YCoordinate = Convert.ToInt32(tableInstanceEl.GetAttribute("YCoordinate"));
            }

            return tableInstance;
        }

        /// <summary>
        ///     Reads the union.
        /// </summary>
        /// <param name="union">
        ///     The union.
        /// </param>
        /// <param name="tableInstanceIdDict">
        ///     The table instance id dictionary.
        /// </param>
        protected virtual void ReadUnion(XmlNode union, Dictionary<string, ITableInstance> tableInstanceIdDict)
        {
            var unionEl = (XmlElement)union;

            var tinFrom = tableInstanceIdDict[unionEl.GetAttribute("TableInstanceRefFrom")];
            var tinTo = tableInstanceIdDict[unionEl.GetAttribute("TableInstanceRefTo")];

            tinFrom.AddUnionRelation(tinTo);
        }

        /// <summary>
        ///     Reads the value.
        /// </summary>
        /// <param name="valueEl">
        ///     The value element.
        /// </param>
        /// <param name="allowedValues">
        ///     The allowed values.
        /// </param>
        protected virtual void ReadValue(XmlElement valueEl, IAllowedValues allowedValues)
        {
            // get the operator if it is set or default to "="
            var op = valueEl.GetAttribute("operator");
            if (string.IsNullOrEmpty(op) || op.Trim().Length == 0)
            {
                // TODO check if the operator is valid
                op = Operators.Get(Operators.OperatorIndex.Equals);
            }

            allowedValues.Values.Add(new Value(valueEl.InnerText, op));
        }

        /// <summary>
        ///     Writes the allowed values.
        /// </summary>
        /// <param name="allowedValuesNode">
        ///     The allowed values node.
        /// </param>
        /// <param name="allowedValues">
        ///     The allowed values.
        /// </param>
        protected virtual void WriteAllowedValues(XmlElement allowedValuesNode, IAllowedValues allowedValues)
        {
            allowedValuesNode.SetAttribute("ColumnInstanceRef", allowedValues.ColumnInstance.ID);
            allowedValuesNode.SetAttribute("logicalOperator", allowedValues.LogicalOperator.ToString());
        }

        /// <summary>
        ///     Writes the column instance.
        /// </summary>
        /// <param name="columnInstanceNode">
        ///     The column instance node.
        /// </param>
        /// <param name="columnInstance">
        ///     The column instance.
        /// </param>
        protected virtual void WriteColumnInstance(XmlElement columnInstanceNode, IColumnInstance columnInstance)
        {
            columnInstanceNode.SetAttribute("ID", columnInstance.ID);
            columnInstanceNode.SetAttribute("Type", columnInstance.Type);
            columnInstanceNode.SetAttribute("Name", columnInstance.Name);
            columnInstanceNode.SetAttribute("IsPrimaryKey", columnInstance.IsPrimaryKey.ToString());
            columnInstanceNode.SetAttribute("IsReturned", columnInstance.IsReturned.ToString());

            columnInstanceNode.SetAttribute("IsFixedValue", columnInstance.IsFixedValue.ToString());
            columnInstanceNode.SetAttribute("FixedValue", columnInstance.FixedValue);
        }

        /// <summary>
        ///     Writes the column instance ref.
        /// </summary>
        /// <param name="columnInstanceNode">
        ///     The column instance node.
        /// </param>
        /// <param name="columnInstance">
        ///     The column instance.
        /// </param>
        protected virtual void WriteColumnInstanceRef(XmlElement columnInstanceNode, IColumnInstance columnInstance)
        {
            columnInstanceNode.SetAttribute("ID", columnInstance.ID);
        }

        /// <summary>
        ///     Writes the column instance ref.
        /// </summary>
        /// <param name="columnInstanceRefNode">
        ///     The column instance ref node.
        /// </param>
        /// <param name="columnInstance">
        ///     The column instance.
        /// </param>
        /// <param name="i">
        ///     The i.
        /// </param>
        protected virtual void WriteColumnInstanceRef(XmlElement columnInstanceRefNode, IColumnInstance columnInstance, int i)
        {
            columnInstanceRefNode.SetAttribute("ID", columnInstance.ID);

            columnInstanceRefNode.SetAttribute("Index", i.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        ///     Writes the column mapping.
        /// </summary>
        /// <param name="columnMappingNode">
        ///     The column mapping node.
        /// </param>
        /// <param name="columnMapping">
        ///     The column mapping.
        /// </param>
        protected virtual void WriteColumnMapping(XmlElement columnMappingNode, IColumnMapping columnMapping)
        {
            columnMappingNode.SetAttribute("ColumnInstanceRefFrom", columnMapping.ColumnInstanceFrom.ID);
            columnMappingNode.SetAttribute("ColumnInstanceRefTo", columnMapping.ColumnInstanceTo.ID);
        }

        /// <summary>
        ///     Writes the columns merging.
        /// </summary>
        /// <param name="queryMetadata">
        ///     The query metadata.
        /// </param>
        /// <param name="columnsMergingNode">
        ///     The columns merging node.
        /// </param>
        protected virtual void WriteColumnsMerging(IQueryMetadata queryMetadata, XmlElement columnsMergingNode)
        {
            columnsMergingNode.SetAttribute("GeneratedMeasureColumnInstanceRef", queryMetadata.ColumnsMerging.GeneratedMeasureColumnInstance.ID);
            columnsMergingNode.SetAttribute("GeneratedDimensionColumnInstanceRef", queryMetadata.ColumnsMerging.GeneratedDimensionColumnInstance.ID);
        }

        /// <summary>
        ///     Writes the column table references.
        /// </summary>
        /// <param name="columnTableReferenceNode">
        ///     The column table reference node.
        /// </param>
        /// <param name="columnTableReference">
        ///     The column table reference.
        /// </param>
        protected virtual void WriteColumnTableReferences(XmlElement columnTableReferenceNode, IColumnTableReference columnTableReference)
        {
            columnTableReferenceNode.SetAttribute("ColumnInstanceRef", columnTableReference.ColumnInstance.ID);
            columnTableReferenceNode.SetAttribute("TableInstanceRef", columnTableReference.TableInstance.ID);
        }

        /// <summary>
        ///     Writes the cross join.
        /// </summary>
        /// <param name="element">
        ///     The element.
        /// </param>
        /// <param name="relation">
        ///     The relation.
        /// </param>
        protected virtual void WriteCrossJoin(XmlElement element, ICrossJoinRelation relation)
        {
            element.SetAttribute("TableInstanceRefFrom", relation.TableInstanceFrom.ID);
            element.SetAttribute("TableInstanceRefTo", relation.TableInstanceTo.ID);
        }

        /// <summary>
        ///     Writes the relation.
        /// </summary>
        /// <param name="relationNode">
        ///     The relation node.
        /// </param>
        /// <param name="relation">
        ///     The relation.
        /// </param>
        protected virtual void WriteRelation(XmlElement relationNode, IRelation relation)
        {
            relationNode.SetAttribute("TableInstanceRefFrom", relation.TableInstanceFrom.ID);
            relationNode.SetAttribute("TableInstanceRefTo", relation.TableInstanceTo.ID);
        }

        /// <summary>
        ///     Writes the table attribute values.
        /// </summary>
        /// <param name="allowedValuesNode">
        ///     The allowed values node.
        /// </param>
        /// <param name="allowedValues">
        ///     The allowed values.
        /// </param>
        protected virtual void WriteTableAttributeValues(XmlElement allowedValuesNode, IAllowedValues allowedValues)
        {
            this.WriteAllowedValues(allowedValuesNode, allowedValues);
        }

        /// <summary>
        ///     Writes the table instance.
        /// </summary>
        /// <param name="tableInstanceNode">
        ///     The table instance node.
        /// </param>
        /// <param name="tableInstance">
        ///     The table instance.
        /// </param>
        protected virtual void WriteTableInstance(XmlElement tableInstanceNode, ITableInstance tableInstance)
        {
            tableInstanceNode.SetAttribute("ID", tableInstance.ID);
            tableInstanceNode.SetAttribute("Name", tableInstance.Name);

            tableInstanceNode.SetAttribute("XCoordinate", tableInstance.XCoordinate.ToString(CultureInfo.InvariantCulture));
            tableInstanceNode.SetAttribute("YCoordinate", tableInstance.YCoordinate.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        ///     Writes the union.
        /// </summary>
        /// <param name="unionRelationNode">
        ///     The union relation node.
        /// </param>
        /// <param name="relation">
        ///     The relation.
        /// </param>
        protected virtual void WriteUnion(XmlElement unionRelationNode, IUnionRelation relation)
        {
            unionRelationNode.SetAttribute("TableInstanceRefFrom", relation.TableInstanceFrom.ID);
            unionRelationNode.SetAttribute("TableInstanceRefTo", relation.TableInstanceTo.ID);
        }

        /// <summary>
        ///     Writes the value.
        /// </summary>
        /// <param name="valueNode">
        ///     The value node.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        protected virtual void WriteValue(XmlElement valueNode, Value value)
        {
            valueNode.SetAttribute("operator", value.Operator);
            valueNode.InnerText = value.Val;
        }

        /// <summary>
        /// Gets the table constraints.
        /// </summary>
        /// <param name="tableConstraintsNodes">The table constraints nodes.</param>
        /// <param name="tableInstanceIdDict">The table instance identifier dictionary.</param>
        /// <param name="columnInstanceIdDict">The column instance identifier dictionary.</param>
        /// <param name="queryMetadata">The query metadata.</param>
        private void GetTableConstraints(XmlNodeList tableConstraintsNodes, Dictionary<string, ITableInstance> tableInstanceIdDict, Dictionary<string, IColumnInstance> columnInstanceIdDict, IQueryMetadata queryMetadata)
        {
            var tableConstraintsNode = tableConstraintsNodes.Item(0);
            var tableConstraintsNodeEl = (XmlElement)tableConstraintsNode;

            foreach (XmlNode tableConstraint in tableConstraintsNodeEl.ChildNodes)
            {
                var tableConstraintNodeEl = (XmlElement)tableConstraint;

                var tin = tableInstanceIdDict[tableConstraintNodeEl.GetAttribute("TableInstanceRef")];

                // ConstraintsCollection tableConstraints = new ConstraintsCollection(tin);
                foreach (XmlNode allowedValuesNode in tableConstraintNodeEl.ChildNodes)
                {
                    // TODO check Node is an Element type
                    var allowedValuesNodeEl = allowedValuesNode as XmlElement;
                    if (allowedValuesNodeEl != null)
                    {
                        var allowedValues = this.ReadAllowedValues(tin, columnInstanceIdDict, allowedValuesNodeEl);

                        foreach (XmlNode valueNode in allowedValuesNodeEl.ChildNodes)
                        {
                            var valueEl = valueNode as XmlElement;
                            if (valueEl != null)
                            {
                                this.ReadValue(valueEl, allowedValues);
                            }
                        }
                    }
                }

                queryMetadata.TableConstraints.Add(tin.Constraints);
            }
        }

        /// <summary>
        /// Gets the where details when count is one.
        /// </summary>
        /// <param name="whereDetailsNodes">The where details nodes.</param>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <param name="columnInstanceIdDict">The column instance identifier dictionary.</param>
        private void GetWhereDetailsWhenCountIsOne(XmlNodeList whereDetailsNodes, IQueryMetadata queryMetadata, Dictionary<string, IColumnInstance> columnInstanceIdDict)
        {
            var whereDetailsNode = whereDetailsNodes.Item(0);
            var whereDetailsNodeEl = (XmlElement)whereDetailsNode;

            foreach (XmlNode allowedValuesNode in whereDetailsNodeEl.ChildNodes)
            {
                // TODO check Node is an Element type
                var allowedValuesNodeEl = allowedValuesNode as XmlElement;
                if (allowedValuesNodeEl != null)
                {
                    var allowedValues = this.ReadAllowedValues(queryMetadata, columnInstanceIdDict, allowedValuesNodeEl);

                    foreach (XmlNode valueNode in allowedValuesNodeEl.ChildNodes)
                    {
                        var valueEl = valueNode as XmlElement;
                        if (valueEl != null)
                        {
                            this.ReadValue(valueEl, allowedValues);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles the single table instance node.
        /// </summary>
        /// <param name="tableInstancesNodes">The table instances nodes.</param>
        /// <param name="queryMetadata">The query metadata.</param>
        /// <param name="tableInstanceIdDict">The table instance identifier dictionary.</param>
        /// <param name="tableInstanceColumnIdDict">The table instance column identifier dictionary.</param>
        /// <param name="columnInstanceSortIndexDict">The column instance sort index dictionary.</param>
        private void HandleSingleTableInstanceNode(XmlNodeList tableInstancesNodes, IQueryMetadata queryMetadata, Dictionary<string, ITableInstance> tableInstanceIdDict, Dictionary<string, ITableInstance> tableInstanceColumnIdDict, Dictionary<string, int> columnInstanceSortIndexDict)
        {
            // throw new Exception("Invalid QueryMetadata XML: number TableInstances != 1");
            var tableInstancesNode = tableInstancesNodes.Item(0);
            foreach (XmlNode tableInstnanceNode in tableInstancesNode.ChildNodes)
            {
                // TODO check Node is an Element type
                var tableInstanceEl = (XmlElement)tableInstnanceNode;

                var tableInstance = this.ReadTableInstance(queryMetadata, tableInstanceEl);

                tableInstanceIdDict.Add(tableInstance.ID, tableInstance);

                var i = 0;

                // get TableInstance's ColumnInstanceRefs
                foreach (XmlNode columnInstancRefNode in tableInstnanceNode.FirstChild.ChildNodes)
                {
                    // TODO check Node is an Element type
                    var columnInstancRefEl = (XmlElement)columnInstancRefNode;
                    var id = columnInstancRefEl.GetAttribute("ID");
                    tableInstanceColumnIdDict.Add(id, tableInstance);
                    var index = i;
                    if (!string.IsNullOrEmpty(columnInstancRefEl.GetAttribute("Index")))
                    {
                        index = int.Parse(columnInstancRefEl.GetAttribute("Index"));
                    }

                    columnInstanceSortIndexDict.Add(id, index);
                    i++;
                }
            }
        }

        /// <summary>
        /// Handles the table constraints.
        /// </summary>
        /// <param name="xmlDoc">The XML document.</param>
        /// <param name="constraints">The constraints.</param>
        /// <param name="tableConstraintsNode">The table constraints node.</param>
        private void HandleTableConstraints(XmlDocument xmlDoc, ConstraintsCollection constraints, XmlElement tableConstraintsNode)
        {
            var tableConstraintNode = xmlDoc.CreateElement("TableConstraint");
            tableConstraintNode.SetAttribute("TableInstanceRef", constraints.TableInstance.ID);
            tableConstraintsNode.AppendChild(tableConstraintNode);

            foreach (var allowedValues in constraints)
            {
                var allowedValuesNode = xmlDoc.CreateElement("AllowedValues");
                tableConstraintNode.AppendChild(allowedValuesNode);

                this.WriteTableAttributeValues(allowedValuesNode, allowedValues);
                foreach (var value in allowedValues.Values)
                {
                    var valueNode = xmlDoc.CreateElement("Value");
                    this.WriteValue(valueNode, value);
                    allowedValuesNode.AppendChild(valueNode);
                }
            }
        }

        /// <summary>
        /// Writes the relations.
        /// </summary>
        /// <param name="xmlDoc">The XML document.</param>
        /// <param name="relationsNode">The relations node.</param>
        /// <param name="relation">The relation.</param>
        private void WriteRelations(XmlDocument xmlDoc, XmlElement relationsNode, IRelation relation)
        {
            var relationNode = xmlDoc.CreateElement("Relation");
            relationsNode.AppendChild(relationNode);

            this.WriteRelation(relationNode, relation);

            foreach (var columnMapping in relation.ColumnMappings)
            {
                var columnMappingNode = xmlDoc.CreateElement("ColumnMapping");
                relationNode.AppendChild(columnMappingNode);

                this.WriteColumnMapping(columnMappingNode, columnMapping);
            }
        }

        /// <summary>
        /// Writes the table instances.
        /// </summary>
        /// <param name="xmlDoc">The XML document.</param>
        /// <param name="tableInstancesNode">The table instances node.</param>
        /// <param name="tableInstance">The table instance.</param>
        private void WriteTableInstances(XmlDocument xmlDoc, XmlElement tableInstancesNode, ITableInstance tableInstance)
        {
            var tableInstanceNode = xmlDoc.CreateElement("TableInstance");
            tableInstancesNode.AppendChild(tableInstanceNode);

            this.WriteTableInstance(tableInstanceNode, tableInstance);

            var columnInstanceRefsNode = xmlDoc.CreateElement("ColumnInstanceRefs");
            tableInstanceNode.AppendChild(columnInstanceRefsNode);

            var i = 0;
            foreach (var columnInstance in tableInstance.ColumnInstances)
            {
                var columnInstanceRefNode = xmlDoc.CreateElement("ColumnInstanceRef");
                columnInstanceRefsNode.AppendChild(columnInstanceRefNode);

                this.WriteColumnInstanceRef(columnInstanceRefNode, columnInstance, i);
                i++;
            }
        }
    }
}