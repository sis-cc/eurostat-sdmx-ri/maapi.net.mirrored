﻿// -----------------------------------------------------------------------
// <copyright file="RelationBase.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System;

    /// <summary>
    ///     The relation base.
    /// </summary>
    public abstract class RelationBase : IRelationBase
    {
        /// <summary>
        ///     The _ query metadata.
        /// </summary>
        private readonly IQueryMetadata _queryMetadata;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RelationBase" /> class.
        /// </summary>
        /// <param name="querymetadata">
        ///     The query metadata.
        /// </param>
        protected RelationBase(IQueryMetadata querymetadata)
        {
            if (querymetadata == null)
            {
                throw new ArgumentNullException("querymetadata");
            }

            this._queryMetadata = querymetadata;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RelationBase" /> class.
        /// </summary>
        /// <param name="tableInstance">
        ///     The table Instance.
        /// </param>
        protected RelationBase(ITableInstance tableInstance)
        {
            if (tableInstance == null)
            {
                throw new ArgumentNullException("tableInstance");
            }

            this._queryMetadata = tableInstance.QueryMetadata;
        }

        /// <summary>
        ///     Gets the query metadata.
        /// </summary>
        public IQueryMetadata QueryMetadata
        {
            get
            {
                return this._queryMetadata;
            }
        }

        /// <summary>
        ///     Gets or sets the table instance from.
        /// </summary>
        public abstract ITableInstance TableInstanceFrom { get; set; }

        /// <summary>
        ///     Gets or sets the table instance to.
        /// </summary>
        public abstract ITableInstance TableInstanceTo { get; set; }
    }
}
