﻿// -----------------------------------------------------------------------
// <copyright file="Operators.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------


namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    /// <summary>
    ///     This static class provides the list of supported comparison SQL operators for use
    ///     in WHERE statements
    /// </summary>
    public static class Operators
    {
        /// <summary>
        ///     This array contains all the supported comparison operators.
        /// </summary>
        private static readonly string[] _items = { "=", ">", "<", ">=", "<=", "!=", "like", "in", "not like", "not in" };

        /// <summary>
        ///     The index of the operator
        /// </summary>
        public enum OperatorIndex
        {
            /// <summary>
            ///     The equals.
            /// </summary>
            Equals = 0,

            /// <summary>
            ///     The greater than.
            /// </summary>
            GreaterThan,

            /// <summary>
            ///     The less than.
            /// </summary>
            LessThan,

            /// <summary>
            ///     The greater than or equals to.
            /// </summary>
            GreaterThanOrEqualsTo,

            /// <summary>
            ///     The less than or equals to.
            /// </summary>
            LessThanOrEqualsTo,

            /// <summary>
            ///     The not equals.
            /// </summary>
            NotEquals,

            /// <summary>
            ///     The like.
            /// </summary>
            Like,

            /// <summary>
            ///     The in.
            /// </summary>
            In,

            /// <summary>
            ///     The not like.
            /// </summary>
            NotLike,

            /// <summary>
            ///     The not in.
            /// </summary>
            NotIn
        }

        /// <summary>
        ///     Gets the items.
        /// </summary>
        /// <value>
        ///     The items.
        /// </value>
        public static object Items
        {
            get
            {
                return _items;
            }
        }

        /// <summary>
        ///     Getter for getting the operator with the <see cref="OperatorIndex" />
        /// </summary>
        /// <param name="index">
        ///     The index of the operator
        /// </param>
        /// <returns>
        ///     A string with the comparison operator
        /// </returns>
        public static string Get(OperatorIndex index)
        {
            return _items[(int)index];
        }
    }
}
