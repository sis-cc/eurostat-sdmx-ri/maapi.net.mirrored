// -----------------------------------------------------------------------
// <copyright file="UnionRelationsCollection.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System.ComponentModel;

    /// <summary>
    ///     The union relations collection.
    /// </summary>
    public class UnionRelationsCollection : BindingList<IUnionRelation>
    {
        /// <summary>
        ///     The _ query metadata.
        /// </summary>
        private readonly IQueryMetadata _queryMetadata;

        /// <summary>
        ///     Initializes a new instance of the <see cref="UnionRelationsCollection" /> class.
        /// </summary>
        /// <param name="querymetadata">
        ///     The query metadata.
        /// </param>
        public UnionRelationsCollection(IQueryMetadata querymetadata)
        {
            this._queryMetadata = querymetadata;
        }

        /// <summary>
        ///     Gets the query metadata.
        /// </summary>
        public IQueryMetadata QueryMetadata
        {
            get
            {
                return this._queryMetadata;
            }
        }

        /// <summary>
        ///     Returns the first table of union.
        /// </summary>
        /// <returns>
        ///     The <see cref="Estat.Ma.Model.QueryEditor.TableInstance" />
        /// </returns>
        public ITableInstance GetFirstTableOfUnion()
        {
            foreach (var relation in this.Items)
            {
                if (relation.TableInstanceFrom.HasOnlyUnionRelationFrom())
                {
                    return relation.TableInstanceFrom;
                }
            }

            return null;
        }

        /// <summary>
        ///     The do add.
        /// </summary>
        /// <param name="ur">
        ///     The ur.
        /// </param>
        public void DoAdd(IUnionRelation ur)
        {
            this.Add(ur);
        }
    }
}