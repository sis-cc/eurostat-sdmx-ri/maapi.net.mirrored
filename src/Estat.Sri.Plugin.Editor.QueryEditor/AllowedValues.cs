﻿// -----------------------------------------------------------------------
// <copyright file="AllowedValues.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System.ComponentModel;

    /// <summary>
    ///     The allowed values.
    /// </summary>
    public class AllowedValues : IAllowedValues
    {
        /// <summary>
        ///     The _ query metadata.
        /// </summary>
        private readonly IQueryMetadata _queryMetadata;

        /// <summary>
        ///     The _logical operator.
        /// </summary>
        private LogicalOperators _logicalOperator;

        /// <summary>
        ///     The _ values.
        /// </summary>
        private BindingList<Value> _values;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AllowedValues" /> class.
        /// </summary>
        /// <param name="querymetadata">
        ///     The query metadata.
        /// </param>
        public AllowedValues(IQueryMetadata querymetadata)
        {
            this._queryMetadata = querymetadata;
            this._values = new BindingList<Value>();
            this._logicalOperator = LogicalOperators.OR;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AllowedValues" /> class.
        /// </summary>
        /// <param name="tableInstance">
        ///     The table instance.
        /// </param>
        public AllowedValues(ITableInstance tableInstance)
        {
            this._queryMetadata = tableInstance.QueryMetadata;
            this._values = new BindingList<Value>();
            this._logicalOperator = LogicalOperators.OR;
        }

        /// <summary>
        ///     Gets or sets the column instance.
        /// </summary>
        public IColumnInstance ColumnInstance { get; set; }

        /// <summary>
        /// Gets or sets the logical operator used between the conditions stored in values
        /// </summary>
        public LogicalOperators LogicalOperator
        {
            get
            {
                return this._logicalOperator;
            }

            set
            {
                this._logicalOperator = value;
            }
        }

        /// <summary>
        ///     Gets the query metadata.
        /// </summary>
        public IQueryMetadata QueryMetadata
        {
            get
            {
                return this._queryMetadata;
            }
        }

        /// <summary>
        ///     Gets or sets the values.
        /// </summary>
        public BindingList<Value> Values
        {
            get
            {
                return this._values;
            }

            set
            {
                this._values = value;
            }
        }
    }
}
