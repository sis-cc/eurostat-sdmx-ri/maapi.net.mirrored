﻿// -----------------------------------------------------------------------
// <copyright file="QueryEditorFactory.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------


using Estat.Sri.Mapping.Api.Factory;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Plugin.Editor.QueryEditor.Engine;
using Org.Sdmxsource.Sdmx.Api.Builder;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Mapping.Api.Manager;

namespace Estat.Sri.Plugin.Editor.QueryEditor.Factory
{
    public class QueryEditorFactory : IDataSetEditorFactory
    {
        private readonly List<string> _supportedEditorTypes = new List<string>();
        private readonly IBuilder<Database, DdbConnectionEntity> _databaseBuilder;
        private readonly IEntityRetrieverManager _entityRetreiverManager;

        public QueryEditorFactory(IBuilder<Database, DdbConnectionEntity> databaseBuilder, IEntityRetrieverManager entityRetrieverManager)
        {
            _databaseBuilder = databaseBuilder;
            _entityRetreiverManager = entityRetrieverManager;
            _supportedEditorTypes.Add("Estat.Ma.Ui.QueryEditors.MdiQueryEditorUC, Estat.Ma.Ui.QueryEditors");
        }

        public string EditorType
        {
            get
            {
                return "Estat.Ma.Ui.QueryEditors.MdiQueryEditorUC, Estat.Ma.Ui.QueryEditors";
            }
        }

        public string Name
        {
            get
            {
                return "Query Editor";
            }
        }

        public IDataSetEditorEngine GetEngine(string editorType)
        {
            if (_supportedEditorTypes.Contains(editorType))
            {
                return new QueryEditorEngine(_databaseBuilder, _entityRetreiverManager);
            }

            return null;
        }

        public IList<string> GetSupportedPlugins()
        {
            return _supportedEditorTypes;
        }

        public IDataSetEditorInfo EditorInfo
        {
            get
            {
                return new DataSetEditorInfo(_supportedEditorTypes[0], "Query Editor");
            }
        }
    }
}
