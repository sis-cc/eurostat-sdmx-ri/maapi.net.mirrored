// -----------------------------------------------------------------------
// <copyright file="ColumnInstancesCollection.cs" company="EUROSTAT">
//   Date Created : 2012-08-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    /// <summary>
    ///     The column instances collection.
    /// </summary>
    public class ColumnInstancesCollection : BindingList<IColumnInstance>
    {
        /// <summary>
        ///     The _ query metadata.
        /// </summary>
        private readonly IQueryMetadata _queryMetadata;

        /// <summary>
        ///     The _ table instance.
        /// </summary>
        private readonly ITableInstance _tableInstance;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ColumnInstancesCollection" /> class.
        /// </summary>
        /// <param name="ti">The table instance.</param>
        /// <exception cref="System.Exception">TableInstance does not belong to a QueryMetadata</exception>
        public ColumnInstancesCollection(ITableInstance ti)
        {
            this._tableInstance = ti;
            this._queryMetadata = ti.QueryMetadata;
            if (ti.QueryMetadata == null)
            {
                throw new Exception("TableInstance does not belong to a QueryMetadata");
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ColumnInstancesCollection" /> class.
        /// </summary>
        /// <param name="querymetadata">
        ///     The query metadata.
        /// </param>
        public ColumnInstancesCollection(IQueryMetadata querymetadata)
        {
            this._queryMetadata = querymetadata;
        }

        /// <summary>
        ///     Gets the query metadata.
        /// </summary>
        public IQueryMetadata QueryMetadata
        {
            get
            {
                return this._queryMetadata;
            }
        }

        /// <summary>
        ///     Gets the table instance.
        /// </summary>
        public ITableInstance TableInstance
        {
            get
            {
                return this._tableInstance;
            }
        }

        /// <summary>
        ///     The sort.
        /// </summary>
        public void Sort()
        {
            var thisInstances = new List<IColumnInstance>(this.Items);

            this.Items.Clear();
            thisInstances.Sort((a, b) => a.Index.CompareTo(b.Index));
            foreach (var colInstance in thisInstances)
            {
                this.Items.Add(colInstance);
            }
        }

        /// <summary>
        ///     The do add.
        /// </summary>
        /// <param name="c">
        ///     The c.
        /// </param>
        public void DoAdd(IColumnInstance c)
        {
            this.Add(c);
        }
    }
}