﻿// -----------------------------------------------------------------------
// <copyright file="ITableInstance.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    /// <summary>
    ///     The TableInstance interface.
    /// </summary>
    public interface ITableInstance
    {
        /// <summary>
        ///     Gets the column instances.
        /// </summary>
        ColumnInstancesCollection ColumnInstances { get; }

        /// <summary>
        ///     Gets the constraints.
        /// </summary>
        ConstraintsCollection Constraints { get; }

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        string ID { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        ///     Gets the query metadata.
        /// </summary>
        IQueryMetadata QueryMetadata { get; }

        /// <summary>
        ///     Gets or sets the x coordinate.
        /// </summary>
        int XCoordinate { get; set; }

        /// <summary>
        ///     Gets or sets the y coordinate.
        /// </summary>
        int YCoordinate { get; set; }

        /// <summary>
        ///     Add a new column instance.
        /// </summary>
        /// <returns>
        ///     The <see cref="ColumnInstance" />
        /// </returns>
        IColumnInstance AddColumnInstance();

        /// <summary>
        ///     Add a new column instance.
        /// </summary>
        /// <param name="mergedColumnType">
        ///     Type of the merged column.
        /// </param>
        /// <returns>
        ///     The <see cref="ColumnInstance" />
        /// </returns>
        IColumnInstance AddColumnInstance(MergedColumnType mergedColumnType);

        /// <summary>
        ///     Add a new constraint.
        /// </summary>
        /// <returns>
        ///     The <see cref="AllowedValues" />
        /// </returns>
        IAllowedValues AddConstraint();

        /// <summary>
        ///     Add a new cross join relation.
        /// </summary>
        /// <param name="destinationTableInstance">
        ///     The destination table instance.
        /// </param>
        /// <returns>
        ///     The <see cref="CrossJoinRelation" />
        /// </returns>
        ICrossJoinRelation AddCrossJoinRelation(ITableInstance destinationTableInstance);

        /// <summary>
        ///     Add a new fixed column instance.
        /// </summary>
        /// <param name="name">
        ///     The name.
        /// </param>
        /// <param name="fixedValue">
        ///     The fixed value.
        /// </param>
        /// <returns>
        ///     The <see cref="ColumnInstance" />
        /// </returns>
        IColumnInstance AddFixedColumnInstance(string name, string fixedValue);

        /// <summary>
        ///     Add a new union relation.
        /// </summary>
        /// <param name="destinationTableInstance">
        ///     The destination table instance.
        /// </param>
        /// <returns>
        ///     The <see cref="UnionRelation" />
        /// </returns>
        IUnionRelation AddUnionRelation(ITableInstance destinationTableInstance);

        /// <summary>
        ///     Returns the next table in the Union Relation that the table participates in (if it participates in a union
        ///     relation)
        /// </summary>
        /// <returns>
        ///     The <see cref="Estat.Ma.Model.QueryEditor.ITableInstance" />.
        /// </returns>
        ITableInstance GetNextUnionTable();

        /// <summary>
        ///     Returns true if the table participates in an Inner Join  Relation
        /// </summary>
        /// <returns>
        ///     true if the table participates in a Inner-Join Relation; otherwise false
        /// </returns>
        bool HasColumnTableRelation();

        /// <summary>
        ///     Returns true if the table participates in a Cross-Join Relation
        /// </summary>
        /// <returns>
        ///     true if the table participates in a Cross-Join Relation; otherwise false
        /// </returns>
        bool HasCrossJoinRelation();

        /// <summary>
        ///     Returns true if the table participates in an Inner Join  Relation
        /// </summary>
        /// <returns>
        ///     true if the table participates in a Inner-Join Relation; otherwise false
        /// </returns>
        bool HasInnerJoinRelation();

        /// <summary>
        ///     Returns true only if the table participates in a Union Relation and NO OTHER Relations (i.e. ColumnTableReference,
        ///     Join Relation)
        ///     AND is the FIRST table in this Union Relation that it participates in
        /// </summary>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        bool HasOnlyUnionRelationFrom();

        /// <summary>
        ///     Returns true only if the table participates in a Union Relation and NO OTHER Relations (i.e. ColumnTableReference,
        ///     Join Relation)
        ///     AND is the LAST table in this Union Relation that it participates in
        /// </summary>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        bool HasOnlyUnionRelationTo();

        /// <summary>
        ///     The has relation of any type.
        /// </summary>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        bool HasRelationOfAnyType();

        /// <summary>
        ///     Returns true if the table participates in a Union Relation
        /// </summary>
        /// <returns>
        ///     true if the table participates in a Union; otherwise false
        /// </returns>
        bool HasUnionRelation();

        /// <summary>
        ///     The remove all references.
        /// </summary>
        void RemoveAllReferences();

        /// <summary>
        ///     The selected columns match columns of.
        /// </summary>
        /// <param name="to">
        ///     The to.
        /// </param>
        /// <returns>
        ///     The System.Boolean.
        /// </returns>
        bool SelectedColumnsMatchColumnsOf(ITableInstance to);
    }
}