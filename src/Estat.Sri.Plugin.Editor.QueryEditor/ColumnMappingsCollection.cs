// -----------------------------------------------------------------------
// <copyright file="ColumnMappingsCollection.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Plugin.Editor.QueryEditor
{
    using System.ComponentModel;

    /// <summary>
    ///     The column mappings collection.
    /// </summary>
    public class ColumnMappingsCollection : BindingList<IColumnMapping>
    {
        /// <summary>
        ///     The _ relation.
        /// </summary>
        private readonly IRelation _relation;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ColumnMappingsCollection" /> class.
        /// </summary>
        /// <param name="relation">
        ///     The relation.
        /// </param>
        public ColumnMappingsCollection(IRelation relation)
        {
            this._relation = relation;
        }

        /// <summary>
        ///     Gets the relation.
        /// </summary>
        public IRelation Relation
        {
            get
            {
                return this._relation;
            }
        }

        /// <summary>
        ///     The do add.
        /// </summary>
        /// <param name="c">
        ///     The c.
        /// </param>
        public void DoAdd(IColumnMapping c)
        {
            this.Add(c);
        }
    }
}