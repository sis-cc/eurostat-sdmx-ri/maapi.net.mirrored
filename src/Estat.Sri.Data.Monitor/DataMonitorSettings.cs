// -----------------------------------------------------------------------
// <copyright file="DataMonitorSettings.cs" company="EUROSTAT">
//   Date Created : 2017-11-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Data.Monitor
{
    using System;
    using System.Configuration;

    /// <summary>
    /// Data Monitor settings
    /// </summary>
    public class DataMonitorSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataMonitorSettings" /> class.
        /// </summary>
        public DataMonitorSettings()
        {
            BaseResourceUrl = TryGetUriOrNull(nameof(BaseResourceUrl));
            WADLUrl = TryGetUriOrNull(nameof(WADLUrl));
            WSDLUrl = TryGetUriOrNull(nameof(WSDLUrl));
        }

        public Uri BaseResourceUrl
        {
            get; set;
        }

        /// <summary>
        /// Gets the WADL URL.
        /// </summary>
        /// <value>The WADL URL.</value>
        public Uri WADLUrl
        {
            get; set;
        }

        /// <summary>
        /// Gets the WSDL URL.
        /// </summary>
        /// <value>The WSDL URL.</value>
        public Uri WSDLUrl
        {
            get; set;
        }

        /// <summary>
        /// Tries the get URI or null.
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <returns>System.Uri.</returns>
        /// <exception cref="ConfigurationErrorsException"></exception>
        private Uri TryGetUriOrNull(string setting)
        {
            var value = ConfigurationManager.AppSettings[setting];
            if (value == null)
            {
                return null;
            }

            Uri url;
            if (Uri.TryCreate(value, UriKind.Absolute, out url))
            {
                return url;
            }

            throw new ConfigurationErrorsException(string.Format("The setting {0} accepts only a absolute URL. The provided value '{1}' cannot be parsed", setting, value));
        }
    }
}
