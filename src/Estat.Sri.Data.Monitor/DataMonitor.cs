// -----------------------------------------------------------------------
// <copyright file="DataMonitor.cs" company="EUROSTAT">
//   Date Created : 2017-11-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Exception;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using log4net;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.Sdmxsource.Util.Extensions;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.MappingStore.Store.Engine;

namespace Estat.Sri.Data.Monitor
{
    /// <summary>
    /// The Mapping Store based implementation of DataMonitor
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Manager.IDataMonitor" />
    public class DataMonitor : IDataMonitor
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DataMonitor));
        /// <summary>
        /// The data monitor settings
        /// </summary>
        private readonly DataMonitorSettings _dataMonitorSettings;
        private readonly DataSourceRetrieverEngine _dataSourceRetriever;

        /// <summary>
        /// The database
        /// </summary>
        private readonly Database _database;
        /// <summary>
        /// The data registrator
        /// </summary>
        private readonly IDataRegistrator _dataRegistrator;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataMonitor"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="dataRegistrator">The data registrator.</param>
        /// <exception cref="System.ArgumentNullException">
        /// dataRegistrator
        /// or
        /// database
        /// </exception>
        public DataMonitor(Database database, IDataRegistrator dataRegistrator, DataMonitorSettings dataMonitorSettings)
        {
            if (dataRegistrator == null)
            {
                throw new ArgumentNullException(nameof(dataRegistrator));
            }

            if (database == null)
            {
                throw new ArgumentNullException(nameof(database));
            }

            if (dataMonitorSettings == null)
            {
                throw new ArgumentNullException(nameof(dataMonitorSettings));
            }

            this._database = database;
            _log.Debug("DataMonitor initialized");
            _dataRegistrator = dataRegistrator;
            _dataMonitorSettings = dataMonitorSettings;
            _dataSourceRetriever = new DataSourceRetrieverEngine(database);
        }

        /// <summary>
        /// Check if an update occurred
        /// </summary>
        public void Monitor()
        {
            _log.Info("Data Monitor started...");
            //var registrations = this.CheckForDataRegistrations().SelectMany(r => r.Value.Select(x=> x.ImmutableInstance)).ToList();
            var registrations = this.CheckForDataRegistrations();
            _log.InfoFormat("Found {0} new data updates to register", registrations.Count);
            if (registrations.Count > 0)
            {
                _log.InfoFormat("Calling data registrator to register {0} registrations", registrations.Count);
                _dataRegistrator.SendSubmitRegistrationsRequest(registrations);
                _log.Info("Call to data registrator completed");
            }
            else
            {
                _log.Info("No action needed");
            }
        }

        /// <summary>
        /// Checks for data registrations.
        /// </summary>
        /// <returns>List&lt;IRegistrationMutableObject&gt;.</returns>
        private Dictionary<DatasetActionEnumType,IList<IRegistrationMutableObject>> CheckForDataRegistrations()
        {
            Dictionary<DatasetActionEnumType, IList<IRegistrationMutableObject>> registrations = new Dictionary<DatasetActionEnumType, IList<IRegistrationMutableObject>>();
            IList<IRegistrationMutableObject> registrationsReplace = new List<IRegistrationMutableObject>();
            IList<IRegistrationMutableObject> registrationsAppend = new List<IRegistrationMutableObject>();
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.Append("SELECT PA.ART_ID AS PRA_SYSID, PA.AGENCY AS PRA_AGENCY, PA.ID AS PRA_ID, PA.VERSION AS PRA_VERSION, ");
            sqlQuery.Append("RC.AGENCY DF_AGENCY, RC.ID AS DF_ID,");
            sqlQuery.Append("RC.VERSION1 AS DF_VERSION1, RC.VERSION2 AS DF_VERSION2, RC.VERSION3 AS DF_VERSION3, ");
            sqlQuery.Append("DF.DATA_LAST_UPDATE,LAST_REG.LAST_UPDATED ");
            sqlQuery.Append("from ARTEFACT_VIEW PA ");
            sqlQuery.Append("INNER JOIN REF_CHILDREN RC on PA.ART_ID = RC.SOURCE_ARTEFACT ");
            sqlQuery.Append("INNER JOIN DATAFLOW DF ON RC.ART_ID = DF.DF_ID ");
            sqlQuery.Append("LEFT OUTER JOIN ( SELECT REG.ENTITY_ID, MAX(REG.LAST_UPDATED) as LAST_UPDATED ");
            sqlQuery.Append("FROM N_REGISTRATION REG INNER JOIN ENTITY_REF_CHILDREN erc on REG.ENTITY_ID = erc.ENTITY_ID ");
            sqlQuery.Append("where erc.ARTEFACT_TYPE = 'ProvisionAgreement' GROUP BY REG.ENTITY_ID ) ");
            sqlQuery.Append("LAST_REG ON LAST_REG.ENTITY_ID = RC.SOURCE_ARTEFACT ");
            sqlQuery.Append("WHERE PA.ARTEFACT_TYPE = 'ProvisionAgreement' and DF.PRODUCTION = 1 ");
            sqlQuery.Append("AND DF.DATA_LAST_UPDATE IS NOT NULL AND ( DF.DATA_LAST_UPDATE > LAST_REG.LAST_UPDATED ");
            sqlQuery.Append("OR LAST_REG.LAST_UPDATED IS NULL ) ");

            using (var dbCommand = _database.GetSqlStringCommand(sqlQuery.ToString()))
            using (var dataReader = _database.ExecuteReader(dbCommand))
            {
                var dataUpdateIdx = dataReader.GetOrdinal("DATA_LAST_UPDATE");
                while (dataReader.Read())
                {
                    IRegistrationMutableObject registrationObject = new RegistrationMutableCore();
                    registrationObject.Id = DataReaderHelper.GetString(dataReader, "REG_ID");

                    string lastRegLastUpdated = DataReaderHelper.GetString(dataReader, "LAST_UPDATED");

                    string praAgency = DataReaderHelper.GetString(dataReader, "PRA_AGENCY");
                    string praId = DataReaderHelper.GetString(dataReader, "PRA_ID");
                    string praVersion1 = DataReaderHelper.GetString(dataReader, "PRA_VERSION1");
                    string praVersion2 = DataReaderHelper.GetString(dataReader, "PRA_VERSION2");
                    string praVersion3 = DataReaderHelper.GetString(dataReader, "PRA_VERSION3");

                    string praVersion = SRUtilites.formulateVersionFromParticles(praVersion1, praVersion2, praVersion3);

                    IStructureReference provisionAgreementRef = new StructureReferenceImpl(
                    new MaintainableRefObjectImpl(praAgency, praId, praVersion), SdmxStructureEnumType.ProvisionAgreement);

                    registrationObject.ProvisionAgreementRef = provisionAgreementRef;

                    string dfAgency = DataReaderHelper.GetString(dataReader, "DF_AGENCY");
                    string dfId = DataReaderHelper.GetString(dataReader, "DF_ID");
                    string dfVersion1 = DataReaderHelper.GetString(dataReader, "DF_VERSION1");
                    string dfVersion2 = DataReaderHelper.GetString(dataReader, "DF_VERSION2");
                    string dfVersion3 = DataReaderHelper.GetString(dataReader, "DF_VERSION3");

                    string dfVersion = SRUtilites.formulateVersionFromParticles(dfVersion1, dfVersion2, dfVersion3);

                    IStructureReference dataflowRef = new StructureReferenceImpl(
                        new MaintainableRefObjectImpl(dfAgency, dfId, dfVersion), SdmxStructureEnumType.Dataflow);

                    
                    DateTime dataLastUpdate = DataReaderHelper.GetStringDate(dataReader, dataUpdateIdx) ?? DateTime.MinValue;

                   // DataSourceMutableCore dataSourceBean = this.CreateDataSource(dataflowRef, dataLastUpdate);

                    registrationObject.DataSource = this.CreateDataSource(dataflowRef, dataLastUpdate);

                    
                    if (!string.IsNullOrEmpty(lastRegLastUpdated))
                    {
                        registrationsReplace.Add(registrationObject);
                    }
                    else
                    {
                        registrationsAppend.Add(registrationObject);
                    }
                }

                if (registrationsReplace.Count > 0)
                {
                    registrations.Add(DatasetActionEnumType.Replace, registrationsReplace);
                }

                if (registrationsAppend.Count > 0)
                {
                    registrations.Add(DatasetActionEnumType.Append, registrationsAppend);
                }
            }

            return registrations;
        }

        /// <summary>
        /// Creates the data source.
        /// </summary>
        /// <param name="dataflowRef">The dataflow reference.</param>
        /// <param name="dataLastUpdate">The data last update.</param>
        /// <returns>The DataSourceMutableCore.</returns>
        /// <exception cref="ConfigurationMissingException">Error while creating the dataUrl. The baseResourceUrl is not configured.</exception>
        private DataSourceMutableCore CreateDataSource(IStructureReference dataflowRef, DateTime dataLastUpdate)
        {
            DataSourceMutableCore dataSourceMutableCore = new DataSourceMutableCore();
            IEntityQuery query = new EntityQuery() {
                ParentId = new Criteria<string>(OperatorType.Exact, dataflowRef.MaintainableUrn.ToString())
            };
            var dataSource = _dataSourceRetriever.GetEntities(query,Detail.Full).FirstOrDefault();
            if (dataSource != null)
            {
                dataSourceMutableCore.DataUrl = new Uri(dataSource.DataUrl);
                if (!string.IsNullOrWhiteSpace(dataSource.WSDLUrl))
                {
                    dataSourceMutableCore.WSDLUrl = new Uri(dataSource.WSDLUrl);
                }
                if (!string.IsNullOrWhiteSpace(dataSource.WADLUrl))
                {
                    dataSourceMutableCore.WADLUrl = new Uri(dataSource.WADLUrl);
                }
                dataSourceMutableCore.SimpleDatasource = dataSource.IsSimple;
                dataSourceMutableCore.RESTDatasource = dataSource.IsRest;
                dataSourceMutableCore.WebServiceDatasource = dataSource.IsWs;
            }
            else
            {
                dataSourceMutableCore.SimpleDatasource = false;
                dataSourceMutableCore.RESTDatasource = false;
                dataSourceMutableCore.WebServiceDatasource = false;

                string dataflowAgency = dataflowRef.AgencyId;
                string dataflowId = dataflowRef.MaintainableId;
                string dataflowVersion = dataflowRef.Version;

                var baseUrl = _dataMonitorSettings.BaseResourceUrl;

                if (baseUrl == null)
                {
                    throw new ConfigurationMissingException("Error while creating the dataUrl. The baseResourceUrl is not configured.");
                }

                if (_dataMonitorSettings.WADLUrl == null && _dataMonitorSettings.WSDLUrl == null)
                {
                    // TODO fix this Java. This should be done *only* for Simple Data Sources. For the others the Registry should know how to build a query
                    StringBuilder dataUrl = new StringBuilder(baseUrl.AbsolutePath);

                    if (!baseUrl.AbsoluteUri.EndsWith("/", StringComparison.Ordinal))
                    {
                        dataUrl.Append("/");
                    }
                    dataUrl.Append("data/").Append(dataflowAgency).Append(",")
                            .Append(dataflowId).Append(",").Append(dataflowVersion)
                            .Append("?detail=full&updatedAfter=").Append(dataLastUpdate);

                    _log.Debug("> DataUrl = " + dataUrl.ToString());

                    dataSourceMutableCore.DataUrl = new Uri(dataUrl.ToString());
                    dataSourceMutableCore.SimpleDatasource = true;
                }
                else if (_dataMonitorSettings.WSDLUrl != null)
                {
                    dataSourceMutableCore.DataUrl = baseUrl;
                    dataSourceMutableCore.WebServiceDatasource = true;
                    dataSourceMutableCore.WSDLUrl = _dataMonitorSettings.WSDLUrl;
                }
                else if (_dataMonitorSettings.WADLUrl != null)
                {
                    dataSourceMutableCore.DataUrl = baseUrl;
                    dataSourceMutableCore.RESTDatasource = true;
                    dataSourceMutableCore.WADLUrl = _dataMonitorSettings.WADLUrl;
                }
            }
            return dataSourceMutableCore;
        }
    }
}