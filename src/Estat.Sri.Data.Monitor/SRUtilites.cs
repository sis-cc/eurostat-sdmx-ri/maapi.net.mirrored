// -----------------------------------------------------------------------
// <copyright file="SRUtilites.cs" company="EUROSTAT">
//   Date Created : 2017-11-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Data.Monitor
{
    public static class SRUtilites
    {
        public static string formulateVersionFromParticles(string version1, string version2, string version3)
        {
            string version = string.Empty;
            if (version1 != null && version2 != null)
            {
                version = version1 + "." + version2;
                if (!string.IsNullOrWhiteSpace(version3))
                {
                    version += "." + version3;
                }
            }
            return version;
        }
    }
}
