// -----------------------------------------------------------------------
// <copyright file="UpgradeOption.cs" company="EUROSTAT">
//   Date Created : 2017-04-20
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.Tool
{
    using CommandLine;

    [Verb("upgrade", HelpText = "upgrade the specified mapping store")]
    public class UpgradeOption : DatabaseIdentificationCommandLineOptions
    {
        [Option('t', nameof(TargetVersion),Required = false, HelpText = "The target version (optional). ONLY LATEST VERSION SUPPORTED")]
        public string TargetVersion { get; set; }

        [Option('f', "force", Required = false, Default = false, HelpText = "Forces execution of upgrade without asking user confirmation")]
        public bool Force { get; set; }

        [Option('i', "initialize", Required = false, Default = false, HelpText = "Initializes the database if not already a mapping store")]
        public bool Initialize { get; set; }

    }
}