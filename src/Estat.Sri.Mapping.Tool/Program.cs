// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="EUROSTAT">
//   Date Created : 2017-04-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using CommandLine;
using Estat.Sri.Plugin.MySql.Factory;
using Estat.Sri.Plugin.Oracle.Factory;
using Estat.Sri.Plugin.SqlServer.Factory;
using log4net;

namespace Estat.Sri.Mapping.Tool
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    using DryIoc;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Factory;

    using log4net.Config;

    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using System.Configuration;
    using System.Diagnostics;

    /// <summary>
    /// The program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The list of connection identifiers to ignore
        /// </summary>
        private static readonly string[] _ignoreConnectionIdList = { "LocalSqlServer", "OraAspNetConString", "LocalMySqlServer" };

        /// <summary>
        /// Entry point
        /// </summary>
        /// <param name="args">The arguments provided at command line.</param>
        /// <returns>
        /// The exit code.
        /// </returns>
        public static int Main(string[] args)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("app.config"));
            var container = Registration();
            var mappingStoreManager = container.Resolve<IMappingStoreManager>();
            return
                Parser.Default.ParseArguments<ListVerb, InitOption, UpgradeOption>(args)
                    .MapResult<ListVerb, InitOption, UpgradeOption, int>(
                        arg => List(mappingStoreManager, arg),
                        arg => Init(mappingStoreManager, arg),
                        arg => Upgrade(mappingStoreManager, arg),
                        _ => 1);
        }

        /// <summary>
        /// Perform the registrations to the container
        /// </summary>
        /// <returns>The <c>DryIOC</c> container.</returns>
        private static Container Registration()
        {
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["RetrieverFactory"]);
            var container =
                new Container(
                    rules =>
                    rules.With(FactoryMethod.ConstructorWithResolvableArguments).WithoutThrowOnRegisteringDisposableTransient());
            var assemblies = new List<Assembly>(new[] { typeof(IDatabaseProviderManager).Assembly, typeof(DatabaseManager).Assembly, typeof(MysqlDatabaseProviderFactory).Assembly, typeof(OracleDatabaseProviderFactory).Assembly, typeof(SqlServerDatabaseProviderFactory).Assembly });
            container.RegisterMany(assemblies, type => !typeof(IEntity).IsAssignableFrom(type));
            container.Register<IStructureParsingManager, StructureParsingManager>();
            container.Register<IRetrievalEngineContainerFactory, RetrievalEngineContainerFactory>();
            container.Unregister<IEntityAuthorizationManager>();

            return container;
        }

        /// <summary>
        /// Gets the current directory.
        /// </summary>
        /// <returns>The current directory</returns>
        private static string GetCurrentDirectory()
        {
            var fileName = typeof(Program).Assembly.Location;
            return new FileInfo(fileName).DirectoryName;
        }

        /// <summary>
        /// List the available databases
        /// </summary>
        /// <param name="mappingStoreManager">
        /// The mapping store manager.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private static int List(IMappingStoreManager mappingStoreManager, ListVerb option)
        {
            var columnsHeader = "Database identification\tCurrent Version\tAvailable Versions\tComment";
            if (!string.IsNullOrWhiteSpace(option.StoreId))
            {
                columnsHeader.Insert(0, "Database Identification\t");
            }
            Console.WriteLine(columnsHeader);

            if (!string.IsNullOrWhiteSpace(option.ConnectionString) && !string.IsNullOrWhiteSpace(option.ProviderName))
            {
                DisplayMappingStoreInformation(mappingStoreManager, option);

            }
            else
            {
                var retrieveStoreIds = mappingStoreManager.RetrieveStoreIds().Except(_ignoreConnectionIdList);
                foreach (var retrieveStoreId in retrieveStoreIds)
                {
                    option.StoreId = retrieveStoreId;
                    DisplayMappingStoreInformation(mappingStoreManager, option);
                }
            }

            return 0;
        }

        private static void DisplayMappingStoreInformation(IMappingStoreManager mappingStoreManager, DatabaseIdentificationCommandLineOptions databaseIdentificationOptions)
        {
            var engineByStoreId = mappingStoreManager.GetEngineByStoreId(databaseIdentificationOptions.StoreId);
            if (engineByStoreId == null)
            {
                return;
            }
            try
            {
                string version;
                string upgradeVersions;
                IActionResult testResult = engineByStoreId.TestConnection(databaseIdentificationOptions);
                if (testResult.Status == StatusType.Error)
                {
                    version = "Not accessible";
                    upgradeVersions = "Reason:" + string.Join(',', testResult.Messages);
                }
                else
                {
                    version = engineByStoreId.RetrieveVersion(databaseIdentificationOptions)?.ToString(2) ?? "Not a Mapping Store";
                    upgradeVersions = VersionsToString(engineByStoreId.GetAvailableVersions(databaseIdentificationOptions));
                }

                var information = $"{databaseIdentificationOptions.StoreId}\t{version}\t{upgradeVersions}";
                Console.WriteLine(information);
            }
            catch (DbException)
            {
                PrintInaccessibleEntry(databaseIdentificationOptions.StoreId);
            }
        }

        /// <summary>
        /// Prints the inaccessible entry.
        /// </summary>
        /// <param name="retrieveStoreId">The retrieve store identifier.</param>
        private static void PrintInaccessibleEntry(string retrieveStoreId)
        {
            Console.WriteLine($"{retrieveStoreId}\tN/A\tN/A\tNot accessible");
        }

        /// <summary>
        /// Initializes the specified mapping store manager.
        /// </summary>
        /// <param name="mappingStoreManager">The mapping store manager.</param>
        /// <param name="option">The option.</param>
        /// <returns>
        /// The exit code. 0 is success; otherwise an error
        /// </returns>
        private static int Init(IMappingStoreManager mappingStoreManager, InitOption option)
        {
            var engine = mappingStoreManager.GetEngineByStoreId(option.StoreId);
            if (engine == null)
            {
                throw new ArgumentNullException(nameof(engine));
            }
            IActionResult testResult = engine.TestConnection(option);
            if (testResult.Status == StatusType.Error)
            {
                Console.WriteLine($"Could not connect to database with identification : {option.GetIdentification()}");
                Console.WriteLine($"{testResult.Status} : " + string.Join(',', testResult.Messages));
                return -1;
            }

            Console.WriteLine(
                "WARNING: This will re-initialize the mapping store with identification {0} and remove all configuration from it",
                option.GetIdentification());

            if (!option.Force)
            {
                Console.Write("Are you sure you want to continue ? [y/N]");
                var info = Console.ReadKey();
                if (info.Key != ConsoleKey.Y)
                {
                    return 1;
                }
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("Force option set, continuing without user confirmation.");
            }

            Console.WriteLine("Initializing please wait...");
            var result = engine.Initialize(option);

            Console.WriteLine("Status : {0}", result.Status);
            if (result.Status == StatusType.Success)
            {
                return 0;
            }

            Console.WriteLine("ErrorCode : {0}", result.ErrorCode);
            Console.WriteLine("Messages:");
            foreach (var message in result.Messages)
            {
                Console.WriteLine(message);
            }

            return 1;
        }

        /// <summary>
        /// The upgrade.
        /// </summary>
        /// <param name="mappingStoreManager">
        /// The mapping store manager.
        /// </param>
        /// <param name="option">
        /// The option.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private static int Upgrade(IMappingStoreManager mappingStoreManager, UpgradeOption option)
        {
            var engine = mappingStoreManager.GetEngineByStoreId(option.StoreId);
            if (engine == null)
            {
                Console.WriteLine($"Could not find a connection string setting with identification : {option.GetIdentification()}");
                return -1;
            }
            IActionResult testResult = engine.TestConnection(option);
            if (testResult.Status == StatusType.Error)
            {
                Console.WriteLine($"Could not connect to the connection string setting with identification : {option.GetIdentification()}");
                Console.WriteLine($"{testResult.Status} : " + string.Join(',', testResult.Messages));
                return -1;
            }

            var availableVersions = engine.GetAvailableVersions(option);
            var availableVersion = new List<Version>(availableVersions);
            Version targetVersion = null;
            if (!availableVersions.Any())
            {
                Console.WriteLine("No available versions found to upgrade to");
                return 0;
            }
            if (!string.IsNullOrWhiteSpace(option.TargetVersion))
            {
                targetVersion = option.TargetVersion == "latest" ? availableVersion.Last() : new Version(option.TargetVersion);
            }
            else
            {
                targetVersion = availableVersion.Last();
            }

            if (!availableVersion.Contains(targetVersion))
            {
                Console.WriteLine(
                    "Target version {0} does not exist. Available versions to upgrade : {1}",
                    targetVersion.ToString(2),
                    VersionsToString(availableVersion));
                return 0;
            }

            var result = UpgradeOrInitialize(option,engine,targetVersion);
            
            if (result == null || result.Status == StatusType.Success)
            {
                return 0;
            }

            Console.WriteLine("ErrorCode : {0}", result.ErrorCode);
            Console.WriteLine("Messages:");
            foreach (var message in result.Messages)
            {
                Console.WriteLine(message);
            }

            return 1;
        }

        private static IActionResult UpgradeOrInitialize(UpgradeOption option, Api.Engine.IMappingStoreEngine engine, Version targetVersion)
        {
            var currentVersion = engine.RetrieveVersion(option);

            if (currentVersion == null)
            {
                if (option.Initialize)
                {
                    if (!CheckForConfirmation(option, targetVersion, "initialize"))
                    {
                        return null;
                    }
                    return engine.Initialize(option);
                }
                else
                {
                    return new ActionResult("500", StatusType.Error, $"database identified with { option.GetIdentification() } is not a mapping store");
                }
            }

            if (!CheckForConfirmation(option, targetVersion, "upgrade"))
            {
                return null;
            }

            
            return engine.Upgrade(option, targetVersion, option.Initialize);
        }

        private static bool CheckForConfirmation(UpgradeOption option, Version targetVersion, string action)
        {
            var confirmation = false;
            Console.WriteLine(
                "\nWARNING: This will {2} Mapping Store with identification : {0} to version {1}",
                option.GetIdentification(), targetVersion.ToString(2),action);

            if (!option.Force)
            {
                Console.Write("Are you sure you want to continue ? [y/n]");
                var info = Console.ReadKey();
                if (info.Key == ConsoleKey.Y)
                {
                    confirmation = true;
                }
            }
            else
            {
                Console.WriteLine("\nForce option set, continuing without user confirmation.");
                confirmation = true;
            }

            if (confirmation)
            {
                Console.WriteLine($"\n{action.TrimEnd('e')}ing please wait...");
            }
            return confirmation;
        }

        /// <summary>
        /// The versions to string.
        /// </summary>
        /// <param name="availableVersion">
        /// The available version.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string VersionsToString(IEnumerable<Version> availableVersion)
        {
            return string.Join(", ", availableVersion.Select(version => version.ToString(2)));
        }
    }
}