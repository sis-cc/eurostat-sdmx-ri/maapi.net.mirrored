// -----------------------------------------------------------------------
// <copyright file="CustomQueryEditorEngine.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------


using Estat.Sri.Mapping.Api.Engine;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Plugin.Editor.CustomQueryEditor.Engine
{
    public class CustomQueryEditorEngine : IDataSetEditorEngine
    {
        /// <inheritdoc/>
        public CustomQueryEditorEngine() { }

        /// <inheritdoc/>
        public string GenerateSqlQuery(DatasetEntity datasetEntity)
        {
            return datasetEntity.Query;
        }

        /// <inheritdoc/>
        public object DeserializeExtraData(string extraData)
        {
            return null;
        }

        /// <inheritdoc/>
        public IList<string> GetSupportedDdbPlugins()
        {
            return Properties.Settings.Default.SupportedProviders.Cast<string>().ToList();
        }

        /// <inheritdoc/>
        public string Migrate(DatasetEntity datasetEntity)
        {
            // no conversion is needed here
            return datasetEntity?.JSONQuery;
        }
    }
}
