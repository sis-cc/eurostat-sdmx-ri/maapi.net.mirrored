// -----------------------------------------------------------------------
// <copyright file="SriPrincipal.cs" company="EUROSTAT">
//   Date Created : 2018-2-28
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Security.Model
{
    public class SriPrincipal : IPrincipal
    {
        /// <summary>
        /// Hard code for now
        /// </summary>
        private const string AdminRole = "CanModifyStoreSettings";
        private readonly HashSet<string> _accessRules;

        private readonly HashSet<string> _mappingStore;

        private readonly string _scope;

        private readonly IIdentity _identity;

        /// <summary>
        /// Initializes a new instance of the <see cref="SriPrincipal"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="scope">The scope.</param>
        /// <param name="identity">The identity.</param>
        /// <exception cref="ArgumentNullException">user is <c>null</c></exception>
        public SriPrincipal(User user, string scope, IIdentity identity)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            _identity = identity;
            _scope = scope;
            if (string.IsNullOrWhiteSpace(_scope))
            {
                _scope = user.DefaultMappingStore?.Name;
            }

            _accessRules = new HashSet<string>(user.AccessRules.Select(x => x.Name), StringComparer.Ordinal);
            _mappingStore = new HashSet<string>(user.MappingStores.Select(x => x.Name), StringComparer.Ordinal);

            if (!_mappingStore.Contains(_scope))
            {
                _scope = null;
            }
        }

        /// <summary>
        /// Gets the identity of the current principal.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Security.Principal.IIdentity"/> object associated with the current principal.
        /// </returns>
        public IIdentity Identity => _identity;

        /// <summary>
        /// Gets the scope. In essence this means the Mapping Store id requested by the user or the default store id.
        /// </summary>
        /// <value>The scope, i.e. The mapping store id; otherwise null.</value>
        public string Scope => _scope;

        /// <summary>
        /// Determines whether the current principal belongs to the specified role.
        /// </summary>
        /// <returns>
        /// true if the current principal is a member of the specified role; otherwise, false.
        /// </returns>
        /// <param name="role">The name of the role for which to check membership. </param>
        public bool IsInRole(string role)
        {
            return _accessRules.Contains(role);
        }

        /// <summary>
        /// Determines whether this instance can access the specified scope (Mapping Store ID).
        /// </summary>
        /// <param name="scope">The scope.</param>
        /// <returns><c>true</c> if this instance can access scope the specified scope; otherwise, <c>false</c>.</returns>
        public bool CanAccessScope(string scope)
        {
            if (_mappingStore.Contains("*") || _accessRules.Contains(AdminRole))
            {
                return true;
            }

            return _mappingStore.Contains(scope);
        }
    }
}
