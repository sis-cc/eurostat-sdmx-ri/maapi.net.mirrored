using System;
using System.Collections.Generic;
using System.Text;

namespace Estat.Sri.Security.Model
{
    /// <summary>
    /// 
    /// </summary>
    public static class AuthDbConstants
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public static string Id => "authdb";
    }
}
