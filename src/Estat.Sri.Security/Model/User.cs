// -----------------------------------------------------------------------
// <copyright file="User.cs" company="EUROSTAT">
//   Date Created : 2018-2-21
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Security.Model
{
    /// <summary>
    /// Model class User.
    /// </summary>
    public class User
    {
        private List<MappingStoreInfo> _mappingStores = new List<MappingStoreInfo>();
        private List<AccessRule> _accessRules = new List<AccessRule>();

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the default mapping store.
        /// </summary>
        /// <value>The default mapping store.</value>
        public MappingStoreInfo DefaultMappingStore { get; set; }

        /// <summary>
        /// Gets the mapping stores.
        /// </summary>
        /// <value>The mapping stores.</value>
        public IList<MappingStoreInfo> MappingStores => _mappingStores;

        /// <summary>
        /// Gets the access rules.
        /// </summary>
        /// <value>The access rules.</value>
        public IList<AccessRule> AccessRules => _accessRules;

        /// <summary>
        /// create a new instance of this user.
        /// </summary>
        /// <returns>The cloned <see cref="User"/></returns>
        public User DeepClone()
        {
            User user = new User();
            user.Name = this.Name;
            if (DefaultMappingStore != null) {
                user.DefaultMappingStore = new MappingStoreInfo() { Name = DefaultMappingStore.Name };
            }

            user._accessRules.AddRange(this._accessRules.Select(x => new AccessRule { Name = x.Name }));
            user._mappingStores.AddRange(this._mappingStores.Select(x => new MappingStoreInfo { Name = x.Name }));

            return user;
        }
    }
}
