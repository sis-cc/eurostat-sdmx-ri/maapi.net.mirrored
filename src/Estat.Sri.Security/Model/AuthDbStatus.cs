// -----------------------------------------------------------------------
// <copyright file="AuthDbSecurityStore.cs" company="EUROSTAT">
//   Date Created : 2018-3-10
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Security.Model
{
    /// <summary>
    /// The status of the Auth database
    /// </summary>
    public enum AuthDbStatus
    {
        /// <summary>
        /// AuthDB connection configured, accessible and initialized
        /// </summary>
        ConfiguredAccessibleInitialized = 200,
        /// <summary>
        /// AuthDB connection configured, accessible and not initialized
        /// </summary>
        ConfiguredAccessibleNotInitialized = 303,
        /// <summary>
        /// Not configured
        /// </summary>
        NotConfigured = 501,
        /// <summary>
        /// Not accessible
        /// </summary>
        NotAccessible = 503,
        /// <summary>
        /// AuthDB settings stored in writeable file
        /// </summary>
        SettingsStoredWriteableConfiguration = 201,
        /// <summary>
        /// AuthDB already configured
        /// </summary>
        AlreadyConfigured = 403,
    }

}
