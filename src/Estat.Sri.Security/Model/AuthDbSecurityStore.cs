// -----------------------------------------------------------------------
// <copyright file="AuthDbSecurityStore.cs" company="EUROSTAT">
//   Date Created : 2018-3-10
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Security.Model
{
    using System;
    using System.Configuration;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// The Authentication database specific security store
    /// </summary>
    /// <seealso cref="Estat.Sri.Security.Model.ISecurityStore" />
    public class AuthDbSecurityStore : ISecurityStore
    {
        private readonly IConfigurationStoreEngine<ConnectionStringSettings> _configurationStoreEngine = new AppConfigStore(ConfigurationManager.AppSettings["authDbConfigLocation"]);
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthDbSecurityStore"/> class.
        /// </summary>
        private Database _database;


        public AuthDbSecurityStore() : this(AuthDbConstants.Id)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthDbSecurityStore"/> class.
        /// </summary>
        public AuthDbSecurityStore(ConnectionStringSettings connectionString)
        {
            if (connectionString == null)
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            _database = new Database(connectionString);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthDbSecurityStore"/> class.
        /// </summary>
        public AuthDbSecurityStore(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new System.ArgumentException("Must contain a value", nameof(name));
            }

            //check first the writable file
            _database = GetAuthDb(name);
        }

        private Database GetAuthDb(string name = null)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = AuthDbConstants.Id;
            }
            var connectionString = _configurationStoreEngine.GetSettings().ToList().FirstOrDefault(x => x.Name == name);
            if (connectionString == null)
            {
                connectionString = ConfigurationManager.ConnectionStrings[name];
            }
            if (connectionString != null)
            {
                return new Database(connectionString);
            }
            return null;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthDbSecurityStore"/> class.
        /// </summary>
        /// <param name="authDb">The authentication database.</param>
        /// <exception cref="ArgumentNullException">authDb</exception>
        public AuthDbSecurityStore(Database authDb)
        {
            if (authDb == null)
            {
                throw new ArgumentNullException(nameof(authDb));
            }

            _database = authDb;
        }

        /// <summary>
        /// Gets the authentication database.
        /// </summary>
        /// <value>The authentication database.</value>
        public Database AuthDb { 
            get 
            { 
                if(_database == null)
                {
                    _database = GetAuthDb();
                }
                return _database;
            } 
        }
    }

}
