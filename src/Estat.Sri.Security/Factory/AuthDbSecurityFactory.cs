// -----------------------------------------------------------------------
// <copyright file="AuthDbSecurityFactory.cs" company="EUROSTAT">
//   Date Created : 2018-3-10
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Security.Engine;
using Estat.Sri.Security.Model;

namespace Estat.Sri.Security.Factory
{
    /// <summary>
    /// Auth DB implementation of the <see cref="ISecurityStoreFactory"/>
    /// </summary>
    /// <seealso cref="Estat.Sri.Security.ISecurityStoreFactory" />
    public class AuthDbSecurityFactory : ISecurityStoreFactory
    {
        private readonly IDatabaseProviderManager _databaseProviderManager;
        private readonly IMappingStoreManager _mappingStoreManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthDbSecurityFactory"/> class.
        /// </summary>
        /// <param name="databaseProviderManager">The database provider manager.</param>
        /// <param name="mappingStoreManager">The mapping store manager.</param>
        public AuthDbSecurityFactory(IDatabaseProviderManager databaseProviderManager, IMappingStoreManager mappingStoreManager)
        {
            _databaseProviderManager = databaseProviderManager;
            _mappingStoreManager = mappingStoreManager;
        }
        /// <summary>
        /// Gets the persistence engine.
        /// </summary>
        /// <param name="storeType">Type of the store.</param>
        /// <returns>The <see cref="ISecurityPersistenceEngine" /></returns>
        public ISecurityPersistenceEngine GetPersistenceEngine(ISecurityStore storeType)
        {
            var authDbStore = storeType as AuthDbSecurityStore;
            if (authDbStore != null)
            {
                return new SecurityPersistenceEngine(authDbStore.AuthDb);
            }

            return null;
        }

        /// <summary>
        /// Gets the retriever engine.
        /// </summary>
        /// <param name="storeType">Type of the store.</param>
        /// <returns>The <see cref="ISecurityRetrieverEngine" />.</returns>
        public ISecurityRetrieverEngine GetRetrieverEngine(ISecurityStore storeType)
        {
            var authDbStore = storeType as AuthDbSecurityStore;
            if (authDbStore != null)
            {
                return new SecurityRetrieverEngine(authDbStore.AuthDb);
            }

            var xml = storeType as XmlSecurityStore;
            if (xml != null)
            {
                return new SecurityRetrieverFromXml(xml.DataLocation);
            }

            return null;
        }

        /// <summary>
        /// Gets the schema engine.
        /// </summary>
        /// <param name="storeType">Type of the store.</param>
        /// <returns>The <see cref="ISecuritySchemaEngine" /></returns>
        public ISecuritySchemaEngine GetSchemaEngine(ISecurityStore storeType)
        {
            var authDbStore = storeType as AuthDbSecurityStore;
            if (authDbStore != null)
            {
                return new AuthDbSchema(authDbStore.AuthDb,this._databaseProviderManager);
            }

            return null;
        }
    }
}
