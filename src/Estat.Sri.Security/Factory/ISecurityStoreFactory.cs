// -----------------------------------------------------------------------
// <copyright file="ISecurityStoreFactory.cs" company="EUROSTAT">
//   Date Created : 2018-3-10
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------


namespace Estat.Sri.Security
{

    using Estat.Sri.Security.Engine;
    using Estat.Sri.Security.Model;

    public interface ISecurityStoreFactory
    {
        /// <summary>
        /// Gets the retriever engine.
        /// </summary>
        /// <param name="storeType">Type of the store.</param>
        /// <returns>The <see cref="ISecurityRetrieverEngine"/>.</returns>
        ISecurityRetrieverEngine GetRetrieverEngine(ISecurityStore storeType);

        /// <summary>
        /// Gets the persistence engine.
        /// </summary>
        /// <param name="storeType">Type of the store.</param>
        /// <returns>The <see cref="ISecurityPersistenceEngine"/></returns>
        ISecurityPersistenceEngine GetPersistenceEngine(ISecurityStore storeType);

        /// <summary>
        /// Gets the schema engine.
        /// </summary>
        /// <param name="storeType">Type of the store.</param>
        /// <returns>The <see cref="ISecuritySchemaEngine"/></returns>
        ISecuritySchemaEngine GetSchemaEngine(ISecurityStore storeType);
    }
}
