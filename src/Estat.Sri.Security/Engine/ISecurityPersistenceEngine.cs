// -----------------------------------------------------------------------
// <copyright file="ISecurityPersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2018-3-10
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Security.Model;

namespace Estat.Sri.Security.Engine
{
    /// <summary>
    /// This interface implementations are responsible for persisting Security objects
    /// </summary>
    public interface ISecurityPersistenceEngine
    {
        /// <summary>
        /// Adds the specified <paramref name="accessRule"/>.
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        void Add(AccessRule accessRule);

        /// <summary>
        /// Adds the specified <paramref name="mappingStore" />.
        /// </summary>
        /// <param name="mappingStore">The mapping store.</param>
        void Add(MappingStoreInfo mappingStore);

        /// <summary>
        /// Adds the specified <paramref name="user" />.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        void Add(User user, string password);

        /// <summary>
        /// Changes the password of the specified <paramref name="user"/>.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        void ChangePassword(User user, string password);

        /// <summary>
        /// Deletes the specified <paramref name="accessRule"/>.
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        void Delete(AccessRule accessRule);

        /// <summary>
        /// Deletes the specified <paramref name="impliedRule"/> for the specified <paramref name="accessRule" />.
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <param name="impliedRule">The implied rule.</param>
        void Delete(AccessRule accessRule, AccessRule impliedRule);

        /// <summary>
        /// Deletes the specified <paramref name="mappingStore"/>.
        /// </summary>
        /// <param name="mappingStore">The mapping store.</param>
        void Delete(MappingStoreInfo mappingStore);

        /// <summary>
        /// Deletes the specified <paramref name="user"/>.
        /// </summary>
        /// <param name="user">The user.</param>
        void Delete(User user);

        /* Oracle doesn't support ON UPDATE CASCADE
        /// <summary>
        /// Renames the specified <paramref name="rule"/>.
        /// </summary>
        /// <param name="rule">The rule.</param>
        /// <param name="newName">The new name.</param>
        void Rename(AccessRule rule, string newName);

        /// <summary>
        /// Renames the specified <paramref name="mappingStoreInfo"/>.
        /// </summary>
        /// <param name="mappingStoreInfo">The mapping store information.</param>
        /// <param name="newName">The new name.</param>
        void Rename(MappingStoreInfo mappingStoreInfo, string newName);
        */

        /// <summary>
        /// Updates the specified <paramref name="accessRule"/> implied rules.
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <param name="impliedRules">The implied rules.</param>
        void Update(AccessRule accessRule, IEnumerable<AccessRule> impliedRules);

        /// <summary>
        /// Updates the specified <paramref name="updatedUser"/>.
        /// </summary>
        /// <param name="updatedUser">The updated user.</param>
        void Update(User updatedUser);
    }
}