// -----------------------------------------------------------------------
// <copyright file="SecurityRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2018-3-10
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Security.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using Dapper;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Security.Builder;
    using Estat.Sri.Security.Model;

    /// <summary>
    /// Security information retriever class
    /// </summary>
    class SecurityRetrieverEngine : ISecurityRetrieverEngine
    {
        /// <summary>
        /// The get user query
        /// </summary>
        private static readonly string _queryUsers = "SELECT ID, USERNAME, DEFAULT_STORE_ID from SRI_USER";

        /// <summary>
        /// The get specific user query
        /// </summary>
        private static readonly string _querySpecificUser = _queryUsers + " WHERE USERNAME = {0}";
        
        /// <summary>
        /// The get access rules for user
        /// </summary>
        private static readonly string _querySpecificUserPermission = "SELECT RULE_NAME FROM USER_PERMISSION WHERE ID = {0}";
        
        /// <summary>
        /// The get implied rules
        /// </summary>
        private static readonly string _queryImpliedRules = "SELECT RULE_NAME, IMPLIES_RULE_NAME FROM ACCESS_RULE_IMPLICIT";
        
        /// <summary>
        /// The get store for user
        /// </summary>
        private static readonly string _querySpecificUserMappingStore = "SELECT STORE_ID FROM MS_STORE_USER WHERE ID = {0}";

        /// <summary>
        /// The query mapping store
        /// </summary>
        private static readonly string _queryMappingStore = "SELECT STORE_ID FROM MS_STORE";

        /// <summary>
        /// The query specific mapping store if exists
        /// </summary>
        private static readonly string _querySpecificMappingStoreIfExists = "SELECT count(*) FROM MS_STORE WHERE STORE_ID = {0}";

        /// <summary>
        /// Query all access rules
        /// </summary>
        private static readonly string _queryAccessRule = "SELECT RULE_NAME FROM ACCESS_RULE";

        /// <summary>
        /// Query specific Access rule if exists
        /// </summary>
        private static readonly string _querySpecificAccessRuleIfExists = "SELECT count(*) FROM ACCESS_RULE WHERE RULE_NAME = {0}";

        /// <summary>
        /// The query if mapping store is used
        /// </summary>
        private static readonly string _queryIfMappingStoreIsUsed = "SELECT count(*) FROM (SELECT STORE_ID FROM MS_STORE_USER UNION ALL SELECT DEFAULT_STORE_ID FROM SRI_USER) T WHERE T.STORE_ID = {0}";
        /// <summary>
        /// The query if access rule is used
        /// </summary>
        private static readonly string _queryIfAccessRuleIsUsed = "SELECT count(*) FROM USER_PERMISSION WHERE RULE_NAME = {0}";

        /// <summary>
        /// The authentication database
        /// </summary>
        private readonly Database _authDatabase;

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityRetrieverEngine"/> class.
        /// </summary>
        /// <param name="authDatabase">The authentication database.</param>
        public SecurityRetrieverEngine(Database authDatabase)
        {
            this._authDatabase = authDatabase;
        }

        /// <summary>
        /// Checks the existence of the specified <paramref name="mappingStoreInfo"/>
        /// </summary>
        /// <param name="mappingStoreInfo">The mapping store information.</param>
        /// <returns><c>true</c> if there is a <paramref name="mappingStoreInfo"/>, <c>false</c> otherwise.</returns>
        public bool Exists(MappingStoreInfo mappingStoreInfo)
        {
            var format = _querySpecificMappingStoreIfExists;
            var name = mappingStoreInfo.Name;
            return CheckIf(_authDatabase, format, name);
        }

        /// <summary>
        /// Checks if the specified <paramref name="mappingStoreInfo"/> is used
        /// </summary>
        /// <param name="mappingStoreInfo">The mapping store information.</param>
        /// <returns><c>true</c> if there is a <paramref name="mappingStoreInfo"/>, <c>false</c> otherwise.</returns>
        public bool IsUsed(MappingStoreInfo mappingStoreInfo)
        {
            var format = _queryIfMappingStoreIsUsed;
            var name = mappingStoreInfo.Name;
            return CheckIf(_authDatabase, format, name);
        }

        /// <summary>
        /// Gets the mapping stores.
        /// </summary>
        /// <returns>The list of <see cref="MappingStoreInfo" /></returns>
        public IList<MappingStoreInfo> GetMappingStores()
        {
            // TODO DB level paging
            var resources = _authDatabase.Query<dynamic>(_queryMappingStore);

            var returnedList = new List<MappingStoreInfo>();
            foreach (var store in resources)
            {
                var model = new MappingStoreInfo() { Name = (string)store.STORE_ID };

                returnedList.Add(model);
            }

            return returnedList;
        }

        /// <summary>
        /// Checks the existence of the specified <paramref name="accessRule"/>
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <returns><c>true</c> if there is a <paramref name="accessRule"/>, <c>false</c> otherwise.</returns>
        public bool Exists(AccessRule accessRule)
        {
            var format = _querySpecificAccessRuleIfExists;
            var name = accessRule.Name;
            return CheckIf(_authDatabase, format, name);
        }

        /// <summary>
        /// Checks if the specified <paramref name="accessRule"/> is used
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <returns><c>true</c> if there is a <paramref name="accessRule"/>, <c>false</c> otherwise.</returns>
        public bool IsUsed(AccessRule accessRule)
        {
            var format = _queryIfAccessRuleIsUsed;
            var name = accessRule.Name;
            return CheckIf(_authDatabase, format, name);
        }

        /// <summary>
        /// Gets the Access rules
        /// </summary>
        /// <returns>The list of <see cref="AccessRule" /></returns>
        public IList<AccessRule> GetAccessRules()
        {
            // TODO DB level paging
            var resources = _authDatabase.Query<dynamic>(_queryAccessRule);

            var returnedList = new List<AccessRule>();
            foreach (var resource in resources)
            {
                var model = new AccessRule() { Name = (string)resource.RULE_NAME };

                returnedList.Add(model);
            }

            return returnedList;
        }
        
        /// <summary>
        /// Gets the implied access rules for this User.
        /// </summary>
        /// <returns>The list of <see cref="AccessRule" /></returns>
        public ILookup<string, string> GetImpliedAccessRules()
        {
            return GetImpliedRules(_authDatabase);
        }

        /// <summary>
        /// Gets the implied access rules for this User.
        /// </summary>
        /// <returns>The list of <see cref="AccessRule" /></returns>
        public IList<AccessRule> GetFlatImpliedAccessRules(AccessRule accessRule)
        {
            var returnedList = new List<AccessRule>();
            returnedList.Add(accessRule);

            var impliedRules = GetImpliedRules(_authDatabase);
            FilterImpliedRules(returnedList, impliedRules);
            return returnedList;
        }

        /// <summary>
        /// Gets the implied access rules for this User.
        /// </summary>
        /// <returns>The list of <see cref="AccessRule" /></returns>
        public IList<AccessRule> GetFlatImpliedAccessRules(IList<AccessRule> accessRules)
        {
            var returnedList = new List<AccessRule>(accessRules);
            var impliedRules = GetImpliedRules(_authDatabase);
            FilterImpliedRules(returnedList, impliedRules);
            return returnedList;
        }

        /// <summary>
        /// Get the users without permissions. Suitable for listing
        /// </summary>
        /// <returns>The list of <see cref="User" /> without the <see cref="User.AccessRules" /> and <see cref="User.MappingStores" /></returns>
        public IList<User> GetUsers()
        {
            // TODO DB level paging
            var users = _authDatabase.Query<dynamic>(_queryUsers);

            var returnedUsers = new List<User>();
            foreach(var user in users)
            {
                User userModel = BuildUser(user);

                returnedUsers.Add(userModel);
            }

            return returnedUsers;
        }
        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>User.</returns>
        public KeyValuePair<long, User> GetUserWithId(string userName)
        {
            var userNameParamName = _authDatabase.BuildParameterName(nameof(userName));
            var query = string.Format(CultureInfo.InvariantCulture, _querySpecificUser, userNameParamName);
            var result = _authDatabase.Query(query, new { userName = new DbString() { IsAnsi = true, Value = userName } }).FirstOrDefault();
            if (result != null)
            {
                User user = BuildUser(result);
                var id = (long)result.ID;
                var userIdParamName = _authDatabase.BuildParameterName(nameof(id));
                GetAccessRules(_authDatabase, user, id, userIdParamName);

                GetMappingStores(_authDatabase, user, id, userIdParamName);

                return new KeyValuePair<long, User>(id, user);
            }

            return new KeyValuePair<long, User>();
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>User.</returns>
        public User GetUser(string userName)
        {
            return GetUserWithId(userName).Value;
        }

        /// <summary>
        /// Builds the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>User.</returns>
        private static User BuildUser(dynamic user)
        {
            var actualUser = new User();
            actualUser.Name = (string)user.USERNAME;
            if (user.DEFAULT_STORE_ID is string)
            {
                actualUser.DefaultMappingStore = new MappingStoreInfo { Name = Convert.ToString(user.DEFAULT_STORE_ID) };
            }

            return actualUser;
        }

        /// <summary>
        /// Gets the access rules.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="user">The user.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="userIdParamName">Name of the user identifier parameter.</param>
        private static void GetAccessRules(Database database, User user, long id, string userIdParamName)
        {
            var queryUserAccessRules = string.Format(CultureInfo.InvariantCulture, _querySpecificUserPermission, userIdParamName);
            var rules = database.Query(queryUserAccessRules, new { id });
            IList<AccessRule> accessRules = user.AccessRules;
            foreach (var rule in rules)
            {
                AccessRule accessRule = new AccessRule();
                accessRule.Name = rule.RULE_NAME;
                accessRules.Add(accessRule);
            }
        }

        /// <summary>
        /// Gets the implied rules.
        /// </summary>
        /// <param name="accessRules">The access rules.</param>
        private static void FilterImpliedRules(IList<AccessRule> accessRules, ILookup<string, string> impliedRules)
        {
            ImpliedRulesFilter.FilterImpliedRules(accessRules, impliedRules);
        }

        private static ILookup<string, string> GetImpliedRules(Database database)
        {
            return database.Query<dynamic>(_queryImpliedRules).ToLookup(k => (string)k.RULE_NAME, v => (string)v.IMPLIES_RULE_NAME, StringComparer.Ordinal);
        }

        /// <summary>
        /// Gets the mapping stores.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="user">The user.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="userIdParamName">Name of the user identifier parameter.</param>
        private static void GetMappingStores(Database database, User user, long id, string userIdParamName)
        {
            var queryUserMappingStore = string.Format(CultureInfo.InvariantCulture, _querySpecificUserMappingStore, userIdParamName);
            var stores = database.Query(queryUserMappingStore, new { id });
            foreach (var store in stores)
            {
                var mappingStore = new MappingStoreInfo();
                mappingStore.Name = store.STORE_ID;
                user.MappingStores.Add(mappingStore);
            }
        }

        /// <summary>
        /// Checks if <paramref name="ifSqlFormat"/> returns true
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="ifSqlFormat">If SQL format.</param>
        /// <param name="name">The name.</param>
        /// <returns><c>true</c> if <paramref name="ifSqlFormat"/> returns 1, <c>false</c> otherwise.</returns>
        private static bool CheckIf(Database database, string ifSqlFormat, string name)
        {
            var paramName = database.BuildParameterName(nameof(name));

            var query = string.Format(CultureInfo.InvariantCulture, ifSqlFormat, paramName);
            var result = database.ExecuteScalar<long?>(query, new { name = new DbString() { IsAnsi = true, Value = name } });
            if (result != null)
            {
                long count = Convert.ToInt64(result, CultureInfo.InvariantCulture);
                return count > 0;
            }

            return false;
        }
    }
}
