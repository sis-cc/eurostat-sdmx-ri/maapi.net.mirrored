// -----------------------------------------------------------------------
// <copyright file="CollectionExtension.cs" company="EUROSTAT">
//   Date Created : 2018-3-7
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Estat.Sri.Security.Model;

namespace Estat.Sri.Security.Engine
{
    static class CollectionExtension
    {
        /// <summary>
        /// Get a distinct set of names of the specified <paramref name="accessRules"/>.
        /// </summary>
        /// <param name="accessRules">The list of access rules.</param>
        /// <returns>The distinct set of names of the specified <paramref name="accessRules"/>.</returns>
        public static HashSet<string> ToSet(this IEnumerable<AccessRule> accessRules)
        {
            return new HashSet<string>(accessRules.Select(x => x.Name), StringComparer.Ordinal);
        }

        /// <summary>
        /// Get a distinct set of names of the specified <paramref name="mappingStore"/>.
        /// </summary>
        /// <param name="mappingStore">The list of mapping stores.</param>
        /// <returns>The distinct set of names of the specified <paramref name="mappingStore"/>.</returns>
        public static HashSet<string> ToSet(this IEnumerable<MappingStoreInfo> mappingStore)
        {
            return new HashSet<string>(mappingStore.Select(x => x.Name), StringComparer.Ordinal);
        }
    }
}
