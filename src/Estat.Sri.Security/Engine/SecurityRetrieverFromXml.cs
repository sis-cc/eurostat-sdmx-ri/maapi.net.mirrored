// -----------------------------------------------------------------------
// <copyright file="SecurityRetrieverFromXml.cs" company="EUROSTAT">
//   Date Created : 2018-3-11
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Estat.Sri.Security.Model;
using System.IO;
using Estat.Sri.Security.Builder;

namespace Estat.Sri.Security.Engine
{
    /// <summary>
    /// An implementation of <see cref="ISecurityRetrieverEngine"/> that reads from a XML file.
    /// Note Mapping Store support is not complete because it was not needed at least when this was implemented
    /// </summary>
    /// <seealso cref="Estat.Sri.Security.Engine.ISecurityRetrieverEngine" />
    internal class SecurityRetrieverFromXml : ISecurityRetrieverEngine
    {
        /// <summary>
        /// The data location
        /// </summary>
        private readonly Func<Stream> _dataLocation;

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityRetrieverFromXml"/> class.
        /// </summary>
        /// <param name="dataLocation">The data location.</param>
        /// <exception cref="ArgumentNullException">dataLocation</exception>
        public SecurityRetrieverFromXml(Func<Stream> dataLocation)
        {
            if (dataLocation == null)
            {
                throw new ArgumentNullException(nameof(dataLocation));
            }

            _dataLocation = dataLocation;
        }

        /// <summary>
        /// Checks the existence of the specified <paramref name="accessRule" />
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <returns><c>true</c> if there is a <paramref name="accessRule" />, <c>false</c> otherwise.</returns>
        public bool Exists(AccessRule accessRule)
        {
            return GetAccessRules().Any(x => x.Name.Equals(accessRule?.Name));
        }

        /// <summary>
        /// Existses the specified mapping store information.
        /// </summary>
        /// <param name="mappingStoreInfo">The mapping store information.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool Exists(MappingStoreInfo mappingStoreInfo)
        {
            return GetMappingStores().Any(x => x.Name.Equals(mappingStoreInfo?.Name));
        }

        /// <summary>
        /// Gets the access rules.
        /// </summary>
        /// <returns>IList&lt;AccessRule&gt;.</returns>
        public IList<AccessRule> GetAccessRules()
        {
            using (var stream = _dataLocation())
            using (var reader = XmlReader.Create(stream, new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true }))
            {
                List<AccessRule> accessRules = new List<AccessRule>();
                while(reader.Read())
                {
                    var nodeType = reader.NodeType;
                    if (nodeType == XmlNodeType.Element)
                    {
                        var localName = reader.LocalName;
                        if (localName.Equals("rule"))
                        {
                            var value = reader.GetAttribute("id");
                            accessRules.Add(new AccessRule() { Name = value });
                        }
                    }
                }

                return accessRules;
            }
        }

        /// <summary>
        /// Gets the flat implied access rules.
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <returns>The flat list of <see cref="AccessRule" />. It includes also <paramref name="accessRule" /></returns>
        public IList<AccessRule> GetFlatImpliedAccessRules(AccessRule accessRule)
        {
            return GetFlatImpliedAccessRules(new[] { accessRule });
        }

        /// <summary>
        /// Gets the flat implied access rules.
        /// </summary>
        /// <param name="accessRules">The access rules.</param>
        /// <returns>The flat list of <see cref="AccessRule" />. It includes also <paramref name="accessRules" /></returns>
        public IList<AccessRule> GetFlatImpliedAccessRules(IList<AccessRule> accessRules)
        {
            var impliedRules = GetImpliedAccessRules();
            ImpliedRulesFilter.FilterImpliedRules(accessRules, impliedRules);
            return accessRules;
        }

        /// <summary>
        /// Gets the implied access rules lookup.
        /// </summary>
        /// <returns>The lookup map of <see cref="AccessRule" /> and its implied <see cref="AccessRule" />.</returns>
        public ILookup<string, string> GetImpliedAccessRules()
        {
            using (var stream = _dataLocation())
            using (var reader = XmlReader.Create(stream, new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true }))
            {
                var accessRules = new List<KeyValuePair<string, string>>();
                var found = false;
                string lastParentRule = null;
                while (reader.Read())
                {
                    var nodeType = reader.NodeType;
                    if (nodeType == XmlNodeType.Element)
                    {
                        var localName = reader.LocalName;
                        var value = reader.GetAttribute("id");
                        if (localName.Equals("impliedRules"))
                        {
                            found = true;
                        }
                        else if (found && localName.Equals("ruleRef"))
                        {
                            lastParentRule = value;
                        }
                        else if (found && localName.Equals("impliedRuleRef"))
                        {
                            if (lastParentRule != null)
                            {
                                accessRules.Add(new KeyValuePair<string, string>(lastParentRule, value));
                            }
                        }
                    }
                    else if (nodeType == XmlNodeType.EndElement)
                    {
                        var localName = reader.LocalName;
                        if (localName.Equals("ruleRef"))
                        {
                            lastParentRule = null;
                        }
                        else if (localName.Equals("impliedRules"))
                        {
                            found = false;
                            lastParentRule = null;
                        }
                    }
                }

                return accessRules.ToLookup(x => x.Key, x => x.Value, StringComparer.Ordinal);
            }
        }

        /// <summary>
        /// Gets the mapping stores.
        /// </summary>
        /// <returns>The list of <see cref="MappingStoreInfo" /></returns>
        public IList<MappingStoreInfo> GetMappingStores()
        {
            using (var stream = _dataLocation())
            using (var reader = XmlReader.Create(stream, new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true }))
            {
                var accessRules = new List<MappingStoreInfo>();
                while (reader.Read())
                {
                    var nodeType = reader.NodeType;
                    if (nodeType == XmlNodeType.Element)
                    {
                        var localName = reader.LocalName;
                        if (localName.Equals("storeId"))
                        {
                            var value = reader.GetAttribute("id");
                            accessRules.Add(new MappingStoreInfo() { Name = value });
                        }
                    }
                }

                return accessRules;
            }
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>The <see cref="User" /></returns>
        public User GetUser(string userName)
        {
            using (var stream = _dataLocation())
            using (var reader = XmlReader.Create(stream, new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true }))
            {
                User currentUser = null;
                while (reader.Read())
                {
                    var nodeType = reader.NodeType;
                    if (nodeType == XmlNodeType.Element)
                    {
                        var localName = reader.LocalName;
                        var value = reader.GetAttribute("id");
                        if (localName.Equals("user") && userName.Equals(value))
                        {
                            currentUser = new User() { Name = value };
                            var defaultStoreId = reader.GetAttribute("defaultStoreId");
                            if (!string.IsNullOrWhiteSpace(defaultStoreId))
                            {
                                currentUser.DefaultMappingStore = new MappingStoreInfo { Name = defaultStoreId };
                            }
                        }
                        else if (currentUser != null && localName.Equals("ruleRef"))
                        {
                            currentUser.AccessRules.Add(new AccessRule { Name = value });
                        }
                        else if (currentUser != null && localName.Equals("storeIdRef"))
                        {
                            currentUser.MappingStores.Add(new MappingStoreInfo { Name = value });
                        }
                    }
                    else if (nodeType == XmlNodeType.EndElement)
                    {
                        var localName = reader.LocalName;

                        if (localName.Equals("user") && currentUser != null)
                        {
                            return currentUser;
                        }
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="userPassword">Name of the user.</param>
        /// <returns>The <see cref="User" /></returns>
        public User GetUser(string userName, string userPassword)
        {
            using (var stream = _dataLocation())
            using (var reader = XmlReader.Create(stream, new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true }))
            {
                User currentUser = null;
                while (reader.Read())
                {
                    var nodeType = reader.NodeType;
                    if (nodeType == XmlNodeType.Element)
                    {
                        var localName = reader.LocalName;
                        var value = reader.GetAttribute("id");
                        var password = reader.GetAttribute("password");
                        if (!string.IsNullOrEmpty(userName))
                        {
                            if (localName.Equals("user") && userName.Equals(value) && (!string.IsNullOrEmpty(userPassword) && userPassword.Equals(password)))
                            {
                                currentUser = new User() { Name = value };
                                var defaultStoreId = reader.GetAttribute("defaultStoreId");

                                if (!string.IsNullOrWhiteSpace(defaultStoreId))
                                {
                                    currentUser.DefaultMappingStore = new MappingStoreInfo { Name = defaultStoreId };
                                }
                            }
                            else if (currentUser != null && localName.Equals("ruleRef"))
                            {
                                currentUser.AccessRules.Add(new AccessRule { Name = value });
                            }
                            else if (currentUser != null && localName.Equals("storeIdRef"))
                            {
                                currentUser.MappingStores.Add(new MappingStoreInfo { Name = value });
                            }
                        }
                    }
                    else if (nodeType == XmlNodeType.EndElement)
                    {
                        var localName = reader.LocalName;

                        if (localName.Equals("user") && currentUser != null)
                        {
                            return currentUser;
                        }
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Get the users without permissions. Suitable for listing
        /// </summary>
        /// <returns>The list of <see cref="User" /> without the <see cref="User.AccessRules" /> and <see cref="User.MappingStores" /></returns>
        public IList<User> GetUsers()
        {
            using (var stream = _dataLocation())
            using (var reader = XmlReader.Create(stream, new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true }))
            {
                var users = new List<User>();
                while(reader.Read())
                {
                    var nodeType = reader.NodeType;
                    if (nodeType == XmlNodeType.Element)
                    {
                        var localName = reader.LocalName;
                        var value = reader.GetAttribute("id");
                        if (localName.Equals("user"))
                        {
                            var currentUser = new User() { Name = value };
                            var defaultStoreId = reader.GetAttribute("defaultStoreId");
                            currentUser.DefaultMappingStore = new MappingStoreInfo { Name =  defaultStoreId };
                            users.Add(currentUser);
                        }
                    }
                }

                return users;
            }
        }

        /// <summary>
        /// Checks if the specified <paramref name="accessRule" /> is used
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <returns><c>true</c> if there is a <paramref name="accessRule" />, <c>false</c> otherwise.</returns>
        public bool IsUsed(AccessRule accessRule)
        {
            using (var stream = _dataLocation())
            using (var reader = XmlReader.Create(stream, new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true }))
            {
                User currentUser = null;
                while (reader.Read())
                {
                    var nodeType = reader.NodeType;
                    if (nodeType == XmlNodeType.Element)
                    {
                        var localName = reader.LocalName;
                        var value = reader.GetAttribute("id");
                        if (localName.Equals("user"))
                        {
                            currentUser = new User() { Name = value };
                        }
                        else if (currentUser != null && localName.Equals("ruleRef"))
                        {
                            if (string.Equals(value, accessRule?.Name))
                            {
                                return true;
                            }
                        }
                    }
                    else if (nodeType == XmlNodeType.EndElement)
                    {
                        var localName = reader.LocalName;

                        if (localName.Equals("user") && currentUser != null)
                        {
                            currentUser = null;
                        }
                        else if (localName.Equals("users"))
                        {
                            return false;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if the specified <paramref name="mappingStoreInfo" /> is used
        /// </summary>
        /// <param name="mappingStoreInfo">The mapping store information.</param>
        /// <returns><c>true</c> if there is a <paramref name="mappingStoreInfo" />, <c>false</c> otherwise.</returns>
        public bool IsUsed(MappingStoreInfo mappingStoreInfo)
        {
            return false;
        }
    }
}
