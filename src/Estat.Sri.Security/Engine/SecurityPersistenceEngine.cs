// -----------------------------------------------------------------------
// <copyright file="SecurityPersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2018-3-5
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Security.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Linq;
    using Dapper;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Security.Builder;
    using Estat.Sri.Security.Exceptions;
    using Estat.Sri.Security.Model;
    using Estat.Sri.Utils.Model;

    /// <summary>
    /// This class ire responsible for storing, updating or delete security information
    /// </summary>
    class SecurityPersistenceEngine : ISecurityPersistenceEngine
    {
        /// <summary>
        /// The update password
        /// </summary>
        private const string UpdatePassword = "UPDATE SRI_USER SET PASSWORD = {0}, SALT = {1}, ALGORITHM = {2} WHERE USERNAME = {3}";
        /// <summary>
        /// The update user default store
        /// </summary>
        private const string UpdateUserDefaultStore = "UPDATE SRI_USER SET DEFAULT_STORE_ID = {0} WHERE USERNAME = {1}";
        /// <summary>
        /// The add user rule
        /// </summary>
        private const string AddUserRule = "INSERT INTO USER_PERMISSION (ID, RULE_NAME) VALUES ({0}, {1})";
        /// <summary>
        /// The delete user rule
        /// </summary>
        private const string DeleteUserRule = "DELETE FROM USER_PERMISSION WHERE ID = {0} and RULE_NAME = {1}";
        /// <summary>
        /// The delete user mapping store
        /// </summary>
        private const string DeleteUserMappingStore = "DELETE FROM MS_STORE_USER WHERE ID = {0} and STORE_ID= {1}";
        /// <summary>
        /// The add user mapping store
        /// </summary>
        private const string AddUserMappingStore = "INSERT INTO MS_STORE_USER (ID, STORE_ID) VALUES ({0}, {1})";
        /// <summary>
        /// The add mapping store
        /// </summary>
        private const string AddMappingStore = "INSERT INTO MS_STORE (STORE_ID) VALUES ({0})";
        /// <summary>
        /// The add access rule
        /// </summary>
        private const string AddAccessRule = "INSERT INTO ACCESS_RULE (RULE_NAME) VALUES ({0})";
        /// <summary>
        /// The delete user
        /// </summary>
        private const string DeleteUser = "DELETE FROM SRI_USER WHERE USERNAME = {0}";
        /// <summary>
        /// The add implied access rule
        /// </summary>
        private const string AddImpliedAccessRule = "INSERT INTO ACCESS_RULE_IMPLICIT (RULE_NAME, IMPLIES_RULE_NAME) VALUES ({0}, {1})";
        /// <summary>
        /// The delete implied access rule
        /// </summary>
        private const string DeleteImpliedAccessRule = "DELETE FROM ACCESS_RULE_IMPLICIT WHERE RULE_NAME = {0} and IMPLIES_RULE_NAME = {1}";
        /// <summary>
        /// The delete access rule
        /// </summary>
        private const string DeleteAccessRule = "DELETE FROM ACCESS_RULE WHERE RULE_NAME = {0}";

        /// <summary>
        /// The delete all specific implied access rule
        /// </summary>
        private const string DeleteAllSpecificImpliedAccessRule = "DELETE FROM ACCESS_RULE_IMPLICIT WHERE IMPLIES_RULE_NAME = {0}";

        /// <summary>
        /// The delete mapping store
        /// </summary>
        private const string DeleteMappingStore = "DELETE FROM MS_STORE WHERE STORE_ID = {0}";
        /// <summary>
        /// The update access rule
        /// </summary>
        private const string UpdateAccessRule = "UPDATE ACCESS_RULE SET RULE_NAME = {1} WHERE RULE_NAME = {0}";
        /// <summary>
        /// The update mapping store
        /// </summary>
        private const string UpdateMappingStore = "UPDATE MS_STORE SET STORE_ID = {1} WHERE STORE_ID = {0}";

        /// <summary>
        /// The user converter engine
        /// </summary>
        private readonly UserConverterEngine _userConverterEngine;

        /// <summary>
        /// MAWEB seems to send a lot of requests at the same time
        /// </summary>
        private static readonly object mappingStoreLock = new object();
        private static readonly object accessRuleLock = new object();
        private static readonly object passwordLock = new object();

        /// <summary>
        /// The database
        /// </summary>
        private readonly Database _database;

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityPersistenceEngine" /> class.
        /// </summary>
        /// <param name="authDatabase">The authentication database.</param>
        public SecurityPersistenceEngine(Database authDatabase)
        {
            _userConverterEngine = new UserConverterEngine();
            _database = authDatabase;
        }

        /// <summary>
        /// Renames the specified <paramref name="mappingStoreInfo" /> with new <paramref name="newName" />.
        /// </summary>
        /// <param name="mappingStoreInfo">The mapping store information.</param>
        /// <param name="newName">The new name.</param>
        /// <exception cref="ArgumentNullException">rule is null</exception>
        /// <exception cref="ArgumentException">is null or empty - newName</exception>
        public void Rename(MappingStoreInfo mappingStoreInfo, string newName)
        {
            // TODO this doesn't work no UPDATE CASCADE in Oracle
            if (mappingStoreInfo == null)
            {
                throw new ArgumentNullException(nameof(mappingStoreInfo));
            }

            if (string.IsNullOrWhiteSpace(newName))
            {
                throw new ArgumentException("is null or empty", nameof(newName));
            }

            lock (mappingStoreLock) 
            {
                ExecuteAffectsOneRecord(UpdateMappingStore, null, new AnsiString(mappingStoreInfo.Name), new AnsiString(newName));
            }
        }

        /// <summary>
        /// Renames the specified <paramref name="rule" /> with new <paramref name="newName" />.
        /// </summary>
        /// <param name="rule">The rule.</param>
        /// <param name="newName">The new name.</param>
        /// <exception cref="ArgumentNullException">rule is null</exception>
        /// <exception cref="ArgumentException">is null or empty - newName</exception>
        public void Rename(AccessRule rule, string newName)
        {
            // TODO this doesn't work no UPDATE CASCADE in Oracle
            if (rule == null)
            {
                throw new ArgumentNullException(nameof(rule));
            }

            if (string.IsNullOrWhiteSpace(newName))
            {
                throw new ArgumentException("is null or empty", nameof(newName));
            }

            lock (accessRuleLock) 
            {
                ExecuteAffectsOneRecord(UpdateAccessRule, null, new AnsiString(rule.Name), new AnsiString(newName));
            }
        }

        /// <summary>
        /// Deletes the specified <paramref name="user" />.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <exception cref="ArgumentNullException">database
        /// or
        /// user</exception>
        public void Delete(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            ExecuteAffectsOneRecord(DeleteUser, parameters: new AnsiString(user.Name));
        }

        /// <summary>
        /// Deletes the specified <paramref name="mappingStore" />.
        /// </summary>
        /// <param name="mappingStore">The mapping store.</param>
        /// <exception cref="ArgumentNullException">database
        /// or
        /// user</exception>
        public void Delete(MappingStoreInfo mappingStore)
        {
            if (mappingStore == null)
            {
                throw new ArgumentNullException(nameof(mappingStore));
            }

            lock (mappingStoreLock)
            {
                ExecuteAffectsOneRecord(DeleteMappingStore, (retriever) => retriever.IsUsed(mappingStore), new AnsiString(mappingStore.Name));
            }
        }

        /// <summary>
        /// Deletes the specified <paramref name="accessRule" />.
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <exception cref="ArgumentNullException">database
        /// or
        /// user</exception>
        public void Delete(AccessRule accessRule)
        {
            if (accessRule == null)
            {
                throw new ArgumentNullException(nameof(accessRule));
            }

            lock (accessRuleLock)
            {
                ExecuteAffectsOneRecord(new[] { DeleteAllSpecificImpliedAccessRule, DeleteAccessRule }, (r) => r.IsUsed(accessRule), new AnsiString(accessRule.Name));
            }
        }

        /// <summary>
        /// Deletes the specified <paramref name="impliedRule" /> for <paramref name="accessRule" />.
        /// </summary>
        /// <param name="accessRule">The access rulee.</param>
        /// <param name="impliedRule">The implied rule.</param>
        /// <exception cref="ArgumentNullException">database
        /// or
        /// accessRule</exception>
        public void Delete(AccessRule accessRule, AccessRule impliedRule)
        {
            if (accessRule == null)
            {
                throw new ArgumentNullException(nameof(accessRule));
            }

            lock (accessRuleLock)
            {
                ExecuteAffectsOneRecord(DeleteImpliedAccessRule, null, new AnsiString(accessRule.Name), new AnsiString(impliedRule.Name));
            }
        }

        /// <summary>
        /// Updates the specified <paramref name="updatedUser" />.
        /// </summary>
        /// <param name="updatedUser">The user.</param>
        /// <exception cref="ArgumentNullException">database
        /// or
        /// user</exception>
        /// <exception cref="Estat.Sri.Security.Exceptions.SecurityResourceNotFoundException">User doesn't exist</exception>
        /// <exception cref="InvalidOperationException">database
        /// or
        /// user</exception>
        public void Update(User updatedUser)
        {
            if (updatedUser == null)
            {
                throw new ArgumentNullException(nameof(updatedUser));
            }

            using (var connection = _database.CreateConnection())
            {
                connection.Open();
                using (DbTransaction dbTransaction = connection.BeginTransaction()) 
                {
                    Database transactionalDatabase = new Database(_database, dbTransaction);
                    var retriever = new SecurityRetrieverEngine(transactionalDatabase);
                    var idAndUser = retriever.GetUserWithId(updatedUser.Name);

                    var existingUser = idAndUser.Value;
                    if (existingUser == null)
                    {
                        throw new SecurityResourceNotFoundException("User doesn't exist");
                    }

                    var validMappingStores = new HashSet<string>(retriever.GetMappingStores().Select(x => x.Name), StringComparer.Ordinal);

                    if (!string.Equals(existingUser.DefaultMappingStore?.Name, updatedUser.DefaultMappingStore.Name))
                    {
                        var newMappingStore = updatedUser.DefaultMappingStore.Name;
                        if (!validMappingStores.Contains(newMappingStore))
                        {
                            throw new SecurityResourceNotFoundException("Default Mapping Store id doesn't exist");
                        }

                        var records = transactionalDatabase.UsingLogger().Execute(UpdateUserDefaultStore, new AnsiString(updatedUser.DefaultMappingStore?.Name), new AnsiString(updatedUser.Name));
                        OneRecordUpdateValidation(records);
                    }

                    if (updatedUser.MappingStores.Any(x => !validMappingStores.Contains(x.Name)))
                    {
                        throw new SecurityResourceNotFoundException("At least one Mapping Store doesn't exist");
                    }

                    UpdateUserPermissions(transactionalDatabase, updatedUser, idAndUser);
                    UpdateUserStoreId(transactionalDatabase, updatedUser, idAndUser);

                    dbTransaction.Commit();
                }
            }
        }

        /// <summary>
        /// Adds the specified <paramref name="accessRule" />.
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <exception cref="ArgumentNullException">accessRule
        /// or
        /// database</exception>
        /// <exception cref="ResourceAlreadyExistsException">The resource already exists in the database</exception>
        public void Add(AccessRule accessRule)
        {
            if (accessRule == null)
            {
                throw new ArgumentNullException(nameof(accessRule));
            }

            lock (accessRuleLock)
            {
                ExecuteAffectsOneRecord(AddAccessRule, (r) =>
                {
                    if (r.Exists(accessRule)) 
                    {
                        throw new ResourceAlreadyExistsException();
                    }
                    return false;
                }, new AnsiString(accessRule.Name));
            }
        }

        /// <summary>
        /// Adds the specified <paramref name="impliedRules" /> for <paramref name="accessRule" /> to the AUTH DB
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <param name="impliedRules">The implied rules.</param>
        /// <exception cref="ArgumentNullException">accessRule
        /// or
        /// database</exception>
        /// <exception cref="Estat.Sri.Security.Exceptions.SecurityResourceNotFoundException"></exception>
        /// <exception cref="ResourceAlreadyExistsException">accessRule
        /// or
        /// database</exception>
        public void Update(AccessRule accessRule, IEnumerable<AccessRule> impliedRules)
        {
            if (accessRule == null)
            {
                throw new ArgumentNullException(nameof(accessRule));
            }

            if (impliedRules == null)
            {
                throw new ArgumentNullException(nameof(impliedRules));
            }

            lock (accessRuleLock)
            {
                using (var connection = _database.CreateConnection())
                {
                    connection.Open();
                    using (DbTransaction dbTransaction = connection.BeginTransaction())
                    {
                        Database transactionalDatabase = new Database(_database, dbTransaction);
                        var retriever = new SecurityRetrieverEngine(transactionalDatabase);

                        if (!retriever.Exists(accessRule))
                        {
                            throw new SecurityResourceNotFoundException();
                        }

                        IList<AccessRule> accessRules = retriever.GetFlatImpliedAccessRules(accessRule);
                        var existingImpliedRules = accessRules.ToSet();
                        var existingAccessRules = retriever.GetAccessRules().ToSet();
                        var newRules = impliedRules.ToSet();

                        var toInsert = newRules.Except(existingImpliedRules).Where(x => !string.Equals(x, accessRule.Name)).ToArray();
                        var toDelete = existingImpliedRules.Except(newRules).Where(x => !string.Equals(x, accessRule.Name));

                        if (toInsert.Any(x => !existingAccessRules.Contains(x)))
                        {
                            throw new SecurityResourceNotFoundException("A implied rule doesn't exist");
                        }

                        foreach (var newRule in toInsert)
                        {
                            transactionalDatabase.UsingLogger().Execute(AddImpliedAccessRule, new AnsiString(accessRule.Name), new AnsiString(newRule));
                        }

                        foreach (var oldRule in toDelete)
                        {
                            transactionalDatabase.UsingLogger().Execute(DeleteImpliedAccessRule, new AnsiString(accessRule.Name), new AnsiString(oldRule));
                        }

                        dbTransaction.Commit();
                    }
                }
            }
        }

        /// <summary>
        /// Adds the specified <paramref name="mappingStore" /> to the AUTH DB .
        /// </summary>
        /// <param name="mappingStore">The mapping store.</param>
        /// <exception cref="ArgumentNullException">mappingStore
        /// or
        /// database</exception>
        /// <exception cref="ResourceAlreadyExistsException">The resource already exists in the database</exception>
        public void Add(MappingStoreInfo mappingStore)
        {
            if (mappingStore == null)
            {
                throw new ArgumentNullException(nameof(mappingStore));
            }
            if (_database == null)
            {
                throw new ArgumentNullException(nameof(_database));
            }

            lock (mappingStoreLock)
            {
                ExecuteAffectsOneRecord(AddMappingStore, (r) =>
                {
                    if(r.Exists(mappingStore)) {
                        throw new ResourceAlreadyExistsException();
                    }
                    return false;
                }, new AnsiString(mappingStore.Name));
            }
        }

        /// <summary>
        /// Adds the specified database.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        /// <exception cref="ArgumentNullException">database
        /// or
        /// user</exception>
        /// <exception cref="ResourceAlreadyExistsException">database
        /// or
        /// user</exception>
        public void Add(User user, string password)
        {
            if (_database == null)
            {
                throw new ArgumentNullException(nameof(_database));
            }

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            using (var connection = _database.CreateConnection())
            {
                connection.Open();
                using (DbTransaction dbTransaction = connection.BeginTransaction())
                {
                    Database transactionalDatabase = new Database(_database, dbTransaction);
                    var retriever = new SecurityRetrieverEngine(transactionalDatabase);

                    UserShouldNotExist(user, retriever);

                    if (user.DefaultMappingStore != null && !retriever.Exists(user.DefaultMappingStore))
                    {
                        throw new SecurityResourceNotFoundException("The DefaultStoreId points to a non-existent Mapping Store");
                    }

                    var passwordDetails = _userConverterEngine.From(password);
                    long sysId = 0;
                    var p = new DynamicParameters();
                    p.Add("p_username", user.Name, DbType.AnsiString);
                    p.Add("p_password", passwordDetails.Password, DbType.AnsiString);
                    p.Add("p_salt", passwordDetails.Salt, DbType.AnsiString);
                    p.Add("p_algorithm", passwordDetails.Algorithm, DbType.AnsiString);
                    p.Add("p_default_store_id", user.DefaultMappingStore?.Name, DbType.AnsiString);
                    p.Add("p_pk", dbType: DbType.Int64, direction: ParameterDirection.Output);
                    connection.Execute("INSERT_SRI_USER", p, commandType: CommandType.StoredProcedure, transaction: dbTransaction);
                    sysId = p.Get<long>("p_pk");

                    if (sysId > 0)
                    {
                        AddUserPermissions(transactionalDatabase, user, sysId);
                        AddUserStoreIds(transactionalDatabase, user, sysId);
                        dbTransaction.Commit();
                    }
                    else 
                    {
                        dbTransaction.Rollback();
                    }
                }
            }
        }

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        /// <exception cref="ArgumentNullException">database
        /// or
        /// user</exception>
        /// <exception cref="SecurityResourceNotFoundException">database
        /// or
        /// user</exception>
        /// <exception cref="InvalidOperationException">User not found</exception>
        public void ChangePassword(User user, string password)
        {
            if (_database == null)
            {
                throw new ArgumentNullException(nameof(_database));
            }

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
                
            var passwordDetails = _userConverterEngine.From(password);
            int records = ExecuteAffectsOneRecord(UpdatePassword, null, new AnsiString(passwordDetails.Password), new AnsiString(passwordDetails.Salt), new AnsiString(passwordDetails.Algorithm), new AnsiString(user.Name));
            OneRecordUpdateValidation(records);
        }

        /// <summary>
        /// Updates the user permissions.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="updatedUser">The updated user.</param>
        /// <param name="idAndUser">The identifier and user.</param>
        private static void UpdateUserPermissions(Database database, User updatedUser, KeyValuePair<long, User> idAndUser)
        {
            var updatedAccessRules = updatedUser.AccessRules.ToSet();
            var existingAccessRules = idAndUser.Value.AccessRules.ToSet();

            var toInsert = updatedUser.AccessRules.Where(x => !existingAccessRules.Contains(x.Name)).ToArray();
            var toDelete = existingAccessRules.Except(updatedAccessRules);
            AddUserPermissions(database, idAndUser.Key, toInsert);
            ExecuteUserResource(database, idAndUser.Key, DeleteUserRule, toDelete);
        }

        /// <summary>
        /// Updates the user store identifier.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="updatedUser">The updated user.</param>
        /// <param name="idAndUser">The identifier and user.</param>
        private static void UpdateUserStoreId(Database database, User updatedUser, KeyValuePair<long, User> idAndUser)
        {
            var updated = updatedUser.MappingStores.ToSet();
            var existing = idAndUser.Value.MappingStores.ToSet();

            var toInsert = updatedUser.MappingStores.Where(x => !existing.Contains(x.Name)).ToArray();
            var toDelete = existing.Except(updated);
            AddUserStoreIds(database, idAndUser.Key, toInsert);
            ExecuteUserResource(database, idAndUser.Key, DeleteUserMappingStore, toDelete);
        }

        /// <summary>
        /// Adds the user permissions.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="user">The user.</param>
        /// <param name="sysId">The system identifier.</param>
        private static void AddUserPermissions(Database database, User user, long sysId)
        {
            IList<AccessRule> accessRules = user.AccessRules;
            AddUserPermissions(database, sysId, accessRules);
        }

        /// <summary>
        /// Adds the user permissions.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="sysId">The system identifier.</param>
        /// <param name="accessRules">The access rules.</param>
        private static void AddUserPermissions(Database database, long sysId, IList<AccessRule> accessRules)
        {
            var names = accessRules.Select(x => x.Name);
            try
            {
                ExecuteUserResource(database, sysId, AddUserRule, names);
            }
            catch(DbException e)
            {
                if (e.Message.Contains("FK_USER_PERMISSION_RULE"))
                {
                    throw new SecurityResourceNotFoundException("One or more Access Rules assigned to the user do not exist");
                }

                throw;
            }
        }

        /// <summary>
        /// Checks the number of records updated.
        /// </summary>
        /// <param name="records">The records.</param>
        /// <exception cref="Estat.Sri.Security.Exceptions.SecurityResourceNotFoundException">User doesn't exist</exception>
        /// <exception cref="InvalidOperationException">More than one record updated</exception>
        private static void OneRecordUpdateValidation(int records)
        {
            if (records == 0)
            {
                throw new SecurityResourceNotFoundException("Resource doesn't exist");
            }

            if (records > 1)
            {
                throw new InvalidOperationException("More than one record updated");
            }
        }

        /// <summary>
        /// Adds the user store ids.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="user">The user.</param>
        /// <param name="sysId">The system identifier.</param>
        private static void AddUserStoreIds(Database database, User user, long sysId)
        {
            IList<MappingStoreInfo> mappingStores = user.MappingStores;
            AddUserStoreIds(database, sysId, mappingStores);
        }

        /// <summary>
        /// Adds the user store ids.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="sysId">The system identifier.</param>
        /// <param name="mappingStores">The mapping stores.</param>
        private static void AddUserStoreIds(Database database, long sysId, IList<MappingStoreInfo> mappingStores)
        {
            var names = mappingStores.Select(x => x.Name);
            ExecuteUserResource(database, sysId, AddUserMappingStore, names);
        }

        /// <summary>
        /// Executes the <paramref name="sqlStatement" />
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="sysId">The user system identifier.</param>
        /// <param name="sqlStatement">The SQL statement.</param>
        /// <param name="names">The names.</param>
        private static void ExecuteUserResource(Database database, long sysId, string sqlStatement, IEnumerable<string> names)
        {
            foreach (var name in names)
            {
                var userId = database.CreateInParameter(nameof(sysId), DbType.Int64, sysId);
                var nameParameter = database.CreateInParameter("name", DbType.AnsiString, name);
                var recordUpdated = database.UsingLogger().ExecuteNonQueryFormat(sqlStatement, userId, nameParameter);
                Debug.Assert(recordUpdated == 1, "Records updated != 1", "Records updated = " + recordUpdated);
            }
        }

        /// <summary>
        /// Executes the <paramref name="sqlStatement" />
        /// </summary>
        /// <param name="sqlStatement">The SQL statement.</param>
        /// <param name="checkIfUsed">The check if used.</param>
        /// <param name="parameters">The parameters.</param>
        /// 
        /// <exception cref="ArgumentNullException">database</exception>
        /// <exception cref="ResourceIsUseException"></exception>
        private int ExecuteAffectsOneRecord(string sqlStatement, Func<ISecurityRetrieverEngine, bool> checkIfUsed = null, params SimpleParameter[] parameters)
        {
            return ExecuteAffectsOneRecord(new string[] {sqlStatement}, checkIfUsed, parameters);
        }

        /// <summary>
        /// Executes the <paramref name="sqlStatements" />. The last one is checked if it updates only one record. 
        /// </summary>
        /// <param name="sqlStatements">The SQL statements. They must use the same <paramref name="parameters"/></param>
        /// <param name="checkIfUsed">The check if used.</param>
        /// <param name="parameters">The parameters. </param>
        /// <exception cref="ResourceIsUseException"></exception>
        /// <exception cref="ArgumentNullException">database</exception>
        private int ExecuteAffectsOneRecord(IList<string> sqlStatements, Func<ISecurityRetrieverEngine, bool> checkIfUsed = null, params SimpleParameter[] parameters)
        {
            /*
            Attempt to avoid getting that we were getting with TransactionScope:
            Transaction (Process ID 52) was deadlocked on lock resources with another process and has been chosen as the deadlock victim. Rerun the transaction

            */
            using (var connection = _database.CreateConnection())
            {
                connection.Open();
                using (DbTransaction dbTransaction = connection.BeginTransaction()) 
                {
                    Database transactionalDatabase = new Database(_database, dbTransaction);
                    if (checkIfUsed != null)
                    {
                        var retriever = new SecurityRetrieverEngine(transactionalDatabase);
                        if (checkIfUsed(retriever))
                        {
                            throw new ResourceIsUseException();
                        }
                    }

                    int last = sqlStatements.Count - 1;
                    for(int i = 0; i < last; i++)
                    {
                        transactionalDatabase.UsingLogger().Execute(sqlStatements[i], parameters);
                    }

                    var records = transactionalDatabase.UsingLogger().Execute(sqlStatements[last], parameters);
                    dbTransaction.Commit();
                    return records;
                }
            }
        }

        /// <summary>
        /// Users the should not exist.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="retriever"></param>
        /// <exception cref="ResourceAlreadyExistsException"></exception>
        private void UserShouldNotExist(User user, SecurityRetrieverEngine retriever)
        {
            var existingUser = retriever.GetUser(user.Name);
            if (existingUser != null)
            {
                throw new ResourceAlreadyExistsException();
            }
        }
    }
}
