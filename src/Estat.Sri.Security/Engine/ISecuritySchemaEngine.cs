// -----------------------------------------------------------------------
// <copyright file="ISecuritySchemaEngine.cs" company="EUROSTAT">
//   Date Created : 2018-3-11
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Security.Model;

namespace Estat.Sri.Security.Engine
{
    /// <summary>
    /// Interface for Security Schema Engine
    /// </summary>
    public interface ISecuritySchemaEngine
    {
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        void Initialize();

        /// <summary>
        /// Retrieves the version.
        /// </summary>
        /// <returns>The version</returns>
        AuthDbVersionAndStatus RetrieveVersion();

        /// <summary>
        /// Gets the available version.
        /// </summary>
        /// <returns>Version.</returns>
        Version GetAvailableVersion();

        /// <summary>
        /// Deletes the AUTH DB store. This could be the tables in a DB or elemnets in a XML
        /// </summary>
        void DeleteStore();
        
        /// <summary>
        /// Stores the connection.
        /// </summary>
        /// <param name="connectionEntity">The mapping store connection entity.</param>
        /// <param name="isAdmin">if set to <c>true</c> [is admin].</param>
        /// <returns></returns>
        IActionResult StoreConnection(ConnectionEntity connectionEntity, bool isAdmin);
        /// <summary>
        /// Tests the connection.
        /// </summary>
        /// <param name="connectionEntity">The mapping store connection entity.</param>
        /// <param name="isAdmin"></param>
        /// <returns></returns>
        IActionResult TestConnection(ConnectionEntity connectionEntity,bool isAdmin);

        /// <summary>
        /// Gets the connection info.
        /// </summary>
        /// <param name="connectionName">The connection string name.</param>
        /// <returns>An <see cref="IConnectionEntity"/> with the information.</returns>
        IConnectionEntity GetConnectionEntity(string connectionName);
    }
}