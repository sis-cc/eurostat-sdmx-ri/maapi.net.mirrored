// -----------------------------------------------------------------------
// <copyright file="AuthDbSchema.cs" company="EUROSTAT">
//   Date Created : 2018-3-10
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Security.Exceptions;
using Estat.Sri.Security.Model;
using Estat.Sri.Utils.Helper;
using Estat.Sri.Utils.Sql;

namespace Estat.Sri.Security.Engine
{
    class AuthDbSchema : ISecuritySchemaEngine
    {
        private Database _database;
        private readonly IDatabaseProviderManager _databaseProviderManager;
        private readonly string _rootNamespace = "Estat.Sri.Security.resources";
        private readonly IConfigurationStoreEngine<ConnectionStringSettings> _configurationStoreEngine = new AppConfigStore(ConfigurationManager.AppSettings["authDbConfigLocation"]);

        /// <summary>
        /// The assembly
        /// </summary>
        private readonly Assembly _assembly;

        /// <summary>
        /// The manifest resource names
        /// </summary>
        private readonly string[] _manifestResourceNames;

        /// <summary>
        /// The normalized resource names
        /// </summary>
        private readonly Dictionary<string, string> _normalizedResourceNames;
        private readonly string _fullInitSql;
        private readonly string _dropFile;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthDbSchema"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="databaseProviderManager"></param>
        /// <exception cref="System.ArgumentNullException">database</exception>
        /// <exception cref="SecurityResourceNotFoundException"></exception>
        public AuthDbSchema(Database database, IDatabaseProviderManager databaseProviderManager)
        {
            _databaseProviderManager = databaseProviderManager;
            if (database != null)
            {
                _database = database;
                var type = this.GetType();
                this._assembly = type.Assembly;
                this._manifestResourceNames = this._assembly.GetManifestResourceNames();
                this._normalizedResourceNames = this._manifestResourceNames.ToDictionary(s => s, StringComparer.OrdinalIgnoreCase);
                var databaseType = GetDatabaseType(_database.ProviderName);
                if (databaseType == null)
                {
                    throw new SecurityResourceNotFoundException($"Support for {_database.ProviderName} not found");
                }
                this._fullInitSql = string.Format(
                   CultureInfo.InvariantCulture,
                   "{0}.sql",
                   databaseType);
                _dropFile = string.Format(CultureInfo.InvariantCulture, "drop.{0}.sql", databaseType);
            }
        }


        public Version GetAvailableVersion()
        {
            return new Version(1, 0);
        }

        public void DeleteStore()
        {
            var sqlStatements = GetSqlStatements(_dropFile);
            ExecuteStatements(sqlStatements);
        }

        public void Initialize()
        {
            //delete store 
            DeleteStore();
            // Init the db
            var sqlStatements = GetSqlStatements(this._fullInitSql, "version.sql");
            ExecuteStatements(sqlStatements);

            // Load the default permissions
            var xmlDataLocation = GetDataLocation("defaultAccessRules.xml");
            var dbStore = new SecurityPersistenceEngine(_database);
            var xmlStore = new SecurityRetrieverFromXml(xmlDataLocation);

            var impliedRules = xmlStore.GetImpliedAccessRules();
            foreach(var rule in xmlStore.GetAccessRules())
            {
                dbStore.Add(rule);
                var impliedRulesForThis = impliedRules[rule.Name];
                dbStore.Update(
                    rule,
                    impliedRulesForThis.Select(
                        x => new AccessRule()
                             {
                                 Name = x
                             }).ToArray());
            }

            foreach(var storeid in xmlStore.GetMappingStores())
            {
                dbStore.Add(storeid);
            }

            var users = xmlStore.GetUsers();
            foreach(var userName in users.Select(x => x.Name))
            {
                var fullUser = xmlStore.GetUser(userName);
                dbStore.Add(fullUser, null);
            }
        }

        /// <summary>
        /// Retrieves the version.
        /// </summary>
        /// <returns>The <see cref="System.Version"/> of the store.</returns>
        public AuthDbVersionAndStatus RetrieveVersion()
        {
            var statusAndMessage = TestConnection();
            if(statusAndMessage.Status == AuthDbStatus.ConfiguredAccessibleNotInitialized)
            {
                if (!IsInitialized())
                {
                    return new AuthDbVersionAndStatus()
                    {
                        StatusAndMessage = statusAndMessage,
                    };
                }
                else
                {
                    try
                    {
                        var enumerable = _database.Query<dynamic>("SELECT MAJOR, MINOR FROM AUTH_DB_VERSION A WHERE NOT EXISTS (SELECT MAJOR FROM AUTH_DB_VERSION B WHERE A.MAJOR < B.MAJOR or ( A.MAJOR = B.MAJOR and A.MINOR < B.MINOR ) )");
                        var versions = enumerable.Select(s => new Version((int)s.MAJOR, (int)s.MINOR));
                        return new AuthDbVersionAndStatus()
                        {
                            Version = versions.FirstOrDefault() ?? new Version(1, 0),
                            StatusAndMessage = new AuthDbStatusWithMessage()
                            {
                                Status = AuthDbStatus.ConfiguredAccessibleInitialized,
                                Message = "Auth database configured, accessible and initialized."
                            }
                        };
                    }
                    catch (DbException)
                    {
                        return null;
                    }
                }
            }
            else
            {
                return new AuthDbVersionAndStatus()
                {
                    StatusAndMessage = statusAndMessage,
                };
            }
        }

        private bool IsInitialized()
        {
            var ddbEngine = this._databaseProviderManager.GetEngineByProvider(_database.ProviderName);
            var connectionStringSettings = _database.ConnectionStringSettings;

            var databaseObjects = ddbEngine.Browser.GetDatabaseObjects(connectionStringSettings).ToList();
            return databaseObjects.Exists(x => x.Name.ToUpper() == "AUTH_DB_VERSION");
        }

        public AuthDbStatusWithMessage TestConnection()
        {
            if (_database == null || !IsConfigured(_database.Name))
            {
                return new AuthDbStatusWithMessage() 
                { 
                    Status = AuthDbStatus.NotConfigured,
                    Message = "Auth database not configured"
                };
            }
            else
            {
                try
                {
                    using (var connection = _database.CreateConnection())
                    {
                        connection.Open();
                    }
                    return new AuthDbStatusWithMessage() 
                    { 
                        Status = AuthDbStatus.ConfiguredAccessibleNotInitialized,
                        Message = "Auth database configured, accessible and not initialized."
                    };
                }
                catch (DbException e)
                {
                    return new AuthDbStatusWithMessage()
                    {
                        Status = AuthDbStatus.NotAccessible,
                        Message = "Auth database not accessible."
                    };
                }
            }
        }

        /// <summary>
        /// Executes the statements.
        /// </summary>
        /// <param name="fullSchema">The full schema.</param>
        private void ExecuteStatements(IEnumerable<string> fullSchema)
        {
            var db = this._database;
            string lastStatement = string.Empty;
            try
            {
                using (var connection = db.CreateConnection())
                {
                    connection.Open();
                    using (var transaction = connection.BeginTransaction())
                    {
                        Database transactionalDatabase = new Database(db, transaction);
                        foreach (var statement in fullSchema)
                        {
                            lastStatement = statement;
                            transactionalDatabase.UsingLogger().ExecuteNonQuery(statement, new DbParameter[0]);
                        }

                        transaction.Commit();
                    }
                }
            }
            catch(DbException e)
            {
                throw new AuthDatabaseException(lastStatement, e);
            }
        }

        /// <summary>
        /// Gets the script reader.
        /// </summary>
        /// <returns>
        /// The <see cref="TextReader" /> stream to the SQL Script.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">databaseType is null</exception>
        private IEnumerable<string> GetSqlStatements(params string[] sqlResources)
        {
            foreach (var sqlResource in sqlResources)
            {
                var location = GetDataLocation(sqlResource);
                foreach (var p in ReadLines(location).SplitStatements())
                {
                    yield return p;
                }
            }
        }

        /// <summary>
        /// Reads the lines.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <returns>The lines from the resource</returns>
        private IEnumerable<string> ReadLines(Func<Stream> location)
        {
            using (var stream = location())
            {
                var statements = stream.ReadLines();
                foreach (var statement in statements)
                {
                    yield return statement;
                }
            }
        }

        /// <summary>
        /// Gets the type of the database.
        /// </summary>
        /// <param name="providerName"></param>
        /// <returns>The database type</returns>
        /// <exception cref="System.NotImplementedException">Support for " + connectionStringSettings.ProviderName</exception>
        private static string GetDatabaseType(string providerName)
        {
            var mappings = DatabaseType.Mappings;
            foreach (MastoreProviderMappingSetting mapping in mappings)
            {
                if (mapping.Provider.Equals(providerName ?? MappingStoreDefaultConstants.SqlServerProvider))
                {
                    return mapping.Name;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the data location.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <returns>The function pointing to the resource.</returns>
        private Func<Stream> GetDataLocation(string location)
        {
            var fullPath = string.Format(CultureInfo.InvariantCulture, "{0}.{1}", this._rootNamespace, location);
            string normPath;
            if (this._normalizedResourceNames.TryGetValue(fullPath, out normPath))
            {
                return () => GetInputStream(normPath);
            }

            throw new SecurityResourceNotFoundException(location);
        }

        /// <summary>
        /// Gets a guaranteed new input stream on each Property call.
        ///                 The input stream will be reading the same underlying data source.
        /// </summary>
        private Stream GetInputStream(string name)
        {
            return this._assembly.GetManifestResourceStream(name);
        }

        public IActionResult StoreConnection(ConnectionEntity connectionEntity, bool isAdmin)
        {
            //connection name should always be authdb
            connectionEntity.Name = AuthDbConstants.Id;
            var testResult = TestConnection(connectionEntity, isAdmin);
            if(testResult.Status != StatusType.Success)
            {
                return testResult;
            }

            var connectionSettingsBuilder = this._databaseProviderManager.GetEngineByType(connectionEntity.DatabaseVendorType).SettingsBuilder;
            var connectionString = connectionSettingsBuilder.CreateConnectionString(connectionEntity);
            _configurationStoreEngine.SaveSettings(connectionString);
            return new ActionResult(AuthDbStatus.SettingsStoredWriteableConfiguration.ToString("d"), StatusType.Success, "Settings stored in the writeable configuration");
        }

        public IActionResult TestConnection(ConnectionEntity connectionEntity, bool isAdmin)
        {
            if(!isAdmin && IsConfigured(connectionEntity.Name))
            {
                return new ActionResult(AuthDbStatus.AlreadyConfigured.ToString("d"), StatusType.Warning, "Auth DB already configured");
            }

            var connectionSettingsBuilder = this._databaseProviderManager.GetEngineByType(connectionEntity.DatabaseVendorType).SettingsBuilder;
            var database = new Database(connectionSettingsBuilder.CreateConnectionString(connectionEntity));

            try
            {
                using (var connection = database.CreateConnection())
                {
                    connection.Open();
                }

                return new ActionResult("200", StatusType.Success, "Auth DB settings correct");
            }
            catch (DbException e)
            {
                return new ActionResult("503", StatusType.Error, e.Message);
            }
        }

        private bool IsConfigured(string connectionName)
        {
           var settings = _configurationStoreEngine.GetSettings().FirstOrDefault(x => x.Name == connectionName);
           return settings != null;
        }

        public IConnectionEntity GetConnectionEntity(string connectionName)
        {
            var connectionStringSettings = _configurationStoreEngine.GetSettings().FirstOrDefault(x => x.Name == connectionName);
            if (connectionStringSettings == null)
            {
                return null;
            }

            var engine = this._databaseProviderManager.GetEngineByProvider(connectionStringSettings.ProviderName);

            return engine.SettingsBuilder.CreateConnectionEntity(connectionStringSettings);
        }
    }
}
