// -----------------------------------------------------------------------
// <copyright file="ISecurityRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2018-3-10
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Security.Model;

namespace Estat.Sri.Security.Engine
{
    /// <summary>
    /// This interface implementations are responsible for retrieving security objects
    /// </summary>
    public interface ISecurityRetrieverEngine
    {
        /// <summary>
        /// Checks the existence of the specified <paramref name="accessRule"/>
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <returns><c>true</c> if there is a <paramref name="accessRule"/>, <c>false</c> otherwise.</returns>
        bool Exists(AccessRule accessRule);

        /// <summary>
        /// Checks the existence of the specified <paramref name="mappingStoreInfo"/>
        /// </summary>
        /// <param name="mappingStoreInfo">The mapping store information.</param>
        /// <returns><c>true</c> if there is a <paramref name="mappingStoreInfo"/>, <c>false</c> otherwise.</returns
        bool Exists(MappingStoreInfo mappingStoreInfo);

        /// <summary>
        /// Gets the Access rules
        /// <returns>The list of <see cref="AccessRule" /></returns>
        IList<AccessRule> GetAccessRules();
        
        /// <summary>
        /// Gets the flat implied access rules.
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <returns>The flat list of <see cref="AccessRule"/>. It includes also <paramref name="accessRule"/></returns>
        IList<AccessRule> GetFlatImpliedAccessRules(AccessRule accessRule);

        /// <summary>
        /// Gets the flat implied access rules.
        /// </summary>
        /// <param name="accessRules">The access rules.</param>
        /// <returns>The flat list of <see cref="AccessRule"/>. It includes also <paramref name="accessRules"/></returns>
        IList<AccessRule> GetFlatImpliedAccessRules(IList<AccessRule> accessRules);

        /// <summary>
        /// Gets the implied access rules lookup.
        /// </summary>
        /// <returns>The lookup map of <see cref="AccessRule"/> and its implied <see cref="AccessRule"/>. </returns>
        ILookup<string, string> GetImpliedAccessRules();

        /// <summary>
        /// Gets the mapping stores.
        /// </summary>
        /// <returns>The list of <see cref="MappingStoreInfo" /></returns>
        IList<MappingStoreInfo> GetMappingStores();

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>The <see cref="User"/></returns>
        User GetUser(string userName);

        /// <summary>
        /// Get the users without permissions. Suitable for listing
        /// </summary>
        /// <returns>The list of <see cref="User" /> without the <see cref="User.AccessRules" /> and <see cref="User.MappingStores" /></returns>
        IList<User> GetUsers();

        /// <summary>
        /// Checks if the specified <paramref name="accessRule"/> is used
        /// </summary>
        /// <param name="accessRule">The access rule.</param>
        /// <returns><c>true</c> if there is a <paramref name="accessRule"/>, <c>false</c> otherwise.</returns>
        bool IsUsed(AccessRule accessRule);
        
        /// <summary>
        /// Checks if the specified <paramref name="mappingStoreInfo"/> is used
        /// </summary>
        /// <param name="mappingStoreInfo">The mapping store information.</param>
        /// <returns><c>true</c> if there is a <paramref name="mappingStoreInfo"/>, <c>false</c> otherwise.</returns>
        bool IsUsed(MappingStoreInfo mappingStoreInfo);
    }
}