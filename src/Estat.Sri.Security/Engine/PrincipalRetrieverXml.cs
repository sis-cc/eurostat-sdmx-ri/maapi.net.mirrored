// -----------------------------------------------------------------------
// <copyright file="PrincipalRetrieverXml.cs" company="EUROSTAT">
//   Date Created : 2018-3-5
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Security.Exceptions;

namespace Estat.Sri.Security.Engine
{
    using System;
    using System.IO;
    using System.Security.Principal;
    using System.Xml;
    using Estat.Sri.Security.Builder;

    public class PrincipalRetrieverXml
    {
        private readonly PrincipalBuilder _principalBuilder;
        /// <summary>
        /// Initializes a new instance of the <see cref="PrincipalRetrieverXml"/> class.
        /// </summary>
        public PrincipalRetrieverXml()
        {
            _principalBuilder = new PrincipalBuilder();
        }

        private Func<Stream> GetDataLocation(string location)
        {
            if (!string.IsNullOrEmpty(location))
            {
                return () => GetInputStream(location);
            }

            throw new SecurityResourceNotFoundException(location);
        }
        /// <summary>
        /// Gets a guaranteed new input stream on each Property call.
        ///                 The input stream will be reading the same underlying data source.
        /// </summary>
        private Stream GetInputStream(string location)
        {
            if (string.IsNullOrEmpty(location))
            {
                return null;
            }
            MemoryStream inMemoryCopy = new MemoryStream();
            XmlDocument doc = new XmlDocument();
            doc.Load(location);
            using (FileStream fs = File.OpenRead(location))
            {
                fs.CopyTo(inMemoryCopy);
                inMemoryCopy.Position = 0;
                return inMemoryCopy;
            }
        }

        /// <summary>
        /// Gets the principal.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="providedPassword">The user's password.</param>
        /// <param name="scope">The scope.</param>
        /// <param name="authXmlLocation">The authXml Location.</param>
        /// <returns>
        /// The <see cref="IPrincipal"/> if authenticated and exists; otherwise null
        /// </returns>
        /// <exception cref="ArgumentNullException">userName is null</exception>
        public IPrincipal GetPrincipal(string userName, string providedPassword, string scope, string authXmlLocation)
        {
            if (userName == null)
            {
                throw new ArgumentNullException(nameof(userName));
            }

            var xmlDataLocation = GetDataLocation(authXmlLocation);
            var securityRetriever = new SecurityRetrieverFromXml(xmlDataLocation);

            var user = securityRetriever.GetUser(userName, providedPassword);
            if (user != null)
            {
                var allUserRules = securityRetriever.GetFlatImpliedAccessRules(user.AccessRules);
                var userWithAllRules = user.DeepClone();

                userWithAllRules.AccessRules.Clear();
                foreach (var rule in allUserRules)
                {
                    userWithAllRules.AccessRules.Add(rule);
                }

                var principal = _principalBuilder.Build(userWithAllRules, scope);
                if (principal != null)
                {
                    return principal;
                }
            }

            return null;
        }
    }
}
