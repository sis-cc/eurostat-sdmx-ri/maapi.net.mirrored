// -----------------------------------------------------------------------
// <copyright file="PrincipalRetriever.cs" company="EUROSTAT">
//   Date Created : 2018-3-5
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Security.Engine
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Security.Principal;
    using Dapper;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Security.Builder;

    public class PrincipalRetriever
    {
        /// <summary>
        /// The ma user password query
        /// </summary>
        private const string MaUserPasswordQuery = "select SALT, PASSWORD, ALGORITHM from SRI_USER where USERNAME={0}";

        private readonly PrincipalBuilder _principalBuilder;
        private readonly BytesConverter _bytesConverter = new BytesConverter();
        private readonly HashBuilder _hashBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrincipalRetriever"/> class.
        /// </summary>
        public PrincipalRetriever()
        {
            _hashBuilder = new HashBuilder(_bytesConverter);
            _principalBuilder = new PrincipalBuilder();
        }

        /// <summary>
        /// Gets the principal.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="providedPassword">The provided password.</param>
        /// <param name="scope">The scope.</param>
        /// <returns>
        /// The <see cref="IPrincipal"/> if authenticated and exists; otherwise null
        /// </returns>
        /// <exception cref="ArgumentNullException">userName is null</exception>
        public IPrincipal GetPrincipal(Database database, string userName, string providedPassword, string scope)
        {
            if (userName == null)
            {
                throw new ArgumentNullException(nameof(userName));
            }

            var securityRetriever = new SecurityRetrieverEngine(database);

            if (database != null && this.CheckPassword(database, userName, providedPassword))
            {
                var user = securityRetriever.GetUser(userName);
                if (user != null)
                {
                    var allUserRules =securityRetriever.GetFlatImpliedAccessRules(user.AccessRules);
                    var userWithAllRules = user.DeepClone();

                    userWithAllRules.AccessRules.Clear();
                    foreach(var rule in allUserRules)
                    {
                        userWithAllRules.AccessRules.Add(rule);
                    }

                    var principal = _principalBuilder.Build(userWithAllRules, scope);
                    if (principal != null)
                    {
                        return principal;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Checks the password.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="providedPassword">The provided password.</param>
        /// <returns>True if the user and password match</returns>
        public bool CheckPassword(Database database, string userName, string providedPassword)
        {
            string pass;
            string algorithm;
            string salt;
            var userNameParam = database.BuildParameterName(nameof(userName));
            var query = string.Format(CultureInfo.InvariantCulture, MaUserPasswordQuery, userNameParam);
            var result =  database.Query(query, new { userName = new DbString() { IsAnsi = true, Value = userName } }).FirstOrDefault();

            if (result != null)
            {
                pass = Convert.ToString(result.PASSWORD);
                salt = Convert.ToString(result.SALT);
                algorithm = Convert.ToString(result.ALGORITHM);
            }
            else
            {
                return false;
            }

            // no password
            if (string.IsNullOrEmpty(pass) && string.IsNullOrEmpty(salt) && string.IsNullOrEmpty(providedPassword))
            {
                return true;
            }

            if (string.IsNullOrEmpty(pass))
            {
                return false;
            }

            byte[] saltBytes;
            if (string.IsNullOrEmpty(salt))
            {
                saltBytes = new byte[0];
            }
            else
            {
                saltBytes = this._bytesConverter.ConvertFromHex(salt);
            }

            if (string.IsNullOrEmpty(salt) && string.IsNullOrEmpty(algorithm))
            {
                algorithm = "MD5"; // Backwards compatibility with old Mapping Assistant
            }
            else
            {
                algorithm = algorithm ?? ConfigManager.Config.DefaultHashAlgorithm;
            }

            var providedPasswordBytes = this._hashBuilder.Build(providedPassword, saltBytes, algorithm);
            var providedPasswordHex = this._bytesConverter.ConvertToHex(providedPasswordBytes);
            return string.Equals(providedPasswordHex, pass);
        }
    }
}
