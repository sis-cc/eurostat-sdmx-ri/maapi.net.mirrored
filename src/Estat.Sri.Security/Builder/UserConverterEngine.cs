// -----------------------------------------------------------------------
// <copyright file="UserConverterEngine.cs" company="EUROSTAT">
//   Date Created : 2017-06-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStoreRetrieval.Builder;
using Estat.Sri.MappingStoreRetrieval.Config;

namespace Estat.Sri.Security.Builder
{
    internal class UserConverterEngine
    {
        readonly string _defaultHashAlgorithm = ConfigManager.Config.DefaultHashAlgorithm;
        readonly SaltBuilder _saltBuilder = new SaltBuilder();
        readonly BytesConverter _bytesConverter = new BytesConverter();
        readonly HashBuilder _hashBuilder;

        public UserConverterEngine()
        {
            _hashBuilder = new HashBuilder(_bytesConverter);
        }

        internal PasswordDetails From(string password)
        {
            if (password == null)
            {
                return new PasswordDetails();
            }

            var salt = _saltBuilder.Build();
            var hash = _hashBuilder.Build(password, salt, _defaultHashAlgorithm);

            var passwordDetails = new PasswordDetails
            {
                Password = _bytesConverter.ConvertToHex(hash),
                Salt = _bytesConverter.ConvertToHex(salt),
                Algorithm = _defaultHashAlgorithm,

            };
            return passwordDetails;
        }
    }

    internal class PasswordDetails
    {
        internal string Password { get; set; }
        internal string Salt { get; set; }
        internal string Algorithm { get; set; }
    }
}