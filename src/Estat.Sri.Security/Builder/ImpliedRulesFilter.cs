// -----------------------------------------------------------------------
// <copyright file="ImpliedRulesFilter.cs" company="EUROSTAT">
//   Date Created : 2018-3-11
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Security.Engine;
using Estat.Sri.Security.Model;

namespace Estat.Sri.Security.Builder
{
    static class ImpliedRulesFilter
    {
        /// <summary>
        /// Gets the implied rules.
        /// </summary>
        /// <param name="accessRules">The access rules.</param>
        public static void FilterImpliedRules(IList<AccessRule> accessRules, ILookup<string, string> impliedRules)
        {
            var ruleDistinctSet = accessRules.ToSet();
            var stack = new Stack<string>(ruleDistinctSet);
            while (stack.Count > 0)
            {
                var current = stack.Pop();
                var impliedRulesForCurrent = impliedRules[current];
                foreach (var implied in impliedRulesForCurrent)
                {
                    if (!ruleDistinctSet.Contains(implied))
                    {
                        ruleDistinctSet.Add(implied);
                        accessRules.Add(new AccessRule() { Name = implied });
                        stack.Push(implied);
                    }
                }
            }
        }
    }
}
