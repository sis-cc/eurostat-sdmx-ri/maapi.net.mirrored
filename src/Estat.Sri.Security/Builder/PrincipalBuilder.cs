// -----------------------------------------------------------------------
// <copyright file="PrincipalBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-2-28
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Security.Principal;
using Estat.Sri.Security.Model;

namespace Estat.Sri.Security.Builder
{
    public class PrincipalBuilder
    {
        /// <summary>
        /// Builds the specified user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="scope">The scope.</param>
        /// <returns>The <see cref="IPrincipal"/></returns>
        /// <exception cref="ArgumentNullException">user is <c>null</c></exception>
        public IPrincipal Build(User user, string scope)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var identity = new GenericIdentity(user.Name);
            return new SriPrincipal(user, scope, identity);
        }
    }
}
