// -----------------------------------------------------------------------
// <copyright file="SecurityManager.cs" company="EUROSTAT">
//   Date Created : 2018-3-10
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Security.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Sri.Security.Engine;
    using Estat.Sri.Security.Model;

    public class SecurityManager
    {
        /// <summary>
        /// The security store factories
        /// </summary>
        private readonly IList<ISecurityStoreFactory> _securityStoreFactories;

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityManager"/> class.
        /// </summary>
        /// <param name="securityStoreFactories">The security store factories.</param>
        /// <exception cref="ArgumentNullException">securityStoreFactories</exception>
        public SecurityManager(params ISecurityStoreFactory[] securityStoreFactories)
        {
            if (securityStoreFactories == null)
            {
                throw new ArgumentNullException(nameof(securityStoreFactories));
            }

            _securityStoreFactories = securityStoreFactories;
        }

        /// <summary>
        /// Gets the persistence engine.
        /// </summary>
        /// <param name="storeType">Type of the store.</param>
        /// <returns>The <see cref="ISecurityPersistenceEngine"/>.</returns>
        public ISecurityPersistenceEngine GetPersistenceEngine(ISecurityStore storeType)
        {
            return _securityStoreFactories.Select(x => x.GetPersistenceEngine(storeType)).FirstOrDefault(x => x != null);
        }

        /// <summary>
        /// Gets the retriever engine.
        /// </summary>
        /// <param name="storeType">Type of the store.</param>
        /// <returns>ISecurityRetrieverEngine.</returns>
        /// <returns>The <see cref="ISecurityRetrieverEngine"/>.</returns>
        public ISecurityRetrieverEngine GetRetrieverEngine(ISecurityStore storeType)
        {
            return _securityStoreFactories.Select(x => x.GetRetrieverEngine(storeType)).FirstOrDefault(x => x != null);
        }

        /// <summary>
        /// Gets the schema engine.
        /// </summary>
        /// <param name="storeType">Type of the store.</param>
        /// <returns>The <see cref="ISecuritySchemaEngine"/>.</returns>
        public ISecuritySchemaEngine GetSchemaEngine(ISecurityStore storeType)
        {
            return _securityStoreFactories.Select(x => x.GetSchemaEngine(storeType)).FirstOrDefault(x => x != null);
        }
    }
}
