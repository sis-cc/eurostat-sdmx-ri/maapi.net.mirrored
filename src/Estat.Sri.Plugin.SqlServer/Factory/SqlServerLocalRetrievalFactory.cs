﻿// -----------------------------------------------------------------------
// <copyright file="SqlServerLocalRetrievalFactory.cs" company="EUROSTAT">
//   Date Created : 2017-03-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.SqlServer.Factory
{
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.MappingStore.Engine;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.MappingStoreRetrieval.Config;

    /// <summary>
    /// The SQL Server plug-in for DDB access
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Factory.ILocalCodeRetrievalFactory" />
    public class SqlServerLocalRetrievalFactory : ILocalCodeRetrievalFactory
    {
        /// <summary>
        /// The DDB SQL builder factory
        /// </summary>
        private readonly IDdbSqlBuilderFactory _ddbSqlBuilderFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlServerLocalRetrievalFactory" /> class.
        /// </summary>
        public SqlServerLocalRetrievalFactory()
        {
            this._ddbSqlBuilderFactory = new DdbSqlBuilderFactory();
        }

        /// <summary>
        /// Gets the <see cref="T:Estat.Sri.Mapping.Api.Engine.ILocalCodeRetrieval" /> for the specified <paramref name="databaseType" />
        /// </summary>
        /// <param name="databaseType">Type of the database.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sri.Mapping.Api.Engine.ILocalCodeRetrieval" /> engine for the specified <paramref name="databaseType" />
        /// </returns>
        public ILocalCodeRetrieval GetEngine(string databaseType)
        {
            if (MappingStoreDefaultConstants.SqlServerName.Equals(databaseType))
            {
                return new DdbLocalCodeRetrieval(this._ddbSqlBuilderFactory);
            }

            return null;
        }
    }
}