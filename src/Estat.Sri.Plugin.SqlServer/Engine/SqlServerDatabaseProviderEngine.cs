// -----------------------------------------------------------------------
// <copyright file="SqlServerDatabaseProviderEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Utils.Helper;

namespace Estat.Sri.Plugin.SqlServer.Engine
{
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Plugin.SqlServer.Builder;

    /// <summary>
    /// The SQL server database provider engine.
    /// </summary>
    public class SqlServerDatabaseProviderEngine : IDatabaseProviderEngine
    {
        /// <summary>
        /// The name
        /// </summary>
        private static readonly string _name = MappingStoreDefaultConstants.SqlServerName;

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; } = _name;

        /// <summary>
        /// Gets the name of the provider.
        /// </summary>
        /// <value>
        /// The name of the provider.
        /// </value>
        public string ProviderName => DatabaseType.GetProviderName(_name);

        /// <summary>
        /// Gets a value indicating whether the parameters are read only.
        /// </summary>
        /// <value>
        /// <c>true</c> if parameters are read only; otherwise, <c>false</c>.
        /// </value>
        public bool ParametersAreReadOnly => false;

        /// <summary>
        /// Gets a value indicating whether to show the description association.
        /// </summary>
        /// <value>
        /// <c>true</c> if it should show the description association; otherwise, <c>false</c>.
        /// </value>
        public bool ShowDescriptionAssociation => true;

        /// <summary>
        /// Gets the browser.
        /// </summary>
        /// <value>
        /// The browser.
        /// </value>
        public IDatabaseSchemaBrowser Browser { get; } = new SqlServerSchemaBrowser();

        /// <summary>
        /// Gets the settings builder.
        /// </summary>
        /// <value>
        /// The settings builder.
        /// </value>
        public IConnectionSettingsBuilder SettingsBuilder { get; } = new SqlServerConnectionBuilder(_name);
    }
}