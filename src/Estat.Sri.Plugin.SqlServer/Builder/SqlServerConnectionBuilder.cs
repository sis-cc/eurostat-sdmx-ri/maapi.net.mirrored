// -----------------------------------------------------------------------
// <copyright file="SqlServerConnectionBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-04-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Utils.Helper;
using Estat.Sri.Utils.Manager;

namespace Estat.Sri.Plugin.SqlServer.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using log4net;
    using Mapping.MappingStore.Extension;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The SQL server connection builder.
    /// </summary>
    public class SqlServerConnectionBuilder : IConnectionSettingsBuilder
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(SqlServerConnectionBuilder));
        /// <summary>
        /// The server keyword
        /// </summary>
        private const string ServerKeyword = "Server";

        /// <summary>
        /// The port keyword
        /// </summary>
        private const string PortKeyword = "Port";

        /// <summary>
        /// The user identifier name
        /// </summary>
        private const string UserIdName = "USER ID";

        /// <summary>
        /// The database name
        /// </summary>
        private const string DatabaseName = "DATABASE";

        /// <summary>
        /// The password name
        /// </summary>
        private const string PasswordName = "PASSWORD";

        /// <summary>
        /// The integrated security true values
        /// </summary>
        private static readonly string[] _integratedSecurityTrueValues = { "yes", "true", "sspi" };

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlServerConnectionBuilder"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public SqlServerConnectionBuilder(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        private string Name { get; }

        /// <summary>
        /// Creates the connection entity.
        /// </summary>
        /// <returns>
        /// The connection entity
        /// </returns>
        public IConnectionEntity CreateConnectionEntity()
        {
            return CreateConnectionEntity((ConnectionStringSettings)null);
        }

        /// <summary>
        /// Creates the connection entity from the specified <see cref="T:System.Configuration.ConnectionStringSettings" />
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sri.Mapping.Api.Model.IConnectionEntity" />
        /// </returns>
        public IConnectionEntity CreateConnectionEntity(ConnectionStringSettings settings)
        {
            var providerName = DatabaseType.GetProviderName(Name);
            var builder = GetConnectionStringBuilder(providerName);
            var connectionEntity = BuildConnectionEntity(builder, settings);

            return connectionEntity;
        }

        /// <summary>
        /// Creates the connection string.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>
        /// The <see cref="T:System.Configuration.ConnectionStringSettings" />
        /// </returns>
        public ConnectionStringSettings CreateConnectionString(IConnectionEntity connection)
        {
            var providerName = DatabaseType.GetProviderName(Name);

            var builder = GetConnectionStringBuilder(providerName);
            if (builder == null)
            {
                throw new InvalidOperationException("Cannot build DbConnectionStringBuilder for provider: " + providerName);
            }

            var connectionParameterEntities = connection.Settings.ToDictionary(entry => entry.Key, entry => entry.Value, StringComparer.OrdinalIgnoreCase); ;

            // Java compatibility. Java sql server driver expects a "userid" but .net doesn't support this
            IConnectionParameterEntity userId;
            if (connectionParameterEntities.TryGetValue("userid", out userId))
            {
                // not supported in .NET driver
                connectionParameterEntities.Remove("userid");
                if (!connectionParameterEntities.ContainsKey(UserIdName) || connectionParameterEntities[UserIdName].IsItDefault())
                {
                    connectionParameterEntities[UserIdName] = userId;
                }
            }
            IConnectionParameterEntity server;
            if (connectionParameterEntities.TryGetValue(ServerKeyword, out server) && server?.Value != null)
            {
                var serverValue = server.Value.ToString();
                IConnectionParameterEntity port;
                if (connectionParameterEntities.TryGetValue(PortKeyword, out port) && port?.Value != null)
                {
                    int portValue;
                    if (int.TryParse(port.Value.ToString(), NumberStyles.Integer, CultureInfo.InvariantCulture, out portValue) && portValue > 0 && portValue < 65536)
                    {
                        serverValue += "," + port.Value;
                    }

                    connectionParameterEntities.Remove(PortKeyword);
                }

                builder.Add(ServerKeyword, serverValue);
                connectionParameterEntities.Remove(ServerKeyword);
            }

            foreach (var connectionParameterEntity in connectionParameterEntities)
            {
                if (!connectionParameterEntity.Value.IsItDefault())
                {
                    builder.Add(connectionParameterEntity.Key, connectionParameterEntity.Value.Value);
                }
            }

            return new ConnectionStringSettings(connection.Name, builder.ConnectionString, providerName);
        }

        /// <summary>
        /// Creates the connection string.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>The <see cref="ConnectionStringSettings"/></returns>
        public ConnectionStringSettings CreateConnectionString(DdbConnectionEntity connection)
        {
            var providerName = DatabaseType.GetProviderName(Name);

            return new ConnectionStringSettings(connection.Name, connection.AdoConnString, providerName);
        }

        /// <summary>
        /// Creates the DDB connection entity.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>The <see cref="DdbConnectionEntity"/></returns>
        public DdbConnectionEntity CreateDdbConnectionEntity(IConnectionEntity connection)
        {
            if (connection == null)
            {
                throw new ArgumentNullException(nameof(connection));
            }

            DdbConnectionEntity connectionEntity = new DdbConnectionEntity();
            connectionEntity.Name = connection.Name;
            connectionEntity.EntityId = connection.EntityId;
            connectionEntity.DbType = connection.DatabaseVendorType;
            connectionEntity.AdoConnString = this.CreateConnectionString(connection).ConnectionString;
            var dbName = connection.DbName ?? connection.GetValue<string>(DatabaseName);
            connectionEntity.DbUser = connection.GetValue<string>(UserIdName);
            connectionEntity.Password = connection.GetValue<string>(PasswordName);
            var server = connection.GetValue<string>(ServerKeyword) ?? connection.GetValue<string>("Data Source");
            IConnectionParameterEntity portValueParam;
            var port = string.Empty;
            if (connection.Settings.TryGetValue(PortKeyword, out portValueParam) && !(portValueParam.Value is default(int)))
            {
                port = portValueParam.Value?.ToString();
            }
            connectionEntity.JdbcConnString = JdbcConnectionStringHelper.GenerateJdbcString(connectionEntity.DbType, dbName, server, port);
            return connectionEntity;
        }

        /// <summary>
        /// Creates the connection entity.
        /// </summary>
        /// <param name="ddbConnection">The DDB connection.</param>
        /// <returns>The <see cref="IConnectionEntity"/></returns>
        public IConnectionEntity CreateConnectionEntity(DdbConnectionEntity ddbConnection)
        {
            if (ddbConnection == null)
            {
                throw new ArgumentNullException(nameof(ddbConnection));
            }

            if (string.IsNullOrEmpty(ddbConnection.Name))
            {
                throw new SdmxException("Ddb connection name can not be empty", SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError));
            }

            var providerName = DatabaseType.GetProviderName(ddbConnection.DbType);
            var connectionEntity = this.CreateConnectionEntity(
                new ConnectionStringSettings(ddbConnection.Name, ddbConnection.AdoConnString, providerName));
            connectionEntity.EntityId = ddbConnection.EntityId;
            connectionEntity.Name = ddbConnection.Name;
            connectionEntity.Description = ddbConnection.Description;
            connectionEntity.StoreId = ddbConnection.StoreId;
            return connectionEntity;
        }

        /// <summary>
        /// Gets the connection string builder.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <returns>The <see cref="DbConnectionStringBuilder"/></returns>
        private static DbConnectionStringBuilder GetConnectionStringBuilder(string providerName)
        {
            var factory = DbProviderFactories.GetFactory(providerName);
            var builder = factory.CreateConnectionStringBuilder();
            return builder;
        }

        /// <summary>
        /// Sets the server.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="connectionEntity">The connection entity.</param>
        private static void SetServer(string value, IConnectionEntity connectionEntity)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                var splitValue = value.Split(',');
                string server;
                int port = 0;
                if (splitValue.Length > 1)
                {
                    server = splitValue[0].Trim();
                    if (!int.TryParse(splitValue[1], out port))
                    {
                        port = 0;
                    }
                }
                else
                {
                    server = value;
                }

                ParameterType type = ParameterType.String;
                connectionEntity.AddSetting(ServerKeyword, type, server, false, true);
                ParameterType type1 = ParameterType.String;
                connectionEntity.AddSetting(PortKeyword, type1, port, false, true);
            }
        }

        /// <summary>
        /// Builds the connection entity.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="settings">The settings.</param>
        /// <returns>The <see cref="IConnectionEntity"/></returns>
        private IConnectionEntity BuildConnectionEntity(DbConnectionStringBuilder builder, ConnectionStringSettings settings)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            IConnectionEntity connectionEntity = new ConnectionEntity();
            connectionEntity.DatabaseVendorType = Name;
            var existingSettings = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            if (settings != null)
            {
                connectionEntity.Name = settings.Name;
                try
                {
                    builder.ConnectionString = settings.ConnectionString;
                }
                catch(Exception e)
                {
                    _logger.Error($"Trying to set connection string {settings.ConnectionString} failed", e);
                }
                existingSettings.UnionWith(builder.Cast<KeyValuePair<string, object>>().Select(pair => pair.Key));
            }

            var originalKeys = builder.Keys;
            if (originalKeys == null)
            {
                builder[ServerKeyword] = string.Empty;
                builder[DatabaseName] = string.Empty;
                builder[UserIdName] = string.Empty;
                builder[PasswordName] = string.Empty;
                builder["Integrated Security"] = false;
                originalKeys = builder.Keys;
                if (originalKeys == null)
                {
                    return connectionEntity;
                }
            }

            foreach (string originalKey in originalKeys)
            {
                var key = originalKey.ToUpperInvariant();
                var entry = builder[originalKey];
                var value = entry?.ToString().Trim();
                switch (key)
                {
                    case UserIdName:
                        connectionEntity.AddSetting(originalKey, ParameterType.String, value, false, true);
                        break;
                    case "INITIAL CATALOG":
                    case DatabaseName:
                        connectionEntity.DbName = value;
                        connectionEntity.AddSetting("Database", ParameterType.String, value, false, true);

                        break;
                    case PasswordName:
                        connectionEntity.AddSetting(originalKey, ParameterType.Password, value, false, true);
                        break;
                    case "DATA SOURCE":
                    case "SERVER":
                    case "ADDRESS":
                    case "NETWORK ADDRESS":
                        SetServer(value, connectionEntity);
                        break;
                    case "INTEGRATED SECURITY":
                        bool isTrue = false;
                        if (!string.IsNullOrWhiteSpace(value))
                        {
                            isTrue = _integratedSecurityTrueValues.Contains(value, StringComparer.OrdinalIgnoreCase);
                        }

                        connectionEntity.AddSetting("Integrated Security", ParameterType.Boolean, isTrue, true, false, false);

                        break;
                    default:
                        connectionEntity.AddSetting(originalKey, value, existingSettings, true);

                        break;
                }
            }

            return connectionEntity;
        }
    }
}