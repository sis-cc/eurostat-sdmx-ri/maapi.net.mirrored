// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="EUROSTAT">
//   Date Created : 2017-11-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Sdmx.DataMonitorJob
{
    using DryIoc;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Data.Monitor;
    using Estat.Sri.Data.Registrator;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Common;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using Estat.Sdmxsource.Extension.Engine.WebService;

    /// <summary>
    /// The Data Monitor Job is a console application that checks for data update and if it does it will try submit registration
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The default store identifier
        /// </summary>
        private const string DefaultStoreId = "MappingStoreServer";
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(Program));
        
        /// <summary>
        /// The list of connection identifiers to ignore
        /// </summary>
        private static string[] _ignoreConnectionIdList = { "LocalSqlServer", "OraAspNetConString", "LocalMySqlServer" };

        static void Main(string[] args)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            _log.InfoFormat("{0} v{1}", assembly.GetName().Name, assembly.GetName().Version);
            _log.Info("How to schedule tasks on Windows : https://technet.microsoft.com/en-us/library/cc748993(v=ws.11).aspx");
            string storeId = DefaultStoreId;
            if (args.Length == 1)
            {
                storeId = args[0];
            }

            var container = Registration();
            var configurationStoreManager = container.Resolve<IConfigurationStoreManager>();

            var allSettings = configurationStoreManager.GetSettings<ConnectionStringSettings>().Where(c => !_ignoreConnectionIdList.Contains(c.Name)).ToArray();
            if (allSettings.Length == 0)
            {
                _log.Error("No connection strings found. Please edit the DataMonitorJob.exe.config file ");
                Environment.Exit(-1);
                return;
            }

            // We use IConfigurationStoreManager to re-use the load config from other location feature
            var connectionStringSettings = allSettings.FirstOrDefault(c => c.Name.Equals(storeId, StringComparison.OrdinalIgnoreCase));
            if (connectionStringSettings == null)
            {
                _log.ErrorFormat("Connection string with name {0} could not be found. Use one of the following:", storeId);

                foreach (var setting in allSettings)
                {
                    _log.Error(setting.Name);
                }
                Environment.Exit(-1);
                return;
            }
            var database = new Database(connectionStringSettings);
            var registrySoapsubmitter = container.Resolve<SubmitRegistrationEngine>();
            var structureSubmitter = container.Resolve<IStructureSubmitter>();
            DataRegistratorSettings registratorSettings = new DataRegistratorSettings();
            IDataRegistrator dataRegistrator = new DataRegistrator(storeId, registrySoapsubmitter, structureSubmitter, registratorSettings);
            DataMonitorSettings settings = new DataMonitorSettings();
            IDataMonitor dataMonitor = new DataMonitor(database, dataRegistrator, settings);

            try
            {
                dataMonitor.Monitor();
                _log.Info("Operation completed without errors");
            }
            catch (DbException ex)
            {
                _log.Error("A database related error occurred. This could be either a configuration issue or a bug in the software", ex);
                Environment.Exit(-2);
            }
            catch (SdmxException ex)
            {
                _log.Error("An SDMX related error occurred. Most likely there was an issue transmitting the Data Registration", ex);
                Environment.Exit(-3);
            }
            catch (WebException ex)
            {
                _log.Error("A comminucation error with Registry occurred. The Provision Agreement might not be present at the Registry, the username and/or password may not be correct or the registry URL might not be correct", ex);
                Environment.Exit(-4);
            }
        }


        /// <summary>
        /// Perform the registrations to the container
        /// </summary>
        /// <returns>The <c>DryIOC</c> container.</returns>
        private static Container Registration()
        {
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["RetrieverFactory"]);
            var container =
                new Container(
                    rules =>
                    rules.With(FactoryMethod.ConstructorWithResolvableArguments).WithoutThrowOnRegisteringDisposableTransient());
            var assemblies = new List<Assembly>(new[] { typeof(IDatabaseProviderManager).Assembly });
            ////Console.WriteLine("Current plugin search path");
            ////var currentDirectory = GetCurrentDirectory();
            ////Console.WriteLine(currentDirectory);
            ////DirectoryInfo currentDir = new DirectoryInfo(currentDirectory);
            ////var fileInfos = currentDir.GetFiles("*.Plugin.*.dll");
            ////foreach (var fileInfo in fileInfos)
            ////{
            ////    Console.WriteLine(fileInfo.Name);
            ////    assemblies.Add(Assembly.LoadFile(fileInfo.FullName));
            ////}

            container.RegisterMany(assemblies, type => !typeof(IEntity).IsAssignableFrom(type));
            container.Register<IStructureParsingManager, StructureParsingManager>();
            container.Register<ConnectionStringFactory>();
            container.Register(made: Made.Of(r => ServiceInfo.Of<ConnectionStringFactory>(), m => m.Function));
            container.Register<IStructureSubmitFactory, StructureSubmitMappingStoreFactory>();
            container.Register<IStructureSubmitter, StructureSubmitter>();
            container.Register<SubmitRegistrationEngine>();
            container.Register<IRetrievalEngineContainerFactory, RetrievalEngineContainerFactory>();
            container.Unregister<IEntityAuthorizationManager>();

            return container;
        }

        /////// <summary>
        /////// Gets the current directory.
        /////// </summary>
        /////// <returns>The current directory</returns>
        ////private static string GetCurrentDirectory()
        ////{
        ////    var fileName = typeof(Program).Assembly.Location;
        ////    if (fileName != null)
        ////    {
        ////        return new FileInfo(fileName).DirectoryName;
        ////    }

        ////    return Directory.GetCurrentDirectory();
        ////}

        /// <summary>
        /// Helper class used for injection
        /// </summary>
        private class ConnectionStringFactory
        {
            readonly IConfigurationStoreManager _configurationStoreManager;

            /// <summary>
            /// Initializes a new instance of the <see cref="ConnectionStringFactory" /> class.
            /// </summary>
            /// <param name="configurationStoreManager">The configuration store manager.</param>
            public ConnectionStringFactory(IConfigurationStoreManager configurationStoreManager)
            {
                if (configurationStoreManager == null)
                {
                    throw new ArgumentNullException(nameof(configurationStoreManager));
                }

                _configurationStoreManager = configurationStoreManager;
            }

            public Func<string, ConnectionStringSettings> Function
            {
                get
                {
                    return x => this._configurationStoreManager.GetSettings<ConnectionStringSettings>().First(s => s.Name.Equals(x));
                }
            }
        }
    }
}
