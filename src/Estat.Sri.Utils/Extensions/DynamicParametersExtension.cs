using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace Estat.Sri.Utils.Extensions
{
    public static class DynamicParametersExtension{
        public static Dictionary<string, object> ToDictionary(this DynamicParameters dynamicParameters)
        {
            return dynamicParameters.ParameterNames.ToDictionary(parameterName => parameterName, parameterName => dynamicParameters.Get<object>(parameterName));
        }
    }
}