// -----------------------------------------------------------------------
// <copyright file="DapperDatabaseExtension.cs" company="EUROSTAT">
//   Date Created : 2018-2-28
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Runtime.CompilerServices;
using Estat.Sri.Utils.Helper;
using Estat.Sri.Utils.Model;

namespace Estat.Sri.MappingStore.Store.Extension
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;
    using System.Transactions;
    using Dapper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Utils;
    using log4net;
    using Estat.Sri.Utils.Extensions;

    /// <summary>
    ///     This class contains extensions for using <see cref="Dapper" /> with <see cref="Database" />
    /// </summary>
    public static class DapperDatabaseExtension
    {

        private static readonly ILog _log = LogManager.GetLogger(typeof(DapperDatabaseExtension));

        static DapperDatabaseExtension()
        {
            SqlMapper.AddTypeHandler(typeof(bool), new BooleanTypeHandler());
        }
        

        /// <summary>
        /// Queries the specified database.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// The <see cref="IEnumerable{dynamic}" />
        /// </returns>
        public static IEnumerable<dynamic> Query(this Database database, string sqlQuery, object parameters, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            var connection = database.CreateConnection();
            if ((connection.State & ConnectionState.Open) == 0)
            {
                using (connection)
                using (var unused = new SqlQueryLogger(sqlQuery, new Dictionary<string, object>() { { "", parameters } }, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    connection.Open();
                    return connection.Query(sqlQuery, parameters);
                }
            }
            else
            {
                using (var unused = new SqlQueryLogger(sqlQuery, new Dictionary<string, object>() { { "", parameters } }, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    return connection.Query(sqlQuery, parameters, database.Transaction);
                }
            }
        }

        /// <summary>
        /// Queries the specified database.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// The result of type
        /// </returns>
        public static TResult ExecuteScalar<TResult>(this Database database, string sqlQuery, object parameters, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            var connection = database.CreateConnection();
            if ((connection.State & ConnectionState.Open) == 0)
            {
                using (connection)
                using (var unused = new SqlQueryLogger(sqlQuery, null, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    connection.Open();
                    return connection.ExecuteScalar<TResult>(sqlQuery, parameters);
                }
            }
            else
            {
                using (var unused = new SqlQueryLogger(sqlQuery, null, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    return connection.ExecuteScalar<TResult>(sqlQuery, parameters, database.Transaction);
                }
            }
        }

        ///// <summary>
        /////     Queries the specified database.
        ///// </summary>
        ///// <typeparam name="T">The type of the result</typeparam>
        ///// <param name="database">The database.</param>
        ///// <param name="sqlQuery">The SQL query.</param>
        ///// <returns>
        /////     The <see cref="IEnumerable{T}" />
        ///// </returns>
        //public static IEnumerable<T> Query<T>(this Database database, string sqlQuery, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        //{
        //    using (var connection = database.CreateConnection())
        //    using (var unused = new SqlQueryLogger(sqlQuery, null, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
        //    {
        //        connection.Open();
        //        return connection.Query<T>(sqlQuery);
        //    }
        //}

        /// <summary>
        ///     Queries the specified database.
        /// </summary>
        /// <typeparam name="T1">The type of the 1.</typeparam>
        /// <typeparam name="T2">The type of the 2.</typeparam>
        /// <param name="database">The database.</param>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <param name="map">The map function.</param>
        /// <param name="splitOn">The split on.</param>
        /// <returns>
        ///     The <see cref="IEnumerable{T1}" />
        /// </returns>
        public static IEnumerable<T1> Query<T1, T2>(this Database database, string sqlQuery, Func<T1, T2, T1> map, string splitOn, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            var connection = database.CreateConnection();
            if ((connection.State & ConnectionState.Open) == 0)
            {
                using (connection)
                using (var unused = new SqlQueryLogger(sqlQuery, null, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    connection.Open();
                    return connection.Query(sqlQuery, map, splitOn);
                }
            }
            else
            {
                using (var unused = new SqlQueryLogger(sqlQuery, null, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    return connection.Query(sqlQuery, map, splitOn, database.Transaction);
                }
            }
        }

        public static IEnumerable<TResult> Query<TResult>(this Database database, string query, DynamicParameters dynamicParameters, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            var connection = database.CreateConnection();
            if ((connection.State & ConnectionState.Open) == 0)
            {
                using (connection)
                using (var unused = new SqlQueryLogger(query, dynamicParameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    return connection.Query<TResult>(query, dynamicParameters);
                }
            }
            else
            {
                using (var unused = new SqlQueryLogger(query, dynamicParameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    return connection.Query<TResult>(query, dynamicParameters, database.Transaction);
                }
            }
        }

        /// <summary>
        /// Executes a query using dapper to map to <typeparamref name="TResult"/>,
        /// without closing the open connection.
        /// </summary>
        /// <typeparam name="TResult">The type to map the results to.</typeparam>
        /// <param name="database">the database object</param>
        /// <param name="query">The query to execute</param>
        /// <param name="dynamicParameters">The parameters of the query</param>
        /// <param name="memberName"></param>
        /// <param name="sourceFilePath"></param>
        /// <param name="sourceLineNumber"></param>
        /// <returns>A collection of <typeparamref name="TResult"/>.</returns>
        public static IEnumerable<TResult> QueryUsingInternalDb<TResult>(this Database database, string query, DynamicParameters dynamicParameters, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            using (var unused = new SqlQueryLogger(query, dynamicParameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
            {
                return database.CreateConnection().Query<TResult>(query, dynamicParameters);
            }
        }

        public static IEnumerable<TResult> QueryWithKey<TResult>(this Database database, string queryFormat, long primaryKey, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            var parameterName = database.BuildParameterName(nameof(primaryKey));
            string query = string.Format(CultureInfo.InvariantCulture, queryFormat, parameterName);
            using (var connection = database.CreateConnection())
            using (var unused = new SqlQueryLogger(query, new Dictionary<string, object>()
            {
                {parameterName,primaryKey }
            }, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
            {
                return connection.Query<TResult>(query, new { primaryKey });
            }
        }

        public static IEnumerable<TResult> Query<TResult>(this Database database, string query, object dynamicObject, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            var connection = database.CreateConnection();
            if ((connection.State & ConnectionState.Open) == 0)
            {
                using (connection)
                using (var unused = new SqlQueryLogger(query, new Dictionary<string, object>()
            {
                {"",dynamicObject }
            }, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    return connection.Query<TResult>(query, dynamicObject);
                }
            }
            else
            {
                using (var unused = new SqlQueryLogger(query, new Dictionary<string, object>()
            {
                {"",dynamicObject }
            }, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    return connection.Query<TResult>(query, dynamicObject, database.Transaction);
                }
            }
        }


        public static IEnumerable<TResult> Query<TResult>(this Database database, string query, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            var connection = database.CreateConnection();
            if ((connection.State & ConnectionState.Open) == 0)
            {
                using (connection)
                using (var unused = new SqlQueryLogger(query, null, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    return connection.Query<TResult>(query);
                }
            }
            else
            {
                using (var unused = new SqlQueryLogger(query, null, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    return connection.Query<TResult>(query,database.Transaction);
                }
            }
        }
        /// <summary>
        ///     Queries the specified database.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <returns>
        ///     The <see cref="IEnumerable{dynamic}" />
        /// </returns>
        public static IEnumerable<dynamic> Query(this Database database, string query, DynamicParameters dynamicParameters, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            var connection = database.CreateConnection();
            if ((connection.State & ConnectionState.Open) == 0)
            {
                using (connection)
                using (var unused = new SqlQueryLogger(query, dynamicParameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    return connection.Query(query, dynamicParameters);
                }
            }
            else
            {
                using (var unused = new SqlQueryLogger(query, dynamicParameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                {
                    return connection.Query(query, dynamicParameters,database.Transaction);
                }
            }
        }

        //public static IEnumerable<TResult> Query<TResult>(this Database database, string query, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        //{
        //    using (var connection = database.CreateConnection())
        //    using (var unused = new SqlQueryLogger(query, null, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
        //    {
        //        return connection.Query<TResult>(query);
        //    }
        //}

        /// <summary>
        /// Executes the command.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="commandDefinitions">The command definition.</param>
        /// <returns>Either the last inserted id or number of row affected</returns>
        public static long ExecuteCommandsWithReturnId(this Database database, IEnumerable<CommandDefinition> commandDefinitions, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            long returnValue = -1;
            using (var scope = new TransactionScope())
            using (var connection = database.CreateConnection())

            {
                var query = string.Empty;
                DynamicParameters parameters = null;
                try
                {
                    connection.Open();
                    foreach (var commandDefinition in commandDefinitions)
                    {
                        query = commandDefinition.CommandText;
                        parameters = (DynamicParameters)commandDefinition.Parameters;
                        using (var unused = new SqlQueryLogger(query, parameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                        {
                            connection.Execute(commandDefinition);
                            returnValue = parameters.ParameterNames.Contains("p_pk") ? parameters.Get<long>("p_pk") : returnValue;
                        }
                    }

                    scope.Complete();
                }
                catch (Exception e)
                {
                    _log.Error(e);
                    _log.ErrorFormat("Query that failed <<{0}>>", query);
                    if (parameters != null)
                    {
                        foreach (var dynamicParameter in parameters.ParameterNames)
                        {
                            _log.ErrorFormat("name: '{0}', value: '{1}", dynamicParameter, parameters.Get<object>(dynamicParameter));
                        }

                    }
                    throw new MadbExceptionWithQuery(e.Message, e) { Query = query };
                }
            }

            return returnValue;
        }


        public static void ExecuteCommandsUnderTransaction(this Database database, CommandDefinition[] commandDefinitions, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            using (var connection = database.CreateConnection())
            {
                var query = string.Empty;
                try
                {
                    foreach (var commandDefinition in commandDefinitions)
                    {
                        var parameters = (DynamicParameters)commandDefinition.Parameters;
                        query = commandDefinition.CommandText;
                        using (var unused = new SqlQueryLogger(query, parameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                        {
                            connection.Execute(commandDefinition);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new MadbExceptionWithQuery(e.Message, e) { Query = query };
                }

            }
        }

        public static void ExecuteCommands(this Database database, CommandDefinition[] commandDefinitions, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            using (var scope = new TransactionScope())
            using (var connection = database.CreateConnection())
            {
                var query = string.Empty;
                try
                {
                    connection.Open();
                    foreach (var commandDefinition in commandDefinitions)
                    {
                        var parameters = (DynamicParameters)commandDefinition.Parameters;
                        query = commandDefinition.CommandText;
                        using (var unused = new SqlQueryLogger(query, parameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                        {
                            connection.Execute(commandDefinition);
                        }
                    }

                    scope.Complete();
                }
                catch (Exception e)
                {
                    throw new MadbExceptionWithQuery(e.Message, e) { Query = query };
                }

            }
        }

        public static long ExecuteCommandWithConnectio(this Database database, IEnumerable<CommandDefinition> commandDefinitions, DbConnection connection, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            var query = string.Empty;
            try
            {
                foreach (var commandDefinition in commandDefinitions)
                {
                    query = commandDefinition.CommandText;
                    var parameters = (DynamicParameters)commandDefinition.Parameters;
                    using (var unused = new SqlQueryLogger(query, parameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                    {
                        connection.Execute(commandDefinition);
                        return parameters.ParameterNames.Contains("p_pk") ? parameters.Get<long>("p_pk") : -1;
                    }
                }
            }
            catch (Exception e)
            {
                throw new MadbExceptionWithQuery(e.Message, e) { Query = query };
            }

            return -1;
        }

        public static void ExecuteCommandsWithConnectio(this Database database, IEnumerable<CommandDefinition> commandDefinitions, DbConnection connection, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            var query = string.Empty;
            try
            {
                foreach (var commandDefinition in commandDefinitions)
                {
                    query = commandDefinition.CommandText;
                    var parameters = (DynamicParameters)commandDefinition.Parameters;
                    using (var unused = new SqlQueryLogger(query, parameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                    {
                        connection.Execute(commandDefinition);
                    }
                }
            }
            catch (Exception e)
            {
                throw new MadbExceptionWithQuery(e.Message, e) { Query = query };
            }

        }

        public static long ExecuteCommandWithConnectionAndTransaction(this Database database, IEnumerable<CommandDefinition> commandDefinitions, DbConnection connection, DbTransaction transaction, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            var query = string.Empty;
            try
            {
                foreach (var commandDefinition in commandDefinitions)
                {
                    query = commandDefinition.CommandText;
                    var parameters = (DynamicParameters)commandDefinition.Parameters;
                    using (var unused = new SqlQueryLogger(query, parameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                    {
                        connection.Execute(commandDefinition);
                        return parameters.ParameterNames.Contains("p_pk") ? parameters.Get<long>("p_pk") : -1;
                    }
                }
            }
            catch (Exception e)
            {
                throw new MadbExceptionWithQuery(e.Message, e) { Query = query };
            }

            return -1;
        }

        public static void ExecuteCodeWithTransaction(this Database database, Func<DbConnection, IEnumerable<CommandDefinition>> func, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {

            using (var scope = new TransactionScope())
            using (var connection = database.CreateConnection())
            {
                var query = string.Empty;
                long foreignKey = 0;
                try
                {

                    var commandDefinitions = func(connection);
                    connection.Open();
                    foreach (var commandDefinition in commandDefinitions)
                    {
                        query = commandDefinition.CommandText;
                        var parameters = (DynamicParameters)commandDefinition.Parameters;
                        using (var unused = new SqlQueryLogger(query, parameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                        {
                            connection.Execute(commandDefinition);
                            foreignKey = parameters.ParameterNames.Contains("p_pk") ? parameters.Get<long>("p_pk") : -1;
                        }
                    }

                    scope.Complete();
                }
                catch (Exception e)
                {
                    throw new MadbExceptionWithQuery(e.Message, e) { Query = query };
                }
            }
        }

        /// <summary>
        /// Gets the <c>ID</c> to <c>ENTITY_ID</c> map.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="sqlQueryFormat">The SQL query format. It must output two fields. One string named <c>ID</c> and one long named <c>ENTITY_ID</c></param>
        /// <param name="parentEntityId">The parent entity identifier.</param>
        /// <returns>A dictionary with key the values of <c>ID</c> and value the values of <c>ENTITY_ID</c></returns>
        public static Dictionary<string, long> GetIdToEntityIdMap(this ContextWithTransaction context, string sqlQueryFormat, long parentEntityId)
        {
            var parameterName = context.Database.BuildParameterName(nameof(parentEntityId));
            var query = string.Format(CultureInfo.InvariantCulture, sqlQueryFormat, parameterName);
            return context.Connection.Query(query, new { parentEntityId }).Select(o => new Tuple<string, long>(o.ID, o.ENTITY_ID)).ToDictionary(tuple => tuple.Item1, tuple => tuple.Item2, StringComparer.Ordinal);
        }

    }
}