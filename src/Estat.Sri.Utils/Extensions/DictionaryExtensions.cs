// -----------------------------------------------------------------------
// <copyright file="DictionaryExtensions.cs" company="EUROSTAT">
//   Date Created : 2022-10-27
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Estat.Sri.Utils.Extensions
{
    /// <summary>
    /// Collection of <see cref="Dictionary{TKey, TValue}"/> extensions
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// Try to get the value mapped to <paramref name="key"/> if it exists, if not add <paramref name="key"/> with <paramref name="value"/>
        /// </summary>
        /// <typeparam name="TKey">The type of the key</typeparam>
        /// <typeparam name="TValue">The type of the value</typeparam>
        /// <param name="dictionary">The dictionary</param>
        /// <param name="key">The key</param>
        /// <param name="value">The value</param>
        /// <returns>The current value of <paramref name="key"/> in <paramref name="dictionary"/>, else the <paramref name="value"/></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static TValue GetOrAdd<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (dictionary is null)
            {
                throw new ArgumentNullException(nameof(dictionary));
            }

            if (dictionary.TryGetValue(key, out TValue currentValue))
            {
                return currentValue;
            }

            dictionary.Add(key, value);
            return value;
        }
    }
}
