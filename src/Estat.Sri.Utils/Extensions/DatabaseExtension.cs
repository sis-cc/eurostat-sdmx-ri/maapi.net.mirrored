// -----------------------------------------------------------------------
// <copyright file="DatabaseExtension.cs" company="EUROSTAT">
//   Date Created : 2013-07-29
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;

    using Dapper;

    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Utils.Model;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     This class contains various database extension methods.
    /// </summary>
    public static class DatabaseExtension
    {
        /// <summary>
        ///     Determines whether the specified <paramref name="reader" /> has a field with the specified
        ///     <paramref name="fieldName" />
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns>True if there is a field with name <paramref name="fieldName" />; otherwise false</returns>
        public static bool HasFieldName(this IDataRecord reader, string fieldName)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).Equals(fieldName, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     Convert the specified <paramref name="value" /> to a value suitable for Mapping Store.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public static int ToDbValue(this bool value)
        {
            return !value ? 0 : 1;
        }

        /// <summary>
        ///     Convert the specified <paramref name="value" /> to a value suitable for Mapping Store.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public static object ToDbValue(this bool? value)
        {
            if (!value.HasValue)
            {
                return DBNull.Value;
            }

            return value.Value.ToDbValue();
        }

        /// <summary>
        ///     Convert the specified <paramref name="value" /> to a value suitable for Mapping Store.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public static object ToDbValue(this long value)
        {
            if (value < 0)
            {
                return DBNull.Value;
            }

            return value;
        }

        /// <summary>
        ///     Convert the specified <paramref name="value" /> to a value suitable for Mapping Store.
        /// </summary>
        /// <typeparam name="T">The type of</typeparam>
        /// <param name="value">The value.</param>
        /// <returns>
        ///     The normalized for DB object.
        /// </returns>
        public static object ToDbValue<T>(this T? value) where T : struct
        {
            return ToDbValue(value, DBNull.Value);
        }

        /// <summary>
        ///     Convert the specified <paramref name="value" /> to a value suitable for Mapping Store.
        /// </summary>
        /// <typeparam name="T">The type of</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        ///     The normalized for DB object.
        /// </returns>
        public static object ToDbValue<T>(this T? value, object defaultValue) where T : struct
        {
            if (!value.HasValue)
            {
                return defaultValue;
            }

            return value.Value;
        }

        /// <summary>
        /// Creates the command. This is mostly used for DDB access since PcAxis will fail.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="sql">The SQL.</param>
        /// <param name="originalParameters">The original parameters.</param>
        /// <returns>The DB Command</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "Mostly OK. We need more parameters. The SQL is build dynamically.")]
        public static DbCommand CreateCommand(this DbConnection connection, string sql, IList<DbParameter> originalParameters)
        {
            DbCommand cmd = null;
            try
            {
                cmd = connection.CreateCommand();
                cmd.CommandText = sql;
                foreach (var originalParameter in originalParameters)
                {
                    var dbParameter = cmd.CreateParameter();
                    dbParameter.DbType = originalParameter.DbType;
                    dbParameter.Direction = originalParameter.Direction;
                    dbParameter.ParameterName = originalParameter.ParameterName;
                    dbParameter.Value = originalParameter.Value;
                    cmd.Parameters.Add(dbParameter);
                }

                return cmd;
            }
            catch (Exception)
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }

                throw;
            }
        }

        public static DbCommand CreateDisseminationDatabaseCommand(this DbConnection connection, string sql, IList<DbParameter> originalParameters)
        {
            var cmd = CreateCommand(connection, sql, originalParameters);
            if (HeaderScope.ReturnDisseminationDbSql)
            {
                HeaderScope.DisseminationDbSqls.Add(cmd.CommandText);
                return null;
            }
            else
            {
                return cmd;
            }
        }

        /// <summary>
        /// Executes the specified SQL statement.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="sqlStatementFormat">The SQL statement format.</param>
        /// <param name="simpleParameters">The simple parameters.</param>
        /// <returns>The number of records affected.</returns>
        public static int Execute(this Database database, string sqlStatementFormat, params SimpleParameter[] simpleParameters)
        {
            DbParameter[] dbParameters = new DbParameter[simpleParameters.Length];
            for(int i=0; i < simpleParameters.Length; i++)
            {
                var simpleParameter = simpleParameters[i];
                var name = string.Format(CultureInfo.InvariantCulture, "p{0:00}", i);
                DbParameter dbParameter = database.CreateInParameter(name, simpleParameter.DatabaseType, simpleParameter.Value);
                dbParameters[i] = dbParameter;
            }

            return database.ExecuteNonQueryFormat(sqlStatementFormat, dbParameters);
        }

        /// <summary>
        /// Queries the specified SQL statement.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="sqlStatementFormat">The SQL statement format.</param>
        /// <param name="simpleParameters">The simple parameters.</param>
        /// <returns>The number of records affected.</returns>
        public static IEnumerable<TResult> QuerySimple<TResult>(this Database database, string sqlStatementFormat, params SimpleParameter[] simpleParameters)
        {
            var dbParameters = new DynamicParameters();
            var parameterNames = new string[simpleParameters.Length];
            for(int i=0; i < simpleParameters.Length; i++)
            {
                var simpleParameter = simpleParameters[i];
                var name = string.Format(CultureInfo.InvariantCulture, "p{0:00}", i);
                var parameterName = database.BuildParameterName(name);
                parameterNames[i] = parameterName;
                dbParameters.Add(parameterName, simpleParameter.Value, simpleParameter.DatabaseType);
            }

            var query = string.Format(CultureInfo.InvariantCulture, sqlStatementFormat, parameterNames);
            return database.Query<TResult>(query, dbParameters);
        }
        
        public static IEnumerable<TResult> Query<TResult>(this Database database, string query, DynamicParameters dynamicParameters)
        {
            return UsingConnection(database, (connection) => connection.Query<TResult>(query, dynamicParameters, transaction:database.Transaction));
        }

        public static IEnumerable<TResult> Query<TResult>(this Database database, string query, object dynamicObject)
        {
            return UsingConnection(database, (connection) => connection.Query<TResult>(query, dynamicObject));
        }

        public static IEnumerable<dynamic> Query(this Database database, string query, DynamicParameters dynamicParameters)
        {
            return UsingConnection(database, (connection) => connection.Query(query, dynamicParameters));
        }

        public static IEnumerable<TResult> Query<TResult>(this Database database, string query)
        {
            return UsingConnection(database, (connection) => connection.Query<TResult>(query,transaction:database.Transaction));
        }

        /// <summary>
        /// Execute <paramref name="action"/> using the connection.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="action">The action.</param>
        private static TReturn UsingConnection<TReturn>(Database database, Func<DbConnection, TReturn> action)
        {
            bool alreadyOpen = true;
            var connection = database.CreateConnection();
            try
            {
                alreadyOpen = connection.State.HasFlag(ConnectionState.Open);
                if (!alreadyOpen)
                {
                    connection.Open();
                }

                return action(connection);
            }
            finally
            {
                if (!alreadyOpen)
                {
                    connection.Dispose();
                }
            }
        }
    }
}