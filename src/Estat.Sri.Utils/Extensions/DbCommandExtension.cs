// -----------------------------------------------------------------------
// <copyright file="DbCommandExtension.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Sri.MappingStoreRetrieval.Extensions
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Runtime.CompilerServices;

    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Utils.Helper;
    using Estat.Sri.Utils.Model;

    using log4net;

    /// <summary>
    ///     Extensions for <see cref="DbCommand" />
    /// </summary>
    public static class DbCommandExtension
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DbCommandExtension));

        public static IDataReader ExecuteReaderAndLog(
            this IDbCommand cmd,
            CommandBehavior behavior,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            using (var unused = new SqlQueryLogger(cmd, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
            {
                return cmd.ExecuteReader(behavior);
            }
        }

        public static async Task<DbDataReader> ExecuteReaderAsyncAndLog(
            this DbCommand cmd,
            CommandBehavior behavior,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            using (var unused = new SqlQueryLogger(cmd, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
            {
                return await cmd.ExecuteReaderAsync(behavior);
            }
        }

        public static IDataReader ExecuteReaderAndLog(
            this IDbCommand cmd,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            using (var unused = new SqlQueryLogger(cmd, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
            {
                return cmd.ExecuteReader();
            }
        }

        public static async Task<DbDataReader> ExecuteReaderAsyncAndLog(
            this DbCommand cmd,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            using (var unused = new SqlQueryLogger(cmd, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
            {
                return await cmd.ExecuteReaderAsync();
            }
        }
        public static object ExecuteScalarAndLog(this IDbCommand cmd, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            using (var unused = new SqlQueryLogger(cmd, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
            {
                return cmd.ExecuteScalar();
            }
        }

        public static int ExecuteNonQueryAndLog(this IDbCommand cmd, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            using (var unused = new SqlQueryLogger(cmd, new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
            {
                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        ///     An exception free <see cref="DbCommand.Cancel()" /> to workaround issues with drivers, notably MySQL.
        /// </summary>
        /// <param name="command">The command.</param>
        public static void SafeCancel(this DbCommand command)
        {
            try
            {
                if (command == null)
                {
                    throw new ArgumentNullException("command");
                }

                command.Cancel();
            }
            catch (DbException e)
            {
                _log.Warn(
                    "Error while trying to cancel the command. In some cases, e.g. MySQL, it is safe to ignore the error.", 
                    e);
            }
        }

        /// <summary>
        ///     The method retrieve a value from the reader and cast it to <see cref="long" /> data type
        ///     In case the retrieved value is null, the returned value is <see cref="long.MinValue" />
        /// </summary>
        /// <param name="reader">
        ///     The source for reading the data
        /// </param>
        /// <param name="fieldName">
        ///     The name of the column containing the value
        /// </param>
        /// <returns>
        ///     The extracted value as <see cref="long" />
        /// </returns>
        public static long GetSafeInt64(this IDataReader reader, string fieldName)
        {
            return DataReaderHelper.GetInt64(reader, fieldName);
        }

        /// <summary>
        ///     The method retrieve a value from the reader and cast it to <see cref="int" /> data type
        ///     In case the retrieved value is null, the returned value is <see cref="int.MinValue" />
        /// </summary>
        /// <param name="reader">
        ///     The source for reading the data
        /// </param>
        /// <param name="fieldName">
        ///     The name of the column containing the value
        /// </param>
        /// <returns>
        ///     The extracted value as <see cref="int" />
        /// </returns>
        public static int GetSafeInt32(this IDataReader reader, string fieldName)
        {
            return DataReaderHelper.GetInt32(reader, fieldName);
        }

        /// <summary>
        /// Gets the integer.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns>An integer</returns>
        /// <exception cref="System.InvalidCastException">Not an integer</exception>
        public static long GetInteger(this IDataReader reader, string fieldName)
        {
            var ordinal = reader.GetOrdinal(fieldName);
            return GetInteger(reader, ordinal);
        }

        /// <summary>
        /// Gets the integer.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="ordinal">The ordinal.</param>
        /// <returns>
        /// An integer
        /// </returns>
        /// <exception cref="System.InvalidCastException">Not an integer</exception>
        public static long GetInteger(this IDataReader reader, int ordinal)
        {
            var dbType = reader.GetFieldType(ordinal);
            switch (Type.GetTypeCode(dbType))
            {
                case TypeCode.Boolean:
                    return reader.GetBoolean(ordinal) ? 1 : 0;
                case TypeCode.Int16:
                case TypeCode.UInt16:
                    return reader.GetInt16(ordinal);
                case TypeCode.UInt32:
                case TypeCode.Int32:
                    return reader.GetInt32(ordinal);
                case TypeCode.UInt64:
                case TypeCode.Int64:
                    return reader.GetInt64(ordinal);
                case TypeCode.SByte:
                case TypeCode.Byte:
                    return reader.GetByte(ordinal);
            }

            throw new InvalidCastException("Not an integer");
        }

        /// <summary>
        ///     The method retrieve a value from the reader and cast it to string data type
        ///     In case the retrieved value is null, the returned value is also null
        /// </summary>
        /// <param name="reader">
        ///     The source for reading the data
        /// </param>
        /// <param name="fieldName">
        ///     The name of the column containing the value
        /// </param>
        /// <returns>
        ///     The extracted value as string
        /// </returns>
        public static string GetSafeString(this IDataReader reader, string fieldName)
        {
            return DataReaderHelper.GetString(reader, fieldName);
        }

        /// <summary>
        ///     The method retrieve a value from the reader and cast it to <see cref="bool" /> data type
        ///     In case the retrieved value is null, the returned <see cref="bool" /> value is false
        /// </summary>
        /// <param name="reader">
        ///     The source for reading the data
        /// </param>
        /// <param name="fieldName">
        ///     The name of the column containing the value
        /// </param>
        /// <returns>
        ///     The extracted value as <see cref="bool" />
        /// </returns>
        public static bool GetSafeBool(this IDataReader reader, string fieldName)
        {
            return DataReaderHelper.GetBoolean(reader, fieldName);
        }
    }
}