using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using System.Runtime.CompilerServices;
using System.Text;
using Dapper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Utils.Helper;
using Estat.Sri.Utils.Model;
using log4net;
using System.Transactions;
using Estat.Sri.Utils.Extensions;
using System.Linq;

namespace Estat.Sri.Utils
{
    public class ContextWithTransaction : IDisposable
    {
        private readonly TransactionScope _scope;
        private readonly DbConnection _connection;
        private readonly bool _nestedConnection;
        private bool _isCompleted;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public ContextWithTransaction(Database database)
        {
            Database = database;
            this._scope = new TransactionScope(TransactionScopeOption.Required, new System.TimeSpan(0, 30, 0));
            this._connection = database.CreateConnection();
            if ((_connection.State & ConnectionState.Open) != ConnectionState.Open)
            {
                try
                {
                    this._connection.Open();
                }
                catch (Exception e)
                {
                    LogManager.GetLogger(this.GetType()).Error(e);
                    throw;
                }
            }
            else
            {
                // Database.CreateConnection() returns open connection if it is already in transaction
                _nestedConnection = true;
            }
            this.DatabaseUnderContext = new Database(database, this._connection);
        }

        public DbConnection Connection
        {
            get
            {
                return _connection;
            }
        }
        /// <summary>
        /// Has the transaction scope been completed
        /// </summary>
        public bool IsCompleted => _isCompleted;

        public Database Database { get; private set; }

        public Database DatabaseUnderContext { get; private set; }

        /// <summary>
        /// Executes the command.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="commandDefinitions">The command definition.</param>
        /// <returns>Either the last inserted id or number of row affected</returns>
        public long ExecuteCommandsWithReturnId(IEnumerable<CommandDefinition> commandDefinitions, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            long returnValue = -1;
            var query = string.Empty;
            try
            {
                foreach (var commandDefinition in commandDefinitions)
                {
                    query = commandDefinition.CommandText;
                    var parameters = (DynamicParameters)commandDefinition.Parameters;
                    using (var unused = new SqlQueryLogger(query, parameters.ToDictionary(), new CallerInfo(memberName, sourceFilePath, sourceLineNumber)))
                    {
                        this._connection.Execute(commandDefinition);
                        returnValue = parameters.ParameterNames.Contains("p_pk") ? parameters.Get<long>("p_pk") : returnValue;
                    }
                }

            }
            catch (Exception e)
            {
                throw new MadbExceptionWithQuery(e.Message, e) { Query = query };
            }


            return returnValue;
        }

        public void Complete()
        {
            this._scope.Complete();
            this._isCompleted = true;
        }
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this._scope.Dispose();
            if (!_nestedConnection)
            {
                this._connection.Dispose();
            }
        }
    }
}
