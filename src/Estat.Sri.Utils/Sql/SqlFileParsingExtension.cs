// -----------------------------------------------------------------------
// <copyright file="SqlFileParsingExtension.cs" company="EUROSTAT">
//   Date Created : 2018-2-20
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Utils.Sql
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    public static class SqlFileParsingExtension
    {
        /// <summary>
        /// Splits the statements.
        /// </summary>
        /// <param name="lines">The lines.</param>
        /// <returns>
        /// The SQL Statement
        /// </returns>
        public static IEnumerable<string> SplitStatements(this IEnumerable<string> lines)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string readLine in lines)
            {
                switch (readLine.ToUpperInvariant())
                {
                    case ";":
                    case "/":
                    case "GO":
                        if (sb.Length > 0)
                        {
                            yield return sb.ToString();
                            sb.Length = 0;
                        }

                        break;
                    default:
                        if (readLine.Length > 0)
                        {
                            sb.Append(readLine);
                            sb.Append('\n');
                        }

                        break;
                }
            }
        }

        /// <summary>
        /// Reads the lines from the specified stream.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns>The lines from the stream</returns>
        public static IEnumerable<string> ReadLines(this Stream stream)
        {
            if (stream != null)
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        yield return line;
                    }
                }
            }
        }
    }
}
