namespace Estat.Sri.Utils.Config
{
    using System;

    public static class ConfigurationProvider
    { 
        public static bool CreateStubCategory { get; set; }

        public static bool AutoDeleteMappingSets { get; set; }

        public static Tuple<string, string> DisseminationDbConfig { get; set; }
    }
}
