﻿// -----------------------------------------------------------------------
// <copyright file="ConnectionStringRetriever.cs" company="EUROSTAT">
//   Date Created : 2016-10-17
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Config
{
    using System;
    using System.Configuration;
    using System.Globalization;

    /// <summary>
    /// The connection string helper.
    /// </summary>
    public class ConnectionStringRetriever
    {
        /// <summary>
        /// The format
        /// </summary>
        private readonly string _format;

        /// <summary>
        /// The format provider
        /// </summary>
        private readonly string _formatProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionStringRetriever"/> class.
        /// </summary>
        public ConnectionStringRetriever() : this("CSTR_{0}", "CSTR_{0}_Provider")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionStringRetriever"/> class.
        /// </summary>
        /// <param name="format">The connection string format.</param>
        /// <param name="formatProvider">The format provider.</param>
        public ConnectionStringRetriever(string format, string formatProvider)
        {
            this._format = format;
            this._formatProvider = formatProvider;
        }

        /// <summary>
        /// Gets the connection string settings.
        /// </summary>
        /// <param name="connectionName">
        /// Name of the connection.
        /// </param>
        /// <returns>
        /// The <see cref="ConnectionStringSettings"/>.
        /// </returns>
        public ConnectionStringSettings GetConnectionStringSettings(string connectionName)
        {
            var environmentVariableName = string.Format(CultureInfo.InvariantCulture, this._format, connectionName);
            var environmentConnectionStringValue = Environment.GetEnvironmentVariable(environmentVariableName);
            if (!string.IsNullOrWhiteSpace(environmentConnectionStringValue))
            {
                var environmentProviderVariable = string.Format(CultureInfo.InvariantCulture, this._formatProvider, connectionName);
                var provider = Environment.GetEnvironmentVariable(environmentProviderVariable) ?? MappingStoreDefaultConstants.SqlServerProvider;
                return new ConnectionStringSettings(connectionName, environmentConnectionStringValue, provider);
            }

            return ConfigurationManager.ConnectionStrings[connectionName];
        }
    }
}