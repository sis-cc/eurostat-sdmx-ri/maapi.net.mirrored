// -----------------------------------------------------------------------
// <copyright file="DbProviderFactories.cs" company="EUROSTAT">
//   Date Created : 2018-11-21
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Reflection;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.Utils.Helper;

namespace Estat.Sri.Utils.Manager
{
    /// <summary>
    /// Class that should simulate the DbProviderFactories from .net framework
    /// </summary>
    public class DbProviderFactories
    {
        private static readonly ConcurrentDictionary<string, DbProviderFactory> _cache;
        private static readonly Dictionary<string, string> _defaultFactory = new Dictionary<string, string>(StringComparer.Ordinal);
        private static readonly Dictionary<string, string> _defaultDll = new Dictionary<string, string>(StringComparer.Ordinal);
        static DbProviderFactories() {
            _cache = new ConcurrentDictionary<string, DbProviderFactory>(StringComparer.OrdinalIgnoreCase);
            // some defaults in order not to break existing configurations:
            _defaultFactory.Add(MappingStoreDefaultConstants.SqlServerProvider, "System.Data.SqlClient.SqlClientFactory");
            _defaultFactory.Add(MappingStoreDefaultConstants.OracleManagedProviderOdp, "Oracle.ManagedDataAccess.Client.OracleClientFactory");
            _defaultFactory.Add(MappingStoreDefaultConstants.MySqlProvider,  "MySql.Data.MySqlClient.MySqlClientFactory");

            _defaultDll.Add(MappingStoreDefaultConstants.SqlServerProvider, "System.Data.SqlClient");
            _defaultDll.Add(MappingStoreDefaultConstants.OracleManagedProviderOdp, "Oracle.ManagedDataAccess");
            _defaultDll.Add(MappingStoreDefaultConstants.MySqlProvider,  "MySql.Data");
            _defaultDll.Add(MappingStoreDefaultConstants.MySqlConnectorName, "MySqlConnector");
        }

        /// <summary>
        /// Get the provider name factory provided it is either provided in configuration <see cref="MappingStoreConfigSection"/> or can be guessed
        /// </summary>
        /// <param name="providerName"></param>
        /// <returns></returns>
        public static DbProviderFactory GetFactory(string providerName)
        {
            if (string.IsNullOrWhiteSpace(providerName))
            {
                // IIS admin, seems to create connection strings without provider name
                providerName = "System.Data.SqlClient";
            }

            return _cache.GetOrAdd(providerName, GetFactoryInternal);
        }

        private static DbProviderFactory GetFactoryInternal(string providerName)
        {
            MastoreProviderMappingSetting factoryClassParams = null;
            foreach (MastoreProviderMappingSetting mapping in DatabaseType.Mappings)
            {
                if (mapping.Provider.Equals(providerName, StringComparison.OrdinalIgnoreCase))
                {
                    factoryClassParams = mapping;
                }
            }

            if (factoryClassParams != null)
            {
                if (string.IsNullOrEmpty(factoryClassParams.DllName))
                {
                    factoryClassParams.DllName = GuessTheDllName(providerName);
                }

                if (string.IsNullOrEmpty(factoryClassParams.FactoryClass))
                {
                    factoryClassParams.FactoryClass = GuessTheFactoryClass(providerName);
                }
             
                return GetDbProviderFactory(factoryClassParams.FactoryClass, factoryClassParams.DllName);
            }

            throw new NotSupportedException($"Unsupported ProviderFactory {providerName}. Please check the configuration file");
        }

        /// <summary>
        /// Try Guess the name of the DLL
        /// </summary>
        /// <param name="providerName"></param>
        /// <returns></returns>
        private static string GuessTheDllName(string providerName)
        {
            string dllName;
            if (_defaultDll.TryGetValue(providerName, out dllName))
            {
                return dllName;
            }

            // dll names of db provider seem to contain the first two parts of the provider name
            int lastDot = providerName.LastIndexOf('.');
            if (lastDot > 0)
            {
                return providerName.Substring(0, lastDot);
            }

            // No idea what to do here
            return providerName;
        }

        private static string GuessTheFactoryClass(string providerName)
        {
            string factory;
            if (_defaultFactory.TryGetValue(providerName, out factory))
            {
                return factory;
            }

            // factory class of provider X.Y.Z seem to be named X.Y.Z.ZFactory
            int lastDot = providerName.LastIndexOf('.');
            if (lastDot > 0)
            {
                string lastPart = providerName.Substring(lastDot);
                return string.Format(CultureInfo.InvariantCulture, "{0}{1}Factory", providerName, lastPart);
            }

            // No idea what to do here
            return providerName + "Factory";
        }

        private static DbProviderFactory GetDbProviderFactory(string dbProviderFactoryTypename, string assemblyName)
        {
            var typeQualifiedName = string.Format(CultureInfo.InvariantCulture, "{0}, {1}", dbProviderFactoryTypename, assemblyName);
            var instance = GetStaticProperty(typeQualifiedName);
            if (instance == null)
            {
                var a = LoadAssembly(assemblyName);
                if (a != null)
                {
                    instance = GetStaticProperty(a, dbProviderFactoryTypename);
                }
            }

            if (instance == null)
            {
                throw new InvalidOperationException($"Unable to retrieve DbProviderFactory from {dbProviderFactoryTypename}");
            }

            return instance;
        }

        private static Assembly LoadAssembly(string assemblyName)
        {
            return Assembly.Load(assemblyName);
        }

        private static DbProviderFactory GetStaticProperty(string typeQualifiedName)
        {
            var type = Type.GetType(typeQualifiedName, false, false);
            if (type == null)
            {
                return null;
            }

            return GetStaticProperty(type);
        }

        private static DbProviderFactory GetStaticProperty(Assembly assembly, string typeName)
        {
            var type = assembly.GetType(typeName, false, false);
            if (type == null)
            {
                return null;
            }

            return GetStaticProperty(type);
        }

        private static DbProviderFactory GetStaticProperty(Type type)
        {
            var instanceProperty = type.InvokeMember("Instance", BindingFlags.Static | BindingFlags.Public | BindingFlags.GetField | BindingFlags.GetProperty, null, type, null);
            if (instanceProperty != null)
            {
                return instanceProperty as DbProviderFactory;
            }

            return null;
        }
    }
}