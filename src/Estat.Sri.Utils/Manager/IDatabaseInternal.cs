// -----------------------------------------------------------------------
// <copyright file="IDatabaseInternal.cs" company="EUROSTAT">
//   Date Created : 2018-8-14
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Manager
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    ///     The DatabaseInternal interface.
    /// </summary>
    internal interface IDatabaseInternal
    {
        /// <summary>
        ///     Executes the non query command
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The number of affected records.</returns>
        int ExecuteNonQuery(DbCommand command);

        /// <summary>
        ///     Executes and return the <see cref="IDataReader" /> from the <paramref name="command" />
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The executed reader.</returns>
        IDataReader ExecuteReader(IDbCommand command);

        /// <summary>
        ///     Executes the scalar command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The result of the <see cref="DbCommand.ExecuteScalar" /></returns>
        object ExecuteScalar(DbCommand command);

        /// <summary>
        ///     Gets the connection.
        /// </summary>
        /// <returns>The connection instance</returns>
        DbConnection GetConnection();

        /// <summary>
        ///     Opens the connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        void OpenConnection(IDbConnection connection);

        /// <summary>
        ///     Setup  the command.
        /// </summary>
        /// <param name="command">The command.</param>
        void SetupCommand(IDbCommand command);

        /// <summary>
        /// Create command
        /// </summary>
        /// <returns>The <see cref="DbCommand"/></returns>
        DbCommand CreateCommand();
    }
}