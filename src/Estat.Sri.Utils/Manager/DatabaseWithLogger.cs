// -----------------------------------------------------------------------
// <copyright file="DatabaseWithLogger.cs" company="EUROSTAT">
//   Date Created : 2018-8-14
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Manager
{
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.Utils.Helper;
    using Estat.Sri.Utils.Model;

    /// <summary>
    /// Decorator that logs Execute* methods
    /// </summary>
    class DatabaseWithLogger : IDatabaseInternal
    {
        private readonly IDatabaseInternal _decorated;

        private readonly CallerInfo _caller;

        public DatabaseWithLogger(IDatabaseInternal decorated, CallerInfo caller)
        {
            _decorated = decorated;
            _caller = caller;
        }

        public int ExecuteNonQuery(DbCommand command)
        {
            using (SqlQueryLogger unused = new SqlQueryLogger(command, _caller))
            {
                return _decorated.ExecuteNonQuery(command);
            }
        }

        public IDataReader ExecuteReader(IDbCommand command)
        {
            SqlQueryLogger logger = new SqlQueryLogger(command, _caller);
            return new LoggingDataReader(_decorated.ExecuteReader(command), logger);
        }

        public object ExecuteScalar(DbCommand command)
        {
            using (SqlQueryLogger unused = new SqlQueryLogger(command, _caller))
            {
                return _decorated.ExecuteScalar(command);
            }
        }

        public DbConnection GetConnection()
        {
            return _decorated.GetConnection();
        }

        public void OpenConnection(IDbConnection connection)
        {
            _decorated.OpenConnection(connection);
        }

        public void SetupCommand(IDbCommand command)
        {
            _decorated.SetupCommand(command);
        }

        public DbCommand CreateCommand()
        {
            return _decorated.CreateCommand();
        }
    }
}