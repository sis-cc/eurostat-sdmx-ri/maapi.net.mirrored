// -----------------------------------------------------------------------
// <copyright file="DatabaseInTransactionScope.cs" company="EUROSTAT">
//   Date Created : 2018-8-14
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Manager
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    ///     The database transactional.
    /// </summary>
    internal class DatabaseInTransactionScope : IDatabaseInternal
    {
        private readonly DbConnection _connection;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DatabaseTransactional" /> class.
        /// </summary>
        /// <param name="connection">The connection</param>
        public DatabaseInTransactionScope(DbConnection connection)
        {
            _connection = connection;
        }

        /// <summary>
        ///     The execute non query.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        public int ExecuteNonQuery(DbCommand command)
        {
            this.SetupCommand(command);
            return command.ExecuteNonQuery();
        }

        /// <summary>
        ///     The execute reader.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The <see cref="IDataReader" />.
        /// </returns>
        public IDataReader ExecuteReader(IDbCommand command)
        {
            return command.ExecuteReader(CommandBehavior.Default);
        }

        /// <summary>
        ///     The execute scalar.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public object ExecuteScalar(DbCommand command)
        {
            this.SetupCommand(command);
            return command.ExecuteScalar();
        }

        /// <summary>
        ///     The get connection.
        /// </summary>
        /// <returns>
        ///     The <see cref="DbConnection" />.
        /// </returns>
        public DbConnection GetConnection()
        {
            return this._connection;
        }

        /// <summary>
        ///     The open connection.
        /// </summary>
        /// <param name="connection">
        ///     The connection.
        /// </param>
        public void OpenConnection(IDbConnection connection)
        {
        }

        /// <summary>
        ///     Setup the command.
        /// </summary>
        /// <param name="command">The command.</param>
        public void SetupCommand(IDbCommand command)
        {
            if (command.Connection != null)
            {
                return;
            }

            command.Connection = this._connection;
        }

        /// <inheritdoc />
        public DbCommand CreateCommand()
        {
            return this._connection.CreateCommand();
        }
    }
}