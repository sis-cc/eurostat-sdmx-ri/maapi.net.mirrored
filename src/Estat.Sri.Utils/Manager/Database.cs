// -----------------------------------------------------------------------
// <copyright file="Database.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
// ReSharper disable ExplicitCallerInfoArgument

using System.Linq;
using Estat.Sri.Utils.Manager;

namespace Estat.Sri.MappingStoreRetrieval.Manager
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using System.Text.RegularExpressions;
    using Dapper;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.Utils.Model;

    using log4net;

    /// <summary>
    ///     The database.
    /// </summary>
    public class Database
    {
        /// <summary>
        ///     The database setting collection.
        /// </summary>
        private static readonly DatabaseSettingCollection _databaseSettingCollection =
            ConfigManager.Config.GeneralDatabaseSettings;

        /// <summary>
        ///     The regular expression that removes the <c>dbo.</c> from a string.
        /// </summary>
        private static readonly Regex _dboRemover = new Regex(
            @"\bdbo\.", 
            RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Singleline);

        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(Database));

        /// <summary>
        ///     The parameter markers per provider.
        /// </summary>
        /// <remarks>
        ///     <c>System.Data.SqlClient</c> is hardcoded because of a bug in <c>System.Data.SqlClient</c>
        /// </remarks>
        private static readonly ConcurrentDictionary<string, string> _parameterMarkersPerProvider =
            new ConcurrentDictionary<string, string>(StringComparer.Ordinal);

        /// <summary>
        ///     The mapping store connection string settings.
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        ///     The internal database helper class
        /// </summary>
        private readonly IDatabaseInternal _databaseInternal;

        /// <summary>
        ///     The factory to create connections.
        /// </summary>
        private readonly DbProviderFactory _factory;

        /// <summary>
        ///     The _provider name.
        /// </summary>
        private readonly string _providerName;

        /// <summary>
        ///     The method that normalizes the schema in query.
        /// </summary>
        private readonly Func<string, string> _schemaNormalizer;

        /// <summary>
        ///     The parameter marker format.
        /// </summary>
        private string _parameterMarkerFormat;

        /// <summary>
        /// Holds the caller info
        /// </summary>
        private CallerInfo _callerInfo;

        public Database(Database database, DbConnection connection)
        {
            if (connection == null)
            {
                throw new ArgumentNullException(nameof(connection));
            }

            // copy stuff from specified database
            this._providerName = database._providerName;
            this._connectionStringSettings = database._connectionStringSettings;
            this._factory = database._factory;
            this._schemaNormalizer = this.GetQueryNormalizer();
            this._parameterMarkerFormat = database._parameterMarkerFormat;

            this._databaseInternal = new DatabaseInTransactionScope(connection);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Database" /> class.
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <param name="transaction">
        ///     The transaction.
        /// </param>
        public Database(Database database, DbTransaction transaction)
        {
            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            if (transaction == null)
            {
                throw new ArgumentNullException("transaction");
            }

            // copy stuff from specified database
            this._providerName = database._providerName;
            this._connectionStringSettings = database._connectionStringSettings;
            this._factory = database._factory;
            this._schemaNormalizer = this.GetQueryNormalizer();
            this._parameterMarkerFormat = database._parameterMarkerFormat;

            this._databaseInternal = new DatabaseTransactional(transaction);
            Transaction = transaction;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Database" /> class.
        /// </summary>
        /// <param name="connectionStringSettings">
        ///     The mapping store connection string settings.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="connectionStringSettings" /> is null
        /// </exception>
        public Database(ConnectionStringSettings connectionStringSettings)
        {
            if (connectionStringSettings == null)
            {
                throw new ArgumentNullException("connectionStringSettings");
            }

            this._connectionStringSettings = connectionStringSettings;

            this._providerName = connectionStringSettings.ProviderName;

            try
            {
                this._factory = DbProviderFactories.GetFactory(this._providerName);
                
            }
            catch (Exception e)
            {
                _log.ErrorFormat("Error while trying to load provider : {0}", this._providerName);
                _log.Error(this._providerName, e);
                throw;
            }

            this._schemaNormalizer = this.GetQueryNormalizer();
            this._databaseInternal = new DatabaseNonTransactional(connectionStringSettings, this._factory);
        }

        /// <summary>
        ///     Gets the provider name.
        /// </summary>
        public string ProviderName
        {
            get
            {
                return this._providerName;
            }
        }

        /// <summary>
        /// Gets the name of the connection
        /// </summary>
        public string Name
        {
            get
            {
                return this._connectionStringSettings.Name;
            }
        }

        /// <summary>
        /// Gets the transaction.
        /// </summary>
        /// <value>
        /// The transaction.
        /// </value>
        public DbTransaction Transaction { get; }

        /// <summary>
        /// Gets the connection string associated with this instance
        /// </summary>
        public ConnectionStringSettings ConnectionStringSettings => _connectionStringSettings;

        /// <summary>
        ///     Return an <see cref="IDataReader" />. It will open the <paramref name="connection" /> if it is closed.
        /// </summary>
        /// <param name="connection">
        ///     The connection.
        /// </param>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The <see cref="IDataReader" />.
        /// </returns>
        public static IDataReader ExecuteReader(DbConnection connection, DbCommand command)
        {
            if (connection == null)
            {
                throw new ArgumentNullException("connection");
            }

            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            if ((connection.State & ConnectionState.Open) != ConnectionState.Open)
            {
                connection.Open();
            }

            command.Connection = connection;
            return command.ExecuteReader(CommandBehavior.Default);
        }

        /// <summary>
        ///     Create and add an input <see cref="DbParameter" /> to the specified <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="name">
        ///     The parameter name.
        /// </param>
        /// <param name="dbType">
        ///     The parameter type.
        /// </param>
        /// <param name="value">
        ///     The parameter value.
        /// </param>
        public void AddInParameter(DbCommand command, string name, DbType dbType, object value)
        {
            DbParameter dbParameter = this.CreateInParameter(name, dbType);
            if (dbParameter != null)
            {
                if (command == null)
                {
                    throw new ArgumentNullException("command");
                }

                dbParameter.Value = value ?? DBNull.Value;
                command.Parameters.Add(dbParameter);
            }
        }

        /// <summary>
        ///     Build the parameter name using the DB vendor specific format.
        /// </summary>
        /// <param name="name">
        ///     The name.
        /// </param>
        /// <returns>
        ///     The parameter name using the DB vendor specific format.
        /// </returns>
        public string BuildParameterName(string name)
        {
            if (this._parameterMarkerFormat == null)
            {
                this._parameterMarkerFormat = _parameterMarkersPerProvider.GetOrAdd(
                    this._providerName, 
                    this.RetrieveMarkerFormat);
            }

            return string.Format(CultureInfo.InvariantCulture, this._parameterMarkerFormat, name);
        }

        /// <summary>
        ///     Returns the SQL in a string with the <paramref name="parameters" /> names applied to the
        ///     <paramref name="queryFormat" />.
        /// </summary>
        /// <param name="queryFormat">
        ///     The query format.
        /// </param>
        /// <param name="parameters">
        ///     The parameters.
        /// </param>
        /// <returns>
        ///     The SQL in a string with the <paramref name="parameters" /> names applied to the <paramref name="queryFormat" />.
        /// </returns>
        public string BuildQuery(string queryFormat, params DbParameter[] parameters)
        {
            if (parameters == null || parameters.Length == 0)
            {
                return queryFormat;
            }

            var names = new object[parameters.Length];
            for (int i = 0; i < parameters.Length; i++)
            {
                var dbParameter = parameters[i];
                names[i] = dbParameter.ParameterName;
            }

            var query = string.Format(CultureInfo.InvariantCulture, queryFormat, names);
            return query;
        }

        /// <summary>
        ///     An exception free <see cref="DbCommand.Cancel()" /> to workaround issues with drivers, notably MySQL.
        /// </summary>
        /// <param name="command">The command.</param>
        public void CancelSafe(DbCommand command)
        {
            // TODO make cancel a config option.
            command.SafeCancel();
        }

        /// <summary>
        ///     Create command of <paramref name="commandType" /> with the specified <paramref name="commandText" />
        /// </summary>
        /// <param name="commandType">
        ///     The command type.
        /// </param>
        /// <param name="commandText">
        ///     The command text.
        /// </param>
        /// <returns>
        ///     The <see cref="DbCommand" />.
        /// </returns>
        public DbCommand CreateCommand(CommandType commandType, string commandText)
        {
            DbCommand cmd = null;
            try
            {
                cmd = this._databaseInternal.CreateCommand();
                if (cmd != null)
                {
                    cmd.CommandType = commandType;
                    DatabaseSetting databaseSetting = _databaseSettingCollection[this._providerName];
                    if (databaseSetting != null && databaseSetting.CommandTimeout >= 0)
                    {
                        cmd.CommandTimeout = databaseSetting.CommandTimeout;
                    }

                    commandText = this.NormalizeQuerySchema(commandText);
                    //commandText = this.ReplaceIsEqualVersion(commandText);
                    cmd.CommandText = commandText;
                    this._databaseInternal.SetupCommand(cmd);
                    this.SetupProviderSpecific(cmd);
                }
            }
            catch
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }

                throw;
            }

            return cmd;
        }

        /// <summary>
        ///     Creates connection for the database
        /// </summary>
        /// <returns>
        ///     The <see cref="DbConnection" />.
        /// </returns>
        public DbConnection CreateConnection()
        {
            return this._databaseInternal.GetConnection();
        }

        /// <summary>
        ///     Create an input <see cref="DbParameter" /> and return it
        /// </summary>
        /// <param name="name">
        ///     The parameter name.
        /// </param>
        /// <param name="dbType">
        ///     The parameter type.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />.
        /// </returns>
        public DbParameter CreateInParameter(string name, DbType dbType)
        {
            DbParameter dbParameter = this._factory.CreateParameter();
            if (dbParameter != null)
            {
                dbParameter.DbType = dbType;
                dbParameter.Direction = ParameterDirection.Input;
                dbParameter.ParameterName = this.BuildParameterName(name);
            }

            return dbParameter;
        }

        /// <summary>
        /// Create a <see cref="DbParameter" /> and return it
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="name">The name.</param>
        /// <param name="dbType">The DB type of the parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The <see cref="DbParameter" />.
        /// </returns>
        public DbParameter CreateInParameter<TValue>(string name, DbType dbType, TValue value)
        {
            DbParameter parameter = this.CreateInParameter(name, dbType);
            parameter.Value = value;

            return parameter;
        }

        /// <summary>
        ///     Executes the specified <paramref name="sqlStatement" /> with the specified <paramref name="parameters" />
        /// </summary>
        /// <param name="sqlStatement">
        ///     The SQL statement to execute
        /// </param>
        /// <param name="parameters">
        ///     The parameters. Can be null.
        /// </param>
        /// <returns>
        ///     The return value of <paramref name="sqlStatement" />
        /// </returns>
        public int ExecuteNonQuery(string sqlStatement, IList<DbParameter> parameters)
        {
            try
            {
                using (var command = this.GetSqlStringCommand(sqlStatement, parameters))
                {
                    return this.GetInternalDatabase().ExecuteNonQuery(command);
                }
            }
            catch (Exception e)
            {
                _log.Error("While ExecuteNonQuery", e);
                throw;
            }
        }

        /// <summary>
        ///     Executes the specified <paramref name="sqlStatement" /> with the specified <paramref name="parameters" />
        /// </summary>
        /// <param name="sqlStatement">
        ///     The SQL statement to execute
        /// </param>
        /// <param name="transaction">
        ///     The transaction.
        /// </param>
        /// <param name="parameters">
        ///     The parameters. Can be null.
        /// </param>
        /// <returns>
        ///     The return value of <paramref name="sqlStatement" />
        /// </returns>
        public int ExecuteNonQuery(string sqlStatement, DbTransaction transaction, IList<DbParameter> parameters)
        {
            try
            {
                IDatabaseInternal databaseInternal = new DatabaseTransactional(transaction);
                using (var command = this.GetSqlStringCommand(sqlStatement, parameters))
                {
                    databaseInternal.SetupCommand(command);
                    return databaseInternal.ExecuteNonQuery(command);
                }
            }
            catch (Exception e)
            {
                _log.Error("While ExecuteNonQuery", e);
                throw;
            }
        }

        /// <summary>
        ///     Executes the specified <paramref name="sqlStatement" /> with the specified <paramref name="parameters" />
        /// </summary>
        /// <param name="sqlStatement">
        ///     The SQL statement to execute
        /// </param>
        /// <param name="transaction">
        ///     The transaction.
        /// </param>
        /// <param name="parameters">
        ///     The parameters. Can be null.
        /// </param>
        /// <returns>
        ///     The return value of <paramref name="sqlStatement" />
        /// </returns>
        public int ExecuteNonQueryFormat(
            string sqlStatement, 
            DbTransaction transaction, 
            params DbParameter[] parameters)
        {
            try
            {
                IDatabaseInternal databaseInternal = new DatabaseTransactional(transaction);
                using (var command = this.GetSqlStringCommandFormat(sqlStatement, parameters))
                {
                    databaseInternal.SetupCommand(command);
                    return databaseInternal.ExecuteNonQuery(command);
                }
            }
            catch (Exception e)
            {
                _log.Error("While ExecuteNonQuery", e);
                throw;
            }
        }

        /// <summary>
        ///     Executes the specified <paramref name="sqlStatement" /> with the specified <paramref name="parameters" />
        /// </summary>
        /// <param name="sqlStatement">
        ///     The SQL statement to execute
        /// </param>
        /// <param name="parameters">
        ///     The parameters. Can be null.
        /// </param>
        /// <returns>
        ///     The return value of <paramref name="sqlStatement" />
        /// </returns>
        public int ExecuteNonQueryFormat(string sqlStatement, params DbParameter[] parameters)
        {
            try
            {
                using (var command = this.GetSqlStringCommandFormat(sqlStatement, parameters))
                {
                    this._databaseInternal.SetupCommand(command);
                    return this.GetInternalDatabase().ExecuteNonQuery(command);
                }
            }
            catch (Exception e)
            {
                _log.Error("While ExecuteNonQuery", e);
                throw;
            }
        }

        /// <summary>
        ///     Executes the specified <paramref name="sqlStatement" /> with the specified <paramref name="parameters" />
        /// </summary>
        /// <param name="sqlStatement">
        ///     The SQL statement to execute
        /// </param>
        /// <param name="parameters">
        ///     The parameters. Can be null.
        /// </param>
        /// <returns>
        ///     The return value of <paramref name="sqlStatement" />
        /// </returns>
        public int ExecuteNonQueryFormat(string sqlStatement, params long[] parameters)
        {
            DbParameter[] dbParameters = CreateInParameters(parameters);
            return this.ExecuteNonQueryFormat(sqlStatement, dbParameters);
        }
        /// <summary>
        /// Log the next statement (if enabled)
        /// </summary>
        /// <returns>This instance</returns>
        public Database UsingLogger(CallerInfo callerInfo)
        {
            this._callerInfo = callerInfo;
            return this;
        }

        /// <summary>
        /// Log the next statement (if enabled)
        /// </summary>
        /// <param name="memberName"></param>
        /// <param name="sourceFilePath"></param>
        /// <param name="sourceLineNumber"></param>
        /// <returns>This instance</returns>
        public Database UsingLogger(
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            this._callerInfo = new CallerInfo(memberName, sourceFilePath, sourceLineNumber);
            return this;
        }

        /// <summary>
        /// Return an <see cref="IDataReader" />.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="memberName">Name of the member.</param>
        /// <param name="sourceFilePath">The source file path.</param>
        /// <param name="sourceLineNumber">The source line number.</param>
        /// <returns>The <see cref="IDataReader" />.</returns>
        /// <exception cref="ArgumentNullException">command</exception>
        public IDataReader ExecuteReader(DbCommand command, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            DbConnection connection = null;
            try
            {
                connection = this.CreateConnection();
                this._databaseInternal.OpenConnection(connection);
                command.Connection = connection;
                this._databaseInternal.SetupCommand(command);
            }
            catch (Exception e)
            {
                if (connection != null)
                {
                    connection.Dispose();
                }

                _log.Error("While ExecuteReader", e);

                throw;
            }

            //// TODO we set this here because there are too many usages of ExecuteReader
            _callerInfo = new CallerInfo(memberName, sourceFilePath, sourceLineNumber);
            return this.GetInternalDatabase().ExecuteReader(command);
        }

        /// <summary>
        ///     Executes the specified <paramref name="query" /> with the specified <paramref name="parameters" />
        /// </summary>
        /// <param name="query">
        ///     The SQL query to execute
        /// </param>
        /// <param name="parameters">
        ///     The parameters. Can be null.
        /// </param>
        /// <returns>
        ///     The return value of <paramref name="query" />
        /// </returns>
        public object ExecuteScalar(string query, IList<DbParameter> parameters)
        {
            try
            {
                using (var command = this.GetSqlStringCommand(query, parameters))
                {
                    return this.GetInternalDatabase().ExecuteScalar(command);
                }
            }
            catch (Exception e)
            {
                _log.Error("While ExecuteScalar", e);
                throw;
            }
        }

        /// <summary>
        ///     Executes the specified <paramref name="query" /> with the specified <paramref name="parameters" />
        /// </summary>
        /// <param name="query">
        ///     The SQL query to execute
        /// </param>
        /// <param name="transaction">
        ///     The transaction.
        /// </param>
        /// <param name="parameters">
        ///     The parameters. Can be null.
        /// </param>
        /// <returns>
        ///     The return value of <paramref name="query" />
        /// </returns>
        public object ExecuteScalar(string query, DbTransaction transaction, IList<DbParameter> parameters)
        {
            try
            {
                IDatabaseInternal databaseInternal = new DatabaseTransactional(transaction);
                using (var command = this.GetSqlStringCommand(query, parameters))
                {
                    databaseInternal.SetupCommand(command);
                    return databaseInternal.ExecuteScalar(command);
                }
            }
            catch (Exception e)
            {
                _log.Error("While ExecuteNonQuery", e);
                throw;
            }
        }

        /// <summary>
        ///     Executes the specified <paramref name="query" /> with the specified <paramref name="parameters" />
        /// </summary>
        /// <param name="query">
        ///     The SQL query to execute
        /// </param>
        /// <param name="parameters">
        ///     The parameters. Can be null.
        /// </param>
        /// <returns>
        ///     The return value of <paramref name="query" />
        /// </returns>
        public object ExecuteScalarFormat(string query, params DbParameter[] parameters)
        {
            try
            {
                using (var command = this.GetSqlStringCommandFormat(query, parameters))
                {
                    return this.GetInternalDatabase().ExecuteScalar(command);
                }
            }
            catch (Exception e)
            {
                _log.Error("While ExecuteScalar", e);
                throw;
            }
        }

        /// <summary>
        ///     Executes the specified <paramref name="query" /> with the specified <paramref name="parameters" />
        /// </summary>
        /// <param name="query">
        ///     The SQL query to execute
        /// </param>
        /// <param name="parameters">
        ///     The parameters. Can be null.
        /// </param>
        /// <returns>
        ///     The return value of <paramref name="query" />
        /// </returns>
        public object ExecuteScalarFormat(string query, params long[] parameters)
        {
            DbParameter[] dbParameters = CreateInParameters(parameters);
            return this.ExecuteScalarFormat(query, dbParameters);
        }

        /// <summary>
        ///  Generate <see cref="DbParameter"/> from the <paramref name="parameters" />
        /// </summary>
        public DbParameter[] CreateInParameters(long[] parameters)
        {
            DbParameter[] dbParameters = null;
            if (parameters != null)
            {
                dbParameters = new DbParameter[parameters.Length];
                for (int i = 0; i < parameters.Length; i++)
                {
                    var name = string.Format(CultureInfo.InvariantCulture, "lparam{0}", i);
                    dbParameters[i] = this.CreateInParameter(name, DbType.Int64, parameters[i]);
                }
            }

            return dbParameters;
        }

        /// <summary>
        ///     Executes the specified <paramref name="query" /> with the specified <paramref name="parameters" />
        /// </summary>
        /// <param name="query">
        ///     The SQL query to execute
        /// </param>
        /// <param name="transaction">
        ///     The transaction.
        /// </param>
        /// <param name="parameters">
        ///     The parameters. Can be null.
        /// </param>
        /// <returns>
        ///     The return value of <paramref name="query" />
        /// </returns>
        public object ExecuteScalarFormat(string query, DbTransaction transaction, params DbParameter[] parameters)
        {
            try
            {
                IDatabaseInternal databaseInternal = new DatabaseTransactional(transaction);
                
                using (var command = this.GetSqlStringCommandFormat(query, parameters))
                {
                    databaseInternal.SetupCommand(command);
                    return databaseInternal.ExecuteScalar(command);
                }
            }
            catch (Exception e)
            {
                _log.Error("While ExecuteNonQuery", e);
                throw;
            }
        }

        /// <summary>
        ///     Returns a <see cref="DbCommand" /> with <see cref="CommandType.Text" />
        /// </summary>
        /// <param name="query">
        ///     The query.
        /// </param>
        /// <returns>
        ///     The <see cref="DbCommand" />.
        /// </returns>
        public DbCommand GetSqlStringCommand(string query)
        {
            return this.CreateCommand(CommandType.Text, query);
        }

        /// <summary>
        ///     Returns a <see cref="DbCommand" /> with <see cref="CommandType.Text" />
        /// </summary>
        /// <param name="query">
        ///     The query.
        /// </param>
        /// <param name="parameters">
        ///     The parameters
        /// </param>
        /// <returns>
        ///     The <see cref="DbCommand" />.
        /// </returns>
        public DbCommand GetSqlStringCommand(string query, IList<DbParameter> parameters)
        {
            DbCommand cmd = null;
            try
            {
                cmd = this.CreateCommand(CommandType.Text, query);
                if (parameters != null)
                {
                    for (int index = 0; index < parameters.Count; index++)
                    {
                        DbParameter dbParameter = parameters[index];
                        cmd.Parameters.Add(dbParameter);
                    }
                }
            }
            catch (Exception e)
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }

                _log.Error("While ExecuteReader", e);

                throw;
            }

            return cmd;
        }

        /// <summary>
        /// Returns a <see cref="DbCommand" /> with <see cref="CommandType.Text" />
        /// </summary>
        /// <param name="queryFormat">The query format.</param>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// The <see cref="DbCommand" />.
        /// </returns>
        public DbCommand GetSqlStringCommandFormat(string queryFormat, DbParameter parameter)
        {
            return this.GetSqlStringCommandFormat(queryFormat, new[] { parameter });
        }

        /// <summary>
        ///     Returns a <see cref="DbCommand" /> with <see cref="CommandType.Text" />
        /// </summary>
        /// <param name="queryFormat">
        ///     The query format.
        /// </param>
        /// <param name="parameters">
        ///     The parameters
        /// </param>
        /// <returns>
        ///     The <see cref="DbCommand" />.
        /// </returns>
        public DbCommand GetSqlStringCommandFormat(string queryFormat, params DbParameter[] parameters)
        {
            var query = this.BuildQuery(queryFormat, parameters);

            return this.GetSqlStringCommand(query, parameters);
        }

        /// <summary>
        ///     Returns a <see cref="DbCommand" /> with <see cref="CommandType.Text" />
        /// </summary>
        /// <param name="query">
        ///     The query.
        /// </param>
        /// <param name="parameters">
        ///     The parameters
        /// </param>
        /// <returns>
        ///     The <see cref="DbCommand" />.
        /// </returns>
        public DbCommand GetSqlStringCommandParam(string query, params DbParameter[] parameters)
        {
            return this.GetSqlStringCommand(query, parameters);
        }

        /// <summary>
        ///     Normalizes the schema of the specified <paramref name="query" />.
        ///     Currently it removes the <c>dbo.</c> from the query string if the database is not a <c>SQL Server.</c>
        /// </summary>
        /// <param name="query">
        ///     The query.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public string NormalizeQuerySchema(string query)
        {
            return this._schemaNormalizer(query);
        }

        /// <summary>
        /// Conditionally get the logger database once
        /// </summary>
        /// <returns></returns>
        private IDatabaseInternal GetInternalDatabase()
        {
            if (_callerInfo == null)
            {
                return _databaseInternal;
            }

            var callerInfo = _callerInfo;
            _callerInfo = null;
            return  new DatabaseWithLogger(this._databaseInternal, callerInfo);
        }

        /// <summary>
        ///     Returns the method for normalizing a SQL query.
        /// </summary>
        /// <returns>
        ///     The <see cref="Func{String, String}" /> method that normalizes a SQL Query .
        /// </returns>
        private Func<string, string> GetQueryNormalizer()
        {
            switch (this._providerName)
            {
                case MappingStoreDefaultConstants.SqlServerProvider:
                    return s => s;
                default:
                    return s => _dboRemover.Replace(s, string.Empty);
            }
        }

        /// <summary>
        ///     Retrieves the marker format.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <returns>
        ///     The parameter marker format.
        /// </returns>
        private string RetrieveMarkerFormat(string providerName)
        {
            string parameterMarkerFormat = "@{0}";

            // 1. check if we have it configured 
            DatabaseSetting databaseSetting = _databaseSettingCollection[providerName];
            if (databaseSetting != null && !string.IsNullOrWhiteSpace(databaseSetting.ParameterMarkerFormat))
            {
                parameterMarkerFormat = databaseSetting.ParameterMarkerFormat;

                _log.DebugFormat(
                    "Got from configuration the parameter marker format '{0}' for provider '{1}'", 
                    parameterMarkerFormat, 
                    providerName);
            }
            else
            {
                // 4. if not, then try to get it from GetSchema(). SqlClient has a bug here.
                using (DbConnection connection = this.CreateConnection())
                {
                    connection.Open();
                    try
                    {
                        DataTable dataTable = connection.GetSchema(DbMetaDataCollectionNames.DataSourceInformation);
                        parameterMarkerFormat =
                            dataTable.Rows[0][DbMetaDataColumnNames.ParameterMarkerFormat].ToString();
                        _log.DebugFormat(
                            "Got from GetSchema the parameter marker format '{0}' for provider '{1}'", 
                            parameterMarkerFormat, 
                            providerName);
                    }
                    catch (Exception e)
                    {
                        _log.Error(
                            "Provider does not support DbMetaDataCollectionNames.DataSourceInformation or DbMetaDataColumnNames.ParameterMarkerFormat", 
                            e);
                        throw;
                    }
                }
            }

            return parameterMarkerFormat;
        }

        /// <summary>
        ///     Setup provider specific configuration.
        /// </summary>
        /// <param name="cmd">
        ///     The <see cref="DbCommand" /> to setup.
        /// </param>
        private void SetupProviderSpecific(DbCommand cmd)
        {
            // Oracle ODP.NET driver by default expects parameters in a specific order
            switch (this._connectionStringSettings.ProviderName)
            {
                case MappingStoreDefaultConstants.OracleProviderOdp:
                case MappingStoreDefaultConstants.OracleManagedProviderOdp:

                    // NOTE this is cached. The slow reflection part occurs only once.
                    var dynamicCmd = (dynamic)cmd;
                    dynamicCmd.BindByName = true;

                    break;
            }
        }


        /// <summary>
        ///    Execute a sequence.
        /// </summary>
        /// <param name="name">Name of the sequence.</param>
        /// <param name="maxValue">Max value of the sequence.</param>
        /// <returns>
        ///    Last id.
        /// </returns>
        public string ExecuteSequence(string name,int maxValue)
        {
            switch (this._connectionStringSettings.ProviderName)
            {
                case MappingStoreDefaultConstants.OracleProviderOdp:
                case MappingStoreDefaultConstants.OracleManagedProviderOdp:
                    return this.ExecuteScalar($"select {name}.nextval from dual", new List<DbParameter>()).ToString();
                case MappingStoreDefaultConstants.SqlServerProvider:
                case MappingStoreDefaultConstants.MySqlProvider:
                    return this.ExecuteScalar($"SELECT NEXT VALUE FOR {name};", new List<DbParameter>()).ToString();
                default:
                    return "1";
            }
        }
    }
}