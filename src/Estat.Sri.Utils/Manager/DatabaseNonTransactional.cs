// -----------------------------------------------------------------------
// <copyright file="DatabaseNonTransactional.cs" company="EUROSTAT">
//   Date Created : 2018-8-14
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Manager
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;

    using log4net;

    /// <summary>
    ///     The database non transactional.
    /// </summary>
    internal class DatabaseNonTransactional : IDatabaseInternal
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(DatabaseNonTransactional));

        /// <summary>
        ///     The mapping store connection string settings.
        /// </summary>
        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        ///     The factory to create connections.
        /// </summary>
        private readonly DbProviderFactory _factory;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DatabaseNonTransactional" /> class.
        /// </summary>
        /// <param name="connectionStringSettings">
        ///     The connection string settings.
        /// </param>
        /// <param name="factory">
        ///     The factory.
        /// </param>
        public DatabaseNonTransactional(
            ConnectionStringSettings connectionStringSettings, 
            DbProviderFactory factory)
        {
            this._connectionStringSettings = connectionStringSettings;
            this._factory = factory;
        }

        /// <summary>
        ///     The execute non query.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        public int ExecuteNonQuery(DbCommand command)
        {
            using (DbConnection connection = this.GetConnection())
            {
                connection.Open();
                command.Connection = connection;
                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        ///     The execute reader.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The <see cref="IDataReader" />.
        /// </returns>
        public IDataReader ExecuteReader(IDbCommand command)
        {
            return command.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        ///     The execute scalar.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public object ExecuteScalar(DbCommand command)
        {
            using (DbConnection connection = this.GetConnection())
            {
                connection.Open();
                command.Connection = connection;
                return command.ExecuteScalar();
            }
        }

        /// <summary>
        ///     The get connection.
        /// </summary>
        /// <returns>
        ///     The <see cref="DbConnection" />.
        /// </returns>
        public DbConnection GetConnection()
        {
            DbConnection connection = null;
            try
            {
                connection = this._factory.CreateConnection();
                if (connection != null)
                {
                    connection.ConnectionString = this._connectionStringSettings.ConnectionString;
                }
            }
            catch (Exception e)
            {
                if (connection != null)
                {
                    connection.Dispose();
                }

                _log.Error("While CreateConnection", e);

                throw;
            }

            return connection;
        }

        /// <summary>
        ///     The open connection.
        /// </summary>
        /// <param name="connection">
        ///     The connection.
        /// </param>
        public void OpenConnection(IDbConnection connection)
        {
            connection.Open();
        }

        /// <summary>
        ///     The setup command.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        public void SetupCommand(IDbCommand command)
        {
        }

        /// <inheritdoc />
        public DbCommand CreateCommand()
        {
            return _factory.CreateCommand();
        }
    }
}