using System;
using System.Data;
using Dapper;

namespace Estat.Sri.Utils.Model
{
    public class BooleanTypeHandler : SqlMapper.TypeHandler<bool>
    {
        public override bool Parse(object value)
        {
            return Convert.ToBoolean(value);
        }

        public override void SetValue(System.Data.IDbDataParameter parameter, bool value)
        {
            parameter.DbType = DbType.Int32;
            parameter.Value = value ? 1 : 0;
        }
    }
}