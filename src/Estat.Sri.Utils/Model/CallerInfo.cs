// -----------------------------------------------------------------------
// <copyright file="CallerInfo.cs" company="EUROSTAT">
//   Date Created : 2018-8-14
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Utils.Model
{
    using System;

    public class CallerInfo
    {
        private readonly string _memberName;
        private readonly string _sourceFile;
        private readonly int _sourceLine;

        public CallerInfo(string memberName, string sourceFile, int sourceLine)
        {
            if (memberName == null)
            {
                throw new ArgumentNullException(nameof(memberName));
            }

            if (sourceFile == null)
            {
                throw new ArgumentNullException(nameof(sourceFile));
            }

            _memberName = memberName;
            _sourceFile = sourceFile;
            _sourceLine = sourceLine;
        }

        public string MemberName => _memberName;

        public string SourceFile => _sourceFile;

        public int SourceLine => _sourceLine;
    }
}