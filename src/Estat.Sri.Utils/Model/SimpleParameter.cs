// -----------------------------------------------------------------------
// <copyright file="SimpleParameter.cs" company="EUROSTAT">
//   Date Created : 2018-3-5
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Data;

namespace Estat.Sri.Utils.Model
{
    public class SimpleParameter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleParameter"/> class.
        /// </summary>
        /// <param name="databaseType">Type of the database.</param>
        /// <param name="value">The value.</param>
        /// <exception cref="ArgumentNullException">value</exception>
        public SimpleParameter(DbType databaseType, object value)
        {
            DatabaseType = databaseType;
            Value = value ?? DBNull.Value;
        }

        /// <summary>
        /// Gets or sets the type of the database.
        /// </summary>
        /// <value>The type of the database.</value>
        public DbType DatabaseType { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public object Value { get; set; }
    }
}
