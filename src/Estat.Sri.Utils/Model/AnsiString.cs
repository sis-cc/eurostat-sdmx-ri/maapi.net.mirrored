// -----------------------------------------------------------------------
// <copyright file="AnsiString.cs" company="EUROSTAT">
//   Date Created : 2018-3-7
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Utils.Model
{
    using System.Data;

    /// <summary>
    /// An extension to <see cref="SimpleParameter"/> that uses <see cref="DbType.AnsiString"/>
    /// </summary>
    /// <seealso cref="Estat.Sri.Utils.Model.SimpleParameter" />
    public class AnsiString : SimpleParameter
    {
        public AnsiString(string value) : base(DbType.AnsiString, value)
        {
        }
    }
}
