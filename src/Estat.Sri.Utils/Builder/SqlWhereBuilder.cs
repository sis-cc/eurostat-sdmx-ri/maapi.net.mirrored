// -----------------------------------------------------------------------
// <copyright file="SqlWhereBuilder.cs" company="EUROSTAT">
//   Date Created : 2020-7-20
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Utils.Builder
{
    public class SqlWhereBuilder
    {
        public static readonly string And = "AND";
        public static readonly string Or = "OR";

        private readonly Database _database;
        private readonly List<string> _clauses = new List<string>();
        private readonly List<string> queue = new List<string>();
        private readonly List<DbParameter> dbParameters = new List<DbParameter>();

        public List<DbParameter> DbParameters => dbParameters;

        public SqlWhereBuilder(Database database)
        {
            if (database is null)
            {
                throw new ArgumentNullException(nameof(database));
            }

            _database = database;
        }

        public void Add(string clause)
        {
            _clauses.Add(clause);
        }

        public void Add<T>(string format, string name, DbType dbType, T value)
        {
            string clause = string.Format(CultureInfo.InvariantCulture, format, _database.BuildParameterName(name));
            dbParameters.Add(_database.CreateInParameter(name, dbType, value));
            Add(clause);
        }

        public bool HasClauses()
        {
            return _clauses.Count > 0;
        }
        public bool HasQueue()
        {
            return queue.Count > 0;
        }

        public void Merge(string logicOperator)
        {
            var clauses = BuildClauses(logicOperator, _clauses);
            _clauses.Clear();
            queue.Add(clauses);
        }

        public string Build(string logicOperator)
        {
            var result = BuildClauses(logicOperator, queue);
            queue.Clear();
            return result;
        }

        private string BuildClauses(string logicOperator, IEnumerable<string> clauses)
        {
            return string.Format(
                CultureInfo.InvariantCulture,
                "({0})",
                string.Join(
                       string.Format(CultureInfo.InvariantCulture, " {0} ", logicOperator),
                       clauses));
        }
    }
}
