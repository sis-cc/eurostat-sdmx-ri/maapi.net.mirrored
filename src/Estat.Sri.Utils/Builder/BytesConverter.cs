// -----------------------------------------------------------------------
// <copyright file="BytesConverter.cs" company="EUROSTAT">
//   Date Created : 2018-3-5
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Converts between strings and bytes
    /// </summary>
    public class BytesConverter
    {
        /// <summary>
        /// Converts the specified <paramref name="bytes"/> to hexadecimal representation.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns>a string with hex-decimal numbers</returns>
        public string ConvertToHex(byte[] bytes)
        {
            var hexArray = bytes.Select(b => b.ToString("X2", CultureInfo.InvariantCulture));
            var hexString = string.Join(string.Empty, hexArray);
            return hexString;
        }

        /// <summary>
        /// Converts the specified <paramref name="bytes"/> to Base64 representation.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns>a base 64 representation of the <paramref name="bytes"/></returns>
        public string ConvertToBase64(byte[] bytes)
        {
            return Convert.ToBase64String(bytes, Base64FormattingOptions.None);
        }

        /// <summary>
        /// Converts from hexadecimal.
        /// </summary>
        /// <param name="hex">The hexadecimal.</param>
        /// <returns>The bytes converted from a string containing hexadecimal</returns>
        public byte[] ConvertFromHex(string hex)
        {
            return Enumerable.Range(0, hex.Length / 2)
                  .Select(x => Convert.ToByte(hex.Substring(x * 2, 2), 16))
                  .ToArray();
        }

        /// <summary>
        /// Converts the specified value to bytes.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>An array of bytes</returns>
        public byte[] ConvertToBytes(string value)
        {
            return Encoding.UTF8.GetBytes(value);
        }
    }
}