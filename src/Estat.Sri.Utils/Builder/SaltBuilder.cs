// -----------------------------------------------------------------------
// <copyright file="SaltBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-3-5
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System.Security.Cryptography;

    /// <summary>
    /// A (crypto) Salt builder 
    /// </summary>
    public class SaltBuilder
    {
        /// <summary>
        /// The size
        /// </summary>
        private const int Size = 16;

        /// <summary>
        /// The random crypto service
        /// </summary>
        private static readonly RNGCryptoServiceProvider _cryptoService = new RNGCryptoServiceProvider();

        /// <summary>
        /// Builds the salt
        /// </summary>
        /// <returns>The bytes containing bytes</returns>
        public byte[] Build()
        {
            var buffer = new byte[Size];
            _cryptoService.GetBytes(buffer);
            return buffer;
        }
    }
}