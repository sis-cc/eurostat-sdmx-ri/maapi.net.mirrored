// -----------------------------------------------------------------------
// <copyright file="HashBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-3-5
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Security.Cryptography;

    /// <summary>
    /// Hash builder
    /// </summary>
    public class HashBuilder
    {
        /// <summary>
        /// The bytes converter
        /// </summary>
        private readonly BytesConverter _bytesConverter;

        /// <summary>
        /// Initializes a new instance of the <see cref="HashBuilder" /> class.
        /// </summary>
        /// <param name="bytesConverter">The bytes converter.</param>
        public HashBuilder(BytesConverter bytesConverter)
        {
            if (bytesConverter == null)
            {
                throw new ArgumentNullException("bytesConverter");
            }

            this._bytesConverter = bytesConverter;
        }

        /// <summary>
        /// Builds the specified hash for <paramref name="plainText"/> + <paramref name="salt"/> using the <paramref name="algorithm"/>.
        /// </summary>
        /// <param name="plainText">The plain text.</param>
        /// <param name="salt">The salt.</param>
        /// <param name="algorithm">The algorithm.</param>
        /// <returns>
        /// The hash
        /// </returns>
        public byte[] Build(string plainText, byte[] salt, string algorithm)
        {
            var hexValue = this._bytesConverter.ConvertToBytes(plainText);
            return this.Build(hexValue, salt, algorithm);
        }

        /// <summary>
        /// Builds the specified hash for <paramref name="plainText"/> + <paramref name="salt"/> using the <paramref name="algorithm"/>.
        /// </summary>
        /// <param name="plainText">The plain text.</param>
        /// <param name="salt">The salt.</param>
        /// <param name="algorithm">The algorithm.</param>
        /// <returns>The hash</returns>
        /// <exception cref="System.ArgumentNullException">
        /// plainText
        /// or
        /// salt
        /// or
        /// algorithm
        /// </exception>
        /// <exception cref="System.ArgumentException">Invalid <paramref name="algorithm"/></exception>
        public byte[] Build(byte[] plainText, byte[] salt, string algorithm)
        {
            if (plainText == null)
            {
                throw new ArgumentNullException("plainText");
            }

            if (salt == null)
            {
                throw new ArgumentNullException("salt");
            }

            if (algorithm == null)
            {
                throw new ArgumentNullException("algorithm");
            }

            using (var hashAlgorithm = HashAlgorithm.Create(algorithm))
            {
                if (hashAlgorithm == null)
                {
                    throw new ArgumentException(string.Format("Invalid algorithm: '{0}'", algorithm), algorithm);
                }

                var length = plainText.Length;
                var textAndSalt = new byte[length + salt.Length];
                for (int i = 0; i < length; i++)
                {
                    textAndSalt[i] = plainText[i];
                }

                for (int i = 0; i < salt.Length; i++)
                {
                    textAndSalt[i + length] = salt[i];
                }

                return hashAlgorithm.ComputeHash(textAndSalt);
            }
        }
    }
}