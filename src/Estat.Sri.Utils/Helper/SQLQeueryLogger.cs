// -----------------------------------------------------------------------
// <copyright file="SQLQeueryLogger.cs" company="EUROSTAT">
//   Date Created : 2018-8-8
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Utils.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Globalization;
    using System.Runtime.CompilerServices;

    using Estat.Sri.Utils.Model;

    using log4net;

    /// <summary>
    /// <para>An SQL Query logger</para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// To disable this feature add the following in the <c>log4net</c> configuration
    /// <code>
    /// <![CDATA[<logger name="org.estat.sri.sqlquerylogger" additivity="false"> <level value = "OFF" /> </ logger >]]>
    /// </code>
    /// </para>
    /// <para>
    /// To enable this feature add the following in the <c>log4net</c> configuration
    /// <code>
    /// <![CDATA[<logger name="org.estat.sri.sqlquerylogger" additivity="false">  <level value="INFO"/> <appender-ref ref="SQLQueryLoger" /> </ logger >]]>
    /// </code>
    /// Example pattern with all parameters:
    /// <code><![CDATA[<conversionPattern value="%d{ISO8601};%P{elapsed};%m;%P{dbparams};%P{sourceMember};%P{sourceFile};%P{sourceLine};%n"/>]]></code>
    /// </para>
    /// </remarks>
    public class SqlQueryLogger : IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(SqlQueryLogger).Assembly, "org.estat.sri.sqlquerylogger");

        private readonly string _sqlQuery;

        private readonly Stopwatch _stopwatch;

        private readonly string _dbParams;

        private bool _disposedValue;
        private readonly string _memberName;
        private readonly string _sourceFilePath;
        private readonly int _sourceLineNumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlQueryLogger"/> class.
        /// </summary>
        /// <param name="command">The command.</param>
        public SqlQueryLogger(IDbCommand command, CallerInfo callerInfo)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            if (callerInfo == null)
            {
                throw new ArgumentNullException(nameof(callerInfo));
            }

            if (_log.IsInfoEnabled)
            {
                _sqlQuery = command.CommandText;
                _stopwatch = Stopwatch.StartNew();
                _memberName = callerInfo.MemberName ?? string.Empty;
                _sourceFilePath = callerInfo.SourceFile ?? string.Empty;
                _sourceLineNumber = callerInfo.SourceLine;
                _dbParams = "{" + string.Join(",", command.Parameters.Cast<DbParameter>().Select(x => string.Format(CultureInfo.InvariantCulture, "'{0}':'{1}'", x.ParameterName, x.Value))) + "}";
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlQueryLogger"/> class.
        /// </summary>
        public SqlQueryLogger(string sqlQuery, Dictionary<string, object> parameters, CallerInfo callerInfo)
        {
            if (sqlQuery == null)
            {
                throw new ArgumentNullException(nameof(sqlQuery));
            }

            if (callerInfo == null)
            {
                throw new ArgumentNullException(nameof(callerInfo));
            }

            if (_log.IsInfoEnabled)
            {
                _sqlQuery = sqlQuery;
                _stopwatch = Stopwatch.StartNew();
                _memberName = callerInfo.MemberName ?? string.Empty;
                _sourceFilePath = callerInfo.SourceFile ?? string.Empty;
                _sourceLineNumber = callerInfo.SourceLine;
                if (parameters != null)
                {
                    _dbParams = "{" + string.Join(
                                    ",",
                                    parameters.Select(
                                        x => string.Format(
                                            CultureInfo.InvariantCulture,
                                            "'{0}':'{1}'",
                                            x.Key,
                                            x.Value))) + "}";
                }
                else
                {
                    _dbParams = "{}";
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlQueryLogger"/> class.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="memberName">Name of the member.</param>
        /// <param name="sourceFilePath">The source file path.</param>
        /// <param name="sourceLineNumber">The source line number.</param>
        public SqlQueryLogger(IDbCommand command, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0) :
            this(command, new CallerInfo(memberName, sourceFilePath, sourceLineNumber))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlQueryLogger" /> class.
        /// </summary>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <param name="memberName">Name of the member.</param>
        /// <param name="sourceFilePath">The source file path.</param>
        /// <param name="sourceLineNumber">The source line number.</param>
        /// <exception cref="ArgumentNullException">sqlQuery</exception>
        public SqlQueryLogger(string sqlQuery, Dictionary<string, object> parameters = null, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
            : this(sqlQuery, parameters, new CallerInfo(memberName, sourceFilePath, sourceLineNumber))
        {
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    if (_stopwatch == null || !_log.IsInfoEnabled)
                    {
                        return;
                    }

                    _stopwatch.Stop();

                    var elapsedMs = _stopwatch.Elapsed;
                    LogicalThreadContext.Properties["elapsed"] = elapsedMs;
                    LogicalThreadContext.Properties["sourceMember"] = _memberName;
                    LogicalThreadContext.Properties["sourceFile"] = _sourceFilePath;
                    LogicalThreadContext.Properties["sourceLine"] = _sourceLineNumber;
                    LogicalThreadContext.Properties["dbparams"] = _dbParams;
                    _log.Info(_sqlQuery);
                }

                _disposedValue = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
    }
}
