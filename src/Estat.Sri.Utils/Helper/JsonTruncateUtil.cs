using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Exception;

namespace Estat.Sri.Utils.Helper
{
    public class JsonTruncateUtil
    {
        public static (string, string) SplitForMSDB(string jsonString, string providerName)
        {
            int maxLength = 0;
            foreach (var val in Enum.GetValues(typeof(MaxStringLength)))
            {
               if(providerName.Contains(val.ToString()))
                {
                    maxLength = (int)val;
                }
            }
            if(maxLength == 0)
            {
                throw new SdmxInternalServerException($"MaxStringLength doesn't have a value for {providerName}");
            }
            // If the JSON string is shorter than or equal to the maxLength, return it as is
            if (Encoding.UTF8.GetByteCount(jsonString) <= maxLength)
            {
                return (jsonString, null);
            }

            // Deserialize the JSON array string to a JArray
            JArray jsonArray =JArray.Parse(jsonString);

            // Calculate the length of the JSON array without the closing bracket
            int arrayLength = Encoding.UTF8.GetByteCount(jsonString) - 1;

            // Traverse the JSON array backwards and remove items until the length is below the maxLength
            for (int i = jsonArray.Count - 1; i >= 0; i--)
            {
                // Serialize the current item
                string serializedItem = JsonConvert.SerializeObject(jsonArray[i], Formatting.None);

                // Calculate the length of the current item and the comma separator
                int itemLength = Encoding.UTF8.GetByteCount(serializedItem) + 1; // Add 1 for the comma separator

                // Check if removing the current item reduces the length below the maxLength
                if (arrayLength - itemLength < maxLength)
                {
                    jsonArray.RemoveAt(i);
                    break; // Stop removing items if the length is below the maxLength
                }

                // Remove the last item
                jsonArray.RemoveAt(i);

                // Adjust the array length
                arrayLength -= itemLength;
            }

            // Serialize the truncated JSON array
            string truncatedJsonArray = jsonArray.ToString(Formatting.None);

            // Get the remaining JSON data
            string remainingJson = "[" + jsonString.Substring(truncatedJsonArray.Length);



            return (truncatedJsonArray, remainingJson);

        }
    }


    public enum MaxStringLength
    {
        Oracle = 32767,
        SqlClient = 65000,
        MySqlConnector = 65535
    }
}
