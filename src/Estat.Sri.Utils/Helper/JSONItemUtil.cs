using System;
using System.Collections.Generic;
using System.Text;

namespace Estat.Sri.Utils.Helper
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;


    public class ItemJsonFieldsUtils
    {
        private const string TEXT_KEY = "T";
        private const string LOCALE_KEY = "L";
        private const string ANNOTATION_ID = "AI";
        private const string ANNOTATION_TITLE = "ATI";
        private const string ANNOTATION_TYPE = "ATY";
        private const string ANNOTATION_URI = "AU";
        private const string ANNOTATION_TEXT = "text";

        public static JArray TextTypeWrapperToJson(List<ITextTypeWrapper> textTypeWrappers)
        {
            if (textTypeWrappers.Count == 0)
            {
                return new JArray();
            }

            return GetTextTypeWrappersAsJArray(textTypeWrappers);
        }

        public static JArray AnnotationToJson(List<IAnnotation> annotations)
        {
            if (annotations.Count == 0)
            {
                return new JArray();
            }

            JArray jsonArray = new JArray();
            foreach (IAnnotation annotation in annotations)
            {
                JObject jsonObject = new JObject
            {
                { ANNOTATION_ID, annotation.Id },
                { ANNOTATION_TITLE, annotation.Title },
                { ANNOTATION_TYPE, annotation.Type },
                { ANNOTATION_URI, annotation.Uri?.ToString() },
                { ANNOTATION_TEXT, GetTextTypeWrappersAsJArray(annotation.Text) }
                };
                jsonArray.Add(jsonObject);
            }

            return jsonArray;
        }

        private static JArray GetTextTypeWrappersAsJArray(IList<ITextTypeWrapper> textTypeWrappers)
        {
            JArray jsonArray = new JArray();
            foreach (ITextTypeWrapper name in textTypeWrappers)
            {
                JObject jsonObject = new JObject
            {
                { TEXT_KEY, name.Value },
                { LOCALE_KEY, name.Locale }
            };

                jsonArray.Add(jsonObject);
            }

            return jsonArray;
        }

        public static List<ITextTypeWrapperMutableObject> ParseItemLocalisedStrings(string jsonString)
        {
            if (string.IsNullOrEmpty(jsonString))
            {
                return new List<ITextTypeWrapperMutableObject>();
            }

            JArray jsonArray = JArray.Parse(jsonString);
            return ParseItemLocalisedStrings(jsonArray);
        }

        public static List<ITextTypeWrapperMutableObject> ParseItemLocalisedStrings(JArray jsonArray)
        {
            List<ITextTypeWrapperMutableObject> textTypeWrappers = new List<ITextTypeWrapperMutableObject>();
            foreach (JObject jsonObject in jsonArray)
            {
                var textTypeWrapperMutable = new TextTypeWrapperMutableCore()
                {
                    Locale = jsonObject[LOCALE_KEY].ToString(),
                    Value = jsonObject[TEXT_KEY].ToString(),
                };

                textTypeWrappers.Add(textTypeWrapperMutable);
            }

            return textTypeWrappers;
        }

        public static List<IAnnotationMutableObject> ParseItemAnnotations(string jsonString)
        {
            List<IAnnotationMutableObject> annotations = new List<IAnnotationMutableObject>();
            if (string.IsNullOrEmpty(jsonString))
            {
                return annotations;
            }

            JArray jsonArray = JArray.Parse(jsonString);
            foreach (JObject jsonObject in jsonArray)
            {
                var uriStr = jsonObject[ANNOTATION_URI]?.ToString();
                var uri = string.IsNullOrWhiteSpace(uriStr) ? null : new Uri(uriStr);

                AnnotationMutableCore annotation = new AnnotationMutableCore();
                annotation.Id = GetValueOrNull(jsonObject, ANNOTATION_ID);
                annotation.Title = GetValueOrNull(jsonObject, ANNOTATION_TITLE);
                annotation.Type = GetValueOrNull(jsonObject, ANNOTATION_TYPE);
                annotation.Uri = uri;

                var annotationText = jsonObject[ANNOTATION_TEXT];
                if (annotationText.Type == JTokenType.Array)
                {
                    foreach (var text in ParseItemLocalisedStrings((JArray)annotationText))
                    {
                        annotation.AddText(text);
                    }
                }
             
                annotations.Add(annotation);
            }

            return annotations;
        }

        private static string GetValueOrNull(JObject parentObject, string propertyName)
        {
            if (parentObject.TryGetValue(propertyName, StringComparison.Ordinal, out JToken token))
            {
                if (token.Type == JTokenType.String)
                {
                    return token.ToString();
                }
            }

            return null;
        }
    }
}

