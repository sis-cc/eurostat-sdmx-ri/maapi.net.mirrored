// -----------------------------------------------------------------------
// <copyright file="DbHelper.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Text;
    using System.Text.RegularExpressions;

    using Estat.Sri.MappingStoreRetrieval.Manager;
    using System.Linq;

    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.Utils.Helper;

    /// <summary>
    ///     A collection of helper methods
    /// </summary>
    public static class DbHelper
    {
        /// <summary>
        ///     The maximum number of parameters
        /// </summary>
        private const int MaxParameters = 1000;

        /// <summary>
        ///     The _select query match REGEX
        /// </summary>
        private static readonly Regex _selectQueryMatchRegex;

        /// <summary>
        ///     Initializes static members of the <see cref="DbHelper" /> class.
        /// </summary>
        static DbHelper()
        {
            _selectQueryMatchRegex = new Regex(@"^\s*select\b", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
        }

        /// <summary>
        ///     Bulk delete from MSDB table with name <paramref name="tableName" />, the records where the field
        ///     <paramref name="primaryKey" /> value is in <paramref name="toDelete" />
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <param name="connection">
        ///     The connection.
        /// </param>
        /// <param name="transaction">
        ///     The transaction.
        /// </param>
        /// <param name="tableName">
        ///     The table name.
        /// </param>
        /// <param name="primaryKey">
        ///     The primary key.
        /// </param>
        /// <param name="toDelete">
        ///     The to delete.
        /// </param>
        /// <param name="additionalWhereClauses">
        ///     The additional Where Clauses.
        /// </param>
        /// <returns>
        ///     The number of records deleted.
        /// </returns>
        /// <remarks>
        ///     It deletes at most <see cref="MaxParameters" /> records at a time.
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "Should be without any user input. DbParameters are also used.")]
        public static int BulkDelete(Database database, DbConnection connection, DbTransaction transaction, string tableName, string primaryKey, Stack<long> toDelete, params string[] additionalWhereClauses)
        {
            int deleted = 0;
            var queryBuilder = new StringBuilder("delete from {0} where {1} in ({2})");
            if (additionalWhereClauses != null)
            {
                for (int i = 0; i < additionalWhereClauses.Length; i++)
                {
                    var additionalWhereClause = additionalWhereClauses[i];
                    queryBuilder.Append(" AND (");
                    queryBuilder.Append(additionalWhereClause);
                    queryBuilder.Append(") ");
                }
            }

            string queryFormat = queryBuilder.ToString();
            using (DbCommand cmd = CreateCommand(connection, string.Empty, transaction))
            {
                var parameters = new DbParameter[MaxParameters];
                var sb = new StringBuilder();
                for (int i = 0; i < MaxParameters; i++)
                {
                    parameters[i] = cmd.CreateParameter();
                    parameters[i].DbType = DbType.Int64;
                    parameters[i].ParameterName = string.Format(CultureInfo.InvariantCulture, "pk{0}", i);
                }

                if (toDelete == null)
                {
                    throw new ArgumentNullException("toDelete");
                }

                if (database == null)
                {
                    throw new ArgumentNullException("database");
                }

                while (toDelete.Count > 0)
                {
                    cmd.Parameters.Clear();
                    sb.Length = 0;
                    int count = Math.Min(toDelete.Count, MaxParameters);
                    for (int i = 0; i < count; i++)
                    {
                        parameters[i].Value = toDelete.Pop();
                        cmd.Parameters.Add(parameters[i]);

                        sb.Append(database.BuildParameterName(string.Format(CultureInfo.InvariantCulture, "pk{0},", i)));
                    }

                    sb.Length--;
                    cmd.CommandText = string.Format(CultureInfo.InvariantCulture, queryFormat, tableName, primaryKey, sb);
                    deleted += cmd.ExecuteNonQueryAndLog();
                }
            }

            return deleted;
        }

        /// <summary>
        /// Bulk delete from MSDB table with name <paramref name="tableName" />, the records where the field
        /// <paramref name="primaryKeyFieldName" /> value is in <paramref name="toDelete" />
        /// </summary>
        /// <typeparam name="T">The type of the primary key</typeparam>
        /// <typeparam name="T2">The type of the 2.</typeparam>
        /// <param name="database">The database.</param>
        /// <param name="tableName">The table name.</param>
        /// <param name="primaryKeyFieldName">Name of the primary key field.</param>
        /// <param name="primaryKey2FieldName">Name of the primary key2 field.</param>
        /// <param name="toDeleteSingle">To delete single.</param>
        /// <param name="dbType2">The database type2.</param>
        /// <param name="toDelete">The to delete.</param>
        /// <param name="dbType">Type of the database.</param>
        /// <param name="additionalWhereClauses">The additional Where Clauses.</param>
        /// <returns>
        /// The number of records deleted.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">toDelete
        /// or
        /// database</exception>
        /// <remarks>
        /// It deletes at most <see cref="MaxParameters" /> records at a time.
        /// </remarks>
        public static int BulkDeleteTwoKeys<T, T2>(Database database, string tableName, string primaryKeyFieldName, string primaryKey2FieldName, T toDeleteSingle, DbType dbType2, Stack<T2> toDelete, DbType dbType, params string[] additionalWhereClauses)
        {
            if (toDelete == null)
            {
                throw new ArgumentNullException("toDelete");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            int deleted = 0;
            var queryBuilder = new StringBuilder("delete from {0} where {1} in ({2}) and {3} = {4}");

            ApplyAdditionalWhereClauses(additionalWhereClauses, queryBuilder);

            string queryFormat = queryBuilder.ToString();

            while (toDelete.Count > 0)
            {
                var currentParameters = new List<DbParameter>();
                var parameterList = new List<string>();
                int count = Math.Min(toDelete.Count, MaxParameters);
                for (int i = 0; i < count; i++)
                {
                    DbParameter parameter = database.CreateInParameter(string.Format(CultureInfo.InvariantCulture, "pk{0}", i), dbType, toDelete.Pop());
                    currentParameters.Add(parameter);
                    parameterList.Add("{" + i.ToString(CultureInfo.InvariantCulture) + "}");
                }

                DbParameter param2 = database.CreateInParameter("p_param", dbType2, toDeleteSingle);
                
                var sqlStatement = string.Format(CultureInfo.InvariantCulture, queryFormat, tableName, primaryKeyFieldName, string.Join(",", parameterList), primaryKey2FieldName, param2.ParameterName);
                currentParameters.Add(param2);
                deleted += database.UsingLogger().ExecuteNonQueryFormat(sqlStatement, currentParameters.ToArray());
            }

            return deleted;
        }

        /// <summary>
        ///     Bulk delete from MSDB table with name <paramref name="tableName" />, the records where the field
        ///     <paramref name="primaryKey" /> value is in <paramref name="toDelete" />
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="tableName">The table name.</param>
        /// <param name="primaryKey">The primary key.</param>
        /// <param name="toDelete">The to delete.</param>
        /// <param name="additionalWhereClauses">The additional Where Clauses.</param>
        /// <returns>
        ///     The number of records deleted.
        /// </returns>
        /// <remarks>
        ///     It deletes at most <see cref="MaxParameters" /> records at a time.
        /// </remarks>
        public static int BulkDelete(Database database, string tableName, string primaryKey, Stack<long> toDelete, params string[] additionalWhereClauses)
        {
            if (toDelete == null)
            {
                throw new ArgumentNullException("toDelete");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            int deleted = 0;
            var queryBuilder = new StringBuilder("delete from {0} where {1} in ({2})");

            ApplyAdditionalWhereClauses(additionalWhereClauses, queryBuilder);

            string queryFormat = queryBuilder.ToString();

            while (toDelete.Count > 0)
            {
                var currentParameters = new List<DbParameter>();
                var parameterList = new List<string>();
                int count = Math.Min(toDelete.Count, MaxParameters);
                for (int i = 0; i < count; i++)
                {
                    DbParameter parameter = database.CreateInParameter(string.Format(CultureInfo.InvariantCulture, "pk{0}", i), DbType.Int64, toDelete.Pop());
                    currentParameters.Add(parameter);
                    parameterList.Add("{" + i.ToString(CultureInfo.InvariantCulture) + "}");
                }

                var sqlStatement = string.Format(CultureInfo.InvariantCulture, queryFormat, tableName, primaryKey, string.Join(",", parameterList));

                deleted += database.UsingLogger().ExecuteNonQueryFormat(sqlStatement, currentParameters.ToArray());
            }

            return deleted;
        }

        /// <summary>
        /// Bulk delete from MSDB table with name <paramref name="tableName" />, the records where the field
        /// <paramref name="primaryKey" /> value is in <paramref name="toUpdate" />
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="tableName">The table name.</param>
        /// <param name="fieldValuePairs">The field value pairs.</param>
        /// <param name="primaryKey">The primary key.</param>
        /// <param name="toUpdate">The to delete.</param>
        /// <param name="additionalWhereClauses">The additional Where Clauses.</param>
        /// <returns>
        /// The number of records deleted.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// toUpdate
        /// or
        /// fieldValuePairs
        /// or
        /// database
        /// or
        /// database
        /// </exception>
        /// <remarks>
        /// It deletes at most <see cref="MaxParameters" /> records at a time.
        /// </remarks>
        public static int BulkUpdate(Database database, string tableName, IList<KeyValuePair<string, long>> fieldValuePairs, string primaryKey, Stack<long> toUpdate, params string[] additionalWhereClauses)
        {
            var queryBuilder = new StringBuilder("update {0} set {1} where {2} in ({3})");
            int deleted = 0;
            ApplyAdditionalWhereClauses(additionalWhereClauses, queryBuilder);

            string queryFormat = queryBuilder.ToString();
            if (toUpdate == null)
            {
                throw new ArgumentNullException("toUpdate");
            }

            while (toUpdate.Count > 0)
            {
                var currentParameters = new List<DbParameter>();
                var parameterList = new List<string>();
                var updateList = new List<string>();
                if (fieldValuePairs == null)
                {
                    throw new ArgumentNullException("fieldValuePairs");
                }

                for (int i = 0; i < fieldValuePairs.Count; i++)
                {
                    if (database == null)
                    {
                        throw new ArgumentNullException("database");
                    }

                    DbParameter parameter = database.CreateInParameter(string.Format(CultureInfo.InvariantCulture, "p_{0}", i), DbType.Int64, fieldValuePairs[i].Value);
                    currentParameters.Add(parameter);
                    updateList.Add(string.Format(CultureInfo.InvariantCulture, "{0} = {{{1}}}", fieldValuePairs[i].Key, i));
                }

                int count = Math.Min(toUpdate.Count, MaxParameters);

                for (int i = fieldValuePairs.Count; i < count + fieldValuePairs.Count; i++)
                {
                    if (database == null)
                    {
                        throw new ArgumentNullException("database");
                    }

                    DbParameter parameter = database.CreateInParameter(string.Format(CultureInfo.InvariantCulture, "pk{0}", i), DbType.Int64, toUpdate.Pop());
                    currentParameters.Add(parameter);
                    parameterList.Add("{" + i.ToString(CultureInfo.InvariantCulture) + "}");
                }

                string setValues = string.Join(", ", updateList);
                string inValues = string.Join(",", parameterList);
                var sqlStatement = string.Format(CultureInfo.InvariantCulture, queryFormat, tableName, setValues, primaryKey, inValues);

                deleted += database.UsingLogger().ExecuteNonQueryFormat(sqlStatement, currentParameters.ToArray());
            }

            return deleted;
        }

        /// <summary>
        ///     Create a <see cref="DbCommand" />
        /// </summary>
        /// <param name="connection">The connection</param>
        /// <param name="commandText">The SQL statement</param>
        /// <returns>The new <see cref="DbCommand" /> </returns>
        public static DbCommand CreateCommand(DbConnection connection, string commandText)
        {
            return CreateCommand(connection, commandText, null);
        }

        /// <summary>
        ///     Create a <see cref="DbCommand" /> with the specified <paramref name="transaction"/>
        /// </summary>
        /// <param name="connection">
        ///     The connection
        /// </param>
        /// <param name="commandText">
        ///     The SQL statement
        /// </param>
        /// <param name="transaction">
        ///     The transaction.
        /// </param>
        /// <returns>
        ///     The new <see cref="DbCommand" />
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "Should be without any user input. DbParameters are also used.")]
        public static DbCommand CreateCommand(DbConnection connection, string commandText, DbTransaction transaction)
        {
            if (connection == null)
            {
                throw new ArgumentNullException("connection");
            }

            DbCommand command = connection.CreateCommand();
            if (transaction != null)
            {
                command.Transaction = transaction;
            }

            command.CommandText = commandText;
            return command;
        }

        /// <summary>
        ///     Create a <see cref="Database" /> instance from the specified <paramref name="connectionStringSettings" />
        /// </summary>
        /// <param name="connectionStringSettings">
        ///     The connection string settings.
        /// </param>
        /// <returns>
        ///     A <see cref="Database" /> instance from the specified <paramref name="connectionStringSettings" />
        /// </returns>
        public static Database CreateDatabase(ConnectionStringSettings connectionStringSettings)
        {
            return new Database(connectionStringSettings);
        }

        /// <summary>
        ///     Create parameter for the specified <paramref name="command" /> and add it to the command
        /// </summary>
        /// <param name="command">The command</param>
        /// <param name="name">The parameter name</param>
        /// <param name="dbType">The type</param>
        /// <returns>The new <see cref="DbParameter" /></returns>
        public static DbParameter CreateParameter(DbCommand command, string name, DbType dbType)
        {
            return CreateParameter(command, name, dbType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Create parameter for the specified <paramref name="command" /> and add it to the command
        /// </summary>
        /// <param name="command">The command</param>
        /// <param name="name">The parameter name</param>
        /// <param name="dbType">The type</param>
        /// <param name="direction">The direction</param>
        /// <returns>The new <see cref="DbParameter" /></returns>
        public static DbParameter CreateParameter(DbCommand command, string name, DbType dbType, ParameterDirection direction)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            DbParameter parameter = command.CreateParameter();
            parameter.ParameterName = name;
            parameter.DbType = dbType;
            parameter.Direction = direction;
            command.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        ///     Finds a Table/sub-query alias that does not exist in the <paramref name="subQuery" />.
        /// </summary>
        /// <param name="subQuery">The sub query.</param>
        /// <param name="template">The template for the table/sub-query alias. Must accept one format parameter e.g. <c>{0}</c>.</param>
        /// <returns>
        ///     The table/sub-query alias
        /// </returns>
        public static string FindUnsedFromAlias(string subQuery, string template)
        {
            uint i = 0;

            string tableAlias = string.Format(CultureInfo.InvariantCulture, template, string.Empty);
            while (Regex.IsMatch(subQuery, string.Format(@"\b{0}\b", tableAlias), RegexOptions.IgnoreCase | RegexOptions.CultureInvariant))
            {
                tableAlias = string.Format(CultureInfo.InvariantCulture, template, i++);
                if (i == uint.MaxValue)
                {
                    i = 0;
                    template = string.Format(CultureInfo.InvariantCulture, "z{0}", template);
                }
            }

            return tableAlias;
        }

        /// <summary>
        /// Normalizes from value.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>
        /// The <see cref="string" />
        /// </returns>
        public static string NormaliseFromValue(string query)
        {
            return _selectQueryMatchRegex.IsMatch(query) ? string.Format("({0}) {1}", query, FindUnsedFromAlias(query, "virtualDataSet{0}")) : query;
        }

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="identifiableSysId">The identifiable system identifier.</param>
        /// <param name="query">The query format. It must accept one format parameter.</param>
        /// <returns>System.Int32.</returns>
        public static int ExecuteQuery(Database database, IList<long> identifiableSysId, string query)
        {
            if (database == null)
            {
                throw new ArgumentNullException(nameof(database));
            }

            if (identifiableSysId == null)
            {
                throw new ArgumentNullException(nameof(identifiableSysId));
            }

            if (identifiableSysId.Count == 0)
            {
                throw new ArgumentException("Empty collection", nameof(identifiableSysId));
            }

            int count = 0;
            var pages = identifiableSysId.Count / MaxParameters;

            for (int i = 0; i <= pages; i++)
            {
                var skip = MaxParameters * i;
                var ten = identifiableSysId.Skip(skip).Take(MaxParameters).ToArray();
                var parameters = new DbParameter[ten.Length];

                for (int p = 0; p < ten.Length; p++)
                {
                    parameters[p] = database.CreateInParameter("p" + p.ToString(CultureInfo.InvariantCulture), DbType.Int64, ten[p]);
                }

                var inClause = Enumerable.Range(0, ten.Length).Select(x => "{" + x.ToString(CultureInfo.InvariantCulture) + "}");
                var normalizedQuery = string.Format(CultureInfo.InvariantCulture, query, string.Join(", ", inClause));
                count += database.UsingLogger().ExecuteNonQueryFormat(normalizedQuery, parameters);
            }

            return count;
        }

        /// <summary>
        /// Applies the additional where clauses.
        /// </summary>
        /// <param name="additionalWhereClauses">The additional where clauses.</param>
        /// <param name="queryBuilder">The query builder.</param>
        private static void ApplyAdditionalWhereClauses(IList<string> additionalWhereClauses, StringBuilder queryBuilder)
        {
            if (additionalWhereClauses != null)
            {
                for (int i = 0; i < additionalWhereClauses.Count; i++)
                {
                    var additionalWhereClause = additionalWhereClauses[i];
                    queryBuilder.Append(" AND (");
                    queryBuilder.Append(additionalWhereClause);
                    queryBuilder.Append(") ");
                }
            }
        }
    }
}