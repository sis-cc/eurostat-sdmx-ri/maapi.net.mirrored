using System;
using System.Collections.Generic;
using System.Text;

namespace Estat.Sri.Utils.Helper
{
    public static class JdbcConnectionStringHelper
    {
        public static string GenerateJdbcString(string dbType, string dbName, string dbServer, string dbPort)
        {

            string jdbcString = "";

            if (dbType.ToLower().Equals("mysql"))
            {
                jdbcString = "jdbc:" + dbType.ToLower() + "://" + dbServer.ToLower() + ":" + dbPort + "/" + dbName;
            }
            else if (dbType.ToLower().Equals("sqlserver"))
            {
                var serverWithPort = dbServer;
                if (!string.IsNullOrWhiteSpace(dbPort))
                {
                    serverWithPort += ":" + dbPort;
                }
                jdbcString = "jdbc:" + dbType.ToLower() + "://" + serverWithPort + ";" + "databaseName=" + dbName;
            }
            else if (dbType.ToLower().Equals("oracle"))
            {
                jdbcString = "jdbc:oracle:thin:@" + dbName;
            }

            return jdbcString;
        }

    }
}
