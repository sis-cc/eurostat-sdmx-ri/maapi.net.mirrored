// -----------------------------------------------------------------------
// <copyright file="StoreContext.cs" company="EUROSTAT">
//   Date Created : 2021-6-21
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Threading;

namespace Estat.Sri.Utils.Helper
{
    /// <summary>
    /// Workaround for getting store id when HttpContext is not available
    /// TODO pass store id or connection string or database as a parameter everywhere
    /// </summary>
    public class StoreContext : IDisposable
    {
        private readonly static AsyncLocal<string> _currentStoreId = new AsyncLocal<string>();
        private bool disposedValue;

        /// <summary>
        /// Initialize a new instance of <see cref="StoreContext"/> with a new store id
        /// It will overwrite any other store id in the current context
        /// </summary>
        /// <param name="storeId"></param>
        public StoreContext(string storeId)
        {
            _currentStoreId.Value = storeId;
        }

        /// <summary>
        /// Gets a value indicating whether we are in a context or not
        /// </summary>
        public static bool IsInContext => !string.IsNullOrEmpty(_currentStoreId.Value);

        /// <summary>
        /// Gets the current store id
        /// </summary>
        public static string CurrentStoreId => _currentStoreId.Value;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _currentStoreId.Value = null;
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
