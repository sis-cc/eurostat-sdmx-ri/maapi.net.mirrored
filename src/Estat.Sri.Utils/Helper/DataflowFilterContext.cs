// -----------------------------------------------------------------------
// <copyright file="DataflowFilterContext.cs" company="EUROSTAT">
//   Date Created : 2017-12-5
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Helper
{
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using System;
    using System.Collections.Generic;
    using System.Threading;

    /// <summary>
    /// A Dataflow Filter context to override configuration
    /// </summary>
    public class DataflowFilterContext : IDisposable
    {
        /// <summary>
        /// The dataflow fliter
        /// </summary>
        private static AsyncLocal<Stack<DataflowFilter>> _dataflowFliter = new AsyncLocal<Stack<DataflowFilter>> { Value = GetDefault() };

        /// <summary>
        /// The disposed value
        /// </summary>
        private bool _disposedValue = false; // To detect redundant calls

        /// <summary>
        /// Initializes a new instance of the <see cref="DataflowFilterContext" /> class.
        /// </summary>
        /// <param name="filter">The filter.</param>
        public DataflowFilterContext(DataflowFilter filter)
        {
            _dataflowFliter = new AsyncLocal<Stack<DataflowFilter>> { Value = GetDefault() };
            _dataflowFliter.Value.Push(filter);
        }

        /// <summary>
        /// Gets the current filter.
        /// </summary>
        /// <value>
        /// The current filter.
        /// </value>
        public static DataflowFilter CurrentFilter
        {
            get
            {
                if (_dataflowFliter.Value == null)
                {
                    _dataflowFliter.Value = GetDefault();
                }

                return _dataflowFliter.Value.Peek();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    if (_dataflowFliter.Value != null && _dataflowFliter.Value.Count > 1)
                    {
                        _dataflowFliter.Value.Pop();
                    }
                }

                _disposedValue = true;
            }
        }

        /// <summary>
        /// Gets the default.
        /// </summary>
        /// <returns></returns>
        private static Stack<DataflowFilter> GetDefault()
        {
            var filter = ConfigManager.Config.DataflowConfiguration.IgnoreProductionForStructure
                                        ? (ConfigManager.Config.DataflowConfiguration.IgnoreExternalUsage
                                               ? DataflowFilter.Usage
                                               : DataflowFilter.Any)
                                        : DataflowFilter.Production;
            var stack = new Stack<DataflowFilter>();
            stack.Push(filter);
            return stack;
        }
    }
}