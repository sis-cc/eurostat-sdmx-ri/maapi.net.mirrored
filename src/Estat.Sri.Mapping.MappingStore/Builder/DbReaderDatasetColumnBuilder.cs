// -----------------------------------------------------------------------
// <copyright file="DbReaderDatasetColumnBuilder.cs" company="EUROSTAT">
//   Date Created : 2014-10-23
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Text.RegularExpressions;

    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Mapping.MappingStore.Helpers;
    using Estat.Sri.Mapping.Api.Exceptions;

    /// <summary>
    ///     The dataset column builder. It uses <see cref="DbDataReader" />
    /// </summary>
    public class DbReaderDatasetColumnBuilder : IDatasetColumnBuilder
    {
        /// <summary>
        ///     The _DDB database
        /// </summary>
        private readonly Database _ddbDatabase;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DbReaderDatasetColumnBuilder" /> class.
        /// </summary>
        /// <param name="ddbDatabase">
        ///     The DDB database.
        /// </param>
        public DbReaderDatasetColumnBuilder(Database ddbDatabase)
        {
            this._ddbDatabase = ddbDatabase;
        }

        /// <summary>
        ///     Builds the specified query.
        /// </summary>
        /// <param name="query">
        ///     The query.
        /// </param>
        /// <returns>
        ///     The list of columns used in the (outer) select statement.
        /// </returns>
        public IEnumerable<string> Build(string query)
        {
            var retVal = new List<string>();
            using (var connection = this._ddbDatabase.CreateConnection())
            {
                connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    using (var reader = cmd.ExecuteReader(CommandBehavior.SchemaOnly))
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            var name = reader.GetName(i);
                            if (string.IsNullOrEmpty(name))
                            {
                                throw new DataSetColumnNameException("Cannot parse column name at position " + i + ". Please check and use an alias, i.e. 'AS'");
                            }

                            retVal.Add(name);
                        }

                        cmd.SafeCancel();
                    }
                }
            }

            return retVal;
        }
    }
}
