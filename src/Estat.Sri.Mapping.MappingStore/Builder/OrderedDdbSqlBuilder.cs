﻿// -----------------------------------------------------------------------
// <copyright file="OrderedDdbSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2014-09-11
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder
{
    using System.Globalization;

    using Estat.Sri.Mapping.MappingStore.Model;

    /// <summary>
    ///     Adds an order by to the SQL Query.
    /// </summary>
    public class OrderedDdbSqlBuilder : IDdbSqlBuilder
    {
        /// <summary>
        ///     The _decorated DDB SQL builder
        /// </summary>
        private readonly IDdbSqlBuilder _decoratedDdbSqlBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="OrderedDdbSqlBuilder" /> class.
        /// </summary>
        /// <param name="decorateDdbSqlBuilder">The decorated DDB SQL builder.</param>
        public OrderedDdbSqlBuilder(IDdbSqlBuilder decorateDdbSqlBuilder)
        {
            this._decoratedDdbSqlBuilder = decorateDdbSqlBuilder;
        }

        /// <summary>
        ///     Builds SQL from the specified <paramref name="buildFrom" />.
        /// </summary>
        /// <param name="buildFrom">
        ///     The <see cref="IDatasetColumnInfo" />.
        /// </param>
        /// <returns>
        ///     The SQL from the specified <paramref name="buildFrom" />.
        /// </returns>
        public string Build(IDatasetColumnInfo buildFrom)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0} order by {1}", this._decoratedDdbSqlBuilder.Build(buildFrom), buildFrom.ColumnNames);
        }
    }
}