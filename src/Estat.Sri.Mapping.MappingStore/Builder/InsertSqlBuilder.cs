﻿// -----------------------------------------------------------------------
// <copyright file="InsertSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-02-23
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Dapper;
using Estat.Sri.Mapping.MappingStore.Manager;

namespace Estat.Sri.Mapping.MappingStore.Builder
{
    using Estat.Sri.MappingStoreRetrieval.Manager;

    public class InsertSqlBuilder : ISqlBuilder
    {
        private const string InsertTemplate = "INSERT INTO {0}({1}) VALUES ({2})";
        private readonly IDatabaseMapperManager _databaseMapperManager;
        private readonly Dictionary<string, object> _setColumnsAndValues;
        private readonly string _table;

        public InsertSqlBuilder(string table, Dictionary<string, object> setColumnsAndValues,
            IDatabaseMapperManager databaseMapperManager)
        {
            this._table = table;
            this._setColumnsAndValues = setColumnsAndValues;
            this._databaseMapperManager = databaseMapperManager;
        }

        /// <summary>
        /// Creates the command definition.
        /// </summary>
        /// <param name="mappingStoredatabase"></param>
        /// <returns></returns>
        public CommandDefinition CreateCommandDefinition(Database mappingStoredatabase)
        {
            var commandBuilder = new CommandBuilder(mappingStoredatabase, this._databaseMapperManager);
            var commandText = string.Format(InsertTemplate, this._table,
                commandBuilder.CreateColumnList(this._setColumnsAndValues.Keys),
                commandBuilder.CreateParameterList(this._setColumnsAndValues.Keys));
            var commandParameters = commandBuilder.CreateParameters(this._setColumnsAndValues);

            return new CommandDefinition(commandText, commandParameters);
        }
    }
}