// -----------------------------------------------------------------------
// <copyright file="CommandBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-02-23
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;

namespace Estat.Sri.Mapping.MappingStore.Builder
{
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// CommandBuilder
    /// </summary>
    public class CommandBuilder
    {
        private readonly Database _mappingStoreDatabase;

        private readonly IDatabaseMapperManager _databaseMapperManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandBuilder"/> class.
        /// </summary>
        /// <param name="mappingStoreDatabase"></param>
        /// <param name="databaseMapperManager">The database mapper.</param>
        public CommandBuilder(Database mappingStoreDatabase, IDatabaseMapperManager databaseMapperManager)
        {
            _mappingStoreDatabase = mappingStoreDatabase;
            this._databaseMapperManager = databaseMapperManager;
        }

        /// <summary>
        /// Creates the parameters.
        /// </summary>
        /// <param name="table">The table.</param>
        /// <param name="setColumnsAndValues">The set columns and values.</param>
        /// <returns></returns>
        public DynamicParameters CreateParameters(Dictionary<string, object> setColumnsAndValues)
        {
            var parameters = new DynamicParameters();
            foreach (var columnsAndValue in setColumnsAndValues)
            {
                parameters.Add(this.BuildParameterName(this._databaseMapperManager.GetColumnMapping(columnsAndValue.Key)),columnsAndValue.Value);
            }

            return parameters;
        }


        public DynamicParameters CreateProcedureParameters(Dictionary<string, object> setColumnsAndValues)
        {
            var parameters = new DynamicParameters();
            foreach (var columnsAndValue in setColumnsAndValues)
            {
                var procedureParameterMapping = this._databaseMapperManager.GetProcedureParameterMapping(columnsAndValue.Key);
                if (!string.IsNullOrWhiteSpace(procedureParameterMapping))
                {
                    // TODO We don't need to build parameter name for stored procedures
                    var buildParameterName = this.BuildParameterName(procedureParameterMapping);
                    // We need to set the type for Oracle and parameter type bool
                    DbType? dbType = null;
                    if (columnsAndValue.Value != null && columnsAndValue.Value.GetType() == typeof(bool))
                    {
                        dbType = DbType.Int32;
                    }
                    parameters.Add(buildParameterName, columnsAndValue.Value, dbType);
                }
            }

            return parameters;
        }

        /// <summary>
        /// Creates the column list.
        /// </summary>
        /// <param name="columns">The columns.</param>
        /// <returns></returns>
        public string CreateColumnList(IEnumerable<string> columns)
        {
            return columns.Aggregate(string.Empty, (current, column) => current + (this.GetLocalColumnName(column) + ",")).TrimEnd(",");
        }

        /// <summary>
        /// Creates the columns with parameters.
        /// </summary>
        /// <param name="columns">The columns.</param>
        /// <param name="separator">The separator.</param>
        /// <returns></returns>
        public string CreateColumnsWithParameters(IEnumerable<string> columns, string separator)
        {
            return columns.Aggregate(string.Empty, (current, columnName) => current + (this.GetLocalColumnName(columnName) + "=" + this.BuildParameterName(this.GetLocalColumnName(columnName)) + separator)).TrimEnd(separator);
        }

        /// <summary>
        /// Creates the parameter list.
        /// </summary>
        /// <param name="columns">The columns.</param>
        /// <returns></returns>
        public string CreateParameterList(IEnumerable<string> columns)
        {
            return columns.Aggregate(string.Empty, (current, column) => current + (this.BuildParameterName(this.GetLocalColumnName(column)) + ",")).TrimEnd(",");
        }

        private string GetLocalColumnName(string columnName)
        {
            return this._databaseMapperManager.GetColumnMapping(columnName);
        }

        private DbParameter CreateParameter(string name, object value)
        {
            var dbParameter = this.CreateInParameter(name, value);
            if (dbParameter != null)
            {
                dbParameter.Value = value ?? DBNull.Value;
            }

            return dbParameter;
        }

        private string BuildParameterName(string name)
        {
            return _mappingStoreDatabase.BuildParameterName(name);
        }

        private DbParameter CreateInParameter(string name, object value)
        {
            var dbType = DatabaseConstants.GetDbType(value.GetType());
            return this._mappingStoreDatabase.CreateInParameter(name, dbType, value);
        }
    }
}