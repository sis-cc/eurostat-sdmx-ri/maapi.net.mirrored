﻿// -----------------------------------------------------------------------
// <copyright file="SingleTableCommandDefinitionBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-02-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Builder
{
    using Estat.Sri.MappingStoreRetrieval.Manager;

    internal class SingleTableCommandDefinitionBuilder : ICommandDefinitionBuilder
    {
        private readonly ISqlBuilderFactory _sqlBuilderFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="SingleTableCommandDefinitionBuilder"/> class.
        /// </summary>
        /// <param name="sqlBuilderFactory">The SQL builder factory.</param>
        public SingleTableCommandDefinitionBuilder(ISqlBuilderFactory sqlBuilderFactory)
        {
            this._sqlBuilderFactory = sqlBuilderFactory;
        }

        /// <summary>
        /// Gets the commands.
        /// </summary>
        /// <param name="updateInfoAction">The update information action.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="mappingStoreDatabase"></param>
        /// <returns></returns>
        public IEnumerable<CommandDefinition> GetCommands(UpdateInfoAction updateInfoAction, string entityId, Database mappingStoreDatabase)
        {
            var sqlBuilders = this._sqlBuilderFactory.GetBuilder(updateInfoAction, entityId);

            return sqlBuilders.Select(sqlBuilder => sqlBuilder.CreateCommandDefinition(mappingStoreDatabase)).ToList();
        }
    }
}