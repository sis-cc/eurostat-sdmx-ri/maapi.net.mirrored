// -----------------------------------------------------------------------
// <copyright file="ComponentMapping1NarrayT.cs" company="EUROSTAT">
//   Date Created : 2023-01-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Utils.Helper;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    using static ComponentMappingCommon;

    /// <summary>
    /// One component is mapped to one dataset column,
    /// with transcoding that maps scalar values to array values.
    /// </summary>
    public class ComponentMapping1NarrayT : IComponentArrayMapper
    {
        /// <summary>
        /// The _entity.
        /// </summary>
        private readonly ComponentMappingEntity _mapping;

        /// <summary>
        /// The column ordinal builder
        /// </summary>
        private readonly ColumnOrdinalBuilder _columnOrdinalBuilder;

        private readonly TranscodingRuleMap _transcodingRuleMap;

        private readonly string _databaseType;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMapping1Narray"/> class.
        /// </summary>
        /// <param name="mapping">The mapping entity.</param>
        /// <param name="transcodingRuleMap"></param>
        /// <param name="databaseType"></param>
        public ComponentMapping1NarrayT(ComponentMappingEntity mapping, TranscodingRuleMap transcodingRuleMap, string databaseType)
        {
            _mapping = mapping;
            _transcodingRuleMap = transcodingRuleMap;
            _databaseType = databaseType;
            _columnOrdinalBuilder = new ColumnOrdinalBuilder(mapping.GetColumns());
        }

        #region invalid scalar implementations

        /// <summary>
        /// Not applicable for array mappers.
        /// Throws always exception.
        /// </summary>
        /// <param name="conditionValue"></param>
        /// <param name="operatorValue"></param>
        /// <returns></returns>
        /// <exception cref="SdmxException">always</exception>
        public string GenerateComponentWhere(string conditionValue, OperatorType operatorValue)
        {
            throw new SdmxException($"{nameof(ComponentMapping1NarrayT)} is an array mapper. Use a method that handles array value.");
        }

        /// <summary>
        /// Not applicable for array mappers.
        /// Throws always exception.
        /// </summary>
        /// <param name="conditionValues"></param>
        /// <param name="withNull"></param>
        /// <returns></returns>
        /// <exception cref="SdmxException"></exception>
        public string GenerateComponentWhere(ISet<string> conditionValues, bool withNull = false, bool withNot = false)
        {
            throw new SdmxException($"{nameof(ComponentMapping1Carray)} is an array mapper. Use a method that handles array value.");
        }

        /// <summary>
        /// Not applicable for array mappers.
        /// Throws always exception.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        /// <exception cref="SdmxException">always</exception>
        public string MapComponent(IDataReader reader)
        {
            throw new SdmxException($"{nameof(ComponentMapping1NarrayT)} is an array mapper. Use a method that handles array value.");
        }

        #endregion

        /// <summary>
        /// Applicable to array components. 
        /// Generates the where clause mapping conditional N sdmx values to a single local value
        /// </summary>
        /// <param name="conditionValue">the sdmx array values</param>
        /// <param name="operatorValue">The operator will apply to the single transcoded value of the column.</param>
        /// <returns></returns>
        public string GenerateComponentWhere(string[] conditionValue, OperatorType operatorValue)
        {
            if (!(operatorValue == OperatorType.Exact ||
                operatorValue == (OperatorType.Not | OperatorType.Exact)))
            {
                throw new SdmxNotImplementedException($"Filtering component array values with operator {operatorValue}is not supported.");
            }

            string localCode = _transcodingRuleMap.GetLocalCode(conditionValue);
            return " ( "
                   + ComponentMappingCommon.SqlOperatorComponent(this._mapping.GetColumns()[0].Name, localCode, operatorValue, _databaseType)
                   + ") ";
        }

        /// <summary>
        /// Generates the SQL Where IN clause for the component used in this mapping
        /// </summary>
        /// <remarks>
        /// This should be used only when the operator is Equal
        /// </remarks>
        /// <param name="conditionValues"></param>
        /// <param name="withNull"></param>
        /// <returns></returns>
        public string GenerateComponentWhere(ISet<string[]> conditionValues, bool withNull = false)
        {
            IList<string> mappedValues = new List<string>();

            foreach (var conditionValue in conditionValues)
            {
                string localCode = _transcodingRuleMap.GetLocalCode(conditionValue);
                mappedValues.Add(QuoteString(EscapeString(localCode)));
            }

            var escapedMappedId = EscapeMappedId(this._mapping.GetColumns()[0].Name, _databaseType);
            return GenerateInClause(escapedMappedId, mappedValues, withNull);
        }

        /// <summary>
        /// Maps a single local code to a N-length sdmx array value
        /// </summary>
        /// <param name="dataReader"></param>
        /// <returns>The sdmx array value.</returns>
        public string[] MapComponentArray(IDataReader dataReader)
        {
            this._columnOrdinalBuilder.BuildOrdinals(dataReader);
            var column = this._columnOrdinalBuilder.ColumnOrdinals[0];
            string columnValue = DataReaderHelper.GetString(dataReader, column.Value);

            var transcodedCodes = this._transcodingRuleMap.GetSdmxCodes(columnValue);

            if (transcodedCodes != null)
            {
                return transcodedCodes.ToArray();
            }
            else
            {
                return _mapping.DefaultValues.ToArray();
            }
        }
    }
}
