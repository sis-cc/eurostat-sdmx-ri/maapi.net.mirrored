// -----------------------------------------------------------------------
// <copyright file="TimeRangeDimensionMappingBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-5-8
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Data;
    using System.Globalization;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;

    /// <summary>
    /// This builder is for transcodings that have a start and end non-compliant columns
    /// </summary>
    class TimeDimensionTimeRangeWithStartEndMappingBuilder : ITimeDimensionMappingBuilder
    {
        private readonly TimeFormatConfiguration _entity;

        private readonly ITimeDimensionMappingBuilder _startConfig;
        private readonly ITimeDimensionMappingBuilder _endConfig;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDimensionTimeRangeWithStartEndMappingBuilder"/> class.
        /// </summary>
        /// <param name="entity">The time format transcoding configuration</param>
        /// <param name="startConfig">The start configuration.</param>
        /// <param name="endConfig">The end configuration.</param>
        /// <exception cref="ArgumentNullException">
        /// startConfig
        /// or
        /// endConfig
        /// </exception>
        public TimeDimensionTimeRangeWithStartEndMappingBuilder(TimeFormatConfiguration entity, ITimeDimensionMappingBuilder startConfig, ITimeDimensionMappingBuilder endConfig)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (startConfig == null)
            {
                throw new ArgumentNullException(nameof(startConfig));
            }

            if (endConfig == null)
            {
                throw new ArgumentNullException(nameof(endConfig));
            }

            _entity = entity;
            _startConfig = startConfig;
            _endConfig = endConfig;
        }

        /// <inheritdoc />
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, bool addIsNull = false)
        {
            // TODO test it
            // 
            var startWhere = _startConfig.GenerateWhere(dateFrom, null);
            var endWhere = _endConfig.GenerateWhere(null, dateTo.ToEndPeriod());

            if (!string.IsNullOrWhiteSpace(startWhere) && !string.IsNullOrWhiteSpace(endWhere))
            {
                return string.Format(CultureInfo.InvariantCulture, "( {0} ) AND ( {1} )", startWhere, endWhere);
            }

            if (!string.IsNullOrWhiteSpace(startWhere))
            {
                return startWhere;
            }

            return endWhere;
        }

        /// <inheritdoc />
        public string MapComponent(IDataReader reader)
        {
            var startValue = _startConfig.MapComponent(reader);
            var endValue = _endConfig.MapComponent(reader);
            if (startValue == null || endValue == null)
            {
                return null;
            }
            startValue = ToIso(startValue, true);
            endValue = ToIso(endValue, false);

            var normizledTimeRange = DateUtil.NormalizeTimeRange(startValue + '/' + endValue);
            if (normizledTimeRange != null)
            {
                return DateUtil.FormatDate(normizledTimeRange, _entity.OutputFormat);
            }

            return null;
        }

        /// <summary>
        /// Converts the <paramref name="startValue"/> to ISO 8601 if needed.
        /// </summary>
        /// <param name="startValue">The start value.</param>
        /// <param name="startPeriod">if set to <see langword="true" /> [start period].</param>
        /// <returns>The data is ISO 8601 format .</returns>
        private static string ToIso(string startValue, bool startPeriod)
        {
            if (startValue.Length < 10)
            {
                var date = DateUtil.FormatDate(startValue, startPeriod);
                startValue = DateUtil.FormatDate(date, TimeFormat.Date);
            }

            return startValue;
        }
    }
}
