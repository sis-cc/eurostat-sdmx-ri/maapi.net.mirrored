// -----------------------------------------------------------------------
// <copyright file="ComponentMapping1Carray.cs" company="EUROSTAT">
//   Date Created : 2023-01-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// One component is mapped to one dataset column,
    /// with transcoding that maps scalar values to array values.
    /// </summary>
    public class ComponentMapping1Carray : ComponentMappingArrayBase, IComponentArrayMapper
    {
        /// <summary>
        /// The _entity.
        /// </summary>
        private readonly ComponentMappingEntity _mapping;
        
        private readonly string _databaseType;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMapping1Narray"/> class.
        /// </summary>
        /// <param name="mapping">The mapping entity.</param>
        /// <param name="databaseType"></param>
        public ComponentMapping1Carray(ComponentMappingEntity mapping, string databaseType)
            : base(databaseType, mapping.ConstantValues.ToList(), true)
        {
            _mapping = mapping;
            _databaseType = databaseType;
        }

        #region invalid scalar implementations

        /// <summary>
        /// Not applicable for array mappers.
        /// Throws always exception.
        /// </summary>
        /// <param name="conditionValue"></param>
        /// <param name="operatorValue"></param>
        /// <returns></returns>
        /// <exception cref="SdmxException">always</exception>
        public string GenerateComponentWhere(string conditionValue, OperatorType operatorValue)
        {
            throw new SdmxException($"{nameof(ComponentMapping1Carray)} is an array mapper. Use a method that handles array value.");
        }

        /// <summary>
        /// Not applicable for array mappers.
        /// Throws always exception.
        /// </summary>
        /// <param name="conditionValues"></param>
        /// <param name="withNull"></param>
        /// <returns></returns>
        /// <exception cref="SdmxException"></exception>
        public string GenerateComponentWhere(ISet<string> conditionValues, bool withNull = false, bool withNot = false)
        {
            throw new SdmxException($"{nameof(ComponentMapping1Carray)} is an array mapper. Use a method that handles array value.");
        }

        /// <summary>
        /// always
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        /// <exception cref="SdmxException"></exception>
        public string MapComponent(IDataReader reader)
        {
            throw new SdmxException($"{nameof(ComponentMapping1Carray)} is an array mapper. Use a method that handles array value.");
        }

        #endregion

        /// <summary>
        /// Maps to a constant array of sdmx values
        /// </summary>
        /// <param name="dataReader"></param>
        /// <returns>The sdmx array value</returns>
        public string[] MapComponentArray(IDataReader dataReader)
        {
            return _mapping.ConstantValues.ToArray();
        }
    }
}
