// -----------------------------------------------------------------------
// <copyright file="TimeDimension1Column.cs" company="EUROSTAT">
//   Date Created : 2013-04-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System.Data;
    using System.Globalization;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    ///     This Time Dimension Transcoding class is used for 1-1 mappings between
    ///     a Time Dimension and one dissemination column (not Date type)
    /// </summary>
    public class TimeDimension1Column : ITimeDimensionMappingBuilder
    {
        /// <summary>
        /// The mapping
        /// </summary>
        private readonly TimeDimensionMappingEntity _mapping;

        /// <summary>
        ///     The field ordinals
        /// </summary>
        private readonly TimeTranscodingFieldOrdinal _fieldOrdinals;

        /// <summary>
        ///     The where builder.
        /// </summary>
        private readonly TimeTranscodingWhereBuilder _whereBuilder;

        /// <summary>
        /// The invariant culture
        /// </summary>
        private readonly CultureInfo _invariantCulture;

        /// <summary>
        /// The expression
        /// </summary>
        private readonly TimeExpressionEntity _expression;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimeDimension1Column" /> class.
        /// </summary>
        /// <param name="mapping">
        ///     The time dimension mapping
        /// </param>
        /// <param name="expression">
        ///     The TRANSCODING.EXPRESSION contents
        /// </param>
        /// <param name="databaseType">
        ///     The dissemination database vendor from  DB_CONNECTION.DB_TYPE at Mapping Store database. It is used to determine
        ///     the substring command to use
        /// </param>
        public TimeDimension1Column(TimeDimensionMappingEntity mapping, TimeExpressionEntity expression, string databaseType)
        {
            _mapping = mapping;
            _expression = expression;
            string yearPeriodColumn = expression.YearColumnSysId;
            var substringClauseBuilder = new SqlSubstringClauseBuilder(databaseType);
            string yearOnlyStart = substringClauseBuilder.CreateSubStringClause(
                yearPeriodColumn, 
                expression.YearStart + 1, 
                expression.YearLength, 
                ">=");
            string yearOnlyEnd = substringClauseBuilder.CreateSubStringClause(
                yearPeriodColumn, 
                expression.YearStart + 1, 
                expression.YearLength, 
                "<=");

            string whereFormat = substringClauseBuilder.CreateSubStringClause(
                yearPeriodColumn, 
                expression.PeriodStart + 1, 
                expression.PeriodLength, 
                "=");

            string yearOnlyWhereFormat = substringClauseBuilder.CreateSubStringClause(
                yearPeriodColumn, 
                expression.YearStart + 1, 
                expression.YearLength, 
                "=");

            this._whereBuilder = new TimeTranscodingWhereBuilder(
                this._expression, 
                whereFormat, 
                yearOnlyEnd, 
                yearOnlyStart, 
                yearOnlyWhereFormat);
            this._fieldOrdinals = new TimeTranscodingFieldOrdinal(this._expression);
            _invariantCulture = CultureInfo.InvariantCulture;
        }

        /// <summary>
        ///     Generates the SQL Query where condition from the SDMX Query TimeBean <see cref="ISdmxDate" />
        /// </summary>
        /// <param name="dateFrom">The start time period</param>
        /// <param name="dateTo">The end time period</param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>
        ///     The string containing SQL Query where condition
        /// </returns>
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, bool addIsNull = false)
        {
            return this._whereBuilder.WhereBuild(dateFrom, dateTo);
        }

        /// <summary>
        ///     Transcodes the time period returned by the local database to SDMX Time period
        /// </summary>
        /// <param name="reader">
        ///     The data reader reading the Dissemination database
        /// </param>
        /// <returns>
        ///     The transcoded time period, as in SDMX Time period type
        /// </returns>
        public string MapComponent(IDataReader reader)
        {
            string ret = this._mapping.DefaultValue;
            this._fieldOrdinals.BuildOrdinal(reader);

            string yearperiod = DataReaderHelper.GetString(reader, this._fieldOrdinals.YearOrdinal);
            if (this._expression.YearStart + this._expression.YearLength > yearperiod.Length)
            {
                return ret;
            }

            string year = yearperiod.Substring(this._expression.YearStart, this._expression.YearLength);
            var rowFreq = this._expression.Freq;
            if (rowFreq != TimeFormatEnumType.Year)
            {
                if (this._expression.PeriodStart >= yearperiod.Length)
                {
                    return ret;
                }

                string period;
                if (this._expression.PeriodLength > 0)
                {
                    int rowPeriodLen = this._expression.PeriodLength;
                    if (this._expression.PeriodLength + this._expression.PeriodStart > yearperiod.Length)
                    {
                        rowPeriodLen = yearperiod.Length - this._expression.PeriodStart;
                    }

                    period = yearperiod.Substring(this._expression.PeriodStart, rowPeriodLen);
                }
                else
                {
                    period = yearperiod.Substring(this._expression.PeriodStart);
                }

                var periodDsdCode = this._expression.TranscodingRules.GetSdmxCode(period);
                if (periodDsdCode == null)
                {
                    return ret; // MAT-495

                    // periodDsdCode = periodLocalCode;
                }

                ret = string.Format(_invariantCulture, "{0}-{1}", year, periodDsdCode);
            }
            else
            {
                ret = year.ToString(_invariantCulture);
            }

            return ret;
        }
    }
}