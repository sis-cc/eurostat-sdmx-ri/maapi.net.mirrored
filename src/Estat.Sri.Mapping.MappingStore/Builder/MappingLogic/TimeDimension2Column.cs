// -----------------------------------------------------------------------
// <copyright file="TimeDimension2Column.cs" company="EUROSTAT">
//   Date Created : 2013-04-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System.Data;
    using System.Globalization;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    ///     This Time Dimension Transcoding class is used for 1-2 mappings between
    ///     a Time Dimension and two dissemination columns as generated from DATASET.QUERY
    /// </summary>
    public class TimeDimension2Column : ITimeDimensionMappingBuilder
    {
        /// <summary>
        ///     The field ordinals
        /// </summary>
        private readonly TimeTranscodingFieldOrdinal _fieldOrdinals;

        /// <summary>
        ///     The where builder.
        /// </summary>
        private readonly TimeTranscodingWhereBuilder _whereBuilder;

        /// <summary>
        /// The mapping
        /// </summary>
        private readonly TimeDimensionMappingEntity _mapping;

        /// <summary>
        /// The expression
        /// </summary>
        private readonly TimeExpressionEntity _expression;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimeDimension2Column" /> class.
        /// </summary>
        /// <param name="mapping">
        ///     The time dimension mapping
        /// </param>
        /// <param name="expression">
        ///     The TRANSCODING.EXPRESSION contents
        /// </param>
        /// <param name="databaseType">
        ///     The dissemination database vendor from  DB_CONNECTION.DB_TYPE at Mapping Store database. It is used to determine
        ///     the substring command to use
        /// </param>
        public TimeDimension2Column(TimeDimensionMappingEntity mapping, TimeExpressionEntity expression, string databaseType)
        {
            _mapping = mapping;
            _expression = expression;
            string yearOnlyWhereFormat;
            string yearOnlyStart;
            string yearOnlyEnd;
            string whereFormat;
            string yearColumn = expression.YearColumnSysId;
            string periodColumn = expression.PeriodColumnSysId;
            SqlSubstringClauseBuilder sqlSubstringClauseBuilder = new SqlSubstringClauseBuilder(databaseType);
            var formatProvider = CultureInfo.InvariantCulture;
            string periodClause = expression.PeriodLength == 0
                                      ? string.Format(formatProvider, "( {0} = '{1}' )", periodColumn, "{0}")
                                      : sqlSubstringClauseBuilder.CreateSubStringClause(
                                          periodColumn, 
                                          expression.PeriodStart + 1, 
                                          expression.PeriodLength, 
                                          "=");

            if (expression.YearLength == 0)
            {
                yearOnlyStart = string.Format(formatProvider, " ( {0} >= '{1}' )", yearColumn, "{0}");
                yearOnlyEnd = string.Format(formatProvider, " ( {0} <= '{1}' )", yearColumn, "{0}");
                yearOnlyWhereFormat = string.Format(formatProvider, "( {0} = '{1}' )", yearColumn, "{0}");

                // whereFormat = String.Format(FormatProvider,"({0} = '{1}' and {2} )",yearColumn, "{0}",periodClause);
                whereFormat = periodClause;
            }
            else
            {
                yearOnlyStart = sqlSubstringClauseBuilder.CreateSubStringClause(
                    yearColumn, 
                    expression.YearStart + 1, 
                    expression.YearLength, 
                    ">=");
                yearOnlyEnd = sqlSubstringClauseBuilder.CreateSubStringClause(
                    yearColumn, 
                    expression.YearStart + 1, 
                    expression.YearLength, 
                    "<=");

                whereFormat = periodClause;
                yearOnlyWhereFormat = sqlSubstringClauseBuilder.CreateSubStringClause(
                    yearColumn, 
                    expression.YearStart + 1, 
                    expression.YearLength, 
                    "=");
            }

            this._whereBuilder = new TimeTranscodingWhereBuilder(
                this._expression, 
                whereFormat, 
                yearOnlyEnd, 
                yearOnlyStart, 
                yearOnlyWhereFormat);
            this._fieldOrdinals = new TimeTranscodingFieldOrdinal(this._expression);
        }

        /// <summary>
        ///     Generates the SQL Query where condition from the SDMX Query TimeBean <see cref="ISdmxDate" />
        /// </summary>
        /// <param name="dateFrom">The start time period</param>
        /// <param name="dateTo">The end time period</param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>
        ///     The string containing SQL Query where condition
        /// </returns>
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, bool addIsNull = false)
        {
            return this._whereBuilder.WhereBuild(dateFrom, dateTo);
        }

        /// <summary>
        ///     Transcodes the time period returned by the local database to SDMX Time period
        /// </summary>
        /// <param name="reader">
        ///     The data reader reading the Dissemination database
        /// </param>
        /// <returns>
        ///     The transcoded time period, as in SDMX Time period type
        /// </returns>
        public string MapComponent(IDataReader reader)
        {
            this._fieldOrdinals.BuildOrdinal(reader);
            string year = DataReaderHelper.GetString(reader, this._fieldOrdinals.YearOrdinal);
            if (this._expression.YearLength > 0)
            {
                year = year.Substring(this._expression.YearStart, this._expression.YearLength);
            }

            string period = DataReaderHelper.GetString(reader, this._fieldOrdinals.PeriodOrdinal);
            if (this._expression.PeriodLength > 0)
            {
                int rowPeriodLen = this._expression.PeriodLength;
                if (this._expression.PeriodLength + this._expression.PeriodStart > period.Length)
                {
                    rowPeriodLen = period.Length - this._expression.PeriodStart;
                }

                period = period.Substring(this._expression.PeriodStart, rowPeriodLen);
            }

            var periodDsdCode = this._expression.TranscodingRules.GetSdmxCode(period);
            if (periodDsdCode == null)
            {
                return this._mapping.DefaultValue; // MAT-495

                // periodDsdCode = periodLocalCode;
            }

            string ret = string.Format(CultureInfo.InvariantCulture, "{0}-{1}", year, periodDsdCode);

            // ret = _timePeriodTranscoding[String.Format(CultureInfo.InvariantCulture,"{0}-{1}", year, period)];
            return ret;
        }
    }
}