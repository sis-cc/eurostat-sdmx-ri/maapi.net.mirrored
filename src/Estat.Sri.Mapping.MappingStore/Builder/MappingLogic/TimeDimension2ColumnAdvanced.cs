// -----------------------------------------------------------------------
// <copyright file="TimeDimension2ColumnAdvanced.cs" company="EUROSTAT">
//   Date Created : 2018-5-7
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    ///     This Time Dimension Transcoding class is used for 1-2 mappings between
    ///     a Time Dimension and two dissemination columns as generated from DATASET.QUERY
    /// </summary>
    internal class TimeDimension2ColumnAdvanced : ITimeDimensionMappingBuilder
    {
        /// <summary>
        ///     The field ordinals
        /// </summary>
        private readonly TimeTranscodingFieldOrdinal _fieldOrdinals;

        /// <summary>
        ///     The where builder.
        /// </summary>
        private readonly TimeTranscodingWhereBuilder _whereBuilder;

        /// <summary>
        /// The period map
        /// </summary>
        private readonly Dictionary<string, string> _periodMap;

        /// <summary>
        /// The year start
        /// </summary>
        private readonly int _yearStart;
        
        /// <summary>
        /// The year length
        /// </summary>
        private readonly int _yearLength;

        /// <summary>
        /// The period start
        /// </summary>
        private readonly int _periodStart;

        /// <summary>
        /// The period length
        /// </summary>
        private readonly int _periodLength;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimeDimension2ColumnAdvanced" /> class.
        /// </summary>
        /// <param name="mapping">
        ///     The time dimension mapping
        /// </param>
        /// <param name="expression">
        ///     The TRANSCODING.EXPRESSION contents
        /// </param>
        /// <param name="databaseType">
        ///     The dissemination database vendor from  DB_CONNECTION.DB_TYPE at Mapping Store database. It is used to determine
        ///     the substring command to use
        /// </param>
        public TimeDimension2ColumnAdvanced(TimeParticleConfiguration expression, string databaseType)
        {
            string yearOnlyWhereFormat;
            string yearOnlyStart;
            string yearOnlyEnd;
            string whereFormat;
            string yearColumn = expression.Year.Column.Name;
            string periodColumn = expression.Period.Column.Name;
            SqlSubstringClauseBuilder sqlSubstringClauseBuilder = new SqlSubstringClauseBuilder(databaseType);
            var formatProvider = CultureInfo.InvariantCulture;
            int periodStart = expression.Period?.Start ?? 0;
            int periodLength = expression.Period?.Length ?? 0;
            string periodClause = periodLength == 0
                                      ? string.Format(formatProvider, "( {0} = '{1}' )", periodColumn, "{0}")
                                      : sqlSubstringClauseBuilder.CreateSubStringClause(
                                          periodColumn,
                                          periodStart + 1,
                                          periodLength, 
                                          "=");
            _periodStart = periodStart;
            _periodLength = periodLength;

            int yearLength = expression.Year.Length ?? 0;
            if (yearLength == 0)
            {
                yearOnlyStart = string.Format(formatProvider, " ( {0} >= '{1}' )", yearColumn, "{0}");
                yearOnlyEnd = string.Format(formatProvider, " ( {0} <= '{1}' )", yearColumn, "{0}");
                yearOnlyWhereFormat = string.Format(formatProvider, "( {0} = '{1}' )", yearColumn, "{0}");

                // whereFormat = String.Format(FormatProvider,"({0} = '{1}' and {2} )",yearColumn, "{0}",periodClause);
                whereFormat = periodClause;
            }
            else
            {
                int yearStart = expression.Year.Start ?? 0;
                yearOnlyStart = sqlSubstringClauseBuilder.CreateSubStringClause(
                    yearColumn,
                    yearStart + 1,
                    yearLength, 
                    ">=");
                yearOnlyEnd = sqlSubstringClauseBuilder.CreateSubStringClause(
                    yearColumn,
                    yearStart + 1,
                    yearLength, 
                    "<=");

                whereFormat = periodClause;
                yearOnlyWhereFormat = sqlSubstringClauseBuilder.CreateSubStringClause(
                    yearColumn,
                    yearStart + 1,
                    yearLength, 
                    "=");
                _yearStart = yearStart;
                _yearLength = yearLength;
            }

            if (expression.Period != null)
            {
                _periodMap = expression.Period.Rules.ToDictionary(k => k.LocalPeriodCode, v => v.SdmxPeriodCode, StringComparer.Ordinal);
            }

            this._whereBuilder = new TimeTranscodingWhereBuilder(
                expression, 
                whereFormat, 
                yearOnlyEnd, 
                yearOnlyStart, 
                yearOnlyWhereFormat);
            this._fieldOrdinals = new TimeTranscodingFieldOrdinal(expression);
        }

        /// <summary>
        ///     Generates the SQL Query where condition from the SDMX Query TimeBean <see cref="ISdmxDate" />
        /// </summary>
        /// <param name="dateFrom">The start time period</param>
        /// <param name="dateTo">The end time period</param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>
        ///     The string containing SQL Query where condition
        /// </returns>
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, bool addIsNull = false)
        {
            return this._whereBuilder.WhereBuild(dateFrom, dateTo);
        }

        /// <summary>
        ///     Transcodes the time period returned by the local database to SDMX Time period
        /// </summary>
        /// <param name="reader">
        ///     The data reader reading the Dissemination database
        /// </param>
        /// <returns>
        ///     The transcoded time period, as in SDMX Time period type
        /// </returns>
        public string MapComponent(IDataReader reader)
        {
            this._fieldOrdinals.BuildOrdinal(reader);
            string year = DataReaderHelper.GetString(reader, this._fieldOrdinals.YearOrdinal);
            if (this._yearLength > 0)
            {
                year = year.Substring(this._yearStart, this._yearLength);
            }

            string period = DataReaderHelper.GetString(reader, this._fieldOrdinals.PeriodOrdinal);
            if (this._periodLength > 0)
            {
                int rowPeriodLen = this._periodLength;
                if (this._periodLength + this._periodStart > period.Length)
                {
                    rowPeriodLen = period.Length - this._periodStart;
                }

                period = period.Substring(this._periodStart, rowPeriodLen);
            }

            string periodDsdCode;
            if (!_periodMap.TryGetValue(period, out periodDsdCode))
            {
                return null; 
            }

            string ret = string.Format(CultureInfo.InvariantCulture, "{0}-{1}", year, periodDsdCode);

            // ret = _timePeriodTranscoding[String.Format(CultureInfo.InvariantCulture,"{0}-{1}", year, period)];
            return ret;
        }
    }
}