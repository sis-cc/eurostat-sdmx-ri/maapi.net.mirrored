// -----------------------------------------------------------------------
// <copyright file="ComponentMapping1N.cs" company="EUROSTAT">
//   Date Created : 2013-04-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Utils.Extensions;
    using Org.Sdmxsource.Util.Collections;
    using Xml.Schema.Linq;
    using static ComponentMappingCommon;

    using DataSetColumnEntity = Estat.Sri.Mapping.Api.Model.DataSetColumnEntity;

    /// <summary>
    ///     Handles the mapping between 1 component and N columns where N &gt; 1
    /// </summary>
    internal class ComponentMapping1N : IComponentMappingBuilder
    {
        /// <summary>
        /// The _entity.
        /// </summary>
        private readonly ComponentMappingEntity _entity;

        /// <summary>
        /// The transcoding rule map
        /// </summary>
        private readonly TranscodingRuleMap _transcodingRuleMap;

        /// <summary>
        /// The database type.
        /// </summary>
        private readonly string _databaseType;

        /// <summary>
        /// The column ordinal builder
        /// </summary>
        private readonly ColumnOrdinalBuilder _columnOrdinalBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMapping1N"/> class.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="transcodingRuleMap">The transcoding rule map.</param>
        /// <param name="databaseType">The database type.</param>
        public ComponentMapping1N(ComponentMappingEntity entity, TranscodingRuleMap transcodingRuleMap, string databaseType)
        {
            _entity = entity;
            _transcodingRuleMap = transcodingRuleMap;
            _databaseType = databaseType;
            _columnOrdinalBuilder = new ColumnOrdinalBuilder(this._transcodingRuleMap.OrderedColumns);
        }

        /// <summary>
        ///     Generates the SQL Where clause for the component used in this mapping
        ///     and the condition value from SDMX Query which is transcoded
        /// </summary>
        /// <param name="conditionValue">
        ///     string with the conditional value from the SDMX query
        /// </param>
        /// <param name="operatorValue">
        ///     string with the operator value from the SDMX query, "=" by default
        /// </param>
        /// <returns>
        ///     A SQL where clause for the columns of the mapping
        /// </returns>
        public string GenerateComponentWhere(string conditionValue, OperatorType operatorValue)
        {
            var ret = new StringBuilder();
            ret.Append(" (");

            var localCodesSet = _transcodingRuleMap.GetLocalCodes(conditionValue);
            if (localCodesSet.Count > 0)
            {
                for (int i = 0; i < localCodesSet.Count; i++)
                {
                    if (i != 0)
                    {
                        ret.Append(" OR ");
                    }

                    ret.Append(" (");
                    var localCodeEntities = localCodesSet[i].LocalCodes;

                    // component to columns
                    var mappedClause = new List<string>();
                    foreach (DataSetColumnEntity column in this._transcodingRuleMap.OrderedColumns)
                    {
                        string mappedId = column.Name;
                        string mappedValue = localCodeEntities.FirstOrDefault(entity => string.Equals(entity.ParentId, column.Name))?.ObjectId ?? conditionValue;
                        mappedClause.Add(SqlOperatorComponent(mappedId, mappedValue, operatorValue, _databaseType));
                    }

                    ret.Append(string.Join(" AND ", mappedClause.ToArray()));
                    ret.Append(" ) ");
                }
            }
            else
            {
                // TODO find out why this is needed. If no transcoding mapping exists we could use a 1=0 clause
                var mappedClause = new List<string>();
                foreach (DataSetColumnEntity column in this._entity.GetColumns())
                {
                    string mappedId = column.Name;
                    string mappedValue = conditionValue;
                    mappedClause.Add(SqlOperatorComponent(mappedId, mappedValue, operatorValue, _databaseType));
                }

                ret.Append(string.Join(" AND ", mappedClause.ToArray()));
            }

            ret.Append(" )");
            return ret.ToString();
        }

        public string GenerateComponentWhere(ISet<string> conditionValues, bool withNull=false, bool withNot = false)
        {
            if (SupportsMultiColumnInClause(this._databaseType))
            {
                return GenerateComponentWhereInTwoOrMoreColumn(conditionValues);
            }
            else
            {
                List<string> orClauses = new List<string>();
                foreach(var condition in conditionValues)
                {
                    orClauses.Add(GenerateComponentWhere(condition, OperatorType.Exact));
                }

                return string.Format(CultureInfo.InvariantCulture, "( {0} )", string.Join(" OR ", orClauses));
            }
        }

        /// <summary>
        ///     Maps the columns of the mapping to the component of this ComponentMapping1N object
        ///     and transcodes it.
        /// </summary>
        /// <param name="reader">
        ///     The DataReader for retrieving the values of the column.
        /// </param>
        /// <returns>
        ///     The value of the component or null if no transcoding rule for the column values is found
        /// </returns>
        public string MapComponent(IDataReader reader)
        {
            var resultCodes = new string[this._entity.GetColumns().Count];
            _columnOrdinalBuilder.BuildOrdinals(reader);

            foreach (var column in _columnOrdinalBuilder.ColumnOrdinals)
            {
                resultCodes[column.ColumnPosition] = DataReaderHelper.GetString(reader, column.Value);
            }

            var transcodedCodes = this._transcodingRuleMap.GetSdmxCode(resultCodes);
            string ret = this._entity.DefaultValue;
            if (transcodedCodes != null) 
            {
                ret = transcodedCodes;
            }

            return ret;
        }

        private string GenerateComponentWhereInTwoOrMoreColumn(IEnumerable<string> conditionValues)
        {
            Dictionary<string, IList<string>> mappedValues = new Dictionary<string, IList<string>>(StringComparer.Ordinal);
            string[] columns = _transcodingRuleMap.OrderedColumns.Select(x => x.Name).ToArray();
            string escapedMappedId = "(" + string.Join(", ", columns.Select(x => EscapeMappedId(x, _databaseType))) + ")";
            IList<string> mappedValuess = new List<string>();

            foreach (var conditionValue in conditionValues)
            {
                var localCodesSet = _transcodingRuleMap.GetLocalCodes(conditionValue);

                foreach (var rule in localCodesSet)
                {
                    if (rule != null)
                    {
                        var localCodeEntities = rule.LocalCodes;
                        var mappedClause = new List<string>();
                        for (int i = 0; i < _transcodingRuleMap.OrderedColumns.Count; i++)
                        {

                            DataSetColumnEntity column = this._transcodingRuleMap.OrderedColumns[i];
                            string mappedId = column.Name;
                            // in madb 7 change to localcode.ParentId == column.Name
                            string mappedValue = localCodeEntities.FirstOrDefault(entity => string.Equals(entity.ParentId, column.EntityId))?.ObjectId ?? conditionValue;
                            if (!string.IsNullOrEmpty(mappedValue))
                            {
                                mappedValue = EscapeString(mappedValue);
                            }
                            else
                            {
                                mappedValue = string.Empty;
                            }

                            mappedClause.Add(QuoteString(mappedValue));
                        }

                        mappedValuess.Add("(" + string.Join(",", mappedClause) + ")");
                    }

                }

            }

           return GenerateInClause(escapedMappedId, mappedValuess);
        }
    }
}