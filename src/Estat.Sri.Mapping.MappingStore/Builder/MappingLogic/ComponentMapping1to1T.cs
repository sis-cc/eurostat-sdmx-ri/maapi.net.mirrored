// -----------------------------------------------------------------------
// <copyright file="ComponentMapping1to1T.cs" company="EUROSTAT">
//   Date Created : 2013-04-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
#define MAT200

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System.Collections.Generic;
    using System.Data;
    using System.Text;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using static ComponentMappingCommon;

    using DataSetColumnEntity = Estat.Sri.Mapping.Api.Model.DataSetColumnEntity;

    /// <summary>
    ///     Component Mapping 1 To 1T
    /// </summary>
    /// Handles 1-1 mappings with transconding
    public class ComponentMapping1To1T : IComponentMappingBuilder
    {
        /// <summary>
        /// The mapping
        /// </summary>
        private readonly ComponentMappingEntity _mapping;
        private readonly TranscodingRuleMap _transcodingRuleMap;
        private readonly string _databaseType;

        private readonly ColumnOrdinalBuilder _columnOrdinalBuilder;

        public ComponentMapping1To1T(ComponentMappingEntity mapping, TranscodingRuleMap transcodingRuleMap, string databaseType)
        {
            this._mapping = mapping;
            _transcodingRuleMap = transcodingRuleMap;
            _databaseType = databaseType;
            _columnOrdinalBuilder = new ColumnOrdinalBuilder(this._transcodingRuleMap.OrderedColumns);
        }

        /// <summary>
        ///     Generates the SQL Where clause for the component used in this mapping
        ///     and the condition value from SDMX Query which is transcoded
        /// </summary>
        /// <param name="conditionValue">
        ///     string with the conditional value from the sdmx query
        /// </param>
        /// <param name="operatorValue">
        ///     string with the operator value from the sdmx query, "=" by default
        /// </param>
        /// <returns>
        ///     A SQL where clause for the columns of the mapping
        /// </returns>
        public string GenerateComponentWhere(string conditionValue, OperatorType operatorValue)
        {
            var ret = new StringBuilder();
            ret.Append(" (");

            var localCodesSet = this._transcodingRuleMap.GetLocalCodes(conditionValue);

            DataSetColumnEntity column = this._mapping.GetColumns()[0];
            string mappedValue = conditionValue;

            // TODO check if columnIndex == 0 always
            int columnIndex = 0; // was this._mapping.Transcoding.TranscodingRules.ColumnAsKeyPosition[column.Name];
            if (localCodesSet.Count > 0)
            {
                for (int i = 0; i < localCodesSet.Count; i++)
                {
                    var localCodes = localCodesSet[i].LocalCodes;
                    if (localCodes != null && localCodes.Count > 0)
                    {
                        mappedValue = localCodes[columnIndex].ObjectId;
                    }

                    if (i != 0)
                    {
                        ret.Append(" or ");
                    }

                    ret.Append("( " + SqlOperatorComponent(column.Name, mappedValue, operatorValue, _databaseType) + ")");

                    // ret.AppendFormat("( {0} " + operatorValue + " '{1}' )", column.Name, mappedValue);
                }
            }
            else
            {
                ret.Append(" " + SqlOperatorComponent(column.Name, mappedValue, operatorValue, _databaseType));

                // ret.AppendFormat(" {0} " + operatorValue + " '{1}' ", column.Name, mappedValue);
            }

            ret.Append(") ");
            return ret.ToString();
        }

        /// <inheritdoc/>
        public string GenerateComponentWhere(ISet<string> conditionValues, bool withNull = false, bool withNot = false)
        {
            IList<string> mappedValues = new List<string>();

            foreach (var conditionValue in conditionValues)
            {
                string mappedValue = conditionValue; // why we do this since we drop either failed transcodings or use the default value
                var localCodesSet = this._transcodingRuleMap.GetLocalCodes(conditionValue);
                if (localCodesSet.Count > 0)
                {
                    // one SDMX code can man to multile local code values
                    for (int i = 0; i < localCodesSet.Count; i++)
                    {
                        var localCodes = localCodesSet[i].LocalCodes;
                        if (localCodes != null && localCodes.Count > 0)
                        {
                            mappedValue = localCodes[0].ObjectId;
                        }

                        mappedValues.Add(QuoteString(EscapeString(mappedValue)));
                    }
                }
            }

            var escapedMappedId = EscapeMappedId(this._mapping.GetColumns()[0].Name, _databaseType);
            return GenerateInClause(escapedMappedId, mappedValues);
        }

        /// <summary>
        ///     Maps the column of the mapping to the component of this ComponentMapping1to1T object
        ///     and transcodes it.
        /// </summary>
        /// <param name="reader">
        ///     The DataReader for retrieving the values of the column.
        /// </param>
        /// <returns>
        ///     The value of the component or null if no transcoding rule for the column values is found
        /// </returns>
        public string MapComponent(IDataReader reader)
        {
            this._columnOrdinalBuilder.BuildOrdinals(reader);
            var column = this._columnOrdinalBuilder.ColumnOrdinals[0];
            string columnValue = DataReaderHelper.GetString(reader, column.Value);
            var transcodedCodes = this._transcodingRuleMap.GetSdmxCode(columnValue);
            string ret = this._mapping.DefaultValue;
            if (transcodedCodes != null)
            {
                ret = transcodedCodes;
            }

            return ret;
        }
    }
}