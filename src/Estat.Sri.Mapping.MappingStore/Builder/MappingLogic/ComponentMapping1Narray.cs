// -----------------------------------------------------------------------
// <copyright file="ComponentMapping1Narray.cs" company="EUROSTAT">
//   Date Created : 2023-01-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// Maps N dataset column values to a component with N array values, no transcoding.
    /// </summary>
    public class ComponentMapping1Narray : ComponentMappingArrayBase, IComponentArrayMapper
    {
        /// <summary>
        /// The _entity.
        /// </summary>
        private readonly ComponentMappingEntity _mapping;

        private readonly string _databaseType;

        /// <summary>
        /// The column ordinal builder
        /// </summary>
        private readonly ColumnOrdinalBuilder _columnOrdinalBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMapping1Narray"/> class.
        /// </summary>
        /// <param name="mapping">The mapping entity.</param>
        /// <param name="databaseType"></param>
        public ComponentMapping1Narray(ComponentMappingEntity mapping, string databaseType)
            : base(databaseType, mapping.GetColumns().Select(c => c.Name).ToList(), false)
        {
            _mapping = mapping;
            _databaseType = databaseType;
            _columnOrdinalBuilder = new ColumnOrdinalBuilder(mapping.GetColumns());
        }

        #region invalid scalar implementations

        /// <summary>
        /// Not applicable for array mappers.
        /// Throws always exception.
        /// </summary>
        /// <param name="conditionValue"></param>
        /// <param name="operatorValue"></param>
        /// <returns></returns>
        /// <exception cref="SdmxException">always</exception>
        public string GenerateComponentWhere(string conditionValue, OperatorType operatorValue)
        {
            throw new SdmxException($"{nameof(ComponentMapping1Narray)} is an array mapper. Use a method that handles array value.");
        }

        /// <summary>
        /// Not applicable for array mappers.
        /// Throws always exception.
        /// </summary>
        /// <param name="conditionValues"></param>
        /// <param name="withNull"></param>
        /// <returns></returns>
        /// <exception cref="SdmxException"></exception>
        public string GenerateComponentWhere(ISet<string> conditionValues, bool withNull = false, bool withNot = false)
        {
            throw new SdmxException($"{nameof(ComponentMapping1Carray)} is an array mapper. Use a method that handles array value.");
        }

        /// <summary>
        /// Not applicable for array mappers.
        /// Throws always exception.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        /// <exception cref="SdmxException">always</exception>
        public string MapComponent(IDataReader reader)
        {
            throw new SdmxException($"{nameof(ComponentMapping1Narray)} is an array mapper. Use a method that handles array value.");
        }

        #endregion

        /// <summary>
        /// Maps N dataset columns to a N-length sdmx array value
        /// </summary>
        /// <param name="reader"></param>
        /// <returns>The sdmx array value.</returns>
        public string[] MapComponentArray(IDataReader reader)
        {
            var resultCodes = new string[this._mapping.GetColumns().Count];
            _columnOrdinalBuilder.BuildOrdinals(reader);

            foreach (var column in _columnOrdinalBuilder.ColumnOrdinals)
            {
                resultCodes[column.ColumnPosition] = DataReaderHelper.GetString(reader, column.Value);
            }

            return resultCodes;
        }
    }
}
