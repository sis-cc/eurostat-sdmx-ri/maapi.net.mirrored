// -----------------------------------------------------------------------
// <copyright file="TimeDimensionMultiFrequency.cs" company="EUROSTAT">
//   Date Created : 2013-07-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Text;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    ///     The time dimension multi frequency transcoding.
    /// </summary>
    public class TimeDimensionMultiFrequency : ITimeDimensionFrequencyMappingBuilder
    {
        /// <summary>
        ///     The _frequency component mapping
        /// </summary>
        private readonly IComponentMappingBuilder _frequencyComponentMapping;

        /// <summary>
        ///     The _time dimension mappings.
        /// </summary>
        private readonly IDictionary<string, ITimeDimensionMappingBuilder> _timeDimensionMappings;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimeDimensionMultiFrequency" /> class.
        /// </summary>
        /// <param name="timeDimensionMappings">The time dimension mappings.</param>
        /// <param name="frequencyComponentMapping">The frequency component mapping.</param>
        /// <exception cref="System.ArgumentNullException"><paramref name="timeDimensionMappings" /> is null</exception>
        /// <exception cref="ArgumentNullException"><paramref name="timeDimensionMappings" /> is null</exception>
        public TimeDimensionMultiFrequency(
            IDictionary<string, ITimeDimensionMappingBuilder> timeDimensionMappings, 
            IComponentMappingBuilder frequencyComponentMapping)
        {
            if (timeDimensionMappings == null)
            {
                throw new ArgumentNullException("timeDimensionMappings");
            }

            this._timeDimensionMappings = timeDimensionMappings;
            this._frequencyComponentMapping = frequencyComponentMapping;
        }

        /// <summary>
        ///     Generates the SQL Query where condition from the SDMX Query Time
        /// </summary>
        /// <param name="dateFrom">
        ///     The start time
        /// </param>
        /// <param name="dateTo">
        ///     The end time
        /// </param>
        /// <param name="frequencyValue">
        ///     The frequency value
        /// </param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>
        ///     The string containing SQL Query where condition
        /// </returns>
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, string frequencyValue, bool addIsNull = false)
        {
            var whereSql = new StringBuilder();
            if (frequencyValue != null)
            {
                ITimeDimensionMappingBuilder engine;
                if (this._timeDimensionMappings.TryGetValue(frequencyValue, out engine))
                {
                    var whereClause = SdmxDateUtils.GenerateWhereClause(dateFrom, dateTo, frequencyValue, engine, this._frequencyComponentMapping);
                    whereSql.Append(whereClause);
                }
            }
            else if (this._timeDimensionMappings.Count > 0)
            {
                string op = string.Empty;
                foreach (var timeDimensionMapping in this._timeDimensionMappings)
                {
                    var generateWhere = SdmxDateUtils.GenerateWhereClause(
                        dateFrom, 
                        dateTo, 
                        timeDimensionMapping.Key, 
                        timeDimensionMapping.Value,
                        this._frequencyComponentMapping);
                    if (!string.IsNullOrWhiteSpace(generateWhere))
                    {
                        whereSql.AppendFormat(CultureInfo.InvariantCulture, "{0} ( {1} )", op, generateWhere);
                        op = " OR ";
                    }
                }
            }

            return whereSql.Length > 0 ? string.Format(CultureInfo.InvariantCulture, "({0})", whereSql) : string.Empty;
        }

        /// <summary>
        ///     Map component.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        /// <param name="frequencyValue">
        ///     The frequency value.
        /// </param>
        /// <returns>
        ///     The transcoded value
        /// </returns>
        public string MapComponent(IDataReader reader, string frequencyValue)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (frequencyValue == null)
            {
                throw new ArgumentNullException("frequencyValue");
            }

            // SDMXRI-842 In order to support Frequency codes with multipliers e.g. A10 (every ten years), M9 (every 9 months) without advanced mapping
            // we get the first character from frequency value since at this time frequencies like A10 are not supported
            string firstFrequencyCharacter;
            if (frequencyValue.Length > 1)
            {
                firstFrequencyCharacter = frequencyValue.Substring(0,1);
            }
            else
            {
                // it could be empty, not sure if this was supported
                firstFrequencyCharacter = frequencyValue;
            }

            ITimeDimensionMappingBuilder engine;
            if (this._timeDimensionMappings.TryGetValue(firstFrequencyCharacter, out engine))
            {
                return engine.MapComponent(reader);
            }

            return null;
        }
    }
}