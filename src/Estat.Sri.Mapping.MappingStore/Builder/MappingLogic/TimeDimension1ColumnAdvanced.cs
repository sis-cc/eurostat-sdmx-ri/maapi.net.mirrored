// -----------------------------------------------------------------------
// <copyright file="TimeDimension1ColumnAdvanced.cs" company="EUROSTAT">
//   Date Created : 2018-5-7
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    ///     This Time Dimension Transcoding class is used for 1-1 mappings between
    ///     a Time Dimension and one dissemination column (not Date type)
    /// </summary>
    internal class TimeDimension1ColumnAdvanced : ITimeDimensionMappingBuilder
    {
        /// <summary>
        ///     The field ordinals
        /// </summary>
        private readonly TimeTranscodingFieldOrdinal _fieldOrdinals;

        /// <summary>
        ///     The where builder.
        /// </summary>
        private readonly TimeTranscodingWhereBuilder _whereBuilder;

        /// <summary>
        /// The invariant culture
        /// </summary>
        private readonly CultureInfo _invariantCulture;

        /// <summary>
        /// The expression
        /// </summary>
        private readonly TimeParticleConfiguration _expression;

        private readonly Dictionary<string, string> _periodMap;

        private readonly int _yearStart;
        private readonly int _yearLength;

        private readonly int _periodStart;
        private readonly int _periodLength;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimeDimension1ColumnAdvanced" /> class.
        /// </summary>
        /// <param name="mapping">
        ///     The time dimension mapping
        /// </param>
        /// <param name="expression">
        ///     The TRANSCODING.EXPRESSION contents
        /// </param>
        /// <param name="databaseType">
        ///     The dissemination database vendor from  DB_CONNECTION.DB_TYPE at Mapping Store database. It is used to determine
        ///     the substring command to use
        /// </param>
        public TimeDimension1ColumnAdvanced(TimeParticleConfiguration expression, string databaseType)
        {
            _expression = expression;
            string yearPeriodColumn = expression.Year.Column.Name;
            var substringClauseBuilder = new SqlSubstringClauseBuilder(databaseType);
            int yearStart = expression.Year.Start.GetValueOrDefault(0);
            int yearLength = expression.Year.Length.GetValueOrDefault(0);
            string yearOnlyStart = substringClauseBuilder.CreateSubStringClause(
                yearPeriodColumn,
                yearStart + 1,
                yearLength,
                ">=");
            string yearOnlyEnd = substringClauseBuilder.CreateSubStringClause(
                yearPeriodColumn,
                yearStart + 1,
                yearLength,
                "<=");

            int start = expression.Period?.Start ?? 0;
            int length = expression.Period?.Length ?? 0;
            string whereFormat = substringClauseBuilder.CreateSubStringClause(
                yearPeriodColumn,
                start + 1,
                length,
                "=");

            string yearOnlyWhereFormat = substringClauseBuilder.CreateSubStringClause(
                yearPeriodColumn,
                yearStart + 1,
                yearLength,
                "=");

            this._whereBuilder = new TimeTranscodingWhereBuilder(
                this._expression, 
                whereFormat, 
                yearOnlyEnd, 
                yearOnlyStart, 
                yearOnlyWhereFormat);
            this._fieldOrdinals = new TimeTranscodingFieldOrdinal(this._expression);
            _invariantCulture = CultureInfo.InvariantCulture;
            _yearStart = yearStart;
            _yearLength = yearLength;
            _periodStart = start;
            _periodLength = length;

            if (expression.Period != null)
            {
                _periodMap = expression.Period.Rules.ToDictionary(k => k.LocalPeriodCode, v => v.SdmxPeriodCode, StringComparer.Ordinal);
            }
        }

        /// <summary>
        ///     Generates the SQL Query where condition from the SDMX Query TimeBean <see cref="ISdmxDate" />
        /// </summary>
        /// <param name="dateFrom">The start time period</param>
        /// <param name="dateTo">The end time period</param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>
        ///     The string containing SQL Query where condition
        /// </returns>
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, bool addIsNull = false)
        {
            return this._whereBuilder.WhereBuild(dateFrom, dateTo);
        }

        /// <summary>
        ///     Transcodes the time period returned by the local database to SDMX Time period
        /// </summary>
        /// <param name="reader">
        ///     The data reader reading the Dissemination database
        /// </param>
        /// <returns>
        ///     The transcoded time period, as in SDMX Time period type
        /// </returns>
        public string MapComponent(IDataReader reader)
        {
            string ret = null;
            this._fieldOrdinals.BuildOrdinal(reader);

            string yearperiod = DataReaderHelper.GetString(reader, this._fieldOrdinals.YearOrdinal);
            int yearStart = _yearStart;
            int yearLength = _yearLength;
            if (yearStart + yearLength > yearperiod.Length)
            {
                return ret;
            }

            string year = yearperiod.Substring(yearStart, yearLength);
            var rowFreq = this._expression.Format;
            if (_periodMap != null)
            {
                int periodStart = _periodStart;
                if (periodStart >= yearperiod.Length)
                {
                    return ret;
                }

                string period;
                if (this._expression.Period.Length > 0)
                {
                    int rowPeriodLen = _periodLength;
                    if (this._expression.Period.Length + periodStart > yearperiod.Length)
                    {
                        rowPeriodLen = yearperiod.Length - periodStart;
                    }

                    period = yearperiod.Substring(periodStart, rowPeriodLen);
                }
                else
                {
                    period = yearperiod.Substring(periodStart);
                }

                string periodDsdCode;
                if (!_periodMap.TryGetValue(period, out periodDsdCode))
                {
                    return ret; // MAT-495

                    // periodDsdCode = periodLocalCode;
                }

                ret = string.Format(_invariantCulture, "{0}-{1}", year, periodDsdCode);
            }
            else
            {
                ret = year;
            }

            return ret;
        }
    }
}