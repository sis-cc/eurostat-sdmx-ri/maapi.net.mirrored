// -----------------------------------------------------------------------
// <copyright file="TimeTranscodingWhereBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-08-05
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;
    using Estat.Sri.Mapping.MappingStore.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;

    /// <summary>
    ///     The time transcoding where builder.
    /// </summary>
    internal class TimeTranscodingWhereBuilder
    {
        /// <summary>
        ///     The _format provider.
        /// </summary>
        private static readonly IFormatProvider _formatProvider = CultureInfo.InvariantCulture;

        /// <summary>
        ///     The _periodicity.
        /// </summary>
        private readonly IPeriodicity _periodicity;

        /// <summary>
        ///     The _where format.
        /// </summary>
        private readonly string _whereFormat;

        /// <summary>
        ///     The _year only end.
        /// </summary>
        private readonly string _yearOnlyEnd;

        /// <summary>
        ///     The _year only start.
        /// </summary>
        private readonly string _yearOnlyStart;

        /// <summary>
        ///     The _year only where format.
        /// </summary>
        private readonly string _yearOnlyWhereFormat;

        private readonly Dictionary<string, string> _periodMap;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeTranscodingWhereBuilder" /> class.
        /// </summary>
        /// <param name="transcodingEntity">The transcoding entity.</param>
        /// <param name="whereFormat">The where Format.</param>
        /// <param name="yearOnlyEnd">The year Only End.</param>
        /// <param name="yearOnlyStart">The year Only Start.</param>
        /// <param name="yearOnlyWhereFormat">The year Only Where Format.</param>
        public TimeTranscodingWhereBuilder(
            TimeParticleConfiguration transcodingEntity,
            string whereFormat,
            string yearOnlyEnd,
            string yearOnlyStart,
            string yearOnlyWhereFormat)
        {
            if (transcodingEntity == null)
            {
                throw new ArgumentNullException(nameof(transcodingEntity));
            }

            this._periodicity = PeriodicityFactory.Create(transcodingEntity.Format.GetTimeFormat());
            _periodMap = transcodingEntity.Period.Rules.ToDictionary(k => k.SdmxPeriodCode, v => v.LocalPeriodCode);
            this._whereFormat = whereFormat;
            this._yearOnlyEnd = yearOnlyEnd;
            this._yearOnlyStart = yearOnlyStart;
            this._yearOnlyWhereFormat = yearOnlyWhereFormat;

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="TimeTranscodingWhereBuilder" /> class.
        /// </summary>
        /// <param name="transcodingEntity">The transcoding entity.</param>
        /// <param name="whereFormat">The where Format.</param>
        /// <param name="yearOnlyEnd">The year Only End.</param>
        /// <param name="yearOnlyStart">The year Only Start.</param>
        /// <param name="yearOnlyWhereFormat">The year Only Where Format.</param>
        public TimeTranscodingWhereBuilder(
            TimeExpressionEntity transcodingEntity, 
            string whereFormat, 
            string yearOnlyEnd, 
            string yearOnlyStart, 
            string yearOnlyWhereFormat)
        {
            if (transcodingEntity == null)
            {
                throw new ArgumentNullException(nameof(transcodingEntity));
            }

            this._periodicity = PeriodicityFactory.Create(transcodingEntity.Freq);
            if (transcodingEntity.TranscodingRules != null)
            {
                _periodMap = transcodingEntity.TranscodingRules.TimePeriodToLocalPeriod;
            }

            this._whereFormat = whereFormat;
            this._yearOnlyEnd = yearOnlyEnd;
            this._yearOnlyStart = yearOnlyStart;
            this._yearOnlyWhereFormat = yearOnlyWhereFormat;

        }

        /// <summary>
        ///     Extracts a SdmxQueryTimeVO<see cref="SdmxQueryTimeVO" /> object from a TimeBean<see cref="ISdmxDate" />
        /// </summary>
        /// <param name="dateFrom">
        ///     Start time <see cref="ISdmxDate" /> A SDMX Query Time element
        /// </param>
        /// <param name="dateTo">
        ///     End time <see cref="ISdmxDate" /> A SDMX Query Time element
        /// </param>
        /// <returns>
        ///     SdmxQueryTimeVO<see cref="SdmxQueryTimeVO" />
        /// </returns>
        public SdmxQueryTimeVO ExtractTimeBean(ISdmxDate dateFrom, ISdmxDate dateTo)
        {
            var startDate = dateFrom.ToQueryPeriod(this._periodicity)
                            ?? new SdmxQueryPeriod { HasPeriod = false, Year = 0, Period = 1 };
            var now = DateTime.Now;
            var endDate = dateTo.ToQueryPeriod(this._periodicity)
                          ?? new SdmxQueryPeriod
                                 {
                                     HasPeriod = false, 
                                     Year = now.Year, 
                                     Period =
                                         ((DateTime.Now.Month - 1) / this._periodicity.MonthsPerPeriod)
                                         + 1
                                 };

            if (!startDate.HasPeriod && startDate.Period == 0)
            {
                startDate.Period = 1;
            }

            if (!endDate.HasPeriod && endDate.Period == 0)
            {
                endDate.Period = this._periodicity.PeriodCount;
            }

            return new SdmxQueryTimeVO
                       {
                           EndPeriod = endDate.Period, 
                           EndYear = endDate.Year, 
                           HasEndPeriod = endDate.HasPeriod, 
                           HasStartPeriod = startDate.HasPeriod, 
                           StartPeriod = startDate.Period, 
                           StartYear = startDate.Year
                       };
        }

        /// <summary>
        ///     The where build.
        /// </summary>
        /// <param name="fromDate">
        ///     The from date.
        /// </param>
        /// <param name="toDate">
        ///     The to date.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public string WhereBuild(ISdmxDate fromDate, ISdmxDate toDate)
        {
            if (fromDate == null && toDate == null)
            {
                return string.Empty;
            }

            // TODO this method needs a clean up
            // NumberFormatInfo f = CultureInfo.InvariantCulture.NumberFormat;
            var ret = new StringBuilder("(");

            SdmxQueryTimeVO time = this.ExtractTimeBean(fromDate, toDate);

            // start time includes a period and the freq is not annual
            var endYearString = time.EndYear;
            var previousYearBeforeEnd = time.EndYear - 1;
            if (time.HasStartPeriod && this._periodicity.TimeFormat.EnumType != TimeFormatEnumType.Year)
            {
                int firstPeriodLength = this._periodicity.PeriodCount;
                if (toDate != null && time.StartYear == time.EndYear)
                {
                    firstPeriodLength = time.EndPeriod;
                }

                var startYearString = time.StartYear;
                int lastClause = this.BuildPeriodWhere(
                    ret,
                    startYearString,
                    time.StartPeriod,
                    firstPeriodLength,
                    this._periodicity);
                var nextYearAfterStart = (time.StartYear + 1).ToString(_formatProvider);
                if (toDate != null)
                {
                    if (time.StartYear + 1 < time.EndYear)
                    {
                        ret.Append(" (");
                        ret.AppendFormat(CultureInfo.InvariantCulture, this._yearOnlyStart, nextYearAfterStart);
                        ret.Append(" and ");
                        ret.AppendFormat(CultureInfo.InvariantCulture, this._yearOnlyEnd, previousYearBeforeEnd);
                        ret.Append(")");

                        lastClause = ret.Length;
                        ret.Append(" or ");
                    }

                    if (time.StartYear != time.EndYear)
                    {
                        if (time.HasEndPeriod)
                        {
                            lastClause = this.BuildPeriodWhere(ret, endYearString, 1, time.EndPeriod, this._periodicity);
                        }
                        else
                        {
                            ret.Append(
                                string.Format(
                                    _formatProvider,
                                    this._yearOnlyWhereFormat,
                                    endYearString));
                            lastClause = ret.Length;
                            ret.Append(" or ");
                        }
                    }

                    ret.Length = lastClause;
                }
                else
                {
                    ret.Append(" (");
                    ret.AppendFormat(_formatProvider, this._yearOnlyStart, nextYearAfterStart);
                    ret.Append(")");
                }
            }
            else if (time.HasEndPeriod)
            {
                // we have only endDate. So we that records that are before the requested year or are in one of the months
                ret.AppendFormat(_formatProvider, this._yearOnlyEnd, previousYearBeforeEnd);
                ret.Append(" or ");
                var lastClause = this.BuildPeriodWhere(ret, endYearString, 1, time.EndPeriod, this._periodicity);
                ret.Length = lastClause;
            }
            else
            {
                ret.AppendFormat(
                    _formatProvider, 
                    this._yearOnlyStart, 
                    time.StartYear.ToString(_formatProvider));
                if (toDate != null)
                {
                    ret.Append(" and ");
                    ret.AppendFormat(
                        _formatProvider, 
                        this._yearOnlyEnd, 
                        endYearString);
                }
            }

            ret.Append(")");
            return ret.ToString();
        }

        /// <summary>
        ///     Build where clauses in the form of : (year and (periodA or periodB or ... ) and write them
        ///     to a specified StringBuilder
        /// </summary>
        /// <param name="ret">The StringBuilder to write the output</param>
        /// <param name="yearStr">The year string</param>
        /// <param name="firstPeriod">The first period</param>
        /// <param name="lastPeriod">The last period</param>
        /// <param name="periodicity">The periodicity.</param>
        /// 
        /// <returns>
        ///     The position of the last clause inside the StringBuilder
        /// </returns>
        private int BuildPeriodWhere(
            StringBuilder ret,
            int yearStr,
            int firstPeriod,
            int lastPeriod,
            IPeriodicity periodicity)
        {
            ret.Append("( ");
            ret.AppendFormat(_formatProvider, this._yearOnlyWhereFormat, yearStr);
            int lastClause = ret.Length;
            ret.Append(" and ");
            ret.Append("( ");
            for (int x = firstPeriod; x <= lastPeriod; x++)
            {
                string periodStr = x.ToString(periodicity.Format, _formatProvider);

                // TODO old code had [0][0]. How it could be certain that transcoding was set
                string periodCode;
                if (_periodMap.TryGetValue(periodStr, out periodCode))
                {
                    ret.AppendFormat(CultureInfo.InvariantCulture, this._whereFormat, periodCode);
                    lastClause = ret.Length;
                    ret.Append(" or ");
                }
                else
                {
                    Debug.Fail("Invalid period", "Could not find:" + periodStr);
                }
            }

            ret.Length = lastClause;
            ret.Append(") ");
            ret.Append(") ");
            lastClause = ret.Length;
            ret.Append(" or ");
            return lastClause;
        }
    }
}