// -----------------------------------------------------------------------
// <copyright file="TimeDimensionEndDurationMappingBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-5-14
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Data;
    using Estat.Sri.Mapping.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;

    internal class TimeDimensionEndDurationMappingBuilder : ITimeDimensionMappingBuilder
    {
        private readonly ITimeDimensionMappingBuilder _endBuilder;

        private readonly IPeriodicity _outputPeriodicity;

        private readonly IComponentMappingBuilder _durationMappingBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDimensionEndDurationMappingBuilder"/> class.
        /// </summary>
        /// <param name="endBuilder">The end builder.</param>
        /// <param name="durationMappingBuilder">The duration mapping builder.</param>
        /// <param name="outputFormat">The output format.</param>
        /// <exception cref="ArgumentNullException">
        /// endBuilder
        /// or
        /// durationMappingBuilder
        /// </exception>
        public TimeDimensionEndDurationMappingBuilder(ITimeDimensionMappingBuilder endBuilder, IComponentMappingBuilder durationMappingBuilder, TimeFormat outputFormat)
        {
            if (endBuilder == null)
            {
                throw new ArgumentNullException(nameof(endBuilder));
            }

            if (durationMappingBuilder == null)
            {
                throw new ArgumentNullException(nameof(durationMappingBuilder));
            }

            _endBuilder = endBuilder;
            _durationMappingBuilder = durationMappingBuilder;
            _outputPeriodicity = PeriodicityFactory.Create(outputFormat);
        }

        /// <summary>
        /// Generates the SQL Query where condition from the SDMX Query Time
        /// </summary>
        /// <param name="dateFrom">The start time</param>
        /// <param name="dateTo">The end time</param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>The string containing SQL Query where condition</returns>
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, bool addIsNull = false)
        {
            return _endBuilder.GenerateWhere(dateFrom, dateTo.ToEndPeriod());
        }

        /// <summary>
        /// Maps the column(s) of the mapping to the component(s) of this IComponentMapping
        /// </summary>
        /// <param name="reader">The DataReader for retrieving the values of the column.</param>
        /// <returns>The value of the component</returns>
        public string MapComponent(IDataReader reader)
        {
            var endPeriod = _endBuilder.MapComponent(reader);

            if (endPeriod == null)
            {
                return null;
            }

            var duration = _durationMappingBuilder.MapComponent(reader);

            if (duration != null)
            {
                var dateWithDuration = DateUtil.NormalizeTimeRange(duration + "/" + endPeriod);
                return _outputPeriodicity.ToString(dateWithDuration);
            }

            var lastCase = DateUtil.FormatDateOffSetDuration(endPeriod);
            return _outputPeriodicity.ToString(lastCase);
        }
    }
}