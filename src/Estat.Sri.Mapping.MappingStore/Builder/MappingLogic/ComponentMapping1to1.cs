// -----------------------------------------------------------------------
// <copyright file="ComponentMapping1to1.cs" company="EUROSTAT">
//   Date Created : 2013-04-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model.AdvancedTime;

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using static ComponentMappingCommon;

    /// <summary>
    ///     Component Mapping 1 To 1
    /// </summary>
    /// Handles one to one mappings without transconding
    public class ComponentMapping1To1 : IComponentMappingBuilder
    {
        /// <summary>
        /// The mapping
        /// </summary>
        private readonly ComponentMappingEntity _mapping;

        /// <summary>
        /// The database type.
        /// </summary>
        private readonly string _databaseType;

        /// <summary>
        ///     The position of the column of this mapping inside the row
        ///     in the reader
        /// </summary>
        private int _columnOrdinal = -1;

        /// <summary>
        ///     The _field type
        /// </summary>
        private Type _fieldType;

        /// <summary>
        ///     The last data reader
        /// </summary>
        private IDataReader _lastReader;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMapping1To1"/> class.
        /// </summary>
        /// <param name="mapping">The mapping.</param>
        /// <param name="databaseType">The database type.</param>
        public ComponentMapping1To1(DurationMappingEntity mapping, string databaseType)
        {
            this._mapping = new ComponentMappingEntity() { EntityId = mapping.EntityId, DefaultValue = mapping.DefaultValue};
            this._mapping.AddColumn(mapping.Column);
            _databaseType = databaseType;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMapping1To1"/> class.
        /// </summary>
        /// <param name="mapping">The mapping.</param>
        /// <param name="databaseType">The database type.</param>
        public ComponentMapping1To1(ComponentMappingEntity mapping, string databaseType)
        {
            this._mapping = mapping;
            _databaseType = databaseType;
        }

        /// <summary>
        ///     Generates the SQL Where clause for the component used in this mapping
        ///     and the condition value from SDMX Query as it is
        /// </summary>
        /// <param name="conditionValue">
        ///     string with the conditional value from the SDMX query
        /// </param>
        /// <param name="operatorValue">
        ///     string with the operator value from the sdmx query, "=" by default
        /// </param>
        /// <returns>
        ///     A SQL where clause for the column of the mapping
        /// </returns>
        public string GenerateComponentWhere(string conditionValue, OperatorType operatorValue)
        {
            return " ( "
                   + ComponentMappingCommon.SqlOperatorComponent(this._mapping.GetColumns()[0].Name, conditionValue, operatorValue, _databaseType)
                   + ") ";

            // return string.Format(CultureInfo.InvariantCulture, " ( {0} " + operatorValue + " '{1}' ) ", this._mapping.Columns[0].Name, EscapeString(conditionValue));
        }

        /// <inheritdoc/>
        public string GenerateComponentWhere(ISet<string> conditionValues, bool withNull = false, bool withNot = false)
        {
            var quotedConditionValues =conditionValues.Select(x => QuoteString(EscapeString(x))).ToArray();
            var escapedMappedId = EscapeMappedId(this._mapping.GetColumns()[0].Name, _databaseType);
            return GenerateInClause(escapedMappedId, quotedConditionValues, withNull, withNot);
        }

        /// <summary>
        ///     Maps the column of the mapping to the component of this ComponentMapping1to1 object
        /// </summary>
        /// <param name="reader">
        ///     The DataReader for retrieving the values of the column.
        /// </param>
        /// <returns>
        ///     The value of the component or String.Empty in case the column value is null
        /// </returns>
        public string MapComponent(IDataReader reader)
        {
            if (!ReferenceEquals(reader, this._lastReader))
            {
                this._columnOrdinal = -1;
                this._lastReader = reader;
            }

            if (this._columnOrdinal == -1)
            {
                if (reader == null)
                {
                    throw new ArgumentNullException("reader");
                }

                this._columnOrdinal = reader.GetOrdinal(this._mapping.GetColumns()[0].Name);
                this._fieldType = reader.GetFieldType(this._columnOrdinal);
            }

            string val = this._mapping.DefaultValue ?? string.Empty;
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (!reader.IsDBNull(this._columnOrdinal))
            {
                if (this._fieldType == typeof(double))
                {
                    val = reader.GetDouble(this._columnOrdinal).ToString(CultureInfo.InvariantCulture);
                }
                else if (this._fieldType == typeof(float))
                {
                    val = reader.GetFloat(this._columnOrdinal).ToString(CultureInfo.InvariantCulture);
                }
                else if (this._fieldType == typeof(string))
                {
                    val = reader.GetString(this._columnOrdinal);
                }
                else if (this._fieldType == typeof(DateTime))
                {
                    // TODO New feature : Check the Component text format to select the appropriate format
                    val = DateUtil.FormatDateUTC(reader.GetDateTime(this._columnOrdinal), false);
                }
                else
                {
                    val = Convert.ToString(reader.GetValue(this._columnOrdinal), CultureInfo.InvariantCulture);
                }
            }

            return val;
        }
    }
}