// -----------------------------------------------------------------------
// <copyright file="SdmxDateUtils.cs" company="EUROSTAT">
//   Date Created : 2018-5-14
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Globalization;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    internal static class SdmxDateUtils
    {
        /// <summary>
        /// To the end period.
        /// </summary>
        /// <param name="sdmxDate">The SDMX date.</param>
        /// <returns>ISdmxDate.</returns>
        public static ISdmxDate ToEndPeriod(this ISdmxDate sdmxDate)
        {
            if (sdmxDate == null)
            {
                return null;
            }

            var duration = sdmxDate.Duration;
            var date = sdmxDate.Date;
            date = date.AddYears(duration.Years);
            date = date.AddMonths(duration.Months);
            date = date.AddDays(duration.Days);
            date = date.AddHours(duration.Hours);
            date = date.AddMinutes(duration.Minutes);
            date = date.AddSeconds(duration.Seconds - 1);
            return new SdmxDateCore(date, sdmxDate.TimeFormatOfDate);
        }

        /// <summary>
        ///     Generates the where clause.
        /// </summary>
        /// <param name="dateFrom">The date from.</param>
        /// <param name="dateTo">The date to.</param>
        /// <param name="frequencyValue">The frequency value.</param>
        /// <param name="engine">The engine.</param>
        /// <param name="frequencyComponentMapping"></param>
        /// <returns>The where clause </returns>
        public static string GenerateWhereClause(
            ISdmxDate dateFrom, 
            ISdmxDate dateTo, 
            string frequencyValue, 
            ITimeDimensionMappingBuilder engine,
            IComponentMappingBuilder frequencyComponentMapping)
        {

            // SDMXRI-842 In order to support Frequency codes with multipliers e.g. A10 (every ten years), M9 (every 9 months) without advanced mapping
            // we use the StartsWith operator at frequency column since at this time frequencies like A10 are not supported
            // this is translated to a LIKE '<Freq Code>%'
            // TODO possibly translate StartsWith to SUBSTR[ING](0,1) instead of LIKE but we need performance tests first.
            string frequencyWhereClause = frequencyComponentMapping.GenerateComponentWhere(frequencyValue, OperatorType.StartsWith);
            if (!string.IsNullOrWhiteSpace(frequencyWhereClause))
            {
                string timePeriodsWhereClauses = engine.GenerateWhere(dateFrom, dateTo);
                if (!string.IsNullOrWhiteSpace(timePeriodsWhereClauses))
                {
                    var whereClause = string.Format(
                        CultureInfo.InvariantCulture, 
                        "(( {0} ) and ( {1} ))", 
                        frequencyWhereClause, 
                        timePeriodsWhereClauses);
                    return whereClause;
                }
            }

            return string.Empty;
        }
    }
}