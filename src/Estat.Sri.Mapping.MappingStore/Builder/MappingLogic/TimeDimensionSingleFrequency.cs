// -----------------------------------------------------------------------
// <copyright file="TimeDimensionSingleFrequency.cs" company="EUROSTAT">
//   Date Created : 2013-07-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    using Estat.Sri.Mapping.Api.Builder;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    ///     The time dimension single frequency.
    /// </summary>
    public class TimeDimensionSingleFrequency : ITimeDimensionFrequencyMappingBuilder
    {
        /// <summary>
        ///     The _time dimension mapping.
        /// </summary>
        private readonly ITimeDimensionMappingBuilder _timeDimensionMapping;

        private readonly IComponentMappingBuilder _frequencyMapping;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimeDimensionSingleFrequency" /> class.
        /// </summary>
        /// <param name="timeDimensionMapping">
        ///     The time dimension mapping.
        /// </param>
        /// <param name="frequencyMapping"></param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="timeDimensionMapping" /> is null
        /// </exception>
        public TimeDimensionSingleFrequency(
            ITimeDimensionMappingBuilder timeDimensionMapping,
            IComponentMappingBuilder frequencyMapping)
        {
            if (timeDimensionMapping == null)
            {
                throw new ArgumentNullException("timeDimensionMapping");
            }

            this._timeDimensionMapping = timeDimensionMapping;
            this._frequencyMapping = frequencyMapping;
        }

        /// <summary>
        ///     Generates the SQL Query where condition from the SDMX Query Time
        /// </summary>
        /// <param name="dateFrom">
        ///     The start time
        /// </param>
        /// <param name="dateTo">
        ///     The end time
        /// </param>
        /// <param name="frequencyValue">
        ///     The frequency value
        /// </param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>
        ///     The string containing SQL Query where condition
        /// </returns>
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, string frequencyValue, bool addIsNull = false)
        {
            // For DSD wih Time Dimension having a FREQ dimension is is strongly recommended but not mandatory
            if (_frequencyMapping == null)
            {
                return this._timeDimensionMapping.GenerateWhere(dateFrom, dateTo);
            }

            // TODO for not-transcoded we may need to produce complex where to cover cases like
            /*
             len(time) = 4 and time = 2010 OR
             substr(time,6, 1) = A and time => 2010-A1 OR
			substr(time,6, 1) = Q and time => 2010-Q1 OR
			substr(time,6, 1) = S and time => 2010-S1 OR
			substr(time,6, 1) = T and time => 2010-T1 OR 
			substr(time,6, 1) = M and time => 2010-M01 OR
			substr(time,6, 1) = W and time => 2010-W01 OR
			len(time) = 7 and time => 2010-01 OR
			len(time) = 10 and time => 2010-01-01 OR
			substr(time,11,1) = 'T' and time => 2010-01-01T00:00:00 OR
			substring(time,11,1) = '/' and time >= 2010-01-01 
             */
            List<string> clauses = new List<string>();
            TimeFormat[] formats;
            
            if (!string.IsNullOrEmpty(frequencyValue))
            {
                formats = TimeFormat.GetTimeFormatsFromCodeId(frequencyValue.Substring(0,1)).ToArray();
            }
            else
            {
                formats = TimeFormat.Values.Where(format => !string.IsNullOrEmpty(format.FrequencyCode) && format != TimeFormat.TimeRange).ToArray();
            }

            foreach (var timeFormat in formats)
            {
                string whereClause = GenerateWhereForSpecificFreq(dateFrom, dateTo, timeFormat);
                if (!string.IsNullOrWhiteSpace(whereClause))
                {
                    clauses.Add(whereClause);
                }
            }

            if (clauses.Count == 0)
            {
                return string.Empty;
            }

            string whereSql = string.Join(" OR ", clauses);
            return whereSql.Length > 0 ? string.Format(CultureInfo.InvariantCulture, "({0})", whereSql) : string.Empty;
        }

        private string GenerateWhereForSpecificFreq(ISdmxDate dateFrom, ISdmxDate dateTo, TimeFormat format)
        {
            if (dateFrom != null)
            {
                dateFrom = new SdmxDateCore(dateFrom.Date, format.EnumType);
            }

            if (dateTo != null)
            {
                var endPeriod = dateTo.ToEndPeriod();
                dateTo = new SdmxDateCore(endPeriod.Date, format.EnumType);
            }

            return SdmxDateUtils.GenerateWhereClause(dateFrom, dateTo, format.FrequencyCode, this._timeDimensionMapping, this._frequencyMapping);
        }

        /// <summary>
        ///     Map component.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        /// <param name="frequencyValue">
        ///     The frequency value.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public string MapComponent(IDataReader reader, string frequencyValue)
        {
            return this._timeDimensionMapping.MapComponent(reader);
        }
    }
}