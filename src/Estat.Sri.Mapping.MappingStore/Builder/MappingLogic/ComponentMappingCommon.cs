// -----------------------------------------------------------------------
// <copyright file="ComponentMappingCommon.cs" company="EUROSTAT">
//   Date Created : 2017-09-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using System.Runtime.CompilerServices;
// Used for testing without making the class public
[assembly: InternalsVisibleTo("Estat.Sri.Mapping.MappingStore.Tests")] 
namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Utils.Helper;
    

    /// <summary>
    /// The component mapping common code.
    /// </summary>
    internal static class ComponentMappingCommon
    {
        /// <summary>
        ///     Escape <paramref name="input" /> to make it safer for SQL. It replaces a single quote with two single quotes
        ///     characters.
        /// </summary>
        /// <param name="input">
        ///     The literal to escape
        /// </param>
        /// <returns>
        ///     The escaped <paramref name="input" />
        /// </returns>
        public static string EscapeString(string input)
        {
            // TODO Use DbParameters.
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            return input.Replace("'", "''");
        }

        /// <summary>
        ///     Quote <paramref name="input" /> to make it safer for SQL. It replaces a single quote with two single quotes
        ///     characters.
        /// </summary>
        /// <param name="input">
        ///     The literal to escape
        /// </param>
        /// <returns>
        ///     The escaped <paramref name="input" />
        /// </returns>
        public static string QuoteString(string input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            return string.Format(CultureInfo.InvariantCulture, "'{0}'", input);
        }

        /// <summary>
        /// Gets a value indicating if the specific <paramref name="databaseType"/> supports multi column IN clauses
        /// e.g. <code>... WHERE (COLA,COLB) in (('A1', 'B1'), ('A2', 'B3'))</code>
        /// </summary>
        /// <param name="databaseType"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static bool SupportsMultiColumnInClause(string databaseType)
        {
            if (databaseType == null)
            {
                throw new ArgumentNullException(nameof(databaseType));
            }

            var provider = DatabaseType.GetProviderName(databaseType);
            return DatabaseType.DatabaseSettings[provider].SupportsMultiColumnInClause;
        }

        /// <summary>
        ///     Build the SQL condition part (based on the provided operator)
        /// </summary>
        /// <param name="mappedId">
        ///     The mapped identifier
        /// </param>
        /// <param name="mappedValue">
        ///     The mapped value
        /// </param>
        /// <param name="operatorValue">
        ///     The value of the ordinal or text search operator
        /// </param>
        /// <param name="databaseType">
        ///     The database type. Is used to escape <paramref name="mappedId"/> when it is a column name.
        ///     Pass in <c>null</c> when <paramref name="mappedId"/> is not a column name.
        /// </param>
        /// <returns>
        ///     The SQL condition part
        /// </returns>
        public static string SqlOperatorComponent(string mappedId, string mappedValue, OperatorType operatorValue, string databaseType)
        {
            var value = EscapeString(mappedValue);

            if (databaseType != null)
            {
                mappedId = EscapeMappedId(mappedId, databaseType);
            }

            // TODO 2 see if we need to escape the % and _ in like operations. All supported DB vendors seem to support ESCAPE keyword
            // TODO 3 use DbParameters
            switch (operatorValue)
            {
                case OperatorType.Contains:
                    return string.Format(CultureInfo.InvariantCulture, "{0} LIKE '%{1}%' ", mappedId, value);
                case OperatorType.Not | OperatorType.Contains:
                    return string.Format(CultureInfo.InvariantCulture, "{0} NOT LIKE '%{1}%' ", mappedId, value);
                case OperatorType.Not | OperatorType.EndsWith:
                    return string.Format(CultureInfo.InvariantCulture, "{0} NOT LIKE '%{1}' ", mappedId, value);
                case OperatorType.Not | OperatorType.StartsWith:
                    return string.Format(CultureInfo.InvariantCulture, "{0} NOT LIKE '{1}%' ", mappedId, value);
                case OperatorType.EndsWith:
                    return string.Format(CultureInfo.InvariantCulture, "{0} LIKE '%{1}' ", mappedId, value);
                case OperatorType.StartsWith:
                    return string.Format(CultureInfo.InvariantCulture, "{0} LIKE '{1}%' ", mappedId, value);
                case OperatorType.Exact:
                    return string.IsNullOrEmpty(mappedValue)
                        ? string.Format(CultureInfo.InvariantCulture, "{0} IS NULL ", mappedId)
                        : string.Format(CultureInfo.InvariantCulture, "{0} = '{1}' ", mappedId, value);
                case OperatorType.Not | OperatorType.Exact:
                    return string.IsNullOrEmpty(mappedValue)
                        ? string.Format(CultureInfo.InvariantCulture, "{0} IS NOT NULL ", mappedId)
                        : string.Format(CultureInfo.InvariantCulture, "{0} != '{1}' ", mappedId, value);
                case OperatorType.LessThanOrEqual:
                    return string.Format(CultureInfo.InvariantCulture, "{0} <= '{1}' ", mappedId, value);
                case OperatorType.GreaterThanOrEqual:
                    return string.Format(CultureInfo.InvariantCulture, "{0} >= '{1}' ", mappedId, value);
                case OperatorType.LessThan:
                    return string.Format(CultureInfo.InvariantCulture, "{0} < '{1}' ", mappedId, value);
                case OperatorType.GreaterThan:
                    return string.Format(CultureInfo.InvariantCulture, "{0} > '{1}' ", mappedId, value);
                case OperatorType.NullOrExact:
                    return string.IsNullOrEmpty(mappedValue)
                        ? string.Format(CultureInfo.InvariantCulture, "{0} IS NULL ", mappedId)
                        : string.Format(CultureInfo.InvariantCulture, "({0} IS NULL OR {0} = '{1}') ", mappedId, value);
            }

           throw new ArgumentOutOfRangeException(nameof(operatorValue), operatorValue, "Not supported");
        }

        public static string EscapeMappedId(string mappedId, string databaseType, bool quoteMappedId = false)
        {
            if (mappedId == null)
            {
                throw new ArgumentNullException(nameof(mappedId));
            }

            if (databaseType == null)
            {
                throw new ArgumentNullException(nameof(databaseType));
            }

            var provider = DatabaseType.GetProviderName(databaseType);
            var reservedKeywordToStringFormat = DatabaseType.DatabaseSettings[provider].ReservedKeywordToStringFormat;

            var ret = string.Format(reservedKeywordToStringFormat, mappedId);
            if (quoteMappedId)
            {
                return QuoteString(ret);
            }
            else
            {
                return ret;
            }
        }

        public static string GenerateInClause(string escapedColumnOrLeftPart, IList<string> mappedValues, bool withNull = false, bool withNot = false)
        {
            List<string> partClauses = new List<string>();
            // Oracle has a hardcoded limit of 1000 IN clauses
            // But in the future we could consider a faster way, maybe a temp table ?
            for (int i = 0; i < mappedValues.Count; i += 1000)
            {
                var partition = string.Join(",", mappedValues.Skip(i).Take(1000));
                partClauses.Add(string.Format(CultureInfo.InvariantCulture, "{0} {1}IN ({2})", escapedColumnOrLeftPart, withNot ? "NOT " : string.Empty, partition));
            }

            if (withNull)
            {
                partClauses.Add(string.Format(CultureInfo.InvariantCulture, "{0} IS NULL", escapedColumnOrLeftPart));
            }

            if (partClauses.Count > 0)
            {
                return string.Format(CultureInfo.InvariantCulture, " ( {0} ) ", string.Join(" OR ", partClauses));
            }


            return string.Empty;
        }
    }
}