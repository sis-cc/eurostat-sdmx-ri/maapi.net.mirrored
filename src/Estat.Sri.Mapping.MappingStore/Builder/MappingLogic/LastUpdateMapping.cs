// -----------------------------------------------------------------------
// <copyright file="LastUpdateMapping.cs" company="EUROSTAT">
//   Date Created : 2016-11-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Model.Base;

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    /// Handles the mapping of <see cref="Api.Model.LastUpdatedEntity"/> 
    /// </summary>
    public class LastUpdateMapping : ILastUpdateMappingBuilder
    {
        /// <summary>
        /// The last updated entity
        /// </summary>
        private readonly LastUpdatedEntity _lastUpdatedEntity;

        /// <summary>
        /// The Dissemination database
        /// </summary>
        private readonly Database _ddbDatabase;

        /// <summary>
        /// Initializes a new instance of the <see cref="LastUpdateMapping" /> class.
        /// </summary>
        /// <param name="lastUpdatedEntity">The last updated entity.</param>
        /// <param name="ddbDatabase">The Dissemination database.</param>
        public LastUpdateMapping(LastUpdatedEntity lastUpdatedEntity, Database ddbDatabase)
        {
            _lastUpdatedEntity = lastUpdatedEntity;
            this._ddbDatabase = ddbDatabase;
        }

        /// <summary>
        /// Generates the where.
        /// </summary>
        /// <param name="timeRanges">The time ranges.</param>
        /// <returns>The <see cref="MappingStoreRetrieval.Model.SqlClause"/></returns>
        /// <exception cref="System.ArgumentException">Neither column or constant value set - entity</exception>
        public SqlClause GenerateWhere(IList<ITimeRange> timeRanges)
        {
            var entity = _lastUpdatedEntity;
            if (entity.Column == null && entity.Constant.HasValue)
            {
                if (!timeRanges.All(range => range.IsInRange(entity.Constant.Value)))
                {
                    throw new SdmxNoResultsException("No data found in the requested ranges");
                }

                return null;
            }

            if (entity.Column == null)
            {
                throw new ArgumentException("Neither column or constant value set", "entity");
            }

            List<string> clauses = new List<string>();
            List<DbParameter> parameters = new List<DbParameter>();
            int count = 0;
            foreach (var timeRange in timeRanges)
            {
                string startOp = timeRange.IsStartInclusive ? ">=" : ">";
                string endOp = timeRange.IsEndInclusive ? "<=" : "<";
                if (timeRange.IsRange)
                {
                    var baseLastStartName = string.Format(CultureInfo.InvariantCulture, "last_up_start_param{0}", count);
                    var baseLastEndName = string.Format(CultureInfo.InvariantCulture, "last_up_end_param{0}", count);
                    var startParam = this._ddbDatabase.CreateInParameter(baseLastStartName, DbType.DateTime, timeRange.StartDate.Date);
                    var endParam = this._ddbDatabase.CreateInParameter(baseLastEndName, DbType.DateTime, timeRange.EndDate.Date);

                    var clauseFormat = string.Format(CultureInfo.InvariantCulture, "(({0} {1} {3}) AND ({0} {2} {4}))", entity.Column.Name, startOp, endOp, startParam.ParameterName, endParam.ParameterName);
                    clauses.Add(clauseFormat);
                    parameters.Add(startParam);
                    parameters.Add(endParam);
                }
                else
                {
                    var sqlOperator = timeRange.StartDate != null ? startOp : endOp;
                    var lastUpdated = timeRange.StartDate ?? timeRange.EndDate;
                    var baseParamName = string.Format(CultureInfo.InvariantCulture, "last_update_param{0}", count);

                    var parameter = this._ddbDatabase.CreateInParameter(baseParamName, DbType.DateTime, lastUpdated.Date);
                    var clauseFormat = string.Format(CultureInfo.InvariantCulture, "({0} {1} {2})", entity.Column.Name, sqlOperator, parameter.ParameterName);
                    clauses.Add(clauseFormat);
                    parameters.Add(parameter);
                }

                count++;
            }

            var allClauses = string.Join(" AND ", clauses);
            return new SqlClause(allClauses, parameters);
        }

        /// <summary>
        /// Generates where statement for asOf querystring parameter
        /// </summary>
        /// <param name="validTo">ValidTo mapping</param>
        /// <param name="asOfDate">AsOfDate query parameter</param>
        /// <returns></returns>
        public SqlClause AsOfWhere(ComponentMappingEntity validTo, ISdmxDate asOfDate)
        {
            var validToColumn = validTo.GetColumns().FirstOrDefault();

            if (_lastUpdatedEntity.Column == null || validToColumn == null)
            {
                throw new ArgumentException("Invalid mappings for asOf parameter");
            }

            var asOfParam = _ddbDatabase.CreateInParameter("asOf", DbType.DateTime, asOfDate.Date);

            return new SqlClause($"{_lastUpdatedEntity.Column.Name} <= {asOfParam.ParameterName} AND {validToColumn.Name} > {asOfParam.ParameterName}", new List<DbParameter>()
            {
                asOfParam
            });
        }
    }
}