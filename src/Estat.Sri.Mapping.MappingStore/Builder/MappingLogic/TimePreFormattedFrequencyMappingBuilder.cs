// -----------------------------------------------------------------------
// <copyright file="TimePreFormattedFrequencyMappingBuilder.cs" company="EUROSTAT">
//   Date Created : 2021-06-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System.Data;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    /// A <see cref="ITimeDimensionFrequencyMappingBuilder"/> that uses the <see cref="TimePreFormattedEntity"/>.
    /// </summary>
    public class TimePreFormattedFrequencyMappingBuilder : ITimeDimensionFrequencyMappingBuilder
    {
        /// <summary>
        ///     The _time dimension mapping.
        /// </summary>
        private readonly ITimeDimensionMappingBuilder _timeDimensionMapping;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimePreFormattedFrequencyMappingBuilder"/> class.
        /// </summary>
        /// <param name="timePreFormattedEntity">The <see cref="TimePreFormattedEntity"/></param>
        /// <param name="mapping">The time dimension mapping.</param>
        /// <param name="databaseType">The type of database.</param>
        public TimePreFormattedFrequencyMappingBuilder(TimePreFormattedEntity timePreFormattedEntity, TimeDimensionMappingEntity mapping, string databaseType)
        {
            _timeDimensionMapping = new TimePreFormattedMappingBuilder(
                timePreFormattedEntity.FromColumn.Name,
                timePreFormattedEntity.ToColumn.Name,
                mapping,
                databaseType);
        }

        /// <summary>
        ///     Generates the SQL Query where condition from the SDMX Query Time
        /// </summary>
        /// <param name="dateFrom">
        ///     The start time
        /// </param>
        /// <param name="dateTo">
        ///     The end time
        /// </param>
        /// <param name="frequencyValue">
        ///     The frequency value
        /// </param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>
        ///     The string containing SQL Query where condition
        /// </returns>
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, string frequencyValue, bool addIsNull = false)
        {
            return this._timeDimensionMapping.GenerateWhere(dateFrom, dateTo, addIsNull);
        }

        /// <summary>
        ///     Map component.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        /// <param name="frequencyValue">
        ///     The frequency value.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public string MapComponent(IDataReader reader, string frequencyValue)
        {
            return this._timeDimensionMapping.MapComponent(reader);
        }
    }
}
