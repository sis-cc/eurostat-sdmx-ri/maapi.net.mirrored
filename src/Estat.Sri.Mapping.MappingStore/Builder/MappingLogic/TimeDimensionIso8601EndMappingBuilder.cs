// -----------------------------------------------------------------------
// <copyright file="TimeDimensionIso8601EndMappingBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-6-4
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;

    /// <summary>
    /// Class TimeDimensionIso8601EndMappingBuilder.
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Builder.ITimeDimensionMappingBuilder" />
    internal class TimeDimensionIso8601EndMappingBuilder : ITimeDimensionMappingBuilder
    {
        private readonly IComponentMappingBuilder _durationMapping;

        private readonly IPeriodicity _outputPeriodicity;

        private readonly string _startWhereFormat;

        private readonly string _endWhereFormat;

        private readonly string _duration;

        /// <summary>
        ///     The field ordinals
        /// </summary>
        private readonly TimeTranscodingFieldOrdinal _fieldOrdinals;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDimensionIso8601EndMappingBuilder"/> class.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <param name="outputFormat">The output format.</param>
        /// <param name="durationMapping">The duration mapping.</param>
        /// <exception cref="ArgumentNullException">
        /// expression
        /// or
        /// outputFormat
        /// </exception>
        public TimeDimensionIso8601EndMappingBuilder(TimeParticleConfiguration expression, TimeFormat outputFormat, IComponentMappingBuilder durationMapping)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            if (outputFormat == null)
            {
                throw new ArgumentNullException(nameof(outputFormat));
            }

            _durationMapping = durationMapping;

            _outputPeriodicity = PeriodicityFactory.Create(TimeFormat.TimeRange);

            var dateColumnSysId = expression.DateColumn.Name;
            string columnName = dateColumnSysId;
            _startWhereFormat = string.Format(
                CultureInfo.InvariantCulture,
                "({0}>= '{1}')",
                columnName,
                "{0}");
            _endWhereFormat = string.Format(
                CultureInfo.InvariantCulture,
                "({0}<= '{1}')",
                columnName,
                "{0}");
            _fieldOrdinals = new TimeTranscodingFieldOrdinal(expression);
            if (outputFormat != TimeFormat.TimeRange)
            {
                _duration = outputFormat.IsoDuration;
            }
        }

        /// <inheritdoc />
        public string MapComponent(IDataReader reader)
        {
            _fieldOrdinals.BuildOrdinal(reader);

            var result = DataReaderHelper.GetString(reader, _fieldOrdinals.DateOrdinal);

            if (result == null)
            {
                return null;
            }

            var duration = _duration;

            if (_durationMapping != null)
            {
                duration = _durationMapping.MapComponent(reader);
            }

            if (result.Contains("/"))
            {
                var normalized = DateUtil.NormalizeTimeRange(result);
                return _outputPeriodicity.ToString(normalized);
            }

            if (duration != null)
            {
                var normalized = DateUtil.NormalizeTimeRange(duration + '/' + result);
                return _outputPeriodicity.ToString(normalized);
            }

            var date = DateUtil.FormatDateOffSetDuration(result);
            return _outputPeriodicity.ToString(date);
        }

        /// <inheritdoc />
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, bool addIsNull = false)
        {
            if (dateFrom == null && dateTo == null)
            {
                return string.Empty;
            }

            var sqlArray = new List<string>();
            IFormatProvider fmt = CultureInfo.InvariantCulture;

            // DateTime startTime = SdmxPeriodToDateTime(timeBean.StartTime,true);
            if (dateFrom != null)
            {
                DateTime startTime = dateFrom.Date;

                sqlArray.Add(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        _startWhereFormat,
                        startTime.ToString("yyyy-MM-dd", fmt)));
            }

            if (dateTo != null)
            {
                // DateTime endTime = SdmxPeriodToDateTime(timeBean.EndTime, false);
                DateTime endTime = dateTo.ToEndPeriod().Date;

                sqlArray.Add(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        _endWhereFormat,
                        endTime.ToString("yyyy-MM-dd", fmt)));
            }

            return string.Format(CultureInfo.InvariantCulture, "({0})", string.Join(" and ", sqlArray.ToArray()));
        }
    }
}