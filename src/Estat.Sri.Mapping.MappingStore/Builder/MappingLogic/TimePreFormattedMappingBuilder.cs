// -----------------------------------------------------------------------
// <copyright file="TimePreFormattedMappingBuilder.cs" company="EUROSTAT">
//   Date Created : 2021-06-03
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Utils.Helper;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    ///     This Time Dimension mapping class is used for mappings between
    ///     either a starting period and/or ending period and the corresponding dissemination columns (of Date type)
    ///     using the time-pre-formatted entity.
    /// </summary>
    public class TimePreFormattedMappingBuilder : ITimeDimensionMappingBuilder
    {
        /// <summary>
        ///     The format template for the greater than 'from' where clause.
        /// </summary>
        private readonly string _fromWhere;

        /// <summary>
        ///     The format template for the lesser to 'to' where clause.
        /// </summary>
        private readonly string _toWhere;

        /// <summary>
        ///     The from column name.
        /// </summary>
        private readonly string _fromColumn;

        /// <summary>
        ///     The to column name.
        /// </summary>
        private readonly string _toColumn;

        /// <summary>
        /// The mapping
        /// </summary>
        private readonly TimeDimensionMappingEntity _mapping;

        /// <summary>
        /// The column ordinal builder
        /// </summary>
        private readonly ColumnOrdinalBuilder _columnOrdinalBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimePreFormattedMappingBuilder"/> class.
        /// </summary>
        /// <param name="fromColumn">The column name that contains the start-period information.</param>
        /// <param name="toColumn">The column name that contains the end-period information.</param>
        /// <param name="mapping">The time dimension mapping.</param>
        /// <param name="databaseType">The database type.</param>
        public TimePreFormattedMappingBuilder(string fromColumn, string toColumn, TimeDimensionMappingEntity mapping, string databaseType)
        {
            if (fromColumn == null)
            {
                throw new ArgumentNullException("fromColumn");
            }
            if (toColumn == null)
            {
                throw new ArgumentNullException("toColumn");
            }
            if (databaseType == null)
            {
                throw new ArgumentNullException("databaseType");
            }

            _fromColumn = fromColumn;
            _toColumn = toColumn;
            _mapping = mapping;
            _columnOrdinalBuilder = new ColumnOrdinalBuilder(mapping.GetColumns());

            string provider = DatabaseType.GetProviderName(databaseType);
            string cast = DatabaseType.DatabaseSettings[provider].DateCast;

            this._fromWhere = string.Format(
                CultureInfo.InvariantCulture,
                " ( {0} >= {2}'{1}' ) {3} ",
                fromColumn,
                "{0}",
                cast,
                "{1}");
            this._toWhere = string.Format(
                CultureInfo.InvariantCulture,
                " ( {0} <= {2}'{1}' ) {3} ",
                toColumn,
                "{0}",
                cast,
                "{1}");
        }

        /// <summary>
        ///     Generates the SQL Query where condition from the SDMX Query TimeBean <see cref="ISdmxDate" />
        /// </summary>
        /// <param name="dateFrom">The start time period</param>
        /// <param name="dateTo">The end time period</param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>
        ///     The string containing SQL Query where condition
        /// </returns>
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, bool addIsNull = false)
        {
            if (dateFrom == null && dateTo == null)
            {
                return string.Empty;
            }

            var sqlArray = new List<string>();
            IFormatProvider fmt = CultureInfo.InvariantCulture;

            if (dateFrom != null)
            {
                var startTime = dateFrom.Date;

                sqlArray.Add(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        this._fromWhere,
                        startTime.ToString("yyyy-MM-dd HH:mm:ss", fmt),
                        addIsNull ? $" OR ({this._fromColumn} IS NULL)" : string.Empty));
            }

            if (dateTo != null)
            {
                var endTime = IsWithTimeComponent(dateTo)
                    ? dateTo.Date
                    : dateTo.ToEndPeriod().Date;

                sqlArray.Add(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        this._toWhere,
                        endTime.ToString("yyyy-MM-dd HH:mm:ss", fmt),
                        addIsNull ? $" OR ({this._toColumn} IS NULL)" : string.Empty));
            }

            return string.Format(CultureInfo.InvariantCulture, "({0})", string.Join(" and ", sqlArray.ToArray()));
        }

        private bool IsWithTimeComponent(ISdmxDate date)
        {
            var format = date.TimeFormatOfDate;
            return format == TimeFormat.Hour || format == TimeFormat.DateTime;
        }

        /// <summary>
        ///     Transcodes the time period returned by the local database to SDMX Time period
        /// </summary>
        /// <param name="reader">
        ///     The data reader reading the Dissemination database
        /// </param>
        /// <returns>
        ///     The transcoded time period, as in SDMX Time period type
        /// </returns>
        public string MapComponent(IDataReader reader)
        {
            _columnOrdinalBuilder.BuildOrdinals(reader);
            return DataReaderHelper.GetString(reader, _columnOrdinalBuilder.ColumnOrdinals[0].Value) ?? this._mapping.DefaultValue;
        }
    }
}
