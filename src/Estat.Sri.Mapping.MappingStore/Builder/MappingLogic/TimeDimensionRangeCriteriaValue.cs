// -----------------------------------------------------------------------
// <copyright file="TimeDimensionRangeCriteriaValue.cs" company="EUROSTAT">
//   Date Created : 2018-5-4
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;
    using Estat.Sri.Mapping.MappingStore.Factory.MappingLogic;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    public class TimeDimensionRangeCriteriaValue : ITimeDimensionFrequencyMappingBuilder
    {
        private readonly TimeTranscodingAdvancedEntity _advancedEntity;
        
        /// <summary>
        ///     The _time dimension mappings.
        /// </summary>
        private readonly IDictionary<string, ITimeDimensionMappingBuilder> _timeDimensionMappings;

        /// <summary>
        /// The default
        /// </summary>
        private ITimeDimensionMappingBuilder _default;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDimensionRangeCriteriaValue"/> class.
        /// </summary>
        /// <param name="advancedEntity">The advanced entity.</param>
        /// <param name="durationMappingBuilders">The duration mapping builders.</param>
        /// <param name="databaseType">Type of the database.</param>
        /// <exception cref="ArgumentNullException">advancedEntity</exception>
        public TimeDimensionRangeCriteriaValue(
            TimeTranscodingAdvancedEntity advancedEntity,
            Dictionary<string, IComponentMappingBuilder> durationMappingBuilders,
            string databaseType)
        {
            if (advancedEntity == null)
            {
                throw new ArgumentNullException(nameof(advancedEntity));
            }

            if (advancedEntity.CriteriaColumn == null)
            {
                throw new ArgumentException("Missing Criteria Column", nameof(advancedEntity));
            }

            _advancedEntity = advancedEntity;
            _timeDimensionMappings = new Dictionary<string, ITimeDimensionMappingBuilder>(StringComparer.Ordinal);
            var x = new TimeDimensionMappingWithTranscodingFactory();
            foreach (var formatConfiguration in advancedEntity.TimeFormatConfigurations)
            {
                IComponentMappingBuilder durationMappingBuilder = null;
                if (durationMappingBuilders == null || formatConfiguration.Value.DurationMap == null || !durationMappingBuilders.TryGetValue(formatConfiguration.Value.DurationMap.EntityId, out durationMappingBuilder))
                {
                    durationMappingBuilder = null;
                }

                var builder = x.GetBuilder(
                    advancedEntity.CriteriaColumn,
                    formatConfiguration.Value,
                    durationMappingBuilder,
                    databaseType);
                if (builder != null)
                {
                    _timeDimensionMappings.Add(formatConfiguration.Key, builder);
                }
            }

            if (_timeDimensionMappings.Count == 1)
            {
                _default = _timeDimensionMappings.First().Value;
            }
        }

        /// <summary>
        /// Maps the Time Dimension value (which extracted from <paramref name="reader" /> and normalized according to the <paramref name="frequencyValue" />
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="frequencyValue">The frequency value.</param>
        /// <returns>The normalized time dimension value</returns>
        public string MapComponent(IDataReader reader, string frequencyValue)
        {
            // frequencyValue is ignored as we don't use it in the advanced mapping to define the output format.

            // we have a criteria column that may or may not be a frequency column
            // TODO profile to see if we need ColumnOrdinalBuilder (.net specific optimization)
            string currentCriteriaValue = reader.GetSafeString(_advancedEntity.CriteriaColumn.Name);

            ITimeDimensionMappingBuilder builder;
            if (_timeDimensionMappings.TryGetValue(currentCriteriaValue, out builder))
            {
                return builder.MapComponent(reader);
            }

            if (_default != null)
            {
                return _default.MapComponent(reader);
            }

            return null;
        }

        /// <summary>
        /// Generates the SQL Query where condition from the SDMX Query Time
        /// </summary>
        /// <param name="dateFrom">The start time</param>
        /// <param name="dateTo">The end time</param>
        /// <param name="frequencyValue">The frequency value</param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>The string containing SQL Query where condition</returns>
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, string frequencyValue, bool addIsNull = false)
        {
            if (_timeDimensionMappings.Count == 0)
            {
                return string.Empty;
            }

            // Because the criteria column may not be the frequency 
            // We cannot expect the frequencyValue to be used for this case
            var whereClaueses = new List<string>();
            foreach (var timeDimensionMappingBuilder in _timeDimensionMappings)
            {
                string item = GenerateWhereClause(dateFrom, dateTo, timeDimensionMappingBuilder.Key, timeDimensionMappingBuilder.Value, _advancedEntity.CriteriaColumn);
                if (!string.IsNullOrWhiteSpace(item))
                {
                    whereClaueses.Add(item);
                }
            }

            if (whereClaueses.Count == 0)
            {
                return string.Empty;
            }

            return string.Format(CultureInfo.InvariantCulture, "( {0} )", string.Join(" OR ", whereClaueses));
        }

        /// <summary>
        ///     Generates the where clause.
        /// </summary>
        /// <param name="dateFrom">The date from.</param>
        /// <param name="dateTo">The date to.</param>
        /// <param name="frequencyValue">The frequency value.</param>
        /// <param name="engine">The engine.</param>
        /// <returns>The where clause </returns>
        private string GenerateWhereClause(
            ISdmxDate dateFrom, 
            ISdmxDate dateTo, 
            string frequencyValue, 
            ITimeDimensionMappingBuilder engine, DataSetColumnEntity criteriaColumn)
        {
            string frequencyWhereClause = string.Format(CultureInfo.InvariantCulture, "{0} = '{1}'", criteriaColumn.Name, frequencyValue);
            if (!string.IsNullOrWhiteSpace(frequencyWhereClause))
            {
                string timePeriodsWhereClauses = engine.GenerateWhere(dateFrom, dateTo);
                if (!string.IsNullOrWhiteSpace(timePeriodsWhereClauses))
                {
                    var whereClause = string.Format(
                        CultureInfo.InvariantCulture, 
                        "(( {0} ) and ( {1} ))", 
                        frequencyWhereClause, 
                        timePeriodsWhereClauses);
                    return whereClause;
                }
            }

            return string.Empty;
        }
    }
}