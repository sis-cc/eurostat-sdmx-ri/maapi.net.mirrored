// -----------------------------------------------------------------------
// <copyright file="TimeDimension1to1.cs" company="EUROSTAT">
//   Date Created : 2013-04-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Data;
    using System.Globalization;
    using System.Text;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Utils.Helper;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    ///     This Time Dimension mapping class is used for 1-1 mappings between
    ///     a Time Dimension and one dissemination column (not Date type) without Transcoding
    /// </summary>
    public class TimeDimension1To1 : ITimeDimensionMappingBuilder
    {
        /// <summary>
        /// The mapping
        /// </summary>
        private readonly TimeDimensionMappingEntity _mapping;

        /// <summary>
        ///     The format template for checking if two year/period are equal
        /// </summary>
        private readonly string _equalsWhere;

        /// <summary>
        ///     The format template for the 'from' where clause.
        /// </summary>
        private readonly string _fromWhere;

        /// <summary>
        ///     The format template for the 'to' where clause.
        /// </summary>
        private readonly string _toWhere;

        /// <summary>
        /// The column ordinal builder
        /// </summary>
        private readonly ColumnOrdinalBuilder _columnOrdinalBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimeDimension1To1" /> class.
        ///     Initialize an new instance of the TimeDimensionMapping based class
        /// </summary>
        /// <param name="mapping">
        ///     The time dimension mapping
        /// </param>
        /// <param name="databaseType">The database type</param>
        public TimeDimension1To1(TimeDimensionMappingEntity mapping, string databaseType)
        {
            _mapping = mapping;
            _columnOrdinalBuilder = new ColumnOrdinalBuilder(mapping.GetColumns());

            if (mapping == null)
            {
                throw new ArgumentNullException("mapping");
            }

            string fieldName = mapping.SimpleMappingColumn;
            
            string provider = DatabaseType.GetProviderName(databaseType);
            string cast = DatabaseType.DatabaseSettings[provider].CastToString;
            if (!string.IsNullOrWhiteSpace(cast))
            {
                fieldName = string.Format(CultureInfo.InvariantCulture, cast, fieldName);
            }

            this._fromWhere = string.Format(
                CultureInfo.InvariantCulture, 
                " ( {0} >= '{1}' )", 
                fieldName, 
                "{0}");
            this._toWhere = string.Format(
                CultureInfo.InvariantCulture, 
                " ( {0} <= '{1}' )", 
                fieldName, 
                "{0}");
            this._equalsWhere = string.Format(
                CultureInfo.InvariantCulture, 
                " ( {0} = '{1}' )", 
                fieldName, 
                "{0}");
        }

        /// <summary>
        ///     Generates the SQL Query where condition from the SDMX Query TimeBean <see cref="ISdmxDate" />
        /// </summary>
        /// <param name="dateFrom">The start time period</param>
        /// <param name="dateTo">The end time period</param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>
        ///     The string containing SQL Query where condition
        /// </returns>
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, bool addIsNull = false)
        {
            if (dateFrom == null && dateTo == null)
            {
                return string.Empty;
            }

            var ret = new StringBuilder("(");
            bool timeWhereStarted = false;
            bool areEqual = Equals(dateFrom, dateTo);
            if (dateFrom != null)
            {
                var startTime = dateFrom.DateInSdmxFormat.Replace("'", "''");
                if (areEqual)
                {
                    ret.AppendFormat(this._equalsWhere, startTime);
                }
                else
                {
                    ret.AppendFormat(this._fromWhere, startTime);
                    timeWhereStarted = true;
                }
            }

            if (dateTo != null && !areEqual)
            {
                if (timeWhereStarted)
                {
                    ret.Append(" and ");
                }

                var endTime = dateTo.DateInSdmxFormat.Replace("'", "''");
                ret.AppendFormat(this._toWhere, endTime);
            }

            ret.Append(") ");
            return ret.ToString();
        }

        /// <summary>
        ///     Transcodes the time period returned by the local database to SDMX Time period
        /// </summary>
        /// <param name="reader">
        ///     The data reader reading the Dissemination database
        /// </param>
        /// <returns>
        ///     The transcoded time period, as in SDMX Time period type
        /// </returns>
        public string MapComponent(IDataReader reader)
        {
            _columnOrdinalBuilder.BuildOrdinals(reader);
            return DataReaderHelper.GetString(reader, _columnOrdinalBuilder.ColumnOrdinals[0].Value) ?? this._mapping.DefaultValue;
        }
    }
}