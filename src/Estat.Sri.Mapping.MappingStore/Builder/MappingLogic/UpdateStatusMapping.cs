﻿// -----------------------------------------------------------------------
// <copyright file="UpdateStatusMapping.cs" company="EUROSTAT">
//   Date Created : 2016-11-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The update status mapping.
    /// </summary>
    public class UpdateStatusMapping : IUpdateStatusMappingBuilder
    {
        private readonly UpdateStatusEntity _updateStatus;

        /// <summary>
        /// The Dissemination database
        /// </summary>
        private readonly Database _ddbDatabase;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateStatusMapping" /> class.
        /// </summary>
        /// <param name="updateStatus">The update status.</param>
        /// <param name="ddbDatabase">The Dissemination database.</param>
        public UpdateStatusMapping(UpdateStatusEntity updateStatus, Database ddbDatabase)
        {
            _updateStatus = updateStatus;
            this._ddbDatabase = ddbDatabase;
        }

        /// <summary>
        /// The generate where.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns>
        /// The <see cref="MappingStoreRetrieval.Model.SqlClause" />.
        /// </returns>
        /// <exception cref="SdmxNoResultsException">Could not find any data with the requested observation action(s)</exception>
        public SqlClause GenerateWhere(ObservationAction action)
        {
            var entity = _updateStatus;
            if (entity == null || action == null)
            {
                return null;
            }

            if (entity.Constant != null)
            {
                if (action.EnumType == ObservationActionEnumType.Active && entity.Constant.IsActive())
                {
                    return null;
                }

                if (entity.Constant.EnumType == ObservationActionEnumType.Active && action.IsActive())
                {
                    return null;
                }

                if (entity.Constant.Equals(action))
                {
                    return null;
                }

                throw new SdmxNoResultsException("Could not find any data with the requested observation action(s)");
            }

            var parameterNames = new List<string>();
            var parameters = new List<DbParameter>();
            if (action.EnumType == ObservationActionEnumType.Active)
            {
                var addedValue = entity.GetLocalAction(ObservationActionEnumType.Added);
                var updatedValue = entity.GetLocalAction(ObservationActionEnumType.Updated);
                this.BuildParameter(addedValue, parameterNames, parameters, "added_param");
                this.BuildParameter(updatedValue, parameterNames, parameters, "updated_param");
            }
            else
            {
                var value = entity.GetLocalAction(action);
                this.BuildParameter(value, parameterNames, parameters, "action_param");
            }

            return BuildSqlClause(entity, parameterNames, parameters);
        }

        /// <summary>
        /// Builds the SQL clause.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="parameterNames">The parameter names.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// The <see cref="SqlClause" />.
        /// </returns>
        /// <exception cref="SdmxNoResultsException">Could not find any data with the requested observation action(s)</exception>
        private static SqlClause BuildSqlClause(UpdateStatusEntity entity, ICollection<string> parameterNames, IList<DbParameter> parameters)
        {
            string whereClause;
            if (parameterNames.Count > 1)
            {
                whereClause = string.Format(CultureInfo.InvariantCulture, "({0} in ({1}))", entity.Column.Name, string.Join(", ", parameterNames));
            }
            else if (parameterNames.Count == 1)
            {
                whereClause = string.Format(CultureInfo.InvariantCulture, "({0} = {1})", entity.Column.Name, parameterNames.First());
            }
            else
            {
                // we cannot find a suitable mapping for such action so we throw
                throw new SdmxNoResultsException("Could not find any data with the requested observation action(s)");
            }

            return new SqlClause(whereClause, parameters);
        }

        /// <summary>
        /// Builds the parameter and adds it to <paramref name="parameterNameList" /> and <paramref name="parameters" />.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="parameterNameList">The parameter name list.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        private void BuildParameter(string value, ICollection<string> parameterNameList, ICollection<DbParameter> parameters, string parameterName)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                var parameter = this._ddbDatabase.CreateInParameter(parameterName, DbType.String, value);
                parameterNameList.Add(parameter.ParameterName);
                parameters.Add(parameter);
            }
        }
    }
}