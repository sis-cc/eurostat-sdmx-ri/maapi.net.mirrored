// -----------------------------------------------------------------------
// <copyright file="SqlSubstringClauseBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-09-08
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Utils.Helper;

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Data;
    using System.Globalization;

    using Estat.Sri.MappingStoreRetrieval.Helper;

    /// <summary>
    /// This class is responsible for building a substring 
    /// </summary>
    /// <remarks>TODO use <see cref="IDbDataParameter"/></remarks>
    internal class SqlSubstringClauseBuilder
    {
        /// <summary>
        ///     The cast to string
        /// </summary>
        private string _castToString;

        /// <summary>
        ///     This field has the DDB specific SUBSTRING or SUBSTR SQL command
        /// </summary>
        private string _substringCmd;

        /// <summary>
        ///     This field holds whether the substring command requires the LEN parameter.
        /// </summary>
        private bool _substringCmdRequiresLen;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlSubstringClauseBuilder"/> class.
        /// </summary>
        /// <param name="databaseType">Type of the database.</param>
        public SqlSubstringClauseBuilder(string databaseType)
        {
            this.SetDbType(databaseType);
        }

        /// <summary>
        ///     Gets the number format to invariant culture to speed up <c>int.ToString()</c>
        /// </summary>
        protected static IFormatProvider FormatProvider
        {
            get
            {
                return CultureInfo.InvariantCulture;
            }
        }

        /// <summary>
        ///     Creates the sub string clause.
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="start">The start.</param>
        /// <param name="length">The length.</param>
        /// <param name="sqlOperator">The SQL operator.</param>
        /// <returns>The WHERE Clause with substring</returns>
        public string CreateSubStringClause(string columnName, int start, int length, string sqlOperator)
        {
            string yearColumnCast = string.Format(FormatProvider, this._castToString, columnName);
            var normLength = length <= 0 && this._substringCmdRequiresLen ? 100 : length;

            if (normLength > 0)
            {
                return string.Format(
                    FormatProvider,
                    " ( {0}({1},{2},{3}) {4} '{{0}}' )",
                    this._substringCmd,
                    yearColumnCast,
                    start,
                    normLength,
                    sqlOperator);
            }

            return string.Format(
                FormatProvider,
                " ( {0}({1},{2}) {3} '{{0}}' )",
                this._substringCmd,
                yearColumnCast,
                start,
                sqlOperator);
        }

        /// <summary>
        ///     The dissemination database vendor
        /// </summary>
        /// <param name="dataBaseType">
        ///     Database type
        /// </param>
        private void SetDbType(string dataBaseType)
        {
            var providerName = DatabaseType.GetProviderName(dataBaseType);
            var setting = DatabaseType.DatabaseSettings[providerName];
            if (setting != null)
            {
                this._substringCmd = setting.SubstringCommand;
                this._substringCmdRequiresLen = setting.SubstringCommandRequiresLength;
                this._castToString = setting.CastToString;
            }
        }
    }
}