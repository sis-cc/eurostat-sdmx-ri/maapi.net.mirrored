// -----------------------------------------------------------------------
// <copyright file="TimeDimensionStartDurationMappingBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-5-11
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System.Data;

    using Estat.Sri.Mapping.Api.Builder;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;

    class TimeDimensionStartDurationMappingBuilder : ITimeDimensionMappingBuilder
    {
        private readonly ITimeDimensionMappingBuilder _startBuilder;

        private readonly IPeriodicity _outputPeriodicity;

        private readonly IComponentMappingBuilder _durationMappingBuilder;

        public TimeDimensionStartDurationMappingBuilder(ITimeDimensionMappingBuilder startBuilder, IComponentMappingBuilder durationMappingBuilder, TimeFormat outputFormat)
        {
            _startBuilder = startBuilder;
            _durationMappingBuilder = durationMappingBuilder;
            _outputPeriodicity = PeriodicityFactory.Create(outputFormat);
        }

        /// <summary>
        /// Maps the column(s) of the mapping to the component(s) of this IComponentMapping
        /// </summary>
        /// <param name="reader">The DataReader for retrieving the values of the column.</param>
        /// <returns>The value of the component</returns>
        public string MapComponent(IDataReader reader)
        {
            var startPeriod = _startBuilder.MapComponent(reader);

            if (startPeriod == null)
            {
                return null;
            }

            var duration = _durationMappingBuilder.MapComponent(reader);
            if (duration != null)
            {
                var dateWithDuration = DateUtil.NormalizeTimeRange(startPeriod + "/" + duration);
                return _outputPeriodicity.ToString(dateWithDuration);
            }

            var lastCase = DateUtil.FormatDateOffSetDuration(startPeriod);
            return _outputPeriodicity.ToString(lastCase);
        }

        /// <summary>
        /// Generates the SQL Query where condition from the SDMX Query Time
        /// </summary>
        /// <param name="dateFrom">The start time</param>
        /// <param name="dateTo">The end time</param>
        /// <param name="addIsNull">The add isNull boolean</param>
        /// <returns>The string containing SQL Query where condition</returns>
        public string GenerateWhere(ISdmxDate dateFrom, ISdmxDate dateTo, bool addIsNull = false)
        {
            return _startBuilder.GenerateWhere(dateFrom, dateTo);
        }
    }
}