﻿// -----------------------------------------------------------------------
// <copyright file="ColumnOrdinalBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-09-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The column ordinal builder.
    /// </summary>
    internal class ColumnOrdinalBuilder
    {
        /// <summary>
        ///     The column ordinal of the local columns
        /// </summary>
        private readonly List<ColumnOrdinal> _columnOrdinals = new List<ColumnOrdinal>();

        /// <summary>
        /// The order columns
        /// </summary>
        private readonly IList<DataSetColumnEntity> _orderColumns;

        /// <summary>
        ///     The last <see cref="IDataReader" /> that was used. This field is used to cache the ordinal of the column(s).
        /// </summary>
        private IDataReader _prevReader;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnOrdinalBuilder"/> class.
        /// </summary>
        /// <param name="orderColumns">The order columns.</param>
        public ColumnOrdinalBuilder(IList<DataSetColumnEntity> orderColumns)
        {
            this._orderColumns = orderColumns;
        }

        /// <summary>
        ///     Gets the column ordinal of the local columns
        /// </summary>
        public IList<ColumnOrdinal> ColumnOrdinals
        {
            get
            {
                return this._columnOrdinals;
            }
        }

        /// <summary>
        ///     Build the <see cref="ColumnOrdinals" />
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        public void BuildOrdinals(IDataReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (!ReferenceEquals(reader, this._prevReader))
            {
                this._columnOrdinals.Clear();
                this._prevReader = reader;
            }

            if (this._columnOrdinals.Count == 0)
            {
                for (int i = 0; i < this._orderColumns.Count; i++)
                {
                    DataSetColumnEntity column = this._orderColumns[i];
                    this._columnOrdinals.Add(new ColumnOrdinal { Key = column, Value = reader.GetOrdinal(column.Name), ColumnPosition = i });
                }
            }
        }

        /// <summary>
        ///     This class stores the <see cref="DataSetColumnEntity" /> ordinal
        /// </summary>
        internal class ColumnOrdinal
        {
            /// <summary>
            ///     Gets or sets the Column position 
            /// </summary>
            public int ColumnPosition { get; set; }

            /// <summary>
            ///     Gets or sets the <see cref="DataSetColumnEntity" />
            /// </summary>
            public DataSetColumnEntity Key { get; set; }

            /// <summary>
            ///     Gets or sets the ordinal of <see cref="Key" />
            /// </summary>
            public int Value { get; set; }
        }
    }
}