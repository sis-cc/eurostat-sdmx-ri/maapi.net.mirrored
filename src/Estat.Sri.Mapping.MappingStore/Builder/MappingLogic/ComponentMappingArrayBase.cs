// -----------------------------------------------------------------------
// <copyright file="ComponentMappingArrayBase.cs" company="EUROSTAT">
//   Date Created : 2023-01-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Builder.MappingLogic
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using Estat.Sri.Mapping.Api.Constant;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    using static ComponentMappingCommon;

    /// <summary>
    /// Abstract class that implements part of IComponentArrayMapper for more than one mapping types.
    /// </summary>
    public abstract class ComponentMappingArrayBase
    {
        private readonly string _databaseType;
        private readonly IList<string> _mappedIds;
        private readonly bool _quoteMappedIds;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="databaseType"></param>
        /// <param name="mappedIds">
        /// The ddb column names or constant values to be used against the condition values.
        /// For constant mapping, the constant values should be provided.
        /// For mapping with N columns, the column names should be provided.
        /// </param>
        /// <param name="quoteMappedIds">Constant values should be quoted.</param>
        internal ComponentMappingArrayBase(string databaseType, IList<string> mappedIds, bool quoteMappedIds)
        {
            _databaseType = databaseType;
            _mappedIds = mappedIds;
            _quoteMappedIds = quoteMappedIds;
        }

        /// <summary>
        /// Applicable to array components.
        /// Generates the where clause mapping condition N sdmx array values to N condition values.
        /// </summary>
        /// <param name="conditionValue"></param>
        /// <param name="operatorValue"></param>
        /// <returns></returns>
        public string GenerateComponentWhere(string[] conditionValue, OperatorType operatorValue)
        {
            if (!(operatorValue == OperatorType.Exact ||
                operatorValue == (OperatorType.Not | OperatorType.Exact)))
            {
                throw new SdmxNotImplementedException($"Filtering component array values with operator {operatorValue} is not supported.");
            }

            var ret = new StringBuilder();
            ret.Append(" (");
            var mappedClause = new List<string>();

            for (int i = 0; i < _mappedIds.Count; i++)
            {
                string value = conditionValue[i];
                string mappedId = _mappedIds.ElementAt(i);
                string quotedValue = mappedId;
                if (_quoteMappedIds)
                {
                    quotedValue = QuoteString(EscapeString(mappedId));
                }

                mappedClause.Add(SqlOperatorComponent(quotedValue, value, operatorValue, _databaseType));
            }

            ret.Append(string.Join(" AND ", mappedClause.ToArray()));
            ret.Append(" )");
            return ret.ToString();
        }

        /// <summary>
        /// Generates the SQL Where IN clause for the component used in this mapping.
        /// For db servers that support IN clause for multiple columns, use this.
        /// For db servers that do not support it, multiple values are joined with OR clauses.
        /// </summary>
        /// <param name="conditionValues">The order of the values should be the same as the order of the columns they correspond to.</param>
        /// <param name="withNull">not used, defaults to <c>false</c></param>
        /// <returns></returns>
        public string GenerateComponentWhere(ISet<string[]> conditionValues, bool withNull = false)
        {
            if (SupportsMultiColumnInClause(this._databaseType))
            {
                return GenerateComponentWhereInTwoOrMoreColumn(conditionValues);
            }
            else
            {
                List<string> orClauses = new List<string>();
                foreach (var condition in conditionValues)
                {
                    orClauses.Add(GenerateComponentWhere(condition, OperatorType.Exact));
                }

                return string.Format(CultureInfo.InvariantCulture, "( {0} )", string.Join(" OR ", orClauses));
            }
        }

        private string GenerateComponentWhereInTwoOrMoreColumn(IEnumerable<string[]> conditionValues)
        {
            Dictionary<string, IList<string>> mappedValues = new Dictionary<string, IList<string>>(StringComparer.Ordinal);
            string escapedMappedId = "(" + string.Join(", ", _mappedIds.Select(x => EscapeMappedId(x, _databaseType, _quoteMappedIds))) + ")";
            IList<string> mappedValuess = new List<string>();

            foreach (var conditionValue in conditionValues)
            {
                var mappedClause = new List<string>();
                foreach (var value in conditionValue)
                {
                    string quotedValue = QuoteString(EscapeString(value));
                    mappedClause.Add(quotedValue);
                }
                mappedValuess.Add("(" + string.Join(",", mappedClause) + ")");
            }

            return GenerateInClause(escapedMappedId, mappedValuess);
        }
    }
}
