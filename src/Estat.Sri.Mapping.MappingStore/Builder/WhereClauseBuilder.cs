// -----------------------------------------------------------------------
// <copyright file="WhereClauseBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-02-14
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Exceptions;

namespace Estat.Sri.Mapping.MappingStore.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    using Dapper;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    public class WhereClauseBuilder
    {
        private readonly Database _mappingStoreDatabase;

        private readonly ISet<string> _usedParameters = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        private int _parameterIndex = 0;

        public WhereClauseBuilder(Database mappingStoreDatabase)
        {
            if (mappingStoreDatabase == null)
            {
                throw new ArgumentNullException(nameof(mappingStoreDatabase));
            }

            _mappingStoreDatabase = mappingStoreDatabase;
        }

        public WhereClauseParameters Build(ICriteria<long> criteria, string columnId, string tableAlias = null)
        {
             if (criteria == null)
            {
                return null;
            }

            var operatorType = criteria.Operator;
            var args = criteria.Value;
            if (operatorType == OperatorType.AnyValue)
            {
                return null;
            }

            var parameters = new List<KeyValuePair<string, object>>();
            var whereClause = BuildWhereSqlClause(columnId, operatorType, (long?)args, parameters, tableAlias);
            if (whereClause == null)
            {
                return null;
            }

            return new WhereClauseParameters(whereClause, parameters);
        }
        public WhereClauseParameters Build(ICriteria<long?> criteria, string columnId, string tableAlias = null)
        {
            if (criteria == null)
            {
                return null;
            }

            var operatorType = criteria.Operator;
            var args = criteria.Value;
            if (operatorType == OperatorType.AnyValue)
            {
                return null;
            }

            var parameters = new List<KeyValuePair<string, object>>();
            var whereClause = BuildWhereSqlClause(columnId, operatorType, args, parameters, tableAlias);
            if (whereClause == null)
            {
                return null;
            }

            return new WhereClauseParameters(whereClause, parameters);
        }

        public WhereClauseParameters Build(ICriteria<string> criteria, string columnId, string tableAlias = null)
        {
            if (criteria == null)
            {
                return null;
            }

            var operatorType = criteria.Operator;
            var args = criteria.Value;
            if (operatorType == OperatorType.AnyValue)
            {
                return null;
            }

            var parameters = new List<KeyValuePair<string, object>>();
            var whereClause = BuildWhereSqlClause(columnId, operatorType, args, parameters, tableAlias);
            if (whereClause == null)
            {
                return null;
            }

            return new WhereClauseParameters(whereClause, parameters);
        }
        public WhereClauseParameters BuildFromUrn(ICriteria<string> criteria, string idColumn, string agencyColumn, string[] versionColumns, string idAndAgencyTableAlias,string versionTable)
        {
            if (criteria == null)
            {
                return null;
            }

            if (criteria.Operator == OperatorType.AnyValue)
            {
                return null;
            }

            var structureReference = new StructureReferenceImpl(criteria.Value);

            var clauses = new List<string>();
            var parameters = new List<KeyValuePair<string, object>>();

            clauses.Add(BuildWhereSqlClause(idColumn, criteria.Operator, structureReference.MaintainableId, parameters, idAndAgencyTableAlias));

            clauses.Add(BuildWhereSqlClause(agencyColumn, criteria.Operator, structureReference.AgencyId, parameters, idAndAgencyTableAlias));

            IVersionRequest versionRequest = new VersionRequestCore(structureReference.Version);

            clauses.Add(
                      BuildWhereSqlClause(
                            versionColumns[0],
                            criteria.Operator,
                           (long?)versionRequest.Major,
                            parameters, versionTable));
            clauses.Add(
                       BuildWhereSqlClause(
                            versionColumns[1],
                            criteria.Operator,
                           (long?)versionRequest.Minor,
                            parameters, versionTable));
            if (versionRequest.Patch.HasValue)
            {
                clauses.Add(
                     BuildWhereSqlClause(
                            versionColumns[2],
                            criteria.Operator,
                           (long?)versionRequest.Patch,
                            parameters, versionTable));

            }
             else if (criteria.Operator.HasFlag(OperatorType.Exact))
                {
                    clauses.Add(
                         BuildWhereSqlClause(
                            versionColumns[2],
                            criteria.Operator,
                            (long?)-1,
                            parameters, versionTable));
                }

            var whereClause = string.Join(criteria.Operator.HasFlag(OperatorType.Not) ? " OR " : " AND ", clauses.Where(s => !string.IsNullOrWhiteSpace(s)));
            return new WhereClauseParameters(whereClause, parameters);
        }
        public WhereClauseParameters BuildFromUrn(ICriteria<string> criteria, string idColumn, string agencyColumn, string[] versionColumns,string tableAlias)
        {
            return BuildFromUrn(criteria, idColumn, agencyColumn, versionColumns, tableAlias, tableAlias);
        }
       
        /// <summary>
        /// Builds the where SQL clause.
        /// </summary>
        /// <param name="columnId">The column identifier.</param>
        /// <param name="operatorType">Type of the operator.</param>
        /// <param name="args">The arguments.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="tableAlias"></param>
        /// <returns>WHERE SQL Clause</returns>
        private string BuildWhereSqlClause(string columnId, OperatorType operatorType, long? args, ICollection<KeyValuePair<string, object>> parameters, string tableAlias)
        {
            if (!args.HasValue)
            {
                return string.Format(CultureInfo.InvariantCulture, "({0} is null)", columnId);
            }
            StringBuilder sb = new StringBuilder();

            sb.Append("(");
            string clauseFormat;
            if (operatorType.HasFlag(OperatorType.Contains))
            {
                throw new ResourceConflictException("Contains not supported for numbers");
            }
            else if (operatorType.HasFlag(OperatorType.Exact))
            {
                clauseFormat = operatorType.HasFlag(OperatorType.Not) ? " {0} != {1} " : " {0} = {1} ";
            }
            else if(operatorType.HasFlag(OperatorType.LessThanOrEqual))
            {
                clauseFormat =" {0} <= {1} ";
            }
            else if (operatorType.HasFlag(OperatorType.GreaterThanOrEqual))
            {
                clauseFormat = " {0} => {1} ";
            }
            else
            {
                return null;
            }

            var parameterName = BuildParameterName(columnId);
            var inParameter = new KeyValuePair<string, object>(parameterName, args);
            var columnString = string.IsNullOrWhiteSpace(tableAlias) ? columnId : tableAlias + "." + columnId;
            sb.AppendFormat(clauseFormat, columnString, parameterName);
            parameters.Add(inParameter);

            sb.Append(")");

            var whereClause = sb.ToString();
            return whereClause;
        }

        /// <summary>
        /// Builds the where SQL clause.
        /// </summary>
        /// <param name="columnId">The column identifier.</param>
        /// <param name="operatorType">Type of the operator.</param>
        /// <param name="args">The arguments.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="tableAlias"></param>
        /// <returns>WHERE SQL Clause</returns>
        private string BuildWhereSqlClause<T>(string columnId, OperatorType operatorType, T args, ICollection<KeyValuePair<string, object>> parameters, string tableAlias)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("(");
            string valueFormat;
            string clauseFormat;
            if (operatorType.HasFlag(OperatorType.Contains))
            {
                valueFormat = "%{0}%";
                clauseFormat = operatorType.HasFlag(OperatorType.Not) ? " {0} NOT LIKE {1} " : " {0} LIKE {1} ";
            }
            else if (operatorType.HasFlag(OperatorType.Exact))
            {
                valueFormat = "{0}";
                clauseFormat = operatorType.HasFlag(OperatorType.Not) ? " {0} != {1} " : " {0} = {1} ";
            }
            else if(operatorType.HasFlag(OperatorType.LessThanOrEqual))
            {
                valueFormat = "{0}";
                clauseFormat =" {0} <= {1} ";
            }
            else if (operatorType.HasFlag(OperatorType.GreaterThanOrEqual))
            {
                valueFormat = "{0}";
                clauseFormat = " {0} => {1} ";
            }
            else
            {
                return null;
            }

            var parameterName = BuildParameterName(columnId);
            var value = new DbString {Value = string.Format(CultureInfo.InvariantCulture, valueFormat, args) ,IsAnsi = true};
            var inParameter = new KeyValuePair<string, object>(parameterName, value);
            var columnString = string.IsNullOrWhiteSpace(tableAlias) ? columnId : tableAlias + "." + columnId;
            sb.AppendFormat(clauseFormat, columnString, parameterName);
            parameters.Add(inParameter);

            sb.Append(")");

            var whereClause = sb.ToString();
            return whereClause;
        }

        /// <summary>
        /// Builds the where SQL clause for NULL check
        /// </summary>
        /// <param name="columnId">The column identifier.</param>
        /// <param name="operatorType">Type of the operator.</param>
        /// <param name="tableAlias"></param>
        /// <returns>WHERE SQL Clause</returns>
        private string BuildNullWhereSqlClause(string columnId, OperatorType operatorType, string tableAlias)
        {
            if (!operatorType.HasFlag(OperatorType.Exact))
            {
                return null;
            }

            var clauseFormat = operatorType.HasFlag(OperatorType.Not) ? " {0} IS NOT NULL " : " {0} IS NULL ";
            var columnString = string.IsNullOrWhiteSpace(tableAlias) ? columnId : tableAlias + "." + columnId;

            StringBuilder sb = new StringBuilder();
            
            sb.Append("(");
            sb.AppendFormat(clauseFormat, columnString);
            sb.Append(")");

            var whereClause = sb.ToString();

            return whereClause;
        }

        private string BuildParameterName(string columnId)
        {
            string parameterName;
            do
            {
                parameterName = string.Format(CultureInfo.InvariantCulture, "{0}{1}", columnId, this._parameterIndex++);
                if (parameterName.Length > 30)
                {
                    parameterName = string.Format(CultureInfo.InvariantCulture, "p{0}", this._parameterIndex);
                    columnId = "p";
                }
            }
            while (!this._usedParameters.Add(parameterName));

            return this._mappingStoreDatabase.BuildParameterName(parameterName);
        }
    }
}