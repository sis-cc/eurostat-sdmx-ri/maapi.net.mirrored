﻿// -----------------------------------------------------------------------
// <copyright file="DisseminationDatabaseBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-11-11
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder
{
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Builder;

    /// <summary>
    /// Build a <see cref="Database"/> from a <see cref="Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel.ConnectionEntity"/>
    /// </summary>
    public class DisseminationDatabaseBuilder : IBuilder<Database, DdbConnectionEntity>
    {
        /// <summary>
        /// The database provider manager
        /// </summary>
        private readonly IDatabaseProviderManager _databaseProviderManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="DisseminationDatabaseBuilder"/> class.
        /// </summary>
        /// <param name="databaseProviderManager">The database provider manager.</param>
        public DisseminationDatabaseBuilder(IDatabaseProviderManager databaseProviderManager)
        {
            _databaseProviderManager = databaseProviderManager;
        }

        /// <summary>
        /// The method that builds a <see cref="Database"/> from a <see cref="Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel.ConnectionEntity"/>
        /// </summary>
        /// <param name="connectionEntity">The input <see cref="Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel.ConnectionEntity"/> .</param>
        /// <returns>
        /// The output <see cref="Database"/>
        /// </returns>
        public Database Build(DdbConnectionEntity connectionEntity)
        {
            var connectionStringSettings = _databaseProviderManager.GetConnectionStringSettings(connectionEntity);
            return new Database(connectionStringSettings);
        }
    }
}