// -----------------------------------------------------------------------
// <copyright file="DDbConnectionBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder
{
    using System;
    using System.Data.Common;

    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Utils.Config;

    using Org.Sdmxsource.Sdmx.Api.Builder;

    /// <summary>
    ///     Dissemination database connection builder
    /// </summary>
    public class DDbConnectionBuilder : IBuilder<DbConnection, DdbConnectionEntity>
    {
        /// <summary>
        /// The dissemination database builder
        /// </summary>
        private readonly IBuilder<Database, DdbConnectionEntity> _databaseBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="DDbConnectionBuilder"/> class.
        /// </summary>
        /// <param name="databaseBuilder">The database builder.</param>
        public DDbConnectionBuilder(IBuilder<Database, DdbConnectionEntity> databaseBuilder)
        {
            _databaseBuilder = databaseBuilder;
        }

        /// <summary>
        ///     The method that builds a <see cref="DbConnection" /> from the specified <paramref name="connectionEntity" />
        /// </summary>
        /// <param name="connectionEntity">
        ///     The current Data retrieval state
        /// </param>
        /// <returns>
        ///     The <see cref="DbConnection" />
        /// </returns>
        public DbConnection Build(DdbConnectionEntity connectionEntity)
        {
            if (connectionEntity == null)
            {
                throw new ArgumentNullException("connectionEntity");
            }

            if (ConfigurationProvider.DisseminationDbConfig != null &&
                !string.IsNullOrEmpty(ConfigurationProvider.DisseminationDbConfig.Item1) && 
                !string.IsNullOrEmpty(ConfigurationProvider.DisseminationDbConfig.Item2))
            {
                connectionEntity.DbType = ConfigurationProvider.DisseminationDbConfig.Item1;
                connectionEntity.AdoConnString = ConfigurationProvider.DisseminationDbConfig.Item2;
            }

            var disseminationDb = this._databaseBuilder.Build(connectionEntity);
            return disseminationDb.CreateConnection();
        }
    }
}