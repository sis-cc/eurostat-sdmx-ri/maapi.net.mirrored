// -----------------------------------------------------------------------
// <copyright file="ExportImportPathBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-08-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder
{
    using System;
    using System.IO.Compression;

    using Estat.Sri.Mapping.MappingStore.Model;

    internal class ExportImportPathBuilder
    {
        private const string MappingMappingXML = "mapping/mapping.json";

        private const string SdmxV2StructureXML = "sdmx/v2.0/structure.xml";

        private const string SdmxV21StructureXML = "sdmx/v2.1/structure.xml";

        private const string SdmxV30StructureXML = "sdmx/v3.0/structure.xml";

        public ExportImportEntryPaths Build(ZipArchive archive)
        {
            if (archive == null)
            {
                throw new ArgumentNullException(nameof(archive));
            }

            ExportImportEntryPaths exportPath = new ExportImportEntryPaths();
            switch (archive.Mode)
            {
                case ZipArchiveMode.Create:
                    exportPath.Mapping = new Lazy<ZipArchiveEntry>(() => archive.CreateEntry(MappingMappingXML));
                    exportPath.SdmxV20 = new Lazy<ZipArchiveEntry>(() => archive.CreateEntry(SdmxV2StructureXML));
                    exportPath.SdmxV21 = new Lazy<ZipArchiveEntry>(() => archive.CreateEntry(SdmxV21StructureXML));
                    exportPath.SdmxV30 = new Lazy<ZipArchiveEntry>(() => archive.CreateEntry(SdmxV30StructureXML));

                    break;
                case ZipArchiveMode.Read:
                    exportPath.Mapping = new Lazy<ZipArchiveEntry>(() => archive.GetEntry(MappingMappingXML));
                    exportPath.SdmxV20 = new Lazy<ZipArchiveEntry>(() => archive.GetEntry(SdmxV2StructureXML));
                    exportPath.SdmxV21 = new Lazy<ZipArchiveEntry>(() => archive.GetEntry(SdmxV21StructureXML));
                    exportPath.SdmxV30 = new Lazy<ZipArchiveEntry>(() => archive.GetEntry(SdmxV30StructureXML));
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(archive));
            }

            return exportPath;
        }
    }
}