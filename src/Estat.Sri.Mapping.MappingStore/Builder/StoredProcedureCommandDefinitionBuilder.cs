﻿// -----------------------------------------------------------------------
// <copyright file="StoredProcedureCommandDefinitionBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-02-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Dapper;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Builder
{
    using Estat.Sri.MappingStoreRetrieval.Manager;

    public class StoredProcedureCommandDefinitionBuilder : ICommandDefinitionBuilder
    {
        public IEnumerable<CommandDefinition> GetCommands(UpdateInfoAction updateInfoAction, string entityId, Database mappingStoreDatabase)
        {
            var databaseMapperManager = new DatabaseMapperManager(updateInfoAction.DataInformationType);
            var storeProcedureBuilder = new StoredProcedureBuilder(databaseMapperManager, new CommandBuilder(mappingStoreDatabase, databaseMapperManager));
            return new List<CommandDefinition>
            {
                storeProcedureBuilder.Get(updateInfoAction)
            };
        }
    }
}