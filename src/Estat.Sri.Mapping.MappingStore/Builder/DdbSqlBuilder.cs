﻿// -----------------------------------------------------------------------
// <copyright file="DdbSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2014-09-11
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Builder
{
    using System.Globalization;

    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;

    /// <summary>
    ///     Builds the following SQL <c>select distinct {0} from {1} virtualDataset</c>.
    /// </summary>
    internal class DdbSqlBuilder : IDdbSqlBuilder
    {
        /// <summary>
        ///     Builds SQL from the specified <paramref name="buildFrom" />.
        /// </summary>
        /// <param name="buildFrom">
        ///     The <see cref="IDatasetColumnInfo" />.
        /// </param>
        /// <returns>
        ///     The SQL from the specified <paramref name="buildFrom" />.
        /// </returns>
        public string Build(IDatasetColumnInfo buildFrom)
        {
            var subQuery = DbHelper.NormaliseFromValue(buildFrom.TableOrQuery);
            return string.Format(CultureInfo.InvariantCulture, "select distinct {0} from {1}", buildFrom.ColumnNames, subQuery);
        }

        /// <summary>
        /// Builds the count query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>The SQL Query for getting number of rows.</returns>
        public string BuildCountQuery(string query)
        {
            var subQuery = DbHelper.NormaliseFromValue(query);
            return string.Format(CultureInfo.InvariantCulture, "select count(*) from {0}", subQuery);
        }
    }
}