﻿// -----------------------------------------------------------------------
// <copyright file="StoredProcedureBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-02-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Builder
{
    public class StoredProcedureBuilder : IStoredProcedureBuilder
    {
        private readonly CommandBuilder _commandBuilder;
        private readonly IDatabaseMapperManager _databaseMapperManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="StoredProcedureBuilder"/> class.
        /// </summary>
        /// <param name="databaseMapperManager">The database mapper manager.</param>
        /// <param name="commandBuilder">The command builder.</param>
        public StoredProcedureBuilder(IDatabaseMapperManager databaseMapperManager, CommandBuilder commandBuilder)
        {
            this._databaseMapperManager = databaseMapperManager;
            this._commandBuilder = commandBuilder;
        }

        /// <summary>
        /// Gets the specified update information action.
        /// </summary>
        /// <param name="updateInfoAction">The update information action.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public CommandDefinition Get(UpdateInfoAction updateInfoAction)
        {
            switch (updateInfoAction.SqlStatementType)
            {
                case SqlStatementType.Insert:
                    return this.CreateProcedure("INSERT", updateInfoAction.DataInformationType, updateInfoAction.PathAndValues);
                default:
                    throw new NotImplementedException($"SqlStatementType {updateInfoAction.SqlStatementType} not implemented");
            }
        }

        /// <summary>
        /// Creates the procedure.
        /// </summary>
        /// <param name="procedureNamePrefix">The procedure name prefix.</param>
        /// <param name="databaseInformationType">Type of the database information.</param>
        /// <param name="columnsAndValues">The columns and values.</param>
        /// <returns></returns>
        private CommandDefinition CreateProcedure(string procedureNamePrefix, DatabaseInformationType databaseInformationType, Dictionary<string, object> columnsAndValues)
        {
            var tableName = this._databaseMapperManager.GetTableName();
            var procedureName = procedureNamePrefix + "_" + tableName;
            var commandParameters = this._commandBuilder.CreateProcedureParameters(columnsAndValues);
            commandParameters.Add("p_pk", null, DbType.Int64, ParameterDirection.Output);
            return new CommandDefinition(procedureName, commandParameters, null, null, CommandType.StoredProcedure);
        }
    }
}