// -----------------------------------------------------------------------
// <copyright file="EntitySdmxReferencePersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2023-3-6
//   Copyright (c) 2009, 2023 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Resources;
using Dapper;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Utils;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

namespace Estat.Sri.Mapping.MappingStore
{
    public class EntitySdmxReferencePersistenceEngine
    {
        private readonly Database _database;

        public EntitySdmxReferencePersistenceEngine(Database database)
        {
            this._database = database;
        }

        public EntitySdmxReferencePersistenceEngine(DatabaseManager databaseManager, string mappingStoreIdentifier)
        {
            this._database = databaseManager.GetDatabase(mappingStoreIdentifier);
        }

        public void Insert(IStructureReference structureReference, long sourceEntityId, ContextWithTransaction context)
        {
            this.Insert(new Dictionary<IStructureReference, string> { { structureReference, string.Empty } }, sourceEntityId, context);
        }

        public void Replace(Dictionary<IStructureReference, string> map, long sourceEntityId, ContextWithTransaction context)
        {
            this.Delete(sourceEntityId, context);
            this.Insert(map, sourceEntityId, context);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="map">The permissions/structure reference. The value can be ignored</param>
        /// <param name="sourceEntityId">The source entity</param>
        /// <param name="context"></param>
        public void Insert(Dictionary<IStructureReference, string> map, long sourceEntityId, ContextWithTransaction context)
        {
            StructureCache structureCache = new StructureCache();
            foreach (var structureReference in map.Keys)
            {
                ArtefactFinalStatus artefactFinalStatus = structureCache.GetArtefactFinalStatus(context.DatabaseUnderContext,  structureReference);
                if (artefactFinalStatus == null || artefactFinalStatus.IsEmpty)
                {
                    throw new SdmxNoResultsException($"Cannot find SDMX artefact {structureReference}");
                }
                var parameters = new DynamicParameters();

                parameters.Add("p_source_entity_id", sourceEntityId, DbType.Int64);
                parameters.Add("p_target_artefact", artefactFinalStatus.ArtefactBaseKey, DbType.Int64);
                var versionRequest = new VersionRequestCore(structureReference.Version);
                parameters.Add("p_major", versionRequest.Major ?? -1, DbType.Int64);
                parameters.Add("p_minor", versionRequest.Minor ?? -1, DbType.Int64);
                parameters.Add("p_patch", versionRequest.Patch ?? -1, DbType.Int64);
                // the version extension is not included in the Urn from which the structure reference is created.
                // But for the entity reference we need to record the exact unstable version (if so) it references.
                if (string.IsNullOrEmpty(artefactFinalStatus.VersionExtension))
                {
                    parameters.Add("p_extension", DBNull.Value, dbType: DbType.AnsiString);
                }
                else
                {
                    parameters.Add("p_extension", artefactFinalStatus.VersionExtension, DbType.AnsiString);
                }
               
                parameters.Add("p_wildcard_pos", GetWildcardPosition(versionRequest.WildCard), DbType.Int64);
                // get child id 
                string targetChildId = null;
                if (structureReference.ChildReference != null)
                {
                    targetChildId = structureReference.FullId;
                }

                parameters.Add("p_target_child_full_id", targetChildId, DbType.AnsiString);
                parameters.Add("p_pk", dbType: DbType.Int64, direction: ParameterDirection.Output);
                context.Connection.Execute("INSERT_ENTITY_SDMX_REF", parameters,
                    commandType: CommandType.StoredProcedure);

                var sysId = parameters.Get<long>("p_pk");
            }
        }

        private object GetWildcardPosition(IVersionWildcard wildCard)
        {
            if (wildCard == null)
            {
                return 0;
            }

            switch(wildCard.WildcardPosition)
            {
                case VersionPosition.Major:
                    return 1;
                case VersionPosition.Minor:
                    return 2;
                case VersionPosition.Patch:
                    return 3;
                default:
                    throw new ArgumentOutOfRangeException(nameof(wildCard.WildcardPosition), "Unexpected value:" + wildCard.WildcardPosition);
            }
        }

        public int Delete(string entityId)
        {
            return Delete(entityId, _database);
        }
        public int Delete(string entityId, ContextWithTransaction context)
        {
            var database = context.DatabaseUnderContext;
            return Delete(entityId, database);
        }
        public int Delete(long entityId, ContextWithTransaction context)
        {
            var database = context.DatabaseUnderContext;
            return Delete(entityId, database);
        }

        private static int Delete<T>(T entityId, Database database)
        {
            return database.UsingLogger().ExecuteNonQueryFormat("DELETE FROM ENTITY_SDMX_REF WHERE SOURCE_ENTITY_ID = {0}", database.CreateInParameter("entityIdParam", DbType.Int64, entityId));
        }
    }  
}
