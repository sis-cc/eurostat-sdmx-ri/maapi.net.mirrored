// -----------------------------------------------------------------------
// <copyright file="EntityAuthFactory.cs" company="EUROSTAT">
//   Date Created : 2017-06-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Factory.Auth
{
    using System;

    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Engine.Authorization;

    /// <summary>
    /// The entity auth factory.
    /// </summary>
    public class EntityAuthFactory : IEntityAuthorizationFactory
    {
        /// <summary>
        /// The retriever manager
        /// </summary>
        private readonly Lazy<IEntityRetrieverManager> _retrieverManager;

        /// <summary>
        /// The principal manager
        /// </summary>
        private readonly IDataflowPrincipalManager _principalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityAuthFactory"/> class.
        /// </summary>
        /// <param name="retrieverManager">The retriever manager.</param>
        /// <param name="principalManager">The principal manager.</param>
        public EntityAuthFactory(Lazy<IEntityRetrieverManager> retrieverManager, IDataflowPrincipalManager principalManager)
        {
            _retrieverManager = retrieverManager;
            _principalManager = principalManager;
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="IEntityAuthorizationEngine"/>.
        /// </returns>
        public IEntityAuthorizationEngine GetEngine(EntityType type)
        {
            var engine = GetEngineInternal(type);
            if (engine == null)
            {
                return null;
            }

            return new CacheEntityAuthorizationEngine(engine, type);
        }

        private IEntityAuthorizationEngine GetEngineInternal(EntityType type)
        {
            var entityRetrieverManager = _retrieverManager.Value;
            switch (type)
            {
                case EntityType.DdbConnectionSettings:
                    return new DdbConnectionSettingsAuthCheckEngine(_principalManager, entityRetrieverManager);
                case EntityType.DataSet:
                    return new DataSetAuthCheckEngine(_principalManager, entityRetrieverManager, this);
                case EntityType.DataSetColumn:
                    return new ChildEntityAuthCheckEngine<DataSetColumnEntity>(entityRetrieverManager, this, EntityType.DataSet);
                case EntityType.LocalCode:
                    return new ChildEntityAuthCheckEngine<LocalCodeEntity>(entityRetrieverManager, this, EntityType.DataSetColumn);
                case EntityType.MappingSet:
                    return new MappingSetAuthCheckEngine(_principalManager, this, entityRetrieverManager);
                case EntityType.Mapping:
                    return new ChildEntityAuthCheckEngine<ComponentMappingEntity>(entityRetrieverManager, this, EntityType.MappingSet);
                case EntityType.TimeMapping:
                    return new ChildEntityAuthCheckEngine<TimeDimensionMappingEntity>(entityRetrieverManager, this, EntityType.MappingSet);
                case EntityType.LastUpdated:
                    return new ChildEntityAuthCheckEngine<LastUpdatedEntity>(entityRetrieverManager, this, EntityType.MappingSet);
                case EntityType.UpdateStatusMapping:
                    return new ChildEntityAuthCheckEngine<UpdateStatusEntity>(entityRetrieverManager, this, EntityType.MappingSet);
                case EntityType.ValidToMapping:
                    return new ChildEntityAuthCheckEngine<ValidToMappingEntity>(entityRetrieverManager, this, EntityType.MappingSet);
                case EntityType.Transcoding:
                    throw new NotImplementedException("Transcoding is obsolete");
                case EntityType.TimeTranscoding:
                    return new ChildEntityAuthCheckEngine<TimeTranscodingEntity>(entityRetrieverManager, this, EntityType.TimeMapping);
                case EntityType.TranscodingRule:
                    return new ChildEntityAuthCheckEngine<TranscodingRuleEntity>(entityRetrieverManager, this, EntityType.Mapping);
                case EntityType.TranscodingScript:
                    return new ChildEntityAuthCheckEngine<TranscodingScriptEntity>(entityRetrieverManager, this, EntityType.Mapping);
                case EntityType.DescSource:
                    return new ChildEntityAuthCheckEngine<ColumnDescriptionSourceEntity>(entityRetrieverManager, this, EntityType.DataSetColumn);
                case EntityType.Header:
                    return new HeaderTemplateAuthCheckEngine(_principalManager, entityRetrieverManager);
                case EntityType.Registry:
                    return new RegistryAuthCheckEngine(_principalManager);
                case EntityType.Nsiws:
                    return new NsiwsAuthCheckEngine(_principalManager);
                case EntityType.User:
                    return new UserAuthCheckEngine(_principalManager, entityRetrieverManager);
                case EntityType.UserAction:
                    return new UserActionAuthCheckEngine(_principalManager, entityRetrieverManager);
                case EntityType.TemplateMapping:
                    return new TemplateMappingCheckEngine(_principalManager, entityRetrieverManager);
                case EntityType.DataSource:
                    return new DataSourceCheckEngine(_principalManager, entityRetrieverManager);
            }

            return null;
        }
    }
}