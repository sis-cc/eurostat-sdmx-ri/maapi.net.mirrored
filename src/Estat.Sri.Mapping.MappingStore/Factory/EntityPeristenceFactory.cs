// -----------------------------------------------------------------------
// <copyright file="EntityPeristenceFactory.cs" company="EUROSTAT">
//   Date Created : 2017-02-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Manager;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;

    public class EntityPeristenceFactory : IEntityPeristenceFactory
    {
        private readonly DatabaseManager _databaseManager;

        private readonly IDatabaseProviderManager _databaseProviderManager;

        private readonly IEntityRetrieverManager _entityRetrieverManager;

        private readonly CommandsFromUpdateInfo _commmandsFromUpdateInfo = new CommandsFromUpdateInfo();

        private readonly MultipleEntitiesUserAction _multipleEntitiesUserAction;

        private readonly UserActionByType _userActionByType;

        public EntityPeristenceFactory(DatabaseManager databaseManager,
            IDatabaseProviderManager databaseProviderManager, IEntityRetrieverManager entityRetrieverManager = null)
        {
            this._databaseManager = databaseManager;
            this._databaseProviderManager = databaseProviderManager;
            this._entityRetrieverManager = entityRetrieverManager;
            _multipleEntitiesUserAction = new MultipleEntitiesUserAction(this._entityRetrieverManager);
            _userActionByType = new UserActionByType();
        }

        public IEntityPersistanceStreamEngine GetEngine(string mappingStoreIdentifier, EntityType entityType)
        {
            var entityPersistenceEngineInternal =
                GetEntityPersistenceEngineInternal1(mappingStoreIdentifier, entityType);
            if (entityPersistenceEngineInternal == null)
            {
                return null;
            }

            // Avoid a Null Reference exception
            if (_entityRetrieverManager == null)
            {
                return entityPersistenceEngineInternal;
            }

            var userActionPathAndValues =
                new UserActionPathAndValues(this._entityRetrieverManager, mappingStoreIdentifier);
            UserActionRecorder userActionRecorder = new UserActionRecorder(_userActionByType, userActionPathAndValues,
                _databaseManager, _commmandsFromUpdateInfo, mappingStoreIdentifier);
            var userActionEngine = new UserActionEngine<IEntity>(entityPersistenceEngineInternal, userActionRecorder,
                mappingStoreIdentifier, _multipleEntitiesUserAction, this._entityRetrieverManager);


            return userActionEngine;
        }


        private IEntityPersistanceStreamEngine GetEntityPersistenceEngineInternal1(string mappingStoreIdentifier,
            EntityType entityType)
        {
            // TODO FIXME Split this into separate factories
            var databse = this._databaseManager.GetDatabase(mappingStoreIdentifier);
            if (databse == null)
            {
                return null;
            }

            switch (entityType)
            {
                //if (typeof(TEntity) == typeof(TranscodingRuleEntity))
                case EntityType.TranscodingRule:
                    return
                        new TranscodingRulePersistFactory(_databaseManager).GetEngine<TranscodingRuleEntity>(
                            mappingStoreIdentifier);
                //if (typeof(TEntity) == typeof(TranscodingEntity))
                case EntityType.Transcoding:
                    return
                        new TranscodingPersistenceEngine(this._databaseManager, mappingStoreIdentifier);
                //if (typeof(TEntity) == typeof(HeaderEntity))
                case EntityType.Header:
                    return
                    new SdmxHeaderTemplatePersistEngine(databse, this._databaseManager, mappingStoreIdentifier);
                //if (entityType == EntityType.Header)//if (typeof(TEntity) == typeof(TranscodingEntity))
                //{
                //    return new HeaderTemplatePersistenceEngine<IEntity>(
                //        this._databaseManager,
                //        mappingStoreIdentifier,
                //        new CommandsFromUpdateInfo(),
                //        new EntityIdFromUrn(databse));
                //}
                //if (typeof(TEntity) == typeof(RegistryEntity))
                case EntityType.Registry:
                    // TODO why this is generic
                    return new RegistryPersistenceEngine<IEntity>(
                        this._databaseManager,
                        mappingStoreIdentifier,
                        _commmandsFromUpdateInfo,
                        new UpdateInfoFactoryManager());
                case EntityType.TimeMapping:
                    return new TimeMappingPersistEngine(databse);
                case EntityType.LastUpdated:
                    return new LastUpdateStatusPersistEngine(databse);
                case EntityType.ValidToMapping:
                    return new ValidToMappingPersistEngine(databse);
                case EntityType.UpdateStatusMapping:
                    return new UpdateStatusPersistEngine(databse);
                //if (typeof(TEntity) == typeof(RegistryEntity))
                case EntityType.Nsiws:
                    // TODO why this is generic
                    return new NsiwsPersistenceEngine<IEntity>(
                        this._databaseManager,
                        mappingStoreIdentifier,
                        _commmandsFromUpdateInfo,
                        new UpdateInfoFactoryManager());
            }


            var entityIdFromUrn = new EntityIdFromUrn(databse);

            if (entityType == EntityType.DescSource) //if (typeof(TEntity) == typeof(ColumnDescriptionSourceEntity))
            {
                return new DescSourcePersistenceEngine(
                    this._databaseManager,
                    new UpdateInfoFactoryManager(),
                    mappingStoreIdentifier,
                    new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo);
            }

            if (entityType == EntityType.MappingSet) //if (typeof(TEntity) == typeof(MappingSetEntity))
            {
                return
                    new MappingSetPersistenceEngine(
                        this._databaseManager,
                        new UpdateInfoFactoryManager(),
                        mappingStoreIdentifier,
                        new EntityPropertiesExtractor(entityIdFromUrn),
                        _commmandsFromUpdateInfo,
                        entityIdFromUrn);
            }

            if (entityType == EntityType.DataSet) //if (typeof(TEntity) == typeof(DatasetEntity))
            {
                // TODO why this is generic
                return new DatasetPersistenceEngine<IEntity>(
                    this._databaseManager,
                    new UpdateInfoFactoryManager(),
                    mappingStoreIdentifier,
                    new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo);
            }

            if (entityType == EntityType.LocalCode) //if (typeof(TEntity) == typeof(LocalCodeEntity))
            {
                return new LocalCodePersistenceEngine(
                    this._databaseManager,
                    new UpdateInfoFactoryManager(),
                    mappingStoreIdentifier,
                    new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo);
            }

            if (entityType == EntityType.User) //if (typeof(TEntity) == typeof(UserEntity))
            {
                // TODO why this is generic
                return new UserEntityPersistenceEngine(
                    this._databaseManager,
                    new UpdateInfoFactoryManager(),
                    mappingStoreIdentifier,
                    new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo, entityIdFromUrn);
            }

            if (entityType == EntityType.TemplateMapping) //if (typeof(TEntity) == typeof(TemplateMapping))
            {
                return new TemplateMappingPersistenceEngine(this._databaseManager, new UpdateInfoFactoryManager(),
                    mappingStoreIdentifier, new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo);
            }

            if (entityType == EntityType.Mapping) //if (typeof(TEntity) == typeof(ComponentMappingEntity))
            {
                return new ComponentMappingPersistenceEngine(
                    this._databaseManager,
                    new UpdateInfoFactoryManager(),
                    mappingStoreIdentifier,
                    new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo);
            }

            if (entityType == EntityType.DdbConnectionSettings)
            {
                return
                    new ConnectionEntityPersistenceEngine(
                        this._databaseProviderManager,
                        new EntityPersistenceEngine<DdbConnectionEntity>(
                            this._databaseManager,
                            new UpdateInfoFactoryManager(),
                            mappingStoreIdentifier,
                            new EntityPropertiesExtractor(entityIdFromUrn),
                            _commmandsFromUpdateInfo),
                        databse);
            }

            if (entityType == EntityType.DataSource)
            {
                return
                    new DataSourcePersistenceEngine(
                        this._databaseManager,
                        new UpdateInfoFactoryManager(),
                        mappingStoreIdentifier,
                        new EntityPropertiesExtractor(entityIdFromUrn),
                        _commmandsFromUpdateInfo,
                        entityIdFromUrn);
            }

            //todo actually get a DatabaseManger and change the CommandDefinitionManager to be injected
            return
                new EntityPersistenceEngine<IEntity>(
                    this._databaseManager,
                    new UpdateInfoFactoryManager(),
                    mappingStoreIdentifier,
                    new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo);
        }

        public IEntityPersistenceEngine<TEntity> GetEngine<TEntity>(string mappingStoreIdentifier)
            where TEntity : IEntity
        {
            var entityPersistenceEngineInternal = GetEntityPersistenceEngineInternal<TEntity>(mappingStoreIdentifier);
            if (entityPersistenceEngineInternal == null)
            {
                return null;
            }

            // Avoid a Null Reference exception
            if (_entityRetrieverManager == null)
            {
                return entityPersistenceEngineInternal;
            }

            var userActionPathAndValues =
                new UserActionPathAndValues(this._entityRetrieverManager, mappingStoreIdentifier);
            UserActionRecorder userActionRecorder = new UserActionRecorder(_userActionByType, userActionPathAndValues,
                _databaseManager, _commmandsFromUpdateInfo, mappingStoreIdentifier);
            var userActionEngine = new UserActionEngine<TEntity>(entityPersistenceEngineInternal, userActionRecorder,
                mappingStoreIdentifier,
                _multipleEntitiesUserAction, this._entityRetrieverManager);


            return userActionEngine;
        }

        private IEntityPersistenceEngine<TEntity> GetEntityPersistenceEngineInternal<TEntity>(
            string mappingStoreIdentifier)
            where TEntity : IEntity
        {
            // TODO FIXME Split this into separate factories
            var databse = this._databaseManager.GetDatabase(mappingStoreIdentifier);
            if (databse == null)
            {
                return null;
            }

            if (typeof(TEntity) == typeof(TranscodingRuleEntity))
            {
                return null;
            }

            if (typeof(TEntity) == typeof(TranscodingEntity))
            {
                throw new NotSupportedException("No longer supported in MADB7. Please use TimeDimensionMappingEntity for time transcoding");
            }

            if (typeof(TEntity) == typeof(HeaderEntity))
            {
                return
                    (IEntityPersistenceEngine<TEntity>)
                    new SdmxHeaderTemplatePersistEngine(databse, this._databaseManager, mappingStoreIdentifier);
            }

            ////if (typeof(TEntity) == typeof(HeaderEntity))
            ////{
            ////    return new HeaderTemplatePersistenceEngine<TEntity>(
            ////        this._databaseManager,
            ////        mappingStoreIdentifier,
            ////        new CommandsFromUpdateInfo(),
            ////        new EntityIdFromUrn(databse));
            ////}

            if (typeof(TEntity) == typeof(RegistryEntity))
            {
                return new RegistryPersistenceEngine<TEntity>(
                    this._databaseManager,
                    mappingStoreIdentifier,
                    _commmandsFromUpdateInfo,
                    new UpdateInfoFactoryManager());
            }

            if (typeof(TEntity) == typeof(NsiwsEntity))
            {
                return new NsiwsPersistenceEngine<TEntity>(
                    this._databaseManager,
                    mappingStoreIdentifier,
                    _commmandsFromUpdateInfo,
                    new UpdateInfoFactoryManager());
            }

            var entityIdFromUrn = new EntityIdFromUrn(databse);

            if (typeof(TEntity) == typeof(ColumnDescriptionSourceEntity))
            {
                return (IEntityPersistenceEngine<TEntity>)new DescSourcePersistenceEngine(
                    this._databaseManager,
                    new UpdateInfoFactoryManager(),
                    mappingStoreIdentifier,
                    new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo);
            }

            if (typeof(TEntity) == typeof(MappingSetEntity))
            {
                return
                    (IEntityPersistenceEngine<TEntity>)
                    new MappingSetPersistenceEngine(
                        this._databaseManager,
                        new UpdateInfoFactoryManager(),
                        mappingStoreIdentifier,
                        new EntityPropertiesExtractor(entityIdFromUrn),
                        _commmandsFromUpdateInfo,
                        entityIdFromUrn);
            }

            if (typeof(TEntity) == typeof(DataSourceEntity))
            {
                return
                    (IEntityPersistenceEngine<TEntity>)
                    new DataSourcePersistenceEngine(
                        this._databaseManager,
                        new UpdateInfoFactoryManager(),
                        mappingStoreIdentifier,
                        new EntityPropertiesExtractor(entityIdFromUrn),
                        _commmandsFromUpdateInfo,
                        entityIdFromUrn);
            }

            if (typeof(TEntity) == typeof(DatasetEntity))
            {
                return new DatasetPersistenceEngine<TEntity>(
                    this._databaseManager,
                    new UpdateInfoFactoryManager(),
                    mappingStoreIdentifier,
                    new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo);
            }

            if (typeof(TEntity) == typeof(LocalCodeEntity))
            {
                throw new NotSupportedException("No longer supported in MADB7. Values are stored as they are");
            }

            if (typeof(TEntity) == typeof(UserEntity))
            {
                return (IEntityPersistenceEngine<TEntity>)new UserEntityPersistenceEngine(
                    this._databaseManager,
                    new UpdateInfoFactoryManager(),
                    mappingStoreIdentifier,
                    new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo, entityIdFromUrn);
            }

            if (typeof(TEntity) == typeof(TemplateMapping))
            {
                return (IEntityPersistenceEngine<TEntity>)new TemplateMappingPersistenceEngine(this._databaseManager,
                    new UpdateInfoFactoryManager(), mappingStoreIdentifier,
                    new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo);
            }

            if (typeof(TEntity) == typeof(ComponentMappingEntity))
            {
                return (IEntityPersistenceEngine<TEntity>)new ComponentMappingPersistenceEngine(
                    this._databaseManager,
                    new UpdateInfoFactoryManager(),
                    mappingStoreIdentifier,
                    new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo);
            }

            if (typeof(IConnectionEntity).IsAssignableFrom(typeof(TEntity)))
            {
                return
                    (IEntityPersistenceEngine<TEntity>)
                    new ConnectionEntityPersistenceEngine(
                        this._databaseProviderManager,
                        new EntityPersistenceEngine<DdbConnectionEntity>(
                            this._databaseManager,
                            new UpdateInfoFactoryManager(),
                            mappingStoreIdentifier,
                            new EntityPropertiesExtractor(entityIdFromUrn),
                            _commmandsFromUpdateInfo),
                        databse);
            }

            if (typeof(TEntity) == typeof(TimeDimensionMappingEntity))
            {
                return
                    (IEntityPersistenceEngine<TEntity>)
                    new TimeMappingPersistEngine(databse);
            }
            if (typeof(TEntity) == typeof(LastUpdatedEntity))
            {
                return
                    (IEntityPersistenceEngine<TEntity>)
                    new LastUpdateStatusPersistEngine(databse);
            }
            if (typeof(TEntity) == typeof(ValidToMappingEntity))
            {
                return
                    (IEntityPersistenceEngine<TEntity>)
                    new ValidToMappingPersistEngine(databse);
            }
            if (typeof(TEntity) == typeof(UpdateStatusEntity))
            {
                return
                    (IEntityPersistenceEngine<TEntity>)
                    new UpdateStatusPersistEngine(databse);
            }

            if (typeof(TEntity) == typeof(TimePreFormattedEntity))
            {
                throw new NotSupportedException("No longer a separate entity in MADB7. Please use TimeDimensionMappingEntity for time preformatted");
            }

            if (typeof(TEntity) == typeof(DatasetPropertyEntity))
            {
                return
                    (IEntityPersistenceEngine<TEntity>)
                    new DatasetPropertyPersistenceEngine(
                        this._databaseManager,
                        new UpdateInfoFactoryManager(),
                        mappingStoreIdentifier,
                        new EntityPropertiesExtractor(entityIdFromUrn),
                        _commmandsFromUpdateInfo);
            }

            //todo actually get a DatabaseManger and change the CommandDefinitionManager to be injected
            return
                new EntityPersistenceEngine<TEntity>(
                    this._databaseManager,
                    new UpdateInfoFactoryManager(),
                    mappingStoreIdentifier,
                    new EntityPropertiesExtractor(entityIdFromUrn),
                    _commmandsFromUpdateInfo);
        }
    }
}