using System;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public class DataTableInformationFactory : IDataTableInformationFactory
    {
        public IDataTableInformations GetInformations(DatabaseInformationType entityType)
        {
            switch (entityType)
            {
                case DatabaseInformationType.LocalCode:
                    return new LocalCodeDataTableInformations();
                case DatabaseInformationType.MappingSet:
                    return new MappingSetDataTableInformations();
                case DatabaseInformationType.DdbConnectionSettings:
                    return new DdbConnectionDataTableInformations();
                case DatabaseInformationType.DataSet:
                    return new DatasetDataTableInformations();
                case DatabaseInformationType.DataSetColumn:
                    return new DataSetColumnDataTableInformations();
                case DatabaseInformationType.Transcoding:
                    return new TranscodingDataTableInformations();
                case DatabaseInformationType.TimeTranscoding:
                    return new TimeTranscodingDataTableInformations();
                case DatabaseInformationType.TranscodingRule:
                    return new TranscodingRuleDataTableInformations();
                case DatabaseInformationType.CategoryDbConnection:
                    return new CategoryDbConnectionDataTableInformations();
                case DatabaseInformationType.DataflowDbConnection:
                    return new DataflowDbConnectionDataTableInformations();
                case DatabaseInformationType.Item:
                    return new ItemTableInformations();
                case DatabaseInformationType.TimeMapping:
                    return new TimeDimensionMappingTableInformations();
                case DatabaseInformationType.MappingDataValidity:
                    return new MappingDataValidityTableInformations();
                case DatabaseInformationType.UpdateStatusMapping:
                    return new MappingUpdateStatusTableInformations();
                case DatabaseInformationType.TranscodingScript:
                    return new TranscodingScriptTableInformations();
                case DatabaseInformationType.Mapping:
                    return new MappingTableInformations();
                case DatabaseInformationType.ComponentMappingColumn:
                    return new ComponentMappingColumnDataTableInformations();
                case DatabaseInformationType.ComponentMappingComponent:
                    return new ComponentMappingComponentDataTableInformations();
                case DatabaseInformationType.DescSource:
                    return new DescSourceTableInformation();
                case DatabaseInformationType.Header:
                    return new HeaderDataTableInformations();
                    case DatabaseInformationType.HeaderLocalisedString:
                    return new HeaderLocalisedStringDataTableInformations();
                case DatabaseInformationType.HeaderParty:
                    return new PartyDataTableInformations();
                case DatabaseInformationType.ContactDetails:
                    return new ContactDetailsDataTableInfo();
                case DatabaseInformationType.Contact:
                    return new ContactDataTableInfo();
                case DatabaseInformationType.Dataflow:
                    return new DataflowTableInformations();
                case DatabaseInformationType.Registry:
                    return new RegistryTableInformations();
                case DatabaseInformationType.User:
                    return new UserTableInformations();
                case DatabaseInformationType.DataflowUser:
                    return new DataflowUserTableInformations();
                case DatabaseInformationType.CategoryUser:
                    return new CategoryUserTableInformations();
                case DatabaseInformationType.UserAction:
                    return new UserActionTableInformations();
                case DatabaseInformationType.TemplateMapping:
                    return new TemplateMappingTableInformations();
                case DatabaseInformationType.TemplateMappingTranscodings:
                    return new TemplateMappingTranscodings();
                case DatabaseInformationType.TemplateMappingTimeTranscodings:
                    return new TemplateMappingTimeTranscoding();
                case DatabaseInformationType.DataSource:
                    return new DataSourceTableInformation();
                case DatabaseInformationType.TimePreFormatted:
                    return new TimePreFormattedTableInformation();
                case DatabaseInformationType.Nsiws:
                    return new NsiwsTableInformations();
                case DatabaseInformationType.ComponentMappingMetadataAttribute:
                    return new ComponentMappingMetadataAttributeDataTableInformations();
                case DatabaseInformationType.DatasetProperty:
                    return new DatasetPropertyTableInformation();
                default:
                    throw new NotImplementedException($"entity type {entityType} not implemented");
            }
        }
    }

    
}   