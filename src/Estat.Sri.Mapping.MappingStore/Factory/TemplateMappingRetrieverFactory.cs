using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public class TemplateMappingRetrieverFactory : BaseRetrieverFactory<TemplateMapping>, IEntityRetrieverFactory
    {
        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Estat.Sri.Mapping.MappingStore.Factory.DescSourceRetrieverFactory" /> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        public TemplateMappingRetrieverFactory(DatabaseManager databaseManager)
            : base(databaseManager)
        {
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>The <see cref="T:Estat.Sri.Mapping.Api.Engine.IEntityRetrieverEngine`1" />.</returns>
        protected override IEntityRetrieverEngine<TemplateMapping> GetEngine(Database database, string storeId)
        {
            return new TemplateMappingRetriever(database);
        }
    }
}