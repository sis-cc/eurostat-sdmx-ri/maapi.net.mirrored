// -----------------------------------------------------------------------
// <copyright file="ConnectionEntityRetrieverFactory.cs" company="EUROSTAT">
//   Date Created : 2017-05-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Engine;
    using Estat.Sri.Mapping.MappingStore.Engine.Authorization;
    using Estat.Sri.Mapping.MappingStore.Manager;

    /// <summary>
    /// The connection entity retriever factory.
    /// </summary>
    public class ConnectionEntityRetrieverFactory : IEntityRetrieverFactory
    {
        /// <summary>
        /// The DDB connection settings retriever factory
        /// </summary>
        private readonly DdbConnectioSettingsRetrieverFactory _ddbConnectioSettingsRetrieverFactory;

        /// <summary>
        /// The data provide manager
        /// </summary>
        private readonly IDatabaseProviderManager _dataProvideManager;

        /// <summary>
        /// The database manager
        /// </summary>
        private readonly DatabaseManager _databaseManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionEntityRetrieverFactory" /> class.
        /// </summary>
        /// <param name="ddbConnectioSettingsRetrieverFactory">The DDB connection settings retriever factory.</param>
        /// <param name="dataProvideManager">The data provide manager.</param>
        /// <param name="databaseManager">The database manager.</param>
        public ConnectionEntityRetrieverFactory(DdbConnectioSettingsRetrieverFactory ddbConnectioSettingsRetrieverFactory, IDatabaseProviderManager dataProvideManager, DatabaseManager databaseManager)
        {
            this._ddbConnectioSettingsRetrieverFactory = ddbConnectioSettingsRetrieverFactory;
            this._dataProvideManager = dataProvideManager;
            this._databaseManager = databaseManager;
        }

        /// <summary>
        /// Get the retriever engine.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="mappingAssistantStoreId">The mapping assistant store identifier.</param>
        /// <returns>The <see cref="IEntityRetrieverEngine{TEntity}"/> if a matching is found; otherwise the <c>null</c>.</returns>
        public IEntityRetrieverEngine<TEntity> GetRetrieverEngine<TEntity>(string mappingAssistantStoreId) where TEntity : IEntity
        {
            if (typeof(IConnectionEntity).IsAssignableFrom(typeof(TEntity)))
            {
                var entityRetrieverEngine = this._ddbConnectioSettingsRetrieverFactory.GetRetrieverEngine<DdbConnectionEntity>(mappingAssistantStoreId);
                if (entityRetrieverEngine == null)
                {
                    return null;
                }
                var permissionRetriever = new PermissionRetriever(this._databaseManager.GetDatabase(mappingAssistantStoreId));
                IEntityRetrieverEngine<IConnectionEntity> connectionEntityRetriever = new ConnectionEntityRetriever(entityRetrieverEngine, this._dataProvideManager, permissionRetriever);

                return (IEntityRetrieverEngine<TEntity>)connectionEntityRetriever;
            }

            return null;
        }

        /// <summary>
        /// Gets the retriever engine.
        /// </summary>
        /// <param name="mappingAssistantStoreId">The mapping assistant store identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>The <see cref="IEntityRetrieverEngine{IEntity}"/></returns>
        public IEntityRetrieverEngine<IEntity> GetRetrieverEngine(string mappingAssistantStoreId, EntityType entityType)
        {
            if (entityType == EntityType.DdbConnectionSettings)
            {
                return GetRetrieverEngine<IConnectionEntity>(mappingAssistantStoreId);
            }

            return null;
        }
    }
}