using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using System.Linq;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public class TemplateMappingUpdateFactory : UpdateInfoFactory
    {
        public TemplateMappingUpdateFactory(Database database)
            : base(database)
        {
        }

        public override UpdateInfo Create(PatchRequest patchRequest, string entityId)
        {
            var actions = new List<UpdateInfoAction>
            {
                this.CreateParentTableUpdateInfoAction(patchRequest.Where(item => !item.HasPath("transcodings") && !item.HasPath("timeTranscodings")), DatabaseInformationType.TemplateMapping)
            };
            return new UpdateInfo
            {
                Actions = actions
            };
        }
    }
}