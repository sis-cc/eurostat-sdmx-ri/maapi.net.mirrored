// -----------------------------------------------------------------------
// <copyright file="SqlBuilderFactory.cs" company="EUROSTAT">
//   Date Created : 2017-02-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public class SqlBuilderFactory : ISqlBuilderFactory
    {
        private readonly IDatabaseMapperManager _databaseMapperManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlBuilderFactory"/> class.
        /// </summary>
        /// <param name="databaseMapperManager">The database mapper manager.</param>
        public SqlBuilderFactory(IDatabaseMapperManager databaseMapperManager)
        {
            this._databaseMapperManager = databaseMapperManager;
        }

        /// <summary>
        /// Gets the builder.
        /// </summary>
        /// <param name="updateInfoAction">The update information action.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IList<ISqlBuilder> GetBuilder(UpdateInfoAction updateInfoAction, string entityId)
        {
            var builders = new List<ISqlBuilder>();

            var tableName = this._databaseMapperManager.GetTableName();
            var primaryKeyColumn = this._databaseMapperManager.GetPrimaryKeyColumn();
            var identityColumnsAndValues = new Dictionary<string, object>
            {
                {primaryKeyColumn, entityId}
            };
            var specificTableUpdates = updateInfoAction.PathAndValues.ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
            if (!string.IsNullOrEmpty(_databaseMapperManager.GetBaseTableName()))
            {
                var baseTableUpdates = updateInfoAction.PathAndValues.Where(x => x.Key.Equals("Name", StringComparison.OrdinalIgnoreCase) || x.Key.Equals("Description", StringComparison.OrdinalIgnoreCase)).ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
                specificTableUpdates.Remove("Name");
                specificTableUpdates.Remove("Description");
                if (baseTableUpdates.Any())
                {
                    builders.Add(GetBuilderByTable(_databaseMapperManager.GetBaseTableName(), baseTableUpdates, updateInfoAction.SqlStatementType, identityColumnsAndValues));
                }
            }
            
            if (specificTableUpdates.Any())
            {
                builders.Add(GetBuilderByTable(tableName, specificTableUpdates, updateInfoAction.SqlStatementType, identityColumnsAndValues));
            }
            

            return builders;
        }

        private ISqlBuilder GetBuilderByTable(string tableName, Dictionary<string, object> setColumnsAndValues, SqlStatementType sqlStatementType, Dictionary<string, object> identityColumnsAndValues)
        {
            switch (sqlStatementType)
            {
                case SqlStatementType.Insert:
                    return this.CreateInsertSqlBuilder(tableName, setColumnsAndValues);
                case SqlStatementType.Update:
                    return this.CreateUpdateSqlBuilder(tableName,setColumnsAndValues, identityColumnsAndValues);
                case SqlStatementType.Delete:
                    return this.CreateDeleteSqlBuilder(tableName, setColumnsAndValues.Count != 0 ? setColumnsAndValues : identityColumnsAndValues);
                default:
                    throw new NotImplementedException($"SqlStatementType {sqlStatementType} not implemented");
            }
        }
        private ISqlBuilder CreateDeleteSqlBuilder(string tableName, Dictionary<string, object> whereClause)
        {
            return new DeleteSqlBuilder(tableName, whereClause, this._databaseMapperManager);
        }

        private ISqlBuilder CreateInsertSqlBuilder(string tableName, Dictionary<string, object> setColumnsAndValues)
        {
            return new InsertSqlBuilder(tableName, setColumnsAndValues, this._databaseMapperManager);
        }

        private ISqlBuilder CreateUpdateSqlBuilder(string tableName, Dictionary<string, object> setColumnsAndValues, Dictionary<string, object> whereClause)
        {
            return new UpdateSqlBuilder(tableName, setColumnsAndValues, whereClause, this._databaseMapperManager);
        }
    }
}