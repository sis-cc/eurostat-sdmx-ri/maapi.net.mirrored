using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public class UserRetrieverFactory : BaseRetrieverFactory<UserEntity>, IEntityRetrieverFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRetrieverFactory"/> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        public UserRetrieverFactory(DatabaseManager databaseManager)
            : base(databaseManager)
        {
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>The <see cref="IEntityRetrieverEngine{TRequestedEntity}"/>.</returns>
        protected override IEntityRetrieverEngine<UserEntity> GetEngine(Database database, string storeId)
        {
            return new UserRetrieverEngine(database,new PermissionRetriever(database));
        }
    }
}