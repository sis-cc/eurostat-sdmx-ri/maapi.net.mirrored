﻿// -----------------------------------------------------------------------
// <copyright file="BaseDataProviderFactory.cs" company="EUROSTAT">
//   Date Created : 2017-04-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Factory
{
    using System;

    using Estat.Sri.Mapping.Api.Engine;

    /// <summary>
    /// The base data provider factory.
    /// </summary>
    public abstract class BaseDataProviderFactory
    {
        /// <summary>
        /// The engine
        /// </summary>
        private readonly IDatabaseProviderEngine _engine;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseDataProviderFactory"/> class.
        /// </summary>
        /// <param name="engine">The engine.</param>
        protected BaseDataProviderFactory(IDatabaseProviderEngine engine)
        {
            if (engine == null)
            {
                throw new ArgumentNullException(nameof(engine));
            }

            _engine = engine;
            ProviderName = _engine.ProviderName;
            ProviderType = _engine.Name;
        }

        /// <summary>
        /// Gets the name of the provider.
        /// </summary>
        /// <value>
        /// The name of the provider.
        /// </value>
        public string ProviderName { get; private set; } 

        /// <summary>
        /// Gets the type of the provider.
        /// </summary>
        /// <value>
        /// The type of the provider.
        /// </value>
        public string ProviderType { get; private set; }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sri.Mapping.Api.Engine.IDatabaseProviderEngine" />.
        /// </returns>
        public IDatabaseProviderEngine GetEngineByProvider(string providerName)
        {
            if (this._engine.ProviderName.Equals(providerName))
            {
                return this._engine;
            }

            return null;
        }

        /// <summary>
        /// Gets the engine given the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sri.Mapping.Api.Engine.IDatabaseProviderEngine" />.
        /// </returns>
        public IDatabaseProviderEngine GetEngineByType(string type)
        {
            if (this._engine.Name.Equals(type))
            {
                return this._engine;
            }

            return null;
        }
    }
}