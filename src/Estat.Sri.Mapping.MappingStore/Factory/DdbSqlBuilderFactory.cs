﻿// -----------------------------------------------------------------------
// <copyright file="DdbSqlBuilderFactory.cs" company="EUROSTAT">
//   Date Created : 2014-08-26
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Factory
{
    using System;

    using Estat.Sri.Mapping.MappingStore.Builder;
    using Estat.Sri.Mapping.MappingStore.Constant;

    /// <summary>
    ///     The SQL Builder factory
    /// </summary>
    public class DdbSqlBuilderFactory : IDdbSqlBuilderFactory
    {
        /// <summary>
        /// The normal SQL builder
        /// </summary>
        private readonly IDdbSqlBuilder _normalSqlBuilder;

        /// <summary>
        /// The ordered DDB SQL builder
        /// </summary>
        private readonly OrderedDdbSqlBuilder _orderedDdbSqlBuilder;

        /// <summary>
        /// The count SQL builder
        /// </summary>
        private readonly CountSqlBuilder _countSqlBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="DdbSqlBuilderFactory"/> class.
        /// </summary>
        public DdbSqlBuilderFactory()
        {
            _normalSqlBuilder = new DdbSqlBuilder();
            _orderedDdbSqlBuilder = new OrderedDdbSqlBuilder(this._normalSqlBuilder);
            _countSqlBuilder = new CountSqlBuilder(this._normalSqlBuilder);
        }

        /// <summary>
        ///     Creates the builder.
        /// </summary>
        /// <param name="sqlQueryType"></param>
        /// <returns>
        ///     The <see cref="IDdbSqlBuilder" />
        /// </returns>
        public IDdbSqlBuilder CreateBuilder(SqlBuilderType sqlQueryType)
        {
            switch (sqlQueryType)
            {
                case SqlBuilderType.Normal:
                    return this._normalSqlBuilder;
                case SqlBuilderType.Ordered:
                    return _orderedDdbSqlBuilder;
                case SqlBuilderType.Count:
                    return _countSqlBuilder;
                default:
                    throw new ArgumentOutOfRangeException(nameof(sqlQueryType), sqlQueryType, null);
            }
        }
    }
}