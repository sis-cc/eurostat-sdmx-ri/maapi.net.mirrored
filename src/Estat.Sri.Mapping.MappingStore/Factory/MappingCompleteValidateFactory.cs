﻿// -----------------------------------------------------------------------
// <copyright file="MappingCompleteValidateFactory.cs" company="EUROSTAT">
//   Date Created : 2017-09-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Factory
{
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.MappingStore.Helper;

    /// <summary>
    /// Provides the SRI default validator
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Factory.IComponentMappingValidatorFactory" />
    public class MappingCompleteValidateFactory : IComponentMappingValidatorFactory
    {
        /// <summary>
        /// The mapping validator
        /// </summary>
        private readonly IComponentMappingValidator _mappingValidator = new MappingUtils();

        /// <summary>
        /// Gets the component mapping validation engine
        /// </summary>
        /// <returns>
        /// The <see cref="IComponentMappingValidator"/>.
        /// </returns>
        public IComponentMappingValidator GetValidator()
        {
            return _mappingValidator;
        }
    }
}