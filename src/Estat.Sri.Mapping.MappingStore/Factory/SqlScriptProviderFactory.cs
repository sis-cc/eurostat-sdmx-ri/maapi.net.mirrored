// -----------------------------------------------------------------------
// <copyright file="SqlScriptProviderFactory.cs" company="EUROSTAT">
//   Date Created : 2017-04-27
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Factory
{
    using System;

    using Estat.Sri.Mapping.MappingStore.Engine;
    using Estat.Sri.MappingStoreRetrieval.Config;

    /// <summary>
    /// The SQL script provider factory.
    /// </summary>
    public class SqlScriptProviderFactory : ISqlScriptProviderFactory
    {
        /// <summary>
        /// The resource data location factory
        /// </summary>
        private readonly ResourceDataLocationFactory _resourceDataLocationFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlScriptProviderFactory"/> class.
        /// </summary>
        public SqlScriptProviderFactory()
        {
            this._resourceDataLocationFactory = new ResourceDataLocationFactory();
        }

        /// <summary>
        /// Gets the engine for providing SQL Statements.
        /// </summary>
        /// <param name="databaseType">Type of the database.</param>
        /// <returns>The <see cref="ISqlScriptProviderEngine"/></returns>
        public ISqlScriptProviderEngine GetEngine(string databaseType)
        {
            if (databaseType == null)
            {
                throw new ArgumentNullException(nameof(databaseType));
            }

            switch (databaseType)
            {
                case MappingStoreDefaultConstants.SqlServerName:
                case MappingStoreDefaultConstants.OracleName:
                case MappingStoreDefaultConstants.MySqlName:
                    return new SqlScriptProviderEngine(databaseType, this._resourceDataLocationFactory);
            }

            return null;
        }
    }
}