// -----------------------------------------------------------------------
// <copyright file="UpdateInfoFactory.cs" company="EUROSTAT">
//   Date Created : 2017-03-02
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public abstract class UpdateInfoFactory
    {
        private readonly Database _database;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        protected UpdateInfoFactory(Database database)
        {
            this._database = database;
        }

        public abstract UpdateInfo Create(PatchRequest patchRequest, string entityId);

        protected KeyValuePair<string, object> CreateParentTablePathAndValue(DatabaseInformationType databaseInformationType, string entityId)
        {
            var primaryKeyColumn = new DatabaseMapperManager(databaseInformationType).GetPrimaryKeyColumn();
            return new KeyValuePair<string, object>(primaryKeyColumn, entityId);
        }

        protected UpdateInfoAction CreateParentTableUpdateInfoAction(IEnumerable<PatchDocument> patchRequest, DatabaseInformationType databaseInformationType)
        {
            var pathAndValues = patchRequest.ToDictionary(patchDocument => patchDocument.Path.TrimStart('/'), patchDocument => patchDocument.Op == "remove" ? null : patchDocument.Value);

            if (pathAndValues.Count == 0)
            {
                return null;
            }
            return new UpdateInfoAction
            {
                DataInformationType = databaseInformationType,
                SqlStatementType = SqlStatementType.Update,
                PathAndValues = pathAndValues
            };
        }

        protected UpdateInfoAction CreateRelatedTableUpdateInfoAction(PatchDocument patchDocument, DatabaseInformationType databaseInformationType, Func<object, object> getValue, KeyValuePair<string, object> parentTablePathAndValue, string relatedJsonName, bool fromPath = false)
        {
            UpdateInfoAction updateInfoAction = null;
            object value = null;
            if (fromPath || patchDocument.Op == "remove")
            {
                var strings = patchDocument.Path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                value = getValue(strings.Last());
            }
            else
            {
                value = getValue(patchDocument.Value);
            }
            switch (patchDocument.Op)
            {
                case "add":
                    updateInfoAction = new UpdateInfoAction
                    {
                        DataInformationType = databaseInformationType,
                        SqlStatementType = SqlStatementType.Insert,
                        PathAndValues = new Dictionary<string, object> {{relatedJsonName, value}, { parentTablePathAndValue.Key, parentTablePathAndValue.Value } }
                    };
                    break;
                case "replace":
                    updateInfoAction = new UpdateInfoAction
                    {
                        DataInformationType = databaseInformationType,
                        SqlStatementType = SqlStatementType.Update,
                        PathAndValues = new Dictionary<string, object> {{relatedJsonName, value}}
                    };
                    break;
                case "remove":
                   
                    updateInfoAction = new UpdateInfoAction
                    {
                        DataInformationType = databaseInformationType,
                        SqlStatementType = SqlStatementType.Delete,
                        PathAndValues = new Dictionary<string, object> {{relatedJsonName, value}}
                    };
                    break;
            }

            return updateInfoAction;
        }

        protected object GetReferenceId(object value)
        {
            //var patchDocument = value as PatchDocument;
            var urn = value.ToString();
            var structureReferenceImpl = new StructureReferenceImpl(urn);
            return new MaintainableRefRetrieverEngine(this._database, new MappingStoreRetrievalManager(new RetrievalEngineContainer(this._database))).Retrieve(structureReferenceImpl);
        }

        protected object GetReferenceIdFromValue(object value)
        {
            //var patchDocument = value as PatchDocument;
            var urn = value.ToString();
            var structureReferenceImpl = new StructureReferenceImpl(urn);
            return new MaintainableRefRetrieverEngine(this._database, new MappingStoreRetrievalManager(new RetrievalEngineContainer(this._database))).Retrieve(structureReferenceImpl);
        }
    }
}