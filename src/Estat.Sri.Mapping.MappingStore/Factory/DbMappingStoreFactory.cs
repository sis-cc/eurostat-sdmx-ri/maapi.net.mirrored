﻿// -----------------------------------------------------------------------
// <copyright file="DbMappingStoreFactory.cs" company="EUROSTAT">
//   Date Created : 2017-03-27
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Factory
{
    using System.Linq;

    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.MappingStore.Engine;
    using Estat.Sri.Mapping.MappingStore.Engine.Migration;
    using Estat.Sri.Mapping.MappingStore.Manager;

    /// <summary>
    /// The Mapping Store Database specific factory implementation
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Factory.IMappingStoreFactory" />
    public class DbMappingStoreFactory : IMappingStoreFactory
    {
        /// <summary>
        /// The engine
        /// </summary>
        private readonly DbMappingStoreEngine _engine;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbMappingStoreFactory" /> class.
        /// </summary>
        /// <param name="configurationStoreManager">The configuration store manager.</param>
        /// <param name="databaseProviderManager">The database provider manager.</param>
        /// <param name="sqlScriptProviderManager">The SQL script provider manager.</param>
        /// <param name="bundledSdmxArtefactImporter">The bundled SDMX artefact importer.</param>
        /// <param name="migrationEngine">The migration engine.</param>
        public DbMappingStoreFactory(IConfigurationStoreManager configurationStoreManager, IDatabaseProviderManager databaseProviderManager, ISqlScriptProviderManager sqlScriptProviderManager, BundledSdmxArtefactImporter bundledSdmxArtefactImporter)
        {
            this._engine = new DbMappingStoreEngine(
                configurationStoreManager,
                databaseProviderManager,
                sqlScriptProviderManager,
                bundledSdmxArtefactImporter);
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="storeType">Type of the store.</param>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>The <see cref="IMappingStoreEngine"/>.</returns>
        public IMappingStoreEngine GetEngine(string storeType, string storeId)
        {
            if ((storeType == null || this._engine.StoreType.Equals(storeType)) && (storeId == null || this._engine.AvailableStoreId.Contains(storeId)))
            {
                return this._engine;
            }

            return null;
        }
    }
}