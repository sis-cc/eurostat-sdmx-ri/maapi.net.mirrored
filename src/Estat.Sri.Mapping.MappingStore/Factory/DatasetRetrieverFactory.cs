// -----------------------------------------------------------------------
// <copyright file="DatasetRetrieverFactory.cs" company="EUROSTAT">
//   Date Created : 2017-03-17
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    using System;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Engine;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// The dataset retriever factory.
    /// </summary>
    public class DatasetRetrieverFactory : BaseRetrieverFactory<DatasetEntity>, IEntityRetrieverFactory
    {
        private readonly Lazy<IDataSetEditorManager> dataSetEditorManager;
        /// <summary>
        /// Initializes a new instance of the <see cref="DatasetRetrieverFactory" /> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        /// <param name="dataSetEditorManager"></param>
        public DatasetRetrieverFactory(DatabaseManager databaseManager, Lazy<IDataSetEditorManager> dataSetEditorManager = null)
            : base(databaseManager)
        {
            this.dataSetEditorManager = dataSetEditorManager;
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>The <see cref="IEntityRetrieverEngine{TRequestedEntity}"/>.</returns>
        protected override IEntityRetrieverEngine<DatasetEntity> GetEngine(Database database,string storeId)
        {
            var datasetRetrieverEngine = new DatasetRetrieverEngine(database,new PermissionRetriever(database), dataSetEditorManager);
            return datasetRetrieverEngine;
        }
    }
}