// -----------------------------------------------------------------------
// <copyright file="TimeDimensionMappingWithTranscodingFactory.cs" company="EUROSTAT">
//   Date Created : 2017-09-08
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Factory.MappingLogic
{
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;
    using Estat.Sri.Mapping.MappingStore.Builder.MappingLogic;
    using Estat.Sri.Mapping.MappingStore.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The time dimension mapping with transcoding factory.
    /// </summary>
    public class TimeDimensionMappingWithTranscodingFactory : ITimeDimensionMappingFactory
    {
        /// <summary>
        /// Gets the builder.
        /// </summary>
        /// <param name="componentMapping">The component mapping.</param>
        /// <param name="timeTranscoding">The time transcoding.</param>
        /// <param name="databaseType">Type of the database.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sri.Mapping.Api.Builder.ITimeDimensionMappingBuilder" />.
        /// </returns>
        public ITimeDimensionMappingBuilder GetBuilder(TimeDimensionMappingEntity componentMapping, TimeTranscodingEntity timeTranscoding, string databaseType)
        {
            if (componentMapping == null || databaseType == null)
            {
                return null;
            }

            if (componentMapping.HasTranscoding() && timeTranscoding != null)
            {
                TimeExpressionEntity expression = new TimeExpressionEntity(timeTranscoding, componentMapping);
                if (!expression.IsDateTime)
                {
                    if (expression.OneColumnMapping)
                    {
                        return new TimeDimension1Column(componentMapping, expression, databaseType);
                    }
                    
                    return new TimeDimension2Column(componentMapping, expression, databaseType);
                }

                return new TimeDimensionDateType(expression, databaseType);
            }

            return null;
        }

        /// <summary>
        /// Gets the builder for advanced mapping.
        /// </summary>
        /// <param name="criteriaColumn">The criteria column</param>
        /// <param name="timeTranscoding">The time transcoding.</param>
        /// <param name="durationMappingBuilder">ComponentMapping Entity id to <see cref="IComponentMappingBuilder"/> </param>
        /// <param name="databaseType">Type of the database.</param>
        /// <returns>The <see cref="ITimeDimensionMappingBuilder" />.</returns>
        public ITimeDimensionMappingBuilder GetBuilder(
            DataSetColumnEntity criteriaColumn,
            TimeFormatConfiguration timeTranscoding,
            IComponentMappingBuilder durationMappingBuilder,
            string databaseType)
        {
            if (criteriaColumn == null)
            {
                return null;
            }

            if (timeTranscoding == null)
            {
                return null;
            }

            if (!timeTranscoding.HasParticleConfiguration)
            {
                return null;
            }


            if (durationMappingBuilder == null)
            {
                if (timeTranscoding.EndConfig != null)
                {
                    // we have separate start and end configurations
                    // we need the output to be date to compine the start and end in TimeDimensionTimeRangeWithStartEndMappingBuilder
                    ITimeDimensionMappingBuilder startConfigBuilder = BuildParticleBuilder(databaseType, timeTranscoding.StartConfig, TimeFormat.Date, null);
                    ITimeDimensionMappingBuilder endConfigBuilder = BuildParticleBuilder(databaseType, timeTranscoding.EndConfig, TimeFormat.Date, null);

                    if (startConfigBuilder != null && endConfigBuilder != null)
                    {
                        return new TimeDimensionTimeRangeWithStartEndMappingBuilder(
                            timeTranscoding,
                            startConfigBuilder,
                            endConfigBuilder);
                    }
                }
                else if (timeTranscoding.StartConfig != null)
                {
                    // ISO 8601 configuration or Date Time or non-Time range
                    return new TimeDimensionIso8601StartMappingBuilder(timeTranscoding.StartConfig, timeTranscoding.OutputFormat, null);
                }
            }
            else
            {
                if (timeTranscoding.StartConfig != null)
                {
                    if (timeTranscoding.StartConfig.Format == DisseminationDatabaseTimeFormat.Iso8601Compatible)
                    {
                        return new TimeDimensionIso8601StartMappingBuilder(timeTranscoding.StartConfig, timeTranscoding.OutputFormat, durationMappingBuilder);
                    }

                    ITimeDimensionMappingBuilder startConfigBuilder = BuildParticleBuilder(databaseType, timeTranscoding.StartConfig, TimeFormat.Date, durationMappingBuilder);
                    return new TimeDimensionStartDurationMappingBuilder(
                        startConfigBuilder,
                        durationMappingBuilder,
                        timeTranscoding.OutputFormat);
                }

                if (timeTranscoding.EndConfig != null)
                {
                    if (timeTranscoding.EndConfig.Format == DisseminationDatabaseTimeFormat.Iso8601Compatible)
                    {
                        return new TimeDimensionIso8601EndMappingBuilder(timeTranscoding.EndConfig, TimeFormat.Date, durationMappingBuilder);
                    }

                    ITimeDimensionMappingBuilder endConfigBuilder = BuildParticleBuilder(databaseType, timeTranscoding.EndConfig, TimeFormat.Date, durationMappingBuilder);
                    return new TimeDimensionEndDurationMappingBuilder(endConfigBuilder, durationMappingBuilder, timeTranscoding.OutputFormat);
                }
            }

            return null;
        }

        private static ITimeDimensionMappingBuilder BuildParticleBuilder(
            string databaseType,
            TimeParticleConfiguration particleConfiguration,
            TimeFormat outputFormat,
            IComponentMappingBuilder durationMappingBuilder)
        {
            ITimeDimensionMappingBuilder particleBuilder = null;
            if (particleConfiguration != null)
            {
                switch (particleConfiguration.Format)
                {
                    case DisseminationDatabaseTimeFormat.TimestampOrDate:
                        particleBuilder = new TimeDimensionDateType(particleConfiguration, databaseType, outputFormat);
                        break;
                    case DisseminationDatabaseTimeFormat.Iso8601Compatible:
                        particleBuilder = new TimeDimensionIso8601StartMappingBuilder(particleConfiguration, outputFormat, durationMappingBuilder);
                        break;
                    case DisseminationDatabaseTimeFormat.Annual:
                        particleBuilder = new TimeDimension1ColumnAdvanced(particleConfiguration, databaseType);
                        break;
                    default:
                        var yearColum = particleConfiguration.Year.Column.Name;
                        if (particleConfiguration.Period != null)
                        {
                            if (string.Equals(particleConfiguration.Period.Column.Name, yearColum))
                            {
                                particleBuilder = new TimeDimension1ColumnAdvanced(particleConfiguration, databaseType);
                            }
                            else
                            {
                                particleBuilder = new TimeDimension2ColumnAdvanced(particleConfiguration, databaseType);
                            }
                        }
                        else
                        {
                            particleBuilder = new TimeDimension1ColumnAdvanced(particleConfiguration, databaseType);
                        }
                        break;
                }
            }

            return particleBuilder;
        }
    }
}