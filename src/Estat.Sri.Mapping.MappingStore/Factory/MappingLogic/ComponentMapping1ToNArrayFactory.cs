// -----------------------------------------------------------------------
// <copyright file="ComponentMapping1ToNArrayFactory.cs" company="EUROSTAT">
//   Date Created : 2023-01-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Factory.MappingLogic
{
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder.MappingLogic;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Can return a mapping builder capable of mapping an array component to N dataset columns without transcoding.
    /// </summary>
    public class ComponentMapping1ToNArrayFactory : IComponentMappingFactory
    {
        /// <summary>
        /// Applied to array components. 
        /// Checks if mapping to N dataset columns without transcoding can be performed and builds the mapper.
        /// </summary>
        /// <param name="componentMapping"></param>
        /// <param name="transcodingRuleMap"></param>
        /// <param name="databaseType"></param>
        /// <returns>
        /// <see cref="ComponentMapping1Narray"/> or 
        /// <c>null</c>.
        /// </returns>
        public IComponentMappingBuilder GetBuilder(ComponentMappingEntity componentMapping, TranscodingRuleMap transcodingRuleMap, string databaseType)
        {
            if (componentMapping.Component.ValueType == Api.Constant.ComponentValueType.Array
                && componentMapping.IsNormalOrDurationOrMetadataMapping()
                && !componentMapping.IsTimeDimensionMapping()
                && !componentMapping.HasTranscoding()
                && ObjectUtil.ValidCollection(componentMapping.GetColumns())
                && componentMapping.GetColumns().Count > 1)
            {
                return new ComponentMapping1Narray(componentMapping, databaseType);
            }

            return null;
        }
    }
}
