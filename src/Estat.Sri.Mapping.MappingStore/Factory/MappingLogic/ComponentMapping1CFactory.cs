// -----------------------------------------------------------------------
// <copyright file="ComponentMapping1CFactory.cs" company="EUROSTAT">
//   Date Created : 2017-09-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Factory.MappingLogic
{
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder.MappingLogic;

    using Org.Sdmxsource.Util;

    /// <summary>
    /// Can return a mapping builder capable of mapping a component to a constant value.
    /// </summary>
    public class ComponentMapping1CFactory : IComponentMappingFactory
    {
        /// <summary>
        /// Applied to scalar components. 
        /// Checks if mapping with a constant value is possible and builds the mapper.
        /// </summary>
        /// <param name="componentMapping"></param>
        /// <param name="transcodingRuleMap"></param>
        /// <param name="databaseType"></param>
        /// <returns>A <see cref="ComponentMapping1C"/> or <c>null</c>.</returns>
        public IComponentMappingBuilder GetBuilder(ComponentMappingEntity componentMapping, TranscodingRuleMap transcodingRuleMap, string databaseType)
        {
            if (componentMapping.Component.ValueType == Api.Constant.ComponentValueType.Scalar
                && !componentMapping.HasTranscoding() 
                && componentMapping.IsNormalOrDurationOrMetadataMapping() 
                && componentMapping.ConstantValue != null 
                && !ObjectUtil.ValidCollection(componentMapping.GetColumns()) 
                && !componentMapping.IsTimeDimensionMapping())
            {
                return new ComponentMapping1C(componentMapping, databaseType);
            }

            return null;
        }
    }
}