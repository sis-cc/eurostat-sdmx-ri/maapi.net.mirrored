﻿// -----------------------------------------------------------------------
// <copyright file="UpdateStatusMappingFactory.cs" company="EUROSTAT">
//   Date Created : 2017-09-13
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Factory.MappingLogic
{
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder.MappingLogic;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Builder;

    /// <summary>
    /// The default update status mapping implementation
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Factory.IUpdateStatusMappingFactory" />
    public class UpdateStatusMappingFactory : IUpdateStatusMappingFactory
    {
        /// <summary>
        /// The builder
        /// </summary>
        private readonly IBuilder<Database, DdbConnectionEntity> _builder;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateStatusMappingFactory"/> class.
        /// </summary>
        /// <param name="builder">The builder.</param>
        public UpdateStatusMappingFactory(IBuilder<Database, DdbConnectionEntity> builder)
        {
            _builder = builder;
        }

        /// <summary>
        /// Gets the builder.
        /// </summary>
        /// <param name="connectionEntity">The connection entity.</param>
        /// <param name="updateStatus"></param>
        /// <returns>
        /// The <see cref="IUpdateStatusMappingBuilder" />.
        /// </returns>
        public IUpdateStatusMappingBuilder GetBuilder(DdbConnectionEntity connectionEntity, UpdateStatusEntity updateStatus)
        {
            if (connectionEntity != null)
            {
                var ddb = _builder?.Build(connectionEntity);
                if (ddb != null)
                {
                    return new UpdateStatusMapping(updateStatus, ddb);
                }
            }

            return null;
        }
    }
}