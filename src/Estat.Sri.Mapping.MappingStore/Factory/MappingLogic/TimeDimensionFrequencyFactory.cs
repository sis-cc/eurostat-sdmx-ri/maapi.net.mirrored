// -----------------------------------------------------------------------
// <copyright file="TimeDimensionFrequencyFactory.cs" company="EUROSTAT">
//   Date Created : 2017-09-08
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Factory.MappingLogic
{
    using System;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder.MappingLogic;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The time dimension frequency factory.
    /// </summary>
    public class TimeDimensionFrequencyFactory : ITimeDimensionFrequencyFactory
    {
        /// <summary>
        /// The dimension mapping factory
        /// </summary>
        private readonly ITimeDimensionMappingFactory[] _dimensionMappingFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDimensionFrequencyFactory"/> class.
        /// </summary>
        /// <param name="factories">The factories.</param>
        /// <exception cref="System.ArgumentNullException">factories is null</exception>
        public TimeDimensionFrequencyFactory(params ITimeDimensionMappingFactory[] factories)
        {
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories));
            }

            this._dimensionMappingFactory = factories.ToArray();
        }

        /// <summary>
        /// Gets the builder.
        /// </summary>
        /// <param name="timeDimensionMappingSettings">The settings to create the builder with.</param>
        /// <returns>
        /// The <see cref="ITimeDimensionFrequencyMappingBuilder" />.
        /// </returns>
        public ITimeDimensionFrequencyMappingBuilder GetBuilder(
            TimeDimensionMappingSettings timeDimensionMappingSettings)
        {
            if (timeDimensionMappingSettings == null)
            {
                return null;
            }

            // obsolete but could be setup somewhere
            if (timeDimensionMappingSettings.TimePreFormattedEntity != null)
            {
                return new TimePreFormattedFrequencyMappingBuilder(
                    timeDimensionMappingSettings.TimePreFormattedEntity,
                    timeDimensionMappingSettings.ComponentMapping,
                    timeDimensionMappingSettings.DatabaseType);
            }

            if (timeDimensionMappingSettings.ComponentMapping == null)
            {
                return null;
            }

            switch (timeDimensionMappingSettings.ComponentMapping.TimeMappingType)
            {
                case TimeDimensionMappingType.MappingWithColumn:
                case TimeDimensionMappingType.MappingWithConstant:
                    return new TimeDimensionSingleFrequency(GetTimeMappingBuilder(timeDimensionMappingSettings.ComponentMapping, null, timeDimensionMappingSettings.DatabaseType), timeDimensionMappingSettings.FrequencyMapping);
                case TimeDimensionMappingType.PreFormatted:
                    return new TimePreFormattedFrequencyMappingBuilder(
                        timeDimensionMappingSettings.ComponentMapping.PreFormatted,
                        timeDimensionMappingSettings.ComponentMapping,
                        timeDimensionMappingSettings.DatabaseType);
                case TimeDimensionMappingType.TranscodingWithCriteriaColumn:
                    return new TimeDimensionRangeCriteriaValue(timeDimensionMappingSettings.ComponentMapping.Transcoding, timeDimensionMappingSettings.DurationMappings, timeDimensionMappingSettings.DatabaseType);
                case TimeDimensionMappingType.TranscodingWithFrequencyDimension:
                    {
                        var timeDimensionMappings = timeDimensionMappingSettings.SimpleTranscodingEntity.ToDictionary(
                         entity => entity.Frequency,
                         entity => GetTimeMappingBuilder(timeDimensionMappingSettings.ComponentMapping, entity, timeDimensionMappingSettings.DatabaseType),
                         StringComparer.Ordinal);
                        return new TimeDimensionMultiFrequency(timeDimensionMappings, timeDimensionMappingSettings.FrequencyMapping);
                    }
                    default: throw new SdmxNotImplementedException("Time Mapping type not set");
            }
        }

        /// <summary>
        /// Gets the time mapping builder.
        /// </summary>
        /// <param name="componentMapping">The component mapping.</param>
        /// <param name="timeTranscoding">The time transcoding.</param>
        /// <param name="databaseType">Type of the database.</param>
        /// <returns>
        /// The <see cref="ITimeDimensionMappingBuilder" />.
        /// </returns>
        private ITimeDimensionMappingBuilder GetTimeMappingBuilder(TimeDimensionMappingEntity componentMapping, TimeTranscodingEntity timeTranscoding, string databaseType)
        {
            return
                _dimensionMappingFactory.Select(
                    factory => factory.GetBuilder(componentMapping, timeTranscoding, databaseType)).First(builder => builder != null);
        }
    }
}