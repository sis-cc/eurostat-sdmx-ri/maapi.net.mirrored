// -----------------------------------------------------------------------
// <copyright file="ComponentMappingTranscodingArrayFactory.cs" company="EUROSTAT">
//   Date Created : 2023-01-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Factory.MappingLogic
{
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder.MappingLogic;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Can return a mapping builder capable of mapping an array component with transcoding.
    /// </summary>
    public class ComponentMappingTranscodingArrayFactory : IComponentMappingFactory
    {
        /// <summary>
        /// Applied to array components. 
        /// Checks if mapping with transcoding can be performed and builds the mapper.
        /// </summary>
        /// <param name="componentMapping"></param>
        /// <param name="transcodingRuleMap"></param>
        /// <param name="databaseType"></param>
        /// <returns>r
        /// <see cref="ComponentMapping1NarrayT"/> or 
        /// <c>null</c>.
        /// </returns>
        public IComponentMappingBuilder GetBuilder(ComponentMappingEntity componentMapping, TranscodingRuleMap transcodingRuleMap, string databaseType)
        {
            if (componentMapping.Component.ValueType != Api.Constant.ComponentValueType.Array 
                || !componentMapping.IsNormalOrDurationOrMetadataMapping()
                || componentMapping.IsTimeDimensionMapping()
                || !componentMapping.HasTranscoding()
                || !ObjectUtil.ValidCollection(componentMapping.GetColumns()))
            {
                return null;
            }

            // array type component (N) maps to 1 column, transcoding 1 local code to N sdmx array values
            if (componentMapping.GetColumns().Count == 1)
            {
                return new ComponentMapping1NarrayT(componentMapping, transcodingRuleMap, databaseType);
            }
            // array type component (N) maps to N columns, transcoding 1 local code to 1 sdmx value with transcoding (not supported yet)
            else if (componentMapping.GetColumns().Count > 1)
            {
                throw new SdmxNotImplementedException("Mapping array values N to N with transcoding is not supported yet.");
            }

            return null;
        }
    }
}
