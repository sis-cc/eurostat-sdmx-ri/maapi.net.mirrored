using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Manager;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    using Estat.Sri.MappingStoreRetrieval.Manager;

    public class HeaderRetrieverFactory : BaseRetrieverFactory<HeaderEntity>, IEntityRetrieverFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRetrieverFactory{TEntity}" /> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        public HeaderRetrieverFactory(DatabaseManager databaseManager)
            : base(databaseManager)
        {
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>The <see cref="IEntityRetrieverEngine{TRequestedEntity}"/>.</returns>
        protected override IEntityRetrieverEngine<HeaderEntity> GetEngine(Database database, string storeId)
        {
            return new SdmxHeaderTemplateRetrieverEngine(database);
        }
    }
}