using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Engine.Cloning;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using System.Configuration;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public class DataStructureOnTheFlyFactory : IDataStructureOnTheFlyFactory
    {
        private IConfigurationStoreManager configurationStoreManager;
        private readonly IEntityRetrieverManager entityRetrieverManager;
        private readonly IEntityPersistenceManager entityPersistenceManager;
        private readonly ICommonSdmxObjectRetrievalFactory commonSdmxObjectRetrievalFactory;

        public DataStructureOnTheFlyFactory(IConfigurationStoreManager configurationStoreManager, IEntityRetrieverManager entityRetrieverManager,IEntityPersistenceManager entityPersistenceManager, ICommonSdmxObjectRetrievalFactory commonSdmxObjectRetrievalFactory)
        {
            this.configurationStoreManager = configurationStoreManager;
            this.entityRetrieverManager = entityRetrieverManager;
            this.entityPersistenceManager = entityPersistenceManager;
            this.commonSdmxObjectRetrievalFactory = commonSdmxObjectRetrievalFactory;
        }

        public IDataStructureOnTheFlyEngine GetEngine(string sid)
        {
            var connectionStringSettings = ConfigurationManager.ConnectionStrings[sid];
            var databaseManager = new DatabaseManager(configurationStoreManager);

            var db = databaseManager.GetDatabase(sid);
                  IEntityRetrieverManager retrieverManager = new EntityRetrieverManager(new DatasetColumnRetrieverFactory(databaseManager), new DatasetRetrieverFactory(databaseManager));
            var datasetSetColumnRetriver = (DefaultsEntityRetrieverEngine<DataSetColumnEntity>)retrieverManager.GetRetrieverEngine<DataSetColumnEntity>(connectionStringSettings.Name);
            var datasetRetriver = (DefaultsEntityRetrieverEngine<DatasetEntity>)retrieverManager.GetRetrieverEngine<DatasetEntity>(connectionStringSettings.Name);
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(configurationStoreManager), null);

            MappingSetPersistenceEngine mappingSetPersistenceEngine = (MappingSetPersistenceEngine)entityPeristenceFactory.GetEngine<MappingSetEntity>(sid);
            ComponentMappingPersistenceEngine componentMappingPersistenceEngine = (ComponentMappingPersistenceEngine)entityPeristenceFactory.GetEngine<ComponentMappingEntity>(sid);
            TranscodingRuleEntityPersistenceEngine transcodingRuleEntityPersistenceEngine = (TranscodingRuleEntityPersistenceEngine)new TranscodingRulePersistFactory(databaseManager).GetEngine<TranscodingRuleEntity>(sid);
            var mappingSetRetriever = new MappingSetEntityRetriever(db, sid);
            var componentMappingRetriever = new ComponentMappingRetrieverEngine(db);
            var transcodingRetriever = new TranscodingRetrieverEngine(db, null, sid);
            var transcodingRuleRetriever = new TranscodingRuleRetrieverEngine(db);
            var entityCloneHelper = new EntityCloneHelper(entityRetrieverManager, entityPersistenceManager, sid);
            return new DataStructureOnTheFlyEngine(databaseManager,datasetSetColumnRetriver,mappingSetPersistenceEngine,componentMappingPersistenceEngine,transcodingRuleEntityPersistenceEngine
                ,mappingSetRetriever,componentMappingRetriever,transcodingRuleRetriever,entityCloneHelper,commonSdmxObjectRetrievalFactory,retrieverManager,entityPersistenceManager);
        }
    }
}
