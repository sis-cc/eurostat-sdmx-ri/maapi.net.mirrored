// -----------------------------------------------------------------------
// <copyright file="HeaderInfoFactory.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public class HeaderInfoFactory : UpdateInfoFactory
    {
        private readonly string _sender = "sender";

        private readonly string _receiver = "receiver";

        public HeaderInfoFactory(Database database)
            : base(database)
        {
        }

        public override UpdateInfo Create(PatchRequest patchRequest, string entityId)
        {
            var updateInfoActions = new List<UpdateInfoAction>();
            this.CreatePartyInfoActions(patchRequest,entityId, updateInfoActions);
            foreach (var patchDocument in patchRequest)
            {
                UpdateInfoAction updateInfoAction = null;
                if (patchDocument.HasPath(this._receiver))
                {
                    updateInfoAction = this.CreateRelatedTableUpdateInfoAction(patchDocument, DatabaseInformationType.Header, this.GetReferenceId, this.CreateParentTablePathAndValue(DatabaseInformationType.HeaderLocalisedString, entityId), this._receiver);
                }

                if (patchDocument.HasPath(this._sender))
                {
                    updateInfoAction = this.CreateRelatedTableUpdateInfoAction(patchDocument, DatabaseInformationType.Header, this.GetReferenceId, this.CreateParentTablePathAndValue(DatabaseInformationType.HeaderParty, entityId), this._sender);
                }

                if (updateInfoAction != null)
                {
                    updateInfoActions.Add(updateInfoAction);
                }
            }

            var patchDocuments = patchRequest.FindAll(item => !item.HasPath(this._receiver) && !item.HasPath(this._sender));
            updateInfoActions.Add(this.CreateParentTableUpdateInfoAction(patchDocuments, DatabaseInformationType.Header));
            return new UpdateInfo
            {
                Actions = updateInfoActions
            };
        }

        private void CreatePartyInfoActions(PatchRequest patchRequest, string entityId, List<UpdateInfoAction> updateInfoActions)
        {
            var patchDocuments = patchRequest.FindAll(item => item.HasPath(this._sender));
            if (patchDocuments.Any())
            {
                var groupByOperation = patchDocuments.GroupBy(item => item.Op);
                foreach (var group in groupByOperation)
                {
                    switch (group.Key)
                    {
                        case "add":
                            updateInfoActions.AddRange(this.GetAddActions(group.ToList(), "en",entityId,this._sender));
                            break;
                        case "remove":

                            break;
                        case "replace":
                            break;
                    }
                }
            }
        }

        private List<UpdateInfoAction> GetAddActions(List<PatchDocument> documents, string language, string entityId, string partyType)
        {
            return null;
            var localisedStringAction = new UpdateInfoAction()
            {
                DataInformationType = DatabaseInformationType.HeaderLocalisedString,
                SqlStatementType = SqlStatementType.Insert,
                PathAndValues = new Dictionary<string, object>()
            };

            var localisedStringActionForParty = new UpdateInfoAction()
            {
                DataInformationType = DatabaseInformationType.HeaderLocalisedString,
                SqlStatementType = SqlStatementType.Insert,
                PathAndValues = new Dictionary<string, object>()
            };
            var relevantDocument = documents.Find(item => item.HasPath("contact/name"));
            if (relevantDocument != null)
            {
                localisedStringAction.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.Language.ModelName,language);
                localisedStringAction.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.Text.ModelName, relevantDocument.Value);
                localisedStringAction.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.Type.ModelName, "Name");
            }

            relevantDocument = documents.Find(item => item.HasPath("contact/department"));
            if (relevantDocument != null)
            {
                localisedStringAction.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.Language.ModelName, language);
                localisedStringAction.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.Text.ModelName, relevantDocument.Value);
                localisedStringAction.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.Type.ModelName, "Department");
            }

            relevantDocument = documents.Find(item => item.HasPath("contact/role"));
            if (relevantDocument != null)
            {
                localisedStringAction.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.Language.ModelName, language);
                localisedStringAction.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.Text.ModelName, relevantDocument.Value);
                localisedStringAction.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.Type.ModelName, "Role");
            }

            relevantDocument = documents.Find(item => item.HasPath("name") && !item.HasPath("contact/name"));
            if (relevantDocument != null)
            {
                localisedStringActionForParty.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.Language.ModelName, language);
                localisedStringActionForParty.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.Text.ModelName, relevantDocument.Value);
                localisedStringActionForParty.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.Type.ModelName, "Name");
            }
            var contactDetailsAction = new UpdateInfoAction()
            {
                DataInformationType = DatabaseInformationType.ContactDetails,
                SqlStatementType = SqlStatementType.Insert,
                PathAndValues = new Dictionary<string, object>()
            };

            relevantDocument = documents.Find(item => item.HasPath("email"));
            if (relevantDocument != null)
            {
                contactDetailsAction.PathAndValues.Add(ContactDetailsDataTableInfo.Type.ModelName, "Email");
                contactDetailsAction.PathAndValues.Add(ContactDetailsDataTableInfo.Value.ModelName, relevantDocument.Value);
            }

            var partyAction = new UpdateInfoAction()
            {
                DataInformationType = DatabaseInformationType.HeaderParty,
                SqlStatementType = SqlStatementType.Insert,
                PathAndValues = new Dictionary<string, object>()
            };

            relevantDocument = documents.Find(item => item.HasPath("id"));
            if (relevantDocument != null)
            {
                partyAction.PathAndValues.Add(PartyDataTableInfo.Id.ModelName, relevantDocument.Value);
            }
            partyAction.PathAndValues.Add(PartyDataTableInfo.HeaderId.ModelName, entityId);
            partyAction.PathAndValues.Add(PartyDataTableInfo.Type.ModelName, partyType);
        }
    }
}