using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.MappingStore.Engine;

namespace Estat.Sri.Mapping.MappingStore.Factory
{

    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;

    public class CopyFactory : ICopyFactory {
        private readonly IEntityPersistenceManager _entityPersistenceManager;
        private readonly IEntityRetrieverManager _entityRetrieverManager;

        private readonly DatabaseManager _databaseManager;
        private readonly ICommonSdmxObjectRetrievalFactory commonSdmxObjectRetrievalFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public CopyFactory(IEntityPersistenceManager entityPersistenceManager, IEntityRetrieverManager entityRetrieverManager, DatabaseManager databaseManager,ICommonSdmxObjectRetrievalFactory commonSdmxObjectRetrievalFactory)
        {
            this._entityPersistenceManager = entityPersistenceManager;
            this._entityRetrieverManager = entityRetrieverManager;
            _databaseManager = databaseManager;
            this.commonSdmxObjectRetrievalFactory = commonSdmxObjectRetrievalFactory;
        }

        public ICopyEngine GetEngine(string sourceStoreId)
        {
            var database = _databaseManager.GetDatabase(sourceStoreId);
            if (database != null)
            {
                return new CopyEngine(
                    sourceStoreId,
                    this._entityPersistenceManager,
                    this._entityRetrieverManager,
                    database,
                    this._databaseManager,commonSdmxObjectRetrievalFactory);
            }

            return null;
        }
    }
}