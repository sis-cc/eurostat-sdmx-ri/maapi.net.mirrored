// -----------------------------------------------------------------------
// <copyright file="BaseRetrieverFactory.cs" company="EUROSTAT">
//   Date Created : 2017-04-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Factory
{
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Engine;
    using Estat.Sri.Mapping.MappingStore.Engine.Authorization;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// Class BaseRetrieverFactory.
    /// </summary>
    /// <typeparam name="TActualEntity">The type of the t entity.</typeparam>
    public abstract class BaseRetrieverFactory<TActualEntity> where TActualEntity : class, IEntity
    {
        /// <summary>
        /// The database manager
        /// </summary>
        private readonly DatabaseManager _databaseManager;

        /// <summary>
        /// The enity type
        /// </summary>
        private readonly EntityType _enityType;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRetrieverFactory{TActualEntity}" /> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        protected BaseRetrieverFactory(DatabaseManager databaseManager)
        {
            this._databaseManager = databaseManager;
            var entityBuilder = new EntityTypeBuilder();
            _enityType = entityBuilder.Build(typeof(TActualEntity));
        }

        /// <summary>
        /// Gets the retriever engine.
        /// </summary>
        /// <typeparam name="TEntity">The type of the t requested entity.</typeparam>
        /// <param name="mappingAssistantStoreId">The mapping assistant store identifier.</param>
        /// <returns>The <see cref="IEntityRetrieverEngine{TRequestedEntity}"/>.</returns>
        public IEntityRetrieverEngine<TEntity> GetRetrieverEngine<TEntity>(string mappingAssistantStoreId) where TEntity : IEntity
        {
            var database = this._databaseManager.GetDatabase(mappingAssistantStoreId);
            if (database == null)
            {
                return null;
            }

            if (typeof(TEntity) == typeof(TActualEntity))
            {
                var entityRetrieverEngine = (IEntityRetrieverEngine<TEntity>)this.GetEngine(database, mappingAssistantStoreId);
                var defaultsEntityRetrieverEngine = new DefaultsEntityRetrieverEngine<TEntity>(entityRetrieverEngine, mappingAssistantStoreId);

                return defaultsEntityRetrieverEngine;
            }

            return null;
        }

        /// <summary>
        /// Gets the retriever engine.
        /// </summary>
        /// <param name="mappingAssistantStoreId">The mapping assistant store identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>The <see cref="IEntityRetrieverEngine{IEntity}"/></returns>
        public IEntityRetrieverEngine<IEntity> GetRetrieverEngine(string mappingAssistantStoreId, EntityType entityType)
        {
            if (this._enityType == entityType)
            {
                return GetRetrieverEngine<TActualEntity>(mappingAssistantStoreId);
            }

            return null;
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>The <see cref="IEntityRetrieverEngine{TRequestedEntity}"/>.</returns>
        protected abstract IEntityRetrieverEngine<TActualEntity> GetEngine(Database database, string storeId);
    }
}