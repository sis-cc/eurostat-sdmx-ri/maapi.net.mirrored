// -----------------------------------------------------------------------
// <copyright file="RegistryInfoFactory.cs" company="EUROSTAT">
//   Date Created : 2017-06-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public class RegistryInfoFactory : UpdateInfoFactory
    {
        public RegistryInfoFactory(Database database)
            : base(database)
        {
        }

        public override UpdateInfo Create(PatchRequest patchRequest, string entityId)
        {
            // Normalize values... 
            foreach(var doc in patchRequest)
            {
                // true values to integer 1 for Oracle, false to 0
                if (doc.Value is bool b)
                {
                    doc.Value = b ? 1 : 0;
                }
            }

            var actions = new List<UpdateInfoAction>
            {
                this.CreateParentTableUpdateInfoAction(patchRequest, DatabaseInformationType.Registry)
            };
            return new UpdateInfo
            {
                Actions = actions
            };
        }
    }
}