// -----------------------------------------------------------------------
// <copyright file="UserInfoFactory.cs" company="EUROSTAT">
//   Date Created : 2017-06-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public class UserInfoFactory : UpdateInfoFactory
    {
        private readonly string _categoryId = CategoryTableInformations.CategoryId.ModelName;
        private readonly string _dataflowId = DataflowTableInformations.DataflowId.ModelName;
        private readonly string _permissions = "permissions";

        public UserInfoFactory(Database database)
            : base(database)
        {
        }

        public override UpdateInfo Create(PatchRequest patchRequest, string entityId)
        {
            var updateInfoActions = new List<UpdateInfoAction>();

            foreach (var patchDocument in patchRequest)
            {
                UpdateInfoAction updateInfoAction = null;
                if (patchDocument.HasPath(this._permissions))
                {
                    if (patchDocument.Op != "replace")
                    {
                        var urn = patchDocument.Path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Last();
                        updateInfoAction = this.UpdateInfoActionForPermissions(entityId, urn, patchDocument, true);
                    }
                    else
                    {
                        var urn = ((JArray)patchDocument.Value).First().ToString();
                        updateInfoAction = this.UpdateInfoActionForPermissions(entityId, urn, patchDocument, false);
                    }
                }


                if (updateInfoAction != null)
                {
                    updateInfoActions.Add(updateInfoAction);
                }
            }

            var pathAndValues = new Dictionary<string, object>();
            foreach (var patchDocument in patchRequest)
            {
                pathAndValues.AddAll(this.ExtractFrom(patchDocument));
            }
            if (pathAndValues.Any())
            {
                updateInfoActions.Add(new UpdateInfoAction
                {
                    DataInformationType = DatabaseInformationType.User,
                    SqlStatementType = SqlStatementType.Update,
                    PathAndValues = pathAndValues
                });
            }
            

            return new UpdateInfo
            {
                Actions = updateInfoActions
            };
        }

        private IEnumerable<KeyValuePair<string, object>> ExtractFrom(PatchDocument patchDocument)
        {
            var userConverterEngine = new UserConverterEngine();
            var defaultKey = patchDocument.Path.TrimStart('/');
            var values = new Dictionary<string, object>();
            if (patchDocument.HasPath("name"))
            {
                values.Add(defaultKey, patchDocument.Value);
            }
            if (patchDocument.HasPath("password"))
            {
                if (patchDocument.Op == "remove")
                {
                    values.Add(defaultKey, null);
                }
                var passwordDetails = userConverterEngine.From(patchDocument.Value.ToString());
                values.Add(defaultKey, passwordDetails.Password);
                values.Add("Salt", passwordDetails.Salt);
                values.Add("Algorithm", passwordDetails.Algorithm);
            }

            if (patchDocument.HasPath("roles"))
            {
                var array = (JArray) patchDocument.Value;
                var userType = userConverterEngine.GetUserType(array.ToObject<List<string>>());
                values.Add(nameof(UserEntity.Roles), userType);
            }

            return values;
        }

        private SdmxStructureEnumType GetPermissionType(string urn)
        {
            var structureReferenceImpl = new StructureReferenceImpl(urn);
            return structureReferenceImpl.MaintainableStructureEnumType.EnumType;
        }

        private UpdateInfoAction UpdateInfoActionForPermissions(string entityId, string urn, PatchDocument patchDocument,bool fromPath)
        {
            UpdateInfoAction updateInfoAction = null;
            var sdmxStructureEnumType = this.GetPermissionType(urn);
            if (sdmxStructureEnumType == SdmxStructureEnumType.Dataflow)
            {
                updateInfoAction = this.CreateRelatedTableUpdateInfoAction(patchDocument, DatabaseInformationType.DataflowUser, this.GetReferenceIdFromValue, this.CreateParentTablePathAndValue(DatabaseInformationType.DataflowUser, entityId), this._dataflowId, fromPath);
            }
            if (sdmxStructureEnumType == SdmxStructureEnumType.CategoryScheme)
            {
                updateInfoAction = this.CreateRelatedTableUpdateInfoAction(patchDocument, DatabaseInformationType.CategoryUser, this.GetReferenceIdFromValue, this.CreateParentTablePathAndValue(DatabaseInformationType.CategoryUser, entityId), this._categoryId, fromPath);
            }
            return updateInfoAction;
        }
    }
}