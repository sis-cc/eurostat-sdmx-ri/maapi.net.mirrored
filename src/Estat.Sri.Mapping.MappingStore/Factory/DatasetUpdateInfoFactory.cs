// -----------------------------------------------------------------------
// <copyright file="DatasetUpdateInfoFactory.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public class DatasetUpdateInfoFactory : UpdateInfoFactory
    {
        private readonly string _permissions = "permissions";
        private readonly string _categoryId = CategoryTableInformations.CategoryId.ModelName;
        private readonly string _dataflowId = DataflowTableInformations.DataflowId.ModelName;

        public DatasetUpdateInfoFactory(Database database)
            : base(database)
        {
        }

        public override UpdateInfo Create(PatchRequest patchRequest, string entityId)
        {
            var updateInfoActions = new List<UpdateInfoAction>();
/*
            foreach (var patchDocument in patchRequest)
            {
                UpdateInfoAction updateInfoAction = null;
                if (patchDocument.HasPath(this._permissions))
                {
                    if (patchDocument.Op != "replace")
                    {
                        var urn = patchDocument.Path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Last();
                   //     updateInfoAction = this.UpdateInfoActionForPermissions(entityId, urn, patchDocument, true);
                        updateInfoActions.Add(updateInfoAction);
                    }
                    else
                    {
                        var permissionObject = JsonConvert.DeserializeObject<JObject>(patchDocument.Value.ToString());
                        foreach(JProperty permission in permissionObject.Children())
                        {
                            var urn = permission.Name;
                            updateInfoAction = this.UpdateInfoActionForPermissions(entityId, urn, patchDocument, false);
                            updateInfoActions.Add(updateInfoAction);
                        }
                        
                        var updateTypes =updateInfoActions.Select(x => x.DataInformationType).Distinct().ToList();
                        
                        foreach(var updateType in updateTypes)
                        {
                           var pathAndValues = new Dictionary<string, object>();
                           pathAndValues.Add("EntityId", entityId);
                           var deleteInfo = new UpdateInfoAction
                           {
                               DataInformationType = updateType,
                               SqlStatementType = SqlStatementType.Delete,
                               PathAndValues = pathAndValues
                           };
                           updateInfoActions.Insert(0, deleteInfo);
                        }
                    }
                }
            }
*/
            var patchDocuments = patchRequest.FindAll(item => !item.HasPath(this._permissions));
            var otherColumnsInfoAction = this.CreateParentTableUpdateInfoAction(patchDocuments, DatabaseInformationType.DataSet);
            if (otherColumnsInfoAction != null)
            { 
                updateInfoActions.Add(otherColumnsInfoAction); 
            }

            return new UpdateInfo {Actions = updateInfoActions};
        }

        private UpdateInfoAction UpdateInfoActionForPermissions(string entityId, string urn, PatchDocument patchDocument, bool fromPath)
        {
            UpdateInfoAction updateInfoAction = null;
            var sdmxStructureEnumType = this.GetPermissionType(urn);
            var singleUrnPatchDocument = new PatchDocument("add", patchDocument.Path, urn);
            if (sdmxStructureEnumType == SdmxStructureEnumType.Dataflow)
            {
                //updateInfoAction = this.CreateRelatedTableUpdateInfoAction(singleUrnPatchDocument, DatabaseInformationType.DataflowDataSet, this.GetReferenceIdFromValue, this.CreateParentTablePathAndValue(DatabaseInformationType.DataflowDataSet, entityId), this._dataflowId, fromPath);
            }
            if (sdmxStructureEnumType == SdmxStructureEnumType.CategoryScheme)
            {
                //updateInfoAction = this.CreateRelatedTableUpdateInfoAction(singleUrnPatchDocument, DatabaseInformationType.CategoryDataSet, this.GetReferenceIdFromValue, this.CreateParentTablePathAndValue(DatabaseInformationType.CategoryDataSet, entityId), this._categoryId, fromPath);
            }
            return updateInfoAction;
        }

        private SdmxStructureEnumType GetPermissionType(string urn)
        {
            var structureReferenceImpl = new StructureReferenceImpl(urn);
            return structureReferenceImpl.MaintainableStructureEnumType.EnumType;   
        }
    }
}