// -----------------------------------------------------------------------
// <copyright file="TranscodingRuleRetrieverFactory.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Engine;
    using Estat.Sri.Mapping.MappingStore.Helpers;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    public class TranscodingRuleRetrieverFactory : BaseRetrieverFactory<TranscodingRuleEntity>, IEntityRetrieverFactory
    {
        private readonly IMappingStoreManager _mappingStoreManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="TranscodingRuleRetrieverFactory" /> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        public TranscodingRuleRetrieverFactory(DatabaseManager databaseManager, IMappingStoreManager mappingStoreManager)
            : base(databaseManager)
        {
            this._mappingStoreManager = mappingStoreManager;
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>The <see cref="IEntityRetrieverEngine{TRequestedEntity}"/>.</returns>
        protected override IEntityRetrieverEngine<TranscodingRuleEntity> GetEngine(Database database, string storeId)
        {
            if (MappingStoreVersionHelper.IsVersion53(_mappingStoreManager, storeId))
            {
                return new TranscodingRuleRetrieverEngine53(database);
            }
            return new TranscodingRuleRetrieverEngine(database);
        }
    }
}