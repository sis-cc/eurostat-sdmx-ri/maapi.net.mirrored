// -----------------------------------------------------------------------
// <copyright file="DdbConnectionSettingsUpdateInfoFactory.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using System.Linq;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public class DdbConnectionSettingsUpdateInfoFactory : UpdateInfoFactory
    {

        private readonly string _permissions = "permissions";
        private readonly string _categoryId = CategoryTableInformations.CategoryId.ModelName;

        private readonly string _dataflowId = DataflowTableInformations.DataflowId.ModelName;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public DdbConnectionSettingsUpdateInfoFactory(Database database)
            : base(database)
        {
        }

        public override UpdateInfo Create(PatchRequest patchRequest, string entityId)
        {
            var updateInfoActions = new List<UpdateInfoAction>();
            foreach (var patchDocument in patchRequest)
            {
                UpdateInfoAction updateInfoAction = null;
                if (patchDocument.HasPath(this._permissions))
                {
                    var urn = patchDocument.Path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Last();
                    var sdmxStructureEnumType = this.GetPermissionType(urn);
                    if (sdmxStructureEnumType == SdmxStructureEnumType.Dataflow)
                    {
                        updateInfoAction = this.CreateRelatedTableUpdateInfoAction(patchDocument, DatabaseInformationType.DataflowDbConnection, this.GetReferenceId, this.CreateParentTablePathAndValue(DatabaseInformationType.DdbConnectionSettings, entityId), this._dataflowId, true);
                    }
                    if (sdmxStructureEnumType == SdmxStructureEnumType.CategoryScheme)
                    {
                        updateInfoAction = this.CreateRelatedTableUpdateInfoAction(patchDocument, DatabaseInformationType.CategoryDbConnection, this.GetReferenceId, this.CreateParentTablePathAndValue(DatabaseInformationType.DdbConnectionSettings, entityId), this._categoryId, true);
                    }
                }

                if (updateInfoAction != null)
                {
                    updateInfoActions.Add(updateInfoAction);
                }
            }

            var patchDocuments = patchRequest.FindAll(item => !item.HasPath(this._permissions));
            updateInfoActions.Add(this.CreateParentTableUpdateInfoAction(patchDocuments, DatabaseInformationType.DdbConnectionSettings));
            return new UpdateInfo
            {
                Actions = updateInfoActions
            };
        }


        private SdmxStructureEnumType GetPermissionType(string urn)
        {
            var structureReferenceImpl = new StructureReferenceImpl(urn);
            return structureReferenceImpl.MaintainableStructureEnumType.EnumType;
        }
    }
}