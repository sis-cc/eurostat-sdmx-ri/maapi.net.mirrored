// -----------------------------------------------------------------------
// <copyright file="DataflowUpgradeFactory.cs" company="EUROSTAT">
//   Date Created : 2017-07-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Factory.Cloning
{
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Engine.Cloning;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;

    /// <summary>
    /// The dataflow upgrade factory.
    /// </summary>
    public class DataflowUpgradeFactory : IDataflowUpgradeFactory
    {
        /// <summary>
        /// The database manager
        /// </summary>
        private readonly DatabaseManager _databaseManager;

        /// <summary>
        /// The persistence manager
        /// </summary>
        private readonly IEntityPersistenceManager _persistenceManager;

        /// <summary>
        /// The retriever manager
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;
        private readonly ICommonSdmxObjectRetrievalFactory commonSdmxObjectRetrievalfactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataflowUpgradeFactory"/> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        /// <param name="persistenceManager">The persistence manager.</param>
        /// <param name="retrieverManager">The retriever manager.</param>
        /// <param name="commonSdmxObjectRetrievalfactory"></param>
        public DataflowUpgradeFactory(DatabaseManager databaseManager, IEntityPersistenceManager persistenceManager, IEntityRetrieverManager retrieverManager, ICommonSdmxObjectRetrievalFactory commonSdmxObjectRetrievalfactory)
        {
            _databaseManager = databaseManager;
            _persistenceManager = persistenceManager;
            _retrieverManager = retrieverManager;
            this.commonSdmxObjectRetrievalfactory = commonSdmxObjectRetrievalfactory;
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="commonSdmxObjectRetrievalManager"></param>
        /// <returns>
        /// The <see cref="IDataflowUpgrade" />.
        /// </returns>
        public IDataflowUpgrade GetEngine(string storeId)
        {
            var database = _databaseManager.GetDatabase(storeId);
            if (database == null)
            {
                return null;
            }
            var entityCloneHelper = new EntityCloneHelper(_retrieverManager, _persistenceManager, storeId);
            var sdmxStoreCloneHelper = new SdmxStoreCloneHelper(database, commonSdmxObjectRetrievalfactory);
            return new DataflowUpgradeEngine(entityCloneHelper, sdmxStoreCloneHelper);
        }
    }
}