// -----------------------------------------------------------------------
// <copyright file="MappingInfoFactory.cs" company="EUROSTAT">
//   Date Created : 2017-03-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Factory
{
    public class MappingInfoFactory : UpdateInfoFactory
    {
        private readonly string _column = "datasetColumn";
        private readonly string _component = "component";

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public MappingInfoFactory(Database database)
            : base(database)
        {
        }

        public override UpdateInfo Create(PatchRequest patchRequest, string entityId)
        {
            var updateInfoActions = new List<UpdateInfoAction>();
            foreach (var patchDocument in patchRequest)
            {
                UpdateInfoAction updateInfoAction = null;
                if (patchDocument.HasPath(this._column))
                {
                    updateInfoAction = this.CreateRelatedTableUpdateInfoAction(patchDocument, DatabaseInformationType.ComponentMappingColumn, item => item, this.CreateParentTablePathAndValue(DatabaseInformationType.Mapping, entityId), this._column);
                }

                if (patchDocument.HasPath(this._component))
                {
                    updateInfoAction = this.CreateRelatedTableUpdateInfoAction(patchDocument, DatabaseInformationType.ComponentMappingComponent, item=>item, this.CreateParentTablePathAndValue(DatabaseInformationType.Mapping, entityId), this._component);
                }

                if (updateInfoAction != null)
                {
                    updateInfoActions.Add(updateInfoAction);
                }
            }

            var patchDocuments = patchRequest.FindAll(item => !item.HasPath(this._column) && !item.HasPath(this._component));
            updateInfoActions.Add(this.CreateParentTableUpdateInfoAction(patchDocuments, DatabaseInformationType.Mapping));
            return new UpdateInfo
            {
                Actions = updateInfoActions
            };
        }
    }
}