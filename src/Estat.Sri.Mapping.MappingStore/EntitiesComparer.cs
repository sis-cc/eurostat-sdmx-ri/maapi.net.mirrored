using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore
{
    public class EntitiesComparer : IEqualityComparer<ISimpleNameableEntity>
    {
        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <returns>
        /// true if the specified objects are equal; otherwise, false.
        /// </returns>
        /// <param name="x">The first object of type <paramref name="T"/> to compare.</param><param name="y">The second object of type <paramref name="T"/> to compare.</param>
        public bool Equals(ISimpleNameableEntity x, ISimpleNameableEntity y)
        {
            if (x == null || y == null)
            {
                return false;
            }
            return x.Name.Equals(y.Name);
        }

        /// <summary>
        /// Returns a hash code for the specified object.
        /// </summary>
        /// <returns>
        /// A hash code for the specified object.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> for which a hash code is to be returned.</param><exception cref="T:System.ArgumentNullException">The type of <paramref name="obj"/> is a reference type and <paramref name="obj"/> is null.</exception>
        public int GetHashCode(ISimpleNameableEntity obj)
        {
            if (obj?.Name == null)
            {
                return 0;
            }
            return obj.Name.GetHashCode();
        }
    }
}