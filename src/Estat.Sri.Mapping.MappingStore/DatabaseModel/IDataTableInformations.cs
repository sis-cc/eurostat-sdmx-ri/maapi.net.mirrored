// -----------------------------------------------------------------------
// <copyright file="IDataTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Mapping.MappingStore.Constant;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public interface IDataTableInformations
    {
        /// <summary>
        /// Gets the name of the table.
        /// </summary>
        /// <value>
        /// The name of the table.
        /// </value>
        string TableName { get; }

        /// <summary>
        /// Gets the child table informations.
        /// </summary>
        /// <value>
        /// The child table informations.
        /// </value>
        IEnumerable<DatabaseInformationType> ChildTableInformations { get; }
        /// <summary>
        /// Gets the mappings.
        /// </summary>
        /// <value>
        /// The mappings.
        /// </value>
        List<IDataColumnInformation> Mappings { get; }

        /// <summary>
        /// Gets the column.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        string GetColumn(string key);
        /// <summary>
        /// Gets the name of the primary key json property.
        /// </summary>
        /// <returns></returns>
        string GetPrimaryKeyModelName();
        /// <summary>
        /// Gets the proc parameter.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        string GetProcParameter(string key);

        /// <summary>
        /// Gets the base table if any (e.g. ENTITY_BASE); else null
        /// </summary>
        string BaseTable { get; }
    }
}