// -----------------------------------------------------------------------
// <copyright file="HeaderDataTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class HeaderDataTableInformations : EntityBaseTableInformations
    {
        public static readonly IDataColumnInformation DatasetAgencyId = new DataColumnInformation
        {
            ColumnName = "DATASET_AGENCY",
            ModelName = nameof(HeaderTemplate.DataSetAgencyId)
        };

        public static readonly IDataColumnInformation Test = new DataColumnInformation
        {
            ColumnName = "TEST",
            ModelName = nameof(HeaderTemplate.Test)
        };

        public static readonly IDataColumnInformation HeaderId = new DataColumnInformation
        {
            ColumnName = "ENTITY_ID",
            ModelName = nameof(HeaderTemplate.EntityId),
            IsIdentityColumn = true,
            IsPrimaryKey = true
        };

        [Obsolete("Field doesn't longer exist")]
        public static readonly IDataColumnInformation DataflowId = new DataColumnInformation
        {
            ColumnName = "DF_ID",
            ModelName = nameof(HeaderEntity.DataflowId),
        };

        public HeaderDataTableInformations()
        {
            this.mappings.AddRange(new[] {HeaderId, DataflowId, Test, DatasetAgencyId});
        }

        public override string TableName { get; } = "N_HEADER";
    }
}