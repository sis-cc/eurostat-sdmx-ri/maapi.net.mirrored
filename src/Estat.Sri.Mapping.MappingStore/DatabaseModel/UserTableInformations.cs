using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class UserTableInformations : DataTableInformations
    {
        public static readonly IDataColumnInformation Id = new DataColumnInformation
        {
            ColumnName = "ID",
            ModelName = nameof(UserEntity.EntityId),
            IsPrimaryKey = true,
            IsIdentityColumn = true
        };

        public static readonly IDataColumnInformation Username = new DataColumnInformation
        {
            ColumnName = "USERMASK",
            ModelName = nameof(UserEntity.UserName)
        };

        public static readonly IDataColumnInformation IsGroup = new DataColumnInformation
        {
            ColumnName = "ISGROUP",
            ModelName = nameof(UserEntity.IsGroup)
        };

        public static readonly IDataColumnInformation Dataspace = new DataColumnInformation
        {
            ColumnName = "DATASPACE",
            ModelName = nameof(UserEntity.Dataspace)
        };

        public static readonly IDataColumnInformation EditedBy = new DataColumnInformation
        {
            ColumnName = "EDITEDBY",
            ModelName = nameof(UserEntity.EditedBy)
        };

        public static readonly IDataColumnInformation EditDate = new DataColumnInformation
        {
            ColumnName = "EDITDATE",
            ModelName = nameof(UserEntity.EditDate)
        };

        public UserTableInformations()
        {
            this.mappings.AddRange(new[] { Id, Username, IsGroup, Dataspace, EditedBy, EditDate, });
        }

        public override string TableName { get; } = "AUTHORIZATIONRULES";
    }
}