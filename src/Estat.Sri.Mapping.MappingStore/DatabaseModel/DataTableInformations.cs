// -----------------------------------------------------------------------
// <copyright file="DataTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.MappingStore.Constant;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public abstract class DataTableInformations : IDataTableInformations
    {
        protected readonly List<IDataColumnInformation> mappings = new List<IDataColumnInformation>();

        public List<IDataColumnInformation> Mappings => this.mappings;

        public virtual string TableName { get; }

        public string GetColumn(string key)
        {
            var column = this.mappings.Find(x => x.ModelName.Equals(key, StringComparison.OrdinalIgnoreCase));
            if(column == null)
            {
                throw new Exception($"could not find a mapping for key {key}");
            }
            return column.ColumnName;
        }

        public string GetPrimaryKeyModelName()
        {
            var tuple = this.mappings.Find(x => x.IsPrimaryKey);
            if (tuple == null)
            {
                throw new NotSupportedException();
            }
            return tuple.ModelName;
        }

        public virtual string GetProcParameter(string key)
        {
            var dataColumnInformation = this.mappings.Find(x => x.ModelName == key);
            if (dataColumnInformation == null)
            {
                // TODO a better approach is needed
                return null;
                // throw new ArgumentException($"Key {key} not found in mapping");
            }
            var nameForParameter = string.IsNullOrWhiteSpace(dataColumnInformation.ParmeterName)? dataColumnInformation.ColumnName.ToLower() : dataColumnInformation.ParmeterName.ToLower();
            return "p_" + nameForParameter;
        }

        public virtual IEnumerable<DatabaseInformationType> ChildTableInformations => null;

        public virtual string BaseTable { get; }
    }
}