// -----------------------------------------------------------------------
// <copyright file="ContactDetailsDataTableInfo.cs" company="EUROSTAT">
//   Date Created : 2017-04-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class ContactDetailsDataTableInfo : DataTableInformations
    {
        public static readonly IDataColumnInformation ContactDetailsId = new DataColumnInformation
        {
            ColumnName = "CD_ID",
            ModelName = "CONTACTDETAILSID",
            IsIdentityColumn = true,
            IsPrimaryKey = true
        };

        public static readonly IDataColumnInformation ContactId = new DataColumnInformation
        {
            ColumnName = "CONTACT_ID",
            ModelName = "CONTACTID"
        };

        public static readonly IDataColumnInformation Type = new DataColumnInformation
        {
            ColumnName = "TYPE",
            ModelName = "TYPE"
        };

        public static readonly IDataColumnInformation Value = new DataColumnInformation
        {
            ColumnName = "VALUE",
            ModelName = "VALUE"
        };

        public ContactDetailsDataTableInfo()
        {
            this.mappings.AddRange(new[] {ContactDetailsId, Value, ContactId, Type});
        }

        public override string TableName { get; } = "CONTACT_DETAIL";
    }
}