using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class TemplateMappingTableInformations : DataTableInformations
    {
        private static readonly IDataColumnInformation _templateId = new DataColumnInformation
        {
            ColumnName = "ENTITY_ID",
            ModelName = nameof(TemplateMapping.EntityId),
            IsPrimaryKey = true,
            IsIdentityColumn = true
        };


        private readonly IDataColumnInformation _name = new DataColumnInformation
        {
            ColumnName = "OBJECT_ID",
            ModelName = nameof(TemplateMapping.Name)
        };

        private readonly IDataColumnInformation _description = new DataColumnInformation
        {
            ColumnName = "DESCRIPTION",
            ModelName = nameof(TemplateMapping.Description)
        };

        private readonly IDataColumnInformation _columnName = new DataColumnInformation
        {
            ColumnName = "COLUMN_NAME",
            ModelName = nameof(TemplateMapping.ColumnName)
        };

        private readonly IDataColumnInformation _columnDescription = new DataColumnInformation
        {
            ColumnName = "COL_DESC",
            ModelName = nameof(TemplateMapping.ColumnDescription)
        };

        private readonly IDataColumnInformation _componentId = new DataColumnInformation
        {
            ColumnName = "COMPONENT_ID",
            ModelName = nameof(TemplateMapping.ComponentId)
        };

        private readonly IDataColumnInformation _componentType = new DataColumnInformation
        {
            ColumnName = "COMPONENT_TYPE",
            ModelName = nameof(TemplateMapping.ComponentType)
        };

        private readonly IDataColumnInformation _connectionId = new DataColumnInformation
        {
            ColumnName = "CON_ID",
            ModelName = nameof(TemplateMapping.ConceptId)
        };

        private readonly IDataColumnInformation _itemSchemeId = new DataColumnInformation
        {
            ColumnName = "ITEM_SCHEME_ID",
            ModelName = nameof(TemplateMapping.ItemSchemeId)
        };


        public TemplateMappingTableInformations()
        {
            this.mappings.AddRange(new[] { _templateId, this._columnName, this._connectionId, this._componentType, this._columnDescription, this._componentId, this._itemSchemeId,this._description,this._name});
        }

        public override string TableName { get; } = "N_TEMPLATE_MAPPING";
    }
}