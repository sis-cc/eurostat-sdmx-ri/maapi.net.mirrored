namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class DataflowUserTableInformations : DataTableInformations
    {
        public DataflowUserTableInformations()
        {
            var userId = UserTableInformations.Id;
            userId.IsIdentityColumn = false;
            var dataflowId = DataflowTableInformations.DataflowId;
            dataflowId.IsIdentityColumn = false;
            this.mappings.AddRange(new[] { userId, dataflowId });
        }

        public override string TableName { get; } = "DATAFLOW_USER";
    }
}