﻿// -----------------------------------------------------------------------
// <copyright file="TimeTranscodingDataTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class TimeTranscodingDataTableInformations : DataTableInformations
    {
        private readonly IDataColumnInformation _dateColId = new DataColumnInformation
        {
            ColumnName = "DATE_COL_ID",
            ModelName = "DATE_COL_ID"
        };

        private readonly IDataColumnInformation _expression = new DataColumnInformation
        {
            ColumnName = "EXPRESSION",
            ModelName = "EXPRESSION"
        };

        private readonly IDataColumnInformation _frequency = new DataColumnInformation
        {
            ColumnName = "FREQ",
            ModelName = nameof(TimeTranscodingEntity.Frequency)
        };

        private readonly IDataColumnInformation _periodColId = new DataColumnInformation
        {
            ColumnName = "PERIOD_COL_ID",
            ModelName = "PERIOD_COL_ID"
        };

        private readonly IDataColumnInformation _transcodingId = new DataColumnInformation
        {
            ColumnName = "TR_ID",
            IsPrimaryKey = true,
            ModelName = "transcodingId"
        };

        private readonly IDataColumnInformation _yearColId = new DataColumnInformation
        {
            ColumnName = "YEAR_COL_ID",
            ModelName = "YEAR_COL_ID"
        };

        public TimeTranscodingDataTableInformations()
        {
            this.mappings.AddRange(new[] {this._transcodingId, this._frequency, this._yearColId, this._periodColId, this._dateColId, this._expression});
        }

        public override string TableName { get; } = "TIME_TRANSCODING";
    }
}