// -----------------------------------------------------------------------
// <copyright file="DdbConnectionDataTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class DdbConnectionDataTableInformations : EntityBaseTableInformations
    {

        private readonly IDataColumnInformation _adoConnectionString = new DataColumnInformation
        {
            ColumnName = "ADO_CONNECTION_STRING",
            ModelName = nameof(DdbConnectionEntity.AdoConnString)
        };

        private readonly IDataColumnInformation _encrypted = new DataColumnInformation
        {
            ColumnName = "IS_ENCRYPTED",
            ModelName = nameof(DdbConnectionEntity.IsEncrypted)
        };

        private readonly IDataColumnInformation _connName = new DataColumnInformation
        {
            ColumnName = "NAME",
            ModelName = nameof(DdbConnectionEntity.Name)
        };

        private readonly IDataColumnInformation _dbType = new DataColumnInformation
        {
            ColumnName = "DB_TYPE",
            ModelName = nameof(DdbConnectionEntity.DbType)
        };

        private readonly IDataColumnInformation _jdbcConnectionString = new DataColumnInformation
        {
            ColumnName = "JDBC_CONNECTION_STRING",
            ModelName = nameof(DdbConnectionEntity.JdbcConnString)
        };

        private readonly IDataColumnInformation _password = new DataColumnInformation
        {
            ColumnName = "DB_PASSWORD",
            ModelName = nameof(DdbConnectionEntity.Password)
        };

        private readonly IDataColumnInformation _user = new DataColumnInformation
        {
            ColumnName = "DB_USER",
            ModelName = nameof(DdbConnectionEntity.DbUser)
        };

        private readonly IDataColumnInformation _properties = new DataColumnInformation
        {
            ColumnName = "PROPERTIES",
            ModelName = nameof(DdbConnectionEntity.Properties)
        };

        public DdbConnectionDataTableInformations()
        {
            this.mappings.AddRange(new[] { EntityId,Name,Description, this._dbType, this._connName, this._password,  this._adoConnectionString, this._jdbcConnectionString, this._user, this._properties, _encrypted });
        }

        public override string TableName { get; } = "N_DB_CONNECTION";

        public override IEnumerable<DatabaseInformationType> ChildTableInformations => new List<DatabaseInformationType> {DatabaseInformationType.DataflowDbConnection,DatabaseInformationType.CategoryDbConnection,DatabaseInformationType.DataSet};
    }
}