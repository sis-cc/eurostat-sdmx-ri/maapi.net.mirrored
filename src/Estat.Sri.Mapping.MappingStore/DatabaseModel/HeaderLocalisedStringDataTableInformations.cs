using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class HeaderLocalisedStringDataTableInformations : DataTableInformations
    {
        public static readonly IDataColumnInformation Type = new DataColumnInformation
        {
            ColumnName = "TYPE",
            ModelName = "TYPE"
        };

        public static readonly IDataColumnInformation Language = new DataColumnInformation
        {
            ColumnName = "LANGUAGE",
            ModelName = "LANGUAGE"
        };

        public static readonly IDataColumnInformation HlsId = new DataColumnInformation
        {
            ColumnName = "HLS_ID",
            ModelName = "LOCALISEDHEADERID",
            IsIdentityColumn = true,
            IsPrimaryKey = true
        };

        public static readonly IDataColumnInformation Text = new DataColumnInformation
        {
            ColumnName = "TEXT",
            ModelName = "TEXT",
        };


        public static readonly IDataColumnInformation HeaderId = new DataColumnInformation
        {
            ColumnName = "HEADER_ID",
            ModelName = "HeaderId",
        };

        public static readonly IDataColumnInformation PartyId = new DataColumnInformation
        {
            ColumnName = "PARTY_ID",
            ModelName = "PartyId",
        };

        public static readonly IDataColumnInformation ContactId = new DataColumnInformation
        {
            ColumnName = "CONTACT_ID",
            ModelName = "ContactId",
        };

        public HeaderLocalisedStringDataTableInformations()
        {
            this.mappings.AddRange(new[] { HlsId, Text, Language, Type,HeaderId,PartyId, ContactId });
        }

        public override string TableName { get; } = "HEADER_LOCALISED_STRING";
    }
}