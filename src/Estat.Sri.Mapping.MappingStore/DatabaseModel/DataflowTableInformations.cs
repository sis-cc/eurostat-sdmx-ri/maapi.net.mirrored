// -----------------------------------------------------------------------
// <copyright file="DataflowTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    /// <summary>
    /// DataflowTableInformations
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.MappingStore.DatabaseModel.DataTableInformations" />
    public class DataflowTableInformations : DataTableInformations
    {
        public static readonly IDataColumnInformation DataflowId = new DataColumnInformation
        {
            ColumnName = "DF_ID",
            ModelName = "dataflowId",
            IsPrimaryKey = true,
            IsIdentityColumn = true
        };

        public static readonly IDataColumnInformation MapSetId = new DataColumnInformation()
        {
            ColumnName = "MAP_SET_ID",
            ModelName = nameof(MappingSetEntity.EntityId)
        };

        public static readonly IDataColumnInformation DataSourceId = new DataColumnInformation()
        {
            ColumnName = "DATA_SOURCE_ID",
            ModelName = nameof(DataSourceEntity.EntityId)
        };

        public DataflowTableInformations()
        {
            this.mappings.AddRange(new[] {DataflowId, MapSetId,DataSourceId});
        }

        public override string TableName { get; } = "DATAFLOW";
    }
}