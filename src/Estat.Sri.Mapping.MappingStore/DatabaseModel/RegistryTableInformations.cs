// -----------------------------------------------------------------------
// <copyright file="RegistryTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-06-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    /// <summary>
    /// DataflowTableInformations
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.MappingStore.DatabaseModel.RegistryTableInformations" />
    public class RegistryTableInformations : DataTableInformations
    {
        public static readonly IDataColumnInformation Id = new DataColumnInformation
        {
            ColumnName = "ID",
            ModelName = nameof(RegistryEntity.EntityId),
            IsPrimaryKey = true,
            IsIdentityColumn = true
        };

        public static readonly IDataColumnInformation Url = new DataColumnInformation
        {
            ColumnName = "URL",
            ModelName = nameof(RegistryEntity.Url)
        };

        public static readonly IDataColumnInformation UserId = new DataColumnInformation
        {
            ColumnName = "USERNAME",
            ModelName = nameof(RegistryEntity.UserName)
        };

        public static readonly IDataColumnInformation Availability = new DataColumnInformation
        {
            ColumnName = "PASSWORD",
            ModelName = nameof(RegistryEntity.Password)
        };

        public static readonly IDataColumnInformation Description = new DataColumnInformation
        {
            ColumnName = "DESCRIPTION",
            ModelName = nameof(RegistryEntity.Description)
        };

        public static readonly IDataColumnInformation Technology = new DataColumnInformation
        {
            ColumnName = "TECHNOLOGY",
            ModelName = nameof(RegistryEntity.Technology)
        };

        public static readonly IDataColumnInformation IsPublic = new DataColumnInformation
        {
            ColumnName = "Is_Public",
            ModelName = nameof(RegistryEntity.IsPublic),
            ParmeterName = "is_public"
        };

        public static readonly IDataColumnInformation Upgrades = new DataColumnInformation
        {
            ColumnName = "Upgrades",
            ModelName = nameof(RegistryEntity.Upgrades)
        };

        public static readonly IDataColumnInformation Proxy = new DataColumnInformation
        {
            ColumnName = "Proxy",
            ModelName = nameof(RegistryEntity.Proxy)
        };
        public static readonly IDataColumnInformation ProxyAlias = new DataColumnInformation
        {
            ColumnName = "Proxy",
            ModelName = "useProxy"
        };

        public static readonly IDataColumnInformation Name = new DataColumnInformation
        {
            ColumnName = "Name",
            ModelName = nameof(RegistryEntity.Name)
        };

        public RegistryTableInformations()
        {
            this.mappings.AddRange(new[] {Id, Url,UserId, Availability,Description, Technology,IsPublic,Upgrades,Proxy, Name , ProxyAlias});
        }

        public override string TableName { get; } = "REGISTRY";
    }
}