// -----------------------------------------------------------------------
// <copyright file="TranscodingScriptTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class TranscodingScriptTableInformations : DataTableInformations
    {
        private readonly IDataColumnInformation _transcodingId = new DataColumnInformation
        {
            ColumnName = "TR_ID",
            ModelName = nameof(TranscodingScriptEntity.ParentId) ,
        };

        private readonly IDataColumnInformation _transcodingScriptId = new DataColumnInformation
        {
            ColumnName = "TR_SCRIPT_ID",
            ModelName = "transcodingId",
            IsPrimaryKey = true,
            IsIdentityColumn = true
        };

        private readonly IDataColumnInformation _scriptTitle = new DataColumnInformation
        {
            ColumnName = "SCRIPT_TITLE",
            ParmeterName = "SCRIPT_TITLE",
            ModelName = nameof(TranscodingScriptEntity.ScriptTile)
            
        };

        private readonly IDataColumnInformation _scriptContent = new DataColumnInformation
        {
            ColumnName = "SCRIPT_CONTENT",
            ParmeterName = "SCRIPT_CONTENT",
            ModelName = nameof(TranscodingScriptEntity.ScriptContent),
        };

        public TranscodingScriptTableInformations()
        {
            this.mappings.AddRange(new[] { this._transcodingId, this._transcodingScriptId, this._scriptTitle,this._scriptContent });
        }

        public override string TableName { get; } = "TRANSCODING_SCRIPT";
    }
}