// -----------------------------------------------------------------------
// <copyright file="TimePreFormattedTableInformation.cs" company="EUROSTAT">
//   Date Created : 2021-05-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Model;

    /// <summary>
    /// Db table information for the <see cref="TimePreFormattedEntity"/>.
    /// </summary>
    [Obsolete("Included in TimeDimensionMappingEntity")]
    public class TimePreFormattedTableInformation : DataTableInformations
    {
        private readonly IDataColumnInformation _mappingSetId = new DataColumnInformation
        {
            ColumnName = "MAP_SET_ID",
            ModelName = nameof(MATimePreFormattedEntity.MappingSetId),
            IsPrimaryKey = true
        };

        private readonly IDataColumnInformation _outputColumnId = new DataColumnInformation
        {
            ColumnName = "OUTPUT_COLUMN",
            ModelName = nameof(MATimePreFormattedEntity.OutputColumnId)
        };

        private readonly IDataColumnInformation _fromColumnId = new DataColumnInformation
        {
            ColumnName = "FROM_COLUMN",
            ModelName = nameof(MATimePreFormattedEntity.FromColumnId)
        };

        private readonly IDataColumnInformation _toColumnId = new DataColumnInformation
        {
            ColumnName = "TO_COLUMN",
            ModelName = nameof(MATimePreFormattedEntity.ToColumnId)
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="TimePreFormattedTableInformation"/> class.
        /// </summary>
        public TimePreFormattedTableInformation()
        {
            this.mappings.AddRange(new[] 
                { this._mappingSetId, this._outputColumnId, this._fromColumnId, this._toColumnId });
        }

        /// <summary>
        /// The db table name.
        /// </summary>
        public override string TableName { get; } = "TIME_PRE_FORMATED";
    }
}
