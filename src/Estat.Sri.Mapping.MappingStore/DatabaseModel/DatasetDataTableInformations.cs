// -----------------------------------------------------------------------
// <copyright file="DatasetDataTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-02-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class DatasetDataTableInformations : EntityBaseTableInformations
    {
        private readonly IDataColumnInformation _editorType = new DataColumnInformation
        {
            ColumnName = "EDITOR_TYPE",
            ModelName = nameof(DatasetEntity.EditorType)
        };

        private readonly IDataColumnInformation _query = new DataColumnInformation
        {
            ColumnName = "SQL_QUERY",
            ModelName = nameof(DatasetEntity.Query)
        };

        private readonly IDataColumnInformation _orderByClause = new DataColumnInformation
        {
            ColumnName = "ORDER_BY_CLAUSE",
            ModelName = nameof(DatasetEntity.OrderByClause)
        };

        private readonly IDataColumnInformation _xmlQuery = new DataColumnInformation
        {
            ColumnName = "EDITOR_QUERY",
            ModelName = nameof(DatasetEntity.JSONQuery)
        };

        private static readonly IDataColumnInformation _connectionId = new DataColumnInformation
        {
            ColumnName = "CONNECTION_ID",
            ModelName = nameof(DatasetEntity.ParentId),
        };

        public DatasetDataTableInformations()
        {
            this.mappings.AddRange(new[] { EntityId, Name, Description, this._query, this._orderByClause, _connectionId, this._xmlQuery, this._editorType});
        }

        public override string TableName { get; } = "N_DATASET";
    }
}