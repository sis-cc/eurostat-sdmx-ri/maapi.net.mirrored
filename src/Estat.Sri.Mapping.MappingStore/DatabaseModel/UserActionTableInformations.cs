﻿using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class UserActionTableInformations : DataTableInformations
    {
        private static readonly IDataColumnInformation _id = new DataColumnInformation
        {
            ColumnName = "UA_ID",
            ModelName = nameof(UserActionEntity.Id),
            IsPrimaryKey = true,
            IsIdentityColumn = true
        };

        private readonly IDataColumnInformation _actionWhen = new DataColumnInformation
        {
            ColumnName = "ACTION_WHEN",
            ModelName = nameof(UserActionEntity.ActionWhen)
        };

        private readonly IDataColumnInformation _operationType = new DataColumnInformation
        {
            ColumnName = "OPERATION_TYPE",
            ModelName = nameof(UserActionEntity.OperationType)
        };

        private readonly IDataColumnInformation _userName = new DataColumnInformation
        {
            ColumnName = "USERNAME",
            ModelName = nameof(UserActionEntity.UserName)
        };

        private readonly IDataColumnInformation _entityType = new DataColumnInformation
        {
            ColumnName = "ENTITY_TYPE",
            ModelName = nameof(UserActionEntity.EntityType)
        };

        private readonly IDataColumnInformation _entityName = new DataColumnInformation
        {
            ColumnName = "ENTITY_NAME",
            ModelName = nameof(UserActionEntity.EntityName)
        };

        private readonly IDataColumnInformation _entityId = new DataColumnInformation
        {
            ColumnName = "ENTITY_ID",
            ModelName = nameof(UserActionEntity.EntityId)
        };


        public UserActionTableInformations()
        {
            this.mappings.AddRange(new[] { _id, this._actionWhen, this._entityName, this._entityType, this._operationType, this._userName, this._entityId,   });
        }

        public override string TableName { get; } = "USER_ACTION";
    }
}