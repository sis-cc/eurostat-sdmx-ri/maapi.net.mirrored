﻿// -----------------------------------------------------------------------
// <copyright file="ComponentMappingComponentDataTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class ComponentMappingComponentDataTableInformations : DataTableInformations
    {
        public static readonly IDataColumnInformation ComponentId = new DataColumnInformation
        {
            ColumnName = "COMP_ID",
            ModelName = "component"
        };

        private readonly IDataColumnInformation _mappingId = new DataColumnInformation
        {
            ColumnName = "MAP_ID",
            ModelName = nameof(ComponentMappingEntity.EntityId),
            IsPrimaryKey = true
        };

        public ComponentMappingComponentDataTableInformations()
        {
            this.mappings.AddRange(new[] {this._mappingId, ComponentId});
        }

        public override string TableName { get; } = "COM_COL_MAPPING_COMPONENT";
    }
}