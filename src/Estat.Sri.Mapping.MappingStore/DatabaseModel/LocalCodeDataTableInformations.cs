// -----------------------------------------------------------------------
// <copyright file="LocalCodeDataTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    [Obsolete("Table doesn't longer exist")]
    public class LocalCodeDataTableInformations : DataTableInformations
    {
        private readonly IDataColumnInformation _columnId = new DataColumnInformation
        {
            ColumnName = "COLUMN_ID",
            ModelName = nameof(LocalCodeEntity.ParentId),
            IsPrimaryKey = true
        };

        private readonly IDataColumnInformation _localCodeId = new DataColumnInformation
        {
            ColumnName = "LCD_ID",
            ModelName = nameof(LocalCodeEntity.EntityId),
            IsPrimaryKey = true
        };

        public LocalCodeDataTableInformations()
        {
            this.mappings.AddRange(new[] {this._localCodeId, this._columnId});
        }

        public override string TableName => "LOCAL_CODE";
    }
}