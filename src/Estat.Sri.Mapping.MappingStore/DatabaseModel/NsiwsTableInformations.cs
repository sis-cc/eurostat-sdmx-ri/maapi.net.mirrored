// -----------------------------------------------------------------------
// <copyright file="NsiwsTableInformations.cs" company="EUROSTAT">
//   Date Created : 2021-08-10
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class NsiwsTableInformations : DataTableInformations
    {
        public static readonly IDataColumnInformation Id = new DataColumnInformation
        {
            ColumnName = "ID",
            ModelName = nameof(NsiwsEntity.EntityId),
            IsPrimaryKey = true,
            IsIdentityColumn = true
        };

        public static readonly IDataColumnInformation Url = new DataColumnInformation
        {
            ColumnName = "URL",
            ModelName = nameof(NsiwsEntity.Url)
        };

        public static readonly IDataColumnInformation UserId = new DataColumnInformation
        {
            ColumnName = "USERNAME",
            ModelName = nameof(NsiwsEntity.UserName)
        };

        public static readonly IDataColumnInformation Password = new DataColumnInformation
        {
            ColumnName = "PASSWORD",
            ModelName = nameof(NsiwsEntity.Password)
        };

        public static readonly IDataColumnInformation Description = new DataColumnInformation
        {
            ColumnName = "DESCRIPTION",
            ModelName = nameof(NsiwsEntity.Description)
        };

        public static readonly IDataColumnInformation Technology = new DataColumnInformation
        {
            ColumnName = "TECHNOLOGY",
            ModelName = nameof(NsiwsEntity.Technology)
        };


        public static readonly IDataColumnInformation Proxy = new DataColumnInformation
        {
            ColumnName = "Proxy",
            ModelName = nameof(NsiwsEntity.Proxy)
        };

        public static readonly IDataColumnInformation Name = new DataColumnInformation
        {
            ColumnName = "NAME",
            ModelName = nameof(NsiwsEntity.Name)
        };

        public NsiwsTableInformations()
        {
            this.mappings.AddRange(new[] { Id, Url, UserId, Password, Description, Technology, Proxy, Name });
        }

        public override string TableName { get; } = "NSIWS";
    }
}