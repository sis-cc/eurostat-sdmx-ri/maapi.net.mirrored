﻿using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class TemplateMappingTimeTranscoding : DataTableInformations
    {
        public static readonly IDataColumnInformation TemplateId = new DataColumnInformation
        {
            ColumnName = "TEMPLATE_ID",
            ModelName = nameof(TemplateMapping.EntityId),
            IsPrimaryKey = true
        };

        public static readonly IDataColumnInformation Frequency = new DataColumnInformation
        {
            ColumnName = "FREQ",
            ModelName = nameof(TimeTranscodingEntity.Frequency)
        };

        public static readonly IDataColumnInformation IsDate = new DataColumnInformation
        {
            ColumnName = "IS_DATE",
            ModelName = "IS_DATE",
        };

        public static readonly IDataColumnInformation YearStart = new DataColumnInformation
        {
            ColumnName = "Y_START",
            ModelName = "Y_START",
        };

        public static readonly IDataColumnInformation YearLength = new DataColumnInformation
        {
            ColumnName = "Y_LENGTH",
            ModelName = "Y_LENGTH",
        };

        public static readonly IDataColumnInformation PeriodStart = new DataColumnInformation
        {
            ColumnName = "P_START",
            ModelName = "P_START",
        };

        public static readonly IDataColumnInformation PeriodLength = new DataColumnInformation
        {
            ColumnName = "P_LENGTH",
            ModelName = "P_LENGTH",
        };


        public TemplateMappingTimeTranscoding()
        {
            this.mappings.AddRange(new[] { TemplateId, Frequency, IsDate, YearStart,YearLength,PeriodStart,PeriodLength });
        }

        public override string TableName { get; } = "TM_TIME_TRANSCODING";
    }
}
