// -----------------------------------------------------------------------
// <copyright file="DatasetPropertyTableInformation.cs" company="EUROSTAT">
//   Date Created : 2021-11-11
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Model;

    /// <summary>
    /// Db table information for the <see cref="MADatasetPropertyEntity"/>.
    /// </summary>
    public class DatasetPropertyTableInformation : DataTableInformations
    {
        private readonly IDataColumnInformation _entityId = new DataColumnInformation
        {
            ColumnName = "DATASET_PROPERTY_ID",
            ModelName = nameof(MADatasetPropertyEntity.EntityId),
            IsPrimaryKey = true
        };


        private readonly IDataColumnInformation _datasetId = new DataColumnInformation
        {
            ColumnName = "DATASET_ID",
            ModelName = nameof(MADatasetPropertyEntity.DataSetId)
        };

        private readonly IDataColumnInformation _propertyTypeId = new DataColumnInformation
        {
            ColumnName = "PROPERTY_TYPE_ENUM",
            ModelName = nameof(MADatasetPropertyEntity.PropertyTypeId)
        };

        private readonly IDataColumnInformation _propertyValue = new DataColumnInformation
        {
            ColumnName = "PROPERTY_VALUE",
            ModelName = nameof(MADatasetPropertyEntity.PropertyValue)
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="DatasetPropertyTableInformation"/> class.
        /// </summary>
        public DatasetPropertyTableInformation()
        {
            this.mappings.AddRange(new[] 
                { this._entityId, this._datasetId, this._propertyTypeId, this._propertyValue });
        }

        /// <summary>
        /// The db table name.
        /// </summary>
        public override string TableName { get; } = "DATASET_PROPERTY";

    }
}
