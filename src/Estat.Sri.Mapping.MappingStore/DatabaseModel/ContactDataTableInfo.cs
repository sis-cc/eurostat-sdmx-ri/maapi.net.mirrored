// -----------------------------------------------------------------------
// <copyright file="ContactDataTableInfo.cs" company="EUROSTAT">
//   Date Created : 2017-04-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class ContactDataTableInfo : DataTableInformations
    {
        public static readonly IDataColumnInformation ContactId = new DataColumnInformation
        {
            ColumnName = "CONTACT_ID",
            ModelName = "CONTACTID",
            IsIdentityColumn = true,
            IsPrimaryKey = true
        };

        public static readonly IDataColumnInformation PartyId = new DataColumnInformation
        {
            ColumnName = "PARTY_ID",
            ModelName = "PARTYID"
        };

        public ContactDataTableInfo()
        {
            this.mappings.AddRange(new[] {PartyId, ContactId});
        }

        public override string TableName { get; } = "CONTACT";
    }
}