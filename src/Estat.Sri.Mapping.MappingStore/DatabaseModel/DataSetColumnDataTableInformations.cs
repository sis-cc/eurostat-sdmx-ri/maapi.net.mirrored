﻿// -----------------------------------------------------------------------
// <copyright file="DataSetColumnDataTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class DataSetColumnDataTableInformations : DataTableInformations
    {
        public static readonly IDataColumnInformation ColumnId = new DataColumnInformation
        {
            ColumnName = "COL_ID",
            ModelName = nameof(DataSetColumnEntity.EntityId),
            IsPrimaryKey = true,
            IsIdentityColumn = true
        };

        private readonly IDataColumnInformation _datasetId = new DataColumnInformation
        {
            ColumnName = "DS_ID",
            ModelName = nameof(DataSetColumnEntity.ParentId)
        };

        private readonly IDataColumnInformation _description = new DataColumnInformation
        {
            ColumnName = "DESCRIPTION",
            ModelName = nameof(DataSetColumnEntity.Description)
        };

        private readonly IDataColumnInformation _lastRetreval = new DataColumnInformation
        {
            ColumnName = "LASTRETRIEVAL",
            ModelName = nameof(DataSetColumnEntity.LastRetrieval)
        };

        private readonly IDataColumnInformation _name = new DataColumnInformation
        {
            ColumnName = "NAME",
            ModelName = nameof(DataSetColumnEntity.Name)
        };

        public DataSetColumnDataTableInformations()
        {
            this.mappings.AddRange(new[] {ColumnId, this._name, this._description, this._datasetId, this._lastRetreval});
        }

        public override string TableName { get; } = "DATASET_COLUMN";
    }
}