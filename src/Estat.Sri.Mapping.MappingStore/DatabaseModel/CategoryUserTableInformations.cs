﻿namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class CategoryUserTableInformations : DataTableInformations
    {
        public CategoryUserTableInformations()
        {
            var userId = UserTableInformations.Id;
            userId.IsIdentityColumn = false;
            var categoryId = CategoryTableInformations.CategoryId;
            categoryId.IsIdentityColumn = false;
            this.mappings.AddRange(new[] { userId, categoryId });
        }

        public override string TableName { get; } = "CATEGORY_USER";
    }
}