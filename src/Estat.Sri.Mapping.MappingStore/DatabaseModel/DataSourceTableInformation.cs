﻿using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class DataSourceTableInformation : DataTableInformations
    {
        private readonly IDataColumnInformation _dataSourceId = new DataColumnInformation
        {
            ColumnName = "DATA_SOURCE_ID",
            ModelName = nameof(DataSourceEntity.EntityId),
            IsIdentityColumn = true,
            IsPrimaryKey = true
        };

        private readonly IDataColumnInformation _dataUrl = new DataColumnInformation
        {
            ColumnName = "DATA_URL",
            ModelName = nameof(DataSourceEntity.DataUrl),
        };

        private readonly IDataColumnInformation _wsdlUrl = new DataColumnInformation
        {
            ColumnName = "WSDL_URL",
            ModelName = nameof(DataSourceEntity.WSDLUrl),
        };

        private readonly IDataColumnInformation _wadlUrl = new DataColumnInformation
        {
            ColumnName = "WADL_URL",
            ModelName = nameof(DataSourceEntity.WADLUrl),
        };

        private readonly IDataColumnInformation _isSimple = new DataColumnInformation
        {
            ColumnName = "IS_SIMPLE",
            ModelName = nameof(DataSourceEntity.IsSimple),
        };

        private readonly IDataColumnInformation _isRest = new DataColumnInformation
        {
            ColumnName = "IS_REST",
            ModelName = nameof(DataSourceEntity.IsRest),
        };

        private readonly IDataColumnInformation _isWs = new DataColumnInformation
        {
            ColumnName = "IS_WS",
            ModelName = nameof(DataSourceEntity.IsWs),
        };

        public DataSourceTableInformation()
        {
            this.mappings.AddRange(new[] { this._dataSourceId, this._dataUrl, this._wsdlUrl, this._wadlUrl, this._isWs,this._isRest,this._isSimple });
        }

        public override string TableName { get; } = "DATA_SOURCE";
    }
    
}