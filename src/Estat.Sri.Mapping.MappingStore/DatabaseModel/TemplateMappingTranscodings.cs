﻿using Estat.Sri.Mapping.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class TemplateMappingTranscodings: DataTableInformations
    {
        public static readonly IDataColumnInformation TemplateId = new DataColumnInformation
        {
            ColumnName = "TEMPLATE_ID",
            ModelName = nameof(TemplateMapping.EntityId),
            IsPrimaryKey = true
        };

        public static readonly IDataColumnInformation LocalCode = new DataColumnInformation
        {
            ColumnName = "LOCAL_CODE",
            ModelName = "LOCAL_CODE"
        };

        public static readonly IDataColumnInformation DSDCode = new DataColumnInformation
        {
            ColumnName = "DSD_CODE",
            ModelName = "DSD_CODE",
        };


        public TemplateMappingTranscodings()
        {
            this.mappings.AddRange(new[] { TemplateId, LocalCode, DSDCode });
        }

        public override string TableName { get; } = "TM_TRANSCODING";
    }
}
