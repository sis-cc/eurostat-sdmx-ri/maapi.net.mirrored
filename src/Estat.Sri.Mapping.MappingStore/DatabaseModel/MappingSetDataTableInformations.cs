// -----------------------------------------------------------------------
// <copyright file="MappingSetDataTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class MappingSetDataTableInformations : EntityBaseTableInformations
    {
        [Obsolete("Field doesn't longer exist")]
        private readonly IDataColumnInformation _dataflowId = new DataColumnInformation
        {
            ColumnName = "DF_ID",
            ModelName = nameof(MappingSetEntity.ParentId)
        };

        private readonly IDataColumnInformation _datasetId = new DataColumnInformation
        {
            ColumnName = "DS_ID",
            ModelName = nameof(MappingSetEntity.DataSetId),
        };

        private readonly IDataColumnInformation _mappingSetDescription = new DataColumnInformation
        {
            ColumnName = "DESCRIPTION",
            ModelName = nameof(MappingSetEntity.Description)
        };

        private readonly IDataColumnInformation _mappingSetId = new DataColumnInformation
        {
            ColumnName = "STR_MAP_SET_ID",
            IsIdentityColumn = true,
            ModelName = "id",
            IsPrimaryKey = true
        };

        /// <summary>
        /// The mapping set name
        /// </summary>
        [Obsolete("Field doesn't longer exist")]
        private readonly IDataColumnInformation _mappingSetName = new DataColumnInformation
        {
            ColumnName = "ID",
            ModelName = nameof(MappingSetEntity.Name)
        };

        [Obsolete("Field doesn't longer exist")]
        private readonly IDataColumnInformation _userId = new DataColumnInformation
        {
            ColumnName = "USER_ID",
            ModelName = "userId"
        };

        private readonly IDataColumnInformation _validFrom = new DataColumnInformation
        {
            ColumnName = "VALID_FROM",
            ModelName = nameof(MappingSetEntity.ValidFrom)
        };

        private readonly IDataColumnInformation _validTo = new DataColumnInformation
        {
            ColumnName = "VALID_TO",
            ModelName = nameof(MappingSetEntity.ValidTo)
        };

        private readonly IDataColumnInformation _isMetadata = new DataColumnInformation
        {
            ColumnName = "IS_METADATA",
            ModelName = nameof(MappingSetEntity.IsMetadata)
        };

        public MappingSetDataTableInformations()
        {
            this.mappings.AddRange(new[] {this._mappingSetId, this._mappingSetName, this._mappingSetDescription, this._datasetId, this._dataflowId, this._userId, this._validFrom, this._validTo, this._isMetadata});
        }

        public override string TableName { get; } = "N_MAPPING_SET";
    }
}