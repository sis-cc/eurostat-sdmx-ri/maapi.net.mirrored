// -----------------------------------------------------------------------
// <copyright file="TranscodingScriptTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class MappingUpdateStatusTableInformations : DataTableInformations
    {
        private readonly IDataColumnInformation _mappingSetId = new DataColumnInformation
        {
            ColumnName = "STR_MAP_SET_ID",
            IsPrimaryKey = true,
            IsIdentityColumn = false,
            ParmeterName = "ENTITY_ID",
            ModelName = nameof(UpdateStatusEntity.EntityId) 
        };
         private readonly IDataColumnInformation _column = new DataColumnInformation
        {
            ColumnName = "DATASET_COLUMN_NAME",
            ParmeterName = "COLUMN",
            ModelName = nameof(UpdateStatusEntity.Column) 
        };
      
        private readonly IDataColumnInformation _constantValue = new DataColumnInformation
        {
            ColumnName = "CONSTANT_OR_DEFAULT",
            ParmeterName = "CONSTANT_VALUE",
            ModelName = nameof(UpdateStatusEntity.Constant)
            
        };

        private readonly IDataColumnInformation _insertValue = new DataColumnInformation
        {
            ColumnName = "INSERT_VALUE",
            ParmeterName = "RULE_INSERT",
            ModelName = nameof(UpdateStatusEntity.InsertValue)

        };
        private readonly IDataColumnInformation _deleteValue = new DataColumnInformation
        {
            ColumnName = "DELETE_VALUE",
            ParmeterName = "RULE_DELETE",
            ModelName = nameof(UpdateStatusEntity.DeleteValue)

        };
        private readonly IDataColumnInformation _updateValue = new DataColumnInformation
        {
            ColumnName = "UPDATE_VALUE",
            ParmeterName = "RULE_UPDATE",
            ModelName = nameof(UpdateStatusEntity.UpdateValue)

        };

        public MappingUpdateStatusTableInformations()
        {
            this.mappings.AddRange(new[] { this._mappingSetId, _column, _constantValue, _insertValue, _deleteValue, _updateValue });
        }

        public override string TableName { get; } = "N_UPDATE_STATUS";
    }
}