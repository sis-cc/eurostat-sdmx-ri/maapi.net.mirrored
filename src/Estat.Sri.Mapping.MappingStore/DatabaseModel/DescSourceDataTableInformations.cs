using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class DescSourceTableInformation : DataTableInformations
    {
        private readonly IDataColumnInformation _descSourceId = new DataColumnInformation
        {
            ColumnName = "DESC_SOURCE_ID",
            ModelName = nameof(ColumnDescriptionSourceEntity.EntityId),
            IsIdentityColumn = true,
            IsPrimaryKey = true
        };

        private readonly IDataColumnInformation _descTableId = new DataColumnInformation
        {
            ColumnName = "DESC_TABLE",
            ModelName = nameof(ColumnDescriptionSourceEntity.DescriptionTable),
        };

        private readonly IDataColumnInformation _relatedField = new DataColumnInformation
        {
            ColumnName = "RELATED_FIELD",
            ModelName = nameof(ColumnDescriptionSourceEntity.RelatedField),
        };

        private readonly IDataColumnInformation _description = new DataColumnInformation
        {
            ColumnName = "DESC_FIELD",
            ModelName = nameof(ColumnDescriptionSourceEntity.DescriptionField),
        };

        private readonly IDataColumnInformation _columnId = new DataColumnInformation
        {
            ColumnName = "COL_ID",
            ModelName = nameof(ColumnDescriptionSourceEntity.ParentId),
        };

        public DescSourceTableInformation()
        {
            this.mappings.AddRange(new[] { this._descSourceId, this._descTableId, this._relatedField, this._description, this._columnId });
        }

        public override string TableName { get; } = "DESC_SOURCE";
    }
    
}