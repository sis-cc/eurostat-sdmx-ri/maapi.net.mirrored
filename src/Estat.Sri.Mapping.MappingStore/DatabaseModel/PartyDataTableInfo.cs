namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class PartyDataTableInfo : DataTableInformations
    {
        public static readonly IDataColumnInformation Type = new DataColumnInformation
        {
            ColumnName = "TYPE",
            ModelName = "TYPE"
        };

        public static readonly IDataColumnInformation Id = new DataColumnInformation
        {
            ColumnName = "ID",
            ModelName = "ID"
        };

        public static readonly IDataColumnInformation PartyId = new DataColumnInformation
        {
            ColumnName = "PARTY_ID",
            ModelName = "PARTYID",
            IsIdentityColumn = true,
            IsPrimaryKey = true
        };

        public static readonly IDataColumnInformation HeaderId = new DataColumnInformation
        {
            ColumnName = "HEADER_ID",
            ModelName = "HEADERID",
        };

        public PartyDataTableInfo()
        {
            this.mappings.AddRange(new[] { PartyId, HeaderId, Id, Type });
        }

        public override string TableName { get; } = "PARTY";
    }
}