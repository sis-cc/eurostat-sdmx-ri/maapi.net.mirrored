﻿// -----------------------------------------------------------------------
// <copyright file="TranscodingScriptTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class TimeDimensionMappingTableInformations : DataTableInformations
    {
        private readonly IDataColumnInformation _mappingSetId = new DataColumnInformation
        {
            ColumnName = "STR_MAP_SET_ID",
            IsPrimaryKey = true,
            IsIdentityColumn = false,
            ParmeterName = "ENTITY_ID",
            ModelName = nameof(TimeDimensionMappingEntity.EntityId) 
        };
         private readonly IDataColumnInformation _outputColumn = new DataColumnInformation
        {
            ColumnName = "OUTPUT_COLUMN",
            ParmeterName = "COLUMN",
            ModelName = nameof(TimePreFormattedEntity.OutputColumn) 
        };
        private readonly IDataColumnInformation _simpleColumnName = new DataColumnInformation
        {
            ColumnName = "OUTPUT_COLUMN",
            ParmeterName = "COLUMN",
            ModelName = nameof(TimeDimensionMappingEntity.SimpleMappingColumn) 
        };

        private readonly IDataColumnInformation _isFrequencyUsedAsCriteria = new DataColumnInformation
        {
            ColumnName = "IS_FREQ_CRITERIA",
            ParmeterName = "IS_FREQ",
            ModelName = nameof(TimeDimensionMappingEntity.Transcoding.IsFrequencyDimension)
            
        };
         private readonly IDataColumnInformation _criteriaColumn = new DataColumnInformation
                {
                    ColumnName = "CRITERIA_COL_OR_DIM",
                    ParmeterName = "CRITERIA",
                    ModelName = nameof(TimeDimensionMappingEntity.Transcoding.CriteriaColumn)
                };
             private readonly IDataColumnInformation _frequencyDim = new DataColumnInformation
                         {
                             ColumnName = "CRITERIA_COL_OR_DIM",
                             ParmeterName = "FREQ",
                             ModelName = nameof(TimeDimensionMappingEntity.Transcoding.FrequencyDimension)
                         };
         private readonly IDataColumnInformation _constantValue = new DataColumnInformation
        {
            ColumnName = "CONSTANT_OR_DEFAULT",
            ParmeterName = "CONSTANT_VALUE",
            ModelName = nameof(TimeDimensionMappingEntity.ConstantValue)
            
        };
        private readonly IDataColumnInformation _defaultValue = new DataColumnInformation
        {
            ColumnName = "CONSTANT_OR_DEFAULT",
            ParmeterName = "DEFAULT_VALUE",
            ModelName = nameof(TimeDimensionMappingEntity.DefaultValue)
        };

        private readonly IDataColumnInformation _fromSource = new DataColumnInformation
        {
            ColumnName = "FROM_SOURCE",
            ParmeterName = "FROM_COL",
            ModelName = nameof(TimePreFormattedEntity.FromColumn),
        };
        private readonly IDataColumnInformation _toSource = new DataColumnInformation
        {
            ColumnName = "TO_SOURCE",
            ParmeterName = "TO_COL",
            ModelName = nameof(TimePreFormattedEntity.ToColumn),
        };

        public TimeDimensionMappingTableInformations()
        {
            this.mappings.AddRange(new[] { this._mappingSetId, this._simpleColumnName, this._isFrequencyUsedAsCriteria,this._fromSource, _outputColumn, _constantValue, _defaultValue, _toSource, _criteriaColumn, _frequencyDim});
        }

        public override string TableName { get; } = "N_MAPPING_TIME_DIMENSION";
    }
}