using System;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class ComponentMappingMetadataAttributeDataTableInformations : DataTableInformations
    {
        public static readonly IDataColumnInformation SdmxId = new DataColumnInformation
        {
            ColumnName = "SDMX_ID",
            ModelName = "sdmx_id"
        };

        private readonly IDataColumnInformation _mappingId = new DataColumnInformation
        {
            ColumnName = "MAP_ID",
            ModelName = nameof(ComponentMappingEntity.EntityId),
            IsPrimaryKey = true
        };

        public ComponentMappingMetadataAttributeDataTableInformations()
        {
            this.mappings.AddRange(new[] {this._mappingId, SdmxId});
        }

        public override string TableName { get; } = "META_COL_MAPPING_COMPONENT";
    }
}