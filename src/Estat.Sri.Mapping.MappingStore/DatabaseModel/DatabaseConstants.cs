﻿// -----------------------------------------------------------------------
// <copyright file="DatabaseConstants.cs" company="EUROSTAT">
//   Date Created : 2017-02-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public static class DatabaseConstants
    {
        [Obsolete("Use Database.BuildParameterName as it does the same thing and this is hard coded and doesn't work with additional providers")]
        private static readonly Dictionary<string, string> _parameterMarkerFormats = new Dictionary<string, string>
        {
            {"System.Data.SqlClient", "@{0}"},
            {"Oracle.DataAccess.Client", ":{0}"},
            {"MySql.Data.MySqlClient", "@{0}"}
        };

        private static readonly Dictionary<Type, DbType> _typeMap = new Dictionary<Type, DbType>
        {
            {typeof (byte), DbType.Byte},
            {typeof (sbyte), DbType.SByte},
            {typeof (short), DbType.Int16},
            {typeof (ushort), DbType.UInt16},
            {typeof (int), DbType.Int32},
            {typeof (uint), DbType.UInt32},
            {typeof (long), DbType.Int64},
            {typeof (ulong), DbType.UInt64},
            {typeof (float), DbType.Single},
            {typeof (double), DbType.Double},
            {typeof (decimal), DbType.Decimal},
            {typeof (bool), DbType.Boolean},
            {typeof (string), DbType.String},
            {typeof (char), DbType.StringFixedLength},
            {typeof (Guid), DbType.Guid},
            {typeof (DateTime), DbType.DateTime},
            {typeof (DateTimeOffset), DbType.DateTimeOffset},
            {typeof (byte[]), DbType.Binary},
            {typeof (byte?), DbType.Byte},
            {typeof (sbyte?), DbType.SByte},
            {typeof (short?), DbType.Int16},
            {typeof (ushort?), DbType.UInt16},
            {typeof (int?), DbType.Int32},
            {typeof (uint?), DbType.UInt32},
            {typeof (long?), DbType.Int64},
            {typeof (ulong?), DbType.UInt64},
            {typeof (float?), DbType.Single},
            {typeof (double?), DbType.Double},
            {typeof (decimal?), DbType.Decimal},
            {typeof (bool?), DbType.Boolean},
            {typeof (char?), DbType.StringFixedLength},
            {typeof (Guid?), DbType.Guid},
            {typeof (DateTime?), DbType.DateTime},
            {typeof (DateTimeOffset?), DbType.DateTimeOffset}
        };

        public static DbType GetDbType(Type type)
        {
            return _typeMap[type];
        }

        [Obsolete("Use Database.BuildParameterName as it does the same thing and this is hard coded and doesn't work with additional providers")]
        public static string GetParameterMarkerFormat(string provider)
        {
            return _parameterMarkerFormats[provider];
        }
    }
}