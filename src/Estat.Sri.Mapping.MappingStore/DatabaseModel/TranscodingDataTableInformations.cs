﻿// -----------------------------------------------------------------------
// <copyright file="TranscodingDataTableInformations.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.DatabaseModel
{
    public class TranscodingDataTableInformations : DataTableInformations
    {
        public static readonly IDataColumnInformation Expression = new DataColumnInformation
        {
            ColumnName = "EXPRESSION",
            ModelName = nameof(TranscodingEntity.Expression),
        };

        private readonly IDataColumnInformation _mapId = new DataColumnInformation
        {
            ColumnName = "MAP_ID",
            ModelName = nameof(TranscodingEntity.ParentId),
        };

        private readonly IDataColumnInformation _transcodingId = new DataColumnInformation
        {
            ColumnName = "TR_ID",
            ModelName = "transcodingId",
            IsPrimaryKey = true,
            IsIdentityColumn = true
        };

        public TranscodingDataTableInformations()
        {
            this.mappings.AddRange(new[] {this._transcodingId, Expression, this._mapId});
        }

        public override string TableName { get; } = "TRANSCODING";
    }
}