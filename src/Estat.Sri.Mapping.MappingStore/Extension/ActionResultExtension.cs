﻿// -----------------------------------------------------------------------
// <copyright file="ActionResultExtension.cs" company="EUROSTAT">
//   Date Created : 2017-04-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Extension
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Model;
    using System.Linq;

    public static class ActionResultExtension
    {
        public static IActionResult ToActionsResult(this IEnumerable<ArtefactImportStatus> artefactImportStatusList)
        {
            StatusType statusType = StatusType.Success;

            List<string> errorMessages = new List<string>();
            string errorCode = "200";
            foreach (var artefactImportStatus in artefactImportStatusList)
            {
                if (artefactImportStatus.ImportMessage.Status == ImportMessageStatus.Warning && statusType == StatusType.Success)
                {
                    statusType = StatusType.Warning;
                    errorCode = "500"; // TODO Dummy value
                }

                if (artefactImportStatus.ImportMessage.Status == ImportMessageStatus.Error)
                {
                    statusType = StatusType.Error;
                    errorCode = "500"; // TODO Dummy value
                }

                errorMessages.Add(artefactImportStatus.ImportMessage.StructureReference.TargetUrn + " : " + artefactImportStatus.ImportMessage.Message);
            }

            return new ActionResult(errorCode, statusType,null, errorMessages.ToArray());
        }
    }
}