﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Extension
{
    public static class PatchDocumentExtension
    {
        public static int? PeriodStart(this IEnumerable<PatchDocument> patchDocuments)
        {
            var patchDocument = patchDocuments.FirstOrDefault(item => item.HasPath("start") && item.HasPath("period"));
            if (!string.IsNullOrWhiteSpace(patchDocument?.Value.ToString()))
            {
                return Convert.ToInt32(patchDocument.Value);
            }
            return null;
        }


        public static int? PeriodLength(this IEnumerable<PatchDocument> patchDocuments)
        {
            var patchDocument = patchDocuments.FirstOrDefault(item => item.HasPath("length") && item.HasPath("period"));
            if (!string.IsNullOrWhiteSpace(patchDocument?.Value.ToString()))
            {
                return Convert.ToInt32(patchDocument.Value);
            }
            return null;
        }


        public static int? YearStart(this IEnumerable<PatchDocument> patchDocuments)
        {
            var patchDocument = patchDocuments.FirstOrDefault(item => item.HasPath("start") && item.HasPath("year"));
            if (!string.IsNullOrWhiteSpace(patchDocument?.Value.ToString()))
            {
                return Convert.ToInt32(patchDocument.Value);
            }
            return null;
        }

        public static int? YearLength(this IEnumerable<PatchDocument> patchDocuments)
        {
            var patchDocument = patchDocuments.FirstOrDefault(item => item.HasPath("length") && item.HasPath("year"));
            if (!string.IsNullOrWhiteSpace(patchDocument?.Value.ToString()))
            {
                return Convert.ToInt32(patchDocument.Value);
            }
            return null;
        }
    }
}
