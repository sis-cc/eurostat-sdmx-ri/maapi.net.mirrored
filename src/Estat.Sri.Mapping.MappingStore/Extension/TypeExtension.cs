﻿// -----------------------------------------------------------------------
// <copyright file="TypeExtension.cs" company="EUROSTAT">
//   Date Created : 2017-03-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Extension
{
    using System;
    using System.Data.SqlTypes;

    /// <summary>
    /// The <see cref="Type"/> extension.
    /// </summary>
    public static class TypeExtension
    {
        /// <summary>
        /// Gets the parent namespace.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The namespace as string; otherwise <see cref="string.Empty"/></returns>
        /// <exception cref="System.ArgumentNullException">type is null</exception>
        public static string GetParentNamespace(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            var ns = type.Namespace;
            if (!string.IsNullOrWhiteSpace(ns))
            {
                var lastIndexOf = ns.LastIndexOf('.');
                if (lastIndexOf > 0)
                {
                    return ns.Substring(0, lastIndexOf);
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the default value for a specific <see cref="Type"/>.
        /// </summary>
        /// <param name="t">The type.</param>
        /// <returns>The default value of an object is it is a value type; otherwise null</returns>
        public static object GetDefaultValue(this Type t)
        {
            if (t == null)
            {
                return null;
            }

            return t.IsValueType ? Activator.CreateInstance(t) : null;
        }

        /// <summary>
        /// Gets the value or default.
        /// </summary>
        /// <typeparam name="T">The value type</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>If it is set the <c>value.Value</c>; otherwise the <paramref name="defaultValue"/></returns>
        public static T GetValueOrDefault<T>(this T? value, T defaultValue) where T : struct
        {
            if (value == null)
            {
                return defaultValue;
            }

            return value.Value;
        }
    }
}