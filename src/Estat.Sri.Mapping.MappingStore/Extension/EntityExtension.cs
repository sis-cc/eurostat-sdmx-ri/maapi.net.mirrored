// -----------------------------------------------------------------------
// <copyright file="EntityExtension.cs" company="EUROSTAT">
//   Date Created : 2017-04-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Extension
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Model;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Mapping Store specific entity related extensions.
    /// </summary>
    public static class EntityExtension
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(EntityExtension));

        /// <summary>
        /// Gets the parent identifier.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The entity parent id as long.</returns>
        /// <exception cref="MissingInformationException">ParentID is not set for entity</exception>
        public static long GetParentId(this IEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            return entity.ParentId.AsMappingStoreEntityId();
        }

        /// <summary>
        /// Gets the entity identifier.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The entity id as long.</returns>
        /// <exception cref="MissingInformationException">id is not set for entity</exception>
        public static long GetEntityId(this IEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            return entity.EntityId.AsMappingStoreEntityId();
        }

        /// <summary>
        /// Apply the mapping store identifier to all entities
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entities">The entities.</param>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        /// <returns>The entity</returns>
        public static IEnumerable<TEntity> WithMappingStoreId<TEntity>(
            this IEnumerable<TEntity> entities,
            string mappingStoreId) where TEntity : IEntity
        {
            foreach (var entity in entities)
            {
                Debug.Assert(entity.GetType().IsClass, "Entity not a class");
                // ReSharper disable once PossibleAssignmentToReadonlyField
                entity.StoreId = mappingStoreId;

                yield return entity;
            }
        }

        /// <summary>
        /// Gets the value from <see cref="IConnectionEntity.Settings"/> using the specified <paramref name="key"/>.
        /// </summary>
        /// <typeparam name="T">The type of value</typeparam>
        /// <param name="connection">The connection.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The value</returns>
        public static T GetValue<T>(this IConnectionEntity connection, string key, T defaultValue = default(T))
        {
            IConnectionParameterEntity parameter;
            if (connection.Settings.TryGetValue(key, out parameter))
            {
                return (T)parameter.Value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Gets the value from <see cref="IConnectionEntity.Settings"/> using the specified <paramref name="key"/>.
        /// </summary>
        /// <typeparam name="T">The type of value</typeparam>
        /// <param name="connection">The connection.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The value as string</returns>
        public static string GetValueAsString<T>(this IConnectionEntity connection, string key, T defaultValue = default(T))
        {
            IConnectionParameterEntity parameter;
            if (connection.Settings.TryGetValue(key, out parameter))
            {
                return Convert.ToString(parameter.Value, CultureInfo.InvariantCulture);
            }

            return Convert.ToString(defaultValue, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Gets the value from <see cref="IConnectionEntity.Settings"/> using the specified <paramref name="key"/>.
        /// </summary>
        /// <typeparam name="T">The type of value</typeparam>
        /// <param name="connection">The connection.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The value as string</returns>
        public static string GetNotNullValueAsString<T>(this IConnectionEntity connection, string key, T defaultValue)
        {
            IConnectionParameterEntity parameter;
            if (connection.Settings.TryGetValue(key, out parameter) && parameter?.Value != null)
            {
                return Convert.ToString(parameter.Value, CultureInfo.InvariantCulture);
            }

            return Convert.ToString(defaultValue, CultureInfo.InvariantCulture);
        }


        /// <summary>
        /// Determines whether the <paramref name="entity"/> has a valid <see cref="IEntity.EntityId"/>
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        ///   <c>true</c> if <see cref="IEntity.EntityId"/> is valid; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasEntityId(this IEntity entity)
        {
            long id;
            if (!long.TryParse(entity?.EntityId, NumberStyles.None, CultureInfo.InvariantCulture, out id))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Convert to the mapping store entity identifier.
        /// </summary>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns>The mapping store entity id as <see cref="long"/></returns>
        /// <exception cref="Estat.Sri.Mapping.Api.Exceptions.BadRequestException">id is not set for entity</exception>
        public static long AsMappingStoreEntityId(this string entityId)
        {
            long id;
            if (!long.TryParse(entityId, NumberStyles.None, CultureInfo.InvariantCulture, out id))
            {
                _log.ErrorFormat(CultureInfo.InvariantCulture, "Could not convert the following entity or parent id to a number: '{0}'", entityId);
                throw new BadRequestException("id is not set for entity");
            }

            return id;
        }

        /// <summary>
        /// Gets the permissions.
        /// </summary>
        /// <param name="permissionEntity">
        /// The permission entity.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{IStructureReference}"/>.
        /// </returns>
        public static IEnumerable<IStructureReference> GetPermissions(this IPermissionEntity permissionEntity)
        {
            return permissionEntity?.Permissions.Select(pair => new StructureReferenceImpl(pair.Key));
        }

        /// <summary>
        /// Sets the entity identifier.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        public static void SetEntityId(this IEntity entity, long entityId)
        {
            var normEntityId = Convert.ToString(entityId, CultureInfo.InvariantCulture);
            entity.EntityId = normEntityId;
        }

        /// <summary>
        /// Sets the parent identifier.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="parentId">The parent identifier.</param>
        public static void SetParentId(this IEntity entity, long parentId)
        {
            var normEntityId = Convert.ToString(parentId, CultureInfo.InvariantCulture);
            entity.ParentId = normEntityId;
        }

        /// <summary>
        /// Determines whether the value of the [the specified connection parameter entity is default.
        /// </summary>
        /// <param name="parameterEntity">The parameter entity.</param>
        /// <returns>
        ///   <c>true</c> if [is it default] [the specified connection parameter entity]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsItDefault(this IConnectionParameterEntity parameterEntity)
        {
            if (parameterEntity == null)
            {
                return true;
            }

            if (parameterEntity.Required)
            {
                return false;
            }

            return string.IsNullOrWhiteSpace(parameterEntity.Value?.ToString()) || Equals(parameterEntity.Value, parameterEntity.DefaultValue);
        }

        /// <summary>
        /// Adds the other setting.
        /// </summary>
        /// <param name="connectionEntity">The connection entity.</param>
        /// <param name="key">The key.</param>
        /// <param name="type">The type.</param>
        /// <param name="value">The value.</param>
        /// <param name="advancedSetting">if set to <c>true</c> [advanced setting].</param>
        /// <param name="required">if set to <c>true</c> [required].</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// The <see cref="ConnectionParameterEntity" />.
        /// </returns>
        public static ConnectionParameterEntity AddSetting(this IConnectionEntity connectionEntity, string key, ParameterType type, object value, bool advancedSetting = false, bool required = false, object defaultValue = null)
        {
            var connectionParameterEntity = new ConnectionParameterEntity()
                                                                      {
                                                                          Advanced = advancedSetting,
                                                                          DataType = type,
                                                                          Value = value,
                                                                          Required = required,
                                                                          DefaultValue = defaultValue
                                                                      };
            connectionEntity.Settings[key] = connectionParameterEntity;
            return connectionParameterEntity;
        }

        /// <summary>
        /// Adds the other setting.
        /// </summary>
        /// <param name="connectionEntity">The connection entity.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="existingSettings">The existing settings.</param>
        /// <param name="advancedSetting">if set to <c>true</c> [advanced setting].</param>
        /// <param name="required">if set to <c>true</c> [required].</param>
        /// <returns>
        /// The <see cref="ConnectionParameterEntity" />.
        /// </returns>
        public static ConnectionParameterEntity AddSetting(
            this IConnectionEntity connectionEntity,
            string key,
            object value,
            ISet<string> existingSettings,
            bool advancedSetting = false,
            bool required = false)
        {
            var type = value?.GetType();
            var defaultValue = type.GetDefaultValue();
            if (!existingSettings.Contains(key))
            {
                defaultValue = value;
            }

            var parameterType = type.ToParameterType();
            var connectionParameterEntity = new ConnectionParameterEntity
                                                {
                                                    Advanced = advancedSetting,
                                                    DataType = parameterType,
                                                    Value = value,
                                                    Required = required,
                                                    DefaultValue = defaultValue
                                                };
            connectionEntity.Settings[key] = connectionParameterEntity;
            return connectionParameterEntity;
        }
    }
}