﻿// -----------------------------------------------------------------------
// <copyright file="ConversionExtension.cs" company="EUROSTAT">
//   Date Created : 2017-02-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Globalization;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Exception;

namespace Estat.Sri.Mapping.MappingStore.Extension
{
    public static class ConversionExtension
    {
        /// <summary>
        /// Convert the value of the criteria from string to long
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns>The Criteria with the value converted </returns>
        /// <exception cref="SdmxNoResultsException">No results where found</exception>
        public static ICriteria<long?> ValueConvert(this ICriteria<string> criteria)
        {
            if (criteria == null)
            {
                return null;
            }

            if (criteria.Operator == OperatorType.AnyValue || string.IsNullOrWhiteSpace(criteria.Value))
            {
                return new Criteria<long?>(criteria.Operator, null);
            }

            long value;
            if (long.TryParse(criteria.Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out value))
            {
                return new Criteria<long?>(criteria.Operator, value);
            }

            throw new SdmxNoResultsException("No results where found");
        }
    }
}