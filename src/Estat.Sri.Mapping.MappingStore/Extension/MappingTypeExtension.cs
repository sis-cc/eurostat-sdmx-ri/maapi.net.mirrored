// -----------------------------------------------------------------------
// <copyright file="MappingTypeExtension.cs" company="EUROSTAT">
//   Date Created : 2017-07-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Extension
{
    using System;

    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// A set of extension methods for <see cref="ComponentMappingType"/> to convert between API mapping types and MSDB types
    /// </summary>
    public static class MappingTypeExtension
    {
        /// <summary>
        /// Convert from a <c>MSDB</c> <c>COMPONENT_MAPPING.TYPE</c> field value to <see cref="ComponentMappingType"/>
        /// </summary>
        /// <param name="mappingStoreComponentMappingType">
        /// Type of the mapping store component mapping.
        /// </param>
        /// <returns>
        /// The <see cref="ComponentMappingType"/>.
        /// </returns>
        public static ComponentMappingType AsApiMappingType(this string mappingStoreComponentMappingType)
        {
            switch (mappingStoreComponentMappingType)
            {
                case null:
                case "A":
                    return ComponentMappingType.Normal;
                case "LAST_UP":
                    return ComponentMappingType.LastUpdated;
                case "UP_STATUS":
                    return ComponentMappingType.UpdatedStatus;
                case "DURATION":
                    return ComponentMappingType.Duration;
                case "VALID_TO":
                    return ComponentMappingType.ValidTo;
            }

            return ComponentMappingType.Other;
        }
    }
}