using System.Linq;
using System.Xml.Linq;

namespace Estat.Sri.Mapping.MappingStore.Extension
{
    public static class XElementExtension
    {
        public static XElement Descendant(this XElement parent,string nodeName)
        {
            return parent.Descendants().FirstOrDefault(x=>x.Name.LocalName == nodeName);
        }
    }
}