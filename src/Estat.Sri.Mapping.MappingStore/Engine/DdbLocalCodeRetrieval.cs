// -----------------------------------------------------------------------
// <copyright file="DdbLocalCodeRetrieval.cs" company="EUROSTAT">
//   Date Created : 2014-09-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.Mapping.MappingStore.Builder;
    using Estat.Sri.Mapping.MappingStore.Constant;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Builder;

    /// <summary>
    ///     The DDB local code retrieval.
    /// </summary>
    public class DdbLocalCodeRetrieval : ILocalCodeRetrieval
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DdbLocalCodeRetrieval));

        /// <summary>
        ///     The _normal DDB SQL builder
        /// </summary>
        private readonly IDdbSqlBuilder _normalDdbSqlBuilder;

        /// <summary>
        ///     The _ordered SQL builder
        /// </summary>
        private readonly IDdbSqlBuilder _orderedSqlBuilder;

        /// <summary>
        /// The count SQL builder
        /// </summary>
        private readonly IDdbSqlBuilder _countSqlBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DdbLocalCodeRetrieval" /> class.
        /// </summary>
        /// <param name="ddbSqlBuilderFactory">The DDB SQL builder factory.</param>
        public DdbLocalCodeRetrieval(IDdbSqlBuilderFactory ddbSqlBuilderFactory)
        {
            this._normalDdbSqlBuilder = ddbSqlBuilderFactory.CreateBuilder(SqlBuilderType.Normal);
            this._orderedSqlBuilder = ddbSqlBuilderFactory.CreateBuilder(SqlBuilderType.Ordered);
            this._countSqlBuilder = ddbSqlBuilderFactory.CreateBuilder(SqlBuilderType.Count);
        }

        /// <summary>
        /// Gets the local codes list from the DDB.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="query">The dataset query.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="page"></param>
        /// <returns>
        /// The enumeration of local codes from the DDB
        /// </returns>
        public IEnumerable<object> GetLocalCodeList(ConnectionStringSettings settings, string query, string columnName, int limit, int page)
        {
            //// TODO try to use database specific ways to limit rows.
            return this.GetLocalCodeList(settings, query, columnName, limit, this._normalDdbSqlBuilder, page);
        }

        /// <summary>
        /// Gets the local codes list from the DDB.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="query">The query.</param>
        /// <returns>
        /// The number of records
        /// </returns>
        public int GetCount(ConnectionStringSettings settings, string query)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var ddbDatabase = new Database(settings);

            using (var connection = ddbDatabase.CreateConnection())
            {
                connection.Open();
                var buildCountQuery = this._countSqlBuilder.Build(new QueryInfo() { ColumnNames = "*", TableOrQuery = query });
                using (var getCodesCommand = connection.CreateCommand())
                {
                    getCodesCommand.CommandText = buildCountQuery;
                    getCodesCommand.CommandType = CommandType.Text;
                    getCodesCommand.CommandTimeout = 0;
                    var executeScalar = getCodesCommand.ExecuteScalarAndLog();
                    return Convert.ToInt32(executeScalar, CultureInfo.InvariantCulture);
                }
            }
        }

        /// <summary>
        /// Gets the local code list for multiple columns from the DDB
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="query">The query.</param>
        /// <param name="columns">The columns.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="page">The page.</param>
        /// <returns>
        /// The enumeration of local codes from the DDB
        /// </returns>
        /// <exception cref="System.ArgumentNullException">query is null</exception>
        public IEnumerable<object[]> GetLocalCodeList(ConnectionStringSettings settings, string query, IList<string> columns, int limit, int page)
        {
            return this.GetLocalCodesFromDdb(settings, query, columns, limit, page, this._normalDdbSqlBuilder);
        }

        /// <summary>
        /// Gets the local codes list from the DDB.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="query">The query.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="page"></param>
        /// <returns>
        /// The enumeration of local codes from the DDB
        /// </returns>
        public IEnumerable<object> GetLocalCodeListOrdered(ConnectionStringSettings settings, string query, string columnName, int limit, int page)
        {
            //// TODO try to use database specific ways to limit rows.
            return this.GetLocalCodeList(settings, query, columnName, limit, this._orderedSqlBuilder, page);
        }

        /// <summary>
        /// Gets the column names.
        /// </summary>
        /// <param name="columns">The columns.</param>
        /// <returns>the columns names as a string separated by comma; if there are no columns a <c>*</c></returns>
        private static string GetColumnNames(IList<string> columns)
        {
            if (columns.Count > 0)
            {
                return string.Join(",", columns);
            }

            return "*";
        }

        /// <summary>
        /// Gets the local codes from DDB. If <paramref name="columns"/> is empty then it will also populate them.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="query">The query.</param>
        /// <param name="columns">The columns.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="page">The page.</param>
        /// <param name="sqlBuilder">The SQL builder.</param>
        /// <returns>
        /// The enumeration of local codes from the DDB
        /// </returns>
        /// <exception cref="System.ArgumentNullException">settings
        /// or
        /// query</exception>
        private IEnumerable<object[]> GetLocalCodesFromDdb(ConnectionStringSettings settings, string query, IList<string> columns, int limit, int page, IDdbSqlBuilder sqlBuilder)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var ddbDatabase = new Database(settings);

            int skip = limit * page;
            using (DbConnection connection = ddbDatabase.CreateConnection())
            {
                connection.Open();
                var finalQuery =
                    sqlBuilder.Build(
                        new QueryInfo { ColumnNames = GetColumnNames(columns), TableOrQuery = query });
                using (var getCodesCommand = connection.CreateCommand())
                {
                    getCodesCommand.CommandText = finalQuery;
                    getCodesCommand.CommandType = CommandType.Text;
                    getCodesCommand.CommandTimeout = 0;
                    using (var reader = getCodesCommand.ExecuteReader())
                    {
                        int count = 0;
                        var values = new object[reader.FieldCount];
                        if (columns.Count == 0)
                        {
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                columns.Add(reader.GetName(i));
                            }
                        }

                        while (skip > 0 && reader.Read())
                        {
                            skip--;
                        }

                        while (count < limit && reader.Read())
                        {
                            var len = reader.GetValues(values);
                            yield return values.Take(len).ToArray();
                            count++;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the local code list.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="query">The query.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="ddbSqlBuilder">The DDB SQL builder.</param>
        /// <param name="page"></param>
        /// <returns>
        /// The enumeration of local codes from the DDB
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// settings
        /// or
        /// query
        /// or
        /// columnName
        /// </exception>
        private IEnumerable<object> GetLocalCodeList(ConnectionStringSettings settings, string query, string columnName, int limit, IBuilder<string, IDatasetColumnInfo> ddbSqlBuilder, int page)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            if (columnName == null)
            {
                throw new ArgumentNullException(nameof(columnName));
            }
            int skip = limit * page;
            var ddbDatabase = new Database(settings);
            using (DbConnection connection = ddbDatabase.CreateConnection())
            {
                connection.Open();
                var finalQuery = ddbSqlBuilder.Build(new QueryInfo { ColumnNames = columnName, TableOrQuery = query });
                using (var getCodesCommand = connection.CreateCommand())
                {
                    getCodesCommand.CommandText = finalQuery;
                    getCodesCommand.CommandType = CommandType.Text;
                    getCodesCommand.CommandTimeout = 0;

                    _log.DebugFormat("Executed query {0}", getCodesCommand.CommandText);
                    using (var reader = getCodesCommand.ExecuteReader())
                    {
                        // TODO FIXME this doesn't account the null values
                        while (skip > 0 && reader.Read())
                        {
                            skip--;
                        }

                        int count = 0;
                        while ((limit < 0 || limit > count) && reader.Read())
                        {
                            object result = reader.GetValue(0);
                            if (!Convert.IsDBNull(result) && result != null)
                            {
                                yield return result;
                                count++;
                            }
                        }

                        getCodesCommand.SafeCancel();
                    }
                }
            }
        }

        /// <inherit />
        public void WriteLocalCodeList(ConnectionStringSettings connectionStringSettings, ILocalDataQuery dataQuery, ILocalDataWriter writer, ILocalCodeValidator validator)
        {
            if (connectionStringSettings == null)
            {
                throw new ArgumentNullException(nameof(connectionStringSettings));
            }

            if (dataQuery == null)
            {
                throw new ArgumentNullException(nameof(dataQuery));
            }

            if (dataQuery.CheckForErrors && validator == null)
            {
                throw new ArgumentException("Check for errors enabled but no validator provided", nameof(validator));
            }

            if (dataQuery.IsOrdered && dataQuery.ColumnNames.Count != 1)
            {
                throw new ArgumentException("Ordered queries supported only for 1 column", nameof(dataQuery));
            }
            var query = dataQuery.Query ?? dataQuery.Table;
            var count = GetCount(connectionStringSettings, query);
            if (count ==0)
            {
                // TODO check with Java implementation, either 404 or empty message
                throw new ResourceNotFoundException("The query returned no results");
            }

            IDdbSqlBuilder sqlBuilder = dataQuery.IsOrdered ? this._orderedSqlBuilder : this._normalDdbSqlBuilder;
            WriteLocalCodes(connectionStringSettings, dataQuery, writer, sqlBuilder, validator, count);
        }

        private static void WriteLocalCodes(ConnectionStringSettings connectionStringSettings, ILocalDataQuery dataQuery, ILocalDataWriter writer, IDdbSqlBuilder sqlBuilder, ILocalCodeValidator validator, int totalCount)
        {
            int limit = dataQuery.Count;
            int skip = dataQuery.Count * dataQuery.Page;
            var ddbDatabase = new Database(connectionStringSettings);
            var columnNames = dataQuery.ColumnNames;
            string query = dataQuery.Query ?? dataQuery.Table;
           
            using (DbConnection connection = ddbDatabase.CreateConnection())
            {
                connection.Open();
                var finalQuery = sqlBuilder.Build(new QueryInfo { ColumnNames = GetColumnNames(columnNames), TableOrQuery = query });
                using (var getCodesCommand = connection.CreateCommand())
                {
                    getCodesCommand.CommandText = finalQuery;
                    getCodesCommand.CommandType = CommandType.Text;
                    getCodesCommand.CommandTimeout = 0;

                 
                    _log.DebugFormat("Executed query {0}", getCodesCommand.CommandText);
                    using (var reader = getCodesCommand.ExecuteReader())
                    {
                        var values = new object[reader.FieldCount];
                        _log.DebugFormat("GIven column count {0}", columnNames.Count);
                        if (columnNames.Count == 0)
                        {
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                columnNames.Add(reader.GetName(i));
                            }
                        }
                        var columns = new List<string>();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            columns.Add(reader.GetName(i));
                        }
                        writer.StartMessage(columns, dataQuery.Page * dataQuery.Count, (dataQuery.Count + 1) * dataQuery.Page);
                        writer.WriteProperty("page", dataQuery.Page);
                        writer.WriteProperty("totalRows", totalCount);
                        // TODO FIXME this doesn't account the empty values
                        while (skip > 0 && reader.Read())
                        {
                            if (reader.FieldCount > 1 || !reader.IsDBNull(0))
                            {
                                skip--;
                            }
                        }
                        int count = 0;
                        while ((limit < 0 || limit > count) && reader.Read())
                        {
                            if (columns.Count == 1 && validator != null)
                            {
                                // we skip empty values when we have one column
                                string result = DataReaderHelper.GetString(reader, 0);
                                if (validator.IsValidLocalCode(result, columns[0]))
                                {
                                    writer.WriteRow(result);
                                    count++;
                                }
                            }
                            else
                            {
                                var len = reader.GetValues(values);
                                count++;
                                if (validator != null)
                                {
                                    for (int i = 0; i < len; i++)
                                    {
                                        validator.IsValidLocalCode(values[i], columns[i]);
                                    }
                                }

                                if (len == values.Length)
                                {
                                    writer.WriteRow(Array.ConvertAll(values, x => Convert.ToString(x, CultureInfo.InvariantCulture)));
                                }
                                else
                                {
                                    // This was translated from the original values.Take(len) 
                                    // TODO it might not possible to reach here ?
                                    object[] valuesSubset = new object[len];
                                    Array.Copy(values, valuesSubset, len);
                                    writer.WriteRow(Array.ConvertAll(valuesSubset, x => Convert.ToString(x, CultureInfo.InvariantCulture)));
                                }
                            }
                        }
                        getCodesCommand.SafeCancel();
                    }
                }
            }
        }

        public void ValidatedValues(ConnectionStringSettings connectionStringSettings, ILocalDataQuery dataQuery, ILocalCodeValidator validator)
        {
            IDdbSqlBuilder sqlBuilder = dataQuery.IsOrdered ? this._orderedSqlBuilder : this._normalDdbSqlBuilder;
            var ddbDatabase = new Database(connectionStringSettings);
            var columnNames = dataQuery.ColumnNames;
            string query = dataQuery.Query ?? dataQuery.Table;

            using (DbConnection connection = ddbDatabase.CreateConnection())
            {
                connection.Open();
                var finalQuery = sqlBuilder.Build(new QueryInfo { ColumnNames = GetColumnNames(columnNames), TableOrQuery = query });
                using (var getCodesCommand = connection.CreateCommand())
                {
                    getCodesCommand.CommandText = finalQuery;
                    getCodesCommand.CommandType = CommandType.Text;
                    getCodesCommand.CommandTimeout = 0;


                    _log.DebugFormat("Executed query {0}", getCodesCommand.CommandText);
                    using (var reader = getCodesCommand.ExecuteReader())
                    {
                        var values = new object[reader.FieldCount];
                        _log.DebugFormat("GIven column count {0}", columnNames.Count);
                        var columns = new string[reader.FieldCount];
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            columns[i] = reader.GetName(i);
                        }
                        
                        while (reader.Read())
                        {
                            var len = reader.GetValues(values);
                            for (int i = 0; i < len; i++)
                            {
                                validator.IsValidLocalCode(values[i], columns[i]);
                            }
                        }
                        getCodesCommand.SafeCancel();
                    }
                }
            }
        }
    }
}