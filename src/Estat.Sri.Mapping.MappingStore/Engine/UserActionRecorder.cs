// -----------------------------------------------------------------------
// <copyright file="UserActionRecorder.cs" company="EUROSTAT">
//   Date Created : 2019-11-12
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Utils;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStore.Store.Extension;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class UserActionRecorder
    {
        private readonly IUserActionByType _userActionByType;
        private readonly IUserActionPathAndValues _userActionPathAndValues;
        private readonly DatabaseManager _databaseManager;
        private readonly ICommmandsFromUpdateInfo _commmandsFromUpdateInfo;
        private readonly string _mappingStoreIdentifier;

        public UserActionRecorder(IUserActionByType userActionByType, IUserActionPathAndValues userActionPathAndValues, DatabaseManager databaseManager, ICommmandsFromUpdateInfo commandsFromUpdateInfo, string mappingStoreIdentifier)
        {
            if (userActionByType == null)
            {
                throw new ArgumentNullException(nameof(userActionByType));
            }

            if (userActionPathAndValues == null)
            {
                throw new ArgumentNullException(nameof(userActionPathAndValues));
            }

            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }

            _userActionByType = userActionByType;
            _userActionPathAndValues = userActionPathAndValues;
            _databaseManager = databaseManager;
            _commmandsFromUpdateInfo = commandsFromUpdateInfo;
            _mappingStoreIdentifier = mappingStoreIdentifier;
        }

        public void TryInsertUserAction(IEntity entity, UserAction userAction)
        {
            if (!_userActionByType.ShouldRecord(entity.TypeOfEntity))
            {
                return;
            }

            InsertUserAction(entity, userAction);
        }
        public void InsertUserAction(IEntity entity, UserAction userAction)
        {
            if (SingleRequestScope.EnsureActionRecorded(entity))
            {
                return;
            }
           

            var database = _databaseManager.GetDatabase(_mappingStoreIdentifier);

            Dictionary<string, object> pathAndValues = null;
            using (var context = new SingleRequestScope())
            {
                context.TrySetAuthorizationState(true);
                pathAndValues = _userActionPathAndValues.CreateFor(entity, userAction);
            }

            if (pathAndValues == null || !pathAndValues.Any())
            {
                return;
            }

            var updateInfoAction = new UpdateInfoAction
                                   {
                                       DataInformationType = DatabaseInformationType.UserAction,
                                       SqlStatementType = SqlStatementType.Insert,
                                       PathAndValues = pathAndValues
                                   };
            var commandDefinitions = _commmandsFromUpdateInfo.Get(entity.EntityId, updateInfoAction, database);
            database.ExecuteCommandsWithReturnId(commandDefinitions.ToArray());
        }

        public bool ShouldRecord(EntityType typeOfEntity)
        {
            return _userActionByType.ShouldRecord(typeOfEntity);
        }
    }
}
