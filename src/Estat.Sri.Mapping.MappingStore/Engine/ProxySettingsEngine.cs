// -----------------------------------------------------------------------
// <copyright file="ProxySettingsEngine.cs" company="EUROSTAT">
//   Date Created : --
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.Data.Common;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Extensions;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Utils.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    /// <summary></summary>
    public class ProxySettingsEngine : IProxySettingsEngine
    {
        private readonly DatabaseManager _databaseManager;

        /// <summary>Initializes a new instance of the <see cref="ProxySettingsEngine"/> class.</summary>
        /// <param name="configurationStoreManager">The configuration store manager.</param>
        public ProxySettingsEngine(IConfigurationStoreManager configurationStoreManager)
        {
            this._databaseManager = new DatabaseManager(configurationStoreManager);
        }

        /// <summary>Gets this instance.</summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        public ProxySettings Get(string sid)
        {
            var database = this._databaseManager.GetDatabase(sid);
            return database.Query<ProxySettings>("Select ENABLE_PROXY as ENABLEPROXY,AUTHENTICATION,URL,USERNAME,PASSWORD from PROXY_SETTINGS").FirstOrDefault();
        }

        /// <summary>Updates the specified settings.</summary>
        /// <param name="sid"></param>
        /// <param name="settings">The settings.</param>
        public void Update(string sid, ProxySettings settings)
        {
            Delete(sid);
            Insert(sid,settings);
        }

        /// <summary>Inserts the specified settings.</summary>
        /// <param name="sid"></param>
        /// <param name="settings">The settings.</param>
        private void Insert(string sid, ProxySettings settings)
        {
            var database = this._databaseManager.GetDatabase(sid);

            if (database.ProviderName.Equals(MappingStoreDefaultConstants.OracleManagedProviderOdp))
            {
                database.UsingLogger().Execute(
                    "INSERT INTO PROXY_SETTINGS (ID, ENABLE_PROXY, URL, AUTHENTICATION, USERNAME, PASSWORD) VALUES ({0}, {1}, {2}, {3}, {4}, {5})",
                    new Int64Parameter(1),
                    new Int64Parameter(settings.EnableProxy ? 1 : 0),
                    new AnsiString(string.IsNullOrWhiteSpace(settings.URL) ? null : settings.URL),
                    new Int64Parameter(settings.Authentication ? 1 : 0),
                    new AnsiString(string.IsNullOrWhiteSpace(settings.Username) ? null : settings.Username),
                    new AnsiString(string.IsNullOrWhiteSpace(settings.Password) ? null : settings.Password)); 
            }
            else
            {
                var dbCommand = database.CreateCommand(CommandType.StoredProcedure, "INSERT_PROXY_SETTINGS");
                AddParameter(database, dbCommand, "p_enable_proxy", settings.EnableProxy ? "1" : "0");
                AddParameter(database, dbCommand, "p_authentication", settings.Authentication ? "1" : "0");
                AddParameter(database, dbCommand, "p_password", string.IsNullOrWhiteSpace(settings.Password) ? null : settings.Password);
                AddParameter(database, dbCommand, "p_username", string.IsNullOrWhiteSpace(settings.Username) ? null : settings.Username);
                AddParameter(database, dbCommand, "p_url", string.IsNullOrWhiteSpace(settings.URL) ? null : settings.URL);
                var dbParameter = dbCommand.CreateParameter();
                dbParameter.ParameterName = "p_pk";
                dbParameter.DbType = DbType.Int32;
                dbParameter.Direction = ParameterDirection.Output;
                dbCommand.Parameters.Add(dbParameter);

                using (var connection = database.CreateConnection())
                {
                    connection.Open();
                    dbCommand.Connection = connection;
                    dbCommand.ExecuteNonQuery();
                }
            }
        }

        private void AddParameter(Database database,DbCommand dbCommand,string name, object value)
        {
            var dbType = value == null ? DbType.String:DatabaseConstants.GetDbType(value.GetType());
            database.AddInParameter(dbCommand,name, dbType, value);
        }
        /// <summary>Deletes this instance.</summary>
        /// <param name="sid"></param>
        public void Delete(string sid)
        {
            var database = this._databaseManager.GetDatabase(sid);
            database.ExecuteNonQuery("Delete from PROXY_SETTINGS", new DbParameter[0]);
        }
    }
}