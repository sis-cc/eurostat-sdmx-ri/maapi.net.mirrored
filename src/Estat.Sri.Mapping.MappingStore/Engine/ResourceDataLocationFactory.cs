// -----------------------------------------------------------------------
// <copyright file="ResourceDataLocationFactory.cs" company="EUROSTAT">
//   Date Created : 2017-04-27
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Msdb.Sql;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// A factory for creating a <see cref="IReadableDataLocation"/> for resources.
    /// </summary>
    public class ResourceDataLocationFactory
    {
        private readonly ResourceDataLocation _resourceDatalocation;
        /// <summary>
        /// The assembly
        /// </summary>
        private readonly Assembly _assembly;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceDataLocationFactory" /> class.
        /// </summary>
        public ResourceDataLocationFactory()
        {
            this._resourceDatalocation = new ResourceDataLocation();
            this._assembly = this._resourceDatalocation.ResourceAssembly;
        }

        /// <summary>
        /// Gets the available resources.
        /// </summary>
        /// <param name="childNamespace">The child namespace.</param>
        /// <returns>The available resources under the specified <paramref name="childNamespace"/></returns>
        public IEnumerable<string> GetAvailableResources(string childNamespace)
        {
            return this._resourceDatalocation.GetAvailableResources(childNamespace);
        }

        /// <summary>
        /// Determines if a resource exists
        /// </summary>
        /// <param name="location">The location.</param>
        /// <returns><c>true</c> if it exists; otherwise <c>false</c>. </returns>
        public bool Exists(string location)
        {
            return this._resourceDatalocation.Exists(location);
        }

        /// <summary>
        /// Gets the data location.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <returns>The <see cref="IReadableDataLocation"/> pointing to the resource.</returns>
        public IReadableDataLocation GetDataLocation(string location)
        {
            
            return new ResourceReadableDataLocation(this._resourceDatalocation.GetDataLocation(location), this._assembly);
        }

        internal IEnumerable<IReadableDataLocation> GetDataLocations(string resourcePath)
        {
            foreach(var resource in _resourceDatalocation.GetDataLocations(resourcePath))
            {
                yield return new ResourceReadableDataLocation(resource, this._assembly);
            }
        }

        /// <summary>
        /// A <see cref="IReadableDataLocation"/> implementation for Resources
        /// </summary>
        /// <seealso cref="Org.Sdmxsource.Util.Io.BaseReadableDataLocation" />
        private class ResourceReadableDataLocation : BaseReadableDataLocation
        {
            /// <summary>
            /// The assembly
            /// </summary>
            private readonly Assembly _assembly;

            /// <summary>
            /// Initializes a new instance of the <see cref="ResourceReadableDataLocation"/> class.
            /// </summary>
            /// <param name="name">The name.</param>
            /// <param name="assembly">The assembly.</param>
            public ResourceReadableDataLocation(string name, Assembly assembly)
            {
                this._assembly = assembly;
                this.Name = name;
            }

            /// <summary>
            /// Gets a guaranteed new input stream on each Property call.
            ///                 The input stream will be reading the same underlying data source.
            /// </summary>
            public override Stream InputStream
            {
                get
                {
                    var manifestResourceStream = this._assembly.GetManifestResourceStream(this.Name);
                    if (manifestResourceStream != null)
                    {
                        this.AddDisposable(manifestResourceStream);
                    }
                    else
                    {
                        throw new ResourceNotFoundException($"could not open stream for {Name}");
                    }

                    return manifestResourceStream;
                }
            }
        }
    }
}