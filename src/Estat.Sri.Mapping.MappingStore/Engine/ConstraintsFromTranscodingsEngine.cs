// -----------------------------------------------------------------------
// <copyright file="ConstraintsFromTranscodingsEngine.cs" company="EUROSTAT">
//   Date Created : 2019-03-04
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Factory;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    /// <summary>
    /// Class that creates constraints from transcodings
    /// </summary>
    public class ConstraintsFromTranscodingsEngine : IConstraintsFromTranscodingsEngine
    {
        private readonly IEntityRetrieverManager _entityRetrieverManager;
        private readonly IStructureSubmitter _structureSubmitter;
        private readonly IRetrieverManager _retrieverManager;
        private readonly DatabaseManager _databaseManager;

        /// <summary>Initializes a new instance of the <see cref="ConstraintsFromTranscodingsEngine"/> class.</summary>
        /// <param name="entityRetrieverManager">The entity retriever manager.</param>
        /// <param name="structureSubmitter"></param>
        /// <param name="retrieverManager"></param>
        public ConstraintsFromTranscodingsEngine(IEntityRetrieverManager entityRetrieverManager,
            IStructureSubmitter structureSubmitter, IRetrieverManager retrieverManager,DatabaseManager databaseManager)
        {
            _entityRetrieverManager = entityRetrieverManager;
            _structureSubmitter = structureSubmitter;
            _retrieverManager = retrieverManager;
            _databaseManager = databaseManager;
        }

        /// <summary>
        /// Creates for.
        /// </summary>
        /// <param name="sid">The sid.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="actual"></param>
        /// <param name="componentId"></param>
        /// <param name="sdmxSchema"></param>
        /// <returns></returns>
        public bool CreateOrUpdateFor(string sid, string id,bool actual, string componentId = null,
            SdmxSchemaEnumType sdmxSchema = SdmxSchemaEnumType.VersionTwoPointOne)
        {
            var mappingSetEntity = _entityRetrieverManager.GetEntities<MappingSetEntity>(sid, new EntityQuery
            {
                EntityId = new Criteria<string>(OperatorType.Exact, id)
            }, Detail.Full).FirstOrDefault();

            var entityQuery = new EntityQuery
            {
                ParentId = new Criteria<string>(OperatorType.Exact, id)
            };
            var mappingsWithTranscoding = _entityRetrieverManager.GetEntities<ComponentMappingEntity>(sid, entityQuery, Detail.Full)
                .Where(item => item.HasTranscoding());
            var timeMappings = _entityRetrieverManager.GetEntities<TimeDimensionMappingEntity>(sid, entityQuery, Detail.Full);

            var dataflowStructureReference = new StructureReferenceImpl(mappingSetEntity.ParentId);

            var isPerMappingSet = string.IsNullOrWhiteSpace(componentId);
            var components = isPerMappingSet
                ? mappingsWithTranscoding
                : mappingsWithTranscoding.Where(item => item.Component.ObjectId == componentId);
            var dsd = GetDsd(sid, mappingSetEntity.ParentId);

            return CreateOrUpdateContentContraint(sid, components, timeMappings, mappingSetEntity,
                dataflowStructureReference, SdmxSchema.GetFromEnum(sdmxSchema), isPerMappingSet,actual, dsd);
        }

        private IDataStructureMutableObject GetDsd(string sid,string dataflow)
        {
            var database = _databaseManager.GetDatabase(sid);
            var retrievalContainer = new RetrievalEngineContainerFactory().GetRetrievalEngineContainer(database);
            IDataStructureMutableObject dsd = new Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure.DataStructureMutableCore();

            var structure = new StructureReferenceImpl(dataflow);
            if (structure == null)
            {
                throw new ArgumentNullException("structureReference");
            }

            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
               .NewQuery(CommonStructureQueryType.Other, null)
               .SetStructureIdentification(structure)
               .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
               .Build();

            var _retrievalEng = retrievalContainer.GetEngine(structure.MaintainableStructureEnumType);
            var structureReferenceList = _retrievalEng.RetrieveResolvedParents(structureQuery).ToList();

            foreach (var struRef in structureReferenceList)
            {
                if (struRef.MaintainableStructureEnumType == SdmxStructureEnumType.Dsd)
                {
                    ICommonStructureQuery structureQueryC = CommonStructureQueryCore.Builder
                       .NewQuery(CommonStructureQueryType.Other, null)
                       .SetStructureIdentification(struRef)
                       .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                       .Build();

                    var maintainableObj = retrievalContainer.GetEngine(struRef.MaintainableStructureEnumType)
                                                                .Retrieve(structureQueryC).FirstOrDefault();

                    if (maintainableObj != null)
                        dsd = maintainableObj.ImmutableInstance.MutableInstance as IDataStructureMutableObject;
                }
            }

            return dsd;
        }
        private bool CreateOrUpdateContentContraint(string sid,
            IEnumerable<ComponentMappingEntity> components,
            IEnumerable<TimeDimensionMappingEntity> timeMappings, IIdentifiableEntity mappingSetEntity,
            IStructureReference dataflowStructureReference, SdmxSchema sdmxSchema, bool isPerMappingSet,bool actual,
            IDataStructureMutableObject dsd)
        {
            var actualId = actual ? "ACTUAL_" : "ALLOWED_";
            var newEntity = false;
            var contentConstraintReference = new StructureReferenceImpl()
            {
                AgencyId = dataflowStructureReference.AgencyId,
                MaintainableId = "CONS_" + actualId + dataflowStructureReference.MaintainableId,
                Version = dataflowStructureReference.Version,
                TargetStructureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ContentConstraint)
            };

            IContentConstraintMutableObject contentConstraint = null;
            if (!isPerMappingSet)
            {
                contentConstraint = GetContentConstraint(sdmxSchema, contentConstraintReference);
            }

            if (contentConstraint == null || contentConstraint.IsDefiningActualDataPresent != actual)
            {
                newEntity = true;
                contentConstraint = new ContentConstraintMutableCore
                {
                    Id = "CONS_" + actualId + dataflowStructureReference.MaintainableId,
                    AgencyId = dataflowStructureReference.AgencyId,
                    Version = dataflowStructureReference.Version,
                    Names =
                    {
                        new TextTypeWrapperMutableCore("en",
                            $"Autogenerated for dataflow {mappingSetEntity.ParentId}")
                    },
                    IsDefiningActualDataPresent = actual
                    
                };
            }

            if (contentConstraint.IncludedCubeRegion == null)
            {
                contentConstraint.IncludedCubeRegion = new CubeRegionMutableCore();
            }

            foreach (var entity in components)
            {
                if (entity.Component == null)
                {
                    continue;
                }

                var transcodingRules = _entityRetrieverManager.GetEntities<TranscodingRuleEntity>(sid,
                    new EntityQuery { ParentId = new Criteria<string>(OperatorType.Exact, entity.EntityId) }, 
                    Detail.Full).ToList();

                if (transcodingRules.Any())
                {
                    IKeyValuesMutable keyValues = new KeyValuesMutableImpl();
                    foreach (var transcodingRuleEntity in transcodingRules)
                    {
                        keyValues.Id = entity.Component.ObjectId;
                        // first try the array transcodings
                        transcodingRuleEntity.DsdCodeEntities.ToList().ForEach(c => keyValues.AddValue(c.ObjectId));
                        // then the scalar ones
                        string scalarValue = transcodingRuleEntity.DsdCodeEntity == null
                            ? transcodingRuleEntity.UncodedValue
                            : transcodingRuleEntity.DsdCodeEntity.ObjectId;
                        if (!string.IsNullOrEmpty(scalarValue))
                        {
                            keyValues.AddValue(scalarValue);
                        }
                    }
                    AddToCubeRegion(contentConstraint, keyValues, dsd);
                }
            }

            foreach (var timeMapping in timeMappings)
            {
                if (timeMapping.Transcoding == null)
                {
                    continue;
                }

                var contentConstraintMutableObject = GetTimePeriodContentConstraint(mappingSetEntity.ParentId, sdmxSchema);
                var timeRange = contentConstraintMutableObject.IncludedCubeRegion.KeyValues.FirstOrDefault(x => x.Id == "TIME_PERIOD");
                if (timeRange != null)
                {
                    AddToCubeRegion(contentConstraint, timeRange, dsd);
                }
            }

            var contentConstraintAttachmentMutableCore = new ContentConstraintAttachmentMutableCore();
            contentConstraintAttachmentMutableCore.AddStructureReference(dataflowStructureReference);
            contentConstraint.ConstraintAttachment = contentConstraintAttachmentMutableCore;
            var sdmxObjectsImpl = new SdmxObjectsImpl();
            sdmxObjectsImpl.AddContentConstraintObject(contentConstraint.ImmutableInstance);
            _structureSubmitter.SubmitStructures(sid, sdmxObjectsImpl);
            return newEntity;
        }

        private void AddToCubeRegion(IContentConstraintMutableObject contentConstraint, IKeyValuesMutable keyValuesMutable,IDataStructureMutableObject dsd)
        {
            if (dsd.GetAttribute(keyValuesMutable.Id) != null)
            {
                var attributeValues = contentConstraint.IncludedCubeRegion.AttributeValues;
                attributeValues.Remove(attributeValues.SingleOrDefault(item => item.Id == keyValuesMutable.Id));
                attributeValues.Add(keyValuesMutable);
            }
            else
            {
                var keyValues = contentConstraint.IncludedCubeRegion.KeyValues;
                keyValues.Remove(keyValues.SingleOrDefault(item => item.Id == keyValuesMutable.Id));
                keyValues.Add(keyValuesMutable);
            }
        }


        private IContentConstraintMutableObject GetTimePeriodContentConstraint(string parentId, SdmxSchema sdmxSchema)
        {
            var restQuery = CreateRestAvailableConstraintQuery(parentId);
            var sdmxObjects =
                _retrieverManager.ParseRequest(sdmxSchema, restQuery);
            return sdmxObjects.ContentConstraintObjects.First().MutableInstance;
        }

        private static RestAvailableConstraintQuery CreateRestAvailableConstraintQuery(string parentId)
        {
            var dataflowReference = new StructureReferenceImpl(parentId);
            var restQuery = new RestAvailableConstraintQuery(
                $"/availableconstraint/{dataflowReference.AgencyId},{dataflowReference.MaintainableId},{dataflowReference.Version}/ALL/ALL/TIME_PERIOD",
                null);
            return restQuery;
        }

        private IContentConstraintMutableObject GetContentConstraint(SdmxSchema sdmxSchema, StructureReferenceImpl contentConstraint)
        {
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, null)
                .SetStructureIdentification(contentConstraint)
                .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                .Build();
            try
            {
                var sdmxObjects =
                    _retrieverManager.ParseRequest(structureQuery);
                return sdmxObjects.ContentConstraintObjects.First().MutableInstance;
            }
            catch (SdmxNoResultsException)
            {
                return null;
            }
        }
    }
}