using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class LocalCodePersistenceEngine: EntityPersistenceEngine<LocalCodeEntity>
    {
        /// <summary>
        /// The mapping store database
        /// </summary>
        private readonly Database _mappingStoreDatabase;
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityPersistenceEngine{TEntity}"/> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        /// <param name="updateInfoFactoryManager">The update information factory manager.</param>
        /// <param name="mappingStoreIdentifier"></param>
        /// <param name="entityPropertiesExtractor"></param>
        /// <param name="commmandsFromUpdateInfo"></param>
        public LocalCodePersistenceEngine(DatabaseManager databaseManager, UpdateInfoFactoryManager updateInfoFactoryManager, string mappingStoreIdentifier, IEntityPropertiesExtractor entityPropertiesExtractor, ICommmandsFromUpdateInfo commmandsFromUpdateInfo)
            : base(databaseManager, updateInfoFactoryManager, mappingStoreIdentifier, entityPropertiesExtractor, commmandsFromUpdateInfo)
        {
            this._mappingStoreDatabase = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
        }

        public override IEnumerable<long> Add(IList<LocalCodeEntity> entities)
        {
            using (var connection = this._databaseManager.GetDatabase(this._mappingStoreIdentifier).CreateConnection())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {

                    // store the new local codes
                    var lastInsertIds = AddNewLocalCode(connection, transaction,entities.Cast<LocalCodeEntity>()).ToArray();
                    
                    transaction.Commit();

                    return lastInsertIds;
                }
            }
        }

        public override long Add(LocalCodeEntity entity)
        {
            return this.Add(new []{ entity }).FirstOrDefault();
        }

        private static IEnumerable<long> AddNewLocalCode(IDbConnection connection, IDbTransaction transaction, IEnumerable<LocalCodeEntity> localCodeEntities)
        {
            var listOfParameters = new List<DynamicParameters>();
            var newLocalCodes = new List<ISimpleNameableEntity>();
            foreach (var localCode in localCodeEntities)
            {
                long parentId = localCode.GetParentId();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("p_id", localCode.ObjectId, DbType.AnsiString, ParameterDirection.Input); // TODO change to DbType.String if LOCAL_CODE will include new script
                parameters.Add("p_column_id", parentId, DbType.Int64, ParameterDirection.Input);
                parameters.Add("p_pk", dbType: DbType.Int64, direction: ParameterDirection.Output);
                listOfParameters.Add(parameters);
                newLocalCodes.Add(localCode);
            }

            int addedCodes = connection.Execute("INSERT_LOCAL_CODE", listOfParameters, transaction, commandType: CommandType.StoredProcedure);
            for (int i = 0; i < listOfParameters.Count; i++)
            {
                var lastInsertId = listOfParameters[i].Get<long>("p_pk");
                newLocalCodes[i].EntityId = lastInsertId.ToString(CultureInfo.InvariantCulture);
                yield return lastInsertId;
            }
        }

        public override void DeleteChildren(string parentId,EntityType childrenEntityType)
        {
            var db = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            using (var connection = db.CreateConnection())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    var ruleParameterName = this._mappingStoreDatabase.BuildParameterName("COLUMN_ID");

                    var parameters = new DynamicParameters();
                    parameters.Add(ruleParameterName, parentId.AsMappingStoreEntityId(), DbType.Int64);

                    var localCodesIds = db.Query<int>($"SELECT LCD_ID FROM LOCAL_CODE WHERE COLUMN_ID={ruleParameterName}", parameters);

                    connection.Execute($"DELETE FROM LOCAL_CODE WHERE COLUMN_ID={ruleParameterName}", parameters, transaction);

                    var localCodesIdsString = localCodesIds.Select(x => x.ToString()).Aggregate((i, j) => i + "," + j).TrimEnd(',');
                    connection.Execute($"DELETE FROM ITEM WHERE ITEM_ID IN ({localCodesIdsString})",null,transaction);

                    transaction.Commit();
                }
            }
        }
    }
}