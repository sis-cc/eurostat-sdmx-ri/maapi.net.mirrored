// -----------------------------------------------------------------------
// <copyright file="DataSetImportExportEntity.cs" company="EUROSTAT">
//   Date Created : 2017-07-31
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Extension;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class DataSetImportExportEntity : IImportExportEntity
    {
        public EntityType EntityType => EntityType.DataSet;

        public string RootName { get; set; } = "Dataset";

        public List<IEntity> Read(XmlReader xmlReader, string id)
        {
            var element = (XElement)XNode.ReadFrom(xmlReader);
            return new List<IEntity>()
            {
                new DatasetEntity
                {
                    EntityId = element.Attribute("id")?.Value,
                    EditorType = element.Attribute("editor")?.Value,
                    Name = element.Descendant("Name")?.Value,
                    Description = element.Descendant("Description")?.Value,
                    ParentId = element.Descendant("Connection")?.Descendant("Ref")?.Attribute("id")?.Value,
                    Query = element.Descendant("SqlDataset")?.Value,
                    OrderByClause = element.Descendant("OrderByClause")?.Value,
                    JSONQuery = element.Descendant("XmlDataset")?.Value
                }
            };
        }

        public void Write(XmlWriter xmlWriter, EntitiesByType entities)
        {
            xmlWriter.WriteStartElement("Datasets");
            foreach (var datasetEntity in entities.GetEntities(EntityType.DataSet).Cast<DatasetEntity>())
            {
                xmlWriter.WriteStartElement(this.RootName);
                xmlWriter.WriteAttributeString("id", datasetEntity.EntityId);
                xmlWriter.WriteAttributeString("editor", datasetEntity.EditorType);
                xmlWriter.WriteElementString("Name", datasetEntity.Name);
                xmlWriter.WriteElementString("Description", datasetEntity.Description);
                this.WriteConnectionReference(xmlWriter, datasetEntity.ParentId);
                xmlWriter.WriteElementString("SqlDataset", datasetEntity.Query);
                xmlWriter.WriteElementString("OrderByClause", datasetEntity.OrderByClause);
                xmlWriter.WriteElementString("XmlDataset", datasetEntity.JSONQuery);
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
        }

        private void WriteConnectionReference(XmlWriter xmlWriter, string parentId)
        {
            xmlWriter.WriteStartElement("Connection");
            xmlWriter.WriteStartElement("Ref");
            xmlWriter.WriteAttributeString("id", parentId);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
        }
    }
}