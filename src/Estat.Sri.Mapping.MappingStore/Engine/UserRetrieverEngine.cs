// -----------------------------------------------------------------------
// <copyright file="UserRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-06-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sdmxsource.Extension.Constant;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Util;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class UserRetrieverEngine : IEntityRetrieverEngine<UserEntity>
    {
        private readonly Database _databaseManager;
        private readonly IPermissionRetriever _permissionRetriever;

        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;
        private readonly IDictionary<string, string> _integerAdditionalProperties;

        public UserRetrieverEngine(Database databaseManager, IPermissionRetriever permissionRetriever)
        {
            this._databaseManager = databaseManager;
            this._permissionRetriever = permissionRetriever;
            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }

            this._integerAdditionalProperties = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                {nameof(UserEntity.UserName), "USERMASK"},
            };
            this._databaseManager = databaseManager;
            this._fieldsToReturn = new Dictionary<Detail, IList<string>>();
            this._fieldsToReturn.Add(Detail.Full, new[]
            {
                "ID as ENTITYID", "USERMASK as USERNAME","DATASPACE","ISGROUP","ARTEFACTTYPE","ARTEFACTID","ARTEFACTAGENCYID","ARTEFACTVERSION","FULL_PATH_CHILD as FULLPATHCHILD","PERMISSION",
                "EDITEDBY","EDITDATE"
            });
            this._fieldsToReturn.Add(Detail.IdOnly, new[] { "ID as ENTITYID" });
            this._fieldsToReturn.Add(Detail.IdAndName, new[] { "ID as ENTITYID", "USERMASK as USERNAME" });
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<UserEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var db = this._databaseManager;
            var whereClauseBuilder = new WhereClauseBuilder(db);

            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId, "USERMASK"));
            foreach (var integerAdditionalProperty in this._integerAdditionalProperties)
            {
                ICriteria<string> additionalCriteria;
                if (query.AdditionalCriteria.TryGetValue(integerAdditionalProperty.Key, out additionalCriteria))
                {
                    clauses.Add(whereClauseBuilder.Build(additionalCriteria, integerAdditionalProperty.Value));
                }
            }
            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3}", fields, "AUTHORIZATIONRULES", whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);
            var rows = db.Query(sqlQuery, param);
            List<UserEntity> users = new List<UserEntity>();
            if (rows.Any())
            {
                foreach (var group in rows.GroupBy(x=>x.USERNAME))
                {
                    var userEntity = new UserEntity()
                    {
                        UserName = group.Key,
                    };
                    foreach (var row in group)
                    {
                        
                        if (detail == Detail.Full)
                        {
                            userEntity.Roles = GetRoles(row.PERMISSION);
                            userEntity.EditedBy = row.EDITEDBY;
                            userEntity.EditDate = row.EDITDATE;
                            userEntity.IsGroup = Convert.ToBoolean(row.ISGROUP);
                            userEntity.Dataspace = row.DATASPACE;
                            userEntity.AddPermission(ExtractUrnFromRow(row), "read_write");
                        }
                    }
                    users.Add(userEntity);
                }
            }

            return users;
        }

        private string ExtractUrnFromRow(dynamic row)
        {
            if(row.FULLPATHCHILD != null)
            {
                return SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category).GenerateUrn(row.ARTEFACTAGENCYID, row.ARTEFACTID, row.ARTEFACTVERSION, row.FULLPATHCHILD).ToString();
            }
            else if (ObjectUtil.ValidString(row.ARTEFACTAGENCYID, row.ARTEFACTID, row.ARTEFACTVERSION))
            {
                return SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow).GenerateUrn(row.ARTEFACTAGENCYID, row.ARTEFACTID, row.ARTEFACTVERSION).ToString();
            }
            else
            {
                return "*";
            }
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }

        private IList<string> GetRoles(object usertype)
        {
            var permissionType = (PermissionType)Enum.ToObject(typeof(PermissionType), usertype);
            return (from Enum value in Enum.GetValues(typeof(PermissionType)) where permissionType.HasFlag(value) select value.ToString()).ToList();
        }
    }
}