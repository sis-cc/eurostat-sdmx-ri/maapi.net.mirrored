// -----------------------------------------------------------------------
// <copyright file="TranscodingRuleRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Dapper;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// The TRANSCODING rule retriever engine.
    /// </summary>
    public class TranscodingRuleRetrieverEngine : IEntityRetrieverEngine<TranscodingRuleEntity>
    {
        //SELECT NMWC.MAP_ID AS PARENT_ID, NVMR.RULE_ID AS ENTITY_ID, NMWC.DATASET_COLUMN_NAME, LOCAL_VALUE, SDMX_VALUE
        //from N_VALUE_MAPPING_RULES NVMR
        //INNER JOIN N_MAPPING_WITH_COLUMN NMWC ON NMWC.MAP_C_ID  = NVMR.MAP_C_ID
        private const string JoinString = "N_VALUE_MAPPING_RULES " + TranscodingRuleAlias;

        private const string TranscodingRuleAlias = "NVMR";

        private readonly Database _databaseManager;

        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;

        private readonly IDictionary<string, string> _integerAdditionalProperties;

        public TranscodingRuleRetrieverEngine(Database databaseManager)
        {
            this._databaseManager = databaseManager;
            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }
            this._integerAdditionalProperties = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                {"ParentId", "COMP_ID"},
                {"ColumnId", "COL_ID"}
            };

            this._databaseManager = databaseManager;
            this._fieldsToReturn = new Dictionary<Detail, IList<string>>();
            this._fieldsToReturn.Add(
                Detail.Full,
                new[]
                    {
                        "NVMR.RULE_ID as " + nameof(TranscodingRuleRecord.Id),
                        "NVMR.MAP_ID as " + nameof(TranscodingRuleRecord.ParentId),
                        "NVMR.DATASET_COLUMN_NAME as " + nameof(TranscodingRuleRecord.DatasetColumnName),
                        $"NVMR.LOCAL_VALUE as {nameof(TranscodingRuleRecord.LocalCodeObjectId)}",
                        $"NVMR.SDMX_VALUE as {nameof(TranscodingRuleRecord.CodeObjectId)}",
                    });
            this._fieldsToReturn.Add(Detail.IdAndName, new[] { "NVMR.RULE_ID as " + nameof(TranscodingRuleRecord.Id), "NVMR.DATASET_COLUMN_NAME as " + nameof(TranscodingRuleRecord.DatasetColumnName) });
            this._fieldsToReturn.Add(Detail.IdOnly, new[] { "NVMR.RULE_ID as " + nameof(TranscodingRuleRecord.Id)});
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<TranscodingRuleEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var db = this._databaseManager;
            var whereClauseBuilder = new WhereClauseBuilder(db);

            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "RULE_ID", TranscodingRuleAlias));
            // component mapping is the parent but if it is not convinient for MAWEB
            // we could get the transcoding rules for each column separately
            clauses.Add(whereClauseBuilder.Build(query.ParentId, "MAP_ID", TranscodingRuleAlias));

            // when we request rules for a specific parent it should return the time transcoding rules
            // because if it is not for a time transcoding it wont return them
            // but if it is for a time transcoding it will return them
            //if (string.IsNullOrWhiteSpace(query.ParentId?.Value))
            //{
            //    clauses.Add(new WhereClauseParameters("TR.TR_ID not in (select TR_ID from TIME_TRANSCODING)", new List<KeyValuePair<string, object>>()));
            //}
            // TODO PAGING !!!!! There might 1m rows here
            // Check portability of ROW_NUMBER() OVER (PARTITION 
            // http://stackoverflow.com/questions/7182325/calculate-number-of-pages-of-records-in-select-statement
            // TODO
            foreach (var integerAdditionalProperty in this._integerAdditionalProperties)
            {
                ICriteria<string> additionalCriteria;
                if (query.AdditionalCriteria.TryGetValue(integerAdditionalProperty.Key, out additionalCriteria))
                {
                    clauses.Add(whereClauseBuilder.Build(additionalCriteria.ValueConvert(), integerAdditionalProperty.Value, TranscodingRuleAlias));
                }
            }

            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3}", fields, JoinString, whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);

            var entities = new Dictionary<string, TranscodingRuleEntity>(StringComparer.Ordinal);

            var transcodingRuleRecords = db.Query<TranscodingRuleRecord>(sqlQuery, param);
            foreach (var entry in transcodingRuleRecords)
            {
                TranscodingRuleEntity entity;
                if (!entities.TryGetValue(entry.Id, out entity))
                {
                    entity = new TranscodingRuleEntity();
                    entity.EntityId = entry.Id;
                    entity.ParentId = entry.ParentId;
                    entities.Add(entity.EntityId, entity);
                }


                //simple map
                if (transcodingRuleRecords.Count() == 1)
                {
                    var dsdCode = new IdentifiableEntity(EntityType.Sdmx);
                    dsdCode.ObjectId = entry.CodeObjectId;
                    entity.DsdCodeEntity = dsdCode;
                }
                //array values
                else
                {
                    IIdentifiableEntity dsdCode = entity.DsdCodeEntities
                    .SingleOrDefault(c => c.ObjectId == entry.CodeObjectId);
                    if (dsdCode == null && entry.CodeObjectId != null)
                    {
                        dsdCode = new IdentifiableEntity(EntityType.Sdmx);
                        dsdCode.ObjectId = entry.CodeObjectId;
                        entity.DsdCodeEntities.Add(dsdCode);
                    }
                }
                

                LocalCodeEntity localCode = entity.LocalCodes
                    .SingleOrDefault(c => c.ObjectId == entry.LocalCodeObjectId && c.ParentId == entry.DatasetColumnName);
                if (localCode == null)
                {
                    localCode = new LocalCodeEntity();
                    localCode.ObjectId = entry.LocalCodeObjectId;
                    localCode.ParentId = entry.DatasetColumnName; // We need the parent in order to identify each local code column
                    entity.LocalCodes.Add(localCode);
                }
            }

            return entities.Values;
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }

        /// <summary>
        /// The TRANSCODING rule record.
        /// </summary>
        private class TranscodingRuleRecord
        {
            /// <summary>
            /// Gets or sets the identifier
            /// </summary>
            public string Id{ get; set; }

            /// <summary>
            /// Gets or sets the parent identifier
            /// </summary>
            public string ParentId{ get; set; }

            /// <summary>
            /// Gets or sets the code object identifier
            /// </summary>
            public string CodeObjectId{ get; set; }

            /// <summary>
            /// Gets or sets the local code object identifier
            /// </summary>
            public string LocalCodeObjectId{ get; set; }

            /// <summary>
            /// Gets or sets the dataset collumn name.
            /// </summary>
            public string DatasetColumnName { get; set; }
        }
    }
}