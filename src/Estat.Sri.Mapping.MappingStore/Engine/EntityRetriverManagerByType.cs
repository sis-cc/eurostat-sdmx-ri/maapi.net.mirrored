using System;
using System.Globalization;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    internal class EntityRetriverManagerByType 
    {
        private readonly DatabaseManager _databaseManager;

        private readonly IMappingStoreManager _mappingStoreManager;

        public EntityRetriverManagerByType(DatabaseManager databaseManager, IMappingStoreManager mappingStoreManager)
        {
            this._databaseManager = databaseManager;
            this._mappingStoreManager = mappingStoreManager;
        }

        /// <summary>
        /// Gets the retriever engine.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns></returns>
        public IEntityRetrieverEngine<IEntity> GetRetrieverEngine(string storeId, EntityType entityType)
        {
            if (storeId == null)
            {
                throw new ArgumentNullException(nameof(storeId));
            }

            switch (entityType)
            {
                case EntityType.MappingSet:
                    return new MappingSetFactory(this._databaseManager).GetRetrieverEngine<MappingSetEntity>(storeId);
                case EntityType.DdbConnectionSettings:
                    return new DdbConnectioSettingsRetrieverFactory(this._databaseManager).GetRetrieverEngine<DdbConnectionEntity>(storeId);
                case EntityType.DataSet:
                    return new DatasetRetrieverFactory(this._databaseManager).GetRetrieverEngine<DatasetEntity>(storeId);
                case EntityType.DataSetColumn:
                    return new DatasetColumnRetrieverFactory(this._databaseManager).GetRetrieverEngine<DataSetColumnEntity>(storeId);
                case EntityType.LocalCode:
                    return new LocalCodeRetrieverFactory(this._databaseManager).GetRetrieverEngine<LocalCodeEntity>(storeId);
                case EntityType.DescSource:
                    return new DescSourceRetrieverFactory(this._databaseManager).GetRetrieverEngine<ColumnDescriptionSourceEntity>(storeId);
                case EntityType.Mapping:
                    return new ComponentMappingRetrieverFactory(this._databaseManager,this._mappingStoreManager).GetRetrieverEngine<ComponentMappingEntity>(storeId);
                case EntityType.Transcoding:
                   return new TranscodingRetrieverFactory(this._databaseManager, this._mappingStoreManager).GetRetrieverEngine<TranscodingEntity>(storeId);
                //case EntityType.TranscodingRule:
                //    return new TranscodingRuleRetrieverFactory(this._databaseManager, this._authorizationManager).GetRetrieverEngine<TranscodingRuleEntity>(storeId);
                case EntityType.UserAction:
                    return new UserActionRetrieverFactory(this._databaseManager).GetRetrieverEngine<TranscodingRuleEntity>(storeId);
                case EntityType.TemplateMapping:
                        return new TemplateMappingRetrieverFactory(this._databaseManager).GetRetrieverEngine<TemplateMapping>(storeId);
            }

            throw new NotImplementedException(string.Format(CultureInfo.InvariantCulture, "Could not find an engine for type {0} and store {1}", entityType, storeId));
        }
    }
}