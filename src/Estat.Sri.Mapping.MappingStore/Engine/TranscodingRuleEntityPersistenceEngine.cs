// -----------------------------------------------------------------------
// <copyright file="TranscodingRuleEntityPersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2017-04-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;

using Dapper;

using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Extensions;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Utils;
using Estat.Sri.Utils.Model;
using log4net;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    /// <summary>
    /// The Trans-coding rule entity persistence engine.
    /// </summary>
    public class TranscodingRuleEntityPersistenceEngine : IEntityPersistenceEngine<TranscodingRuleEntity>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(TranscodingRuleEntityPersistenceEngine));

        private readonly EntityImporterWithIdMapping _entityImporterWithIdMapping = new EntityImporterWithIdMapping();

        /// <summary>
        /// The mapping store database
        /// </summary>
        private readonly Database _mappingStoreDatabase;

        /// <summary>
        /// Initializes a new instance of the <see cref="TranscodingRuleEntityPersistenceEngine"/> class.
        /// </summary>
        /// <param name="mappingStoreDatabase">The mapping store database.</param>
        public TranscodingRuleEntityPersistenceEngine(Database mappingStoreDatabase)
        {
            this._mappingStoreDatabase = mappingStoreDatabase;
        }

        /// <summary>
        /// Persists the specified update information.
        /// </summary>
        /// <param name="patchRequest"></param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <exception cref="System.ArgumentException">entity EntityId format not supported - entityId</exception>
        /// <remarks>
        /// It supports only replacing the id of a local code and the dsd code entity id
        /// </remarks>
        public void Update(PatchRequest patchRequest, EntityType entityType, string entityId)
        {
            long entityIdValue;
            if (!long.TryParse(entityId, NumberStyles.Integer, CultureInfo.InvariantCulture, out entityIdValue))
            {
                throw new ArgumentException("entity EntityId format not supported", nameof(entityId));
            }

            using (var connection = this._mappingStoreDatabase.CreateConnection())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    foreach (var document in patchRequest)
                    {
                        var pathSplit = document.Path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                        
                        if (document.Path.StartsWith("/local_code", StringComparison.OrdinalIgnoreCase))
                        {
                            ReplaceLocalCodeId(document, pathSplit, entityIdValue, connection, transaction);
                        }
                        else if (document.Path.StartsWith("/code", StringComparison.OrdinalIgnoreCase))
                        {
                            ReplaceCode(document, pathSplit, entityIdValue, connection, transaction);
                        }
                        else if (document.Path.StartsWith("/uncodedValue", StringComparison.OrdinalIgnoreCase))
                        {
                            ReplaceUncodedValue(document, entityIdValue, connection, transaction);
                        }
                    }

                    transaction.Commit();
                }
            }
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// The entity id
        /// </returns>
        /// <exception cref="System.ArgumentNullException">entity</exception>
        public long Add(TranscodingRuleEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            return this.Add(new[] { entity }).First();
        }

        /// <summary>
        /// Adds the specified entities.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <returns>System.Int64.</returns>
        /// <exception cref="System.ArgumentNullException">entities</exception>
        /// <exception cref="System.ArgumentException">Argument is empty collection - entities</exception>
        public IEnumerable<long> Add(IList<TranscodingRuleEntity> entities)
        {
            if (entities == null)
            {
                throw new ArgumentNullException(nameof(entities));
            }

            if (entities.Count == 0)
            {
                throw new ArgumentException("Argument is empty collection", nameof(entities));
            }

            var entitiesIds = new List<long>();
            using (var context = new ContextWithTransaction(_mappingStoreDatabase))
            {
                Add(entities, entitiesIds, context);

                context.Complete();
            }

            return entitiesIds;
        }

        private static void Add(IList<TranscodingRuleEntity> entities, List<long> entitiesIds, ContextWithTransaction context)
        {
            // group by component mapping
            var entityByParentId = entities.ToLookup(r => r.ParentId);
            foreach (var entityGroup in entityByParentId)
            {
                long componentMappingEntityId = entityGroup.Key.AsMappingStoreEntityId();
                // so we get the list of dataset columns used in this mapping
                var columnNameToMapCid = context.GetIdToEntityIdMap("select DATASET_COLUMN_NAME as ID, MAP_C_ID as ENTITY_ID from N_MAPPING_WITH_COLUMN where MAP_ID = {0} ", componentMappingEntityId);
                // we don't support Nto1 transcodings yet
                if (columnNameToMapCid.Count > 1 && entityGroup.Any(g => g.DsdCodeEntities.Count > 1))
                {
                    throw new NotImplementedException("Transcoding to array values for N to 1 component mapping is not supported yet.");
                }

                foreach (var entity in entityGroup)
                {
                    RemoveEmptyDsdCodes(entity);
                    if (!IsValidTranscodingRule(entity))
                    {
                        entitiesIds.Add(-1);
                        continue;
                    }
                    var parameters = new DynamicParameters();
                    parameters.Add("p_map_id", componentMappingEntityId , dbType: DbType.Int64);
                    parameters.Add("p_pk", dbType: DbType.Int64, direction: ParameterDirection.Output);
                    context.Connection.Execute("INSERT_N_TRANSCODING_RULE", parameters, commandType: CommandType.StoredProcedure);
                    long ruleIdPk = parameters.Get<long>("p_pk");
                    entitiesIds.Add(ruleIdPk);

                    // insert SDMX codes
                    string insertSDMXCodeSQL = "INSERT INTO N_SDMX_VALUE_RULE (RULE_ID, SDMX_VALUE) VALUES ({0}, {1})";
                    if (entity.DsdCodeEntities.Any())// first check for array values
                    {
                        foreach (var code in entity.DsdCodeEntities)
                        {
                            context.DatabaseUnderContext.Execute(insertSDMXCodeSQL, new Int64Parameter(ruleIdPk), new SimpleParameter(DbType.String, code.ObjectId));
                        }
                    }
                    else // then for simple code or uncoded
                    {
                        context.DatabaseUnderContext.Execute(insertSDMXCodeSQL, new Int64Parameter(ruleIdPk), new SimpleParameter(DbType.String, entity.DsdCodeEntity?.ObjectId ?? entity.UncodedValue));
                    }

                    foreach (var localCode in entity.LocalCodes)
                    {
                        if (columnNameToMapCid.TryGetValue(localCode.ParentId, out long dbParentId))
                        {
                            context.DatabaseUnderContext.Execute("INSERT INTO N_LOCAL_VALUE_RULE (RULE_ID, LOCAL_VALUE, MAP_C_ID) VALUES ({0}, {1}, {2})", new Int64Parameter(ruleIdPk), new SimpleParameter(DbType.String, localCode.ObjectId), new Int64Parameter(dbParentId));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Workaround as some clients might send empty Transcoding Rules
        /// </summary>
        private static bool IsValidTranscodingRule(TranscodingRuleEntity entity)
        {
            return !entity.LocalCodes.Any(l => string.IsNullOrEmpty(l.ObjectId))
                ? !string.IsNullOrEmpty(entity.DsdCodeEntity?.ObjectId) || entity.DsdCodeEntities.Count > 0
                : false;
        }

        private static void RemoveEmptyDsdCodes(TranscodingRuleEntity entity)
        {
            for (int c = entity.DsdCodeEntities.Count - 1; c >= 0; c--)
            {
                var code = entity.DsdCodeEntities[c];
                if (code == null || string.IsNullOrEmpty(code.ObjectId))
                {
                    entity.DsdCodeEntities.RemoveAt(c);
                }
            }
        }

        /// <summary>
        /// Deletes the specified entity identifier.
        /// </summary>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        public void Delete(string entityId, EntityType entityType)
        {
            using (var connection = this._mappingStoreDatabase.CreateConnection())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    var ruleParameterName = this._mappingStoreDatabase.BuildParameterName("rule_id");
                    var parameters = new DynamicParameters();
                    parameters.Add(ruleParameterName, Convert.ToInt64(entityId), DbType.Int64);
                    connection.Execute($"DELETE FROM N_TRANSCODING_RULE WHERE RULE_ID={ruleParameterName}", parameters, transaction);

                    transaction.Commit();
                }
            }
        }
       
        /// <summary>
        /// Replaces the code.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="pathSplit">The path split.</param>
        /// <param name="entityIdValue">The entity identifier value.</param>
        /// <param name="connection">The connection.</param>
        /// <param name="transaction">The transaction.</param>
        private void ReplaceCode(
            PatchDocument document,
            string[] pathSplit,
            long entityIdValue,
            DbConnection connection,
            DbTransaction transaction)
        {
            if (document.Op == "replace")
            {
                var value = new DbString() { Value = document.Value?.ToString() ?? string.Empty, IsAnsi = true };
                var newValueParam = this._mappingStoreDatabase.BuildParameterName(nameof(value));
                var transcodingRuleIdParam = this._mappingStoreDatabase.BuildParameterName(nameof(entityIdValue));
                connection.Execute($"UPDATE N_SDMX_VALUE_RULE SET SDMX_VALUE = {newValueParam} WHERE RULE_ID = {transcodingRuleIdParam}", new { value, entityIdValue }, transaction);
            }
        }

        /// <summary>
        /// Replaces the code.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="entityIdValue">The entity identifier value.</param>
        /// <param name="connection">The connection.</param>
        /// <param name="transaction">The transaction.</param>
        private void ReplaceUncodedValue(
            PatchDocument document,
            long entityIdValue,
            DbConnection connection,
            DbTransaction transaction)
        {
            if (document.Op == "replace")
            {
                var value = document.Value?.ToString();
                var itemIdParam = this._mappingStoreDatabase.BuildParameterName(nameof(value));
                var transcodingRuleIdParam = this._mappingStoreDatabase.BuildParameterName(nameof(entityIdValue));

                connection.Execute(
                    $"UPDATE N_SDMX_VALUE_RULE SET SDMX_VALUE = {itemIdParam} WHERE RULE_ID = {transcodingRuleIdParam}",
                    new { value, entityIdValue },
                    transaction);
            }
        }

        /// <summary>
        /// Replaces the local code identifier.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="pathSplit">The path split.</param>
        /// <param name="entityIdValue">The entity identifier value.</param>
        /// <param name="connection">The connection.</param>
        /// <param name="transaction">The transaction.</param>
        private void ReplaceLocalCodeId(
            PatchDocument document,
            string[] pathSplit,
            long entityIdValue,
            DbConnection connection,
            DbTransaction transaction)
        {
            if (document.Op == "replace")
            {
                var value = new DbString() { Value = document.Value?.ToString() ?? string.Empty, IsAnsi = true };
                var newValueParam = this._mappingStoreDatabase.BuildParameterName(nameof(value));
                var transcodingRuleIdParam = this._mappingStoreDatabase.BuildParameterName(nameof(entityIdValue));
                connection.Execute($"UPDATE N_LOCAL_VALUE_RULE SET LOCAL_VALUE = {newValueParam} WHERE RULE_ID = {transcodingRuleIdParam}", new { value, entityIdValue }, transaction);
            }
        }

        /// <summary>
        /// Deletes all the transcoding rules of a component mapping.
        /// </summary>
        /// <param name="entityId">The PK of the component mapping.</param>
        /// <param name="childrenEntityType">does nothing in this implementation.</param>
        public void DeleteChildren(string entityId, EntityType childrenEntityType)
        {
            using (var connection = this._mappingStoreDatabase.CreateConnection())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    var ruleParameterName = this._mappingStoreDatabase.BuildParameterName("map_id");
                    var parameters = new DynamicParameters();
                    parameters.Add(ruleParameterName, Convert.ToInt64(entityId), DbType.Int64);
                    connection.Execute($"DELETE FROM N_TRANSCODING_RULE WHERE MAP_ID = {ruleParameterName}", parameters, transaction);

                    transaction.Commit();
                }
            }
        }

        public List<StatusType> Add(IEntityStreamingReader input, IEntityStreamingWriter responseWriter)
        {
            List<StatusType> statuses = new List<StatusType>();
            while (input.MoveToNextEntity())
            {
                var entity = input.CurrentEntity;
                if (entity.Status == StatusType.Success)
                {
                    long entityId = Add((TranscodingRuleEntity)entity.Entity);
                    if (responseWriter != null)
                    {
                        entity.Entity.SetEntityId(entityId);
                        responseWriter.Write(entity.Entity);
                    }
                }
                else
                {
                    responseWriter?.WriteStatus(entity.Status, entity.Message);
                }

                statuses.Add(entity.Status);
            }

            return statuses;
        }

        public void Import(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping)
        {
            // Build dsd-code entity map
            Dictionary<long, Dictionary<string, string>> cache = new Dictionary<long, Dictionary<string, string>>();
            Dictionary<string, string> specialMappingCache = new Dictionary<string, string>(StringComparer.Ordinal);


            using (var context = new ContextWithTransaction(this._mappingStoreDatabase))
            {
                _entityImporterWithIdMapping.Import<TranscodingRuleEntity>(input, responseWriter, mapping, x =>
                {
                   var entitiesIds = new List<long>();
                    Add(new[] { x }, entitiesIds, context);
                    return entitiesIds.First();
                }, (x,y,z)=> { return true; });
                context.Complete();
            }
        }
    }
}