﻿using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public interface IUserActionEngine
    {
        void AddAction(List<string> entity, UserAction actionType);
        void AddAction(IEntity entity, UserAction actionType);
    }

    public enum UserAction
    {
        Update,
        Insert,
        Delete
    }
}