// -----------------------------------------------------------------------
// <copyright file="TimeMappingRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2022-08-01
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Model.AdvancedTime;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class TimeMappingRetrieverEngine : IEntityRetrieverEngine<TimeDimensionMappingEntity>
    {
        private const string ComponentMappingAlias = "CM";
        private readonly Database _database;
        private readonly AdvanceTimeTranscodingRetriever _advanceTimeTranscodingRetriever;

        public TimeMappingRetrieverEngine(Database database)
            {
                if (database == null)
                {
                    throw new ArgumentNullException(nameof(database));
                }

                _database = database;
                _advanceTimeTranscodingRetriever = new AdvanceTimeTranscodingRetriever(database);
            }

        public IEnumerable<TimeDimensionMappingEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            var whereClauseBuilder = new WhereClauseBuilder(_database);
            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "STR_MAP_SET_ID", ComponentMappingAlias));
            clauses.Add(whereClauseBuilder.Build(query.ParentId, "STR_MAP_SET_ID", ComponentMappingAlias));
            
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
             
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, 
                "select STR_MAP_SET_ID, OUTPUT_COLUMN, IS_FREQ_CRITERIA, CRITERIA_COL_OR_DIM, CONSTANT_OR_DEFAULT, FROM_SOURCE, TO_SOURCE from N_MAPPING_TIME_DIMENSION {0} {1} {2}",
                ComponentMappingAlias,whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);
                        var rows = _database.Query(sqlQuery, param);
                        return this.ExtractMappings(rows);
             
        }

        private IEnumerable<TimeDimensionMappingEntity> ExtractMappings(IEnumerable<dynamic> rows)
        {
            foreach (var timeMapping in rows)
            {
                TimeDimensionMappingEntity mappingEntity = new TimeDimensionMappingEntity();
                mappingEntity.SetEntityId((long)timeMapping.STR_MAP_SET_ID);
                mappingEntity.ParentId = mappingEntity.EntityId;
                if (timeMapping.FROM_SOURCE != null)
                {
                    TimePreFormattedEntity preFormattedEntity = new TimePreFormattedEntity();
                    preFormattedEntity.FromColumn = new DataSetColumnEntity() { Name = timeMapping.FROM_SOURCE};
                    preFormattedEntity.ToColumn = new DataSetColumnEntity() { Name = timeMapping.TO_SOURCE };
                    preFormattedEntity.OutputColumn = new DataSetColumnEntity() { Name = timeMapping.OUTPUT_COLUMN };
                    mappingEntity.PreFormatted = preFormattedEntity;
                    mappingEntity.DefaultValue = timeMapping.CONSTANT_OR_DEFAULT;
                    mappingEntity.TimeMappingType = TimeDimensionMappingType.PreFormatted;
                }
                else if (timeMapping.CRITERIA_COL_OR_DIM != null)
                {
                    TimeTranscodingAdvancedEntity advancedEntity = new TimeTranscodingAdvancedEntity();
                    bool isFrequency = timeMapping.IS_FREQ_CRITERIA != null && Convert.ToBoolean(timeMapping.IS_FREQ_CRITERIA);
                    if (isFrequency)
                    {
                        advancedEntity.FrequencyDimension = timeMapping.CRITERIA_COL_OR_DIM;
                        advancedEntity.IsFrequencyDimension = true;
                        mappingEntity.TimeMappingType = TimeDimensionMappingType.TranscodingWithFrequencyDimension;
                    }
                    else
                    {
                        advancedEntity.CriteriaColumn =
                            new DataSetColumnEntity() { Name = timeMapping.CRITERIA_COL_OR_DIM };
                        advancedEntity.IsFrequencyDimension = false;
                        mappingEntity.TimeMappingType = TimeDimensionMappingType.TranscodingWithCriteriaColumn;
                    }

                    advancedEntity = _advanceTimeTranscodingRetriever.Retrieve(advancedEntity, mappingEntity.GetEntityId());
                    mappingEntity.Transcoding = advancedEntity;
                    mappingEntity.DefaultValue = timeMapping.CONSTANT_OR_DEFAULT;
                }
                else if (timeMapping.OUTPUT_COLUMN != null)
                {
                    mappingEntity.SimpleMappingColumn = timeMapping.OUTPUT_COLUMN;
                    mappingEntity.DefaultValue = timeMapping.CONSTANT_OR_DEFAULT;
                    mappingEntity.TimeMappingType = TimeDimensionMappingType.MappingWithColumn;
                }
                else
                {
                    mappingEntity.ConstantValue = timeMapping.CONSTANT_OR_DEFAULT;
                    mappingEntity.TimeMappingType = TimeDimensionMappingType.MappingWithConstant;
                }

                yield return mappingEntity;
            }
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }
    }
}