// -----------------------------------------------------------------------
// <copyright file="NsiwsPersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2021-08-10
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Exceptions;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class NsiwsPersistenceEngine<TEntity> : IEntityPersistenceEngine<TEntity> where TEntity : IEntity
    {
        private readonly ICommmandsFromUpdateInfo _commmandsFromUpdateInfo;
        private readonly Database _database;
        private readonly UpdateInfoFactoryManager _updateInfoFactoryManager;
        private readonly EntityImporterWithIdMapping _entityImporterWithIdMapping = new EntityImporterWithIdMapping();

        public NsiwsPersistenceEngine(DatabaseManager databaseManager, string mappingStoreIdentifier, ICommmandsFromUpdateInfo commmandsFromUpdateInfo, UpdateInfoFactoryManager updateInfoFactoryManager)
        {
            this._database = databaseManager.GetDatabase(mappingStoreIdentifier);
            this._commmandsFromUpdateInfo = commmandsFromUpdateInfo;
            this._updateInfoFactoryManager = updateInfoFactoryManager;
        }

        public IEnumerable<long> Add(IList<TEntity> entities)
        {
            foreach (var entity1 in entities)
            {
                yield return this.Add(entity1);
            }
        }

        public long Add(TEntity entity)
        {
            return this.InsertNsiwsEntry(entity as NsiwsEntity, this._database);
        }

        public List<StatusType> Add(IEntityStreamingReader input, IEntityStreamingWriter responseWriter)
        {
            List<StatusType> statuses = new List<StatusType>();
            while (input.MoveToNextEntity())
            {
                var entity = input.CurrentEntity;
                if (entity.Status == StatusType.Success)
                {
                    long entityId = Add((TEntity)entity.Entity);
                    if (responseWriter != null)
                    {
                        entity.Entity.SetEntityId(entityId);
                        responseWriter.Write(entity.Entity);
                    }
                }
                else
                {
                    responseWriter?.WriteStatus(entity.Status, entity.Message);
                }
                statuses.Add(entity.Status);
            }

            return statuses;
        }

        public void Delete(string entityId, EntityType entityType)
        {
            var databaseMapperManager = new DatabaseMapperManager(DatabaseInformationType.Nsiws);

            var tableName = databaseMapperManager.GetTableName();
            var primaryKeyColumn = databaseMapperManager.GetPrimaryKeyColumn();
            var identityColumnsAndValues = new Dictionary<string, object>
            {
                {primaryKeyColumn, entityId}
            };
            var commandDefinition = new DeleteSqlBuilder(tableName, identityColumnsAndValues, databaseMapperManager).CreateCommandDefinition(this._database);
            this._database.ExecuteCommands(new[] { commandDefinition });
        }

        public void DeleteChildren(string entityId, EntityType childrenEntityType)
        {
            throw new System.NotImplementedException();
        }

        public void Import(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping)
        {
            _entityImporterWithIdMapping.Import<TEntity>(input, responseWriter, mapping, Add, (x, y, z) => { return true; });
        }

        public void Update(PatchRequest patchRequest, EntityType entityType, string entityId)
        {
            var updateInfo = this._updateInfoFactoryManager.GetFactory(entityType, this._database).Create(patchRequest, entityId);
            var commandDefinitions = new List<CommandDefinition>();
            foreach (var updateInfoAction in updateInfo.Actions)
            {
                var commandDefinitionManager = new CommandDefinitionManager(new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
                var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType, updateInfoAction.SqlStatementType);
                commandDefinitions.AddRange(commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, this._database));
            }

            this._database.ExecuteCommandsWithReturnId(commandDefinitions);
        }

        private long InsertNsiwsEntry(NsiwsEntity nsiwsEntity, Database database)
        {
            var headerUpdateInforAction = new UpdateInfoAction
            {
                DataInformationType = DatabaseInformationType.Nsiws,
                SqlStatementType = SqlStatementType.Insert,
                PathAndValues = new Dictionary<string, object>
                {
                    {nameof(NsiwsEntity.Url), nsiwsEntity.Url},
                    {nameof(NsiwsEntity.UserName), nsiwsEntity.UserName},
                    {nameof(NsiwsEntity.Password), nsiwsEntity.Password},
                    {nameof(NsiwsEntity.Description), nsiwsEntity.Description},
                    {nameof(NsiwsEntity.Proxy), nsiwsEntity.Proxy},
                    {nameof(NsiwsEntity.Name), nsiwsEntity.Name},
                    {nameof(NsiwsEntity.Technology), NormalizeTechnology(nsiwsEntity.Technology)}
                }
            };

            var commands = this._commmandsFromUpdateInfo.Get(string.Empty, headerUpdateInforAction, database).ToList();

            return database.ExecuteCommandsWithReturnId(commands);
        }
        
        private static string NormalizeTechnology(string techology)
        {
            if (string.IsNullOrWhiteSpace(techology))
            {
                throw new MissingInformationException("Technology is missing");
            }
            switch (techology.ToUpperInvariant())
            {
                case "REST":
                    return "REST";
                case "RESTV20":
                    return "RestV20";
                case "SOAPV20":
                    return "SoapV20";
                case "SOAPV21":
                    return "SoapV21";
            }

            throw new MissingInformationException("Technology not supported");
        }
    }
    
}