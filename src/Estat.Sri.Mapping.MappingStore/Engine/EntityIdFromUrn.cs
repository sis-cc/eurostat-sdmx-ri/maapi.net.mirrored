// -----------------------------------------------------------------------
// <copyright file="EntityIdFromUrn.cs" company="EUROSTAT">
//   Date Created : 2017-05-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class EntityIdFromUrn : IEntityIdFromUrn
    {
        private readonly Database _database;

        public EntityIdFromUrn(Database database)
        {
            this._database = database;
        }

        public long Get(string urn)
        {
            var structureReferenceImpl = new StructureReferenceImpl(urn);
            return new MaintainableRefRetrieverEngine(this._database, new MappingStoreRetrievalManager(new RetrievalEngineContainer(this._database))).Retrieve(structureReferenceImpl);
        }
    }
}