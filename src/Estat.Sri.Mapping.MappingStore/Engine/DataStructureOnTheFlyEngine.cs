using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using System;
using System.Collections.Generic;
using System.Linq;
using MappingSetEntity = Estat.Sri.Mapping.Api.Model.MappingSetEntity;
using MappingStoreManager = Estat.Sri.MappingStore.Store.Manager.MappingStoreManager;
using TranscodingEntity = Estat.Sri.Mapping.Api.Model.TranscodingEntity;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using Estat.Sri.MappingStoreRetrieval.Constants;
using Estat.Sri.MappingStoreRetrieval.Model;
using Estat.Sri.Mapping.MappingStore.Engine.Cloning;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Estat.Sri.Mapping.Api.Extension;
using System.Runtime.CompilerServices;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Mapping.Api.Manager;
using System.Security.Cryptography;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class DataStructureOnTheFlyEngine : Api.Engine.IDataStructureOnTheFlyEngine
    {
        private readonly DatabaseManager _databaseManager;
        private readonly DefaultsEntityRetrieverEngine<DataSetColumnEntity> _datasetSetColumnRetriver;
        private readonly MappingSetPersistenceEngine _mappingSetPersistenceEngine;
        private readonly ComponentMappingPersistenceEngine _componentMappingPersistenceEngine;
        private readonly TranscodingRuleEntityPersistenceEngine _transcodingRuleEntityPersistenceEngine;
        private readonly MappingSetEntityRetriever _mappingSetEntityRetriever;
        private readonly ComponentMappingRetrieverEngine _componentMappingRetriever;
        private readonly TranscodingRuleRetrieverEngine _transcodingRuleRetrieverEngine;
        private readonly EntityCloneHelper _entityCloneHelper;
        private readonly ICommonSdmxObjectRetrievalFactory _commonSdmxObjectRetrievalFactory;
        private readonly IEntityRetrieverManager _retrieveManager;
        private readonly IEntityPersistenceManager _entityPersistenceManager;

        public DataStructureOnTheFlyEngine(DatabaseManager databaseManager, DefaultsEntityRetrieverEngine<DataSetColumnEntity> datasetSetColumnRetriver, MappingSetPersistenceEngine mappingSetPersistenceEngine, ComponentMappingPersistenceEngine componentMappingPersistenceEngine,TranscodingRuleEntityPersistenceEngine transcodingRuleEntityPersistenceEngine
            , MappingSetEntityRetriever mappingSetEntityRetriever, ComponentMappingRetrieverEngine componentMappingRetriever,
            TranscodingRuleRetrieverEngine transcodingRuleRetrieverEngine, EntityCloneHelper entityCloneHelper, ICommonSdmxObjectRetrievalFactory commonSdmxObjectRetrievalFactory,
            IEntityRetrieverManager retrieveManager, IEntityPersistenceManager entityPersistenceManager)
        {
            this._databaseManager = databaseManager;
            this._datasetSetColumnRetriver = datasetSetColumnRetriver;
            this._mappingSetPersistenceEngine = mappingSetPersistenceEngine;
            this._componentMappingPersistenceEngine = componentMappingPersistenceEngine;
            this._transcodingRuleEntityPersistenceEngine = transcodingRuleEntityPersistenceEngine;
            this._mappingSetEntityRetriever = mappingSetEntityRetriever;
            this._componentMappingRetriever = componentMappingRetriever;
            this._transcodingRuleRetrieverEngine = transcodingRuleRetrieverEngine;
            this._entityCloneHelper = entityCloneHelper;
            this._commonSdmxObjectRetrievalFactory = commonSdmxObjectRetrievalFactory;
            this._retrieveManager = retrieveManager;
            this._entityPersistenceManager = entityPersistenceManager;
        }

        public void Add(string sid,DSDOnTheFlyEntity dSDOnTheFlyEntity)
        {
            var dataflowUrn = this.CreateAndSaveDSD(sid, dSDOnTheFlyEntity.DsdId, dSDOnTheFlyEntity.Mappings, dSDOnTheFlyEntity.DsdNames, dSDOnTheFlyEntity.Dataset);
            this.CreateAndSaveMappingSetAndChildEntities(sid,dSDOnTheFlyEntity.Mappings, dSDOnTheFlyEntity.Dataset, dataflowUrn, dSDOnTheFlyEntity.DsdId);
        }


        public void Delete(string sid,StructureReferenceImpl dsd)
        {
            var database = this._databaseManager.GetConnectionStringSettings(new DatabaseIdentificationOptions { StoreId = sid });
            var storeManager = new MappingStoreManager(database, new List<ArtefactImportStatus>());
            var dsdStructure = GetDSDByAnnotation(sid, dsd);
            if(dsdStructure == null)
            {
                return;
            }
            var mappingSet = _mappingSetEntityRetriever.GetEntities(new EntityQuery() { ParentId = new Criteria<string>(OperatorType.Exact, new DataflowMutableCore(dsdStructure).Urn.ToString()) }, Detail.Full).FirstOrDefault();
            if (mappingSet != null)
            {
                this._mappingSetPersistenceEngine.Delete(mappingSet.EntityId, EntityType.MappingSet);
            }

            ISdmxObjects objects = new SdmxObjectsImpl();
            var conceptScheme = new ConceptSchemeMutableCore() { Id = dsd.MaintainableId, Version = dsd.Version, AgencyId = dsd.AgencyId ,FinalStructure = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True) };
            conceptScheme.AddName("en", "en");
     
            var dataflow = new DataflowMutableCore(dsdStructure) { FinalStructure = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True) }.ImmutableInstance;
            objects.AddIdentifiable(dsdStructure);
            objects.AddIdentifiable(dataflow);
            objects.AddIdentifiable(conceptScheme.ImmutableInstance);
            storeManager.DeleteStructures(objects);

        }

        public DSDOnTheFlyEntity GetByAnnotation(string sid,StructureReferenceImpl dsd)
        {
            var dsdStructure = GetDSDByAnnotation(sid,dsd);
            if(dsdStructure == null)
            {
                return null;
            }
            var dataset = new SimpleNameableEntity(EntityType.DataSet)
                {
                    Name = dsdStructure.Annotations.First(x => !string.IsNullOrWhiteSpace(x.Title)).Title
                };

                DSDOnTheFlyEntity dsdOnTheFlyEntity = new DSDOnTheFlyEntity()
                {
                    DsdId = new StructureReferenceImpl(dsdStructure),
                    Dataset = dataset,
                    DsdNames = dsdStructure.Names.ToDictionary(x => x.Locale, x => x.Value),
                    Mappings = new List<TemplateMapping>()
                };
                var mappingSet = _mappingSetEntityRetriever.GetEntities(new EntityQuery() { ParentId = new Criteria<string>(OperatorType.Exact,new DataflowMutableCore(dsdStructure).Urn.ToString()) }, Detail.Full).First();
                var componentMappings = _componentMappingRetriever.GetEntities(new EntityQuery() { ParentId = new Criteria<string>(OperatorType.Exact, mappingSet.EntityId.ToString()) }, Detail.Full);
                foreach (var componentMapping in componentMappings)
                {
                    var dsdComponent = dsdStructure.GetComponent(componentMapping.Component.ObjectId);
                    var templateMapping = new TemplateMapping()
                    {
                        ColumnName = componentMapping.GetColumns()?.FirstOrDefault()?.Name,
                        ColumnDescription = componentMapping.GetColumns()?.FirstOrDefault()?.Description,
                        ComponentId = componentMapping.Component.ObjectId,
                        ComponentType = dsdComponent.StructureType.StructureType,
                        ItemSchemeId = dsdComponent.HasCodedRepresentation() ? dsdComponent.Representation.Representation : null,
                    };
                    
                    //var transcodings = _retrieveManager.GetEntities<TimeTranscodingEntity>(sid,new EntityQuery() { ParentId = new Criteria<string>(OperatorType.Exact, componentMapping.EntityId.ToString()) }, Detail.Full);
                    //templateMapping.TimeTranscoding = transcodings.ToList();
                    var transcodingRules = _transcodingRuleRetrieverEngine.GetEntities(new EntityQuery() { ParentId = new Criteria<string>(OperatorType.Exact, componentMapping.EntityId.ToString()) }, Detail.Full);
                    if (transcodingRules.Any())
                    {
                        templateMapping.Transcodings = transcodingRules.ToDictionary(x => x.DsdCodeEntity != null? x.DsdCodeEntity.ObjectId : x.UncodedValue,x => x.LocalCodes.First().ObjectId);
                    }
                    dsdOnTheFlyEntity.Mappings.Add(templateMapping);
                }

            return dsdOnTheFlyEntity;
        }

        private IDataStructureObject GetDSDByAnnotation(string sid, StructureReferenceImpl dsd)
        {
            var agency = new ComplexTextReferenceCore(null, TextSearch.GetFromEnum(TextSearchEnumType.Equal), dsd.AgencyId);
            var id = new ComplexTextReferenceCore(null, TextSearch.GetFromEnum(TextSearchEnumType.Equal), dsd.MaintainableId);
            IComplexVersionReference versionRef = new ComplexVersionReferenceCore(
               TertiaryBool.ParseBoolean(false),
               dsd.Version,
               null,
               null);

            IComplexAnnotationReference annotationRef = new ComplexAnnotationReferenceCore(
                new ComplexTextReferenceCore(null, TextSearch.GetFromEnum(TextSearchEnumType.Equal), DSDAnnotationEnum.DSDOnTheFly.ToString()),
                null,
                null);

            var complexStructureReferenceCore = new ComplexStructureReferenceCore(
                agency,
                id,
                versionRef,
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd),
                annotationRef,
                null,
                null, 
                null);
            
            var retriever = GetRetrievalManager(this._databaseManager.GetDatabase(sid));
            ICommonStructureQuery commonQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, null)
                .SetAgencyIds(dsd.AgencyId)
                .SetMaintainableIds(dsd.MaintainableId)
                .SetVersionRequests(new VersionRequestCore(dsd.Version))
                .SetAnnotationReference(annotationRef)
                .SetMaintainableTarget(SdmxStructureEnumType.Dsd)
                .Build();
            return retriever.GetMaintainables(commonQuery).DataStructures.FirstOrDefault();
        }

        /// <summary>
        /// Gets the retrieval manager.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>The SDMX retrieval manager</returns>
        private ICommonSdmxObjectRetrievalManager GetRetrievalManager(Database database)
        {
            return this._commonSdmxObjectRetrievalFactory.GetManager(database.ConnectionStringSettings);
        }

        private void CreateAndSaveMappingSetAndChildEntities(string sid,List<TemplateMapping> mappings,SimpleNameableEntity dataset,string dataflowUrn, StructureReferenceImpl dsd)
        {
            
            var mappingSetEntity = new MappingSetEntity() {DataSetId = dataset.EntityId, ParentId = dataflowUrn ,StoreId = sid,Name = dataset.Name};
            var mappingSetName = _entityCloneHelper.GetCloneName<MappingSetEntity>(mappingSetEntity, $"{dsd.AgencyId}_{dsd.MaintainableId}_{dsd.Version}_" + "{0}");
            mappingSetEntity.Name = mappingSetName;
            var mappingSetId = this._mappingSetPersistenceEngine.Add(mappingSetEntity);
            var datasetColumns = this._datasetSetColumnRetriver.GetEntities(new EntityQuery() { ParentId = new Criteria<string>(OperatorType.Exact, dataset.EntityId) }, Detail.Full);

            //create ComponentMappings
            foreach (var mapping in mappings)
            {
                var datasetColumn = datasetColumns.First(item => item.Name == mapping.ColumnName);
                var componentEntity = new ComponentMappingEntity() {
                    Component = new Component() { ObjectId = mapping.ComponentId},
                    Type = ComponentMappingType.Normal.AsMappingStoreType()
                };
                componentEntity.SetColumns(
                    new List<Api.Model.DataSetColumnEntity>()
                    {
                        datasetColumn
                    });
                componentEntity.ParentId = mappingSetId.ToString();
                var mapId = _componentMappingPersistenceEngine.Add(componentEntity);

                //create time transcodings
                var timeTranscodings = new List<TimeTranscodingEntity>();
                if(mapping.TimeTranscoding != null)
                {
                    PeriodObject codeList;
                    
                    foreach (var timeTranscoding in mapping.TimeTranscoding)
                    {
                        var timeTranscodingEntity = new TimeTranscodingEntity() { ParentId = componentEntity.EntityId };
                        if (PeriodCodelist.PeriodCodelistIdMap.TryGetValue(timeTranscodingEntity.Frequency, out codeList))
                        {
                            timeTranscodingEntity.Period.Rules = mapping.Transcodings.Select(x=>CreateRuleFrom(x, datasetColumn)).Where(x => codeList.Codes.Contains(x.DsdCodeEntity.ObjectId)).ToList();
                        }
                        timeTranscodings.Add(timeTranscodingEntity);
                    }

                    this._entityPersistenceManager.AddEntities(timeTranscodings);
                }

                
                //create TranscodinRules

                if (mapping.Transcodings != null)
                {
                    foreach (var transcoding in mapping.Transcodings)
                    {
                        var transcodingRule = new TranscodingRuleEntity()
                        {
                            ParentId = mapId.ToString(),
                        };

                        if (mapping.ItemSchemeId != null)
                        {
                            transcodingRule.LocalCodes = new List<LocalCodeEntity>() { new LocalCodeEntity() { ObjectId = transcoding.Value, ParentId = datasetColumn.EntityId } };
                            transcodingRule.DsdCodeEntity = new IdentifiableEntity(EntityType.LocalCode) { ObjectId = transcoding.Key };
                        }
                        else
                        {
                            transcodingRule.LocalCodes = new List<LocalCodeEntity>() { new LocalCodeEntity() { ObjectId = transcoding.Value, ParentId = datasetColumn.EntityId } };
                            transcodingRule.UncodedValue = mapping.Transcodings.Keys.First();
                        }

                        _transcodingRuleEntityPersistenceEngine.Add(transcodingRule);
                    }
                }
                
            }
        }

        private TranscodingRuleEntity CreateRuleFrom(KeyValuePair<string, string> x,DataSetColumnEntity datasetColumn)
        {
            return new TranscodingRuleEntity() { LocalCodes = new List<LocalCodeEntity> { new LocalCodeEntity() { ObjectId = x.Value, ParentId = datasetColumn.EntityId } }, DsdCodeEntity = new IdentifiableEntity(EntityType.LocalCode) { ObjectId = x.Key } };
        }

        private string CreateAndSaveDSD(string sid, StructureReferenceImpl dsd, List<TemplateMapping> mappings, Dictionary<string, string> dsdNames,SimpleNameableEntity dataset)
        {
            var database = this._databaseManager.GetConnectionStringSettings(new DatabaseIdentificationOptions { StoreId = sid });
            var mappingStoreManager = new MappingStoreManager(database, new List<ArtefactImportStatus>());
            ISdmxObjects objects = new SdmxObjectsImpl();
            var conceptScheme = this.BuildConceptScheme(dsd, mappings, dsdNames);
            objects.AddIdentifiable(this.BuildConceptScheme(dsd,mappings,dsdNames));
            var dsdStructure = this.BuildDataStructure(dsd, mappings, dsdNames, dataset,sid);
            objects.AddIdentifiable(dsdStructure);
            var dataflow = new DataflowMutableCore(dsdStructure) { FinalStructure = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True) }.ImmutableInstance;
            
            objects.AddIdentifiable(dataflow);

            mappingStoreManager.SaveStructures(objects);

            return dataflow.Urn.AbsoluteUri;
        }

        private IConceptSchemeObject BuildConceptScheme(StructureReferenceImpl dsd,List<TemplateMapping> mappings, Dictionary<string, string> dsdNames)
        {
            IConceptSchemeMutableObject conceptScheme = new ConceptSchemeMutableCore() { Id = dsd.MaintainableId, Version = dsd.Version, AgencyId = dsd.AgencyId };
            conceptScheme.FinalStructure = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);
            foreach (var pair in dsdNames)
            {
                conceptScheme.AddName(pair.Key, pair.Value);
            }
            foreach (var mapping in mappings)
            {
                conceptScheme.CreateItem(mapping.ComponentId, mapping.ColumnName);
            }
            return conceptScheme.ImmutableInstance;
        }

        private IDataStructureObject BuildDataStructure(StructureReferenceImpl dsdReference, List<TemplateMapping> mappings, Dictionary<string,string> dsdNames,SimpleNameableEntity dataset,string storeId)
        {
            IDataStructureMutableObject dsd = new DataStructureMutableCore();
            
            dsd.AgencyId = dsdReference.AgencyId;
            dsd.Id = dsdReference.MaintainableId;
            dsd.Version = dsdReference.Version;
            foreach(var item in dsdNames)
            {
                dsd.AddName(item.Key, item.Value);
            }

            var annotation = new Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base.AnnotationMutableCore
            {
                Type = DSDAnnotationEnum.DSDOnTheFly.ToString(),
                Title = dataset.Name
            };

            dsd.AddAnnotation(annotation);
            
            foreach (var mapping in mappings)
            {
               
                IStructureReference codelist = mapping.ItemSchemeId;
                
                SdmxStructureEnumType componentType;
                if (Enum.TryParse<SdmxStructureEnumType>(mapping.ComponentType, out componentType))
                {
                    switch (componentType)
                    {
                        case SdmxStructureEnumType.Dimension:
                            dsd.AddDimension(CreateConceptReference(dsdReference, mapping.ComponentId), codelist);
                            break;
                        case SdmxStructureEnumType.TimeDimension:
                            var dimension = dsd.AddDimension(CreateConceptReference(dsdReference, mapping.ComponentId), codelist);
                            dimension.TimeDimension = true;
                            break;
                        case SdmxStructureEnumType.PrimaryMeasure:
                            dsd.AddPrimaryMeasure(CreateConceptReference(dsdReference, mapping.ComponentId));
                            break;
                        case SdmxStructureEnumType.DataAttribute:
                            dsd.AddAttribute(CreateConceptReference(dsdReference, mapping.ComponentId), codelist);
                            break;
                        case SdmxStructureEnumType.MeasureDimension:
                            dsd.AddDimension(new DimensionMutableCore { ConceptRef = new StructureReferenceImpl(dsdReference.AgencyId, dsdReference.MaintainableId, dsdReference.Version, SdmxStructureEnumType.Concept, mapping.ComponentId) });
                            break;

                    }
                }
            }

            dsd.FinalStructure = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);

            return dsd.ImmutableInstance;
        }

        /// <summary>
        /// The create codelist reference.
        /// </summary>
        /// <param name="codelistRef"></param>
        /// <returns>
        /// The <see cref="IStructureReference"/>.
        /// </returns>
        private static IStructureReference CreateCodelistReference(IMaintainableRefObject codelistRef)
        {
            return new StructureReferenceImpl(codelistRef.AgencyId, codelistRef.MaintainableId, codelistRef.Version, SdmxStructureEnumType.CodeList);
        }

        /// <summary>
        /// The create concept reference.
        /// </summary>
        /// <param name="dsdReference"></param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IStructureReference"/>.
        /// </returns>
        private static IStructureReference CreateConceptReference(StructureReferenceImpl dsdReference,string id)
        {
            return new StructureReferenceImpl(dsdReference.AgencyId, dsdReference.MaintainableId, dsdReference.Version, SdmxStructureEnumType.Concept, id);
        }
    }
}
