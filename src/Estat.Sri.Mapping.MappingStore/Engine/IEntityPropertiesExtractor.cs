﻿using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public interface IEntityPropertiesExtractor
    {
        IEnumerable<KeyValuePair<string, object>> GetFrom(IEntity entity);
    }
}