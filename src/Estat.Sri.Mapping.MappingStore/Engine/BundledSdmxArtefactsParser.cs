﻿// -----------------------------------------------------------------------
// <copyright file="BundledSdmxArtefactsParser.cs" company="EUROSTAT">
//   Date Created : 2017-04-27
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    /// The bundled SDMX artefacts parser.
    /// </summary>
    public class BundledSdmxArtefactsParser
    {
        /// <summary>
        /// The data location factory
        /// </summary>
        private readonly ResourceDataLocationFactory _dataLocationFactory;

        /// <summary>
        /// The parsing manager
        /// </summary>
        private readonly IStructureParsingManager _parsingManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="BundledSdmxArtefactsParser"/> class.
        /// </summary>
        /// <param name="parsingManager">
        /// The parsing manager.
        /// </param>
        public BundledSdmxArtefactsParser(IStructureParsingManager parsingManager)
        {
            if (parsingManager == null)
            {
                throw new ArgumentNullException(nameof(parsingManager));
            }

            this._dataLocationFactory = new ResourceDataLocationFactory();
            this._parsingManager = parsingManager;
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <param name="location">
        /// The location.
        /// </param>
        /// <returns>
        /// The <see cref="ISdmxObjects"/>.
        /// </returns>
        public ISdmxObjects GetSdmxObjects(string location)
        {
            var sdmxObjects = new SdmxObjectsImpl();
            foreach (
                var availableResource in
                    this._dataLocationFactory.GetAvailableResources(location)
                        .Where(s => s.EndsWith("xml", StringComparison.OrdinalIgnoreCase)))
            {
                using (var readableDataLocation = this._dataLocationFactory.GetDataLocation(availableResource))
                {
                    sdmxObjects.Merge(readableDataLocation.GetSdmxObjects(this._parsingManager));
                }
            }

            return sdmxObjects;
        }
    }
}