// -----------------------------------------------------------------------
// <copyright file="UserActionWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2019-11-12
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    class UserActionWriterEngine : IEntityStreamingWriter
    {
        private readonly IEntityStreamingWriter _decoratedEngine;
        private readonly UserActionRecorder _userActionRecorder;

        public UserActionWriterEngine(IEntityStreamingWriter decoratedEngine, UserActionRecorder userActionRecorder)
        {
            if (decoratedEngine == null)
            {
                throw new ArgumentNullException(nameof(decoratedEngine));
            }

            if (userActionRecorder == null)
            {
                throw new ArgumentNullException(nameof(userActionRecorder));
            }

            _decoratedEngine = decoratedEngine;
            _userActionRecorder = userActionRecorder;
        }

        public void Write(IEnumerable<TimeDimensionMappingEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }
       public void Write(IEnumerable<LastUpdatedEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }
        public void Write(IEnumerable<ValidToMappingEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<UpdateStatusEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }



        public void Close()
        {
            _decoratedEngine.Close();
        }

        public void Dispose()
        {
            _decoratedEngine.Dispose();
        }

        public void StartMessage()
        {
            _decoratedEngine.StartMessage();
        }

        public void StartSection(EntityType entityType)
        {
            _decoratedEngine.StartSection(entityType);
        }

        public void Write(IEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(IConnectionEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(DatasetEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(DatasetPropertyEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(DataSetColumnEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(LocalCodeEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(MappingSetEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(ColumnDescriptionSourceEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(ComponentMappingEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(TranscodingEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(TranscodingRuleEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(HeaderEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(RegistryEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(TimeDimensionMappingEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }
        public void Write(LastUpdatedEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }
        public void Write(ValidToMappingEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }
        public void Write(UpdateStatusEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(NsiwsEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(UserEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(DataSourceEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(TimePreFormattedEntity entity)
        {
            _decoratedEngine.Write(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
        }

        public void Write(IEnumerable<IEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<IConnectionEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<DatasetEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<DataSetColumnEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<LocalCodeEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<MappingSetEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<ColumnDescriptionSourceEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<ComponentMappingEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<TranscodingEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<TranscodingRuleEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<HeaderEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<RegistryEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<NsiwsEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<UserEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(TemplateMapping entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<TemplateMapping> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(UserActionEntity entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void Write(IEnumerable<UserActionEntity> entity)
        {
            _decoratedEngine.Write(entity);
        }

        public void WriteProperty(string name, string value)
        {
            _decoratedEngine.WriteProperty(name, value);
        }

        public void WriteStatus(StatusType status, string message)
        {
            _decoratedEngine.WriteStatus(status, message);
        }
        public int Status
        {
            get
            {
                return _decoratedEngine.Status;
            }
            set
            {
                _decoratedEngine.Status = value;
            }
        }
    }
}
