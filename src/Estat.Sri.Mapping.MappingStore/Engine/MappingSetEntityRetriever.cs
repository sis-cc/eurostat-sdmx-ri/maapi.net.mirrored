// -----------------------------------------------------------------------
// <copyright file="MappingSetEntityRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-02-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Dapper;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    public class MappingSetEntityRetriever : EntityBaseRetrieverEngine<MappingSetEntity>
    {

        private const string MappingSetAlias = "MSET";
        private string TableAlias = "N_MAPPING_SET " + MappingSetAlias;
        private const string MappingSetDataflow = @" inner join ENTITY_REF_CHILDREN ERC on EB.ENTITY_ID = ERC.ENTITY_ID";
        private readonly IDictionary<string, string> _integerAdditionalProperties;

        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;

        private readonly Database _database;
        private readonly string sid;

        private readonly TimePreFormattedEntityRetriever _timePreFormattedEntityRetriever;

        public MappingSetEntityRetriever(Database databaseManager, string sid) : base(databaseManager)
        {
            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }

            this._timePreFormattedEntityRetriever = new TimePreFormattedEntityRetriever(databaseManager, sid);

            this._database = databaseManager;
            this.sid = sid;
            this._integerAdditionalProperties = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                                                    {
                                                        { EntityType.DataSet.ToString(), "SOURCE_DS" }
                                                    };
            this._fieldsToReturn = new Dictionary<Detail, IList<string>>();
            this._fieldsToReturn.Add(Detail.Full, new[]
            {
                "MSET.STR_MAP_SET_ID AS id",
                 NameAlias,DescriptionAlias,
                "MSET.SOURCE_DS as dataSetId",
                "MSET.VALID_FROM as validFrom",
                "MSET.VALID_TO as validTo",
                "MSET.IS_METADATA as isMetadata",
                "ERC.ID as dfid",
                "ERC.VERSION1 as version1",
                "ERC.VERSION2 as version2",
                "ERC.VERSION3 as version3",
                "ERC.EXTENSION as extension",
                "ERC.AGENCY as dfag"
            });

            this._fieldsToReturn.Add(Detail.IdAndName, new[] { EntityIdAlias, NameAlias });
            this._fieldsToReturn.Add(Detail.IdOnly, new[] { EntityIdAlias });
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>
        /// The matching entities
        /// </returns>
        public override IEnumerable<MappingSetEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var db = this._database;
            IEnumerable<MAMappingSetEntity> enumerable = GetRecordsFromDatabase(query, detail, db);
            return enumerable.Select(entity => new MappingSetEntity
            {
                DataSetId = entity.DataSetId,
                ValidFrom = entity.ValidFrom,
                ValidTo = entity.ValidTo,
                Description = entity.Description,
                EntityId = entity.EntityId,
                Name = entity.Name,
                Owner = entity.Owner,
                ParentId = entity.ParentId,
                StoreId = this.sid,
                IsMetadata = entity.IsMetadata
            });
        }

        private IEnumerable<MAMappingSetEntity> GetRecordsFromDatabase(IEntityQuery query, Detail detail, Database db)
        {
            WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder(db);


            // TODO check Adi's isqlbuilder
            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "STR_MAP_SET_ID", MappingSetAlias));
            clauses.Add(whereClauseBuilder.Build(query.ObjectId, "OBJECT_ID", EntityBaseAlias));
            clauses.Add(whereClauseBuilder.BuildFromUrn(query.ParentId, "ID", "AGENCY", new[] { "VERSION1", "VERSION2", "VERSION3", "EXTENSION" }, "ERC", "ERC"));
            foreach (var integerAdditionalProperty in this._integerAdditionalProperties)
            {
                ICriteria<string> additionalCriteria;
                if (query.AdditionalCriteria.TryGetValue(integerAdditionalProperty.Key, out additionalCriteria))
                {
                    clauses.Add(whereClauseBuilder.Build(additionalCriteria.ValueConvert(), integerAdditionalProperty.Value, MappingSetAlias));
                }
            }

            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3}", fields, GetBaseJoinString(TableAlias, MappingSetAlias + ".STR_MAP_SET_ID") + MappingSetDataflow, whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);
            return db.Query<MAMappingSetEntity>(sqlQuery, param);
        }

        public override void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            if (entityWriter == null)
            {
                throw new ArgumentNullException(nameof(entityWriter));
            }

           
            foreach(MAMappingSetEntity mset in GetRecordsFromDatabase(query, detail, this._database))
            {
                mset.StoreId = this.sid;
                mset.TimePreFormattedEntity = GetTimePreFormattedEntity(mset.EntityId, query, detail);
                entityWriter.Write(mset);
            }
        }

        private TimePreFormattedEntity GetTimePreFormattedEntity(string mappingSetId, IEntityQuery query, Detail detail)
        {
            if (detail != Detail.Full)
            {
                return null;
            }

            if (query.EntityId == null)
            {
                query = new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, mappingSetId) };
            }

            return _timePreFormattedEntityRetriever.GetEntities(query, detail).SingleOrDefault();
        }
    }
}