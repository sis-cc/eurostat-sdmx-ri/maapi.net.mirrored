using System;
using System.Collections.Generic;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class EntityBaseRetrieverEngine<T> : IEntityRetrieverEngine<T> where T : IEntity
    {
        protected readonly Database _databaseManager;

        protected IDictionary<Detail, IList<string>> _fieldsToReturn;
        protected readonly IDictionary<string, string> _integerAdditionalProperties;
        protected const string EntityBaseAlias = "EB";

        protected string EntityIdAlias = EntityBaseAlias + ".ENTITY_ID AS " + nameof(SimpleNameableEntity.EntityId);
        protected string NameAlias = EntityBaseAlias + ".OBJECT_ID AS " + nameof(SimpleNameableEntity.Name);
        protected string DescriptionAlias = EntityBaseAlias + ".Description AS " + nameof(SimpleNameableEntity.Description);

        private Dictionary<string, string> _baseProperties = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    {nameof(SimpleNameableEntity.Name),"OBJECT_ID" }
                };

        private string baseJoinString = " INNER JOIN ENTITY_BASE " + EntityBaseAlias + " ON EB.ENTITY_ID = ";

        public EntityBaseRetrieverEngine(Database databaseManager)
        {
            this._databaseManager = databaseManager;
            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }
            this._integerAdditionalProperties = BaseIntegerAdditionalProperties;
        }

        public IList<string> BaseFieldsToReturn
        {
            get
            {
                return new[]
            {
                EntityIdAlias,NameAlias,DescriptionAlias

            };
            }
        }

        public string GetBaseJoinString(string childTableWithAlias, string childTableJoinCondition)
        {
            return childTableWithAlias + baseJoinString + childTableJoinCondition;
        }

        public Dictionary<string, string> BaseIntegerAdditionalProperties
        {
            get
            {
                return _baseProperties;
            }
        }

        public bool IsBaseProperty(string propertyName)
        {
            return propertyName == nameof(SimpleNameableEntity.Name);
        }

        public virtual IEnumerable<T> GetEntities(IEntityQuery query, Detail detail)
        {
            throw new NotImplementedException();
        }

        public virtual void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            throw new NotImplementedException();
        }
    }
}
