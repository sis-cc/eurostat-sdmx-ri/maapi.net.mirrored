using System.Threading;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public interface IUserActionByType
    {
        bool ShouldRecord(EntityType entityType);
    }

    public class UserActionByType : IUserActionByType
    {
        public bool ShouldRecord(EntityType entityType)
        {
            if (Thread.CurrentPrincipal == null || Thread.CurrentPrincipal.Identity == null || string.IsNullOrEmpty(Thread.CurrentPrincipal.Identity.Name))
            {
                return false;
            }

            return entityType == EntityType.DataSet || entityType == EntityType.MappingSet || entityType == EntityType.DdbConnectionSettings || entityType == EntityType.DataSetColumn
                   || entityType == EntityType.LocalCode || entityType == EntityType.DescSource || entityType == EntityType.Mapping 
                   || entityType == EntityType.TimeMapping || entityType == EntityType.LastUpdated || entityType == EntityType.UpdateStatusMapping  
                   || entityType == EntityType.ValidToMapping || entityType == EntityType.TranscodingRule;
        }
    }
}