// -----------------------------------------------------------------------
// <copyright file="MappingMigrationEngine.cs" company="EUROSTAT">
//   Date Created : 2017-05-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Migration
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Dapper;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.Mapping.MappingStore.Model.Migration;

    /// <summary>
    /// The mapping migration engine.
    /// </summary>
    public class MappingMigrationEngine
    {
        /// <summary>
        /// The SQL query for retrieving the mappings that have more than one component.
        /// </summary>
        private const string IncompatibleMappingQuery = @"select MAP_ID as ITEM1, COMP_ID as ITEM2
from 
 COM_COL_MAPPING_COMPONENT c 
where exists (select MAP_ID from COM_COL_MAPPING_COMPONENT c2 where c.COMP_ID != c2.COMP_ID and c.MAP_ID = c2.MAP_ID)";

        /// <summary>
        /// The mapping query
        /// </summary>
        private const string MappingQuery = "select cm.MAP_ID as " + nameof(BaseRecord.Id) + ", cm.MAP_SET_ID as " + nameof(BaseRecord.ParentId) + ", cm.TYPE as " + nameof(MappingRecord.MappingType) + ", cm.CONSTANT as " + nameof(MappingRecord.Constant) + ", t.TR_ID as " + nameof(MappingRecord.TranscodingId) + ", t.EXPRESSION as " + nameof(MappingRecord.Expression)
            + " from COMPONENT_MAPPING  cm LEFT OUTER JOIN TRANSCODING t ON t.MAP_ID = cm.MAP_ID where CM.MAP_ID = {0}";

        /// <summary>
        /// The transcoding script query
        /// </summary>
        private const string TranscodingScriptQuery = "select ts.TR_SCRIPT_ID as " + nameof(BaseRecord.Id) + ", ts.TR_ID as " + nameof(BaseRecord.ParentId) + ", ts.SCRIPT_TITLE as " + nameof(TranscodingScriptRecord.Title)  + ", SCRIPT_CONTENT as " + nameof(TranscodingScriptRecord.Content)
      + " from TRANSCODING_SCRIPT ts INNER JOIN TRANSCODING t ON ts.TR_ID = t.TR_ID where t.MAP_ID = {0}";

        /// <summary>
        /// The transcoding rules query
        /// </summary>
        private const string TranscodingRulesQuery =
            "select tr.TR_RULE_ID as " + nameof(TranscodingRulesRecord.Id) + ", lcd.LCD_ID as "
            + nameof(BaseRecord.ParentId) + ", cd.CD_ID as " + nameof(TranscodingRulesRecord.SdmxCodeId)
            + @" FROM TRANSCODING_RULE tr
INNER JOIN TRANSCODING_RULE_DSD_CODE cd ON tr.TR_RULE_ID = cd.TR_RULE_ID
INNER JOIN TRANSCODING_RULE_LOCAL_CODE lcd ON tr.TR_RULE_ID = lcd.TR_RULE_ID
WHERE tr.TR_ID = {0} AND EXISTS(
SELECT c.COMP_ID
FROM DSD_CODE dc
INNER JOIN COMPONENT c ON dc.CL_ID = c.CL_ID
WHERE dc.LCD_ID = cd.CD_ID AND c.COMP_ID = {1})";

        /// <summary>
        /// The mapping column query
        /// </summary>
        private const string MappingColumnQuery = "select DATASET_COLUMN_NAME  as " + nameof(BaseRecord.Id) + ", MAP_ID as " + nameof(BaseRecord.ParentId) 
          + " from N_MAPPING_WITH_COLUMN where MAP_ID = {0}";

        /// <summary>
        /// The database manager
        /// </summary>
        private readonly DatabaseManager _databaseManager;

        /// <summary>
        /// The entity persistence manager
        /// </summary>
        private readonly IEntityPersistenceManager _entityPersistenceManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingMigrationEngine"/> class.
        /// </summary>
        /// <param name="databaseManager">
        /// The database manager.
        /// </param>
        /// <param name="entityPersistenceManager">
        /// The entity Persistence Manager.
        /// </param>
        public MappingMigrationEngine(DatabaseManager databaseManager, IEntityPersistenceManager entityPersistenceManager)
        {
            _databaseManager = databaseManager;
            _entityPersistenceManager = entityPersistenceManager;
        }

        /// <summary>
        /// Migrates the specified store identifier.
        /// </summary>
        /// <param name="storeId">
        /// The store identifier.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        public IActionResult Migrate(string storeId)
        {
            // TODO remove the EntityType from the parameter. We get the info from type parameter
            var mappingPersist = _entityPersistenceManager.GetEngine<ComponentMappingEntity>(
                storeId);
            var transcodingPersist = _entityPersistenceManager.GetEngine<TranscodingEntity>(storeId);
            var transcodingRulePersist = _entityPersistenceManager.GetEngine<TranscodingRuleEntity>(storeId);

            var database = _databaseManager.GetDatabase(storeId);
            using (var con = database.CreateConnection())
            {
                con.Open();
                using (var transaction = con.BeginTransaction())
                {
                    var migrationRecordRetriever = new MigrationRecordRetriever(database, transaction);
                    var lookup = con.Query<Tuple<long, long>>(IncompatibleMappingQuery, transaction: transaction).ToLookup(pair => pair.Item1, pair => pair.Item2);
                    foreach (var valuePairs in lookup)
                    {
                        Migrate(valuePairs, migrationRecordRetriever, mappingPersist, transcodingPersist, transcodingRulePersist);
                    }
                   
                    transaction.Commit();
                }
            }

            return ActionResults.Success;
        }

        /// <summary>
        /// Migrates the specified value pairs.
        /// </summary>
        /// <param name="valuePairs">The value pairs.</param>
        /// <param name="migrationRecordRetriever">The migration record retriever.</param>
        /// <param name="mappingPersist">The mapping persist.</param>
        /// <param name="transcodingPersist">The transcoding persist.</param>
        /// <param name="transcodingRulePersist">The transcoding rule persist.</param>
        private static void Migrate(
            IGrouping<long, long> valuePairs,
            MigrationRecordRetriever migrationRecordRetriever,
            IEntityPersistenceEngine<ComponentMappingEntity> mappingPersist,
            IEntityPersistenceEngine<TranscodingEntity> transcodingPersist,
            IEntityPersistenceEngine<TranscodingRuleEntity> transcodingRulePersist)
        {
            long originalMappingId = valuePairs.Key;
            var mappingRecord = migrationRecordRetriever.GetDataSingle<MappingRecord>(originalMappingId, MappingQuery);

            var mappingColumnRecord = migrationRecordRetriever.GetDataSingle<BaseRecord>(originalMappingId, MappingColumnQuery);

            List<TranscodingScriptRecord> scripts = new List<TranscodingScriptRecord>();
            if (mappingRecord.TranscodingId.HasValue)
            {
                scripts.AddRange(
                    migrationRecordRetriever.GetData<TranscodingScriptRecord>(originalMappingId, TranscodingScriptQuery));
            }

            var columnnId = mappingColumnRecord.Id.ToString(CultureInfo.InvariantCulture);
            var dataSetColumnEntity = new DataSetColumnEntity() { EntityId = columnnId };
            var parentId = mappingRecord.ParentId.ToString(CultureInfo.InvariantCulture);

            // we cannot represent the 1 column to N components mapping in MAAPI .NET
            // so we need to manually extract the information and updated/insert it
            foreach (var compId in valuePairs)
            {
                ComponentMappingEntity componentMapping = new ComponentMappingEntity();
                componentMapping.SetColumns(new List<DataSetColumnEntity>());
                componentMapping.GetColumns().Add(dataSetColumnEntity);

                // TODO Re-factor/Check Component.Id is actually entity id ??
                var id = compId.ToString(CultureInfo.InvariantCulture);
                componentMapping.Component = new Component() { EntityId = id };
                componentMapping.ConstantValue = mappingRecord.Constant;
                componentMapping.Type = mappingRecord.MappingType;
                componentMapping.ParentId = parentId;

                var newMappingId = mappingPersist.Add(componentMapping);

                SaveTranscoding(
                    mappingRecord,
                    newMappingId,
                    scripts,
                    transcodingPersist,
                    migrationRecordRetriever,
                    compId,
                    transcodingRulePersist);
            }

            mappingPersist.Delete(originalMappingId.ToString(CultureInfo.InvariantCulture), EntityType.Mapping);
        }

        /// <summary>
        /// Saves the transcoding.
        /// </summary>
        /// <param name="mappingRecord">The mapping record.</param>
        /// <param name="newMappingId">The new mapping identifier.</param>
        /// <param name="scripts">The scripts.</param>
        /// <param name="transcodingPersist">The transcoding persist.</param>
        /// <param name="migrationRecordRetriever">The migration record retriever.</param>
        /// <param name="compId">The comp identifier.</param>
        /// <param name="transcodingRulePersist">The transcoding rule persist.</param>
        private static void SaveTranscoding(
            MappingRecord mappingRecord,
            long newMappingId,
            List<TranscodingScriptRecord> scripts,
            IEntityPersistenceEngine<TranscodingEntity> transcodingPersist,
            MigrationRecordRetriever migrationRecordRetriever,
            long compId,
            IEntityPersistenceEngine<TranscodingRuleEntity> transcodingRulePersist)
        {
            // get all rules for this component 
            if (mappingRecord.TranscodingId != null)
            {
                // TODO double check The Mapping Assistant doesn't support 1 column to many SDMX components mapping for Time Transcoding
                TranscodingEntity transcoding = new TranscodingEntity
                                                    {
                                                        ParentId =
                                                            newMappingId.ToString(
                                                                CultureInfo.InvariantCulture),
                                                        Expression = mappingRecord.Expression,
                                                        Script =
                                                            scripts.Select(ScriptConverter).ToList()
                                                    };

                var transcodingEntityId = transcodingPersist.Add(transcoding).ToString(CultureInfo.InvariantCulture);

                var transcodingRulesRecords =
                    migrationRecordRetriever.GetData<TranscodingRulesRecord>(
                        mappingRecord.TranscodingId.Value,
                        compId,
                        TranscodingRulesQuery);
                var transcodingRuleEntities =
                    transcodingRulesRecords.Select(record => RuleConverter(record, transcodingEntityId));
                transcodingRulePersist.Add(transcodingRuleEntities.ToList());
            }
        }

        /// <summary>
        /// Scripts the converter.
        /// </summary>
        /// <param name="transcodingScriptRecord">
        /// The transcoding script record.
        /// </param>
        /// <returns>
        /// The <see cref="TranscodingScriptEntity"/>.
        /// </returns>
        private static TranscodingScriptEntity ScriptConverter(TranscodingScriptRecord transcodingScriptRecord)
        {
            return new TranscodingScriptEntity()
                       {
                           ScriptContent = transcodingScriptRecord.Content,
                           ScriptTile = transcodingScriptRecord.Title
                       };
        }

        /// <summary>
        /// Rules the converter.
        /// </summary>
        /// <param name="record">
        /// The record.
        /// </param>
        /// <param name="parentId">
        /// The parent identifier.
        /// </param>
        /// <returns>
        /// The <see cref="TranscodingRuleEntity"/>.
        /// </returns>
        private static TranscodingRuleEntity RuleConverter(TranscodingRulesRecord record, string parentId)
        {
            var dsdCodeEntity = new IdentifiableEntity()
                                                   {
                                                       EntityId =
                                                           record.SdmxCodeId.ToString(
                                                               CultureInfo.InvariantCulture)
                                                   };
            var localCodeEntity = new LocalCodeEntity { EntityId = record.ParentId.ToString(CultureInfo.InvariantCulture) };

            var localCodes = new List<LocalCodeEntity> { localCodeEntity };
            return new TranscodingRuleEntity()
                       {
                           DsdCodeEntity = dsdCodeEntity,
                           LocalCodes = localCodes,
                           ParentId = parentId
                       };
        }
    }
}