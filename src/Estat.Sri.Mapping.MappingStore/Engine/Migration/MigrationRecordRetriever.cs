// -----------------------------------------------------------------------
// <copyright file="MigrationRecordRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-07-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Migration
{
    using System.Collections.Generic;
    using System.Data.Common;

    using Dapper;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// The migration record retriever.
    /// </summary>
    internal class MigrationRecordRetriever
    {
        /// <summary>
        /// The _database.
        /// </summary>
        private readonly Database _database;

        /// <summary>
        /// The _transaction.
        /// </summary>
        private readonly DbTransaction _transaction;

        /// <summary>
        /// Initializes a new instance of the <see cref="MigrationRecordRetriever"/> class.
        /// </summary>
        /// <param name="database">
        /// The database.
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        public MigrationRecordRetriever(Database database, DbTransaction transaction)
        {
            _database = database;
            _transaction = transaction;
        }

        /// <summary>
        /// Gets the data single.
        /// </summary>
        /// <typeparam name="TRecord">
        /// The type of the record.
        /// </typeparam>
        /// <param name="originalMappingId">
        /// The original mapping identifier.
        /// </param>
        /// <param name="mappingQuery">
        /// The mapping query.
        /// </param>
        /// <returns>
        /// The <see cref="TRecord"/>.
        /// </returns>
        public TRecord GetDataSingle<TRecord>(long originalMappingId, string mappingQuery)
        {
            var parameterName = _database.BuildParameterName(nameof(originalMappingId));
            var query = string.Format(mappingQuery, parameterName);
            var mappingRecord = _transaction.Connection.QuerySingle<TRecord>(
                query,
                new { originalMappingId },
                _transaction);
            return mappingRecord;
        }

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <typeparam name="TRecord">
        /// The type of the record.
        /// </typeparam>
        /// <param name="originalMappingId">
        /// The original mapping identifier.
        /// </param>
        /// <param name="mappingQuery">
        /// The mapping query.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{TRecord}"/>.
        /// </returns>
        public IEnumerable<TRecord> GetData<TRecord>(long originalMappingId, string mappingQuery)
        {
            var parameterName = _database.BuildParameterName(nameof(originalMappingId));
            var query = string.Format(mappingQuery, parameterName);
            var mappingRecord = _transaction.Connection.Query<TRecord>(query, new { originalMappingId }, _transaction);
            return mappingRecord;
        }

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <typeparam name="TRecord">
        /// The type of the record.
        /// </typeparam>
        /// <param name="originalTranscodingId">
        /// The original transcoding identifier.
        /// </param>
        /// <param name="componentId">
        /// The component identifier.
        /// </param>
        /// <param name="mappingQuery">
        /// The mapping query.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{TRecord}"/>.
        /// </returns>
        public IEnumerable<TRecord> GetData<TRecord>(long originalTranscodingId, long componentId, string mappingQuery)
        {
            var parameterName = _database.BuildParameterName(nameof(originalTranscodingId));
            var componentParameterName = _database.BuildParameterName(nameof(componentId));
            var query = string.Format(mappingQuery, parameterName, componentParameterName);

            var mappingRecord = _transaction.Connection.Query<TRecord>(
                query,
                new { originalTranscodingId, componentId },
                _transaction);
            return mappingRecord;
        }

        /// <summary>
        /// Gets the data single or default.
        /// </summary>
        /// <typeparam name="TRecord">
        /// The type of the record.
        /// </typeparam>
        /// <param name="originalMappingId">
        /// The original mapping identifier.
        /// </param>
        /// <param name="mappingQuery">
        /// The mapping query.
        /// </param>
        /// <returns>
        /// The <see cref="TRecord"/>.
        /// </returns>
        public TRecord GetDataSingleOrDefault<TRecord>(long originalMappingId, string mappingQuery)
        {
            var parameterName = _database.BuildParameterName(nameof(originalMappingId));
            var query = string.Format(mappingQuery, parameterName);
            var mappingRecord = _transaction.Connection.QuerySingleOrDefault<TRecord>(
                query,
                new { originalMappingId },
                _transaction);
            return mappingRecord;
        }
    }
}