using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class UserActionPathAndValues : IUserActionPathAndValues
    {
        private readonly IEntityRetrieverManager _entityRetrieverManager;
        private readonly string _storeId;

        public UserActionPathAndValues(IEntityRetrieverManager entityRetrieverManager,string storeId)
        {
            this._entityRetrieverManager = entityRetrieverManager;
            this._storeId = storeId;
        }

        public Dictionary<string,object> CreateFor(IEntity entity, UserAction userAction)
        {
            var pathAndValues = new Dictionary<string, object>();
            var nameableEntity = entity as ISimpleNameableEntity;
            ComponentMappingEntity componentMapping;
            DataSetColumnEntity dataSetColumnEntity;
            switch (entity.TypeOfEntity)
            {
                case EntityType.MappingSet:
                case EntityType.DdbConnectionSettings:
                case EntityType.DataSet:
                    pathAndValues.Add(nameof(UserActionEntity.EntityId), entity.EntityId);
                    pathAndValues.Add(nameof(UserActionEntity.EntityName), nameableEntity.Name);
                    pathAndValues.Add(nameof(UserActionEntity.EntityType), entity.TypeOfEntity.ToString());
                    pathAndValues.Add(nameof(UserActionEntity.OperationType), userAction.ToString());
                    break;
                case EntityType.User:
                case EntityType.Registry:
                    pathAndValues.Add(nameof(UserActionEntity.EntityId), entity.EntityId);
                    pathAndValues.Add(nameof(UserActionEntity.EntityType), entity.TypeOfEntity.ToString());
                    pathAndValues.Add(nameof(UserActionEntity.OperationType), userAction.ToString());
                    break;
                case EntityType.Header:
                    pathAndValues.Add(nameof(UserActionEntity.EntityId), entity.EntityId);
                    pathAndValues.Add(nameof(UserActionEntity.EntityName), entity.ParentId);
                    pathAndValues.Add(nameof(UserActionEntity.EntityType), entity.TypeOfEntity.ToString());
                    pathAndValues.Add(nameof(UserActionEntity.OperationType), userAction.ToString());
                    break;
                case EntityType.DataSetColumn:
                    pathAndValues.Add(nameof(UserActionEntity.EntityId), entity.ParentId);
                    pathAndValues.Add(nameof(UserActionEntity.EntityName), this.GetDataset(entity.ParentId).Name);
                    pathAndValues.Add(nameof(UserActionEntity.EntityType), EntityType.DataSet.ToString());
                    pathAndValues.Add(nameof(UserActionEntity.OperationType), UserAction.Update.ToString());
                    break;
                case EntityType.LocalCode:
                    dataSetColumnEntity = this.GetDatasetColumn(entity.ParentId);
                    var dataset = this.GetDataset(dataSetColumnEntity.ParentId);
                    pathAndValues.Add(nameof(UserActionEntity.EntityId), dataset.EntityId);
                    pathAndValues.Add(nameof(UserActionEntity.EntityName), dataset.Name);
                    pathAndValues.Add(nameof(UserActionEntity.EntityType), EntityType.DataSet.ToString());
                    pathAndValues.Add(nameof(UserActionEntity.OperationType), UserAction.Update.ToString());
                    break;
                case EntityType.DescSource:
                    dataSetColumnEntity = this.GetDatasetColumn(entity.ParentId);
                    var datasetEntity = this.GetDataset(dataSetColumnEntity.ParentId);
                    pathAndValues.Add(nameof(UserActionEntity.EntityId), datasetEntity.EntityId);
                    pathAndValues.Add(nameof(UserActionEntity.EntityName), datasetEntity.Name);
                    pathAndValues.Add(nameof(UserActionEntity.EntityType), EntityType.DataSet.ToString());
                    pathAndValues.Add(nameof(UserActionEntity.OperationType), UserAction.Update.ToString());
                    break;
                case EntityType.Mapping:
                case EntityType.TimeMapping:
                case EntityType.LastUpdated:
                case EntityType.ValidToMapping:
                case EntityType.UpdateStatusMapping:
                    pathAndValues.Add(nameof(UserActionEntity.EntityId), entity.ParentId);
                    pathAndValues.Add(nameof(UserActionEntity.EntityName), this.GetMappingSet(entity.ParentId).Name);
                    pathAndValues.Add(nameof(UserActionEntity.EntityType), EntityType.MappingSet.ToString());
                    pathAndValues.Add(nameof(UserActionEntity.OperationType), UserAction.Update.ToString());
                    break;
                case EntityType.TranscodingRule:
                case EntityType.Transcoding:
                    componentMapping = this.ComponentMapping(entity.ParentId);
                    pathAndValues.Add(nameof(UserActionEntity.EntityId), componentMapping.ParentId);
                    pathAndValues.Add(nameof(UserActionEntity.EntityName), this.GetMappingSet(componentMapping.ParentId).Name);
                    pathAndValues.Add(nameof(UserActionEntity.EntityType), EntityType.MappingSet.ToString());
                    pathAndValues.Add(nameof(UserActionEntity.OperationType), UserAction.Update.ToString());
                    break;
            }
            pathAndValues.Add(nameof(UserActionEntity.UserName),Thread.CurrentPrincipal.Identity.Name);
            pathAndValues.Add(nameof(UserActionEntity.ActionWhen),DateTime.Now);

            return pathAndValues;
        }

        private DataSetColumnEntity GetDatasetColumn(string entityId)
        {
            return this._entityRetrieverManager.GetEntities<DataSetColumnEntity>(this._storeId, new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) }, Detail.Full).FirstOrDefault();
        }

        private TranscodingEntity GetTranscoding(string entityId)
        {
            return this._entityRetrieverManager.GetEntities<TranscodingEntity>(this._storeId, new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) }, Detail.Full).FirstOrDefault();
        }

        private ComponentMappingEntity ComponentMapping(string entityId)
        {
            return this._entityRetrieverManager.GetEntities<ComponentMappingEntity>(this._storeId, new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) }, Detail.Full).FirstOrDefault();
        }

        private MappingSetEntity GetMappingSet(string entityId)
        {
            return this._entityRetrieverManager.GetEntities<MappingSetEntity>(this._storeId, new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) }, Detail.IdAndName).FirstOrDefault();
        }

        private DatasetEntity GetDataset(string entityId)
        {
            return this._entityRetrieverManager.GetEntities<DatasetEntity>(this._storeId, new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) }, Detail.IdAndName).FirstOrDefault();
        }
    }
}