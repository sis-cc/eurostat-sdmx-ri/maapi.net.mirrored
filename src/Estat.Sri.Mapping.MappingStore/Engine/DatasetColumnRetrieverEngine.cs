// -----------------------------------------------------------------------
// <copyright file="DatasetColumnRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-17
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.Mapping.Api.Extension;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Util.Extensions;

    public class DatasetColumnRetrieverEngine : IEntityRetrieverEngine<DataSetColumnEntity>
    {
        /// <summary>
        /// This would return 0 if there is no mapping, 1 if there is mapping but no transcoding and 2 mapping & transcoding
        /// </summary>
        private const string ExtraFields =
            @"INNER JOIN N_DATASET ND ON
            ND.ENTITY_ID = D.DS_ID
            LEFT OUTER JOIN
(SELECT
	   1 AS IsMapped,
       MCOL.DATASET_COLUMN_NAME AS DATASETCOLUMNNAME,
	   OUTPUT_COLUMN
FROM N_COMPONENT_MAPPING CM
JOIN N_MAPPING_SET MSET ON MSET.STR_MAP_SET_ID = CM.STR_MAP_SET_ID
LEFT OUTER JOIN N_MAPPING_WITH_COLUMN MCOL ON MCOL.MAP_ID = CM.MAP_ID
LEFT OUTER JOIN (select OUTPUT_COLUMN,MSET.STR_MAP_SET_ID as MappingSetId from N_MAPPING_TIME_DIMENSION CM 
	JOIN N_MAPPING_SET MSET ON MSET.STR_MAP_SET_ID = CM.STR_MAP_SET_ID
	JOIN N_DATASET ND on ND.ENTITY_ID = MSET.SOURCE_DS) TimeMappings on TimeMappings.MappingSetId = MSET.STR_MAP_SET_ID
) MAPPINGS ON D.NAME = MAPPINGS.DATASETCOLUMNNAME Or D.NAME = MAPPINGS.OUTPUT_COLUMN
";

        //private const string TimePreFormattedStatus =
        //    "(select case when count(DISTINCT TF_OUT.MAP_SET_ID) > 0 OR count(DISTINCT TF_FROM.MAP_SET_ID) > 0 OR count(DISTINCT TF_TO.MAP_SET_ID) > 0 then 1 else 0 end " +
        //    "from DATASET_COLUMN DCI left outer join TIME_PRE_FORMATED TF_OUT on TF_OUT.OUTPUT_COLUMN = DCI.COL_ID " +
        //    "left outer join TIME_PRE_FORMATED TF_FROM on TF_FROM.FROM_COLUMN= DCI.COL_ID left outer join TIME_PRE_FORMATED TF_TO on TF_TO.TO_COLUMN = DCI.COL_ID " +
        //    "where DCI.COL_ID = DC.COL_ID) as " + nameof(DataSetColumnEntity.IsTimePreFormatted);

        private const string DatasetColumnAlias = "D";

        private readonly Database _databaseManager;
        private readonly IEntityRetrieverManager entityRetrieverManager;
        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;

        public DatasetColumnRetrieverEngine(Database databaseManager)
        {
            this._databaseManager = databaseManager;
            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }

            this._databaseManager = databaseManager;
            this._fieldsToReturn = new Dictionary<Detail, IList<string>>();
            this._fieldsToReturn.Add(Detail.Full, new[]
            {
                "D.COL_ID as " + nameof(DataSetColumnEntity.EntityId), "D.NAME as " + nameof(DataSetColumnEntity.Name), "D.Description " + nameof(DataSetColumnEntity.Description), "D.DS_ID as " + nameof(DataSetColumnEntity.ParentId),
                "D.LASTRETRIEVAL as " + nameof(DataSetColumnEntity.LastRetrieval),
                //transcodings to be implemented and IsTimePreFormatted is not used by the frontend
                "IsMapped as " + nameof(DataSetColumnEntity.IsMapped),
                "0 as " + nameof(DataSetColumnEntity.IsTimePreFormatted)
            });
            this._fieldsToReturn.Add(Detail.IdAndName, new[] {"D.COL_ID as " + nameof(DataSetColumnEntity.EntityId), "D.NAME as " + nameof(DataSetColumnEntity.Name)});
            this._fieldsToReturn.Add(Detail.IdOnly, new[] {"D.COL_ID as " + nameof(DataSetColumnEntity.EntityId)});
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<DataSetColumnEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var columns = new List<DataSetColumnEntity>();
            RetrieveEntities(query, detail, columns.Add);
            return columns;
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            RetrieveEntities(query, detail, entityWriter.Write);
        }

        private void RetrieveEntities(IEntityQuery query, Detail detail, Action<DataSetColumnEntity> action)
        {
            var db = this._databaseManager;
            var whereClauseBuilder = new WhereClauseBuilder(db);

            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "COL_ID", DatasetColumnAlias));
            clauses.Add(whereClauseBuilder.Build(query.ParentId, "DS_ID", DatasetColumnAlias));

            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select distinct {0} FROM {1} {2} {3} {4}", fields, "DATASET_COLUMN " + DatasetColumnAlias, ExtraFields, whereStatement, string.Join(" AND ", whereClauses));
            CommonEntityRetriever.RetrieveEntities(action, db, normalizedClauses, sqlQuery);
        }
    }
}