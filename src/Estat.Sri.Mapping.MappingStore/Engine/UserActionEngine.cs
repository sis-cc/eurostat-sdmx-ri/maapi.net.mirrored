// -----------------------------------------------------------------------
// <copyright file="UserActionEngine.cs" company="EUROSTAT">
//   Date Created : 2017-09-01
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Manager;

    public class UserActionEngine<TPermissionEntity> : IEntityPersistenceEngine<TPermissionEntity>, IEntityPersistenceForUpdate<TPermissionEntity>
        where TPermissionEntity : IEntity
    {

        private readonly IEntityPersistenceEngine<TPermissionEntity> _decoratedEngine;
        private readonly IEntityPersistanceStreamEngine _streamingDecoratedEngine;

        private readonly IEntityRetrieverManager _entityRetrieverManager;

        private readonly string _mappingStoreIdentifier;

        private readonly IMultipleEntitiesUserAction _multipleEntitiesUserAction;

        private readonly UserActionRecorder _userActionRecorder;

        public UserActionEngine(
           IEntityPersistanceStreamEngine decoratedEngine,
           UserActionRecorder userActionRecorder,
           string mappingStoreIdentifier,
           IMultipleEntitiesUserAction multipleEntitiesUserAction,
           IEntityRetrieverManager entityRetrieverManager)
        {
            _streamingDecoratedEngine = decoratedEngine;
            _userActionRecorder = userActionRecorder;
            _mappingStoreIdentifier = mappingStoreIdentifier;
            _multipleEntitiesUserAction = multipleEntitiesUserAction;
            _entityRetrieverManager = entityRetrieverManager;
        }

        public UserActionEngine(
            IEntityPersistenceEngine<TPermissionEntity> decoratedEngine,
           UserActionRecorder userActionRecorder,
            string mappingStoreIdentifier,
            IMultipleEntitiesUserAction multipleEntitiesUserAction,
            IEntityRetrieverManager entityRetrieverManager)
        {
            _decoratedEngine = decoratedEngine;
            _userActionRecorder = userActionRecorder;
            _streamingDecoratedEngine = decoratedEngine;
            _mappingStoreIdentifier = mappingStoreIdentifier;
            _multipleEntitiesUserAction = multipleEntitiesUserAction;
            _entityRetrieverManager = entityRetrieverManager;
        }

        /// <summary>
        ///     Persists the specified update information.
        /// </summary>
        /// <param name="patchRequest"></param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        public void Update(PatchRequest patchRequest, EntityType entityType, string entityId)
        {
            if (_decoratedEngine == null)
            {
                return;
            }

            var entity = GetEntity(entityId, entityType);
            if (entity == null)
            {
                throw new ResourceNotFoundException(
                    $"entity of type {entityType} with id {entityId} was not found in the database");
            }

            _decoratedEngine.Update(patchRequest, entityType, entityId);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Update);
        }

        public long Add(TPermissionEntity entity)
        {
            if (_decoratedEngine == null)
            {
                return -1;
            }

            var originalEntityId = entity.EntityId;
            var entityId = _decoratedEngine.Add(entity);

            entity.EntityId = entityId.ToString(CultureInfo.InvariantCulture);

            var userAction = string.Equals(originalEntityId, entity.EntityId) ? UserAction.Update : UserAction.Insert;
            _userActionRecorder.TryInsertUserAction(entity, userAction);

            return entityId;
        }

        public IEnumerable<long> Add(IList<TPermissionEntity> entities)
        {
            var ids = new List<long>();
            if (_decoratedEngine == null)
            {
                return ids;
            }
            foreach (var entity in entities)
            {
                var entityId = _decoratedEngine.Add(entity);
                entity.EntityId = entityId.ToString();
                ids.Add(entityId);
            }

            if (_userActionRecorder.ShouldRecord(entities.First().TypeOfEntity))
            {
                foreach (var entity in _multipleEntitiesUserAction.GetEntities(
                    entities.Cast<IEntity>().ToList(),
                    _mappingStoreIdentifier))
                {
                    _userActionRecorder.TryInsertUserAction(entity, UserAction.Insert);
                }
            }

            return ids;
        }

        public void Delete(string entityId, EntityType entityType)
        {
            var entity = GetEntity(entityId, entityType);
            if (entity == null)
            {
                throw new ResourceNotFoundException(
                    $"entity of type {entityType} with id {entityId} was not found in the database");
            }

            _streamingDecoratedEngine.Delete(entityId, entityType);

            _userActionRecorder.TryInsertUserAction(entity, UserAction.Delete);
        }

        private IEntity GetEntity(string entityId, EntityType entityType)
        {
            var entityRetrieverEngine = _entityRetrieverManager.GetRetrieverEngine(_mappingStoreIdentifier, entityType);
            return entityRetrieverEngine.GetEntities(
                new EntityQuery
                {
                    EntityId = new Criteria<string>(OperatorType.Exact, entityId)
                },
                Detail.Full).FirstOrDefault();
        }

        public void DeleteChildren(string entityId, EntityType childrenEntityType)
        {
            // TODO should we record deleting children
            _streamingDecoratedEngine.DeleteChildren(entityId, childrenEntityType);
        }

        public List<StatusType> Add(IEntityStreamingReader input, IEntityStreamingWriter responseWriter)
        {
            UserActionWriterEngine userActionWriterEngine = new UserActionWriterEngine(responseWriter, _userActionRecorder);
            return _streamingDecoratedEngine.Add(input, userActionWriterEngine);
        }

        public void Import(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping)
        {
            UserActionWriterEngine userActionWriterEngine = new UserActionWriterEngine(responseWriter, _userActionRecorder);
            _streamingDecoratedEngine.Import(input, userActionWriterEngine, mapping);
        }

        public void Update(IEntity entity)
        {
            if (_decoratedEngine == null)
            {
                return;
            }

            var engineForUpdate = _decoratedEngine as IEntityPersistenceForUpdate<TPermissionEntity>;
            engineForUpdate.Update(entity);
            _userActionRecorder.TryInsertUserAction(entity, UserAction.Update);
        }
    }
}