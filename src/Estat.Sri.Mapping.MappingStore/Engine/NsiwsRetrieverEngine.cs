// -----------------------------------------------------------------------
// <copyright file="NsiwsRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2021-08-10
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class NsiwsRetrieverEngine : IEntityRetrieverEngine<NsiwsEntity>
    {
        private readonly Database _databaseManager;
        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;

        public NsiwsRetrieverEngine(Database databaseManager)
        {
            this._databaseManager = databaseManager;
            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }

            this._databaseManager = databaseManager;
            this._fieldsToReturn = new Dictionary<Detail, IList<string>>();
            this._fieldsToReturn.Add(Detail.Full, new[]
            {
                "ID as " + nameof(NsiwsEntity.EntityId), "URL as " + nameof(NsiwsEntity.Url), "USERNAME as " + nameof(NsiwsEntity.UserName),
                "PASSWORD as " + nameof(NsiwsEntity.Password),nameof(NsiwsEntity.Description),nameof(NsiwsEntity.Technology),nameof(NsiwsEntity.Name)
                ,nameof(NsiwsEntity.Proxy)
            });
            this._fieldsToReturn.Add(Detail.IdOnly, new[] { "ID as " + nameof(NsiwsEntity.EntityId) });
        }

        /// <summary>
        /// Gets the entities.
        ///  </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<NsiwsEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var db = this._databaseManager;
            var whereClauseBuilder = new WhereClauseBuilder(db);

            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "ID"));
            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3}", fields, "NSIWS", whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);
            return db.Query<NsiwsEntity>(sqlQuery, param);
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }
    }
}