﻿// -----------------------------------------------------------------------
// <copyright file="XmlEntityWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2017-07-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.IO;
using System.Text;
using System.Xml;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class XmlEntityWriterEngine : IEntityWriterEngine
    {
        private readonly Encoding _encoding;
        private readonly IImportExportEntity[] _helpers;
        private readonly Stream _stream;
        private readonly string _defaultNamespace = "http://europa.eu/resources/sdmxri/schemas/mappingstore";
        private string _rootElement;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public XmlEntityWriterEngine(Stream stream, Encoding encoding, params IImportExportEntity[] helpers)
        {
            this._stream = stream;
            this._encoding = encoding;
            this._helpers = helpers;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this._stream.Flush();
 //           this._stream.Dispose();
        }

        public void Write(EntitiesByType entities)
        {
            var xmlWriterSettings = new XmlWriterSettings
            {
                Encoding = this._encoding,
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };
            Array.Sort(this._helpers,new ImportExportComparer());
            using (var xmlWriter = XmlWriter.Create(this._stream, xmlWriterSettings))
            {
                this._rootElement = "MappingStore";
                xmlWriter.WriteStartElement(this._rootElement, this._defaultNamespace);
                foreach (var entityInOutHelper in this._helpers)
                {
                    entityInOutHelper.Write(xmlWriter, entities);
                }
                xmlWriter.WriteEndElement();
                xmlWriter.Flush();
            }
        }
    }
}