using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.XPath;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class DatasetColumnPersistenceEngine : EntityPersistenceEngine<DataSetColumnEntity>
    {
        private Database _database;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityPersistenceEngine{TEntity}"/> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        /// <param name="updateInfoFactoryManager">The update information factory manager.</param>
        /// <param name="mappingStoreIdentifier"></param>
        /// <param name="entityPropertiesExtractor"></param>
        /// <param name="commmandsFromUpdateInfo"></param>
        public DatasetColumnPersistenceEngine(DatabaseManager databaseManager, UpdateInfoFactoryManager updateInfoFactoryManager, string mappingStoreIdentifier, IEntityPropertiesExtractor entityPropertiesExtractor, ICommmandsFromUpdateInfo commmandsFromUpdateInfo)
            : base(databaseManager, updateInfoFactoryManager, mappingStoreIdentifier, entityPropertiesExtractor, commmandsFromUpdateInfo)
        {
            this._database = databaseManager.GetDatabase(mappingStoreIdentifier);
        }

        public override void DeleteChildren(string entityId, EntityType childrenEntityType)
        {
            var parentEntityId = entityId.AsMappingStoreEntityId();
            using (var context = new ContextWithTransaction(_database))
            {
                DeleteAsChildren(parentEntityId, context);
                context.Complete();
            }
        }

        /// <summary>
        /// Delete the dataset column that belong to the dataset entity with the specified <paramref name="parentEntityId"/>
        /// </summary>
        /// <param name="parentEntityId">The dataset entity id</param>
        /// <param name="context">The transaction context</param>
        public static void DeleteAsChildren(long parentEntityId, ContextWithTransaction context)
        {
            context.DatabaseUnderContext.ExecuteNonQueryFormat("DELETE FROM DATASET_COLUMN WHERE DS_ID = {0}", parentEntityId);
        }
    }
}
