// -----------------------------------------------------------------------
// <copyright file="MappingSetImportExport.cs" company="EUROSTAT">
//   Date Created : 2017-07-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;

using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System.Diagnostics;
    using System.Globalization;
    using Estat.Sri.Mapping.Api.Extension;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Util.Xml;

    public class MappingSetImportExport : IImportExportEntity
    {
        private const string _id = "id";

        private const string _dataflow = "Dataflow";

        private const string _ref = "Ref";

        private const string _agencyid = "agencyId";

        private const string _version = "version";

        private const string _dataset = "Dataset";

        private const string _timeComponentMap = "TimeComponentMap";

        private const string _timeComponent = "SdmxTimeComponent";

        private const string _localName = "LocalColumn";

        private const string _constantvalue = "ConstantValue";

        private const string _timeMap = "TimeMap";

        private const string _dateColumn = "DateColumn";

        private const string _frequencytype = "frequencyType";

        private const string _year = "YearColumn";

        private const string _start = "start";

        private const string _length = "length";

        private const string _period = "PeriodColumn";

        private const string _componentMap = "ComponentMap";

        private const string _codeMap = "CodeMap";

        private const string _sdmxcode = "SdmxCode";

        private const string _localCode = "LocalCode";

        private const string _sdmxComponent = "SdmxComponent";

        private const string Name = "Name";

        private const string RootTagName = "MappingSet";

        private readonly SdmxStructureType _dataflowSdmxType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow);

        private long _entityIndex = 1;

        public EntityType EntityType => EntityType.MappingSet;

        public string RootName { get; set; } = RootTagName;
        public List<IEntity> Read(XmlReader xmlReader, string id)
        {
            var mappingSet = new MappingSetEntity() { EntityId = id };
            List<IEntity> entities = new List<IEntity>();
            entities.Add(mappingSet);
            ComponentMappingEntity lastMapping = null;
            ComponentMappingEntity timeComponentMapping = null;
            TranscodingEntity timeTranscoding = null;
            TranscodingRuleEntity currentRule = null;
            TranscodingEntity lastTranscoding = null;
            string text = null;
            while (xmlReader.Read())
            {
                string localName;
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element:
                        localName = xmlReader.LocalName;
                        switch (localName)
                        {
                            case _dataflow:
                                if (StaxUtil.JumpToNode(xmlReader, _ref, localName))
                                {
                                    mappingSet.ParentId =
                                        this._dataflowSdmxType.GenerateUrn(xmlReader.GetAttribute(_agencyid), xmlReader.GetAttribute(_id), xmlReader.GetAttribute(_version)).ToString();
                                }
                                break;
                            case _dataset:
                                if (StaxUtil.JumpToNode(xmlReader, _ref, localName))
                                {
                                    mappingSet.DataSetId = xmlReader.GetAttribute(_id);
                                }
                                break;
                            case _timeComponentMap:
                               timeComponentMapping = this.BuildComponentMappingEntity(id);
                                lastMapping = timeComponentMapping;
                                entities.Add(timeComponentMapping);
                                break;
                            case _timeMap:
                                if (timeComponentMapping != null)
                                {
                                    if (timeTranscoding == null)
                                    {
                                        timeTranscoding = this.BuildTranscodingEntity(timeComponentMapping);
                                        timeTranscoding.TimeTranscoding = new List<TimeTranscodingEntity>();
                                        entities.Add(timeTranscoding);
                                    }
                                    
                                    TimeTranscodingEntity timeTranscodingEntity = new TimeTranscodingEntity();
                                    timeTranscodingEntity.Frequency = xmlReader.GetAttribute(_frequencytype);
                                    timeTranscoding.TimeTranscoding.Add(timeTranscodingEntity);
                                }
                                break;
                            case _year:
                                {
                                    var lastTimeMap = timeTranscoding?.TimeTranscoding?.LastOrDefault();
                                    if (lastTimeMap != null)
                                    {
                                        if (lastTimeMap.Year == null)
                                        {
                                            lastTimeMap.Year = new TimeTranscoding();
                                        }

                                        lastTimeMap.Year.Length = TryGetNumber(xmlReader, -1);
                                        lastTimeMap.Year.Start = TryGetNumber(xmlReader, 0);
                                    }
                                }
                                break;
                            case _period:
                                {
                                    var lastTimeMap = timeTranscoding?.TimeTranscoding?.LastOrDefault();
                                    if (lastTimeMap != null)
                                    {
                                        if (lastTimeMap.Period == null)
                                        {
                                            lastTimeMap.Period = new PeriodTimeTranscoding();
                                        }

                                        lastTimeMap.Period.Length = TryGetNumber(xmlReader, -1);
                                        lastTimeMap.Period.Start = TryGetNumber(xmlReader, 0);
                                    }
                                }
                                break;
                            case _componentMap:
                                {
                                    lastMapping = BuildComponentMappingEntity(id);
                                    entities.Add(lastMapping);
                                }
                                break;
                            case _codeMap:
                                {
                                    if (lastMapping != null)
                                    {
                                        if (lastTranscoding == null)
                                        {
                                            lastTranscoding = this.BuildTranscodingEntity(lastMapping);
                                            entities.Add(lastTranscoding);
                                        }

                                        currentRule = new TranscodingRuleEntity();
                                        currentRule.ParentId = lastMapping.TranscodingId;
                                        entities.Add(currentRule);
                                    }
                                }
                                break;
                        }
                        break;
                    case XmlNodeType.Text:
                    case XmlNodeType.CDATA:
                        text = xmlReader.Value;
                        break;
                    case XmlNodeType.EndElement:
                        localName = xmlReader.LocalName;
                        switch (localName)
                        {
                            case _timeComponent:
                                if (timeComponentMapping != null)
                                {
                                    if (!string.IsNullOrWhiteSpace(text))
                                    {
                                        timeComponentMapping.Component = new Component() { ObjectId = text };
                                    }
                                    else
                                    {
                                        timeComponentMapping.Component = new Component() { ObjectId = DimensionObject.TimeDimensionFixedId };
                                    }
                                }
                                break;
                            case _dateColumn:
                                {
                                    var lastTimeMap = timeTranscoding?.TimeTranscoding.LastOrDefault();
                                    if (lastTimeMap != null && !string.IsNullOrWhiteSpace(text))
                                    {
                                        lastTimeMap.DateColumn = this.BuildDataSetColumn(text, mappingSet, entities);

                                        lastTimeMap.IsDateTime = true;
                                        AddColumnToTImeMapping(timeComponentMapping, lastTimeMap.DateColumn);
                                    }
                                }
                                break;
                            case _localName:
                                if (lastMapping != null && !string.IsNullOrWhiteSpace(text))
                                {
                                    var column = this.BuildDataSetColumn(text, mappingSet, entities);
                                    if (lastMapping.GetColumns() == null)
                                    {
                                        lastMapping.SetColumns(new List<DataSetColumnEntity>());
                                    }

                                    lastMapping.GetColumns().Add(column);
                                }
                                break;
                            case _year:
                                {
                                    var lastTimeMap = timeTranscoding?.TimeTranscoding?.LastOrDefault();
                                    if (lastTimeMap != null && !string.IsNullOrWhiteSpace(text))
                                    {
                                        if (lastTimeMap.Year == null)
                                        {
                                            lastTimeMap.Year = new TimeTranscoding();
                                        }
                                       lastTimeMap.Year.Column = this.BuildDataSetColumn(text, mappingSet, entities);
                                        AddColumnToTImeMapping(timeComponentMapping, lastTimeMap.Year.Column);
                                    }
                                }
                                break;
                            case _period:
                                {
                                    var lastTimeMap = timeTranscoding?.TimeTranscoding?.LastOrDefault();
                                    if (lastTimeMap != null && !string.IsNullOrWhiteSpace(text))
                                    {
                                        if (lastTimeMap.Period == null)
                                        {
                                            lastTimeMap.Period = new PeriodTimeTranscoding();
                                        }

                                        lastTimeMap.Period.Column = this.BuildDataSetColumn(text, mappingSet, entities);
                                        AddColumnToTImeMapping(timeComponentMapping, lastTimeMap.Period.Column);
                                    }
                                }
                                break;
                            case _sdmxComponent:
                                {
                                    AddComponentToMapping(lastMapping, text);
                                }
                                break;
                            case _sdmxcode:
                                {
                                    AddSdmxCode(currentRule, text);
                                }
                                break;
                            case _localCode:
                                {
                                    AddLocalCode(currentRule, lastMapping, text);
                                }
                                break;
                            case Name:
                                mappingSet.Name = text;
                                break;
                            case _constantvalue:
                                if (lastMapping != null)
                                {
                                    lastMapping.ConstantValue = text;
                                }
                                break;
                            case _codeMap:
                                currentRule = null;
                                break;
                            case _componentMap:
                            case _timeComponentMap:
                                lastMapping = null;
                                timeComponentMapping = null;
                                timeTranscoding = null;
                                lastTranscoding = null;
                                break;
                            case RootTagName:
                                return entities;

                        }
                        text = null;
                        break;
                }
            }

            return entities;
        }

        private ComponentMappingEntity BuildComponentMappingEntity(string id)
        {
            ComponentMappingEntity timeComponentMapping;
            timeComponentMapping = new ComponentMappingEntity();
            timeComponentMapping.Type = ComponentMappingType.Normal.AsMappingStoreType();
            timeComponentMapping.ParentId = id;
            timeComponentMapping.EntityId = this.CreateNextEntityId();
            return timeComponentMapping;
        }

        private static void AddColumnToTImeMapping(ComponentMappingEntity timeComponentMapping, DataSetColumnEntity dataSetColumnEntity)
        {
            if (timeComponentMapping != null)
            {
                if (timeComponentMapping.GetColumns() == null)
                {
                    timeComponentMapping.SetColumns(new List<DataSetColumnEntity>());
                }

                timeComponentMapping.GetColumns().Add(dataSetColumnEntity);
            }
        }

        private static void AddComponentToMapping(ComponentMappingEntity lastMapping, string text)
        {
            if (lastMapping != null && !string.IsNullOrWhiteSpace(text))
            {
                lastMapping.Component = new Component() { ObjectId = text };
            }
        }

        private static void AddSdmxCode(TranscodingRuleEntity currentRule, string text)
        {
            if (currentRule != null)
            {
                currentRule.DsdCodeEntity = new IdentifiableEntity() { ObjectId = text };
            }
        }

        private static void AddLocalCode(TranscodingRuleEntity currentRule, ComponentMappingEntity lastMapping, string text)
        {
            if (currentRule != null)
            {
                if (currentRule.LocalCodes == null)
                {
                    currentRule.LocalCodes = new List<LocalCodeEntity>();
                }

                string columnEntityId = lastMapping?.GetColumns()?.FirstOrDefault()?.EntityId;
                if (lastMapping?.GetColumns().Count > currentRule.LocalCodes.Count)
                {
                    columnEntityId = lastMapping.GetColumns()[currentRule.LocalCodes.Count].EntityId;
                }

                currentRule.LocalCodes.Add(new LocalCodeEntity() { Name = text, ParentId = columnEntityId });
            }
        }

        private DataSetColumnEntity BuildDataSetColumn(string text, MappingSetEntity mappingSet, ICollection<IEntity> entities)
        {
            var dataSetColumnEntity = entities.OfType<DataSetColumnEntity>().FirstOrDefault(x => x.Name.Equals(text));
            if (dataSetColumnEntity == null)
            {
                dataSetColumnEntity = new DataSetColumnEntity() { Name = text, ParentId = mappingSet.DataSetId, EntityId = this.CreateNextEntityId() };
                entities.Add(dataSetColumnEntity);
            }

            return dataSetColumnEntity;
        }

        private TranscodingEntity BuildTranscodingEntity(ComponentMappingEntity componentMapping)
        {
            var transcoding = new TranscodingEntity();
            transcoding.ParentId = componentMapping.EntityId;
            transcoding.EntityId = this.CreateNextEntityId();
            componentMapping.TranscodingId = transcoding.EntityId;
            return transcoding;
        }

        private static int TryGetNumber(XmlReader reader, int defaultValue)
        {
            int number;
            var attribute = reader.GetAttribute(_length);
            if (attribute == null)
            {
                return defaultValue;
            }
            if (int.TryParse(attribute, NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite | NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out number))
            {
                return number;
            }

            return defaultValue;
        }

        private string CreateNextEntityId()
        {
            return (this._entityIndex++).ToString(CultureInfo.InvariantCulture);
        }

        //public List<IEntity> Read(XmlReader xmlReader)
        //{
        //    var mappingSet = new MappingSetEntity();
        //    var xElement = (XElement)XNode.ReadFrom(xmlReader);
        //    mappingSet.Name = xElement.Attribute(this._id)?.Value;
        //    var dataflowRef = xElement.Descendant(this._dataflow).Descendant(this._ref);
        //    if (dataflowRef != null)
        //    {
        //        var agencyId = dataflowRef.Attribute(this._agencyid)?.Value;
        //        var maintainableId = dataflowRef.Attribute(this._id)?.Value;
        //        var version = dataflowRef.Attribute(this._version)?.Value;
        //        mappingSet.ParentId = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)
        //            .GenerateUrn(agencyId, maintainableId, version).ToString();
        //    }
        //    mappingSet.DataSetId = xElement.Descendant(this._dataset)?.Descendant(this._ref)?.Attribute(this._id)?.Value;
        //    var timeComponents = xElement.Descendants().Where(x=>x.Name.LocalName == this._timeComponentMap);
        //    var timeComponentMappings = new List<ComponentMappingEntity>();
        //    var timeTranscodings = new List<TimeTranscodingEntity>();
        //    foreach (var timeComponent in timeComponents)
        //    {
        //        var componentMapping = new ComponentMappingEntity()
        //        {
        //            Component = new Component(),
        //            Columns = new List<DataSetColumnEntity>()
        //        };
        //        componentMapping.Component.ObjectId = timeComponent.Descendant(this._timeComponent)?.Value;
        //        componentMapping.Columns.Add(new DataSetColumnEntity()
        //        {
        //            Name = timeComponent.Descendant(this._localName)?.Value
        //        });
        //        componentMapping.ConstantValue = timeComponent.Descendant(this._constantvalue)?.Value;
        //        timeComponentMappings.Add(componentMapping);

        //        var timeMaps = timeComponent.Descendants().Where(x => x.Name.LocalName == this._timeMap);
        //        foreach (var timeMap in timeMaps)
        //        {
        //            var transcodingEntity = new TimeTranscodingEntity {DateColumn = new DataSetColumnEntity()};
        //            transcodingEntity.DateColumn.Name = timeMap.Descendant(this._dateColumn)?.Value;
        //            transcodingEntity.Frequency = timeMap.Attribute(this._frequencytype)?.Value;

        //            var yearStart = timeMap.Descendant(this._year)?.Descendant(this._start)?.Value;
        //            var yearLength = timeMap.Descendant(this._year)?.Descendant(this._length)?.Value;
        //            transcodingEntity.Year = new TimeTranscoding()
        //            {
        //                Start = string.IsNullOrWhiteSpace(yearStart)? (int?) null: int.Parse(yearStart),
        //                Length = string.IsNullOrWhiteSpace(yearLength)?(int?) null: int.Parse(yearLength),
        //            };
        //            var periodStart = timeMap.Descendant(this._period)?.Descendant(this._start)?.Value;
        //            var periodLength = timeMap.Descendant(this._period)?.Descendant(this._length)?.Value;
        //            transcodingEntity.Period = new PeriodTimeTranscoding()
        //            {
        //                Start = string.IsNullOrWhiteSpace(periodStart)?(int?) null :int.Parse(periodStart),
        //                Length = string.IsNullOrWhiteSpace(periodLength)?(int?) null :int.Parse(periodLength),
        //            };

        //            timeTranscodings.Add(transcodingEntity);
        //        }
        //    }

        //    var components = new List<ComponentMappingEntity>();
        //    var transcodingRuleEntities = new List<TranscodingRuleEntity>();
        //    var componentMaps = xElement.Descendants().Where(x => x.Name.LocalName == this._componentMap);
        //    var transcodingRule = new TranscodingRuleEntity();
        //    foreach (var componentMap in componentMaps)
        //    {
        //        var componentMapping = new ComponentMappingEntity()
        //        {
        //            Component = new Component(),
        //            Columns = new List<DataSetColumnEntity>()
        //        };
        //        componentMapping.Component.ObjectId = componentMap.Descendant(this._sdmxComponent)?.Value;
        //        componentMapping.ConstantValue = componentMap.Descendant(this._constantvalue)?.Value;
        //        componentMapping.Columns.Add(new DataSetColumnEntity()
        //        {
        //            Name = componentMap.Descendant(this._localName)?.Value
        //        });

        //        foreach (var codeMap in componentMap.Descendants().Where(x => x.Name.LocalName == this._codeMap))
        //        {
        //            transcodingRule.DsdCodeEntity = new IdentifiableEntity()
        //            {
        //                ObjectId = codeMap.Descendant(this._sdmxcode)?.Value
        //            };
        //            transcodingRule.LocalCodes = new List<LocalCodeEntity>();

        //            foreach (var localCode in codeMap.Descendants().Where(x => x.Name.LocalName == this._localCode))
        //            {
        //                transcodingRule.LocalCodes.Add(new LocalCodeEntity()
        //                {
        //                    ObjectId = localCode.Value
        //                });
        //            }

        //            transcodingRuleEntities.Add(transcodingRule);
        //        }

        //        components.Add(componentMapping);
        //    }

        //    var entities = new List<IEntity>()
        //    {
        //        mappingSet
        //    };
        //    entities.AddRange(timeComponentMappings);
        //    entities.AddRange(timeTranscodings);
        //    entities.AddRange(components);
        //    entities.AddRange(transcodingRuleEntities);
        //    return entities;
        //}

        public void Write(XmlWriter xmlWriter, EntitiesByType entities)
        {
            xmlWriter.WriteStartElement("MappingSets");
            foreach (var mappingSetEntity in entities.GetEntities<MappingSetEntity>())
            {
                if (!entities.GetEntities<ComponentMappingEntity>().Any(entity => string.Equals(entity.ParentId, mappingSetEntity.EntityId)))
                {
                    continue;
                }

                xmlWriter.WriteStartElement(this.RootName);
                xmlWriter.WriteAttributeString(_id, mappingSetEntity.EntityId);
                xmlWriter.WriteElementString("Name", mappingSetEntity.Name);
                this.WriteDataflowElement(xmlWriter, mappingSetEntity);
                this.WriteDatasetElement(xmlWriter, mappingSetEntity);
                this.WriteTimeComponent(xmlWriter, mappingSetEntity.EntityId, entities);
                this.WriteCodeMap(xmlWriter, mappingSetEntity.EntityId, entities);
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
        }

        private void WriteCodeMap(XmlWriter xmlWriter, string entityId, EntitiesByType entities)
        {
            var componentMappingEntities = entities.GetEntities(EntityType.Mapping).Cast<ComponentMappingEntity>().Where(x => x.ParentId == entityId  && x.Component != null && !DimensionObject.TimeDimensionFixedId.Equals(x.Component?.ObjectId));
            foreach (var componentMappingEntity in componentMappingEntities)
            {
                xmlWriter.WriteStartElement(_componentMap);
                xmlWriter.WriteElementString(_sdmxComponent, componentMappingEntity.Component?.ObjectId);

                if (!string.IsNullOrWhiteSpace(componentMappingEntity.ConstantValue))
                {
                    xmlWriter.WriteElementString(_constantvalue, componentMappingEntity.ConstantValue);
                }
                else
                {
                    foreach (var dataSetColumnEntity in componentMappingEntity.GetColumns())
                    {
                        xmlWriter.WriteElementString(_localName, dataSetColumnEntity.Name);
                    }

                    if (!string.IsNullOrWhiteSpace(componentMappingEntity.TranscodingId))
                    {
                        var transcodingEntities = entities.GetEntities(EntityType.Transcoding).Cast<TranscodingEntity>().Where(x => x.EntityId == componentMappingEntity.TranscodingId);
                        foreach (var transcodingEntity in transcodingEntities)
                        {
                            try
                            {
                                var transcodingRuleEntities = entities.GetEntities(EntityType.TranscodingRule).Cast<TranscodingRuleEntity>().Where(x => x.ParentId == transcodingEntity.EntityId);
                                foreach (var transcodingRule in transcodingRuleEntities)
                                {
                                    xmlWriter.WriteStartElement(_codeMap);
                                    xmlWriter.WriteElementString(_sdmxcode, transcodingRule.DsdCodeEntity.ObjectId);
                                    foreach (var localCodeEntity in transcodingRule.LocalCodes)
                                    {
                                        xmlWriter.WriteElementString(_localCode, localCodeEntity.ObjectId);
                                    }
                                    xmlWriter.WriteEndElement();
                                }
                            }
                            catch (Exception)
                            {
                                Console.WriteLine($"Exception {transcodingEntity.EntityId}");
                                throw;
                            }
                        }
                    }
                }
                xmlWriter.WriteEndElement();
            }
        }

        private void WriteDataflowElement(XmlWriter xmlWriter, MappingSetEntity mappingSetEntity)
        {
            var dataflow = new StructureReferenceImpl(mappingSetEntity.ParentId);
            xmlWriter.WriteStartElement(_dataflow);
            xmlWriter.WriteStartElement(_ref);
            xmlWriter.WriteAttributeString(_id, dataflow.MaintainableId);
            xmlWriter.WriteAttributeString(_version, dataflow.Version);
            xmlWriter.WriteAttributeString(_agencyid, dataflow.AgencyId);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
        }

        private void WriteDatasetElement(XmlWriter xmlWriter, MappingSetEntity mappingSetEntity)
        {
            xmlWriter.WriteStartElement(_dataset);
            xmlWriter.WriteStartElement(_ref);
            xmlWriter.WriteAttributeString(_id, mappingSetEntity.DataSetId);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
        }

        private void WriteTimeColumn(XmlWriter xmlWriter, string elementName, TimeTranscoding year)
        {
            if (year == null)
            {
                return;
            }

            xmlWriter.WriteStartElement(elementName);
            if (year.Start != null)
            {
                xmlWriter.WriteAttributeString(_start, XmlConvert.ToString(year.Start.Value));
            }

            if (year.Length != null)
            {
                xmlWriter.WriteAttributeString(_length, XmlConvert.ToString(year.Length.Value));
            }

            xmlWriter.WriteString(year.Column.Name);

            xmlWriter.WriteEndElement();
        }

        private void WriteTimeComponent(XmlWriter xmlWriter, string entityId, EntitiesByType entities)
        {
            var componentMappingEntities = entities.GetEntities(EntityType.Mapping).Cast<ComponentMappingEntity>().Where(x => x.ParentId == entityId && x.Component != null && x.Component.ObjectId == "TIME_PERIOD").ToList();
            if (componentMappingEntities.Any())
            {
                xmlWriter.WriteStartElement(_timeComponentMap);
                foreach (var componentMappingEntity in componentMappingEntities)
                {
                    var transcodingEntity = entities.GetEntities<TranscodingEntity>().FirstOrDefault(x => x.ParentId == componentMappingEntity.EntityId);

                    if (componentMappingEntity.Component != null)
                    {
                        xmlWriter.WriteElementString(_timeComponent, componentMappingEntity.Component.ObjectId);
                    }

                    if (!string.IsNullOrWhiteSpace(componentMappingEntity.ConstantValue))
                    {
                        xmlWriter.WriteElementString(_constantvalue, componentMappingEntity.ConstantValue);
                    }
                    else if (transcodingEntity != null && transcodingEntity.HasTimeTrascoding())
                    {
                        var timeTranscodingEntities = transcodingEntity.TimeTranscoding;
                        foreach (var timeTranscoding in timeTranscodingEntities)
                        {
                            xmlWriter.WriteStartElement(_timeMap);
                            xmlWriter.WriteAttributeString(_frequencytype, timeTranscoding.Frequency);
                            if (timeTranscoding.DateColumn != null && !string.IsNullOrWhiteSpace(timeTranscoding.DateColumn.Name))
                            {
                                xmlWriter.WriteElementString(_dateColumn, timeTranscoding.DateColumn.Name);
                            }
                            else
                            {
                                this.WriteTimeColumn(xmlWriter, _year, timeTranscoding.Year);
                                this.WriteTimeColumn(xmlWriter, _period, timeTranscoding.Period);
                            }
                            xmlWriter.WriteEndElement();
                        }
                    }
                    else if (componentMappingEntity.GetColumns() != null)
                    {
                        foreach (var dataSetColumnEntity in componentMappingEntity.GetColumns())
                        {
                            xmlWriter.WriteElementString(_localName, dataSetColumnEntity.Name);
                        }
                    }
                }
                xmlWriter.WriteEndElement();
            }
        }
    }
}