// -----------------------------------------------------------------------
// <copyright file="ValiditiyBaseRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2022-09-13
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    /// <summary>
    /// A base class for retrieving entities that use the <c>N_UPDATE_STATUS</c> table
    /// </summary>
    public class UpdateStatusRetrieverEngine : IEntityRetrieverEngine<UpdateStatusEntity> 
    {
        private const string ComponentMappingAlias = "CM";
        private readonly Database _database;
        private readonly AdvanceTimeTranscodingRetriever _advanceTimeTranscodingRetriever;

        /// <summary>
        /// Initializes a new instance
        /// </summary>
        /// <param name="database">the mapping store database</param>
        /// <exception cref="ArgumentNullException">database is null</exception>
        public UpdateStatusRetrieverEngine(Database database)
        {
            if (database == null)
            {
                throw new ArgumentNullException(nameof(database));
            }

            _database = database;
        }
/// <inheritdoc/>

        public IEnumerable<UpdateStatusEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            var whereClauseBuilder = new WhereClauseBuilder(_database);
            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "STR_MAP_SET_ID", ComponentMappingAlias));
            clauses.Add(whereClauseBuilder.Build(query.ParentId, "STR_MAP_SET_ID", ComponentMappingAlias));
            
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
             
            var sqlQuery = string.Format(CultureInfo.InvariantCulture,
                "select STR_MAP_SET_ID, DATASET_COLUMN_NAME, CONSTANT_OR_DEFAULT, INSERT_VALUE, UPDATE_VALUE, DELETE_VALUE  from N_UPDATE_STATUS {0} {1} {2} ",
                ComponentMappingAlias, whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);
            var rows = _database.Query(sqlQuery, param);
            return this.ExtractMappings(rows);
             
        }

        private IEnumerable<UpdateStatusEntity> ExtractMappings(IEnumerable<dynamic> rows)
        {
            foreach (var row in rows)
            {
                var mappingEntity = new UpdateStatusEntity();
                mappingEntity.SetEntityId((long)row.STR_MAP_SET_ID);
                mappingEntity.ParentId = mappingEntity.EntityId;
                if (row.DATASET_COLUMN_NAME != null)
                {
                    mappingEntity.Column = new DataSetColumnEntity() { Name = row.DATASET_COLUMN_NAME};
                    AddRuleIfSet(ObservationActionEnumType.Added, row.INSERT_VALUE, mappingEntity);
                    AddRuleIfSet(ObservationActionEnumType.Updated, row.UPDATE_VALUE, mappingEntity);
                    AddRuleIfSet(ObservationActionEnumType.Deleted, row.DELETE_VALUE, mappingEntity);
                }
               else
                {
                    mappingEntity.ConstantValue= (string) row.CONSTANT_OR_DEFAULT;
                }

                yield return mappingEntity;
            }
        }
/// <inheritdoc/>

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }

        private void AddRuleIfSet(ObservationActionEnumType action, string localValue, UpdateStatusEntity entity)
        {
            if (localValue != null)
            {
                entity.Add(action, localValue);
            }
        }
    }
}