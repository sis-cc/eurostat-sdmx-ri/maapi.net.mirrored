﻿using System.Collections.Generic;
using System.Xml;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public interface IImportExportEntity
    {
        EntityType EntityType { get; }
        string RootName { get; set; }
        void Write(XmlWriter xmlWriter, EntitiesByType entities);
        List<IEntity> Read(XmlReader xmlReader, string id);
    }
}