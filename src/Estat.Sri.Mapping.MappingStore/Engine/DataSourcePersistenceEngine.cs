// -----------------------------------------------------------------------
// <copyright file="MappingSetPersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2017-02-23
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using Dapper;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Utils;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    public class DataSourcePersistenceEngine : EntityPersistenceEngine<DataSourceEntity>
    {
        private readonly IEntityIdFromUrn _entityIdFromUrn;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityPersistenceEngine{TEntity}" /> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        /// <param name="updateInfoFactoryManager">The update information factory manager.</param>
        /// <param name="mappingStoreIdentifier">The mapping store identifier.</param>
        /// <param name="entityPropertiesExtractor">The entity properties extractor.</param>
        /// <param name="commmandsFromUpdateInfo">The commmands from update information.</param>
        /// <param name="entityIdFromUrn">The entity identifier from urn.</param>
        public DataSourcePersistenceEngine(DatabaseManager databaseManager, UpdateInfoFactoryManager updateInfoFactoryManager, string mappingStoreIdentifier, IEntityPropertiesExtractor entityPropertiesExtractor, ICommmandsFromUpdateInfo commmandsFromUpdateInfo, IEntityIdFromUrn entityIdFromUrn) : base(databaseManager, updateInfoFactoryManager, mappingStoreIdentifier, entityPropertiesExtractor, commmandsFromUpdateInfo)
        {
            this._entityIdFromUrn = entityIdFromUrn;
        }

        public override void Delete(string entityId, EntityType entityType)
        {
            var updateInfo = new UpdateInfoAction
            {
                SqlStatementType = SqlStatementType.Update,
                DataInformationType = DatabaseInformationType.Dataflow,
                PathAndValues = new Dictionary<string, object>
                {
                    {DataflowTableInformations.DataSourceId.ModelName,null}
                }
            };

            long entityIdValue;
            if (!long.TryParse(entityId, NumberStyles.None, CultureInfo.InvariantCulture, out entityIdValue))
            {
                throw new BadRequestException("Invalid entity id value");
            }

            var sqlUpdate = "Update DATAFLOW set DATA_SOURCE_ID = null where DATA_SOURCE_ID = {0}";
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            database.UsingLogger().ExecuteNonQueryFormat(sqlUpdate, database.CreateInParameter(nameof(entityIdValue), DbType.Int64, entityIdValue));

            // No mapping set without dataflow
            base.Delete(entityId, entityType);
        }

        public override IEnumerable<long> Add(IList<DataSourceEntity> entities)
        {
            var result = new List<long>();
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            using (var context = new ContextWithTransaction(database))
            {
                MappingStoreRetrieval.Manager.Database transactionDb = context.DatabaseUnderContext;
                EntityIdFromUrn entityIdFromUrn = new EntityIdFromUrn(transactionDb);
                foreach (var dataSourceEntity in entities)
                {
                    // get the given entity id if provided                    
                    long? givenMappingSetEntityId = dataSourceEntity.HasEntityId() ? dataSourceEntity.GetEntityId() : (long?)null;

                    // get and check if the given dataflow exists
                    var dataflowSysId = entityIdFromUrn.Get(dataSourceEntity.ParentId);
                    if (dataflowSysId < 1)
                    {
                        throw new SdmxNoResultsException("Could not find the dataflow with urn:" + dataSourceEntity.ParentId);
                    }

                    var parameters = new DynamicParameters();
                    parameters.Add("p_data_url", dataSourceEntity.DataUrl, dbType: DbType.AnsiString);
                    parameters.Add("p_wsdl_url", dataSourceEntity.WSDLUrl, DbType.AnsiString);
                    parameters.Add("p_wadl_url", dataSourceEntity.WADLUrl, dbType: DbType.AnsiString);
                    parameters.Add("p_is_rest", dataSourceEntity.IsRest, dbType: DbType.Int64);
                    parameters.Add("p_is_ws", dataSourceEntity.IsWs, dbType: DbType.Int64);
                    parameters.Add("p_is_simple", dataSourceEntity.IsSimple, dbType: DbType.Int64);
                    parameters.Add("p_pk", dbType: DbType.Int64, direction: ParameterDirection.Output);
                    context.Connection.Execute("INSERT_DATA_SOURCE", parameters, commandType: CommandType.StoredProcedure);
                    var dataSourceId = parameters.Get<long>("p_pk");

                    result.Add(dataSourceId);
                    var dataflowIdParameter = database.CreateInParameter("DF_ID", DbType.Int64, dataflowSysId);
                    var getDataSourceParameters = new List<DbParameter>() { dataflowIdParameter };
                    var oldDataSourceId = transactionDb.ExecuteScalar("select DATA_SOURCE_ID from DATAFLOW where DF_ID = " + dataflowIdParameter.ParameterName,getDataSourceParameters );
                    transactionDb.ExecuteNonQueryFormat("UPDATE DATAFLOW SET DATA_SOURCE_ID = {0} WHERE DF_ID ={1}", dataSourceId, dataflowSysId);
                    if (!DBNull.Value.Equals(oldDataSourceId))
                    {
                        transactionDb.ExecuteNonQueryFormat("DELETE FROM DATA_SOURCE WHERE DATA_SOURCE_ID = {0}", (long)oldDataSourceId);
                    }
                }

                context.Complete();
            }

            return result;
        }
        public override long Add(DataSourceEntity entity)
        {
            return this.Add(new[] { entity }).First();
        }

        public override bool SetActualRelationId(IEntity entity, EntityIdMapping mapping, IEntityStreamingWriter responseWriter)
        {
            //DataSourceEntity dataSourceEntity = (DataSourceEntity)entity;
            //String actualDatasetId = mapping.Get(EntityType., mappingSet.DataSetId);
            //if (String.IsNullOrWhiteSpace(actualDatasetId))
            //{
            //    responseWriter.WriteStatus(StatusType.Error, "could not find entity id for dataset id " + mappingSet.DataSetId);
            //    return false;
            //}
            //dataSourceEntity.ParentId = actualDatasetId;
            return true;
        }
    }
}