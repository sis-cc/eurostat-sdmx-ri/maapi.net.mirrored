// -----------------------------------------------------------------------
// <copyright file="TimePreFormattedEntityRetriever.cs" company="EUROSTAT">
//   Date Created : 2021-05-26
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Dapper;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// Engine to retrieve the <see cref="TimePreFormattedEntity"/>.
    /// </summary>
    public class TimePreFormattedEntityRetriever : IEntityRetrieverEngine<TimePreFormattedEntity>
    {
        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;
        private const string TimePreFormattedAlias = "TPF";
        private List<string> DatasetColumnJoins => new List<string>()
        {
            "INNER JOIN DATASET_COLUMN OUTCOL ON " + TimePreFormattedAlias + ".OUTPUT_COLUMN = OUTCOL.COL_ID",
            "INNER JOIN DATASET_COLUMN FROMCOL ON " + TimePreFormattedAlias + ".FROM_SOURCE = FROMCOL.COL_ID",
            "INNER JOIN DATASET_COLUMN TOCOL ON " + TimePreFormattedAlias + ".TO_SOURCE = TOCOL.COL_ID",
        };
        private readonly Database _database;
        private readonly string sid;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimePreFormattedEntityRetriever"/> class.
        /// </summary>
        /// <param name="databaseManager">The database to use.</param>
        /// <param name="sid">The store name.</param>
        public TimePreFormattedEntityRetriever(Database databaseManager, string sid)
        {
            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }

            this._database = databaseManager;
            this.sid = sid;

            this._fieldsToReturn = new Dictionary<Detail, IList<string>>();
            this._fieldsToReturn.Add(Detail.IdOnly, new[] { "TPF.STR_MAP_SET_ID AS MappingSetId", "OUTCOL.COL_ID as OutputColumnId", "FROMCOL.COL_ID as FromColumnId", "TOCOL.COL_ID as ToColumnId" });
            this._fieldsToReturn.Add(Detail.IdAndName, this._fieldsToReturn[Detail.IdOnly].Concat(new[] { "OUTCOL.NAME as OutputColumnName", "FROMCOL.NAME as FromColumnName", "TOCOL.NAME as ToColumnName" }).ToArray());
            this._fieldsToReturn.Add(Detail.Full, this._fieldsToReturn[Detail.IdAndName].Concat(new[] { "OUTCOL.DESCRIPTION as OutputColumnDescription", "FROMCOL.DESCRIPTION as FromColumnDescription", "TOCOL.DESCRIPTION as ToColumnDescription" }).ToArray());
        }

        /// <summary>
        /// Get all <see cref="TimePreFormattedEntity"/> entites matching the <paramref name="query"/>.
        /// </summary>
        /// <param name="query">The search criteria.</param>
        /// <param name="detail">The detail level of the return info.</param>
        /// <returns>A collection of <see cref="TimePreFormattedEntity"/>.</returns>
        public IEnumerable<TimePreFormattedEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var db = this._database;
            return GetRecordsFromDatabase(query, detail, db).Select(record => record.GetEntity());
        }

        /// <summary>
        /// Write in a stream the <see cref="TimePreFormattedEntity"/> matching the <paramref name="query"/>.
        /// </summary>
        /// <param name="query">The search criteria.</param>
        /// <param name="detail">The detail level of the return info.</param>
        /// <param name="entityWriter">The stream writer to write the <see cref="TimePreFormattedEntity"/> in.</param>
        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            if (entityWriter == null)
            {
                throw new ArgumentNullException(nameof(entityWriter));
            }


            foreach (TimePreFormattedEntity entity in GetRecordsFromDatabase(query, detail, this._database).Select(record => record.GetEntity()))
            {
                entity.StoreId = this.sid;
                entityWriter.Write(entity);
            }
        }

        private IEnumerable<MATimePreFormattedEntity> GetRecordsFromDatabase(IEntityQuery query, Detail detail, Database db)
        {
            WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder(db);
            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "STR_MAP_SET_ID", TimePreFormattedAlias));

            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;

            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3} {4}", fields, "N_MAPPING_TIME_DIMENSION " + TimePreFormattedAlias, string.Join(" ", DatasetColumnJoins), whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);

            return db.Query<MATimePreFormattedEntity>(sqlQuery, param);
        }
    }
}
