// -----------------------------------------------------------------------
// <copyright file="HeaderTemplatePersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2017-04-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Transactions;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Newtonsoft.Json;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class HeaderTemplatePersistenceEngine<TEntity> : IEntityPersistenceEngine<TEntity> where TEntity : IEntity
    {
        private readonly ICommmandsFromUpdateInfo _commmandsFromUpdateInfo;
        private readonly DatabaseManager _databaseManager;
        private readonly IEntityIdFromUrn _entityIdFromUrn;
        private readonly string _mappingStoreIdentifier;
        private readonly string _language = "en";
        private readonly EntityImporterWithIdMapping _entityImporterWithIdMapping = new EntityImporterWithIdMapping();

        /// <summary>
        /// Initializes a new instance of the <see cref="HeaderTemplatePersistenceEngine"/> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        /// <param name="mappingStoreIdentifier"></param>
        /// <param name="commmandsFromUpdateInfo"></param>
        /// <param name="entityIdFromUrn"></param>
        public HeaderTemplatePersistenceEngine(DatabaseManager databaseManager, string mappingStoreIdentifier, ICommmandsFromUpdateInfo commmandsFromUpdateInfo, IEntityIdFromUrn entityIdFromUrn)
        {
            this._databaseManager = databaseManager;
            this._mappingStoreIdentifier = mappingStoreIdentifier;
            this._commmandsFromUpdateInfo = commmandsFromUpdateInfo;
            this._entityIdFromUrn = entityIdFromUrn;
        }

        public long Add(TEntity entity)
        {
            var headerEntity = entity as HeaderEntity;
            long headerId;

            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            var dataflowId = this._entityIdFromUrn.Get(headerEntity.ParentId);
            using (var scope = new TransactionScope())
            using (var connection = database.CreateConnection())
            {
                connection.Open();

                var headerUpdateInforAction = new UpdateInfoAction
                {
                    DataInformationType = DatabaseInformationType.Header,
                    SqlStatementType = SqlStatementType.Insert,
                    PathAndValues = this.GetHeaderPathAndValues(headerEntity, dataflowId)
                };

                var commands = this._commmandsFromUpdateInfo.Get(string.Empty, headerUpdateInforAction, database).ToList();

                headerId = database.ExecuteCommandWithConnectio(commands, connection);

                this.InsertParty(headerEntity.Sender, headerId, "Sender", database, connection);

                foreach (var party in headerEntity.Receiver)
                {
                    this.InsertParty(party, headerId, "Receiver", database, connection);
                }

                this.InsertHeaderDetails(headerEntity, database, connection, headerId.ToString());

                scope.Complete();
                connection.Close();
            }

            return headerId;
        }

        public IEnumerable<long> Add(IList<TEntity> entities)
        {
            var ids = new List<long>();
            entities.ToList().ForEach(item => ids.Add(this.Add(item)));
            return ids;
        }

        public void Delete(string entityId, EntityType entityType)
        {
            var databaseInformationType = (DatabaseInformationType) Enum.Parse(typeof (DatabaseInformationType), entityType.ToString());
            this.Delete(entityId, databaseInformationType);
        }

        /// <summary>
        /// Persists the specified update information.
        /// </summary>
        /// <param name="patchRequest"></param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        public void Update(PatchRequest patchRequest, EntityType entityType, string entityId)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);

            using (var scope = new TransactionScope())
            using (var connection = database.CreateConnection())
            {
                connection.Open();

                var commandDefinitions = new List<CommandDefinition>();
                this.CreateCommandForHeader(patchRequest, entityId, commandDefinitions, database);
                this.CreateCommandForTransmissionAndSource(patchRequest, entityId, commandDefinitions, database, this._language, connection);
                database.ExecuteCommandWithConnectio(commandDefinitions, connection);
                var patchDocument = patchRequest.Find(item => item.Path.Contains("sender"));
                if (patchDocument != null)
                {
                    var party = JsonConvert.DeserializeObject<Party>(patchDocument.Value.ToString());
                    this.ReplaceParty(party, entityId, connection, database, "Sender");
                }
                patchDocument = patchRequest.Find(item => item.Path.Contains("receiver"));
                if (patchDocument != null)
                {
                    var parties = JsonConvert.DeserializeObject<List<Party>>(patchDocument.Value.ToString());
                    foreach (var party in parties)
                    {
                        this.ReplaceParty(party, entityId, connection, database, "Receiver");
                    }
                }
                scope.Complete();
            }
        }

        private static CommandDefinition GetDeleteCommand(string entityId, DatabaseInformationType databaseInformationType, Database database)
        {
            var databaseMapperManager = new DatabaseMapperManager(databaseInformationType);

            var tableName = databaseMapperManager.GetTableName();
            var primaryKeyColumn = databaseMapperManager.GetPrimaryKeyColumn();
            var identityColumnsAndValues = new Dictionary<string, object>
            {
                {primaryKeyColumn, entityId}
            };
            var commandDefinition = new DeleteSqlBuilder(tableName, identityColumnsAndValues, databaseMapperManager).CreateCommandDefinition(database);
            return commandDefinition;
        }

        private void CreateCommandForHeader(PatchRequest patchRequest, string entityId, List<CommandDefinition> commandDefinitions, Database database)
        {
            var headerAction = new UpdateInfoAction
            {
                DataInformationType = DatabaseInformationType.Header,
                SqlStatementType = SqlStatementType.Update,
                PathAndValues = new Dictionary<string, object>()
            };

            var patchDocument = patchRequest.Find(item => item.HasPath("test"));

            if (patchDocument != null)
            {
                headerAction.PathAndValues.Add(HeaderDataTableInformations.Test.ModelName, patchDocument.Value);
            }

            patchDocument = patchRequest.Find(item => item.HasPath("dataSetAgencyId"));

            if (patchDocument != null)
            {
                headerAction.PathAndValues.Add(HeaderDataTableInformations.DatasetAgencyId.ModelName, patchDocument.Value);
            }

            patchDocument = patchRequest.Find(item => item.HasPath("parentId"));

            if (patchDocument != null)
            {
                headerAction.PathAndValues.Add(HeaderDataTableInformations.DataflowId.ModelName, patchDocument.Value);
            }
            if (headerAction.PathAndValues.Any())
            {
                commandDefinitions.AddRange(this.GetCommandFromUpdateInfo(headerAction, entityId, database));
            }
        }

        private void CreateCommandForHeaderRelatedLocalisedString(string entityId, List<CommandDefinition> commandDefinitions, Database database, string language, List<PatchDocument> patchDocuments, string type, DbConnection connection)
        {
            var document = patchDocuments.Find(item => item.HasPath(type));
            if (document != null)
            {
                var localisedString = new UpdateInfoAction
                {
                    DataInformationType = DatabaseInformationType.HeaderLocalisedString,
                    PathAndValues = new Dictionary<string, object> {{HeaderLocalisedStringDataTableInformations.Type.ModelName, type}, {HeaderLocalisedStringDataTableInformations.Language.ModelName, language}, {HeaderLocalisedStringDataTableInformations.Text.ModelName, document.Value}}
                };
                var headerIdParam = database.BuildParameterName("HEADER_ID");
                var typeParam = database.BuildParameterName("Type");
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(headerIdParam, entityId);
                dynamicParameters.Add(typeParam, type);
                string sql = $"Select Count(*) from HEADER_LOCALISED_STRING where HEADER_ID ={headerIdParam} AND Type = {typeParam}";
                var numberOfRows = connection.ExecuteScalar(sql, dynamicParameters);
                if (numberOfRows.Equals(0))
                {
                    localisedString.SqlStatementType = SqlStatementType.Insert;
                    localisedString.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.HeaderId.ModelName, entityId);
                    localisedString.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.ContactId.ModelName, null);
                    localisedString.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.PartyId.ModelName, null);
                }
                else
                {
                    localisedString.SqlStatementType = SqlStatementType.Update;
                }
                commandDefinitions.AddRange(this.GetCommandFromUpdateInfo(localisedString, entityId, database));
            }
        }

        private void CreateCommandForTransmissionAndSource(PatchRequest patchRequest, string entityId, List<CommandDefinition> commandDefinitions, Database database, string language, DbConnection connection)
        {
            var patchDocuments = patchRequest.FindAll(item => !item.HasPath("sender") && !item.HasPath("receiver"));
            this.CreateCommandForHeaderRelatedLocalisedString(entityId, commandDefinitions, database, language, patchDocuments, "Name", connection);
            this.CreateCommandForHeaderRelatedLocalisedString(entityId, commandDefinitions, database, language, patchDocuments, "Source", connection);
        }

        private void Delete(string entityId, DatabaseInformationType databaseInformationType)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            var commandDefinition = GetDeleteCommand(entityId, databaseInformationType, database);

            database.ExecuteCommands(new [] {commandDefinition});
        }

        private IEnumerable<CommandDefinition> GetCommandFromUpdateInfo(UpdateInfoAction updateInfoAction, string entityId, Database mappingStoreDatabase)
        {
            var commandDefinitionManager = new CommandDefinitionManager(new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
            var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType, updateInfoAction.SqlStatementType);
            return commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, mappingStoreDatabase);
        }

        private Dictionary<string, object> GetHeaderPathAndValues(HeaderEntity headerEntity, long dataflowId)
        {
            var pathAndValues = new Dictionary<string, object>
            {
                {HeaderDataTableInformations.DatasetAgencyId.ModelName, headerEntity.DataSetAgencyId},
                {HeaderDataTableInformations.Test.ModelName, headerEntity.Test},
                {HeaderDataTableInformations.DataflowId.ModelName, dataflowId}
            };
            return pathAndValues;
        }

        private Dictionary<string, object> GetPartyPathAndValues(Party party, string headerId, string partyType)
        {
            var pathAndValues = new Dictionary<string, object>
            {
                {PartyDataTableInformations.HeaderId.ModelName, headerId},
                {PartyDataTableInformations.Type.ModelName, partyType},
                {PartyDataTableInformations.Id.ModelName, party.Name}
            };
            return pathAndValues;
        }

        private void InsertContactDetails(Contact contact, long contactId, Database database, DbConnection connection)
        {
            var updateInfoActions = new List<UpdateInfoAction>();
            if (!string.IsNullOrWhiteSpace(contact.Department))
            {
                updateInfoActions.Add(new UpdateInfoAction
                {
                    DataInformationType = DatabaseInformationType.HeaderLocalisedString,
                    SqlStatementType = SqlStatementType.Insert,
                    PathAndValues = new Dictionary<string, object>
                    {
                        {HeaderLocalisedStringDataTableInformations.ContactId.ModelName, contactId},
                        {HeaderLocalisedStringDataTableInformations.PartyId.ModelName, null},
                        {HeaderLocalisedStringDataTableInformations.HeaderId.ModelName, null},
                        {HeaderLocalisedStringDataTableInformations.Text.ModelName, contact.Department},
                        {HeaderLocalisedStringDataTableInformations.Type.ModelName, "Department"},
                        {HeaderLocalisedStringDataTableInformations.Language.ModelName, this._language}
                    }
                });
            }

            if (!string.IsNullOrWhiteSpace(contact.Role))
            {
                updateInfoActions.Add(new UpdateInfoAction
                {
                    DataInformationType = DatabaseInformationType.HeaderLocalisedString,
                    SqlStatementType = SqlStatementType.Insert,
                    PathAndValues = new Dictionary<string, object>
                    {
                        {HeaderLocalisedStringDataTableInformations.ContactId.ModelName, contactId},
                        {HeaderLocalisedStringDataTableInformations.PartyId.ModelName, null},
                        {HeaderLocalisedStringDataTableInformations.HeaderId.ModelName, null},
                        {HeaderLocalisedStringDataTableInformations.Text.ModelName, contact.Role},
                        {HeaderLocalisedStringDataTableInformations.Type.ModelName, "Role"},
                        {HeaderLocalisedStringDataTableInformations.Language.ModelName, this._language}
                    }
                });
            }

            if (!string.IsNullOrWhiteSpace(contact.Name))
            {
                updateInfoActions.Add(new UpdateInfoAction
                {
                    DataInformationType = DatabaseInformationType.HeaderLocalisedString,
                    SqlStatementType = SqlStatementType.Insert,
                    PathAndValues = new Dictionary<string, object>
                    {
                        {HeaderLocalisedStringDataTableInformations.ContactId.ModelName, contactId},
                        {HeaderLocalisedStringDataTableInformations.PartyId.ModelName, null},
                        {HeaderLocalisedStringDataTableInformations.HeaderId.ModelName, null},
                        {HeaderLocalisedStringDataTableInformations.Text.ModelName, contact.Name},
                        {HeaderLocalisedStringDataTableInformations.Type.ModelName, "Name"},
                        {HeaderLocalisedStringDataTableInformations.Language.ModelName, this._language}
                    }
                });
            }

            if (contact.Email != null && contact.Email.Any())
            {
                foreach (var email in contact.Email)
                {
                    updateInfoActions.Add(new UpdateInfoAction
                    {
                        DataInformationType = DatabaseInformationType.ContactDetails,
                        SqlStatementType = SqlStatementType.Insert,
                        PathAndValues = new Dictionary<string, object>
                        {
                            {ContactDetailsDataTableInfo.ContactId.ModelName, contactId},
                            {ContactDetailsDataTableInfo.Value.ModelName, email},
                            {ContactDetailsDataTableInfo.Type.ModelName, "Email"}
                        }
                    });
                }
            }

            var commandDefinitions = updateInfoActions.SelectMany(item => this._commmandsFromUpdateInfo.Get(string.Empty, item, database));
            database.ExecuteCommandsWithConnectio(commandDefinitions, connection);
        }

        private void InsertHeaderDetails(HeaderEntity headerEntity, Database database, DbConnection connection, string headerId)
        {
            if (!string.IsNullOrWhiteSpace(headerEntity.Source))
            {
                var headerDetailsAction = new UpdateInfoAction
                {
                    DataInformationType = DatabaseInformationType.HeaderLocalisedString,
                    SqlStatementType = SqlStatementType.Insert,
                    PathAndValues = new Dictionary<string, object>
                    {
                        {HeaderLocalisedStringDataTableInformations.ContactId.ModelName, null},
                        {HeaderLocalisedStringDataTableInformations.PartyId.ModelName, null},
                        {HeaderLocalisedStringDataTableInformations.HeaderId.ModelName, headerId},
                        {HeaderLocalisedStringDataTableInformations.Type.ModelName, "Source"},
                        {HeaderLocalisedStringDataTableInformations.Text.ModelName, headerEntity.Source},
                        {HeaderLocalisedStringDataTableInformations.Language.ModelName, this._language}
                    }
                };

                var commandDefinitions = this._commmandsFromUpdateInfo.Get(string.Empty, headerDetailsAction, database).ToList();
                database.ExecuteCommandWithConnectio(commandDefinitions, connection);
            }

            if (!string.IsNullOrWhiteSpace(headerEntity.TransmissionName))
            {
                var headerDetailsAction = new UpdateInfoAction
                {
                    DataInformationType = DatabaseInformationType.HeaderLocalisedString,
                    SqlStatementType = SqlStatementType.Insert,
                    PathAndValues = new Dictionary<string, object>
                    {
                        {HeaderLocalisedStringDataTableInformations.ContactId.ModelName, null},
                        {HeaderLocalisedStringDataTableInformations.PartyId.ModelName, null},
                        {HeaderLocalisedStringDataTableInformations.Type.ModelName, "Name"},
                        {HeaderLocalisedStringDataTableInformations.Text.ModelName, headerEntity.TransmissionName},
                        {HeaderLocalisedStringDataTableInformations.Language.ModelName, this._language},
                        {HeaderLocalisedStringDataTableInformations.HeaderId.ModelName, headerId}
                    }
                };

                var commandDefinitions = this._commmandsFromUpdateInfo.Get(null, headerDetailsAction, database).ToList();
                database.ExecuteCommandWithConnectio(commandDefinitions, connection);
            }
        }

        private void InsertParty(Party party, long headerId, string partyType, Database database, DbConnection connection)
        {
            if (party != null)
            {
                var senderInfoActions = new UpdateInfoAction
                {
                    DataInformationType = DatabaseInformationType.HeaderParty,
                    SqlStatementType = SqlStatementType.Insert,
                    PathAndValues = this.GetPartyPathAndValues(party, headerId.ToString(), partyType)
                };

                var commands = this._commmandsFromUpdateInfo.Get(string.Empty, senderInfoActions, database).ToList();
                var senderId = database.ExecuteCommandWithConnectio(commands, connection);
                if (!string.IsNullOrWhiteSpace(party.Name))
                {
                    var contactInfoActions = new UpdateInfoAction
                    {
                        DataInformationType = DatabaseInformationType.HeaderLocalisedString,
                        SqlStatementType = SqlStatementType.Insert,
                        PathAndValues = new Dictionary<string, object>
                        {
                            {HeaderLocalisedStringDataTableInformations.PartyId.ModelName, senderId},
                            {HeaderLocalisedStringDataTableInformations.HeaderId.ModelName, null},
                            {HeaderLocalisedStringDataTableInformations.ContactId.ModelName, null},
                            {HeaderLocalisedStringDataTableInformations.Text.ModelName, party.Name},
                            {HeaderLocalisedStringDataTableInformations.Type.ModelName, "Name"},
                            {HeaderLocalisedStringDataTableInformations.Language.ModelName, this._language}
                        }
                    };

                    commands = this._commmandsFromUpdateInfo.Get(string.Empty, contactInfoActions, database).ToList();
                    database.ExecuteCommandWithConnectio(commands, connection);
                }
                var contacts = party.Contact;
                if (contacts != null && contacts.Any())
                {
                    foreach (var contact in contacts)
                    {
                        var contactInfoActions = new UpdateInfoAction
                        {
                            DataInformationType = DatabaseInformationType.Contact,
                            SqlStatementType = SqlStatementType.Insert,
                            PathAndValues = new Dictionary<string, object>
                            {
                                {ContactDataTableInfo.PartyId.ModelName, senderId.ToString()}
                            }
                        };

                        commands = this._commmandsFromUpdateInfo.Get(senderId.ToString(), contactInfoActions, database).ToList();
                        var contactId = database.ExecuteCommandWithConnectio(commands, connection);

                        this.InsertContactDetails(contact, contactId, database, connection);
                    }
                }
            }
        }

        private void ReplaceParty(Party party, string entityId, DbConnection connection, Database database, string partyType)
        {
            var sqlstring = $"Select PARTY_ID from Party where Header_id={entityId} and TYPE = '{partyType}'";
            var partyId = connection.ExecuteScalar(sqlstring).ToString();
            var commandDefinition = GetDeleteCommand(partyId, DatabaseInformationType.HeaderParty, database);
            database.ExecuteCommandWithConnectio(new[] {commandDefinition}, connection);
            this.InsertParty(party, Convert.ToInt64(entityId), partyType, database, connection);
        }

        public void DeleteChildren(string entityId, EntityType childrenEntityType)
        {
            throw new NotImplementedException();
        }

        public List<StatusType> Add(IEntityStreamingReader input, IEntityStreamingWriter responseWriter)
        {
            List<StatusType> statuses = new List<StatusType>();
            while (input.MoveToNextEntity())
            {
                var entity = input.CurrentEntity;
                if (entity.Status == StatusType.Success)
                {
                    long entityId = Add((TEntity)entity.Entity);
                    if (responseWriter != null)
                    {
                        entity.Entity.SetEntityId(entityId);
                        responseWriter.Write(entity.Entity);
                    }
                }
                else
                {
                    responseWriter?.WriteStatus(entity.Status, entity.Message);
                }
                statuses.Add(entity.Status);
            }
            return statuses;
        }

        public void Import(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping)
        {
            _entityImporterWithIdMapping.Import<TEntity>(input, responseWriter, mapping, Add, (x, y, z) => { return true; });
        }
    }
}