// -----------------------------------------------------------------------
// <copyright file="SdmxHeaderTemplateRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2018-06-08
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// An alternative implementation that retrieves the 
    /// </summary>
    internal class SdmxHeaderTemplateRetrieverEngine : IEntityRetrieverEngine<HeaderEntity>
    {
        private const string SqlQueryGetDataflowFromHeaderEntityId =
            @"SELECT A.AGENCY as AGENCYID,  A.ID as MAINTAINABLEID, A.VERSION FROM ARTEFACT_VIEW A 
                    INNER JOIN N_ARTEFACT NA ON NA.ART_ID = A.ART_ID
                    INNER JOIN ARTEFACT_BASE AB ON AB.ART_BASE_ID = NA.ART_BASE_ID
                    INNER JOIN ENTITY_SDMX_REF ESR on ESR.TARGET_ARTEFACT = AB.ART_BASE_ID
                    WHERE EXISTS (select 1 from N_HEADER H where H.ENTITY_ID = {0} and H.ENTITY_ID = ESR.SOURCE_ENTITY_ID)";
        private const string SqlQueryGetAllDataflowWithHeader =
            "SELECT H.ENTITY_ID, EB.OBJECT_ID from ENTITY_BASE EB INNER JOIN N_HEADER H ON H.ENTITY_ID = EB.ENTITY_ID";
        private readonly IHeaderRetrieverEngine _headerRetrieverEngine;

        private readonly Database _mappingStoreDb;

        public SdmxHeaderTemplateRetrieverEngine(Database mappingStoreDb)
        {
            if (mappingStoreDb == null)
            {
                throw new ArgumentNullException(nameof(mappingStoreDb));
            }

            _mappingStoreDb = mappingStoreDb;
            _headerRetrieverEngine = new MappingStoreRetrieval.Engine.HeaderRetrieverEngine(_mappingStoreDb);
        }

        public IEnumerable<HeaderEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            StructureReferenceImpl structureReference;
            var entityId = query.EntityId.ValueConvert();
            if (query.ParentId != null && !string.IsNullOrWhiteSpace(query.ParentId.Value))
            {
                if (query.ParentId.Operator != OperatorType.Exact)
                {
                    throw new ResourceNotFoundException();
                }

                structureReference = new StructureReferenceImpl(query.ParentId.Value);
            }
            else if (entityId?.Value != null)
            {
                var sysId = entityId.Value.Value;
                var sqlQuery = string.Format(
                    CultureInfo.InvariantCulture,
                    SqlQueryGetDataflowFromHeaderEntityId,
                    _mappingStoreDb.BuildParameterName(nameof(sysId)));
                var result = _mappingStoreDb.Query<MaintainableRefObjectImpl>(
                    sqlQuery,
                    new
                    {
                        sysId
                    }).FirstOrDefault();
                if (result == null)
                {
                    throw new ResourceNotFoundException();
                }

                structureReference = new StructureReferenceImpl(result, SdmxStructureEnumType.Dataflow);
            }
            else
            {
                var result = _mappingStoreDb.Query<dynamic>(SqlQueryGetAllDataflowWithHeader);
                var headersIds = result.Select(d => d.ENTITY_ID);
                var headers = new List<HeaderEntity>();
                foreach(var headerId in headersIds)
                {
                    var header = GetEntities(new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact,headerId.ToString()) }, Detail.Full).FirstOrDefault();
                    if(header != null)
                    {
                        headers.Add(header);
                    }
                }
                return headers;
            }

            var dataflowMutable = new DataflowMutableCore();
            dataflowMutable.Id = structureReference.MaintainableId;
            dataflowMutable.AgencyId = structureReference.AgencyId;
            dataflowMutable.Version = structureReference.Version;
            dataflowMutable.AddName("en", "Autogenerated from structure reference");
            dataflowMutable.Stub = true;
            dataflowMutable.StructureURL = structureReference.MaintainableUrn;
            var dataflow = dataflowMutable.ImmutableInstance;

            var sdmxHeader = _headerRetrieverEngine.GetHeader(dataflow, null, null);

            if (detail != Detail.Full)
            {
                return new[]
                       {
                           new HeaderEntity()
                           {
                               ParentId = structureReference.MaintainableUrn.ToString(),
                               EntityId =  MappingStoreRetrieval.Engine.HeaderRetrieverEngine.LastHeaderSysId.ToString(CultureInfo.InvariantCulture)
                           }
                       };
            }
            
            return new[]
                   {
                       new HeaderEntity()
                       {
                           ParentId = structureReference.MaintainableUrn.ToString(),
                           SdmxHeader = sdmxHeader,
                           EntityId =  MappingStoreRetrieval.Engine.HeaderRetrieverEngine.LastHeaderSysId.ToString(CultureInfo.InvariantCulture)
                       }
                   };
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }
    }
}