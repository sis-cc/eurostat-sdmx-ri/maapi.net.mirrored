using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.XPath;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class DatasetPersistenceEngine<TEntity> : EntityPersistenceEngine<TEntity> where TEntity : IEntity
    {
        private Database _database;
        private DatasetPropertyPersistenceEngine _datasetPropertyPersistenceEngine;
        private EntitySdmxReferencePersistenceEngine _entitySdmxReferencePersistenceEngine;
        private static readonly string _permissions = "permissions";

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityPersistenceEngine{TEntity}"/> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        /// <param name="updateInfoFactoryManager">The update information factory manager.</param>
        /// <param name="mappingStoreIdentifier"></param>
        /// <param name="entityPropertiesExtractor"></param>
        /// <param name="commmandsFromUpdateInfo"></param>
        public DatasetPersistenceEngine(DatabaseManager databaseManager, UpdateInfoFactoryManager updateInfoFactoryManager, string mappingStoreIdentifier, IEntityPropertiesExtractor entityPropertiesExtractor, ICommmandsFromUpdateInfo commmandsFromUpdateInfo)
            : base(databaseManager, updateInfoFactoryManager, mappingStoreIdentifier, entityPropertiesExtractor, commmandsFromUpdateInfo)
        {
            this._database = databaseManager.GetDatabase(mappingStoreIdentifier);
            this._datasetPropertyPersistenceEngine = new DatasetPropertyPersistenceEngine(databaseManager,
                updateInfoFactoryManager, mappingStoreIdentifier, entityPropertiesExtractor, commmandsFromUpdateInfo);
            this._entitySdmxReferencePersistenceEngine = new EntitySdmxReferencePersistenceEngine(databaseManager, mappingStoreIdentifier);
        }

        public override void Update(PatchRequest patchRequest, EntityType entityType, string entityId)
        {
            base.Update(patchRequest, entityType, entityId);
            // update permissions
            Dictionary<IStructureReference, string> replace = new Dictionary<IStructureReference, string>();
            foreach (var patchDocument in patchRequest)
            {
                if (patchDocument.HasPath(_permissions))
                {
                    if (patchDocument.Op.Equals("replace", StringComparison.OrdinalIgnoreCase))
                    {
                        var permissionObject = JsonConvert.DeserializeObject<JObject>(patchDocument.Value.ToString());
                        foreach (JProperty permission in permissionObject.Children())
                        {
                            var urn = permission.Name;
                            var value = permission.HasValues ? permission.Value.ToString() : null;
                            replace.Add(new StructureReferenceImpl(urn), value);
                        }
                    }
                }
            }

            if (replace.Count > 0)
            {
                using (ContextWithTransaction contextWithTransaction = new ContextWithTransaction(this._database))
                {
                    this._entitySdmxReferencePersistenceEngine.Replace(replace, entityId.AsMappingStoreEntityId(), contextWithTransaction);
                    contextWithTransaction.Complete();
                }
            }
        }

        public override long Add(TEntity entity)
        {
            var datasetId = base.Add(entity);
            var permissionEntity = entity as IPermissionEntity;
            
            if (permissionEntity?.Permissions != null)
            {
                var map = new Dictionary<IStructureReference, string>();
                foreach (var permission in permissionEntity.Permissions)
                {
                    map.Add(new StructureReferenceImpl(permission.Key), permission.Value);
                }
                // TODO contex should apply to the whole add operation
                using (var context = new ContextWithTransaction(this._database))
                {
                    InsertPermissions(map, datasetId,context);
                    context.Complete();
                }
            }

            var datasetEntity = entity as DatasetEntity;
            
            if (datasetEntity?.DatasetPropertyEntity != null)
            {
                datasetEntity.DatasetPropertyEntity.EntityId = datasetId.ToString();
                datasetEntity.DatasetPropertyEntity.StoreId = entity.StoreId;
                _datasetPropertyPersistenceEngine.Add(datasetEntity.DatasetPropertyEntity);
            }

            return datasetId;
        }

        private void InsertPermissions(Dictionary<IStructureReference, string> map, long parentId, ContextWithTransaction context)
        {
            _entitySdmxReferencePersistenceEngine.Insert(map, parentId,context);
        }

        public override void Delete(string entityId, EntityType entityType)
        {
            long mappingStoreEntity = entityId.AsMappingStoreEntityId();

            var entityQuery = new EntityQuery();
            entityQuery.AddAdditionalCriteria(EntityType.DataSet.ToString(),new Criteria<string>(OperatorType.Exact, entityId));
          
            using(var context = new ContextWithTransaction(this._database))
            {
                // MappingSetEntityRetriever with IdOnly query fails with Dapper on Oracle with
                //  System.PlatformNotSupportedException : Operation is not supported on this platform.
                // with Full mariadb fails with deadlock

                var result = Convert.ToInt64(context.DatabaseUnderContext.ExecuteScalarFormat("select count(1) from N_MAPPING_SET where SOURCE_DS = {0}", mappingStoreEntity));
                if (result > 0)
                {
                    throw new SdmxConflictException("DataSet could not been deleted because it is used by a MappingSet");
                }

                DatasetColumnPersistenceEngine.DeleteAsChildren(mappingStoreEntity, context);
                _entitySdmxReferencePersistenceEngine.Delete(mappingStoreEntity, context);
                Delete(mappingStoreEntity, EntityType.DataSet, context.DatabaseUnderContext);
                context.Complete();
            }
        }
    }
}
