// -----------------------------------------------------------------------
// <copyright file="ConnectionEntityRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-05-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.Utils.Helper;

    /// <summary>
    /// The connection entity retriever.
    /// </summary>
    public class ConnectionEntityRetriever : IEntityRetrieverEngine<IConnectionEntity>
    {
        /// <summary>
        /// The DDB entity retriever engine
        /// </summary>
        private readonly IEntityRetrieverEngine<DdbConnectionEntity> _ddbEntityRetrieverEngine;

        /// <summary>
        /// The database provider manager
        /// </summary>
        private readonly IDatabaseProviderManager _databaseProviderManager;

        /// <summary>
        /// The permission retriever
        /// </summary>
        private readonly IPermissionRetriever _permissionRetriever;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionEntityRetriever" /> class.
        /// </summary>
        /// <param name="ddbEntityRetrieverEngine">The DDB entity retriever engine.</param>
        /// <param name="databaseProviderManager">The database provider manager.</param>
        /// <param name="permissionRetriever">The permission retriever.</param>
        public ConnectionEntityRetriever(IEntityRetrieverEngine<DdbConnectionEntity> ddbEntityRetrieverEngine, IDatabaseProviderManager databaseProviderManager, IPermissionRetriever permissionRetriever)
        {
            if (ddbEntityRetrieverEngine is null)
            {
                throw new ArgumentNullException(nameof(ddbEntityRetrieverEngine));
            }

            this._ddbEntityRetrieverEngine = ddbEntityRetrieverEngine;
            this._databaseProviderManager = databaseProviderManager;
            this._permissionRetriever = permissionRetriever;
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<IConnectionEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            var ddbConnectionEntities = this._ddbEntityRetrieverEngine.GetEntities(query, detail);
            List<IConnectionEntity> connectionEntities = new List<IConnectionEntity>();
            foreach (var ddbConnectionEntity in ddbConnectionEntities)
            {
                IDatabaseProviderEngine engine = null;
                try
                {
                    engine = this._databaseProviderManager.GetEngineByType(ddbConnectionEntity.DbType);
                }
                catch (NotImplementedException)
                {
                    //workaround for PCAxis
                    var connectionEntity = new ConnectionEntity();
                    connectionEntity.EntityId = ddbConnectionEntity.EntityId;
                    connectionEntity.Name = ddbConnectionEntity.Name;
                    connectionEntity.Description = ddbConnectionEntity.Description;
                    connectionEntity.StoreId = ddbConnectionEntity.StoreId;
                    connectionEntity.DatabaseVendorType = ddbConnectionEntity.DbType;
                    connectionEntity.AddSetting("type", ParameterType.String, ddbConnectionEntity.DbType, false, true);
                    connectionEntity.AddSetting("userId", ParameterType.String, ddbConnectionEntity.DbUser, false, true);
                    connectionEntity.AddSetting("DataSource", ParameterType.String, ddbConnectionEntity.AdoConnString, false, true);
                    connectionEntities.Add(connectionEntity);
                }

                if (engine != null)
                {
                    var connectionEntity = engine.SettingsBuilder.CreateConnectionEntity(ddbConnectionEntity);

                   connectionEntities.Add(connectionEntity);
                }
            }

            this._permissionRetriever.UpdatePermissions(connectionEntities);
            return connectionEntities;
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }
    }
}