// -----------------------------------------------------------------------
// <copyright file="AdvanceTimeTranscodingPersistEngine.cs" company="EUROSTAT">
//   Date Created : 2018-4-17
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Data.Common;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    using Dapper;

    using Estat.Sri.Mapping.Api.Model.AdvancedTime;
    using Estat.Sri.Mapping.MappingStore.Constant;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Utils.Model;

    /// <summary>
    /// Class AdvanceTimeTranscodingPersistEngine.
    /// </summary>
    public class AdvanceTimeTranscodingPersistEngine
    {
        /// <summary>
        /// The database
        /// </summary>
        private readonly Database _database;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvanceTimeTranscodingPersistEngine"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        public AdvanceTimeTranscodingPersistEngine(Database database)
        {
            _database = database;
        }

        /// <summary>
        /// Deletes the specified transcoding identifier.
        /// </summary>
        /// <param name="mappingSetId">The mapping set entity id.</param>
        /// <returns>System.Int32.</returns>
        public int Delete(long mappingSetId)
        {
            var trId = new Int64Parameter(mappingSetId);
            var count = _database.UsingLogger().Execute(
                "DELETE FROM N_TIME_COLUMN_CONFIG WHERE EXISTS (SELECT 1 FROM N_TIME_FORMAT_CONFIG WHERE STR_MAP_SET_ID = {0} AND (START_CONF = TPC_ID or END_CONF = TPC_ID))",
                trId);
            _database.UsingLogger().Execute(
                "DELETE FROM N_MAPPING_DURATION_VALUES WHERE EXISTS (SELECT 1 FROM N_TIME_FORMAT_CONFIG WHERE STR_MAP_SET_ID = {0} AND (DURATION_MAP = DUR_MAP_ID ))",
                trId);
            var tpcToDelete = _database.QuerySimple<long>(@"SELECT START_CONF FROM N_TIME_FORMAT_CONFIG WHERE START_CONF is not null and STR_MAP_SET_ID = {0}
                                                            UNION ALL SELECT END_CONF FROM N_TIME_FORMAT_CONFIG WHERE END_CONF is not null and STR_MAP_SET_ID = {0}", trId);
            var durToDelete = _database.QuerySimple<long>(@"SELECT distinct DURATION_MAP FROM N_TIME_FORMAT_CONFIG WHERE DURATION_MAP is not null and STR_MAP_SET_ID = {0}", trId);

            count += _database.UsingLogger().Execute("DELETE FROM N_TIME_FORMAT_CONFIG WHERE STR_MAP_SET_ID = {0}", trId);

            count += DbHelper.BulkDelete(_database, "N_TIME_PARTICLE_CONFIG", "TPC_ID", new Stack<long>(tpcToDelete));
            count += DbHelper.BulkDelete(_database, "N_MAPPING_DURATION", "DUR_MAP_ID", new Stack<long>(durToDelete));

            ////var durationMap = _database.QuerySimple<long>(
            ////    "SELECT DURATION_MAP FROM TIME_FORMAT_CONFIG WHERE TR_ID = {0} AND DURATION_MAP IS NOT NULL",
            ////    trId);

            ////foreach (var duration in durationMap)
            ////{
            ////    EntityPersistenceEngine<ComponentMappingEntity>.Delete(
            ////        duration,
            ////        EntityType.Mapping,
            ////        _database);
            ////}

            return count;
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="mappingSetId">The transcoding identifier.</param>
        /// <returns>System.Int32.</returns>
        public int Add(TimeTranscodingAdvancedEntity entity, long mappingSetId)
        {
            // TODO maybe switch to CommandBuilder
            int count = 0;
            foreach (var timeFormatConfiguration in entity.TimeFormatConfigurations)
            {
                SimpleParameter durationMap = AddDurationMap(timeFormatConfiguration.Value.DurationMap);

                var startConf = Add(timeFormatConfiguration.Value.StartConfig);
                var endConf = Add(timeFormatConfiguration.Value.EndConfig);

                count += _database.UsingLogger().Execute(
                    "INSERT INTO N_TIME_FORMAT_CONFIG (STR_MAP_SET_ID, CRITERIA_VALUE, OUTPUT_FORMAT, START_CONF, END_CONF, DURATION_MAP) VALUES ({0}, {1}, {2}, {3}, {4}, {5})",
                    new Int64Parameter(mappingSetId),
                    new SimpleParameter(DbType.String, timeFormatConfiguration.Key),
                    new AnsiString(timeFormatConfiguration.Value.OutputFormat.ReadableCode),
                    startConf,
                    endConf,
                    durationMap);
            }

            return count;
        }

        private Int64Parameter AddDurationMap(DurationMappingEntity entity)
        {
            if (entity != null)
            {
                long primaryKey;
                using (var command = _database.UsingLogger()
                           .CreateCommand(CommandType.StoredProcedure, "INSERT_N_MAPPING_DURATION"))
                {
                    AddParameter(command, "p_constant_or_default", entity.ConstantValue ?? entity.DefaultValue);
                    AddParameter(command, "p_dataset_column_name", entity.Column?.Name);
                    var parameter = AddParameter(command, "p_pk", DbType.Int64, ParameterDirection.Output);
                    command.ExecuteNonQueryAndLog();
                    
                    primaryKey = (long)parameter.Value;
                }

                if (entity.HasTranscoding())
                {
                    foreach (var rule in entity.TranscodingRules)
                    {
                        // TODO use command definition
                        _database.UsingLogger()
                            .Execute(@"INSERT INTO N_MAPPING_DURATION_VALUES (DUR_MAP_ID, LOCAL_VALUE, DURATION_CODE)
                                                   VALUES ({0}, {1}, {2})",
                                new Int64Parameter(primaryKey),
                                new SimpleParameter(DbType.String, rule.Value),
                                new SimpleParameter(DbType.String, rule.Key)
                            );
                    }

                }

                return new Int64Parameter(primaryKey);
            }

            return new Int64Parameter();
        }

        private static void AddParameter(DbCommand command, string name, string value)
        {
            var parameter = AddParameter(command, name, DbType.String, ParameterDirection.Input);
            parameter.Value = (object)value ?? DBNull.Value;
        }

        private static DbParameter AddParameter(DbCommand command, string name, DbType dbType, ParameterDirection direction)
        {
            DbParameter parameter = command.CreateParameter();
            parameter.Direction = direction;
            parameter.DbType = dbType;
            parameter.ParameterName = name;
            command.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        /// Adds the specified particle.
        /// </summary>
        /// <param name="particle">The particle.</param>
        /// <returns>Int64Parameter.</returns>
        private Int64Parameter Add(TimeParticleConfiguration particle)
        {
            if (particle == null)
            {
                return new Int64Parameter();
            }

            long sysId;
            var connection = _database.CreateConnection();
            bool shouldClose = !connection.State.HasFlag(ConnectionState.Open);
            try
            {
                var p = new DynamicParameters();
                p.Add("p_db_format", particle.Format.ToString("G"), DbType.AnsiString);
                p.Add("p_pk", dbType: DbType.Int64, direction: ParameterDirection.Output);
                connection.Execute("INSERT_N_TIME_PARTICLE_CONFIG", p, commandType: CommandType.StoredProcedure);
                sysId = p.Get<long>("p_pk");
            }
            finally
            {
                if (shouldClose)
                {
                    connection.Dispose();
                }
            }

            var tpcId = new Int64Parameter(sysId);
            if (particle.DateColumn != null)
            {
                AddDateColumn(particle, tpcId);
            }
            else if (particle.Year != null)
            {
                AddYearColumn(particle, tpcId);

                // Period is present only when year is present
                if (particle.Period != null)
                {
                    AddPeriodColumn(particle, tpcId);
                }
            }

            return tpcId;
        }

        /// <summary>
        /// Adds the year column.
        /// </summary>
        /// <param name="particle">The particle.</param>
        /// <param name="tpcId">The TPC identifier.</param>
        private void AddYearColumn(TimeParticleConfiguration particle, Int64Parameter tpcId)
        {
            var columnType = new AnsiString(nameof(TimeColumnType.Year).ToUpperInvariant());
            var timeRelatedColumnInfo = particle.Year;
            AddRelatedColumnConfig(tpcId, timeRelatedColumnInfo, columnType);
        }

        /// <summary>
        /// Adds the related column configuration.
        /// </summary>
        /// <param name="tpcId">The TPC identifier.</param>
        /// <param name="timeRelatedColumnInfo">The time related column information.</param>
        /// <param name="columnType">Type of the column.</param>
        private void AddRelatedColumnConfig(
            Int64Parameter tpcId,
            TimeRelatedColumnInfo timeRelatedColumnInfo,
            AnsiString columnType)
        {
            var colId = new SimpleParameter(DbType.String, timeRelatedColumnInfo.Column.Name);
            var startPos = new SimpleParameter(
                DbType.Int32,
                timeRelatedColumnInfo.Start.HasValue ? (object)timeRelatedColumnInfo.Start.Value : DBNull.Value);
            var length = new SimpleParameter(
                DbType.Int32,
                timeRelatedColumnInfo.Length.HasValue ? (object)timeRelatedColumnInfo.Length.Value : DBNull.Value);

            AddTimeColumnConfig(tpcId, columnType, colId, startPos, length);
        }

        /// <summary>
        /// Adds the period column.
        /// </summary>
        /// <param name="particle">The particle.</param>
        /// <param name="tpcId">The TPC identifier.</param>
        private void AddPeriodColumn(TimeParticleConfiguration particle, Int64Parameter tpcId)
        {
            var columnType = new AnsiString(nameof(TimeColumnType.Period).ToUpperInvariant());
            var timeRelatedColumnInfo = particle.Period;
            AddRelatedColumnConfig(tpcId, timeRelatedColumnInfo, columnType);
            if (timeRelatedColumnInfo.Rules.Count > 0)
            {
                foreach (var periodCodeMap in timeRelatedColumnInfo.Rules)
                {
                    _database.UsingLogger().Execute(
                        "INSERT INTO N_PERIOD_RULES (TPC_ID, SDMX_PERIOD_CODE, LOCAL_PERIOD_CODE) VALUES ({0}, {1}, {2} )", tpcId, new AnsiString(periodCodeMap.SdmxPeriodCode), new SimpleParameter(DbType.String, periodCodeMap.LocalPeriodCode));
                }
            }
        }

        /// <summary>
        /// Adds the date column.
        /// </summary>
        /// <param name="particle">The particle.</param>
        /// <param name="tpcId">The TPC identifier.</param>
        private void AddDateColumn(TimeParticleConfiguration particle, Int64Parameter tpcId)
        {
            var columnType = new AnsiString(nameof(TimeColumnType.Date).ToUpperInvariant());
            var colId = new SimpleParameter(DbType.String, particle.DateColumn.Name);
            var startPos = new SimpleParameter(DbType.Int32, null);
            var length = new SimpleParameter(DbType.Int32, null);

            AddTimeColumnConfig(tpcId, columnType, colId, startPos, length);
        }

        /// <summary>
        /// Adds the time column configuration.
        /// </summary>
        /// <param name="tpcId">The TPC identifier.</param>
        /// <param name="columnType">Type of the column.</param>
        /// <param name="colId">The col identifier.</param>
        /// <param name="startPos">The start position.</param>
        /// <param name="length">The length.</param>
        private void AddTimeColumnConfig(
            Int64Parameter tpcId,
            AnsiString columnType,
            SimpleParameter colId,
            SimpleParameter startPos,
            SimpleParameter length)
        {
            _database.UsingLogger().Execute(
                "INSERT INTO N_TIME_COLUMN_CONFIG (TPC_ID, COLUMN_TYPE, DATASET_COLUMN_NAME, START_POS, P_LENGTH) VALUES ({0}, {1}, {2}, {3}, {4})",
                tpcId,
                columnType,
                colId,
                startPos,
                length);
        }
    }
}