// -----------------------------------------------------------------------
// <copyright file="CommonEntityRetriever.cs" company="EUROSTAT">
//   Date Created : 2020-2-3
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    class CommonEntityRetriever
    {
        public static void RetrieveEntities<TEntity>(Action<TEntity> action, Database db, WhereClauseParameters[] normalizedClauses, string sqlQuery)  where TEntity : IEntity
        {
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters)
                .ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);
            using (var connection = db.UsingLogger().CreateConnection())
            {
                foreach (TEntity dataSetColumnEntity in connection.Query<TEntity>(sqlQuery, param, buffered: false))
                {
                    dataSetColumnEntity.StoreId = db.Name;
                    action(dataSetColumnEntity);
                }
            }
        }
    }
}
