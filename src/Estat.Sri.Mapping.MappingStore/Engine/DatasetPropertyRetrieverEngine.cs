// -----------------------------------------------------------------------
// <copyright file="DatasetPropertyEntityRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2021-11-11
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.MappingStore.Helper;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Dapper;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// Engine to retrieve the <see cref="TimePreFormattedEntity"/>.
    /// </summary>
    public class DatasetPropertyEntityRetrieverEngine : IEntityRetrieverEngine<DatasetPropertyEntity>
    {
        private const string DatasetPropertyAlias = "DSP";
        private const string DatasetAlias = "DS";
        private const string EnumerationsAlias = "DSPT";

        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;

        private List<string> Joins => new List<string>()
        {
            $"INNER JOIN N_DATASET {DatasetAlias} ON {DatasetPropertyAlias}.DATASET_ID = {DatasetAlias}.ENTITY_ID",
            $"INNER JOIN DATASET_PROPERTY_TYPE {EnumerationsAlias} ON {DatasetPropertyAlias}.PROPERTY_TYPE_ENUM = {EnumerationsAlias}.PROPERTY_ID"
        };

        private readonly Database _database;
        private readonly string _sid;

        private readonly DatasetPropertyHelper _datasetPropertyHelper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatasetPropertyEntityRetrieverEngine"/> class.
        /// </summary>
        /// <param name="databaseManager">The database to use.</param>
        /// <param name="sid">The store name.</param>
        public DatasetPropertyEntityRetrieverEngine(Database databaseManager, string sid)
        {
            this._database = databaseManager ?? throw new ArgumentNullException(nameof(databaseManager));
            this._sid = sid;

            this._datasetPropertyHelper = new DatasetPropertyHelper(this._database);

            this._fieldsToReturn = new Dictionary<Detail, IList<string>>
            {
                {
                    Detail.IdOnly,
                    new[]
                    {
                        $"{DatasetPropertyAlias}.DATASET_PROPERTY_ID AS {nameof(DatasetPropertyEntity.EntityId)}",
                        $"{DatasetAlias}.ENTITY_ID as DataSetId",
                        $"{DatasetPropertyAlias}.PROPERTY_TYPE_ENUM as PropertyTypeId"
                    }
                }
            };

            this._fieldsToReturn.Add(Detail.IdAndName,
                this._fieldsToReturn[Detail.IdOnly]
                    .Concat(new[] {$"{EnumerationsAlias}.PROPERTY_NAME as PropertyTypeName" }).ToArray());

            this._fieldsToReturn.Add(Detail.Full,
                this._fieldsToReturn[Detail.IdAndName].Concat(new[]
                {
                    $"{DatasetPropertyAlias}.PROPERTY_VALUE as PropertyValue"
                    
                }).ToArray());
        }

        /// <summary>
        /// Get all <see cref="DatasetPropertyEntity"/> entites matching the <paramref name="query"/>.
        /// </summary>
        /// <param name="query">The search criteria.</param>
        /// <param name="detail">The detail level of the return info.</param>
        /// <returns>A collection of <see cref="DatasetPropertyEntity"/>.</returns>
        public IEnumerable<DatasetPropertyEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var datasetPropertyEntities = GetRecordsFromDatabase(query, detail).ToList();

            return datasetPropertyEntities;
        }

        /// <summary>
        /// Write in a stream the <see cref="DatasetPropertyEntity"/> matching the <paramref name="query"/>.
        /// </summary>
        /// <param name="query">The search criteria.</param>
        /// <param name="detail">The detail level of the return info.</param>
        /// <param name="entityWriter">The stream writer to write the <see cref="DatasetPropertyEntity"/> in.</param>
        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            if (entityWriter == null)
            {
                throw new ArgumentNullException(nameof(entityWriter));
            }

            foreach (DatasetPropertyEntity entity in GetRecordsFromDatabase(query, detail))
            {
                entity.StoreId = this._sid;
                entityWriter.Write(entity);
            }
        }

        private IEnumerable<DatasetPropertyEntity> GetRecordsFromDatabase(IEntityQuery query, Detail detail)
        {
            WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder(this._database);
            var clauses = new List<WhereClauseParameters>();

            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "DATASET_ID", DatasetPropertyAlias));

            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;

            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3} {4}", fields,
                "DATASET_PROPERTY " + DatasetPropertyAlias, string.Join(" ", Joins), whereStatement,
                string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters)
                .ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);

            var datasetProperties = this._database.Query<MADatasetPropertyEntity>(sqlQuery, param).ToList();

            if (!datasetProperties.Any())
            {
                return new List<DatasetPropertyEntity>();
            }

            var datasetPropertyEntity = new DatasetPropertyEntity();

            datasetPropertyEntity.EntityId = query.EntityId.Value;

            foreach (var datasetProperty in datasetProperties)
            {
                PopulateDatasetProperty(datasetPropertyEntity, datasetProperty.PropertyTypeId, datasetProperty.PropertyValue);
            }

            return new[] { datasetPropertyEntity };
        }

        private void PopulateDatasetProperty(DatasetPropertyEntity datasetPropertyEntity, long propertyTypeId, string propertyValue)
        {
            var propertyType = this._datasetPropertyHelper.GetDatasetPropertyTypeFromID(propertyTypeId);

            switch (propertyType)
            {
                case DatasetPropertyType.OrderByTimePeriodAsc:
                    datasetPropertyEntity.OrderByTimePeriodAsc = propertyValue;
                    break;
                case DatasetPropertyType.OrderByTimePeriodDesc:
                    datasetPropertyEntity.OrderByTimePeriodDesc = propertyValue;
                    break;
                case DatasetPropertyType.CrossApplyColumn:
                    datasetPropertyEntity.CrossApplyColumn = propertyValue;
                    break;
                case DatasetPropertyType.CrossApplySupported:
                    datasetPropertyEntity.CrossApplySupported =
                        bool.TryParse(propertyValue, out var boolValue) ? boolValue : (bool?)null;
                    break;
                default:
                    throw new ArgumentException($@"DatasetPopertyType not supported: {propertyTypeId}", nameof(propertyTypeId));
            }
        }
    }
}
