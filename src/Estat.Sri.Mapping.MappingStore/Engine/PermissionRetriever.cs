// -----------------------------------------------------------------------
// <copyright file="PermissionRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-05-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Estat.Sri.MappingStore.Store.Extension;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;

    public class PermissionRetriever : IPermissionRetriever
    {
        private readonly IDictionary<long, string> _categoryUrnMap;
        private readonly IDictionary<long, string> _dataflowUrnMap;
        private readonly Database _db;

        public PermissionRetriever(Database db)
        {
            this._db = db;
        }

        /// <summary>
        /// Updates the permissions.
        /// </summary>
        /// <typeparam name="TPermissionEntity">The type of the permission entity.</typeparam>
        /// <param name="entities">The entities.</param>
        public void UpdatePermissions<TPermissionEntity>(List<TPermissionEntity> entities) where TPermissionEntity : class, IPermissionEntity
        {
            if (entities == null || entities.Count == 0)
            {
                return;
            }

            var type = entities.First().TypeOfEntity;
            if (type == EntityType.User)
            {
                foreach (var entity in entities)
                {
                    entity.Permissions = new Dictionary<string, string>(StringComparer.Ordinal);
                    var maintainableRefRetrieverEngine = new MaintainableRefRetrieverEngine(_db, new MappingStoreRetrievalManager(_db));
                    var dataflowUrnMapForUser = maintainableRefRetrieverEngine.RetrievesUrnMapForUser(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow), entity.GetEntityId());

                    foreach (var dMap in dataflowUrnMapForUser)
                    {
                        // dataflows may appear in different paths
                        if (!entity.Permissions.ContainsKey(dMap.Value))
                        {
                            entity.Permissions.Add(dMap.Value, "read_write");
                        }

                    }

                    var categoryUrnMapForUser = maintainableRefRetrieverEngine.RetrievesUrnMapForUser(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category), entity.GetEntityId());
                    foreach (var cMap in categoryUrnMapForUser)
                    {
                        entity.Permissions.Add(cMap.Value, "read_write");
                    }
                }
            }
            else
            {
                var permissions = this.GetPermissions(entities);

                foreach (var entity in entities)
                {
                    entity.Permissions = new Dictionary<string, string>(StringComparer.Ordinal);
                    foreach (var permissionRow in permissions.Where(p => Convert.ToString(p.ENTITYID, CultureInfo.InvariantCulture).Equals(entity.EntityId, StringComparison.Ordinal)))
                    {
                        if(permissionRow.CATEGORYID != null)
                        {
                            var permission = CreatePermissionForCategory(permissionRow);
                            if (!entity.Permissions.ContainsKey(permission.Key))
                            {
                                entity.Permissions.Add(permission.Key, permission.Value);
                            }
                        }
                        else
                        {
                            var permission = CreatePermissionForDataflow(permissionRow);
                            // dataflows may appear in different paths
                            if (!entity.Permissions.ContainsKey(permission.Key))
                            {
                                entity.Permissions.Add(permission.Key, permission.Value);
                            }
                        }
                    }
                }
            }
        }

        private List<dynamic> GetPermissions<TPermissionEntity>(List<TPermissionEntity> entities) where TPermissionEntity : class, IPermissionEntity
        {
            long[] currentParameters = new long[10];
            var values = entities.Select(entity => entity.GetEntityId()).ToArray();
            var arrayParam = this._db.BuildParameterName(nameof(currentParameters));
            var query =
                $@"select distinct erc.SOURCE_ENTITY_ID as ENTITYID,
                         erc.MAJOR as VERSION1,
                         erc.MINOR as VERSION2,
                         erc.PATCH as VERSION3,
                         erc.EXTENSION,
                         erc.WILDCARD_POS,
                         erc.TARGET_CHILD_FULL_ID as CATEGORYID,
                         ab.ID ,
                         ab.AGENCY,
                         ab.ARTEFACT_TYPE
                 FROM
                         ENTITY_SDMX_REF erc
                         inner join ARTEFACT_BASE ab on erc.TARGET_ARTEFACT = ab.ART_BASE_ID
                         where erc.SOURCE_ENTITY_ID IN {arrayParam}";
            
            // this is needed because there is a limit of parameters we can supply
            int offSet = 0;
            List<dynamic> permissions = new List<dynamic>();
            while (offSet < values.Length)
            {
                int length = Math.Min(currentParameters.Length, values.Length - offSet);
                Array.Copy(values, offSet, currentParameters, 0, length);
                offSet += length;
                permissions.AddRange(this._db.Query(query, new { currentParameters }));
            }
            return permissions;
        }

        private KeyValuePair<string, string> CreatePermissionForDataflow(dynamic row)
        {
            SdmxStructureType sdmxStructure = SdmxStructureType.ParseClass(row.ARTEFACT_TYPE);
            var version = string.Join(".", new[] {row.VERSION1, row.VERSION2, row.VERSION3}.Where(l => l != -1).Select(l => l.ToString(CultureInfo.InvariantCulture)));
            var urn = sdmxStructure.GenerateUrn(row.AGENCY, row.ID, version).ToString();
            return new KeyValuePair<string, string>(urn, "read_write");
        }

        private KeyValuePair<string, string> CreatePermissionForCategory(dynamic row)
        {
            var version = string.Join(".", new[] { row.VERSION1, row.VERSION2, row.VERSION3 }.Where(l => l != -1).Select(l => l.ToString(CultureInfo.InvariantCulture)));
            var urn = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category).GenerateUrn(row.AGENCY, row.ID , version, row.CATEGORYID).ToString();
            return new KeyValuePair<string, string>(urn, "read_write");
        }
    }
}
