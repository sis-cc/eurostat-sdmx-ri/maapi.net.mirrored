// -----------------------------------------------------------------------
// <copyright file="ISQLScriptLocationManager.cs" company="EUROSTAT">
//   Date Created : 2017-03-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The SQL Script Provider engine interface.
    /// This interface is responsible for providing SQL Statements
    /// </summary>
    public interface ISqlScriptProviderEngine
    {
        /// <summary>
        /// Gets the upgrade SQL script statements for specified <paramref name="version"/> as they are in the DDL. 
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>The lines are read by the SQL script stream.</returns>
        IEnumerable<string> GetUpgradeSqlStatements(Version version);

        /// <summary>
        /// Gets the upgrade common (<c>ANSI SQL</c>) SQL script statements for specified <paramref name="version"/> as they are in the DDL. 
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>The lines are read by the SQL script stream.</returns>
        IEnumerable<string> GetUpgradeSqlStatementsCommon(Version version);

        /// <summary>
        /// Gets the available versions.
        /// </summary>
        /// <param name="startVersion">The start version (Exclusive).</param>
        /// <param name="endVersion">The end version (Inclusive).</param>
        /// <returns>The available versions for the specified range.</returns>
        IEnumerable<Version> GetAvailableVersions(Version startVersion, Version endVersion);

        /// <summary>
        /// Gets the full initialization SQL script statements
        /// </summary>
        /// <returns>The lines are read by the SQL script stream.</returns>
        IEnumerable<string> GetSqlStatements();
    }
}