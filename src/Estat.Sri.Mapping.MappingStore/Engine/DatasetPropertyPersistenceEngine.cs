// -----------------------------------------------------------------------
// <copyright file="DatasetPropertyPersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2021-11-11
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Globalization;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Exceptions;
using Estat.Sri.Mapping.MappingStore.Helper;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using System.Data;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.Utils.Model;
    using Estat.Sri.Utils;

    /// <summary>
    /// Responsible to persist the <see cref="TimePreFormattedEntity"/>.
    /// </summary>
    public class DatasetPropertyPersistenceEngine : EntityPersistenceEngine<DatasetPropertyEntity>
    {
        private readonly DatasetPropertyHelper _datasetPropertyHelper;

        /// <summary>
        /// Initializes a new instance of the class <see cref="DatasetPropertyPersistenceEngine"/>.
        /// </summary>
        /// <param name="databaseManager"></param>
        /// <param name="updateInfoFactoryManager"></param>
        /// <param name="mappingStoreIdentifier"></param>
        /// <param name="entityPropertiesExtractor"></param>
        /// <param name="commmandsFromUpdateInfo"></param>
        public DatasetPropertyPersistenceEngine(
            DatabaseManager databaseManager,
            UpdateInfoFactoryManager updateInfoFactoryManager,
            string mappingStoreIdentifier,
            IEntityPropertiesExtractor entityPropertiesExtractor,
            ICommmandsFromUpdateInfo commmandsFromUpdateInfo)
            : base(databaseManager, updateInfoFactoryManager, mappingStoreIdentifier, entityPropertiesExtractor,
                commmandsFromUpdateInfo)
        {
            this._datasetPropertyHelper = new DatasetPropertyHelper(this._databaseManager.GetDatabase(mappingStoreIdentifier));
        }

        /// <summary>
        /// Insert or update multiple entities of <see cref="DatasetPropertyEntity"/>.
        /// </summary>
        /// <param name="entities">The collection of entities to insert or update.</param>
        /// <returns>The collection of primary keys for the <see cref="DatasetPropertyEntity"/>.</returns>
        public override IEnumerable<long> Add(IList<DatasetPropertyEntity> entities)
        {
            var result = new List<long>();
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            using (var context = new ContextWithTransaction(database))
            {
                foreach (var datasetProperty in entities)
                {
                    result.Add(PerstistChanges(datasetProperty, context));
                }

                context.Complete();
            }

            return result;
        }

        /// <summary>
        /// Persist a <see cref="DatasetPropertyEntity"/>.
        /// </summary>
        /// <param name="entity">The entity to persist.</param>
        /// <returns>The entity primary key.</returns>
        public override long Add(DatasetPropertyEntity entity)
        {
            return this.Add(new[] {entity}).First();
        }

        public override void Delete(string entityId, EntityType entityType)
        {

            if (entityType != EntityType.DataSetProperty)
            {
                return;
            }

            if (!long.TryParse(entityId, NumberStyles.None, CultureInfo.InvariantCulture, out var entityIdValue))
            {
                throw new BadRequestException("Invalid entity id value");
            }

            var sql = "DELETE DATASET_PROPERTY WHERE DATASET_ID = {0}";

            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);

            database.UsingLogger().ExecuteNonQueryFormat(sql, database.CreateInParameter(nameof(entityIdValue), DbType.Int64, entityIdValue));
        }

        private long PerstistChanges(DatasetPropertyEntity entity, ContextWithTransaction context)
        {
            long datasetId = long.Parse(entity.EntityId); //todo:

            var database = context.DatabaseUnderContext;

            var parameterName = database.BuildParameterName("datasetId");

            var sqlQuery =string.Format(
                "SELECT dsp.DATASET_PROPERTY_ID EntityId, dsp.PROPERTY_TYPE_ENUM PropertyTypeId, dsp.PROPERTY_VALUE PropertyValue FROM DATASET_PROPERTY dsp WHERE dsp.DATASET_ID = {0}",
                parameterName);

            var param = new DynamicParameters();
            param.Add(parameterName, datasetId, DbType.Int64);

            var datasetPropertiesInDb = context.DatabaseUnderContext.Query<MADatasetPropertyEntity>(sqlQuery, param).ToArray();

            PerstistPropertyChange(datasetId, DatasetPropertyType.OrderByTimePeriodAsc, entity.OrderByTimePeriodAsc,
                datasetPropertiesInDb, context);
            PerstistPropertyChange(datasetId, DatasetPropertyType.OrderByTimePeriodDesc, entity.OrderByTimePeriodDesc,
                datasetPropertiesInDb, context);
            PerstistPropertyChange(datasetId, DatasetPropertyType.CrossApplyColumn, entity.CrossApplyColumn,
                datasetPropertiesInDb, context);
            PerstistPropertyChange(datasetId, DatasetPropertyType.CrossApplySupported, entity.CrossApplySupported?.ToString(), datasetPropertiesInDb, context);

            return datasetId;
        }

        private void PerstistPropertyChange(long datasetId, DatasetPropertyType datasetPropertyType, string propertyValue, IEnumerable<MADatasetPropertyEntity> datasetPropertiesInDb, ContextWithTransaction context)
        {
            var propertyTypeId = _datasetPropertyHelper.GetDatasetPropertyIdFromType(datasetPropertyType);

            if (propertyTypeId == null)
            {
                return;
            }

            var propertyInDb =
                datasetPropertiesInDb.FirstOrDefault(dsp => dsp.PropertyTypeId.Equals(propertyTypeId));
            
            if (propertyInDb != null)
            {
                var transactionDb = context.DatabaseUnderContext;

                if (string.IsNullOrEmpty(propertyValue))
                {
                    // Delete existing value stored in database
                    var entityIdParam = new Int64Parameter(propertyInDb.EntityId);
                    transactionDb.UsingLogger().Execute("DELETE FROM DATASET_PROPERTY WHERE DATASET_PROPERTY_ID = {0}",
                        entityIdParam);
                }
                else if (!string.Equals(propertyValue, propertyInDb.PropertyValue))
                {
                    //Update existing value
                    transactionDb.UsingLogger().ExecuteNonQueryFormat(
                        "UPDATE DATASET_PROPERTY SET PROPERTY_TYPE_ENUM = {0}, PROPERTY_VALUE = {1} WHERE DATASET_PROPERTY_ID = {2}",
                        transactionDb.CreateInParameter("propertyEnumId", DbType.Int64, propertyTypeId),
                        transactionDb.CreateInParameter("propertyValue", DbType.String, propertyValue),
                        transactionDb.CreateInParameter("propertyId", DbType.Int64, propertyInDb.EntityId));
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(propertyValue))
                {
                    // No such dataset property in db yet, insert if it has a value
                    var parameters = new DynamicParameters();

                    parameters.Add("p_dataset_id", datasetId, DbType.Int64);
                    parameters.Add("p_property_type_enum", propertyTypeId, DbType.Int64);
                    parameters.Add("p_property_value", propertyValue, DbType.AnsiString);
                    parameters.Add("p_pk", dbType: DbType.Int64, direction: ParameterDirection.Output);
                    context.Connection.Execute("INSERT_DATASET_PROPERTY", parameters,
                        commandType: CommandType.StoredProcedure);

                    var sysId = parameters.Get<long>("p_pk");
                }
            }

        }
    }
}
