// -----------------------------------------------------------------------
// <copyright file="LocalCodeRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-20
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System.Data;
    using System.Data.Common;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    public class LocalCodeRetrieverEngine : IEntityRetrieverEngine<LocalCodeEntity>
    {
        private const string JoinString = "LOCAL_CODE " + LocalCodeAlias + " INNER JOIN ITEM IT ON LC.LCD_ID = IT.ITEM_ID";
        private const string LocalCodeAlias = "LC";

        private readonly Database _database;

        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;

        private readonly IDictionary<string, string> _integerAdditionalProperties;

        public LocalCodeRetrieverEngine(Database databaseManager)
        {
            this._database = databaseManager;
            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }

            this._database = databaseManager;
            this._integerAdditionalProperties = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                {"DatasetId", "DS_ID"}
            };

            this._fieldsToReturn = new Dictionary<Detail, IList<string>>();
            this._fieldsToReturn.Add(Detail.Full, new[] {"LC.LCD_ID as " + nameof(LocalCodeEntity.EntityId), "IT.ID as " + nameof(LocalCodeEntity.ObjectId), "LC.COLUMN_ID " + nameof(LocalCodeEntity.ParentId)});
            this._fieldsToReturn.Add(Detail.IdAndName, new[] {"LC.LCD_ID as " + nameof(LocalCodeEntity.EntityId), "IT.ID as " + nameof(LocalCodeEntity.ObjectId) });
            this._fieldsToReturn.Add(Detail.IdOnly, new[] {"LC.LCD_ID as " + nameof(LocalCodeEntity.EntityId)});
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<LocalCodeEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }
            Dictionary<string, object> valuePairs;
            string sqlQuery = BuildQuery(query, detail, out valuePairs);
            var param = new DynamicParameters(valuePairs);
            return this._database.Query<LocalCodeEntity>(sqlQuery, param);
        }

        private string BuildQuery(IEntityQuery query, Detail detail, out Dictionary<string, object> valuePairs)
        {
            var whereClauseBuilder = new WhereClauseBuilder(this._database);

            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "LCD_ID", LocalCodeAlias));
            clauses.Add(whereClauseBuilder.Build(query.ParentId, "COLUMN_ID", LocalCodeAlias));
            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            return string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3}", fields, JoinString, whereStatement, string.Join(" AND ", whereClauses));
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            Dictionary<string, object> valuePairs;
            string sqlquery = BuildQuery(query, detail, out valuePairs);
            DbParameter[] parameters = valuePairs.Select(x => this._database.CreateInParameter(x.Key, System.Data.DbType.Int64, x.Value)).ToArray();
        
            using(DbCommand cmd = this._database.GetSqlStringCommand(sqlquery, parameters))
            using(IDataReader reader = this._database.ExecuteReader(cmd))
            {
                switch(detail)
                {
                    case Detail.Full:
                        ReadFull(reader, entityWriter);
                        break;
                    case Detail.IdAndName:
                        ReadIdAndName(reader, entityWriter);
                        break;
                    case Detail.IdOnly:
                        ReadIdOnly(reader, entityWriter);
                        break;
                }
                // TODO limit
            }
            entityWriter.Write(GetEntities(query, detail));
        }

        private void ReadIdOnly(IDataReader reader, IEntityStreamingWriter entityWriter)
        {
            while (reader.Read())
            {
                LocalCodeEntity entity = new LocalCodeEntity();
                entity.SetEntityId(reader.GetInt64(0));
                entity.StoreId = this._database.Name;
                entityWriter.Write(entity);
            }
        }

        private void ReadIdAndName(IDataReader reader, IEntityStreamingWriter entityWriter)
        {
            while (reader.Read())
            {
                LocalCodeEntity entity = new LocalCodeEntity();
                entity.SetEntityId(reader.GetInt64(0));
                entity.ObjectId = reader.GetString(1);
                entity.StoreId = this._database.Name;
                entityWriter.Write(entity);
            }
        }

        private void ReadFull(IDataReader reader, IEntityStreamingWriter entityWriter)
        {
            while (reader.Read())
            {
                LocalCodeEntity entity = new LocalCodeEntity();
                entity.SetEntityId(reader.GetInt64(0));
                entity.ObjectId = reader.GetString(1);
                entity.SetParentId(reader.GetInt64(2));
                entity.StoreId = this._database.Name;
                entityWriter.Write(entity);
            }
        }
    }
}