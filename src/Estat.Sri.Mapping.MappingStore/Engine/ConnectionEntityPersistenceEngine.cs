// -----------------------------------------------------------------------
// <copyright file="ConnectionEntityPersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2017-05-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;
    using Estat.Sri.Utils;
    using Estat.Sri.Utils.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    public class ConnectionEntityPersistenceEngine : IEntityPersistenceEngine<IConnectionEntity>
    {
        private readonly IEntityPersistenceEngine<DdbConnectionEntity> _entityPersistenceEngine;

        private readonly IDatabaseProviderManager _databaseProviderManager;
        private readonly Database _database;
        private readonly EntityImporterWithIdMapping _entityImporterWithIdMapping = new EntityImporterWithIdMapping();
        private readonly EntitySdmxReferencePersistenceEngine _entitySdmxReferencePersistenceEngine;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionEntityPersistenceEngine"/> class.
        /// </summary>
        /// <param name="databaseProviderManager">The database provider manager.</param>
        /// <param name="entityPersistenceEngine">The entity persistence engine.</param>
        public ConnectionEntityPersistenceEngine(IDatabaseProviderManager databaseProviderManager, EntityPersistenceEngine<DdbConnectionEntity> entityPersistenceEngine, Database database)
        {
            this._databaseProviderManager = databaseProviderManager;
            this._entityPersistenceEngine = entityPersistenceEngine;
            this._entitySdmxReferencePersistenceEngine = new EntitySdmxReferencePersistenceEngine(database);
            _database = database;
        }

        /// <summary>
        ///     Persists the specified update information.
        /// </summary>
        /// <param name="patchRequest"></param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        public void Update(PatchRequest patchRequest, EntityType entityType, string entityId)
        {
            // TODO FIXME transform a PatchRequest for ConnectionEntity to DdbConnectionEntity
            this._entityPersistenceEngine.Update(patchRequest, EntityType.DdbConnectionSettings, entityId);
        }

        public long Add(IConnectionEntity entity)
        {
            DdbConnectionEntity ddbConnectionEntity = new DdbConnectionEntity();
            if (entity.DatabaseVendorType == "PCAxis")
            {
                ddbConnectionEntity.EntityId = entity.EntityId;
                ddbConnectionEntity.Name = entity.Name;
                ddbConnectionEntity.Description = entity.Description;
                ddbConnectionEntity.StoreId = entity.StoreId;
                ddbConnectionEntity.DbType = entity.DatabaseVendorType;
                ddbConnectionEntity.DbUser = entity.GetValue<string>("userId");
                ddbConnectionEntity.AdoConnString = entity.GetValue<string>("DataSource");
            }
            else
            {
                var databaseProviderEngine = this._databaseProviderManager.GetEngineByType(entity.DatabaseVendorType);
                ddbConnectionEntity = databaseProviderEngine.SettingsBuilder.CreateDdbConnectionEntity(entity);
                
            }
            long entityId = TryToUpdate(ddbConnectionEntity);
            if (entityId < 1)
            {
                entityId = this._entityPersistenceEngine.Add(ddbConnectionEntity);
            }

            if (entityId > 0)
            {
               ReplacePermissions(entity, entityId);
            }

            return entityId;

        }

        private long TryToUpdate(DdbConnectionEntity ddbConnectionEntity)
        {
            if (!string.IsNullOrEmpty(ddbConnectionEntity.EntityId))
            {
                var database = _database;
                var possibleEntityId = ddbConnectionEntity.GetEntityId();

                if (possibleEntityId > 0)
                {
                    int updatedRecords = UpdateDbConnectionTable(ddbConnectionEntity, database, possibleEntityId);
                    updatedRecords += UpdateEntityBaseTable(ddbConnectionEntity, database, possibleEntityId);
                    if (updatedRecords > 0)
                    {
                        return possibleEntityId;
                    }
                }
            }

            return -1;
        }

        private void ReplacePermissions(IConnectionEntity entity, long entityId)
        {
            if (ObjectUtil.ValidMap(entity.Permissions))
            {
                var map = new Dictionary<IStructureReference, string>();
                foreach (var permission in entity.Permissions)
                {
                    map.Add(new StructureReferenceImpl(permission.Key), permission.Value);
                }
                // TODO contex should apply to the whole add operation
                using (var context = new ContextWithTransaction(this._database))
                {
                    _entitySdmxReferencePersistenceEngine.Replace(map, entityId, context);
                    context.Complete();
                }
            }
        }

        private static int UpdateDbConnectionTable(DdbConnectionEntity ddbConnectionEntity, Database database, long entityId)
        {
            // HACK to update DDB connection via POST.
            int formatIdx = 0;
            var fieldsToChange = new List<Tuple<string, SimpleParameter>>();

            var entityParam = GetParameter("ENTITY_ID", entityId, formatIdx++);
            fieldsToChange.Add(entityParam);

            fieldsToChange.Add(GetParameter("DB_TYPE", ddbConnectionEntity.DbType, formatIdx++));

            // Password is special because it should be send only on updates
            if (!string.IsNullOrEmpty(ddbConnectionEntity.Password))
            {
                fieldsToChange.Add(GetParameter("DB_PASSWORD", ddbConnectionEntity.Password, formatIdx++));
            }


            fieldsToChange.Add(GetParameter("DB_USER", ddbConnectionEntity.DbUser, formatIdx++));
            fieldsToChange.Add(
                GetParameter("ADO_CONNECTION_STRING", ddbConnectionEntity.AdoConnString, formatIdx++));
            fieldsToChange.Add(
                GetParameter("JDBC_CONNECTION_STRING", ddbConnectionEntity.JdbcConnString, formatIdx++));
            var setFields = string.Join(",", fieldsToChange.Skip(1).Select(x => x.Item1));
            var updateSql = string.Format(
                CultureInfo.InvariantCulture,
                "UPDATE N_DB_CONNECTION SET {0} WHERE {1}",
                setFields,
                entityParam.Item1);

            var updatedRecords = database.Execute(updateSql, fieldsToChange.Select(x => x.Item2).ToArray());
            return updatedRecords;
        }
        private static int UpdateEntityBaseTable(DdbConnectionEntity ddbConnectionEntity, Database database, long entityId)
        {
            // HACK to update DDB connection via POST.
            int formatIdx = 0;
            var fieldsToChange = new List<Tuple<string, SimpleParameter>>();

            var entityParam = GetParameter("ENTITY_ID", entityId, formatIdx++);
            fieldsToChange.Add(entityParam);

            fieldsToChange.Add(GetParameter("OBJECT_ID", ddbConnectionEntity.Name, formatIdx++));
            fieldsToChange.Add(GetParameter("DESCRIPTION", ddbConnectionEntity.Description, formatIdx++));
            var setFields = string.Join(",", fieldsToChange.Skip(1).Select(x => x.Item1));
            var updateSql = string.Format(
                CultureInfo.InvariantCulture,
                "UPDATE ENTITY_BASE SET {0} WHERE {1}",
                setFields,
                entityParam.Item1);

            var updatedRecords = database.Execute(updateSql, fieldsToChange.Select(x => x.Item2).ToArray());
            return updatedRecords;
        }

        public IEnumerable<long> Add(IList<IConnectionEntity> entities)
        {
            var ddbEntity= new List<DdbConnectionEntity>();
            foreach (var connectionEntity in entities)
            {
                var databaseProviderEngine = this._databaseProviderManager.GetEngineByType(connectionEntity.DatabaseVendorType);
                var ddbConnectionEntity = databaseProviderEngine.SettingsBuilder.CreateDdbConnectionEntity(connectionEntity);
                ddbEntity.Add(ddbConnectionEntity);
            }

            return this._entityPersistenceEngine.Add(ddbEntity);
        }

        public void Delete(string entityId, EntityType entityType)
        {
            this._entityPersistenceEngine.Delete(entityId, entityType);
        }

        private static Tuple<string, SimpleParameter> GetParameter(string fieldName, long value, int index)
        {
            SimpleParameter parameter = new Int64Parameter(value);
            var setValue = string.Format(CultureInfo.InvariantCulture, "{0} = {{{1}}}", fieldName, index);
            return Tuple.Create(setValue, parameter);
        }
        private static Tuple<string, SimpleParameter> GetParameter(string fieldName, string value, int index)
        {
            SimpleParameter parameter = new AnsiString(value);
            var setValue = string.Format(CultureInfo.InvariantCulture, "{0} = {{{1}}}", fieldName, index);
            return Tuple.Create(setValue, parameter);
        }


        public void DeleteChildren(string entityId, EntityType childrenEntityType)
        {
            throw new NotImplementedException();
        }

        public List<StatusType> Add(IEntityStreamingReader input, IEntityStreamingWriter responseWriter)
        {
            List<StatusType> statuses = new List<StatusType>();
            if (input.CurrentEntityType != EntityType.DdbConnectionSettings)
            {
                return statuses;
            }
            if (responseWriter != null)
            {
                responseWriter.StartSection(EntityType.DdbConnectionSettings);
            }

            while(input.MoveToNextEntity())
            {
                // TODO get StoreId from Constructor
                IConnectionEntity connectionEntity = input.CurrentEntity.Entity as IConnectionEntity;
                if (connectionEntity == null)
                {
                    return statuses;
                }
                long entityId = Add(connectionEntity);
                if (responseWriter!=null)
                {
                    connectionEntity.SetEntityId(entityId);                   
                    IConnectionEntity connectionEntityWithSettings = null;                   
                    IDatabaseProviderEngine engine = null;
                    try
                    {
                        engine = this._databaseProviderManager.GetEngineByType(connectionEntity.DatabaseVendorType);
                    }
                    catch (NotImplementedException)
                    {
                    }

                    if (engine != null)
                    {
                       DdbConnectionEntity ddbConnectionEntity = engine.SettingsBuilder.CreateDdbConnectionEntity(connectionEntity);
                       connectionEntityWithSettings = engine.SettingsBuilder.CreateConnectionEntity(ddbConnectionEntity);                       
                    }                    

                    if (connectionEntityWithSettings != null)
                    {
                        responseWriter.Write(connectionEntityWithSettings);
                    }
                    else {
                        responseWriter.Write(connectionEntity);
                    }                   
                }
            }

            return statuses;
        }

        public void Import(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping)
        {
            _entityImporterWithIdMapping.Import<IConnectionEntity>(input, responseWriter, mapping, Add, (x, y, z) => { return true; });
        }
    }
}