using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public interface IUserActionPathAndValues
    {
        Dictionary<string,object> CreateFor(IEntity entity, UserAction userAction);
    }
}