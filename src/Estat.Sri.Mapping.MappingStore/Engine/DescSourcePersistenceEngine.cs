using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class DescSourcePersistenceEngine : EntityPersistenceEngine<ColumnDescriptionSourceEntity>
    {
       
            /// <summary>
            /// The mapping store database
            /// </summary>
            private readonly Database _mappingStoreDatabase;
            /// <summary>
            /// Initializes a new instance of the <see cref="EntityPersistenceEngine{TEntity}"/> class.
            /// </summary>
            /// <param name="databaseManager">The database manager.</param>
            /// <param name="updateInfoFactoryManager">The update information factory manager.</param>
            /// <param name="mappingStoreIdentifier"></param>
            /// <param name="entityPropertiesExtractor"></param>
            /// <param name="commmandsFromUpdateInfo"></param>
            public DescSourcePersistenceEngine(DatabaseManager databaseManager, UpdateInfoFactoryManager updateInfoFactoryManager, string mappingStoreIdentifier, IEntityPropertiesExtractor entityPropertiesExtractor, ICommmandsFromUpdateInfo commmandsFromUpdateInfo)
                : base(databaseManager, updateInfoFactoryManager, mappingStoreIdentifier, entityPropertiesExtractor, commmandsFromUpdateInfo)
            {
                this._mappingStoreDatabase = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            }


            public override void DeleteChildren(string parentId, EntityType childrenEntityType)
            {
                using (var connection = this._databaseManager.GetDatabase(this._mappingStoreIdentifier).CreateConnection())
                {
                    connection.Open();
                    using (var transaction = connection.BeginTransaction())
                    {
                        var ruleParameterName = this._mappingStoreDatabase.BuildParameterName("COL_ID");
                        var parameters = new DynamicParameters();
                        parameters.Add(ruleParameterName, Convert.ToInt64(parentId), DbType.Int64);

                        connection.Execute($"DELETE FROM DESC_SOURCE WHERE COL_ID={ruleParameterName}", parameters, transaction);

                        transaction.Commit();
                    }
                }
            }
        }
}
