﻿using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class ImportExportComparer : IComparer<IImportExportEntity>
    {
        private readonly List<EntityType> _entityTypes = new List<EntityType>() {EntityType.DdbConnectionSettings, EntityType.DataSet, EntityType.MappingSet,};
        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x"/> and <paramref name="y"/>, as shown in the following table.Value Meaning Less than zero<paramref name="x"/> is less than <paramref name="y"/>.Zero<paramref name="x"/> equals <paramref name="y"/>.Greater than zero<paramref name="x"/> is greater than <paramref name="y"/>.
        /// </returns>
        /// <param name="x">The first object to compare.</param><param name="y">The second object to compare.</param>
        public int Compare(IImportExportEntity x, IImportExportEntity y)
        {
            return this._entityTypes.IndexOf(x.EntityType).CompareTo(this._entityTypes.IndexOf(y.EntityType));
        }
    }
}