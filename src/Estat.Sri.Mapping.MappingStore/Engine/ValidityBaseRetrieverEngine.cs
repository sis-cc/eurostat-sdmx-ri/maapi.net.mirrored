// -----------------------------------------------------------------------
// <copyright file="ValiditiyBaseRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2022-09-13
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    /// <summary>
    /// A base class for retrieving entities that use the <c>N_MAPPING_DATA_VALIDITY</c> table
    /// </summary>
    /// <typeparam name="T">The <see cref="ValidityBaseEntity"/> based class</typeparam>
    public abstract class ValidityBaseRetrieverEngine<T> : IEntityRetrieverEngine<T> where T : ValidityBaseEntity
    {
        private const string ComponentMappingAlias = "CM";
        private readonly Database _database;
        private readonly AdvanceTimeTranscodingRetriever _advanceTimeTranscodingRetriever;

        /// <summary>
        /// Initializes a new instance
        /// </summary>
        /// <param name="database">the mapping store database</param>
        /// <exception cref="ArgumentNullException">database is null</exception>
        protected ValidityBaseRetrieverEngine(Database database)
        {
            if (database == null)
            {
                throw new ArgumentNullException(nameof(database));
            }

            _database = database;
        }
/// <inheritdoc/>

        public IEnumerable<T> GetEntities(IEntityQuery query, Detail detail)
        {
            var whereClauseBuilder = new WhereClauseBuilder(_database);
            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "STR_MAP_SET_ID", ComponentMappingAlias));
            clauses.Add(whereClauseBuilder.Build(query.ParentId, "STR_MAP_SET_ID", ComponentMappingAlias));
            clauses.Add(whereClauseBuilder.Build(new Criteria<string>(OperatorType.Exact, GetValidityType()), "VALIDITY_TYPE", ComponentMappingAlias));
            
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
             
            var sqlQuery = string.Format(CultureInfo.InvariantCulture,
                "select STR_MAP_SET_ID, DATASET_COLUMN_NAME, CONSTANT_OR_DEFAULT  from N_MAPPING_DATA_VALIDITY {0} WHERE {1} ",
                ComponentMappingAlias, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);
            var rows = _database.Query(sqlQuery, param);
            return this.ExtractMappings(rows);
             
        }

        /// <summary>
        /// Get the validity type, one of VALID_TO, LAST_UP
        /// </summary>
        /// <returns>One of VALID_TO, LAST_UP</returns>
        protected abstract string GetValidityType();

        /// <summary>
        /// Create a entity, it usually just returns the object created with <c>new</c> e.g. <c>new LastUpdateEntity()</c>
        /// </summary>
        /// <returns>The new object</returns>
        protected abstract T CreateEntity();

        private IEnumerable<T> ExtractMappings(IEnumerable<dynamic> rows)
        {
            foreach (var timeMapping in rows)
            {
                var mappingEntity = CreateEntity();
                mappingEntity.SetEntityId((long)timeMapping.STR_MAP_SET_ID);
                mappingEntity.ParentId = mappingEntity.EntityId;
                if (timeMapping.DATASET_COLUMN_NAME != null)
                {
                    mappingEntity.Column = new DataSetColumnEntity() { Name = timeMapping.DATASET_COLUMN_NAME};
                }
               else
                {
                    mappingEntity.Constant= (DateTime) timeMapping.CONSTANT_OR_DEFAULT;
                }

                yield return mappingEntity;
            }
        }
/// <inheritdoc/>

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }
    }
}