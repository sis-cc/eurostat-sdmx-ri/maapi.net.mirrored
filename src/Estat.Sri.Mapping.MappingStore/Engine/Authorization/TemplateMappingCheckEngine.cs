﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Estat.Sdmxsource.Extension.Constant;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Exceptions;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine.Authorization
{
    public class TemplateMappingCheckEngine : IEntityAuthorizationEngine
    {
        /// <summary>
        /// The _dataflow principal manager.
        /// </summary>
        private readonly IDataflowPrincipalManager _dataflowPrincipalManager;

        /// <summary>
        /// The _retriever manager.
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserAuthCheckEngine"/> class.
        /// </summary>
        /// <param name="dataflowPrincipalManager">The dataflow principal manager.</param>
        /// <param name="retrieverManager">The retriever manager.</param>
        public TemplateMappingCheckEngine(IDataflowPrincipalManager dataflowPrincipalManager, IEntityRetrieverManager retrieverManager)
        {
            this._dataflowPrincipalManager = dataflowPrincipalManager;
            this._retrieverManager = retrieverManager;
        }

        /// <summary>
        /// Determines whether this instance can access the specified entity identifier.
        /// </summary>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity identifier; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccess(string mappingStoreId, string entityId, AccessType accessType)
        {
            var entityRetrieverEngine = this._retrieverManager.GetRetrieverEngine<TemplateMapping>(mappingStoreId);
            var templateMapping =
                entityRetrieverEngine.GetEntities(
                    new EntityQuery { EntityId = new Criteria<string>(OperatorType.Exact, entityId) },
                    Detail.Full).FirstOrDefault();
            if (templateMapping == null)
            {
                throw new ResourceNotFoundException("User doesn't exist");
            }

            return this.CheckDataflowAccess();
        }

        /// <summary>
        /// Determines whether this instance can access the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccess(IEntity entity, AccessType accessType)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (!(entity is TemplateMapping))
            {
                throw new ArgumentException("Entity not a TemplateMapping");
            }

            return this.CheckDataflowAccess();
        }

        /// <summary>
        /// Determines whether this instance can access the specified array of entities.
        /// </summary>
        /// <typeparam name="TPermissionEntity">The type of the t permission entity.</typeparam>
        /// <param name="arrayOfEntities">The array of entities.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>The entities that can be accessed</returns>
        public IEnumerable<TPermissionEntity> CanAccess<TPermissionEntity>(IList<TPermissionEntity> arrayOfEntities, AccessType accessType) where TPermissionEntity : IEntity
        {
            return from permissionEntity in arrayOfEntities where this.CanAccess(permissionEntity, accessType) select permissionEntity;
        }

        /// <summary>
        /// Checks the dataflow access.
        /// </summary>
        /// <returns>
        /// True if we can access DR and
        /// </returns>
        /// <exception cref="MissingInformationException">
        /// Dataflow URN not set
        /// or
        /// Mapping Set parent not a dataflow URN
        /// </exception>
        /// <exception cref="ResourceNotFoundException">Possibly orphan Mapping Set</exception>
        private bool CheckDataflowAccess()
        {
            var principal = this._dataflowPrincipalManager.GetCurrentPrincipal();
            if (principal != null && principal.IsInRole(nameof(PermissionType.CanModifyStoreSettings)))
            {
                // admin can do anything
                return true;
            }

            if (principal == null)
            {
                return false;
            }

            if (principal.IsInRole(nameof(PermissionType.CanModifyStoreSettings)) || principal.IsInRole(nameof(PermissionType.CanPerformInternalMappingConfig)))
            {
                return true;
            }

            return false;
        }
    }
}