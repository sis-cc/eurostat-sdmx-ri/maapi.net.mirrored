// -----------------------------------------------------------------------
// <copyright file="AuthorizationDecoratorRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-06-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Authorization
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Engine.Streaming;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;

    /// <summary>
    /// The authorization decorator retriever engine.
    /// </summary>
    /// <typeparam name="TPermissionEntity">The type of the permission entity.</typeparam>
    /// <seealso cref="Estat.Sri.Mapping.Api.Engine.IEntityRetrieverEngine{TPermissionEntity}" />
    internal class AuthorizationDecoratorRetrieverEngine<TPermissionEntity> : IEntityRetrieverEngine<TPermissionEntity> where TPermissionEntity : IEntity
    {
        /// <summary>
        /// The decorated engine
        /// </summary>
        private readonly IEntityRetrieverEngine<TPermissionEntity> _decoratedEngine;

        /// <summary>
        /// The authorization manager
        /// </summary>
        private readonly IEntityAuthorizationManager _authorizationManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizationDecoratorRetrieverEngine{TPermissionEntity}" /> class.
        /// </summary>
        /// <param name="decoratedEngine">The decorated engine.</param>
        /// <param name="authorizationManager">The authorization manager.</param>
        public AuthorizationDecoratorRetrieverEngine(IEntityRetrieverEngine<TPermissionEntity> decoratedEngine, IEntityAuthorizationManager authorizationManager)
        {
            _decoratedEngine = decoratedEngine;
            _authorizationManager = authorizationManager;
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<TPermissionEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            var entities = this._decoratedEngine.GetEntities(query, detail);
            
            return this._authorizationManager.CanAccess(entities, AccessType.Read);
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            // To workaround existing IEntityRetrieverEngine decorators, we introduce IEntityStreamingWriter
            IEntityStreamingWriter authorizationWrapper = new AuthEntityStreamingWriterDecorator(entityWriter, this._authorizationManager, new SdmxAuthorizationScope(Mapping.Api.Constant.Authorisation.Optional));
            _decoratedEngine.WriteEntities(query, detail, authorizationWrapper);
        }
    }
}