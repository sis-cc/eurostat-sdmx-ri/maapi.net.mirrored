// -----------------------------------------------------------------------
// <copyright file="MappingSetAuthCheckEngine.cs" company="EUROSTAT">
//   Date Created : 2017-06-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Authorization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.Mapping.MappingStore.Extension;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// The mapping set authorization check engine.
    /// </summary>
    public class MappingSetAuthCheckEngine : IEntityAuthorizationEngine
    {
        /// <summary>
        /// The _dataflow principal manager.
        /// </summary>
        private readonly IDataflowPrincipalManager _dataflowPrincipalManager;

        /// <summary>
        /// The _authorization manager.
        /// </summary>
        private readonly IEntityAuthorizationFactory _datasetAuthorizationFactory;

        /// <summary>
        /// The _retriever manager.
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingSetAuthCheckEngine" /> class.
        /// </summary>
        /// <param name="dataflowPrincipalManager">The dataflow principal manager.</param>
        /// <param name="datasetAuthorizationFactory">The dataset authorization factory.</param>
        /// <param name="retrieverManager">The retriever manager.</param>
        public MappingSetAuthCheckEngine(IDataflowPrincipalManager dataflowPrincipalManager, IEntityAuthorizationFactory datasetAuthorizationFactory, IEntityRetrieverManager retrieverManager)
        {
            _dataflowPrincipalManager = dataflowPrincipalManager;
            _datasetAuthorizationFactory = datasetAuthorizationFactory;
            _retrieverManager = retrieverManager;
        }

        /// <summary>
        /// Determines whether this instance can access the specified entity identifier.
        /// </summary>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity identifier; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="ResourceNotFoundException">Mapping Set doesn't exist</exception>
        public bool CanAccess(string mappingStoreId, string entityId, AccessType accessType)
        {
            var principal = this._dataflowPrincipalManager.GetCurrentPrincipal();
            if (principal != null && principal.IsInRole(nameof(PermissionType.CanModifyStoreSettings)))
            {
                // admin can do anything
                return true;
            }

            var id = entityId.AsMappingStoreEntityId();
            var entityRetrieverEngine = this._retrieverManager.GetRetrieverEngine<MappingSetEntity>(mappingStoreId);
            var mappingSetEntity = entityRetrieverEngine.GetEntities(
                new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) },
                Detail.Full).FirstOrDefault();
            if (mappingSetEntity == null)
            {
                throw new ResourceNotFoundException("Mapping Set doesn't exist");
            }

            return this.CheckDataflowAccess(mappingSetEntity, accessType);
        }

        /// <summary>
        /// Determines whether this instance can access the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccess(IEntity entity, AccessType accessType)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var mappingSetEntity = entity as MappingSetEntity;
            if (mappingSetEntity == null)
            {
                throw new ArgumentException("Entity not a MappingSet");
            }
            
            return this.CheckDataflowAccess(mappingSetEntity, accessType);
        }

        /// <summary>
        /// Determines whether this instance can access the specified array of entities.
        /// </summary>
        /// <typeparam name="TPermissionEntity">The type of the t permission entity.</typeparam>
        /// <param name="arrayOfEntities">The array of entities.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>The entities that can be accessed</returns>
        public IEnumerable<TPermissionEntity> CanAccess<TPermissionEntity>(IList<TPermissionEntity> arrayOfEntities, AccessType accessType) where TPermissionEntity : IEntity
        {
            return from permissionEntity in arrayOfEntities where CanAccess(permissionEntity, accessType) select permissionEntity;
        }

        /// <summary>
        /// Checks the dataflow access.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        /// True if we can access DR and
        /// </returns>
        /// <exception cref="MissingInformationException">
        /// Dataflow URN not set
        /// or
        /// Mapping Set parent not a dataflow URN
        /// </exception>
        /// <exception cref="ResourceNotFoundException">Possibly orphan Mapping Set</exception>
        private bool CheckDataflowAccess(MappingSetEntity entity, AccessType accessType)
        {
            if (string.IsNullOrWhiteSpace(entity?.ParentId))
            {
                // either retrieved without full details or it doesn't have a parent yet.
                return true;
////                throw new MissingInformationException("Dataflow URN not set");
            }

            // we either have access or not 
            var principal = this._dataflowPrincipalManager.GetCurrentPrincipal();
            if (principal != null && principal.IsInRole(nameof(PermissionType.CanModifyStoreSettings)))
            {
                // admin can do anything
                return true;
            }

            using (var scope = new SdmxAuthorizationScope(Mapping.Api.Constant.Authorisation.Optional))
            {
                if (scope.IsEnabled)
                {
                    bool isAuthorized = scope.CurrentSdmxAuthorisation.CanRead(new StructureReferenceImpl(entity.ParentId));
                    if (isAuthorized)
                    {
                        if (!string.IsNullOrWhiteSpace(entity.DataSetId) && accessType != AccessType.Delete)
                        {
                            var datasetEngine = this._datasetAuthorizationFactory.GetEngine(EntityType.DataSet);
                            return datasetEngine.CanAccess(
                                entity.StoreId,
                                entity.DataSetId,
                                AccessType.Read);
                        }

                        return true;
                    }

                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}