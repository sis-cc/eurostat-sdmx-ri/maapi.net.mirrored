// -----------------------------------------------------------------------
// <copyright file="AuthorizationDecoratorPersistanceEngine.cs" company="EUROSTAT">
//   Date Created : 2017-06-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Authorization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    internal class AuthorizationDecoratorPersistanceEngine<TEntity> : IEntityPersistenceEngine<TEntity>
        where TEntity : IEntity
    {
        private readonly IEntityPersistenceEngine<TEntity> _entityPersistenceEngine;

        private readonly IEntityAuthorizationManager _authorizationManager;

        private readonly string _mappingStoreId;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizationDecoratorPersistanceEngine{TEntity}" /> class.
        /// </summary>
        /// <param name="entityPersistenceEngine">The entity persistence engine.</param>
        /// <param name="authorizationManager">The authorization manager.</param>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        public AuthorizationDecoratorPersistanceEngine(IEntityPersistenceEngine<TEntity> entityPersistenceEngine, IEntityAuthorizationManager authorizationManager, string mappingStoreId)
        {
            _entityPersistenceEngine = entityPersistenceEngine;
            _authorizationManager = authorizationManager;
            _mappingStoreId = mappingStoreId;
        }

        /// <summary>
        /// Persists the specified update information.
        /// </summary>
        /// <param name="patchRequest"></param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        public void Update(PatchRequest patchRequest, EntityType entityType, string entityId)
        {
            if (this._authorizationManager.CanAccess(this._mappingStoreId, entityId, entityType, AccessType.Update))
            {
                _entityPersistenceEngine.Update(patchRequest, entityType, entityId);
            }
            else
            {
                throw new SdmxUnauthorisedException();
            }
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The added EntityId</returns>
        public long Add(TEntity entity)
        {
            entity.StoreId = this._mappingStoreId;
            if (this._authorizationManager.CanAccess(entity, AccessType.Create))
            {
                return _entityPersistenceEngine.Add(entity);
            }

            throw new SdmxUnauthorisedException();
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entities">The entity.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">entity</exception>
        public IEnumerable<long> Add(IList<TEntity> entities)
        {
            if (entities == null)
            {
                throw new ArgumentNullException(nameof(entities));
            }

            entities.First().StoreId = this._mappingStoreId;
            if (this._authorizationManager.CanAccess(entities.First(), AccessType.Create))
            {
                return _entityPersistenceEngine.Add(entities);
            }

            throw new SdmxUnauthorisedException();
        }

        public void Delete(string entityId, EntityType entityType)
        {
            if (_authorizationManager.CanAccess(this._mappingStoreId, entityId, entityType, AccessType.Delete))
            {
                _entityPersistenceEngine.Delete(entityId, entityType);
                return;
            }

            throw new SdmxUnauthorisedException();
        }

        public void DeleteChildren(string entityId, EntityType childrenEntityType)
        {
            _entityPersistenceEngine.DeleteChildren(entityId, childrenEntityType);
        }

        public List<StatusType> Add(IEntityStreamingReader input, IEntityStreamingWriter responseWriter)
        {
            throw new NotImplementedException();
        }

        public void Import(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping)
        {
            _entityPersistenceEngine.Import(input, responseWriter, mapping);
        }
    }
}