// -----------------------------------------------------------------------
// <copyright file="DataSetColumnAuthCheckEngine.cs" company="EUROSTAT">
//   Date Created : 2017-06-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Authorization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;

    /// <summary>
    /// The data set authorization check engine.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <seealso cref="Estat.Sri.Mapping.Api.Engine.IEntityAuthorizationEngine" />
    internal class ChildEntityAuthCheckEngine<TEntity> : IEntityAuthorizationEngine where TEntity : class, IEntity
    {
             /// <summary>
        /// The _retriever manager.
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;
        private readonly EntityType parentType;

        /// <summary>
        /// The entity authorization engine
        /// </summary>
        private readonly IEntityAuthorizationEngine _entityAuthorizationEngine;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChildEntityAuthCheckEngine{TEntity}" /> class.
        /// </summary>
        /// <param name="retrieverManager">The retriever manager.</param>
        /// <param name="datasetAuthorizationFactory">The DDB connection authorization factory.</param>
        /// <param name="parentType">Type of the parent.</param>
        public ChildEntityAuthCheckEngine(IEntityRetrieverManager retrieverManager, IEntityAuthorizationFactory datasetAuthorizationFactory, EntityType parentType)
        {
            _retrieverManager = retrieverManager;
            this.parentType = parentType;
            _entityAuthorizationEngine = datasetAuthorizationFactory.GetEngine(parentType);
        }

        /// <summary>
        /// Determines whether this instance can access the specified entity identifier.
        /// </summary>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity identifier; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccess(string mappingStoreId, string entityId, AccessType accessType)
        {
            var entityRetrieverEngine = this._retrieverManager.GetRetrieverEngine<TEntity>(mappingStoreId);
            var columnEntity =
                entityRetrieverEngine.GetEntities(
                    new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) },
                    Detail.Full).FirstOrDefault();
            if (columnEntity == null)
            {
                throw new ResourceNotFoundException($"{typeof(TEntity)} doesn't exist");
            }


            bool? cached = AuthorizationCache.CanAccess(parentType, mappingStoreId, columnEntity.ParentId, accessType);
            if (cached.HasValue)
            {
                return cached.Value;
            }

            bool result = _entityAuthorizationEngine.CanAccess(mappingStoreId, columnEntity.ParentId, accessType);
            AuthorizationCache.Add(parentType, mappingStoreId, columnEntity.ParentId, accessType, result);
            return result;
        }

        /// <summary>
        /// Determines whether this instance can access the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccess(IEntity entity, AccessType accessType)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var columnEntity = entity as TEntity;
            if (columnEntity == null)
            {
                throw new ArgumentException($"Entity not a {typeof(TEntity)}");
            }

            var mappingStoreId = columnEntity.StoreId;
            bool? cached = AuthorizationCache.CanAccess(parentType, mappingStoreId, columnEntity.ParentId, accessType);
            if (cached.HasValue)
            {
                return cached.Value;
            }

            var value = _entityAuthorizationEngine.CanAccess(columnEntity.StoreId ?? AuthorizationCache.StoreId, columnEntity.ParentId, accessType);
            AuthorizationCache.Add(parentType, mappingStoreId, columnEntity.ParentId, accessType, value);
            return value;
        }

        /// <summary>
        /// Determines whether this instance can access the specified array of entities.
        /// </summary>
        /// <param name="arrayOfEntities">The array of entities.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>The entities that can be accessed</returns>
        public IEnumerable<TPermissionEntity> CanAccess<TPermissionEntity>(IList<TPermissionEntity> arrayOfEntities, AccessType accessType) where TPermissionEntity : IEntity
        {
            List<TPermissionEntity> authorizedEntities = new List<TPermissionEntity>();
            var groupByStoreId = arrayOfEntities.GroupBy(entity => entity.StoreId);
            foreach (var storeIdEntities in groupByStoreId)
            {
                var groupedByParent = storeIdEntities.GroupBy(entity => entity.ParentId);
                foreach (var permissionEntities in groupedByParent)
                {
                    bool? cached = AuthorizationCache.CanAccess(parentType, storeIdEntities.Key, permissionEntities.Key, accessType);
                    var canAccess = cached ?? _entityAuthorizationEngine.CanAccess(storeIdEntities.Key, permissionEntities.Key, accessType);
                    if (!cached.HasValue)
                    {
                        AuthorizationCache.Add(parentType, storeIdEntities.Key, permissionEntities.Key, accessType, canAccess);
                    }
                    if (canAccess)
                    {
                        authorizedEntities.AddRange(permissionEntities.Select(entity => entity));
                    }
                }
            }

            return authorizedEntities;
        }
    }
}