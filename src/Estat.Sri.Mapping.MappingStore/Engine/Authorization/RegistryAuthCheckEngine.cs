// -----------------------------------------------------------------------
// <copyright file="RegistryAuthCheckEngine.cs" company="EUROSTAT">
//   Date Created : 2017-06-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Authorization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The data set authorization check engine.
    /// </summary>
    public class RegistryAuthCheckEngine : IEntityAuthorizationEngine
    {
        /// <summary>
        /// The _dataflow principal manager.
        /// </summary>
        private readonly IDataflowPrincipalManager _dataflowPrincipalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegistryAuthCheckEngine"/> class.
        /// </summary>
        /// <param name="dataflowPrincipalManager">The dataflow principal manager.</param>
        public RegistryAuthCheckEngine(IDataflowPrincipalManager dataflowPrincipalManager)
        {
            _dataflowPrincipalManager = dataflowPrincipalManager;
        }

        /// <summary>
        /// Determines whether this instance can access the specified entity identifier.
        /// </summary>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity identifier; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccess(string mappingStoreId, string entityId, AccessType accessType)
        {
            return this.CheckDataflowAccess(accessType);
        }

        /// <summary>
        /// Determines whether this instance can access the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccess(IEntity entity, AccessType accessType)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var datasetEntity = entity as RegistryEntity;
            if (datasetEntity == null)
            {
                throw new ArgumentException("Entity not a Registry");
            }

            return this.CheckDataflowAccess(accessType);
        }

        /// <summary>
        /// Determines whether this instance can access the specified array of entities.
        /// </summary>
        /// <typeparam name="TPermissionEntity">The type of the t permission entity.</typeparam>
        /// <param name="arrayOfEntities">The array of entities.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>The entities that can be accessed</returns>
        public IEnumerable<TPermissionEntity> CanAccess<TPermissionEntity>(IList<TPermissionEntity> arrayOfEntities, AccessType accessType) where TPermissionEntity : IEntity
        {
            return from permissionEntity in arrayOfEntities where CheckDataflowAccess(accessType) select permissionEntity;
        }

        /// <summary>
        /// Checks the dataflow access.
        /// </summary>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        /// True if we can access DR and
        /// </returns>
        /// <exception cref="MissingInformationException">
        /// Dataflow URN not set
        /// or
        /// Mapping Set parent not a dataflow URN
        /// </exception>
        /// <exception cref="ResourceNotFoundException">Possibly orphan Mapping Set</exception>
        private bool CheckDataflowAccess(AccessType accessType)
        {
            var principal = this._dataflowPrincipalManager.GetCurrentPrincipal();
            if (principal != null && principal.IsInRole(nameof(PermissionType.CanModifyStoreSettings)))
            {
                // admin can do anything
                return true;
            }

            // Only read access
            if (accessType != AccessType.Read)
            {
                return false;
            }

            return principal != null && principal.IsInRole(nameof(PermissionType.CanImportStructures));
        }
    }
}