// -----------------------------------------------------------------------
// <copyright file="DataSetAuthCheckEngine.cs" company="EUROSTAT">
//   Date Created : 2017-06-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Authorization
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// The data set authorization check engine.
    /// </summary>
    internal class DataSetAuthCheckEngine : IEntityAuthorizationEngine
    {
        /// <summary>
        /// The _dataflow principal manager.
        /// </summary>
        private readonly IDataflowPrincipalManager _dataflowPrincipalManager;

        /// <summary>
        /// The _retriever manager.
        /// </summary>
        private readonly IEntityRetrieverManager _retrieverManager;

        /// <summary>
        /// The DDB connection authorization factory
        /// </summary>
        private readonly IEntityAuthorizationFactory _ddbConnectionAuthorizationFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSetAuthCheckEngine"/> class.
        /// </summary>
        /// <param name="dataflowPrincipalManager">The dataflow principal manager.</param>
        /// <param name="retrieverManager">The retriever manager.</param>
        /// <param name="ddbConnectionAuthorizationFactory">The DDB connection authorization factory.</param>
        public DataSetAuthCheckEngine(IDataflowPrincipalManager dataflowPrincipalManager, IEntityRetrieverManager retrieverManager, IEntityAuthorizationFactory ddbConnectionAuthorizationFactory)
        {
            _dataflowPrincipalManager = dataflowPrincipalManager;
            _retrieverManager = retrieverManager;
            _ddbConnectionAuthorizationFactory = ddbConnectionAuthorizationFactory;
        }

        /// <summary>
        /// Determines whether this instance can access the specified entity identifier.
        /// </summary>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity identifier; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccess(string mappingStoreId, string entityId, AccessType accessType)
        {
            var entityRetrieverEngine = this._retrieverManager.GetRetrieverEngine<DatasetEntity>(mappingStoreId);
            var mappingSetEntity =
                entityRetrieverEngine.GetEntities(
                    new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) },
                    Detail.Full).FirstOrDefault();
            if (mappingSetEntity == null)
            {
                throw new ResourceNotFoundException("DataSet doesn't exist");
            }

            return this.CheckDataflowAccess(mappingSetEntity, accessType);
        }

        /// <summary>
        /// Determines whether this instance can access the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified entity; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccess(IEntity entity, AccessType accessType)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var datasetEntity = entity as DatasetEntity;
            if (datasetEntity == null)
            {
                throw new ArgumentException("Entity not a DataSet");
            }

            return this.CheckDataflowAccess(datasetEntity, accessType);
        }

        /// <summary>
        /// Determines whether this instance can access the specified array of entities.
        /// </summary>
        /// <typeparam name="TPermissionEntity">The type of the t permission entity.</typeparam>
        /// <param name="arrayOfEntities">The array of entities.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>The entities that can be accessed</returns>
        public IEnumerable<TPermissionEntity> CanAccess<TPermissionEntity>(IList<TPermissionEntity> arrayOfEntities, AccessType accessType) where TPermissionEntity : IEntity
        {
            return from permissionEntity in arrayOfEntities where CanAccess(permissionEntity, accessType) select permissionEntity;
        }

        /// <summary>
        /// Checks the dataflow access.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <returns>
        /// True if we can access DR and
        /// </returns>
        /// <exception cref="MissingInformationException">
        /// Dataflow URN not set
        /// or
        /// Mapping Set parent not a dataflow URN
        /// </exception>
        /// <exception cref="ResourceNotFoundException">Possibly orphan Mapping Set</exception>
        private bool CheckDataflowAccess(IPermissionEntity entity, AccessType accessType)
        {
        
            // we either have access or not 
            var principal = this._dataflowPrincipalManager.GetCurrentPrincipal();
            if (principal != null && principal.IsInRole(nameof(PermissionType.CanModifyStoreSettings)))
            {
                // admin can do anything
                return true;
            }


            using (var scope = new SdmxAuthorizationScope(Mapping.Api.Constant.Authorisation.Optional))
            {
                if (scope.IsEnabled)
                {
                    if (accessType == AccessType.Create)
                    {
                        if (string.IsNullOrWhiteSpace(entity?.ParentId))
                        {
                            throw new MissingInformationException("DDB Connection ID not set");
                        }
                        var ddbId = entity.GetParentId();
                        var entityAuthorizationEngine = this._ddbConnectionAuthorizationFactory.GetEngine(EntityType.DdbConnectionSettings);
                        return entityAuthorizationEngine.CanAccess(
                            entity.StoreId,
                            ddbId.ToString(CultureInfo.InvariantCulture),
                            AccessType.Read);
                    }

                    var isAuthorized = false;
                    foreach (var permission in entity.GetPermissions())
                    {
                        isAuthorized |= scope.CurrentSdmxAuthorisation.CanRead(permission);
                    }
                    return isAuthorized;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}