// -----------------------------------------------------------------------
// <copyright file="EntityPersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2017-02-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
using Org.Sdmxsource.Util.Extensions;


namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System.Globalization;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval;

    /// <summary>
    /// EntityPersistenceEngine
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Engine.IEntityPersistenceEngine" />
    public class EntityPersistenceEngine<TEntity> : IEntityPersistenceEngine<TEntity> 
        where TEntity : IEntity
    {
        protected readonly DatabaseManager _databaseManager;
        private readonly UpdateInfoFactoryManager _updateInfoFactoryManager;
        protected readonly string _mappingStoreIdentifier;
        private readonly IEntityPropertiesExtractor _entityPropertiesExtractor;
        protected readonly ICommmandsFromUpdateInfo _commmandsFromUpdateInfo;
        private readonly EntityImporterWithIdMapping _entityImporterWithIdMapping = new EntityImporterWithIdMapping();

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityPersistenceEngine"/> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        /// <param name="updateInfoFactoryManager">The update information factory manager.</param>
        /// <param name="mappingStoreIdentifier"></param>
        /// <param name="entityPropertiesExtractor"></param>
        /// <param name="commmandsFromUpdateInfo"></param>
        public EntityPersistenceEngine(DatabaseManager databaseManager, UpdateInfoFactoryManager updateInfoFactoryManager, string mappingStoreIdentifier, IEntityPropertiesExtractor entityPropertiesExtractor, ICommmandsFromUpdateInfo commmandsFromUpdateInfo)
        {
            this._databaseManager = databaseManager;
            this._updateInfoFactoryManager = updateInfoFactoryManager;
            this._mappingStoreIdentifier = mappingStoreIdentifier;
            this._entityPropertiesExtractor = entityPropertiesExtractor;
            this._commmandsFromUpdateInfo = commmandsFromUpdateInfo;
        }

        /// <summary>
        /// Persists the specified update information.
        /// </summary>
        /// <param name="patchRequest"></param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        public virtual void Update(PatchRequest patchRequest, EntityType entityType, string entityId)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            var updateInfo = this._updateInfoFactoryManager.GetFactory(entityType, database).Create(patchRequest, entityId);
            var commandDefinitions = new List<CommandDefinition>();
            foreach (var updateInfoAction in updateInfo.Actions)
            {
                var commandDefinitionManager = new CommandDefinitionManager(new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
                var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType, updateInfoAction.SqlStatementType);
                commandDefinitions.AddRange(commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, database));
            }

            database.ExecuteCommandsWithReturnId(commandDefinitions);
        }

        public virtual IEnumerable<long> Add(IList<TEntity> entities)
        {
            var ids = new List<long>();
            entities.ToList().ForEach(item => ids.Add(this.Add(item)));
            return ids;
        }

        public virtual long Add(TEntity entity)
        {
            var databaseInformationType = (DatabaseInformationType)Enum.Parse(typeof(DatabaseInformationType), entity.TypeOfEntity.ToString());
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            var pathAndValues = this.GetPathAndValues(entity);
            var updateInfoAction = new UpdateInfoAction()
            {
                DataInformationType = databaseInformationType,
                SqlStatementType = SqlStatementType.Insert,
                PathAndValues = pathAndValues
            };
            var commandDefinitions = this._commmandsFromUpdateInfo.Get(entity.EntityId, updateInfoAction, database);
            return database.ExecuteCommandsWithReturnId(commandDefinitions.ToArray());
        }

        private Dictionary<string, object> GetPathAndValues(TEntity entity)
        {
            var pathAndValues = new Dictionary<string, object>();
            foreach (var property in entity.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly))
            {
                if (property.IsDefined(typeof(OptionalAttribute), true))
                {
                    continue;
                }
                var value = property.GetValue(entity);
                pathAndValues.Add(property.Name, value);
            }

            pathAndValues.AddAll(this._entityPropertiesExtractor.GetFrom(entity));
            return pathAndValues;
        }

        public virtual void Delete(string entityId, EntityType entityType)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            Delete(Convert.ToInt64(entityId, CultureInfo.InvariantCulture), entityType, database);
        }

        public static void Delete(long entityId, EntityType entityType, Database database)
        {
            var databaseInformationType =
                (DatabaseInformationType)Enum.Parse(typeof(DatabaseInformationType), entityType.ToString());
            var databaseMapperManager = new DatabaseMapperManager(databaseInformationType);

            var tableName = databaseMapperManager.GetTableName();
            var primaryKeyColumn = databaseMapperManager.GetPrimaryKeyColumn();
            var identityColumnsAndValues = new Dictionary<string, object> { { primaryKeyColumn, entityId } };
            var commandDefinition = new DeleteSqlBuilder(tableName, identityColumnsAndValues, databaseMapperManager)
                .CreateCommandDefinition(database);

            var commands = new List<CommandDefinition>() { commandDefinition };
            if (entityType == EntityType.DdbConnectionSettings || entityType == EntityType.DataSet || entityType == EntityType.MappingSet)
            {
                var deleteStatement = string.Format(CultureInfo.InvariantCulture,
                    "delete from ENTITY_BASE where ENTITY_ID = {0} ", database.BuildParameterName(nameof(entityId)));
                commands.Add(new CommandDefinition(deleteStatement, new DynamicParameters(new { entityId })));
            }
            database.ExecuteCommandsUnderTransaction(commands.ToArray());
        }

        public virtual void DeleteChildren(string entityId, EntityType childrenEntityType)
        {
            throw new NotImplementedException();
        }

        public List<StatusType> Add(IEntityStreamingReader input, IEntityStreamingWriter responseWriter)
        {
            List<StatusType> statuses = new List<StatusType>();
            while (input.MoveToNextEntity())
            {
                // TODO get StoreId from Constructor
                var entity = input.CurrentEntity;
                if (entity.Status == StatusType.Success)
                {
                    long entityId = Add((TEntity)entity.Entity);
                    if (responseWriter != null)
                    {
                        entity.Entity.SetEntityId(entityId);
                        responseWriter.Write(entity.Entity);
                    }
                }
                else
                {
                    responseWriter?.WriteStatus(entity.Status, entity.Message);
                }
                statuses.Add(entity.Status);
            }
            return statuses;
        }

        public void Import(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping)
        {
            _entityImporterWithIdMapping.Import<TEntity>(input, responseWriter, mapping, Add, SetActualRelationId);
        }

        public virtual bool SetActualRelationId(IEntity entity, EntityIdMapping mapping, IEntityStreamingWriter responseWriter)
        {
            return true;
        }
    }
}