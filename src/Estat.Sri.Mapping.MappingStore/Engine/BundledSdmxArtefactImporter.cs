// -----------------------------------------------------------------------
// <copyright file="BundledSdmxArtefactImporter.cs" company="EUROSTAT">
//   Date Created : 2017-04-27
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStore.Store.Manager;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Manager.Persist;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;

    /// <summary>
    /// This class is responsible for importing infrastructure SDMX artefacts, which are used to internally
    /// </summary>
    public class BundledSdmxArtefactImporter
    {
        /// <summary>
        /// The artefacts parser
        /// </summary>
        private readonly BundledSdmxArtefactsParser _artefactsParser;

        /// <summary>
        /// Initializes a new instance of the <see cref="BundledSdmxArtefactImporter"/> class.
        /// </summary>
        /// <param name="artefactsParser">The artefacts parser.</param>
        public BundledSdmxArtefactImporter(BundledSdmxArtefactsParser artefactsParser)
        {
            _artefactsParser = artefactsParser;
        }

        /// <summary>
        /// Saves the specified mapping store settings.
        /// </summary>
        /// <param name="mappingStoreSettings">The mapping store settings.</param>
        /// <returns>The result of the save action.</returns>
        public IActionResult Save(ConnectionStringSettings mappingStoreSettings)
        {
            Database mappingStoreDb = new Database(mappingStoreSettings);
            // TODO is this in SR ? ICommonSdmxObjectRetrievalManager sdmxObjectRetrievalManager; 
            var retrievalEngineContainer = new RetrievalEngineContainer(mappingStoreDb);
            var artefactImportStatuses = new List<ArtefactImportStatus>();
            IStructurePersistenceManager persistenceManager = new MappingStoreManager(
                mappingStoreSettings,
                artefactImportStatuses);
            var sdmxObjects = this._artefactsParser.GetSdmxObjects("sdmx");
            foreach (var maintainableObject in sdmxObjects.GetAllMaintainables())
            {
                var structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Stub))
                    .SetReferencedDetail(ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.Stub))
                    .SetMaintainableTarget(maintainableObject.StructureType)
                    .SetAgencyIds(maintainableObject.AgencyId)
                    .SetMaintainableIds(maintainableObject.Id)
                    .SetVersionRequests(new VersionRequestCore(maintainableObject.Version))
                    .Build();

                var exists = retrievalEngineContainer.GetEngine(maintainableObject.StructureType).Retrieve(structureQuery).Any();
                if (!exists)
                {
                    persistenceManager.SaveStructure(maintainableObject);
                }
            }

            return artefactImportStatuses.ToActionsResult();
        }
    }
}