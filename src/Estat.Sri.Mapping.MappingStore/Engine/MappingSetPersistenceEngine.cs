// -----------------------------------------------------------------------
// <copyright file="MappingSetPersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2017-02-23
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;

    using Dapper;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Constant;
    using Estat.Sri.Mapping.MappingStore.DatabaseModel;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStore.Store.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Utils;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    public class MappingSetPersistenceEngine : EntityPersistenceEngine<MappingSetEntity>
    {
        private readonly ComponentMappingPersistenceEngine _mappingPersistanceEngine;
        private readonly EntitySdmxReferencePersistenceEngine _entitySdmxReferencePersistenceEngine;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityPersistenceEngine{TEntity}" /> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        /// <param name="updateInfoFactoryManager">The update information factory manager.</param>
        /// <param name="mappingStoreIdentifier">The mapping store identifier.</param>
        /// <param name="entityPropertiesExtractor">The entity properties extractor.</param>
        /// <param name="commmandsFromUpdateInfo">The commmands from update information.</param>
        /// <param name="entityIdFromUrn">The entity identifier from urn.</param>
        public MappingSetPersistenceEngine(DatabaseManager databaseManager, UpdateInfoFactoryManager updateInfoFactoryManager, string mappingStoreIdentifier, IEntityPropertiesExtractor entityPropertiesExtractor, ICommmandsFromUpdateInfo commmandsFromUpdateInfo, IEntityIdFromUrn entityIdFromUrn) : base(databaseManager, updateInfoFactoryManager, mappingStoreIdentifier, entityPropertiesExtractor, commmandsFromUpdateInfo)
        {
            _mappingPersistanceEngine = new ComponentMappingPersistenceEngine(databaseManager, updateInfoFactoryManager, mappingStoreIdentifier, entityPropertiesExtractor, commmandsFromUpdateInfo);
            this._entitySdmxReferencePersistenceEngine = new EntitySdmxReferencePersistenceEngine(databaseManager, mappingStoreIdentifier);
        }

        public override void Delete(string entityId, EntityType entityType)
        {
            var updateInfo = new UpdateInfoAction
            {
                SqlStatementType = SqlStatementType.Update,
                DataInformationType = DatabaseInformationType.Dataflow,
                PathAndValues = new Dictionary<string, object>
                {
                    {DataflowTableInformations.MapSetId.ModelName,null}
                }
            };

            long entityIdValue;
            if (!long.TryParse(entityId, NumberStyles.None, CultureInfo.InvariantCulture, out entityIdValue))
            {
                throw new BadRequestException("Invalid entity id value");
            }

            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            using(var context = new ContextWithTransaction(database))
            {
                TimeMappingPersistEngine timeMappingPersistEngine =
                    new TimeMappingPersistEngine(context.DatabaseUnderContext);
                timeMappingPersistEngine.Delete(entityIdValue, context.DatabaseUnderContext);
                _mappingPersistanceEngine.DeleteChildren(entityIdValue, context);

                // No mapping set without dataflow
                Delete(entityIdValue, entityType, context.DatabaseUnderContext);
                context.Complete();
            }
        }

        public override IEnumerable<long> Add(IList<MappingSetEntity> entities)
        {
            var result = new List<long>();
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            StructureCache structureCache = new StructureCache();
            using(var context = new ContextWithTransaction(database))
            {
                Database transactionDb = context.DatabaseUnderContext;

                foreach (var mappingSet in entities)
                {
                    if (mappingSet.ParentId == null)
                    {
                        throw new MissingInformationException("Mapping Set ParentId not defined");
                    }

                    // get and check if the given dataflow exists
                    var dataflowSysId = structureCache.GetArtefactFinalStatus(transactionDb, new StructureReferenceImpl(mappingSet.ParentId));
                    if (dataflowSysId == null || dataflowSysId.ArtefactBaseKey < 1)
                    {
                        throw new SdmxNoResultsException("Could not find the dataflow with urn:" + mappingSet.ParentId);
                    }

                    long? requestDatasetEntityId = null;
                    if (!string.IsNullOrWhiteSpace(mappingSet.DataSetId))
                    {
                        var datasetEntityId = mappingSet.DataSetId.AsMappingStoreEntityId();
                        var datasetParam = database.CreateInParameter(nameof(datasetEntityId), DbType.Int64, datasetEntityId);
                        requestDatasetEntityId = transactionDb.UsingLogger().ExecuteScalarFormat("select ENTITY_ID from N_DATASET where ENTITY_ID = {0}", datasetParam) as long?;
                        if (!requestDatasetEntityId.HasValue)
                        {
                            throw new SdmxNoResultsException("Could not find the dataset with entity id:" + mappingSet.DataSetId);
                        }
                    }

                    if (mappingSet.ValidFrom.HasValue && mappingSet.ValidTo.HasValue)
                    {
                        if (mappingSet.ValidFrom.Value >= mappingSet.ValidTo.Value)
                        {
                            throw new ValidationException("ValidFrom date cannot be later than ValidTo");
                        }

                        var mappingSetId = context.DatabaseUnderContext.ExecuteScalarFormat(
                            @"SELECT EB.OBJECT_ID
                                     FROM N_MAPPING_SET NMS
                                     JOIN ENTITY_BASE EB on EB.ENTITY_ID = NMS.STR_MAP_SET_ID
                                     JOIN ENTITY_SDMX_REF ESR on EB.ENTITY_ID = ESR.SOURCE_ENTITY_ID 
                                     where NMS.VALID_FROM < {0} and NMS.VALID_TO > {1} and ESR.TARGET_ARTEFACT = {2} and EB.OBJECT_ID != {3}",
                            GetValidityParameter("validToParam", mappingSet.ValidTo, transactionDb),
                            GetValidityParameter("validFromParam", mappingSet.ValidFrom, transactionDb),
                            context.Database.CreateInParameter("dataFlowIdParam", DbType.Int64, dataflowSysId.ArtefactBaseKey), // TARGET is ARTEFACT_BASE 
                            context.Database.CreateInParameter("mappingSetIdParam", DbType.String, mappingSet.Name));

                        if (mappingSetId is string overlappingMappingSetId)
                        {
                            throw new ValidationException("Overlapping mapping set: " + overlappingMappingSetId);
                        }
                    }

                    // check the number of mappingsets that exists 
                    var numOfExistingMappingSets = mappingSet.HasEntityId()
                        ? Convert.ToInt64(transactionDb.UsingLogger().ExecuteScalarFormat("select count(*) from N_MAPPING_SET where STR_MAP_SET_ID = {0}", mappingSet.GetEntityId()))
                        : 0;

                    if (numOfExistingMappingSets > 0)
                    {
                        var mappingSetEntityId = mappingSet.GetEntityId();

                        // we have the case where the entity ID matches
                        // we get the existing dataset
                        var existingDataSet = transactionDb.UsingLogger().ExecuteScalarFormat("select SOURCE_DS from N_MAPPING_SET where STR_MAP_SET_ID={0}", mappingSetEntityId) as long?;

                        // we can update the dataset ID only if there are no mappings
                        if (existingDataSet.HasValue && (!requestDatasetEntityId.HasValue || requestDatasetEntityId.Value != existingDataSet.Value))
                        {
                            // no dataset id given, so we check if there are any component mappings
                            var scalarResult = transactionDb.UsingLogger().ExecuteScalarFormat("select count(*) from N_COMPONENT_MAPPING cm where cm.STR_MAP_SET_ID = {0}", mappingSetEntityId);
                            long numberOfMappings = Convert.ToInt64(scalarResult, CultureInfo.InvariantCulture);
                            if (numberOfMappings > 0)
                            {
                                throw new SdmxConflictException($"Cannot unset/change dataset from mapping set as it has mappings");
                            }

                            if (!requestDatasetEntityId.HasValue)
                            {
                                transactionDb.UsingLogger().ExecuteNonQueryFormat("update N_MAPPING_SET set SOURCE_DS = NULL where STR_MAP_SET_ID={0}", mappingSetEntityId);
                            }
                            else
                            {
                                transactionDb.UsingLogger().ExecuteNonQueryFormat("update N_MAPPING_SET set SOURCE_DS = {0} where STR_MAP_SET_ID={1}", requestDatasetEntityId.Value, mappingSetEntityId);
                            }
                        }
                        else if (!existingDataSet.HasValue && requestDatasetEntityId.HasValue)
                        {
                            transactionDb.UsingLogger().ExecuteNonQueryFormat("update N_MAPPING_SET set SOURCE_DS = {0} where STR_MAP_SET_ID={1}", requestDatasetEntityId.Value, mappingSetEntityId);
                        }
                        if (string.IsNullOrWhiteSpace(mappingSet.Name))
                        {
                            throw new MissingInformationException("Mapping Set Name is mandatory");
                        }

                        // update name, description, valid_from, valid_to
                        transactionDb.UsingLogger().ExecuteNonQueryFormat("update ENTITY_BASE set OBJECT_ID = {0}, DESCRIPTION={1} where ENTITY_ID={2}",
                            transactionDb.CreateInParameter("idParam", DbType.AnsiString, mappingSet.Name),
                            transactionDb.CreateInParameter("descParam", DbType.AnsiString, mappingSet.Description),
                            transactionDb.CreateInParameter("mappingSetEntityId", DbType.Int64, mappingSetEntityId));


                        transactionDb.UsingLogger().ExecuteNonQueryFormat("update N_MAPPING_SET set VALID_FROM={0}, VALID_TO={1} where STR_MAP_SET_ID={2}",
                            GetValidityParameter("validFromParam", mappingSet.ValidFrom, transactionDb),
                            GetValidityParameter("validToParam", mappingSet.ValidTo, transactionDb),
                            transactionDb.CreateInParameter("mappingSetEntityId", DbType.Int64, mappingSetEntityId));

                        result.Add(mappingSetEntityId);
                    }
                    else
                    {
                        // add new mapping set
                        // using old fashion way to avoid:
                        /* Mysql:
                         * Multiple simultaneous connections or connections with different connection strings inside the same transaction are not currently supported
                         * SqlServer: MSDTC on server '4027807761af' is unavailable.
                         * Oracle: Operation is not supported on this platform
                         */
                        var parameters = new DynamicParameters();
                        parameters.Add("p_object_id", mappingSet.Name, dbType: DbType.AnsiString);
                        parameters.Add("p_description", mappingSet.Description, DbType.AnsiString);
                        parameters.Add("p_source_ds", requestDatasetEntityId.Value, dbType: DbType.Int64);
                        parameters.Add("p_valid_from", mappingSet.ValidFrom, dbType: DbType.DateTime);
                        parameters.Add("p_valid_to", mappingSet.ValidTo, dbType: DbType.DateTime);
                        parameters.Add("p_is_metadata", mappingSet.IsMetadata, DbType.Int32);
                        parameters.Add("p_pk", dbType: DbType.Int64, direction: ParameterDirection.Output);
                        context.Connection.Execute("INSERT_N_MAPPING_SET", parameters, commandType: CommandType.StoredProcedure);
                        var mapSetId = parameters.Get<long>("p_pk");

                        result.Add(mapSetId);
                        _entitySdmxReferencePersistenceEngine.Insert(new StructureReferenceImpl(mappingSet.ParentId), mapSetId, context);
                    }

                    // Time Format is added within TimeDimensionMappingEntity
                }
                  
                context.Complete();
            }

            return result;
        }

        public override long Add(MappingSetEntity entity)
        {
           return this.Add(new [] {entity}).First();
        }

        public override bool SetActualRelationId(IEntity entity, EntityIdMapping mapping, IEntityStreamingWriter responseWriter)
        {
            MappingSetEntity mappingSet = (MappingSetEntity)entity;
            String actualDatasetId = mapping.Get(EntityType.DataSet, mappingSet.DataSetId);
            if (String.IsNullOrWhiteSpace(actualDatasetId))
            {
                responseWriter.WriteStatus(StatusType.Error, "could not find entity id for dataset id " + mappingSet.DataSetId);
                return false;
            }
            mappingSet.DataSetId = actualDatasetId;
            return true;
        }

        private static DbParameter GetValidityParameter(string name, DateTime? dateTime, Database transactionDb)
        {
            var validityParam = transactionDb.CreateInParameter(name, DbType.DateTime);

            if (dateTime.HasValue)
            {
                validityParam.Value = dateTime.Value;
            }
            else
            {
                validityParam.Value = DBNull.Value;
            }

            return validityParam;
        }
    }
}