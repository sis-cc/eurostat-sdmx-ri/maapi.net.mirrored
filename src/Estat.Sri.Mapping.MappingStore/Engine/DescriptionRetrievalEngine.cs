// -----------------------------------------------------------------------
// <copyright file="DescriptionRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2014-09-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     The description retrieval engine.
    /// </summary>
    public class DescriptionRetrievalEngine : IDescriptionRetrievalEngine
    {
        /// <summary>
        ///     The _DDB database
        /// </summary>
        private readonly Database _ddbDatabase;

        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptionRetrievalEngine" /> class.
        /// </summary>
        /// <param name="ddbDatabase">The DDB database.</param>
        public DescriptionRetrievalEngine(Database ddbDatabase)
        {
            this._ddbDatabase = ddbDatabase;
        }

        /// <summary>
        ///     Fills the specified <paramref name="localCodelist" /> description.
        /// </summary>
        /// <param name="descriptionSource">The description source.</param>
        /// <param name="localCodelist">The local code list.</param>
        public void FillDescription(ColumnDescriptionSourceEntity descriptionSource, IEnumerable<LocalCodeEntity> localCodelist)
        {
            if (descriptionSource == null)
            {
                return;
            }

            this.FillDescriptionsFromDdb(descriptionSource, localCodelist);
        }

        /// <inherit />
        public Dictionary<string, string> GetMap(string ddbEntityId, ColumnDescriptionSourceEntity descSource)
        {
            Dictionary<string, string> codeDescr = new Dictionary<string, string>(StringComparer.Ordinal);
            string query = BuildSqlQuery(descSource);
            using (var getCodesCommand = this._ddbDatabase.GetSqlStringCommand(query))
            {
                // getCodesCommand.Connection = this.connection;
                getCodesCommand.CommandTimeout = 0;
                using (var reader = this._ddbDatabase.ExecuteReader(getCodesCommand))
                {
                    var resultValues = new object[reader.FieldCount];
                    while (reader.Read())
                    {
                        int len = reader.GetValues(resultValues);

                        object result = resultValues[0];
                        string colval = Convert.ToString(result, CultureInfo.InvariantCulture);
                        if (!string.IsNullOrEmpty(colval))
                        {
                            codeDescr[colval] = BuildDescription(resultValues, len);
                        }
                    }
                }
            }

            return codeDescr;
        }

        /// <summary>
        ///     Fills the descriptions from DDB.
        /// </summary>
        /// <param name="descriptionSource">
        ///     The description source.
        /// </param>
        /// <param name="localCodesList">
        ///     The local codes list.
        /// </param>
        private void FillDescriptionsFromDdb(ColumnDescriptionSourceEntity descriptionSource, IEnumerable<LocalCodeEntity> localCodesList)
        {
            Dictionary<string, LocalCodeEntity> codes = new Dictionary<string, LocalCodeEntity>(StringComparer.Ordinal);
            foreach (var localCode in localCodesList)
            {
                codes[localCode.ObjectId] = localCode;
            }
            string query = BuildSqlQuery(descriptionSource);
            using (var getCodesCommand = this._ddbDatabase.GetSqlStringCommand(query))
            {
                // getCodesCommand.Connection = this.connection;
                getCodesCommand.CommandTimeout = 0;
                using (var reader = this._ddbDatabase.ExecuteReader(getCodesCommand))
                {
                    var resultValues = new object[reader.FieldCount];
                    while (reader.Read())
                    {
                        int len = reader.GetValues(resultValues);

                        object result = resultValues[0];
                        if (!Convert.IsDBNull(result) && result != null)
                        {
                            string colval = Convert.ToString(result);
                            LocalCodeEntity localCode;
                            if (codes.TryGetValue(colval, out localCode))
                            {
                                string coldescr = BuildDescription(resultValues, len);
                                localCode.Name = coldescr;
                            }
                        }
                    }
                }
            }
        }

        private static string BuildDescription(object[] resultValues, int len)
        {
            StringBuilder sb = new StringBuilder();
            // must start at 1 because the first item in the array is code ID
            for(int i=1; i < len; i++)
            {
                var value = Convert.ToString(resultValues[i], CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(value))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(';').Append(' ');
                    }

                    sb.Append(value);
                }

            }

            return sb.ToString();
        }

        private static string BuildSqlQuery(ColumnDescriptionSourceEntity descriptionSource)
        {
            return string.Format(CultureInfo.InvariantCulture, "select distinct {0}, {1} from {2}", descriptionSource.RelatedField, descriptionSource.DescriptionField, descriptionSource.DescriptionTable);
        }
    }
}