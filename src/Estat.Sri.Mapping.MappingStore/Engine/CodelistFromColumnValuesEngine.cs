using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStore.Store.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class CodelistFromColumnValuesEngine : Api.Engine.ICodelistFromColumnValuesEngine
    {
        private readonly IDataSetDataRetrievalManager _dataSetDataRetrievalManager;
        private readonly IEntityRetrieverManager _retrieverManager;
        private readonly IConfigurationStoreManager configurationStoreManager;

        public CodelistFromColumnValuesEngine(IDataSetDataRetrievalManager dataSetDataRetrievalManager, IEntityRetrieverManager retrieverManager, IConfigurationStoreManager configurationStoreManager)
        {
            _dataSetDataRetrievalManager = dataSetDataRetrievalManager;
            _retrieverManager = retrieverManager;
            this.configurationStoreManager = configurationStoreManager;
        }

        public long Create(string sid, string datasetId, string column, ICodelistMutableObject codelist)
        {
            var entityRetrieverEngine = _retrieverManager.GetRetrieverEngine<DatasetEntity>(sid);
            var datasetEntity =
                entityRetrieverEngine.GetEntities(
                    new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, datasetId) },
                    Detail.Full).FirstOrDefault();
            

            var retrieverEngine = this._retrieverManager.GetRetrieverEngine<DataSetColumnEntity>(datasetEntity.StoreId);
            var datasetColumns = retrieverEngine.GetEntities(new EntityQuery() { ParentId = new Criteria<string>(OperatorType.Exact, datasetEntity.EntityId) }, Detail.Full).ToArray();
            var datasetColumn = datasetColumns.FirstOrDefault(entity => entity.Name == column);
            
            var localCodeResult = _dataSetDataRetrievalManager.GetLocalCodesWithDescription(datasetColumn, 0, 0);

            var connectionStringSettings = ConfigurationManager.ConnectionStrings[sid];
            var databaseManager = new DatabaseManager(configurationStoreManager);
            var database = databaseManager.GetConnectionStringSettings(new DatabaseIdentificationOptions { StoreId = sid });

            var artefactStatus = new List<ArtefactImportStatus>();
            var mappingStoreManager = new Estat.Sri.MappingStore.Store.Manager.MappingStoreManager(database, artefactStatus);
            ISdmxObjects objects = new SdmxObjectsImpl();
            foreach (var localCode in localCodeResult)
            {
               var code = codelist.CreateItem(localCode.ObjectId, !string.IsNullOrWhiteSpace(localCode.Description)? localCode.Description : localCode.ObjectId);
            }
            
            objects.AddIdentifiable(codelist.ImmutableInstance);

            mappingStoreManager.SaveStructures(objects);


            return artefactStatus.First().PrimaryKeyValue;
        }
    }
}
