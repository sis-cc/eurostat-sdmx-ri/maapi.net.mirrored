using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public interface IPermissionRetriever
   {
        /// <summary>
        /// Updates the permissions.
        /// </summary>
        /// <typeparam name="TPermissionEntity">The type of the permission entity.</typeparam>
        /// <param name="entities">The entities.</param>
        void UpdatePermissions<TPermissionEntity>(List<TPermissionEntity> entities) where TPermissionEntity : class, IPermissionEntity;
    }
}