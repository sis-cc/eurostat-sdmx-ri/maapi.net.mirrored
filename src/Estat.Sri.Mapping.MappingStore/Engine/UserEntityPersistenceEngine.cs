using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using System.Linq;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Mapping.Api.Extension;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Dapper;
using System.Data;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Estat.Sri.Mapping.Api.Constant;
using System.Globalization;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sdmxsource.Extension.Manager;
using System.Threading;
using Estat.Sri.Utils;
using Estat.Sdmxsource.Extension.Constant;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class UserEntityPersistenceEngine : EntityPersistenceEngine<UserEntity>, IEntityPersistenceForUpdate<UserEntity>
    {
        private readonly IEntityIdFromUrn _entityIdFromUrn;
        private readonly Database database;
        private readonly IAuthorizationManager _structureAuthManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityPersistenceEngine{TEntity}"/> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        /// <param name="updateInfoFactoryManager">The update information factory manager.</param>
        /// <param name="mappingStoreIdentifier"></param>
        /// <param name="entityPropertiesExtractor"></param>
        /// <param name="commmandsFromUpdateInfo"></param>
        /// <param name="entityIdFromUrn"></param>
        public UserEntityPersistenceEngine(DatabaseManager databaseManager, UpdateInfoFactoryManager updateInfoFactoryManager, string mappingStoreIdentifier, IEntityPropertiesExtractor entityPropertiesExtractor, ICommmandsFromUpdateInfo commmandsFromUpdateInfo, IEntityIdFromUrn entityIdFromUrn)
            : base(databaseManager, updateInfoFactoryManager, mappingStoreIdentifier, entityPropertiesExtractor, commmandsFromUpdateInfo)
        {
            this._entityIdFromUrn = entityIdFromUrn;
            database = databaseManager.GetDatabase(mappingStoreIdentifier);
        }

        public override long Add(UserEntity entity)
        {
            if (entity is null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (string.IsNullOrWhiteSpace(entity.UserName))
            {
                throw new ArgumentException("UserName is mandatory", nameof(entity));
            }

            // first delete any existing rules for this user
            Delete(entity.UserName, EntityType.User);

            using (var context = new ContextWithTransaction(database))
            {
                foreach (var permission in entity.Permissions)
                {

                    
                    var reference = permission.Key == "*"? new StructureReferenceImpl() : new StructureReferenceImpl(permission.Key);
                    var parameters = new DynamicParameters();

                    parameters.Add("p_usermask", entity.UserName, DbType.AnsiString);
                    parameters.Add("p_dataspace", entity.Dataspace, DbType.AnsiString);
                    parameters.Add("p_isgroup", entity.IsGroup, DbType.Int16);
                    parameters.Add("p_artefacttype", reference.MaintainableStructureEnumType == null ? null : reference.MaintainableStructureEnumType.EnumType.ToString(), DbType.AnsiString);
                    parameters.Add("p_artefactagencyid", reference.AgencyId, DbType.AnsiString);
                    parameters.Add("p_artefactid", reference.MaintainableId, DbType.AnsiString);
                    parameters.Add("p_artefactversion", reference.Version, DbType.AnsiString);
                    parameters.Add("p_full_path_child",reference.MaintainableStructureEnumType != null && reference.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.CategoryScheme ? reference.FullId : null, DbType.AnsiString);
                    parameters.Add("p_permission", PermissionType.WsUserRole, DbType.Int64);
                    parameters.Add("p_editedby", GetCurrentUser(), DbType.AnsiString);
                    parameters.Add("p_editdate", DateTime.UtcNow, DbType.DateTime);
                    parameters.Add("p_pk", dbType: DbType.Int64, direction: ParameterDirection.Output);
                    context.Connection.Execute("INSERT_AUTHORIZATIONRULES", parameters,
                        commandType: CommandType.StoredProcedure);

                    var sysId = parameters.Get<long>("p_pk");
                }
                context.Complete();
                return 1;
            }
        }

        public override void Delete(string entityId, EntityType entityType)
        {
            database.UsingLogger().ExecuteNonQueryFormat("delete from AUTHORIZATIONRULES where USERMASK = {0} ", database.CreateInParameter("USERMASK", DbType.AnsiString, entityId));
        }

        public void Update(IEntity entity)
        {
            var userEntity = entity as UserEntity;
            this.Delete(userEntity.UserName, EntityType.User);
            this.Add(userEntity).ToString();
        }

        private string GetCurrentUser()
        {
            return Thread.CurrentPrincipal?.Identity.Name ?? "admin";
        }
    }
}