// -----------------------------------------------------------------------
// <copyright file="DbMappingStoreEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-20
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Utils.Helper;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using log4net;

    /// <summary>
    /// The DB mapping store engine.
    /// </summary>
    public class DbMappingStoreEngine : IMappingStoreEngine
    {
        private static readonly string[] _driverIds = { "LocalSqlServer", "OraAspNetConString", "LocalMySqlServer", "authdb" };

        private static readonly object SyncObject = new object();
        private static readonly ILog _log = LogManager.GetLogger(typeof(DbMappingStoreEngine));

        /// <summary>
        /// The configuration store manager
        /// </summary>
        private readonly IConfigurationStoreManager _configurationStoreManager;

        /// <summary>
        /// The database provider manager
        /// </summary>
        private readonly IDatabaseProviderManager _databaseProviderManager;

        /// <summary>
        /// The database manager
        /// </summary>
        private readonly DatabaseManager _databaseManager;

        /// <summary>
        /// The SQL script provider manager
        /// </summary>
        private readonly ISqlScriptProviderManager _sqlScriptProviderManager;

        /// <summary>
        /// The bundled SDMX artefact importer
        /// </summary>
        private readonly BundledSdmxArtefactImporter _bundledSdmxArtefactImporter;

        private readonly Version _defaultVersion = new Version("0.0");

        /// <summary>
        /// Initializes a new instance of the <see cref="DbMappingStoreEngine" /> class.
        /// </summary>
        /// <param name="configurationStoreManager">The configuration store manager.</param>
        /// <param name="databaseProviderManager">The database provider manager.</param>
        /// <param name="sqlScriptProviderManager">The SQL script provider manager.</param>
        /// <param name="bundledSdmxArtefactImporter">The bundled SDMX artefact importer.</param>
        /// <param name="migrationEngine">The migration engine.</param>
        public DbMappingStoreEngine(IConfigurationStoreManager configurationStoreManager, IDatabaseProviderManager databaseProviderManager, ISqlScriptProviderManager sqlScriptProviderManager, BundledSdmxArtefactImporter bundledSdmxArtefactImporter)
        {
            this._configurationStoreManager = configurationStoreManager;
            this._databaseProviderManager = databaseProviderManager;
            this._sqlScriptProviderManager = sqlScriptProviderManager;
            this._bundledSdmxArtefactImporter = bundledSdmxArtefactImporter;
            this._databaseManager = new DatabaseManager(configurationStoreManager);
        }

        /// <summary>
        /// Gets the available store identifier.
        /// </summary>
        /// <value>The available store identifier.</value>
        public IEnumerable<string> AvailableStoreId => this._configurationStoreManager.GetSettings<ConnectionStringSettings>().Select(x => x.Name);


        public IEnumerable<StoreDetails> GetStoresDetails()
        {
            List<StoreDetails> availableStoreDetails = new List<StoreDetails>();
            foreach (var connectionStringSettings in this._configurationStoreManager.GetSettings<ConnectionStringSettings>().Where(s => !_driverIds.Contains(s.Name)).Distinct())
            {
                var engine = this._databaseProviderManager.GetEngineByProvider(connectionStringSettings.ProviderName);

                var connectionEntity = engine.SettingsBuilder.CreateConnectionEntity(connectionStringSettings);
                var storeDetails = new StoreDetails()
                {
                    StoreId = connectionEntity.Name,
                    DbType = connectionEntity.DatabaseVendorType,
                    DbName = connectionEntity.DbName
                };

                availableStoreDetails.Add(storeDetails);
            }

            return availableStoreDetails;
        }
        /// <summary>
        /// Gets the type of the store.
        /// </summary>
        /// <value>The type of the store.</value>
        public string StoreType { get; } = "msdb";

        /// <summary>
        /// Retrieves the version.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <returns>The <see cref="System.Version"/> of the store.</returns>
        public Version RetrieveVersion(DatabaseIdentificationOptions databaseIdentificationOptions)
        {
            var database = this._databaseManager.GetDatabase(databaseIdentificationOptions);

            try
            {
                var enumerable = database?.Query<string>("SELECT VERSION FROM DB_VERSION");
                var versions = enumerable?.Select(s => new Version(s));
                return versions?.FirstOrDefault();
            }
            catch (DbException e)
            {
                _log.Debug("Could not retrieve version", e);
                return null;
            }
        }

        /// <summary>
        /// Gets the available versions that the store with id <paramref name="storeId"/> can be upgraded to. 
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <returns>The list possible <see cref="System.Version"/> that the store can be upgrade.</returns>
        public IEnumerable<Version> GetAvailableVersions(DatabaseIdentificationOptions databaseIdentificationOptions)
        {
            var connectionStringSettings = GetConnectionString(databaseIdentificationOptions);
            if (connectionStringSettings == null)
            {
                return new Version[0];
            }

            var databaseType = GetDatabaseType(connectionStringSettings.ProviderName);
            if (databaseType == null)
            {
                return Enumerable.Empty<Version>();
            }

            var currentVersion = this.RetrieveVersion(databaseIdentificationOptions);
            return this._sqlScriptProviderManager.GetAvailableUpgradeVersion(databaseType, currentVersion ?? this._defaultVersion);
        }

        /// <summary>
        /// Tests the connection.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        public IActionResult TestConnection(DatabaseIdentificationOptions databaseIdentificationOptions)
        {
            var database = this._databaseManager.GetDatabase(databaseIdentificationOptions);
            if (database == null)
            {
                return ActionResults.NoResult;
            }

            try
            {
                using (var connection = database.CreateConnection())
                {
                    connection.Open();
                }

                return ActionResults.Success;
            }
            catch (DbException e)
            {
                _log.Debug("Could connect to store with id" + databaseIdentificationOptions.StoreId, e);
                return new ActionResult("500", StatusType.Error,new List<Exception> {e}, e.Message);
            }
        }

        /// <summary>
        /// Initializes the specified store identifier.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        public IActionResult Initialize(DatabaseIdentificationOptions databaseIdentificationOptions)
        {
            var connectionStringSettings = GetConnectionString(databaseIdentificationOptions);
            if (connectionStringSettings == null)
            {
                return ActionResults.NoResult;
            }

            var databaseType = GetDatabaseType(connectionStringSettings.ProviderName);
            if (databaseType == null)
            {
                return ActionResults.NoResult;
            }

            var fullSchema = this._sqlScriptProviderManager.GetFullSchema(databaseType);

            var executeStatements = this.ExecuteStatements(databaseIdentificationOptions, fullSchema);
            if (executeStatements.Status == StatusType.Success)
            {
                this._bundledSdmxArtefactImporter.Save(connectionStringSettings);
                this.AddDefaultUser(databaseIdentificationOptions);
            }

            return executeStatements;
        }

        /// <summary>
        /// Verifies the integrity.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <param name="action">The action.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        public IActionResult VerifyIntegrity(DatabaseIdentificationOptions databaseIdentificationOptions, VerifyStoreAction action)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Upgrades the specified store identifier.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <param name="targetVersion">The target version.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        public IActionResult Upgrade(DatabaseIdentificationOptions databaseIdentificationOptions, Version targetVersion, bool shoudInitialize)
        {
            var connectionStringSettings = GetConnectionString(databaseIdentificationOptions);
            if (connectionStringSettings == null)
            {
                return ActionResults.NoResult;
            }

            var databaseType = GetDatabaseType(connectionStringSettings.ProviderName);
            if (databaseType == null)
            {
                return ActionResults.NoResult;
            }

            var currentVersion = this.RetrieveVersion(databaseIdentificationOptions);
            var fullSchema = this._sqlScriptProviderManager.GetUpgrade(databaseType, currentVersion, targetVersion);

            var executeStatements = this.ExecuteStatements(databaseIdentificationOptions, fullSchema);
            if (executeStatements.Status == StatusType.Success)
            {
                this._bundledSdmxArtefactImporter.Save(connectionStringSettings);
                this.AddDefaultUser(databaseIdentificationOptions);
            }

            return executeStatements;
        }

        /// <summary>
        /// Retrieves the specified store identifier.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        public IConnectionEntity Retrieve(DatabaseIdentificationOptions databaseIdentificationOptions)
        {
            var connectionStringSettings = GetConnectionString(databaseIdentificationOptions);
            if (connectionStringSettings == null)
            {
                return null;
            }

            var engine = this._databaseProviderManager.GetEngineByProvider(connectionStringSettings.ProviderName);

            return engine.SettingsBuilder.CreateConnectionEntity(connectionStringSettings);
        }

        /// <summary>
        /// Saves the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        public IActionResult Save(IConnectionEntity entity)
        {
            var connectionSettingsBuilder = this._databaseProviderManager.GetEngineByType(entity.DatabaseVendorType).SettingsBuilder;
            var connectionString = connectionSettingsBuilder.CreateConnectionString(entity);
            // TODO check if Configuration.Save is thread safe
            lock (SyncObject)
            {
               return this._configurationStoreManager.SaveSettings(connectionString);
            }
        }

        /// <summary>
        /// Saves the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        public IActionResult TestMappingStoreConnection(IConnectionEntity entity)
        {
            var connectionSettingsBuilder = this._databaseProviderManager.GetEngineByType(entity.DatabaseVendorType).SettingsBuilder;
            var connectionString = connectionSettingsBuilder.CreateConnectionString(entity);
            var engine = _databaseProviderManager.GetEngineByType(entity.DatabaseVendorType);
            try
            {
                using (var connection = engine.Browser.CreateConnection(connectionString))
                {
                    connection.Open();
                    return ActionResults.Success;
                }
            }
            catch (DbException e)
            {
                _log.Debug("Could connect to store with id" + entity.Name, e);
                return new ActionResult("500", StatusType.Error, new List<Exception> { e }, e.Message);
            }
        }
        /// <summary>
        /// Deletes the specified store identifier.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        public IActionResult Delete(DatabaseIdentificationOptions databaseIdentificationOptions)
        {
            // TODO check if Configuration.Save is thread safe
            lock (SyncObject)
            {
                var saveSettings = this._configurationStoreManager.DeleteSettings<ConnectionStringSettings>(databaseIdentificationOptions.StoreId);

                return saveSettings;
            }
        }

        /// <summary>
        /// Gets the type of the database.
        /// </summary>
        /// <param name="providerName"></param>
        /// <returns>The database type</returns>
        /// <exception cref="System.NotImplementedException">Support for " + connectionStringSettings.ProviderName</exception>
        private static string GetDatabaseType(string providerName)
        {
            var mappings = DatabaseType.Mappings;
            foreach (MastoreProviderMappingSetting mapping in mappings)
            {
                if (mapping.Provider.Equals(providerName ?? MappingStoreDefaultConstants.SqlServerProvider))
                {
                    return mapping.Name;
                }
            }

            return null;
        }

        /// <summary>
        /// Get the connection string for the specified <paramref name="databaseIdentificationOptions"/>
        /// </summary>
        /// <param name="databaseIdentificationOptions">The mapping store connection name</param>
        /// <returns>The <see cref="ConnectionStringSettings"/> with connection name equal to <paramref name="storeId"/>.</returns>
        private ConnectionStringSettings GetConnectionString(DatabaseIdentificationOptions databaseIdentificationOptions)
        {
            return this._databaseManager.GetConnectionStringSettings(databaseIdentificationOptions);
        }

        /// <summary>
        /// Executes the statements.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The store identifier.</param>
        /// <param name="fullSchema">The full schema.</param>
        /// <returns>The <see cref="IActionResult"/> .</returns>
        private IActionResult ExecuteStatements(DatabaseIdentificationOptions databaseIdentificationOptions, IEnumerable<string> fullSchema)
        {
            var db = this._databaseManager.GetDatabase(databaseIdentificationOptions);
            string lastStatement = string.Empty;
            try
            {
                using (var connection = db.CreateConnection())
                {
                    connection.Open();
                    using (var transaction = connection.BeginTransaction())
                    {
                        Database transactionalDatabase = new Database(db, transaction);
                        foreach (var statement in fullSchema)
                        {
                            lastStatement = statement;
                            transactionalDatabase.UsingLogger().ExecuteNonQuery(statement, new DbParameter[0]);
                        }

                        transaction.Commit();
                    }
                }

                return ActionResults.Success;
            }
            catch (DbException e)
            {
                return new ActionResult("500", StatusType.Error, new List<Exception> {e}, lastStatement, e.Message);
            }
        }

        /// <summary>
        /// Adds the default user.
        /// </summary>
        /// <param name="databaseIdentificationOptions">The database identification options.</param>
        private void AddDefaultUser(DatabaseIdentificationOptions databaseIdentificationOptions)
        {
            var db = this._databaseManager.GetDatabase(databaseIdentificationOptions);
            using (var connection = db.CreateConnection())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    Database transactionalDatabase = new Database(db, transaction);
                    var numberOfUsersObj = transactionalDatabase.UsingLogger().ExecuteScalarFormat("select count(*) from MA_USER", new DbParameter[0]);
                    if (numberOfUsersObj != null && !Convert.IsDBNull(numberOfUsersObj))
                    {
                        var value = Convert.ToInt64(numberOfUsersObj, CultureInfo.InvariantCulture);
                        if (value > 0)
                        {
                            return;
                        }
                    }
                    
                    transactionalDatabase.UsingLogger().ExecuteNonQueryFormat(
                        "INSERT INTO AUTHORIZATIONRULES (USERMASK, PERMISSION, EDITEDBY) VALUES ({0}, {1}, {2})",
                        transactionalDatabase.CreateInParameter("p1", DbType.AnsiString, "admin"),
                        transactionalDatabase.CreateInParameter("p2", DbType.Int16, (int)PermissionType.AdminRole),
                        transactionalDatabase.CreateInParameter("p3", DbType.AnsiString, "admin"));
                    transaction.Commit();
                }
            }
        }
    }
}
