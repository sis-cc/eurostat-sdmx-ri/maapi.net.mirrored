// -----------------------------------------------------------------------
// <copyright file="SdmxStoreCloneHelper.cs" company="EUROSTAT">
//   Date Created : 2017-07-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Cloning
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sri.MappingStore.Store.Engine;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// The SDMX store manager.
    /// </summary>
    internal class SdmxStoreCloneHelper
    {
        /// <summary>
        /// The object retrieval manager
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _objectRetrievalManager;

        /// <summary>
        /// The structure parsing manager
        /// </summary>
        private readonly IStructureParsingManager _structureParsingManager;

        /// <summary>
        /// The cache
        /// </summary>
        private readonly StructureCache _cache;

        /// <summary>
        /// The structure map path builder
        /// </summary>
        private readonly StructureMapPathBuilder _structureMapPathBuilder = new StructureMapPathBuilder();

        /// <summary>
        /// The structure set builder
        /// </summary>
        private readonly StructureSetBuilder _structureSetBuilder = new StructureSetBuilder();

        /// <summary>
        /// The merge builder
        /// </summary>
        private readonly StructureSetMergeBuilder _mergeBuilder = new StructureSetMergeBuilder();

        /// <summary>
        /// The database
        /// </summary>
        private readonly Database _database;

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxStoreCloneHelper" /> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="factory"></param>
        public SdmxStoreCloneHelper(Database database, ICommonSdmxObjectRetrievalFactory factory)
        {
            _structureParsingManager = new StructureParsingManager();
            _cache = new StructureCache();
            _database = database;

            
            _objectRetrievalManager = new SdmxObjectRetrievalManagerWrapper(factory,database.ConnectionStringSettings);
        }

        /// <summary>
        /// Gets the dataflow.
        /// </summary>
        /// <param name="dataflowUrn">
        /// The dataflow urn.
        /// </param>
        /// <returns>
        /// The <see cref="IDataflowObject"/>.
        /// </returns>
        public IDataflowObject GetDataflow(Uri dataflowUrn)
        {
            IStructureReference dataflowReference = new StructureReferenceImpl(dataflowUrn);
            return GetDataflow(dataflowReference);
        }

        /// <summary>
        /// Gets the dataflow.
        /// </summary>
        /// <param name="dataflowReference">The dataflow reference.</param>
        /// <returns>
        /// The <see cref="IDataflowObject"/>.
        /// </returns>
        public IDataflowObject GetDataflow(IMaintainableRefObject dataflowReference)
        {
            return this._objectRetrievalManager.GetMaintainableObject<IDataflowObject>(dataflowReference);
        }

        /// <summary>
        /// Gets the DSD.
        /// </summary>
        /// <param name="dataflow">The dataflow.</param>
        /// <returns>
        /// The <see cref="IDataStructureObject" />.
        /// </returns>
        public IDataStructureObject GetDsd(IDataflowObject dataflow)
        {
            return this._objectRetrievalManager.GetMaintainableObject<IDataStructureObject>(dataflow.DataStructureRef);
        }

        /// <summary>
        /// Gets the merged structure set.
        /// </summary>
        /// <param name="structureSetFiles">
        /// The structure set files.
        /// </param>
        /// <param name="sourceDataflow">
        /// The source dataflow.
        /// </param>
        /// <param name="targetDataflow">
        /// The target dataflow.
        /// </param>
        /// <returns>
        /// The <see cref="IStructureSetObject"/>.
        /// </returns>
        public IStructureSetObject GetMergedStructureSet(IEnumerable<FileInfo> structureSetFiles, IDataflowObject sourceDataflow, IDataflowObject targetDataflow)
        {
            if (sourceDataflow == null)
            {
                throw new ArgumentNullException(nameof(sourceDataflow));
            }

            if (targetDataflow == null)
            {
                throw new ArgumentNullException(nameof(targetDataflow));
            }

            var structureSets = structureSetFiles.Select(info => info.GetSdmxObjects(_structureParsingManager))
                .SelectMany(objects => objects.StructureSets).ToArray();
            if (structureSets.Length == 0)
            {
                throw new ArgumentException("No structure sets found in provided stream", nameof(structureSetFiles));
            }

            var path = _structureMapPathBuilder.BuildStructureMapPathCrossSet(
                structureSets,
                sourceDataflow,
                targetDataflow);
            return _mergeBuilder.Build(path, _objectRetrievalManager);
        }

        /// <summary>
        /// Gets the target components.
        /// </summary>
        /// <param name="structureMap">The structure map.</param>
        /// <returns>The map between component id and component.comp_id (entity ID) </returns>
        public IDictionary<string, long> GetTargetComponents(IStructureMapObject structureMap)
        {
            IDictionary<string, long> targetComponents;
            using (var state = DbTransactionState.Create(_database))
            {
                targetComponents = StructureCache.GetComponentMapIds(state, structureMap.TargetRef);
            }

            return targetComponents;
        }

        /// <summary>
        /// Gets the code entity identifier.
        /// </summary>
        /// <param name="targetCodeReference">The target code reference.</param>
        /// <returns>The SDMX Code Entity ID (DSD_CODE.LCD_ID value)</returns>
        public long GetCodeEntityId(IStructureReference targetCodeReference)
        {
            ItemSchemeFinalStatus itemSchemeFinalStatus;
            using (var state = DbTransactionState.Create(_database))
            {
                itemSchemeFinalStatus = _cache.GetStructure(state, targetCodeReference);
            }

            ItemStatus item;
            if (itemSchemeFinalStatus.ItemIdMap.TryGetValue(targetCodeReference.ChildReference.Id, out item))
            {
                return item.SysID;
            }

            return -1;
        }

        /// <summary>
        /// Gets the dataflow.
        /// </summary>
        /// <param name="providedDataflow">The provided dataflow.</param>
        /// <param name="structureOrUsageReference">The structure or usage reference.</param>
        /// <returns>
        /// The <see cref="IDataflowObject" />.
        /// </returns>
        /// <exception cref="SdmxNoResultsException">Could not find the referenced dataflow or DSD</exception>
        public IDataflowObject GetDataflow(IDataflowObject providedDataflow, ICrossReference structureOrUsageReference)
        {
            IDataflowObject targetDataflow = null;
            if (providedDataflow.IsStructureOrUsageMatch(structureOrUsageReference))
            {
                targetDataflow = providedDataflow;
            }
            else if (structureOrUsageReference.TargetReference.EnumType == SdmxStructureEnumType.Dataflow)
            {
                targetDataflow = GetDataflow(structureOrUsageReference);
            }

            if (targetDataflow == null)
            {
                throw new SdmxNoResultsException($"Dataflow with URN '{structureOrUsageReference.TargetUrn}' not found");
            }

            return targetDataflow;
        }

        /// <summary>
        /// Builds the structure set on the fly.
        /// </summary>
        /// <param name="sourceDataflowRef">The source Dataflow Ref.</param>
        /// <param name="targetDataflowRef">The target Dataflow Ref.</param>
        /// <returns>
        /// The <see cref="IStructureSetObject" /> if it can be generated; otherwise null
        /// </returns>
        public IStructureSetObject BuildStructureSetOnTheFly(IStructureReference sourceDataflowRef, IStructureReference targetDataflowRef)
        {
            return _structureSetBuilder.Build(sourceDataflowRef, targetDataflowRef, _objectRetrievalManager);
        }
    }
}