// -----------------------------------------------------------------------
// <copyright file="EntityCloneHelper.cs" company="EUROSTAT">
//   Date Created : 2017-07-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Cloning
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;

    /// <summary>
    /// The entity clone helper.
    /// </summary>
    public class EntityCloneHelper
    {
        /// <summary>
        /// The entity persistence manager
        /// </summary>
        private readonly IEntityPersistenceManager _entityPersistenceManager;

        /// <summary>
        /// The store identifier
        /// </summary>
        private readonly string _storeId;

        /// <summary>
        /// The entity retriever manager
        /// </summary>
        private readonly IEntityRetrieverManager _entityRetrieverManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityCloneHelper" /> class.
        /// </summary>
        /// <param name="entityRetrieverManager">The entity retriever manager.</param>
        /// <param name="entityPersistenceManager">The entity persistence manager.</param>
        /// <param name="storeId">The store identifier.</param>
        public EntityCloneHelper(IEntityRetrieverManager entityRetrieverManager, IEntityPersistenceManager entityPersistenceManager, string storeId)
        {
            _entityRetrieverManager = entityRetrieverManager;
            _entityPersistenceManager = entityPersistenceManager;
            _storeId = storeId;
        }

        /// <summary>
        /// Retrieves entities, copies them and then saves the copies
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="parentEntity">
        /// The parent entity.
        /// </param>
        /// <param name="newParentId">
        /// The new parent identifier.
        /// </param>
        /// <param name="action">
        /// The action to take after saving an Entity.
        /// </param>
        public void RetrieveCopyAndSave<TEntity>(
            IEntity parentEntity,
            string newParentId,
            Action<TEntity, TEntity> action) where TEntity : class, IEntity
        {
            var sourceEntities = RetrieveEntities<TEntity>(parentEntity);
            foreach (var sourceEntity in sourceEntities)
            {
                var added = CopyAndSave(sourceEntity, newParentId);
                if (action != null)
                {
                    action(sourceEntity, added);
                }
            }
        }

        /// <summary>
        /// Retrieves entities, copies them and then saves the copies
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="parentEntity">The parent entity.</param>
        /// <param name="newParentId">The new parent identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="action">The action to take after saving an Entity.</param>
        public void RetrieveCopyAndSave<TEntity>(
            IEntity parentEntity,
            string newParentId,
            string targetStoreId,
            Action<TEntity, TEntity> action) where TEntity : class, IEntity
        {
            var sourceEntities = RetrieveEntities<TEntity>(parentEntity);
            foreach (var sourceEntity in sourceEntities)
            {
                var added = CopyAndSave(sourceEntity, newParentId, targetStoreId, x => x.CreateDeepCopy<TEntity>());
                if (action != null)
                {
                    action(sourceEntity, added);
                }
            }
        }


        /// <summary>
        /// Retrieves entities, copies them and then saves the copies
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="parentEntity">The parent entity.</param>
        /// <param name="newParentId">The new parent identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="action">The action to take after saving an Entity.</param>
        public void RetrieveCopyAndSave<TEntity>(
            IEntity parentEntity,
            string newParentId,
            string targetStoreId,
            Func<TEntity,TEntity> cloneFunc,
            Action<TEntity, TEntity> action) where TEntity : class, IEntity
        {
            var sourceEntities = RetrieveEntities<TEntity>(parentEntity);
            foreach (var sourceEntity in sourceEntities)
            {
                var added = CopyAndSave(sourceEntity, newParentId, targetStoreId, cloneFunc);
                if (action != null)
                {
                    action(sourceEntity, added);
                }
            }
        }


        /// <summary>
        /// Retrieves the source entities.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="parentEntity">
        /// The parent entity.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{TEntity}"/>.
        /// </returns>
        public IEnumerable<TEntity> RetrieveEntities<TEntity>(IEntity parentEntity) where TEntity : class, IEntity
        {
            var sourceEntities = _entityRetrieverManager.GetEntities<TEntity>(
                parentEntity.StoreId,
                parentEntity.QueryForThisParentId(),
                Detail.Full);
            return sourceEntities;
        }

        /// <summary>
        /// Retrieves the source entities.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="parentEntity">
        /// The parent entity.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{TEntity}"/>.
        /// </returns>
        public IEnumerable<TEntity> RetrieveTranscodingChildEntities<TEntity>(ComponentMappingEntity parentEntity)
            where TEntity : class, IEntity
        {
            if (!parentEntity.HasTranscoding())
            {
                return Enumerable.Empty<TEntity>();
            }

            var sourceEntities = _entityRetrieverManager.GetEntities<TEntity>(
                parentEntity.StoreId,
                parentEntity.TranscodingId.QueryForThisParentId(),
                Detail.Full);
            return sourceEntities;
        }

        /// <summary>
        /// Retrieves the mapping set.
        /// </summary>
        /// <param name="parentUrn">
        /// The parent urn.
        /// </param>
        /// <returns>
        /// The <see cref="MappingSetEntity"/>.
        /// </returns>
        public MappingSetEntity RetrieveMappingSet(Uri parentUrn)
        {
            var sourceEntities = _entityRetrieverManager.GetEntities<MappingSetEntity>(
                _storeId,
                parentUrn.ToString().QueryForThisParentId(),
                Detail.Full);
            return sourceEntities.FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the source entities.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="entityId">
        /// The entity identifier.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{TEntity}"/>.
        /// </returns>
        public IEnumerable<TEntity> RetrieveEntities<TEntity>(string entityId)
            where TEntity : class, IEntity
        {
            var sourceEntities = _entityRetrieverManager.GetEntities<TEntity>(
                _storeId,
                entityId.QueryForThisEntityId(),
                Detail.Full);
            return sourceEntities;
        }

        public IEnumerable<TEntity> RetrieveEntitiesForParent<TEntity>(string parentId)
            where TEntity : class, IEntity
        {
            var sourceEntities = _entityRetrieverManager.GetEntities<TEntity>(
                _storeId,
                parentId.QueryForThisParentId(),
                Detail.Full);
            return sourceEntities;
        }

        /// <summary>
        /// Retrieves the source entities.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="parentEntityId">The parent entity identifier.</param>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>
        /// The <see cref="IEnumerable{TEntity}" />.
        /// </returns>
        public bool AnyEntitiesForParent<TEntity>(string parentEntityId, string storeId)
            where TEntity : class, IEntity
        {
            var sourceEntities = _entityRetrieverManager.GetEntities<TEntity>(
                storeId,
                parentEntityId.QueryForThisParentId(),
                Detail.IdOnly).Any();
            return sourceEntities;
        }

        /// <summary>
        /// Gets the a "cloned" name. This is only needed for root entities like DDB Connections, DataSet, Mapping Set and Registry
        /// </summary>
        /// <typeparam name="TEntity">The type of the t entity.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <param name="template">The template.</param>
        /// <returns>The new name</returns>
        public string GetCloneName<TEntity>(TEntity entity, string template = "Copy of {0} ")
            where TEntity : ISimpleNameableEntity
        {
            IEnumerable<TEntity> entities;
            using (var authorizationScope = new SingleRequestScope())
            {
                authorizationScope.TrySetAuthorizationState(true);
                entities = _entityRetrieverManager.GetEntities<TEntity>(entity.StoreId, EntityQuery.Empty, Detail.IdAndName);
            }

            var listOfNames = entities.Select(entity1 => entity1.Name).Where(s => !string.IsNullOrWhiteSpace(s));
            var setOfNames = new HashSet<string>(listOfNames, StringComparer.OrdinalIgnoreCase);
            int x = 1;

            if (!setOfNames.Contains(entity.Name))
            {
                return entity.Name;
            }

            var newName = GetNewName(entity, x, template);
            while (setOfNames.Contains(newName))
            {
                x++;
                newName = GetNewName(entity, x, template);
            }

            return newName;
        }

        /// <summary>
        /// Copies the entity and saves it.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="sourceEntity">
        /// The source entity.
        /// </param>
        /// <param name="parentId">
        /// The parent identifier.
        /// </param>
        /// <returns>
        /// The newly added <see cref="IEntity"/>
        /// </returns>
        public TEntity CopyAndSave<TEntity>(TEntity sourceEntity, string parentId) where TEntity : IEntity
        {
            var toAddEntity = sourceEntity.CreateDeepCopy<TEntity>();
            return SaveEntity(toAddEntity, parentId);
        }

        /// <summary>
        /// Copies the entity and saves it.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="sourceEntity">The source entity.</param>
        /// <param name="parentId">The parent identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="cloneFunc">The clone function.</param>
        /// <returns>
        /// The newly added <see cref="IEntity" />
        /// </returns>
        public TEntity CopyAndSave<TEntity>(TEntity sourceEntity, string parentId, string targetStoreId, Func<TEntity, TEntity> cloneFunc) where TEntity : IEntity
        {
            var toAddEntity = cloneFunc(sourceEntity);
            toAddEntity.StoreId = targetStoreId;
            return SaveEntity(toAddEntity, parentId);
        }

        /// <summary>
        /// Copies the entity and saves it.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="sourceEntity">
        /// The source entity.
        /// </param>
        /// <param name="parentId">
        /// The parent identifier.
        /// </param>
        /// <returns>
        /// The newly added <see cref="IEntity"/>
        /// </returns>
        public TEntity CopyAndSaveUniqueName<TEntity>(TEntity sourceEntity, string parentId) where TEntity : ISimpleNameableEntity
        {
            var toAddEntity = sourceEntity.CreateDeepCopy<TEntity>();
            toAddEntity.Name = GetCloneName(toAddEntity);
            return SaveEntity(toAddEntity, parentId);
        }

        /// <summary>
        /// Copies the entity and saves it.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="sourceEntity">The source entity.</param>
        /// <param name="parentId">The parent identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <returns>
        /// The newly added <see cref="IEntity" />
        /// </returns>
        public TEntity CopyAndSaveUniqueName<TEntity>(TEntity sourceEntity, string parentId, string targetStoreId) where TEntity : ISimpleNameableEntity
        {
            return CopyAndSaveUniqueName(
                sourceEntity,
                parentId,
                targetStoreId,
                x => sourceEntity.CreateDeepCopy<TEntity>());
        }

        /// <summary>
        /// Copies the entity and saves it.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="sourceEntity">The source entity.</param>
        /// <param name="parentId">The parent identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="cloneFunc">The clone function.</param>
        /// <returns>
        /// The newly added <see cref="IEntity" />
        /// </returns>
        public TEntity CopyAndSaveUniqueName<TEntity>(TEntity sourceEntity, string parentId, string targetStoreId, Func<TEntity, TEntity> cloneFunc) where TEntity : ISimpleNameableEntity
        {
            var toAddEntity = cloneFunc(sourceEntity);
            toAddEntity.StoreId = targetStoreId;
            toAddEntity.Name = GetCloneName(toAddEntity);
            return SaveEntity(toAddEntity, parentId);
        }

        /// <summary>
        /// Saves the entity.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="toAddEntity">
        /// To add entity.
        /// </param>
        /// <param name="parentId">
        /// The parent identifier.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public TEntity SaveEntity<TEntity>(TEntity toAddEntity, string parentId) where TEntity : IEntity
        {
            toAddEntity.ParentId = parentId;
            return _entityPersistenceManager.Add(toAddEntity);
        }

        /// <summary>
        /// Saves the entity.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="toAddEntity">
        /// To add entity.
        /// </param>
        /// <param name="parentId">
        /// The parent identifier.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{TEntit}"/>.
        /// </returns>
        public IEnumerable<TEntity> SaveEntities<TEntity>(IList<TEntity> toAddEntity, string parentId)
            where TEntity : IEntity
        {
            for (int index = 0; index < toAddEntity.Count; index++)
            {
                var entity = toAddEntity[index];
                entity.ParentId = parentId;
            }

            return _entityPersistenceManager.AddEntities(toAddEntity);
        }

        /// <summary>
        /// Gets the new name.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="x">The current index.</param>
        /// <param name="template">The template.</param>
        /// <returns>The new name</returns>
        public static string GetNewName(ISimpleNameableEntity entity, int x, string template)
        {
            var copyNumber = string.Format(CultureInfo.InvariantCulture, "({0})", x);
            var maxLength = 50 - copyNumber.Length;
            
            string substring = string.Format(CultureInfo.InvariantCulture, template, entity.Name);
            if (substring.Length > maxLength)
            {
                substring = substring.Substring(0, maxLength);
            }

            return string.Format(CultureInfo.InvariantCulture, "{0}{1}", substring, copyNumber);
        }
    }
}