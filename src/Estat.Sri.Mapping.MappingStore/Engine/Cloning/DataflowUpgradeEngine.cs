// -----------------------------------------------------------------------
// <copyright file="DataflowUpgradeEngine.cs" company="EUROSTAT">
//   Date Created : 2017-07-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Cloning
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.Mapping.MappingStore.Model.EntityMapObjects;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// The dataflow upgrade engine.
    /// </summary>
    internal class DataflowUpgradeEngine : IDataflowUpgrade
    {
        /// <summary>
        /// The data set copy
        /// </summary>
        private readonly DataSetCopy _dataSetCopy;

        /// <summary>
        /// The entity clone helper
        /// </summary>
        private readonly EntityCloneHelper _entityCloneHelper;

        /// <summary>
        /// The SDMX store clone helper
        /// </summary>
        private readonly SdmxStoreCloneHelper _sdmxStoreCloneHelper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataflowUpgradeEngine" /> class.
        /// </summary>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        /// <param name="sdmxStoreCloneHelper">The SDMX store clone helper.</param>
        public DataflowUpgradeEngine(EntityCloneHelper entityCloneHelper, SdmxStoreCloneHelper sdmxStoreCloneHelper)
        {
            _entityCloneHelper = entityCloneHelper;

            _dataSetCopy = new DataSetCopy(_entityCloneHelper);
            _sdmxStoreCloneHelper = sdmxStoreCloneHelper;
        }

        /// <summary>
        /// Upgrades, in other words copy the mapping set, of the Dataflow identified by <paramref name="sourceDataflowUrn" />  to dataflow <paramref name="targetDataflowUrn" />
        /// </summary>
        /// <param name="sourceDataflowUrn">The source dataflow urn.</param>
        /// <param name="targetDataflowUrn">The target dataflow urn.</param>
        /// <param name="structureSets">The structure sets. If it is null  the tool will try to auto-generate the structure set</param>
        /// <returns>
        /// The <see cref="MappingSetEntity" />.
        /// </returns>
        public MappingSetEntity Upgrade(
            Uri sourceDataflowUrn,
            Uri targetDataflowUrn,
            IEnumerable<FileInfo> structureSets)
        {
            var sdmxStoreCloneHelper = _sdmxStoreCloneHelper;
            var sourceDataflow = sdmxStoreCloneHelper.GetDataflow(sourceDataflowUrn);
            if (sourceDataflow == null)
            {
                throw new ResourceNotFoundException($"Source Dataflow not found: {sourceDataflowUrn}");
            }
            var targetDataflow = sdmxStoreCloneHelper.GetDataflow(targetDataflowUrn);
            if (targetDataflow == null)
            {
                throw new ResourceNotFoundException($"Targer Dataflow not found: {targetDataflowUrn}");
            }
            IStructureSetObject structureSet;
            if (structureSets != null)
            {
                structureSet = sdmxStoreCloneHelper.GetMergedStructureSet(
                    structureSets,
                    sourceDataflow,
                    targetDataflow);
            }
            else
            {
                structureSet = sdmxStoreCloneHelper.BuildStructureSetOnTheFly(sourceDataflow.AsReference, targetDataflow.AsReference);
            }

            return Copy(structureSet, sourceDataflow, targetDataflow);
        }

        /// <summary>
        /// Saves the map.
        /// </summary>
        /// <param name="mappingSetMap">
        /// The mapping set map.
        /// </param>
        /// <returns>
        /// The <see cref="MappingSetEntity"/>.
        /// </returns>
        private MappingSetEntity SaveMap(MappingSetFullObject mappingSetMap)
        {
            var addedMappingSet = _entityCloneHelper.SaveEntity(
                mappingSetMap.Entity,
                mappingSetMap.Dataflow.Urn.ToString());

            foreach (var componentMappingMap in mappingSetMap.Mappings)
            {
                var addedComponentMappingEntity = _entityCloneHelper.SaveEntity(
                    componentMappingMap.Entity,
                    addedMappingSet.EntityId);
                if (componentMappingMap.HasTranscoding)
                {
                     _entityCloneHelper.SaveEntities(componentMappingMap.Transcoding.Rules,addedComponentMappingEntity.EntityId);
                }
            }

            foreach(var timeDimensionMappingEntity in mappingSetMap.TimeTranscodings)
            {
                _entityCloneHelper.SaveEntity(
                timeDimensionMappingEntity,
                addedMappingSet.EntityId);
            }

            foreach (var lastUpdatedEntity in mappingSetMap.LastUpdatedEntities)
            {
                _entityCloneHelper.SaveEntity(
                lastUpdatedEntity,
                addedMappingSet.EntityId);
            }

            foreach (var validToMappingEntity in mappingSetMap.ValidToMappingEntities)
            {
                _entityCloneHelper.SaveEntity(
                validToMappingEntity,
                addedMappingSet.EntityId);
            }

            foreach (var updateStatusEntity in mappingSetMap.UpdateStatusEntities)
            {
                _entityCloneHelper.SaveEntity(
                updateStatusEntity,
                addedMappingSet.EntityId);
            }


            return addedMappingSet;
        }

        /// <summary>
        /// Copies the specified store identifier.
        /// </summary>
        /// <param name="structureSetObject">
        /// The structure set object.
        /// </param>
        /// <param name="sourceDataflow">
        /// The source dataflow.
        /// </param>
        /// <param name="destincationDataflow">
        /// The target dataflow.
        /// </param>
        /// <returns>
        /// The <see cref="MappingSetEntity"/>.
        /// </returns>
        private MappingSetEntity Copy(
            IStructureSetObject structureSetObject,
            IDataflowObject sourceDataflow,
            IDataflowObject destincationDataflow)
        {
            var structureMap = structureSetObject.StructureMapList.Single();
            var structureMapTargetDataflow = _sdmxStoreCloneHelper.GetDataflow(destincationDataflow, structureMap.TargetRef);
            var structureMapsSourceDataflow = _sdmxStoreCloneHelper.GetDataflow(sourceDataflow, structureMap.SourceRef);

            ValidateStructureMap(structureMapsSourceDataflow, structureMapTargetDataflow, structureMap);

            using (var noAuthContext = new SingleRequestScope())
            {
                noAuthContext.RecordOnlyRoot = true;
                noAuthContext.TrySetAuthorizationState(true);
                var mappingSetMap = this.BuildFullObject(
                    structureMap,
                    structureMapsSourceDataflow.AsReference,
                    structureMapTargetDataflow);
                return SaveMap(mappingSetMap);
            }
        }

        /// <summary>
        /// Validates the structure map.
        /// </summary>
        /// <param name="structureMapsSourceDataflow">The structure maps source dataflow.</param>
        /// <param name="structureMapTargetDataflow">The structure map target dataflow.</param>
        /// <param name="structureMap">The structure map.</param>
        /// <exception cref="ValidationException">Mapping of components with different code-lists requires a CodelistMap</exception>
        private void ValidateStructureMap(
            IDataflowObject structureMapsSourceDataflow,
            IDataflowObject structureMapTargetDataflow,
            IStructureMapObject structureMap)
        {
            var sourceDsd = _sdmxStoreCloneHelper.GetDsd(structureMapsSourceDataflow);
            var targetDsd = _sdmxStoreCloneHelper.GetDsd(structureMapTargetDataflow);
            var componentMapObjects = structureMap.Components.ToDictionary(o => o.MapConceptRef);
            var targetDsdComponents = targetDsd.Components.ToDictionary(component => component.Id);
            foreach (var component in sourceDsd.Components)
            {
                string targetComponentId = component.Id;
                ICodelistMapObject codelistMapObject = null;
                IComponentMapObject componentMap;
                if (componentMapObjects.TryGetValue(component.Id, out componentMap))
                {
                    targetComponentId = componentMap.MapTargetConceptRef;
                    codelistMapObject = componentMap.GetCodelistMap();
                }

                IComponent targetComponent;
                if (targetDsdComponents.TryGetValue(targetComponentId, out targetComponent))
                {
                    if (codelistMapObject == null)
                    {
                        if (component.HasCodedRepresentation() && targetComponent.HasCodedRepresentation())
                        {
                            var sourceUrn = component.Representation.Representation.MaintainableUrn.ToString();
                            var targetUrn = targetComponent.Representation.Representation.MaintainableUrn.ToString();
                            if (!sourceUrn.Equals(targetUrn))
                            {
                                throw new ValidationException(
                                    "Mapping of components with different code-lists requires a CodelistMap");
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds the full object.
        /// </summary>
        /// <param name="structureMapObject">
        ///     The structure map object.
        /// </param>
        /// <param name="sourceDataflow">
        ///     The source dataflow.
        /// </param>
        /// <param name="targetDataflow">
        ///     The target dataflow.
        /// </param>
        /// <exception cref="ResourceConflictException">
        /// Target dataflow has already a Mapping Set defined. Please delete it first
        /// </exception>
        /// <returns>
        /// The <see cref="MappingSetFullObject"/>.
        /// </returns>
        private MappingSetFullObject BuildFullObject(IStructureMapObject structureMapObject, IStructureReference sourceDataflow, IDataflowObject targetDataflow)
        {
            var sourceEntity = _entityCloneHelper.RetrieveMappingSet(sourceDataflow.TargetUrn);
            if (sourceEntity == null)
            {
                return null;
            }

            var existingTargetMappingSet = _entityCloneHelper.RetrieveMappingSet(targetDataflow.Urn);

            if (existingTargetMappingSet != null)
            {
                throw new ResourceConflictException(
                    $"Dataflow '{targetDataflow.Urn}' already has a Mapping Set defined");
            }

            if (string.IsNullOrWhiteSpace(sourceEntity.DataSetId))
            {
                return null;
            }

            var dataSetMapObject = _dataSetCopy.CloneDataset(sourceEntity.DataSetId);
            if (dataSetMapObject == null)
            {
                return null;
            }

            var mappingSetEntity = sourceEntity.CreateDeepCopy<MappingSetEntity>();
            mappingSetEntity.Name = this._entityCloneHelper.GetCloneName(sourceEntity);
            mappingSetEntity.DataSetId = dataSetMapObject.TargetEntity.EntityId;
            var mappingSetMap = new MappingSetFullObject { Dataflow = targetDataflow, Entity = mappingSetEntity };

            var componentMappingEntities = _entityCloneHelper.RetrieveEntities<ComponentMappingEntity>(sourceEntity).ToArray();

            BuildMap(
                mappingSetMap,
                componentMappingEntities,
                structureMapObject,
                dataSetMapObject);

            return mappingSetMap;
        }

        /// <summary>
        /// Builds the map.
        /// </summary>
        /// <param name="mappingSetMap">
        /// The mapping set map.
        /// </param>
        /// <param name="sourceEntities">
        /// The source entities.
        /// </param>
        /// <param name="structureMap">
        /// The structure map.
        /// </param>
        /// <param name="dataSetMapObject">
        /// The data set map object.
        /// </param>
        private void BuildMap(
            MappingSetFullObject mappingSetMap,
            IList<ComponentMappingEntity> sourceEntities,
            IStructureMapObject structureMap,
            DataSetMapObject dataSetMapObject)
        {
            var sdmxStoreCloneHelper = _sdmxStoreCloneHelper;
            var componentMappingMaps = mappingSetMap.Mappings;

            var codelistMapObjects = ((IStructureSetObject)structureMap.MaintainableParent).CodelistMapList;

            // get the map/dictionary between a component.id (aka the ObjectId) and component.comp_id (aka the EntityId)
            var targetComponents = sdmxStoreCloneHelper.GetTargetComponents(structureMap);

            // build another dictionary with component id to IComponentMapObject. 
            // This is needed to find the corresponding ComponentMap for each Mapping Assistant mapping
            var componentMapObjects = structureMap.Components.ToDictionary(o => o.MapConceptRef);

            // Next we got through all existing source entities (normal component mappings)
            foreach (var sourceMappingEntity in sourceEntities.Where(m => m.Type.AsApiMappingType() == ComponentMappingType.Normal))
            {
                var componentId = sourceMappingEntity.Component.ObjectId;
                if (string.IsNullOrWhiteSpace(componentId))
                {
                    throw new MissingInformationException(
                        "Unsupported mapping found. Normal mappings should have a component Id");
                }

                IComponentMapObject componentMap;
                string targetComponentId;

                // Try to get ComponentMap for the component that is used in the Mapping
                // If we don't find one, we assume that the target component id remains the same as the source component id
                if (componentMapObjects.TryGetValue(componentId, out componentMap))
                {
                    targetComponentId = componentMap.MapTargetConceptRef;
                }
                else
                {
                    targetComponentId = componentId;
                }

                // Build the target mapping entity. We don't assign any parent id or entity id yet
                long targetComponentEntityId;
                if (!targetComponents.TryGetValue(targetComponentId, out targetComponentEntityId))
                {
                    // we couldn't find the target component, maybe it doesn't exist in the target DSD
                    continue;
                }

                var targetMappingEntity = BuildTargetComponentMappingEntity(
                    sourceMappingEntity,
                    targetComponentId,
                    dataSetMapObject,
                    targetComponentEntityId);

                var componentMappingMap = new ComponentMappingFullObject { Entity = targetMappingEntity };

                HandleTranscoding(dataSetMapObject, sourceMappingEntity, codelistMapObjects, componentMap, componentMappingMap);
                

                componentMappingMaps.Add(componentMappingMap);
            }
            var sourceMappingSetId = sourceEntities.First().ParentId;
            // Build TimeMappings
            var timeMappings = _entityCloneHelper.RetrieveEntitiesForParent<TimeDimensionMappingEntity>(sourceMappingSetId);
            foreach(var timeMappingEntity in timeMappings)
            {
                mappingSetMap.TimeTranscodings.Add(timeMappingEntity);
            }
            
            var lastUpdatedEntities = _entityCloneHelper.RetrieveEntitiesForParent<LastUpdatedEntity>(sourceMappingSetId);
            foreach (var lastUpdatedEntity in lastUpdatedEntities)
            {
                mappingSetMap.LastUpdatedEntities.Add(lastUpdatedEntity);
            }
            var validToMappingEntities = _entityCloneHelper.RetrieveEntitiesForParent<ValidToMappingEntity>(sourceMappingSetId);
            foreach (var validToMappingEntity in validToMappingEntities)
            {
                mappingSetMap.ValidToMappingEntities.Add(validToMappingEntity);
            }
            var updateStatusEntities = _entityCloneHelper.RetrieveEntitiesForParent<UpdateStatusEntity>(sourceMappingSetId);
            foreach (var updateStatusEntity in updateStatusEntities)
            {
                mappingSetMap.UpdateStatusEntities.Add(updateStatusEntity);
            }
        }

        /// <summary>
        /// Handles the transcoding.
        /// </summary>
        /// <param name="dataSetMapObject">The data set map object.</param>
        /// <param name="sourceMappingEntity">The source mapping entity.</param>
        /// <param name="codelistMapObjects">The codelist map objects.</param>
        /// <param name="componentMap">The component map.</param>
        /// <param name="componentMappingMap">The component mapping map.</param>
        private void HandleTranscoding(
            DataSetMapObject dataSetMapObject,
            ComponentMappingEntity sourceMappingEntity,
            IList<ICodelistMapObject> codelistMapObjects,
            IComponentMapObject componentMap,
            ComponentMappingFullObject componentMappingMap)
        {
            // Handle transcoding if there is transcoding 
            if (sourceMappingEntity.HasTranscoding())
            {
                // TODO Support non coded transcoding when support is added
                // Get code mapper, this will transcoding codes from the source to the target, 
                // componentMap can be null
                var codeMapper = GetCodeMapper(codelistMapObjects, componentMap);

                var targetTranscodingEntity = new TranscodingEntity();
                componentMappingMap.Transcoding.Entity = targetTranscodingEntity;

                var rules = BuildTargetTranscodingRules(
                    targetTranscodingEntity,
                    sourceMappingEntity,
                    codeMapper,
                    dataSetMapObject);
                componentMappingMap.Transcoding.Rules.AddAll(rules);
            }
        }

        /// <summary>
        /// Builds the target transcoding rules.
        /// </summary>
        /// <param name="targetTranscodingEntity">The target transcoding entity.</param>
        /// <param name="sourceMappingEntity">The source mapping entity.</param>
        /// <param name="codeMapper">The code mapper.</param>
        /// <param name="dataSetMapObject">The data set map object.</param>
        /// <returns>
        /// The <see cref="IEnumerable{TranscodingRuleEntity}" />.
        /// </returns>
        private IEnumerable<TranscodingRuleEntity> BuildTargetTranscodingRules(TranscodingEntity targetTranscodingEntity, ComponentMappingEntity sourceMappingEntity, Func<IIdentifiableEntity, IIdentifiableEntity> codeMapper, DataSetMapObject dataSetMapObject)
        {
            // we get the rules when we don't have a time transcoding because we already copied the rules
            if (!targetTranscodingEntity.HasTimeTrascoding())
            {
                var sourceRules =
                    _entityCloneHelper.RetrieveTranscodingChildEntities<TranscodingRuleEntity>(sourceMappingEntity);
                foreach (var sourceRule in sourceRules)
                {
                    var targetRule = sourceRule.CreateDeepCopy<TranscodingRuleEntity>();
                    yield return targetRule;
                }
            }
        }

        /// <summary>
        /// Builds the target transcoding entity.
        /// </summary>
        /// <param name="sourceMappingEntity">
        /// The source mapping entity.
        /// </param>
        /// <param name="dataSetMapObject">
        /// The data set map object.
        /// </param>
        /// <returns>
        /// The <see cref="TranscodingEntity"/>.
        /// </returns>
        private TranscodingEntity BuildTargetTranscodingEntity(
            ComponentMappingEntity sourceMappingEntity,
            DataSetMapObject dataSetMapObject)
        {
            var sourceTranscodingEntity =
                _entityCloneHelper.RetrieveEntities<TranscodingEntity>(sourceMappingEntity.TranscodingId)
                    .First();
            var targetTranscodingEntity = sourceTranscodingEntity.CreateDeepCopy<TranscodingEntity>();

            CloneTimeTranscoding(dataSetMapObject, sourceTranscodingEntity, targetTranscodingEntity);
            return targetTranscodingEntity;
        }

        /// <summary>
        /// Clones the time transcoding.
        /// </summary>
        /// <param name="dataSetMapObject">
        /// The data set map object.
        /// </param>
        /// <param name="sourceTranscodingEntity">
        /// The source transcoding entity.
        /// </param>
        /// <param name="targetTranscodingEntity">
        /// The target transcoding entity.
        /// </param>
        /// <exception cref="ResourceNotFoundException">
        /// Target DataSet column doesn't exist in DataSetMap
        /// </exception>
        private void CloneTimeTranscoding(
            DataSetMapObject dataSetMapObject,
            TranscodingEntity sourceTranscodingEntity,
            TranscodingEntity targetTranscodingEntity)
        {
            if (sourceTranscodingEntity.TimeTranscoding == null)
            {
                return;
            }

            foreach (var sourceTimeTranscoding in sourceTranscodingEntity.TimeTranscoding)
            {
                var targetTimeTranscoding =
                    targetTranscodingEntity.TimeTranscoding.First(
                        entity => string.Equals(entity.Frequency, sourceTimeTranscoding.Frequency));
                if (sourceTimeTranscoding.DateColumn.HasEntityId())
                {
                    targetTimeTranscoding.DateColumn =
                        dataSetMapObject.ColumnMap.GetTargetEntity(sourceTimeTranscoding.DateColumn);
                    if (targetTimeTranscoding.DateColumn == null)
                    {
                        throw new ResourceNotFoundException("Target DataSet column doesn't exist in DataSetMap");
                    }
                }

                if (sourceTimeTranscoding.Period != null)
                {
                    var targetPeriodColumn =
                        dataSetMapObject.ColumnMap.GetTargetEntity(sourceTimeTranscoding.Period.Column);
                    targetTimeTranscoding.Period.Column = targetPeriodColumn;

                    for (var index = 0; index < sourceTimeTranscoding.Period.Rules.Count; index++)
                    {
                        var sourceRule = sourceTimeTranscoding.Period.Rules[index];
                        var targetRule = targetTimeTranscoding.Period.Rules[index];
                        targetRule.LocalCodes =
                            sourceRule.LocalCodes.Select(
                                entity => dataSetMapObject.LocalCodeMap.GetTargetEntity(entity)).ToList();
                    }
                }

                if (sourceTimeTranscoding.Year != null)
                {
                    var targetPeriodColumn =
                        dataSetMapObject.ColumnMap.GetTargetEntity(sourceTimeTranscoding.Year.Column);
                    targetTimeTranscoding.Year.Column = targetPeriodColumn;
                }
            }
        }

        /// <summary>
        /// Builds the target component mapping entity.
        /// </summary>
        /// <param name="sourceMappingEntity">
        /// The source mapping entity.
        /// </param>
        /// <param name="targetComponentId">
        /// The target component identifier.
        /// </param>
        /// <param name="dataSetMapObject">
        /// The data set map object.
        /// </param>
        /// <param name="targetComponentEntityId">
        /// The target component entity identifier.
        /// </param>
        /// <returns>
        /// The <see cref="ComponentMappingEntity"/>.
        /// </returns>
        /// <exception cref="ResourceNotFoundException">
        /// DataSet Column entity not found
        /// </exception>
        private ComponentMappingEntity BuildTargetComponentMappingEntity(
            ComponentMappingEntity sourceMappingEntity,
            string targetComponentId,
            DataSetMapObject dataSetMapObject,
            long targetComponentEntityId)
        {
            var targetMappingEntity = sourceMappingEntity.CreateDeepCopy<ComponentMappingEntity>();

            // TODO do we need to get the EntityID (COMPONENT.COMP_ID) here or ?
            if (targetComponentId != null)
            {
                targetMappingEntity.Component = new Component
                                                    {
                                                        EntityId =
                                                            targetComponentEntityId.ToString(
                                                                CultureInfo.InvariantCulture),
                                                        ObjectId = targetComponentId
                                                    };
            }

            targetMappingEntity.GetColumns().Clear();
            foreach (var dataSetColumnEntity in sourceMappingEntity.GetColumns())
            {
                var columnWithNewEntityId = dataSetMapObject.ColumnMap.GetTargetEntityName(dataSetColumnEntity.Name);
                if (columnWithNewEntityId == null)
                {
                    throw new ResourceNotFoundException("DataSet Column entity not found");
                }

                targetMappingEntity.GetColumns().Add(columnWithNewEntityId);
            }

            return targetMappingEntity;
        }

        /// <summary>
        /// Gets the code mapper.
        /// </summary>
        /// <param name="codelistMapObjects">
        /// The codelist map objects.
        /// </param>
        /// <param name="componentMap">
        /// The component map.
        /// </param>
        /// <returns>
        /// The method that builds a SDMX code entity from another
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// codelistMapObjects
        /// or
        /// database
        /// or
        /// cache
        /// </exception>
        private Func<IIdentifiableEntity, IIdentifiableEntity> GetCodeMapper(
            IList<ICodelistMapObject> codelistMapObjects,
            IComponentMapObject componentMap)
        {
            if (codelistMapObjects == null)
            {
                throw new ArgumentNullException(nameof(codelistMapObjects));
            }

            // ValueMap not supported by the registry so we don't support it as well
            // TO support ValueMap we need to get the destination component's codelist if one exists
            // We don't yet support non-coded transcoding
            Func<IIdentifiableEntity, IIdentifiableEntity> codeMapper;
            var codelistMapObject = componentMap.GetCodelistMap();
            if (codelistMapObject != null)
            {
                var dictionary = codelistMapObject.Items.ToDictionary(map => map.SourceId, map => map.TargetId);
                var targetCodelistRef = codelistMapObject.TargetRef;

                codeMapper = entity => CloneCodeEntity(targetCodelistRef, entity, dictionary);
            }
            else
            {
                codeMapper = GetCodeEntity;
            }

            return codeMapper;
        }

        /// <summary>
        /// Gets the code reference.
        /// </summary>
        /// <param name="codelistReference">
        /// The codelist reference.
        /// </param>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <returns>
        /// The <see cref="IStructureReference"/>.
        /// </returns>
        private IStructureReference GetCodeReference(IMaintainableRefObject codelistReference, string code)
        {
            return new StructureReferenceImpl(
                codelistReference.AgencyId,
                codelistReference.MaintainableId,
                codelistReference.Version,
                SdmxStructureEnumType.Code,
                code);
        }

        /// <summary>
        /// Gets the code entity.
        /// </summary>
        /// <param name="sourceCode">
        /// The source code.
        /// </param>
        /// <returns>
        /// The <see cref="IIdentifiableEntity"/>.
        /// </returns>
        private IIdentifiableEntity GetCodeEntity(IIdentifiableEntity sourceCode)
        {
            var identifiableEntity = sourceCode.CreateDeepCopy<IIdentifiableEntity>();
            identifiableEntity.EntityId = sourceCode.EntityId;
            return identifiableEntity;
        }

        /// <summary>
        /// Gets the code entity.
        /// </summary>
        /// <param name="targetCodelistReference">The target codelist reference.</param>
        /// <param name="sourceCode">The source code.</param>
        /// <param name="dictionary">The dictionary.</param>
        /// <returns>
        /// The <see cref="IIdentifiableEntity" />.
        /// </returns>
        private IIdentifiableEntity CloneCodeEntity(IStructureReference targetCodelistReference, IIdentifiableEntity sourceCode, Dictionary<string, string> dictionary)
        {
            var targetCode = GetCodeWithCodelistMap(dictionary, sourceCode.ObjectId);
            var targetCodeReference = GetCodeReference(targetCodelistReference, targetCode);
            var targetEntityId = _sdmxStoreCloneHelper.GetCodeEntityId(targetCodeReference);
            if (targetEntityId > 0)
            {
                var targetEntity = sourceCode.CreateDeepCopy<IIdentifiableEntity>();
                targetEntity.ObjectId = targetCode;
                targetEntity.EntityId = targetEntityId.ToString(CultureInfo.InvariantCulture);
                return targetEntity;
            }

            return null;
        }

        /// <summary>
        /// Gets the code with codelist map.
        /// </summary>
        /// <param name="codeDictionary">
        /// The code dictionary.
        /// </param>
        /// <param name="sourceCode">
        /// The source code.
        /// </param>
        /// <returns>
        /// The code
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// sourceCode is <c>null</c>
        /// </exception>
        private string GetCodeWithCodelistMap(Dictionary<string, string> codeDictionary, string sourceCode)
        {
            if (sourceCode == null)
            {
                throw new ArgumentNullException(nameof(sourceCode));
            }

            string targetCode;
            if (codeDictionary.TryGetValue(sourceCode, out targetCode))
            {
                return targetCode;
            }

            // we return the original code if no match is found because the CodelistMap may contain only the differences
            // TODO we will need later to check if a code with code if exists ? 
            return sourceCode;
        }
    }
}