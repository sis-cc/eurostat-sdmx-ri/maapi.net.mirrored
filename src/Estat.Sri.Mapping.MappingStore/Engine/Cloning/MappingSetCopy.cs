﻿// -----------------------------------------------------------------------
// <copyright file="MappingSetCopy.cs" company="EUROSTAT">
//   Date Created : 2017-07-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Cloning
{
    using System;
    using System.Collections;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.Mapping.MappingStore.Model.EntityMapObjects;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// A class responsible for cloning a DataSet
    /// </summary>
    internal class MappingSetCopy
    {
        /// <summary>
        /// The entity clone helper
        /// </summary>
        private readonly EntityCloneHelper _entityCloneHelper;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingSetCopy" /> class.
        /// </summary>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        public MappingSetCopy(EntityCloneHelper entityCloneHelper)
        {
            _entityCloneHelper = entityCloneHelper;
        }

        /// <summary>
        /// Clones the dataset.
        /// </summary>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <returns>
        /// The <see cref="MappingSetFullObject" />.
        /// </returns>
        public MappingSetFullObject CloneMappingSet(string entityId, IDataflowObject dataflow)
        {
            var mappingSet = _entityCloneHelper.RetrieveEntities<MappingSetEntity>(entityId).FirstOrDefault();
            if (mappingSet == null)
            {
                return null;
            }

            return CloneMappingSet(mappingSet, dataflow);
        }

        /// <summary>
        /// Copies the specified Mapping Set and the children. It assumes that Dataflow is already present
        /// </summary>
        /// <param name="sourceMappingSet">The data set.</param>
        /// <param name="dataSetMapObject">The data set map object.</param>
        /// <returns>
        /// The <see cref="MappingSetFullObject" />.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">dataSet is null</exception>
        public MappingSetFullObject CopyMappingSet(MappingSetEntity sourceMappingSet, DataSetMapObject dataSetMapObject)
        {
            if (sourceMappingSet == null)
            {
                throw new ArgumentNullException(nameof(sourceMappingSet));
            }

            if (dataSetMapObject == null)
            {
                throw new ArgumentNullException(nameof(dataSetMapObject));
            }

            var mappingSetFullObject = new MappingSetFullObject();
            var targetParent = dataSetMapObject.TargetEntity;
            var targetStoreId = targetParent.StoreId;
            var retrieveEntitiesForParent = _entityCloneHelper.AnyEntitiesForParent<MappingSetEntity>(sourceMappingSet.ParentId, targetStoreId);
            if (retrieveEntitiesForParent)
            {
                throw new ResourceConflictException(string.Format(CultureInfo.InvariantCulture, "The dataflow '{0}' has already a Mapping Set at the target mapping store : {1}", sourceMappingSet.ParentId, targetStoreId));
            }

            var targetMappingSet = _entityCloneHelper.CopyAndSaveUniqueName(sourceMappingSet, sourceMappingSet.ParentId, targetStoreId, entity => CloneMappingSetEntity(entity, targetParent));
            mappingSetFullObject.Entity = targetMappingSet;
            using (var disableAuth = new SingleRequestScope())
            {
                disableAuth.TrySetAuthorizationState(true);
                disableAuth.RecordOnlyRoot = true;
                CopyMappingSetChildren(sourceMappingSet, dataSetMapObject, mappingSetFullObject);
            }

            return mappingSetFullObject;
        }

        /// <summary>
        /// Copies the specified Mapping Set and the children.
        /// </summary>
        /// <param name="sourceMappingSet">The data set.</param>
        /// <param name="targetDataflow">The target dataflow.</param>
        /// <returns>
        /// The <see cref="MappingSetFullObject" />.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">sourceMappingSet is null</exception>
        /// <exception cref="System.ArgumentException">Cannot copy mapping set to the same dataflow</exception>
        public MappingSetFullObject CloneMappingSet(MappingSetEntity sourceMappingSet, IDataflowObject targetDataflow)
        {
            if (sourceMappingSet == null)
            {
                throw new ArgumentNullException(nameof(sourceMappingSet));
            }

            if (targetDataflow == null)
            {
                throw new ArgumentNullException(nameof(targetDataflow));
            }

            var targetParentId = targetDataflow.Urn.ToString();
            if (targetParentId.Equals(sourceMappingSet.ParentId))
            {
                throw new ArgumentException("Cannot copy mapping set to the same dataflow");
            }

            var mappingSetFullObject = new MappingSetFullObject();

            var targetMappingSet = _entityCloneHelper.CopyAndSaveUniqueName(sourceMappingSet, targetParentId);

            using (var disableAuth = new SingleRequestScope())
            {
                disableAuth.TrySetAuthorizationState(true);
                disableAuth.RecordOnlyRoot = true;
                _entityCloneHelper.RetrieveCopyAndSave<ComponentMappingEntity>(
                    sourceMappingSet,
                    targetMappingSet.EntityId,
                    (sourceEntity, targetEntity) =>
                        {
                            CloneComponentMappingAndChildren(targetEntity, mappingSetFullObject, sourceEntity, null);
                        });
            }

            return mappingSetFullObject;
        }

        /// <summary>
        /// Clones the component mapping.
        /// </summary>
        /// <param name="sourceEntity">The source entity.</param>
        /// <param name="dataSetMapObject">The data set map object.</param>
        /// <returns>
        /// The <see cref="ComponentMappingEntity" />
        /// </returns>
        private static ComponentMappingEntity CloneComponentMapping(ComponentMappingEntity sourceEntity, DataSetMapObject dataSetMapObject)
        {
            var clone = sourceEntity.CreateDeepCopy<ComponentMappingEntity>();
            if (ObjectUtil.ValidCollection(sourceEntity.GetColumns()) && dataSetMapObject?.ColumnMap != null)
            {
                clone.GetColumns().Clear();
                foreach (var dataSetColumnEntity in sourceEntity.GetColumns())
                {
                    clone.GetColumns().Add(dataSetMapObject.ColumnMap.GetTargetEntity(dataSetColumnEntity));
                }
            }

            return clone;
        }

        /// <summary>
        /// Clones the mapping set entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="targetParent">The target parent.</param>
        /// <returns>
        /// The <see cref="MappingSetEntity" />.
        /// </returns>
        private static MappingSetEntity CloneMappingSetEntity(MappingSetEntity entity, DatasetEntity targetParent)
        {
            var clone = entity.CreateDeepCopy<MappingSetEntity>();
            clone.DataSetId = targetParent?.EntityId;
            return clone;
        }

        /// <summary>
        /// Copies the mapping set children.
        /// </summary>
        /// <param name="sourceMappingSet">The source mapping set.</param>
        /// <param name="dataSetMapObject">The data set map object.</param>
        /// <param name="mappingSetFullObject">The mapping set full object.</param>
        private void CopyMappingSetChildren(
            MappingSetEntity sourceMappingSet,
            DataSetMapObject dataSetMapObject,
            MappingSetFullObject mappingSetFullObject)
        {
            _entityCloneHelper.RetrieveCopyAndSave<ComponentMappingEntity>(
                sourceMappingSet,
                mappingSetFullObject.Entity.EntityId,
                mappingSetFullObject.Entity.StoreId,
                x => CloneComponentMapping(x, dataSetMapObject),
                (sourceEntity, targetEntity) =>
                {
                    CloneComponentMappingAndChildren(targetEntity, mappingSetFullObject, sourceEntity, dataSetMapObject);
                });
        }

        /// <summary>
        /// Clones the component mapping and children.
        /// </summary>
        /// <param name="targetEntity">The target entity.</param>
        /// <param name="mappingSetFullObject">The mapping set full object.</param>
        /// <param name="sourceEntity">The source entity.</param>
        /// <param name="dataSetMapObject">The data set map object.</param>
        private void CloneComponentMappingAndChildren(ComponentMappingEntity targetEntity, MappingSetFullObject mappingSetFullObject, ComponentMappingEntity sourceEntity, DataSetMapObject dataSetMapObject)
        {
            var componentMappingFullObject = new ComponentMappingFullObject();
            componentMappingFullObject.Entity = targetEntity;
            mappingSetFullObject.Mappings.Add(componentMappingFullObject);
            if (sourceEntity.HasTranscoding())
            {
                _entityCloneHelper.RetrieveCopyAndSave<TranscodingEntity>(
                    sourceEntity,
                    targetEntity.EntityId,
                    targetEntity.StoreId,
                    sourceTranscoding => CloneTranscodingEntity(sourceTranscoding, dataSetMapObject),
                    (sourceTranscoding, targetTranscoding) =>
                    CloneTranscodingAndChildren(componentMappingFullObject, targetTranscoding, sourceTranscoding, dataSetMapObject));
            }
        }

        /// <summary>
        /// Clones the transcoding entity.
        /// </summary>
        /// <param name="sourceTranscodingEntity">The source transcoding entity.</param>
        /// <param name="dataSetMapObject">The data set map object.</param>
        /// <returns>
        /// The <see cref="TranscodingEntity" />.
        /// </returns>
        private TranscodingEntity CloneTranscodingEntity(TranscodingEntity sourceTranscodingEntity, DataSetMapObject dataSetMapObject)
        {
            var clone = sourceTranscodingEntity.CreateDeepCopy<TranscodingEntity>();
            if (clone.HasTimeTrascoding() && dataSetMapObject.ColumnMap != null)
            {
                foreach (var timeTranscodingEntity in clone.TimeTranscoding)
                {
                    if (timeTranscodingEntity.DateColumn != null)
                    {
                        timeTranscodingEntity.DateColumn = dataSetMapObject.ColumnMap.GetTargetEntity(timeTranscodingEntity.DateColumn);
                    }

                    if (timeTranscodingEntity.Year?.Column != null)
                    {
                        timeTranscodingEntity.Year.Column = dataSetMapObject.ColumnMap.GetTargetEntity(timeTranscodingEntity.Year.Column);
                    }

                    var periodMapping = timeTranscodingEntity.Period;
                    if (periodMapping != null)
                    {
                        if (periodMapping.Column != null)
                        {
                            periodMapping.Column = dataSetMapObject.ColumnMap.GetTargetEntity(periodMapping.Column);
                        }

                        if (ObjectUtil.ValidCollection((ICollection)periodMapping.Rules))
                        {
                            foreach (var rule in periodMapping.Rules)
                            {
                                if (rule.LocalCodes?.Count == 1)
                                {
                                    rule.LocalCodes[0] = dataSetMapObject.LocalCodeMap.GetTargetEntity(rule.LocalCodes[0]);
                                }
                            }
                        }
                    }
                }
            }

            return clone;
        }

        /// <summary>
        /// Clones the transcoding and children.
        /// </summary>
        /// <param name="componentMappingFullObject">The component mapping full object.</param>
        /// <param name="targetTranscoding">The target transcoding.</param>
        /// <param name="sourceTranscoding">The source transcoding.</param>
        /// <param name="dataSetMapObject">The data set map object.</param>
        private void CloneTranscodingAndChildren(ComponentMappingFullObject componentMappingFullObject, TranscodingEntity targetTranscoding, TranscodingEntity sourceTranscoding, DataSetMapObject dataSetMapObject)
        {
            componentMappingFullObject.Transcoding.Entity = targetTranscoding;
            if (!sourceTranscoding.HasTimeTrascoding())
            {
                _entityCloneHelper.RetrieveCopyAndSave<TranscodingRuleEntity>(
                    sourceTranscoding,
                    targetTranscoding.EntityId,
                    targetTranscoding.StoreId,
                    x => CloneRule(x, dataSetMapObject),
                    (sourceRule, targetRule) => componentMappingFullObject.Transcoding.Rules.Add(targetRule));
            }
        }

        /// <summary>
        /// Clones the rule.
        /// </summary>
        /// <param name="sourceRuleEntity">The source rule entity.</param>
        /// <param name="dataSetMapObject">The data set map object.</param>
        /// <returns>
        /// The <see cref="TranscodingRuleEntity" />.
        /// </returns>
        private TranscodingRuleEntity CloneRule(TranscodingRuleEntity sourceRuleEntity, DataSetMapObject dataSetMapObject)
        {
            var clone = sourceRuleEntity.CreateDeepCopy<TranscodingRuleEntity>();
            if (!ObjectUtil.ValidCollection(sourceRuleEntity.LocalCodes))
            {
                return clone;
            }

            clone.LocalCodes.Clear();
            foreach (var localCodeEntity in sourceRuleEntity.LocalCodes)
            {
                clone.LocalCodes.Add(dataSetMapObject.LocalCodeMap.GetTargetEntity(localCodeEntity));
            }

            return clone;
        }
    }
}