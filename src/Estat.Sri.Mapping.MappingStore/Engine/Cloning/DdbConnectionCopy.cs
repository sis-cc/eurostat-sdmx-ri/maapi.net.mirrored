﻿// -----------------------------------------------------------------------
// <copyright file="DdbConnectionCopy.cs" company="EUROSTAT">
//   Date Created : 2017-07-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Cloning
{
    using System;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// A class responsible for cloning a DDB Connection setting
    /// </summary>
    internal class DdbConnectionCopy
    {
        /// <summary>
        /// The entity clone helper
        /// </summary>
        private readonly EntityCloneHelper _entityCloneHelper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DdbConnectionCopy" /> class.
        /// </summary>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        public DdbConnectionCopy(EntityCloneHelper entityCloneHelper)
        {
            _entityCloneHelper = entityCloneHelper;
        }

        /// <summary>
        /// Clones the dataset.
        /// </summary>
        /// <param name="entityId">
        /// The entity identifier.
        /// </param>
        /// <returns>
        /// The <see cref="IConnectionEntity"/>.
        /// </returns>
        public IConnectionEntity Clone(string entityId)
        {
            var mappingSet = _entityCloneHelper.RetrieveEntities<IConnectionEntity>(entityId).FirstOrDefault();
            if (mappingSet == null)
            {
                return null;
            }

            return Clone(mappingSet);
        }

        /// <summary>
        /// Copies the specified Mapping Set and the children.
        /// </summary>
        /// <param name="sourceIConnectionEntity">The data set.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <returns>
        /// The <see cref="IConnectionEntity"/>.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">dataSet is null</exception>
        public IConnectionEntity Copy(IConnectionEntity sourceIConnectionEntity, string targetStoreId)
        {
            if (sourceIConnectionEntity == null)
            {
                throw new ArgumentNullException(nameof(sourceIConnectionEntity));
            }

            // TODO permission in IConnectionEntity
            return _entityCloneHelper.CopyAndSaveUniqueName(sourceIConnectionEntity, null, targetStoreId);
        }

        /// <summary>
        /// Copies the specified Mapping Set and the children.
        /// </summary>
        /// <param name="sourceConnectionEntity">
        /// The data set.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// dataSet is null
        /// </exception>
        /// <returns>
        /// The <see cref="IConnectionEntity"/>.
        /// </returns>
        public IConnectionEntity Clone(IConnectionEntity sourceConnectionEntity)
        {
            if (sourceConnectionEntity == null)
            {
                throw new ArgumentNullException(nameof(sourceConnectionEntity));
            }

           return _entityCloneHelper.CopyAndSaveUniqueName(sourceConnectionEntity, sourceConnectionEntity.ParentId);
        }
    }
}