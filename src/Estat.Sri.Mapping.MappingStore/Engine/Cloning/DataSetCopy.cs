// -----------------------------------------------------------------------
// <copyright file="DataSetCopy.cs" company="EUROSTAT">
//   Date Created : 2017-07-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine.Cloning
{
    using System;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.Mapping.MappingStore.Model.EntityMapObjects;

    /// <summary>
    /// A class responsible for cloning a DataSet
    /// </summary>
    internal class DataSetCopy
    {
        /// <summary>
        /// The entity clone helper
        /// </summary>
        private readonly EntityCloneHelper _entityCloneHelper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSetCopy" /> class.
        /// </summary>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        public DataSetCopy(EntityCloneHelper entityCloneHelper)
        {
            _entityCloneHelper = entityCloneHelper;
        }

        /// <summary>
        /// Clones the dataset.
        /// </summary>
        /// <param name="entityId">
        /// The entity identifier.
        /// </param>
        /// <returns>
        /// The <see cref="DataSetMapObject"/>.
        /// </returns>
        public DataSetMapObject CloneDataset(string entityId)
        {
            var datasetEntity = _entityCloneHelper.RetrieveEntities<DatasetEntity>(entityId).FirstOrDefault();
            if (datasetEntity == null)
            {
                return null;
            }

            return CloneDataset(datasetEntity);
        }

        /// <summary>
        /// Copies the dataset.
        /// </summary>
        /// <param name="sourceDataSet">The source data set.</param>
        /// <param name="connectionMap">The connection map.</param>
        /// <returns>
        /// The <see cref="DataSetMapObject"/>.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">dataSet is null</exception>
        /// <exception cref="System.ArgumentException">Connection doesn't match</exception>
        public DataSetMapObject CopyDataset(DatasetEntity sourceDataSet, EntityMap<IConnectionEntity> connectionMap)
        {
            if (sourceDataSet == null)
            {
                throw new ArgumentNullException(nameof(sourceDataSet));
            }

            var datasetMapObject = new DataSetMapObject() { SourceEntity = sourceDataSet };

            var targetParentEntity = connectionMap.GetTargetEntity(sourceDataSet.ParentId);
            if (targetParentEntity == null)
            {
                throw new ArgumentException("Connection doesn't match ");
            }

            datasetMapObject.TargetEntity = _entityCloneHelper.CopyAndSaveUniqueName(sourceDataSet, targetParentEntity.EntityId, targetParentEntity.StoreId);

            using (var disableAuth = new SingleRequestScope())
            {
                disableAuth.TrySetAuthorizationState(true);
                disableAuth.RecordOnlyRoot = true;
                _entityCloneHelper.RetrieveCopyAndSave<DataSetColumnEntity>(
                    sourceDataSet,
                    datasetMapObject.TargetEntity.EntityId,
                    datasetMapObject.TargetEntity.StoreId,
                    (sourceEntity, targetEntity) =>
                    {
                        datasetMapObject.ColumnMap.Add(sourceEntity, targetEntity);
                        _entityCloneHelper.RetrieveCopyAndSave<ColumnDescriptionSourceEntity>(sourceEntity, targetEntity.EntityId, targetEntity.StoreId, datasetMapObject.DescriptionSourceMap.Add);
                        //_entityCloneHelper.RetrieveCopyAndSave<LocalCodeEntity>(sourceEntity, targetEntity.EntityId, targetEntity.StoreId, datasetMapObject.LocalCodeMap.Add);
                    });
            }

            return datasetMapObject;
        }

        /// <summary>
        /// Copies the dataset.
        /// </summary>
        /// <param name="dataSet">
        /// The data set.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// dataSet is null
        /// </exception>
        /// <returns>
        /// The <see cref="DataSetMapObject"/>.
        /// </returns>
        public DataSetMapObject CloneDataset(DatasetEntity dataSet)
        {
            if (dataSet == null)
            {
                throw new ArgumentNullException(nameof(dataSet));
            }

            var datasetMapObject = new DataSetMapObject() { SourceEntity = dataSet };

            // we keep the same parent (DDB connection) for the cloned dataset
            datasetMapObject.TargetEntity = _entityCloneHelper.CopyAndSaveUniqueName(dataSet, dataSet.ParentId);

            using (var disableAuth = new SingleRequestScope())
            {
                disableAuth.TrySetAuthorizationState(true);
                disableAuth.RecordOnlyRoot = true;
                _entityCloneHelper.RetrieveCopyAndSave<DataSetColumnEntity>(
                    dataSet,
                    datasetMapObject.TargetEntity.EntityId,
                    (sourceEntity, targetEntity) =>
                        {
                            datasetMapObject.ColumnMap.Add(sourceEntity, targetEntity);
                            _entityCloneHelper.RetrieveCopyAndSave<ColumnDescriptionSourceEntity>(sourceEntity, targetEntity.EntityId, datasetMapObject.DescriptionSourceMap.Add);
                            //_entityCloneHelper.RetrieveCopyAndSave<LocalCodeEntity>(sourceEntity, targetEntity.EntityId, datasetMapObject.LocalCodeMap.Add);
                        });
            }

            return datasetMapObject;
        }
    }
}