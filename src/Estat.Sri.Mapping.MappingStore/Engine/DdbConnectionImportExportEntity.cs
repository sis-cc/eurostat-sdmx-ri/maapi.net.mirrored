using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Extension;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class DdbConnectionImportExportEntity : IImportExportEntity
    {
        public EntityType EntityType => EntityType.DdbConnectionSettings;

        public string RootName { get; set; } = "Connection";

        public void Write(XmlWriter xmlWriter, EntitiesByType entities)
        {
            xmlWriter.WriteStartElement("Connections");
            foreach (var connection in entities.GetEntities(EntityType.DdbConnectionSettings).Cast<DdbConnectionEntity>())
            {
                xmlWriter.WriteStartElement(this.RootName);
                xmlWriter.WriteAttributeString("id",connection.EntityId);
                xmlWriter.WriteAttributeString("type", connection.DbType??"mysql");
                xmlWriter.WriteElementString("Name",connection.Name);
                xmlWriter.WriteElementString("DbUser",connection.DbUser);
                xmlWriter.WriteElementString("DbPassword", connection.Password);
                xmlWriter.WriteElementString("AdoString", connection.AdoConnString);
                xmlWriter.WriteElementString("JdbcString", connection.JdbcConnString);
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
        }

        public List<IEntity> Read(XmlReader xmlReader, string id)
        {
            var element = (XElement) XNode.ReadFrom(xmlReader);
            return new List<IEntity>()
            {
                new DdbConnectionEntity()
                {
                    EntityId = element.Attribute("id")?.Value,
                    DbType = element.Attribute("type")?.Value,
                    Name = element.Descendant("Name")?.Value,
                    //todo the DBName property is populated because it is required by the DB,this should be fixed
                    DbUser = element.Descendant("DbUser")?.Value,
                    Password = element.Descendant("DbPassword")?.Value,
                    AdoConnString = element.Descendant("AdoString")?.Value,
                    JdbcConnString = element.Descendant("JdbcString")?.Value,
                }
            };
        }
    }
}