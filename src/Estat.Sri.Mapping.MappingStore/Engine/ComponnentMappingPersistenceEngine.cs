using System.Collections.Generic;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Data;
    using System.Globalization;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.Utils;
    using Estat.Sri.Utils.Model;
    using static Dapper.SqlMapper;

    public class ComponentMappingPersistenceEngine : EntityPersistenceEngine<ComponentMappingEntity>
    {

        /// <summary>
        /// The SQL Query for getting the component identifier entity identifier map
        /// </summary>
        private const string GetComponentIdEntityIdSqlQuery = @"SELECT C.ID, C.COMP_ID as ENTITY_ID from DATAFLOW D
	    join ARTEFACT A on A.ART_ID = D.DF_ID
		join ENTITY_SDMX_REF ESR on ESR.TARGET_ARTEFACT = A.ART_BASE_ID 
		join N_MAPPING_SET M on M.STR_MAP_SET_ID = ESR.SOURCE_ENTITY_ID
		join COMPONENT C on M.SOURCE_DS = C.DSD_ID
		where M.STR_MAP_SET_ID = {0}";

        /// <summary>
        /// The SQL Query for getting the column name entity identifier map
        /// </summary>
        private const string GetColumnNameEntityIdSqlQuery = @"select C.NAME as ID, C.COL_ID as ENTITY_ID
from DATASET_COLUMN C
where
EXISTS ( SELECT M.MAP_SET_ID from MAPPING_SET M where M.DS_ID = C.DS_ID and M.MAP_SET_ID = {0})";

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMappingPersistenceEngine" /> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        /// <param name="updateInfoFactoryManager">The update information factory manager.</param>
        /// <param name="mappingStoreIdentifier"></param>
        /// <param name="entityPropertiesExtractor"></param>
        /// <param name="commmandsFromUpdateInfo"></param>
        public ComponentMappingPersistenceEngine(DatabaseManager databaseManager, UpdateInfoFactoryManager updateInfoFactoryManager, string mappingStoreIdentifier, IEntityPropertiesExtractor entityPropertiesExtractor, ICommmandsFromUpdateInfo commmandsFromUpdateInfo)
            : base(databaseManager, updateInfoFactoryManager, mappingStoreIdentifier, entityPropertiesExtractor, commmandsFromUpdateInfo)
        {
        }

        public override void Delete(string entityId, EntityType entityType)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            long mapId = entityId.AsMappingStoreEntityId();
            database.ExecuteNonQueryFormat("delete from N_TRANSCODING_RULE where MAP_ID = {0}", mapId);
            
            base.Delete(entityId, entityType);
        }

        public override long Add(ComponentMappingEntity componentMappingEntity)
        {
            if (!componentMappingEntity.HasConstantValue && !componentMappingEntity.HasColumns)
            {
                throw new ValidationException($"Component Mapping without constant value or columns is not valid for component '{componentMappingEntity?.Component?.ObjectId}' with parent '{componentMappingEntity.ParentId}'");
            }
            else if (componentMappingEntity.HasConstantValue && componentMappingEntity.HasColumns)
            {
                throw new ValidationException($"Component Mapping with constant value and any dataset columns is not valid for component '{componentMappingEntity?.Component?.ObjectId}' with parent '{componentMappingEntity.ParentId}'");
            }

            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            long mapId;

            var parentId = componentMappingEntity.GetParentId();
            using (var context = new ContextWithTransaction(database))
            {
                if (string.IsNullOrWhiteSpace(componentMappingEntity.Component?.ObjectId))
                {
                    // TODO Component EntityId is no longer needed  , we store the ObjectId directly in the Component Mapping table
                    UpdateComponentInfo(new[] { componentMappingEntity }, context, parentId);
                }

                if (componentMappingEntity.GetColumns() != null && componentMappingEntity.GetColumns().Any(entity => string.IsNullOrWhiteSpace(entity.Name)))
                {
                    // TODO Column EntityId is no longer needed  , we store the Name directly in the Component Mapping table
                    AddMissingColumnEntityId(new[] { componentMappingEntity }, context, parentId);
                }

                var commandDefinitions = this.InsertInformation(DatabaseInformationType.Mapping, new Dictionary<string, object>
                {
                    {nameof(ComponentMappingEntity.Component), componentMappingEntity.Component?.ObjectId},
                    {nameof(ComponentMappingEntity.ParentId), componentMappingEntity.GetParentId()},
                    {nameof(ComponentMappingEntity.IsMetadata), componentMappingEntity.IsMetadata},
                }, componentMappingEntity.EntityId);

                mapId = context.ExecuteCommandsWithReturnId(commandDefinitions);
                var definitions = new List<CommandDefinition>();
                var id = mapId.ToString(CultureInfo.InvariantCulture);
                if (componentMappingEntity.GetColumns() != null && componentMappingEntity.GetColumns().Any())
                {
                    foreach (var dataSetColumnEntity in componentMappingEntity.GetColumns())
                    {
                        definitions.AddRange(this.CreateInsertDatasetColumnCommands(id, dataSetColumnEntity));
                    }
                }

                if (componentMappingEntity.Script != null && componentMappingEntity.Script.Any())
                {
                    foreach (var transcodingScriptEntity in componentMappingEntity.Script)
                    {
                        definitions.AddRange(
                            this.CreateInsertTranscodingScriptCommands(mapId, transcodingScriptEntity));
                    }
                }

                context.ExecuteCommandsWithReturnId(definitions);

                // add constant or default values
                string insertConstOrDefaultValuesSql = "insert into N_MAPPING_CONST_DEFAULT_VALUE (MAP_ID, CONSTANT_OR_DEFAULT) values ({0}, {1})";
                if (componentMappingEntity.ConstantValues.Any())// first check for array values
                {
                    foreach (var constValue in componentMappingEntity.ConstantValues)
                    {
                        context.DatabaseUnderContext.Execute(insertConstOrDefaultValuesSql, 
                            new Int64Parameter(mapId), new SimpleParameter(DbType.String, constValue));
                    }
                }
                else if (!string.IsNullOrWhiteSpace(componentMappingEntity.ConstantValue)) // then for simple value
                {
                    context.DatabaseUnderContext.Execute(insertConstOrDefaultValuesSql, 
                        new Int64Parameter(mapId), new SimpleParameter(DbType.String, componentMappingEntity.ConstantValue));
                }
                if (componentMappingEntity.DefaultValues.Any())// first check for array values
                {
                    foreach (var defaultValue in componentMappingEntity.DefaultValues)
                    {
                        context.DatabaseUnderContext.Execute(insertConstOrDefaultValuesSql,
                            new Int64Parameter(mapId), new SimpleParameter(DbType.String, defaultValue));
                    }
                }
                else if (!string.IsNullOrWhiteSpace(componentMappingEntity.DefaultValue)) // then for simple value
                {
                    context.DatabaseUnderContext.Execute(insertConstOrDefaultValuesSql,
                        new Int64Parameter(mapId), new SimpleParameter(DbType.String, componentMappingEntity.DefaultValue));
                }

                context.Complete();
            }
            return mapId;
        }


        private IEnumerable<CommandDefinition> CreateInsertTranscodingScriptCommands(long transcodingId,
            TranscodingScriptEntity transcodingScriptEntity)
        {
            return this.InsertInformation(DatabaseInformationType.TranscodingScript,
                new Dictionary<string, object>
                {
                    { nameof(TranscodingEntity.ParentId), transcodingId },
                    { nameof(TranscodingScriptEntity.ScriptContent), transcodingScriptEntity.ScriptContent },
                    { nameof(TranscodingScriptEntity.ScriptTile), transcodingScriptEntity.ScriptTile }
                }, transcodingId.ToString());
        }

        private IEnumerable<CommandDefinition> CreateInsertComponentCommands(string mapId, IIdentifiableEntity componentEntity)
        {
            var pathAndValues = new Dictionary<string, object>
            {
                {ComponentMappingComponentDataTableInformations.ComponentId.ModelName, componentEntity.EntityId},
                {nameof(ComponentMappingEntity.EntityId), mapId},
            };
            return this.InsertInformation(DatabaseInformationType.ComponentMappingComponent, pathAndValues, mapId);
        }

        private IEnumerable<CommandDefinition> CreateInsertMetaAttributeCommands(string mapId, string metadataAttributeSdmxId)
        {
            var pathAndValues = new Dictionary<string, object>
            {
                {ComponentMappingMetadataAttributeDataTableInformations.SdmxId.ModelName, metadataAttributeSdmxId},
                {nameof(ComponentMappingEntity.EntityId), mapId},
            };
            return this.InsertInformation(DatabaseInformationType.ComponentMappingMetadataAttribute, pathAndValues, mapId);
        }

        private IEnumerable<CommandDefinition> CreateInsertDatasetColumnCommands(string mapId, DataSetColumnEntity dataSetColumnEntity)
        {
            var pathAndValues = new Dictionary<string, object>
            {
                {ComponentMappingColumnDataTableInformations.ColumnName.ModelName, dataSetColumnEntity.Name},
                {nameof(ComponentMappingEntity.EntityId), mapId},
            };
            return this.InsertInformation(DatabaseInformationType.ComponentMappingColumn, pathAndValues, mapId);
        }

        private IEnumerable<CommandDefinition> InsertInformation(DatabaseInformationType informationType, Dictionary<string, object> pathAndValues, string entityId)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);

            var updateInfoAction = new UpdateInfoAction()
            {
                DataInformationType = informationType,
                SqlStatementType = SqlStatementType.Insert,
                PathAndValues = pathAndValues
            };
            var commandDefinitionManager = new CommandDefinitionManager(new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
            var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType, updateInfoAction.SqlStatementType);
            return commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, database);
        }

        /// <summary>
        /// Adds the missing component entity identifier.
        /// </summary>
        /// <param name="componentMappingEntities">The component mapping entities.</param>
        /// <param name="context">The context.</param>
        /// <param name="mapSetId">The map set identifier.</param>
        /// <exception cref="MissingInformationException">Component without EntityId and without ObjectId</exception>
        /// <exception cref="ResourceNotFoundException">Column not found in the mapping store DB</exception>
        private void UpdateComponentInfo(IEnumerable<ComponentMappingEntity> componentMappingEntities, ContextWithTransaction context, long mapSetId)
        {
            var objects = context.GetIdToEntityIdMap(GetComponentIdEntityIdSqlQuery, mapSetId);
            foreach (var component in componentMappingEntities.Where(entity => entity.Component != null).Select(entity => entity.Component).Where(entity => !string.IsNullOrWhiteSpace(entity.ObjectId)))
            {
                if (string.IsNullOrWhiteSpace(component.ObjectId))
                {
                    component.ObjectId = objects.FirstOrDefault(x => x.Value == long.Parse(component.EntityId)).Key;
                }
                else
                {
                    long entityId;
                    if (objects.TryGetValue(component.ObjectId, out entityId))
                    {
                        component.EntityId = entityId.ToString(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        throw new ResourceNotFoundException($"Component with ID: '{component.ObjectId}' not found");
                    }
                }
            }
        }

        /// <summary>
        /// Adds the missing column entity identifier.
        /// </summary>
        /// <param name="componentMappingEntities">The component mapping entities.</param>
        /// <param name="context">The context.</param>
        /// <param name="mapSetId">The map set identifier.</param>
        /// <exception cref="MissingInformationException">Column without EntityId and without Name</exception>
        /// <exception cref="ResourceNotFoundException">Column not found in the mapping store DB</exception>
        private void AddMissingColumnEntityId(IList<ComponentMappingEntity> componentMappingEntities, ContextWithTransaction context, long mapSetId)
        {
            var objects = context.GetIdToEntityIdMap(GetColumnNameEntityIdSqlQuery, mapSetId);
            foreach (var columnEntity in componentMappingEntities.Where(entity => entity.GetColumns() != null).SelectMany(entity => entity.GetColumns()).Where(entity => string.IsNullOrWhiteSpace(entity.EntityId)))
            {
                if (string.IsNullOrWhiteSpace(columnEntity.Name))
                {
                    throw new MissingInformationException("Column without EntityId and without Name");
                }

                long entityId;
                if (objects.TryGetValue(columnEntity.Name, out entityId))
                {
                    columnEntity.EntityId = entityId.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    throw new ResourceNotFoundException($"Column with Name: '{columnEntity.Name}' not found");
                }
            }
        }
        internal void DeleteChildren(long mappingSetPk, ContextWithTransaction context)
        {
            context.DatabaseUnderContext.ExecuteNonQueryFormat("delete from N_TRANSCODING_RULE where MAP_ID in (select MAP_ID from N_COMPONENT_MAPPING where STR_MAP_SET_ID = {0})", mappingSetPk);
            context.DatabaseUnderContext.ExecuteNonQueryFormat("delete from N_COMPONENT_MAPPING where STR_MAP_SET_ID = {0}", mappingSetPk);
        }
    }
}