using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStore.Store.Engine;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class TemplateMappingRetriever : IEntityRetrieverEngine<TemplateMapping>
    {
        private readonly Database _databaseManager;
        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;
        

        public TemplateMappingRetriever(Database databaseManager)
        {
            this._databaseManager = databaseManager;
            this._fieldsToReturn = new Dictionary<Detail, IList<string>>
            {
                {
                    Detail.Full, new[]
                    {
                        "ENTITY_ID as " + nameof(TemplateMapping.EntityId), "COLUMN_NAME as " + nameof(TemplateMapping.ColumnName), "COL_DESC as " + nameof(TemplateMapping.ColumnDescription),
                        "COMPONENT_ID as " + nameof(TemplateMapping.ComponentId), "COMPONENT_TYPE as " + nameof(TemplateMapping.ComponentType),
                    }
                },
                {Detail.IdOnly, new[] { "ENTITY_ID as " + nameof(ColumnDescriptionSourceEntity.EntityId)}}
            };
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<TemplateMapping> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var db = this._databaseManager;
            var whereClauseBuilder = new WhereClauseBuilder(db);

            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "ENTITY_ID"));
            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3}", fields, "N_TEMPLATE_MAPPING", whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);
            var templateMappings = db.Query<TemplateMapping>(sqlQuery, param).ToArray();
            if (templateMappings.Length > 0)
            {
                //UpdateConceptUrn(templateMappings);
                //UpdateItemScheme(templateMappings, db);
                UpdateTranscodings(templateMappings, db);
                UpdateTimeTranscodings(templateMappings, db);
            }

            return templateMappings;
        }

        private void UpdateConceptUrn(IEnumerable<TemplateMapping> templateMappings)
        {
           var retriverEngine = new MaintainableRefRetrieverEngine(this._databaseManager, new MappingStoreRetrievalManager(this._databaseManager));
           var urnMap = retriverEngine.RetrievesUrnMap(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept));
           foreach (var templateMapping in templateMappings)
           {
                templateMapping.ConceptUrn = urnMap[templateMapping.ConceptId];
           }
        }

        private void UpdateItemScheme(IEnumerable<TemplateMapping> templateMappings, Database db)
        {
            var ids = templateMappings.Select(item => item.EntityId).Aggregate((i, j) => i + "," + j).TrimEnd(',');
            var sqlString = $"SELECT ENTITY_ID,ITEM_SCHEME_ID,A.AGENCY,A.ID,A.VERSION1,A.VERSION2,A.VERSION3 from N_TEMPLATE_MAPPING TM INNER JOIN ARTEFACT A ON TM.ITEM_SCHEME_ID = A.ART_ID where ENTITY_ID in ({ids})";
            var rows = db.Query<dynamic>(sqlString).ToList();
            foreach (var templateMapping in templateMappings)
            {
                var row = rows.First(item => item.ENTITY_ID.ToString() == templateMapping.EntityId);
                var version = string.Join(".", new[] { row.VERSION1, row.VERSION2, row.VERSION3 }.Where(l => l != null).Select(l => l.ToString(CultureInfo.InvariantCulture)));
                templateMapping.ItemSchemeId = new StructureReferenceImpl(row.AGENCY, row.ID, version, SdmxStructureEnumType.CodeList);
            }
        }

        private void UpdateTranscodings(IEnumerable<TemplateMapping> templateMappings, Database db)
        {
            var ids = templateMappings.Select(x => x.EntityId).Aggregate((i, j) => i + "," + j).TrimEnd(',');
            var sqlString = $"SELECT * from TM_TRANSCODING WHERE TEMPLATE_ID IN ({ids})";
            var rows = db.Query<dynamic>(sqlString).ToList();
            foreach (var templateMapping in templateMappings)
            {
                templateMapping.Transcodings = new Dictionary<string, string>();
                foreach (var row in rows.Where(x => x.TEMPLATE_ID.ToString() == templateMapping.EntityId))
                {
                    templateMapping.Transcodings.Add(row.LOCAL_CODE, row.DSD_CODE);
                }
            }
        }

        private void UpdateTimeTranscodings(IEnumerable<TemplateMapping> templateMappings, Database db)
        {
            var ids = templateMappings.Select(x => x.EntityId).Aggregate((i, j) => i + "," + j).TrimEnd(',');
            var sqlString = $"SELECT * from TM_TIME_TRANSCODING WHERE TEMPLATE_ID IN ({ids})";
            var rows = db.Query<dynamic>(sqlString).ToList();
            foreach (var templateMapping in templateMappings)
            {
                templateMapping.TimeTranscoding = new List<TimeTranscodingEntity>();
                foreach (var row in rows.Where(x => x.TEMPLATE_ID.ToString() == templateMapping.EntityId))
                {
                    var timeTranscodingEntity = new TimeTranscodingEntity();
                    timeTranscodingEntity.IsDateTime = Convert.ToBoolean(row.IS_DATE);
                    timeTranscodingEntity.Frequency = row.FREQ;
                    timeTranscodingEntity.Period = new PeriodTimeTranscoding()
                    {
                        Start = (int?)row.P_START,
                        Length = (int?)row.P_LENGTH
                    };

                    timeTranscodingEntity.Year = new TimeTranscoding()
                    {
                        Start = (int?)row.Y_START,
                        Length = (int?)row.Y_LENGTH
                    };

                    templateMapping.TimeTranscoding.Add(timeTranscodingEntity);
                }
            }
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }
    }
}