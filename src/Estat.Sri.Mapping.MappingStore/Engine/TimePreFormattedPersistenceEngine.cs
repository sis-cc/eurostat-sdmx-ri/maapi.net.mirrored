// -----------------------------------------------------------------------
// <copyright file="TimePreFormattedPersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2021-05-25
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Mapping.Api.Exceptions;
    using System.Data;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Dapper;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.Utils.Model;
    using Estat.Sri.Mapping.MappingStore.Builder;
    using System.Globalization;
    using Estat.Sri.Mapping.MappingStore.DatabaseModel;
    using Estat.Sri.Utils;
    using Estat.Sri.MappingStore.Store.Extension;

    /// <summary>
    /// Responsible to persist the <see cref="TimePreFormattedEntity"/>.
    /// </summary>
    public class TimePreFormattedPersistenceEngine : EntityPersistenceEngine<TimePreFormattedEntity>
    {
        /// <summary>
        /// Initializes a new instance of the class <see cref="TimePreFormattedPersistenceEngine"/>.
        /// </summary>
        /// <param name="databaseManager"></param>
        /// <param name="updateInfoFactoryManager"></param>
        /// <param name="mappingStoreIdentifier"></param>
        /// <param name="entityPropertiesExtractor"></param>
        /// <param name="commmandsFromUpdateInfo"></param>
        public TimePreFormattedPersistenceEngine(
            DatabaseManager databaseManager, 
            UpdateInfoFactoryManager updateInfoFactoryManager, 
            string mappingStoreIdentifier, 
            IEntityPropertiesExtractor entityPropertiesExtractor, 
            ICommmandsFromUpdateInfo commmandsFromUpdateInfo) 
                : base(databaseManager, updateInfoFactoryManager, mappingStoreIdentifier, entityPropertiesExtractor, commmandsFromUpdateInfo)
        {
        }

        /// <summary>
        /// Insert or update multiple entities of <see cref="TimePreFormattedEntity"/>.
        /// </summary>
        /// <param name="entities">The collection of entities to insert or update.</param>
        /// <returns>The collection of primary keys for the <see cref="TimePreFormattedEntity"/>.</returns>
        public override IEnumerable<long> Add(IList<TimePreFormattedEntity> entities)
        {
            var result = new List<long>();
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            using (var context = new ContextWithTransaction(database))
            {
                Database transactionDb = context.DatabaseUnderContext;
                EntityIdFromUrn entityIdFromUrn = new EntityIdFromUrn(transactionDb);

                foreach (var timePreFormatted in entities)
                {
                    result.Add(PerstistChanges(timePreFormatted, transactionDb));
                }

                context.Complete();
            }

            return result;
        }

        /// <summary>
        /// Persist a <see cref="TimePreFormattedEntity"/>.
        /// </summary>
        /// <param name="entity">The entity to persist.</param>
        /// <returns>The entity primary key.</returns>
        public override long Add(TimePreFormattedEntity entity)
        {
            return this.Add(new[] { entity }).First();
        }

        /// <summary>
        /// Persist a <see cref="TimePreFormattedEntity"/> using an open transaction.
        /// </summary>
        /// <param name="entity">The entity to persist.</param>
        /// <param name="transactionDb">the transaction to use.</param>
        /// <returns>The entity primary key</returns>
        public long Add(TimePreFormattedEntity entity, Database transactionDb)
        {
            return PerstistChanges(entity, transactionDb);
        }

        private long PerstistChanges(TimePreFormattedEntity entity, Database transactionDb)
        {
            bool requestEntityExists = RequestEntityExists(entity);
            long mappingSetEntityId = entity.GetEntityId();

            var existingTimePreFormatted = GetExistingTimePreFormatted(transactionDb, entity.EntityId);
            bool recordExists = existingTimePreFormatted != null;

            if (!recordExists && requestEntityExists)
            {
                // insert new record
                transactionDb.UsingLogger().ExecuteNonQueryFormat(
                    "insert into N_MAPPING_TIME_DIMENSION (STR_MAP_SET_ID, OUTPUT_COLUMN, FROM_SOURCE, TO_SOURCE) VALUES ({0}, {1}, {2}, {3})",
                    transactionDb.CreateInParameter("mappingSetEntityId", DbType.Int64, mappingSetEntityId),
                    transactionDb.CreateInParameter("outputColumnParam", DbType.Int64, entity.OutputColumn.EntityId),
                    transactionDb.CreateInParameter("fromColumnParam", DbType.Int64, entity.FromColumn.EntityId),
                    transactionDb.CreateInParameter("toColumnParam", DbType.Int64, entity.ToColumn.EntityId));
            }
            else if (recordExists && !requestEntityExists)
            {
                // delete record
                var entityId = new Int64Parameter(mappingSetEntityId);
                transactionDb.UsingLogger().Execute("DELETE FROM TIME_PRE_FORMATED WHERE STR_MAP_SET_ID = {0}", entityId);
            }
            if (recordExists && requestEntityExists && IsChanged(entity, existingTimePreFormatted))
            {
                // update record
                transactionDb.UsingLogger().ExecuteNonQueryFormat(
                    "update N_MAPPING_TIME_DIMENSION set OUTPUT_COLUMN = {0}, FROM_SOURCE = {1}, TO_SOURCE = {2} where STR_MAP_SET_ID = {3}",
                    transactionDb.CreateInParameter("outputColumnParam", DbType.Int64, entity.OutputColumn.EntityId),
                    transactionDb.CreateInParameter("fromColumnParam", DbType.Int64, entity.FromColumn.EntityId),
                    transactionDb.CreateInParameter("toColumnParam", DbType.Int64, entity.ToColumn.EntityId),
                    transactionDb.CreateInParameter("mappingSetEntityId", DbType.Int64, mappingSetEntityId));
            }

            return mappingSetEntityId;
        }

        /// <summary>
        /// Validates the <paramref name="requestEntity"/> and returns if has value or empty.
        /// Will throw exceptions if not valid.
        /// </summary>
        /// <param name="requestEntity">|The entity to check.</param>
        /// <returns><c>true</c> if <paramref name="requestEntity"/> is valid and has values, <c>false</c>, if is valid but empty.</returns>
        private bool RequestEntityExists(TimePreFormattedEntity requestEntity)
        {
            if (requestEntity == null)
            {
                throw new ArgumentNullException("time-pre-formatted entity should not be null");
            }
            if (requestEntity.EntityId == null)
            {
                throw new MissingInformationException("Mapping Set Id not defined");
            }

            int valuesFound = 0;
            if (requestEntity.OutputColumn != null && requestEntity.OutputColumn.EntityId != null)
            {
                valuesFound++;
            }
            if (requestEntity.FromColumn != null && requestEntity.FromColumn.EntityId != null)
            {
                valuesFound++;
            }
            if (requestEntity.ToColumn != null && requestEntity.ToColumn.EntityId != null)
            {
                valuesFound++;
            }

            if (valuesFound == 3)
            {
                return true;
            }
            else if(valuesFound == 0)
            {
                return false;
            }
            else
            {
                throw new MissingInformationException("All columns should have a value.");
            }
        }

        private MATimePreFormattedEntity GetExistingTimePreFormatted(Database transactionDb, string mappingSetId)
        {
            WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder(transactionDb);
            var criteria = new Criteria<long>(OperatorType.Exact, mappingSetId.AsMappingStoreEntityId());
            WhereClauseParameters whereClauseParameters = whereClauseBuilder.Build(criteria, "STR_MAP_SET_ID");
            var sqlQuery = string.Format(CultureInfo.InvariantCulture,
                "select STR_MAP_SET_ID as MappingSetId, OUTPUT_COLUMN as OutputColumnId, FROM_SOURCE as FromColumnId, TO_SOURCE as ToColumnId " +
                "from N_MAPPING_TIME_DIMENSION where {0}", whereClauseParameters.WhereClause);
            var valuePairs = whereClauseParameters.Parameters.ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);

            return transactionDb.UsingLogger().QueryUsingInternalDb<MATimePreFormattedEntity>(sqlQuery, param).SingleOrDefault();
        }

        private bool IsChanged(TimePreFormattedEntity requestEntity, MATimePreFormattedEntity existingEntity)
        {
            if (requestEntity == null || existingEntity == null)
            {
                return false;
            }
            if (requestEntity.EntityId != existingEntity.MappingSetId.ToString())
            {
                throw new InvalidOperationException("The TimePreFormatted entity doesn't match the provided mapping set.");
            }
            if (requestEntity.OutputColumn.EntityId != existingEntity.OutputColumnId.ToString())
            {
                return true;
            }
            if (requestEntity.FromColumn.EntityId != existingEntity.FromColumnId.ToString())
            {
                return true;
            }
            if (requestEntity.ToColumn.EntityId != existingEntity.ToColumnId.ToString())
            {
                return true;
            }
            return false;
        }
    }
}
