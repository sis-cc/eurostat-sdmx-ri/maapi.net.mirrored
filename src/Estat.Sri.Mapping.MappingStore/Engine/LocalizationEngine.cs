// -----------------------------------------------------------------------
// <copyright file="LocalizationEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStore.Store.Extension;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class LocalizationEngine : ILocalizationEngine
    {
        private readonly DatabaseManager _databaseManager;
        private readonly string _mappingStoreId;

        public LocalizationEngine(DatabaseManager databaseManager, string mappingStoreId)
        {
            this._databaseManager = databaseManager;
            this._mappingStoreId = mappingStoreId;
        }

        public IList<string> GetLanguages()
        {
            var db = this._databaseManager.GetDatabase(this._mappingStoreId);
            return db.Query<dynamic>("SELECT DISTINCT LANGUAGE FROM LOCALISED_STRING").Select(item => (string) item.LANGUAGE).ToList();
        }
    }
}