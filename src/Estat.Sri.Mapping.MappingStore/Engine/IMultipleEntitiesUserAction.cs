﻿using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public interface IMultipleEntitiesUserAction
    {
        IList<IEntity> GetEntities(IList<IEntity> entities,string storeId);
    }

    public class MultipleEntitiesUserAction : IMultipleEntitiesUserAction
    {
        readonly IEntityRetrieverManager _entityRetrieverManager;

        public MultipleEntitiesUserAction(IEntityRetrieverManager entityRetrieverManager)
        {
            this._entityRetrieverManager = entityRetrieverManager;
        }

        public IList<IEntity> GetEntities(IList<IEntity> entities, string storeId)
        {
            var typeOfEntity = entities.First().TypeOfEntity;
            switch (typeOfEntity)
            {
                case EntityType.DataSetColumn:
                case EntityType.Mapping: 
                    return entities.GroupBy(x=>x.ParentId).Select(@group => @group.First()).ToList();
                case EntityType.DescSource:
                case EntityType.LocalCode:
                    return this.GroupByDatasetId(entities,storeId);
                case EntityType.Transcoding:
                    return this.GroupByMappingSetId(entities.Cast<TranscodingEntity>().ToList(), storeId);
                case EntityType.TranscodingRule:
                    return this.GroupByMappingSetId(entities.Cast<TranscodingRuleEntity>().ToList(), storeId);
            }
            return entities;
        }


        private IList<IEntity> GroupByMappingSetId(IList<TranscodingRuleEntity> entities, string storeId)
        {
            var values = new Dictionary<string, IEntity>();
            foreach (var entity in entities)
            {
                var transcoding = this.GetTranscoding(entity.ParentId, storeId);
                var componentMapping = this.GetComponentMapping(transcoding.ParentId, storeId);
                var mappingSet = this.GetMappingSet(componentMapping.ParentId, storeId);
                if (!values.ContainsKey(mappingSet.ParentId))
                {
                    values.Add(mappingSet.EntityId, entity);
                }
            }
            return values.Values.ToList();
        }

        private IList<IEntity> GroupByMappingSetId(IList<TranscodingEntity> entities, string storeId)
        {
            var values = new Dictionary<string, IEntity>();
            foreach (var entity in entities)
            {
                var componentMapping = this.GetComponentMapping(entity.ParentId, storeId);
                var mappingSet = this.GetMappingSet(componentMapping.ParentId, storeId);
                if (!values.ContainsKey(mappingSet.ParentId))
                {
                    values.Add(mappingSet.EntityId, entity);
                }
            }
            return values.Values.ToList();
        }

        private IList<IEntity> GroupByDatasetId(IList<IEntity> entities, string storeId)
        {
            var values = new Dictionary<string, IEntity>();
            foreach (var entity in entities)
            {
                var dataSetColumnEntity = this.GetDatasetColumn(entity.ParentId,storeId);
                var datasetEntity = this.GetDataset(dataSetColumnEntity.ParentId,storeId);
                if (!values.ContainsKey(datasetEntity.ParentId))
                {
                    values.Add(datasetEntity.EntityId,entity);
                }
            }
            return values.Values.ToList();
        }

        private DataSetColumnEntity GetDatasetColumn(string entityId, string storeId)
        {
            return this._entityRetrieverManager.GetEntities<DataSetColumnEntity>(storeId, new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) }, Detail.Full).FirstOrDefault();
        }

        private DatasetEntity GetDataset(string entityId, string storeId)
        {
            return this._entityRetrieverManager.GetEntities<DatasetEntity>(storeId, new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) }, Detail.Full).FirstOrDefault();
        }

        private TranscodingEntity GetTranscoding(string entityId, string storeId)
        {
            return this._entityRetrieverManager.GetEntities<TranscodingEntity>(storeId, new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) }, Detail.Full).FirstOrDefault();
        }

        private ComponentMappingEntity GetComponentMapping(string entityId, string storeId)
        {
            return this._entityRetrieverManager.GetEntities<ComponentMappingEntity>(storeId, new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) }, Detail.Full).FirstOrDefault();
        }

        private MappingSetEntity GetMappingSet(string entityId, string storeId)
        {
            return this._entityRetrieverManager.GetEntities<MappingSetEntity>(storeId, new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, entityId) }, Detail.Full).FirstOrDefault();
        }
    }
}