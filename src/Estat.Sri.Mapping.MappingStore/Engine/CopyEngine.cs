// -----------------------------------------------------------------------
// <copyright file="CopyEngine.cs" company="EUROSTAT">
//   Date Created : 2017-09-11
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net;
    using System.Security.Cryptography;
    using System.Text;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Extension;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Engine.Cloning;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.Mapping.MappingStore.Model.EntityMapObjects;
    using Estat.Sri.MappingStore.Store;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Persist;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    using MappingStoreManager = Estat.Sri.MappingStore.Store.Manager.MappingStoreManager;

    /// <summary>
    /// The SDMX RI implementation of the copy engine
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Engine.ICopyEngine" />
    public class CopyEngine : ICopyEngine
    {
        /// <summary>
        /// The source store identifier
        /// </summary>
        private readonly string _sourceStoreId;

        /// <summary>
        /// The entity persistence manager
        /// </summary>
        private readonly IEntityPersistenceManager _entityPersistenceManager;

        /// <summary>
        /// The entity retriever manager
        /// </summary>
        private readonly IEntityRetrieverManager _entityRetrieverManager;

        /// <summary>
        /// The database manager
        /// </summary>
        private readonly DatabaseManager _databaseManager;
        private readonly ICommonSdmxObjectRetrievalFactory _commonSdmxObjectRetrievalFactory;

        /// <summary>
        /// The source SDMX retrieval manager
        /// </summary>
        private readonly ICommonSdmxObjectRetrievalManager _sourceSdmxRetrievalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="CopyEngine"/> class.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="entityPersistenceManager">The entity persistence manager.</param>
        /// <param name="entityRetrieverManager">The entity retriever manager.</param>
        /// <param name="database">The database.</param>
        /// <param name="databaseManager">The database manager.</param>
        public CopyEngine(string sourceStoreId, IEntityPersistenceManager entityPersistenceManager, IEntityRetrieverManager entityRetrieverManager, Database database, DatabaseManager databaseManager,ICommonSdmxObjectRetrievalFactory commonSdmxObjectRetrievalFactory)
        {
            this._sourceStoreId = sourceStoreId;
            this._entityPersistenceManager = entityPersistenceManager;
            this._entityRetrieverManager = entityRetrieverManager;
            _databaseManager = databaseManager;
            this._commonSdmxObjectRetrievalFactory = commonSdmxObjectRetrievalFactory;
            _sourceSdmxRetrievalManager = GetRetrievalManager(database);
        }

        /// <summary>
        /// Copy everything (including SDMX) from the sourceStoreId to targetStoreId
        /// </summary>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <returns>
        /// The result of the action
        /// </returns>
        /// <exception cref="BadRequestException">The source and target database can not be the same</exception>
        /// <remarks>
        /// Cannot copy user if it exists
        /// Cannot copy header if the dataflow already has a header
        /// Cannot copy MappingSet if the dataflow already has a mapping set
        /// </remarks>
        public IActionResult Copy(string targetStoreId)
        {
            if (_sourceStoreId != targetStoreId)
            {
                var sb = new StringBuilder();
                var artefactImportStatuses = this.CopySdmx(targetStoreId);
                var entityResults = CopyEntities(_sourceStoreId, targetStoreId);

                var exceptions = entityResults.Item2;
                sb.Append(string.Join("\n", entityResults.Item1));

                var numberOfArtefactsImported = artefactImportStatuses.Select(x => x.ImportMessage.Status == ImportMessageStatus.Success).Count();
                sb.AppendLine($"Imported {numberOfArtefactsImported} SDMX artifacts");
                if (exceptions.Any())
                {
                    return new ActionResult("199", StatusType.Warning, exceptions, sb.ToString());
                }

                return new ActionResult(HttpStatusCode.OK.ToString(), StatusType.Success, sb.ToString());
            }
            else
            {
                throw new BadRequestException("The source and target database can not be the same");
            }
        }

        /// <summary>
        /// Copy the selected entities from the sourceStoreId to targetStoreId
        /// </summary>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="entityType">Type of the entity. The entity can be one following entities
        /// <see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.DdbConnectionSettings" />, <see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.DataSet" /> and <see cref="T:Estat.Sri.Mapping.Api.Model.RegistryEntity" /><see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.MappingSet" />, <see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.User" /> and <see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.Header" /></param>
        /// <param name="entityQuery">The entity query.</param>
        /// <returns>
        /// The result of the action
        /// </returns>
        /// <remarks>
        /// Parents will be copied including SDMX dependencies for Mapping Sets. Some restrictions exist.
        /// Cannot copy user if it exists
        /// Cannot copy header if the dataflow already has a header
        /// Cannot copy MappingSet if the dataflow already has a mapping set
        /// </remarks>
        public IActionResult Copy(string targetStoreId, EntityType entityType, IEntityQuery entityQuery)
        {
            var copiedEntities = this.CopyEntities(_sourceStoreId, targetStoreId, entityType, entityQuery);
            if (copiedEntities.Item2.Any())
            {
                return new ActionResult("199", StatusType.Warning, copiedEntities.Item2, copiedEntities.Item1);
            }

            return new ActionResult(HttpStatusCode.OK.ToString(), StatusType.Success, copiedEntities.Item1);
        }

        /// <summary>
        /// Copy the selected entities within the same store id
        /// </summary>
        /// <param name="entityType">Type of the entity. The entity can be one following entities <see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.DdbConnectionSettings" />, <see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.DataSet" /> and <see cref="T:Estat.Sri.Mapping.Api.Model.RegistryEntity" /></param>
        /// <param name="entityQuery">The entity query.</param>
        /// <param name="copyParents">if set to <c>true</c> copy parents. Currently it only applies to <see cref="T:Estat.Sri.Mapping.Api.Model.DatasetEntity" /></param>
        /// <returns>
        /// The result of the action
        /// </returns>
        public IActionResult Copy(EntityType entityType, IEntityQuery entityQuery, bool copyParents)
        {
            var storeId = _sourceStoreId;
            var exceptions = new List<Exception>();
            var parentCopyResult = string.Empty;
            Tuple<string, List<Exception>> copyEntities = null;
            try
            {
                copyEntities = this.CopyEntities(storeId, entityType, entityQuery, copyParents);
            }
            catch (SystemException)
            {
                throw;
            }
            catch (Exception e)
            {
                exceptions.Add(e);
            }

            if (exceptions.Any())
            {
                return new ActionResult("199", StatusType.Warning, exceptions, copyEntities?.Item1 + parentCopyResult);
            }

            return new ActionResult(HttpStatusCode.OK.ToString(), StatusType.Success, copyEntities?.Item1 + parentCopyResult);
        }

        /// <summary>
        /// Copies the header.
        /// </summary>
        /// <param name="sourceDataflowUrn">
        /// The source dataflow urn.
        /// </param>
        /// <param name="targetDataflowUrn">
        /// The target dataflow urn.
        /// </param>
        /// <exception cref="ResourceConflictException">
        /// Target dataflow already has a header. Remove first and then copy
        /// </exception>
        /// <exception cref="ResourceNotFoundException">
        /// Header with the specified dataflow could not be found
        /// </exception>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        public IActionResult CopyHeader(Uri sourceDataflowUrn, Uri targetDataflowUrn)
        {
            var exceptions = new List<Exception>();
            var message = string.Empty;
            try
            {
                var targetDataflowAsParentId = targetDataflowUrn.ToString();
                var sourceDataflowAsParentId = sourceDataflowUrn.ToString();
                var existingHeaderOnTargetDataflow = _entityRetrieverManager.GetEntities<HeaderEntity>(
                    _sourceStoreId,
                    targetDataflowAsParentId.QueryForThisParentId(),
                    Detail.Full).FirstOrDefault();
                if (existingHeaderOnTargetDataflow != null)
                {
                    throw new ResourceConflictException("Target dataflow already has a header. Remove first and then copy");
                }

                ICommonStructureQuery commonQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.Other, null)
                    .SetStructureIdentification(new StructureReferenceImpl(targetDataflowUrn))
                    .SetMaintainableTarget(SdmxStructureEnumType.Dataflow)
                    .Build();
                var dataflow = _sourceSdmxRetrievalManager.GetMaintainables(commonQuery).Dataflows.FirstOrDefault();
                if (dataflow == null)
                {
                    throw new SdmxNoResultsException("Target dataflow doesn't exist");
                }

                var header = _entityRetrieverManager.GetEntities<HeaderEntity>(
                    _sourceStoreId,
                    sourceDataflowAsParentId.QueryForThisParentId(),
                    Detail.Full).FirstOrDefault();
                if (header == null)
                {
                    throw new ResourceNotFoundException("Header with the specified dataflow could not be found");
                }

                var clone = header.CreateDeepCopy<HeaderEntity>();
                clone.StoreId = _sourceStoreId;
                clone.ParentId = targetDataflowAsParentId;
                _entityPersistenceManager.Add(clone);
                message = CreateResultMessage(1, 1, EntityType.Header);
            }
            catch (SystemException)
            {
                throw;
            }
            catch (Exception e)
            {
                exceptions.Add(e);
            }

            if (exceptions.Any())
            {
                return new ActionResult("199", StatusType.Warning, exceptions, message);
            }

            return new ActionResult(HttpStatusCode.OK.ToString(), StatusType.Success, message);
        }

        /// <summary>
        /// Copies the dataflow.
        /// </summary>
        /// <param name="targetMappingStore">The target mapping store.</param>
        /// <param name="dataflowUrn">The dataflow urn.</param>
        /// <param name="targetRetrievalManager">The target retrieval manager.</param>
        private void CopyDataflow(
            ConnectionStringSettings targetMappingStore,
            Uri dataflowUrn,
            ICommonSdmxObjectRetrievalManager targetRetrievalManager)
        {
            // we convert the urn to IStructureReference
            var dataflowReference = new StructureReferenceImpl(dataflowUrn);

            ICommonStructureQuery commonQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, null)
                .SetStructureIdentification(new StructureReferenceImpl(dataflowReference))
                .SetMaintainableTarget(SdmxStructureEnumType.Dataflow)
                .Build();

            // then check if the dataflow already exists
            var targetExists = targetRetrievalManager.GetMaintainables(commonQuery).Dataflows.FirstOrDefault();
            if (targetExists != null)
            {
                // we don't need to copy. it exists
                return;
            }

            // since it doesn't exist we get the dataflow and its descendants

            // so first we build the query

            ICommonStructureQuery commonQueryDesc = CommonStructureQueryCore.Builder
                   .NewQuery(CommonStructureQueryType.Other, null)
                   .SetStructureIdentification(new StructureReferenceImpl(dataflowReference))
                   .SetReferences(StructureReferenceDetailEnumType.Descendants)
                   .SetMaintainableTarget(SdmxStructureEnumType.Dataflow)
                   .Build();
            // and the we retrieve
            var sdmxOjects = _sourceSdmxRetrievalManager.GetMaintainables(commonQueryDesc);

            // finally we store the sdmxObjects in the target DB
            var persistenceManager = GetPersistManager(targetMappingStore, new List<ArtefactImportStatus>());
            persistenceManager.SaveStructures(sdmxOjects);
        }

        /// <summary>
        /// Copies the entities.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <returns>The list of messages and exceptions</returns>
        private Tuple<List<string>, List<Exception>> CopyEntities(string sourceStoreId, string targetStoreId)
        {
            var exceptions = new List<Exception>();
            var messages = new List<string>();
            IEntityQuery query = EntityQuery.Empty;

            // Copy Users (The id must be unique) 
            messages.Add(this.CopyUser(sourceStoreId, targetStoreId, exceptions, query));

            // we specify the source store id here and use the methods that allow to specify the target id
            var entityCloneHelper = new EntityCloneHelper(this._entityRetrieverManager, this._entityPersistenceManager, sourceStoreId);

            // Copy DDB Connection (with Permissions depends on Users)
            var ddbConnectionMap = new EntityMap<IConnectionEntity>();
            messages.Add(this.CopyDdbConnection(sourceStoreId, targetStoreId, entityCloneHelper, ddbConnectionMap, exceptions, query));

            // Copy DataSet and children (depends on above depends on Users)
            var datasetMap = new Dictionary<string, DataSetMapObject>();

            messages.Add(this.CopyDataSet(sourceStoreId, entityCloneHelper, ddbConnectionMap, datasetMap, exceptions, query));

            // Copy MappingSet and children (depends on above and SDMX). Only if the target dataflow doesn't have already a mapping set
            messages.Add(this.CopyMappingSet(sourceStoreId, entityCloneHelper, datasetMap, exceptions, query));

            // Copy Registry
            messages.Add(this.CopyRegistry(sourceStoreId, targetStoreId, exceptions, entityCloneHelper, query));

            // Copy Nsiws
            messages.Add(this.CopyNsiws(sourceStoreId, targetStoreId, exceptions, entityCloneHelper, query));
            // copy header *only* if the target dataflow doesn't have already a header
            messages.Add(this.CopyHeaderTemplate(sourceStoreId, targetStoreId, exceptions, query));

            return Tuple.Create(messages, exceptions);
        }

        /// <summary>
        /// Copies the mapping set.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        /// <param name="datasetMap">The dataset map.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="query">The query.</param>
        /// <returns>
        /// The result
        /// </returns>
        private string CopyMappingSet(string sourceStoreId, EntityCloneHelper entityCloneHelper, IReadOnlyDictionary<string, DataSetMapObject> datasetMap, ICollection<Exception> exceptions, IEntityQuery query)
        {
            var mappingSets = this._entityRetrieverManager.GetEntities<MappingSetEntity>(sourceStoreId, query, Detail.Full).ToArray();
            return CopyMappingSet(entityCloneHelper, datasetMap, exceptions, mappingSets);
        }

        /// <summary>
        /// Copies the mapping set.
        /// </summary>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        /// <param name="datasetMap">The dataset map.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="mappingSets">The mapping sets.</param>
        /// <returns>
        /// The result
        /// </returns>
        private static string CopyMappingSet(
            EntityCloneHelper entityCloneHelper,
            IReadOnlyDictionary<string, DataSetMapObject> datasetMap,
            ICollection<Exception> exceptions,
            MappingSetEntity[] mappingSets)
        {
            var mappingSetCopy = new MappingSetCopy(entityCloneHelper);
            var succesfulEntitiesCopied = 0;
            foreach (var entity in mappingSets)
            {
                try
                {
                    DataSetMapObject dataSetMapObject;
                    if (!string.IsNullOrWhiteSpace(entity.DataSetId)
                        && datasetMap.TryGetValue(entity.DataSetId, out dataSetMapObject))
                    {
                        mappingSetCopy.CopyMappingSet(entity, dataSetMapObject);
                        succesfulEntitiesCopied++;
                    }
                    else
                    {
                        mappingSetCopy.CopyMappingSet(entity, new DataSetMapObject());
                        succesfulEntitiesCopied++;
                    }
                }
                catch (SystemException)
                {
                    throw;
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }

            var result = CreateResultMessage(succesfulEntitiesCopied, mappingSets.Length, EntityType.DataSet);
            return result;
        }

        /// <summary>
        /// Copies the data set.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        /// <param name="ddbConnectionMap">The DDB connection map.</param>
        /// <param name="datasetMap">The dataset map.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="query">The query.</param>
        /// <returns>
        /// The result
        /// </returns>
        private string CopyDataSet(string sourceStoreId, EntityCloneHelper entityCloneHelper, EntityMap<IConnectionEntity> ddbConnectionMap, IDictionary<string, DataSetMapObject> datasetMap, ICollection<Exception> exceptions, IEntityQuery query)
        {
            var datasets = this._entityRetrieverManager.GetEntities<DatasetEntity>(sourceStoreId, query, Detail.Full).ToArray();
            return CopyDataSet(entityCloneHelper, ddbConnectionMap, datasetMap, exceptions, datasets);
        }

        /// <summary>
        /// Copies the data set.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        /// <param name="datasetMap">The dataset map.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="query">The query.</param>
        /// <returns>
        /// The result
        /// </returns>
        private string CloneDataSet(string sourceStoreId, EntityCloneHelper entityCloneHelper, IDictionary<string, DataSetMapObject> datasetMap, ICollection<Exception> exceptions, IEntityQuery query)
        {
            var datasets = this._entityRetrieverManager.GetEntities<DatasetEntity>(sourceStoreId, query, Detail.Full).ToArray();
            return CloneDataSet(entityCloneHelper, datasetMap, exceptions, datasets);
        }

        /// <summary>
        /// Copies the data set.
        /// </summary>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        /// <param name="ddbConnectionMap">The DDB connection map.</param>
        /// <param name="datasetMap">The dataset map.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="datasets">The datasets.</param>
        /// <returns>The result</returns>
        private static string CopyDataSet(
            EntityCloneHelper entityCloneHelper,
            EntityMap<IConnectionEntity> ddbConnectionMap,
            IDictionary<string, DataSetMapObject> datasetMap,
            ICollection<Exception> exceptions,
            DatasetEntity[] datasets)
        {
            var datasetCopy = new DataSetCopy(entityCloneHelper);
            Func<DatasetEntity, DataSetMapObject> copyFunc = entity => datasetCopy.CopyDataset(entity, ddbConnectionMap);
            return CopyDataSet(datasetMap, exceptions, datasets, copyFunc);
        }

        /// <summary>
        /// Clones the data set.
        /// </summary>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        /// <param name="datasetMap">The dataset map.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="datasets">The datasets.</param>
        /// <returns>The result</returns>
        private static string CloneDataSet(
            EntityCloneHelper entityCloneHelper,
            IDictionary<string, DataSetMapObject> datasetMap,
            ICollection<Exception> exceptions,
            DatasetEntity[] datasets)
        {
            var datasetCopy = new DataSetCopy(entityCloneHelper);
            Func<DatasetEntity, DataSetMapObject> copyFunc = entity => datasetCopy.CloneDataset(entity);
            return CopyDataSet(datasetMap, exceptions, datasets, copyFunc);
        }

        /// <summary>
        /// Copies the data set.
        /// </summary>
        /// <param name="datasetMap">The dataset map.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="datasets">The datasets.</param>
        /// <param name="copyFunc">The copy function.</param>
        /// <returns>The result</returns>
        private static string CopyDataSet(
            IDictionary<string, DataSetMapObject> datasetMap,
            ICollection<Exception> exceptions,
            DatasetEntity[] datasets,
            Func<DatasetEntity, DataSetMapObject> copyFunc)
        {
            var succesfulEntitiesCopied = 0;
            foreach (var entity in datasets)
            {
                try
                {
                    var dataSetMapObject = copyFunc(entity);
                    datasetMap.Add(entity.EntityId, dataSetMapObject);
                    succesfulEntitiesCopied++;
                }
                catch (SystemException)
                {
                    throw;
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }

            var result = CreateResultMessage(succesfulEntitiesCopied, datasets.Length, EntityType.DataSet);
            return result;
        }

        /// <summary>
        /// Copies the DDB connection.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        /// <param name="ddbConnectionMap">The DDB connection map.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="query">The query.</param>
        /// <returns>
        /// The result
        /// </returns>
        private string CopyDdbConnection(string sourceStoreId, string targetStoreId, EntityCloneHelper entityCloneHelper, EntityMap<IConnectionEntity> ddbConnectionMap, ICollection<Exception> exceptions, IEntityQuery query)
        {
            var connections = this._entityRetrieverManager.GetEntities<IConnectionEntity>(sourceStoreId, query, Detail.Full).ToArray();
            return CopyDdbConnection(targetStoreId, entityCloneHelper, ddbConnectionMap, exceptions, connections);
        }

        /// <summary>
        /// Copies the DDB connection.
        /// </summary>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        /// <param name="ddbConnectionMap">The DDB connection map.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="connections">The connections.</param>
        /// <returns>
        /// The result
        /// </returns>
        private static string CopyDdbConnection(
            string targetStoreId,
            EntityCloneHelper entityCloneHelper,
            EntityMap<IConnectionEntity> ddbConnectionMap,
            ICollection<Exception> exceptions,
            IReadOnlyCollection<IConnectionEntity> connections)
        {
            var ddbConnectionCopy = new DdbConnectionCopy(entityCloneHelper);
            Func<IConnectionEntity, IConnectionEntity> copyMethod =
                entity => ddbConnectionCopy.Copy(entity, targetStoreId);
            return CopyDdbConnection(ddbConnectionMap, exceptions, connections, copyMethod);
        }

        /// <summary>
        /// Clones the DDB connection.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        /// <param name="ddbConnectionMap">The DDB connection map.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="query">The query.</param>
        /// <returns>The result</returns>
        private string CloneDdbConnection(string sourceStoreId, EntityCloneHelper entityCloneHelper, EntityMap<IConnectionEntity> ddbConnectionMap, ICollection<Exception> exceptions, IEntityQuery query)
        {
            var connections = this._entityRetrieverManager.GetEntities<IConnectionEntity>(sourceStoreId, query, Detail.Full).ToArray();
            return CloneDdbConnection(entityCloneHelper, ddbConnectionMap, exceptions, connections);
        }

        /// <summary>
        /// Clones the DDB connection.
        /// </summary>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        /// <param name="ddbConnectionMap">The DDB connection map.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="connections">The connections.</param>
        /// <returns>The result</returns>
        private static string CloneDdbConnection(
          EntityCloneHelper entityCloneHelper,
          EntityMap<IConnectionEntity> ddbConnectionMap,
          ICollection<Exception> exceptions,
          IReadOnlyCollection<IConnectionEntity> connections)
        {
            var ddbConnectionCopy = new DdbConnectionCopy(entityCloneHelper);
            Func<IConnectionEntity, IConnectionEntity> copyMethod =
                entity => ddbConnectionCopy.Clone(entity);
            return CopyDdbConnection(ddbConnectionMap, exceptions, connections, copyMethod);
        }

        /// <summary>
        /// Copies the DDB connection.
        /// </summary>
        /// <param name="ddbConnectionMap">The DDB connection map.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="connections">The connections.</param>
        /// <param name="copyMethod">The copy method.</param>
        /// <returns>The result</returns>
        private static string CopyDdbConnection(
            EntityMap<IConnectionEntity> ddbConnectionMap,
            ICollection<Exception> exceptions,
            IReadOnlyCollection<IConnectionEntity> connections,
            Func<IConnectionEntity, IConnectionEntity> copyMethod)
        {
            var existingCount = connections.Count;
            var succesfulEntitiesCopied = 0;
            foreach (var connection in connections)
            {
                try
                {
                    var targetEntity = copyMethod(connection);
                    ddbConnectionMap.Add(connection, targetEntity);
                    succesfulEntitiesCopied++;
                }
                catch (SystemException)
                {
                    throw;
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }

            var result = CreateResultMessage(succesfulEntitiesCopied, existingCount, EntityType.DdbConnectionSettings);
            return result;
        }

        /// <summary>
        /// Copies the registry.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="entityCloneHelper">The entity clone helper.</param>
        /// <param name="query"></param>
        /// <returns>
        /// The result
        /// </returns>
        private string CopyRegistry(string sourceStoreId, string targetStoreId, ICollection<Exception> exceptions, EntityCloneHelper entityCloneHelper, IEntityQuery query)
        {
            int succesfulEntitiesCopied = 0;

            var registryEntities = this._entityRetrieverManager.GetEntities<RegistryEntity>(sourceStoreId, query, Detail.Full).ToArray();
            var sourceEntityCount = registryEntities.Length;
            foreach (var registryEntity in registryEntities)
            {
                try
                {
                    entityCloneHelper.CopyAndSaveUniqueName(registryEntity, null, targetStoreId);
                    succesfulEntitiesCopied++;
                }
                catch (SystemException)
                {
                    throw;
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }

            return CreateResultMessage(succesfulEntitiesCopied, sourceEntityCount, EntityType.Registry);
        }

        private string CopyNsiws(string sourceStoreId, string targetStoreId, ICollection<Exception> exceptions, EntityCloneHelper entityCloneHelper, IEntityQuery query)
        {
            int succesfulEntitiesCopied = 0;

            var registryEntities = this._entityRetrieverManager.GetEntities<NsiwsEntity>(sourceStoreId, query, Detail.Full).ToArray();
            var sourceEntityCount = registryEntities.Length;
            foreach (var registryEntity in registryEntities)
            {
                try
                {
                    entityCloneHelper.CopyAndSaveUniqueName(registryEntity, null, targetStoreId);
                    succesfulEntitiesCopied++;
                }
                catch (SystemException)
                {
                    throw;
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }

            return CreateResultMessage(succesfulEntitiesCopied, sourceEntityCount, EntityType.Nsiws);
        }

        /// <summary>
        /// Copies the header template.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="query">The query.</param>
        /// <returns>
        /// The result
        /// </returns>
        private string CopyHeaderTemplate(string sourceStoreId, string targetStoreId, ICollection<Exception> exceptions, IEntityQuery query)
        {
            int succesfulEntitiesCopied = 0;

            var entities = this._entityRetrieverManager.GetEntities<HeaderEntity>(sourceStoreId, query, Detail.Full).ToDictionary(entity => entity.ParentId);
            var sourceEntityCount = entities.Count;
            var existingEntities = this._entityRetrieverManager.GetEntities<HeaderEntity>(targetStoreId, EntityQuery.Empty, Detail.Full);
            var persistenceEngine = this._entityPersistenceManager.GetEngine<HeaderEntity>(targetStoreId);
            foreach (var existingEntity in existingEntities)
            {
                entities.Remove(existingEntity.ParentId);
            }

            foreach (var entity in entities)
            {
                try
                {
                    var newTargetEntity = entity.Value.CreateDeepCopy<HeaderEntity>();
                    newTargetEntity.StoreId = targetStoreId;
                    persistenceEngine.Add(newTargetEntity);
                    succesfulEntitiesCopied++;
                }
                catch (SystemException)
                {
                    throw;
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }

            return CreateResultMessage(succesfulEntitiesCopied, sourceEntityCount, EntityType.User);
        }

        /// <summary>
        /// Copies the user.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="exceptions">The exceptions.</param>
        /// <param name="query">The query.</param>
        /// <returns>
        /// The result
        /// </returns>
        private string CopyUser(string sourceStoreId, string targetStoreId, ICollection<Exception> exceptions, IEntityQuery query)
        {
            int succesfulEntitiesCopied = 0;

            var userEntities = this._entityRetrieverManager.GetEntities<UserEntity>(sourceStoreId, query, Detail.Full).ToDictionary(entity => entity.UserName);
            var sourceEntityCount = userEntities.Count;
            var existingEntities = this._entityRetrieverManager.GetEntities<UserEntity>(targetStoreId, EntityQuery.Empty, Detail.IdAndName);
            var userEngine = this._entityPersistenceManager.GetEngine<UserEntity>(targetStoreId);
            foreach (var existingEntity in existingEntities)
            {
                userEntities.Remove(existingEntity.UserName);
            }

            foreach (var userEntity in userEntities)
            {
                try
                {
                    var newTargetEntity = userEntity.Value.CreateDeepCopy<UserEntity>();
                    newTargetEntity.StoreId = targetStoreId;
                    userEngine.Add(newTargetEntity);
                    succesfulEntitiesCopied++;
                }
                catch (SystemException)
                {
                    throw;
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }

            return CreateResultMessage(succesfulEntitiesCopied, sourceEntityCount, EntityType.User);
        }

        /// <summary>
        /// Copies the entities.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityQuery">The entity query.</param>
        /// <returns>The results with exceptions</returns>
        private Tuple<string, List<Exception>> CopyEntities(string sourceStoreId, string targetStoreId, EntityType entityType, IEntityQuery entityQuery)
        {
            var exceptions = new List<Exception>();
            var messages = new List<string>();
            try
            {
                var entityCloneHelper = new EntityCloneHelper(this._entityRetrieverManager, this._entityPersistenceManager, sourceStoreId);
                switch (entityType)
                {
                    case EntityType.DdbConnectionSettings:
                        messages.Add(CopyDdbConnection(
                            sourceStoreId,
                            targetStoreId,
                            entityCloneHelper,
                            new EntityMap<IConnectionEntity>(),
                            exceptions,
                            entityQuery));
                        break;
                    case EntityType.MappingSet:
                        {
                            var mappingSetEntities =
                                this._entityRetrieverManager.GetEntities<MappingSetEntity>(
                                    sourceStoreId,
                                    entityQuery,
                                    Detail.Full).ToArray();
                            var datasetEntityIdWithMappingsSets = mappingSetEntities.ToLookup(entity => entity.DataSetId);
                            var targetRetrievalManager = GetRetrievalManager(this._databaseManager.GetDatabase(targetStoreId));
                            var dataflowUrns = mappingSetEntities.Select(entity => entity.ParentId).Distinct(StringComparer.Ordinal);
                            var connectionStringSettings = _databaseManager.GetConnectionStringSettings(new DatabaseIdentificationOptions { StoreId = targetStoreId } );
                            foreach (var urns in dataflowUrns)
                            {
                                CopyDataflow(connectionStringSettings, new Uri(urns), targetRetrievalManager);
                            }

                            // TODO an In operator
                            var ddbConnectionEntityIdWithDataSets = _entityRetrieverManager.GetEntities<DatasetEntity>(
                                sourceStoreId,
                                EntityQuery.Empty,
                                Detail.Full).Where(entity => datasetEntityIdWithMappingsSets.Contains(entity.EntityId)).ToLookup(e => e.ParentId);

                            var ddbConnections =
                                _entityRetrieverManager.GetEntities<IConnectionEntity>(
                                    sourceStoreId,
                                    EntityQuery.Empty,
                                    Detail.Full)
                                    .Where(entity => ddbConnectionEntityIdWithDataSets.Contains(entity.EntityId)).ToArray();

                            var ddbConnectionMap = new EntityMap<IConnectionEntity>();
                            messages.Add(CopyDdbConnection(
                                targetStoreId,
                                entityCloneHelper,
                                ddbConnectionMap,
                                exceptions,
                                ddbConnections));

                            var datasetMap = new Dictionary<string, DataSetMapObject>(StringComparer.Ordinal);
                            messages.Add(CopyDataSet(entityCloneHelper, ddbConnectionMap, datasetMap, exceptions, ddbConnectionEntityIdWithDataSets.SelectMany(entities => entities).ToArray()));
                            messages.Add(CopyMappingSet(entityCloneHelper, datasetMap, exceptions, mappingSetEntities));
                        }

                        break;
                    case EntityType.DataSet:
                        {
                            var ddbConnectionEntityIdWithDataSets = _entityRetrieverManager.GetEntities<DatasetEntity>(
                                sourceStoreId,
                                entityQuery,
                                Detail.Full).ToLookup(e => e.ParentId);

                            var ddbConnections =
                                _entityRetrieverManager.GetEntities<IConnectionEntity>(
                                    sourceStoreId,
                                    EntityQuery.Empty,
                                    Detail.Full)
                                    .Where(entity => ddbConnectionEntityIdWithDataSets.Contains(entity.EntityId)).ToArray();

                            var ddbConnectionMap = new EntityMap<IConnectionEntity>();
                            messages.Add(CopyDdbConnection(
                                targetStoreId,
                                entityCloneHelper,
                                ddbConnectionMap,
                                exceptions,
                                ddbConnections));

                            var datasetMap = new Dictionary<string, DataSetMapObject>(StringComparer.Ordinal);
                            messages.Add(CopyDataSet(entityCloneHelper, ddbConnectionMap, datasetMap, exceptions, ddbConnectionEntityIdWithDataSets.SelectMany(entities => entities).ToArray()));
                        }

                        break;
                    case EntityType.Registry:
                        messages.Add(CopyRegistry(sourceStoreId, targetStoreId, exceptions, entityCloneHelper, entityQuery));
                        break;
                    case EntityType.Nsiws:
                        messages.Add(CopyNsiws(sourceStoreId, targetStoreId, exceptions, entityCloneHelper, entityQuery));
                        break;
                    case EntityType.User:
                        messages.Add(CopyUser(sourceStoreId, targetStoreId, exceptions, entityQuery));
                        break;
                    case EntityType.Header:
                        messages.Add(CopyHeaderTemplate(sourceStoreId, targetStoreId, exceptions, entityQuery));
                        break;
                }
            }
            catch (Exception e)
            {
                exceptions.Add(e);
                return new Tuple<string, List<Exception>>(string.Join("\n", messages), exceptions);
            }

            return new Tuple<string, List<Exception>>(string.Join("\n", messages), exceptions);
        }

        /// <summary>
        /// Copies the entities.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityQuery">The entity query.</param>
        /// <param name="copyParents">if set to <c>true</c> [copy parents].</param>
        /// <returns>The results with exceptions</returns>
        /// <exception cref="ResourceConflictException">
        /// Mapping Set cannot be copied to the same dataflow
        /// or
        /// Header Template cannot be copied to the same dataflow
        /// or
        /// User cannot be copied to the same mapping assistant db
        /// </exception>
        private Tuple<string, List<Exception>> CopyEntities(string sourceStoreId, EntityType entityType, IEntityQuery entityQuery, bool copyParents)
        {
            var exceptions = new List<Exception>();
            var messages = new List<string>();
            try
            {
                var entityCloneHelper = new EntityCloneHelper(this._entityRetrieverManager, this._entityPersistenceManager, sourceStoreId);
                switch (entityType)
                {
                    case EntityType.DdbConnectionSettings:
                        messages.Add(CloneDdbConnection(
                            sourceStoreId,
                            entityCloneHelper,
                            new EntityMap<IConnectionEntity>(),
                            exceptions,
                            entityQuery));
                        break;
                    case EntityType.MappingSet:
                        throw new ResourceConflictException("Mapping Set cannot be copied to the same dataflow");
                    case EntityType.Header:
                        throw new ResourceConflictException("Header Template cannot be copied to the same dataflow");
                    case EntityType.DataSet:
                        {
                            var datasetMap = new Dictionary<string, DataSetMapObject>(StringComparer.Ordinal);
                            if (copyParents)
                            {
                                var ddbConnectionEntityIdWithDataSets =
                                    _entityRetrieverManager.GetEntities<DatasetEntity>(
                                        sourceStoreId,
                                        entityQuery,
                                        Detail.Full).ToLookup(e => e.ParentId);

                                var ddbConnections =
                                    _entityRetrieverManager.GetEntities<IConnectionEntity>(
                                        sourceStoreId,
                                        EntityQuery.Empty,
                                        Detail.Full)
                                        .Where(entity => ddbConnectionEntityIdWithDataSets.Contains(entity.EntityId))
                                        .ToArray();


                                var ddbConnectionMap = new EntityMap<IConnectionEntity>();
                                messages.Add(
                                    CloneDdbConnection(entityCloneHelper, ddbConnectionMap, exceptions, ddbConnections));
                                messages.Add(
                                    CopyDataSet(
                                        entityCloneHelper,
                                        ddbConnectionMap,
                                        datasetMap,
                                        exceptions,
                                        ddbConnectionEntityIdWithDataSets.SelectMany(entities => entities).ToArray()));
                            }
                            else
                            {
                                messages.Add(
                                    CloneDataSet(
                                        this._sourceStoreId,
                                        entityCloneHelper,
                                        datasetMap,
                                        exceptions,
                                        entityQuery));
                            }
                        }

                        break;
                    case EntityType.Registry:
                        messages.Add(CopyRegistry(sourceStoreId, sourceStoreId, exceptions, entityCloneHelper, entityQuery));
                        break;
                    case EntityType.Nsiws:
                        messages.Add(CopyNsiws(sourceStoreId, sourceStoreId, exceptions, entityCloneHelper, entityQuery));
                        break;
                    case EntityType.User:
                        throw new ResourceConflictException("User cannot be copied to the same mapping assistant db");
                }
            }
            catch (Exception e)
            {
                exceptions.Add(e);
                return new Tuple<string, List<Exception>>(string.Join("\n", messages), exceptions);
            }

            return new Tuple<string, List<Exception>>(string.Join("\n", messages), exceptions);
        }

        /// <summary>
        /// Creates the result message.
        /// </summary>
        /// <param name="succesfulEntitiesCopied">The successful entities copied.</param>
        /// <param name="count">The count.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>The result message</returns>
        private static string CreateResultMessage(int succesfulEntitiesCopied, int count, EntityType entityType)
        {
            // TODO to Resource
            return $"Copied {succesfulEntitiesCopied} out of {count} of type " + entityType;
        }

        /// <summary>
        /// Copies the SDMX.
        /// </summary>
        /// <param name="targetId">The target identifier.</param>
        /// <returns>The import status list</returns>
        private List<ArtefactImportStatus> CopySdmx(string targetId)
        {
            var targetMappingStore = this._databaseManager.GetConnectionStringSettings(new DatabaseIdentificationOptions { StoreId = targetId });
            var retrievalManager = this._sourceSdmxRetrievalManager;
            var artefactImportStatuses = new List<ArtefactImportStatus>();
            var persistenceManager = GetPersistManager(targetMappingStore, artefactImportStatuses);
            var sdmxObjectsImpl = new SdmxObjectsImpl();

            // and the we retrieve
            var requestedTypes = new[] { SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AgencyScheme),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProviderScheme),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataConsumerScheme),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.OrganisationUnitScheme),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Msd),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataFlow),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.HierarchicalCode),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.StructureSet),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ContentConstraint),
            };
            
            foreach (var type in requestedTypes)
            {
                ICommonStructureQuery commonQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, null)
                .SetMaintainableTarget(type)
                .Build();
                var structuresFromDatabase = retrievalManager.GetMaintainables(commonQuery);
                sdmxObjectsImpl.Merge(structuresFromDatabase);
            }

            // finally we store the sdmxObjects in the target DB
            persistenceManager.SaveStructures(sdmxObjectsImpl);
            return artefactImportStatuses;
        }

        /// <summary>
        /// Gets the persist manager.
        /// </summary>
        /// <param name="destMappingStore">The target mapping store.</param>
        /// <param name="artefactImportStatuses">The artefact import statuses.</param>
        /// <returns>
        /// The <see cref="IStructurePersistenceManager" />.
        /// </returns>
        private static IStructurePersistenceManager GetPersistManager(ConnectionStringSettings destMappingStore, List<ArtefactImportStatus> artefactImportStatuses)
        {
            return new MappingStoreManager(destMappingStore, artefactImportStatuses);
        }

        /// <summary>
        /// Gets the retrieval manager.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>The SDMX retrieval manager</returns>
        private ICommonSdmxObjectRetrievalManager GetRetrievalManager(Database database)
        {
            return this._commonSdmxObjectRetrievalFactory.GetManager(database.ConnectionStringSettings);
        }
    }
}