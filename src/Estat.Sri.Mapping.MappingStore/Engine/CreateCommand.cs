using System.Collections.Generic;
using Dapper;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using System.Linq;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    internal class CreateCommand
    {
        private readonly List<PatchDocument> _documents;
        private readonly Database _database;
        private readonly string _entityId;
        private readonly string _typeOfParty;
        private readonly string _language;
        private readonly HeaderIdsRetriever _headerIdsRetriever;

        public CreateCommand(List<PatchDocument> documents, Database database, string entityId, string typeOfParty, string language)
        {
            this._headerIdsRetriever = new HeaderIdsRetriever(database,entityId,typeOfParty);
            this._documents = documents;
            this._database = database;
            this._entityId = entityId;
            this._typeOfParty = typeOfParty;
            this._language = language;
        }

        public IEnumerable<CommandDefinition> ForPartyId()
        {
            var relevantDocument = this._documents.Find(item => item.HasPath("id"));
            if (relevantDocument != null)
            {
                var updateInfo = new UpdateInfoAction()
                {
                    SqlStatementType = SqlStatementType.Update,
                    DataInformationType = DatabaseInformationType.HeaderParty,
                    PathAndValues = new Dictionary<string, object>() { { PartyDataTableInformations.Id.ModelName, relevantDocument.Value } }
                };

                return this.GetCommandFromUpdateInfo(updateInfo, this._headerIdsRetriever.GetPartyId(relevantDocument), this._database);
            }

            return new List<CommandDefinition>();
        }


        private IEnumerable<CommandDefinition> GetCommandFromUpdateInfo(UpdateInfoAction updateInfoAction, string entityId, Database mappingStoreDatabase)
        {
            var commandDefinitionManager = new CommandDefinitionManager(new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
            var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType, updateInfoAction.SqlStatementType);
            return commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, mappingStoreDatabase);
        }

        public IEnumerable<CommandDefinition> ForEmail(SqlStatementType sqlStatementType, long? contactId)
        {
            var relevantDocument = this._documents.Find(item => item.HasPath("email"));
            if (relevantDocument != null)
            {
                var updateInfo = new UpdateInfoAction()
                {
                    SqlStatementType = sqlStatementType,
                    DataInformationType = DatabaseInformationType.ContactDetails,
                    PathAndValues = new Dictionary<string, object>()
                    {
                        { ContactDetailsDataTableInfo.Value.ModelName, relevantDocument.Value },
                        { ContactDetailsDataTableInfo.Type.ModelName,"Email" }
                    }
                };
                if (sqlStatementType == SqlStatementType.Insert)
                {
                    updateInfo.PathAndValues.Add(ContactDetailsDataTableInfo.ContactId.ModelName,contactId);
                }
                var contactDetailId = sqlStatementType == SqlStatementType.Update? this._headerIdsRetriever.GetContactDetailId(relevantDocument):null;
                return this.GetCommandFromUpdateInfo(updateInfo, contactDetailId, this._database);
            }
            return new List<CommandDefinition>();
        }

        public IEnumerable<CommandDefinition> ForPartyName(SqlStatementType sqlStatementType, long? partyId)
        {
            var relevantDocument = this._documents.Find(item => item.HasPath("name") && !item.HasPath("contact/name"));
            if (relevantDocument != null)
            {
                var updateInfo = new UpdateInfoAction()
                {
                    SqlStatementType = sqlStatementType,
                    DataInformationType = DatabaseInformationType.HeaderLocalisedString,
                    PathAndValues = new Dictionary<string, object>() {
                    { HeaderLocalisedStringDataTableInformations.Text.ModelName, relevantDocument.Value },
                    { HeaderLocalisedStringDataTableInformations.Language.ModelName, this._language},
                    { HeaderLocalisedStringDataTableInformations.Type.ModelName,"Name"} }
                };
                if (sqlStatementType == SqlStatementType.Insert)
                {
                    updateInfo.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.PartyId.ModelName,partyId);
                }
                var headerLocalisedStringId = sqlStatementType == SqlStatementType.Update?this._headerIdsRetriever.GetHeaderLocalisedStringId(relevantDocument) : null;
                return this.GetCommandFromUpdateInfo(updateInfo, headerLocalisedStringId, this._database);
            }
            return new List<CommandDefinition>();
        }

        public IEnumerable<CommandDefinition> ForContactDetails(SqlStatementType sqlStatementType, string type, long? contactId)
        {
            var relevantDocument = this._documents.Find(item => item.HasPath("contact/" + type));
            if (relevantDocument != null)
            {
                var updateInfo = new UpdateInfoAction()
                {
                    SqlStatementType = sqlStatementType,
                    DataInformationType = DatabaseInformationType.HeaderLocalisedString,
                    PathAndValues = new Dictionary<string, object>() {
                    { HeaderLocalisedStringDataTableInformations.Text.ModelName, relevantDocument.Value },
                    { HeaderLocalisedStringDataTableInformations.Language.ModelName, this._language},
                    { HeaderLocalisedStringDataTableInformations.Type.ModelName,type} }
                };

                if (sqlStatementType == SqlStatementType.Insert)
                {
                    updateInfo.PathAndValues.Add(HeaderLocalisedStringDataTableInformations.ContactId.ModelName, contactId);
                }
                var headerLocalisedStringIdForType = sqlStatementType == SqlStatementType.Update? this._headerIdsRetriever.GetHeaderLocalisedStringIdForType(relevantDocument,type) : null;
                return this.GetCommandFromUpdateInfo(updateInfo, headerLocalisedStringIdForType, this._database);
            }
            return new List<CommandDefinition>();
        }

        public IEnumerable<CommandDefinition> ForPartyTable()
        {
            var relevantDocument = this._documents.Find(item => item.HasPath("id"));
            if (relevantDocument != null)
            {
                var updateInfo = new UpdateInfoAction()
                {
                    SqlStatementType = SqlStatementType.Insert,
                    DataInformationType = DatabaseInformationType.HeaderParty,
                    PathAndValues = new Dictionary<string, object>() { { PartyDataTableInformations.Id.ModelName, relevantDocument.Value }
                        , {PartyDataTableInformations.Type.ModelName, this._typeOfParty}
                        , {PartyDataTableInformations.HeaderId.ModelName, this._entityId} }
                };

                return this.GetCommandFromUpdateInfo(updateInfo, null, this._database);
            }

            return new List<CommandDefinition>();
        }

        public IEnumerable<CommandDefinition> ForContactTable(long partyId)
        {
            var updateInfo = new UpdateInfoAction
            {
                SqlStatementType = SqlStatementType.Insert,
                DataInformationType = DatabaseInformationType.Contact,
                PathAndValues = new Dictionary<string, object>
                {
                    {ContactDataTableInfo.PartyId.ModelName, partyId}
                }
            };

            return this.GetCommandFromUpdateInfo(updateInfo, null, this._database);
        }
    }
}