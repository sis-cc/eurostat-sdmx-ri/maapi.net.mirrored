// -----------------------------------------------------------------------
// <copyright file="TranscodingRuleRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Dapper;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Builder;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// The TRANSCODING rule retriever engine.
    /// </summary>
    public class TranscodingRuleRetrieverEngine53 : IEntityRetrieverEngine<TranscodingRuleEntity>
    {
        private const string JoinString = "TRANSCODING_RULE " + TranscodingRuleAlias 
            + @" LEFT OUTER JOIN TRANSCODING_RULE_DSD_CODE DC 
                  ON DC.TR_RULE_ID = TR.TR_RULE_ID
                 INNER JOIN TRANSCODING_RULE_LOCAL_CODE LC 
                    ON LC.TR_RULE_ID = TR.TR_RULE_ID 
                 INNER JOIN LOCAL_CODE L
                    ON L.LCD_ID = LC.LCD_ID
                 LEFT OUTER JOIN ITEM DI 
                   ON DC.CD_ID = DI.ITEM_ID 
                 INNER JOIN ITEM LI 
                    ON LI.ITEM_ID = LC.LCD_ID";

        private const string TranscodingRuleAlias = "TR";

        private readonly Database _databaseManager;

        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;

        private readonly IDictionary<string, string> _integerAdditionalProperties;

        public TranscodingRuleRetrieverEngine53(Database databaseManager)
        {
            this._databaseManager = databaseManager;
            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }
            this._integerAdditionalProperties = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                {"ParentId", "COMP_ID"},
                {"ColumnId", "COL_ID"}
            };

            this._databaseManager = databaseManager;
            this._fieldsToReturn = new Dictionary<Detail, IList<string>>();
            this._fieldsToReturn.Add(
                Detail.Full,
                new[]
                    {
                        "TR.TR_RULE_ID as " + nameof(TranscodingRuleRecord.Id),
                        "TR.TR_ID as " + nameof(TranscodingRuleRecord.ParentId),
                        $"DC.CD_ID as {nameof(TranscodingRuleRecord.CodeEntityId)}",
                        $"LC.LCD_ID as {nameof(TranscodingRuleRecord.LocalCodeEntityId)}",
                        $"DI.ID as {nameof(TranscodingRuleRecord.CodeObjectId)}",
                        $"LI.ID as {nameof(TranscodingRuleRecord.LocalCodeObjectId)}",
                        $"L.COLUMN_ID as {nameof(TranscodingRuleRecord.LocalCodeParentId)}",
                    });
            this._fieldsToReturn.Add(Detail.IdAndName, new[] {"TR.TR_RULE_ID as " + nameof(TranscodingRuleEntity.EntityId), "DC.NAME as " + nameof(DataSetColumnEntity.Name)});
            this._fieldsToReturn.Add(Detail.IdOnly, new[] {"TR.TR_RULE_ID as " + nameof(TranscodingRuleEntity.EntityId)});
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<TranscodingRuleEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var db = this._databaseManager;
            var whereClauseBuilder = new WhereClauseBuilder(db);

            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "TR_RULE_ID", TranscodingRuleAlias));
            clauses.Add(whereClauseBuilder.Build(query.ParentId, "TR_ID", TranscodingRuleAlias));

            // TODO PAGING !!!!! There might 1m rows here
            // Check portability of ROW_NUMBER() OVER (PARTITION 
            // http://stackoverflow.com/questions/7182325/calculate-number-of-pages-of-records-in-select-statement
            // TODO
            foreach (var integerAdditionalProperty in this._integerAdditionalProperties)
            {
                ICriteria<string> additionalCriteria;
                if (query.AdditionalCriteria.TryGetValue(integerAdditionalProperty.Key, out additionalCriteria))
                {
                    clauses.Add(whereClauseBuilder.Build(additionalCriteria.ValueConvert(), integerAdditionalProperty.Value, TranscodingRuleAlias));
                }
            }

            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3}", fields, JoinString, whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);

            var entities = new Dictionary<string, TranscodingRuleEntity>(StringComparer.Ordinal);

            var transcodingRuleRecords = db.Query<TranscodingRuleRecord>(sqlQuery, param);
            foreach (var entry in transcodingRuleRecords)
            {
                TranscodingRuleEntity entity;
                if (!entities.TryGetValue(entry.Id, out entity))
                {
                    entity = new TranscodingRuleEntity();
                    if (entry.CodeEntityId != null)
                    {
                        entity.DsdCodeEntity = new IdentifiableEntity(EntityType.Sdmx);
                        entity.DsdCodeEntity.EntityId = entry.CodeEntityId;
                        entity.DsdCodeEntity.ObjectId = entry.CodeObjectId;
                    }
                    entity.UncodedValue = entry.UncodedValue;
                    entity.EntityId = entry.Id;
                    entity.ParentId = entry.ParentId;
                    entities.Add(entity.EntityId, entity);
                }

                var localCode = new LocalCodeEntity();
                localCode.EntityId = entry.LocalCodeEntityId;
                localCode.ObjectId = entry.LocalCodeObjectId;
                localCode.ParentId = entry.LocalCodeParentId; // We need the parent in order to identify each local code column
                entity.LocalCodes.Add(localCode);
            }

            return entities.Values;
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }

        /// <summary>
        /// The TRANSCODING rule record.
        /// </summary>
        private class TranscodingRuleRecord
        {
            /// <summary>
            /// Gets or sets the identifier
            /// </summary>
            public string Id{ get; set; }

            /// <summary>
            /// Gets or sets the parent identifier
            /// </summary>
            public string ParentId{ get; set; }

            /// <summary>
            /// Gets or sets the code entity identifier
            /// </summary>
            public string CodeEntityId{ get; set; }

            /// <summary>
            /// Gets or sets the code object identifier
            /// </summary>
            public string CodeObjectId{ get; set; }

            /// <summary>
            /// Gets or sets the local code entity identifier
            /// </summary>
            public string LocalCodeEntityId{ get; set; }

            /// <summary>
            /// Gets or sets the local code object identifier
            /// </summary>
            public string LocalCodeObjectId{ get; set; }

            /// <summary>
            /// Gets or sets the local code parent identifier
            /// </summary>
            public string LocalCodeParentId{ get; set; }

            /// <summary>
            /// Gets or sets the uncoded value.
            /// </summary>
            /// <value>
            /// The uncoded value.
            /// </value>
            public string UncodedValue { get; set; }
        }
    }
}