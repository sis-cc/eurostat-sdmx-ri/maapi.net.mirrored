// -----------------------------------------------------------------------
// <copyright file="DdbConnectionSettings.cs" company="EUROSTAT">
//   Date Created : 2017-03-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Util.Extensions;

    public class DdbConnectionSettingsRetrieverEngine : EntityBaseRetrieverEngine<DdbConnectionEntity>
    {
        private const string DdbConnectionAlias = "DBC";

        private const string TableAlias = "N_DB_CONNECTION " + DdbConnectionAlias;

        public DdbConnectionSettingsRetrieverEngine(Database databaseManager):base(databaseManager)
        {
            this._integerAdditionalProperties.Add("DatabaseType", "DB_TYPE");
            this._fieldsToReturn = new Dictionary<Detail, IList<string>>();
            var fullFields = BaseFieldsToReturn.Union(new[]
            {

                "DBC.DB_TYPE as " + nameof(DdbConnectionEntity.DbType),
                "DBC.IS_ENCRYPTED as " + nameof(DdbConnectionEntity.IsEncrypted),
                "DBC.DB_PASSWORD as " + nameof(DdbConnectionEntity.Password),
                "DBC.DB_USER as " + nameof(DdbConnectionEntity.DbUser),
                "DBC.ADO_CONNECTION_STRING as " + nameof(DdbConnectionEntity.AdoConnString),
                "DBC.JDBC_CONNECTION_STRING as " + nameof(DdbConnectionEntity.JdbcConnString),
                "DBC.PROPERTIES as " + nameof(DdbConnectionEntity.Properties),
            });
            this._fieldsToReturn.Add(Detail.Full, fullFields.ToList());
            this._fieldsToReturn.Add(Detail.IdAndName, new[] { EntityIdAlias, NameAlias});
            this._fieldsToReturn.Add(Detail.IdOnly, new[] { EntityIdAlias });
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public override IEnumerable<DdbConnectionEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var db = this._databaseManager;
            var whereClauseBuilder = new WhereClauseBuilder(db);

            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "ENTITY_ID", "EB"));
            clauses.Add(whereClauseBuilder.Build(query.ObjectId, "OBJECT_ID", "EB"));
            foreach (var integerAdditionalProperty in this._integerAdditionalProperties)
            {
                ICriteria<string> additionalCriteria;
                if (query.AdditionalCriteria.TryGetValue(integerAdditionalProperty.Key, out additionalCriteria))
                {
                    clauses.Add(whereClauseBuilder.Build(additionalCriteria, integerAdditionalProperty.Value, IsBaseProperty(integerAdditionalProperty.Key)? EntityBaseAlias: DdbConnectionAlias));
                }
            }

            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3}", fields, GetBaseJoinString(TableAlias, DdbConnectionAlias + ".ENTITY_ID"), whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);

            return db.Query<DdbConnectionEntity>(sqlQuery, param);
        }

        public override void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }
    }
}