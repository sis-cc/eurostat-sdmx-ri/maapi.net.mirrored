// -----------------------------------------------------------------------
// <copyright file="TimeMappingPersistEngine.cs" company="EUROSTAT">
//   Date Created : 2022-08-03
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Utils;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public abstract class MappingBasePersistEngine<T> : IEntityPersistenceEngine<T> where T : class, IMappingEntity, IEntity
    {

        private readonly Database _database;
        private readonly EntityImporterWithIdMapping _entityImporterWithIdMapping = new EntityImporterWithIdMapping();

        protected MappingBasePersistEngine(Database database)
        {
            _database = database;
        }


        public void Update(IEntity entity)
        {
            if (!string.IsNullOrEmpty(entity?.EntityId))
            {
                long mappingSetId = entity.GetEntityId();
                using (var context = new ContextWithTransaction(_database))
                {
                    Delete(mappingSetId, context.DatabaseUnderContext);
                    Add((T)entity, context);
                    context.Complete();
                }
            }
        }

        public void Delete(string entityId, EntityType entityType)
        {
            long mappingSetId = entityId.AsMappingStoreEntityId();
            using (var context = new ContextWithTransaction(_database))
            {
                Delete(mappingSetId, context.DatabaseUnderContext);
                context.Complete();
            }
        }

        public abstract void Delete(long mappingSetId, Database databaseInContext);


        public void DeleteChildren(string entityId, EntityType childrenEntityType)
        {
            throw new System.NotImplementedException();
        }

        public List<StatusType> Add(IEntityStreamingReader input, IEntityStreamingWriter responseWriter)
        {
            List<StatusType> statuses = new List<StatusType>();
            while (input.MoveToNextEntity())
            {
                // TODO get StoreId from Constructor
                var entity = input.CurrentEntity;
                if (entity.Status == StatusType.Success)
                {
                    long entityId = Add((T)entity.Entity);
                    if (responseWriter != null)
                    {
                        entity.Entity.SetEntityId(entityId);
                        responseWriter.Write(entity.Entity);
                    }
                }
                else
                {
                    responseWriter?.WriteStatus(entity.Status, entity.Message);
                }
                statuses.Add(entity.Status);
            }
            return statuses;
        }

        public void Import(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping)
        {
            // Build dsd-code entity map
            Dictionary<long, Dictionary<string, string>> cache = new Dictionary<long, Dictionary<string, string>>();
            Dictionary<string, string> specialMappingCache = new Dictionary<string, string>(StringComparer.Ordinal);


            using (var context = new ContextWithTransaction(this._database))
            {
                _entityImporterWithIdMapping.Import<T>(input, responseWriter, mapping, x =>
                {
                    return Add( x , context);
                }, (x, y, z) => { return true; });
                context.Complete(); 
            }
        }

        public void Update(PatchRequest patchRequest, EntityType entityType, string entityId)
        {
            throw new NotImplementedException();
        }

        public long Add(T entity)
        {
            using (var context = new ContextWithTransaction(_database))
            {
                var val = Add(entity, context);
                context.Complete();
                return val;
            }
        }

        protected abstract long Add(T entity, ContextWithTransaction context);
       
        public IEnumerable<long> Add(IList<T> entities)
        {
            var ids = new List<long>();
            entities.ToList().ForEach(item => ids.Add(this.Add(item)));
            return ids;
        }
    }
}