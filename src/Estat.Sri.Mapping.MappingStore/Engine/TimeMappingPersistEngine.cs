// -----------------------------------------------------------------------
// <copyright file="TimeMappingPersistEngine.cs" company="EUROSTAT">
//   Date Created : 2022-08-03
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Helper;
using Estat.Sri.MappingStoreRetrieval.Extensions;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Utils;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class TimeMappingPersistEngine : MappingBasePersistEngine<TimeDimensionMappingEntity>
    {
        private readonly EntityImporterWithIdMapping _entityImporterWithIdMapping = new EntityImporterWithIdMapping();

        private readonly Database _database;

        public TimeMappingPersistEngine(Database database) : base(database)
        {
            _database = database;
        }
/// <inheritdoc/>


        public override void Delete(long mappingSetId, Database databaseInContext)
        {
            AdvanceTimeTranscodingPersistEngine timeTranscodingPersistEngine =
                new AdvanceTimeTranscodingPersistEngine(databaseInContext);
            timeTranscodingPersistEngine.Delete(mappingSetId);
            databaseInContext.ExecuteNonQueryFormat(
                "delete from N_MAPPING_TIME_DIMENSION where STR_MAP_SET_ID = {0}",
                mappingSetId);
        }

        protected override long Add(TimeDimensionMappingEntity entity, ContextWithTransaction context)
        {
            entity.AutoDetectTypeIfNotSet();
            long mappingSetId;
            if (entity.ParentId != null)
            {
                mappingSetId = entity.GetParentId();
            }
            else
            {
                mappingSetId = entity.GetEntityId();
            }
            Delete(mappingSetId, context.DatabaseUnderContext);

            var value = Insert(entity, context);
            entity.SetEntityId(mappingSetId);
            return value;
        }
        private long Insert(TimeDimensionMappingEntity entity, ContextWithTransaction context)
        {
            long mappingSetId;
            if (entity.ParentId != null)
            {
                mappingSetId = entity.GetParentId();
            }
            else
            {
                mappingSetId = entity.GetEntityId();
            }

            var insertStatement = CreateInsertTimeMappingCommands(mappingSetId, entity, context.DatabaseUnderContext);
            context.ExecuteCommandsWithReturnId(insertStatement);
            if (entity.HasTranscoding())
            {
                AdvanceTimeTranscodingPersistEngine timeTranscodingPersistEngine =
                    new AdvanceTimeTranscodingPersistEngine(context.DatabaseUnderContext);
                timeTranscodingPersistEngine.Add(entity.Transcoding, mappingSetId);
            }

            return mappingSetId;
        }

        private IEnumerable<CommandDefinition> CreateInsertTimeMappingCommands(long mappingSetId, TimeDimensionMappingEntity entity, Database database)
        {
            var pathAndValues = new Dictionary<string, object>
            {
                { nameof(TimeDimensionMappingEntity.EntityId), mappingSetId }
            };
            switch (entity.TimeMappingType)
            {
                case TimeDimensionMappingType.MappingWithColumn:
                    pathAndValues.Add(nameof(TimeDimensionMappingEntity.SimpleMappingColumn), entity.SimpleMappingColumn);
                    pathAndValues.Add(nameof(TimeDimensionMappingEntity.DefaultValue), entity.DefaultValue );
                    break;
                case TimeDimensionMappingType.MappingWithConstant:
                    pathAndValues.Add(nameof(TimeDimensionMappingEntity.ConstantValue), entity.ConstantValue );
                    break;
                case TimeDimensionMappingType.PreFormatted:
                    pathAndValues.Add(nameof(TimePreFormattedEntity.OutputColumn), entity.PreFormatted.OutputColumn?.Name );
                    pathAndValues.Add(nameof(TimePreFormattedEntity.ToColumn), entity.PreFormatted.ToColumn?.Name );
                    pathAndValues.Add(nameof(TimePreFormattedEntity.FromColumn), entity.PreFormatted.FromColumn?.Name );
                    pathAndValues.Add(nameof(TimeDimensionMappingEntity.DefaultValue), entity.DefaultValue );
                    break;
                case TimeDimensionMappingType.TranscodingWithCriteriaColumn:
                    
                    pathAndValues.Add(nameof(TimeDimensionMappingEntity.Transcoding.CriteriaColumn), entity.Transcoding.CriteriaColumn.Name );
                    pathAndValues.Add(nameof(TimeDimensionMappingEntity.Transcoding.IsFrequencyDimension), entity.Transcoding.IsFrequencyDimension.ToDbValue() );
                    pathAndValues.Add(nameof(TimeDimensionMappingEntity.DefaultValue), entity.DefaultValue );
                    break;
                case TimeDimensionMappingType.TranscodingWithFrequencyDimension:
                    pathAndValues.Add(nameof(TimeDimensionMappingEntity.Transcoding.FrequencyDimension), entity.Transcoding.FrequencyDimension );
                    pathAndValues.Add(nameof(TimeDimensionMappingEntity.Transcoding.IsFrequencyDimension), entity.Transcoding.IsFrequencyDimension.ToDbValue() );
                    pathAndValues.Add(nameof(TimeDimensionMappingEntity.DefaultValue), entity.DefaultValue );
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return CommandBuilderHelper.InsertInformation(database,  DatabaseInformationType.TimeMapping,
                pathAndValues, mappingSetId);
        } 

    }
}