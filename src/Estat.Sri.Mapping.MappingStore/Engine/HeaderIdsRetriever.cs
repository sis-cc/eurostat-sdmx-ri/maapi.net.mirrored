// -----------------------------------------------------------------------
// <copyright file="HeaderIdsRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-04-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    internal class HeaderIdsRetriever
    {
        private readonly Database _database;
        private readonly string _entityId;
        private readonly string _typeOfParty;
        private string _contactId;
        private string _partyId;

        public HeaderIdsRetriever(Database database, string entityId, string typeOfParty)
        {
            this._database = database;
            this._entityId = entityId;
            this._typeOfParty = typeOfParty;
        }

        public string GetPartyId(PatchDocument relevantDocument)
        {
            if (string.IsNullOrWhiteSpace(this._partyId))
            {
                this._partyId = this._typeOfParty == "receiver" ? relevantDocument.Path.Split('/')[2] : this._database.Query($"SELECT PARTY_ID FROM PARTY WHERE HEADER_ID = {this._entityId} AND TYPE = 'Sender'", null).First().PARTY_ID.ToString();
            }
            return this._partyId;
        }

        public string GetContactDetailId(PatchDocument relevantDocument)
        {
            return this._database.Query($"SELECT CD_ID FROM CONTACT_DETAIL WHERE CONTACT_ID = {this.GetContactId(relevantDocument)}", null).First().CD_ID.ToString();
        }

        public string GetContactId(PatchDocument relevantDocument)
        {
            if (string.IsNullOrWhiteSpace(this._contactId))
            {
                this._contactId = this._database.Query($"SELECT CONTACT_ID FROM CONTACT WHERE PARTY_ID = {this.GetPartyId(relevantDocument)}", null).First().CONTACT_ID.ToString();
            }
            return this._contactId;
        }

        public string GetHeaderLocalisedStringId(PatchDocument relevantDocument)
        {
            return this._database.Query($"SELECT HLS_ID FROM HEADER_LOCALISED_STRING WHERE PARTY_ID = {this.GetPartyId(relevantDocument)}", null).First().HLS_ID.ToString();
        }

        public string GetHeaderLocalisedStringIdForType(PatchDocument relevantDocument, string type)
        {
            return this._database.Query($"SELECT HLS_ID FROM HEADER_LOCALISED_STRING WHERE CONTACT_ID = {this.GetContactId(relevantDocument)} AND TYPE = '{type}'", null).First().HLS_ID.ToString();
        }
    }
}