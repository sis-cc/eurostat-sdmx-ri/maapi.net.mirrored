// -----------------------------------------------------------------------
// <copyright file="DefaultsEntityRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-05-09
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Extension;

    /// <summary>
    /// Apply the mapping store id to all entities
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <seealso cref="Estat.Sri.Mapping.Api.Engine.IEntityRetrieverEngine{TEntity}" />
    public class DefaultsEntityRetrieverEngine<TEntity> : IEntityRetrieverEngine<TEntity>
        where TEntity : IEntity
    {
        /// <summary>
        /// The decorated engine
        /// </summary>
        private readonly IEntityRetrieverEngine<TEntity> _decoratedEngine;

        /// <summary>
        /// The mapping store identifier
        /// </summary>
        private readonly string _mappingStoreId;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultsEntityRetrieverEngine{TEntity}" /> class.
        /// </summary>
        /// <param name="decoratedEngine">The decorated engine.</param>
        /// <param name="mappingStoreId">The mapping store identifier.</param>
        public DefaultsEntityRetrieverEngine(IEntityRetrieverEngine<TEntity> decoratedEngine, string mappingStoreId)
        {
            _decoratedEngine = decoratedEngine;
            _mappingStoreId = mappingStoreId;
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<TEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            return this._decoratedEngine.GetEntities(query, detail).WithMappingStoreId(this._mappingStoreId);
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            this._decoratedEngine.WriteEntities(query, detail, entityWriter);
        }
    }
}