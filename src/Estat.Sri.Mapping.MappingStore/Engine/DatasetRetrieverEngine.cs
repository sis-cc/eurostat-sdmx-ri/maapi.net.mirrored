// -----------------------------------------------------------------------
// <copyright file="DatasetRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-17
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    public class DatasetRetrieverEngine : EntityBaseRetrieverEngine<DatasetEntity>
    {
        private readonly IPermissionRetriever _permissionRetriever;
        private readonly IDataSetEditorManager dataSetEditorManager; 
        private const string DatasetAlias = "DS";
        private string TableAlias = "N_DATASET " + DatasetAlias;
        private const string AdditionalJoinString = " Left outer JOIN ENTITY_SDMX_REF ESR ON EB.ENTITY_ID = ESR.SOURCE_ENTITY_ID";

        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;
        private readonly IDictionary<string, string> _integerAdditionalProperties;

        private readonly DatasetPropertyEntityRetrieverEngine _datasetPropertyEntityRetriever;

        public DatasetRetrieverEngine(Database databaseManager, IPermissionRetriever permissionRetriever, Lazy<IDataSetEditorManager> dataSetEditorManager):base(databaseManager)
        {
            this._permissionRetriever = permissionRetriever;
            this.dataSetEditorManager = dataSetEditorManager?.Value;
            this._integerAdditionalProperties = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                {"EditorType", "EDITOR_TYPE"}
            };

            this._fieldsToReturn = new Dictionary<Detail, IList<string>>();
            var fullFields = BaseFieldsToReturn.Union(new[]
            {
                "DS.SQL_QUERY " + nameof(DatasetEntity.Query), 
                "DS.ORDER_BY_CLAUSE " + nameof(DatasetEntity.OrderByClause), 
                "DS.CONNECTION_ID as " + nameof(DatasetEntity.ParentId),
                "DS.EDITOR_QUERY as " + nameof(DatasetEntity.JSONQuery),
                "DS.EDITOR_TYPE as " + nameof(DatasetEntity.EditorType)
            });

            this._fieldsToReturn.Add(Detail.Full, fullFields.ToList());
            this._fieldsToReturn.Add(Detail.IdAndName, new[] { EntityIdAlias, NameAlias });
            this._fieldsToReturn.Add(Detail.IdOnly, new[] { EntityIdAlias });

            this._datasetPropertyEntityRetriever =
                new DatasetPropertyEntityRetrieverEngine(databaseManager, databaseManager.Name);
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public override IEnumerable<DatasetEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var db = this._databaseManager;
            var whereClauseBuilder = new WhereClauseBuilder(db);

            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "ENTITY_ID", "EB"));
            clauses.Add(whereClauseBuilder.Build(query.ParentId.ValueConvert(), "CONNECTION_ID", DatasetAlias));
            clauses.Add(whereClauseBuilder.Build(query.ObjectId, "OBJECT_ID", "EB"));
            foreach (var integerAdditionalProperty in this._integerAdditionalProperties)
            {
                ICriteria<string> additionalCriteria;
                if (query.AdditionalCriteria.TryGetValue(integerAdditionalProperty.Key, out additionalCriteria))
                {
                    clauses.Add(whereClauseBuilder.Build(additionalCriteria, integerAdditionalProperty.Value, DatasetAlias));
                }
            }

            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3}", fields, GetBaseJoinString(TableAlias, DatasetAlias + ".ENTITY_ID"), whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);
            var datasetEntities = db.Query<DatasetEntity>(sqlQuery, param).ToList();
            
            foreach (var datasetEntity in datasetEntities )
            {
                datasetEntity.DatasetPropertyEntity = GetDatasetPropertyEntity(datasetEntity.EntityId, query, detail);

                if (this.dataSetEditorManager != null && this.dataSetEditorManager.CanMigrate)
                {
                    for (int i = 0; i < datasetEntities.Count; i++)
                    {
                        var migratedExtraData = dataSetEditorManager.Migrate(datasetEntities[i]);
                        datasetEntities[i].JSONQuery = migratedExtraData;
                        datasetEntities[i].StoreId = db.Name;
                    }
                }
            }

            this._permissionRetriever.UpdatePermissions(datasetEntities);
            return datasetEntities;
        }

        public override void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }

        private DatasetPropertyEntity GetDatasetPropertyEntity(string dataSetId, IEntityQuery query, Detail detail)
        {
            if (detail != Detail.Full)
            {
                return null;
            }

            if (query.EntityId == null)
            {
                query = new EntityQuery() { EntityId = new Criteria<string>(OperatorType.Exact, dataSetId) };
            }

            return _datasetPropertyEntityRetriever.GetEntities(query, detail).SingleOrDefault();
        }
    }
}