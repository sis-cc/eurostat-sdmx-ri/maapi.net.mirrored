// -----------------------------------------------------------------------
// <copyright file="DescSourcerRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    public class DataSourceRetrieverEngine : IEntityRetrieverEngine<DataSourceEntity>
    {
        private readonly Database _databaseManager;
        private const string DataSourceAlias = "DS";
        private const string DataSourceDataflow = " Data_Source " + DataSourceAlias + " INNER JOIN DATAFLOW T ON DS.DATA_SOURCE_ID = T.DATA_SOURCE_ID INNER JOIN ARTEFACT A ON T.DF_ID = A.ART_ID ";
        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;

        public DataSourceRetrieverEngine(Database databaseManager)
        {
            this._databaseManager = databaseManager;
            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }

            this._databaseManager = databaseManager;
            this._fieldsToReturn = new Dictionary<Detail, IList<string>>();
            this._fieldsToReturn.Add(Detail.Full, new[]
            {
                "DS.DATA_SOURCE_ID as " + nameof(DataSourceEntity.EntityId), "DATA_URL as " + nameof(DataSourceEntity.DataUrl), "WSDL_URL as " + nameof(DataSourceEntity.WSDLUrl),
                "WADL_URL as " + nameof(DataSourceEntity.WADLUrl), "IS_SIMPLE as " + nameof(DataSourceEntity.IsSimple)
                , "IS_REST as " + nameof(DataSourceEntity.IsRest), "IS_WS as " + nameof(DataSourceEntity.IsWs),
                "A.ID as DFID", "A.VERSION1", "A.VERSION2", "A.VERSION3", "A.AGENCY as DFAG"
            });
            this._fieldsToReturn.Add(Detail.IdOnly, new[] { "DATA_SOURCE_ID as " + nameof(DataSourceEntity.EntityId) });
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<DataSourceEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var db = this._databaseManager;
            var whereClauseBuilder = new WhereClauseBuilder(db);

            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "DATA_SOURCE_ID",DataSourceAlias));
            clauses.Add(whereClauseBuilder.BuildFromUrn(query.ParentId, "ID", "AGENCY", new[] { "VERSION1", "VERSION2", "VERSION3" }, "A"));
            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3}", fields, DataSourceDataflow, whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);
            var rows = db.Query(sqlQuery, param).ToList();
            return ExtractDataSources(rows);
        }

        private IEnumerable<DataSourceEntity> ExtractDataSources(List<dynamic> rows)
        {
            List<DataSourceEntity> dataSources = new List<DataSourceEntity>();
            foreach(var row in rows){
                dataSources.Add(ExtractDataSource(row));
            }
            return dataSources;
        }

        private DataSourceEntity ExtractDataSource(dynamic row)
        {
            var dataSource = new DataSourceEntity()
            {
                DataUrl = row.DataUrl,
                WADLUrl = row.WADLUrl,
                WSDLUrl = row.WSDLUrl,
                IsRest = row.IsRest,
                IsWs = row.IsWs,
                IsSimple = row.IsSimple,
                EntityId = row.EntityId.ToString()
            };


            if (!string.IsNullOrWhiteSpace(row.DFID))
            {
                var version = string.Join(".", new[] { row.VERSION1, row.VERSION2, row.VERSION3 }.Where(l => l !=null).Select(l => l.ToString(CultureInfo.InvariantCulture)));

                dataSource.ParentId = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)
                    .GenerateUrn(row.DFAG, row.DFID, version).ToString();
            }
            return dataSource;
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }
    }
}