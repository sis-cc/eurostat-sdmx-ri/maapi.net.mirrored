namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class PermissionsDatabaseDetails
    {
        public string DataflowTable { get; set; }
        public string CategoryTable { get; set; }
        public string ForeignKey { get; set; }
        public string EntityIdColumn { get; set; }
        public string EntityTable { get; set; }
    }
}