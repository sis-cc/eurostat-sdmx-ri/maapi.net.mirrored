﻿// -----------------------------------------------------------------------
// <copyright file="EntityPropertiesExtractor.cs" company="EUROSTAT">
//   Date Created : 2017-05-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class EntityPropertiesExtractor : IEntityPropertiesExtractor
    {
        private readonly IEntityIdFromUrn _entityIdFromUrn;

        public EntityPropertiesExtractor(IEntityIdFromUrn entityIdFromUrn)
        {
            this._entityIdFromUrn = entityIdFromUrn;
        }

        public IEnumerable<KeyValuePair<string, object>> GetFrom(IEntity entity)
        {
            var entityValues = new Dictionary<string, object>();
            var parentIdProperty = entity.GetType().GetProperty(nameof(Entity.ParentId));
            var parentIdValue = parentIdProperty.GetValue(entity);
            if (parentIdValue != null)
            {
                if (entity.TypeOfEntity == EntityType.MappingSet)
                {
                    parentIdValue = this._entityIdFromUrn.Get(parentIdValue.ToString());
                }
                entityValues.Add(parentIdProperty.Name, parentIdValue);
            }

            if (entity.TypeOfEntity == EntityType.DataSetColumn || entity.TypeOfEntity == EntityType.DataSet || entity.TypeOfEntity == EntityType.MappingSet || entity.TypeOfEntity == EntityType.DdbConnectionSettings)
            {
                var nameProperty = entity.GetType().GetProperty(nameof(SimpleNameableEntity.Name));
                var nameValue = nameProperty.GetValue(entity);
                entityValues.Add(nameProperty.Name, nameValue);
                
                var descriptionProperty = entity.GetType().GetProperty(nameof(SimpleNameableEntity.Description));
                var descriptionValue = descriptionProperty.GetValue(entity);
                entityValues.Add(descriptionProperty.Name, descriptionValue);
            }

            if (entity.TypeOfEntity == EntityType.MappingSet)
            {
                //todo this should be a field and used for all entities that need a user id 
                var userid = "userId";
                //todo this should be taken from the context of the application
                var userIdValue = "1";
                entityValues.Add(userid,userIdValue);
            }
           
            return entityValues;
        }
    }
}