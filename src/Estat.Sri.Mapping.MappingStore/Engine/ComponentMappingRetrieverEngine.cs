// -----------------------------------------------------------------------
// <copyright file="ComponentMappingRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-20
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using log4net;
    using Org.Sdmxsource.Util;

    public class ComponentMappingRetrieverEngine : IEntityRetrieverEngine<ComponentMappingEntity>
    {
        private const string ComponentMappingAlias = "CM";

        private const string JoinString = "N_COMPONENT_MAPPING " + ComponentMappingAlias
                                                                 + " JOIN N_MAPPING_SET MSET on MSET.STR_MAP_SET_ID = CM.STR_MAP_SET_ID " +
                                                                 "LEFT OUTER JOIN N_MAPPING_WITH_COLUMN MCOL ON MCOL.MAP_ID = CM.MAP_ID " +
                                                                 "LEFT OUTER JOIN N_MAPPING_CONST_DEFAULT_VALUE CDV ON CDV.MAP_ID = CM.MAP_ID " +
                                                                 "LEFT OUTER JOIN (select TR.MAP_C_ID, count(*) as TRULES from N_VALUE_MAPPING_RULES TR group by TR.MAP_C_ID) T " +
                                                                 "    on MCOL.MAP_C_ID = T.MAP_C_ID";

        private static readonly ILog _log = LogManager.GetLogger(typeof(ComponentMappingRetrieverEngine));

        private readonly Database _databaseManager;

        private readonly IDictionary<Detail, IList<string>> _fieldsToReturn;

        private readonly IDictionary<string, string> _integerAdditionalProperties;

        public ComponentMappingRetrieverEngine(Database databaseManager)
        {
            this._databaseManager = databaseManager;
            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }
            this._integerAdditionalProperties = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                //{"ParentId", "COMPONENT_ID"}, this is varchar and can also be taken from the IEntityQuery.ParentId
                {"ColumnId", "COL_ID"}
            };

            this._databaseManager = databaseManager;
            this._fieldsToReturn = new Dictionary<Detail, IList<string>>();
            this._fieldsToReturn.Add(Detail.Full, new[]
            {
                string.Format(CultureInfo.InvariantCulture,"CM.MAP_ID as {0}", nameof(ComponentMappingEntity.EntityId).ToUpper(CultureInfo.InvariantCulture)),
                string.Format(CultureInfo.InvariantCulture,"CM.COMPONENT_ID as {0}" , nameof(ComponentMappingEntity.Name).ToUpper(CultureInfo.InvariantCulture)),
                //string.Format(CultureInfo.InvariantCulture,"CM.Type as {0}",nameof(ComponentMappingEntity.Type).ToUpper(CultureInfo.InvariantCulture)),
                string.Format(CultureInfo.InvariantCulture,"CM.STR_MAP_SET_ID as {0}",nameof(ComponentMappingEntity.ParentId).ToUpper(CultureInfo.InvariantCulture)),
                "CDV.CONSTANT_OR_DEFAULT",
                "MCOL.DATASET_COLUMN_NAME as DATASETCOLUMNNAME",
                "MSET.SOURCE_DS as DATASETID",
                "CM.IS_METADATA",
                "T.TRULES as TRANSCODINGS"
                //"META.SDMX_ID as METADATA_ATTRIBUTE_SDMX_ID"
            });

            this._fieldsToReturn.Add(Detail.IdAndName, new[] {
                string.Format(CultureInfo.InvariantCulture,"CM.MAP_ID as {0}" , nameof(ComponentMappingEntity.EntityId).ToUpper(CultureInfo.InvariantCulture)),
                string.Format(CultureInfo.InvariantCulture,"CM.COMPONENT_ID as {0}",nameof(ComponentMappingEntity.Name).ToUpper(CultureInfo.InvariantCulture))});
            this._fieldsToReturn.Add(Detail.IdOnly, new[]
            {
                string.Format(CultureInfo.InvariantCulture,"CM.MAP_ID as {0}",nameof(ComponentMappingEntity.EntityId).ToUpper(CultureInfo.InvariantCulture)),
                string.Format(CultureInfo.InvariantCulture,"CM.COMPONENT_ID as {0}",nameof(ComponentMappingEntity.Name).ToUpper(CultureInfo.InvariantCulture))
            });
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<ComponentMappingEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var db = this._databaseManager;
            var whereClauseBuilder = new WhereClauseBuilder(db);

            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "MAP_ID", ComponentMappingAlias));
            clauses.Add(whereClauseBuilder.Build(query.ParentId, "STR_MAP_SET_ID", ComponentMappingAlias));
            clauses.Add(whereClauseBuilder.Build(query.ObjectId, "COMPONENT_ID", ComponentMappingAlias));
            foreach (var integerAdditionalProperty in this._integerAdditionalProperties)
            {
                ICriteria<string> additionalCriteria;
                if (query.AdditionalCriteria.TryGetValue(integerAdditionalProperty.Key, out additionalCriteria))
                {
                    clauses.Add(whereClauseBuilder.Build(additionalCriteria.ValueConvert(), integerAdditionalProperty.Value, ComponentMappingAlias));
                }
            }
            var fields = string.Join(", ", this._fieldsToReturn[detail]);
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            var sqlQuery = string.Format(CultureInfo.InvariantCulture, "select {0} FROM {1} {2} {3}", fields, JoinString, whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);
            var rows = db.Query(sqlQuery, param).ToList();
            return this.ExtractComponentMappings(rows);
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }

        private List<ComponentMappingEntity> ExtractComponentMappings(List<dynamic> rows)
        {
            var entities = new List<ComponentMappingEntity>();
            var groupBy = rows.GroupBy(item => item.ENTITYID);
            foreach (var componentMappingRows in groupBy)
            {
                dynamic componentMapping = componentMappingRows.First();
                var componentMappingEntity = new ComponentMappingEntity
                {
                    
                    Type = "A",//we default to A for now
                    IsMetadata = Convert.ToBoolean(componentMapping.IS_METADATA)
                };
                if (componentMapping.PARENTID != null)
                {
                    componentMappingEntity.SetParentId((long)componentMapping.PARENTID);
                }
                componentMappingEntity.SetEntityId((long)componentMapping.ENTITYID);

                componentMappingEntity.Component = new Component { ObjectId = (string)componentMapping.NAME };

                if (componentMappingEntity.IsMetadata)
                {
                    // the COMPONENT_ID also stores the component id
                    componentMappingEntity.MetadataAttributeSdmxId = componentMappingEntity.Component.ObjectId;
                }

                if (componentMapping.TRANSCODINGS != null)
                {
                    // Transcoding entity doesn't exist in the database. We us as placeholder 
                    // the component mapping entity id
                    // unless something else is more convenient
                    componentMappingEntity.TranscodingId = componentMappingEntity.EntityId;
                }

                foreach (var row in componentMappingRows)
                {
                    if (componentMapping.DATASETCOLUMNNAME != null) // with columns
                    {
                        // rows may either contain different columns or constant_or_default values
                        string defaultValue = (string)row.CONSTANT_OR_DEFAULT;
                        if (!string.IsNullOrWhiteSpace(defaultValue))
                        {
                            componentMappingEntity.DefaultValues.Add(defaultValue);//it's a Set, so will not add it if already there
                        }
                        if (!componentMappingEntity.GetColumns().Any(c => c.Name.Equals(row.DATASETCOLUMNNAME)))
                        {
                            var dataSetColumnEntity = new DataSetColumnEntity();
                            dataSetColumnEntity.SetParentId((long)row.DATASETID);
                            dataSetColumnEntity.Name = row.DATASETCOLUMNNAME;
                            componentMappingEntity.GetColumns().Add(dataSetColumnEntity);
                        }
                    }
                    else // or not
                    {
                        componentMappingEntity.ConstantValues.Add(row.CONSTANT_OR_DEFAULT);
                    }
                }

                // validation
                if (!componentMappingEntity.HasColumns && !componentMappingEntity.HasConstantValue)
                {
                    _log.ErrorFormat("Mapping without columns or constant value detected with entity id {0}", componentMappingEntity.EntityId);
                    if (string.IsNullOrWhiteSpace(componentMappingEntity?.Component?.ObjectId) && "A".Equals(componentMappingEntity.Type))
                    {
                        _log.ErrorFormat("Mapping with entity id={0} seems corrupted skipping", componentMappingEntity.EntityId);
                        continue; 
                    }
                    componentMappingEntity.ConstantValue = "PLACEHOLDER";
                }
                this.UpdateTranscodingScript(componentMappingEntity);
                entities.Add(componentMappingEntity);
            }


            //foreach (var row in rows)
            //{
            //    var componentMappingEntity = entities.Find(item => item.EntityId == row.ENTITYID?.ToString());
            //    if (componentMappingEntity != null)
            //    {
            //        if (row.COLUMNID != null)
            //        {
            //            var column = new DataSetColumnEntity()
            //            {
            //                ParentId = row.DATASETID?.ToString(),
            //                Description = row.DATACOLUMNDESCRIPTION,
            //                EntityId = row.COLUMNID?.ToString(),
            //                LastRetrieval = row.LASTRETRIVAL,
            //                Name = row.DATASETCOLUMNNAME,
            //            };
            //            componentMappingEntity.Columns.Add(column);
            //        }
            //    }
            //    else
            //    {
            //        componentMappingEntity = new ComponentMappingEntity
            //        {
            //            EntityId = row.ENTITYID?.ToString(),
            //            Component = new Component {Id = row.COMPONENTID?.ToString(), Name = row.NAME},
            //            Constant = row.CONSTANT,
            //            Name = row.NAME,
            //            Type = row.TYPE,
            //            TranscodingId = row.TRANSCODINGID?.ToString(),
            //            ParentId = row.PARENTID?.ToString(),
            //            Columns = new List<DataSetColumnEntity>()
            //        };
            //        if (row.COLUMNID != null)
            //        {
            //            var dataSetColumnEntity = new DataSetColumnEntity();
            //            dataSetColumnEntity.ParentId = row.DATASETID?.ToString();
            //            dataSetColumnEntity.Description = row.DATACOLUMNDESCRIPTION;
            //            dataSetColumnEntity.EntityId = row.COLUMNID?.ToString();
            //            dataSetColumnEntity.LastRetrieval = row.LASTRETRIVAL;
            //            dataSetColumnEntity.Name = row.DATASETCOLUMNNAME;
            //            componentMappingEntity.Columns.Add(dataSetColumnEntity);
            //        }
            //        entities.Add(componentMappingEntity);
            //    }
            //}
            return entities;
        }

        private void UpdateTranscodingScript(ComponentMappingEntity componentMappingEntity)
        {
            var id = componentMappingEntity.GetEntityId();
            var paramName = this._databaseManager.BuildParameterName(nameof(id));
            string query = string.Format(CultureInfo.InvariantCulture, "SELECT TR_SCRIPT_ID,SCRIPT_TITLE,SCRIPT_CONTENT FROM TRANSCODING_SCRIPT WHERE TR_ID = {0}", paramName);
            var objects = this._databaseManager.Query<dynamic>(query, new { id }).ToList();
            componentMappingEntity.Script = new List<TranscodingScriptEntity>();

            foreach (var item in objects)
            {
                componentMappingEntity.Script.Add(new TranscodingScriptEntity()
                {
                    EntityId = item.TR_SCRIPT_ID.ToString(),
                    ParentId = componentMappingEntity.EntityId,
                    ScriptContent = item.SCRIPT_CONTENT,
                    ScriptTile = item.SCRIPT_TITLE
                });
            }
        }
    }
}