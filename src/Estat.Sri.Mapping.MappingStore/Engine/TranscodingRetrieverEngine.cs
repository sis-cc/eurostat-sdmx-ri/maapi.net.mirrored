// -----------------------------------------------------------------------
// <copyright file="TranscodingRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-20
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;
using DataSetColumnEntity = Estat.Sri.Mapping.Api.Model.DataSetColumnEntity;
using TimeTranscodingEntity = Estat.Sri.Mapping.Api.Model.TimeTranscodingEntity;
using TranscodingEntity = Estat.Sri.Mapping.Api.Model.TranscodingEntity;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System.Data;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    using TimeExpressionEntity = Estat.Sri.Mapping.MappingStore.Model.TimeExpressionEntity;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.MappingStore.Helpers;
    using Estat.Sri.MappingStore.Store.Extension;

    public class TranscodingRetrieverEngine : IEntityRetrieverEngine<TranscodingEntity>
    {
        private const string TranscodingAlias = "TR";

        private const string TranscodingQuery = "SELECT TR.MAP_ID, TR.TR_ID, TR.EXPRESSION FROM TRANSCODING TR {0} {1}";

        private const string TimeTranscodingSqlFormat = @"SELECT T.FREQ, T.YEAR_COL_ID, T.PERIOD_COL_ID, T.DATE_COL_ID, T.EXPRESSION , DC.NAME as DATE_NAME, YC.NAME as YEAR_NAME, PC.NAME as PERIOD_NAME
FROM TIME_TRANSCODING T 
LEFT OUTER JOIN DATASET_COLUMN DC ON DC.COL_ID = T.DATE_COL_ID
LEFT OUTER JOIN DATASET_COLUMN YC ON YC.COL_ID = T.YEAR_COL_ID
LEFT OUTER JOIN DATASET_COLUMN PC ON PC.COL_ID = T.PERIOD_COL_ID
WHERE TR_ID = {0}  ";

        private readonly Database _databaseManager;

        private readonly IMappingStoreManager _mappingStoreManager;

        private readonly AdvanceTimeTranscodingRetriever _advanceTimeTranscodingRetriever;

        private readonly string _storeId;

        public TranscodingRetrieverEngine(Database databaseManager, IMappingStoreManager mappingStoreManager, string storeId)
        {
            this._databaseManager = databaseManager;
            this._mappingStoreManager = mappingStoreManager;
            this._storeId = storeId;
            if (databaseManager == null)
            {
                throw new ArgumentNullException(nameof(databaseManager));
            }

            _advanceTimeTranscodingRetriever = new AdvanceTimeTranscodingRetriever(databaseManager);

            this._databaseManager = databaseManager;
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="detail">The detail.</param>
        /// <returns>The matching entities</returns>
        public IEnumerable<TranscodingEntity> GetEntities(IEntityQuery query, Detail detail)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var db = this._databaseManager;
            var whereClauseBuilder = new WhereClauseBuilder(db);

            var clauses = new List<WhereClauseParameters>();
            clauses.Add(whereClauseBuilder.Build(query.EntityId.ValueConvert(), "TR_ID", TranscodingAlias));
            clauses.Add(whereClauseBuilder.Build(query.ParentId, "MAP_ID", TranscodingAlias));
            var normalizedClauses = clauses.Where(parameters => parameters != null).ToArray();
            var whereClauses = normalizedClauses.Select(parameters => parameters.WhereClause);
            var whereStatement = normalizedClauses.Length > 0 ? "WHERE" : string.Empty;
            var sqlQuery = string.Format(CultureInfo.InvariantCulture,TranscodingQuery, whereStatement, string.Join(" AND ", whereClauses));
            var valuePairs = normalizedClauses.SelectMany(parameters => parameters.Parameters).ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);
            var param = new DynamicParameters(valuePairs);
            var transcodingEntities = db.Query<dynamic>(sqlQuery, param).Select(t => new TranscodingEntity() { ParentId = Convert.ToString((long)t.MAP_ID, CultureInfo.InvariantCulture), EntityId = Convert.ToString((long)t.TR_ID, CultureInfo.InvariantCulture), StoreId = db.Name, Expression = t.EXPRESSION as string}).ToArray();

            for (var index = 0; index < transcodingEntities.Length; index++)
            {
                var transcodingEntity = transcodingEntities[index];
                this.UpdateTranscodingScript(transcodingEntity, db);

                // transcodings with scripts do not have a time dimension
                if (!transcodingEntity.HasScript())
                {
                    this.UpdateTranscodingTime(transcodingEntity, db);
                    if (!transcodingEntity.HasTimeTrascoding())
                    {
                        transcodingEntity.AdvancedTimeTranscoding =
                            _advanceTimeTranscodingRetriever.Retrieve(transcodingEntity.GetEntityId());
                    }
                }
            }

            return transcodingEntities;
        }

        private void UpdateTranscodingTime(TranscodingEntity transcodingEntity, Database db)
        {
            var transcodingId = transcodingEntity.GetEntityId();
            var parameter = db.BuildParameterName(nameof(transcodingId));
            var dynamicObject = new DynamicParameters();
            dynamicObject.Add(nameof(transcodingId), transcodingId, DbType.Int64);
            var objects = db.Query(
                string.Format(CultureInfo.InvariantCulture, TimeTranscodingSqlFormat, parameter), dynamicObject).ToList();

            if (!objects.Any())
            {
                return;
            }
            transcodingEntity.TimeTranscoding = new List<TimeTranscodingEntity>();

            List<TranscodingRuleEntity> transcodingRuleEntitys = new List<TranscodingRuleEntity>();

            if (_mappingStoreManager != null && MappingStoreVersionHelper.IsVersion53(_mappingStoreManager, this._storeId))
            {
                transcodingRuleEntitys = new TranscodingRuleRetrieverEngine53(this._databaseManager).GetEntities(new EntityQuery { ParentId = new Criteria<string>(OperatorType.Exact, transcodingEntity.EntityId) }, Detail.Full).ToList();
            }
            else
            {
                transcodingRuleEntitys = new TranscodingRuleRetrieverEngine(this._databaseManager).GetEntities(new EntityQuery { ParentId = new Criteria<string>(OperatorType.Exact, transcodingEntity.EntityId) }, Detail.Full).ToList();
            }

            foreach (var timeTranscoding in objects)
            {
                var timeTranscodingEntity = new TimeTranscodingEntity()
                { ParentId = transcodingEntity.EntityId,
                    Frequency = timeTranscoding.FREQ };

                var dateColumnId = timeTranscoding.DATE_COL_ID?.ToString();
                if (dateColumnId != null)
                {
                    timeTranscodingEntity.DateColumn = new DataSetColumnEntity() { EntityId = dateColumnId, Name = timeTranscoding.DATE_NAME };
                }

                var transcoding = new MappingStoreRetrieval.Model.MappingStoreModel.TimeTranscodingEntity(timeTranscoding.FREQ, Convert.ToInt64(transcodingEntity.EntityId))
                {
                    Expression = timeTranscoding.EXPRESSION
                };
                var timeExpressionEntity = TimeExpressionParser.CreateExpression(transcoding);
                if (timeTranscoding.PERIOD_COL_ID != null)
                {
                    timeTranscodingEntity.Period = new PeriodTimeTranscoding();
                    timeTranscodingEntity.Period.Start = timeExpressionEntity.PeriodStart;
                    timeTranscodingEntity.Period.Length = timeExpressionEntity.PeriodLength;
                    timeTranscodingEntity.Period.Column = new DataSetColumnEntity { EntityId = timeTranscoding.PERIOD_COL_ID?.ToString(), Name = timeTranscoding.PERIOD_NAME };

                    PeriodObject codeList;

                    if (PeriodCodelist.PeriodCodelistIdMap.TryGetValue(timeTranscodingEntity.Frequency, out codeList)){
                        timeTranscodingEntity.Period.Rules = transcodingRuleEntitys.Where(x => codeList.Codes.Contains(x.DsdCodeEntity.ObjectId)).ToList();
                    }
                }

                if (timeTranscoding.YEAR_COL_ID != null)
                {
                    timeTranscodingEntity.Year = new TimeTranscoding();
                    timeTranscodingEntity.Year.Start = timeExpressionEntity.YearStart;
                    timeTranscodingEntity.Year.Length = timeExpressionEntity.YearLength;
                    timeTranscodingEntity.Year.Column = new DataSetColumnEntity { EntityId = timeTranscoding.YEAR_COL_ID?.ToString(), Name = timeTranscoding.YEAR_NAME };
                }
                timeTranscodingEntity.IsDateTime = timeExpressionEntity.IsDateTime;
                transcodingEntity.TimeTranscoding.Add(timeTranscodingEntity);
            }
        }

        private void UpdateTranscodingScript(TranscodingEntity transcodingEntity, Database db)
        {
            var id = transcodingEntity.GetEntityId();
            var paramName = db.BuildParameterName(nameof(id));
            string query = string.Format(CultureInfo.InvariantCulture, "SELECT TR_SCRIPT_ID,SCRIPT_TITLE,SCRIPT_CONTENT FROM TRANSCODING_SCRIPT WHERE TR_ID = {0}", paramName);
            var objects = db.Query<dynamic>(query, new { id } ).ToList();
            transcodingEntity.Script = new List<TranscodingScriptEntity>();

            foreach (var item in objects)
            {
                transcodingEntity.Script.Add(new TranscodingScriptEntity()
                {
                    EntityId = item.TR_SCRIPT_ID.ToString(),
                    ParentId = transcodingEntity.EntityId,
                    ScriptContent = item.SCRIPT_CONTENT,
                    ScriptTile = item.SCRIPT_TITLE
                });
            }
        }

        public void WriteEntities(IEntityQuery query, Detail detail, IEntityStreamingWriter entityWriter)
        {
            entityWriter.Write(GetEntities(query, detail));
        }
    }
}