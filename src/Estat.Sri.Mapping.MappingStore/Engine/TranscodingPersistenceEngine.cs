// -----------------------------------------------------------------------
// <copyright file="TranscodingPersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2017-5-19
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using Dapper;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Model.AdvancedTime;
using Estat.Sri.Mapping.Api.Utils;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Helper;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;
using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Util.Extensions;
using DataSetColumnEntity = Estat.Sri.Mapping.Api.Model.DataSetColumnEntity;
using TimeTranscodingEntity = Estat.Sri.Mapping.Api.Model.TimeTranscodingEntity;
using TranscodingEntity = Estat.Sri.Mapping.Api.Model.TranscodingEntity;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System.Data;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.Utils;
    using Estat.Sri.Utils.Model;

    /// <summary>
    /// Transcoding entity does not exist any more as a table.
    /// </summary>
    [Obsolete("Time transcoding moved to TimeDimemnsionMappingEntity and related retrievers/persistance")]
    public class TranscodingPersistenceEngine : IEntityPersistenceForUpdate<TranscodingEntity>
    {
        private readonly DatabaseManager _databaseManager;
        private readonly string _mappingStoreIdentifier;

        private readonly Database _mappingStoreDatabase;
        private readonly EntityImporterWithIdMapping _entityImporterWithIdMapping = new EntityImporterWithIdMapping();

        /// <summary>
        ///     The SDMX code dictionary.
        /// </summary>
        private readonly IDictionary<TimeFormatEnumType, IDictionary<string, long>> _sdmxCodeDictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="TranscodingPersistenceEngine"/> class.
        /// </summary>
        /// <param name="databaseManager">The database manager.</param>
        /// <param name="mappingStoreIdentifier">The mapping store identifier.</param>
        public TranscodingPersistenceEngine(DatabaseManager databaseManager, string mappingStoreIdentifier)
        {
            this._databaseManager = databaseManager;
            this._mappingStoreIdentifier = mappingStoreIdentifier;
            this._mappingStoreDatabase = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            var sdmxCodeMapRetriever = new SdmxCodeMapRetriever(this._mappingStoreDatabase);
            this._sdmxCodeDictionary = sdmxCodeMapRetriever.BuildSdmxCodeIdToSysIdMap();
        }

        /// <summary>
        /// Persists the specified update information.
        /// </summary>
        /// <param name="patchRequest"></param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        public void Update(PatchRequest patchRequest, EntityType entityType, string entityId)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);

            database.ExecuteCodeWithTransaction((connection) =>
            {
                var commandDefinitions = new List<CommandDefinition>();
                this.CreateCommandForTranscoding(patchRequest, entityId, commandDefinitions, database);
                this.CreateCommandForScript(patchRequest, entityId, commandDefinitions, database);
                this.CreateCommandForTimeTranscoding(patchRequest, entityId, commandDefinitions, database, connection);
                return commandDefinitions;
            });
        }

        private void CreateCommandForTimeTranscoding(PatchRequest patchRequest, string entityId,
            List<CommandDefinition> commandDefinitions, Database database, DbConnection connection)
        {
            var patchDocuments = patchRequest.FindAll(item => item.HasPath("time_transcoding"));
            if (patchDocuments.Any())
            {
                var groupedByOperation = patchDocuments.GroupBy(item => item.Op);
                foreach (var group in groupedByOperation)
                {
                    switch (group.Key)
                    {
                        case "remove":
                            this.CreateDeleteCommand(group.ToList(), commandDefinitions, database,
                                DatabaseInformationType.TimeTranscoding);
                            break;
                        case "replace":
                            var entity = CreateTimeTranscodingEntity(@group);
                            var timeTranscodingPathAndValues = this.CreateTimeTranscodingPathAndValues(entity,
                                this.GetExpressionForUpdate(entity, entityId, connection));
                            commandDefinitions.AddRange(
                                this.CreateTimeTranscodingUpdateCommand(entityId, timeTranscodingPathAndValues));

                            break;
                        case "add":
                            var timeTranscodingEntity = CreateTimeTranscodingEntity(@group);
                            commandDefinitions.AddRange(
                                this.CreateInsertTimeTranscodingCommands(entityId, timeTranscodingEntity));

                            break;
                    }
                }
            }
        }

        private IEnumerable<CommandDefinition> CreateTimeTranscodingUpdateCommand(string entityId,
            Dictionary<string, object> timeTranscodingPathAndValues)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);

            var updateInfoAction = new UpdateInfoAction()
            {
                DataInformationType = DatabaseInformationType.TimeTranscoding,
                SqlStatementType = SqlStatementType.Update,
                PathAndValues = timeTranscodingPathAndValues
            };
            var commandDefinitionManager =
                new CommandDefinitionManager(
                    new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
            var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType,
                updateInfoAction.SqlStatementType);
            return commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, database);
        }

        private static TimeTranscodingEntity CreateTimeTranscodingEntity(IGrouping<string, PatchDocument> @group)
        {
            var patchDocuments = @group.ToList();
            var isDateTime = @group.FirstOrDefault(item => item.HasPath("is_date_time"));
            //var transcodingRuleEntities = JsonConvert.DeserializeObject<List<TranscodingRuleEntity>>(@group.FirstOrDefault(item => item.HasPath("items"))?.Value.ToString());

            return new TimeTranscodingEntity()
            {
                DateColumn =
                    new DataSetColumnEntity()
                    {
                        EntityId = @group.FirstOrDefault(item => item.HasPath("data_column"))?.Value.ToString()
                    },
                Frequency = @group.FirstOrDefault(item => item.HasPath("frequency"))?.Value.ToString(),
                IsDateTime = isDateTime != null && bool.Parse(isDateTime.Value.ToString()),
                Period = new PeriodTimeTranscoding()
                {
                    Column = new DataSetColumnEntity()
                    {
                        EntityId = @group.FirstOrDefault(item => item.HasPath("column") && item.HasPath("period"))
                            ?.Value.ToString()
                    },
                    Start = group.PeriodStart(),
                    Length = group.PeriodLength(),
                    //  Rules = transcodingRuleEntities
                },
                Year = new TimeTranscoding()
                {
                    Column = new DataSetColumnEntity()
                    {
                        EntityId = @group.FirstOrDefault(item => item.HasPath("column") && item.HasPath("year"))
                            ?.Value.ToString()
                    },
                    Start = group.YearStart(),
                    Length = group.YearLength(),
                }
            };
        }

        private void CreateCommandForScript(PatchRequest patchRequest, string entityId,
            List<CommandDefinition> commandDefinitions, Database database)
        {
            var patchDocuments = patchRequest.FindAll(item => item.HasPath("script"));
            if (patchDocuments.Any())
            {
                var groupedByOperation = patchDocuments.GroupBy(item => item.Op);
                foreach (var group in groupedByOperation)
                {
                    switch (group.Key)
                    {
                        case "remove":
                            this.CreateDeleteCommand(group.ToList(), commandDefinitions, database,
                                DatabaseInformationType.TranscodingScript);
                            break;
                        case "replace":
                            this.CreateDeleteCommand(group.ToList(), commandDefinitions, database,
                                DatabaseInformationType.TranscodingScript);
                            foreach (var patchDocument in group)
                            {
                                var scriptEntity =
                                    JsonConvert.DeserializeObject<TranscodingScriptEntity>(
                                        patchDocument.Value.ToString());
                                commandDefinitions.AddRange(
                                    this.CreateInsertTranscodingScriptCommands(Convert.ToInt64(entityId),
                                        scriptEntity));
                            }

                            break;
                        case "add":
                            foreach (var patchDocument in group)
                            {
                                var scriptEntity =
                                    JsonConvert.DeserializeObject<TranscodingScriptEntity>(
                                        patchDocument.Value.ToString());
                                commandDefinitions.AddRange(
                                    this.CreateInsertTranscodingScriptCommands(Convert.ToInt64(entityId),
                                        scriptEntity));
                            }

                            break;
                    }
                }
            }
        }

        private void CreateDeleteCommand(IEnumerable<PatchDocument> documents,
            List<CommandDefinition> commandDefinitions, Database database,
            DatabaseInformationType databaseInformationType)
        {
            foreach (var patchDocument in documents)
            {
                var updateInfo = new UpdateInfoAction()
                {
                    SqlStatementType = SqlStatementType.Delete, DataInformationType = databaseInformationType,
                };

                var entityId = patchDocument.Path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Last();
                commandDefinitions.AddRange(this.GetCommandFromUpdateInfo(updateInfo, entityId, database));
            }
        }

        private void CreateCommandForTranscoding(PatchRequest patchRequest, string entityId,
            List<CommandDefinition> commandDefinitions, Database database)
        {
            var patchDocuments =
                patchRequest.FindAll(item => !item.HasPath("time_transcoding") && !item.HasPath("script"));

            var transcodingAction = new UpdateInfoAction()
            {
                DataInformationType = DatabaseInformationType.Transcoding,
                SqlStatementType = SqlStatementType.Update,
                PathAndValues = new Dictionary<string, object>()
            };

            var patchDocument = patchDocuments.Find(item => item.HasPath("expression"));

            if (patchDocument != null)
            {
                transcodingAction.PathAndValues.Add(TranscodingDataTableInformations.Expression.ModelName,
                    patchDocument.Value);
            }

            commandDefinitions.AddRange(this.GetCommandFromUpdateInfo(transcodingAction, entityId, database));
        }

        private IEnumerable<CommandDefinition> GetCommandFromUpdateInfo(UpdateInfoAction updateInfoAction,
            string entityId, Database mappingStoreDabase)
        {
            var commandDefinitionManager =
                new CommandDefinitionManager(
                    new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
            var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType,
                updateInfoAction.SqlStatementType);
            return commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, mappingStoreDabase);
        }

        public IEnumerable<long> Add(IList<TranscodingEntity> entities)
        {
            var ids = new List<long>();
            entities.ToList().ForEach(item => ids.Add(this.Add(item)));
            return ids;
        }

        public long Add(TranscodingEntity entity)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            long transcodingId = entity.GetParentId(); // Transcoding doesn't existing any more so parent is the entityid
            // parent can be the N_MAPPING_WITH_COLUMN.MAP_C_ID For non time transcoding. This use is deprecated
            // or N_COMPONENT_MAPPING.MAP_ID for transcoding script. Not used by MAWEB at the moment
            // or N_MAPPING_SET.STR_MAP_SET_ID for time transcoding
            using (var context = new ContextWithTransaction(database))
            {
                var databaseUnderContext = context.DatabaseUnderContext;
             
                var definitions = new List<CommandDefinition>();
                if (entity.HasTimeTrascoding())
                {

                    // TODO get actual Frequency Diemension ID from Frequencyid
                    TimeTranscodingAdvancedEntity advancedEntity = TimeTranscodingConversionHelper
                        .Convert(entity.TimeTranscoding, "FREQ");
                    
                    var advanceTimeTranscodingPersistEngine =
                        new AdvanceTimeTranscodingPersistEngine(databaseUnderContext);
                    // add advanced time transcoding
                    advanceTimeTranscodingPersistEngine.Add(advancedEntity, transcodingId);
                }
                else if (entity.HasScript())
                {
                    foreach (var transcodingScriptEntity in entity.Script)
                    {
                        definitions.AddRange(
                            this.CreateInsertTranscodingScriptCommands(transcodingId, transcodingScriptEntity));
                    }
                }
                else if (entity.AdvancedTimeTranscoding != null)
                {
                    var advanceTimeTranscodingPersistEngine =
                        new AdvanceTimeTranscodingPersistEngine(databaseUnderContext);
                    // add advanced time transcoding
                    advanceTimeTranscodingPersistEngine.Add(entity.AdvancedTimeTranscoding, transcodingId);
                }

                if (definitions.Count > 0)
                {
                    context.ExecuteCommandsWithReturnId(definitions);
                }

                // MySQL/MariaDB HACK FIXME
                context.Connection.Close();
                context.Complete();
            }

            return transcodingId;
        }

       


        private long GetSysId(TimeFormatEnumType freq, string codeId)
        {
            IDictionary<string, long> codeMap;
            if (this._sdmxCodeDictionary.TryGetValue(freq, out codeMap))
            {
                long sysId;
                if (codeMap.TryGetValue(codeId, out sysId))
                {
                    return sysId;
                }
            }

            throw new InvalidOperationException("_sdmxCodeDictionary not initialized");
        }

        private IEnumerable<CommandDefinition> CreateInsertTranscodingScriptCommands(long transcodingId,
            TranscodingScriptEntity transcodingScriptEntity)
        {
            return this.InsertInformation(DatabaseInformationType.TranscodingScript,
                new Dictionary<string, object>
                {
                    { nameof(TranscodingEntity.ParentId), transcodingId },
                    { nameof(TranscodingScriptEntity.ScriptContent), transcodingScriptEntity.ScriptContent },
                    { nameof(TranscodingScriptEntity.ScriptTile), transcodingScriptEntity.ScriptTile }
                }, transcodingId.ToString());
        }

        private List<CommandDefinition> CreateInsertTimeTranscodingCommands(string transcodingId,
            TimeTranscodingEntity timeTranscodingEntity)
        {
            var timeTranscodingPathAndValues =
                this.CreateTimeTranscodingPathAndValues(timeTranscodingEntity,
                    this.GetExpression(timeTranscodingEntity));
            timeTranscodingPathAndValues.Add("transcodingId", transcodingId);
            return this.InsertInformation(DatabaseInformationType.TimeTranscoding, timeTranscodingPathAndValues,
                transcodingId).ToList();
        }

        [Obsolete("Expression column replaced by N_TIME_COLUMN_CONFIG")]
        private Dictionary<string, object> CreateTimeTranscodingPathAndValues(TimeTranscodingEntity timeTranscodingEntity, string expression)
        {
            return new Dictionary<string, object>
            {
                {"DATE_COL_ID", timeTranscodingEntity.DateColumn?.EntityId},
                {nameof(TimeTranscodingEntity.Frequency), TimeFormat.GetFromEnum(GetTimeFormat(timeTranscodingEntity)).FrequencyCode},
                {"PERIOD_COL_ID", timeTranscodingEntity.Period?.Column?.EntityId},
                {"YEAR_COL_ID", timeTranscodingEntity.Year?.Column?.EntityId},
                {"EXPRESSION", expression}
            };
        }

        [Obsolete("Expression column replaced by N_TIME_COLUMN_CONFIG")]
        private string GetExpressionForUpdate(TimeTranscodingEntity timeTranscodingEntity, string entityId, DbConnection connection)
        {
            var parameterName = _mappingStoreDatabase.BuildParameterName(nameof(entityId));
            // TODO Transaction ?
            var sql = $"Select FREQ,EXPRESSION from TIME_TRANSCODING where TR_ID = ${parameterName}";
            var values = _mappingStoreDatabase.Query<dynamic>(sql, new { entityId = Convert.ToInt64(entityId, CultureInfo.InvariantCulture) }).FirstOrDefault();
            var transcoding = new MappingStoreRetrieval.Model.MappingStoreModel.TimeTranscodingEntity(values.FREQ, 0)
            {
                Expression = values.EXPRESSION
            };
            var timeExpressionEntity = TimeExpressionParser.CreateExpression(transcoding);
            
            if (!timeTranscodingEntity.Year.Start.HasValue)
            {
                timeTranscodingEntity.Year.Start = timeExpressionEntity.YearStart;
            }

            if (!timeTranscodingEntity.Year.Length.HasValue)
            {
                timeTranscodingEntity.Year.Length = timeExpressionEntity.YearLength;
            }

            if (!timeTranscodingEntity.Period.Start.HasValue)
            {
                timeTranscodingEntity.Period.Start = timeExpressionEntity.PeriodStart;
            }

            if (!timeTranscodingEntity.Period.Length.HasValue)
            {
                timeTranscodingEntity.Period.Length = timeExpressionEntity.PeriodLength;
            }
            
           return this.GetExpression(timeTranscodingEntity);
        }

        [Obsolete("Expression column replaced by N_TIME_COLUMN_CONFIG")]
        private string GetExpression(TimeTranscodingEntity timeTranscoding)
        {
            var expression = new StringBuilder();
            if (!timeTranscoding.IsDateTime)
            {
                expression.AppendFormat(CultureInfo.InvariantCulture, "year={0},{1};", timeTranscoding.Year.Start, timeTranscoding.Year.Length);
                if (GetTimeFormat(timeTranscoding) != TimeFormatEnumType.Year)
                {
                    expression.AppendFormat(CultureInfo.InvariantCulture, "period={0},{1};", timeTranscoding.Period.Start, timeTranscoding.Period.Length);
                }
            }
            else
            {
                expression.AppendFormat("datetime={0};", 1);
            }

            return expression.ToString();
        }

        private static TimeFormatEnumType GetTimeFormat(TimeTranscodingEntity timeTranscoding)
        {
            return TimeFormat.GetTimeFormatFromCodeId(timeTranscoding.Frequency).EnumType;
        }

        private IEnumerable<CommandDefinition> InsertInformation(DatabaseInformationType informationType, Dictionary<string, object> pathAndValues, string entityId)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);

            var updateInfoAction = new UpdateInfoAction()
            {
                DataInformationType = informationType,
                SqlStatementType = SqlStatementType.Insert,
                PathAndValues = pathAndValues
            };
            var commandDefinitionManager = new CommandDefinitionManager(new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
            var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType, updateInfoAction.SqlStatementType);
            return commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, database);
        }

        public void Delete(string entityId, EntityType entityType)
        {
            var transcodingId = Convert.ToInt64(entityId, CultureInfo.InvariantCulture);

            var databaseInformationType = (DatabaseInformationType)Enum.Parse(typeof(DatabaseInformationType), entityType.ToString());
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            var databaseMapperManager = new DatabaseMapperManager(databaseInformationType);

            var tableName = databaseMapperManager.GetTableName();
            var primaryKeyColumn = databaseMapperManager.GetPrimaryKeyColumn();
            var identityColumnsAndValues = new Dictionary<string, object>
            {
                {primaryKeyColumn, transcodingId}
            };
            var commandDefinition = new DeleteSqlBuilder(tableName, identityColumnsAndValues, databaseMapperManager).CreateCommandDefinition(database);
            using (var context = new ContextWithTransaction(database))
            {
                var databaseUnderContext = context.DatabaseUnderContext;
                var advanceTimeTranscodingPersistEngine = new AdvanceTimeTranscodingPersistEngine(databaseUnderContext);
                advanceTimeTranscodingPersistEngine.Delete(transcodingId);
                databaseUnderContext.ExecuteCommandsUnderTransaction(new[] { commandDefinition });

                context.Complete();
            }
        }

        public void Update(IEntity entity)
        {
            this.Delete(entity.EntityId, EntityType.Transcoding);
            this.Add((TranscodingEntity) entity);
        }

        public void DeleteChildren(string entityId, EntityType childrenEntityType)
        {
            using (var connection = this._databaseManager.GetDatabase(this._mappingStoreIdentifier).CreateConnection())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    var ruleParameterName = this._mappingStoreDatabase.BuildParameterName("MAP_ID");
                    var parameters = new DynamicParameters();
                    parameters.Add(ruleParameterName, entityId.AsMappingStoreEntityId(), DbType.Int64);

                    connection.Execute($"DELETE FROM TRANSCODING WHERE MAP_ID ={ruleParameterName}", parameters, transaction);

                    transaction.Commit();
                }
            }
        }

        public List<StatusType> Add(IEntityStreamingReader input, IEntityStreamingWriter responseWriter)
        {
            List<StatusType> statuses = new List<StatusType>();
            while (input.MoveToNextEntity())
            {
                var entity = input.CurrentEntity;
                if (entity.Status == StatusType.Success)
                {
                    long entityId = Add((TranscodingEntity)entity.Entity);
                    if (responseWriter != null)
                    {
                        entity.Entity.SetEntityId(entityId);
                        responseWriter.Write(entity.Entity);
                    }
                }
                else
                {
                    responseWriter?.WriteStatus(entity.Status, entity.Message);
                }
                statuses.Add(entity.Status);
            }
            return statuses;
        }

        public void Import(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping)
        {
            _entityImporterWithIdMapping.Import<TranscodingEntity>(input, responseWriter, mapping, Add, UpdateDatasetColumnEntityId);
        }

        private bool UpdateDatasetColumnEntityId(IEntity entity, EntityIdMapping idMapping, IEntityStreamingWriter writer)
        {
            var transcodingEntity = entity as TranscodingEntity;
            if (transcodingEntity.HasTimeTrascoding())
            {
                foreach(var timeTranscoding in transcodingEntity.TimeTranscoding)
                {
                    if(timeTranscoding.Period != null && timeTranscoding.Period.Column != null)
                    {
                        timeTranscoding.Period.Column.EntityId = idMapping.Get(EntityType.DataSetColumn, timeTranscoding.Period.Column.EntityId);
                    }

                    if (timeTranscoding.DateColumn != null)
                    {
                        timeTranscoding.DateColumn.EntityId = idMapping.Get(EntityType.DataSetColumn, timeTranscoding.DateColumn.EntityId);
                    }

                    if (timeTranscoding.Year != null && timeTranscoding.Year.Column != null)
                    {
                        timeTranscoding.Year.Column.EntityId = idMapping.Get(EntityType.DataSetColumn, timeTranscoding.Year.Column.EntityId);
                    }
                }
            }

            return true;
        }
    }
}