// -----------------------------------------------------------------------
// <copyright file="SqlScriptProviderEngine.cs" company="EUROSTAT">
//   Date Created : 2017-03-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Estat.Sri.Utils.Sql;

    /// <summary>
    /// This implementation will read SQL DDL scripts from resources folder and split the statements with either <c>;,/,GO</c>
    /// </summary>
    /// <seealso cref="ISqlScriptProviderEngine" />
    internal class SqlScriptProviderEngine : ISqlScriptProviderEngine
    {
        /// <summary>
        /// The database type
        /// </summary>
        private readonly string _databaseType;

        /// <summary>
        /// The upgrade namespace
        /// </summary>
        private readonly string _upgradeNamespace;

        /// <summary>
        /// The resource data location
        /// </summary>
        private readonly ResourceDataLocationFactory _resourceDataLocationFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlScriptProviderEngine" /> class.
        /// </summary>
        /// <param name="databaseType">Type of the database.</param>
        /// <param name="resourceDataLocationFactory">The resource data location factory.</param>
        /// <exception cref="System.ArgumentNullException">databaseType
        /// or
        /// resourceDataLocationFactory</exception>
        public SqlScriptProviderEngine(string databaseType, ResourceDataLocationFactory resourceDataLocationFactory)
        {
            if (databaseType == null)
            {
                throw new ArgumentNullException(nameof(databaseType));
            }

            if (resourceDataLocationFactory == null)
            {
                throw new ArgumentNullException(nameof(resourceDataLocationFactory));
            }

            this._databaseType = databaseType;
            this._resourceDataLocationFactory = resourceDataLocationFactory;
            this._upgradeNamespace = "upgrades";
        }

        /// <summary>
        /// Gets the script stream.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>
        /// The <see cref="TextReader" /> stream to the SQL Script.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// databaseType
        /// or
        /// version
        /// </exception>
        public IEnumerable<string> GetUpgradeSqlStatements(Version version)
        {
            if (version == null)
            {
                throw new ArgumentNullException(nameof(version));
            }

            var versionString = version.ToString(2);
            var normalizedVersion = versionString.Replace(".", "._");
            var resourcePath = string.Format(CultureInfo.InvariantCulture, "{0}._{1}.{2}_to_v{3}.sql", this._upgradeNamespace, normalizedVersion, this._databaseType, versionString);
            var vendorFolder = string.Format(CultureInfo.InvariantCulture, "{0}._{1}.{2}", this._upgradeNamespace, normalizedVersion, this._databaseType);
            if (_resourceDataLocationFactory.Exists(vendorFolder))
            {
                foreach (var statement in ReadLinesFromFolder(vendorFolder).SplitStatements())
                {
                    yield return statement;
                }
            }

            foreach (var statement in ReadLines(resourcePath).SplitStatements())
            {
                yield return statement;
            }
        }

        /// <summary>
        /// Gets the script stream.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>
        /// The <see cref="TextReader" /> stream to the SQL Script.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// databaseType
        /// or
        /// version
        /// </exception>
        public IEnumerable<string> GetUpgradeSqlStatementsCommon(Version version)
        {
            if (version == null)
            {
                throw new ArgumentNullException(nameof(version));
            }

            var versionString = version.ToString(2);
            var normalizedVersion = versionString.Replace(".", "._");
            var resourcePath = string.Format(CultureInfo.InvariantCulture, "{0}._{1}.common_to_v{2}.sql", this._upgradeNamespace, normalizedVersion, versionString);
            
            var commonFolder = string.Format(CultureInfo.InvariantCulture, "{0}._{1}.common", this._upgradeNamespace, normalizedVersion);
            if (_resourceDataLocationFactory.Exists(commonFolder))
            {
                foreach (var statement in ReadLinesFromFolder(commonFolder).SplitStatements())
                {
                    yield return statement;
                }
            }

            if (_resourceDataLocationFactory.Exists(resourcePath))
            {
                foreach (var statement in ReadLines(resourcePath).SplitStatements())
                {
                    yield return statement;
                }
            }
        }

        /// <summary>
        /// Gets the available versions.
        /// </summary>
        /// <param name="startVersion">The start version (Exclusive).</param>
        /// <param name="endVersion">The end version (Inclusive).</param>
        /// <returns>
        /// The available versions for the specified range.
        /// </returns>
        public IEnumerable<Version> GetAvailableVersions(Version startVersion, Version endVersion)
        {
            if (startVersion == null)
            {
                throw new ArgumentNullException(nameof(startVersion));
            }

            if (endVersion == null)
            {
                throw new ArgumentNullException(nameof(endVersion));
            }

            var versions = this.GetUnsortedAvailableVersion().Where(version => startVersion < version && endVersion >= version).OrderBy(version => version);
            return versions;
        }

        /// <summary>
        /// Gets the script reader.
        /// </summary>
        /// <returns>
        /// The <see cref="TextReader" /> stream to the SQL Script.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">databaseType is null</exception>
        public IEnumerable<string> GetSqlStatements()
        {
            foreach (var p in ReadLinesFromFolder(this._databaseType).SplitStatements())
            {
                yield return p;
            }

            foreach (var p in ReadLines("sql_insert_enums.sql").SplitStatements())
            {
                yield return p;
            }

            foreach (var p in ReadLines("version.sql").SplitStatements())
            {
                yield return p;
            }
        }

        /// <summary>
        /// Reads the lines.
        /// </summary>
        /// <param name="resourcePath">The resource path.</param>
        /// <returns>The lines from the resource</returns>
        private IEnumerable<string> ReadLines(string resourcePath)
        {
            using (var location = this._resourceDataLocationFactory.GetDataLocation(resourcePath))
            using (var stream = location.InputStream)
            {
                var statements = stream.ReadLines();
                foreach (var statement in statements)
                {
                    yield return statement;
                }
            }
        }

        /// <summary>
        /// Reads the lines.
        /// </summary>
        /// <param name="resourcePath">The resource path.</param>
        /// <returns>The lines from the resource</returns>
        private IEnumerable<string> ReadLinesFromFolder(string resourcePath)
        {
            foreach(var location in this._resourceDataLocationFactory.GetDataLocations(resourcePath))
            {
                using (location)
                using (var stream = location.InputStream)
                {
                    var statements = stream.ReadLines();
                    foreach (var statement in statements)
                    {
                        yield return statement;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the unsorted available version.
        /// </summary>
        /// <returns>The available versions</returns>
        private IEnumerable<Version> GetUnsortedAvailableVersion()
        {
            Regex extractVersion = new Regex(
                "^" + this._upgradeNamespace + "\\._(?<version>[0-9_\\.]+)\\." + this._databaseType + ".*$",
                RegexOptions.IgnoreCase);
            var availableResources = this._resourceDataLocationFactory.GetAvailableResources(this._upgradeNamespace);
            return (from name in availableResources
                   select extractVersion.Match(name)
                   into match
                   where match.Success
                   select match.Groups["version"].Value.Replace("_", string.Empty)
                   into versionString
                   select new Version(versionString)).Distinct();
        }
    }
}