// -----------------------------------------------------------------------
// <copyright file="AdvanceTimeTranscodingRetriever.cs" company="EUROSTAT">
//   Date Created : 2018-4-13
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Model.AdvancedTime;
    using Constant;
    using Extension;
    using MappingStoreRetrieval;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Util.Extensions;
    using Estat.Sri.MappingStore.Store.Extension;

    /// <summary>
    /// Class AdvanceTimeTranscodingRetriever.
    /// </summary>
    internal class AdvanceTimeTranscodingRetriever
    {
        /// <summary>
        /// The query criteria column
        /// </summary>
        [Obsolete("column name directly stored in N_MAPPING_TIME_DIMENSION and retrieved TimeMappingRetrieverEngine")]
        private const string QueryCriteriaColumn =
            "SELECT D.COL_ID as EntityId, D.NAME as Name, D.DS_ID as ParentId from DATASET_COLUMN D WHERE EXISTS (SELECT T.CRITERIA_COLUMN FROM TIME_TRANSCODING_TR T WHERE T.CRITERIA_COLUMN = D.COL_ID and T.TR_ID={0})";

        /// <summary>
        /// The query time format configuration
        /// </summary>
        private const string QueryTimeFormatConfig =
            @"select F.CRITERIA_VALUE, F.OUTPUT_FORMAT,  F.START_CONF, SP.DB_FORMAT as START_FORMAT, F.END_CONF, EP.DB_FORMAT as END_FORMAT, F.DURATION_MAP
            from N_TIME_FORMAT_CONFIG F
            left outer join N_TIME_PARTICLE_CONFIG SP ON F.START_CONF = SP.TPC_ID
             left outer join N_TIME_PARTICLE_CONFIG EP ON F.END_CONF = EP.TPC_ID
            where STR_MAP_SET_ID = {0}";

        /// <summary>
        /// The query time particle configuration
        /// </summary>
        private const string QueryTimeParticleConfig = 
            @"select c.TPC_ID, c.COLUMN_TYPE, c.DATASET_COLUMN_NAME, c.START_POS, c.P_LENGTH, c.TPC_ID 
                from N_TIME_COLUMN_CONFIG c
                where c.TPC_ID in {0}";
        /// <summary>
        /// The query period rules
        /// </summary>
        private const string QueryPeriodRules = "select SDMX_PERIOD_CODE, LOCAL_PERIOD_CODE from N_PERIOD_RULES where TPC_ID = {0}";

        /// <summary>
        /// Duration mapping
        /// </summary>
        private const string QueryDurationMap =
            "select CONSTANT_OR_DEFAULT, DATASET_COLUMN_NAME from N_MAPPING_DURATION where DUR_MAP_ID = {0}";

        /// <summary>
        /// Duration mapping transcoding 
        /// </summary>
        private const string QueryDurationTranscoding =
            "select LOCAL_VALUE, DURATION_CODE from N_MAPPING_DURATION_VALUES where DUR_MAP_ID = {0}";
        
        /// <summary>
        /// The database
        /// </summary>
        private readonly Database _database;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvanceTimeTranscodingRetriever"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        public AdvanceTimeTranscodingRetriever(Database database)
        {
            _database = database;
        }

        /// <summary>
        /// Retrieves the specified transcoding.
        /// </summary>
        /// <param name="mappingSetId">The mapping set id.</param>
        /// <returns>TimeTranscodingAdvancedEntity.</returns>
        [Obsolete("User Retrieve(TimeTranscodingAdvancedEntity,long)")]
        public TimeTranscodingAdvancedEntity Retrieve(long mappingSetId)
        {
            var parameterName = _database.BuildParameterName(nameof(mappingSetId));
            var criteriaColumn = GetCriteriaColumn(mappingSetId, parameterName);
            if (criteriaColumn == null)
            {
                return null;
            }

            TimeTranscodingAdvancedEntity entity = new TimeTranscodingAdvancedEntity();
            entity.CriteriaColumn = criteriaColumn;
            GetTimeFormatConfig(entity, mappingSetId);

            return entity;
        }

        /// <summary>
        /// Retrieves the specified mapping set.
        /// </summary>
        /// <param name="entity">The entity</param>
        /// <param name="mappingSetId">The mapping set id.</param>
        /// <returns>TimeTranscodingAdvancedEntity.</returns>
        public TimeTranscodingAdvancedEntity Retrieve(TimeTranscodingAdvancedEntity entity, long mappingSetId)
        {
            GetTimeFormatConfig(entity, mappingSetId);

            return entity;
        }
        /// <summary>
        /// Gets the criteria column.
        /// </summary>
        /// <param name="transcoding">The transcoding.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <returns>DataSetColumnEntity.</returns>
        [Obsolete("dataset column name included ")]
        private DataSetColumnEntity GetCriteriaColumn(long transcoding,string parameterName)
        {
            var query = string.Format(CultureInfo.InvariantCulture, QueryCriteriaColumn, parameterName);
            var criteriaColumn = _database.Query<DataSetColumnEntity>(query, new { transcoding }).FirstOrDefault();
            return criteriaColumn;
        }

        /// <summary>
        /// Gets the time format configuration.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="mappingSetId">The transcoding.</param>
        /// <exception cref="Estat.Sri.MappingStoreRetrieval.IncompleteMappingSetException">Cannot determine output time format from the database value '" + outputFormatText + "'</exception>
        private void GetTimeFormatConfig(TimeTranscodingAdvancedEntity entity, long mappingSetId)
        {
            var rows = _database.QueryWithKey<dynamic>(QueryTimeFormatConfig, mappingSetId);
            foreach (var row in rows)
            {
                TimeFormatConfiguration timeFormatConfiguration = new TimeFormatConfiguration();

                var outputFormatText = row.OUTPUT_FORMAT as string;

                // using readable code as it is the only thing that is unique and common with Java
                var outputFormat = TimeFormat.Values.FirstOrDefault(t => t.ReadableCode.Equals(outputFormatText));
                if (outputFormat == null)
                {
                    throw new IncompleteMappingSetException("Cannot determine output time format from the database value '" + outputFormatText + "'");
                }

                timeFormatConfiguration.OutputFormat = outputFormat;

                timeFormatConfiguration.CriteriaValue = (string)row.CRITERIA_VALUE;
                // get duration mapping
                var durationMap = row.DURATION_MAP is long durationMapId ? RetrieveDurationMapping(durationMapId) : null;

                long? startConfValue = row.START_CONF == null ? (long?)null : (long)row.START_CONF;
                long? endConfValue = row.END_CONF == null ? (long?)null : (long)row.END_CONF;

                TimeParticleConfiguration startConfig = null;
                TimeParticleConfiguration endConfig = null;
                if (startConfValue.HasValue || endConfValue.HasValue)
                {
                    var columnConfiguration = GetColumnConfiguration(startConfValue, endConfValue);

                    if (startConfValue.HasValue)
                    {
                        var startFormatValue = (string)row.START_FORMAT;
                        startConfig = SetupTimeParticleConfig(
                                                    startConfValue.Value,
                                                    columnConfiguration,
                                                    startFormatValue);
                    }

                    if (endConfValue.HasValue)
                    {
                        var formatValue = (string)row.END_FORMAT;
                        endConfig = SetupTimeParticleConfig(
                            endConfValue.Value,
                            columnConfiguration,
                            formatValue);
                    }
                }

                timeFormatConfiguration.SetConfiguration(durationMap, startConfig, endConfig);
                entity.TimeFormatConfigurations.Add(timeFormatConfiguration.CriteriaValue, timeFormatConfiguration);
            }
        }

        private DurationMappingEntity RetrieveDurationMapping(long durationMapId)
        {
            var durationMapping = _database.QueryWithKey<dynamic>(QueryDurationMap, durationMapId).FirstOrDefault();
            if (durationMapping == null)
            {
                return null;
            }

            DurationMappingEntity durationMappingEntity = new DurationMappingEntity { Column = durationMapping.DATASET_COLUMN_NAME, EntityId = Convert.ToString(durationMapId, CultureInfo.InvariantCulture) };
            if (durationMappingEntity.Column == null)
            {
                durationMappingEntity.ConstantValue = durationMapping.CONSTANT_OR_DEFAULT;
            }
            else
            {
                durationMappingEntity.DefaultValue = durationMapping.CONSTANT_OR_DEFAULT;
                var durationTranscodings = _database.QueryWithKey<dynamic>(QueryDurationTranscoding, durationMapId);
                foreach (var transcoding in durationTranscodings)
                {
                    durationMappingEntity.TranscodingRules[(string)transcoding.DURATION_CODE] =
                        (string)transcoding.LOCAL_VALUE;
                }
            }

            return durationMappingEntity;
        }

        /// <summary>
        /// Setups the time particle configuration.
        /// </summary>
        /// <param name="confValue">The conf value.</param>
        /// <param name="columnConfiguration">The column configuration.</param>
        /// <param name="formatValue">The format value.</param>
        /// <returns>TimeParticleConfiguration.</returns>
        private static TimeParticleConfiguration SetupTimeParticleConfig(long confValue, ILookup<long, Tuple<TimeColumnType, TimeRelatedColumnInfo>> columnConfiguration, string formatValue)
        {
            var startColumnConfig = columnConfiguration[confValue];
            DisseminationDatabaseTimeFormat databaseTimeFormat;
            if (Enum.TryParse(formatValue, out databaseTimeFormat))
            {
                TimeParticleConfiguration particle = new TimeParticleConfiguration();
                particle.Format = databaseTimeFormat;
                foreach (var columnConfig in startColumnConfig)
                {
                    switch (columnConfig.Item1)
                    {
                        case TimeColumnType.Date:
                            particle.DateColumn = columnConfig.Item2.Column;
                            break;
                        case TimeColumnType.Period:
                            particle.Period = (PeriodTimeRelatedColumnInfo)columnConfig.Item2;
                            break;
                        case TimeColumnType.Year:
                            particle.Year = columnConfig.Item2;
                            break;
                    }
                }

                return particle;
            }

            return null;
        }

        /// <summary>
        /// Gets the time column.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <returns>A tuple of column type and <see cref="TimeRelatedColumnInfo" />.</returns>
        /// <exception cref="Estat.Sri.MappingStoreRetrieval.IncompleteMappingSetException">Cannot determine column type</exception>
        private Tuple<TimeColumnType, TimeRelatedColumnInfo> GetTimeColumn(dynamic row)
        {
            TimeColumnType columnType;
            if (!Enum.TryParse((string)row.COLUMN_TYPE, true, out columnType))
            {
                throw new IncompleteMappingSetException("Cannot determine column type");
            }

            DataSetColumnEntity dataSetColumn = new DataSetColumnEntity() { Name = (string)row.DATASET_COLUMN_NAME};
            TimeRelatedColumnInfo columnInfo;
            switch (columnType)
            {
                case TimeColumnType.Year:
                    columnInfo = new TimeRelatedColumnInfo { Column = dataSetColumn, Start = (int?)row.START_POS, Length = (int?)row.P_LENGTH };
                    break;
                case TimeColumnType.Period:
                    columnInfo = BuildPeriodTimeRelatedColumnInfo(row, dataSetColumn);
                    break;
                default:
                    columnInfo = new TimeRelatedColumnInfo { Column = dataSetColumn };
                    break;
            }

            return Tuple.Create(columnType, columnInfo);
        }

        /// <summary>
        /// Builds the period time related column information.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="dataSetColumn">The data set column.</param>
        /// <returns>TimeRelatedColumnInfo.</returns>
        private TimeRelatedColumnInfo BuildPeriodTimeRelatedColumnInfo(dynamic row, DataSetColumnEntity dataSetColumn)
        {
            TimeRelatedColumnInfo columnInfo;
            var periodColumnInfo = new PeriodTimeRelatedColumnInfo { Column = dataSetColumn, Start = (int?)row.START_POS, Length = (int?)row.P_LENGTH };
            var id = (long)row.TPC_ID;
            var paramName = _database.BuildParameterName(nameof(id));
            var query = string.Format(CultureInfo.InvariantCulture, QueryPeriodRules, paramName);
            var periodRules = _database.Query<dynamic>(query, new { id }).Select(x => new PeriodCodeMap { SdmxPeriodCode = (string)x.SDMX_PERIOD_CODE, LocalPeriodCode = (string)x.LOCAL_PERIOD_CODE });
            periodColumnInfo.Rules.AddAll(periodRules);
            columnInfo = periodColumnInfo;
            return columnInfo;
        }

        /// <summary>
        /// Gets the column configuration.
        /// </summary>
        /// <param name="startConfValue">The start conf value.</param>
        /// <param name="endConfValue">The end conf value.</param>
        /// <returns>A lookup between <c>TPC_ID</c> and column type</returns>
        private ILookup<long, Tuple<TimeColumnType, TimeRelatedColumnInfo>> GetColumnConfiguration(long? startConfValue, long? endConfValue)
        {
            var list = new[] { startConfValue, endConfValue }.Where(x => x.HasValue).Select(x => x.Value).ToArray();
            Debug.Assert(list.Length > 0, "Neither Start or End is set");
            var parameterName = _database.BuildParameterName(nameof(list));
            var query = string.Format(CultureInfo.InvariantCulture, QueryTimeParticleConfig, parameterName);
            var rows = _database.Query<dynamic>(query, new { list });
            return rows.ToLookup(x => (long)x.TPC_ID, GetTimeColumn);
        }
    }
}