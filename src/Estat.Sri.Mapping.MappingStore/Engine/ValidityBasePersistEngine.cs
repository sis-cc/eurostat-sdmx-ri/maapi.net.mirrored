// -----------------------------------------------------------------------
// <copyright file="TimeMappingPersistEngine.cs" company="EUROSTAT">
//   Date Created : 2022-08-03
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Data;
using Dapper;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Utils;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public abstract class ValidityBasePersistEngine<T> : MappingBasePersistEngine<T> where T : ValidityBaseEntity
    {

        private readonly Database _database;
        private readonly EntityImporterWithIdMapping _entityImporterWithIdMapping = new EntityImporterWithIdMapping();

        protected ValidityBasePersistEngine(Database database): base(database)
        {
            _database = database;
        }


        public override void Delete(long mappingSetId, Database databaseInContext)
        {
            databaseInContext.ExecuteNonQueryFormat(
                "delete from N_MAPPING_DATA_VALIDITY where STR_MAP_SET_ID = {0} and VALIDITY_TYPE= {1}",
                databaseInContext.CreateInParameter("p1", DbType.Int64, mappingSetId),
                databaseInContext.CreateInParameter("p2", DbType.AnsiString, GetValidityType()));
        }


        protected override long Add(T entity, ContextWithTransaction context)
        {

            long mappingSetId;
            if(entity.ParentId!= null) { 
                mappingSetId = entity.GetParentId();
            }
            else
            {
                mappingSetId = entity.GetEntityId();
            }

            Delete(mappingSetId, context.DatabaseUnderContext);
            var value = Insert(entity, context);
            entity.SetEntityId(mappingSetId);
            return value;
        }

        /// <summary>
        /// Get the validity type, one of VALID_TO, LAST_UP
        /// </summary>
        /// <returns>One of VALID_TO, LAST_UP</returns>
        protected abstract string GetValidityType();


        private long Insert(T entity, ContextWithTransaction context)
        {
            long mappingSetId;
            if (entity.ParentId != null)
            {
                mappingSetId = entity.GetParentId();
            }
            else
            {
                mappingSetId = entity.GetEntityId();
            }

            var insertStatement = CreateInsertTimeMappingCommands(mappingSetId, entity, context.DatabaseUnderContext);
            context.ExecuteCommandsWithReturnId(insertStatement);
            return mappingSetId;
        }

        private IEnumerable<CommandDefinition> CreateInsertTimeMappingCommands(long mappingSetId, T entity, Database database)
        {
            var pathAndValues = new Dictionary<string, object>
            {
                { nameof(ValidityBaseEntity.EntityId), mappingSetId },
                { nameof(ValidityBaseEntity.Column), entity.Column?.Name },
                { nameof(ValidityBaseEntity.Constant), entity.Constant },
                { nameof(ValidityBaseEntity.MappingType), GetValidityType() }
            };

            return CommandBuilderHelper.InsertInformation(database, DatabaseInformationType.MappingDataValidity, pathAndValues, mappingSetId);
        }

    }
}