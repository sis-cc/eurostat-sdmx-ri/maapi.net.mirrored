// -----------------------------------------------------------------------
// <copyright file="UpdateInfoActionForPermissions.cs" company="EUROSTAT">
//   Date Created : 2017-06-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    internal class UpdateInfoActionForPermissions
    {
        private readonly IEntityIdFromUrn _entityIdFromUrn;
        private readonly DatabaseInformationType _dataflowDatabaseInformationType;
        private readonly DatabaseInformationType _categoryDatabaseInformationType;

        public UpdateInfoActionForPermissions(IEntityIdFromUrn entityIdFromUrn, DatabaseInformationType dataflowDatabaseInformationType, DatabaseInformationType categoryDatabaseInformationType)
        {
            this._entityIdFromUrn = entityIdFromUrn;
            this._dataflowDatabaseInformationType = dataflowDatabaseInformationType;
            this._categoryDatabaseInformationType = categoryDatabaseInformationType;
        }

        public UpdateInfoAction For(string urn, long parentId)
        {
            UpdateInfoAction updateInfoAction = null;
            var sdmxStructureEnumType = this.GetPermissionType(urn);
            if (sdmxStructureEnumType == SdmxStructureEnumType.Dataflow)
            {
                
                updateInfoAction = new UpdateInfoAction
                {
                    DataInformationType = this._dataflowDatabaseInformationType,
                    SqlStatementType = SqlStatementType.Insert,
                    PathAndValues = new Dictionary<string, object>
                    {
                        {DataflowTableInformations.DataflowId.ModelName, this._entityIdFromUrn.Get(urn)},
                        {nameof(DatasetEntity.EntityId), parentId}
                    }
                };
            }
            if (sdmxStructureEnumType == SdmxStructureEnumType.CategoryScheme)
            {
                
                updateInfoAction = new UpdateInfoAction
                {
                    DataInformationType = this._categoryDatabaseInformationType,
                    SqlStatementType = SqlStatementType.Insert,
                    PathAndValues = new Dictionary<string, object>
                    {
                        {CategoryTableInformations.CategoryId.ModelName, this._entityIdFromUrn.Get(urn)},
                        {nameof(DatasetEntity.EntityId), parentId}
                    }
                };
            }
            return updateInfoAction;
        }

        private SdmxStructureEnumType GetPermissionType(string urn)
        {
            var structureReferenceImpl = new StructureReferenceImpl(urn);
            return structureReferenceImpl.MaintainableStructureEnumType.EnumType;
        }
    }
}