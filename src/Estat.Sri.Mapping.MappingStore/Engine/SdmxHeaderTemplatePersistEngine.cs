// -----------------------------------------------------------------------
// <copyright file="SdmxHeaderTemplatePersistEngine.cs" company="EUROSTAT">
//   Date Created : 2018-06-08
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.MappingStore.Constant;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;

    using Dapper;
    
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStore.Store.Engine;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStore.Store.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.SdmxXmlConstants;
    using Estat.Sri.Utils;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    internal class SdmxHeaderTemplatePersistEngine : IEntityPersistenceForUpdate<HeaderEntity>
    {
        private readonly EntitySdmxReferencePersistenceEngine _entitySdmxReferencePersistenceEngine;
        private const string OutputParametername = "p_pk";

        private readonly Database _mappingStoreDb;
        private readonly EntityImporterWithIdMapping _entityImporterWithIdMapping = new EntityImporterWithIdMapping();

        /// <summary>
        /// The delete localised string query format
        /// </summary>

        private const string RefConnection = "SELECT SOURCE_ENTITY_ID FROM ENTITY_SDMX_REF " +
            "ESR INNER JOIN ENTITY_BASE EB on EB.ENTITY_ID = ESR.SOURCE_ENTITY_ID " +
            "INNER JOIN N_HEADER h ON h.ENTITY_ID = ESR.SOURCE_ENTITY_ID " +
            "WHERE TARGET_ARTEFACT = {0} AND ENTITY_TYPE = 'HeaderTemplate'";

        private const string GetDataflowId = "SELECT TARGET_ARTEFACT FROM ENTITY_SDMX_REF " +
           "ESR INNER JOIN ENTITY_BASE EB on EB.ENTITY_ID = ESR.SOURCE_ENTITY_ID " +
           "INNER JOIN N_HEADER h ON h.ENTITY_ID = ESR.SOURCE_ENTITY_ID " +
           "WHERE SOURCE_ENTITY_ID = {0} AND ENTITY_TYPE = 'HeaderTemplate'";
        

        private const string DataflowReplacementCondition = "EXISTS(SELECT SOURCE_ENTITY_ID FROM ENTITY_SDMX_REF " +
            "ESR INNER JOIN ENTITY_BASE EB on EB.ENTITY_ID = ESR.SOURCE_ENTITY_ID " +
            "WHERE TARGET_ARTEFACT = {0} AND ENTITY_TYPE = 'HeaderTemplate' and h.ENTITY_ID = ESR.SOURCE_ENTITY_ID)";

        private const string DeleteLocalisedStringQueryFormat = "delete from HEADER_LOCALISED_STRING where " +
            "CONTACT_ID in (select CONTACT_ID from CONTACT c inner join PARTY p on c.PARTY_ID= p.PARTY_ID where p.HEADER_ID = {0})"
            + " OR PARTY_ID in (select PARTY_ID from PARTY p where p.HEADER_ID = {0})"
            + " OR HEADER_ID in (select ENTITY_ID from N_HEADER h WHERE h.ENTITY_ID={0})";
        private const string DeleteFromHeader = "DELETE FROM N_HEADER where ENTITY_ID = {0}";
        private const string SelectEntityIdFromObjectId = "select EB.ENTITY_ID from ENTITY_BASE EB where ENTITY_TYPE = 'HeaderTemplate' and EB.OBJECT_ID = {0} ";
        private const string DeleteFromHeaderWhereDataflow = "DELETE h FROM N_HEADER h where " + DataflowReplacementCondition;
        

        /// <summary>
        /// The delete localised string query format
        /// </summary>
        private const string DeleteLocalisedStringQueryFormatWhereDataflow = "delete from HEADER_LOCALISED_STRING where " +
            "CONTACT_ID in (select CONTACT_ID from CONTACT c inner join PARTY p on c.PARTY_ID= p.PARTY_ID inner join N_HEADER h on p.HEADER_ID = h.ENTITY_ID WHERE "+ DataflowReplacementCondition +")"
            + " OR PARTY_ID in (select PARTY_ID from PARTY p inner join N_HEADER h on p.HEADER_ID = h.ENTITY_ID WHERE " + DataflowReplacementCondition + ")"
            + " OR HEADER_ID in (select ENTITY_ID from N_HEADER h WHERE " + DataflowReplacementCondition + ")";

        public SdmxHeaderTemplatePersistEngine(Database mappingStoreDb, DatabaseManager databaseManager, string mappingStoreIdentifier)
        {
            _mappingStoreDb = mappingStoreDb;
            this._entitySdmxReferencePersistenceEngine = new EntitySdmxReferencePersistenceEngine(databaseManager, mappingStoreIdentifier);
        }

        public void Update(PatchRequest patchRequest, EntityType entityType, string entityId)
        {
            throw new System.NotImplementedException("Updating via patch not supported. Use Add to replace");
        }

        /// <inheritdoc />
        public long Add(HeaderEntity entity)
        {
            return Add(new[] { entity }).FirstOrDefault();
        }

        public IEnumerable<long> Add(IList<HeaderEntity> entities)
        {
            var result = new List<long>();
            using (var context = new ContextWithTransaction(_mappingStoreDb))
            {
                StructureCache structureCache = new StructureCache();
                // TODO use databaseUnderContext after SDMXRI-667 merge
                foreach (var headerEntity in entities)
                {
                    var sdmxHeader = headerEntity.SdmxHeader;
                    if (sdmxHeader == null)
                    {
                        throw new MissingInformationException("SDMX Header not defined");
                    }

                    if (headerEntity.ParentId == null)
                    {
                        throw new MissingInformationException("Header ParentId not defined");
                    }
                    var dataflowStatus = structureCache.GetArtefactFinalStatus(context.DatabaseUnderContext, new StructureReferenceImpl(headerEntity.ParentId)) ?? ArtefactFinalStatus.Empty;
                    var dataflowSysId = dataflowStatus?.PrimaryKey; 
                    if (dataflowSysId < 1)
                    {
                        throw new SdmxNoResultsException("Could not find the dataflow with urn:" + headerEntity.ParentId);
                    }

                    ValidateHeader(sdmxHeader);

                    DeleteHeader(headerEntity.ParentId, EntityType.Sdmx, context);
                    var objectId = UrnUtil.GetUrnPostfix(headerEntity.ParentId);

                    var headerSysId = SaveHeader(sdmxHeader, objectId,headerEntity.Description,GetJSONFromHeader(headerEntity), context);
                    result.Add(headerSysId);

                    //we need to add the reference in ENTITY_SDMX_REF for dataflowSysId
                    _entitySdmxReferencePersistenceEngine.Insert(new StructureReferenceImpl(headerEntity.ParentId), headerSysId, context);

                    WriteLocalisedString(sdmxHeader.Name, headerSysId, null, null, context);
                    WriteLocalisedString(sdmxHeader.Source, MappingStoreRetrieval.Engine.HeaderRetrieverEngine.SourceText, headerSysId, null, null, context);
                    WriteParty(sdmxHeader.Sender, headerSysId, MappingStoreRetrieval.Engine.HeaderRetrieverEngine.SenderText, context);
                    foreach (var party in sdmxHeader.Receiver)
                    {
                        var partyType = MappingStoreRetrieval.Engine.HeaderRetrieverEngine.ReceiverText;
                        WriteParty(party, headerSysId, partyType, context);
                    }
                }

                context.Complete();
            }

            return result;
        }

        private string GetJSONFromHeader(HeaderEntity entity)
        {
            //Possibly an alternative to store this as JSON as it is usually small complex and only used as one piece
            return "{}";
        }
        /// <summary>
        /// Should validate the header before writing to the database.
        /// </summary>
        /// <param name="header">The <see cref="IHeader"/> object to validate.</param>
        /// <exception cref="ValidationException">Unknown structure usage.</exception>
        /// <exception cref="ValidationException">Unknown dataset action.</exception>
        private static void ValidateHeader(IHeader header)
        {
            foreach (var attribute in header.AdditionalAttribtues)
            {
                switch (attribute.Key)
                {
                    case nameof(ElementNameTable.StructureUsage):
                        if (!string.IsNullOrEmpty(attribute.Value) &&
                            attribute.Value != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd).UrnClass &&
                            attribute.Value != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow).UrnClass)
                        {
                            throw new ValidationException($"Struture type '{attribute.Value}' is not a valid value.");
                        }
                        break;
                    case nameof(ElementNameTable.DataSetAction):
                        if (!string.IsNullOrEmpty(attribute.Value))
                        {
                            if (!Enum.TryParse(attribute.Value, false, out DatasetActionEnumType enumVal))
                            {
                                throw new ValidationException($"Dataset action '{attribute.Value}' is not a valid value.");
                            }
                            else if (enumVal == DatasetActionEnumType.Null)
                            {
                                throw new ValidationException($"Dataset action '{attribute.Value}' is not a valid value.");
                            }
                        }
                        break;
                    default: continue;
                }
            }
        }

        private static void WriteParty(IParty party, long headerSysId, string partyType, ContextWithTransaction context)
        {
            var partyParameters = new DynamicParameters();
            partyParameters.Add("p_id", dbType: DbType.AnsiString, value: party.Id);
            partyParameters.Add("p_header_id", headerSysId, dbType: DbType.Int64);
            partyParameters.Add("p_type", partyType, dbType: DbType.AnsiString);
            AddOutputParameter(partyParameters);
            context.Connection.Execute("INSERT_PARTY", partyParameters, commandType: CommandType.StoredProcedure);
            var partySysId = partyParameters.Get<long>(OutputParametername);
            WriteLocalisedString(party.Name, null, partySysId, null, context);
            foreach (var contact in party.Contacts)
            {
                var parameters = new DynamicParameters();
                parameters.Add("p_party_id", partySysId, dbType:  DbType.Int64);
                AddOutputParameter(parameters);

                context.Connection.Execute("INSERT_CONTACT", parameters, commandType: CommandType.StoredProcedure);
                var contactSysId = parameters.Get<long>(OutputParametername);

                WriteLocalisedString(contact.Name, null, null, contactSysId, context);
                WriteLocalisedString(contact.Departments, MappingStoreRetrieval.Engine.HeaderRetrieverEngine.DepartmentText, null, null, contactSysId, context);
                WriteLocalisedString(contact.Role, MappingStoreRetrieval.Engine.HeaderRetrieverEngine.RoleText, null, null, contactSysId, context);

                // TODO introduce propoper and common constants with retrievers 
                WriteContactDetail(context, contactSysId, contact.Email, nameof(IContact.Email));
                WriteContactDetail(context, contactSysId, contact.Fax, nameof(IContact.Fax));
                WriteContactDetail(context, contactSysId, contact.Telephone, nameof(IContact.Telephone));
                WriteContactDetail(context, contactSysId, contact.X400, nameof(IContact.X400));
                WriteContactDetail(context, contactSysId, contact.Uri, nameof(IContact.Uri).ToUpperInvariant());
            }
        }

        private static void WriteContactDetail(ContextWithTransaction context, long contactSysId, IList<string> details, string detailType)
        {
            foreach (var detail in details)
            {
                var detailParameters = new DynamicParameters();
                detailParameters.Add("p_contact_id", contactSysId, dbType: DbType.Int64);
                detailParameters.Add("p_type", detailType, dbType: DbType.AnsiString);
                detailParameters.Add("p_value", detail, dbType: DbType.AnsiString);
                AddOutputParameter(detailParameters);
                context.Connection.Execute("INSERT_CONTACT_DETAIL", detailParameters, commandType: CommandType.StoredProcedure);
            }
        }

        private static void AddOutputParameter(DynamicParameters parameters)
        {
            parameters.Add(OutputParametername, dbType: DbType.Int64, direction: ParameterDirection.Output);
        }

        private static void WriteLocalisedString(
            IList<ITextTypeWrapper> names,
            long? headerSysId,
            long? partyId,
            long? contactId,
            ContextWithTransaction context)
        {
            WriteLocalisedString(names, MappingStoreRetrieval.Engine.HeaderRetrieverEngine.NameText, headerSysId, partyId, contactId, context);
        }

        private static void WriteLocalisedString(
            IList<ITextTypeWrapper> names,
            string nameText,
            long? headerSysId,
            long? partyId,
            long? contactId,
            ContextWithTransaction context)
        {
            foreach (var textTypeWrapper in names)
            {
                var nameParameters = new DynamicParameters();
                nameParameters.Add("p_type", nameText, dbType: DbType.AnsiString);
                TryAddParameter(headerSysId, nameParameters, "p_header_id");
                TryAddParameter(partyId, nameParameters, "p_party_id");
                TryAddParameter(contactId, nameParameters, "p_contact_id");
                nameParameters.Add("p_language", textTypeWrapper.Locale, dbType: DbType.AnsiString);
                nameParameters.Add("p_text", textTypeWrapper.Value, dbType: DbType.AnsiString);
                AddOutputParameter(nameParameters);
                context.Connection.Execute(
                    "INSERT_HEADER_LOCALISED_STRING",
                    nameParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        private static void TryAddParameter(long? headerSysId, DynamicParameters nameParameters, string parameterName)
        {
            if (headerSysId.HasValue)
            {
                nameParameters.Add(parameterName, headerSysId, dbType: DbType.Int64);
            }
            else
            {
                nameParameters.Add(parameterName, DBNull.Value, dbType: DbType.Int64);
            }
        }

        private static long SaveHeader(IHeader sdmxHeader, string name, string description,string jsonValue, ContextWithTransaction context)
        {
            var test = sdmxHeader.Test.ToDbValue();
            var datasetAgency = sdmxHeader.GetAdditionalAttribtue(nameof(ElementNameTable.DataSetAgency))
                                ?? (object)DBNull.Value;
            var structureType = sdmxHeader.GetAdditionalAttribtue(nameof(ElementNameTable.StructureUsage))
                                ?? (object)DBNull.Value;
            var datasetId = sdmxHeader.GetAdditionalAttribtue(nameof(ElementNameTable.DataSetID))
                                ?? (object)DBNull.Value;
            var datasetAction = sdmxHeader.GetAdditionalAttribtue(nameof(ElementNameTable.DataSetAction))
                                ?? (object)DBNull.Value;
            var parameters = new DynamicParameters();
            parameters.Add("p_object_id", name, DbType.AnsiString);
            // nvarchar must have String for nvarchar, not AnsiString else mariadb/mysql will fail with type error
            parameters.Add("p_description", description, DbType.String);
            parameters.Add("p_test", test, dbType: DbType.Int32);
            parameters.Add("p_dataset_agency", datasetAgency, DbType.AnsiString);
            parameters.Add("p_structure_type", structureType, DbType.AnsiString);
            parameters.Add("p_dataset_id", datasetId, DbType.AnsiString);
            parameters.Add("p_dataset_action", datasetAction, DbType.AnsiString);
            parameters.Add("p_json_value", jsonValue, DbType.String);
            AddOutputParameter(parameters);
            context.Connection.Execute("INSERT_N_HEADER", parameters, commandType: CommandType.StoredProcedure);
            var headerSysId = parameters.Get<long>(OutputParametername);
            return headerSysId;
        }

        public void Delete(string entityId, EntityType entityType)
        {
          
            using (var context = new ContextWithTransaction(_mappingStoreDb))
            {
                int deletedCount = DeleteHeader(entityId, entityType, context);
                if (deletedCount == 0)
                {
                    throw new ResourceNotFoundException("Could not find header");
                }

                context.Complete();
            }

        }

        private int DeleteHeader(string entityId, EntityType entityType, ContextWithTransaction context)
        {

            long sysId;
            var param = _mappingStoreDb.BuildParameterName(nameof(sysId));

            string headerEntityId;
            string deleteStatementLocalised = string.Format(CultureInfo.InvariantCulture, DeleteLocalisedStringQueryFormat, param);
            string deleteStatementHeader = string.Format(CultureInfo.InvariantCulture, DeleteFromHeader, param);
            string deleteParty = string.Format(CultureInfo.InvariantCulture, "DELETE FROM PARTY WHERE HEADER_ID = {0}", param);
            if (entityType == EntityType.Header)
            {
                sysId = entityId.AsMappingStoreEntityId();
                headerEntityId = entityId;
            }
            else if (entityType == EntityType.Sdmx)
            {
                // get header entity_id by Object ID
                var structureType = new StructureReferenceImpl(entityId);
                if (structureType.TargetStructureType.EnumType != SdmxStructureEnumType.Dataflow)
                {
                    throw new SdmxNotImplementedException("Cannot delete header with non-Dataflow entity id");
                }
                
                var objectId = new DbString { Value = UrnUtil.GetUrnPostfix(structureType.MaintainableUrn.ToString()), IsAnsi = true };
                var objectIdParam = _mappingStoreDb.BuildParameterName(nameof(objectId));
                var query = string.Format(CultureInfo.InvariantCulture, SelectEntityIdFromObjectId, objectIdParam);
                sysId = context.Connection.ExecuteScalar<long>(query, param: new { objectId });
                headerEntityId = sysId.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                throw new NotSupportedException("Deleting a header only via its entity id is supported");
            }


            //first we need to identify the header id based on the id of the dataflow id
            //var headerId = context.Connection.Execute(deleteStatementLocalised, new { sys });
            //  delete first HEADER_LOCALISED_STRING related entries given the HEADER_ID
            // They could relate directly or indirectly via PARTY or PARTY -> CONTACT
            // This is needed because there is no ON DELETE CASCADE because SQL Server doesn't allow it
            context.Connection.Execute(deleteStatementLocalised, new { sysId });
            context.Connection.Execute(deleteParty, new { sysId });


            // then delete the HEADER which should delete the PARTY and cascade to CONTACT and CONTACT_DETAILS
            var deletedCount = context.Connection.Execute(deleteStatementHeader, new { sysId });
            if (deletedCount > 0)
            {
                _entitySdmxReferencePersistenceEngine.Delete(headerEntityId, context);
                context.DatabaseUnderContext.UsingLogger().ExecuteNonQueryFormat("delete from ENTITY_BASE where ENTITY_ID = {0}", _mappingStoreDb.CreateInParameter("entityIdParam", DbType.Int64, sysId));
            }
            return deletedCount;
        }


        public void Update(IEntity entity)
        {
            this.Delete(entity.EntityId, EntityType.Header);
            entity.EntityId = this.Add((HeaderEntity)entity).ToString();
        }

        public void DeleteChildren(string entityId, EntityType childrenEntityType)
        {
            throw new NotImplementedException();
        }

        public List<StatusType> Add(IEntityStreamingReader input, IEntityStreamingWriter responseWriter)
        {
            List<StatusType> statuses = new List<StatusType>();
            while (input.MoveToNextEntity())
            {
                var entity = input.CurrentEntity;
                if (entity.Status == StatusType.Success)
                {
                    long entityId = Add((HeaderEntity)entity.Entity);
                    if (responseWriter != null)
                    {
                        entity.Entity.SetEntityId(entityId);
                        responseWriter.Write(entity.Entity);
                    }
                }
                else
                {
                    responseWriter?.WriteStatus(entity.Status, entity.Message);
                }
                statuses.Add(entity.Status);
            }
            return statuses;
        }

        public void Import(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping)
        {
            _entityImporterWithIdMapping.Import<HeaderEntity>(input, responseWriter, mapping, Add, (x, y, z) => { return true; });
        }
    }
}