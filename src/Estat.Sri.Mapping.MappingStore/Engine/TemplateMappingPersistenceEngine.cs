using Estat.Sri.Mapping.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.Api.Engine;
using Dapper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.Mapping.MappingStore.Factory;
using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Estat.Sri.MappingStore.Store.Engine;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using System.Data;
using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.Utils;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    public class TemplateMappingPersistenceEngine : IEntityPersistenceForUpdate<TemplateMapping>
    {
        private readonly DatabaseManager _databaseManager;
        private readonly UpdateInfoFactoryManager _updateInfoFactoryManager;
        private readonly string _mappingStoreIdentifier;
        private readonly EntityImporterWithIdMapping _entityImporterWithIdMapping = new EntityImporterWithIdMapping();
        private readonly Database database;
        private readonly MaintainableRefRetrieverEngine refRetriver;

        public TemplateMappingPersistenceEngine(DatabaseManager databaseManager, UpdateInfoFactoryManager updateInfoFactoryManager, string mappingStoreIdentifier, IEntityPropertiesExtractor entityPropertiesExtractor, ICommmandsFromUpdateInfo commmandsFromUpdateInfo) 
        {
            this._databaseManager = databaseManager;
            this._updateInfoFactoryManager = updateInfoFactoryManager;
            this._mappingStoreIdentifier = mappingStoreIdentifier;
            this.database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            this.refRetriver = new MaintainableRefRetrieverEngine(this.database, new MappingStoreRetrievalManager(this.database));
        }

        public long Add(TemplateMapping entity)
        {
            
            long templateMappingId;
            using (var context = new ContextWithTransaction(database))
            {
                var commandDefinitions = this.InsertInformation(DatabaseInformationType.TemplateMapping, new Dictionary<string, object>
                {
                    {nameof(TemplateMapping.ColumnDescription), entity.ColumnDescription},
                    {nameof(TemplateMapping.Name), entity.ColumnName},
                    {nameof(TemplateMapping.ColumnName), entity.ColumnName},
                    {nameof(TemplateMapping.ComponentId), entity.ComponentId},
                    {nameof(TemplateMapping.ComponentType), entity.ComponentType},
                    {nameof(TemplateMapping.Description), entity.Description},
                }, entity.EntityId);

                templateMappingId = context.ExecuteCommandsWithReturnId(commandDefinitions);
                var definitions = new List<CommandDefinition>();
                if (entity.TimeTranscoding != null)
                {
                    foreach (var timeTranscodingEntity in entity.TimeTranscoding)
                    {
                        definitions.AddRange(this.CreateInsertTimeTranscodingCommands(templateMappingId.ToString(), timeTranscodingEntity));
                    }
                }

                if (entity.Transcodings != null)
                {
                    foreach (var item in entity.Transcodings)
                    {
                        definitions.AddRange(this.CreateTranscodingCommand(templateMappingId, item.Key,item.Value));
                    }
                }

                context.ExecuteCommandsWithReturnId(definitions);
                context.Complete();
            }

            return templateMappingId;
        }


        public IEnumerable<long> Add(IList<TemplateMapping> entities)
        {
            throw new NotImplementedException();
        }

        public void Delete(string entityId, EntityType entityType)
        {
            var databaseInformationType = (DatabaseInformationType)Enum.Parse(typeof(DatabaseInformationType), entityType.ToString());
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);
            var databaseMapperManager = new DatabaseMapperManager(databaseInformationType);

            var tableName = databaseMapperManager.GetTableName();
            var primaryKeyColumn = databaseMapperManager.GetPrimaryKeyColumn();
            var identityColumnsAndValues = new Dictionary<string, object>
            {
                {primaryKeyColumn, entityId}
            };
            database.UsingLogger().ExecuteNonQueryFormat("delete from TM_TRANSCODING where TEMPLATE_ID = {0}", database.CreateInParameter("entityIdParam", DbType.Int64, entityId));
            database.UsingLogger().ExecuteNonQueryFormat("delete from TM_TIME_TRANSCODING where TEMPLATE_ID = {0}", database.CreateInParameter("entityIdParam", DbType.Int64, entityId));
            database.UsingLogger().ExecuteNonQueryFormat("delete from ENTITY_BASE where ENTITY_ID = {0}", database.CreateInParameter("entityIdParam", DbType.Int64, entityId));
            var commandDefinition = new DeleteSqlBuilder(tableName, identityColumnsAndValues, databaseMapperManager).CreateCommandDefinition(database);
            database.ExecuteCommands(new[] { commandDefinition });
        }

        public void Update(PatchRequest patchRequest, EntityType entityType, string entityId)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);

            database.ExecuteCodeWithTransaction((connection) =>
            {
                var commandDefinitions = new List<CommandDefinition>();
                this.CreateCommandForTemplateMapping(patchRequest, entityId, commandDefinitions, database);
                this.CreateCommandForTranscodings(patchRequest,entityId,commandDefinitions,database);
                this.CreateCommandForTimeTranscodings(patchRequest, entityId, commandDefinitions, database);
                return commandDefinitions;
            });
        }

        private void CreateCommandForTimeTranscodings(PatchRequest patchRequest, string entityId, List<CommandDefinition> commandDefinitions, Database database)
        {
            var patchDocuments = patchRequest.FindAll(item => item.HasPath("timeTranscodings"));
            if (patchDocuments.Any())
            {
                var groupedByOperation = patchDocuments.GroupBy(item => item.Op);
                foreach (var group in groupedByOperation)
                {
                    switch (group.Key)
                    {
                        case "remove":
                            this.CreateDeleteCommand(group.ToList(), commandDefinitions, database, DatabaseInformationType.TemplateMappingTimeTranscodings,entityId);
                            break;
                        case "replace":
                            var entity = CreateTimeTranscodingEntity(@group);
                            var timeTranscodingPathAndValues = this.CreateTimeTranscodingPathAndValues(entity);
                            commandDefinitions.AddRange(this.CreateTimeTranscodingUpdateCommand(entityId, timeTranscodingPathAndValues));

                            break;
                        case "add":
                            var timeTranscodingEntity = CreateTimeTranscodingEntity(@group);
                            commandDefinitions.AddRange(this.CreateInsertTimeTranscodingCommands(entityId, timeTranscodingEntity));

                            break;
                    }
                }
            }
        }

        private IEnumerable<CommandDefinition> CreateTimeTranscodingUpdateCommand(string entityId, Dictionary<string, object> timeTranscodingPathAndValues)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);

            var updateInfoAction = new UpdateInfoAction()
            {
                DataInformationType = DatabaseInformationType.TemplateMappingTimeTranscodings,
                SqlStatementType = SqlStatementType.Update,
                PathAndValues = timeTranscodingPathAndValues
            };
            var commandDefinitionManager = new CommandDefinitionManager(new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
            var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType, updateInfoAction.SqlStatementType);
            return commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, database);
        }


        private List<CommandDefinition> CreateInsertTimeTranscodingCommands(string templateId, TimeTranscodingEntity timeTranscodingEntity)
        {
            var timeTranscodingPathAndValues = this.CreateTimeTranscodingPathAndValues(timeTranscodingEntity);
            timeTranscodingPathAndValues.Add(nameof(TemplateMapping.EntityId), templateId);
            return this.InsertInformation(DatabaseInformationType.TemplateMappingTimeTranscodings, timeTranscodingPathAndValues, templateId).ToList();
        }

        private Dictionary<string, object> CreateTimeTranscodingPathAndValues(TimeTranscodingEntity timeTranscodingEntity)
        {
            return new Dictionary<string, object>
            {
                {nameof(TimeTranscodingEntity.Frequency), TimeFormat.GetFromEnum(GetTimeFormat(timeTranscodingEntity)).FrequencyCode},
                {"IS_DATE", timeTranscodingEntity.IsDateTime?1:0},
                {"Y_START", timeTranscodingEntity.Year?.Start},
                {"Y_LENGTH", timeTranscodingEntity.Year?.Length},
                {"P_START", timeTranscodingEntity.Period?.Start},
                {"P_LENGTH", timeTranscodingEntity.Period?.Start},
            };
        }

        private static TimeFormatEnumType GetTimeFormat(TimeTranscodingEntity timeTranscoding)
        {
            return TimeFormat.GetTimeFormatFromCodeId(timeTranscoding.Frequency).EnumType;
        }

        private static TimeTranscodingEntity CreateTimeTranscodingEntity(IGrouping<string, PatchDocument> @group)
        {
            var patchDocuments = @group.ToList();
            var isDateTime = @group.FirstOrDefault(item => item.HasPath("is_date_time"));

            return new TimeTranscodingEntity()
            {
                DateColumn = new DataSetColumnEntity() { EntityId = @group.FirstOrDefault(item => item.HasPath("data_column"))?.Value.ToString() },
                Frequency = @group.FirstOrDefault(item => item.HasPath("frequency"))?.Value.ToString(),
                IsDateTime = isDateTime != null && bool.Parse(isDateTime.Value.ToString()),
                Period = new PeriodTimeTranscoding()
                {
                    Column = new DataSetColumnEntity() { EntityId = @group.FirstOrDefault(item => item.HasPath("column") && item.HasPath("period"))?.Value.ToString() },
                    Start = group.PeriodStart(),
                    Length = group.PeriodLength(),
                },
                Year = new TimeTranscoding()
                {
                    Column = new DataSetColumnEntity() { EntityId = @group.FirstOrDefault(item => item.HasPath("column") && item.HasPath("year"))?.Value.ToString() },
                    Start = group.YearStart(),
                    Length = group.YearLength(),
                }
            };
        }

        private void CreateCommandForTemplateMapping(PatchRequest patchRequest, string entityId, List<CommandDefinition> commandDefinitions, Database database)
        {
            var patchDocuments = patchRequest.FindAll(item => !item.HasPath("transcodings") && !item.HasPath("timeTranscodings"));

            var transcodingAction = new UpdateInfoAction()
            {
                DataInformationType = DatabaseInformationType.TemplateMapping,
                SqlStatementType = SqlStatementType.Update,
                PathAndValues = new Dictionary<string, object>()
            };

            var updateInfo = this._updateInfoFactoryManager.GetFactory(EntityType.TemplateMapping, database).Create(patchRequest, entityId);
            foreach (var updateInfoAction in updateInfo.Actions)
            {
                var commandDefinitionManager = new CommandDefinitionManager(new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
                var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType, updateInfoAction.SqlStatementType);
                commandDefinitions.AddRange(commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, database));
            }
        }

        private void CreateCommandForTranscodings(PatchRequest patchRequest, string entityId, List<CommandDefinition> commandDefinitions, Database database)
        {
            var patchDocuments = patchRequest.FindAll(item => item.HasPath("transcodings"));
            if (patchDocuments.Any())
            {
                var groupedByOperation = patchDocuments.GroupBy(item => item.Op);
                foreach (var group in groupedByOperation)
                {
                    switch (group.Key)
                    {
                        case "remove":
                            this.CreateDeleteCommand(group.ToList(), commandDefinitions, database, DatabaseInformationType.TemplateMappingTranscodings,entityId);
                            break;
                        case "replace":
                            this.CreateDeleteCommand(group.ToList(), commandDefinitions, database, DatabaseInformationType.TemplateMappingTranscodings,entityId);
                            foreach (var patchDocument in group)
                            {
                                var localCode = patchDocument.Path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Last();
                                var dsdCode = patchDocument.Value.ToString();
                                commandDefinitions.AddRange(this.CreateTranscodingCommand(Convert.ToInt64(entityId), localCode,dsdCode));
                                
                            }
                            break;
                        case "add":
                            foreach (var patchDocument in group)
                            {
                                var transcodings = JsonConvert.DeserializeObject<Dictionary<string, string>>(patchDocument.Value.ToString());
                                foreach (var item in transcodings)
                                {
                                    commandDefinitions.AddRange(this.CreateTranscodingCommand(Convert.ToInt64(entityId), item.Key, item.Value));
                                }
                            }
                            break;
                    }
                }
            }
        }


        private void CreateDeleteCommand(IEnumerable<PatchDocument> documents, List<CommandDefinition> commandDefinitions, Database database, DatabaseInformationType databaseInformationType,string entityId)
        {
            foreach (var patchDocument in documents)
            {
                var updateInfo = new UpdateInfoAction()
                {
                    SqlStatementType = SqlStatementType.Delete,
                    DataInformationType = databaseInformationType,
                    PathAndValues = new Dictionary<string, object>()
                    {
                        {nameof(Entity.EntityId),entityId },
                        {"LOCAL_CODE", patchDocument.Path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Last()}
                    }
                };

                commandDefinitions.AddRange(this.GetCommandFromUpdateInfo(updateInfo, entityId, database));
            }
        }


        private IEnumerable<CommandDefinition> GetCommandFromUpdateInfo(UpdateInfoAction updateInfoAction, string entityId, Database mappingStoreDabase)
        {
            var commandDefinitionManager = new CommandDefinitionManager(new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
            var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType, updateInfoAction.SqlStatementType);
            return commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, mappingStoreDabase);
        }

        private IEnumerable<CommandDefinition> CreateTranscodingCommand(long entityId, string localCode,string dsdCode)
        {
            return this.InsertInformation(DatabaseInformationType.TemplateMappingTranscodings, new Dictionary<string, object>
            {
                {nameof(TemplateMapping.EntityId), entityId},
                {"LOCAL_CODE", localCode},
                {"DSD_CODE", dsdCode}
            }, entityId.ToString());
        }

        private IEnumerable<CommandDefinition> InsertInformation(DatabaseInformationType informationType, Dictionary<string, object> pathAndValues, string entityId)
        {
            var database = this._databaseManager.GetDatabase(this._mappingStoreIdentifier);

            var updateInfoAction = new UpdateInfoAction()
            {
                DataInformationType = informationType,
                SqlStatementType = SqlStatementType.Insert,
                PathAndValues = pathAndValues
            };
            var commandDefinitionManager = new CommandDefinitionManager(new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
            var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType, updateInfoAction.SqlStatementType);
            return commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, database);
        }

        public void Update(IEntity entity)
        {
            this.Delete(entity.EntityId, EntityType.TemplateMapping);
            entity.EntityId = this.Add((TemplateMapping)entity).ToString();
        }

        public void DeleteChildren(string entityId, EntityType childrenEntityType)
        {
            throw new NotImplementedException();
        }

        public List<StatusType> Add(IEntityStreamingReader input, IEntityStreamingWriter responseWriter)
        {
            List<StatusType> statuses = new List<StatusType>();
            while (input.MoveToNextEntity())
            {
                var entity = input.CurrentEntity;
                if (entity.Status == StatusType.Success)
                {
                    long entityId = Add((TemplateMapping)entity.Entity);
                    if (responseWriter != null)
                    {
                        entity.Entity.SetEntityId(entityId);
                        responseWriter.Write(entity.Entity);
                    }
                }
                else
                {
                    responseWriter?.WriteStatus(entity.Status, entity.Message);
                }
                statuses.Add(entity.Status);
            }
            return statuses;
        }

        public void Import(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping)
        {
            _entityImporterWithIdMapping.Import<TemplateMapping>(input, responseWriter, mapping, Add,(x,y,z)=> { return true; });
        }
    }
}
