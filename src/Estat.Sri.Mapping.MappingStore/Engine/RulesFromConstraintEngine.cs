// -----------------------------------------------------------------------
// <copyright file="RulesFromConstraintEngine.cs" company="EUROSTAT">
//   Date Created : 2019-03-25
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Extension;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Utils;
using log4net;
using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    /// <summary></summary>
    public class RulesFromConstraintEngine : IRulesFromConstraintEngine
    {
        private readonly IRetrieverManager _retrieverManager;
        private readonly IEntityPersistenceManager _entityPersistenceManager;
        private readonly DatabaseManager _databaseManager;
        private readonly ILog _logger = LogManager.GetLogger(typeof(RulesFromConstraintEngine));

        /// <summary>Initializes a new instance of the <see cref="RulesFromConstraintEngine"/> class.</summary>
        /// <param name="retrieverManager">The retriever manager.</param>
        /// <param name="entityPersistenceManager">The entity persistence manager.</param>
        /// <param name="databaseManager">The database manager.</param>
        public RulesFromConstraintEngine(IRetrieverManager retrieverManager, IEntityPersistenceManager entityPersistenceManager, DatabaseManager databaseManager)
        {
            _retrieverManager = retrieverManager;
            _entityPersistenceManager = entityPersistenceManager;
            this._databaseManager = databaseManager;
        }

        
        /// <summary>Creates the rules.</summary>
        /// <param name="sid">The sid.</param>
        /// <param name="mappingSetId">The mapping set identifier.</param>
        /// <param name="agency">The agency.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="version">The version.</param>
        public void CreateRules(string sid, string mappingSetId, string agency, string id, string version)
        {
            var contentConstraint = new StructureReferenceImpl
            {
                AgencyId = agency,
                MaintainableId = id,
                Version = version,
                TargetStructureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ContentConstraint)
            };


            var contentConstraintMutableObject =
                GetContentConstraint(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne),
                    contentConstraint);
            if (contentConstraintMutableObject != null)
            {
                var database = this._databaseManager.GetDatabase(sid);
                var entityQuery = new EntityQuery
                {
                    ParentId = new Criteria<string>(OperatorType.Exact, mappingSetId)
                };
                var mappingsWithTranscoding = new ComponentMappingRetrieverEngine(database).GetEntities(entityQuery, Detail.Full);
                _logger.Debug($"mappings for mapping set id {mappingSetId} : {JsonConvert.SerializeObject(mappingsWithTranscoding)}");
                if (contentConstraintMutableObject.IncludedCubeRegion != null)
                {
                    foreach (var keyValuesMutable in contentConstraintMutableObject.IncludedCubeRegion.KeyValues)
                    {
                        var componentId = keyValuesMutable.Id;

                        var componentMappingEntity = mappingsWithTranscoding.FirstOrDefault(FilterComponent(componentId));

                        if (componentMappingEntity != null)
                        {
                            if (keyValuesMutable.KeyValues.Count == 0)
                            {
                                _logger.Debug($"component with id {componentId} has no cube values");
                                continue;
                            }
                            _logger.Debug($"found mapping for component id {componentId}");
                            if (componentMappingEntity.HasTranscoding())
                            {
                                _entityPersistenceManager.GetEngine<TranscodingRuleEntity>(sid)
                                    .DeleteChildren(componentMappingEntity.EntityId, EntityType.TranscodingRule);
                                _logger.Debug($"deleted transcoding with id {componentMappingEntity.EntityId}");
                            }

                            _logger.Debug($"add new transcoding with parent id {componentMappingEntity.EntityId}");
                            foreach (var keyValue in keyValuesMutable.KeyValues)
                            {
                                _entityPersistenceManager.Add(new TranscodingRuleEntity
                                {
                                    ParentId = componentMappingEntity.EntityId,
                                    StoreId = sid,
                                    DsdCodeEntity = new IdentifiableEntity(EntityType.Sdmx)
                                    {
                                        ObjectId = keyValue
                                    },
                                    LocalCodes = new List<LocalCodeEntity>
                                    {
                                        new LocalCodeEntity
                                        {
                                            ParentId = componentMappingEntity.GetColumns().First().Name,
                                            ObjectId = keyValue
                                        }
                                    }
                                });
                                _logger.Debug($"added transcoding rule for mapping id {componentMappingEntity.EntityId} with object id {keyValue}");

                            }
                        }
                    }
                }
            }
        }

        private static Func<ComponentMappingEntity, bool> FilterComponent(string componentId)
        {
            return item => item.Component != null && item.GetColumns() != null && item.GetColumns().Count == 1 &&
                           item.Component.ObjectId == componentId;
        }

        private IContentConstraintMutableObject GetContentConstraint(SdmxSchema sdmxSchema, StructureReferenceImpl contentConstraint)
        {
            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, null)
                .SetStructureIdentification(contentConstraint)
                .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                .Build();
            var sdmxObjects =
                _retrieverManager.ParseRequest(structureQuery);
            return sdmxObjects.ContentConstraintObjects.First().MutableInstance;
        }
    }
}