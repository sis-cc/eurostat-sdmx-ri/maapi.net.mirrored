// -----------------------------------------------------------------------
// <copyright file="EntityPersistenceEngine.cs" company="EUROSTAT">
//   Date Created : 2017-02-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Utils;
using Estat.Sri.MappingStoreRetrieval;
using log4net;

namespace Estat.Sri.Mapping.MappingStore.Engine
{
    internal class EntityImporterWithIdMapping
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EntityImporterWithIdMapping));
        public EntityImporterWithIdMapping()
        {
        }

        internal void Import<TEntity>(IEntityStreamingReader input, IEntityStreamingWriter responseWriter, EntityIdMapping mapping, Func<TEntity, long> add, Func<IEntity, EntityIdMapping, IEntityStreamingWriter, bool> setActualRelationId) where TEntity : IEntity
        {
            while (input.MoveToNextEntity())
            {
                var entity = input.CurrentEntity.Entity;
                if (SetActualParentId(entity, mapping, responseWriter) && setActualRelationId(entity, mapping,responseWriter))
                {
                    var oldEntityId = entity.EntityId;
                    // Some persistance engines will try to update/validate existing entities when an entity is given
                    entity.EntityId = null;
                    try
                    {
                        var newEntityId = add((TEntity)entity);
                        mapping.Add(entity.TypeOfEntity, oldEntityId, newEntityId.ToString(CultureInfo.InvariantCulture));
                    }
                    catch (Exception e)
                    {
                        log.DebugFormat("Importing entity {0} with old entityid {1} and old parent id {2} failed : {3}", entity.TypeOfEntity, oldEntityId, entity.ParentId, e);
                        throw;
                    }
                }
            }
        }

        private bool SetActualParentId(IEntity entity, EntityIdMapping mapping, IEntityStreamingWriter responseWriter)
        {
            var parentEntityType = entity.TypeOfEntity.ParentType();
            if (parentEntityType == EntityType.None)
            {
                return true;
            }

            var newEntityId = mapping.Get(parentEntityType, entity.ParentId);
            if (string.IsNullOrWhiteSpace(newEntityId))
            {
                if (!string.IsNullOrWhiteSpace(entity.ParentId))
                {
                    return true;
                }
                responseWriter.WriteStatus(StatusType.Error, $"Could not find parent id {entity.ParentId} for entity {entity.TypeOfEntity} with id {entity.EntityId}");
                return false;
            }

            entity.ParentId = newEntityId;
            return true; 
        }
    }
}