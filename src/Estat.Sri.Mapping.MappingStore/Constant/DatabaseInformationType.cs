// -----------------------------------------------------------------------
// <copyright file="DatabaseInformationType.cs" company="EUROSTAT">
//   Date Created : 2017-03-02
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Constant
{
    public enum DatabaseInformationType
    {
        /// <summary>
        /// The DDB connection settings
        /// </summary>
        DdbConnectionSettings,

        /// <summary>
        /// The data set
        /// </summary>
        DataSet,

        /// <summary>
        /// The data set column
        /// </summary>
        DataSetColumn,

        /// <summary>
        /// The local code
        /// </summary>
        LocalCode,

        /// <summary>
        /// The mapping set
        /// </summary>
        MappingSet,

        /// <summary>
        /// The mapping
        /// </summary>
        Mapping,

        /// <summary>
        /// The trans-coding
        /// </summary>
        Transcoding,

        /// <summary>
        /// The time trans-coding
        /// </summary>
        TimeTranscoding,

        /// <summary>
        /// The trans-coding rule
        /// </summary>
        TranscodingRule,

        /// <summary>
        /// The dataflow database connection
        /// </summary>
        DataflowDbConnection,
        /// <summary>
        /// The category database connection
        /// </summary>
        CategoryDbConnection,
        
        /// <summary>
        /// The item
        /// </summary>
        Item,
        /// <summary>
        /// The transcoding script
        /// </summary>
        TranscodingScript,
        /// <summary>
        /// The component mapping column
        /// </summary>
        ComponentMappingColumn,
        /// <summary>
        /// The component mapping component
        /// </summary>
        ComponentMappingComponent,
        /// <summary>
        /// The data source
        /// </summary>
        DescSource,
        /// <summary>
        /// The header
        /// </summary>
        Header,
        /// <summary>
        /// The header localised string
        /// </summary>
        HeaderLocalisedString,
        /// <summary>
        /// The header party
        /// </summary>
        HeaderParty,
        /// <summary>
        /// The contact details
        /// </summary>
        ContactDetails,
        /// <summary>
        /// The contact
        /// </summary>
        Contact,

        /// <summary>
        /// The dataflow
        /// </summary>
        Dataflow,

        /// <summary>
        /// The registry
        /// </summary>
        Registry,
        /// <summary>
        /// The user
        /// </summary>
        User,
        /// <summary>
        /// The dataflow user
        /// </summary>
        DataflowUser,
        /// <summary>
        /// The category user
        /// </summary>
        CategoryUser,

        /// <summary>
        /// The user action
        /// </summary>
        UserAction,
        /// <summary>
        /// The template mapping
        /// </summary>
        TemplateMapping,
        /// <summary>
        /// The template mapping transcodings
        /// </summary>
        TemplateMappingTranscodings,
        /// <summary>
        /// The template mapping time transcodings
        /// </summary>
        TemplateMappingTimeTranscodings,
        /// <summary>
        /// Data Source
        /// </summary>
        DataSource,
        /// <summary>
        /// Time-pre-formatted
        /// </summary>
        TimePreFormatted,
        /// <summary>
        /// Nsiws url
        /// </summary>
        Nsiws,
        /// <summary>
        /// The component mapping - Metadata attribute
        /// </summary>
        ComponentMappingMetadataAttribute,
        /// <summary>
        /// Dataset property
        /// </summary>
        DatasetProperty,
        /// <summary>
        /// Time dimension mapping and transcoding
        /// </summary>
        TimeMapping,

        /// <summary>
        /// Last updated and valid to mappings
        /// </summary>
        MappingDataValidity,

        /// <summary>
        /// Update status mapping
        /// </summary>
        UpdateStatusMapping
    }
}