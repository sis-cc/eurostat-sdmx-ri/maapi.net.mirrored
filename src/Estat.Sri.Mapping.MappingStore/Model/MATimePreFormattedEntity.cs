// -----------------------------------------------------------------------
// <copyright file="MATimePreFormattedEntity.cs" company="EUROSTAT">
//   Date Created : 2021-05-27
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Model
{
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// This class is to populate the <see cref="TimePreFormattedEntity"/> from Dapper.
    /// </summary>
    public class MATimePreFormattedEntity
    {
        /// <summary>
        /// The mapping set pk.
        /// </summary>
        public long MappingSetId { get; set; }

        /// <summary>
        /// The output-column pk.
        /// </summary>
        public long OutputColumnId { get; set; }
        /// <summary>
        /// the output-column name.
        /// </summary>
        public string OutputColumnName { get; set; }
        /// <summary>
        /// The output-column description.
        /// </summary>
        public string OutputColumnDescription { get; set; }

        /// <summary>
        /// The from-column pk.
        /// </summary>
        public long FromColumnId { get; set; }
        /// <summary>
        /// the from-column name.
        /// </summary>
        public string FromColumnName { get; set; }
        /// <summary>
        /// The from-column description.
        /// </summary>
        public string FromColumnDescription { get; set; }

        /// <summary>
        /// The to-column pk.
        /// </summary>
        public long ToColumnId { get; set; }
        /// <summary>
        /// the to-column name.
        /// </summary>
        public string ToColumnName { get; set; }
        /// <summary>
        /// The to-column description.
        /// </summary>
        public string ToColumnDescription { get; set; }

        /// <summary>
        /// Maps to <see cref="TimePreFormattedEntity"/>.
        /// </summary>
        /// <returns>A new instance of <see cref="TimePreFormattedEntity"/>.</returns>
        public TimePreFormattedEntity GetEntity()
        {
            var entity = new TimePreFormattedEntity();
            entity.EntityId = MappingSetId.ToString();

            entity.OutputColumn = new DataSetColumnEntity()
            {
                EntityId = OutputColumnId.ToString(),
                Name = OutputColumnName,
                Description = OutputColumnDescription,
                IsMapped = Api.Constant.ColumnMappingStatus.Yes
            };
            entity.FromColumn = new DataSetColumnEntity()
            {
                EntityId = FromColumnId.ToString(),
                Name = FromColumnName,
                Description = FromColumnDescription,
                IsMapped = Api.Constant.ColumnMappingStatus.Yes
            };
            entity.ToColumn = new DataSetColumnEntity()
            {
                EntityId = ToColumnId.ToString(),
                Name = ToColumnName,
                Description = ToColumnDescription,
                IsMapped = Api.Constant.ColumnMappingStatus.Yes
            };

            return entity;
        }
    }
}
