﻿// -----------------------------------------------------------------------
// <copyright file="TimeExpressionEntity.cs" company="EUROSTAT">
//   Date Created : 2013-04-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Model
{
    using System;

    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The time expression entity.
    /// </summary>
    public class TimeExpressionEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TimeExpressionEntity"/> class.
        /// </summary>
        /// <param name="timeTranscodingEntity">The time transcoding entity.</param>
        /// <param name="mapping">The mapping.</param>
        public TimeExpressionEntity(TimeTranscodingEntity timeTranscodingEntity, TimeDimensionMappingEntity mapping)
        {
            this.Freq = TimeFormat.GetTimeFormatFromCodeId(timeTranscodingEntity.Frequency);
            this.IsDateTime = timeTranscodingEntity.IsDateTime;

            if (!IsDateTime)
            {
                if (timeTranscodingEntity.Year == null)
                {
                    throw new ArgumentException("Year is not set", nameof(timeTranscodingEntity));
                }
                YearColumnSysId = timeTranscodingEntity.Year.Column.Name;
                YearStart = timeTranscodingEntity.Year.Start.GetValueOrDefault(0);
                YearLength = timeTranscodingEntity.Year.Length.GetValueOrDefault(4);
                if (YearLength == 0)
                {
                    YearLength = 4;
                }

                if (Freq != TimeFormatEnumType.Year)
                {
                    if (timeTranscodingEntity.Period == null)
                    {
                        throw new ArgumentException("Period is not set for non-annual frequency", nameof(timeTranscodingEntity));
                    }
                    PeriodColumnSysId = timeTranscodingEntity.Period.Column.Name;
                    TranscodingRules = new TranscodingRuleMap(timeTranscodingEntity.Period);
                    PeriodStart = timeTranscodingEntity.Period.Start.GetValueOrDefault(0);
                    PeriodLength = timeTranscodingEntity.Period.Length.GetValueOrDefault(0);
                    OneColumnMapping = string.Equals(YearColumnSysId, PeriodColumnSysId);
                }
                else
                {
                    OneColumnMapping = true;
                }
            }
            else
            {
                DateColumnSysId = timeTranscodingEntity.DateColumn?.Name;
            }
        }

        /// <summary>
        ///     Gets the year DATASET_COLUMN.COL_ID from <c>TIME_TRANSCODING.DATE_COL_ID</c>
        /// </summary>
        public string DateColumnSysId { get; private set; }

        /// <summary>
        ///     Gets the frequency value from <c>TIME_TRANSCODING.FREQ</c>
        /// </summary>
        public TimeFormatEnumType Freq { get; private set; }

        /// <summary>
        ///     Gets a value indicating whether the isDateTime value from TRANSCODING.EXPRESSION is set
        /// </summary>
        public bool IsDateTime { get; private set; }

        /// <summary>
        ///     Gets a value indicating whether the same column is used for both <see cref="YearColumnSysId" /> and
        ///     <see
        ///         cref="PeriodColumnSysId" />
        ///     .
        /// </summary>
        public bool OneColumnMapping { get; private set; }

        /// <summary>
        ///     Gets the <c>periodColid </c> (The DATASET_COLUMN.COL_ID) value from <c>TIME_TRANSCODING.PERIOD_COL_ID</c>
        /// </summary>
        public string PeriodColumnSysId { get; private set; }

        /// <summary>
        ///     Gets the periodLen value from TRANSCODING.EXPRESSION
        /// </summary>
        public int PeriodLength { get; private set; }

        /// <summary>
        ///     Gets the periodStart value from TRANSCODING.EXPRESSION
        /// </summary>
        public int PeriodStart { get; private set; }

        /// <summary>
        ///     Gets the transcoding rules.
        /// </summary>
        public TranscodingRuleMap TranscodingRules { get; private set; }

        /// <summary>
        ///     Gets the year DATASET_COLUMN.COL_ID from <c>TIME_TRANSCODING.YEAR_COL_ID</c>
        /// </summary>
        public string YearColumnSysId { get; private set; }

        /// <summary>
        ///     Gets the yearLen value from TRANSCODING.EXPRESSION
        /// </summary>
        public int YearLength { get; private set; }

        /// <summary>
        ///     Gets the yearStart value from TRANSCODING.EXPRESSION
        /// </summary>
        public int YearStart { get; private set; }
    }
}