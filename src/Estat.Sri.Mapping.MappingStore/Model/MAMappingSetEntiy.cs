// -----------------------------------------------------------------------
// <copyright file="MAMappingSetEntiy.cs" company="EUROSTAT">
//   Date Created : 2017-02-14
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Dapper;

namespace Estat.Sri.Mapping.MappingStore.Model
{
    using System;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// This class is used to populate the <see cref="MappingSetEntity"/> from Dapper. 
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Model.MappingSetEntity" />
    public class MAMappingSetEntity : MappingSetEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MAMappingSetEntity"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="Name"></param>
        /// <param name="Description"></param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="dataSetId">The data set identifier.</param>
        /// <param name="validFrom">The valid from datetime.</param>
        /// <param name="validTo">The valid to datetime.</param>
        /// <param name="isMetadata"></param>
        /// <param name="dfId">The dataflow identifier.</param>
        /// <param name="version1">The version1.</param>
        /// <param name="version2">The version2.</param>
        /// <param name="version3">The version3.</param>
        /// <param name="extension"></param>
        /// <param name="dfag">The dataflow agency.</param>
        public MAMappingSetEntity(
            long id, 
            string Name, 
            string Description, 
            long? dataSetId, 
            DateTime? validFrom, 
            DateTime? validTo,
            bool isMetadata,
            string dfId, 
            long version1, 
            long version2, 
            long version3, 
            string extension,
            string dfag
            ) : this(id, Name)
        {
            this.DataSetId = dataSetId?.ToString(CultureInfo.InvariantCulture);
            this.ValidFrom = validFrom;
            this.ValidTo = validTo;
            this.IsMetadata = isMetadata;

            if (!string.IsNullOrWhiteSpace(dfId))
            {

                string version;
                if (version3 == -1)
                {
                    version = string.Join(".", new[] { version1, version2 });
                }
                else 
                {
                    version = string.Join(".", new[] { version1, version2, version3 });
                    if (!string.IsNullOrWhiteSpace(extension))
                    {
                        version = string.Format("{0}-{1}", version, extension);
                    }
                }

                this.ParentId = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)
                    .GenerateUrn(dfag, dfId, version).ToString();
            }

            this.Description = Description;
        }

        public MAMappingSetEntity(
            long entityId)
        {
            this.EntityId = entityId.ToString(CultureInfo.InvariantCulture);
        }

        public MAMappingSetEntity(long entityId, string name) : this(entityId)
        {
            this.Name = name;
        }
    }
}