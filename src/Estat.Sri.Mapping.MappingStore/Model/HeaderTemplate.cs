// -----------------------------------------------------------------------
// <copyright file="HeaderTemplate.cs" company="EUROSTAT">
//   Date Created : 2017-03-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Estat.Sri.Mapping.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Sri.Mapping.MappingStore.Model
{
    public class HeaderTemplate : HeaderEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HeaderTemplate" /> class.
        /// </summary>
        /// <param name="transmissionName">The transmission name.</param>
        /// <param name="dataSetAgencyId">The DataSet Agency ID.</param>
        /// <param name="source">Source.</param>
        /// <param name="test">Indicates whether the message is for test purposes or not. False for normal messages.</param>
        /// <param name="sender">Sender.</param>
        /// <param name="receiver">Receiver.</param>
        public HeaderTemplate(string transmissionName, string dataSetAgencyId, string source, bool? test , Party sender, List<Party> receiver)
            : base()
        {
            this.TransmissionName = transmissionName;
            this.DataSetAgencyId = dataSetAgencyId;
            this.Source = source;
            this.Test = test;
            this.Sender = sender;
            this.Receiver = receiver;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        private HeaderTemplate(long entityId, short test, string datasetagencyid, string dfid, long? version1, long? version2, long? version3, string agency)
            : this(entityId)
        {
            this.Test = Convert.ToBoolean(test);
            this.DataSetAgencyId = datasetagencyid;
            if (!string.IsNullOrWhiteSpace(dfid))
            {

                var version = string.Join(".", new[] { version1, version2, version3 }.Where(l => l.HasValue).Select(l => l.Value.ToString(CultureInfo.InvariantCulture)));

                this.ParentId = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)
                    .GenerateUrn(agency, dfid, version).ToString();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        private HeaderTemplate(long entityId, byte test, string datasetagencyid, string dfid, long? version1, long? version2, long? version3, string agency)
            : this(entityId)
        {
            this.Test = Convert.ToBoolean(test);
            this.DataSetAgencyId = datasetagencyid;
            if (!string.IsNullOrWhiteSpace(dfid))
            {

                var version = string.Join(".", new[] { version1, version2, version3 }.Where(l => l.HasValue).Select(l => l.Value.ToString(CultureInfo.InvariantCulture)));

                this.ParentId = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)
                    .GenerateUrn(agency, dfid, version).ToString();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        private HeaderTemplate(long entityId, bool test, string datasetagencyid, string dfid, long? version1, long? version2, long? version3, string agency)
            : this(entityId)
        {
            this.Test = test;
            this.DataSetAgencyId = datasetagencyid;
            if (!string.IsNullOrWhiteSpace(dfid))
            {

                var version = string.Join(".", new[] { version1, version2, version3 }.Where(l => l.HasValue).Select(l => l.Value.ToString(CultureInfo.InvariantCulture)));

                this.ParentId = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)
                    .GenerateUrn(agency, dfid, version).ToString();
            }
        }


        public HeaderTemplate(
            long entityId):base()
        {
            this.EntityId = entityId.ToString(CultureInfo.InvariantCulture);
        }

        public static bool operator ==(HeaderTemplate left, HeaderTemplate right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(HeaderTemplate left, HeaderTemplate right)
        {
            return !Equals(left, right);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != this.GetType())
            {
                return false;
            }
            return this.Equals((HeaderTemplate) obj);
        }

        /// <summary>
        /// Returns true if HeaderTemplate instances are equal
        /// </summary>
        /// <param name="other">Instance of HeaderTemplate to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(HeaderTemplate other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return
                (
                    this.TransmissionName == other.TransmissionName ||
                    this.TransmissionName != null &&
                    this.TransmissionName.Equals(other.TransmissionName)
                    ) &&
                (
                    this.DataSetAgencyId == other.DataSetAgencyId ||
                    this.DataSetAgencyId != null &&
                    this.DataSetAgencyId.Equals(other.DataSetAgencyId)
                    ) &&
                (
                    this.Source == other.Source ||
                    this.Source != null &&
                    this.Source.Equals(other.Source)
                    ) &&
                (
                    this.Test == other.Test ||
                    this.Test != null &&
                    this.Test.Equals(other.Test)
                    ) &&
                (
                    this.Sender == other.Sender ||
                    this.Sender != null &&
                    this.Sender.Equals(other.Sender)
                    ) &&
                (
                    this.Receiver == other.Receiver ||
                    this.Receiver != null &&
                    this.Receiver.SequenceEqual(other.Receiver)
                    );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                var hash = 41;

                // Suitable nullity checks etc, of course :)
                if (this.TransmissionName != null)
                {
                    hash = hash*59 + this.TransmissionName.GetHashCode();
                }
                if (this.DataSetAgencyId != null)
                {
                    hash = hash*59 + this.DataSetAgencyId.GetHashCode();
                }
                if (this.Source != null)
                {
                    hash = hash*59 + this.Source.GetHashCode();
                }
                if (this.Test != null)
                {
                    hash = hash*59 + this.Test.GetHashCode();
                }
                if (this.Sender != null)
                {
                    hash = hash*59 + this.Sender.GetHashCode();
                }
                if (this.Receiver != null)
                {
                    hash = hash*59 + this.Receiver.GetHashCode();
                }
                return hash;
            }
        }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class HeaderTemplate {\n");
            sb.Append("  Name: ").Append(this.TransmissionName).Append("\n");
            sb.Append("  DataSetAgencyId: ").Append(this.DataSetAgencyId).Append("\n");
            sb.Append("  Source: ").Append(this.Source).Append("\n");
            sb.Append("  Test: ").Append(this.Test).Append("\n");
            sb.Append("  Sender: ").Append(this.Sender).Append("\n");
            sb.Append("  Receiver: ").Append(this.Receiver).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
    }
}