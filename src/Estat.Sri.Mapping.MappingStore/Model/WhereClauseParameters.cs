﻿// -----------------------------------------------------------------------
// <copyright file="WhereClauseParameters.cs" company="EUROSTAT">
//   Date Created : 2017-02-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Model
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data.Common;
    using System.Globalization;

    public class WhereClauseParameters
    {
        
        /// <summary>
        /// Initializes a new instance of the <see cref="WhereClauseParameters"/> class.
        /// </summary>
        /// <param name="whereClause">The clause string.</param>
        /// <param name="parameters">The parameters.</param>
        public WhereClauseParameters(string whereClause, IList<KeyValuePair<string, object>> parameters)
        {
            this.WhereClause = whereClause;
            this.Parameters = new ReadOnlyCollection<KeyValuePair<string, object>>(parameters);
        }

        /// <summary>
        /// Gets the clause string.
        /// </summary>
        /// <value>
        /// The clause.
        /// </value>
        public string WhereClause { get; private set; }

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        public IReadOnlyCollection<KeyValuePair<string, object>> Parameters { get; private set; }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "Clause: {0}, Parameters: {1}", this.WhereClause, this.Parameters);
        }
    }
}