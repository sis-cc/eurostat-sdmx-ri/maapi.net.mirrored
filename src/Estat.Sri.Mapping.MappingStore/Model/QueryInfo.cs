﻿// -----------------------------------------------------------------------
// <copyright file="DatasetColumnInfo.cs" company="EUROSTAT">
//   Date Created : 2014-09-11
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Model
{
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    ///     The dataset column info.
    /// </summary>
    internal class QueryInfo : IDatasetColumnInfo
    {
        public QueryInfo()
        {
        }

        /// <summary>
        ///     Gets or sets the name of the dataset column name.
        /// </summary>
        /// <value>
        ///     The name of the data column.
        /// </value>
        public string ColumnNames { get; set; }

        /// <summary>
        ///     Gets or sets the name of the dataset table. Can be an SQL Query
        /// </summary>
        /// <value>
        ///     The name of the dataset table.
        /// </value>
        public string TableOrQuery { get; set; }
    }
}