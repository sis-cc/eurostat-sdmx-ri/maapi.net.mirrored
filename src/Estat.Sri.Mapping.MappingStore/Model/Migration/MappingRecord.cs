// -----------------------------------------------------------------------
// <copyright file="MappingRecord.cs" company="EUROSTAT">
//   Date Created : 2017-07-05
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Model.Migration
{
    /// <summary>
    /// The mapping record.
    /// </summary>
    internal class MappingRecord : BaseRecord
    {
        /// <summary>
        /// Gets or sets the mapping type.
        /// </summary>
        public string MappingType { get; set; }

        /// <summary>
        /// Gets or sets the constant.
        /// </summary>
        public string Constant { get; set; }

        /// <summary>
        /// Gets or sets the transcoding id.
        /// </summary>
        public long? TranscodingId { get; set; }

        /// <summary>
        /// Gets or sets the expression.
        /// </summary>
        public string Expression { get; set; }
    }
}