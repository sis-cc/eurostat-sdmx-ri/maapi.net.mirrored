// -----------------------------------------------------------------------
// <copyright file="EntityMap.cs" company="EUROSTAT">
//   Date Created : 2017-07-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Model.EntityMapObjects
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// A model class representing an entity map
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public class EntityMap<TEntity> where TEntity : IEntity
    {
        /// <summary>
        /// The dictionary
        /// </summary>
        private readonly Dictionary<string, TEntity> _dictionary = new Dictionary<string, TEntity>(StringComparer.Ordinal);

        /// </summary>
        private readonly Dictionary<string, TEntity> _nameDictionary = new Dictionary<string, TEntity>(StringComparer.Ordinal);

        /// <summary>
        /// Adds a entry to the map
        /// </summary>
        /// <param name="sourceEntity">The source entity.</param>
        /// <param name="targetEntity">The target entity.</param>
        /// <exception cref="System.ArgumentNullException">
        /// sourceEntity
        /// or
        /// targetEntity
        /// </exception>
        public void Add(TEntity sourceEntity, TEntity targetEntity)
        {
            if (sourceEntity == null || string.IsNullOrWhiteSpace(sourceEntity.EntityId))
            {
                throw new ArgumentNullException(nameof(sourceEntity));
            }

            if (targetEntity == null || string.IsNullOrWhiteSpace(targetEntity.EntityId))
            {
                throw new ArgumentNullException(nameof(targetEntity));
            }
            if (!_dictionary.ContainsKey(sourceEntity.EntityId))
            {
                _dictionary.Add(sourceEntity.EntityId, targetEntity);
            }
            var nameableEntity = sourceEntity as ISimpleNameableEntity;
            if (nameableEntity != null)
            {
                if (!_nameDictionary.ContainsKey(nameableEntity.Name))
                {
                    _nameDictionary.Add(nameableEntity.Name, targetEntity);
                }
            }
        }

        /// <summary>
        /// Gets the target entity.
        /// </summary>
        /// <param name="sourceEntity">The source entity.</param>
        /// <returns>The target entity</returns>
        /// <exception cref="System.ArgumentNullException">sourceEntity is null</exception>
        public TEntity GetTargetEntity(TEntity sourceEntity)
        {
            if (sourceEntity == null)
            {
                throw new ArgumentNullException(nameof(sourceEntity));
            }

            return GetTargetEntity(sourceEntity.EntityId);
        }


        /// <summary>
        /// Gets the target entity.
        /// </summary>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns>The target entity</returns>
        /// <exception cref="System.ArgumentNullException">entityId is null</exception>
        public TEntity GetTargetEntity(string entityId)
        {
            if (entityId == null)
            {
                throw new ArgumentNullException(nameof(entityId));
            }

            TEntity targetEntity;
            if (_dictionary.TryGetValue(entityId, out targetEntity))
            {
                return targetEntity;
            }

            return default;
        }

        /// <summary>
        /// Gets the target entity by name.
        /// </summary>
        /// <param name="name">The entity identifier.</param>
        /// <returns>The target entity</returns>
        /// <exception cref="System.ArgumentNullException">entityId is null</exception>
        public TEntity GetTargetEntityName(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            TEntity targetEntity;
            if (_nameDictionary.TryGetValue(name, out targetEntity))
            {
                return targetEntity;
            }

            return default;
        }
    }
}