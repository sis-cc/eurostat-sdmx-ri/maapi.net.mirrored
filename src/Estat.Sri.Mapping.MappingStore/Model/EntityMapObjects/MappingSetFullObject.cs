// -----------------------------------------------------------------------
// <copyright file="MappingSetFullObject.cs" company="EUROSTAT">
//   Date Created : 2017-07-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Model.EntityMapObjects
{
    using System.Collections.Generic;

    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    /// The Mapping Set Map object
    /// </summary>
    internal class MappingSetFullObject
    {
        /// <summary>
        /// Gets or sets the entity.
        /// </summary>
        /// <value>
        /// The entity.
        /// </value>
        public MappingSetEntity Entity { get; set; }

        /// <summary>
        /// Gets or sets the dataflow.
        /// </summary>
        /// <value>
        /// The dataflow.
        /// </value>
        public IDataflowObject Dataflow { get; set; }

        /// <summary>
        /// Gets the mappings.
        /// </summary>
        /// <value>
        /// The mappings.
        /// </value>
        public IList<ComponentMappingFullObject> Mappings { get; } = new List<ComponentMappingFullObject>();
        public IList<TimeDimensionMappingEntity> TimeTranscodings { get; } = new List<TimeDimensionMappingEntity>();
        public IList<LastUpdatedEntity> LastUpdatedEntities { get;  } = new List<LastUpdatedEntity>();
        public IList<ValidToMappingEntity> ValidToMappingEntities { get; } = new List<ValidToMappingEntity>();
        public IList<UpdateStatusEntity> UpdateStatusEntities { get;  } = new List<UpdateStatusEntity>();

    }
}