// -----------------------------------------------------------------------
// <copyright file="MADatasetPropertyEntity.cs" company="EUROSTAT">
//   Date Created : 2021-11-11
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Constant;

namespace Estat.Sri.Mapping.MappingStore.Model
{
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// This class is to populate the <see cref="DatasetPropertyEntity"/> from Dapper.
    /// </summary>
    public class MADatasetPropertyEntity
    {
        private long _propertyTypeId;
        private DatasetPropertyType _propertyType;

        public MADatasetPropertyEntity()
        {
        }

        public MADatasetPropertyEntity(long datasetId, long propertyTypeId, string propertyValue)
        {
            DataSetId = datasetId;
            PropertyTypeId = propertyTypeId;
            PropertyValue = propertyValue;
        }

        /// <summary>
        /// The pk. of property record
        /// </summary>
        public long EntityId { get; set; }


        /// <summary>
        /// The dataset set fk.
        /// </summary>
        public long DataSetId { get; set; }

        /// <summary>
        /// The property type id.
        /// </summary>
        public long PropertyTypeId { get; set; }


        /// <summary>
        /// The property value
        /// </summary>
        public string PropertyValue { get; set; }




    }
}
