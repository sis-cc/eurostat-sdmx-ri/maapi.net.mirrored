// -----------------------------------------------------------------------
// <copyright file="TimeTranscodingFieldOrdinal.cs" company="EUROSTAT">
//   Date Created : 2013-08-05
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Model
{
    using System;
    using System.Data;

    using Estat.Sri.Mapping.Api.Model.AdvancedTime;

    /// <summary>
    ///     The time transcoding field ordinal.
    /// </summary>
    internal class TimeTranscodingFieldOrdinal
    {
        /// <summary>
        ///     The _date column.
        /// </summary>
        private readonly NameOrdinal _dateColumn;

        /// <summary>
        ///     The _name ordinals.
        /// </summary>
        private readonly NameOrdinal[] _nameOrdinals;

        /// <summary>
        ///     The _period column.
        /// </summary>
        private readonly NameOrdinal _periodColumn;

        /// <summary>
        ///     The _year column.
        /// </summary>
        private readonly NameOrdinal _yearColumn;

        /// <summary>
        ///     The _last reader.
        /// </summary>
        private IDataReader _lastReader;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimeTranscodingFieldOrdinal" /> class.
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        /// <param name="expression">
        ///     The expression.
        /// </param>
        public TimeTranscodingFieldOrdinal(TimeExpressionEntity expression)
        {
            this._yearColumn = new NameOrdinal(expression.YearColumnSysId);
            this._periodColumn = new NameOrdinal(expression.PeriodColumnSysId);
            this._dateColumn = new NameOrdinal(expression.DateColumnSysId);
            if (this._yearColumn.IsSet)
            {
                this._nameOrdinals = this._periodColumn.IsSet
                                         ? new[] { this._yearColumn, this._periodColumn }
                                         : new[] { this._yearColumn };
            }
            else
            {
                this._nameOrdinals = new[] { this._dateColumn };
            }
        }

        public TimeTranscodingFieldOrdinal(TimeParticleConfiguration expression)
        {
            this._yearColumn = new NameOrdinal(expression.Year?.Column?.Name);
            this._periodColumn = new NameOrdinal(expression.Period?.Column?.Name);
            this._dateColumn = new NameOrdinal(expression.DateColumn?.Name);
            if (this._yearColumn.IsSet)
            {
                this._nameOrdinals = this._periodColumn.IsSet
                                         ? new[] { this._yearColumn, this._periodColumn }
                                         : new[] { this._yearColumn };
            }
            else
            {
                this._nameOrdinals = new[] { this._dateColumn };
            } 
        }

        /// <summary>
        ///     Gets the date ordinal.
        /// </summary>
        public int DateOrdinal
        {
            get
            {
                return this._dateColumn.Ordinal;
            }
        }

        /// <summary>
        ///     Gets the period ordinal.
        /// </summary>
        public int PeriodOrdinal
        {
            get
            {
                return this._periodColumn.Ordinal;
            }
        }

        /// <summary>
        ///     Gets the year ordinal.
        /// </summary>
        public int YearOrdinal
        {
            get
            {
                return this._yearColumn.Ordinal;
            }
        }

        /// <summary>
        ///     Builds the ordinal.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <exception cref="System.ArgumentNullException"><paramref name="reader" /> is null</exception>
        public void BuildOrdinal(IDataReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (!ReferenceEquals(this._lastReader, reader))
            {
                this._lastReader = reader;
                for (int i = 0; i < this._nameOrdinals.Length; i++)
                {
                    var nameOrdinal = this._nameOrdinals[i];
                    nameOrdinal.Ordinal = reader.GetOrdinal(nameOrdinal.Name);
                }
            }
        }

        /// <summary>
        ///     The name ordinal.
        /// </summary>
        private class NameOrdinal
        {
            /// <summary>
            ///     The _name.
            /// </summary>
            private readonly string _name;

            /// <summary>
            ///     Initializes a new instance of the <see cref="NameOrdinal" /> class.
            ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
            /// </summary>
            /// <param name="name">
            ///     The name.
            /// </param>
            public NameOrdinal(string name)
            {
                this._name = name;
                this.Ordinal = -1;
            }

            /// <summary>
            ///     Gets a value indicating whether is set.
            /// </summary>
            public bool IsSet
            {
                get
                {
                    return this._name != null;
                }
            }

            /// <summary>
            ///     Gets the name.
            /// </summary>
            public string Name
            {
                get
                {
                    return this._name;
                }
            }

            /// <summary>
            ///     Gets or sets the ordinal.
            /// </summary>
            public int Ordinal { get; set; }
        }
    }
}