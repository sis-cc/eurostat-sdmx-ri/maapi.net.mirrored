﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estat.Sri.Mapping.MappingStore.Helpers
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// SQLHelper class
    /// </summary>
    public static class SQLHelper
    {
        /// <summary>
        /// The SQL key words
        /// </summary>
        private static readonly List<string> sqlkeywords;

        /// <summary>
        /// Initializes static members of the <see cref="SQLHelper"/> class.
        /// </summary>
        static SQLHelper()
        {
            sqlkeywords = new List<string>();
            sqlkeywords.Add("ADD");
            sqlkeywords.Add("ALL");
            sqlkeywords.Add("ALLOCATE");
            sqlkeywords.Add("ALTER");
            sqlkeywords.Add("AND");
            sqlkeywords.Add("ANY");
            sqlkeywords.Add("ARE");
            sqlkeywords.Add("ARRAY");
            sqlkeywords.Add("AS");
            sqlkeywords.Add("ASENSITIVE");
            sqlkeywords.Add("ASYMMETRIC");
            sqlkeywords.Add("AT");
            sqlkeywords.Add("ATOMIC");
            sqlkeywords.Add("AUTHORIZATION");
            sqlkeywords.Add("BEGIN");
            sqlkeywords.Add("BETWEEN");
            sqlkeywords.Add("BIGINT");
            sqlkeywords.Add("BINARY");
            sqlkeywords.Add("BLOB");
            sqlkeywords.Add("BOOLEAN");
            sqlkeywords.Add("BOTH");
            sqlkeywords.Add("BY");
            sqlkeywords.Add("CALL");
            sqlkeywords.Add("CALLED");
            sqlkeywords.Add("CASCADED");
            sqlkeywords.Add("CASE");
            sqlkeywords.Add("CAST");
            sqlkeywords.Add("CHAR");
            sqlkeywords.Add("CHARACTER");
            sqlkeywords.Add("CHECK");
            sqlkeywords.Add("CLOB");
            sqlkeywords.Add("CLOSE");
            sqlkeywords.Add("COLLATE");
            sqlkeywords.Add("COLUMN");
            sqlkeywords.Add("COMMIT");
            sqlkeywords.Add("CONDITION");
            sqlkeywords.Add("CONNECT");
            sqlkeywords.Add("CONSTRAINT");
            sqlkeywords.Add("CONTINUE");
            sqlkeywords.Add("CORRESPONDING");
            sqlkeywords.Add("CREATE");
            sqlkeywords.Add("CROSS");
            sqlkeywords.Add("CUBE");
            sqlkeywords.Add("CURRENT");
            sqlkeywords.Add("CURRENT_DATE");
            sqlkeywords.Add("CURRENT_DEFAULT_TRANSFORM_GROUP");
            sqlkeywords.Add("CURRENT_PATH");
            sqlkeywords.Add("CURRENT_ROLE");
            sqlkeywords.Add("CURRENT_TIME");
            sqlkeywords.Add("CURRENT_TIMESTAMP");
            sqlkeywords.Add("CURRENT_TRANSFORM_GROUP_FOR_TYPE");
            sqlkeywords.Add("CURRENT_USER");
            sqlkeywords.Add("CURSOR");
            sqlkeywords.Add("CYCLE");
            sqlkeywords.Add("DATE");
            sqlkeywords.Add("DAY");
            sqlkeywords.Add("DEALLOCATE");
            sqlkeywords.Add("DEC");
            sqlkeywords.Add("DECIMAL");
            sqlkeywords.Add("DECLARE");
            sqlkeywords.Add("DEFAULT");
            sqlkeywords.Add("DELETE");
            sqlkeywords.Add("DEREF");
            sqlkeywords.Add("DESCRIBE");
            sqlkeywords.Add("DETERMINISTIC");
            sqlkeywords.Add("DISCONNECT");
            sqlkeywords.Add("DISTINCT");
            sqlkeywords.Add("DO");
            sqlkeywords.Add("DOUBLE");
            sqlkeywords.Add("DROP");
            sqlkeywords.Add("DYNAMIC");
            sqlkeywords.Add("EACH");
            sqlkeywords.Add("ELEMENT");
            sqlkeywords.Add("ELSE");
            sqlkeywords.Add("ELSEIF");
            sqlkeywords.Add("END");
            sqlkeywords.Add("ESCAPE");
            sqlkeywords.Add("EXCEPT");
            sqlkeywords.Add("EXEC");
            sqlkeywords.Add("EXECUTE");
            sqlkeywords.Add("EXISTS");
            sqlkeywords.Add("EXIT");
            sqlkeywords.Add("EXTERNAL");
            sqlkeywords.Add("FALSE");
            sqlkeywords.Add("FETCH");
            sqlkeywords.Add("FILTER");
            sqlkeywords.Add("FLOAT");
            sqlkeywords.Add("FOR");
            sqlkeywords.Add("FOREIGN");
            sqlkeywords.Add("FREE");
            sqlkeywords.Add("FROM");
            sqlkeywords.Add("FULL");
            sqlkeywords.Add("FUNCTION");
            sqlkeywords.Add("GET");
            sqlkeywords.Add("GLOBAL");
            sqlkeywords.Add("GRANT");
            sqlkeywords.Add("GROUP");
            sqlkeywords.Add("GROUPING");
            sqlkeywords.Add("HANDLER");
            sqlkeywords.Add("HAVING");
            sqlkeywords.Add("HOLD");
            sqlkeywords.Add("HOUR");
            sqlkeywords.Add("IDENTITY");
            sqlkeywords.Add("IF");
            sqlkeywords.Add("IMMEDIATE");
            sqlkeywords.Add("IN");
            sqlkeywords.Add("INDICATOR");
            sqlkeywords.Add("INNER");
            sqlkeywords.Add("INOUT");
            sqlkeywords.Add("INPUT");
            sqlkeywords.Add("INSENSITIVE");
            sqlkeywords.Add("INSERT");
            sqlkeywords.Add("INT");
            sqlkeywords.Add("INTEGER");
            sqlkeywords.Add("INTERSECT");
            sqlkeywords.Add("INTERVAL");
            sqlkeywords.Add("INTO");
            sqlkeywords.Add("IS");
            sqlkeywords.Add("ITERATE");
            sqlkeywords.Add("JOIN");
            sqlkeywords.Add("LANGUAGE");
            sqlkeywords.Add("LARGE");
            sqlkeywords.Add("LATERAL");
            sqlkeywords.Add("LEADING");
            sqlkeywords.Add("LEAVE");
            sqlkeywords.Add("LEFT");
            sqlkeywords.Add("LIKE");
            sqlkeywords.Add("LOCAL");
            sqlkeywords.Add("LOCALTIME");
            sqlkeywords.Add("LOCALTIMESTAMP");
            sqlkeywords.Add("LOOP");
            sqlkeywords.Add("MATCH");
            sqlkeywords.Add("MEMBER");
            sqlkeywords.Add("MERGE");
            sqlkeywords.Add("METHOD");
            sqlkeywords.Add("MINUTE");
            sqlkeywords.Add("MODIFIES");
            sqlkeywords.Add("MODULE");
            sqlkeywords.Add("MONTH");
            sqlkeywords.Add("MULTISET");
            sqlkeywords.Add("NATIONAL");
            sqlkeywords.Add("NATURAL");
            sqlkeywords.Add("NCHAR");
            sqlkeywords.Add("NCLOB");
            sqlkeywords.Add("NEW");
            sqlkeywords.Add("NO");
            sqlkeywords.Add("NONE");
            sqlkeywords.Add("NOT");
            sqlkeywords.Add("NULL");
            sqlkeywords.Add("NUMERIC");
            sqlkeywords.Add("OF");
            sqlkeywords.Add("OLD");
            sqlkeywords.Add("ON");
            sqlkeywords.Add("ONLY");
            sqlkeywords.Add("OPEN");
            sqlkeywords.Add("OR");
            sqlkeywords.Add("ORDER");
            sqlkeywords.Add("OUT");
            sqlkeywords.Add("OUTER");
            sqlkeywords.Add("OUTPUT");
            sqlkeywords.Add("OVER");
            sqlkeywords.Add("OVERLAPS");
            sqlkeywords.Add("PARAMETER");
            sqlkeywords.Add("PARTITION");
            sqlkeywords.Add("PRECISION");
            sqlkeywords.Add("PREPARE");
            sqlkeywords.Add("PRIMARY");
            sqlkeywords.Add("PROCEDURE");
            sqlkeywords.Add("RANGE");
            sqlkeywords.Add("READS");
            sqlkeywords.Add("REAL");
            sqlkeywords.Add("RECURSIVE");
            sqlkeywords.Add("REF");
            sqlkeywords.Add("REFERENCES");
            sqlkeywords.Add("REFERENCING");
            sqlkeywords.Add("RELEASE");
            sqlkeywords.Add("REPEAT");
            sqlkeywords.Add("RESIGNAL");
            sqlkeywords.Add("RESULT");
            sqlkeywords.Add("RETURN");
            sqlkeywords.Add("RETURNS");
            sqlkeywords.Add("REVOKE");
            sqlkeywords.Add("RIGHT");
            sqlkeywords.Add("ROLLBACK");
            sqlkeywords.Add("ROLLUP");
            sqlkeywords.Add("ROW");
            sqlkeywords.Add("ROWS");
            sqlkeywords.Add("SAVEPOINT");
            sqlkeywords.Add("SCOPE");
            sqlkeywords.Add("SCROLL");
            sqlkeywords.Add("SEARCH");
            sqlkeywords.Add("SECOND");
            sqlkeywords.Add("SELECT");
            sqlkeywords.Add("SENSITIVE");
            sqlkeywords.Add("SESSION_USER");
            sqlkeywords.Add("SET");
            sqlkeywords.Add("SIGNAL");
            sqlkeywords.Add("SIMILAR");
            sqlkeywords.Add("SMALLINT");
            sqlkeywords.Add("SOME");
            sqlkeywords.Add("SPECIFIC");
            sqlkeywords.Add("SPECIFICTYPE");
            sqlkeywords.Add("SQL");
            sqlkeywords.Add("SQLEXCEPTION");
            sqlkeywords.Add("SQLSTATE");
            sqlkeywords.Add("SQLWARNING");
            sqlkeywords.Add("START");
            sqlkeywords.Add("STATIC");
            sqlkeywords.Add("SUBMULTISET");
            sqlkeywords.Add("SYMMETRIC");
            sqlkeywords.Add("SYSTEM");
            sqlkeywords.Add("SYSTEM_USER");
            sqlkeywords.Add("TABLE");
            sqlkeywords.Add("TABLESAMPLE");
            sqlkeywords.Add("THEN");
            sqlkeywords.Add("TIME");
            sqlkeywords.Add("TIMESTAMP");
            sqlkeywords.Add("TIMEZONE_HOUR");
            sqlkeywords.Add("TIMEZONE_MINUTE");
            sqlkeywords.Add("TO");
            sqlkeywords.Add("TRAILING");
            sqlkeywords.Add("TRANSLATION");
            sqlkeywords.Add("TREAT");
            sqlkeywords.Add("TRIGGER");
            sqlkeywords.Add("TRUE");
            sqlkeywords.Add("UNDO");
            sqlkeywords.Add("UNION");
            sqlkeywords.Add("UNIQUE");
            sqlkeywords.Add("UNKNOWN");
            sqlkeywords.Add("UNNEST");
            sqlkeywords.Add("UNTIL");
            sqlkeywords.Add("UPDATE");
            sqlkeywords.Add("USER");
            sqlkeywords.Add("USING");
            sqlkeywords.Add("VALUE");
            sqlkeywords.Add("VALUES");
            sqlkeywords.Add("VARCHAR");
            sqlkeywords.Add("VARYING");
            sqlkeywords.Add("WHEN");
            sqlkeywords.Add("WHENEVER");
            sqlkeywords.Add("WHERE");
            sqlkeywords.Add("WHILE");
            sqlkeywords.Add("WINDOW");
            sqlkeywords.Add("WITH");
            sqlkeywords.Add("WITHIN");
            sqlkeywords.Add("WITHOUT");
            sqlkeywords.Add("YEAR");
        }

        /// <summary>
        /// Gets the SQL keywords.
        /// </summary>
        /// <value>
        /// The SQL keywords.
        /// </value>
        public static ICollection<string> SQLKeywords
        {
            get
            {
                return sqlkeywords;
            }
        }

        /// <summary>
        /// Determines whether [is valid column] [the specified column].
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns>True if valid</returns>
        /// <exception cref="ArgumentNullException">Exception column</exception>
        public static bool IsValidColumn(string column)
        {
            if (column == null)
            {
                throw new ArgumentNullException("column");
            }

            if (sqlkeywords.Contains(column.ToUpper()))
            {
                return false;
            }

            return true;
        }
    }
}
