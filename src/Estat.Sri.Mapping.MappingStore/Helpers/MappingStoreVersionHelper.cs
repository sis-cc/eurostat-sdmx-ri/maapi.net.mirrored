namespace Estat.Sri.Mapping.MappingStore.Helpers
{
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using System.Configuration;

    /// <summary>
    /// MappingStoreVersionHelper class
    /// </summary>
    public static class MappingStoreVersionHelper
    {
        public static string GetVersion(IMappingStoreManager mappingStoreManager, string storeId)
        {
            var engine = mappingStoreManager.GetEngineByStoreId(storeId);
            return engine.RetrieveVersion(new DatabaseIdentificationOptions() { StoreId = storeId }).ToString();
        }

        public static bool IsVersion53(IMappingStoreManager mappingStoreManager, string storeId)
        {
            if (ConfigurationManager.AppSettings["MappingStoreVersion53"] == null)
                return false;

            var engine = mappingStoreManager.GetEngineByStoreId(storeId);
            return ConfigurationManager.AppSettings["MappingStoreVersion53"] == engine.RetrieveVersion(new DatabaseIdentificationOptions() { StoreId = storeId }).ToString();
        }
    }
}
