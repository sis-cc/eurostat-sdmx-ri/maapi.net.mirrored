// -----------------------------------------------------------------------
// <copyright file="MappingUtils.cs" company="EUROSTAT">
//   Date Created : 2013-12-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Sdmx.MappingStore.Retrieve;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     Helper class with methods that check if the mapping is complete and other helper methods
    /// </summary>
    public class MappingUtils : IComponentMappingValidator
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(MappingUtils));

        /// <summary>
        /// Validates the specified component mappings.
        /// </summary>
        /// <param name="componentMappings">The component mappings.</param>
        /// <param name="dataStructure">The data structure.</param>
        /// <returns>
        /// The <see cref="IActionResult" />.
        /// </returns>
        public IActionResult Validate(IComponentMappingContainer componentMappings, IDataStructureObject dataStructure)
        {
            return IsMappingSetComplete(dataStructure, componentMappings);
        }

        /// <summary>
        ///     This method checks whether a mapping set is complete, in order to be used for the Data Retriever.
        ///     For example,one of the checks is whether all dimensions and mandatory attributes are mapped.
        /// </summary>
        /// <param name="dsd">The DSD to be checked</param>
        /// <param name="componentMapping">Map between components and their mapping</param>
        /// <returns>
        ///     True if the mapping is complete
        /// </returns>
        private static IActionResult IsMappingSetComplete(
            IDataStructureObject dsd, 
            IComponentMappingContainer componentMapping)
        {
            var errorMessages = new List<string>();
            var returnValue = StatusType.Success;

            bool measureDimensionNotMapped = false;
            if (dsd == null)
            {
                throw new ArgumentNullException("dsd");
            }

            var dimensions = dsd.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension);
            if (ObjectUtil.ValidCollection(dimensions))
            {
                int i = 0;

                // check if all dimensions are mapped
                while (i < dimensions.Count)
                {
                    if (componentMapping == null)
                    {
                        throw new ArgumentNullException("componentMapping");
                    }

                    var dimension = dimensions[i];
                    var id = dimension.Id;
                    if (!dimension.MeasureDimension && !componentMapping.ComponentMappingBuilders.ContainsKey(id))
                    {
                        var message = string.Format(
                            CultureInfo.InvariantCulture, 
                            "Dimension '{0}' is not mapped.", 
                            id);
                        _log.Warn(message);
                        errorMessages.Add(message);
                        returnValue = StatusType.Error;
                    }
                    else if (dimension.MeasureDimension && !componentMapping.ComponentMappingBuilders.ContainsKey(id))
                    {
                        measureDimensionNotMapped = true;
                    }

                    i++;
                }
            }
            else
            {
                string message = string.Format(
                    CultureInfo.InvariantCulture, 
                    "No dimensions defined in the DSD {0}", 
                    dsd.Id);

                _log.Info(message);
                errorMessages.Add(message);

                // throw new DataRetrieverException(ErrorTypes.NO_MAPPING_SET, message);
                return new ActionResult("400", StatusType.Error, message);
            }

            // check if time dimension is mapped, if one exists.
            if (dsd.TimeDimension != null)
            {
                returnValue = CheckTimeDimension(dsd, componentMapping, errorMessages, returnValue);
            }

            // check xs-measures
            var crossDsd = dsd as ICrossSectionalDataStructureObject;
            if (crossDsd != null && ObjectUtil.ValidCollection(crossDsd.CrossSectionalMeasures) && measureDimensionNotMapped)
            {
                returnValue = CheckXsMeasures(crossDsd, componentMapping, errorMessages, returnValue);
            }
            else if(dsd.Measures != null)
            {
                foreach (var measure in dsd.Measures)
                {
                    if (measure.Usage == UsageType.Mandatory && !componentMapping.ComponentMappingBuilders.ContainsKey(measure.Id))
                    {
                        var message = string.Format(
                            CultureInfo.InvariantCulture,
                            "Dimension '{0}' is not mapped.",
                            measure.Id);
                        _log.Warn(message);
                        errorMessages.Add(message);
                        returnValue = StatusType.Error;
                    }
                }
            }
            else if (dsd.PrimaryMeasure != null)
            {
                returnValue = CheckPrimaryMeasure(dsd, componentMapping, errorMessages, returnValue);
            }

            // atributes
            if (dsd.Attributes != null && dsd.Attributes.Count > 0)
            {
                returnValue = CheckAttributes(dsd, componentMapping, errorMessages, returnValue);
            }

            if (componentMapping == null)
            {
                throw new ArgumentNullException("componentMapping");
            }

            // test if 1-N or N-1 scalar mappings have transcoding
            // or 1-1 array mappings have transcoding
            foreach (var kv in componentMapping.ComponentMappings)
            {
                if (!kv.Value.HasTranscoding() && kv.Value.GetColumns().Count > 1 &&
                    kv.Value.Component.ValueType == ComponentValueType.Scalar)
                {
                    var message = string.Format(
                        CultureInfo.CurrentCulture, 
                        ErrorMessages.ComponentMappingNoTranscodingFormat1, 
                        kv.Key);
                    
                    errorMessages.Add(message);
                    _log.Warn(message);
                    returnValue = StatusType.Error;
                }
                if (!kv.Value.HasTranscoding() && kv.Value.GetColumns().Count == 1 &&
                    kv.Value.Component.ValueType == ComponentValueType.Array)
                {
                    var message = string.Format(
                        CultureInfo.CurrentCulture,
                        ErrorMessages.ComponentMappingArrayNoTranscodingFormat1,
                        kv.Key);

                    errorMessages.Add(message);
                    _log.Warn(message);
                    returnValue = StatusType.Error;
                }
            }

            var infoMessage = string.Format(
                CultureInfo.InvariantCulture,
                "End of mapping completeness check. IsMappingSetComplete result is : {0}",
                returnValue != StatusType.Error ? "OK" : "Incomplete");
            _log.Info(infoMessage);
            errorMessages.Add(infoMessage);

            return new ActionResult(returnValue == StatusType.Error ? "400" : "200", returnValue, errorMessages.ToArray());
        }
        

        /// <summary>
        /// Checks the attributes.
        /// </summary>
        /// <param name="dsd">The DSD.</param>
        /// <param name="componentMapping">The component mapping.</param>
        /// <param name="messages">The messages.</param>
        /// <param name="returnValue">if set to <c>true</c> [return value].</param>
        /// <returns>
        /// true if set, false otherwise
        /// </returns>
        private static StatusType CheckAttributes(
            IDataStructureObject dsd, 
            IComponentMappingContainer componentMapping,
               IList<string> messages,
            StatusType returnValue)
        {
            foreach (var component in dsd.Attributes)
            {
                if (componentMapping == null)
                {
                    throw new ArgumentNullException("componentMapping");
                }

                if (string.Equals(component.AssignmentStatus, AttributeAssignmentStatus.Mandatory.ToString(), StringComparison.OrdinalIgnoreCase) && !componentMapping.ComponentMappingBuilders.ContainsKey(component.Id))
                {
                    var message = string.Format(
                        CultureInfo.InvariantCulture, 
                        ErrorMessages.MandatoryAttributeNotMappedFormat1, 
                        component.Id);
                    messages.Add(message);
                    _log.Warn(message);
                    returnValue = StatusType.Error;
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Checks the primary measure.
        /// </summary>
        /// <param name="dsd">
        /// The DSD.
        /// </param>
        /// <param name="componentMapping">
        /// The component mapping.
        /// </param>
        /// <param name="messages">
        /// The messages.
        /// </param>
        /// <param name="returnValue">
        /// if set to <c>true</c> [return value].
        /// </param>
        /// <returns>
        /// true if set
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// component Mapping
        /// </exception>
        private static StatusType CheckPrimaryMeasure(
            IDataStructureObject dsd, 
            IComponentMappingContainer componentMapping,
             IList<string> messages,
            StatusType returnValue)
        {
            if (componentMapping == null)
            {
                throw new ArgumentNullException("componentMapping");
            }

            // primary measure
            if (!componentMapping.ComponentMappingBuilders.ContainsKey(dsd.PrimaryMeasure.Id))
            {
                {
                    var message = string.Format(
                        CultureInfo.InvariantCulture, 
                        ErrorMessages.PrimaryMeasureNotMappedFormat1, 
                        dsd.PrimaryMeasure.Id);
                    messages.Add(message);
                    _log.Warn(message);
                    returnValue = StatusType.Error;
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Checks the time dimension.
        /// </summary>
        /// <param name="dsd">The DSD.</param>
        /// <param name="componentMapping">The component mapping.</param>
        /// <param name="messages">The messages.</param>
        /// <param name="returnValue">if set to <c>true</c> [return value].</param>
        /// <returns>
        /// true if set
        /// </returns>
        private static StatusType CheckTimeDimension(
            IDataStructureObject dsd, 
            IComponentMappingContainer componentMapping, 
            IList<string> messages,
            StatusType returnValue)
        {
            if (componentMapping == null)
            {
                throw new ArgumentNullException("componentMapping");
            }

            if (componentMapping.TimeDimensionBuilder == null)
            {
                var message = string.Format(
                    CultureInfo.InvariantCulture, 
                    "Time Dimension '{0}' is not mapped.", 
                    dsd.TimeDimension.Id);
                _log.Warn(message);
                messages.Add(message);

                returnValue = StatusType.Error;
            }

            return returnValue;
        }

        /// <summary>
        /// Checks the XS measures.
        /// </summary>
        /// <param name="dsd">The DSD.</param>
        /// <param name="componentMapping">The component mapping.</param>
        /// <param name="messages">The messages.</param>
        /// <param name="returnValue">if set to <c>true</c> [return value].</param>
        /// <returns>
        /// true if set
        /// </returns>
        private static StatusType CheckXsMeasures(
            ICrossSectionalDataStructureObject dsd,
            IComponentMappingContainer componentMapping,
               IList<string> messages,
            StatusType returnValue)
        {
            foreach (ICrossSectionalMeasure component in dsd.CrossSectionalMeasures)
            {
                if (!componentMapping.ComponentMappingBuilders.ContainsKey(component.Id))
                {
                    var message = string.Format(
                        CultureInfo.InvariantCulture, 
                        "Cross Sectional Measure '{0}' is not mapped.", 
                        component.Id);
                    messages.Add(message);
                    _log.Warn(message);
                    returnValue = StatusType.Error;
                }
            }

            return returnValue;
        }
   }
}