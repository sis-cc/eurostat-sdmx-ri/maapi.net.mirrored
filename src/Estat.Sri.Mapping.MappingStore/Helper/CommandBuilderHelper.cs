// -----------------------------------------------------------------------
// <copyright file="CommandBuilderHelper.cs" company="EUROSTAT">
//   Date Created : 2022-08-03
//   Copyright (c) 2009, 2022 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using Dapper;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.Mapping.MappingStore.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Helper
{
    public static class CommandBuilderHelper
    {
        public static IEnumerable<CommandDefinition> InsertInformation(Database database,
            DatabaseInformationType informationType, Dictionary<string, object> pathAndValues, long entityId)
        {
            return InsertInformation(database, informationType, pathAndValues,
                Convert.ToString(entityId, CultureInfo.InvariantCulture));
        }
        public static IEnumerable<CommandDefinition> InsertInformation(Database database, DatabaseInformationType informationType, Dictionary<string, object> pathAndValues, string entityId)
        {
            var updateInfoAction = new UpdateInfoAction()
            {
                DataInformationType = informationType,
                SqlStatementType = SqlStatementType.Insert,
                PathAndValues = pathAndValues
            };
            var commandDefinitionManager = new CommandDefinitionManager(new SqlBuilderFactory(new DatabaseMapperManager(updateInfoAction.DataInformationType)));
            var commandDefinitionBuilder = commandDefinitionManager.GetBuilder(updateInfoAction.DataInformationType, updateInfoAction.SqlStatementType);
            return commandDefinitionBuilder.GetCommands(updateInfoAction, entityId, database);
        }
    }
}