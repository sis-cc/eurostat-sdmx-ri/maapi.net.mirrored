// -----------------------------------------------------------------------
// <copyright file="DatasetPropertyHelper.cs" company="EUROSTAT">
//   Date Created : 2021-11-11
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.Linq;
using Dapper;
using Estat.Sri.MappingStore.Store.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Helper
{
    using System;
    using System.Collections.Concurrent;
    using System.Globalization;

    using Estat.Sri.Mapping.Api.Constant;
    using log4net;

    /// <summary>
    ///     Helper class with methods that check if the mapping is complete and other helper methods
    /// </summary>
    public class DatasetPropertyHelper
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(MappingUtils));

        private static readonly ConcurrentDictionary<string, ConcurrentDictionary<DatasetPropertyType, long>> _propertyTypeMapDict = new ConcurrentDictionary<string, ConcurrentDictionary<DatasetPropertyType, long>>();

        private readonly ConcurrentDictionary<DatasetPropertyType, long> _propertyTypeMap;

        private static readonly object _buildPropertyTypeMapLock = new object();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="database"></param>
        /// <param name="transaction"></param>
        public DatasetPropertyHelper(Database database, IDbTransaction transaction = null)
        {
            _propertyTypeMap = GetDatasetPropertyTypeMap(database, transaction);
        }

        /// <summary>
        ///     Gets DatasetPropertyType from database id
        /// </summary>
        /// <param name="idValue">The database id of dataset property type</param>
        public DatasetPropertyType? GetDatasetPropertyTypeFromID(long idValue)
        {
            if (!_propertyTypeMap.Values.Contains(idValue))
            {
                return null;
            }

            return _propertyTypeMap.First(kv => kv.Value == idValue).Key;
        }

        /// <summary>
        ///     Gets database id from DatasetPropertyType
        /// </summary>
        /// <param name="enumValue">The <see cref="DatasetPropertyType"/> value</param>
        public long? GetDatasetPropertyIdFromType(DatasetPropertyType enumValue)
        {
            if (!_propertyTypeMap.TryGetValue(enumValue, out var datasetPropertyId))
            {
                return null;
            }

            return datasetPropertyId;
        }

        /// <summary>
        ///     Retrieves the property type mappings
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="transaction">The transaction.</param>
        private ConcurrentDictionary<DatasetPropertyType, long> GetDatasetPropertyTypeMap(Database database, IDbTransaction transaction = null)
        {
            ConcurrentDictionary<DatasetPropertyType, long> propertyTypeMap = null;

            if (!_propertyTypeMapDict.TryGetValue(database.Name, out propertyTypeMap))
            {
                lock (_buildPropertyTypeMapLock)
                {
                    if (!_propertyTypeMapDict.TryGetValue(database.Name, out propertyTypeMap))
                    {
                        if (transaction != null)
                        {
                            propertyTypeMap = BuildPropertyTypeMap(transaction.Connection, transaction);
                        }
                        else
                        {
                            using (var connection = database.CreateConnection())
                            {
                                connection.Open();
                                propertyTypeMap = BuildPropertyTypeMap(connection);
                            }
                        }

                        _propertyTypeMapDict[database.Name] = propertyTypeMap;
                    }
                }
            }

            return propertyTypeMap;
        }

        /// <summary>
        ///     Builds property type map.
        /// </summary>
        /// <param name="connection">
        ///     The connection.
        /// </param>
        /// <param name="transaction">
        ///     The transaction.
        /// </param>
        private ConcurrentDictionary<DatasetPropertyType, long> BuildPropertyTypeMap(IDbConnection connection, IDbTransaction transaction = null)
        {
            var propertyTypeMap = new ConcurrentDictionary<DatasetPropertyType, long>();

            foreach (var enumValue in
                connection.Query<EnumerationValue>("select PROPERTY_ID AS ID, PROPERTY_NAME AS NAME, PROPERTY_DATATYPE AS VALUE from DATASET_PROPERTY_TYPE", null, transaction, false))
            {
                if (Enum.TryParse(enumValue.Name, true, out DatasetPropertyType datasetPropertyType))
                {
                    propertyTypeMap[datasetPropertyType] = enumValue.ID;
                }
                else
                {
                    _log.WarnFormat(CultureInfo.InvariantCulture, "Unsupported DATASET_PROPERTY_TYPE: {0}",
                        enumValue.Name);
                }
            }

            return propertyTypeMap;
        }
    }
}