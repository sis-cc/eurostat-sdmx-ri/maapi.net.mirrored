// -----------------------------------------------------------------------
// <copyright file="SdmxCodeMapRetriever.cs" company="EUROSTAT">
//   Date Created : 2013-07-26
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The SDMX code map retriever.
    /// </summary>
    [Obsolete("No longer required and doesn't work with MADB7")]
    public class SdmxCodeMapRetriever
    {
        /// <summary>
        ///     The _database.
        /// </summary>
        private readonly Database _database;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxCodeMapRetriever" /> class.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        public SdmxCodeMapRetriever(Database database)
        {
            this._database = database;
        }

        /// <summary>
        ///     Build the SDMX code maps.
        /// </summary>
        /// <returns>
        ///     The SDMX code maps.
        /// </returns>
        public IDictionary<TimeFormatEnumType, IDictionary<string, long>> BuildSdmxCodeIdToSysIdMap()
        {
            IDictionary<TimeFormatEnumType, IDictionary<string, long>> sdmxCodeDictionary = new Dictionary<TimeFormatEnumType, IDictionary<string, long>>();
            var lastType = TimeFormatEnumType.Null;
            IDictionary<string, long> codeMaps = new Dictionary<string, long>(StringComparer.Ordinal);
            Action<TimeFormatEnumType, string, long> addToMap = (timeFormat, codeId, primaryKey) =>
                {
                    if (lastType != timeFormat)
                    {
                        codeMaps = new Dictionary<string, long>(StringComparer.Ordinal);
                        sdmxCodeDictionary.Add(timeFormat, codeMaps);
                        lastType = timeFormat;
                    }

                    codeMaps.Add(codeId, primaryKey);
                };

            BuildSdmxCodeMapsInternal(this._database, addToMap);

            return sdmxCodeDictionary;
        }

        /// <summary>
        ///     Build the SDMX code maps.
        /// </summary>
        /// <returns>
        ///     The SDMX code maps.
        /// </returns>
        public IDictionary<TimeFormatEnumType, IDictionary<long, string>> BuildSdmxSysIdToCodeIdMap()
        {
            IDictionary<TimeFormatEnumType, IDictionary<long, string>> sdmxCodeDictionary = new Dictionary<TimeFormatEnumType, IDictionary<long, string>>();
            var lastType = TimeFormatEnumType.Null;
            IDictionary<long, string> codeMaps = new Dictionary<long, string>();
            Action<TimeFormatEnumType, string, long> addToMap = (timeFormat, codeId, primaryKey) =>
                {
                    if (lastType != timeFormat)
                    {
                        codeMaps = new Dictionary<long, string>();
                        sdmxCodeDictionary.Add(timeFormat, codeMaps);
                        lastType = timeFormat;
                    }

                    codeMaps.Add(primaryKey, codeId);
                };

            BuildSdmxCodeMapsInternal(this._database, addToMap);

            return sdmxCodeDictionary;
        }

        /// <summary>
        ///     Build the SDMX code maps.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <param name="addToMap">
        ///     The add To Map method.
        /// </param>
        private static void BuildSdmxCodeMapsInternal(Database database, Action<TimeFormatEnumType, string, long> addToMap)
        {
            var version = PeriodCodelist.VersionParticles;
            DbParameter idParameter = database.CreateInParameter("id", DbType.AnsiString);
            DbParameter agencyIdParameter = database.CreateInParameter("agencyId", DbType.AnsiString, PeriodCodelist.Agency);
            DbParameter version1Parameter = database.CreateInParameter("version1", DbType.Int64, version[0].HasValue ? version[0].Value : 0);
            DbParameter version2Parameter = database.CreateInParameter("version2", DbType.Int64, version[1].HasValue ? version[1].Value : 0);
            DbParameter version3Parameter = database.CreateInParameter("version3", DbType.Int64, version[2].HasValue ? (object)version[2] : DBNull.Value);

            using (var command = database.GetSqlStringCommandFormat("select i.ID as codeId, i.ITEM_ID as sysId from ITEM i inner join DSD_CODE dc on i.ITEM_ID = dc.LCD_ID inner join ARTEFACT a on dc.CL_ID = a.ART_ID where a.ID = {0} and a.AGENCY = {1} and " +
                                                                    "A.Version1 ={2} AND A.Version2 = {3} AND (((A.Version3 is null) and ({4} is null)) or (((A.Version3 is not null) and ({4} is not null)) and ( A.Version3 = {4} )))", idParameter, agencyIdParameter, version1Parameter, version2Parameter, version3Parameter))
            {
                foreach (var periodObject in PeriodCodelist.PeriodCodelistIdMap)
                {
                    idParameter.Value = periodObject.Value.Id;
                    var timeFormat = TimeFormat.GetTimeFormatFromCodeId(periodObject.Key);

                    using (var reader = database.ExecuteReader(command))
                    {
                        var idIdx = reader.GetOrdinal("codeId");
                        var valueIdx = reader.GetOrdinal("sysId");
                        while (reader.Read())
                        {
                            string key = DataReaderHelper.GetString(reader, idIdx);
                            long value = DataReaderHelper.GetInt64(reader, valueIdx);
                            addToMap(timeFormat.EnumType, key, value);
                        }
                    }
                }
            }
        }
    }
}