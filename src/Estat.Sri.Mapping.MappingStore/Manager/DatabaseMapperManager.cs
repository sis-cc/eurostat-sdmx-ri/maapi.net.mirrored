// -----------------------------------------------------------------------
// <copyright file="DatabaseMapperManager.cs" company="EUROSTAT">
//   Date Created : 2017-02-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.DatabaseModel;
using Estat.Sri.Mapping.MappingStore.Factory;

namespace Estat.Sri.Mapping.MappingStore.Manager
{
    /// <summary>
    /// IDataTableInformations
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.MappingStore.Manager.IDatabaseMapperManager" />
    public class DatabaseMapperManager : IDatabaseMapperManager
    {
        private readonly IDataTableInformations _dataTableInformations;
        private DataTableInformationFactory _dataTableInformationFactory = new DataTableInformationFactory();

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseMapperManager"/> class.
        /// </summary>
        /// <param name="databaseInformationType">Type of the database information.</param>
        public DatabaseMapperManager(DatabaseInformationType databaseInformationType)
        {
            this._dataTableInformations = this._dataTableInformationFactory.GetInformations(databaseInformationType);
        }

        /// <summary>
        /// Gets the column mapping.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string GetColumnMapping(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return string.Empty;
            }

            return this._dataTableInformations.GetColumn(key);
        }

        /// <summary>
        /// Gets the primary key column.
        /// </summary>
        /// <returns></returns>
        public string GetPrimaryKeyColumn()
        {
            return this._dataTableInformations.GetPrimaryKeyModelName();
        }

        /// <summary>
        /// Gets the procedure parameter mapping.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string GetProcedureParameterMapping(string key)
        {
            return this._dataTableInformations.GetProcParameter(key);
        }

        /// <summary>
        /// Gets the name of the table.
        /// </summary>
        /// <returns></returns>
        public string GetTableName()
        {
            return this._dataTableInformations.TableName;
        }
        /// <summary>
        /// Gets the name of the table.
        /// </summary>
        /// <returns></returns>
        public string GetBaseTableName()
        {
            return this._dataTableInformations.BaseTable;
        }


        public IEnumerable<DatabaseInformationType> GetChildTableInformations()
        {
            return this._dataTableInformations.ChildTableInformations;
        }


        public string GetForeignKey(DatabaseInformationType parentType)
        {
            var dataColumnInformations = this._dataTableInformations.Mappings.Intersect(this._dataTableInformationFactory.GetInformations(parentType).Mappings);
            return dataColumnInformations.First().ModelName;
        }
    }
}