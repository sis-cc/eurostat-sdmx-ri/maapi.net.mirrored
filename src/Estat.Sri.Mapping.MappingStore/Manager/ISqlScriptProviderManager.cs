﻿// -----------------------------------------------------------------------
// <copyright file="ISqlScriptProviderManager.cs" company="EUROSTAT">
//   Date Created : 2017-03-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Manager
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The SqlScriptProviderManager interface.
    /// </summary>
    public interface ISqlScriptProviderManager
    {
        /// <summary>
        /// Gets the full schema.
        /// </summary>
        /// <param name="databaseType">Type of the database.</param>
        /// <returns>The SQL Statements</returns>
        IEnumerable<string> GetFullSchema(string databaseType);

        /// <summary>
        /// Gets the upgrade.
        /// </summary>
        /// <param name="databaseType">Type of the database.</param>
        /// <param name="currentVersion">The current version.</param>
        /// <param name="targetVersion">The target version.</param>
        /// <returns>The SQL Statements</returns>
        IEnumerable<string> GetUpgrade(string databaseType, Version currentVersion, Version targetVersion);

        /// <summary>
        /// Gets the available upgrade version.
        /// </summary>
        /// <param name="databaseType">Type of the database.</param>
        /// <param name="currentVersion">The current version.</param>
        /// <returns>The available versions for upgrading</returns>
        IEnumerable<Version> GetAvailableUpgradeVersion(string databaseType, Version currentVersion);
    }
}