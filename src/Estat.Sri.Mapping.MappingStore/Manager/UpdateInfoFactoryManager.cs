// -----------------------------------------------------------------------
// <copyright file="UpdateInfoFactoryManager.cs" company="EUROSTAT">
//   Date Created : 2017-03-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Manager
{
    /// <summary>
    /// UpdateInfoFactoryManager
    /// </summary>
    public class UpdateInfoFactoryManager
    {
        /// <summary>
        /// Gets the factory.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="database">The database.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public UpdateInfoFactory GetFactory(EntityType entityType, Database database)
        {
            switch (entityType)
            {
                case EntityType.MappingSet:
                    return new MappingSetUpdateInfoFactory(database);
                case EntityType.DdbConnectionSettings:
                    return new DdbConnectionSettingsUpdateInfoFactory(database);
                case EntityType.DataSetColumn:
                    return new DataSetColumnUpdateInfoFactory(database);
                case EntityType.DataSet:
                    return new DatasetUpdateInfoFactory(database);
                case EntityType.LocalCode:
                    return new LocalCodeUpdateInfoFactory(database);
                case EntityType.Transcoding:
                    return new TranscodingInfoFactory(database);
                case EntityType.TimeTranscoding:
                    return new TimeTranscodingInfoFactory(database);
                case EntityType.TranscodingRule:
                    return new TranscodingRuleInfoFactory(database);
                case EntityType.TranscodingScript:
                    return new TranscodingScriptInfoFactory(database);
                case EntityType.Mapping:
                    return new MappingInfoFactory(database);
                case EntityType.DescSource:
                    return new DescSourceInfoFactory(database);
                case EntityType.Header:
                    return new HeaderInfoFactory(database);
                case EntityType.Registry:
                    return new RegistryInfoFactory(database);
                case EntityType.User:
                    return new UserInfoFactory(database);
                case EntityType.TemplateMapping:
                    return new TemplateMappingUpdateFactory(database);
                case EntityType.DataSource:
                    return new DataSourceInfoFactory(database);
                case EntityType.Nsiws:
                    return new NsiwsInfoFactory(database);
            }
            throw new NotImplementedException($"UpdateInfoFactoryManager does not have a case value for entity type{entityType}");
        }
    }

   
}