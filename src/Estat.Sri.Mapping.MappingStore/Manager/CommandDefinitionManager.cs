﻿// -----------------------------------------------------------------------
// <copyright file="CommandDefinitionManager.cs" company="EUROSTAT">
//   Date Created : 2017-02-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.MappingStore.Builder;
using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Factory;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Manager
{
    /// <summary>
    /// CommandDefinitionManager
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.MappingStore.Manager.ICommandDefinitionManager" />
    public class CommandDefinitionManager : ICommandDefinitionManager
    {
        private readonly ISqlBuilderFactory _sqlBuilderFactory;
        private readonly ICommandDefinitionSpecification _storedProcedureCommandDefinitionSpecification;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandDefinitionManager"/> class.
        /// </summary>
        /// <param name="sqlBuilderFactory">The SQL builder factory.</param>
        public CommandDefinitionManager(ISqlBuilderFactory sqlBuilderFactory)
        {
            this._sqlBuilderFactory = sqlBuilderFactory;
            this._storedProcedureCommandDefinitionSpecification = new StoredProcedureCommandDefinitionSpecification();
        }

        /// <summary>
        /// Gets the builder.
        /// </summary>
        /// <param name="databaseInformationType">Type of the database information.</param>
        /// <param name="sqlStatementType">The json operation.</param>
        /// <returns></returns>
        public ICommandDefinitionBuilder GetBuilder(DatabaseInformationType databaseInformationType, SqlStatementType sqlStatementType)
        {
            if (this._storedProcedureCommandDefinitionSpecification.IsSatisfied(databaseInformationType, sqlStatementType))
            {
                return new StoredProcedureCommandDefinitionBuilder();
            }

            return new SingleTableCommandDefinitionBuilder(this._sqlBuilderFactory);
        }
    }
}