// -----------------------------------------------------------------------
// <copyright file="StoreImportManager.cs" company="EUROSTAT">
//   Date Created : 2017-08-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;

namespace Estat.Sri.Mapping.MappingStore.Manager
{
    using System;
    using System.Diagnostics;
    using System.IO.Compression;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Engine.Streaming;
    using Estat.Sri.Mapping.MappingStore.Builder;
    using Estat.Sri.Mapping.MappingStore.Engine.Cloning;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStore.Store.Manager;
    using Estat.Sri.MappingStore.Store.Model;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Persist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    public class StoreImportManager : IStoreImportManager
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(StoreImportManager));

        private readonly IEntityPersistenceManager _persistenceManager;
        private readonly IEntityRetrieverManager _retrieverManager;

        private readonly IEntityTypeBuilder _entityTypeBuilder;
        private readonly ExportImportPathBuilder _exportImportPathBuilder = new ExportImportPathBuilder();

        private readonly IStructureParsingManager _structureParsingManager;

        private readonly IReadableDataLocationFactory _dataLocationFactory;

        private readonly DatabaseManager _databaseManager;

        public StoreImportManager(IEntityPersistenceManager persistenceManager, IEntityRetrieverManager retrieverManager, IEntityTypeBuilder entityTypeBuilder, IStructureParsingManager structureParsingManager, IReadableDataLocationFactory dataLocationFactory, DatabaseManager databaseManager)
        {
            this._persistenceManager = persistenceManager;
            this._retrieverManager = retrieverManager;
            this._entityTypeBuilder = entityTypeBuilder;
            this._structureParsingManager = structureParsingManager;
            this._dataLocationFactory = dataLocationFactory;
            this._databaseManager = databaseManager;
        }

        public void Import(Stream stream, Encoding encoding, string sid)
        {
            this.ImportEntities(stream, encoding, sid);
        }

        public void ImportWithSdmx(Stream stream, Encoding encoding, string sid)
        {
            using (ZipArchive archive = new ZipArchive(stream, ZipArchiveMode.Read, true))
            {
                var exportImportEntryPaths = this._exportImportPathBuilder.Build(archive);
                ISdmxObjects objects = new SdmxObjectsImpl();
                objects.Action = DatasetAction.FromDatasetActionEnumType(DatasetActionEnumType.Append);
                this.GetSdmxObjects(exportImportEntryPaths.SdmxV20, objects);
                this.GetSdmxObjects(exportImportEntryPaths.SdmxV21, objects);
                this.GetSdmxObjects(exportImportEntryPaths.SdmxV30, objects);
                var database = this._databaseManager.GetConnectionStringSettings(new DatabaseIdentificationOptions { StoreId = sid });
                IStructurePersistenceManager structurePersistence = new MappingStoreManager(database, new List<ArtefactImportStatus>());
                _log.Debug("Start importing SDMX");
                structurePersistence.SaveStructures(objects);
                _log.Debug("End import SDMX");
                if (exportImportEntryPaths.Mapping != null)
                {
                    _log.Debug("Start importing Mapping");
                    using (var mappingStream = exportImportEntryPaths.Mapping.Value.Open())
                    {
                         this.ImportEntities(mappingStream, encoding, sid);
                    }

                    _log.Debug("End import Mapping");
                }
            }
        }

        private void GetSdmxObjects(Lazy<ZipArchiveEntry> path, ISdmxObjects objects)
        {
            if (path != null && path.Value != null)
            {
                using (var streamReader = path.Value.Open())
                {
                    using (var readableLocation = this._dataLocationFactory.GetReadableDataLocation(streamReader))
                    {
                        objects.Merge(readableLocation.GetSdmxObjects(this._structureParsingManager));
                    }
                }
            }
        }

        private void ImportEntities(Stream stream, Encoding encoding, string sid)
        {
            _persistenceManager.Import(sid, new EntityStreamingReader(sid, stream,EntityType.None), new EntityStreamingWriter(Stream.Null));
           // EntitiesByType entitiesByType;
           // using (var xmlEntityReaderEngine = new EntityStreamingReader(sid, stream))
           // {
           //     xmlEntityReaderEngine.MoveToNextMessage();
           //     entitiesByType = xmlEntityReaderEngine.ReadAll();
           // }
           // // TODO Details.AllIds
           // // There should be only one Mapping Set per parent
           // var dataflowsWithMappingset = new HashSet<string>(this._retrieverManager.GetEntities<MappingSetEntity>(sid, EntityQuery.Empty, Detail.Full).Select(entity => entity.ParentId), StringComparer.Ordinal);

           // // we need to add the entities in a specific order and map the XML id to 
           // var ddbConnectionEntities = entitiesByType.GetEntities<IConnectionEntity>().WithMappingStoreId(sid).ToArray();
           // var datasets = entitiesByType.GetEntities<DatasetEntity>().WithMappingStoreId(sid).ToArray();
           // var datasetColumns = entitiesByType.GetEntities<DataSetColumnEntity>().WithMappingStoreId(sid).ToArray();
           // var mappingSet = entitiesByType.GetEntities<MappingSetEntity>().WithMappingStoreId(sid).Where(entity => !dataflowsWithMappingset.Contains(entity.ParentId)).ToArray();
           // var mappings = entitiesByType.GetEntities<ComponentMappingEntity>().WithMappingStoreId(sid).ToArray();
           // var transcoding = entitiesByType.GetEntities<TranscodingEntity>().WithMappingStoreId(sid).ToArray();
           // var rules = entitiesByType.GetEntities<TranscodingRuleEntity>().WithMappingStoreId(sid).ToArray();
           // var columnDescriptions = entitiesByType.GetEntities<ColumnDescriptionSourceEntity>().WithMappingStoreId(sid).ToArray();
           //// var registries = entitiesByType.GetEntities<RegistryEntity>().WithMappingStoreId(sid).ToArray();
           //// var templateMappings = entitiesByType.GetEntities<TemplateMapping>().WithMappingStoreId(sid).ToArray();


           // // Before doing anything we rename root entities with names that already exist
           // // This is needed because we don't the existing entities are compatible and using them might leave the MSDB in an inrecoverable state
           // this.RenameRootEntities(ddbConnectionEntities, sid);
           // this.RenameRootEntities(datasets, sid);
           // this.RenameRootEntities(mappingSet, sid);

           // // first we must add the DDB connections and update the parent id of all datasets
           // this.PersistAndUpdateChildRefs(ddbConnectionEntities, datasets);
           // this.PersistAndUpdateChildRefs(datasets, datasetColumns, mappingSet);

           // var timeTranscodingEntities = transcoding.Where(entity => entity.HasTimeTrascoding()).SelectMany(entity => entity.TimeTranscoding).ToArray();
           // this.PersistAndUpdateChildRefs(datasetColumns, mappings, timeTranscodingEntities, rules, columnDescriptions);

           // this.PersistAndUpdateChildRefs(mappingSet, mappings);

           // this.PersistAndUpdateChildRefs(mappings, transcoding);

           // this.PersistAndUpdateChildRefs(transcoding, rules);

           // this._persistenceManager.AddEntities(rules.Where(entity => entity.ParentId != null));
        }

        private void PersistAndUpdateChildRefs(IList<TranscodingEntity> transcoding, IList<TranscodingRuleEntity> rules)
        {
            var rulesGroupByParent = rules.ToLookup(entity => entity.ParentId);
            ResetParentId(rules);
            this.Persist(
                transcoding.Where(entity => entity.ParentId != null),
                (x, y) =>
                    {
                        foreach (var child in rulesGroupByParent[x])
                        {
                            child.ParentId = y;
                        }
                    });
        }

        private void PersistAndUpdateChildRefs(IList<ComponentMappingEntity> mappings, IList<TranscodingEntity> transcoding)
        {
            var rulesGroupByParent = transcoding.ToLookup(entity => entity.ParentId);
            ResetParentId(transcoding);
            this.Persist(
                mappings.Where(entity => entity.ParentId != null),
                (x, y) =>
                    {
                        foreach (var child in rulesGroupByParent[x])
                        {
                            child.ParentId = y;
                        }
                    });
        }

        private void PersistAndUpdateChildRefs(IList<MappingSetEntity> mappingSet, IList<ComponentMappingEntity> mappings)
        {
            var rulesGroupByParent = mappings.ToLookup(entity => entity.ParentId);
            ResetParentId(mappings);
            this.Persist(
                mappingSet,
                (x, y) =>
                    {
                        foreach (var mapping in rulesGroupByParent[x])
                        {
                            mapping.ParentId = y;
                        }
                    });
        }

        private void PersistAndUpdateChildRefs(IList<DataSetColumnEntity> datasetColumns, IList<ComponentMappingEntity> mappings, TimeTranscodingEntity[] timeTranscodingEntities, IList<TranscodingRuleEntity> rules,IList<ColumnDescriptionSourceEntity> columnDescriptions)
        {
            var mappingColumnsLookup = mappings.Where(entity => entity.GetColumns() != null).SelectMany(entity => entity.GetColumns()).ToLookup(entity => entity.EntityId);
            var dateColumns = timeTranscodingEntities.Where(entity => entity.DateColumn != null).ToLookup(entity => entity.EntityId);
            var yearColumns = timeTranscodingEntities.Where(entity => entity.Year?.Column != null).ToLookup(entity => entity.EntityId);
            var periodColumns = timeTranscodingEntities.Where(entity => entity.Period?.Column != null).ToLookup(entity => entity.EntityId);
            var localCodes = rules.Where(entity => entity.LocalCodes != null).SelectMany(entity => entity.LocalCodes).ToLookup(entity => entity.ParentId);
            ResetParentId(localCodes.SelectMany(entities => entities));
            this.Persist(
                datasetColumns,
                (x, y) =>
                    {
                        foreach (var child in mappingColumnsLookup[x])
                        {
                            child.EntityId = y;
                        }
                        foreach (var child in dateColumns[x])
                        {
                            child.EntityId = y;
                        }

                        foreach (var child in yearColumns[x])
                        {
                            child.EntityId = y;
                        }

                        foreach (var child in periodColumns[x])
                        {
                            child.EntityId = y;
                        }

                        foreach (var localCode in localCodes[x])
                        {
                            localCode.ParentId = y;
                        }

                        //foreach (var columnDescription in columnDescriptions[x])
                        //{
                        //    columnDescription.ParentId = y;
                        //}
                    });
        }

        private void PersistAndUpdateChildRefs(IList<DatasetEntity> datasets, IList<DataSetColumnEntity> datasetColumns, IList<MappingSetEntity> mappingSet)
        {
            var rulesGroupByParent = datasetColumns.ToLookup(entity => entity.ParentId);
            var rulesGroupByDataSetId = mappingSet.ToLookup(entity => entity.DataSetId);
            this.Persist(
                datasets,
                (x, y) =>
                    {
                        foreach (var child in rulesGroupByParent[x])
                        {
                            child.ParentId = y;
                        }
                        foreach (var child in rulesGroupByDataSetId[x])
                        {
                            child.DataSetId = y;
                        }
                    });
        }

        private void PersistAndUpdateChildRefs(IList<IConnectionEntity> ddbConnectionEntities, IList<DatasetEntity> datasets)
        {
            var rulesGroupByParent = datasets.ToLookup(entity => entity.ParentId);
            this.Persist(
                ddbConnectionEntities,
                (x, y) =>
                    {
                        foreach (var datasetEntity in rulesGroupByParent[x])
                        {
                            datasetEntity.ParentId = y;
                        }
                    });
        }

        private void RenameRootEntities<TRootEntity>(IEnumerable<TRootEntity> rootEntities, string sid) where  TRootEntity : class, ISimpleNameableEntity
        {
            var type = this._entityTypeBuilder.Build<TRootEntity>();
            var simpleNameableEntities = this.GetDbEntities(sid, type);
            if (simpleNameableEntities == null)
            {
                return;
            }

            var set = new HashSet<string>(simpleNameableEntities.Select(entity => entity.Name), StringComparer.OrdinalIgnoreCase);
            int x = 0;
            foreach (var rootEntity in rootEntities)
            {
                var name = rootEntity.Name;
                while (set.Contains(name))
                {
                    name = EntityCloneHelper.GetNewName(rootEntity, x++, "Import of {0} ");
                }

                set.Add(name);
                rootEntity.Name = name;
            }
        }

        private void Persist<TEntity>(IEnumerable<TEntity> entities, Action<string, string> changeId) where TEntity : class, IEntity
        {
            foreach (var entity in entities)
            {
                var xmlEntityId = entity.EntityId;
                var add = this._persistenceManager.Add(entity);
                if (add != null)
                {
                    changeId(xmlEntityId, add.EntityId);
                } 
            }
        }

        private void ResetParentId<TEnity>(IEnumerable<TEnity> entities) where TEnity : class, IEntity
        {
            foreach (var entity in entities)
            {
                entity.ParentId = null;
            }
        }

        private List<ISimpleNameableEntity> GetDbEntities(string sid, EntityType key)
        {
            var entityQuery = new EntityQuery();
            switch (key)
            {
                case EntityType.MappingSet:
                    return this._retrieverManager.GetEntities<MappingSetEntity>(sid, entityQuery, Detail.IdAndName).Cast<ISimpleNameableEntity>().ToList();
                case EntityType.DdbConnectionSettings:
                    return this._retrieverManager.GetEntities<DdbConnectionEntity>(sid, entityQuery, Detail.IdAndName).Cast<ISimpleNameableEntity>().ToList();
                case EntityType.DataSet:
                    return this._retrieverManager.GetEntities<DatasetEntity>(sid, entityQuery, Detail.IdAndName).Cast<ISimpleNameableEntity>().ToList();

            }
            return null;
        }
    }
}