﻿// -----------------------------------------------------------------------
// <copyright file="CopyManager.cs" company="EUROSTAT">
//   Date Created : 2017-09-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Manager
{
    using System;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;

    /// <summary>
    /// The copy manager.
    /// </summary>
    public class CopyManager : ICopyManager
    {
        /// <summary>
        /// The factories
        /// </summary>
        private readonly ICopyFactory[] _factories;

        /// <summary>
        /// Initializes a new instance of the <see cref="CopyManager"/> class.
        /// </summary>
        /// <param name="factories">The factories.</param>
        /// <exception cref="System.ArgumentNullException">factories - Must provide at least one factory</exception>
        /// <exception cref="System.ArgumentException">Must provide at least one factory - factories</exception>
        public CopyManager(params ICopyFactory[] factories)
        {
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories), "Must provide at least one factory");
            }

            if (factories.Length == 0)
            {
                throw new ArgumentException("Must provide at least one factory", nameof(factories));
            }

            this._factories = factories.ToArray();
        }

        /// <summary>
        /// Copy everything (including SDMX) from the sourceStoreId to targetStoreId
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <returns>
        /// The result of the action
        /// </returns>
        /// <remarks>
        /// Cannot copy user if it exists
        /// Cannot copy header if the dataflow already has a header
        /// Cannot copy MappingSet if the dataflow already has a mapping set
        /// </remarks>
        public IActionResult Copy(string sourceStoreId, string targetStoreId)
        {
            using (var singleRequest = new SingleRequestScope())
            {
                singleRequest.RecordOnlyRoot = true;
                singleRequest.AuthorizeOnlyRootEntities(true);
                return this.GetEngine(sourceStoreId).Copy(targetStoreId);
            }
        }

        /// <summary>
        /// Copy the selected entities from the sourceStoreId to targetStoreId
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <param name="targetStoreId">The target store identifier.</param>
        /// <param name="entityType">Type of the entity. The entity can be one following entities
        /// <see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.DdbConnectionSettings" />, <see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.DataSet" /> and <see cref="T:Estat.Sri.Mapping.Api.Model.RegistryEntity" /><see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.MappingSet" />, <see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.User" /> and <see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.Header" /></param>
        /// <param name="entityQuery">The entity query.</param>
        /// <returns>
        /// The result of the action
        /// </returns>
        /// <remarks>
        /// Parents will be copied including SDMX dependencies for Mapping Sets. Some restrictions exist.
        /// Cannot copy user if it exists
        /// Cannot copy header if the dataflow already has a header
        /// Cannot copy MappingSet if the dataflow already has a mapping set
        /// </remarks>
        public IActionResult Copy(string sourceStoreId, string targetStoreId, EntityType entityType, IEntityQuery entityQuery)
        {
            using (var singleRequest = new SingleRequestScope())
            {
                singleRequest.RecordOnlyRoot = true;
                singleRequest.AuthorizeOnlyRootEntities(true);
                return this.GetEngine(sourceStoreId).Copy(targetStoreId, entityType, entityQuery);
            }
        }

        /// <summary>
        /// Copy the selected entities within the same store id
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="entityType">Type of the entity. The entity can be one following entities <see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.DdbConnectionSettings" />, <see cref="F:Estat.Sri.Mapping.Api.Constant.EntityType.DataSet" /> and <see cref="T:Estat.Sri.Mapping.Api.Model.RegistryEntity" /></param>
        /// <param name="entityQuery">The entity query.</param>
        /// <param name="copyParents">if set to <c>true</c> copy parents. Currently it only applies to <see cref="T:Estat.Sri.Mapping.Api.Model.DatasetEntity" /></param>
        /// <returns>
        /// The result of the action
        /// </returns>
        public IActionResult Copy(string storeId, EntityType entityType, IEntityQuery entityQuery, bool copyParents)
        {
            using (var singleRequest = new SingleRequestScope())
            {
                singleRequest.RecordOnlyRoot = true;
                singleRequest.AuthorizeOnlyRootEntities(true);
                return this.GetEngine(storeId).Copy(entityType, entityQuery, copyParents);
            }
        }

        /// <summary>
        /// Copies the header from the <paramref name="sourceDataflowUrn"/> to the <paramref name="targetDataflowUrn"/>.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="sourceDataflowUrn">The source dataflow urn.</param>
        /// <param name="targetDataflowUrn">The target dataflow urn.</param>
        /// <returns>The <see cref="IActionResult"/></returns>
        public IActionResult CopyHeader(string storeId, Uri sourceDataflowUrn, Uri targetDataflowUrn)
        {
            using (var singleRequest = new SingleRequestScope())
            {
                singleRequest.RecordOnlyRoot = true;
                singleRequest.AuthorizeOnlyRootEntities(true);
                return this.GetEngine(storeId).CopyHeader(sourceDataflowUrn, targetDataflowUrn);
            }
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="sourceStoreId">The source store identifier.</param>
        /// <returns>
        /// The <see cref="ICopyEngine" />.
        /// </returns>
        /// <exception cref="System.NotImplementedException">Could not find an engine</exception>
        private ICopyEngine GetEngine(string sourceStoreId)
        {
            foreach (var copyFactory in this._factories)
            {
                var copyEngine = copyFactory.GetEngine(sourceStoreId);
                if (copyEngine != null)
                {
                    return copyEngine;
                }
            }

            throw new NotImplementedException("Could not find an engine");
        }
    }
}