// -----------------------------------------------------------------------
// <copyright file="SQLScriptProviderManager.cs" company="EUROSTAT">
//   Date Created : 2017-03-24
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Mapping.MappingStore.Manager
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.Mapping.MappingStore.Engine;
    using Estat.Sri.Mapping.MappingStore.Factory;

    /// <summary>
    /// The SQL script provider manager.
    /// </summary>
    public class SqlScriptProviderManager : ISqlScriptProviderManager
    {
        /// <summary>
        /// The location manager
        /// </summary>
        private readonly IList<ISqlScriptProviderFactory> _providerFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlScriptProviderManager" /> class.
        /// </summary>
        /// <param name="providerFactory">The provider factory.</param>
        /// <exception cref="System.ArgumentNullException">providerFactory is null</exception>
        public SqlScriptProviderManager(params ISqlScriptProviderFactory[] providerFactory)
        {
            if (providerFactory == null)
            {
                throw new ArgumentNullException(nameof(providerFactory));
            }

            this._providerFactory = providerFactory;
        }

        /// <summary>
        /// Gets the full schema.
        /// </summary>
        /// <param name="databaseType">Type of the database.</param>
        /// <returns>
        /// The SQL Statements
        /// </returns>
        /// <exception cref="System.ArgumentNullException">databaseType is null</exception>
        public IEnumerable<string> GetFullSchema(string databaseType)
        {
            if (databaseType == null)
            {
                throw new ArgumentNullException(nameof(databaseType));
            }

            var scriptReader = this.GetEngine(databaseType).GetSqlStatements();
            return scriptReader;
        }

        /// <summary>
        /// Gets the upgrade.
        /// </summary>
        /// <param name="databaseType">Type of the database.</param>
        /// <param name="currentVersion">The current version.</param>
        /// <param name="targetVersion">The target version.</param>
        /// <returns>
        /// The SQL Statements
        /// </returns>
        public IEnumerable<string> GetUpgrade(string databaseType, Version currentVersion, Version targetVersion)
        {
            foreach (var availableVersion in this.GetEngine(databaseType).GetAvailableVersions(currentVersion, targetVersion))
            {
                var lines = this.GetEngine(databaseType).GetUpgradeSqlStatements(availableVersion);
                foreach (var p in lines)
                {
                    yield return p;
                }

                var commonLines = this.GetEngine(databaseType).GetUpgradeSqlStatementsCommon(availableVersion);
                foreach (var p in commonLines)
                {
                    yield return p;
                }
            }
        }

        /// <summary>
        /// Gets the available upgrade version.
        /// </summary>
        /// <param name="databaseType">Type of the database.</param>
        /// <param name="currentVersion">The current version.</param>
        /// <returns>The available versions for upgrading</returns>
        public IEnumerable<Version> GetAvailableUpgradeVersion(string databaseType, Version currentVersion)
        {
            return this.GetEngine(databaseType).GetAvailableVersions(currentVersion, new Version(int.MaxValue, int.MaxValue));
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="databaseType">Type of the database.</param>
        /// <returns>The <see cref="ISqlScriptProviderEngine"/></returns>
        /// <exception cref="System.NotImplementedException">Not implemented. Support for the specified <paramref name="databaseType"/></exception>
        private ISqlScriptProviderEngine GetEngine(string databaseType)
        {
            foreach (var providerFactory in this._providerFactory)
            {
                var engine = providerFactory.GetEngine(databaseType);
                if (engine != null)
                {
                    return engine;
                }
            }

            throw new NotImplementedException($"Support for DB {databaseType}");
        }
    }
}