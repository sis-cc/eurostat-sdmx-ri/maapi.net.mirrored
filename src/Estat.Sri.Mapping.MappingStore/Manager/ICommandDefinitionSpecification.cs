﻿// -----------------------------------------------------------------------
// <copyright file="ICommandDefinitionSpecification.cs" company="EUROSTAT">
//   Date Created : 2017-02-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Manager
{
    public interface ICommandDefinitionSpecification
    {
        /// <summary>
        /// Determines whether the specified entity type is satisfied.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="sqlStatementType">The json operation.</param>
        /// <returns>
        ///   <c>true</c> if the specified entity type is satisfied; otherwise, <c>false</c>.
        /// </returns>
        bool IsSatisfied(DatabaseInformationType entityType, SqlStatementType sqlStatementType);
    }
}