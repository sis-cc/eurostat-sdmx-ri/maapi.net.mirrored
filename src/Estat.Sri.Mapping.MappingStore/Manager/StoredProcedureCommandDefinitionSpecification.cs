using Estat.Sri.Mapping.MappingStore.Constant;
using Estat.Sri.Mapping.MappingStore.Model;

namespace Estat.Sri.Mapping.MappingStore.Manager
{
    /// <summary>
    /// StoredProcedureCommandDefinitionSpecification
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.MappingStore.Manager.ICommandDefinitionSpecification" />
    public class StoredProcedureCommandDefinitionSpecification : ICommandDefinitionSpecification
    {
        public bool IsSatisfied(DatabaseInformationType entityType, SqlStatementType sqlStatementType)
        {
            switch (sqlStatementType)
            {
                case SqlStatementType.Insert:
                    return entityType == DatabaseInformationType.LocalCode || entityType == DatabaseInformationType.MappingSet ||
                           entityType == DatabaseInformationType.Mapping || entityType == DatabaseInformationType.DataSet ||
                           entityType == DatabaseInformationType.DataSetColumn || entityType == DatabaseInformationType.DdbConnectionSettings ||
                           entityType == DatabaseInformationType.TranscodingScript || entityType == DatabaseInformationType.TranscodingRule ||
                           entityType == DatabaseInformationType.Transcoding || entityType == DatabaseInformationType.Contact ||
                           entityType == DatabaseInformationType.ContactDetails || entityType == DatabaseInformationType.HeaderParty || 
                           entityType == DatabaseInformationType.DescSource || entityType == DatabaseInformationType.Header || 
                           entityType == DatabaseInformationType.HeaderLocalisedString || entityType == DatabaseInformationType.HeaderLocalisedString || entityType == DatabaseInformationType.Registry ||
                           entityType == DatabaseInformationType.User || entityType == DatabaseInformationType.UserAction || entityType == DatabaseInformationType.TemplateMapping || entityType == DatabaseInformationType.Nsiws;
            }
            return false;
        }
    }
}