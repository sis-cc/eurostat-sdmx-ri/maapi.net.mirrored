// -----------------------------------------------------------------------
// <copyright file="IDatabaseMapperManager.cs" company="EUROSTAT">
//   Date Created : 2017-02-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Mapping.MappingStore.Manager
{
    /// <summary>
    /// IDatabaseMapperManager
    /// </summary>
    public interface IDatabaseMapperManager
    {
        /// <summary>
        /// Gets the column mapping.
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
        string GetColumnMapping(string columnName);
        /// <summary>
        /// Gets the primary key column.
        /// </summary>
        /// <returns></returns>
        string GetPrimaryKeyColumn();
        /// <summary>
        /// Gets the name of the table.
        /// </summary>
        /// <returns></returns>
        string GetTableName();

        /// <summary>
        /// Gets the name of the base table if any.
        /// </summary>
        /// <returns>The base table e.g. <c>ENTITY_BASE</c>; else null</returns>
        string GetBaseTableName();

        /// <summary>
        /// Gets the procedure parameter mapping.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        string GetProcedureParameterMapping(string key);
    }
}