// -----------------------------------------------------------------------
// <copyright file="DatabaseManager.cs" company="EUROSTAT">
//   Date Created : 2017-02-21
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;
using System.Linq;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.Mapping.MappingStore.Manager
{
    /// <summary>
    /// DatabaseManager
    /// </summary>
    public class DatabaseManager
    {
        private readonly IConfigurationStoreManager _configurationStoreManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseManager"/> class.
        /// </summary>
        /// <param name="configurationStoreManager">The configuration store manager.</param>
        public DatabaseManager(IConfigurationStoreManager configurationStoreManager)
        {
            this._configurationStoreManager = configurationStoreManager;
        }

        /// <summary>
        /// Gets the database.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <returns></returns>
        public Database GetDatabase(string storeId)
        {
            var connectionSettings = this.GetConnectionStringSettings(new DatabaseIdentificationOptions {StoreId = storeId } );
            if (string.IsNullOrWhiteSpace(connectionSettings?.ProviderName))
            {
                return null;
            }

            return DatabasePool.GetDatabase(connectionSettings);
        }

        public Database GetDatabase(DatabaseIdentificationOptions databaseIdentificationOptions)
        {
            ConnectionStringSettings connectionSettings = null;
            connectionSettings = this.GetConnectionStringSettings(databaseIdentificationOptions);
            

            if (string.IsNullOrWhiteSpace(connectionSettings?.ProviderName))
            {
                return null;
            }

            return DatabasePool.GetDatabase(connectionSettings);
        }
        /// <summary>
        /// Gets the connection string settings.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>The <see cref="ConnectionStringSettings"/></returns>
        public ConnectionStringSettings GetConnectionStringSettings(DatabaseIdentificationOptions databaseIdentificationOptions)
        {
            ConnectionStringSettings connectionSettings = null;
            if (string.IsNullOrWhiteSpace(databaseIdentificationOptions.StoreId))
            {
                connectionSettings = new ConnectionStringSettings
                {
                    ConnectionString = databaseIdentificationOptions.ConnectionString,
                    ProviderName = databaseIdentificationOptions.ProviderName
                };
            }
            else
            {
                connectionSettings = this._configurationStoreManager.GetSettings<ConnectionStringSettings>().FirstOrDefault(settings => settings.Name.Equals(databaseIdentificationOptions.StoreId));
            }
            return connectionSettings;
        }
    }
}