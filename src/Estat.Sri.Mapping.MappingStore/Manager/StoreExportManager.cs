// -----------------------------------------------------------------------
// <copyright file="StoreExportManager.cs" company="EUROSTAT">
//   Date Created : 2017-08-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.MappingStore.Engine;

namespace Estat.Sri.Mapping.MappingStore.Manager
{
    using System.IO.Compression;
    using System.Security.Cryptography;
    using DryIoc.ImTools;
    using Estat.Sri.Mapping.Api.Engine.Streaming;
    using Estat.Sri.Mapping.MappingStore.Builder;
    using Estat.Sri.Mapping.MappingStore.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Util;

    public class StoreExportManager : IStoreExporttManager
    {
        private readonly IEntityRetrieverManager _retrieverManager;

        private readonly DatabaseManager _databaseManager;

        private readonly IStructureWriterManager _structureWriterManager;
        private readonly ICommonSdmxObjectRetrievalFactory commonSdmxObjectRetrievalFactory;
        private readonly StructureOutputFormat _structurev21Format;
        private readonly StructureOutputFormat _structurev30Format;
        private readonly StructureOutputFormat _structurev20Format;
        private readonly ExportImportPathBuilder _exportImportPathBuilder = new ExportImportPathBuilder();

        public StoreExportManager(IEntityRetrieverManager retrieverManager, DatabaseManager databaseManager, IStructureWriterManager structureWriterManager, ICommonSdmxObjectRetrievalFactory commonSdmxObjectRetrievalFactory)
        {
            this._retrieverManager = retrieverManager;
            this._databaseManager = databaseManager;
            this._structureWriterManager = structureWriterManager;
            this.commonSdmxObjectRetrievalFactory = commonSdmxObjectRetrievalFactory;
            _structurev21Format = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);
            _structurev20Format = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV2StructureDocument);
            _structurev30Format = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV3StructureDocument);
        }

        public void Export(Stream stream, Encoding encoding, string sid)
        {
            using (var writer = new EntityStreamingWriter(stream))
            {
                writer.WriteRoot("mappingStore");
                writer.WriteForExport(this.GetEntities<IConnectionEntity>(sid));
                writer.WriteForExport(this.GetEntities<DatasetEntity>(sid));
                writer.WriteForExport(this.GetEntities<DataSetColumnEntity>(sid));
                writer.WriteForExport(this.GetEntities<MappingSetEntity>(sid));
                writer.WriteForExport(this.GetEntities<ComponentMappingEntity>(sid));
                writer.WriteForExport(this.GetEntities<HeaderEntity>(sid));
                writer.WriteForExport(this.GetEntities<TimeDimensionMappingEntity>(sid));
                writer.WriteForExport(this.GetEntities<TranscodingRuleEntity>(sid));
                writer.WriteForExport(this.GetEntities<ColumnDescriptionSourceEntity>(sid));
                writer.WriteForExport(this.GetEntities<RegistryEntity>(sid));
                writer.WriteForExport(this.GetEntities<TemplateMapping>(sid));
                writer.WriteForExport(this.GetEntities<DataSourceEntity>(sid));
                writer.WriteForExport(this.GetEntities<NsiwsEntity>(sid));
               // writer.WriteForExport(this.GetEntities<TranscodingEntity>(sid));
                writer.WriteForExport(this.GetEntities<LastUpdatedEntity>(sid));
                writer.WriteForExport(this.GetEntities<ValidToMappingEntity>(sid));
                writer.WriteForExport(this.GetEntities<UpdateStatusEntity>(sid));
                writer.WriteForExport(this.GetEntities<UserEntity>(sid));


            }
        }

        /// <summary>
        /// Exports all entities in the Mapping Store and SDMX to the specified stream.
        /// </summary>
        /// <param name="stream">The stream to write the ZIP archive.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="sid">The sid.</param>
        public void ExportWithSdmx(Stream stream, Encoding encoding, string sid)
        {
            using (ZipArchive archive = new ZipArchive(stream, ZipArchiveMode.Create, true, encoding))
            {
                var exportPath = _exportImportPathBuilder.Build(archive);
                this.WriteSdmx(encoding, sid, exportPath);
                using (var outputStream = exportPath.Mapping.Value.Open())
                {
                    Export(outputStream, encoding, sid);
                }
            }
        }

        private void WriteSdmx(Encoding encoding, string sid, ExportImportEntryPaths exportPath)
        {
            Database database = this._databaseManager.GetDatabase(sid);
            ISdmxObjects v21 = new SdmxObjectsImpl();
            ISdmxObjects v20 = new SdmxObjectsImpl();                      
            ISdmxObjects v30 = new SdmxObjectsImpl();
            var manager = commonSdmxObjectRetrievalFactory.GetManager(database.ConnectionStringSettings);
            var requestedTypes = new[] { SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation),
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme)};
            IDataStructureObject[] crossDsd = null;
            IDataStructureObject[] Dsd30 = null;          
            foreach (var type in requestedTypes )
            {
                ICommonStructureQuery commonQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, null)
                .SetMaintainableTarget(type)
                .Build();
                var structuresFromDatabase = manager.GetMaintainables(commonQuery);                    
                if(type == SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd))
                {
                    var dataStructureObjects = structuresFromDatabase.DataStructures;                
                    crossDsd = dataStructureObjects.Where(o => o is ICrossSectionalDataStructureObject).ToArray();
                    Dsd30 = dataStructureObjects.Where(x => !x.IsCompatible(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne))).ToArray();                  
                    dataStructureObjects.ExceptWith(crossDsd);
                    dataStructureObjects.ExceptWith(Dsd30);          
                    v21.AddIdentifiables(dataStructureObjects);
                }
                else if (type == SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow))
                {
                    var dataflows = structuresFromDatabase.Dataflows;
                    foreach (var dataflow in dataflows)
                    {
                        var version = dataflow.DataStructureRef.Version;
                        if (VersionableUtil.IsSdmxV30Valid(version) && version.Contains("+"))
                        {
                            v30.AddDataflow(dataflow);
                            //v21.RemoveDataflow(dataflow);
                        }
                        else
                        {
                            v21.AddIdentifiable(dataflow);
                        }                                            
                    }
                }
                else 
                { 
                    v21.Merge(structuresFromDatabase);
                }
            }


            // TODO add other arterfacts that are supported by Mapping Store
            using (var outputStream = exportPath.SdmxV21.Value.Open())
            {
                this._structureWriterManager.WriteStructures(v21, new SdmxStructureFormat(this._structurev21Format, encoding), outputStream);
                outputStream.Flush();
            }
          
            using (var outputStream = exportPath.SdmxV30.Value.Open())
            {
                v30.AddIdentifiables(Dsd30);
                this._structureWriterManager.WriteStructures(v30, new SdmxStructureFormat(this._structurev30Format, encoding), outputStream);
                outputStream.Flush();
            }

            if (crossDsd.Length > 0)
            {
                v20.AddIdentifiables(crossDsd);
                var zipArchiveEntry = exportPath.SdmxV20;
                using (var outputStream = zipArchiveEntry.Value.Open())
                {
                    this._structureWriterManager.WriteStructures(v20, new SdmxStructureFormat(this._structurev20Format, encoding), outputStream);
                    outputStream.Flush();
                }
                
            }
        }

        public List<IEntity> GetEntities<TEntity>(string storeId) where TEntity: class, IEntity
        {
            return this._retrieverManager.GetEntities<TEntity>(storeId, new EntityQuery(), Detail.Full).Cast<IEntity>().ToList();
        }
    }
}