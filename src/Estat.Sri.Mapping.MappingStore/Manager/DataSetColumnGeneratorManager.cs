﻿// -----------------------------------------------------------------------
// <copyright file="DataSetColumnGeneratorManager.cs" company="EUROSTAT">
//   Date Created : 2017-09-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Extension;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.MappingStore.Builder;
using Org.Sdmxsource.Sdmx.Api.Builder;
using Estat.Sri.Mapping.Api.Exceptions;

namespace Estat.Sri.Mapping.MappingStore
{
    public class DataSetColumnGeneratorManager : IDataSetColumnGeneratorManager
    {
        private readonly IEntityRetrieverManager _entityRetrieverManager;

        private readonly IBuilder<Database, DdbConnectionEntity> _databaseBuilder;

        private readonly ILocalCodeRetrieverManager _localCodeRetrieverManager;

        public DataSetColumnGeneratorManager(IEntityRetrieverManager entityRetrieverManager, IBuilder<Database, DdbConnectionEntity> databaseBuilder, ILocalCodeRetrieverManager localCodeRetrieverManager)    
        {
            _entityRetrieverManager = entityRetrieverManager;
            _databaseBuilder = databaseBuilder;
            _localCodeRetrieverManager = localCodeRetrieverManager;
        }


        public IList<DataSetColumnEntity> GetDatasetColumns(LocalDataQuery localDataQuery)
        {
            var retVal = new List<DataSetColumnEntity>();
            var DdbConnection = this._entityRetrieverManager.GetRetrieverEngine<DdbConnectionEntity>(localDataQuery.MappingStoreId);

            var conn = DdbConnection.GetEntities(EntityQueryExtension.QueryForThisEntityId(localDataQuery.DisseminationDatabaseSettingEntityId),
                    Detail.Full).FirstOrDefault();

            DbReaderDatasetColumnBuilder dbReaderDatasetColumnBuilder = new DbReaderDatasetColumnBuilder(_databaseBuilder.Build(conn));

            foreach (var columnName in dbReaderDatasetColumnBuilder.Build(localDataQuery.Query))
            {
                retVal.Add(new DataSetColumnEntity{ Name = columnName});
            }

            return retVal;
        }

        public IList<DataSetColumnEntity> GetDatasetColumns(string storeId, string datasetEntityId)
        {
            var entityRetrieverEngine = _entityRetrieverManager.GetRetrieverEngine<DatasetEntity>(storeId);
            var datasetEntity =
                entityRetrieverEngine.GetEntities(
                    new EntityQuery { EntityId = new Criteria<string>(OperatorType.Exact, datasetEntityId) },
                    Detail.Full).FirstOrDefault();
            if (datasetEntity == null)
            {
                throw new ResourceNotFoundException("Could not locate requested Dataset");
            }

            LocalDataQuery localDataQuery = new LocalDataQuery
            {
                Query = datasetEntity.Query,
                MappingStoreId = storeId,
                DisseminationDatabaseSettingEntityId = datasetEntity.ParentId
            };

            return GetDatasetColumns(localDataQuery);
        }
    }
}
