// -----------------------------------------------------------------------
// <copyright file="SpecialMutableObjectRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-04-15
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;

    using DryIoc;

    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Extensions;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     The special mutable object retrieval manager.
    /// </summary>
    public class SpecialMutableObjectRetrievalManager : ISpecialMutableObjectRetrievalManager
    {
        /// <summary>
        ///     The retrieval engine container.
        /// </summary>
        private readonly IRetrievalEngineContainer _retrievalEngineContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecialMutableObjectRetrievalManager"/> class.
        /// </summary>
        /// <param name="retrievalEngineContainer">The retrieval engine container.</param>
        public SpecialMutableObjectRetrievalManager(IRetrievalEngineContainer retrievalEngineContainer)
        {
            if (retrievalEngineContainer == null)
            {
                throw new ArgumentNullException("retrievalEngineContainer");
            }

            this._retrievalEngineContainer = retrievalEngineContainer;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SpecialMutableObjectRetrievalManager" /> class.
        /// </summary>
        /// <param name="connectionStringSettings">
        ///     The connection string settings.
        /// </param>
        public SpecialMutableObjectRetrievalManager(ConnectionStringSettings connectionStringSettings)
        {
            var database = new Database(connectionStringSettings);
            this._retrievalEngineContainer = MappingStoreIoc.Container.Resolve<IRetrievalEngineContainerFactory>(serviceKey: MappingStoreIoc.ServiceKey).GetRetrievalEngineContainer(database);
        }

        /// <summary>
        ///     Get a list of CodeList matching the given id, version and agencyID and dataflow/component information.
        /// </summary>
        /// <param name="codelistReference">
        ///     The codelist reference
        /// </param>
        /// <param name="dataflowReference">
        ///     The dataflow reference
        /// </param>
        /// <param name="componentConceptRef">
        ///     The component in the dataflow that this codelist is used and for which partial codes are requests.
        /// </param>
        /// <param name="isTranscoded">
        ///     if true will get the codes from the transcoding rules. Otherwise from locally stored codes.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed dataflows.
        /// </param>
        /// <returns>
        ///     The list of matched CodeListBean objects
        /// </returns>
        /// <remarks>
        ///     I.e. codes used for the given dataflow.
        ///     It can look codes actually used from transcoding rule or from locally stored codes.
        ///     If any of the Codelist identification parameters is null or empty it will act as wildcard.
        /// </remarks>
        [Obsolete]
        public ISet<ICodelistMutableObject> GetMutableCodelistObjects(
            IMaintainableRefObject codelistReference, 
            IMaintainableRefObject dataflowReference, 
            string componentConceptRef, 
            bool isTranscoded, 
            IList<IMaintainableRefObject> allowedDataflows)
        {
            if (SecurityHelper.Contains(allowedDataflows, dataflowReference))
            {
                throw new NotImplementedException("SpecialMutableObjectRetrievalManager.GetMutableCodelistObjects method needs to be rewritten.");
                //return this._retrievalEngineContainer.PartialCodeListRetrievalEngine.Retrieve(
                //    codelistReference, 
                //    ComplexStructureQueryDetailEnumType.Full, 
                //    dataflowReference, 
                //    componentConceptRef, 
                //    isTranscoded, 
                //    allowedDataflows);
                // TODO-1920: implement this ?
            }

            return new HashSet<ICodelistMutableObject>();
        }

        /// <summary>
        /// Get a Content Constraint matching the given id, version and agencyID and dataflow/component information.
        /// </summary>
        /// <param name="dataflowReference">The dataflow reference</param>
        /// <param name="componentConceptRef">The component in the dataflow that this codelist is used and for which partial codes are requests.</param>
        /// <returns>The list of matched CodeListBean objects</returns>
        /// <remarks>I.e. codes used for the given dataflow.
        /// It can look codes actually used from transcoding rule or from locally stored codes.
        /// If any of the Codelist identification parameters is null or empty it will act as wildcard.</remarks>
        [Obsolete]
        public IContentConstraintMutableObject GetContentConstraintUncoded(
            IMaintainableRefObject dataflowReference,
            string componentConceptRef)
        {
            throw new NotImplementedException("SpecialMutableObjectRetrievalManager.GetContentConstraintUncoded method needs to be rewritten.");
            //return this._retrievalEngineContainer.PartialCodeListRetrievalEngine.Retrieve(
            //    dataflowReference,
            //    componentConceptRef,
            //    TranscodingType.UnCoded);
            // TODO-1920: implement this ?
        }

        /// <summary>
        /// Get a Content Constraint matching the given id, version and agencyID and dataflow/component information.
        /// </summary>
        /// <param name="dataflowReference">The dataflow reference</param>
        /// <param name="componentConceptRef">The component in the dataflow that this codelist is used and for which partial codes are requests.</param>
        /// <returns>The list of matched CodeListBean objects</returns>
        /// <remarks>I.e. codes used for the given dataflow.
        /// It can look codes actually used from transcoding rule or from locally stored codes.
        /// If any of the Codelist identification parameters is null or empty it will act as wildcard.</remarks>
        [Obsolete]
        public IContentConstraintMutableObject GetContentConstraintTranscoded(
            IMaintainableRefObject dataflowReference,
            string componentConceptRef)
        {
            throw new NotImplementedException("SpecialMutableObjectRetrievalManager.GetContentConstraintTranscoded method needs to be rewritten.");
            //return this._retrievalEngineContainer.PartialCodeListRetrievalEngine.Retrieve(
            //    dataflowReference,
            //    componentConceptRef,
            //    TranscodingType.Coded);
            // TODO-1920: implement this ?
        }

        /// <summary>
        /// Get a Content Constraint matching the given id, version and agencyID and dataflow/component information.
        /// </summary>
        /// <param name="dataflowReference">The dataflow reference</param>
        /// <param name="componentConceptRef">The component in the dataflow that this codelist is used and for which partial codes are requests.</param>
        /// <returns>The list of matched CodeListBean objects</returns>
        /// <remarks>I.e. codes used for the given dataflow.
        /// It can look codes actually used from transcoding rule or from locally stored codes.
        /// If any of the Codelist identification parameters is null or empty it will act as wildcard.</remarks>
        [Obsolete]
        public IContentConstraintMutableObject GetContentConstraintNoTranscoding(
            IMaintainableRefObject dataflowReference,
            string componentConceptRef)
        {
            throw new NotImplementedException("SpecialMutableObjectRetrievalManager.GetContentConstraintNoTranscoding method needs to be rewritten.");
            //return this._retrievalEngineContainer.PartialCodeListRetrievalEngine.Retrieve(
            //    dataflowReference,
            //    componentConceptRef,
            //    TranscodingType.None);
            // TODO-1920: implement this ?
        }

        /// <summary>
        ///     Get a list of CodeList matching the given id, version and agencyID. If any of the parameter is null or empty it
        ///     will act as wildcard
        /// </summary>
        /// <param name="codelistReference">
        ///     The codelist reference
        /// </param>
        /// <param name="subset">
        ///     The list of items to retrieve
        /// </param>
        /// <returns>
        ///     The list of matched CodeListBean objects
        /// </returns>
        public ISet<ICodelistMutableObject> GetMutableCodelistObjects(
        IMaintainableRefObject codelistReference,
        IList<string> subset)
        {
            return this._retrievalEngineContainer.SubsetCodeListRetrievalEngine.Retrieve(
                codelistReference, 
                ComplexStructureQueryDetailEnumType.Full, 
                subset);
        }

        /// <summary>
        ///     Gets CodelistObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="codelistReference">
        ///     The codelist reference
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public ISet<ICodelistMutableObject> GetMutableCodelistObjects(IMaintainableRefObject codelistReference)
        {
            //return this._retrievalEngineContainer.SubsetCodeListRetrievalEngine.Retrieve(
            //     codelistReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.CodeList,false), 
            //    ComplexStructureQueryDetailEnumType.Full);
            throw new NotImplementedException();
            //TODO SDMXRI-1839: implement this ?
        }
    }
}