// -----------------------------------------------------------------------
// <copyright file="RetrievalManagerBase.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Manager
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    ///     The retrieval manager base class.
    /// </summary>
    [Obsolete("MSDB 7.0: After migrating to msdb 7")]
    public abstract class RetrievalManagerBase : ISdmxMutableObjectRetrievalManager
    {
        /// <summary>
        ///     Gets a single Agency Scheme, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IAgencySchemeMutableObject" /> .
        /// </returns>
        public abstract IAgencySchemeMutableObject GetMutableAgencyScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets AgencySchemeMutableObject that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IAgencySchemeMutableObject> GetMutableAgencySchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a single Categorisation, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme.ICategorisationMutableObject" /> .
        /// </returns>
        public abstract ICategorisationMutableObject GetMutableCategorisation(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets CategorisationObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<ICategorisationMutableObject> GetMutableCategorisationObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a single CategoryScheme , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme.ICategorySchemeMutableObject" /> .
        /// </returns>
        public abstract ICategorySchemeMutableObject GetMutableCategoryScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets CategorySchemeObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CategorySchemeObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<ICategorySchemeMutableObject> GetMutableCategorySchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a single CodeList , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist.ICodelistMutableObject" /> .
        /// </returns>
        public abstract ICodelistMutableObject GetMutableCodelist(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets CodelistObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<ICodelistMutableObject> GetMutableCodelistObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a single ConceptScheme , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme.IConceptSchemeMutableObject" /> .
        /// </returns>
        public abstract IConceptSchemeMutableObject GetMutableConceptScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets ConceptSchemeObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all ConceptSchemeObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IConceptSchemeMutableObject> GetMutableConceptSchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Returns a single Content Constraint, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The Content constraint.
        /// </returns>
        public abstract IContentConstraintMutableObject GetMutableContentConstraint(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Returns ContentConstraintBeans that match the parameters in the ref bean.  If the ref bean is null or
        ///     has no attributes set, then this will be interpreted as a search for all ContentConstraintObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     the reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of objects that match the search criteria
        /// </returns>
        public abstract ISet<IContentConstraintMutableObject> GetMutableContentConstraintObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a single data consumer scheme, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IDataConsumerSchemeMutableObject" /> .
        /// </returns>
        public abstract IDataConsumerSchemeMutableObject GetMutableDataConsumerScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets DataConsumerSchemeMutableObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IDataConsumerSchemeMutableObject> GetMutableDataConsumerSchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a single Dataflow , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure.IDataflowMutableObject" /> .
        /// </returns>
        public abstract IDataflowMutableObject GetMutableDataflow(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets DataflowObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all DataflowObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IDataflowMutableObject> GetMutableDataflowObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets DataflowObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all DataflowObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="metadata">The metadata</param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IDataflowMutableObject> GetMutableDataflowObjects(
            IMaintainableRefObject maintainableReference,
            IObjectQueryMetadata metadata);

        /// <summary>
        ///     Gets a single Data Provider Scheme, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IDataProviderSchemeMutableObject" /> .
        /// </returns>
        public abstract IDataProviderSchemeMutableObject GetMutableDataProviderScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets DataProviderSchemeMutableObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IDataProviderSchemeMutableObject> GetMutableDataProviderSchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a single DataStructure.
        ///     This expects the ref object either to contain a URN or all the attributes required to uniquely identify the object.
        ///     If version information is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure.IDataStructureMutableObject" /> .
        /// </returns>
        public abstract IDataStructureMutableObject GetMutableDataStructure(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets DataStructureObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all dataStructureObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IDataStructureMutableObject> GetMutableDataStructureObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a single HierarchicCodeList , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist.IHierarchicalCodelistMutableObject" /> .
        /// </returns>
        public abstract IHierarchicalCodelistMutableObject GetMutableHierarchicCodeList(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets HierarchicalCodelistObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all HierarchicalCodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IHierarchicalCodelistMutableObject> GetMutableHierarchicCodeListObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a set of maintainable objects which includes the maintainable being queried for, defined by the
        ///     StructureQueryObject parameter.
        ///     <p />
        ///     Expects only ONE maintainable to be returned from this query
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="IMaintainableMutableObject" /> .
        /// </returns>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxNotImplementedException">Unsupported artefact</exception>
        public IMaintainableMutableObject GetMutableMaintainable(
            IStructureReference query, 
            bool returnLatest, 
            bool returnStub)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            IMaintainableRefObject maintainableReference = query.MaintainableReference;
            switch (query.MaintainableStructureEnumType.EnumType)
            {
                case SdmxStructureEnumType.AgencyScheme:
                    return this.GetMutableAgencyScheme(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.DataConsumerScheme:
                    return this.GetMutableDataConsumerScheme(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.DataProviderScheme:
                    return this.GetMutableDataProviderScheme(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.Categorisation:
                    return this.GetMutableCategorisation(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.CategoryScheme:
                    return this.GetMutableCategoryScheme(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.CodeList:
                    return this.GetMutableCodelist(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.ConceptScheme:
                    return this.GetMutableConceptScheme(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.Dataflow:
                    return this.GetMutableDataflow(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.HierarchicalCodelist:
                    return this.GetMutableHierarchicCodeList(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.Dsd:
                    return this.GetMutableDataStructure(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.MetadataFlow:
                    return this.GetMutableMetadataflow(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.Msd:
                    return this.GetMutableMetadataStructure(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.OrganisationUnitScheme:
                    return this.GetMutableOrganisationUnitScheme(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.Process:
                    return this.GetMutableProcessObject(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.ReportingTaxonomy:
                    return this.GetMutableReportingTaxonomy(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.StructureSet:
                    return this.GetMutableStructureSet(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.ProvisionAgreement:
                    return this.GetMutableProvisionAgreement(maintainableReference, returnLatest, returnStub);
                case SdmxStructureEnumType.ContentConstraint:
                    return this.GetMutableContentConstraint(maintainableReference, returnLatest, returnStub);
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, query.TargetReference);
            }
        }

        /// <summary>
        ///     Gets a set of maintainable objects which includes the maintainable being queried for, defined by the
        ///     StructureQueryObject parameter.
        /// </summary>
        /// <param name="query">
        ///     The query.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     The <see cref="ISet{IMaintainableMutableObject}" /> .
        /// </returns>
        public ISet<IMaintainableMutableObject> GetMutableMaintainables(
            IStructureReference query, 
            bool returnLatest, 
            bool returnStub)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            IMaintainableRefObject maintainableReference = query.MaintainableReference;
            switch (query.MaintainableStructureEnumType.EnumType)
            {
                case SdmxStructureEnumType.AgencyScheme:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableAgencySchemeObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.DataConsumerScheme:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableDataConsumerSchemeObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.DataProviderScheme:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableDataProviderSchemeObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.Categorisation:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableCategorisationObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.CategoryScheme:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableCategorySchemeObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.CodeList:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableCodelistObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.ConceptScheme:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableConceptSchemeObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.Dataflow:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableDataflowObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.HierarchicalCodelist:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableHierarchicCodeListObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.Dsd:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableDataStructureObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.MetadataFlow:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableMetadataflowObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.Msd:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableMetadataStructureObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.OrganisationUnitScheme:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableOrganisationUnitSchemeObjects(
                                maintainableReference, 
                                returnLatest, 
                                returnStub));
                case SdmxStructureEnumType.Process:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableProcessObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.ReportingTaxonomy:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableReportingTaxonomyObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.StructureSet:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableStructureSetObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.ProvisionAgreement:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableProvisionAgreementObjects(maintainableReference, returnLatest, returnStub));
                case SdmxStructureEnumType.ContentConstraint:
                    return
                        new HashSet<IMaintainableMutableObject>(
                            this.GetMutableContentConstraintObjects(maintainableReference, returnLatest, returnStub));
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, query.TargetReference);
            }
        }

        /// <summary>
        ///     Gets a single Metadataflow , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure.IMetadataFlowMutableObject" /> .
        /// </returns>
        public abstract IMetadataFlowMutableObject GetMutableMetadataflow(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets MetadataFlowObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all MetadataFlowObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IMetadataFlowMutableObject> GetMutableMetadataflowObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a single MetadataStructure , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The
        ///     <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure.IMetadataStructureDefinitionMutableObject" />
        ///     .
        /// </returns>
        public abstract IMetadataStructureDefinitionMutableObject GetMutableMetadataStructure(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets MetadataStructureObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all MetadataStructureObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IMetadataStructureDefinitionMutableObject> GetMutableMetadataStructureObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a single organization scheme, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IOrganisationUnitSchemeMutableObject" /> .
        /// </returns>
        public abstract IOrganisationUnitSchemeMutableObject GetMutableOrganisationUnitScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets OrganisationUnitSchemeMutableObject that match the parameters in the ref @object.  If the ref @object is null
        ///     or
        ///     has no attributes set, then this will be interpreted as a search for all OrganisationUnitSchemeMutableObject
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IOrganisationUnitSchemeMutableObject> GetMutableOrganisationUnitSchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a process @object, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process.IProcessMutableObject" /> .
        /// </returns>
        public abstract IProcessMutableObject GetMutableProcessObject(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets ProcessObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all IProcessObject
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IProcessMutableObject> GetMutableProcessObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Returns a provision agreement bean, this expects the ref object to contain
        ///     all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract IProvisionAgreementMutableObject GetMutableProvisionAgreement(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Returns ProvisionAgreement beans that match the parameters in the ref bean. If the ref bean is null or
        ///     has no attributes set, then this will be interpreted as a search for all ProvisionAgreement beans.
        /// </summary>
        /// <param name="maintainableReference">
        ///     the reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of objects that match the search criteria
        /// </returns>
        public abstract ISet<IProvisionAgreementMutableObject> GetMutableProvisionAgreementObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a reporting taxonomy @object, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme.IReportingTaxonomyMutableObject" /> .
        /// </returns>
        public abstract IReportingTaxonomyMutableObject GetMutableReportingTaxonomy(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets ReportingTaxonomyObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all ReportingTaxonomyObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IReportingTaxonomyMutableObject> GetMutableReportingTaxonomyObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets a structure set @object, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping.IStructureSetMutableObject" /> .
        /// </returns>
        public abstract IStructureSetMutableObject GetMutableStructureSet(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);

        /// <summary>
        ///     Gets StructureSetObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all StructureSetObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public abstract ISet<IStructureSetMutableObject> GetMutableStructureSetObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub);
    }
}