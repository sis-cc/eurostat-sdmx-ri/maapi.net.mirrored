// -----------------------------------------------------------------------
// <copyright file="CachedRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.MappingStoreRetrieval.Builder;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Collections;

    /// <summary>
    ///     The cached retrieval manager.
    /// </summary>
    [Obsolete("Since MSDB 7.0")]
    public class CachedRetrievalManager : RetrievalManagerBase
    {
        ///// <summary>
        /////     The builder that builds a <see cref="IStructureReference" /> from a <see cref="IMaintainableMutableObject" />
        ///// </summary>
        //private static readonly StructureReferenceFromMutableBuilder _fromMutable =
        //    new StructureReferenceFromMutableBuilder();

        ///// <summary>
        /////     The _log.
        ///// </summary>
        //private static readonly ILog _log = LogManager.GetLogger(typeof(CachedRetrievalManager));

        ///// <summary>
        /////     The _request to artefacts.
        ///// </summary>
        //private readonly IDictionaryOfSets<IStructureReference, IMaintainableMutableObject> _requestToArtefacts;

        ///// <summary>
        /////     The _maintainable mutable objects used as cache.
        ///// </summary>
        //private readonly Dictionary<IStructureReference, IMaintainableMutableObject> _requestToLatestArtefacts;

        ///// <summary>
        /////     The retrieval manager.
        ///// </summary>
        //private readonly ISdmxMutableObjectRetrievalManager _retrievalManager;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CachedRetrievalManager" /> class.
        /// </summary>
        /// <param name="maintainableMutableObjects">
        ///     The mutable objects.
        /// </param>
        /// <param name="retrievalManager">
        ///     The retrieval manager.
        /// </param>
        public CachedRetrievalManager(
            IEnumerable<IMaintainableMutableObject> maintainableMutableObjects, 
            ISdmxMutableObjectRetrievalManager retrievalManager)
        {
            //this._retrievalManager = retrievalManager;
            //this._requestToLatestArtefacts = new Dictionary<IStructureReference, IMaintainableMutableObject>();
            //this._requestToArtefacts = new DictionaryOfSets<IStructureReference, IMaintainableMutableObject>();

            //if (maintainableMutableObjects != null)
            //{
            //    foreach (IMaintainableMutableObject mutableObject in maintainableMutableObjects)
            //    {
            //        this._requestToArtefacts.Add(
            //            _fromMutable.Build(mutableObject), 
            //            new HashSet<IMaintainableMutableObject> { mutableObject });
            //    }
            //}
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single Agency Scheme, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IAgencySchemeMutableObject" /> .
        /// </returns>
        public override IAgencySchemeMutableObject GetMutableAgencyScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.AgencyScheme, 
            //    this._retrievalManager.GetMutableAgencyScheme);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete (used into the RetrievalManagerBase)");
        }

        /// <summary>
        ///     Gets AgencySchemeMutableObject that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IAgencySchemeMutableObject> GetMutableAgencySchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.AgencyScheme, 
            //    this._retrievalManager.GetMutableAgencySchemeObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single Categorisation, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme.ICategorisationMutableObject" /> .
        /// </returns>
        public override ICategorisationMutableObject GetMutableCategorisation(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.Categorisation, 
            //    this._retrievalManager.GetMutableCategorisation);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets CategorisationObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<ICategorisationMutableObject> GetMutableCategorisationObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.Categorisation, 
            //    this._retrievalManager.GetMutableCategorisationObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single CategoryScheme , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme.ICategorySchemeMutableObject" /> .
        /// </returns>
        public override ICategorySchemeMutableObject GetMutableCategoryScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.CategoryScheme, 
            //    this._retrievalManager.GetMutableCategoryScheme);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets CategorySchemeObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CategorySchemeObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<ICategorySchemeMutableObject> GetMutableCategorySchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.CategoryScheme, 
            //    this._retrievalManager.GetMutableCategorySchemeObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single CodeList , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist.ICodelistMutableObject" /> .
        /// </returns>
        public override ICodelistMutableObject GetMutableCodelist(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.CodeList, 
            //    this._retrievalManager.GetMutableCodelist);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets CodelistObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<ICodelistMutableObject> GetMutableCodelistObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.CodeList, 
            //    this._retrievalManager.GetMutableCodelistObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single ConceptScheme , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme.IConceptSchemeMutableObject" /> .
        /// </returns>
        public override IConceptSchemeMutableObject GetMutableConceptScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.ConceptScheme, 
            //    this._retrievalManager.GetMutableConceptScheme);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets ConceptSchemeObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all ConceptSchemeObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IConceptSchemeMutableObject> GetMutableConceptSchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.ConceptScheme, 
            //    this._retrievalManager.GetMutableConceptSchemeObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Returns a single Content Constraint, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The Content constraint.
        /// </returns>
        public override IContentConstraintMutableObject GetMutableContentConstraint(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.ContentConstraint, 
            //    this._retrievalManager.GetMutableContentConstraint);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Returns ContentConstraintBeans that match the parameters in the ref bean.  If the ref bean is null or
        ///     has no attributes set, then this will be interpreted as a search for all ContentConstraintObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     the reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of objects that match the search criteria
        /// </returns>
        public override ISet<IContentConstraintMutableObject> GetMutableContentConstraintObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.ContentConstraint, 
            //    this._retrievalManager.GetMutableContentConstraintObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single data consumer scheme, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IDataConsumerSchemeMutableObject" /> .
        /// </returns>
        public override IDataConsumerSchemeMutableObject GetMutableDataConsumerScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.DataConsumerScheme, 
            //    this._retrievalManager.GetMutableDataConsumerScheme);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets DataConsumerSchemeMutableObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IDataConsumerSchemeMutableObject> GetMutableDataConsumerSchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.DataConsumerScheme, 
            //    this._retrievalManager.GetMutableDataConsumerSchemeObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single Dataflow , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure.IDataflowMutableObject" /> .
        /// </returns>
        public override IDataflowMutableObject GetMutableDataflow(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.Dataflow, 
            //    this._retrievalManager.GetMutableDataflow);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets DataflowObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all DataflowObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IDataflowMutableObject> GetMutableDataflowObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.Dataflow, 
            //    this._retrievalManager.GetMutableDataflowObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets DataflowObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all DataflowObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="metadata">The metadata</param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IDataflowMutableObject> GetMutableDataflowObjects(
            IMaintainableRefObject maintainableReference,
            IObjectQueryMetadata metadata)
        {
            //return this.GetArtefacts(
            //    maintainableReference,
            //    metadata.IsReturnLatest,
            //    metadata.IsReturnStub,
            //    SdmxStructureEnumType.Dataflow,
            //    this._retrievalManager.GetMutableDataflowObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single Data Provider Scheme, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IDataProviderSchemeMutableObject" /> .
        /// </returns>
        public override IDataProviderSchemeMutableObject GetMutableDataProviderScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.DataProviderScheme, 
            //    this._retrievalManager.GetMutableDataProviderScheme);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets DataProviderSchemeMutableObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IDataProviderSchemeMutableObject> GetMutableDataProviderSchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.DataProviderScheme, 
            //    this._retrievalManager.GetMutableDataProviderSchemeObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single DataStructure.
        ///     This expects the ref object either to contain a URN or all the attributes required to uniquely identify the object.
        ///     If version information is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure.IDataStructureMutableObject" /> .
        /// </returns>
        public override IDataStructureMutableObject GetMutableDataStructure(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.Dsd, 
            //    this._retrievalManager.GetMutableDataStructure);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets DataStructureObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all dataStructureObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IDataStructureMutableObject> GetMutableDataStructureObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.Dsd, 
            //    this._retrievalManager.GetMutableDataStructureObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single HierarchicCodeList , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist.IHierarchicalCodelistMutableObject" /> .
        /// </returns>
        public override IHierarchicalCodelistMutableObject GetMutableHierarchicCodeList(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.HierarchicalCodelist, 
            //    this._retrievalManager.GetMutableHierarchicCodeList);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets HierarchicalCodelistObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all HierarchicalCodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IHierarchicalCodelistMutableObject> GetMutableHierarchicCodeListObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.HierarchicalCodelist, 
            //    this._retrievalManager.GetMutableHierarchicCodeListObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single Metadataflow , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure.IMetadataFlowMutableObject" /> .
        /// </returns>
        public override IMetadataFlowMutableObject GetMutableMetadataflow(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.MetadataFlow, 
            //    this._retrievalManager.GetMutableMetadataflow);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets MetadataFlowObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all MetadataFlowObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IMetadataFlowMutableObject> GetMutableMetadataflowObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.MetadataFlow, 
            //    this._retrievalManager.GetMutableMetadataflowObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single MetadataStructure , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The
        ///     <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure.IMetadataStructureDefinitionMutableObject" />
        ///     .
        /// </returns>
        public override IMetadataStructureDefinitionMutableObject GetMutableMetadataStructure(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.Msd, 
            //    this._retrievalManager.GetMutableMetadataStructure);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets MetadataStructureObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all MetadataStructureObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IMetadataStructureDefinitionMutableObject> GetMutableMetadataStructureObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.Msd, 
            //    this._retrievalManager.GetMutableMetadataStructureObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single organization scheme, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IOrganisationUnitSchemeMutableObject" /> .
        /// </returns>
        public override IOrganisationUnitSchemeMutableObject GetMutableOrganisationUnitScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.OrganisationUnitScheme, 
            //    this._retrievalManager.GetMutableOrganisationUnitScheme);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets OrganisationUnitSchemeMutableObject that match the parameters in the ref @object.  If the ref @object is null
        ///     or
        ///     has no attributes set, then this will be interpreted as a search for all OrganisationUnitSchemeMutableObject
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IOrganisationUnitSchemeMutableObject> GetMutableOrganisationUnitSchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.OrganisationUnitScheme, 
            //    this._retrievalManager.GetMutableOrganisationUnitSchemeObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a process @object, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process.IProcessMutableObject" /> .
        /// </returns>
        public override IProcessMutableObject GetMutableProcessObject(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.Process, 
            //    this._retrievalManager.GetMutableProcessObject);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets ProcessObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all IProcessObject
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IProcessMutableObject> GetMutableProcessObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.Process, 
            //    this._retrievalManager.GetMutableProcessObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Returns a provision agreement bean, this expects the ref object to contain
        ///     all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override IProvisionAgreementMutableObject GetMutableProvisionAgreement(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.ProvisionAgreement, 
            //    this._retrievalManager.GetMutableProvisionAgreement);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Returns ProvisionAgreement beans that match the parameters in the ref bean. If the ref bean is null or
        ///     has no attributes set, then this will be interpreted as a search for all ProvisionAgreement beans.
        /// </summary>
        /// <param name="maintainableReference">
        ///     the reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of objects that match the search criteria
        /// </returns>
        public override ISet<IProvisionAgreementMutableObject> GetMutableProvisionAgreementObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.ProvisionAgreement, 
            //    this._retrievalManager.GetMutableProvisionAgreementObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a reporting taxonomy @object, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme.IReportingTaxonomyMutableObject" /> .
        /// </returns>
        public override IReportingTaxonomyMutableObject GetMutableReportingTaxonomy(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.ReportingTaxonomy, 
            //    this._retrievalManager.GetMutableReportingTaxonomy);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets ReportingTaxonomyObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all ReportingTaxonomyObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IReportingTaxonomyMutableObject> GetMutableReportingTaxonomyObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.ReportingTaxonomy, 
            //    this._retrievalManager.GetMutableReportingTaxonomyObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a structure set @object, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping.IStructureSetMutableObject" /> .
        /// </returns>
        public override IStructureSetMutableObject GetMutableStructureSet(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetLatest(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.StructureSet, 
            //    this._retrievalManager.GetMutableStructureSet);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets StructureSetObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all StructureSetObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IStructureSetMutableObject> GetMutableStructureSetObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this.GetArtefacts(
            //    maintainableReference, 
            //    returnLatest, 
            //    returnStub, 
            //    SdmxStructureEnumType.StructureSet, 
            //    this._retrievalManager.GetMutableStructureSetObjects);
            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Returns the artefacts of <typeparamref name="T" /> that match <paramref name="maintainableReference" />
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference. ID, Agency and/or Version can have a value or null. Null is considered a wildcard.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <param name="sdmxStructure">
        ///     The SDMX structure type.
        /// </param>
        /// <param name="getter">
        ///     The getter method to retrieve the artefacts if <see cref="_requestToArtefacts" /> doesn't not contain them.
        /// </param>
        /// <typeparam name="T">
        ///     The type of the returned artefacts.
        /// </typeparam>
        /// <returns>
        ///     The <see cref="ISet{T}" />.
        /// </returns>
        private ISet<T> GetArtefacts<T>(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub, 
            SdmxStructureEnumType sdmxStructure, 
            Func<IMaintainableRefObject, bool, bool, ISet<T>> getter) where T : class, IMaintainableMutableObject
        {
            //ISet<IMaintainableMutableObject> mutableObjects;
            //ISet<T> retrievedObjects;
            //IStructureReference structureReference = new StructureReferenceImpl(maintainableReference, sdmxStructure);
            //if (!this._requestToArtefacts.TryGetValue(structureReference, out mutableObjects))
            //{
            //    _log.DebugFormat(CultureInfo.InvariantCulture, "Cache miss: {0}", structureReference);
            //    retrievedObjects = getter(maintainableReference, returnLatest, returnStub);
            //    this._requestToArtefacts.Add(
            //        structureReference, 
            //        new HashSet<IMaintainableMutableObject>(retrievedObjects));
            //    foreach (T retrievedObject in retrievedObjects)
            //    {
            //        var reference = _fromMutable.Build(retrievedObject);
            //        this._requestToArtefacts.AddToSet(reference, retrievedObject);
            //    }
            //}
            //else
            //{
            //    retrievedObjects = new HashSet<T>(mutableObjects.Cast<T>());
            //}

            //return retrievedObjects;

            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Return the latest artefact of type <typeparamref name="T" /> that matches the
        ///     <paramref name="maintainableReference" />.
        /// </summary>
        /// <typeparam name="T">The type of the requested artefact</typeparam>
        /// <param name="maintainableReference">The maintainable reference. The version must be null.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <param name="sdmxStructure">The SDMX structure type.</param>
        /// <param name="getter">
        ///     The getter method to retrieve the artefact if <see cref="_requestToLatestArtefacts" /> doesn't not
        ///     contain it.
        /// </param>
        /// <returns>
        ///     The <see cref="IMaintainableMutableObject" /> of type <typeparamref name="T" />; otherwise null
        /// </returns>
        private T GetLatest<T>(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub, 
            SdmxStructureEnumType sdmxStructure, 
            Func<IMaintainableRefObject, bool, bool, T> getter) where T : class, IMaintainableMutableObject
        {
            //IMaintainableMutableObject mutableObject;
            //IStructureReference structureReference = new StructureReferenceImpl(maintainableReference, sdmxStructure);
            //if (!this._requestToLatestArtefacts.TryGetValue(structureReference, out mutableObject))
            //{
            //    _log.DebugFormat(CultureInfo.InvariantCulture, "Cache miss: {0}", structureReference);
            //    T retrievedObject = getter(maintainableReference, returnLatest, returnStub);
            //    if (retrievedObject != null)
            //    {
            //        this._requestToLatestArtefacts.Add(structureReference, retrievedObject);
            //        this._requestToArtefacts.AddToSet(_fromMutable.Build(retrievedObject), retrievedObject);
            //        return retrievedObject;
            //    }

            //    return null;
            //}

            //return mutableObject as T;

            throw new InvalidOperationException("CachedRetrievalManager class is obsolete");
        }
    }
}