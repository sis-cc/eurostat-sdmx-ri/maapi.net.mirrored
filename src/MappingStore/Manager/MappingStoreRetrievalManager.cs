// -----------------------------------------------------------------------
// <copyright file="MappingStoreRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using DryIoc;

    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    ///     The mapping store retrieval manager.
    /// </summary>
    [Obsolete("Since MSDB 7.0")]
    public class MappingStoreRetrievalManager : RetrievalManagerBase
    {
        /// <summary>
        ///     The retrieval engine container.
        /// </summary>
        private readonly IRetrievalEngineContainer _retrievalEngineContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingStoreRetrievalManager"/> class.
        /// </summary>
        /// <param name="retrievalEngineContainer">The retrieval engine container.</param>
        public MappingStoreRetrievalManager(IRetrievalEngineContainer retrievalEngineContainer)
        {
            if (retrievalEngineContainer == null)
            {
                throw new ArgumentNullException("retrievalEngineContainer");
            }

            this._retrievalEngineContainer = retrievalEngineContainer;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MappingStoreRetrievalManager" /> class.
        /// </summary>
        /// <param name="connectionStringSettings">
        ///     The connection string settings.
        /// </param>
        public MappingStoreRetrievalManager(ConnectionStringSettings connectionStringSettings)
            : this(connectionStringSettings != null ? new Database(connectionStringSettings) : null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MappingStoreRetrievalManager" /> class.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        public MappingStoreRetrievalManager(Database database)
        {
            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this._retrievalEngineContainer = MappingStoreIoc.Container.Resolve<IRetrievalEngineContainerFactory>(serviceKey: MappingStoreIoc.ServiceKey).GetRetrievalEngineContainer(database);
        }

        /// <summary>
        ///     Gets a single Agency Scheme, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IAgencySchemeMutableObject" /> .
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implemented</exception>
        public override IAgencySchemeMutableObject GetMutableAgencyScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            // 7.1 ISTAT ENHANCEMENT
            //return maintainableReference.HasVersion()
            //           ? this._retrievalEngineContainer.AgencySchemeRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //           : this._retrievalEngineContainer.AgencySchemeRetrievalEngine.RetrieveLatest(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets AgencySchemeMutableObject that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IAgencySchemeMutableObject> GetMutableAgencySchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            // 7.2 ISTAT ENHANCEMENT
            //return this._retrievalEngineContainer.AgencySchemeRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single Categorisation, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme.ICategorisationMutableObject" /> .
        /// </returns>
        public override ICategorisationMutableObject GetMutableCategorisation(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //if (maintainableReference == null)
            //{
            //    throw new ArgumentNullException("maintainableReference");
            //}

            //if (maintainableReference.HasVersion())
            //{
            //    return
            //        this._retrievalEngineContainer.CategorisationRetrievalEngine.Retrieve(
            //            maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest), 
            //            returnStub.GetComplexQueryDetail()).FirstOrDefault();
            //}

            ////// TODO Change this when mapping store is modified to host proper categorisation artefacts
            //return this._retrievalEngineContainer.CategorisationRetrievalEngine.RetrieveLatest(
            //    maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest), 
            //    returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets CategorisationObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<ICategorisationMutableObject> GetMutableCategorisationObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //// TODO Change this when mapping store is modified to host proper categorisation artefacts
            //return this._retrievalEngineContainer.CategorisationRetrievalEngine.Retrieve(
            //    maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest), 
            //    returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single CategoryScheme , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme.ICategorySchemeMutableObject" /> .
        /// </returns>
        public override ICategorySchemeMutableObject GetMutableCategoryScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //if (maintainableReference == null)
            //{
            //    throw new ArgumentNullException("maintainableReference");
            //}

            //return maintainableReference.HasVersion() && !returnLatest
            //           ? this._retrievalEngineContainer.CategorySchemeRetrievalEngine.Retrieve(
            //               maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.CategoryScheme,false), 
            //               returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //           : this._retrievalEngineContainer.CategorySchemeRetrievalEngine.RetrieveLatest(
            //               maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.CategoryScheme,returnLatest), 
            //               returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets CategorySchemeObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CategorySchemeObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<ICategorySchemeMutableObject> GetMutableCategorySchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this._retrievalEngineContainer.CategorySchemeRetrievalEngine.Retrieve(
            //    maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.CategoryScheme,returnLatest), 
            //    returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single CodeList , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist.ICodelistMutableObject" /> .
        /// </returns>
        public override ICodelistMutableObject GetMutableCodelist(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //if (maintainableReference == null)
            //{
            //    throw new ArgumentNullException("maintainableReference");
            //}

            //return maintainableReference.HasVersion() && !returnLatest
            //           ? this._retrievalEngineContainer.CodeListRetrievalEngine.Retrieve(
            //               maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.CodeList,false), 
            //               returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //           : this._retrievalEngineContainer.CodeListRetrievalEngine.RetrieveLatest(
            //               maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.CodeList,returnLatest), 
            //               returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets CodelistObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<ICodelistMutableObject> GetMutableCodelistObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this._retrievalEngineContainer.CodeListRetrievalEngine.Retrieve(
            //    maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.CodeList,returnLatest), 
            //    returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single ConceptScheme , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme.IConceptSchemeMutableObject" /> .
        /// </returns>
        public override IConceptSchemeMutableObject GetMutableConceptScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //if (maintainableReference == null)
            //{
            //    throw new ArgumentNullException("maintainableReference");
            //}

            //var complexStructureReferenceObject = maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.ConceptScheme,returnLatest);
            //return maintainableReference.HasVersion() && !returnLatest
            //           ? this._retrievalEngineContainer.ConceptSchemeRetrievalEngine.Retrieve(
            //               complexStructureReferenceObject, 
            //               returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //           : this._retrievalEngineContainer.ConceptSchemeRetrievalEngine.RetrieveLatest(
            //               complexStructureReferenceObject, 
            //               returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets ConceptSchemeObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all ConceptSchemeObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IConceptSchemeMutableObject> GetMutableConceptSchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this._retrievalEngineContainer.ConceptSchemeRetrievalEngine.Retrieve(
            //    maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.ConceptScheme,returnLatest), 
            //    returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Returns a single Content Constraint, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The Content constraint.
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implemented</exception>
        public override IContentConstraintMutableObject GetMutableContentConstraint(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            // 7.3 ISTAT ENHANCEMENT
            //return maintainableReference.HasVersion() && !returnLatest
            //           ? this._retrievalEngineContainer.ContentConstraintRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //           : this._retrievalEngineContainer.ContentConstraintRetrievalEngine.RetrieveLatest(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Returns ContentConstraintBeans that match the parameters in the ref bean.  If the ref bean is null or
        ///     has no attributes set, then this will be interpreted as a search for all ContentConstraintObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     the reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of objects that match the search criteria
        /// </returns>
        public override ISet<IContentConstraintMutableObject> GetMutableContentConstraintObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            // 7.4 ISTAT ENHANCEMENT
            //return this._retrievalEngineContainer.ContentConstraintRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single data consumer scheme, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IDataConsumerSchemeMutableObject" /> .
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implemented</exception>
        public override IDataConsumerSchemeMutableObject GetMutableDataConsumerScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            // 7.5 ISTAT ENHANCEMENT
            //return maintainableReference.HasVersion()
            //            ? this._retrievalEngineContainer.DataConsumerSchemeRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //            : this._retrievalEngineContainer.DataConsumerSchemeRetrievalEngine.RetrieveLatest(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets DataConsumerSchemeMutableObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IDataConsumerSchemeMutableObject> GetMutableDataConsumerSchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            // 7.6 ISTAT ENHANCEMENT
            //return this._retrievalEngineContainer.DataConsumerSchemeRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single Dataflow , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure.IDataflowMutableObject" /> .
        /// </returns>
        public override IDataflowMutableObject GetMutableDataflow(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //if (maintainableReference == null)
            //{
            //    throw new ArgumentNullException("maintainableReference");
            //}

            //var complexStructureReferenceObject = maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Dataflow,returnLatest);
            //return maintainableReference.HasVersion() && !returnLatest
            //           ? this._retrievalEngineContainer.DataflowRetrievalEngine.Retrieve(
            //               complexStructureReferenceObject, 
            //               returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //           : this._retrievalEngineContainer.DataflowRetrievalEngine.RetrieveLatest(
            //               complexStructureReferenceObject, 
            //               returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets DataflowObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all DataflowObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IDataflowMutableObject> GetMutableDataflowObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this._retrievalEngineContainer.DataflowRetrievalEngine.Retrieve(
            //    maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Dataflow,returnLatest), 
            //    returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets DataflowObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all DataflowObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="metadata">The metadata.</param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IDataflowMutableObject> GetMutableDataflowObjects(
            IMaintainableRefObject maintainableReference,
            IObjectQueryMetadata metadata)
        {
            //return this._retrievalEngineContainer.DataflowRetrievalEngine.Retrieve(
            //    maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Dataflow, metadata.IsReturnLatest, metadata.IsReturnStable),
            //    metadata.IsReturnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single Data Provider Scheme, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IDataProviderSchemeMutableObject" /> .
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implemented</exception>
        public override IDataProviderSchemeMutableObject GetMutableDataProviderScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            // 7.7 ISTAT ENHANCEMENT
            //return maintainableReference.HasVersion()
            //           ? this._retrievalEngineContainer.DataProviderSchemeRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //           : this._retrievalEngineContainer.DataProviderSchemeRetrievalEngine.RetrieveLatest(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets DataProviderSchemeMutableObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IDataProviderSchemeMutableObject> GetMutableDataProviderSchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            // 7.8 ISTAT ENHANCEMENT
            //return this._retrievalEngineContainer.DataProviderSchemeRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single DataStructure.
        ///     This expects the ref object either to contain a URN or all the attributes required to uniquely identify the object.
        ///     If version information is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure.IDataStructureMutableObject" /> .
        /// </returns>
        public override IDataStructureMutableObject GetMutableDataStructure(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //if (maintainableReference == null)
            //{
            //    throw new ArgumentNullException("maintainableReference");
            //}

            //var complexStructureReferenceObject = maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Dsd,returnLatest);
            //return maintainableReference.HasVersion() && !returnLatest
            //           ? this._retrievalEngineContainer.DSDRetrievalEngine.Retrieve(
            //               complexStructureReferenceObject, 
            //               returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //           : this._retrievalEngineContainer.DSDRetrievalEngine.RetrieveLatest(
            //               complexStructureReferenceObject, 
            //               returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets DataStructureObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all dataStructureObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IDataStructureMutableObject> GetMutableDataStructureObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this._retrievalEngineContainer.DSDRetrievalEngine.Retrieve(
            //    maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Dsd,returnLatest), 
            //    returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single HierarchicCodeList , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist.IHierarchicalCodelistMutableObject" /> .
        /// </returns>
        public override IHierarchicalCodelistMutableObject GetMutableHierarchicCodeList(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //if (maintainableReference == null)
            //{
            //    throw new ArgumentNullException("maintainableReference");
            //}

            //var complexStructureReferenceObject = maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.HierarchicalCodelist,returnLatest);
            //return maintainableReference.HasVersion() && !returnLatest
            //           ? this._retrievalEngineContainer.HclRetrievalEngine.Retrieve(
            //               complexStructureReferenceObject, 
            //               returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //           : this._retrievalEngineContainer.HclRetrievalEngine.RetrieveLatest(
            //               complexStructureReferenceObject, 
            //               returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets HierarchicalCodelistObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all HierarchicalCodelistObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IHierarchicalCodelistMutableObject> GetMutableHierarchicCodeListObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this._retrievalEngineContainer.HclRetrievalEngine.Retrieve(
            //    maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.HierarchicalCodelist,returnLatest), 
            //    returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single Metadataflow , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure.IMetadataFlowMutableObject" /> .
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implemented</exception>
        public override IMetadataFlowMutableObject GetMutableMetadataflow(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //if (maintainableReference == null)
            //{
            //    throw new ArgumentNullException("maintainableReference");
            //}

            //var complexStructureReferenceObject = maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.MetadataFlow, returnLatest);
            //return maintainableReference.HasVersion() && !returnLatest
            //           ? this._retrievalEngineContainer.MetadataFlowRetrievalEngine.Retrieve(
            //               complexStructureReferenceObject,
            //               returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //           : this._retrievalEngineContainer.MetadataFlowRetrievalEngine.RetrieveLatest(
            //               complexStructureReferenceObject,
            //               returnStub.GetComplexQueryDetail());
            //;
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets MetadataFlowObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all MetadataFlowObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IMetadataFlowMutableObject> GetMutableMetadataflowObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this._retrievalEngineContainer.MetadataFlowRetrievalEngine.Retrieve(
            //  maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.MetadataFlow, returnLatest),
            //  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single MetadataStructure , this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The
        ///     <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure.IMetadataStructureDefinitionMutableObject" />
        ///     .
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implemented</exception>
        public override IMetadataStructureDefinitionMutableObject GetMutableMetadataStructure(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //if (maintainableReference == null)
            //{
            //    throw new ArgumentNullException("maintainableReference");
            //}

            //var complexStructureReferenceObject = maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Msd, returnLatest);
            //return maintainableReference.HasVersion() && !returnLatest
            //           ? this._retrievalEngineContainer.MsdRetrieverEngine.Retrieve(
            //               complexStructureReferenceObject,
            //               returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //           : this._retrievalEngineContainer.MsdRetrieverEngine.RetrieveLatest(
            //               complexStructureReferenceObject,
            //               returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets MetadataStructureObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all MetadataStructureObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IMetadataStructureDefinitionMutableObject> GetMutableMetadataStructureObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this._retrievalEngineContainer.MsdRetrieverEngine.Retrieve(
            //  maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Msd, returnLatest),
            //  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a single organization scheme, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IOrganisationUnitSchemeMutableObject" /> .
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implemented</exception>
        public override IOrganisationUnitSchemeMutableObject GetMutableOrganisationUnitScheme(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            // 7.9 ISTAT ENHANCEMENT
            //return maintainableReference.HasVersion()
            //            ? this._retrievalEngineContainer.OrganisationUnitSchemeRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //            : this._retrievalEngineContainer.OrganisationUnitSchemeRetrievalEngine.RetrieveLatest(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets OrganisationUnitSchemeMutableObject that match the parameters in the ref @object.  If the ref @object is null
        ///     or
        ///     has no attributes set, then this will be interpreted as a search for all OrganisationUnitSchemeMutableObject
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IOrganisationUnitSchemeMutableObject> GetMutableOrganisationUnitSchemeObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            // 7.10 ISTAT ENHANCEMENT
            //return this._retrievalEngineContainer.OrganisationUnitSchemeRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a process @object, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process.IProcessMutableObject" /> .
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implemented</exception>
        public override IProcessMutableObject GetMutableProcessObject(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Gets ProcessObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all IProcessObject
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IProcessMutableObject> GetMutableProcessObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Returns a provision agreement bean, this expects the ref object to contain
        ///     all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implemented</exception>
        public override IProvisionAgreementMutableObject GetMutableProvisionAgreement(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this._retrievalEngineContainer.ProvisionAgreementRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.ProvisionAgreement, returnLatest), returnStub.GetComplexQueryDetail()).FirstOrDefault();
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Returns ProvisionAgreement beans that match the parameters in the ref bean. If the ref bean is null or
        ///     has no attributes set, then this will be interpreted as a search for all ProvisionAgreement beans.
        /// </summary>
        /// <param name="maintainableReference">
        ///     the reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of objects that match the search criteria
        /// </returns>
        public override ISet<IProvisionAgreementMutableObject> GetMutableProvisionAgreementObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            //return this._retrievalEngineContainer.ProvisionAgreementRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.ProvisionAgreement, returnLatest), returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets a reporting taxonomy @object, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme.IReportingTaxonomyMutableObject" /> .
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implemented</exception>
        public override IReportingTaxonomyMutableObject GetMutableReportingTaxonomy(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Gets ReportingTaxonomyObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all ReportingTaxonomyObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IReportingTaxonomyMutableObject> GetMutableReportingTaxonomyObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Gets a structure set @object, this expects the ref object either to contain
        ///     a URN or all the attributes required to uniquely identify the object.  If version information
        ///     is missing then the latest version is assumed.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">The return Stub.</param>
        /// <returns>
        ///     The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping.IStructureSetMutableObject" /> .
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implemented</exception>
        public override IStructureSetMutableObject GetMutableStructureSet(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            // 7.11 ISTAT ENHANCEMENT
            //return maintainableReference.HasVersion()
            //           ? this._retrievalEngineContainer.StructureSetRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail()).FirstOrDefault()
            //           : this._retrievalEngineContainer.StructureSetRetrievalEngine.RetrieveLatest(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }

        /// <summary>
        ///     Gets StructureSetObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all StructureSetObjects
        /// </summary>
        /// <param name="maintainableReference">
        ///     The maintainable reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <returns>
        ///     list of sdmxObjects that match the search criteria
        /// </returns>
        public override ISet<IStructureSetMutableObject> GetMutableStructureSetObjects(
            IMaintainableRefObject maintainableReference, 
            bool returnLatest, 
            bool returnStub)
        {
            // 7.12 ISTAT ENHANCEMENT
            //return this._retrievalEngineContainer.StructureSetRetrievalEngine.Retrieve(maintainableReference.CreateComplexStructureReferenceObject(SdmxStructureEnumType.Categorisation,returnLatest),  returnStub.GetComplexQueryDetail());
            throw new InvalidOperationException("MappingStoreRetrievalManager class is obsolete");
        }
    }
}