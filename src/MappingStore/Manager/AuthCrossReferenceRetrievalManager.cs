// -----------------------------------------------------------------------
// <copyright file="AuthCrossReferenceRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using DryIoc;

    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Sdmx.MappingStore.Retrieve;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;

    /// <summary>
    ///     The cross reference mutable retrieval manager.
    /// </summary>
    [Obsolete("MSDB 7.0: After migrating to msdb 7")]
    public class AuthCrossReferenceRetrievalManager : IAuthCrossReferenceMutableRetrievalManager
    {
        /// <summary>
        ///     The _from mutable.
        /// </summary>
        private readonly StructureReferenceFromMutableBuilder _fromMutable = new StructureReferenceFromMutableBuilder();

        /// <summary>
        ///     The _retrieval advanced manager
        /// </summary>
        private readonly IAuthAdvancedSdmxMutableObjectRetrievalManager _retrievalAdvancedManager;

        /// <summary>
        ///     The retrieval engine container.
        /// </summary>
        private readonly IRetrievalEngineContainer _retrievalEngineContainer;

        /// <summary>
        ///     The _cross reference.
        /// </summary>
        private readonly IAuthSdmxMutableObjectRetrievalManager _retrievalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthCrossReferenceRetrievalManager"/> class.
        /// </summary>
        /// <param name="retrievalAdvancedManager">The retrieval advanced manager.</param>
        /// <param name="retrievalEngineContainer">The retrieval engine container.</param>
        public AuthCrossReferenceRetrievalManager(IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalAdvancedManager, IRetrievalEngineContainer retrievalEngineContainer)
        {
            if (retrievalAdvancedManager == null)
            {
                throw new ArgumentNullException("retrievalAdvancedManager");
            }

            if (retrievalEngineContainer == null)
            {
                throw new ArgumentNullException("retrievalEngineContainer");
            }

            this._retrievalAdvancedManager = retrievalAdvancedManager;
            this._retrievalEngineContainer = retrievalEngineContainer;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthCrossReferenceRetrievalManager"/> class.
        /// </summary>
        /// <param name="retrievalEngineContainer">The retrieval engine container.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        [Obsolete("MSDB 7.0: After migrating to msdb 7")]
        public AuthCrossReferenceRetrievalManager(IRetrievalEngineContainer retrievalEngineContainer, IAuthSdmxMutableObjectRetrievalManager retrievalManager)
        {
            if (retrievalEngineContainer == null)
            {
                throw new ArgumentNullException("retrievalEngineContainer");
            }

            if (retrievalManager == null)
            {
                throw new ArgumentNullException("retrievalManager");
            }

            this._retrievalEngineContainer = retrievalEngineContainer;
            this._retrievalManager = retrievalManager;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthCrossReferenceRetrievalManager" /> class.
        /// </summary>
        /// <param name="retrievalManager">
        ///     The retrieval manager
        /// </param>
        /// <param name="connectionStringSettings">
        ///     The connection String Settings.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="retrievalManager" /> is null
        ///     -or-
        ///     <paramref name="connectionStringSettings" /> is null
        /// </exception>
        public AuthCrossReferenceRetrievalManager(
            IAuthSdmxMutableObjectRetrievalManager retrievalManager, 
            ConnectionStringSettings connectionStringSettings)
        {
            if (retrievalManager == null)
            {
                throw new ArgumentNullException("retrievalManager");
            }

            if (connectionStringSettings == null)
            {
                throw new ArgumentNullException("connectionStringSettings");
            }

            this._retrievalManager = retrievalManager;
            this._retrievalEngineContainer = MappingStoreIoc.Container.Resolve<IRetrievalEngineContainerFactory>(serviceKey: MappingStoreIoc.ServiceKey).GetRetrievalEngineContainer(new Database(connectionStringSettings));
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthCrossReferenceRetrievalManager" /> class.
        /// </summary>
        /// <param name="retrievalManager">
        ///     The retrieval manager
        /// </param>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="retrievalManager" /> is null
        ///     -or-
        ///     <paramref name="database" /> is null
        /// </exception>
        public AuthCrossReferenceRetrievalManager(
            IAuthSdmxMutableObjectRetrievalManager retrievalManager, 
            Database database)
        {
            if (retrievalManager == null)
            {
                throw new ArgumentNullException("retrievalManager");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this._retrievalManager = retrievalManager;
            this._retrievalEngineContainer = MappingStoreIoc.Container.Resolve<IRetrievalEngineContainerFactory>(serviceKey: MappingStoreIoc.ServiceKey).GetRetrievalEngineContainer(database);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthCrossReferenceRetrievalManager" /> class.
        /// </summary>
        /// <param name="retrievalAdvancedManager">The retrieval advanced manager.</param>
        /// <param name="database">The database.</param>
        /// <exception cref="System.ArgumentNullException">
        ///     retrievalAdvancedManager
        ///     or
        ///     database
        /// </exception>
        public AuthCrossReferenceRetrievalManager(
            IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalAdvancedManager, 
            Database database)
        {
            if (retrievalAdvancedManager == null)
            {
                throw new ArgumentNullException("retrievalAdvancedManager");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this._retrievalAdvancedManager = retrievalAdvancedManager;
            this._retrievalEngineContainer = MappingStoreIoc.Container.Resolve<IRetrievalEngineContainerFactory>(serviceKey: MappingStoreIoc.ServiceKey).GetRetrievalEngineContainer(database);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthCrossReferenceRetrievalManager" /> class.
        /// </summary>
        /// <param name="retrievalAdvancedManager">The retrieval advanced manager.</param>
        /// <param name="connectionStringSettings">The connection string settings.</param>
        /// <exception cref="System.ArgumentNullException">
        ///     retrievalAdvancedManager
        ///     or
        ///     connectionStringSettings
        /// </exception>
        public AuthCrossReferenceRetrievalManager(
            IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalAdvancedManager, 
            ConnectionStringSettings connectionStringSettings)
        {
            if (retrievalAdvancedManager == null)
            {
                throw new ArgumentNullException("retrievalAdvancedManager");
            }

            if (connectionStringSettings == null)
            {
                throw new ArgumentNullException("connectionStringSettings");
            }

            this._retrievalAdvancedManager = retrievalAdvancedManager;
            this._retrievalEngineContainer = MappingStoreIoc.Container.Resolve<IRetrievalEngineContainerFactory>(serviceKey: MappingStoreIoc.ServiceKey).GetRetrievalEngineContainer(new Database(connectionStringSettings));
        }

        /// <summary>
        ///     Returns a list of MaintainableObject that cross reference the structure(s) that match the reference parameter
        /// </summary>
        /// <param name="structureReference">
        ///     - What Do I Reference?
        /// </param>
        /// <param name="queryDetail">
        ///     The query detail parameter.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        /// <param name="structures">
        ///     an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        ///     The <see cref="T:System.Collections.Generic.IList`1" />.
        /// </returns>
        [Obsolete("since MSDB 7.0; kept temporarily from old interface.")]
        public IList<IMaintainableMutableObject> GetCrossReferencedStructures(
            IStructureReference structureReference,
            StructureQueryDetail queryDetail, 
            IList<IMaintainableRefObject> allowedDataflows, 
            params SdmxStructureType[] structures)
        {
            if (structureReference == null)
            {
                throw new ArgumentNullException("structureReference");
            }

            IMaintainableMutableObject maintainableObject = this.GetMutableMaintainable(
                structureReference, 
                allowedDataflows);

            if(maintainableObject == null)
            {
                var structureString = string.Format(CultureInfo.InvariantCulture, "{0} {1}:{2} (v{3})", 
                                                                                   structureReference.MaintainableStructureEnumType, 
                                                                                   structureReference.MaintainableReference.AgencyId, 
                                                                                   structureReference.MaintainableReference.MaintainableId, 
                                                                                   structureReference.MaintainableReference.Version);
                throw new SdmxNoResultsException($"Could not find structure : {structureString}");
            }

            return this.GetCrossReferencedStructures(maintainableObject, queryDetail, allowedDataflows, structures);
        }

        /// <summary>
        ///     Returns a list of MaintainableObject that are cross referenced by the given identifiable structure
        /// </summary>
        /// <param name="identifiable">
        ///     the identifiable bean to retrieve the references for - What Do I Reference?
        /// </param>
        /// <param name="queryDetail">
        ///     The query detail parameter.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        /// <param name="structures">
        ///     an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        ///     The <see cref="IList{IMaintainableMutableObject}" />.
        /// </returns>
        public IList<IMaintainableMutableObject> GetCrossReferencedStructures(
            IIdentifiableMutableObject identifiable, 
            StructureQueryDetail queryDetail, 
            IList<IMaintainableRefObject> allowedDataflows, 
            params SdmxStructureType[] structures)
        {
            if (identifiable == null)
            {
                throw new ArgumentNullException("identifiable");
            }

            var sdmxStructureType = identifiable.StructureType;
            var maintainableMutableObject = identifiable as IMaintainableMutableObject;
            if (!sdmxStructureType.IsMaintainable || maintainableMutableObject == null)
            {
                throw new NotImplementedException(ErrorMessages.CrossReferenceIdentifiable + sdmxStructureType);
            }

            if (maintainableMutableObject.ExternalReference != null
                && maintainableMutableObject.ExternalReference.IsTrue)
            {
                var structureReference = this._fromMutable.Build(maintainableMutableObject);
                maintainableMutableObject = this.GetMutableMaintainable(structureReference, allowedDataflows);
            }

            Func<IStructureReference, IMaintainableMutableObject> retrievalManager =
                reference => this.GetMutableMaintainable(reference, allowedDataflows, queryDetail.ToReferenceQueryDetail());

            var crossReferenceEngine = new CrossReferenceResolverMutableEngine(structures);
            return
                new List<IMaintainableMutableObject>(
                    crossReferenceEngine.ResolveReferences(maintainableMutableObject, 1, retrievalManager));
        }

        /// <summary>
        ///     Returns a tree structure representing the <paramref name="maintainableObject" /> and all the structures that cross
        ///     reference it, and the structures that reference them, and so on.
        /// </summary>
        /// <param name="maintainableObject">
        ///     The maintainable object to build the tree for.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        /// <returns>
        ///     The <see cref="IMutableCrossReferencingTree" />.
        /// </returns>
        [Obsolete("Obsolete method since MSDB 7.0")]
        public IMutableCrossReferencingTree GetCrossReferenceTree(
            IMaintainableMutableObject maintainableObject, 
            IList<IMaintainableRefObject> allowedDataflows)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Returns a list of MaintainableObject that cross reference the structure(s) that match the reference parameter
        /// </summary>
        /// <param name="structureReference">
        ///     Who References Me?
        /// </param>
        /// <param name="queryDetail">
        ///     The query detail parameter.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        /// <param name="structures">
        ///     an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        ///     The <see cref="IList{IMaintainableMutableObject}" />.
        /// </returns>
        [Obsolete("since MSDB 7.0; kept temporarily from old interface.")]
        public IList<IMaintainableMutableObject> GetCrossReferencingStructures(
            IStructureReference structureReference,
            StructureQueryDetail queryDetail, 
            IList<IMaintainableRefObject> allowedDataflows, 
            params SdmxStructureType[] structures)
        {
            if (structureReference == null)
            {
                throw new ArgumentNullException("structureReference");
            }

            IMaintainableMutableObject maintainableObject = this.GetMutableMaintainable(
                structureReference, 
                allowedDataflows, 
                queryDetail);
            
            if(maintainableObject == null)
            {
                throw new SdmxNoResultsException($"structure with URN '{structureReference.TargetUrn}' not found");
            }
            
            return this.GetCrossReferencingStructures(maintainableObject, queryDetail, allowedDataflows, null, structures);
        }

        /// <summary>
        ///     Returns a list of MaintainableObject that cross reference the given identifiable structure
        /// </summary>
        /// <param name="identifiable">
        ///     the identifiable bean to retrieve the references for - Who References Me?
        /// </param>
        /// <param name="queryDetail">
        ///     The query detail parameter.
        /// </param>
        /// <param name="structures">
        ///     an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        ///     The <see cref="IList{IMaintainableMutableObject}" />.
        /// </returns>
        [Obsolete("since MSDB 7.0; kept temporarily from old interface.")]
        public IList<IMaintainableMutableObject> GetCrossReferencingStructures(
            IIdentifiableMutableObject identifiable, 
            StructureQueryDetail queryDetail, 
            params SdmxStructureType[] structures)
        {
            return this.GetCrossReferencingStructures(identifiable, queryDetail, null, null, structures);
        }

        /// <summary>
        ///     Returns a list of MaintainableObject that cross reference the given identifiable structure
        /// </summary>
        /// <param name="identifiable">
        ///     the identifiable bean to retrieve the references for - Who References Me?
        /// </param>
        /// <param name="queryDetail">
        ///     The query detail parameter.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        /// <param name="specificItems">
        ///     The specific item filter.
        /// </param>
        /// <param name="structures">
        ///     an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        ///     The <see cref="IList{IMaintainableMutableObject}" />.
        /// </returns>
        [Obsolete("since MSDB 7.0; kept temporarily from old interface.")]
        public IList<IMaintainableMutableObject> GetCrossReferencingStructures(
            IIdentifiableMutableObject identifiable,
            StructureQueryDetail queryDetail, 
            IList<IMaintainableRefObject> allowedDataflows,
            ISet<string> specificItems,
            params SdmxStructureType[] structures)
        {
            var maintainable = identifiable as IMaintainableMutableObject;
            if (maintainable == null)
            {
                throw new NotImplementedException("Only maintainable are supported by this implementation.");
            }

            IStructureReference structureReference = this._fromMutable.Build(maintainable);
            var mutableObjects = new List<IMaintainableMutableObject>();
            var sdmxStructureTypes = structures?.Length > 0 ? structures : SdmxMaintainableReferenceTree.GetParentReferences(maintainable.StructureType.IsMaintainable ? maintainable.StructureType.MaintainableStructureType : maintainable.StructureType);

            foreach (var sdmxStructureType in sdmxStructureTypes)
            {
                AddReferencedFor(sdmxStructureType.EnumType, mutableObjects, structureReference,
                    queryDetail.ToComplexForReferences(),
                    allowedDataflows,
                    specificItems);
            }

            return mutableObjects;
        }

        [Obsolete("Obsolete method since MSDB 7.0")]
        private void AddReferencedFor(SdmxStructureEnumType structureType,
            List<IMaintainableMutableObject> mutableObjects, IStructureReference structureReference,
            ComplexStructureQueryDetailEnumType complexQueryDetail, 
            IList<IMaintainableRefObject> allowedDataflows, 
            ISet<string> specificItems)
        {
            throw new NotImplementedException("Obsolete method");
        }

        /// <summary>
        ///     Gets the mutable maintainable.
        /// </summary>
        /// <param name="structureReference">The structure reference.</param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <param name="queryDetailEnum">
        ///     The query detail parameter.
        /// </param>
        /// <returns>The mutable maintainable or null</returns>
        [Obsolete("Obsolete method since MSDB 7.0")]
        private IMaintainableMutableObject GetMutableMaintainable(
            IStructureReference structureReference, 
            IList<IMaintainableRefObject> allowedDataflows,
            StructureQueryDetailEnumType queryDetailEnum = StructureQueryDetailEnumType.Full)
        {
            //if (this._retrievalManager != null)
            //{
            //    return this._retrievalManager.GetMutableMaintainable(
            //        structureReference, 
            //        false, 
            //        returnStub, 
            //        allowedDataflows);
            //}

            //var complexStructureQueryDetail = ComplexStructureQueryDetail.GetFromEnum(
            //    returnStub.GetComplexQueryDetail());
            //return this._retrievalAdvancedManager.GetMutableMaintainable(
            //    structureReference.ToComplex(), 
            //    complexStructureQueryDetail, 
            //    allowedDataflows);
            throw new NotImplementedException("Obsolete method");
        }
    }
}