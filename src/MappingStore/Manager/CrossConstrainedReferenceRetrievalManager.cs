// -----------------------------------------------------------------------
// <copyright file="CrossConstrainedReferenceRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2018-06-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Manager
{
    using System;
    using System.Collections.Generic;
    using DryIoc;

    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;

    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Should use the RetrievalEngineContainer directly")]
    public class CrossConstrainedReferenceRetrievalManager : ICrossConstrainedReferenceRetrievalManager
    {
        private readonly Dictionary<string, Dictionary<string, ICategoryMutableObject>> _categoryMap = new Dictionary<string, Dictionary<string, ICategoryMutableObject>>();
        private readonly StructureReferenceFromMutableBuilder _fromMutable = new StructureReferenceFromMutableBuilder();

        private readonly IRetrievalEngineContainer _retrievalEngineContainer;

        private readonly IAuthAdvancedSdmxMutableObjectRetrievalManager _retrievalAdvancedManager;

        private readonly IAuthCrossReferenceMutableRetrievalManager _crossReferenceRetriever;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="database"></param>
        /// <param name="retrievalAdvancedManager"></param>
        /// <param name="crossReferenceFactory"></param>
        [Obsolete("since MSDB 7.0; kept temporarily from old interface.")]
        public CrossConstrainedReferenceRetrievalManager(Database database, IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalAdvancedManager, IAuthCrossRetrievalManagerFactory crossReferenceFactory)
        {
            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            if (retrievalAdvancedManager == null)
            {
                throw new ArgumentNullException("retrievalManager");
            }

            if (crossReferenceFactory == null)
            {
                throw new ArgumentNullException("crossReferenceFactory");
            }

            this._retrievalAdvancedManager = retrievalAdvancedManager;
            this._crossReferenceRetriever = crossReferenceFactory.GetCrossRetrievalManager(database, retrievalAdvancedManager);
            this._retrievalEngineContainer = MappingStoreIoc.Container.Resolve<IRetrievalEngineContainerFactory>(serviceKey: MappingStoreIoc.ServiceKey).GetRetrievalEngineContainer(database);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="structureQuery"></param>
        /// <param name="mutableObjects"></param>
        /// <param name="allowedDataflows"></param>
        [Obsolete("since MSDB 7.0; kept temporarily from old interface.")]
        public void ProcessConstraints(
            IRestStructureQuery structureQuery,
            IMutableObjects mutableObjects,
            IList<IMaintainableRefObject> allowedDataflows)
        {
            //if (mutableObjects == null)
            //{
            //    throw new ArgumentNullException("mutableObjects");
            //}

            //if (mutableObjects.AllMaintainables.Count > 1)
            //{
            //    throw new SdmxNotImplementedException();
            //}

            //IMaintainableMutableObject constrainableArtefact = mutableObjects.AllMaintainables.GetOneOrNothing();
            //if (constrainableArtefact == null)
            //{
            //    return;
            //}

            //if (constrainableArtefact.StructureType != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd)
            //    && constrainableArtefact.StructureType != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow))
            //{
            //    throw new SdmxNotImplementedException();
            //}

            //StructureReferenceDetailEnumType referenceDetail = structureQuery.StructureQueryMetadata.StructureReferenceDetail.EnumType;
            //if (referenceDetail != StructureReferenceDetailEnumType.All &&
            //    referenceDetail != StructureReferenceDetailEnumType.Children &&
            //    referenceDetail != StructureReferenceDetailEnumType.Descendants)
            //{
            //    throw new SdmxNotImplementedException();
            //}
            //mutableObjects.AddIdentifiables(getReferences(referenceDetail, constrainableArtefact, allowedDataflows));
            throw new InvalidOperationException("Obsolete method, after the MSDB 7.0");
        }

        [Obsolete("since MSDB 7.0; kept temporarily from old interface.")]
        private IList<IMaintainableMutableObject> getReferences(
            StructureReferenceDetailEnumType referenceDetail,
            IMaintainableMutableObject maintainable,
            IList<IMaintainableRefObject> allowedDataflows)
        {
            //var references = new List<IMaintainableMutableObject>();
            //if (referenceDetail == StructureReferenceDetailEnumType.Children)
            //{
            //    references.AddRange(GetChildren(maintainable, allowedDataflows));
            //}
            //else if (referenceDetail == StructureReferenceDetailEnumType.Descendants)
            //{
            //    references.AddRange(GetDescendants(maintainable, allowedDataflows));
            //}
            //else if (referenceDetail == StructureReferenceDetailEnumType.All)
            //{
            //    references.AddRange(GetDescendants(maintainable, allowedDataflows));
            //    references.AddRange(GetParentsAndSiblings(maintainable, allowedDataflows));
            //}
            //return references;
            throw new InvalidOperationException("Obsolete method, after the MSDB 7.0");
        }

        [Obsolete("Obsolete since the MSDB 7.0")]
        private IList<IMaintainableMutableObject> GetChildren(
            IMaintainableMutableObject constrainableArtefact,
            IList<IMaintainableRefObject> allowedDataflows)
        {
            //return this.GetCrossReferencedStructures(constrainableArtefact, constrainableArtefact, allowedDataflows);
            throw new InvalidOperationException("Obsolete method, after the MSDB 7.0");
        }

        [Obsolete("Obsolete since the MSDB 7.0")]
        private IList<IMaintainableMutableObject> GetDescendants(
            IMaintainableMutableObject constrainableArtefact,
            IList<IMaintainableRefObject> allowedDataflows)
        {
            //var descendants = new List<IMaintainableMutableObject>();
            //var stack = new Stack<IMaintainableMutableObject>();
            //stack.Push(constrainableArtefact);
            //while (stack.Count > 0)
            //{
            //    var descendant = stack.Pop();
            //    descendants.Add(descendant);
            //    var children = this.GetCrossReferencedStructures(constrainableArtefact, descendant, allowedDataflows);
            //    for (int i = 0; i < children.Count; i++)
            //    {
            //        stack.Push(children[i]);
            //    }
            //}
            //return descendants;
            throw new InvalidOperationException("Obsolete method, after the MSDB 7.0");
        }

        [Obsolete("Obsolete since the MSDB 7.0")]
        private IList<IMaintainableMutableObject> GetParentsAndSiblings(
            IMaintainableMutableObject constrainableArtefact,
            IList<IMaintainableRefObject> allowedDataflows)
        {
            //// get parents
            //var parents = this._crossReferenceRetriever.GetCrossReferencingStructures(constrainableArtefact, false, allowedDataflows);
            //var parentsAndSiblings = new List<IMaintainableMutableObject>(parents);
            //foreach (var parent in parents)
            //{
            //    // get siblings
            //    var siblings = this.GetCrossReferencedStructures(constrainableArtefact, parent, allowedDataflows);
            //    parentsAndSiblings.AddRange(siblings);
            //}
            //return parentsAndSiblings;
            throw new InvalidOperationException("Obsolete method, after the MSDB 7.0");
        }

        [Obsolete("Obsolete since the MSDB 7.0")]
        private IList<IMaintainableMutableObject> GetCrossReferencedStructures(
            IMaintainableMutableObject constrainableArtefact,
            IMaintainableMutableObject maintainableArtefact,
            IList<IMaintainableRefObject> allowedDataflows)
        {
            //if (maintainableArtefact == null)
            //{
            //    throw new ArgumentNullException("maintainableRef");
            //}
            //IStructureReference constrainableArtefactRef = _fromMutable.Build(constrainableArtefact);
            //IStructureReference maintainableArtefactRef = _fromMutable.Build(maintainableArtefact);

            //Func<IStructureReference, IMaintainableMutableObject> retrievalManager =
            //    reference => this.GetMutableMaintainable(constrainableArtefactRef, reference, maintainableArtefactRef, allowedDataflows);

            //return
            //    new List<IMaintainableMutableObject>(
            //        new CrossReferenceResolverMutableEngine().ResolveReferences(maintainableArtefact, 1, retrievalManager));
            throw new InvalidOperationException("Obsolete method, after the MSDB 7.0");
        }

        [Obsolete("Obsolete since the MSDB 7.0")]
        private IMaintainableMutableObject GetMutableMaintainable(
            IStructureReference constrainableArtefactRef,
            IStructureReference structureRef,
            IStructureReference structureParentRef,
            IList<IMaintainableRefObject> allowedDataflows)
        {
            //if (structureRef.MaintainableStructureEnumType == SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme))
            //{
            //    var categoryScheme = this._retrievalAdvancedManager.GetMutableMaintainable(
            //        structureRef.ToComplex(),
            //        ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full),
            //        allowedDataflows) as ICategorySchemeMutableObject;

            //    if (categoryScheme == null)
            //    {
            //        return null;
            //    }

            //    var dict = _categoryMap.ContainsKey(categoryScheme.Id) 
            //        ? _categoryMap[categoryScheme.Id] 
            //        : new Dictionary<string, ICategoryMutableObject>();

            //    if (!categoryScheme.IsPartial || dict.Count == 0)
            //    {
            //        MapCategories(_categoryMap[categoryScheme.Id] = dict, categoryScheme.Items, null);   
            //        categoryScheme.IsPartial = true;
            //    }

            //    var ids = structureRef.IdentifiableIds;
            //    var path = ids[0];
            //    var category = dict[path];

            //    if (!categoryScheme.Items.Contains(category))
            //    {
            //        categoryScheme.AddItem(category);
            //    }

            //    for (var i = 1; i < ids.Count; i++)
            //    {
            //        if (dict.TryGetValue(path = path + "." + ids[i], out var child))
            //        {
            //            if (!category.Items.Contains(child))
            //            {
            //                category.AddItem(child);
            //            }

            //            category = child;
            //        }
            //    }

            //    return categoryScheme;
            //}
            //else if (structureRef.MaintainableStructureEnumType == SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList))
            //{
            //    ISet<ICodelistMutableObject> constrainedCodelist = this._retrievalEngineContainer.ConstrainedCodeListRetrievalEngine.Retrieve(structureRef, constrainableArtefactRef);
            //    return constrainedCodelist.GetOneOrNothing();
            //}
            //else if (structureRef.MaintainableStructureEnumType == SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme)
            //    && structureParentRef.MaintainableStructureEnumType == SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd))
            //{
            //    ISet<IConceptSchemeMutableObject> partialConceptScheme = this._retrievalEngineContainer.PartialConceptSchemeRetrievalEngine.Retrieve(structureRef, structureParentRef);
            //    return partialConceptScheme.GetOneOrNothing();
            //}
            //else
            //{
            //    return this._retrievalAdvancedManager.GetMutableMaintainable(
            //         structureRef.ToComplex(),
            //         ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full),
            //         allowedDataflows);
            //}
            throw new InvalidOperationException("Obsolete method, after the MSDB 7.0");
        }

        [Obsolete("Obsolete since the MSDB 7.0")]
        private void MapCategories(Dictionary<string, ICategoryMutableObject> dict, IList<ICategoryMutableObject> cats, string prefix)
        {
            //foreach (var c in cats)
            //{
            //    dict[prefix + c.Id] = c;
            //    MapCategories(dict, c.Items, prefix + c.Id + ".");
            //}

            //cats.Clear();
            throw new InvalidOperationException("Obsolete method, after the MSDB 7.0");
        }
    }
}