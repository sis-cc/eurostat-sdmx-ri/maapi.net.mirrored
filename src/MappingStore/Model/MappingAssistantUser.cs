﻿// -----------------------------------------------------------------------
// <copyright file="MappingAssistantUser.cs" company="EUROSTAT">
//   Date Created : 2017-05-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model
{
    using System;
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.Mapping.Api.Model;

    /// <summary>
    /// The Mapping Assistant user
    /// </summary>
    /// <seealso cref="System.Security.Principal.IPrincipal" />
    public class MappingAssistantUser : IPrincipalWithStoreId
    {
        /// <summary>
        /// The identity
        /// </summary>
        private readonly IIdentity _identity;

        /// <summary>
        /// The permission
        /// </summary>
        private readonly PermissionType _permission;

        /// <summary>
        /// The user identifier
        /// </summary>
        private readonly long _userId;

        /// <summary>
        /// The store identifier
        /// </summary>
        private readonly string _storeId;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingAssistantUser" /> class.
        /// </summary>
        /// <param name="userCoordinates">The user coordinates.</param>
        /// <param name="identity">The identity.</param>
        /// <param name="permission">The permission.</param>
        public MappingAssistantUser(UserCoordinates userCoordinates, IIdentity identity, PermissionType permission)
        {
            this._identity = identity;
            this._permission = permission;
            this._userId = userCoordinates.UserId;
            this._storeId = userCoordinates.StoreId;
        }

        /// <summary>
        /// Gets the identity of the current principal.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Security.Principal.IIdentity"/> object associated with the current principal.
        /// </returns>
        public IIdentity Identity
        {
            get
            {
                return this._identity;
            }
        }

        /// <summary>
        /// Gets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public long UserId
        {
            get
            {
                return this._userId;
            }
        }

        /// <summary>
        /// Gets the store identifier.
        /// </summary>
        /// <value>
        /// The store identifier.
        /// </value>
        public string StoreId
        {
            get
            {
                return this._storeId;
            }
        }

        /// <summary>
        /// Determines whether the current principal belongs to the specified role.
        /// </summary>
        /// <returns>
        /// true if the current principal is a member of the specified role; otherwise, false.
        /// </returns>
        /// <param name="role">The name of the role for which to check membership. </param>
        public bool IsInRole(string role)
        {
            PermissionType roleOutput;
            if (!Enum.TryParse(role, out roleOutput))
            {
                return false;
            }

            return this.IsInRole(roleOutput);
        }

        /// <summary>
        /// Determines whether the current principal belongs to the specified role.
        /// </summary>
        /// <param name="roleOrAttribute">The role or attribute.</param>
        /// <returns>
        /// true if the current principal is a member of the specified role; otherwise, false.
        /// </returns>
        public bool IsInRole(PermissionType roleOrAttribute)
        {
            if (!this._permission.HasFlag(roleOrAttribute))
            {
                return false;
            }

            return true;
        }
    }
}