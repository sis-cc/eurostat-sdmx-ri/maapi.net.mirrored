// -----------------------------------------------------------------------
// <copyright file="PartialCategorySchemeStructureQuery.cs" company="EUROSTAT">
//   Date Created : 2023-01-20
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// Extends the <see cref="CommonStructureQueryCore"/> to include information needed to retrieve 
    /// partial category schemes.
    /// </summary>
    public class PartialCategorySchemeStructureQuery : CommonStructureQueryCore, IPartialCategorySchemeStructureQuery
    {
        private readonly IList<ICategorisationMutableObject> _categorisations = new List<ICategorisationMutableObject>();

        /// <summary>
        /// Initializes a new instance of the class using an existing instance to extend.
        /// </summary>
        /// <param name="proxy"></param>
        public PartialCategorySchemeStructureQuery(ICommonStructureQuery proxy)
            : base(proxy)
        {
        }

        /// <inheritdoc/>
        public IReadOnlyList<ICategorisationMutableObject> Categorisations 
            => new ReadOnlyCollection<ICategorisationMutableObject>(this._categorisations);

        /// <inheritdoc/>
        public void AddCategorisations(params ICategorisationMutableObject[] categorisations)
        {
            _categorisations.AddAll(categorisations);
        }
    }
}
