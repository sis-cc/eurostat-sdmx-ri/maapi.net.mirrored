﻿// -----------------------------------------------------------------------
// <copyright file="MappingSetEntity.cs" company="EUROSTAT">
//   Date Created : 2013-04-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel
{
    using System;
    using System.Collections.ObjectModel;

    /// <summary>
    ///     This value object represents a mapping set
    /// </summary>
    [Obsolete("Use MA API IComponentMappingContainer")]
    public class MappingSetEntity : PersistentEntityBase
    {
        /// <summary>
        ///     The _mappings.
        /// </summary>
        private readonly Collection<MappingEntity> _mappings;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MappingSetEntity" /> class.
        ///     Default constructor used to initialize the
        ///     <see cref="Mappings" />
        /// </summary>
        /// <param name="sysId">
        ///     The sys Id.
        /// </param>
        public MappingSetEntity(long sysId)
            : base(sysId)
        {
            this._mappings = new Collection<MappingEntity>();
        }

        /// <summary>
        ///     Gets or sets the mapping set dataflow
        /// </summary>
        public DataflowEntity Dataflow { get; set; }

        /// <summary>
        ///     Gets or sets the mapping set dataset
        /// </summary>
        public DataSetEntity DataSet { get; set; }

        /// <summary>
        ///     Gets or sets the mapping set description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets the mapping set identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the last updated.
        /// </summary>
        /// <value>
        /// The last updated.
        /// </value>
        public LastUpdatedEntity LastUpdated { get; set; }

        /// <summary>
        /// Gets or sets the update status.
        /// </summary>
        /// <value>
        /// The update status.
        /// </value>
        public UpdateStatusMappingEntity UpdateStatus { get; set; }

        /// <summary>
        ///     Gets the mapping set list of mappings
        /// </summary>
        public Collection<MappingEntity> Mappings
        {
            get
            {
                return this._mappings;
            }
        }
    }
}