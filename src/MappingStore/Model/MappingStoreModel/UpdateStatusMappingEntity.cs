﻿// -----------------------------------------------------------------------
// <copyright file="UpdateStatusMappingEntity.cs" company="EUROSTAT">
//   Date Created : 2016-10-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The update status mapping.
    /// </summary>
    public class UpdateStatusMappingEntity : PersistentEntityBase
    {
        /// <summary>
        /// The transcoding
        /// </summary>
        private Dictionary<string, ObservationActionEnumType> _transcoding;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateStatusMappingEntity"/> class.
        /// </summary>
        /// <param name="sysId">The unique entity identifier</param>
        public UpdateStatusMappingEntity(long sysId)
            : base(sysId)
        {
            this._transcoding = new Dictionary<string, ObservationActionEnumType>(StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        ///     Gets or sets the mapping constant
        /// </summary>
        public ObservationAction Constant { get; set; }

        /// <summary>
        /// Gets or sets the column.
        /// </summary>
        /// <value>
        /// The column.
        /// </value>
        public DataSetColumnEntity Column { get; set; }

        /// <summary>
        /// Adds the specified SDMX action code.
        /// </summary>
        /// <param name="sdmxActionCode">The SDMX action code.</param>
        /// <param name="localCode">The local code.</param>
        public void Add(string sdmxActionCode, string localCode)
        {
            var obsAction = ObservationAction.ParseString(sdmxActionCode);
            this._transcoding.Add(localCode, obsAction);
        }

        /// <summary>
        /// Gets the action.
        /// </summary>
        /// <param name="localCode">
        /// The local code.
        /// </param>
        /// <returns>
        /// The <see cref="ObservationActionEnumType"/>.
        /// </returns>
        public ObservationActionEnumType GetAction(string localCode)
        {
            ObservationActionEnumType obsAction;
            if (this._transcoding.TryGetValue(localCode, out obsAction))
            {
                return obsAction;
            }

            return ObservationActionEnumType.Null;
        }

        /// <summary>
        /// Gets the local code for action.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns>
        /// The local action.
        /// </returns>
        public string GetLocalAction(ObservationActionEnumType action)
        {
            return this.GetLocalAction(ObservationAction.GetFromEnum(action));
        }

        /// <summary>
        /// Gets the local code for action.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns>
        /// The local action.
        /// </returns>
        public string GetLocalAction(ObservationAction action)
        {
            return this._transcoding.Where(pair => pair.Value == action.EnumType)
                .Select(pair => pair.Key)
                .FirstOrDefault();
        }

        /// <summary>
        /// Gets the active local actions.
        /// </summary>
        /// <returns>The active actions (ADDED/UPDATED) that were mapped.</returns>
        public IEnumerable<string> GetActiveLocalActions()
        {
            return this._transcoding.Where(pair => pair.Value == ObservationActionEnumType.Updated || pair.Value == ObservationActionEnumType.Added).Select(pair => pair.Key);
        }
    }
}