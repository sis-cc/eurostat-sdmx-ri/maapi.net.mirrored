// -----------------------------------------------------------------------
// <copyright file="IPartialCategorySchemeStructureQuery.cs" company="EUROSTAT">
//   Date Created : 2023-01-20
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model
{
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    /// <summary>
    /// Extends the <see cref="ICommonStructureQuery"/> to include info for retrieving partial category schemes.
    /// </summary>
    public interface IPartialCategorySchemeStructureQuery : ICommonStructureQuery
    {
        /// <summary>
        /// All the categorisations that reference codes in the category scheme(s).
        /// </summary>
        IReadOnlyList<ICategorisationMutableObject> Categorisations { get; }

        /// <summary>
        /// Add one or more categorisations
        /// </summary>
        /// <param name="categorisations"></param>
        void AddCategorisations(params ICategorisationMutableObject[] categorisations);
    }
}
