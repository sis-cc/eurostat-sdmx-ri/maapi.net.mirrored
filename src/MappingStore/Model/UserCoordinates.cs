// -----------------------------------------------------------------------
// <copyright file="UserCoordinates.cs" company="EUROSTAT">
//   Date Created : 2017-06-08
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model
{
    /// <summary>
    /// The user coordinates.
    /// </summary>
    public class UserCoordinates
    {
        /// <summary>
        /// The _store id.
        /// </summary>
        private readonly string _storeId;

        /// <summary>
        /// The _user id.
        /// </summary>
        private readonly long _userId;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserCoordinates"/> class.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="storeId">
        /// The store id.
        /// </param>
        public UserCoordinates(long userId, string storeId)
        {
            this._userId = userId;
            this._storeId = storeId;
        }

        /// <summary>
        /// Gets the store id.
        /// </summary>
        public string StoreId
        {
            get
            {
                return this._storeId;
            }
        }

        /// <summary>
        /// Gets the user id.
        /// </summary>
        public long UserId
        {
            get
            {
                return this._userId;
            }
        }
    }
}