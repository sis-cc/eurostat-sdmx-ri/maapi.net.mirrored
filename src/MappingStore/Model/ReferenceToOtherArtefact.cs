// -----------------------------------------------------------------------
// <copyright file="ReferenceToOtherArtefact.cs" company="EUROSTAT">
//   Date Created : 2022-06-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Model
{
    using System;
    using Estat.Sri.Mapping.Api.Constant;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// An object that represents a reference to other artefact.
    /// </summary>
    public class ReferenceToOtherArtefact
    {
        private readonly IStructureReference _target;
        private readonly string _referenceType;
        private readonly string _subStructureFullPath;
        private readonly string _targetSubStructurePath;
        private readonly ITextFormat _textFormat;

        /// <summary>
        /// Gets the maintainable target, i.e. the Concept Scheme not the concept.
        /// Returns The <see cref="IStructureReference"/> to the referenced artefact.
        /// </summary>
        public IStructureReference Target => _target;

        /// <summary>
        /// The reference type.
        /// Returns a value from <see cref="RefTypes"/>
        /// </summary>
        public string ReferenceType => _referenceType;

        /// <summary>
        /// (Optional) The source sub structure i.e. the item of an item scheme or the component in a DSD/MSD. Can be null.
        /// Used when such sub structure is making this reference e.g. a Concept to a codelist, a Dimension to a Concept,
        /// but not when the referencing is done at artefact level e.g. a dataflow to DSD.
        /// </summary>
        public string SubStructureFullPath => _subStructureFullPath;

        /// <summary>
        /// (Optional) The target substructure i.e. item of an item scheme, component of a DSD. Can be null.
        /// Used when the reference is for such substructure.e.g. A Dimension referencing, a concept for conceptIdentity or concept role,
        /// but not when referencing a maintainable artefact e.g. codelist, msd, dsd, concept scheme.
        /// </summary>
        public string TargetSubStructurePath => _targetSubStructurePath;

        /// <summary>
        /// The text format
        /// </summary>
        public ITextFormat TextFormat => _textFormat;

        /// <summary>
        /// Returns whether ther is a source sub structure given.
        /// </summary>
        public bool HasSubStructure => ObjectUtil.ValidString(SubStructureFullPath);

        #region constructors

        /// <summary>
        /// Initializes a new reference object.
        /// </summary>
        /// <param name="target">The <see cref="Target"/></param>
        /// <param name="referenceType">The <see cref="ReferenceType"/></param>
        /// <param name="subStructureFullPath">The <see cref="SubStructureFullPath"/></param>
        /// <param name="targetSubStructurePath">The <see cref="TargetSubStructurePath"/></param>
        /// <exception cref="ArgumentNullException"></exception>
        public ReferenceToOtherArtefact(IStructureReference target, string referenceType, string subStructureFullPath, string targetSubStructurePath)
        {
            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }
            if (referenceType == null)
            {
                throw new ArgumentNullException(nameof(referenceType));
            }
            this._target = target;
            this._referenceType = referenceType;
            this._subStructureFullPath = subStructureFullPath;
            this._targetSubStructurePath = targetSubStructurePath;
            this._textFormat = null;
        }

        /// <summary>
        /// Initializes a new reference object.
        /// </summary>
        /// <param name="crossReferenceObject">the create from</param>
        /// <param name="referenceType"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public ReferenceToOtherArtefact(ICrossReference crossReferenceObject, string referenceType)
        {
            if (crossReferenceObject == null)
            {
                throw new ArgumentNullException(nameof(crossReferenceObject));
            }
            if (referenceType == null)
            {
                throw new ArgumentNullException(nameof(referenceType));
            }
            _target = new StructureReferenceImpl(crossReferenceObject.MaintainableReference, crossReferenceObject.MaintainableStructureEnumType);
            this._referenceType = referenceType;
            ISdmxObject referencedFrom = crossReferenceObject.ReferencedFrom;
            _subStructureFullPath = referencedFrom is IIdentifiableObject
                ? ExtractParent((IIdentifiableObject)referencedFrom)
                : ExtractParent(referencedFrom.GetParent<IIdentifiableObject>(false));

            this._targetSubStructurePath = crossReferenceObject.FullId;
            this._textFormat = null;
        }

        /// <summary>
        /// Initializes a new reference object of a text format.
        /// </summary>
        /// <param name="textFormat"></param>
        /// <param name="referenceType"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public ReferenceToOtherArtefact(ITextFormat textFormat, string referenceType)
        {
            if (textFormat == null)
            {
                throw new ArgumentNullException(nameof(textFormat));
            }
            if (referenceType == null)
            {
                throw new ArgumentNullException(nameof(referenceType));
            }
            _target = null;
            this._referenceType = referenceType;
            IIdentifiableObject parent = textFormat.GetParent<IIdentifiableObject>(false);
            _subStructureFullPath = ExtractParent(parent);
            this._targetSubStructurePath = null;
            this._textFormat = textFormat;
        }

        #endregion

        public IStructureReference BuildSubStructureActualTarget(SdmxStructureType structureType)
        {
            if (!ObjectUtil.ValidString(TargetSubStructurePath))
            {
                throw new InvalidOperationException("This artefact reference has no target sub structure path");
            }

            if (structureType.IsMaintainable)
            {
                throw new InvalidOperationException("The given structureType must not be a maintainable");
            }

            return new StructureReferenceImpl(Target.AgencyId, Target.MaintainableId, Target.Version, structureType, TargetSubStructurePath.Split('.'));
        }

        private string ExtractParent(IIdentifiableObject parent)
        {
            string parentIdPath;
            if (parent != null && !parent.StructureType.IsMaintainable)
            {
                if (parent.IdentifiableParent is IMetadataTarget)
                {
                    parentIdPath = parent.GetFullIdPath(true);
                }
                else
                {
                    parentIdPath = parent.GetFullIdPath(false);
                }
            }
            else
            {
                parentIdPath = null;
            }
            return parentIdPath;
        }
    }
}
