using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model.TextFormatValue
{
    public class SentinelTextFormatValue:TextFormatValue
    {
        [JsonProperty("s")]
        public List<SentinelItem> SentinelItems { get; set; }

        public SentinelTextFormatValue() : base("sentinel") {
        
        }
    }
}
