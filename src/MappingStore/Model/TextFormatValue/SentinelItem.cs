using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model.TextFormatValue
{
    public class SentinelItem
    {
        [JsonProperty("v")]
        public string Value { get; set; }

        [JsonProperty("n")]
        public List<TextItem> Names { get; set; }

        [JsonProperty("d")]
        public List<TextItem> Descriptions { get; set; }
    }
}
