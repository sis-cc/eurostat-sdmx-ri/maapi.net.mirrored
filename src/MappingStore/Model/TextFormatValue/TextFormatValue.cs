using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model.TextFormatValue
{
    public class TextFormatValue
    {
        [JsonProperty("t")] 
        private string Type;

        public TextFormatValue(string type)
        {
            this.Type = type;
        }
    }
}
