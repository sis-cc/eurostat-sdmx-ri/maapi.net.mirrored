using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model.TextFormatValue
{
    public class TextItem
    {
        [JsonProperty("l")]
        public string Locale { get; set; }

        [JsonProperty("v")]
        public string Value { get; set; }
    } 
}
