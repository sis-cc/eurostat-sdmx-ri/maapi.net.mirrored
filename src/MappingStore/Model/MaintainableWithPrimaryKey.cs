// -----------------------------------------------------------------------
// <copyright file="ArtefactRetriever7Engine.cs" company="EUROSTAT">
//   Date Created : 2022-06-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Model
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    /// <summary>
    /// Acts as a DTO to tranfer information for a <see cref="IMaintainableMutableObject"/>
    /// </summary>
    /// <typeparam name="TMaintainable">The type of SDMX object</typeparam>
    public class MaintainableWithPrimaryKey<TMaintainable>
        where TMaintainable : IMaintainableMutableObject
    {
        private readonly TMaintainable _maintainable;

        private readonly long _primaryKey;

        private readonly List<ReferenceToOtherArtefact> _references = new List<ReferenceToOtherArtefact>();

        /// <summary>
        /// Initializes a new instance of the class
        /// </summary>
        /// <param name="maintainable"></param>
        /// <param name="primaryKey"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public MaintainableWithPrimaryKey(TMaintainable maintainable, long primaryKey)
        {
            if (maintainable == null)
            {
                throw new ArgumentNullException(nameof(maintainable));
            }
            this._maintainable = maintainable;
            this._primaryKey = primaryKey;
        }

        /// <summary>
        /// Mutable list of references to other artefacts.
        /// Use this property to add references.
        /// </summary>
        public List<ReferenceToOtherArtefact> References => this._references;

        /// <summary>
        /// The SDMX maintainable object
        /// </summary>
        public TMaintainable Maintainable => this._maintainable;

        /// <summary>
        /// The <see cref="Maintainable"/>'s PK.
        /// </summary>
        public long PrimaryKey => this._primaryKey;
    }
}
