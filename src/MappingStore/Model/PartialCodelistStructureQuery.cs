// -----------------------------------------------------------------------
// <copyright file="PartialCodelistStructureQuery.cs" company="EUROSTAT">
//   Date Created : 2023-01-20
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// Extends the <see cref="CommonStructureQueryCore"/> to include information needed to retrieve 
    /// partial codelists.
    /// </summary>
    public class PartialCodelistStructureQuery : CommonStructureQueryCore, IPartialCodelistStructureQuery
    {
        private readonly IList<IContentConstraintMutableObject> _contentConstraints = new List<IContentConstraintMutableObject>();
        private readonly IDictionary<string, ISet<string>> _codelistToComponents = new Dictionary<string, ISet<string>>();

        /// <summary>
        /// Initializes a new instance of the class using an existing instance to extend.
        /// </summary>
        /// <param name="proxy"></param>
        public PartialCodelistStructureQuery(ICommonStructureQuery proxy)
            : base(proxy)
        {
        }

        /// <inheritdoc/>
        public IReadOnlyList<IContentConstraintMutableObject> ContentConstraints
            => new ReadOnlyCollection<IContentConstraintMutableObject>(_contentConstraints);

        /// <inheritdoc/>
        public void AddContentConstraints(params IContentConstraintMutableObject[] constraints)
        {
            _contentConstraints.AddAll(constraints);
        }

        /// <inheritdoc/>
        public IReadOnlyDictionary<string, ISet<string>> CodelistToComponents
            => new ReadOnlyDictionary<string, ISet<string>>(_codelistToComponents);


        /// <summary>
        /// Will add or append the <paramref name="component"/> to the <paramref name="codelist"/>.
        /// </summary>
        /// <param name="codelist"></param>
        /// <param name="component"></param>
        public void AddCodelistToComponent(string codelist, string component)
        {
            if (!_codelistToComponents.ContainsKey(codelist)) 
            {
                _codelistToComponents.Add(codelist, new HashSet<string>());
            }
            _codelistToComponents[codelist].Add(component);
        }
    }
}
