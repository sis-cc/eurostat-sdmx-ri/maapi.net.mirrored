// -----------------------------------------------------------------------
// <copyright file="AnnotationProcedureBase.cs" company="EUROSTAT">
//   Date Created : 2014-10-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     The annotation procedure base.
    /// </summary>
    public abstract class AnnotationProcedureBase : OutputProcedureBase
    {
        /// <summary>
        ///     The identifier parameter name
        /// </summary>
        private const string IdParameterName = "p_id";

        /// <summary>
        ///     The title parameter name
        /// </summary>
        private const string TitleParameterName = "p_title";

        /// <summary>
        ///     The type parameter name
        /// </summary>
        private const string TypeParameterName = "p_type";

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotationProcedureBase" /> class.
        /// </summary>
        /// <param name="procedureName">
        ///     Name of the procedure.
        /// </param>
        protected AnnotationProcedureBase(string procedureName)
            : base(procedureName)
        {
        }

        /// <summary>
        ///     Gets the name of the annotate-able identifier parameter.
        /// </summary>
        /// <value>
        ///     The name of the annotate able identifier parameter.
        /// </value>
        public abstract string ParentIdParameterName { get; }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateUriParameter(command, null);
            this.CreateIdParameter(command, null);
            this.CreateTitleParameter(command, null);
            this.CreateTypeParameter(command, null);
            return command;
        }

        /// <summary>
        ///     Creates the identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateIdParameter(DbCommand command, string value)
        {
            if (value != null)
            {
                return this.CreateParameter(command, IdParameterName, DbType.String, ParameterDirection.Input, value);
            }

            return this.CreateParameter(command, IdParameterName, DbType.String, ParameterDirection.Input, DBNull.Value);
        }

        /// <summary>
        ///     Creates the parent identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateParentIdParameter(DbCommand command, long value)
        {
            return this.CreateParameter(command, this.ParentIdParameterName, DbType.Int64, ParameterDirection.Input, value);
        }

        /// <summary>
        ///     Creates the identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateTitleParameter(DbCommand command, string value)
        {
            if (value != null)
            {
                return this.CreateParameter(command, TitleParameterName, DbType.String, ParameterDirection.Input, value);
            }

            return this.CreateParameter(command, TitleParameterName, DbType.String, ParameterDirection.Input, DBNull.Value);
        }

        /// <summary>
        ///     Creates the identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateTypeParameter(DbCommand command, string value)
        {
            if (value != null)
            {
                return this.CreateParameter(command, TypeParameterName, DbType.String, ParameterDirection.Input, value);
            }

            return this.CreateParameter(command, TypeParameterName, DbType.String, ParameterDirection.Input, DBNull.Value);
        }

        /// <summary>
        ///     Creates the identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateUriParameter(DbCommand command, string value)
        {
            if (value != null)
            {
                return this.CreateParameter(command, "p_url", DbType.String, ParameterDirection.Input, value);
            }

            return this.CreateParameter(command, "p_url", DbType.String, ParameterDirection.Input, DBNull.Value);
        }
    }
}