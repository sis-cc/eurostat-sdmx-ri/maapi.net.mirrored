﻿// -----------------------------------------------------------------------
// <copyright file="InsertMetadataflow.cs" company="EUROSTAT">
//   Date Created : 2017-04-13
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    ///     This class contains the stored procedure INSERT_METADATAFLOW name and parameter names and types
    /// </summary>
    public class InsertMetadataflow : ArtefactProcedurebase
    {
        /// <summary>
        ///     Gets the INSERT_METADATAFLOW stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_METADATAFLOW";

        /// <summary>
        ///     Gets the p_msd_id parameter name
        /// </summary>
        public const string MsdIdParameterName = "p_msd_id";

        /// <summary>
        /// Initializes a new instance of the <see cref="InsertMetadataflow"/> class.
        /// </summary>
        public InsertMetadataflow()
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Gets the p_msd_id parameter type
        /// </summary>
        public static DbType MsdIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        /// Creates the parameter for the specified <paramref name="command"/> . The parameter is added
        ///     <paramref name="command"/>
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        /// a new instance of database parameter
        /// </returns>
        public DbParameter CreateMsdIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, MsdIdParameterName, MsdIdType, ParameterDirection.Input);
        }
    }
}