// -----------------------------------------------------------------------
// <copyright file="InsertHlevel.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure;

    /// <summary>
    ///     This class contains the stored procedure INSERT_HLEVEL name and parameter names and types
    /// </summary>
    public class InsertHlevel : InsertOtherNameable
    {
        /// <summary>
        ///     Gets the p_h_id parameter name
        /// </summary>
        public const string HIdParameterName = "p_h_id";

        /// <summary>
        ///     Gets the INSERT_HLEVEL stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_HLEVEL";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertHlevel" /> class.
        /// </summary>
        public InsertHlevel() : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Gets the p_h_id parameter type
        /// </summary>
        public static DbType HIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateParentPathParam(command);
            return command;
        }

        /// <summary>
        ///     Creates the <c>p_h_id</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The database command
        /// </param>
        /// <returns>
        ///     a new instance of database Parameter for <c>p_h_id</c> with type integer
        /// </returns>
        public DbParameter CreateHIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, HIdParameterName, HIdType, ParameterDirection.Input);
        }
    }
}