// -----------------------------------------------------------------------
// <copyright file="InsertLocalisedStringOther.cs" company="EUROSTAT">
//   Date Created : 2022-06-25
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     This class contains the stored procedure INSERT_LOCALISED_STRING_OTHER name and parameter names and types
    /// </summary>
    public class InsertLocalisedStringOther : OutputProcedureBase
    {
        /// <summary>
        ///     Gets the INSERT_LOCALISED_STRING_OTHER stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_LOCALISED_STRING_OTHER";

        #region parameter names

        /// <summary>
        ///     Gets the p_art_id parameter name
        /// </summary>
        public const string OtherIdParameterName = "p_oth_id";

        /// <summary>
        ///     Gets the p_language parameter name
        /// </summary>
        public const string LanguageParameterName = "p_language";

        /// <summary>
        ///     Gets the p_text parameter name
        /// </summary>
        public const string TextParameterName = "p_text";

        /// <summary>
        ///     Gets the p_is_name parameter name
        /// </summary>
        public const string IsNameParameterName = "p_is_name";

        #endregion

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertLocalisedStringOther" /> class.
        /// </summary>
        public InsertLocalisedStringOther()
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateOtherIdParameter(command);
            this.CreateTextParameter(command);
            this.CreateLanguageParameter(command).Value = "en";
            this.CreateIsNameParameter(command).Value = true;

            return command;
        }

        #region parameter types

        /// <summary>
        ///     Gets the p_oth_id parameter type
        /// </summary>
        public static DbType OtherIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the p_language parameter type
        /// </summary>
        public static DbType LanguageType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the p_text parameter type
        /// </summary>
        public static DbType TextType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the p_is_name parameter type
        /// </summary>
        public static DbType IsNameType
        {
            get
            {
                // The actual type in msdb is byte. but C# can convert between the two types.
                return DbType.Int16;
            }
        }

        #endregion

        #region create command parameters

        /// <summary>
        ///     Creates the <c>p_oth_id</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for <c>p_oth_id</c> with type string
        /// </returns>
        public DbParameter CreateOtherIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, OtherIdParameterName, OtherIdType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the <c>p_language</c> parameter for the specified <paramref name="command" />. The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for <c>p_language</c> with type String
        /// </returns>
        public DbParameter CreateLanguageParameter(DbCommand command)
        {
            return this.CreateParameter(command, LanguageParameterName, LanguageType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the <c>p_text</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for <c>p_text</c> with type String
        /// </returns>
        public DbParameter CreateTextParameter(DbCommand command)
        {
            return this.CreateParameter(command, TextParameterName, TextType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the <c>p_is_name</c> parameter for the specified <paramref name="command" />. The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for <c>p_is_name</c> with type boolean
        /// </returns>
        public DbParameter CreateIsNameParameter(DbCommand command)
        {
            return this.CreateParameter(command, IsNameParameterName, IsNameType, ParameterDirection.Input);
        }

        #endregion
    }
}