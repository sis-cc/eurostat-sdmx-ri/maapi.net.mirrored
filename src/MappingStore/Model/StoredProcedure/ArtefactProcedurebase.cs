// -----------------------------------------------------------------------
// <copyright file="ArtefactProcedurebase.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     This is the base class for ARTEFACT based stored procedures, and contains the common parameter names and types
    /// </summary>
    public abstract class ArtefactProcedurebase : IdentifiableProcedureBase, IArtefactProcedure
    {
        #region Parameter names

        /// <summary>
        ///     Gets the p_agency parameter name
        /// </summary>
        public const string AgencyParameterName = "p_agency";

        /// <summary>
        ///     Gets the p_is_final parameter name
        /// </summary>
        public const string IsFinalParameterName = "p_is_final";

        /// <summary>
        ///     Gets the p_last_modified parameter name
        /// </summary>
        public const string LastModifiedParameterName = "p_last_modified";

        /// <summary>
        ///     Gets the p_uri parameter name
        /// </summary>
        public const string UriParameterName = "p_uri";

        /// <summary>
        ///     Gets the p_valid_from parameter name
        /// </summary>
        public const string ValidFromParameterName = "p_valid_from";

        /// <summary>
        ///     Gets the p_valid_to parameter name
        /// </summary>
        public const string ValidToParameterName = "p_valid_to";

        /// <summary>
        ///     Gets the p_version parameter name
        /// </summary>
        public const string VersionParameterName = "p_version";

        /// <summary>
        ///     Gets the p_sdmx_schema parameter name
        /// </summary>
        public const string SdmxSchemaParameterName = "p_sdmx_schema";

        /// <summary>
        ///     Gets the p_is_stub parameter name
        /// </summary>
        public const string IsStubParameterName = "p_is_stub";

        /// <summary>
        ///     Gets the p_service_url parameter name
        /// </summary>
        public const string ServiceUrlParameterName = "p_service_url";

        /// <summary>
        ///     Gets the p_structure_url parameter name
        /// </summary>
        public const string StructureUrlParameterName = "p_structure_url";

        /// <summary>
        ///     Gets the p_artefact_type parameter name
        /// </summary>
        public const string ArtefactTypeParameterName = "p_artefact_type";

        #endregion

        /// <summary>
        ///     Initializes a new instance of the <see cref="ArtefactProcedurebase" /> class.
        /// </summary>
        /// <param name="procedureName">
        ///     The procedure name.
        /// </param>
        protected ArtefactProcedurebase(string procedureName)
            : base(procedureName)
        {
        }

        #region parameter types

        /// <summary>
        ///     Gets the p_agency parameter type
        /// </summary>
        public static DbType AgencyType
        {
            get
            {
                return DbType.AnsiString;
            }
        }

        /// <summary>
        ///     Gets the p_is_final parameter type
        /// </summary>
        public static DbType IsFinalType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary>
        ///     Gets the p_last_modified parameter type
        /// </summary>
        public static DbType LastModifiedType
        {
            get
            {
                return DbType.DateTime;
            }
        }

        /// <summary>
        ///     Gets the p_uri parameter type
        /// </summary>
        public static DbType UriType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the p_valid_from parameter type
        /// </summary>
        public static DbType ValidFromType
        {
            get
            {
                return DbType.DateTime;
            }
        }

        /// <summary>
        ///     Gets the p_valid_to parameter type
        /// </summary>
        public static DbType ValidToType
        {
            get
            {
                return DbType.DateTime;
            }
        }

        /// <summary>
        ///     Gets the p_version parameter type
        /// </summary>
        public static DbType VersionType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the p_sdmx_schema parameter type
        /// </summary>
        public static DbType SdmxSchemaType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary>
        ///     Gets the p_is_stub parameter type
        /// </summary>
        public static DbType IsStubType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary>
        ///     Gets the p_service_url parameter type
        /// </summary>
        public static DbType ServiceUrlType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the p_structure_url parameter type
        /// </summary>
        public static DbType StructureUrlType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the p_artefact_type parameter type
        /// </summary>
        public static DbType ArtefactTypeType
        {
            get
            {
                return DbType.String;
            }
        }

        #endregion

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateAgencyParameter(command);
            this.CreateIsFinalParameter(command);
            this.CreateLastModifiedParameter(command).Value = DateTime.UtcNow;
            this.CreateUriParameter(command);
            this.CreateValidFromParameter(command);
            this.CreateValidToParameter(command);
            this.CreateVersionParameter(command).Value = "1.0";
            this.CreateSdmxSchemaParameter(command).Value = 0;
            return command;
        }

        #region Create parameters

        /// <summary>
        ///     Creates the p_agency parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        public DbParameter CreateAgencyParameter(DbCommand command)
        {
            return this.CreateParameter(command, AgencyParameterName, AgencyType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the p_is_final parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        public DbParameter CreateIsFinalParameter(DbCommand command)
        {
            return this.CreateParameter(command, IsFinalParameterName, IsFinalType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the p_uri parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        public DbParameter CreateLastModifiedParameter(DbCommand command)
        {
            return this.CreateParameter(command, LastModifiedParameterName, LastModifiedType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the p_uri parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        public DbParameter CreateUriParameter(DbCommand command)
        {
            return this.CreateParameter(command, UriParameterName, UriType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the p_valid_from parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        public DbParameter CreateValidFromParameter(DbCommand command)
        {
            return this.CreateParameter(command, ValidFromParameterName, ValidFromType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the p_valid_to parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        public DbParameter CreateValidToParameter(DbCommand command)
        {
            return this.CreateParameter(command, ValidToParameterName, ValidToType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the p_version parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        public DbParameter CreateVersionParameter(DbCommand command)
        {
            return this.CreateParameter(command, VersionParameterName, VersionType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the p_sdmx_schema parameter for the specified <paramref name="command" />. 
        /// The parameter is added to <paramref name="command" />.
        /// </summary>
        /// <param name="command">The db Command to add the parameter to.</param>
        /// <returns>A new instance of the parameter</returns>
        public DbParameter CreateSdmxSchemaParameter(DbCommand command)
        {
            return this.CreateParameter(command, SdmxSchemaParameterName, SdmxSchemaType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the p_is_stub parameter for the specified <paramref name="command"/>. 
        /// The parameter is added to <paramref name="command"/>.
        /// </summary>
        /// <param name="command">The db command to add the parameter to.</param>
        /// <returns>A new instance of the parameter</returns>
        public DbParameter CreateIsStubParameter(DbCommand command)
        {
            return this.CreateParameter(command, IsStubParameterName, IsStubType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the p_service_url parameter for the specified <paramref name="command"/>.
        /// The parameter is added to <paramref name="command"/>.
        /// </summary>
        /// <param name="command">The db command to add the parameter to.</param>
        /// <returns>A new instance of the parameter.</returns>
        /// <exception cref="NotImplementedException"></exception>
        public DbParameter CreateServiceUrlParameter(DbCommand command)
        {
            return this.CreateParameter(command, ServiceUrlParameterName, ServiceUrlType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the p_structure_url parameter for the specified <paramref name="command"/>.
        /// The parameter is added to <paramref name="command"/>.
        /// </summary>
        /// <param name="command">The db command to add the parameter to.</param>
        /// <returns>A new instance of the parameter.</returns>
        /// <exception cref="NotImplementedException"></exception>
        public DbParameter CreateStructureUrlParameter(DbCommand command)
        {
            return this.CreateParameter(command, StructureUrlParameterName, StructureUrlType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the p_artefact_type parameter for the specified <paramref name="command"/>.
        /// The parameter is added to <paramref name="command"/>.
        /// </summary>
        /// <param name="command">The db command to add the parameter to.</param>
        /// <returns>A new instance of the parameter.</returns>
        /// <exception cref="NotImplementedException"></exception>
        public DbParameter CreateArtefactTypeParameter(DbCommand command)
        {
            return this.CreateParameter(command, ArtefactTypeParameterName, ArtefactTypeType, ParameterDirection.Input);
        }

        #endregion

        /// <summary>
        /// Indicates whether this artefact type supports stub artefacts or not.
        /// Defaults to <c>false</c>.
        /// </summary>
        /// <returns><c>true</c> if supports stubs, <c>false</c> otherwise.</returns>
        public virtual bool SupportsStub()
        {
            return false;
        }
    }
}