using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using System.Text;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure
{
    public class InsertItemJSONProcedure: OutputProcedureBase
    {
        /// <summary>
        ///     Gets the p_json parameter name
        /// </summary>
        public const string JSONParameterName = "p_json";

        /// <summary>
        ///     Gets the INSERT_LOCALISED_STRING stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_ITEM_JSON";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertItemJSONProcedure" /> class.
        /// </summary>
        public InsertItemJSONProcedure()
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Creates the <c>p_language</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     a new instance of <c>DbParameter</c> for <c>p_language</c> with type <c>DbType.String </c>
        /// </returns>
        public DbParameter CreateJSONParameter(DbCommand command)
        {
            return this.CreateParameter(command, JSONParameterName, DbType.String, ParameterDirection.Input);
        }

    }
}
