// -----------------------------------------------------------------------
// <copyright file="MapProcedureBase.cs" company="EUROSTAT">
//   Date Created : 2014-10-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     The base stored procedure class for structure set map items.
    /// </summary>
    public abstract class MapProcedureBase : IMapProcedureBase
    {
        /// <summary>
        ///     The _output procedure
        /// </summary>
        private readonly IOutputProcedure _outputProcedure;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MapProcedureBase" /> class.
        /// </summary>
        /// <param name="outputProcedure">The output procedure.</param>
        protected MapProcedureBase(IOutputProcedure outputProcedure)
        {
            this._outputProcedure = outputProcedure;
        }

        /// <summary>
        ///     Gets the name of the parent (another map or a structure set) parameter.
        /// </summary>
        /// <value>
        ///     The name of the scheme parameter.
        /// </value>
        public abstract string ParentIdParameterName { get; }

        /// <summary>
        ///     Gets the name of the source identifier parameter.
        /// </summary>
        /// <value>
        ///     The name of the source identifier parameter.
        /// </value>
        public abstract string SourceIdParameterName { get; }

        /// <summary>
        ///     Gets the name of the target identifier parameter.
        /// </summary>
        /// <value>
        ///     The name of the source identifier parameter.
        /// </value>
        public abstract string TargetIdParameterName { get; }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>
        ///     The <paramref name="database" /> instance may or may not be transactional.
        /// </remarks>
        public DbCommand CreateCommand(Database database)
        {
            return this._outputProcedure.CreateCommand(database);
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="connection" /> and <paramref name="transaction" />
        /// </summary>
        /// <param name="connection">
        ///     The connection
        /// </param>
        /// <param name="transaction">
        ///     The transaction; otherwise set to null
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="connection" /> and <paramref name="transaction" />
        /// </returns>
        /// <remarks>
        ///     It sets the following <see cref="DbCommand" /> properties <see cref="DbCommand.Connection" /> ,
        ///     <see cref="DbCommand.Transaction" /> , <see cref="DbCommand.CommandType" /> and
        ///     <see cref="DbCommand.CommandText" />
        /// </remarks>
        public DbCommand CreateCommand(DbConnection connection, DbTransaction transaction)
        {
            return this._outputProcedure.CreateCommand(connection, transaction);
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>
        ///     The <paramref name="database" /> instance may or may not be transactional.
        /// </remarks>
        public DbCommand CreateCommandWithDefaults(Database database)
        {
            return this._outputProcedure.CreateCommandWithDefaults(database);
        }

        /// <summary>
        ///     Creates the <c>'p_pk'</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     The parameter for <c>'p_pk'</c> parameter
        /// </returns>
        public DbParameter CreateOutputParameter(DbCommand command)
        {
            return this._outputProcedure.CreateOutputParameter(command);
        }

        /// <summary>
        ///     Create parameter for the specified <paramref name="command" /> and add it to the command
        /// </summary>
        /// <param name="command">
        ///     The command
        /// </param>
        /// <param name="name">
        ///     The parameter name
        /// </param>
        /// <param name="dbType">
        ///     The type
        /// </param>
        /// <param name="direction">
        ///     The direction
        /// </param>
        /// <returns>
        ///     The new <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateParameter(DbCommand command, string name, DbType dbType, ParameterDirection direction)
        {
            return this._outputProcedure.CreateParameter(command, name, dbType, direction);
        }

        /// <summary>
        ///     Create parameter for the specified <paramref name="command" /> and add it to the command
        /// </summary>
        /// <typeparam name="T">
        ///     The parameter type.
        /// </typeparam>
        /// <param name="command">
        ///     The command
        /// </param>
        /// <param name="name">
        ///     The parameter name
        /// </param>
        /// <param name="dbType">
        ///     The type
        /// </param>
        /// <param name="direction">
        ///     The direction
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The new <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateParameter<T>(DbCommand command, string name, DbType dbType, ParameterDirection direction, T value)
        {
            return this._outputProcedure.CreateParameter(command, name, dbType, direction, value);
        }

        /// <summary>
        ///     Returns the parent (another map or a structure set) identifier parameter of the specified
        ///     <paramref name="command" />.
        ///     If it does not exist it will add it.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The input <see cref="DbParameter" /> of type <see cref="DbType.Int64" /> and name
        ///     <see cref="ParentIdParameterName" />
        /// </returns>
        public virtual DbParameter CreateParentIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, this.ParentIdParameterName, DbType.Int64, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the source identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The input <see cref="DbParameter" /> of type <see cref="DbType.Int64" /> and name
        ///     <see cref="SourceIdParameterName" />
        /// </returns>
        public virtual DbParameter CreateSourceIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, this.SourceIdParameterName, DbType.Int64, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the source identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The input <see cref="DbParameter" /> of type <see cref="DbType.Int64" /> and name
        ///     <see cref="SourceIdParameterName" />
        /// </returns>
        public virtual DbParameter CreateTargetIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, this.TargetIdParameterName, DbType.Int64, ParameterDirection.Input);
        }
    }
}