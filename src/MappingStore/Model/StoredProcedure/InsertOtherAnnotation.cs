// -----------------------------------------------------------------------
// <copyright file="OtherAnnotationInsertEngine.cs" company="EUROSTAT">
//   Date Created : 2022-06-30
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Text;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    public class InsertOtherAnnotation : AnnotationProcedureBase
    {
        private readonly string OtherIdParameterName = "p_oth_id";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertOtherAnnotation" /> class.
        /// </summary>
        public InsertOtherAnnotation()
            : base("INSERT_OTHER_ANNOTATION")
        {
        }

        public override string ParentIdParameterName => throw new NotImplementedException();

        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            CreateOtherIdParameter(command);
            return command;
        }

        public DbParameter CreateOtherIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, OtherIdParameterName, DbType.Int64, ParameterDirection.Input);
        }
    }
}
