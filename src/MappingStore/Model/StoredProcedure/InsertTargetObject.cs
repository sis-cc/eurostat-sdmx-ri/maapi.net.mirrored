// -----------------------------------------------------------------------
// <copyright file="InsertTargetObject.cs" company="EUROSTAT">
//   Date Created : 2017-04-13
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// The insert target object.
    /// </summary>
    public class InsertTargetObject : IdentifiableProcedureBase
    {
        /// <summary> The <c>p_MDT_ID</c> parameter name </summary>
        public const string MdtIDParameterName = "p_MDT_ID";

        /// <summary>
        ///     Gets the INSERT_COMPONENT stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_TARGET_OBJECT";

        /// <summary> The <c>p_TYPE</c> parameter name </summary>
        public const string TypeParameterName = "p_TYPE";

        /// <summary>
        /// Initializes a new instance of the <see cref="InsertTargetObject"/> class.
        /// </summary>
        public InsertTargetObject()
            : base(StoredProcedureName)
        {
        }

        /// <summary> Gets the type of <c>p_MDT_ID</c> parameter </summary>
        public static DbType MdtIDType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary> Gets the type of <c>p_TYPE</c> parameter </summary>
        public static DbType TypeType
        {
            get
            {
                return DbType.AnsiString;
            }
        }

        /// <summary>
        /// Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        /// <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        /// a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        /// <paramref name="database" />.
        /// </returns>
        /// <remarks>
        /// The <paramref name="database" /> instance may or may not be transactional.
        /// </remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
         
            return command;
        }

        /// <summary>
        /// Creates the parameter for <c>p_MDT_ID</c>and adds it to the specified <paramref name="command"/>.
        /// </summary>
        /// <param name="command">
        /// The database command.
        /// </param>
        /// <returns>
        /// A new instance of the database parameter
        /// </returns>
        public DbParameter CreateMdtIDParameter(DbCommand command)
        {
            return this.CreateParameter(command, MdtIDParameterName, MdtIDType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter for <c>p_TYPE</c>and adds it to the specified <paramref name="command"/>.
        /// </summary>
        /// <param name="command">
        /// The database command.
        /// </param>
        /// <returns>
        /// A new instance of the database parameter
        /// </returns>
        public DbParameter CreateTypeParameter(DbCommand command)
        {
            return this.CreateParameter(command, TypeParameterName, TypeType, ParameterDirection.Input);
        }
    }
}