// -----------------------------------------------------------------------
// <copyright file="InsertDataConsumer.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    /// <summary>
    ///     This class contains the stored procedure INSERT_DATACONSUMER name and parameter names and types
    /// </summary>
    public class InsertDataConsumer : ItemProcedureBase
    {
        /// <summary>
        ///     Gets the <c>p_datacons_sch_id</c> parameter name
        /// </summary>
        public const string SchemeIdParameterName = "p_datacons_sch_id";

        /// <summary>
        ///     Gets the INSERT_DATACONSUMER stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_DATACONSUMER";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertDataConsumer" /> class.
        /// </summary>
        public InsertDataConsumer()
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Gets the name of the scheme parameter.
        /// </summary>
        /// <value>
        ///     The name of the scheme parameter.
        /// </value>
        public override string SchemeParameterName
        {
            get
            {
                return SchemeIdParameterName;
            }
        }
    }
}