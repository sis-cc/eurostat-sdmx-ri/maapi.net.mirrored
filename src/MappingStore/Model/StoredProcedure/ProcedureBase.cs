// -----------------------------------------------------------------------
// <copyright file="ProcedureBase.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Reflection;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     The base class for classes that map to procedures in Mapping Store Scheme
    /// </summary>
    public class ProcedureBase : IProcedure
    {
        /// <summary>
        ///     The _procedure name
        /// </summary>
        private readonly string _procedureName;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcedureBase" /> class.
        /// </summary>
        /// <param name="procedureName">Name of the procedure.</param>
        public ProcedureBase(string procedureName)
        {
            this._procedureName = procedureName;
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>
        ///     The <paramref name="database" /> instance may or may not be transactional.
        /// </remarks>
        public DbCommand CreateCommand(Database database)
        {
            return database.CreateCommand(CommandType.StoredProcedure, this._procedureName);
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="connection" /> and <paramref name="transaction" />
        /// </summary>
        /// <param name="connection">
        ///     The connection
        /// </param>
        /// <param name="transaction">
        ///     The transaction; otherwise set to null
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="connection" /> and <paramref name="transaction" />
        /// </returns>
        /// <remarks>
        ///     It sets the following <see cref="DbCommand" /> properties <see cref="DbCommand.Connection" /> ,
        ///     <see cref="DbCommand.Transaction" /> , <see cref="DbCommand.CommandType" /> and
        ///     <see cref="DbCommand.CommandText" />
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "False positive. No SQL text is provided here.")]
        public DbCommand CreateCommand(DbConnection connection, DbTransaction transaction)
        {
            DbCommand command = connection.CreateCommand();

            // Oracle ODP.NET driver by default expects parameters in a specific order
            try
            {
                var propertyInfo = command.GetType().GetProperty("BindByName");

                if (propertyInfo != null)
                {
                    propertyInfo.SetValue(command, true, null);
                }
            }
            catch (AmbiguousMatchException)
            {
            }

            if (transaction != null)
            {
                command.Transaction = transaction;
            }

            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = this._procedureName;
            return command;
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>
        ///     The <paramref name="database" /> instance may or may not be transactional.
        /// </remarks>
        public virtual DbCommand CreateCommandWithDefaults(Database database)
        {
            return this.CreateCommand(database);
        }

        /// <summary>
        ///     Create parameter for the specified <paramref name="command" /> and add it to the command
        /// </summary>
        /// <param name="command">
        ///     The command
        /// </param>
        /// <param name="name">
        ///     The parameter name
        /// </param>
        /// <param name="dbType">
        ///     The type
        /// </param>
        /// <param name="direction">
        ///     The direction
        /// </param>
        /// <returns>
        ///     The new <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateParameter(DbCommand command, string name, DbType dbType, ParameterDirection direction)
        {
            DbParameter parameter;
            if (command.Parameters.Contains(name))
            {
                parameter = command.Parameters[name];
            }
            else
            {
                parameter = command.CreateParameter();
                parameter.ParameterName = name;
                parameter.DbType = dbType;
                parameter.Direction = direction;
                command.Parameters.Add(parameter);
            }

            return parameter;
        }

        /// <summary>
        ///     Create parameter for the specified <paramref name="command" /> and add it to the command
        /// </summary>
        /// <typeparam name="T">The parameter type.</typeparam>
        /// <param name="command">The command</param>
        /// <param name="name">The parameter name</param>
        /// <param name="dbType">The type</param>
        /// <param name="direction">The direction</param>
        /// <param name="value">The value.</param>
        /// <returns>
        ///     The new <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateParameter<T>(DbCommand command, string name, DbType dbType, ParameterDirection direction, T value)
        {
            var parameter = this.CreateParameter(command, name, dbType, direction);
            if (value != null)
            {
                parameter.Value = value;
            }
            else
            {
                parameter.Value = DBNull.Value;
            }
            return parameter;
        }
    }
}