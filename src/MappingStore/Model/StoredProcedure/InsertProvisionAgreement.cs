// -----------------------------------------------------------------------
// <copyright file="InsertProvisionAgreement.cs" company="EUROSTAT">
//   Date Created : 2017-04-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;
    using Manager;

    /// <summary>
    /// This class contains the stored procedure INSERT_PROVISION_AGREEMENT name and parameter names and types
    /// </summary>
    /// <seealso cref="Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure.ArtefactProcedurebase" />
    public class InsertProvisionAgreement : ArtefactProcedurebase
    {
        /// <summary>
        /// The stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_PROVISION_AGREEMENT";

        /// <summary>
        ///     Gets the p_dsd_id parameter name
        /// </summary>
        public const string StructureUsageParameterName = "p_su_id";

        /// <summary>
        ///     Gets the p_map_set_id parameter name
        /// </summary>
        public const string DataProviderParameterName = "p_dp_id";

        /// <summary>
        ///     Initializes a new instance of the <see cref="ArtefactProcedurebase" /> class.
        /// </summary>
        public InsertProvisionAgreement()
            : base(StoredProcedureName)
        {
        }


        /// <summary>
        /// Gets the type of the structure usage parameter.
        /// </summary>
        /// <value>
        /// The type of the structure usage parameter.
        /// </value>
        public static DbType StructureUsageParameterType
        {
            get
            {
                return DbType.Int64;
            }
        }


        /// <summary>
        /// Gets the type of the data provider parameter.
        /// </summary>
        /// <value>
        /// The type of the data provider parameter.
        /// </value>
        public static DbType DataProviderParameterType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The command
        /// </param>
        /// <returns>
        ///     a new instance of Parameter
        /// </returns>
        public DbParameter CreateDataProviderParameter(DbCommand command)
        {
            return this.CreateParameter(command, DataProviderParameterName, DataProviderParameterType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The command
        /// </param>
        /// <returns>
        ///     a new instance of Parameter
        /// </returns>
        public DbParameter CreateStructureUsageParameter(DbCommand command)
        {
            return this.CreateParameter(command, StructureUsageParameterName, StructureUsageParameterType, ParameterDirection.Input);
        }
    }
}