// -----------------------------------------------------------------------
// <copyright file="InsertTranscodingScript.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    ///     This class contains the stored procedure INSERT_TRANSCODING_SCRIPT name and parameter names and types
    /// </summary>
    public class InsertTranscodingScript : OutputProcedureBase
    {
        /// <summary>
        ///     Gets the p_content parameter name
        /// </summary>
        public const string ContentParameterName = "p_content";

        /// <summary>
        ///     Gets the INSERT_TRANSCODING_SCRIPT stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_TRANSCODING_SCRIPT";

        /// <summary>
        ///     Gets the <c>p_title</c> parameter name
        /// </summary>
        public const string TitleParameterName = "p_title";

        /// <summary>
        ///     Gets the <c>p_tr_id</c> parameter name
        /// </summary>
        public const string TrIdParameterName = "p_tr_id";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertTranscodingScript" /> class.
        /// </summary>
        public InsertTranscodingScript()
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Gets the p_content parameter type
        /// </summary>
        public static DbType ContentType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the p_title parameter type
        /// </summary>
        public static DbType TitleType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the <c>p_tr_id</c> parameter type
        /// </summary>
        public static DbType TrIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Creates the p_content parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for p_content with type string
        /// </returns>
        public DbParameter CreateContentParameter(DbCommand command)
        {
            return this.CreateParameter(command, ContentParameterName, ContentType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the p_title parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for p_title with type string
        /// </returns>
        public DbParameter CreateTitleParameter(DbCommand command)
        {
            return this.CreateParameter(command, TitleParameterName, TitleType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the <c>p_tr_id</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for <c>p_tr_id</c> with type integer
        /// </returns>
        public DbParameter CreateTrIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, TrIdParameterName, TrIdType, ParameterDirection.Input);
        }
    }
}