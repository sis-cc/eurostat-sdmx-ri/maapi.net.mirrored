// -----------------------------------------------------------------------
// <copyright file="InsertConcept.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     This class contains the stored procedure INSERT_CONCEPT name and parameter names and types
    /// </summary>
    public class InsertConcept : HierarchicalItemProcedureBase
    {
        /// <summary>
        ///     Gets the <c>p_con_sch_id</c> parameter name
        /// </summary>
        public const string ConSchIdParameterName = "p_con_sch_id";

        /// <summary>
        ///     Gets the <c>p_con_sch_id</c> parameter name
        /// </summary>
        public const string ParentConIdParameterName = "p_parent_con_id";

        /// <summary>
        /// The representation parameter name
        /// </summary>
        public const string RepresentationParameterName = "p_cl_id";

        /// <summary>
        ///     Gets the INSERT_CONCEPT stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_CONCEPT";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertConcept" /> class.
        /// </summary>
        public InsertConcept()
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Gets the <c>p_cl_id</c> parameter type
        /// </summary>
        public static DbType RepresentationIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the name of the parent item parameter.
        /// </summary>
        /// <value>
        ///     The name of the parent item parameter.
        /// </value>
        public override string ParentItemParameterName
        {
            get
            {
                return ParentConIdParameterName;
            }
        }

        /// <summary>
        ///     Gets the name of the scheme parameter.
        /// </summary>
        /// <value>
        ///     The name of the scheme parameter.
        /// </value>
        public override string SchemeParameterName
        {
            get
            {
                return ConSchIdParameterName;
            }
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateRepresentationIdParameter(command).Value = DBNull.Value;
            return command;
        }

        /// <summary>
        /// Creates the representation identifier parameter.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <returns>
        /// The <see cref="DbParameter"/>.
        /// </returns>
        public DbParameter CreateRepresentationIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, RepresentationParameterName, RepresentationIdType, ParameterDirection.Input);
        }
    }
}