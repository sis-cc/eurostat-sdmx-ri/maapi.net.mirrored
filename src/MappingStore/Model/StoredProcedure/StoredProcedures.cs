// -----------------------------------------------------------------------
// <copyright file="StoredProcedures.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure;

namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using Estat.Sri.MappingStoreRetrieval.Config;

    /// <summary>
    ///     This class contains the list of known stored procedures in MSDB
    /// </summary>
    public class StoredProcedures
    {
        /// <summary>
        ///     Gets the insert agency
        /// </summary>
        /// <value>
        ///     The insert agency
        /// </value>
        public InsertAgency InsertAgency
        {
            get
            {
                return new InsertAgency();
            }
        }

        /// <summary>
        ///     Gets the insert agency scheme.
        /// </summary>
        /// <value>
        ///     The insert agency scheme.
        /// </value>
        public InsertAgencyScheme InsertAgencyScheme
        {
            get
            {
                return new InsertAgencyScheme();
            }
        }

        /// <summary>
        ///     Gets the insert annotation Text procedure.
        /// </summary>
        /// <value>
        ///     The insert annotation Text.
        /// </value>
        public InsertAnnotationText InsertAnnotationText
        {
            get
            {
                return new InsertAnnotationText();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertArtefact" /> stored procedure constants
        /// </summary>
        public InsertArtefact InsertArtefact
        {
            get
            {
                return new InsertArtefact();
            }
        }

        /// <summary>
        ///     Gets the insert annotation artefact.
        /// </summary>
        /// <value>
        ///     The insert annotation artefact.
        /// </value>
        public InsertArtefactAnnotation InsertArtefactAnnotation
        {
            get
            {
                return new InsertArtefactAnnotation();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertCategorisation" /> stored procedure constants
        /// </summary>
        public InsertCategorisation InsertCategorisation
        {
            get
            {
                return new InsertCategorisation();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertCategory" /> stored procedure constants
        /// </summary>
        public InsertCategory InsertCategory
        {
            get
            {
                return new InsertCategory();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertCategoryScheme" /> stored procedure constants
        /// </summary>
        public InsertCategoryScheme InsertCategoryScheme
        {
            get
            {
                return new InsertCategoryScheme();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertCodelist" /> stored procedure constants
        /// </summary>
        public InsertCodelist InsertCodelist
        {
            get
            {
                return new InsertCodelist();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertComponent" /> stored procedure constants
        /// </summary>
        public InsertComponent InsertComponent
        {
            get
            {
                return new InsertComponent();
            }
        }

        /// <summary>
        ///     Gets the insert annotation Component procedure.
        /// </summary>
        /// <value>
        ///     The insert annotation Component.
        /// </value>
        public InsertComponentAnnotation InsertComponentAnnotation
        {
            get
            {
                return new InsertComponentAnnotation();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertConcept" /> stored procedure constants
        /// </summary>
        public InsertConcept InsertConcept
        {
            get
            {
                return new InsertConcept();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertConceptScheme" /> stored procedure constants
        /// </summary>
        [Obsolete("Not in MSDB 7.0")]
        public InsertConceptScheme InsertConceptScheme
        {
            get
            {
                return new InsertConceptScheme();
            }
        }

        /// <summary>
        ///     Gets the SP for inserting Structure References
        /// </summary>
        public InsertStructureReference InsertStructureReference
        {
            get
            {
                return new InsertStructureReference();
            }
        }

        /// <summary>
        ///     Gets the SP for inserting Reference Source
        /// </summary>
        public InsertReferenceSource InsertReferenceSource
        {
            get
            {
                return new InsertReferenceSource();
            }
        }

        /// <summary>
        ///     Gets the data consumer
        /// </summary>
        public InsertDataConsumer InsertDataConsumer
        {
            get
            {
                return new InsertDataConsumer();
            }
        }

        /// <summary>
        ///     Gets the data consumer scheme
        /// </summary>
        public InsertDataConsumerScheme InsertDataConsumerScheme
        {
            get
            {
                return new InsertDataConsumerScheme();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertDataflow" /> stored procedure constants
        /// </summary>
        public InsertDataflow InsertDataflow
        {
            get
            {
                return new InsertDataflow();
            }
        }

        /// <summary>
        ///     Gets the data provider
        /// </summary>
        public InsertDataProvider InsertDataProvider
        {
            get
            {
                return new InsertDataProvider();
            }
        }

        /// <summary>
        ///     Gets the data provider scheme
        /// </summary>
        public InsertDataProviderScheme InsertDataProviderScheme
        {
            get
            {
                return new InsertDataProviderScheme();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertDsd" /> stored procedure constants
        /// </summary>
        public InsertDsd InsertDsd
        {
            get
            {
                return new InsertDsd();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertDsdCode" /> stored procedure constants
        /// </summary>
        public InsertDsdCode InsertDsdCode
        {
            get
            {
                return new InsertDsdCode();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertDsdGroup" /> stored procedure constants
        /// </summary>
        public InsertDsdGroup InsertDsdGroup
        {
            get
            {
                return new InsertDsdGroup();
            }
        }

        /// <summary>
        ///     Gets the insert annotation Group procedure.
        /// </summary>
        /// <value>
        ///     The insert annotation Group.
        /// </value>
        public InsertGroupAnnotation InsertGroupAnnotation
        {
            get
            {
                return new InsertGroupAnnotation();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertHcl" /> stored procedure constants
        /// </summary>
        public InsertHcl InsertHcl
        {
            get
            {
                return new InsertHcl();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertHclCode" /> stored procedure constants
        /// </summary>
        public InsertHclCode InsertHclCode
        {
            get
            {
                return new InsertHclCode();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertHierachy" /> stored procedure constants
        /// </summary>
        public InsertHierachy InsertHierachy
        {
            get
            {
                return new InsertHierachy();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertHlevel" /> stored procedure constants
        /// </summary>
        public InsertHlevel InsertHlevel
        {
            get
            {
                return new InsertHlevel();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertItem" /> stored procedure constants
        /// </summary>
        public InsertItem InsertItem
        {
            get
            {
                return new InsertItem();
            }
        }

        /// <summary>
        ///     Gets the insert annotation item.
        /// </summary>
        /// <value>
        ///     The insert annotation item.
        /// </value>
        public InsertItemAnnotation InsertItemAnnotation
        {
            get
            {
                return new InsertItemAnnotation();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertLocalCode" /> stored procedure constants
        /// </summary>
        public InsertLocalCode InsertLocalCode
        {
            get
            {
                return new InsertLocalCode();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertLocalisedString" /> stored procedure constants
        /// </summary>
        public InsertLocalisedString InsertLocalisedString
        {
            get
            {
                return new InsertLocalisedString();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertLocalisedStringOther" /> stored procedure constants
        /// </summary>
        public InsertLocalisedStringOther InsertLocalisedStringOther
        {
            get
            {
                return new InsertLocalisedStringOther();
            }
        }

        /// <summary>
        /// Gets <see cref="InsertMetadataAttribute" /> stored procedure constants
        /// </summary>
        /// <value>
        /// The insert metadata attribute.
        /// </value>
        public InsertMetadataAttribute InsertMetadataAttribute
        {
            get
            {
                return new InsertMetadataAttribute();
            }
        }

        /// <summary>
        /// Gets <see cref="InsertMetadataflow" /> stored procedure constants
        /// </summary>
        public InsertMetadataflow InsertMetadataflow
        {
            get
            {
                return new InsertMetadataflow();
            }
        }

        /// <summary>
        /// Gets <see cref="InsertMetadataTarget" /> stored procedure constants
        /// </summary>
        public InsertMetadataTarget InsertMetadataTarget
        {
            get
            {
                return new InsertMetadataTarget();
            }
        }

        /// <summary>
        /// Gets the <see cref="InsertMsd" /> stored procedure constants .
        /// </summary>
        public InsertMsd InsertMsd
        {
            get
            {
                return new InsertMsd();
            }
        }

        /// <summary>
        ///     Gets the organization unit
        /// </summary>
        public InsertOrganisationUnit InsertOrganisationUnit
        {
            get
            {
                return new InsertOrganisationUnit();
            }
        }

        /// <summary>
        ///     Gets the organization unit scheme
        /// </summary>
        public InsertOrganisationUnitScheme InsertOrganisationUnitScheme
        {
            get
            {
                return new InsertOrganisationUnitScheme();
            }
        }

        /// <summary>
        /// Gets the <see cref="InsertReportStructure" /> stored procedure constants
        /// </summary>
        public InsertReportStructure InsertReportStructure
        {
            get
            {
                return new InsertReportStructure();
            }
        }

        /// <summary>
        ///     Gets the insert structure set.
        /// </summary>
        /// <value>
        ///     The insert structure set.
        /// </value>
        public InsertStructureSet InsertStructureSet
        {
            get
            {
                return new InsertStructureSet();
            }
        }

        /// <summary>
        /// Gets the <see cref="InsertTargetObject" /> stored procedure constants
        /// </summary>
        public InsertTargetObject InsertTargetObject
        {
            get
            {
                return new InsertTargetObject();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertComponentTextFormat" /> stored procedure constants
        /// </summary>
        public InsertTextFormat InsertTextFormat
        {
            get
            {
                return new InsertComponentTextFormat();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertConceptTextFormat" /> stored procedure constants
        /// </summary>
        [Obsolete("Not in MSDB 7.0")]
        public InsertConceptTextFormat InsertConceptTextFormat
        {
            get
            {
                return new InsertConceptTextFormat();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertTranscoding" /> stored procedure constants
        /// </summary>
        public InsertTranscoding InsertTranscoding
        {
            get
            {
                return new InsertTranscoding();
            }
        }

        /// <summary>
        ///     Gets the <see cref="InsertTranscodingScript" /> stored procedure constants
        /// </summary>
        public InsertTranscodingScript InsertTranscodingScript
        {
            get
            {
                return new InsertTranscodingScript();
            }
        }

        /// <summary>
        ///     Gets the UpdateTranscodingRule stored procedure constants
        /// </summary>
        public UpdateTranscodingRule UpdateTranscodingRule
        {
            get
            {
                return new UpdateTranscodingRule();
            }
        }



        /// <summary>
        /// Gets the InsertProvisionAgreement stored procedure.
        /// </summary>
        /// <value>
        /// The insert provision agreement.
        /// </value>
        public InsertProvisionAgreement InsertProvisionAgreement
        {
            get
            {
                return new InsertProvisionAgreement();
            }
        }



        /// <summary>
        /// Gets the insert data source.
        /// </summary>
        /// <value>
        /// The insert data source.
        /// </value>
        public InsertDataSource InsertDataSource
        {
            get
            {
                return new InsertDataSource();
            }
        }

        /// <summary>
        /// Gets the procedure which is responsible for processing content constraints.
        /// </summary>
        public ProcessConstraintsProcedure ProcessConstraintsProcedure
        {
            get
            {
                return new ProcessConstraintsProcedure();
            }
        }
    }
}