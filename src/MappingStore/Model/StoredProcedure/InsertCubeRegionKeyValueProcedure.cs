// -----------------------------------------------------------------------
// <copyright file="InsertCubeRegionKeyValueProcedure.cs" company="EUROSTAT">
//   Date Created : 2014-10-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.MappingStoreRetrieval.Extensions;
using Estat.Sri.MappingStoreRetrieval.Manager;

namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    ///     The insert cube region procedure.
    /// </summary>
    public class InsertCubeRegionKeyValueProcedure : OutputProcedureBase
    {
        /// <summary>
        ///     The stored procedure name
        /// </summary>
        private const string StoredProcedureName = "INSERT_CUBE_REGION_KEY_VALUE";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertCubeRegionKeyValueProcedure" /> class.
        /// </summary>
        public InsertCubeRegionKeyValueProcedure()
            : base(StoredProcedureName)
        {
        }

        /// <inheritdoc />
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            CreateEndInclusiveParameter(command, true);
            CreateStartInclusiveParameter(command, true);
            CreateEndPeriodParameter(command).Value = DBNull.Value;
            CreateStartPeriodParameter(command).Value = DBNull.Value;
            return command;
        }

        /// <summary>
        ///     Creates the component type parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateComponentTypeParameter(DbCommand command, string value)
        {
            return this.CreateParameter(command, "p_component_type", DbType.AnsiString, ParameterDirection.Input, value);
        }

        /// <summary>
        ///     Gets the content constraint identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateCubeRegionIdParameter(DbCommand command, long value)
        {
            return this.CreateParameter(command, "p_cube_region_id", DbType.Int64, ParameterDirection.Input, value);
        }

        /// <summary>
        ///     Creates the include parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     the parameter value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateIncludeParameter(DbCommand command, bool value)
        {
            return this.CreateParameter(command, "p_include", DbType.Int32, ParameterDirection.Input, value ? 1 : 0);
        }

        /// <summary>
        ///     Creates the component identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     the parameter value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateMemberIdParameter(DbCommand command, string value)
        {
            return this.CreateParameter(command, "p_member_id", DbType.AnsiString, ParameterDirection.Input, value);
        }

        /// <summary>
        ///     Creates the start period parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     the parameter value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateStartPeriodParameter(DbCommand command)
        {
            return this.CreateParameter(command, "p_start_period", DbType.AnsiString, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the end period parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     the parameter value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateEndPeriodParameter(DbCommand command)
        {
            return this.CreateParameter(command, "p_end_period", DbType.AnsiString, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the end period parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     the parameter value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateEndInclusiveParameter(DbCommand command, bool value)
        {
            return this.CreateParameter(command, "p_end_inclusive", DbType.Int32, ParameterDirection.Input, value.ToDbValue());
        }

        /// <summary>
        ///     Creates the end period parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     the parameter value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateStartInclusiveParameter(DbCommand command, bool value)
        {
            return this.CreateParameter(command, "p_start_inclusive", DbType.Int32, ParameterDirection.Input, value.ToDbValue());
        }
    }
}