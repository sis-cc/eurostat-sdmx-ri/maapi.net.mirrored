// -----------------------------------------------------------------------
// <copyright file="InsertCodeListMapProcedure.cs" company="EUROSTAT">
//   Date Created : 2014-10-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    /// <summary>
    ///     The code list map procedure.
    /// </summary>
    public class InsertCodeListMapProcedure : SchemeMapProcedureBase
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertCodeListMapProcedure" /> class.
        /// </summary>
        public InsertCodeListMapProcedure()
            : base(new OutputProcedureBase("INSERT_CODELIST_MAP"))
        {
        }

        /// <summary>
        ///     Gets the name of the parent (another map or a structure set) parameter.
        /// </summary>
        /// <value>
        ///     The name of the scheme parameter.
        /// </value>
        public override string ParentIdParameterName
        {
            get
            {
                return "p_ss_id";
            }
        }

        /// <summary>
        ///     Gets the name of the source identifier parameter.
        /// </summary>
        /// <value>
        ///     The name of the source identifier parameter.
        /// </value>
        public override string SourceIdParameterName
        {
            get
            {
                return "p_source_cl_id";
            }
        }

        /// <summary>
        ///     Gets the name of the target identifier parameter.
        /// </summary>
        /// <value>
        ///     The name of the source identifier parameter.
        /// </value>
        public override string TargetIdParameterName
        {
            get
            {
                return "p_target_cl_id";
            }
        }
    }
}