// -----------------------------------------------------------------------
// <copyright file="InsertArtefact.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.Data.Common;
using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure
{
    /// <summary>
    ///     This class contains the stored procedure INSERT_ARTEFACT name and parameter names and types
    /// </summary>
    public class InsertArtefact : ArtefactProcedurebase
    {
        /// <summary>
        ///     Gets the INSERT_ARTEFACT stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_ARTEFACT";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertArtefact" /> class.
        /// </summary>
        public InsertArtefact()
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        /// Creates the service URL parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public DbParameter CreateServiceUrlParam(DbCommand command)
        {
            return this.CreateParameter(command, "p_service_url", DbType.String, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the structure URL parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public DbParameter CreateStructureUrlParam(DbCommand command)
        {
            return this.CreateParameter(command, "p_structure_url", DbType.String, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the is stub parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public DbParameter CreateIsStubParam(DbCommand command)
        {
            return this.CreateParameter(command, "p_is_stub", DbType.Int32, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the artefact type parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public DbParameter CreateArtefactTypeParam(DbCommand command)
        {
            return this.CreateParameter(command, "p_artefact_type", DbType.AnsiString, ParameterDirection.Input);
        }

        /// <summary>
        /// Insert to artefact table supports stub.
        /// </summary>
        /// <returns><c>true</c></returns>
        public override bool SupportsStub()
        {
            return true;
        }
    }
}