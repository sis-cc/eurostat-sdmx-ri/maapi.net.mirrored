// -----------------------------------------------------------------------
// <copyright file="InsertTextFormat.cs" company="EUROSTAT">
//   Date Created : 2012-07-05
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    ///     This class contains the stored procedure INSERT_TEXT_FORMAT name and parameter names and types
    /// </summary>
    public class InsertTextFormat : OutputProcedureBase
    {
        /// <summary>
        ///     Gets the <c>p_compid</c> parameter name
        /// </summary>
        public string ParentId;

        /// <summary>
        ///     Gets the <c>p_facet_type_enum</c> parameter name
        /// </summary>
        public const string FacetTypeEnumParameterName = "p_facet_type_enum";

        /// <summary>
        ///     Gets the p_facet_value parameter name
        /// </summary>
        public const string FacetValueParameterName = "p_facet_value";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertTextFormat" /> class.
        /// </summary>
        public InsertTextFormat(string procedureName)
            : base(procedureName)
        {
        }

        /// <summary>
        ///     Gets the <c>p_compid</c> parameter type
        /// </summary>
        public static DbType ParentIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the <c>p_facet_type_enum</c> parameter type
        /// </summary>
        public static DbType FacetTypeEnumType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the p_facet_value parameter type
        /// </summary>
        public static DbType FacetValueType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Creates the <c>p_compid</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for <c>p_compid</c> with type integer
        /// </returns>
        public DbParameter CreateParentIdParam(DbCommand command)
        {
            return this.CreateParameter(command, ParentId, ParentIdType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the <c>p_facet_type_enum</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for <c>p_facet_type_enum</c> with type integer
        /// </returns>
        public DbParameter CreateFacetTypeEnumParameter(DbCommand command)
        {
            return this.CreateParameter(command, FacetTypeEnumParameterName, FacetTypeEnumType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the p_facet_value parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for p_facet_value with type string
        /// </returns>
        public DbParameter CreateFacetValueParameter(DbCommand command)
        {
            return this.CreateParameter(command, FacetValueParameterName, FacetValueType, ParameterDirection.Input);
        }
    }
}