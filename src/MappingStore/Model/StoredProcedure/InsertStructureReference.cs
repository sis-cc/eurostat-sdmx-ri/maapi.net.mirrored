// -----------------------------------------------------------------------
// <copyright file="InsertStructureReference.cs" company="EUROSTAT">
//   Date Created : 2022-07-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    /// <summary>
    /// The SP for inserting Structure References
    /// </summary>
    public class InsertStructureReference : OutputProcedureBase
    {
        /// <summary>
        /// The name of the procedure
        /// </summary>
        public const string StoredProcedureName = "INSERT_STRUCTURE_REF";

        /// <summary>
        /// Initializes a new insance of the class for the default procedure
        /// </summary>
        public InsertStructureReference() 
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>
        ///     The <paramref name="database" /> instance may or may not be transactional.
        /// </remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateVersionExtensionParameter(command);
            this.CreateChildIdParameter(command);
            return command;
        }

        #region create parameters

        /// <summary>
        /// Creates the parameter object for p_ref_src_id
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public DbParameter CreateSourceIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, "p_ref_src_id", DbType.Int64, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter object for p_target_artefact
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public DbParameter CreateTargetIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, "p_target_artefact", DbType.Int64, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter object for p_major
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public DbParameter CreateMajorVersionParameter(DbCommand command)
        {
            return this.CreateParameter(command, "p_major", DbType.Int64, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter object for p_minor
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public DbParameter CreateMinorVersionParameter(DbCommand command)
        {
            return this.CreateParameter(command, "p_minor", DbType.Int64, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter object for p_patch
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public DbParameter CreatePatchVersionParameter(DbCommand command)
        {
            return this.CreateParameter(command, "p_patch", DbType.Int64, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter object for p_extension
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public DbParameter CreateVersionExtensionParameter(DbCommand command)
        {
            return this.CreateParameter(command, "p_extension", DbType.String, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter object for p_wildcard_pos
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public DbParameter CreateWildcardPositionParameter(DbCommand command)
        {
            return this.CreateParameter(command, "p_wildcard_pos", DbType.Int64, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter object for p_target_child_full_id
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public DbParameter CreateChildIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, "p_target_child_full_id", DbType.String, ParameterDirection.Input);
        }

        #endregion
    }
}
