// -----------------------------------------------------------------------
// <copyright file="IMapProcedureBase.cs" company="EUROSTAT">
//   Date Created : 2014-10-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    /// IMapProcedureBase interface
    /// </summary>
    /// <seealso cref="IOutputProcedure" />
    public interface IMapProcedureBase : IOutputProcedure
    {
        /// <summary>
        ///     Returns the parent (another map or a structure set) identifier parameter of the specified
        ///     <paramref name="command" />.
        ///     If it does not exist it will add it.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The input <see cref="DbParameter" /> of type <see cref="DbType.Int64" /> and name
        ///     <see cref="MapProcedureBase.ParentIdParameterName" />
        /// </returns>
        DbParameter CreateParentIdParameter(DbCommand command);

        /// <summary>
        ///     Creates the source identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The input <see cref="DbParameter" /> of type <see cref="DbType.Int64" /> and name
        ///     <see cref="MapProcedureBase.SourceIdParameterName" />
        /// </returns>
        DbParameter CreateSourceIdParameter(DbCommand command);

        /// <summary>
        ///     Creates the source identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The input <see cref="DbParameter" /> of type <see cref="DbType.Int64" /> and name
        ///     <see cref="MapProcedureBase.SourceIdParameterName" />
        /// </returns>
        DbParameter CreateTargetIdParameter(DbCommand command);
    }
}