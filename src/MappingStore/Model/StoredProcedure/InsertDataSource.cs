// -----------------------------------------------------------------------
// <copyright file="InsertDataSource.cs" company="EUROSTAT">
//   Date Created : 2017-04-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;
    using Manager;
    using System;

    /// <summary>
    ///  This class contains the stored procedure INSERT_DATA_SOURCE name and parameter names and types
    /// </summary>
    public class InsertDataSource : OutputProcedureBase
    {

        /// <summary>
        /// The stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_DATA_SOURCE";

        /// <summary>
        /// The WSDL URL parameter name
        /// </summary>
        public const string WsdlUrlParameterName = "p_wsdl_url";

        /// <summary>
        /// The wadl URL parameter name
        /// </summary>
        public const string WadlUrlParameterName = "p_wadl_url";

        /// <summary>
        /// The data URL parameter name
        /// </summary>
        public const string DataUrlParameterName = "p_data_url";
        /// <summary>
        /// The is simple parameter name
        /// </summary>
        public const string IsSimpleParameterName = "p_is_simple";
        /// <summary>
        /// The is rest parameter name
        /// </summary>
        public const string IsRestParameterName = "p_is_rest";
        /// <summary>
        /// The is ws parameter name
        /// </summary>
        public const string IsWsParameterName = "p_is_ws";


        /// <summary>
        /// Initializes a new instance of the <see cref="InsertDataSource"/> class.
        /// </summary>
        public InsertDataSource()
            : base(StoredProcedureName)
        {
        }




        /// <summary>
        /// Gets the type of the WSDL URL parameter.
        /// </summary>
        /// <value>
        /// The type of the WSDL URL parameter.
        /// </value>
        public static DbType WsdlUrlParameterType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        /// Gets the type of the wadl URL parameter.
        /// </summary>
        /// <value>
        /// The type of the wadl URL parameter.
        /// </value>
        public static DbType WadlUrlParameterType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        /// Gets the type of the data URL.
        /// </summary>
        /// <value>
        /// The type of the data URL.
        /// </value>
        public static DbType DataUrlType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        /// Gets the type of the is simple.
        /// </summary>
        /// <value>
        /// The type of the is simple.
        /// </value>
        public static DbType IsSimpleType
        {
            get
            {
                return DbType.Int16;
            }
        }

        /// <summary>
        /// Gets the type of the is rest.
        /// </summary>
        /// <value>
        /// The type of the is rest.
        /// </value>
        public static DbType IsRestType
        {
            get
            {
                return DbType.Int16;
            }
        }


        /// <summary>
        /// Gets the type of the is ws.
        /// </summary>
        /// <value>
        /// The type of the is ws.
        /// </value>
        public static DbType IsWsType
        {
            get
            {
                return DbType.Int16;
            }
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateWsdlUrlParameter(command).Value = DBNull.Value;
            this.CreateWadlUrlParameter(command).Value = DBNull.Value;
            return command;
        }

        public DbParameter CreateWsdlUrlParameter(DbCommand command)
        {
            return this.CreateParameter(command, WsdlUrlParameterName, WsdlUrlParameterType, ParameterDirection.Input);
        }

        public DbParameter CreateWadlUrlParameter(DbCommand command)
        {
            return this.CreateParameter(command, WadlUrlParameterName, WadlUrlParameterType, ParameterDirection.Input);
        }

        public DbParameter CreateDataUrlParameter(DbCommand command)
        {
            return this.CreateParameter(command, DataUrlParameterName, DataUrlType, ParameterDirection.Input);
        }

        public DbParameter CreateIsSimpleParameter(DbCommand command)
        {
            return this.CreateParameter(command, IsSimpleParameterName, IsSimpleType, ParameterDirection.Input);
        }

        public DbParameter CreateIsRestParameter(DbCommand command)
        {
            return this.CreateParameter(command, IsRestParameterName, IsRestType, ParameterDirection.Input);
        }

        public DbParameter CreateIsWsParameter(DbCommand command)
        {
            return this.CreateParameter(command, IsWsParameterName, IsWsType, ParameterDirection.Input);
        }
    }
}