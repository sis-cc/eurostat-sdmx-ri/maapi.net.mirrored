// -----------------------------------------------------------------------
// <copyright file="InsertGroupAnnotation.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    /// <summary>
    ///     The annotation group procedure.
    /// </summary>
    public class InsertGroupAnnotation : AnnotationProcedureBase
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertGroupAnnotation" /> class.
        /// </summary>
        public InsertGroupAnnotation()
            : base("INSERT_GROUP_ANNOTATION")
        {
        }

        /// <summary>
        ///     Gets the name of the annotate-able identifier parameter.
        /// </summary>
        /// <value>
        ///     The name of the annotate able identifier parameter.
        /// </value>
        public override string ParentIdParameterName
        {
            get
            {
                return "p_gr_id";
            }
        }
    }
}