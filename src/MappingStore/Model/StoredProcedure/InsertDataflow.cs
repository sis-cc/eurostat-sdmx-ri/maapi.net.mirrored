// -----------------------------------------------------------------------
// <copyright file="InsertDataflow.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     This class contains the stored procedure INSERT_DATAFLOW name and parameter names and types
    /// </summary>
    public class InsertDataflow : ArtefactProcedurebase
    {
        /// <summary>
        ///     Gets the p_dsd_id parameter name
        /// </summary>
        public const string DsdIdParameterName = "p_dsd_id";

        /// <summary>
        ///     Gets the p_map_set_id parameter name
        /// </summary>
        public const string MapSetIdParameterName = "p_map_set_id";

        /// <summary>
        ///     Gets the INSERT_DATAFLOW stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_DATAFLOW";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertDataflow" /> class.
        /// </summary>
        public InsertDataflow()
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Gets the p_dsd_id parameter type
        /// </summary>
        public static DbType DsdIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the p_map_set_id parameter type
        /// </summary>
        public static DbType MapSetIdType
        {
            get
            {
                return DbType.Int64;
            }
        }


        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            return command;
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The command
        /// </param>
        /// <returns>
        ///     a new instance of Parameter
        /// </returns>
        public DbParameter CreateDsdIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, DsdIdParameterName, DsdIdType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The command
        /// </param>
        /// <returns>
        ///     a new instance of Parameter
        /// </returns>
        public DbParameter CreateMapSetIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, MapSetIdParameterName, MapSetIdType, ParameterDirection.Input);
        }
    }
}