// -----------------------------------------------------------------------
// <copyright file="InsertOtherNameable.cs" company="EUROSTAT">
//   Date Created : 2022-06-30
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    /// <summary>
    /// The SP for inserting other nameables.
    /// </summary>
    public class InsertOtherNameable : IdentifiableProcedureBase
    {
        /// <summary>
        /// Procedure 
        /// </summary>
        public const string ProcedureName = "INSERT_OTHER_NAMEABLE";

        /// <summary>
        /// Initializes a new instance of the class with the default SP
        /// </summary>
        public InsertOtherNameable() 
            : base(ProcedureName)
        {
        }
        
        /// <summary>
        /// Initializes a new instance of the class with the given SP
        /// </summary>
        protected InsertOtherNameable(string procedureName) : base(procedureName)
        {
        }

        #region create parameters

        /// <summary>
        /// Creates the valid from parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The parameter</returns>
        public DbParameter CreateValidFromParam(DbCommand command)
        {
            return this.CreateParameter(command, "p_valid_from", DbType.DateTime, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the valid to parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The parameter</returns>
        public DbParameter CreateValidToParam(DbCommand command)
        {
            return this.CreateParameter(command, "p_valid_to", DbType.DateTime, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the version parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The parameter</returns>
        public DbParameter CreateVersionParam(DbCommand command)
        {
            return this.CreateParameter(command, "p_version", DbType.String, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parent PK parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The parameter</returns>
        public DbParameter CreateParentIdParam(DbCommand command)
        {
            return this.CreateParameter(command, "p_parent_artefact", DbType.Int64, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parent path parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The parameter</returns>
        public DbParameter CreateParentPathParam(DbCommand command)
        {
            return this.CreateParameter(command, "p_parent_path", DbType.String, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the urn class parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>The parameter</returns>
        public DbParameter CreateUrnClassParam(DbCommand command)
        {
            return this.CreateParameter(command, "p_urn_class", DbType.String, ParameterDirection.Input);
        }

        #endregion
    }
}
