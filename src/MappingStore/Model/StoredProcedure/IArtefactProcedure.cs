// -----------------------------------------------------------------------
// <copyright file="IArtefactProcedure.cs" company="EUROSTAT">
//   Date Created : 2014-10-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data.Common;

    /// <summary>
    ///     The base interface for all maintainable artifacts.
    /// </summary>
    public interface IArtefactProcedure : IIdentifiableProcedure
    {
        /// <summary>
        ///     Creates the p_agency parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        DbParameter CreateAgencyParameter(DbCommand command);

        /// <summary>
        ///     Creates the p_is_final parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        DbParameter CreateIsFinalParameter(DbCommand command);

        /// <summary>
        ///     Creates the p_uri parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        DbParameter CreateLastModifiedParameter(DbCommand command);

        /// <summary>
        ///     Creates the p_uri parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        DbParameter CreateUriParameter(DbCommand command);

        /// <summary>
        ///     Creates the p_valid_from parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        DbParameter CreateValidFromParameter(DbCommand command);

        /// <summary>
        ///     Creates the p_valid_to parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        DbParameter CreateValidToParameter(DbCommand command);

        /// <summary>
        ///     Creates the p_version parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        DbParameter CreateVersionParameter(DbCommand command);
    }
}