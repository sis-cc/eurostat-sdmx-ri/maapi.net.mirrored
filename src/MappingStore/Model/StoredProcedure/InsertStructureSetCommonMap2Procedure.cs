// -----------------------------------------------------------------------
// <copyright file="InsertStructureSetCommonMap2Procedure.cs" company="EUROSTAT">
//   Date Created : 2022-06-30
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Model.StoredProcedure
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Text;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;

    /// <summary>
    /// The INSERT_STRUCTURE_SET_CMN_MAP2 stored procedure
    /// </summary>
    public class InsertStructureSetCommonMap2Procedure : MapProcedureBase
    {
        private static readonly string PARENT_MAP_PARAMETER_NAME = "p_parent_map";
        private static readonly string SOURCE_ID_PARAMETER_NAME = "p_source_id";
        private static readonly string TARGET_ID_PARAMETER_NAME = "p_target_id";

        private static readonly string _procedure = "INSERT_STRUCTURE_SET_CMN_MAP2";

        /// <summary>
        /// Initializes a new instance with a given procedure.
        /// </summary>
        /// <param name="outputProcedure">The procedure to use.</param>
        public InsertStructureSetCommonMap2Procedure(IOutputProcedure outputProcedure) 
            : base(outputProcedure)
        {
        }

        /// <summary>
        /// Initializes a new instance with the default procedure.
        /// </summary>
        public InsertStructureSetCommonMap2Procedure()
            :base(new OutputProcedureBase(_procedure))
        {

        }

        /// <inheritdoc/>
        public override string ParentIdParameterName => PARENT_MAP_PARAMETER_NAME;

        /// <inheritdoc/>
        public override string SourceIdParameterName => SOURCE_ID_PARAMETER_NAME;

        /// <inheritdoc/>
        public override string TargetIdParameterName => TARGET_ID_PARAMETER_NAME;

        /// <summary>
        ///     Creates the source identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The input <see cref="DbParameter" /> of type <see cref="DbType.String" /> and name
        ///     <see cref="SourceIdParameterName" />
        /// </returns>
        public override DbParameter CreateSourceIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, this.SourceIdParameterName, DbType.String, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the target identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The input <see cref="DbParameter" /> of type <see cref="DbType.String" /> and name
        ///     <see cref="TargetIdParameterName" />
        /// </returns>
        public override DbParameter CreateTargetIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, this.TargetIdParameterName, DbType.String, ParameterDirection.Input);
        }
    }
}
