// -----------------------------------------------------------------------
// <copyright file="IdentifiableProcedureBase.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     The base class for all identifiable SDMX artifacts
    /// </summary>
    public class IdentifiableProcedureBase : IIdentifiableProcedure
    {
        /// <summary>
        ///     Gets the p_id parameter name
        /// </summary>
        public const string IdParameterName = "p_id";

        /// <summary>
        ///     The _procedure
        /// </summary>
        private readonly IOutputProcedure _procedure;

        /// <summary>
        ///     Initializes a new instance of the <see cref="IdentifiableProcedureBase" /> class.
        /// </summary>
        /// <param name="procedureName">
        ///     The procedure name.
        /// </param>
        public IdentifiableProcedureBase(string procedureName)
        {
            this._procedure = new OutputProcedureBase(procedureName);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="IdentifiableProcedureBase" /> class.
        /// </summary>
        /// <param name="procedure">The procedure.</param>
        public IdentifiableProcedureBase(IOutputProcedure procedure)
        {
            this._procedure = procedure;
        }

        /// <summary>
        ///     Gets the p_id parameter type
        /// </summary>
        public static DbType IdType
        {
            get
            {
                return DbType.AnsiString;
            }
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>
        ///     The <paramref name="database" /> instance may or may not be transactional.
        /// </remarks>
        public virtual DbCommand CreateCommand(Database database)
        {
            return this._procedure.CreateCommand(database);
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="connection" /> and <paramref name="transaction" />
        /// </summary>
        /// <param name="connection">
        ///     The connection
        /// </param>
        /// <param name="transaction">
        ///     The transaction; otherwise set to null
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="connection" /> and <paramref name="transaction" />
        /// </returns>
        /// <remarks>
        ///     It sets the following <see cref="DbCommand" /> properties <see cref="DbCommand.Connection" /> ,
        ///     <see cref="DbCommand.Transaction" /> , <see cref="DbCommand.CommandType" /> and
        ///     <see cref="DbCommand.CommandText" />
        /// </remarks>
        public virtual DbCommand CreateCommand(DbConnection connection, DbTransaction transaction)
        {
            return this._procedure.CreateCommand(connection, transaction);
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>
        ///     The <paramref name="database" /> instance may or may not be transactional.
        /// </remarks>
        public virtual DbCommand CreateCommandWithDefaults(Database database)
        {
            return this._procedure.CreateCommandWithDefaults(database);
        }

        /// <summary>
        ///     Creates the p_id parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the p_id parameter
        /// </returns>
        public DbParameter CreateIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, IdParameterName, IdType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the <c>'p_pk'</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     The parameter for <c>'p_pk'</c> parameter
        /// </returns>
        public DbParameter CreateOutputParameter(DbCommand command)
        {
            return this._procedure.CreateOutputParameter(command);
        }

        /// <summary>
        ///     Create parameter for the specified <paramref name="command" /> and add it to the command
        /// </summary>
        /// <param name="command">
        ///     The command
        /// </param>
        /// <param name="name">
        ///     The parameter name
        /// </param>
        /// <param name="dbType">
        ///     The type
        /// </param>
        /// <param name="direction">
        ///     The direction
        /// </param>
        /// <returns>
        ///     The new <see cref="DbParameter" />
        /// </returns>
        public virtual DbParameter CreateParameter(DbCommand command, string name, DbType dbType, ParameterDirection direction)
        {
            return this._procedure.CreateParameter(command, name, dbType, direction);
        }

        /// <summary>
        ///     Create parameter for the specified <paramref name="command" /> and add it to the command
        /// </summary>
        /// <typeparam name="T">
        ///     The parameter type.
        /// </typeparam>
        /// <param name="command">
        ///     The command
        /// </param>
        /// <param name="name">
        ///     The parameter name
        /// </param>
        /// <param name="dbType">
        ///     The type
        /// </param>
        /// <param name="direction">
        ///     The direction
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The new <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateParameter<T>(DbCommand command, string name, DbType dbType, ParameterDirection direction, T value)
        {
            return this._procedure.CreateParameter(command, name, dbType, direction, value);
        }
    }
}