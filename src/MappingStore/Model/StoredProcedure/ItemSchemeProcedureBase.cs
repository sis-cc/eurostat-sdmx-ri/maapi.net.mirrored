// -----------------------------------------------------------------------
// <copyright file="ItemSchemeProcedureBase.cs" company="EUROSTAT">
//   Date Created : 2013-06-05
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     This is the base class for ITEM SCHEME based stored procedures, and contains the common parameter names and types
    /// </summary>
    public class ItemSchemeProcedureBase : ArtefactProcedurebase
    {
        /// <summary>
        ///     Gets the <c>p_is_partial</c> parameter name
        /// </summary>
        public const string IsPartialParameterName = "p_is_partial";

        /// <summary>
        /// The SP for inserting an item scheme
        /// </summary>
        protected const string insertProc = "INSERT_ITEM_SCHEME";

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemSchemeProcedureBase" /> class.
        /// </summary>
        /// <param name="procedureName">
        ///     The procedure name.
        /// </param>
        protected ItemSchemeProcedureBase(string procedureName)
            : base(procedureName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the class with the default insert procedure.
        /// </summary>
        public ItemSchemeProcedureBase()
            : base(insertProc)
        {
        }

        /// <summary>
        ///     Gets the <c>p_is_partial</c> parameter type
        /// </summary>
        public static DbType IsPartialType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateIsPartialParameter(command).Value = 0;
            return command;
        }

        /// <summary>
        ///     Creates the <c>p_is_partial</c> parameter for the specified <paramref name="command" /> . The parameter is added to
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The database command
        /// </param>
        /// <returns>
        ///     A new instance of the parameter
        /// </returns>
        public DbParameter CreateIsPartialParameter(DbCommand command)
        {
            return this.CreateParameter(command, IsPartialParameterName, IsPartialType, ParameterDirection.Input);
        }

        /// <summary>
        /// ItemSchemes supports stubs.
        /// </summary>
        /// <returns>true</returns>
        public override bool SupportsStub()
        {
            return true;
        }
    }
}