// -----------------------------------------------------------------------
// <copyright file="SchemeMapProcedureBase.cs" company="EUROSTAT">
//   Date Created : 2014-10-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data.Common;

    /// <summary>
    ///     The scheme map procedure base.
    /// </summary>
    public abstract class SchemeMapProcedureBase : MapProcedureBase, IIdentifiableProcedure
    {
        /// <summary>
        ///     The _identifiable procedure
        /// </summary>
        private readonly IdentifiableProcedureBase _identifiableProcedure;

        /// <summary>
        /// Initializes a new instance of the <see cref="SchemeMapProcedureBase" /> class.
        /// </summary>
        /// <param name="outputProcedure">The output procedure.</param>
        protected SchemeMapProcedureBase(IOutputProcedure outputProcedure)
            : base(outputProcedure)
        {
            this._identifiableProcedure = new IdentifiableProcedureBase(outputProcedure);
        }

        /// <summary>
        ///     Creates the p_id parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     A new instance of the p_id parameter
        /// </returns>
        public DbParameter CreateIdParameter(DbCommand command)
        {
            return this._identifiableProcedure.CreateIdParameter(command);
        }
    }
}