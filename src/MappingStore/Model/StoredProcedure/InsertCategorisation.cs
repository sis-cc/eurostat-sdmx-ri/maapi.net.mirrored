// -----------------------------------------------------------------------
// <copyright file="InsertCategorisation.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    ///     This class contains the stored procedure <c>INSERT_CATEGORISATION</c> name and parameter names and types
    /// </summary>
    public class InsertCategorisation : ArtefactProcedurebase
    {
        /// <summary>
        ///     Gets the <c>p_art_id</c> parameter name
        /// </summary>
        public const string ArtIdParameterName = "p_art_id";

        /// <summary>
        ///     Gets the p_cat_id parameter name
        /// </summary>
        public const string CatIdParameterName = "p_cat_id";

        /// <summary>
        ///     Gets the p_dc_order parameter name
        /// </summary>
        public const string DcOrderParameterName = "p_dc_order";

        /// <summary>
        ///     Gets the <c>INSERT_CATEGORISATION</c> stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_CATEGORISATION";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertCategorisation" /> class.
        /// </summary>
        public InsertCategorisation()
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Gets the <c>p_art_id</c>  parameter type
        /// </summary>
        public static DbType ArtIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the p_map_set_id parameter type
        /// </summary>
        public static DbType CatIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Creates the <c>p_art_id</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The database command
        /// </param>
        /// <returns>
        ///     a new instance of database Parameter for ID
        /// </returns>
        public DbParameter CreateArtIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, ArtIdParameterName, ArtIdType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the p_cat_id parameter for the specified <paramref name="command" /> . The parameter is added
        /// <paramref name="command" />
        /// </summary>
        /// <param name="command">The database command</param>
        /// <returns>
        /// a new instance of database Parameter for p_cat_id with type integer
        /// </returns>
        public DbParameter CreateCatIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, CatIdParameterName, CatIdType, ParameterDirection.Input);
        }
    }
}