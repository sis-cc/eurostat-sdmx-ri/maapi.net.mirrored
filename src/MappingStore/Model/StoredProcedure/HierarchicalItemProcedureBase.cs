﻿// -----------------------------------------------------------------------
// <copyright file="HierarchicalItemProcedureBase.cs" company="EUROSTAT">
//   Date Created : 2013-06-05
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     The hierarchical item procedure base.
    /// </summary>
    public abstract class HierarchicalItemProcedureBase : ItemProcedureBase
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="HierarchicalItemProcedureBase" /> class.
        /// </summary>
        /// <param name="procedureName">The procedure name.</param>
        protected HierarchicalItemProcedureBase(string procedureName)
            : base(procedureName)
        {
        }

        /// <summary>
        ///     Gets the p_parent_code parameter type
        /// </summary>
        public static DbType ParentItemType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the name of the parent item parameter.
        /// </summary>
        /// <value>
        ///     The name of the parent item parameter.
        /// </value>
        public abstract string ParentItemParameterName { get; }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateParentItemIdParameter(command).Value = DBNull.Value;
            return command;
        }

        /// <summary>
        ///     Returns the parent item identifier parameter of the specified <paramref name="command" />.
        ///     If it does not exist it will add it.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>
        ///     The input <see cref="DbParameter" /> of type <see cref="DbType.Int64" /> and name
        ///     <see cref="ParentItemParameterName" />
        /// </returns>
        public virtual DbParameter CreateParentItemIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, this.ParentItemParameterName, ParentItemType, ParameterDirection.Input);
        }
    }
}