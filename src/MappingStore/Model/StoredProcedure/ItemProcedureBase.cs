// -----------------------------------------------------------------------
// <copyright file="ItemProcedureBase.cs" company="EUROSTAT">
//   Date Created : 2014-10-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    ///     The item procedure base.
    /// </summary>
    public abstract class ItemProcedureBase : IdentifiableProcedureBase
    {
        /// <summary>
        /// The p_parent_item_scheme column name
        /// </summary>
        public static readonly string ParentItemScheme = "p_parent_item_scheme";

        /// <summary>
        /// The p_parent_item column name
        /// </summary>
        public static readonly string ParentItem = "p_parent_item";

        /// <summary>
        /// The p_value_id column name
        /// </summary>
        public static readonly string ValueId = "p_value_id";

        /// <summary>
        /// JSON with names
        /// </summary>
        public static readonly string Names = "p_names";
        /// <summary>
        /// JSON with descriptions
        /// </summary>
        public static readonly string Descriptions = "p_descriptions";
        /// <summary>
        /// JSON with annotations
        /// </summary>
        public static readonly string Annotations = "p_annotations";

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemProcedureBase" /> class.
        /// </summary>
        /// <param name="procedureName">The procedure name.</param>
        protected ItemProcedureBase(string procedureName)
            : base(procedureName)
        {
        }

        /// <summary>
        ///     Gets the name of the scheme parameter.
        /// </summary>
        /// <value>
        ///     The name of the scheme parameter.
        /// </value>
        public virtual string SchemeParameterName => ParentItemScheme;

        /// <summary>
        ///     Returns the scheme identifier parameter of the specified <paramref name="command" />.
        ///     If it does not exist it will add it.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>
        ///     The input <see cref="DbParameter" /> of type <see cref="DbType.Int64" /> and name
        ///     <see cref="SchemeParameterName" />
        /// </returns>
        public DbParameter CreateSchemeIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, this.SchemeParameterName, DbType.Int64, ParameterDirection.Input);
        }
    }
}