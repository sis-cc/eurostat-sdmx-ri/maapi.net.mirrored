// -----------------------------------------------------------------------
// <copyright file="InsertContentConstraintAttachmentProcedure.cs" company="EUROSTAT">
//   Date Created : 2014-10-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     The content constraint attachment procedure.
    /// </summary>
    public class InsertContentConstraintAttachmentProcedure : IProcedure
    {
        /// <summary>
        ///     The _procedure
        /// </summary>
        private readonly IProcedure _procedure;

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertContentConstraintAttachmentProcedure" /> class.
        /// </summary>
        public InsertContentConstraintAttachmentProcedure()
        {
            this._procedure = new ProcedureBase("INSERT_CONSTRAINT_ATTACHMENT");
        }

        /// <summary>
        ///     Gets the content constraint identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateArtefactIdParameter(DbCommand command, long value)
        {
            return this.CreateParameter(command, "p_art_id", DbType.Int64, ParameterDirection.Input, value);
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>
        ///     The <paramref name="database" /> instance may or may not be transactional.
        /// </remarks>
        public DbCommand CreateCommand(Database database)
        {
            return this._procedure.CreateCommand(database);
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="connection" /> and <paramref name="transaction" />
        /// </summary>
        /// <param name="connection">
        ///     The connection
        /// </param>
        /// <param name="transaction">
        ///     The transaction; otherwise set to null
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="connection" /> and <paramref name="transaction" />
        /// </returns>
        /// <remarks>
        ///     It sets the following <see cref="DbCommand" /> properties <see cref="DbCommand.Connection" /> ,
        ///     <see cref="DbCommand.Transaction" /> , <see cref="DbCommand.CommandType" /> and
        ///     <see cref="DbCommand.CommandText" />
        /// </remarks>
        public DbCommand CreateCommand(DbConnection connection, DbTransaction transaction)
        {
            return this._procedure.CreateCommand(connection, transaction);
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>
        ///     The <paramref name="database" /> instance may or may not be transactional.
        /// </remarks>
        public DbCommand CreateCommandWithDefaults(Database database)
        {
            return this._procedure.CreateCommandWithDefaults(database);
        }

        /// <summary>
        ///     Gets the content constraint identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateContentConstraintIdParameter(DbCommand command, long value)
        {
            return this.CreateParameter(command, "p_cont_cons_id", DbType.Int64, ParameterDirection.Input, value);
        }

        /// <summary>
        ///     Create parameter for the specified <paramref name="command" /> and add it to the command
        /// </summary>
        /// <param name="command">
        ///     The command
        /// </param>
        /// <param name="name">
        ///     The parameter name
        /// </param>
        /// <param name="dbType">
        ///     The type
        /// </param>
        /// <param name="direction">
        ///     The direction
        /// </param>
        /// <returns>
        ///     The new <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateParameter(DbCommand command, string name, DbType dbType, ParameterDirection direction)
        {
            return this._procedure.CreateParameter(command, name, dbType, direction);
        }

        /// <summary>
        ///     Create parameter for the specified <paramref name="command" /> and add it to the command
        /// </summary>
        /// <typeparam name="T">
        ///     The parameter type.
        /// </typeparam>
        /// <param name="command">
        ///     The command
        /// </param>
        /// <param name="name">
        ///     The parameter name
        /// </param>
        /// <param name="dbType">
        ///     The type
        /// </param>
        /// <param name="direction">
        ///     The direction
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The new <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateParameter<T>(DbCommand command, string name, DbType dbType, ParameterDirection direction, T value)
        {
            return this._procedure.CreateParameter(command, name, dbType, direction, value);
        }
    }
}