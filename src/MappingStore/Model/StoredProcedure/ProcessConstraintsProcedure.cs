// -----------------------------------------------------------------------
// <copyright file="ProcessConstraintsProcedure.cs" company="EUROSTAT">
//   Date Created : 2018-05-30
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// This class contains the stored procedure PROCESS_CONSTRAINTS name and parameter names and types
    /// </summary>    
    public class ProcessConstraintsProcedure : OutputProcedureBase
    {
        /// <summary>
        ///     Gets the p_art_agency parameter name
        /// </summary>
        public const string ArtAgencyParameterName = "p_art_agency";

        /// <summary>
        ///     Gets the p_art_id parameter name
        /// </summary>       
        public const string ArtIdParameterName = "p_art_id";

        /// <summary>
        ///     Gets the p_art_version parameter name        
        /// </summary>
        public const string ArtVersionParameterName = "p_art_version";

        /// <summary>
        ///     Gets the p_art_type parameter name
        /// </summary>
        public const string ArtTypeParameterName = "p_art_type";      
        
        /// <summary>
        ///     Gets the p_cl_sys_id parameter name
        /// </summary>
        public const string ClSysIdParameterName = "p_cl_sys_id";

        /// <summary>
        ///     Gets the p_const_type parameter name
        /// </summary>
        public const string ConstraintTypeParameterName = "p_const_type";

        /// <summary>
        ///     Gets the p_is_cl_partial parameter name
        /// </summary>
        public const string IsClPartialParameterName = "p_is_cl_partial";
        
        /// <summary>
        ///     Gets the PROCESS_CONSTRAINTS stored procedure name
        /// </summary>
        public const string StoredProcedureName = "PROCESS_CONSTRAINTS";

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessConstraintsProcedure" /> class.
        /// </summary>
        public ProcessConstraintsProcedure() : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Gets the <c>p_art_agency</c> parameter type
        /// </summary>
        private static DbType ArtAgencyType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the <c>p_art_id</c> parameter type
        /// </summary>
        private static DbType ArtIdType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the <c>p_art_version</c> parameter type
        /// </summary>
        private static DbType ArtVersionType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the <c>p_art_type</c> parameter type
        /// </summary>
        private static DbType ArtTypeType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the <c>p_cl_sys_id</c> parameter type
        /// </summary>
        private static DbType ClSysIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the <c>p_const_type</c> parameter type
        /// </summary>
        private static DbType ConstraintTypeType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the <c>p_is_cl_partial</c> paramter type
        /// </summary>
        private static DbType IsClPartialType
        {
            get
            {
                return DbType.Int16;
            }
        }

        /// <summary>
        /// Creates the <c>p_art_agency</c> parameter for the specified <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        /// a new instance of database parameter for <c>p_art_agency</c>.
        /// </returns>
        private DbParameter CreateArtAgencyParameter(DbCommand command)
        {
            return this.CreateParameter(command, ArtAgencyParameterName, ArtAgencyType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the <c>p_art_id</c> parameter for the specified <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        /// a new instance of database parameter for <c>p_art_id</c>.
        /// </returns>
        private DbParameter CreateArtIdParameter(DbCommand command) {
            return this.CreateParameter(command, ArtIdParameterName, ArtIdType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the <c>p_art_version</c> parameter for the specified <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        /// a new instance of database parameter for <c>p_art_version</c>.
        /// </returns>
        private DbParameter CreateArtVersionParameter(DbCommand command)
        {
            return this.CreateParameter(command, ArtVersionParameterName, ArtVersionType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the <c>p_art_type</c> parameter for the specified <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        /// a new instance of database parameter for <c>p_art_type</c>.
        /// </returns>
        private DbParameter CreateArtTypeParameter(DbCommand command)
        {
            return this.CreateParameter(command, ArtTypeParameterName, ArtTypeType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the <c>p_cl_id</c> parameter for the specified <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        /// a new instance of database parameter for <c>p_cl_id</c>.
        /// </returns>
        private DbParameter CreateClSysIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, ClSysIdParameterName, ClSysIdType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the <c>p_const_type</c> parameter for the specified <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        /// a new instance of database parameter for <c>p_cons_type</c>.
        /// </returns>
        private DbParameter CreateConstraintTypeParameter(DbCommand command)
        {
            return this.CreateParameter(command, ConstraintTypeParameterName, ConstraintTypeType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the <c>p_is_cl_partial</c> parameter for the specified <paramref name="command"/>
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private DbParameter CreateIsClPartialParameter(DbCommand command)
        {
            return this.CreateParameter(command, IsClPartialParameterName, IsClPartialType, ParameterDirection.Output);
        }

        /// <summary>
        /// Create parameters required from the procedure.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="requestedStructureRef"></param>
        /// <param name="codelistSysId"></param>
        /// <param name="constraintType"></param>
        public virtual void CreateParameters(DbCommand command, IStructureReference requestedStructureRef, long codelistSysId, string constraintType)
        {
            DbParameter artAgencyParameter = CreateArtAgencyParameter(command);
            artAgencyParameter.Value = requestedStructureRef.AgencyId ?? (object)DBNull.Value;

            DbParameter artIdParameter = CreateArtIdParameter(command);
            artIdParameter.Value = requestedStructureRef.MaintainableId ?? (object)DBNull.Value;

            DbParameter artVersionParameter = CreateArtVersionParameter(command);
            artVersionParameter.Value = requestedStructureRef.Version ?? (object)DBNull.Value;

            DbParameter artTypeParameter = CreateArtTypeParameter(command);
            artTypeParameter.Value = requestedStructureRef.MaintainableStructureEnumType.UrnClass;

            DbParameter clSysIdParamter = CreateClSysIdParameter(command);
            clSysIdParamter.Value = codelistSysId;

            DbParameter constTypeParameter = CreateConstraintTypeParameter(command);
            constTypeParameter.Value = constraintType ?? (object)DBNull.Value;

            DbParameter isClPartialParameter = CreateIsClPartialParameter(command);
        }
    }
}