// -----------------------------------------------------------------------
// <copyright file="InsertTranscoding.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     This class contains the stored procedure INSERT_TRANSCODING name and parameter names and types
    /// </summary>
    public class InsertTranscoding : OutputProcedureBase
    {
        /// <summary>
        ///     Gets the p_expression parameter name
        /// </summary>
        public const string ExpressionParameterName = "p_expression";

        /// <summary>
        ///     Gets the p_map_id parameter name
        /// </summary>
        public const string MapIdParameterName = "p_map_id";

        /// <summary>
        ///     Gets the INSERT_TRANSCODING stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_TRANSCODING";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertTranscoding" /> class.
        /// </summary>
        public InsertTranscoding()
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Gets the p_expression parameter type
        /// </summary>
        public static DbType ExpressionType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the p_map_id parameter type
        /// </summary>
        public static DbType MapIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateExpressionParameter(command).Value = DBNull.Value;
            return command;
        }

        /// <summary>
        ///     Creates the p_expression parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for p_expression with type string
        /// </returns>
        public DbParameter CreateExpressionParameter(DbCommand command)
        {
            return this.CreateParameter(command, ExpressionParameterName, ExpressionType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the p_map_id parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for p_map_id with type integer
        /// </returns>
        public DbParameter CreateMapIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, MapIdParameterName, MapIdType, ParameterDirection.Input);
        }
    }
}