// -----------------------------------------------------------------------
// <copyright file="InsertContentConstraintProcedure.cs" company="EUROSTAT">
//   Date Created : 2014-10-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     The content constraint procedure.
    /// </summary>
    public class InsertContentConstraintProcedure : ArtefactProcedurebase
    {
        /// <summary>
        ///     The actual data parameter name
        /// </summary>
        private const string ActualDataParameterName = "p_actual_data";

        /// <summary>
        ///     The offset parameter name
        /// </summary>
        private const string OffsetParameterName = "p_offset";

        /// <summary>
        ///     The periodicity parameter name
        /// </summary>
        private const string PeriodicityParameterName = "p_periodicity";

        /// <summary>
        ///     The tolerance parameter name
        /// </summary>
        private const string ToleranceParameterName = "p_tolerance";
        /// <summary>
        /// The set identifier parameter name
        /// </summary>
        private const string SetIdParameterName = "p_set_id";
        /// <summary>
        /// The simple data source parameter name
        /// </summary>
        private const string SimpleDataSourceParameterName = "p_simple_data_source";

        /// <summary>The attach type</summary>
        private const string AttachType = "p_attach_type";
        
        /// <summary>Reference Period Start Time parameter name</summary>
        private const string StartTimeParameterName = "p_start_time";

        /// <summary>Reference Period End Time parameter name</summary>
        private const string EndTimeParameterName = "p_end_time";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertContentConstraintProcedure" /> class.
        /// </summary>
        public InsertContentConstraintProcedure()
            : base("INSERT_CONTENT_CONSTRAINT")
        {
        }

        /// <summary>
        ///     Gets the actual data parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateActualDataParameter(DbCommand command)
        {
            return this.CreateParameter(command, ActualDataParameterName, DbType.Int32, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateActualDataParameter(command).Value = 1;
            this.CreateOffsetParameter(command).Value = DBNull.Value;
            this.CreatePeriodicityParameter(command).Value = DBNull.Value;
            this.CreateToleranceParameter(command).Value = DBNull.Value;
            this.CreateAttachTypeParameter(command).Value = "Dataflow";
            this.CreateSetIdParameter(command).Value = DBNull.Value;
            this.CreateDataSourceParameter(command).Value = DBNull.Value;
            this.CreateStartTimeParameter(command).Value = DBNull.Value;
            this.CreateEndTimeParameter(command).Value = DBNull.Value;
            return command;
        }

        /// <summary>
        ///     Creates the offset parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateOffsetParameter(DbCommand command)
        {
            return this.CreateParameter(command, OffsetParameterName, DbType.AnsiString, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the periodicity parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreatePeriodicityParameter(DbCommand command)
        {
            return this.CreateParameter(command, PeriodicityParameterName, DbType.AnsiString, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the tolerance parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateToleranceParameter(DbCommand command)
        {
            return this.CreateParameter(command, ToleranceParameterName, DbType.AnsiString, ParameterDirection.Input);
        }

        /// <summary>Creates the attach type parameter.</summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public DbParameter CreateAttachTypeParameter(DbCommand command)
        {
            return this.CreateParameter(command, AttachType, DbType.AnsiString, ParameterDirection.Input);
        }

        /// <summary>Creates the set identifier parameter.</summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public DbParameter CreateSetIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, SetIdParameterName, DbType.AnsiString, ParameterDirection.Input);
        }

        /// <summary>Creates the Reference Period Start Time parameter.</summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public DbParameter CreateStartTimeParameter(DbCommand command)
        {
            return this.CreateParameter(command, StartTimeParameterName, DbType.DateTime, ParameterDirection.Input);
        }

        /// <summary>Creates the Reference Period End Time parameter.</summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public DbParameter CreateEndTimeParameter(DbCommand command)
        {
            return this.CreateParameter(command, EndTimeParameterName, DbType.DateTime, ParameterDirection.Input);
        }


        /// <summary>
        /// Creates the data source parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public DbParameter CreateDataSourceParameter(DbCommand command)
        {
            return this.CreateParameter(command, SimpleDataSourceParameterName, DbType.AnsiString, ParameterDirection.Input);
        }

    }
}