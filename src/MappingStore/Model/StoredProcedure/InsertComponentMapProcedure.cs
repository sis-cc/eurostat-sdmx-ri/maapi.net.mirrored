// -----------------------------------------------------------------------
// <copyright file="InsertComponentMapProcedure.cs" company="EUROSTAT">
//   Date Created : 2014-10-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    /// <summary>
    ///     The code map procedure.
    /// </summary>
    public class InsertComponentMapProcedure : MapProcedureBase
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertComponentMapProcedure" /> class.
        /// </summary>
        public InsertComponentMapProcedure()
            : base(new OutputProcedureBase("INSERT_COMPONENT_MAP"))
        {
        }

        /// <summary>
        ///     Gets the name of the parent (another map or a structure set) parameter.
        /// </summary>
        /// <value>
        ///     The name of the scheme parameter.
        /// </value>
        public override string ParentIdParameterName
        {
            get
            {
                return "p_sm_id";
            }
        }

        /// <summary>
        ///     Gets the name of the source identifier parameter.
        /// </summary>
        /// <value>
        ///     The name of the source identifier parameter.
        /// </value>
        public override string SourceIdParameterName
        {
            get
            {
                return "p_source_comp_id";
            }
        }

        /// <summary>
        ///     Gets the name of the target identifier parameter.
        /// </summary>
        /// <value>
        ///     The name of the source identifier parameter.
        /// </value>
        public override string TargetIdParameterName
        {
            get
            {
                return "p_target_comp_id";
            }
        }
    }
}