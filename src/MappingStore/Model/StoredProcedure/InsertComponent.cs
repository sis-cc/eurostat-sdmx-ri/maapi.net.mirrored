// -----------------------------------------------------------------------
// <copyright file="InsertComponent.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;
    
    /// <summary>
    ///     This class contains the stored procedure INSERT_COMPONENT name and parameter names and types
    /// </summary>
    public class InsertComponent : IdentifiableProcedureBase
    {
        /// <summary>
        ///     Gets the INSERT_COMPONENT stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_N_COMPONENT";

        #region parameter names

        /// <summary>
        /// The ATT ASS level parameter name
        /// </summary>
        public const string AttAssLevelParameterName = "p_att_ass_level";

        /// <summary>
        ///     Gets the is time format parameter name
        /// </summary>
        public const string AttIsTimeFormatParameterName = "p_att_is_time_format";

        /// <summary>
        /// The ATT status parameter name
        /// </summary>
        public const string AttStatusParameterName = "p_att_status";

        /// <summary>
        /// The min occurs parameter name
        /// </summary>
        public const string MinOccursParameterName = "p_min_occurs";

        /// <summary>
        /// The max occurs parameter name
        /// </summary>
        public const string MaxOccursParameterName = "p_max_occurs";

        /// <summary>
        ///     Gets the p_dsd_id parameter name
        /// </summary>
        public const string DsdIdParameterName = "p_dsd_id";

        /// <summary>
        /// The is frequency dimension parameter name
        /// </summary>
        public const string IsFreqDimParameterName = "p_is_freq_dim";

        /// <summary>
        ///     Gets the p_is_measure_dim parameter name
        /// </summary>
        public const string IsMeasureDimParameterName = "p_is_measure_dim";

        /// <summary>
        ///     Gets the p_type parameter name
        /// </summary>
        public const string TypeParameterName = "p_type";

        /// <summary>
        /// The XS attachment level DS parameter name
        /// </summary>
        public const string XsAttlevelDsParameterName = "p_xs_attlevel_ds";

        /// <summary>
        /// The XS attachment level group parameter name
        /// </summary>
        public const string XsAttlevelGroupParameterName = "p_xs_attlevel_group";

        /// <summary>
        /// The XS attachment level observation parameter name
        /// </summary>
        public const string XsAttlevelObsParameterName = "p_xs_attlevel_obs";

        /// <summary>
        /// The XS attachment level section parameter name
        /// </summary>
        public const string XsAttlevelSectionParameterName = "p_xs_attlevel_section";

        /// <summary>
        ///     Gets the p_xs_measure_code parameter name
        /// </summary>
        public const string XsMeasureCodeParameterName = "p_xs_measure_code";

        #endregion

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertComponent" /> class.
        /// </summary>
        public InsertComponent()
            : base(StoredProcedureName)
        {
        }

        #region Parameter types

        /// <summary>
        /// Gets the type of the ATT ASS level.
        /// </summary>
        /// <value>
        /// The type of the ATT ASS level.
        /// </value>
        public static DbType AttAssLevelType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        /// Gets the type of the attachment is time format.
        /// </summary>
        /// <value>
        /// The type of attachment is time format.
        /// </value>
        public static DbType AttIsTimeFormatType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary>
        /// Gets the type of the attachment status.
        /// </summary>
        /// <value>
        /// The type of the attachment status.
        /// </value>
        public static DbType AttStatusType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        /// The min occurs parameter type.
        /// </summary>
        public static DbType MinOccursType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        /// The max occurs parameter type.
        /// </summary>
        public static DbType MaxOccursType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the p_dsd_id parameter type
        /// </summary>
        public static DbType DsdIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        /// Gets the type of the is frequency dimension.
        /// </summary>
        /// <value>
        /// The type of the is frequency dimension.
        /// </value>
        public static DbType IsFreqDimType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary>
        ///     Gets the p_is_measure_dim parameter type
        /// </summary>
        public static DbType IsMeasureDimType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary>
        ///     Gets the p_type parameter type
        /// </summary>
        public static DbType TypeType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        /// Gets the type of the XS attachment level DS.
        /// </summary>
        /// <value>
        /// The type of the XS attachment level DS.
        /// </value>
        public static DbType XsAttlevelDsType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary>
        /// Gets the type of the XS attachment level group.
        /// </summary>
        /// <value>
        /// The type of the XS attachment level group.
        /// </value>
        public static DbType XsAttlevelGroupType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary>
        /// Gets the type of the XS attachment level observation.
        /// </summary>
        /// <value>
        /// The type of the XS attachment level observation.
        /// </value>
        public static DbType XsAttlevelObsType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary>
        /// Gets the type of the XS attachment level section.
        /// </summary>
        /// <value>
        /// The type of the XS attachment level section.
        /// </value>
        public static DbType XsAttlevelSectionType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary>
        ///     Gets the p_xs_measure_code parameter type
        /// </summary>
        public static DbType XsMeasureCodeType
        {
            get
            {
                return DbType.String;
            }
        }

        #endregion

        #region create parameters

        /// <summary>
        /// Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        /// <paramref name="command" />
        /// </summary>
        /// <param name="command">The database command</param>
        /// <returns>
        /// a new instance of parameter
        /// </returns>
        public DbParameter CreateAttAssLevelParameter(DbCommand command)
        {
            return this.CreateParameter(command, AttAssLevelParameterName, AttAssLevelType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter
        /// </returns>
        public DbParameter CreateAttIsTimeFormatParameter(DbCommand command)
        {
            return this.CreateParameter(command, AttIsTimeFormatParameterName, AttIsTimeFormatType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter
        /// </returns>
        public DbParameter CreateAttStatusParameter(DbCommand command)
        {
            return this.CreateParameter(command, AttStatusParameterName, AttStatusType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter for min occurs.
        /// </summary>
        /// <param name="command">The database command to attach the parameter to.</param>
        /// <returns>The <see cref="DbParameter"/>.</returns>
        public DbParameter CreateMinOccursParameter(DbCommand command)
        {
            return this.CreateParameter(command, MinOccursParameterName, MinOccursType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter for max occurs.
        /// </summary>
        /// <param name="command">The database command to attach the parameter to.</param>
        /// <returns>The <see cref="DbParameter"/>.</returns>
        public DbParameter CreateMaxOccursParameter(DbCommand command)
        {
            return this.CreateParameter(command, MaxOccursParameterName, MaxOccursType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter
        /// </returns>
        public DbParameter CreateDsdIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, DsdIdParameterName, DsdIdType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter
        /// </returns>
        public DbParameter CreateIsFreqDimParameter(DbCommand command)
        {
            return this.CreateParameter(command, IsFreqDimParameterName, IsFreqDimType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter
        /// </returns>
        public DbParameter CreateIsMeasureDimParameter(DbCommand command)
        {
            return this.CreateParameter(command, IsMeasureDimParameterName, IsMeasureDimType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter
        /// </returns>
        public DbParameter CreateTypeParameter(DbCommand command)
        {
            return this.CreateParameter(command, TypeParameterName, TypeType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter
        /// </returns>
        public DbParameter CreateXsAttlevelDsParameter(DbCommand command)
        {
            return this.CreateParameter(command, XsAttlevelDsParameterName, XsAttlevelDsType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter
        /// </returns>
        public DbParameter CreateXsAttlevelGroupParameter(DbCommand command)
        {
            return this.CreateParameter(command, XsAttlevelGroupParameterName, XsAttlevelGroupType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter
        /// </returns>
        public DbParameter CreateXsAttlevelObsParameter(DbCommand command)
        {
            return this.CreateParameter(command, XsAttlevelObsParameterName, XsAttlevelObsType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter
        /// </returns>
        public DbParameter CreateXsAttlevelSectionParameter(DbCommand command)
        {
            return this.CreateParameter(command, XsAttlevelSectionParameterName, XsAttlevelSectionType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter
        /// </returns>
        public DbParameter CreateXsMeasureCodeParameter(DbCommand command)
        {
            return this.CreateParameter(command, XsMeasureCodeParameterName, XsMeasureCodeType, ParameterDirection.Input);
        }

        #endregion
    }
}