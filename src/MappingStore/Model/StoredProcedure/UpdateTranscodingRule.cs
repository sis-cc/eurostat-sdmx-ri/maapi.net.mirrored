// -----------------------------------------------------------------------
// <copyright file="UpdateTranscodingRule.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     This class contains the stored procedure UPDATE_TRANSCODING_RULE name and parameter names and types
    /// </summary>
    public class UpdateTranscodingRule : OutputProcedureBase
    {
        /// <summary>
        ///     Gets the p_cd_id parameter name
        /// </summary>
        public const string CdIdParameterName = "p_cd_id";

        /// <summary>
        ///     Gets the <c>p_lcd_id</c> parameter name
        /// </summary>
        public const string LcdIdParameterName = "p_lcd_id";

        /// <summary>
        ///     Gets the UPDATE_TRANSCODING_RULE stored procedure name
        /// </summary>
        public const string StoredProcedureName = "UPDATE_TRANSCODING_RULE";

        /// <summary>
        ///     Gets the <c>p_tr_id</c> parameter name
        /// </summary>
        public const string TrIdParameterName = "p_tr_id";

        /// <summary>
        ///     Gets the <c>p_tr_rule_id</c> parameter name
        /// </summary>
        public const string TrRuleIdParameterName = "p_tr_rule_id";

        /// <summary>
        ///     Initializes a new instance of the <see cref="UpdateTranscodingRule" /> class.
        /// </summary>
        public UpdateTranscodingRule()
            : base(StoredProcedureName)
        {
        }

        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);

            this.CreateCdIdParameter(command).Value = DBNull.Value;
            this.CreateUncodedValueParameter(command).Value = DBNull.Value;
            CreateTrRuleIdParameter(command).Value = DBNull.Value;
            return command;
        }


        /// <summary>
        ///     Gets the p_cd_id parameter type
        /// </summary>
        public static DbType CdIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the <c>p_lcd_id</c> parameter type
        /// </summary>
        public static DbType LcdIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the <c>p_tr_id</c> parameter type
        /// </summary>
        public static DbType TrIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the <c>p_tr_rule_id</c> parameter type
        /// </summary>
        public static DbType TrRuleIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Creates the p_cd_id parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for p_cd_id with type integer
        /// </returns>
        public DbParameter CreateCdIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, CdIdParameterName, CdIdType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the <c>p_lcd_id</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for <c>p_lcd_id</c> with type integer
        /// </returns>
        public DbParameter CreateLcdIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, LcdIdParameterName, LcdIdType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the <c>p_tr_id</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for <c>p_tr_id</c> with type integer
        /// </returns>
        public DbParameter CreateTrIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, TrIdParameterName, TrIdType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the <c>p_tr_rule_id</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for <c>p_tr_rule_id</c> with type integer
        /// </returns>
        public DbParameter CreateTrRuleIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, TrRuleIdParameterName, TrRuleIdType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the <c>p_uncoded_value</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        /// The database command
        /// </param>
        /// <returns>
        ///     a new instance of database parameter for <c>p_uncoded_value</c> with type string
        /// </returns>
        public DbParameter CreateUncodedValueParameter(DbCommand command)
        {
            return this.CreateParameter(command, "p_uncoded_value", DbType.String, ParameterDirection.Input);
        }
    }
}