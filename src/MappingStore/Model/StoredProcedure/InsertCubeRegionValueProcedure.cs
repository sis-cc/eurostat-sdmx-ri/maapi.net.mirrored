// -----------------------------------------------------------------------
// <copyright file="InsertCubeRegionValueProcedure.cs" company="EUROSTAT">
//   Date Created : 2014-10-09
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    ///     The insert cube region procedure.
    /// </summary>
    public class InsertCubeRegionValueProcedure : OutputProcedureBase
    {
        /// <summary>
        ///     The stored procedure name
        /// </summary>
        private const string StoredProcedureName = "INSERT_CUBE_REGION_VALUE";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertCubeRegionValueProcedure" /> class.
        /// </summary>
        public InsertCubeRegionValueProcedure()
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Creates the cube region key value identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     the parameter value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateCubeRegionKeyValueIdParameter(DbCommand command, long value)
        {
            return this.CreateParameter(command, "p_cube_region_key_value_id", DbType.Int64, ParameterDirection.Input, value);
        }

        /// <summary>
        ///     Creates the include parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     the parameter value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateIncludeParameter(DbCommand command, bool value)
        {
            return this.CreateParameter(command, "p_include", DbType.Int32, ParameterDirection.Input, value ? 1 : 0);
        }

        /// <summary>
        ///     Creates the code identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     the parameter value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateMemberValueParameter(DbCommand command, string value)
        {
            return this.CreateParameter(command, "p_member_value", DbType.AnsiString, ParameterDirection.Input, value);
        }

        /// <summary>
        ///     Creates the code identifier parameter.
        /// </summary>
        /// <param name="command">
        ///     The command.
        /// </param>
        /// <param name="value">
        ///     the parameter value.
        /// </param>
        /// <returns>
        ///     The <see cref="DbParameter" />
        /// </returns>
        public DbParameter CreateIsCascadeValueParameter(DbCommand command, bool value)
        {
            return this.CreateParameter(command, "p_cascade_values", DbType.Int32, ParameterDirection.Input, value ? 1 : 0);
        }
    }
}