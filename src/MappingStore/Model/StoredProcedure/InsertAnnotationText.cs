// -----------------------------------------------------------------------
// <copyright file="InsertAnnotationText.cs" company="EUROSTAT">
//   Date Created : 2012-07-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     This class contains the stored procedure INSERT_LOCALISED_STRING name and parameter names and types
    /// </summary>
    public class InsertAnnotationText : OutputProcedureBase
    {
        /// <summary>
        ///     Gets the p_art_id parameter name
        /// </summary>
        public const string AnnIdParameterName = "p_ann_id";

        /// <summary>
        ///     Gets the p_language parameter name
        /// </summary>
        public const string LanguageParameterName = "p_language";

        /// <summary>
        ///     Gets the INSERT_LOCALISED_STRING stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_ANNOTATION_TEXT";

        /// <summary>
        ///     Gets the p_text parameter name
        /// </summary>
        public const string TextParameterName = "p_text";

        /// <summary>
        ///     Initializes a new instance of the <see cref="InsertAnnotationText" /> class.
        /// </summary>
        public InsertAnnotationText()
            : base(StoredProcedureName)
        {
        }

        /// <summary>
        ///     Gets the p_art_id parameter type
        /// </summary>
        public static DbType AnnIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        ///     Gets the p_language parameter type
        /// </summary>
        public static DbType LanguageType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Gets the p_text parameter type
        /// </summary>
        public static DbType TextType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        ///     Creates the <c>p_ann_id</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     a new instance of <c>DbParameter</c> for <c>p_art_id</c> with type <c>DbType.String</c>
        /// </returns>
        public DbParameter CreateAnnIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, AnnIdParameterName, AnnIdType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateLanguageParameter(command).Value = "en";

            return command;
        }

        /// <summary>
        ///     Creates the <c>p_language</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     a new instance of <c>DbParameter</c> for <c>p_language</c> with type <c>DbType.String </c>
        /// </returns>
        public DbParameter CreateLanguageParameter(DbCommand command)
        {
            return this.CreateParameter(command, LanguageParameterName, LanguageType, ParameterDirection.Input);
        }

        /// <summary>
        ///     Creates the <c>p_text</c> parameter for the specified <paramref name="command" /> . The parameter is added
        ///     <paramref name="command" />
        /// </summary>
        /// <param name="command">
        ///     The DB command
        /// </param>
        /// <returns>
        ///     a new instance of <c>DbParameter</c> for <c>p_text</c> with type <c>DbType.String</c>
        /// </returns>
        public DbParameter CreateTextParameter(DbCommand command)
        {
            return this.CreateParameter(command, TextParameterName, TextType, ParameterDirection.Input);
        }
    }
}