// -----------------------------------------------------------------------
// <copyright file="InsertRegistration.cs" company="EUROSTAT">
//   Date Created : 2017-11-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System.Data;
    using System.Data.Common;
    using Manager;
    using System;

    /// <summary>
    ///  This class contains the stored procedure INSERT_DATA_SOURCE name and parameter names and types
    /// </summary>
    public class InsertRegistration : OutputProcedureBase
    {

        /// <summary>
        /// The stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_N_REGISTRATION";

        /// <summary>
        /// The identifier parameter name
        /// </summary>
        public const string IdParameterName = "p_id";
        /// <summary>
        /// The valid from parameter name
        /// </summary>
        public const string ValidFromParameterName = "p_valid_from";
        /// <summary>
        /// The valid to parameter name
        /// </summary>
        public const string ValidToParameterName = "p_valid_to";
        /// <summary>
        /// The last updated parameter name
        /// </summary>
        public const string LastUpdatedParameterName = "p_last_updated";
        /// <summary>
        /// The index ts parameter name
        /// </summary>
        public const string IndexTsParameterName = "p_index_ts";
        /// <summary>
        /// The index ds parameter name
        /// </summary>
        public const string IndexDsParameterName = "p_index_ds";
        /// <summary>
        /// The index attributes parameter name
        /// </summary>
        public const string IndexAttributesParameterName = "p_index_attributes";
        /// <summary>
        /// The index reporting period parameter name
        /// </summary>
        public const string IndexReportingPeriodParameterName = "p_index_reporting_period";
        /// <summary>
        /// The pa identifier parameter name
        /// </summary>
        public const string PaIdParameterName = "p_pa_id";
        /// <summary>
        /// The datasource identifier parameter name
        /// </summary>
        public const string DatasourceIdParameterName = "p_datasource_id";

        /// <summary>
        /// Initializes a new instance of the <see cref="InsertRegistration"/> class.
        /// </summary>
        public InsertRegistration()
            : base(StoredProcedureName)
        {
        }

        /// <summary> Get the type of the <c>id</c> parameter/ </summary>
        public static DbType IdParameterType
        {
            get
            {
                return DbType.AnsiString;
            }
        }
        /// <summary> Get the type of the <c>valid_from</c> parameter/ </summary>
        public static DbType ValidFromParameterType
        {
            get
            {
                return DbType.DateTime;
            }
        }
        /// <summary> Get the type of the <c>valid_to</c> parameter/ </summary>
        public static DbType ValidToParameterType
        {
            get
            {
                return DbType.DateTime;
            }
        }
        /// <summary> Get the type of the <c>last_updated</c> parameter/ </summary>
        public static DbType LastUpdatedParameterType
        {
            get
            {
                return DbType.DateTime;
            }
        }
        /// <summary> Get the type of the <c>index_ts</c> parameter/ </summary>
        public static DbType IndexTsParameterType
        {
            get
            {
                return DbType.Int16;
            }
        }
        /// <summary> Get the type of the <c>index_ds</c> parameter/ </summary>
        public static DbType IndexDsParameterType
        {
            get
            {
                return DbType.Int16;
            }
        }
        /// <summary> Get the type of the <c>index_attributes</c> parameter/ </summary>
        public static DbType IndexAttributesParameterType
        {
            get
            {
                return DbType.Int16;
            }
        }
        /// <summary> Get the type of the <c>index_reporting_period</c> parameter/ </summary>
        public static DbType IndexReportingPeriodParameterType
        {
            get
            {
                return DbType.Int16;
            }
        }
        /// <summary> Get the type of the <c>pa_id</c> parameter/ </summary>
        public static DbType PaIdParameterType
        {
            get
            {
                return DbType.Int64;
            }
        }
        /// <summary> Get the type of the <c>datasource_id</c> parameter/ </summary>
        public static DbType DatasourceIdParameterType
        {
            get
            {
                return DbType.Int64;
            }
        }


        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>The <paramref name="database" /> instance may or may not be transactional.</remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateIdParameter(command).Value = DBNull.Value;
            this.CreateValidFromParameter(command).Value = DBNull.Value;
            this.CreateValidToParameter(command).Value = DBNull.Value;
            this.CreateLastUpdatedParameter(command).Value = DBNull.Value;
            this.CreateIndexTsParameter(command).Value = 0;
            this.CreateIndexDsParameter(command).Value = 0;
            this.CreateIndexAttributesParameter(command).Value = 0;
            this.CreateIndexReportingPeriodParameter(command).Value = 0;
            return command;
        }

        /// <summary> Create the <c>id</c> parameter </summary>
        /// <returns> The DB Command </returns>
        public DbParameter CreateIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, IdParameterName, IdParameterType, ParameterDirection.Input);
        }
        /// <summary> Create the <c>valid_from</c> parameter </summary>
        /// <returns> The DB Command </returns>
        public DbParameter CreateValidFromParameter(DbCommand command)
        {
            return this.CreateParameter(command, ValidFromParameterName, ValidFromParameterType, ParameterDirection.Input);
        }
        /// <summary> Create the <c>valid_to</c> parameter </summary>
        /// <returns> The DB Command </returns>
        public DbParameter CreateValidToParameter(DbCommand command)
        {
            return this.CreateParameter(command, ValidToParameterName, ValidToParameterType, ParameterDirection.Input);
        }
        /// <summary> Create the <c>last_updated</c> parameter </summary>
        /// <returns> The DB Command </returns>
        public DbParameter CreateLastUpdatedParameter(DbCommand command)
        {
            return this.CreateParameter(command, LastUpdatedParameterName, LastUpdatedParameterType, ParameterDirection.Input);
        }
        /// <summary> Create the <c>index_ts</c> parameter </summary>
        /// <returns> The DB Command </returns>
        public DbParameter CreateIndexTsParameter(DbCommand command)
        {
            return this.CreateParameter(command, IndexTsParameterName, IndexTsParameterType, ParameterDirection.Input);
        }
        /// <summary> Create the <c>index_ds</c> parameter </summary>
        /// <returns> The DB Command </returns>
        public DbParameter CreateIndexDsParameter(DbCommand command)
        {
            return this.CreateParameter(command, IndexDsParameterName, IndexDsParameterType, ParameterDirection.Input);
        }
        /// <summary> Create the <c>index_attributes</c> parameter </summary>
        /// <returns> The DB Command </returns>
        public DbParameter CreateIndexAttributesParameter(DbCommand command)
        {
            return this.CreateParameter(command, IndexAttributesParameterName, IndexAttributesParameterType, ParameterDirection.Input);
        }
        /// <summary> Create the <c>index_reporting_period</c> parameter </summary>
        /// <returns> The DB Command </returns>
        public DbParameter CreateIndexReportingPeriodParameter(DbCommand command)
        {
            return this.CreateParameter(command, IndexReportingPeriodParameterName, IndexReportingPeriodParameterType, ParameterDirection.Input);
        }
        /// <summary> Create the <c>pa_id</c> parameter </summary>
        /// <returns> The DB Command </returns>
        public DbParameter CreatePaIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, PaIdParameterName, PaIdParameterType, ParameterDirection.Input);
        }
        /// <summary> Create the <c>datasource_id</c> parameter </summary>
        /// <returns> The DB Command </returns>
        public DbParameter CreateDatasourceIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, DatasourceIdParameterName, DatasourceIdParameterType, ParameterDirection.Input);
        }

    }
}