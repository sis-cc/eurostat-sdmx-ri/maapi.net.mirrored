// -----------------------------------------------------------------------
// <copyright file="InsertMetadataAttribute.cs" company="EUROSTAT">
//   Date Created : 2017-04-13
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure
{
    using System;
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     This class contains the stored procedure INSERT_METADATA_ATTRIBUTE name and parameter names and types
    /// </summary>
    public class InsertMetadataAttribute : IdentifiableProcedureBase
    {
        /// <summary>
        ///     Gets the INSERT_METADATA_ATTRIBUTE stored procedure name
        /// </summary>
        public const string StoredProcedureName = "INSERT_METADATA_ATTRIBUTE";

        #region parameter names

        /// <summary> The <c>p_IS_PRESENTATIONAL</c> parameter name </summary>
        public const string IsPresentationalParameterName = "p_IS_PRESENTATIONAL";

        /// <summary> The <c>p_MAX_OCCURS</c> parameter name </summary>
        public const string MaxOccursParameterName = "p_MAX_OCCURS";

        /// <summary> The <c>p_MIN_OCCURS</c> parameter name </summary>
        public const string MinOccursParameterName = "p_MIN_OCCURS";

        /// <summary> The <c>p_RS_ID</c> parameter name </summary>
        public const string RsIDParameterName = "p_RS_ID";

        /// <summary>
        /// The parent path parameter name
        /// </summary>
        public const string ParentPathParameterName = "p_PARENT_PATH";

        /// <summary>
        /// The MSD id parameter name
        /// </summary>
        public const string MsdIdParameterName = "p_MSD_ID";

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="InsertMetadataAttribute"/> class.
        /// </summary>
        public InsertMetadataAttribute()
            : base(StoredProcedureName)
        {
        }


        #region parameter types

        /// <summary> Gets the type of <c>p_IS_PRESENTATIONAL</c> parameter </summary>
        public static DbType IsPresentationalType
        {
            get
            {
                return DbType.Int16;
            }
        }

        /// <summary> Gets the type of <c>p_MAX_OCCURS</c> parameter </summary>
        public static DbType MaxOccursType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary> Gets the type of <c>p_MIN_OCCURS</c> parameter </summary>
        public static DbType MinOccursType
        {
            get
            {
                return DbType.Int32;
            }
        }

        /// <summary> Gets the type of <c>p_RS_ID</c> parameter </summary>
        public static DbType RsIDType
        {
            get
            {
                return DbType.Int64;
            }
        }

        /// <summary>
        /// The parent path parameter type
        /// </summary>
        public static DbType ParentPathType
        {
            get
            {
                return DbType.String;
            }
        }

        /// <summary>
        /// The MSD id parameter type
        /// </summary>
        public static DbType MsdIdType
        {
            get
            {
                return DbType.Int64;
            }
        }

        #endregion

        /// <summary>
        ///     Creates a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </summary>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <returns>
        ///     a new instance of a <see cref="DbCommand" /> for the stored procedure with the specified
        ///     <paramref name="database" />.
        /// </returns>
        /// <remarks>
        ///     The <paramref name="database" /> instance may or may not be transactional.
        /// </remarks>
        public override DbCommand CreateCommandWithDefaults(Database database)
        {
            var command = base.CreateCommandWithDefaults(database);
            this.CreateMaxOccursParameter(command).Value = DBNull.Value;
            this.CreateMinOccursParameter(command).Value = DBNull.Value;
            this.CreateIsPresentationalParameter(command).Value = DBNull.Value;
            this.CreateParentPathParameter(command).Value = DBNull.Value;
            return command;
        }

        #region create parameters

        /// <summary>
        /// Creates the parameter for <c>p_IS_PRESENTATIONAL</c>and adds it to the specified <paramref name="command"/>.
        /// </summary>
        /// <param name="command">
        /// The database command.
        /// </param>
        /// <returns>
        /// A new instance of the database parameter
        /// </returns>
        public DbParameter CreateIsPresentationalParameter(DbCommand command)
        {
            return this.CreateParameter(command, IsPresentationalParameterName, IsPresentationalType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter for <c>p_MAX_OCCURS</c>and adds it to the specified <paramref name="command"/>.
        /// </summary>
        /// <param name="command">
        /// The database command.
        /// </param>
        /// <returns>
        /// A new instance of the database parameter
        /// </returns>
        public DbParameter CreateMaxOccursParameter(DbCommand command)
        {
            return this.CreateParameter(command, MaxOccursParameterName, MaxOccursType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter for <c>p_MIN_OCCURS</c>and adds it to the specified <paramref name="command"/>.
        /// </summary>
        /// <param name="command">
        /// The database command.
        /// </param>
        /// <returns>
        /// A new instance of the database parameter
        /// </returns>
        public DbParameter CreateMinOccursParameter(DbCommand command)
        {
            return this.CreateParameter(command, MinOccursParameterName, MinOccursType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter for <c>p_RS_ID</c>and adds it to the specified <paramref name="command"/>.
        /// </summary>
        /// <param name="command">
        /// The database command.
        /// </param>
        /// <returns>
        /// A new instance of the database parameter
        /// </returns>
        public DbParameter CreateRsIDParameter(DbCommand command)
        {
            return this.CreateParameter(command, RsIDParameterName, RsIDType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter for <c>p_parent_path</c>.
        /// </summary>
        /// <param name="command">The db command to attach the parameter to.</param>
        /// <returns>The parameter</returns>
        public DbParameter CreateParentPathParameter(DbCommand command)
        {
            return this.CreateParameter(command, ParentPathParameterName, ParentPathType, ParameterDirection.Input);
        }

        /// <summary>
        /// Creates the parameter for <c>p_msd_id</c>.
        /// </summary>
        /// <param name="command">The db command to attach the parameter to.</param>
        /// <returns>The parameter</returns>
        public DbParameter CreateMsdIdParameter(DbCommand command)
        {
            return this.CreateParameter(command, MsdIdParameterName, MsdIdType, ParameterDirection.Input);
        }

        #endregion
    }
}