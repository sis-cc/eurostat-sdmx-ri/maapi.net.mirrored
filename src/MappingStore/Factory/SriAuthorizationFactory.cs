// -----------------------------------------------------------------------
// <copyright file="SriAuthorizationFactory.cs" company="EUROSTAT">
//   Date Created : 2017-06-02
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;

    /// <summary>
    /// Initializes a new instance of the <see cref="SriAuthorizationEngine"/> if the principal is <see cref="MappingAssistantUser"/>
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Factory.IDataflowAuthorizationFactory" />
    public class SriAuthorizationFactory : IDataflowAuthorizationFactory
    {
        /// <summary>
        /// The retrieval engine
        /// </summary>
        private readonly IRetrievalEngine<ICategorisationMutableObject> _retrievalEngine;

        /// <summary>
        /// The urn retriever
        /// </summary>
        private readonly Func<SdmxStructureType, string, IDictionary<long, string>> _urnRetriever;

        /// <summary>
        /// Initializes a new instance of the <see cref="SriAuthorizationFactory"/> class.
        /// </summary>
        /// <param name="urnRetriever">The urn retriever.</param>
        /// <param name="retrievalEngine">The retrieval engine.</param>
        public SriAuthorizationFactory(Func<SdmxStructureType, string, IDictionary<long, string>> urnRetriever, IRetrievalEngine<ICategorisationMutableObject> retrievalEngine)
        {
            this._urnRetriever = urnRetriever;
            this._retrievalEngine = retrievalEngine;
        }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sdmxsource.Extension.Engine.IDataflowAuthorizationEngine"/>.
        /// </returns>
        public IDataflowAuthorizationEngine GetEngine(IPrincipal principal)
        {
            MappingAssistantUser mauser = principal as MappingAssistantUser;
            if (mauser == null)
            {
                return null;
            }

            return new SriAuthorizationEngine(mauser, this._urnRetriever, this._retrievalEngine);
        }
    }
}