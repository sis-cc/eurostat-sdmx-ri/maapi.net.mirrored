﻿// -----------------------------------------------------------------------
// <copyright file="IRetrievalEngineContainerFactory.cs" company="EUROSTAT">
//   Date Created : 2013-04-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Factory
{
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// The retrieval engine container factory.
    /// </summary>
    public class RetrievalEngineContainerFactory : IRetrievalEngineContainerFactory
    {
        /// <summary>
        /// Gets the retrieval engine container.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>An implementation of an <see cref="IRetrievalEngineContainer"/> interface.</returns>
        public IRetrievalEngineContainer GetRetrievalEngineContainer(Database database)
        {
            return new RetrievalEngineContainer(database);
        }
    }
}
