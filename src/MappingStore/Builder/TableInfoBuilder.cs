// -----------------------------------------------------------------------
// <copyright file="TableInfoBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-04-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;

    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The table info builder.
    /// </summary>
    public class TableInfoBuilder : IBuilder<TableInfo, SdmxStructureEnumType>
    {
        /// <summary>
        ///     Builds an <see cref="TableInfo" /> from the specified <paramref name="buildFrom" />
        /// </summary>
        /// <param name="buildFrom">
        ///     An <see cref="SdmxStructureEnumType" /> to build the output object from
        /// </param>
        /// <returns>
        ///     an <see cref="TableInfo" /> from the specified <paramref name="buildFrom" />
        /// </returns>
        public TableInfo Build(SdmxStructureEnumType buildFrom)
        {
            TableInfo tableInfo = null;
            switch (buildFrom)
            {
                case SdmxStructureEnumType.Categorisation:
                case SdmxStructureEnumType.HierarchicalCodelist:
                case SdmxStructureEnumType.Msd:
                case SdmxStructureEnumType.MetadataFlow:
                case SdmxStructureEnumType.StructureSet:
                case SdmxStructureEnumType.ProvisionAgreement:
                    tableInfo = new TableInfo(buildFrom)
                    {
                        Table = "ARTEFACT",
                        PrimaryKey = "ART_ID"
                    };

                    break;
                case SdmxStructureEnumType.CodeList:
                case SdmxStructureEnumType.CategoryScheme:
                case SdmxStructureEnumType.ConceptScheme:
                case SdmxStructureEnumType.OrganisationUnitScheme:
                case SdmxStructureEnumType.AgencyScheme:
                case SdmxStructureEnumType.DataConsumerScheme:
                case SdmxStructureEnumType.DataProviderScheme:
                    tableInfo = new TableInfo(buildFrom)
                    {
                        Table = "ITEM_SCHEME",
                        PrimaryKey = "ITEM_SCHEME_ID",
                        ExtraFields = ", IS_PARTIAL"
                    };
                    break;
                case SdmxStructureEnumType.Dataflow:
                    tableInfo = DataflowConstant.TableInfo;
                    break;
                case SdmxStructureEnumType.Dsd:
                    tableInfo = DsdConstant.TableInfo;
                    break;
                case SdmxStructureEnumType.ContentConstraint:
                    tableInfo = ContentConstraintConstant.TableInfo;
                    break;
            }

            return tableInfo;
        }
    }
}