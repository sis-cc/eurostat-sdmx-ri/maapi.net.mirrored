// -----------------------------------------------------------------------
// <copyright file="IdentifiableSqlQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-11-08
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;

    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Util;

    /// <summary>
    /// The identifiable artefact SQL query builder.
    /// </summary>
    internal class IdentifiableSqlQueryBuilder
    {
        /// <summary>
        /// The system identifier field name
        /// </summary>
        public const string SysIdFieldName = "SYSID";

        /// <summary>
        /// Builds the specified identifier field name.
        /// </summary>
        /// <param name="idFieldName">Name of the identifier field.</param>
        /// <param name="parentTable">The parent table.</param>
        /// <param name="childrenToParentTables">The children to parent tables.</param>
        /// <returns>
        /// The <see cref="SqlQueryInfo" />.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">childrenToParentTables is null</exception>
        public SqlQueryInfo Build(string idFieldName, TableInfo parentTable, params ItemTableInfo[] childrenToParentTables)
        {
            if (!ObjectUtil.ValidArray(childrenToParentTables))
            {
                throw new ArgumentNullException("childrenToParentTables");
            }

            var queue = new Queue<ItemTableInfo>(childrenToParentTables);
            var firstTable = queue.Dequeue();
            var sb = new StringBuilder();
            sb.AppendFormat(
                CultureInfo.InvariantCulture,
                "SELECT P.{0} as {1}, P.{2} {3} FROM {4} P INNER JOIN {5} T ON T.{6} = P.{0} ",
                parentTable.PrimaryKey,
                SysIdFieldName,
                idFieldName,
                GetParentItemString(firstTable),
                parentTable.Table,
                firstTable.Table,
                firstTable.PrimaryKey);

            string lastForeignKey = firstTable.ForeignKey;
            string lastTableAlias = "T";
            int count = 0;
            while (queue.Count > 0)
            {
                var current = queue.Dequeue();
                var currentTableAlias = string.Format(CultureInfo.InvariantCulture, "T{0}", count++);
                sb.AppendLine();
                sb.AppendFormat(
                    CultureInfo.InvariantCulture,
                    " INNER JOIN {0} {1} ON {2}.{3} = {1}.{4} ",
                    current.Table,
                    currentTableAlias,
                    lastTableAlias,
                    lastForeignKey,
                    current.PrimaryKey);
                lastTableAlias = currentTableAlias;
                lastForeignKey = current.ForeignKey;
            }

            sb.AppendLine();
            return new SqlQueryInfo()
            {
                QueryFormat =
                               sb.AppendFormat(
                                   CultureInfo.InvariantCulture,
                                   " where {0}.{1} = {{0}}",
                                   lastTableAlias,
                                   lastForeignKey).ToString(),
                WhereStatus = WhereState.And
            };
        }

        private string GetParentItemString(ItemTableInfo tableInfo)
        {
            return string.IsNullOrWhiteSpace(tableInfo.ParentItem) ? string.Empty : ",T." + tableInfo.ParentItem;
        }

        /// <summary>
        /// Builds the specified identifier field name.
        /// </summary>
        /// <param name="idFieldName">Name of the identifier field.</param>
        /// <param name="childrenToParentTables">The children to parent tables.</param>
        /// <returns>
        /// The <see cref="SqlQueryInfo" />.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">childrenToParentTables is null</exception>
        public SqlQueryInfo Build(string idFieldName, params ItemTableInfo[] childrenToParentTables)
        {
            if (!ObjectUtil.ValidArray(childrenToParentTables))
            {
                throw new ArgumentNullException("childrenToParentTables");
            }

            var queue = new Queue<ItemTableInfo>(childrenToParentTables);
            var firstTable = queue.Dequeue();
            var sb = new StringBuilder();
            sb.AppendFormat(CultureInfo.InvariantCulture, "SELECT T.{0} as {3}, T.{1} FROM {2} T ", firstTable.PrimaryKey, idFieldName, firstTable.Table, SysIdFieldName);

            string lastForeignKey = firstTable.ForeignKey;
            string lastTableAlias = "T";
            int count = 0;
            while (queue.Count > 0)
            {
                var current = queue.Dequeue();
                var currentTableAlias = string.Format(CultureInfo.InvariantCulture, "T{0}", count++);
                sb.AppendLine();
                sb.AppendFormat(
                    CultureInfo.InvariantCulture,
                    " INNER JOIN {0} {1} ON {2}.{3} = {1}.{4} ",
                    current.Table,
                    currentTableAlias,
                    lastTableAlias,
                    lastForeignKey,
                    current.PrimaryKey);
                lastTableAlias = currentTableAlias;
                lastForeignKey = current.ForeignKey;
            }

            sb.AppendLine();
            return new SqlQueryInfo()
                       {
                           QueryFormat =
                               sb.AppendFormat(
                                   CultureInfo.InvariantCulture,
                                   " where {0}.{1} = {{0}}",
                                   lastTableAlias,
                                   lastForeignKey).ToString(),
                           WhereStatus = WhereState.And
                       };
        }
    }
}