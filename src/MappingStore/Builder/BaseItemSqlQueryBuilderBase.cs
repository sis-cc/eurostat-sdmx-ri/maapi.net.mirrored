// -----------------------------------------------------------------------
// <copyright file="BaseItemSqlQueryBuilderBase.cs" company="EUROSTAT">
//   Date Created : 2017-04-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Globalization;

    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    /// <summary>
    /// The base item SQL query builder base.
    /// </summary>
    internal class BaseItemSqlQueryBuilderBase : ISqlQueryInfoBuilder<ItemTableInfo>
    {
        /// <summary>
        /// The _SQL format.
        /// </summary>
        private readonly string _sqlFormat;

        /// <summary>
        ///     The mapping store DB.
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        ///     The _order by.
        /// </summary>
        private readonly string _orderBy;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseItemSqlQueryBuilderBase" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">The mapping store DB.</param>
        /// <param name="orderBy">The order by</param>
        /// <param name="sqlFormat">The SQL format.</param>
        protected BaseItemSqlQueryBuilderBase(Database mappingStoreDb, string orderBy, string sqlFormat)
        {
            this._mappingStoreDb = mappingStoreDb;
            this._orderBy = orderBy;
            this._sqlFormat = sqlFormat;
        }

        /// <summary>
        /// Builds an <see cref="SqlQueryInfo"/> from the specified <paramref name="tableInfo"/>
        /// </summary>
        /// <param name="tableInfo">
        /// An Object to build the output object from
        /// </param>
        /// <returns>
        /// an <see cref="SqlQueryInfo"/> build from the specified <paramref name="tableInfo"/>
        /// </returns>
        public SqlQueryInfo Build(ItemTableInfo tableInfo)
        {
            if (tableInfo == null)
            {
                throw new ArgumentNullException("tableInfo");
            }

            var paramId = this._mappingStoreDb.BuildParameterName(ParameterNameConstants.IdParameter);
            var parentColumn = string.Empty;
            if (!string.IsNullOrEmpty(tableInfo.ParentItem))
            {
                parentColumn = string.Format(CultureInfo.InvariantCulture, ", T.{0} as PARENT ", tableInfo.ParentItem);
            }

            if (!string.IsNullOrEmpty(tableInfo.ExtraFields))
            {
                parentColumn = string.Format(CultureInfo.InvariantCulture, "{0} {1}", parentColumn, tableInfo.ExtraFields);
            }

            var query = string.Format(CultureInfo.InvariantCulture, this._sqlFormat, tableInfo.PrimaryKey, parentColumn, tableInfo.Table, tableInfo.ForeignKey, paramId);
            return new SqlQueryInfo { QueryFormat = query, OrderBy = this._orderBy };
        }
    }
}