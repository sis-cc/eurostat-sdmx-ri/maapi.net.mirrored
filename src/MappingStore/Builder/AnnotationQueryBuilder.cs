// -----------------------------------------------------------------------
// <copyright file="AnnotationQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2014-11-05
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Globalization;

    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// Annotation Query Builder
    /// </summary>
    /// <seealso cref="Estat.Sri.MappingStoreRetrieval.Builder.ISqlQueryInfoBuilder{TableInfo}" />
    /// <seealso cref="Estat.Sri.MappingStoreRetrieval.Builder.ISqlQueryInfoBuilder{ItemTableInfo}" />
    internal class AnnotationQueryBuilder : ISqlQueryInfoBuilder<TableInfo>, ISqlQueryInfoBuilder<ItemTableInfo>
    {
        /// <summary>
        ///     Builds the specified artefact table information.
        /// </summary>
        /// <param name="artefactTableInfo">The artefact table information.</param>
        /// <returns>THe <see cref="SqlQueryInfo" /></returns>
        public SqlQueryInfo Build(TableInfo artefactTableInfo)
        {
            if (artefactTableInfo == null)
            {
                throw new ArgumentNullException("artefactTableInfo");
            }

            return new SqlQueryInfo
                       {
                           QueryFormat =
                               string.Format(
                                   CultureInfo.InvariantCulture, 
                                   "{0} WHERE AA.ART_ID = {{0}}",
                                   AnnotationConstants.ArtefactAnnotationQuery), 
                           WhereStatus = WhereState.And
                       };
        }

        /// <summary>
        ///     Builds the specified nameable table information.
        /// </summary>
        /// <param name="nameableTableInfo">The nameable table information.</param>
        /// <returns>THe <see cref="SqlQueryInfo" /></returns>
        public SqlQueryInfo Build(ItemTableInfo nameableTableInfo)
        {
            if (nameableTableInfo == null)
            {
                throw new ArgumentNullException("nameableTableInfo");
            }

            // we have some non-nameable and/or special cases...
            string annotationRelationTable;
            string annotationRelationTableForeignKey;
            switch (nameableTableInfo.StructureType)
            {
                case SdmxStructureEnumType.Component:
                case SdmxStructureEnumType.MetadataAttribute:
                    annotationRelationTable = AnnotationConstants.ComponentAnnotationTable;
                    annotationRelationTableForeignKey = "COMP_ID";
                    break;
                case SdmxStructureEnumType.Group:
                    annotationRelationTable = AnnotationConstants.DsdGroupAnnotationTable;
                    annotationRelationTableForeignKey = "GR_ID";
                    break;
                case SdmxStructureEnumType.Level:
                case SdmxStructureEnumType.Hierarchy:
                case SdmxStructureEnumType.HierarchicalCode:
                case SdmxStructureEnumType.ReportStructure:
                case SdmxStructureEnumType.MetadataTarget:
                case SdmxStructureEnumType.CodeListMap:
                case SdmxStructureEnumType.StructureMap:
                    annotationRelationTable = AnnotationConstants.OtherAnnotationTable;
                    annotationRelationTableForeignKey = "OTH_ID";
                    break;
                default:
                    throw new SdmxInternalServerException("ERROR should reach here for " + nameableTableInfo.StructureType);
            }

            var format = string.Format(
                CultureInfo.InvariantCulture, 
                AnnotationConstants.AnnotationQuery, 
                nameableTableInfo.PrimaryKey, 
                annotationRelationTable, 
                nameableTableInfo.Table, 
                annotationRelationTableForeignKey);
            return new SqlQueryInfo
                       {
                           QueryFormat =
                               string.Format(
                                   CultureInfo.InvariantCulture, 
                                   "{0} WHERE T.{1} = {{0}}", 
                                   format, 
                                   nameableTableInfo.ForeignKey), 
                           WhereStatus = WhereState.And
                       };
        }
    }
}