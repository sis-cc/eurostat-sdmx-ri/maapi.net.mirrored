// -----------------------------------------------------------------------
// <copyright file="IArtefactSqlQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2022-06-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System.Data.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    /// <summary>
    /// Used for creating all sql queries related to retrieving artefacts.
    /// </summary>
    public interface IArtefactSqlQueryBuilder
    {
        /// <summary>
        /// Builds the sql query for retrieving artefacts specified by the <paramref name="structureQuery"/>.
        /// </summary>
        /// <param name="structureQuery">the query information used for retrieving the desired artefacts</param>
        /// <returns>An <see cref="DbCommand"/> to execute.</returns>
        DbCommand BuildRetrievalQuery(ICommonStructureQuery structureQuery);

        /// <summary>
        /// Builds the sql query for retrieving the references of the artefacts specified by the <paramref name="structureQuery"/>.
        /// </summary>
        /// <param name="structureQuery">the query information used for retrieving the artefacts whose references we desire</param>
        /// <returns>An <see cref="DbCommand"/> to execute.</returns>
        DbCommand BuildReferencesQuery(ICommonStructureQuery structureQuery);

        /// <summary>
        /// Builds the sql query for retrieving the parents of the artefacts specified by the <paramref name="structureQuery"/>.
        /// </summary>
        /// <param name="structureQuery">the query information used for retrieving artefacts whose parents we desire</param>
        /// <param name="parentArtefactType">the type of the parents we wish to retrieve (an artefact may have parents of different types)</param>
        /// <returns>An <see cref="DbCommand"/> to execute.</returns>
        DbCommand BuildParentsRetrievalQuery(ICommonStructureQuery structureQuery, SdmxStructureType parentArtefactType);

        /// <summary>
        /// Builds the sql query for retrieving the references of the parents of the artefacts specified by the structureQuery
        /// </summary>
        /// <param name="structureQuery">the query information used for retrieving artefacts whose parents' references we desire</param>
        /// <returns>An <see cref="DbCommand"/> to execute.</returns>
        DbCommand BuildParentsReferencesQuery(ICommonStructureQuery structureQuery);

        /// <summary>
        /// Builds the sql query for retrieving the children of the artefacts specified by the structureQuery
        /// </summary>
        /// <param name="structureQuery">the query information used for retrieving artefacts whose children we desire</param>
        /// <param name="childArtefactType">the type of the children we wish to retrieve (an artefact may have children of different types)</param>
        /// <returns>An <see cref="DbCommand"/> to execute.</returns>
        DbCommand BuildChildrenRetrievalQuery(ICommonStructureQuery structureQuery, SdmxStructureType childArtefactType);

        /// <summary>
        /// Builds the sql query for retrieving the references of the children of the artefacts specified by the structureQuery
        /// </summary>
        /// <param name="structureQuery">the query information used for retrieving artefacts whose children' references we desire</param>
        /// <returns>An <see cref="DbCommand"/> to execute.</returns>
        DbCommand BuildChildrenReferencesQuery(ICommonStructureQuery structureQuery);

        /// <summary>
        /// Builds a <see cref="DbCommand"/> that finds where an artefact, described in <paramref name="structureQuery"/>, is used from.
        /// </summary>
        /// <param name="structureQuery">The structure query for the artefact.</param>
        /// <returns>The <see cref="DbCommand"/> to execute.</returns>
        DbCommand BuildResolveParentReferences(ICommonStructureQuery structureQuery);
    }
}
