// -----------------------------------------------------------------------
// <copyright file="ArtefactCommandBuilder7.cs" company="EUROSTAT">
//   Date Created : 2022-06-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using System.Text;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Builds the sql commands for retrieving artefacts.
    /// Compatible to MSDB 7.0
    /// </summary>
    public class ArtefactCommandBuilder : IArtefactSqlQueryBuilder
    {
        /// <summary>
        ///     The mapping store DB.
        /// </summary>
        protected readonly Database mappingStoreDb;

        private TableInfo _tableInfo;

        private string _orderBy;

        private readonly List<SdmxStructureEnumType> _structureTypes = new List<SdmxStructureEnumType>();

        #region SQL query formats

        private static readonly string ORDER_BY = " ORDER BY A.ART_ID ";

        /// <summary>
        /// The common select statement.
        /// No format parameters.
        /// </summary>
        private static readonly string COMMON_SELECT = "SELECT A.ART_ID as SYSID, AB.ID, AB.AGENCY, AB.ARTEFACT_TYPE, " +
                " A.VERSION1, A.VERSION2, A.VERSION3, A.EXTENSION, A.VALID_FROM, A.VALID_TO, A.URI, A.SERVICE_URL, " +
                "A.STRUCTURE_URL, A.IS_STUB, " +
                "LN.TEXT, LN.LANGUAGE, LN.IS_NAME ";

        /// <summary>
        /// From ARTEFACT_BASE statement.
        /// No format parameters.
        /// </summary>
        private static readonly string SQL_FROM_BASE_TABLES = " FROM ARTEFACT_BASE AB " +
                "INNER JOIN N_ARTEFACT A ON A.ART_BASE_ID = AB.ART_BASE_ID";

        private static readonly string SQL_FROM_LS = " LEFT OUTER JOIN LOCALISED_STRING_ART LN ON LN.ART_ID = A.ART_ID";

        /// <summary>
        /// No format parameters.
        /// </summary>
        private static readonly string SQL_FROM_TABLES = SQL_FROM_BASE_TABLES + SQL_FROM_LS;

        /// <summary>
        /// For cases where there is no table.
        /// No format parameters.
        /// </summary>
        private static readonly string SQL_QUERY_SIMPLE = COMMON_SELECT + SQL_FROM_TABLES;

        private static readonly string SQL_QUERY_JUST_ART_ID = " select A.ART_ID " + SQL_FROM_BASE_TABLES;

        private static readonly string SQL_QUERY_FORMAT_FOR_JUST_ART_ID = " select A.ART_ID " + SQL_FROM_BASE_TABLES + 
                " LEFT OUTER JOIN {0} T ON T.{1} = A.ART_ID";

        private static readonly string LATEST_WHERE_CLAUSE = " A.VERSION1 >= 0 " +
                "    and not exists ( " +
                "        select 1 " +
                "        from N_ARTEFACT LT " +
                "        where LT.ART_BASE_ID = A.ART_BASE_ID " +
                "            and LT.ART_ID != A.ART_ID " +
                "            and ( " +
                "                LT.VERSION1 > A.VERSION1 " +
                "                or ( " +
                "                    LT.VERSION1 = A.VERSION1 " +
                "                    and ( " +
                "                        LT.VERSION2 > A.VERSION2 " +
                "                        or ( " +
                "                            LT.VERSION2 = A.VERSION2 " +
                "                            and LT.VERSION3 > A.VERSION3 " +
                "                        ) " +
                "                    ) " +
                "                ) " +
                "            )" +
                "       )";

        private static readonly string LATEST_STABLE_WHERE_CLAUSE = " A.VERSION1 >= 0 " +
               "    and not exists ( " +
               "        select 1 " +
               "        from N_ARTEFACT LT " +
               "        where LT.ART_BASE_ID = A.ART_BASE_ID " +
               "            and LT.ART_ID != A.ART_ID " +
               "            and LT.EXTENSION is null " +
               "            and ( " +
               "                LT.VERSION1 > A.VERSION1 " +
               "                or ( " +
               "                    LT.VERSION1 = A.VERSION1 " +
               "                    and ( " +
               "                        LT.VERSION2 > A.VERSION2 " +
               "                        or ( " +
               "                            LT.VERSION2 = A.VERSION2 " +
               "                            and LT.VERSION3 > A.VERSION3 " +
               "                        ) " +
               "                    ) " +
               "                ) " +
               "              ) " +
               "            )";


        protected static readonly string STABLE_WHERE_CLAUSE = " A.EXTENSION is null ";

        /// <summary>
        /// Get artefact readonly status.
        /// </summary>
        private static readonly string SQL_QUERY_STATUS = "select A.ART_ID, A.ART_BASE_ID, A.EXTENSION, A.IS_STUB " + SQL_FROM_BASE_TABLES;

        /// <summary>
        /// Resolve the parents.
        /// Format parameters up to 0.
        /// </summary>
        private static readonly string SQL_PARENT_REF = "select AR.ART_ID, ABR.ID, ABR.AGENCY, ABR.ARTEFACT_TYPE,\n" +
                " AR.VERSION1, AR.VERSION2, AR.VERSION3, AR.EXTENSION \n" +
                " FROM ARTEFACT_BASE ABR \n" +
                "INNER JOIN N_ARTEFACT AR ON AR.ART_BASE_ID = ABR.ART_BASE_ID\n" +
                " INNER JOIN REF_CHILDREN rc ON rc.SOURCE_ARTEFACT  = AR.ART_ID " +
                "where rc.ART_ID in ({0})";

        /// <summary>
        /// Format parameters up to 0.
        /// </summary>
        private static readonly string SQL_PARENT_REF_JUST_ART_ID = "select AR.ART_ID FROM N_ARTEFACT AR " +
                " INNER JOIN REF_CHILDREN rc ON rc.SOURCE_ARTEFACT  = AR.ART_ID " +
                "where rc.ART_ID in ({0})";

        /// <summary>
        /// Format parameters up to 0.
        /// </summary>
        private static readonly string SQL_CHILDREN_REF_JUST_ART_ID = "select AR.ART_ID FROM N_ARTEFACT AR " +
                " INNER JOIN REF_CHILDREN rc ON rc.ART_ID = AR.ART_ID " +
                "where rc.SOURCE_ARTEFACT in ({0})";

        /// <summary>
        /// SQL Query for references, this just gets the list of references e.g. the codelist reference for a component
        /// Format parameters up to 0.
        /// </summary>
        private static readonly string SQL_REFERENCES = "select abr.ID, abr.AGENCY , abr.ARTEFACT_TYPE," +
                " sr.MAJOR , sr.MINOR , sr.PATCH , sr.EXTENSION , sr.WILDCARD_POS , sr.TARGET_CHILD_FULL_ID , \n" +
                "rs.SOURCE_ARTEFACT , rs.SOURCE_CHILD_FULL_ID , rs.REF_TYPE \n" +
                "from STRUCTURE_REF sr \n" +
                "inner join ARTEFACT_BASE abr on abr.ART_BASE_ID = sr.TARGET_ARTEFACT \n" +
                "inner join REFERENCE_SOURCE rs on rs.REF_SRC_ID  = sr.REF_SRC_ID \n" +
                "where rs.SOURCE_ARTEFACT in ({0})";

        /// <summary>
        /// For cases where there is a specific table for artefacts e.g. DATAFLOW or also ITEM_SCHEME
        /// Format parameters up to 2.
        /// </summary>
        private static readonly string SQL_QUERY_FORMAT = COMMON_SELECT +
                " , {2} " +
                SQL_FROM_TABLES +
                // Using LEFT OUTER JOIN to get STUBS in one query
                " LEFT OUTER JOIN {1} T ON T.{0} = A.ART_ID ";

        /// <summary>
        /// For cases where there is a specific table for artefacts e.g. DATAFLOW or also ITEM_SCHEME
        /// </summary>
        private static readonly string SQL_QUERY_FORMAT_FOR_PARENTS = SQL_QUERY_FORMAT +
                " INNER JOIN REF_CHILDREN rc ON rc.SOURCE_ARTEFACT  = A.ART_ID " +
                "where AB.ARTEFACT_TYPE = '{3}' AND rc.ART_ID in ({4})";

        /// <summary>
        /// For cases where there is a specific table for artefacts e.g. DATAFLOW or also ITEM_SCHEME
        /// </summary>
        private static readonly string SQL_QUERY_FORMAT_FOR_CHILDREN = SQL_QUERY_FORMAT +
                " INNER JOIN REF_CHILDREN rc ON rc.ART_ID = A.ART_ID " +
                "where AB.ARTEFACT_TYPE = '{3}' AND rc.SOURCE_ARTEFACT in ({4})";

        /// <summary>
        /// * Resolve the parent maintainables
        /// </summary>
        private static readonly string SQL_QUERY_SIMPLE_FOR_PARENTS = SQL_QUERY_SIMPLE +
                " INNER JOIN REF_CHILDREN rc ON rc.SOURCE_ARTEFACT  = A.ART_ID " +
                "where AB.ARTEFACT_TYPE = '{0}' AND rc.ART_ID in ({1})";

        /// <summary>
        /// * Resolve the children maintainables
        /// </summary>
        private static readonly string SQL_QUERY_SIMPLE_FOR_CHILDREN = SQL_QUERY_SIMPLE +
                " INNER JOIN REF_CHILDREN rc ON rc.ART_ID = A.ART_ID " +
                "where AB.ARTEFACT_TYPE = '{0}' AND rc.SOURCE_ARTEFACT in ({1})";

        private static readonly string[] versionQueriesForLatestWildcards = {
            "(A.VERSION1 >= {0})",
            "(A.VERSION1 = {0} and A.VERSION2 >= {1})",
            "(A.VERSION1 = {0} and A.VERSION2 = {1} and VERSION3 >= {2})"
        };

        #endregion

        /// <summary>
        ///     Initializes a new instance of the <see cref="ArtefactCommandBuilder" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="mappingStoreDb" /> is null
        /// </exception>
        public ArtefactCommandBuilder(Database mappingStoreDb)
        {
            if (mappingStoreDb == null)
            {
                throw new ArgumentNullException("mappingStoreDb");
            }

            this.mappingStoreDb = mappingStoreDb;
        }

        #region Interface implementation

        /// <inheritdoc/>
        public DbCommand BuildChildrenReferencesQuery(ICommonStructureQuery structureQuery)
        {
            if (structureQuery == null)
            {
                throw new ArgumentNullException(nameof(structureQuery));
            }

            DbCommandBuilder sqlCommand = GetSqlCommandWithParameters(structureQuery, SQL_QUERY_JUST_ART_ID);
            string sqlString = string.Format(SQL_CHILDREN_REF_JUST_ART_ID, sqlCommand.GetCommandQuery());
            sqlString = string.Format(SQL_REFERENCES, sqlString);

            return this.mappingStoreDb.GetSqlStringCommand(sqlString, sqlCommand.DbParameters);
        }

        /// <inheritdoc/>
        public DbCommand BuildChildrenRetrievalQuery(ICommonStructureQuery structureQuery, SdmxStructureType childArtefactType)
        {
            if (structureQuery == null)
            {
                throw new ArgumentNullException(nameof(structureQuery));
            }

            string queryFormat;
            if (_tableInfo != null && _tableInfo.ExtraFields.Any())
            {
                string extraFields = "T." + string.Join(", T.", _tableInfo.GetExtraFieldsList());
                queryFormat = string.Format(SQL_QUERY_FORMAT_FOR_CHILDREN, _tableInfo.PrimaryKey, _tableInfo.Table, extraFields,
                        childArtefactType.UrnClass,
                        //this is string placeholder for the "enclose" command
                        "{0}");
            }
            else
            {
                queryFormat = string.Format(SQL_QUERY_SIMPLE_FOR_CHILDREN,
                        childArtefactType.UrnClass,
                        //this is string placeholder for the "enclose" command
                        "{0}");
            }

            DbCommandBuilder sqlCommand = GetSqlCommandWithParameters(structureQuery, SQL_QUERY_JUST_ART_ID);
            string sqlString = string.Format(queryFormat, sqlCommand.GetCommandQuery());

            return this.mappingStoreDb.GetSqlStringCommand(sqlString, sqlCommand.DbParameters);
        }

        /// <inheritdoc/>
        public DbCommand BuildParentsReferencesQuery(ICommonStructureQuery structureQuery)
        {
            if (structureQuery == null)
            {
                throw new ArgumentNullException(nameof(structureQuery));
            }

            // build sub query for IN - select clause
            // O.SOURCE_ARTEFACT IN (SELECT A.ART_ID FROM .... WHERE .... )
            DbCommandBuilder sqlCommand = GetSqlCommandWithParameters(structureQuery, SQL_QUERY_JUST_ART_ID);
            string sqlString = string.Format(SQL_PARENT_REF_JUST_ART_ID, sqlCommand.GetCommandQuery());
            sqlString = string.Format(SQL_REFERENCES, sqlString);

            return this.mappingStoreDb.GetSqlStringCommand(sqlString, sqlCommand.DbParameters);
        }

        /// <inheritdoc/>
        public DbCommand BuildParentsRetrievalQuery(ICommonStructureQuery structureQuery, SdmxStructureType parentArtefactType)
        {
            if (structureQuery == null)
            {
                throw new ArgumentNullException(nameof(structureQuery));
            }

            string queryFormat;
            if (_tableInfo != null && _tableInfo.ExtraFields.Any())
            {
                string extraFields = "T." + string.Join(", T.", _tableInfo.GetExtraFieldsList());
                queryFormat = string.Format(SQL_QUERY_FORMAT_FOR_PARENTS, _tableInfo.PrimaryKey, _tableInfo.Table, extraFields,
                    //note that with current implementation we need to pass this as string parameter, not as sql query parameter
                    parentArtefactType.UrnClass,
                    //this is string placeholder for the "enclose" command
                    "{0}");
            }
            else
            {
                queryFormat = string.Format(SQL_QUERY_SIMPLE_FOR_PARENTS,
                    //note that with current implementation we need to pass this as string parameter, not as sql query parameter
                    parentArtefactType.UrnClass,
                    //this is string placeholder for the "enclose" command
                    "{0}");
            }

            // build sub query for IN - select clause
            // O.SOURCE_ARTEFACT IN (SELECT A.ART_ID FROM .... WHERE .... )
            DbCommandBuilder sqlCommand = GetSqlCommandWithParameters(structureQuery, SQL_QUERY_JUST_ART_ID);
            string sqlString = string.Format(queryFormat, sqlCommand.GetCommandQuery());

            return this.mappingStoreDb.GetSqlStringCommand(sqlString, sqlCommand.DbParameters);
        }

        /// <inheritdoc/>
        public DbCommand BuildReferencesQuery(ICommonStructureQuery structureQuery)
        {
            if (structureQuery == null)
            {
                throw new ArgumentNullException(nameof(structureQuery));
            }

            // build sub query for IN - select clause
            // O.SOURCE_ARTEFACT IN (SELECT A.ART_ID FROM .... WHERE .... )                      
            string subQuery = _tableInfo != null && _tableInfo.ExtraFields.Any() ? string.Format(SQL_QUERY_FORMAT_FOR_JUST_ART_ID, _tableInfo.Table, _tableInfo.PrimaryKey) : SQL_QUERY_JUST_ART_ID;
            DbCommandBuilder sqlCommand = GetSqlCommandWithParameters(structureQuery, subQuery);
            string sqlString = string.Format(SQL_REFERENCES, sqlCommand.GetCommandQuery());
            return this.mappingStoreDb.GetSqlStringCommand(sqlString, sqlCommand.DbParameters);
        }

        /// <inheritdoc/>
        public DbCommand BuildRetrievalQuery(ICommonStructureQuery structureQuery)
        {
            if (structureQuery == null)
            {
                throw new ArgumentNullException(nameof(structureQuery));
            }

            string queryFormat;
            if (_tableInfo != null && _tableInfo.ExtraFields.Any())
            {
                string extraFields = "T." + string.Join(", T.", _tableInfo.GetExtraFieldsList());
                queryFormat = string.Format(SQL_QUERY_FORMAT, _tableInfo.PrimaryKey, _tableInfo.Table, extraFields);
            }
            else
            {
                queryFormat = SQL_QUERY_SIMPLE;
            }
            DbCommandBuilder sqlCommand = GetSqlCommandWithParameters(structureQuery, queryFormat);

            // must be last
            if (ObjectUtil.ValidString(this._orderBy))
            {
                sqlCommand.AddStatement(_orderBy);
            }
            else
            {
                sqlCommand.AddStatement(ORDER_BY);
            }

            return this.mappingStoreDb.GetSqlStringCommand(sqlCommand.GetCommandQuery(), sqlCommand.DbParameters);
        }

        /// <inheritdoc/>
        public DbCommand BuildResolveParentReferences(ICommonStructureQuery structureQuery)
        {
            if (structureQuery == null)
            {
                throw new ArgumentNullException(nameof(structureQuery));
            }

            // build sub query for IN - select clause
            // O.SOURCE_ARTEFACT IN (SELECT A.ART_ID FROM .... WHERE .... )
            DbCommandBuilder sqlCommand = GetSqlCommandWithParameters(structureQuery, SQL_QUERY_JUST_ART_ID);
            string sqlString = string.Format(SQL_PARENT_REF, sqlCommand.GetCommandQuery());
            return this.mappingStoreDb.GetSqlStringCommand(sqlString, sqlCommand.DbParameters);
        }

        #endregion

        /// <summary>
        /// Set the order by clause to override the default order.
        /// </summary>
        /// <param name="orderBy">Should be format like ORDER BY [table_alias].[comun_name]</param>
        /// <returns>The same instance of <see cref="ArtefactCommandBuilder"/>.</returns>
        public ArtefactCommandBuilder SetOrderBy(string orderBy)
        {
            this._orderBy = orderBy;
            return this;
        }

        /// <summary>
        /// Will join the <paramref name="tableInfo"/> in the sql query.
        /// </summary>
        /// <param name="tableInfo">The table to join.</param>
        /// <returns>The same instance of <see cref="ArtefactCommandBuilder"/>.</returns>
        /// <exception cref="ArgumentNullException">for <paramref name="tableInfo"/>.</exception>
        /// <exception cref="SdmxSemmanticException">If <paramref name="tableInfo"/> is not for a SDMX maintainable object.</exception>
        public ArtefactCommandBuilder Join(TableInfo tableInfo)
        {
            if (tableInfo == null)
            {
                throw new ArgumentNullException(nameof(tableInfo));
            }
            if (!SdmxStructureType.GetFromEnum(tableInfo.StructureType).IsMaintainable)
            {
                throw new SdmxSemmanticException("Only maintainable");
            }
            this._tableInfo = tableInfo;
            return this;
        }

        /// <summary>
        /// Will set the SDMX structure types to be filtered in the where statement.
        /// </summary>
        /// <param name="structureTypes">The types to be included in the where statement.</param>
        public void WhereArtefactType(params SdmxStructureEnumType[] structureTypes)
        {
            _structureTypes.AddRange(structureTypes);
        }

        /// <summary>
        /// Build a statement for getting the status of an artefact in the DB. Specifically:
        /// - If it exists and
        /// - If the version retrieved is stable or not.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <returns>The DbCommand to exectute to get the results.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="structureQuery"/>.</exception>
        public DbCommand BuildArtefactStatus(ICommonStructureQuery structureQuery)
        {
            if (structureQuery == null)
            {
                throw new ArgumentNullException(nameof(structureQuery));
            }

            DbCommandBuilder sqlCommand = GetSqlCommandWithParameters(structureQuery, SQL_QUERY_STATUS);
            return this.mappingStoreDb.GetSqlStringCommand(sqlCommand.GetCommandQuery(), sqlCommand.DbParameters);
        }

        /// <summary>
        ///     Create the WHERE clause from the <paramref name="structureQuery" />  and write it to
        ///     <paramref name="sqlCommand" />
        /// </summary>
        /// <param name="structureQuery">
        ///     The structure query.
        /// </param>
        /// <param name="sqlCommand">
        ///     The output string buffer
        /// </param>
        /// <returns>
        ///     The list of <see cref="DbParameter" />
        /// </returns>
        protected virtual void CreateArtefactWhereClause(
            ICommonStructureQuery structureQuery,
            DbCommandBuilder sqlCommand)
        {
            if (structureQuery == null)
            {
                return;
            }

            if (structureQuery.HasSpecificMaintainableId)
            {
                sqlCommand.And();
                sqlCommand.AddInClause("AB.ID", ParameterNameConstants.IdParameter, structureQuery.MaintainableIds);
            }

            // there has to be at least one version request
            bool versionFlag = false;
            for (int i = 0; i < structureQuery.VersionRequests.Count; i++)
            {
                var versionRequest = structureQuery.VersionRequests[i];

                if (versionRequest.VersionQueryType == VersionQueryTypeEnum.All
                    && !versionRequest.SpecifiesVersion)
                {
                    continue; //i.e *
                }
                if (!versionFlag)
                {
                    sqlCommand.And();
                    sqlCommand.OpenParenthesis();
                    versionFlag = true;
                }
                else
                {
                    sqlCommand.Or();
                }
                sqlCommand.OpenParenthesis();
                AddVersionParameters(versionRequest, sqlCommand, i);
                sqlCommand.CloseParenthesis();
            }
            if (versionFlag)
            {
                sqlCommand.CloseParenthesis();
            }

            if (structureQuery.HasSpecificAgencyId)
            {
                sqlCommand.And();
                sqlCommand.AddInClause("AB.AGENCY", ParameterNameConstants.AgencyParameter, structureQuery.AgencyIds);
            }

            using (var scope = new SdmxAuthorizationScope(Mapping.Api.Constant.Authorisation.Optional))
            {
                if (scope.IsEnabled)
                {
                    sqlCommand.AddAuthClause(structureQuery, WhereState.And);
                }
            }
        }

        private void AddVersionParameters(IVersionRequest versionRequest, DbCommandBuilder sqlCommand, int index)
        {
            if (versionRequest == null)
            {
                throw new ArgumentNullException(nameof(versionRequest));
            }

            switch (versionRequest.VersionQueryType)
            {
                case VersionQueryTypeEnum.Latest:
                    AddLatestWhereClause(sqlCommand);
                    break;
                case VersionQueryTypeEnum.LatestStable:
                    AddLatestStableWhereClause(sqlCommand);
                    break;
                case VersionQueryTypeEnum.AllStable:
                    sqlCommand
                        .OpenParenthesis()
                        .AddStatement(STABLE_WHERE_CLAUSE)
                        .CloseParenthesis();
                    break;
            }

            if (!versionRequest.SpecifiesVersion)
            {
                // this means a wildcard is used
                return;
            }

            int? major = versionRequest.VersionPart(VersionPosition.Major);
            // at least major version is present
            if (!major.HasValue)
            {
                throw new SdmxSemmanticException("Not supported  version format");
            }
            int minor = versionRequest.VersionPart(VersionPosition.Minor) ?? 0;
            int patch = versionRequest.VersionPart(VersionPosition.Patch) ?? -1;

            if (versionRequest.WildCard != null)
            {
                IVersionWildcard wildcard = versionRequest.WildCard;

                // Artefact queries for all available semantic versions within the wildcard scope
                // ALL with version X*.Y.Z , X.*,, X.Y.*, X.Y*, it assumes that X*.Y.Z >= X.Y.Z etc
                // ALL stable with version , X.*+. The X.Y.*+  is not supported by the Open API regex for versions
                // Artefact queries for latest available semantic versions within the wildcard scope
                // Stable (X+.Y.Z, X.Y+.Z or X.Y.Z+).
                // Stable and unstable (X~.Y.Z, X.Y~.Z or X.Y.Z~).

                // old style versioning will be still be using X.Y versions in the MADB, in that case PATCH (VERSION3) will be -1
                DbCommandBuilder wildcardCommand = new DbCommandBuilder(mappingStoreDb);
                switch (wildcard.WildcardPosition)
                {
                    case VersionPosition.Major:
                        // means we have X+.Y.Z or X~.Y.Z case
                        wildcardCommand
                            .And()
                            .OpenParenthesis()
                            .AddStatement(versionQueriesForLatestWildcards[0])
                            .Or()
                            .AddStatement(versionQueriesForLatestWildcards[1])
                            .Or()
                            .AddStatement(versionQueriesForLatestWildcards[2])
                            .CloseParenthesis();
                        break;
                    case VersionPosition.Minor:
                        // means we have X.Y+.Z or X.Y~.Z case
                        wildcardCommand
                            .And()
                            .OpenParenthesis()
                            .AddStatement(versionQueriesForLatestWildcards[1])
                            .Or()
                            .AddStatement(versionQueriesForLatestWildcards[2])
                            .CloseParenthesis();
                        break;
                    case VersionPosition.Patch:
                        // means we have X.Y.Z+ or X.Y.Z~ case
                        wildcardCommand
                            .And()
                            .AddStatement(versionQueriesForLatestWildcards[2]);
                        break;
                }
                sqlCommand.MergeFormat(wildcardCommand,
                    mappingStoreDb.CreateInParameter(ParameterNameConstants.VersionParameter1 + index, DbType.Int64, major),
                    mappingStoreDb.CreateInParameter(ParameterNameConstants.VersionParameter2 + index, DbType.Int64, minor),
                    mappingStoreDb.CreateInParameter(ParameterNameConstants.VersionParameter3 + index, DbType.Int64, patch));

                return;
            }

            // version is present but no wildcards
            sqlCommand.AddStatementWithParameter("A.VERSION1 = {0}", ParameterNameConstants.VersionParameter1 + index, major.Value);
            sqlCommand.And().AddStatementWithParameter("A.VERSION2 = {0}", ParameterNameConstants.VersionParameter2 + index, minor);
            sqlCommand.And().AddStatementWithParameter("A.VERSION3 = {0}", ParameterNameConstants.VersionParameter3 + index, patch);


            if (ObjectUtil.ValidString(versionRequest.Extension))
            {
                sqlCommand.And();
                sqlCommand.AddWhereClause("A.EXTENSION", ParameterNameConstants.VersionExtension + index, TextSearchEnumType.Equal, versionRequest.Extension);
            }
        }

        protected virtual void AddLatestStableWhereClause(DbCommandBuilder sqlCommand)
        {
            sqlCommand
                .OpenParenthesis()
                .AddStatement(LATEST_STABLE_WHERE_CLAUSE).And().AddStatement(STABLE_WHERE_CLAUSE)
                .CloseParenthesis();
        }

        protected virtual void AddLatestWhereClause(DbCommandBuilder sqlCommand)
        {
            sqlCommand
                .OpenParenthesis()
                .AddStatement(LATEST_WHERE_CLAUSE)
                .CloseParenthesis();
        }

        private DbCommandBuilder GetSqlCommandWithParameters(ICommonStructureQuery structureQuery, string queryFormat)
        {
            DbCommandBuilder sqlCommand = new DbCommandBuilder(mappingStoreDb).AddStatement(queryFormat);

            sqlCommand.Where();

            if (!_structureTypes.Any())
            {
                sqlCommand.AddWhereClause("AB.ARTEFACT_TYPE", ParameterNameConstants.ArtefactTypeParameter, TextSearchEnumType.Equal, structureQuery.MaintainableTarget.UrnClass);
            }
            else
            {
                sqlCommand.AddInClause("AB.ARTEFACT_TYPE", ParameterNameConstants.ArtefactTypeParameter, _structureTypes.Select(t => SdmxStructureType.GetFromEnum(t).UrnClass).ToList());
            }

            CreateArtefactWhereClause(structureQuery, sqlCommand);

            return sqlCommand;
        }
    }
}
