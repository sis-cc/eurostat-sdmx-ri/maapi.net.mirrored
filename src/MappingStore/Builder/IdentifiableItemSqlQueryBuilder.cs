// -----------------------------------------------------------------------
// <copyright file="IdentifiableItemSqlQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The item SQL query builder.
    /// </summary>
    internal class IdentifiableItemSqlQueryBuilder : BaseItemSqlQueryBuilderBase
    {
        /// <summary>
        ///     The SQL query format.
        /// </summary>
        private const string SqlQueryFormat =
            "select O.OTH_ID as SYSID, O.ID , O.VALID_FROM , O.VALID_TO , O.VERSION1 , O.VERSION2, O.VERSION3, O.PARENT_ARTEFACT, O.URN_CLASS , O.PARENT_PATH " +
            "from OTHER_NAMEABLE O " +
            "WHERE O.PARENT_ARTEFACT  = {0} and O.URN_CLASS  = {1}";

        /// <summary>
        ///     Initializes a new instance of the <see cref="IdentifiableItemSqlQueryBuilder" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <param name="orderBy">
        ///     The order by
        /// </param>
        public IdentifiableItemSqlQueryBuilder(Database mappingStoreDb, string orderBy)
            : base(mappingStoreDb, orderBy, SqlQueryFormat)
        {
        }

        public SqlQueryInfo Build(SdmxStructureType structureType)
        {
            string queryformat = string.Format(SqlQueryFormat, "{0}", $"'{structureType.UrnClass}'");
            return new SqlQueryInfo { QueryFormat = queryformat };
        }
    }
}