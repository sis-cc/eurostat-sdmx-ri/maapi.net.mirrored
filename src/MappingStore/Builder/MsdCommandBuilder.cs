// -----------------------------------------------------------------------
// <copyright file="MsdCommandBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-04-18
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// The MSD command builder.
    /// </summary>
    internal class MsdCommandBuilder
    {
        /// <summary>
        /// The _command builder
        /// </summary>
        private readonly ItemCommandBuilder _commandBuilder;

        /// <summary>
        /// The metadata attribute SQL query information
        /// </summary>
        private readonly SqlQueryInfo _metadataAttributeSqlQueryInfo;

        /// <summary>
        /// The target object
        /// </summary>
        private readonly SqlQueryInfo _targetObject;

        /// <summary>
        /// The report structure metadata target map information
        /// </summary>
        private readonly SqlQueryInfo _reportStructureMetdataTargetMapInfo;

        /// <summary>
        /// The report structure
        /// </summary>
        private readonly SqlQueryInfo _reportStructure;

        /// <summary>
        /// The metadata target
        /// </summary>
        private readonly SqlQueryInfo _metadataTarget;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsdCommandBuilder" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">The mapping store database.</param>
        /// <param name="artefactTypeSqlBuilder">The artefact type SQL builder.</param>
        public MsdCommandBuilder(Database mappingStoreDb, ArtefactTypeSqlBuilder artefactTypeSqlBuilder)
        {
            var sqlQueryBuilder = new ReferencedSqlQueryBuilder(mappingStoreDb, null);
            this._commandBuilder = new ItemCommandBuilder(mappingStoreDb);
            this._metadataAttributeSqlQueryInfo = sqlQueryBuilder.Build(MsdConstant.MetadataAttributesQueryFormat);
            var targetObjectFormat = string.Format(CultureInfo.InvariantCulture, MsdConstant.TargetObjectFormat, artefactTypeSqlBuilder.Build(IsItemScheme));
            this._targetObject = sqlQueryBuilder.Build(targetObjectFormat);
            this._reportStructureMetdataTargetMapInfo = sqlQueryBuilder.Build(MsdConstant.ReportStructureMetadataTargerFormat);

            var itemSqlBuilder = new IdentifiableItemSqlQueryBuilder(mappingStoreDb, null);
            this._reportStructure = itemSqlBuilder.Build(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportStructure));
            this._metadataTarget = itemSqlBuilder.Build(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataTarget));
        }

        /// <summary>
        /// Gets the attribute command.
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <returns>database command</returns>
        public DbCommand GetAttributeCommand(long parentSysId)
        {
            return this._commandBuilder.Build(new ItemSqlQuery(this._metadataAttributeSqlQueryInfo, parentSysId));
        }

        /// <summary>
        /// Gets the target object command.
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <returns>database command</returns>
        public DbCommand GetTargetObjectCommand(long parentSysId)
        {
            return this._commandBuilder.Build(new ItemSqlQuery(this._targetObject, parentSysId));
        }

        /// <summary>
        /// Gets the report structure metadata target command.
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <returns>database command</returns>
        public DbCommand GetReportStructureMetadataTargetCommand(long parentSysId)
        {
            return this._commandBuilder.Build(new ItemSqlQuery(this._reportStructureMetdataTargetMapInfo, parentSysId));
        }

        /// <summary>
        /// Gets the report structure command.
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <returns>database command</returns>
        public DbCommand GetReportStructureCommand(long parentSysId)
        {
            return this._commandBuilder.Build(new ItemSqlQuery(this._reportStructure, parentSysId));
        }

        /// <summary>
        /// Gets the metadata target command.
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <returns>
        /// database command
        /// </returns>
        public DbCommand GetMetadataTargetCommand(long parentSysId)
        {
            return this._commandBuilder.Build(new ItemSqlQuery(this._metadataTarget, parentSysId));
        }

        /// <summary>
        /// Determines whether [is item scheme] [the specified type].
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns><c>true</c> if [is item scheme] [the specified type]; otherwise, <c>false</c>.</returns>
        private static bool IsItemScheme(SdmxStructureType type)
        {
            return type.IsMaintainable && type.MaintainableInterface.GetInterfaces().Any(type1 => type1.IsGenericType);
        }
    }
}