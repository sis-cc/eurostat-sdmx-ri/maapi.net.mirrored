﻿// -----------------------------------------------------------------------
// <copyright file="InequalityClauseBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-11-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Data;
    using System.Globalization;

    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    /// <summary>
    /// The inequality SQL clause builder.
    /// </summary>
    public class InequalityClauseBuilder
    {
        /// <summary>
        /// Builds the where clause for the specified <paramref name="fieldName"/>. The SQL clause depends if <paramref name="value"/> is null
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="dbType">Type of the database.</param>
        /// <param name="value">The value.</param>
        /// <returns>The <see cref="SqlClause"/> containing the where clause and one parameter</returns>
        public SqlClause Build(Database database, string fieldName, DbType dbType, object value)
        {
            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            if (fieldName == null)
            {
                throw new ArgumentNullException("fieldName");
            }

            var name = string.Format(CultureInfo.InvariantCulture, "p_{0}", fieldName.ToLowerInvariant().Replace("_", string.Empty));
            if (value == null)
            {
                var nullParameter = database.CreateInParameter(name, dbType, DBNull.Value);
                return new SqlClause(string.Format(CultureInfo.InvariantCulture, "( {0} is not null )", fieldName), new[] { nullParameter });
            }

            var parameter = database.CreateInParameter(name, dbType, value);
            var whereClause = string.Format(CultureInfo.InvariantCulture, "(( {0} is null) or ( {0} != {1} ))", fieldName, parameter.ParameterName);
            var sqlWhere = new SqlClause(whereClause, new[] { parameter });
            return sqlWhere;
        }
    }
}