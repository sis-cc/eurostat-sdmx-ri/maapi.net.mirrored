// -----------------------------------------------------------------------
// <copyright file="IdentifiableTableInfoBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-04-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The identifiable table info builder.
    /// </summary>
    public class IdentifiableTableInfoBuilder : IBuilder<ItemTableInfo, SdmxStructureEnumType>
    {
        /// <summary>
        /// The item table information builder
        /// </summary>
        private readonly ItemTableInfoBuilder _itemTableInfoBuilder = new ItemTableInfoBuilder();

        /// <summary>
        ///     Builds an <see cref="ItemTableInfo" /> from the specified <paramref name="buildFrom" />
        /// </summary>
        /// <param name="buildFrom">
        ///     An <see cref="SdmxStructureEnumType" /> to build the output object from
        /// </param>
        /// <returns>
        ///     an <see cref="ItemTableInfo" /> from the specified <paramref name="buildFrom" />
        /// </returns>
        public ItemTableInfo Build(SdmxStructureEnumType buildFrom)
        {
            var tableInfo = this._itemTableInfoBuilder.Build(buildFrom);
            if (tableInfo != null)
            {
                return tableInfo;
            }

            switch (buildFrom)
            {
                case SdmxStructureEnumType.ReportStructure:
                    return MsdConstant.ReportStructureTableInfo;
                case SdmxStructureEnumType.MetadataAttribute:
                    return MsdConstant.MetadataAttributeTableInfo;
                case SdmxStructureEnumType.MetadataTarget:
                    return MsdConstant.MetadataTargetTableInfo;
                case SdmxStructureEnumType.ReportPeriodTarget:
                case SdmxStructureEnumType.ConstraintContentTarget:
                case SdmxStructureEnumType.DimensionDescriptorValuesTarget:
                case SdmxStructureEnumType.IdentifiableObjectTarget:
                case SdmxStructureEnumType.DatasetTarget:
                    return MsdConstant.TargetObjectTableInfo;
                case SdmxStructureEnumType.Component:
                case SdmxStructureEnumType.Dimension:
                case SdmxStructureEnumType.TimeDimension:
                case SdmxStructureEnumType.MeasureDimension:
                case SdmxStructureEnumType.DataAttribute:
                case SdmxStructureEnumType.CrossSectionalMeasure:
                case SdmxStructureEnumType.PrimaryMeasure:
                    return DsdConstant.ComponentTableInfo;
                case SdmxStructureEnumType.Group:
                    return DsdConstant.GroupTableInfo;
                case SdmxStructureEnumType.HierarchicalCode:
                    return HclConstant.HierarchicalCodeTableInfo;
                case SdmxStructureEnumType.Level:
                    return HclConstant.LevelTableInfo;
                case SdmxStructureEnumType.Hierarchy:
                    return HclConstant.HierarchyTableInfo;
            }

            return null;
        }
    }
}