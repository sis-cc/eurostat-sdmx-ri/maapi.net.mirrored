// -----------------------------------------------------------------------
// <copyright file="ItemSqlQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    /// <summary>
    ///     The item SQL query builder.
    /// </summary>
    internal class ItemSqlQueryBuilder : BaseItemSqlQueryBuilderBase
    {
        /// <summary>
        ///     The SQL query format.
        /// </summary>
        private const string SqlQueryFormat =
            "SELECT T.{0} as SYSID, I.ID ,LN.TEXT, LN.LANGUAGE, LN.TYPE {1} FROM {2} T INNER JOIN ITEM I ON T.{0} = I.ITEM_ID  LEFT OUTER JOIN LOCALISED_STRING LN ON LN.ITEM_ID = I.ITEM_ID WHERE T.{3} = {4}";

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemSqlQueryBuilder" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <param name="orderBy">
        ///     The order by
        /// </param>
        public ItemSqlQueryBuilder(Database mappingStoreDb, string orderBy)
            : base(mappingStoreDb, orderBy, SqlQueryFormat)
        {
        }
    }

    internal class ConceptItemSqlQueryBuilder : BaseItemSqlQueryBuilderBase
    {
        public ConceptItemSqlQueryBuilder(Database mappingStoreDb, string orderBy)
            : base(mappingStoreDb, orderBy, SqlQueryFormat)
        {
        }
        private const string SqlQueryFormat =
            "SELECT T.{0} as SYSID, I.ID, T.CL_ID, AV.AGENCY, AV.ID as CODELISTID, AV.VERSION," +
            " LN.TEXT, LN.LANGUAGE, LN.TYPE {1}" +
            " FROM {2} T INNER JOIN ITEM I ON T.{0} = I.ITEM_ID" +
            " LEFT OUTER JOIN LOCALISED_STRING LN ON LN.ITEM_ID = I.ITEM_ID" +
            " LEFT OUTER JOIN ARTEFACT_VIEW AV ON T.CL_ID = AV.ART_ID" +
            " WHERE T.{3} = {4}";

    }
}