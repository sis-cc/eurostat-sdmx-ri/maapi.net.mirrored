// -----------------------------------------------------------------------
// <copyright file="DataflowCommandBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using System.Globalization;

    /// <summary>
    ///     The dataflow command builder.
    /// </summary>
    internal class DataflowCommandBuilder : ArtefactCommandBuilder
    {
        /// <summary>
        ///     The filter.
        /// </summary>
        private readonly DataflowFilter _filter;
        private static readonly string PRODUCTION_LATEST_WHERE_CLAUSE = " A.VERSION1 >= 0 " +
                "    and not exists ( " +
                "        select 1 " +
                "        from N_ARTEFACT LT " +
                "        where LT.ART_BASE_ID = A.ART_BASE_ID " +
                "            and EXISTS (select 1 from DATAFLOW D1 where D1.{0} and D1.DF_ID = LT.ART_ID ) " +
                "            and LT.ART_ID != A.ART_ID " +
                "            and ( " +
                "                LT.VERSION1 > A.VERSION1 " +
                "                or ( " +
                "                    LT.VERSION1 = A.VERSION1 " +
                "                    and ( " +
                "                        LT.VERSION2 > A.VERSION2 " +
                "                        or ( " +
                "                            LT.VERSION2 = A.VERSION2 " +
                "                            and LT.VERSION3 > A.VERSION3 " +
                "                        ) " +
                "                    ) " +
                "                ) " +
                "              ) " +
                "            )";

          private static readonly string PRODUCTION_LATEST_STABLE_WHERE_CLAUSE = " A.VERSION1 >= 0 " +
                "    and not exists ( " +
                "        select 1 " +
                "        from N_ARTEFACT LT " +
                "        where LT.ART_BASE_ID = A.ART_BASE_ID " +
                "            and LT.ART_ID != A.ART_ID " +
                "            and EXISTS (select 1 from DATAFLOW D1 where D1.{0} and D1.DF_ID = LT.ART_ID ) " +
                "            and LT.EXTENSION is null " +
                "            and ( " +
                "                LT.VERSION1 > A.VERSION1 " +
                "                or ( " +
                "                    LT.VERSION1 = A.VERSION1 " +
                "                    and ( " +
                "                        LT.VERSION2 > A.VERSION2 " +
                "                        or ( " +
                "                            LT.VERSION2 = A.VERSION2 " +
                "                            and LT.VERSION3 > A.VERSION3 " +
                "                        ) " +
                "                    ) " +
                "                ) " +
                "              ) " +
                "            )";

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataflowCommandBuilder" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <param name="filter">
        ///     The filter. (Optional defaults to <see cref="DataflowFilter.Production" />
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="mappingStoreDb" /> is null
        /// </exception>
        public DataflowCommandBuilder(Database mappingStoreDb, DataflowFilter filter = DataflowFilter.Production)
            : base(mappingStoreDb)
        {
            this._filter = filter;

            TableInfoBuilder builder = new TableInfoBuilder();
            TableInfo tableInfo = builder.Build(SdmxStructureEnumType.Dataflow);
            Join(tableInfo);
        }
        protected override void AddLatestStableWhereClause(DbCommandBuilder sqlCommand)
        {
            if (this._filter == DataflowFilter.Any)
            {
                base.AddLatestStableWhereClause(sqlCommand);
                return;
            }

            string whereClauseFilter = _filter == DataflowFilter.Production ? "PRODUCTION=1" : "EXTERNAL_USAGE=0";

            sqlCommand
                .OpenParenthesis()
                .AddStatement(string.Format(CultureInfo.InvariantCulture, PRODUCTION_LATEST_STABLE_WHERE_CLAUSE, whereClauseFilter))
                .And().AddStatement(STABLE_WHERE_CLAUSE)
                .CloseParenthesis();
        }

        protected override void AddLatestWhereClause(DbCommandBuilder sqlCommand)
        {
              if (this._filter == DataflowFilter.Any)
            {
                base.AddLatestWhereClause(sqlCommand);
                return;
            }

            string whereClauseFilter = _filter == DataflowFilter.Production ? "PRODUCTION=1" : "EXTERNAL_USAGE=0";
            sqlCommand
                .OpenParenthesis()
                .AddStatement(string.Format(CultureInfo.InvariantCulture, PRODUCTION_LATEST_WHERE_CLAUSE, whereClauseFilter))
                .CloseParenthesis();
        }

        /// <inheritdoc/>
        protected override void CreateArtefactWhereClause(
            ICommonStructureQuery structureQuery, 
            DbCommandBuilder sqlCommand)
        {
            base.CreateArtefactWhereClause(structureQuery, sqlCommand);

            switch (_filter)
            {
                case DataflowFilter.Any:
                    break;
                case DataflowFilter.Production:
                    sqlCommand.And().AddStatement("EXISTS (select 1 from DATAFLOW D where D.PRODUCTION = 1 and D.DF_ID = A.ART_ID )");
                    break;
                case DataflowFilter.Usage:                    
                    sqlCommand.And().AddStatement("EXISTS (select 1 from DATAFLOW D where D.EXTERNAL_USAGE = 0 and D.DF_ID = A.ART_ID )");
                    //when we wish to take into account usage value, we should return the dataflows that have value "0".Also default usage value when inserting a dataflow is 0
                    break;
            }
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <param name="buildFrom"></param>
        /// <param name="allowedDataflows"></param>
        /// <returns><c>null</c></returns>
        [Obsolete("Should use methods of IArtefactSqlQueryBuilder instead.")]
        public DbCommand Build(ArtefactSqlQuery buildFrom, IList<IMaintainableRefObject> allowedDataflows)
        {
            return null;
        }
    }
}