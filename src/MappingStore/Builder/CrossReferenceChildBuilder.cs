// -----------------------------------------------------------------------
// <copyright file="CrossReferenceChildBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Util.Sdmx;

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.MappingStoreRetrieval.Extensions;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The cross reference child builder.
    /// </summary>
    public class CrossReferenceChildBuilder : IBuilder<ISet<IStructureReference>, IIdentifiableMutableObject>
    {
        /// <summary>
        ///     Builds an <see cref="ISet{IStructureReference}" /> from the specified <paramref name="buildFrom" />
        /// </summary>
        /// <param name="buildFrom">
        ///     An Object to build the output object from
        /// </param>
        /// <returns>
        ///     The <see cref="ISet{IStructureReference}" /> from the specified <paramref name="buildFrom" />
        /// </returns>
        public ISet<IStructureReference> Build(IIdentifiableMutableObject buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            

            var structureReferences = new HashSet<IStructureReference>();
            if (buildFrom is ICodelistMutableObject)
            {
                return structureReferences;
            }
            if (buildFrom is IMaintainableMutableObject maintainableMutableObject)
            {
                var immutableInstanceCrossReferences = maintainableMutableObject.ImmutableInstance.CrossReferences;
                if (maintainableMutableObject is ICrossSectionalDataStructureMutableObject crossSectionalDataStructureMutableObject)
                {
                    foreach (var structureReference in crossSectionalDataStructureMutableObject.MeasureDimensionCodelistMapping.Values)
                    {
                        structureReferences.Add(structureReference);
                    }
                }
                foreach (var immutableInstanceCrossReference in immutableInstanceCrossReferences)
                {
                    structureReferences.Add(immutableInstanceCrossReference.CreateMutableInstance());
                }
            }

            return structureReferences;
        }

        /// <summary>
        ///     Add references to <paramref name="structureReferences" />
        /// </summary>
        /// <param name="hcl">
        ///     The HCL.
        /// </param>
        /// <param name="structureReferences">
        ///     The structure references.
        /// </param>
        private static void AddReferences(
            IHierarchicalCodelistMutableObject hcl, 
            ISet<IStructureReference> structureReferences)
        {
            if (hcl != null && !hcl.Stub)
            {
                foreach (ICodelistRefMutableObject codelistRefMutableObject in hcl.CodelistRef)
                {
                    structureReferences.Add(codelistRefMutableObject.CodelistReference);
                }
            }
        }

        /// <summary>
        ///     Add references to <paramref name="structureReferences" />
        /// </summary>
        /// <param name="dsd">
        ///     The DSD.
        /// </param>
        /// <param name="structureReferences">
        ///     The structure references.
        /// </param>
        private static void AddReferences(
            IDataStructureMutableObject dsd, 
            ISet<IStructureReference> structureReferences)
        {
            if (dsd != null && !dsd.Stub && dsd.DimensionList != null)
            {
                AddReferences(dsd.Dimensions, structureReferences);

                if (dsd.AttributeList != null)
                {
                    AddReferences(dsd.AttributeList.Attributes, structureReferences);
                }

                AddReferences(dsd.PrimaryMeasure, structureReferences);

                var crossDsd = dsd as ICrossSectionalDataStructureMutableObject;
                if (crossDsd != null)
                {
                    AddReferences(crossDsd.CrossSectionalMeasures, structureReferences);
                    structureReferences.UnionWith(crossDsd.MeasureDimensionCodelistMapping.Values);
                }
            }
        }

        /// <summary>
        ///     Add references to <paramref name="structureReferences" />
        /// </summary>
        /// <param name="categorisation">
        ///     The categorisation.
        /// </param>
        /// <param name="structureReferences">
        ///     The structure references.
        /// </param>
        private static void AddReferences(
            ICategorisationMutableObject categorisation, 
            ISet<IStructureReference> structureReferences)
        {
            if (categorisation != null && !categorisation.Stub)
            {
                structureReferences.Add(categorisation.CategoryReference);
                structureReferences.Add(categorisation.StructureReference);
            }
        }

        /// <summary>
        ///     Add references to <paramref name="structureReferences" />
        /// </summary>
        /// <param name="dataflow">
        ///     The dataflow.
        /// </param>
        /// <param name="structureReferences">
        ///     The structure references.
        /// </param>
        private static void AddReferences(
            IDataflowMutableObject dataflow, 
            ISet<IStructureReference> structureReferences)
        {
            if (dataflow != null && !dataflow.Stub)
            {
                structureReferences.Add(dataflow.DataStructureRef);
            }
        }

        /// <summary>
        ///     Add references to <paramref name="structureReferences" />
        /// </summary>
        /// <param name="components">
        ///     The components.
        /// </param>
        /// <param name="structureReferences">
        ///     The structure references.
        /// </param>
        /// <typeparam name="T">
        ///     The item type of <paramref name="components" />
        /// </typeparam>
        private static void AddReferences<T>(
            IEnumerable<T> components, 
            ICollection<IStructureReference> structureReferences) where T : IComponentMutableObject
        {
            foreach (T component in components)
            {
                AddReferences(component, structureReferences);
            }
        }

        /// <summary>
        ///     Add references to <paramref name="structureReferences" />
        /// </summary>
        /// <param name="component">
        ///     The component.
        /// </param>
        /// <param name="structureReferences">
        ///     The structure references.
        /// </param>
        private static void AddReferences(
            IComponentMutableObject component, 
            ICollection<IStructureReference> structureReferences)
        {
            if (component != null)
            {
                structureReferences.Add(component.ConceptRef);
                if (component.Representation != null && component.Representation.Representation != null)
                {
                    structureReferences.Add(component.Representation.Representation);
                }

                structureReferences.AddAll(component.GetConceptRoles());
            }
        }
    }
}