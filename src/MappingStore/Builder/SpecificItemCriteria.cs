// -----------------------------------------------------------------------
// <copyright file="SpecificItemCriteria.cs" company="EUROSTAT">
//   Date Created : 2022-06-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Creates the sql query for searching for specific items
    /// </summary>
    public class SpecificItemCriteria
    {
        private readonly DbCommandBuilder _commandBuilder;
        private bool _hasClauses = false;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="mappingStoreDb"></param>
        public SpecificItemCriteria(Database mappingStoreDb)
        {
            _commandBuilder = new DbCommandBuilder(mappingStoreDb);
        }

        private class NestedItemCriteria
        {
            public readonly IComplexIdentifiableReferenceObject Leaf;
            public IList<string> parentPath = new List<string>();

            public NestedItemCriteria(IComplexIdentifiableReferenceObject leaf)
            {
                this.Leaf = leaf;
            }
        }

        /// <summary>
        /// Gets the command query built for the specific items.
        /// </summary>
        /// <returns></returns>
        public DbCommandBuilder GetCommandBuilder()
        {
            //TODO find a better way for this
            return this._commandBuilder;
        }

        /// <summary>
        /// This method will flatten the hierarchy since it applies to no nested without cascade
        /// SOAP 2.1 doesn't support such configuration for no nested items, so we ignore any search criteria not supported
        /// by REST
        /// REST allows
        /// </summary>
        /// <param name="referenceBean">The item reference</param>
        /// <returns>The accepted codes</returns>
        private static ISet<string> JoinItemPathNotNested(IComplexIdentifiableReferenceObject referenceBean)
        {
            if (referenceBean == null)
            {
                return new HashSet<string>();
            }
            ISet<string> items = new HashSet<string>();
            var stack = new Stack<IComplexIdentifiableReferenceObject>();
            stack.Push(referenceBean);
            while (stack.Any())
            {
                IComplexIdentifiableReferenceObject current = stack.Pop();
                if (current.Id != null && ObjectUtil.ValidString(current.Id.SearchParameter))
                {
                    items.Add(current.Id.SearchParameter);
                }
                if (current.ChildReference != null)
                {
                    stack.Push(referenceBean);
                }
            }

            return items;
        }

        /// <summary>
        /// Adds the given item references in the search as non-nested items.
        /// </summary>
        /// <remarks>
        ///     It uses multiple OR-where-clauses to make the query.
        ///     TODO: maybe replace with IN-where-clause
        /// </remarks>
        /// <param name="itemRefs">The non-nested items to search for.</param>
        public void AddNotNestedClause(IList<IComplexIdentifiableReferenceObject> itemRefs)
        {
            bool needsOr = false;
            int count = 0;
            foreach (IComplexIdentifiableReferenceObject itemRef in itemRefs)
            {
                if (itemRef.Id != null)
                {
                    if (needsOr)
                    {
                        _commandBuilder.Or();
                    }
                    else
                    {
                        // first time
                        _commandBuilder.And();
                        _commandBuilder.OpenParenthesis();
                    }
                    needsOr = true;
                    switch (itemRef.CascadeSelection)
                    {
                        case CascadeSelection.False:
                            AddNotNestedClause(itemRef, count);
                            break;
                        case CascadeSelection.True:
                            AddNotNestedClause(itemRef, count);
                            _commandBuilder.Or();
                            AddNotNestedParentFilter(itemRef, count);
                            break;
                        case CascadeSelection.ExcludeRoot:
                            AddNotNestedParentFilter(itemRef, count);
                            break;
                    }
                    count++;
                }
            }

            if (needsOr)
            {
                _hasClauses = true;
                _commandBuilder.CloseParenthesis();
            }
        }

        /// <summary>
        /// Adds the given item references in the search as nested items.
        /// </summary>
        /// <param name="itemRefs">The nested items to search for.</param>
        public void AddNestedClause(IList<IComplexIdentifiableReferenceObject> itemRefs)
        {
            bool needsOr = false;
            int count = 0;
            foreach (IComplexIdentifiableReferenceObject itemRef in itemRefs)
            {
                if (itemRef.Id != null)
                {
                    if (needsOr)
                    {
                        _commandBuilder.Or();
                    }
                    else
                    {
                        // first time
                        _commandBuilder.And();
                        _commandBuilder.OpenParenthesis();
                    }
                    needsOr = true;
                    AddNestedClause(itemRef, ref count);
                    count++;
                }
            }
            if (needsOr)
            {
                _hasClauses = true;
                _commandBuilder.CloseParenthesis();
            }
        }

        private void AddNestedClause(IComplexIdentifiableReferenceObject itemRef, ref int count)
        {
            if (itemRef == null || itemRef.Id == null)
            {
                return;
            }

            IList<NestedItemCriteria> parentPathAndLeaf = GetParentPathAndLeaf(itemRef);

            bool needsOr = false;
            foreach (NestedItemCriteria nestedItemCriteria in parentPathAndLeaf)
            {
                if (needsOr)
                {
                    _commandBuilder.Or();
                }
                needsOr = true;
                NestedParentAndId(nestedItemCriteria.parentPath, nestedItemCriteria.Leaf, count);
                count++;
            }
        }

        private void NestedParentAndId(IList<string> parentPath, IComplexIdentifiableReferenceObject last, int parameterSuffix)
        {
            _commandBuilder.OpenParenthesis();
            _commandBuilder.AddWhereClause("I.ID", ParameterNameConstants.ConceptIdParameter + parameterSuffix, last.Id.Operator, last.Id.SearchParameter);
            if (parentPath.Any())
            {
                _commandBuilder.And();
                // Having different for each parent will be difficult to support so we take the last one
                _commandBuilder.AddWhereClause("I.PARENT_ITEM", ParameterNameConstants.ConceptSchemeIdParameter + parameterSuffix, last.Id.Operator, string.Join(".", parentPath));
            }

            _commandBuilder.CloseParenthesis();
        }

        private IList<NestedItemCriteria> GetParentPathAndLeaf(IComplexIdentifiableReferenceObject itemRef)
        {
            var stack = new Stack<NestedItemCriteria>();
            var values = new List<NestedItemCriteria>();
            var last = new NestedItemCriteria(itemRef);
            // build the parent path and find the ID
            stack.Push(last);
            values.Add(last);
            var parentPath = new List<string>();
            while (stack.Any())
            {
                NestedItemCriteria current = stack.Pop();
                if (current.Leaf != null && current.Leaf.Id != null && ObjectUtil.ValidString(current.Leaf.Id.SearchParameter))
                {
                    if (current.Leaf.ChildReference != null)
                    {
                        var node = new NestedItemCriteria(current.Leaf.ChildReference);
                        stack.Push(node);
                        parentPath.Add(current.Leaf.Id.SearchParameter);
                        parentPath.ForEach(p => node.parentPath.Add(p));
                        values.Add(node);
                    }
                }
            }
            return values;
        }

        /// <summary>
        /// Checks if there are search clauses for specific items.
        /// </summary>
        /// <returns></returns>
        public bool HasClauses()
        {
            return _hasClauses;
        }

        private void AddNotNestedParentFilter(IComplexIdentifiableReferenceObject itemRef, int parameterSuffix)
        {
            _commandBuilder.AddWhereClause("I.PARENT_ITEM", ParameterNameConstants.ConceptSchemeIdParameter + parameterSuffix, itemRef.Id.Operator, itemRef.Id.SearchParameter);
        }

        // names descriptions might need to be on the same level...
        private void AddNotNestedClause(IComplexIdentifiableReferenceObject itemRef, int parameterSuffix)
        {
            _commandBuilder.OpenParenthesis();
            //            bool needsAnd = false;
            if (itemRef.ChildReference != null && itemRef.Id != null)
            {
                // Only REST calls for not nested item schemes will have a  child reference value
                // so we can just use in clause
                _commandBuilder.AddInClause("I.ID", ParameterNameConstants.ConceptIdParameter + parameterSuffix, JoinItemPathNotNested(itemRef).ToArray());
            }
            else
            {
                // REST with no dots in item, SOAP 2.1, Data Constraint etc
                if (itemRef.Id != null)
                {
                    _commandBuilder.AddWhereClause("I.ID", ParameterNameConstants.ConceptIdParameter + parameterSuffix, itemRef.Id.Operator, itemRef.Id.SearchParameter);
                }

                // Only supported in SOAP 2.1, included as an example
                // the following won't cover most functionality
                // Since Name is mandatory but description is not, we probably need two inner join
                // one for name (IS_NAME=1) and one for description (IS_NAME=0). But also we have issue of language...
                /* TODO Leaving it in case we supported this
                if (itemRef.NameReference != null) {

                    if (needsAnd) {
                        pb.And();
                    }
                    pb.OpenParenthesis();
                    // Either it is a description or a name that matches that text.
                    // *But* items without names should be dropped.
                    pb.Append(" LN.IS_NAME=0 ").Or();
                    pb.AddWhereClause(" LN.TEXT ", itemRef.Id.Operator, itemRef.NameReference.SearchParameter);
                    pb.CloseParenthesis();
                    needsAnd = true;
                }
                if (itemRef.DescriptionReference != null) {

                    if (needsAnd) {
                        pb.And();
                    }
                    pb.OpenParenthesis();
                    // Either it is a name or a description that matches that text.
                    pb.Append(" LN.IS_NAME=1 ").Or();
                    pb.AddWhereClause(" LN.TEXT ", itemRef.Id.Operator, itemRef.NameReference.SearchParameter);
                    pb.CloseParenthesis();
                }
                 */
            }
            _commandBuilder.CloseParenthesis();
        }
    }
}
