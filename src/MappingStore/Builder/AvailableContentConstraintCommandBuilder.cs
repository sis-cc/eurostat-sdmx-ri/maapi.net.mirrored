// -----------------------------------------------------------------------
// <copyright file="AvailableContentConstraintCommandBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-5-16
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Data;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    /// <summary>
    ///     The available content constraint command builder.
    /// </summary>
    internal class AvailableContentConstraintCommandBuilder : ICommandBuilder<PartialCodesSqlQuery>
    {
        /// <summary>
        ///     The mapping store DB.
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AvailableContentConstraintCommandBuilder" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        public AvailableContentConstraintCommandBuilder(Database mappingStoreDb)
        {
            this._mappingStoreDb = mappingStoreDb;
        }

        /// <summary>
        /// Build a <see cref="DbCommand" /> from <paramref name="buildFrom" />
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <returns>The <see cref="DbCommand" />.</returns>
        /// <exception cref="ArgumentNullException">buildFrom</exception>
        /// <remarks>It expects the input SQL query to have 6 format parameters.
        /// 2. <c>Dataflow ID</c>
        /// 3. <c>Dataflow Agency</c>
        /// 4. <c>Dataflow Version1</c>
        /// 5. <c>Dataflow Version2</c>
        /// 6. <c>Dataflow Version3</c>
        /// 7. <c>Concept ID</c></remarks>
        public DbCommand Build(PartialCodesSqlQuery buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException(nameof(buildFrom));
            }

            var version = buildFrom.DataflowReference.SplitVersion(3);
            var parameters = new []
                                 {
                                     this._mappingStoreDb.CreateInParameter(
                                         ParameterNameConstants.DataflowIdParameter,
                                         DbType.AnsiString,
                                         buildFrom.DataflowReference.MaintainableId),
                                     this._mappingStoreDb.CreateInParameter(
                                         ParameterNameConstants.AgencyParameter,
                                         DbType.AnsiString,
                                         buildFrom.DataflowReference.AgencyId),
                                     this._mappingStoreDb.CreateInParameter(
                                         ParameterNameConstants.VersionParameter1,
                                         DbType.Int64,
                                         version[0].ToDbValue()),
                                     this._mappingStoreDb.CreateInParameter(
                                         ParameterNameConstants.VersionParameter2,
                                         DbType.Int64,
                                         version[1].ToDbValue(0)),
                                     this._mappingStoreDb.CreateInParameter(
                                         ParameterNameConstants.VersionParameter3,
                                         DbType.Int64,
                                         version[2].ToDbValue()),
                                     this._mappingStoreDb.CreateInParameter(
                                         ParameterNameConstants.ConceptIdParameter,
                                         DbType.AnsiString,
                                         buildFrom.ConceptId)
                                 };

            return this._mappingStoreDb.GetSqlStringCommandFormat(buildFrom.QueryInfo.ToString(), parameters);
        }
    }
}