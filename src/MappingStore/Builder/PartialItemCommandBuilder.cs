// -----------------------------------------------------------------------
// <copyright file="PartialCategoriesCommandBuilder.cs" company="EUROSTAT">
//   Date Created : 2022-12-13
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     The partial concepts command builder.
    /// </summary>
    internal class PartialItemCommandBuilder
    {
        private readonly Database _mappingStoreDb;

        // returns the item Ids that belong in this concept scheme and are used by this DSD
        // the reference type could be any; concept identity or concept role
        private const string PARTIAL_QUERY_FORMAT = "select sr.TARGET_CHILD_FULL_ID as ID\r\nfrom STRUCTURE_REF sr " +
            "\r\ninner join REFERENCE_SOURCE rs on rs.REF_SRC_ID = sr.REF_SRC_ID " +
            "\r\nwhere rs.SOURCE_ARTEFACT in (select ART_ID from ARTEFACT_VIEW where AGENCY = {1} and ID = {2} and VERSION = {3} and ARTEFACT_TYPE = {4})" +
            "\r\nand sr.TARGET_ARTEFACT in (select ab.ART_BASE_ID from ARTEFACT_BASE ab inner join N_ARTEFACT a on a.ART_BASE_ID = ab.ART_BASE_ID where a.ART_ID = {0})";

        /// <summary>
        ///     Initializes a new instance of the <see cref="PartialItemCommandBuilder" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        public PartialItemCommandBuilder(Database mappingStoreDb)
        {
            _mappingStoreDb = mappingStoreDb;
        }

        /// <summary>
        /// Will build a <see cref="DbCommand"/> that can retrieve the ITEM.IDs,
        /// that belong in the <paramref name="itemSchemePK"/> and used by <paramref name="parentRef"/>.
        /// </summary>
        /// <param name="itemSchemePK"></param>
        /// <param name="parentRef"></param>
        /// <returns></returns>
        public DbCommand BuildForItemIds(long itemSchemePK, IStructureReference parentRef)
        {
            string commandText = string.Format(PARTIAL_QUERY_FORMAT,
                this._mappingStoreDb.BuildParameterName(ParameterNameConstants.ConceptSchemeIdParameter),
                this._mappingStoreDb.BuildParameterName(ParameterNameConstants.AgencyParameter),
                this._mappingStoreDb.BuildParameterName(ParameterNameConstants.IdParameter),
                this._mappingStoreDb.BuildParameterName(ParameterNameConstants.VersionParameter),
                this._mappingStoreDb.BuildParameterName(ParameterNameConstants.ArtefactTypeParameter));

            var parameters = new List<DbParameter>
            {
                this._mappingStoreDb.CreateInParameter(
                    ParameterNameConstants.ConceptSchemeIdParameter,
                    DbType.Int64,
                    itemSchemePK),

                this._mappingStoreDb.CreateInParameter(
                    ParameterNameConstants.AgencyParameter,
                    DbType.AnsiString,
                    parentRef.AgencyId),

                this._mappingStoreDb.CreateInParameter(
                    ParameterNameConstants.IdParameter,
                    DbType.AnsiString,
                    parentRef.MaintainableId),

                this._mappingStoreDb.CreateInParameter(
                    ParameterNameConstants.VersionParameter,
                    DbType.AnsiString,
                    parentRef.Version),

                this._mappingStoreDb.CreateInParameter(
                    ParameterNameConstants.ArtefactTypeParameter,
                    DbType.AnsiString,
                    parentRef.MaintainableStructureEnumType.UrnClass)
            };

            return this._mappingStoreDb.GetSqlStringCommand(commandText, parameters);
        }
    }
}
