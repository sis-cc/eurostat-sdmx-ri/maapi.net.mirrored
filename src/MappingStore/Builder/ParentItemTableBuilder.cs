

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Build the path of tables that connect to each other via <see cref="ItemTableInfo.ForeignKey"/> and <see cref="TableInfo.PrimaryKey"/>
    /// </summary>
    public class ParentItemTableBuilder : IBuilder<IList<ItemTableInfo>, SdmxStructureType>
    {
        /// <summary>
        /// The identifiable table information builder
        /// </summary>
        private readonly IdentifiableTableInfoBuilder _identifiableTableInfoBuilder = new IdentifiableTableInfoBuilder();

        /// <summary>
        /// Builds a path of <see cref="ItemTableInfo"/> starting from <paramref name="buildFrom"/> to the last non-mainaintable that is related to the maintainable
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from</param>
        /// <returns>
        /// The list of <see cref="ItemTableInfo"/> starting from the <paramref name="buildFrom"/> up to, but not including the maintainable artefact.
        /// </returns>
        public IList<ItemTableInfo> Build(SdmxStructureType buildFrom)
        {
            var childToParentStructures = new List<SdmxStructureEnumType>();
            var current = buildFrom;
            while (current != null && !current.IsMaintainable)
            {
                childToParentStructures.Add(current);
                current = current.ParentStructureType;
            }

            var childrenToParentTables = childToParentStructures.Select(_identifiableTableInfoBuilder.Build).Where(t => t != null).ToArray();
            return childrenToParentTables;
        }
    }
}
