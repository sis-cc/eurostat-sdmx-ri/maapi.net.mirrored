// -----------------------------------------------------------------------
// <copyright file="ItemCommandBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using System.Runtime.InteropServices.ComTypes;

    /// <summary>
    ///     The db command builder for retrieving Items
    /// </summary>
    public class ItemCommandBuilder : ICommandBuilder<ItemSqlQuery>
    {
        /// <summary>
        /// Set max number of retrieved items for performance reasons
        /// </summary>
        public static readonly int MAX_SPECIFIC_ITEMS = 500;

        private static readonly string SQL_QUERY_LIGHT =
            "SELECT I.ITEM_ID as SYSID, I.ID, I.PARENT_ITEM " +
                    "FROM ITEM I " +
                    "where I.PARENT_ITEM_SCHEME = {0} ";
        private static readonly string SQL_QUERY =
            "SELECT I.ITEM_ID as SYSID, I.ID, I.PARENT_ITEM " +
                    ",I.ITEM_NAMES, I.ITEM_DESCRIPTIONS, I.ITEM_ANNOTATIONS " +
                    "FROM ITEM I " +
                    "where I.PARENT_ITEM_SCHEME = {0} ";
        private static readonly string SQL_QUERY_COUNT_ALL =
            "SELECT COUNT(I.ITEM_ID) FROM ITEM I where I.PARENT_ITEM_SCHEME = {0}";

        private readonly SpecificItemCriteria _idCriteria;
        private bool _useSimple = false;

        /// <summary>
        ///     The mapping store DB.
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemCommandBuilder" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        public ItemCommandBuilder(Database mappingStoreDb)
        {
            this._mappingStoreDb = mappingStoreDb;
            _idCriteria = new SpecificItemCriteria(mappingStoreDb);
        }

        /// <summary>
        ///     Gets the mapping store DB.
        /// </summary>
        protected Database MappingStoreDb
        {
            get
            {
                return this._mappingStoreDb;
            }
        }

        /// <summary>
        /// Builds a <see cref="DbCommand"/> for retrieving the items.
        /// </summary>
        /// <param name="sysId">The parent item scheme PK.</param>
        /// <returns>The <see cref="DbCommand"/> to execute.</returns>
        public virtual DbCommand Build(long sysId)
        {
            // put this first
            string query = _useSimple ? SQL_QUERY_LIGHT : SQL_QUERY;
            var commandBuilder = new DbCommandBuilder(MappingStoreDb)
                .AddStatementWithParameter(query, ParameterNameConstants.IdParameter, sysId);

            if (_idCriteria.HasClauses())
            {
                commandBuilder.Merge(_idCriteria.GetCommandBuilder());
            }
            return this.MappingStoreDb.GetSqlStringCommandFormat(commandBuilder.GetCommandQuery(), commandBuilder.DbParameters.ToArray());
        }

        /// <summary>
        ///     Build a <see cref="DbCommand" /> from <paramref name="buildFrom" />
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="DbCommand" />.
        /// </returns>
        public virtual DbCommand Build(ItemSqlQuery buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var inParameter = this.MappingStoreDb.CreateInParameter(
                ParameterNameConstants.IdParameter,
                DbType.Int64,
                buildFrom.ParentSysId);
            return this.MappingStoreDb.GetSqlStringCommandFormat(buildFrom.QueryInfo.ToString(), inParameter);
        }

        /// <summary>
        /// Use the builder without retrieving the localised strings.
        /// </summary>
        /// <returns></returns>
        public ItemCommandBuilder WithoutLocalisedString()
        {
            this._useSimple = true;
            return this;
        }

        /// <summary>
        /// Check whether the <paramref name="specificItems"/> number is within performance range.
        /// </summary>
        /// <param name="specificItems">The number of items to check.</param>
        /// <returns><c>true</c> or <c>false</c>.</returns>
        public bool IsWithinLimits(IList<IComplexIdentifiableReferenceObject> specificItems)
        {
            return specificItems.Count <= MAX_SPECIFIC_ITEMS;
        }

        /// <summary>
        /// Adds the query for retrieving nested specific items. 
        /// </summary>
        /// <param name="specificItems">The nested specific items to retrieve.</param>
        /// <exception cref="ArgumentNullException">For <paramref name="specificItems"/>.</exception>
        /// <exception cref="ConstraintException">The number of <paramref name="specificItems"/> cannot be greater than <see cref="MAX_SPECIFIC_ITEMS"/>.</exception>
        public void WithNestedSpecificItems(IList<IComplexIdentifiableReferenceObject> specificItems)
        {
            if (specificItems == null)
            {
                throw new ArgumentNullException(nameof(specificItems));
            }
            if (!specificItems.Any())
            {
                return;
            }

            // There is a limit of parameters
            if (!IsWithinLimits(specificItems))
            {
                throw new ConstraintException("For performance reasons we don't support over " + MAX_SPECIFIC_ITEMS + " items");
            }

            this._idCriteria.AddNestedClause(specificItems);
        }

        /// <summary>
        /// Adds the query for retrieving non-nested specific items.
        /// </summary>
        /// <param name="specificItems">The non-nested specific items to retrieve.</param>
        /// <exception cref="ArgumentNullException">For <paramref name="specificItems"/>.</exception>
        /// <exception cref="ConstraintException">The number of <paramref name="specificItems"/> cannot be greater than <see cref="MAX_SPECIFIC_ITEMS"/>.</exception>
        public void WithNotNestedSpecificItems(IList<IComplexIdentifiableReferenceObject> specificItems)
        {
            if (specificItems == null)
            {
                throw new ArgumentNullException(nameof(specificItems));
            }
            if (!specificItems.Any())
            {
                return;
            }

            // There is a limit of parameters
            if (!IsWithinLimits(specificItems))
            {
                throw new ConstraintException("For performance reasons we don't support over " + MAX_SPECIFIC_ITEMS + " items");
            }

            this._idCriteria.AddNotNestedClause(specificItems);
        }

        /// <summary>
        /// Builds a <see cref="DbCommand"/> that returns the number of items in the given <paramref name="itemSchemePK"/>
        /// </summary>
        /// <param name="itemSchemePK"></param>
        /// <returns></returns>
        public DbCommandBuilder BuildCountAll(long itemSchemePK)
        {
            return new DbCommandBuilder(MappingStoreDb)
                .AddStatementWithParameter(SQL_QUERY_COUNT_ALL, ParameterNameConstants.IdParameter, itemSchemePK);
        }
    }
}