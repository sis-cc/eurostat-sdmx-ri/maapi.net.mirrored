// -----------------------------------------------------------------------
// <copyright file="ItemBase7Engine.cs" company="EUROSTAT">
//   Date Created : 2022-06-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using System.Text;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    //TODO see if all query builders can be merged here
    // like SqlHelper, SqlWhereBuilder, SecurityHelper
    /// <summary>
    /// Helper class for building sql queries.
    /// Will build a string with the query format
    /// and a list of <see cref="DbParameter"/>s for the values.
    /// When done building use <see cref="GetCommandQuery"/> to get the query format
    /// and <see cref="DbParameters"/> to get the parameter with values.
    /// </summary>
    public class DbCommandBuilder
    {
        private readonly StringBuilder _queryBuilder = new StringBuilder();
        private int _parentCount = 0;
        private readonly List<DbParameter> _dbParameters = new List<DbParameter>();
        private readonly Database _mappingStoreDb;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="mappingStoreDb">The connection to the mapping store.</param>
        public DbCommandBuilder(Database mappingStoreDb)
        {
            _mappingStoreDb = mappingStoreDb;
        }

        /// <summary>
        /// Appends the WHERE literal
        /// </summary>
        /// <returns></returns>
        public DbCommandBuilder Where()
        {
            _queryBuilder.Append(" WHERE ");
            return this;
        }

        /// <summary>
        /// Appends a simple or operator
        /// </summary>
        /// <returns>The same instance of <see cref="DbCommandBuilder"/> (fluent API).</returns>
        public DbCommandBuilder Or()
        {
            _queryBuilder.Append(" OR ");
            return this;
        }

        /// <summary>
        /// Appends a simple and operator
        /// </summary>
        /// <returns></returns>
        public DbCommandBuilder And()
        {
            _queryBuilder.Append(" AND ");
            return this;
        }

        /// <summary>
        /// Opens a parenthesis
        /// </summary>
        /// <returns></returns>
        public DbCommandBuilder OpenParenthesis()
        {
            _queryBuilder.Append(" ( ");
            _parentCount++;
            return this;
        }

        /// <summary>
        /// Closes a parenthesis
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">Not an open parenthesis.</exception>
        public DbCommandBuilder CloseParenthesis()
        {
            if (_parentCount == 0)
            {
                throw new InvalidOperationException("No parenthesis open");
            }

            _queryBuilder.Append(" ) ");
            _parentCount--;
            return this;
        }

        /// <summary>
        /// Adds a where clause for string values.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="paramName"></param>
        /// <param name="operator"></param>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        public DbCommandBuilder AddWhereClause(string fieldName, string paramName, TextSearchEnumType @operator, string searchValue)
        {
            string sql = null;
            string value = null;
            string like = "{0} like {1}";
            string notLike = "{0} not like {1}";
            switch (@operator)
            {
                case TextSearchEnumType.Contains:
                    sql = string.Format(like, fieldName, "{0}");
                    value = string.Format("%{0}%", searchValue);
                    break;
                case TextSearchEnumType.StartsWith:
                    sql = string.Format(like, fieldName, "{0}");
                    value = searchValue + "%";
                    break;
                case TextSearchEnumType.EndsWith:
                    sql = string.Format(like, fieldName, "{0}");
                    value = string.Format("%{0}", searchValue);
                    break;
                case TextSearchEnumType.DoesNotContain:
                    sql = string.Format(notLike, fieldName, "{0}");
                    value = string.Format("%{0}%", searchValue);
                    break;
                case TextSearchEnumType.DoesNotStartWith:
                    sql = string.Format(notLike, fieldName, "{0}");
                    value = searchValue + "%";
                    break;
                case TextSearchEnumType.DoesNotEndWith:
                    sql = string.Format(notLike, fieldName, "{0}");
                    value = string.Format("%{0}", searchValue);
                    break;
                case TextSearchEnumType.Equal:
                    sql = string.Format("{0} = {1}", fieldName, "{0}");
                    value = searchValue;
                    break;
                case TextSearchEnumType.NotEqual:
                    sql = string.Format("{0} != {1}", fieldName, "{0}");
                    value = searchValue;
                    break;
            }

            if (sql != null && value != null)
            {
                this.AddStatementWithParameter(sql, paramName, value);
            }

            return this;
        }

        /// <summary>
        /// Adds a where clause for string values.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="paramName"></param>
        /// <param name="operator"></param>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        public DbCommandBuilder AddWhereClause(string fieldName, string paramName, TextSearch @operator, string searchValue)
        {
            return AddWhereClause(fieldName, paramName, @operator.EnumType, searchValue);
        }

        /// <summary>
        /// Adds an in clause for where (like <paramref name="field"/> in (<paramref name="values"/>))
        /// </summary>
        /// <param name="field">The field to filter for.</param>
        /// <param name="parameterName"></param>
        /// <param name="values">The values of the filter.</param>
        /// <returns></returns>
        public DbCommandBuilder AddInClause<T>(string field, string parameterName, IList<T> values)
        {
            DbType dbType = GetParameterDbType(typeof(T));
            return AddInClause(field, parameterName, values, dbType);
        }

        /// <summary>
        /// Adds an in clause for where (like <paramref name="field"/> in (<paramref name="values"/>))
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="field"></param>
        /// <param name="parameterName"></param>
        /// <param name="values"></param>
        /// <param name="parameterDbType"></param>
        /// <returns></returns>
        public DbCommandBuilder AddInClause<T>(string field, string parameterName, IList<T> values, DbType parameterDbType)
        {
            if (values != null && values.Any())
            {
                StringBuilder valuesArrayBuilder = new StringBuilder();

                for (int i = 0; i < values.Count(); i++)
                {
                    T item = values[i];
                    string itemParamName = parameterName + i;
                    string dbParamName = _mappingStoreDb.BuildParameterName(itemParamName);
                    if (valuesArrayBuilder.Length != 0)
                    {
                        valuesArrayBuilder.Append(", ");
                    }
                    valuesArrayBuilder.Append(dbParamName);

                    _dbParameters.Add(this._mappingStoreDb.CreateInParameter(
                        itemParamName,
                        parameterDbType,
                        item));
                }
                _queryBuilder.AppendFormat("{0} in ({1})", field, valuesArrayBuilder.ToString());
            }

            return this;
        }

        /// <summary>
        /// Adds a statement that contains a single parameter.
        /// The <see cref="DbType"/> of the parameter is converted from the <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryFormat"></param>
        /// <param name="paramName"></param>
        /// <param name="paramValue"></param>
        /// <returns></returns>
        public DbCommandBuilder AddStatementWithParameter<T>(string queryFormat, string paramName, T paramValue)
        {
            DbType dbParamType = GetParameterDbType(typeof(T));
            return AddStatementWithParameter(queryFormat, paramName, paramValue, dbParamType);
        }

        /// <summary>
        /// Adds a statement that contains a single parameter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryFormat"></param>
        /// <param name="paramName"></param>
        /// <param name="paramValue"></param>
        /// <param name="dbParamType"></param>
        /// <returns></returns>
        public DbCommandBuilder AddStatementWithParameter<T>(string queryFormat, string paramName, T paramValue, DbType dbParamType)
        {
            string dbParamName = _mappingStoreDb.BuildParameterName(paramName);
            _queryBuilder.AppendFormat(queryFormat, dbParamName);
            _dbParameters.Add(_mappingStoreDb.CreateInParameter(
                paramName,
                dbParamType,
                paramValue));

            return this;
        }

        /// <summary>
        /// Adds the query format and db parameters of another builder.
        /// </summary>
        /// <param name="secondBuilder"></param>
        /// <returns></returns>
        public DbCommandBuilder Merge(DbCommandBuilder secondBuilder)
        {
            _queryBuilder.Append(secondBuilder.GetCommandQuery());
            _dbParameters.AddRange(secondBuilder.DbParameters);
            return this;
        }

        /// <summary>
        /// Adds the query format and db parameters of another builder,
        /// formatting the query with the parameter names given
        /// </summary>
        /// <param name="secondBuilder"></param>
        /// <param name="dbParameters"
        /// <returns></returns>
        public DbCommandBuilder MergeFormat(DbCommandBuilder secondBuilder, params DbParameter[] dbParameters)
        {
            _queryBuilder.AppendFormat(secondBuilder.GetCommandQuery(),
                dbParameters.Select(p => p.ParameterName).ToArray());
            _dbParameters.AddRange(dbParameters);
            return this;
        }

        /// <summary>
        /// Appends an sql statement.
        /// </summary>
        /// <param name="sqlStatement"></param>
        /// <returns></returns>
        public DbCommandBuilder AddStatement(string sqlStatement)
        {
            _queryBuilder.Append(sqlStatement);
            return this;
        }

        /// <summary>
        /// Wrapper for <see cref="SecurityHelper"/>.
        /// </summary>
        /// <returns></returns>
        public DbCommandBuilder AddAuthClause(ICommonStructureQuery structureQuery, WhereState whereState)
        {
            _dbParameters.AddRange(SecurityHelper.AddWhereClauses(structureQuery, _mappingStoreDb, _queryBuilder, whereState));
            return this;
        }

        /// <summary>
        /// Returns the query being built
        /// </summary>
        /// <returns></returns>
        public string GetCommandQuery()
        {
            return _queryBuilder.ToString();
        }

        /// <summary>
        /// The database parameters for the query.
        /// </summary>
        public IList<DbParameter> DbParameters => new ReadOnlyCollection<DbParameter>(_dbParameters);

        /// <summary>
        /// Makes the standard conversion from C# type to <see cref="DbType"/>.
        /// If other conversion is intended, don't use this method.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        private DbType GetParameterDbType(Type @type)
        {
            if (@type == typeof(string))
            {
                return DbType.AnsiString;
            }
            else if (@type == typeof(int))
            {
                return DbType.Int32;
            }
            else if (@type == typeof(long))
            {
                return DbType.Int64;
            }
            else if (@type == typeof(bool))
            {
                return DbType.Boolean;
            }
            else if (@type == typeof(byte))
            {
                return DbType.Byte;
            }
            // TOOD more type conversions
            throw new InvalidOperationException($"type {@type} cannot be matched to a db type.");
        }
    }
}
