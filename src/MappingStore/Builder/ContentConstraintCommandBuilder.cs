// -----------------------------------------------------------------------
// <copyright file="ContentConstraintCommandBuilder.cs" company="EUROSTAT">
//   Date Created : 2022-07-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
// initial implementation ISTAT
//
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    /// <summary>
    /// Builds the retrieve sql query for Content Constraints
    /// </summary>
    public class ContentConstraintCommandBuilder : ArtefactCommandBuilder
    {
        /// <summary>
        /// Initialised a new instance of the class and prepares the sql query based on the given <paramref name="structureQuery"/>.
        /// </summary>
        /// <param name="mappingStoreDb">The mapping store.</param>
        /// <param name="structureQuery">The query to create the sql from.</param>
        public ContentConstraintCommandBuilder(Database mappingStoreDb, ICommonStructureQuery structureQuery) 
            : base(mappingStoreDb)
        {
            TableInfoBuilder builder = new TableInfoBuilder();
            TableInfo tableInfo = builder.Build(SdmxStructureEnumType.ContentConstraint);
            this.Join(tableInfo);

            switch (structureQuery.MaintainableTarget.EnumType)
            {
                case SdmxStructureEnumType.AllowedConstraint:
                    WhereArtefactType(SdmxStructureEnumType.AllowedConstraint, SdmxStructureEnumType.ContentConstraint);
                    break;
                case SdmxStructureEnumType.ActualConstraint:
                    WhereArtefactType(SdmxStructureEnumType.ActualConstraint, SdmxStructureEnumType.ContentConstraint);
                    break;
                case SdmxStructureEnumType.Dataflow:
                case SdmxStructureEnumType.Dsd:
                    // TODO: possible refactoring this constructor
                    // this class is used in different types of requests, like RetrieveAsParents, RetrieveAsChildren etc
                    // it doesn't always serve to have it filter the artefact type in the constructor.
                    // if the target type is DSD or Dataflow, then the constraint is requestd as parent of a dataflow or DSD (RetrieveAsParents request)
                    // where the artefact type in that query is provided elsewhere
                    break;
                default:
                    WhereArtefactType(SdmxStructureEnumType.ActualConstraint, SdmxStructureEnumType.AllowedConstraint, SdmxStructureEnumType.ContentConstraint);
                    break;
            }
        }

        /// <inheritdoc/>
        protected override void CreateArtefactWhereClause(ICommonStructureQuery structureQuery, DbCommandBuilder sqlCommand)
        {
            base.CreateArtefactWhereClause(structureQuery, sqlCommand);

            switch (structureQuery.MaintainableTarget.EnumType)
            {
                case SdmxStructureEnumType.AllowedConstraint:
                    sqlCommand.And().AddStatementWithParameter("T.ACTUAL_DATA = {0}", "ACTUAL_DATA", 0);
                    break;
                case SdmxStructureEnumType.ActualConstraint:
                    sqlCommand.And().AddStatementWithParameter("T.ACTUAL_DATA = {0}", "ACTUAL_DATA", 1);
                    break;
            }
        }
    }
}
