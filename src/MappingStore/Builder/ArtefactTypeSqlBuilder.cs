﻿// -----------------------------------------------------------------------
// <copyright file="ArtefactTypeSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-04-18
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The artefact type SQL builder.
    /// </summary>
    internal class ArtefactTypeSqlBuilder
    {
        /// <summary>
        /// The SQL query template
        /// </summary>
        public const string SqlQueryTemplate = "SELECT D.{0} as SID, '{1}' as STYPE FROM {2} D ";

        /// <summary>
        /// The table information builder
        /// </summary>
        private readonly TableInfoBuilder _tableInfoBuilder = new TableInfoBuilder();

        /// <summary>
        /// Builds an SQL Query for obtaining the type of an ARTEFACT, some types may need extra SQL code. This should be covered by <paramref name="specialCase" />
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns>
        /// The <see cref="SqlQueryInfo" />
        /// </returns>
        public SqlQueryInfo Build(Func<SdmxStructureType, bool> criteria)
        {
            return this.Build(null, criteria);
        }

        /// <summary>
        /// Builds an SQL Query for obtaining the type of an ARTEFACT. Filter by <paramref name="criteria"/>. 
        /// Also some types may need extra SQL code. This should be covered by <paramref name="specialCase" />
        /// </summary>
        /// <param name="specialCase">A function that returns additional SQL code that is appended at <see cref="SqlQueryTemplate" />.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns>
        /// The <see cref="SqlQueryInfo" />
        /// </returns>
        public SqlQueryInfo Build(Func<SdmxStructureType, string> specialCase, Func<SdmxStructureType, bool> criteria)
        {
            specialCase = specialCase ?? (type => string.Empty);
            criteria = criteria ?? (type => true);
            var tableInfos = SdmxStructureType.Values.Where(type => type.IsMaintainable && criteria(type)).Select(type => this._tableInfoBuilder.Build(type)).Where(info => info != null);
            var queries = from tableInfo in tableInfos let extraSql = specialCase(SdmxStructureType.GetFromEnum(tableInfo.StructureType)) ?? string.Empty select string.Format(CultureInfo.InvariantCulture, SqlQueryTemplate, tableInfo.PrimaryKey, tableInfo.StructureType, tableInfo.Table) + extraSql;

            return new SqlQueryInfo { QueryFormat = string.Join(" UNION ALL ", queries), WhereStatus = WhereState.Nothing };
        }

        /// <summary>
        /// Builds an SQL Query for obtaining the type of an ARTEFACT, some types may need extra SQL code. It also provides special handling for Dataflow
        /// </summary>
        /// <returns>
        /// The <see cref="SqlQueryInfo"/>
        /// </returns>
        public SqlQueryInfo Build()
        {
            return this.Build(type => type.EnumType == SdmxStructureEnumType.Dataflow ? " INNER JOIN ARTEFACT A ON A.ART_ID = D.DF_ID {0} " : string.Empty, null);
        }
    }
}