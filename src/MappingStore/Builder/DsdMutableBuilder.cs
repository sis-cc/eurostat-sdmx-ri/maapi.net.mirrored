using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.MappingStoreRetrieval.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Builder
{
    /// <summary>
    /// Will try to move here the logic for constructing a data structure from separate information.
    /// This corresponds to the DsdBuilder in sr.java
    /// </summary>
    public class DsdMutableBuilder
    {
        private long _sysId;

        private IDataStructureMutableObject _dataStructure;

        private IDictionary<string, IStructureReference> _allConceptRefs;

        public DsdMutableBuilder(MaintainableWithPrimaryKey<IDataStructureMutableObject> artefactPkPair)
        {
            _sysId = artefactPkPair.PrimaryKey;
            _dataStructure = artefactPkPair.Maintainable;

            _allConceptRefs = artefactPkPair.References
                .Where(r => r.ReferenceType.Equals(RefTypes.ConceptIdentity))
                .ToDictionary(
                    r => r.SubStructureFullPath,
                    r => r.BuildSubStructureActualTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept)));
        }

        public IDataStructureMutableObject Build()
        {
            return _dataStructure;
        }
    }
}
