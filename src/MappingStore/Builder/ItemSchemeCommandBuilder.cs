// -----------------------------------------------------------------------
// <copyright file="ItemSchemeCommandBuilder7.cs" company="EUROSTAT">
//   Date Created : 2022-06-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Extends the <see cref="ArtefactCommandBuilder"/> to create the sql command for retrieving artefacts of type ItemScheme
    /// </summary>
    public class ItemSchemeCommandBuilder : ArtefactCommandBuilder
    {
        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="mappingStore">The mapping store connection.</param>
        /// <param name="itemSchemeStructureType">The type of ItemScheme.</param>
        public ItemSchemeCommandBuilder(Database mappingStore, SdmxStructureType itemSchemeStructureType)
            : base(mappingStore)
        {
            Join(new TableInfo(itemSchemeStructureType) 
            {
                Table = "ITEM_SCHEME",
                PrimaryKey = "ITEM_SCHEME_ID",
                ExtraFields = "IS_PARTIAL"
            });
        }
    }
}
