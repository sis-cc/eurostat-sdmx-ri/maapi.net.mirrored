// -----------------------------------------------------------------------
// <copyright file="DsdCommandBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-07
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    /// <summary>
    /// SQL query information
    /// </summary>
    internal class DsdCommandBuilder
    {
        /// <summary>
        /// The _attribute dimension refs information
        /// </summary>
        private readonly SqlQueryInfo _attributeDimensionRefsInfo;

        /// <summary>
        /// The _attribute group query information
        /// </summary>
        private readonly SqlQueryInfo _attributeGroupQueryInfo;

        /// <summary>
        /// The _attribute measure query information
        /// </summary>
        private readonly SqlQueryInfo _attributeMeasureQueryInfo;

        /// <summary>
        /// The _command builder
        /// </summary>
        private readonly ItemCommandBuilder _commandBuilder;

        /// <summary>
        /// The _component query information
        /// </summary>
        private readonly SqlQueryInfo _componentQueryInfo;

        /// <summary>
        /// The _group query information
        /// </summary>
        private readonly SqlQueryInfo _groupQueryInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DsdCommandBuilder"/> class.
        /// </summary>
        /// <param name="mappingStoreDb">The mapping store database.</param>
        public DsdCommandBuilder(Database mappingStoreDb)
        {
            var sqlQueryBuilder = new ReferencedSqlQueryBuilder(mappingStoreDb, null);
            this._commandBuilder = new ItemCommandBuilder(mappingStoreDb);
            this._attributeGroupQueryInfo = sqlQueryBuilder.Build(DsdConstant.AttributeAttachmentGroupQueryFormat);
            this._attributeMeasureQueryInfo = sqlQueryBuilder.Build(DsdConstant.AttributeAttachmentMeasureQueryFormat);
            this._groupQueryInfo = sqlQueryBuilder.Build(DsdConstant.GroupQueryFormat);
            this._attributeDimensionRefsInfo = sqlQueryBuilder.Build(DsdConstant.AttributeDimensionFormat);
            this._componentQueryInfo = sqlQueryBuilder.Build(DsdConstant.ComponentQueryFormat);
            this._componentQueryInfo.OrderBy = DsdConstant.ComponentOrderBy;
        }

        /// <summary>
        /// Gets the attribute command.
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <returns>database command</returns>
        public DbCommand GetAttributeCommand(long parentSysId)
        {
            return this._commandBuilder.Build(new ItemSqlQuery(this._attributeDimensionRefsInfo, parentSysId));
        }

        /// <summary>
        /// Gets the attribute measure command.
        /// </summary>
        /// <param name="dsdId">The DSD identifier.</param>
        /// <returns>The database command</returns>
        public DbCommand GetAttributeMeasureCommand(long dsdId)
        {
            return this._commandBuilder.Build(new ItemSqlQuery(this._attributeMeasureQueryInfo, dsdId));
        }

        /// <summary>
        /// Gets the component command.
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <returns>The command</returns>
        public DbCommand GetComponentCommand(long parentSysId)
        {
            return this._commandBuilder.Build(new ItemSqlQuery(this._componentQueryInfo, parentSysId));
        }

        /// <summary>
        /// Gets the group attribute command.
        /// </summary>
        /// <param name="dsdId">The DSD identifier.</param>
        /// <returns>The command</returns>
        public DbCommand GetGroupAttributeCommand(long dsdId)
        {
            return this._commandBuilder.Build(new ItemSqlQuery(this._attributeGroupQueryInfo, dsdId));
        }

        /// <summary>
        /// Gets the group command.
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <returns>The command</returns>
        public DbCommand GetGroupCommand(long parentSysId)
        {
            return this._commandBuilder.Build(new ItemSqlQuery(this._groupQueryInfo, parentSysId));
        }
    }
}