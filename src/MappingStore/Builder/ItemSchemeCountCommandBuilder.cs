// -----------------------------------------------------------------------
// <copyright file="ItemSchemeCountCommandBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-06-06
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Builder
{
    using System.Data.Common;
    using System.Globalization;
    using System.Text;

    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    internal class ItemSchemeCountCommandBuilder
    {
        /// <summary>
        ///     The mapping store DB.
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemSchemeCountCommandBuilder" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        public ItemSchemeCountCommandBuilder(Database mappingStoreDb)
        {
            this._mappingStoreDb = mappingStoreDb;
        }

        /// <summary>
        ///     Build an SQL query from <paramref name="buildFrom" />
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The SQL query
        /// </returns>
        /// <remarks>
        ///     It expects the input SQL query to have 1 format parameters:
        ///     1. <c>The system id of the itemscheme</c>
        /// </remarks>
        public string Build(ItemSchemeCountSqlQuery buildFrom)
        {
            var query = new StringBuilder();

            query.AppendFormat(CultureInfo.InvariantCulture, buildFrom.QueryInfo.ToString(), buildFrom.ItemSchemeSysId);

            return query.ToString();
        }
    }
}
