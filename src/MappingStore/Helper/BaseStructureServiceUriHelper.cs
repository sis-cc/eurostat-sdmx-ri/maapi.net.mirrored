using System;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Helper
{
    /// <summary>
    /// Base implementation of helper class for setting ServiceURL or StructureURL of a maintainable.
    /// </summary>
    public abstract class BaseStructureServiceUriHelper : IStructureServiceUriHelper
    {
        /// <summary>
        /// Sets which type of uri should be added by this instance of the helper.
        /// </summary>
        /// <param name="uriType"></param>
        public abstract void SetStructureServiceUriType(StructureServiceUriType uriType);

        /// <summary>
        /// Gets which type of uri will be added by this instance of the helper.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public abstract StructureServiceUriType GetStructureServiceUriType();

        /// <summary>
        /// Sets base uri used to generate service or structure url.
        /// </summary>
        /// <param name="scheme"></param>
        /// <param name="host"></param>
        /// <param name="pathBase"></param>
        public abstract void SetBaseUri(string scheme, string host, string pathBase);

        /// <summary>
        /// Gets base uri used to generate service or structure url.
        /// </summary>
        /// <returns></returns>
        public abstract Uri GetBaseUri();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="maintainable"></param>
        /// <returns></returns>
        protected string GetStructurePathOfMaintainable(IMaintainableMutableObject maintainable)
        {
            var relativePath = new StringBuilder();

            relativePath.Append(maintainable.StructureType.UrnClass.ToLower()).Append("/");
            relativePath.Append(maintainable.AgencyId).Append("/");
            relativePath.Append(maintainable.Id).Append("/");
            relativePath.Append(maintainable.Version);

            return relativePath.ToString();
        }

        /// <summary>
        /// Sets structure or service Uri of maintainable depending on the uriType set for the helper.
        /// </summary>
        /// <param name="maintainable"></param>
        /// <param name="overwriteExistingUri"></param>
        /// <returns></returns>
        public void AddStructureServiceUri(IMaintainableMutableObject maintainable, bool overwriteExistingUri = false)
        {
            var uriType = GetStructureServiceUriType();
            var baseUri = GetBaseUri();

            switch (uriType)
            {
                case StructureServiceUriType.StructureUri:

                    if (maintainable.StructureURL == null || overwriteExistingUri)
                    {
                        var relativeStructurePath = GetStructurePathOfMaintainable(maintainable);

                        var structureUri =  baseUri == null ? new Uri(relativeStructurePath, UriKind.Relative) : new Uri(GetBaseUri(), relativeStructurePath);
                        maintainable.StructureURL = structureUri;
                    }

                    break;

                case StructureServiceUriType.ServiceUri:

                    if (maintainable.ServiceURL == null || overwriteExistingUri)
                    {
                        var serviceUri = baseUri ?? new Uri("/", UriKind.Relative);
                        maintainable.ServiceURL = serviceUri;
                    }

                    break;
            }
        }
    }
}
