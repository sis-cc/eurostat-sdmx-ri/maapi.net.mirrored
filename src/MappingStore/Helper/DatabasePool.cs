﻿// -----------------------------------------------------------------------
// <copyright file="DatabasePool.cs" company="EUROSTAT">
//   Date Created : 2013-04-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Helper
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Configuration;

    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     A naive <see cref="Database" /> pool
    /// </summary>
    ///// TODO move to mapping store retrieval. 
    public static class DatabasePool
    {
        /// <summary>
        ///     The _databases.
        /// </summary>
        private static readonly ConcurrentDictionary<ConnectionStringSettings, Database> _databases;

        /// <summary>
        ///     Initializes static members of the <see cref="DatabasePool" /> class.
        /// </summary>
        static DatabasePool()
        {
            IEqualityComparer<ConnectionStringSettings> comparer = new ConnectionStringComparer();

            _databases = new ConcurrentDictionary<ConnectionStringSettings, Database>(comparer);
        }

        /// <summary>
        ///     Returns a database instance for the specified <paramref name="connectionStringSettings" />
        /// </summary>
        /// <param name="connectionStringSettings">
        ///     The connection string settings.
        /// </param>
        /// <returns>
        ///     The <see cref="Database" />.
        /// </returns>
        public static Database GetDatabase(ConnectionStringSettings connectionStringSettings)
        {
            if (connectionStringSettings == null)
            {
                throw new ArgumentNullException("connectionStringSettings");
            }

            return _databases.GetOrAdd(connectionStringSettings, settings => new Database(settings));
        }

        /// <summary>
        ///     The connection string comparer.
        /// </summary>
        private class ConnectionStringComparer : IEqualityComparer<ConnectionStringSettings>
        {
            /// <summary>
            ///     Determines whether the specified objects are equal.
            /// </summary>
            /// <returns>
            ///     true if the specified objects are equal; otherwise, false.
            /// </returns>
            /// <param name="x">The first object of type <see cref="ConnectionStringSettings" /> to compare.</param>
            /// <param name="y">The second object of type <see cref="ConnectionStringSettings" /> to compare.</param>
            public bool Equals(ConnectionStringSettings x, ConnectionStringSettings y)
            {
                if (x == null && y == null)
                {
                    return true;
                }

                if (x == null || y == null)
                {
                    return false;
                }

                return string.Equals(x.ConnectionString, y.ConnectionString) && string.Equals(x.ProviderName, y.ProviderName) && string.Equals(x.Name, y.Name);
            }

            /// <summary>
            ///     Returns a hash code for the specified <see cref="ConnectionStringSettings" />.
            /// </summary>
            /// <returns>
            ///     A hash code for the specified object.
            /// </returns>
            /// <param name="obj">The <see cref="ConnectionStringSettings" /> for which a hash code is to be returned.</param>
            /// <exception cref="T:System.ArgumentNullException">
            ///     The type of <paramref name="obj" /> is a reference type and
            ///     <paramref name="obj" /> is null.
            /// </exception>
            public int GetHashCode(ConnectionStringSettings obj)
            {
                if (obj == null)
                {
                    throw new ArgumentNullException("obj");
                }

                int hash = obj.Name != null ? obj.Name.GetHashCode() : 0;
                hash ^= obj.ConnectionString != null ? obj.ConnectionString.GetHashCode() : 0;
                hash ^= obj.ProviderName != null ? obj.ProviderName.GetHashCode() : 0;

                return hash;
            }
        }
    }
}