// -----------------------------------------------------------------------
// <copyright file="TableInfoComparer.cs" company="EUROSTAT">
//   Date Created : 2017-11-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Helper
{
    using Estat.Sri.MappingStoreRetrieval.Model;
    using System.Collections.Generic;

    /// <summary>
    /// Compare two <see cref="TableInfo"/> based on their <see cref="TableInfo.StructureType"/>
    /// </summary>
    /// <seealso cref="System.Collections.Generic.IEqualityComparer{TableInfo}" />
    public class TableInfoComparer : IEqualityComparer<TableInfo>, IEqualityComparer<ItemTableInfo>
    {
        /// <summary>
        /// The structure type comparison
        /// </summary>
        private static readonly TableInfoComparer _structureTypeComparison;

        /// <summary>
        /// Initializes the <see cref="TableInfoComparer"/> class.
        /// </summary>
        static TableInfoComparer()
        {
            _structureTypeComparison = new TableInfoComparer();
        }

        /// <summary>
        /// Gets the structure type comparison.
        /// </summary>
        /// <value>
        /// The structure type comparison.
        /// </value>
        public static IEqualityComparer<ItemTableInfo> StructureTypeComparison
        {
            get
            {
                return _structureTypeComparison;
            }
        }

        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">The first object of type <see cref="TableInfo"/>  to compare.</param>
        /// <param name="y">The second object of type <see cref="TableInfo"/>  to compare.</param>
        /// <returns>
        /// true if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(TableInfo x, TableInfo y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (x == null || y == null)
            {
                return false;
            }

            return x.StructureType.Equals(y.StructureType);
        }

        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">The first object of type <see cref="ItemTableInfo"/>  to compare.</param>
        /// <param name="y">The second object of type <see cref="ItemTableInfo"/>  to compare.</param>
        /// <returns>
        /// true if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(ItemTableInfo x, ItemTableInfo y)
        {
            return Equals((TableInfo)x, (TableInfo)y);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public int GetHashCode(TableInfo obj)
        {
            if (obj == null)
            {
                return 0;
            }

            return obj.StructureType.GetHashCode();
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public int GetHashCode(ItemTableInfo obj)
        {
            return GetHashCode((TableInfo)obj);
        }
    }
}
