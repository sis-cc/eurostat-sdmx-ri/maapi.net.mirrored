// -----------------------------------------------------------------------
// <copyright file="MappingStoreIoc.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Runtime.CompilerServices;
using System.Threading;
using Estat.Sri.Sdmx.MappingStore.Retrieve.Helper;

namespace Estat.Sri.MappingStoreRetrieval.Helper
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using DryIoc;

    using Estat.Sri.MappingStoreRetrieval.Factory;

    /// <summary>
    /// The mapping store IOC.
    /// </summary>
    public static class MappingStoreIoc
    {
        private static AsyncLocal<string> _serviceKey = new AsyncLocal<string>();

        /// <summary>
        /// The service key.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible", Justification = "Reviewed OK.")]
        [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Reviewed. Suppression is OK here.")]
        public static string ServiceKey
        {
            get => _serviceKey.Value;
            set => _serviceKey.Value = value;
        }
        /// <summary>
        /// Gets the container.
        /// </summary>
        [CLSCompliant(false)]
        public static Container Container { get; private set; }

        /// <summary>
        /// Initializes static members of the <see cref="MappingStoreIoc" /> class.
        /// </summary>
        static MappingStoreIoc()
        {
            Container = new Container();
            Container.Register(typeof(IStructureServiceUriHelper), typeof(AsyncLocalStructureServiceUriHelper), Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
        }

        /// <summary>
        /// Registers an implementation of <see cref="IRetrievalEngineContainerFactory" /> interface.
        /// </summary>
        /// <typeparam name="T">The <see cref="IRetrievalEngineContainerFactory" /> interface.</typeparam>
        /// <param name="serviceKey">The service key.</param>
        public static void Register<T>(string serviceKey) 
            where T : IRetrievalEngineContainerFactory
        {
            MappingStoreIoc._serviceKey.Value = serviceKey;
            MappingStoreIoc.Container.Register(typeof(IRetrievalEngineContainerFactory), typeof(T), Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep, serviceKey: serviceKey);
        }

    }
}
