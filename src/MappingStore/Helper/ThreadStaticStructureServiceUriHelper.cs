using System;
using System.Text;
using System.Threading;

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Helper
{
    /// <summary>
    /// Async local implementation of the helper class for setting ServiceURL or StructureURL of a maintainable.
    /// </summary>
    public class AsyncLocalStructureServiceUriHelper : BaseStructureServiceUriHelper
    {
        /// <summary>
        /// Async local base uri used to generate service or structure url.
        /// </summary>
        protected static AsyncLocal<Uri> BaseUri = new AsyncLocal<Uri>();

        /// <summary>
        /// Async local uri type used to generate service or structure url.
        /// </summary>
        protected static AsyncLocal<StructureServiceUriType> UriType= new AsyncLocal<StructureServiceUriType>();

        /// <summary>
        /// Sets which type of uri should be added by this instance of the helper. The value of uri type is thread static.
        /// </summary>
        /// <param name="uriType"></param>
        public override void SetStructureServiceUriType(StructureServiceUriType uriType)
        {
            UriType.Value= uriType;
        }

        /// <summary>
        /// Gets which type of uri will be added by this instance of the helper. The value of uri type is thread static.
        /// </summary>
        /// <returns></returns>
        public override StructureServiceUriType GetStructureServiceUriType()
        {
            return UriType.Value;
        }

        /// <summary>
        /// Sets base uri used to generate service or structure url. The base uri value is thread static.
        /// </summary>
        /// <param name="scheme"></param>
        /// <param name="host"></param>
        /// <param name="pathBase"></param>
        public override void SetBaseUri(string scheme, string host, string pathBase)
        {
            if (string.IsNullOrWhiteSpace(host) && string.IsNullOrWhiteSpace(pathBase))
            {
                BaseUri.Value = null;

                return;
            }

            var url = new StringBuilder();

            if (!string.IsNullOrWhiteSpace(scheme))
            {
                url.Append(scheme).Append("://");
            }

            url.Append(host);

            if ((string.IsNullOrWhiteSpace(host) || !host.EndsWith("/")) && (string.IsNullOrWhiteSpace(pathBase) || !pathBase.StartsWith("/")))
            {
                url.Append("/");
            }

            url.Append(pathBase);

            if (!string.IsNullOrWhiteSpace(pathBase) && !pathBase.EndsWith("/"))
            {
                url.Append("/");
            }

            var baseUri = url.ToString();

            BaseUri.Value = string.IsNullOrWhiteSpace(baseUri) ? null : new Uri(baseUri);
        }

        /// <summary>
        /// Gets base uri used to generate service or structure url. The base uri value is thread static.
        /// </summary>
        /// <returns></returns>
        public override Uri GetBaseUri()
        {
            return BaseUri.Value;
        }
    }
}
