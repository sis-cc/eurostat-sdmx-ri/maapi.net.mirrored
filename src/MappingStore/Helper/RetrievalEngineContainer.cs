// -----------------------------------------------------------------------
// <copyright file="RetrievalEngineContainer.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Helper
{
    using System;
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;

    /// <summary>
    ///     The retrieval engine container.
    /// </summary>
    public class RetrievalEngineContainer : IRetrievalEngineContainer
    {
        #region "10.1 ISTAT ENHANCEMENTS"

        /// <summary>
        ///     The constraint list retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<IContentConstraintMutableObject> _contentConstraintListRetrievalEngine;

        /// <summary>
        ///     The agency scheme list retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<IAgencySchemeMutableObject> _agencySchemeListRetrievalEngine;

        /// <summary>
        ///     The organisation unit scheme list retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<IOrganisationUnitSchemeMutableObject> _organisationUnitSchemeListRetrievalEngine;

        /// <summary>
        ///     The data provider scheme list retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<IDataProviderSchemeMutableObject> _dataProviderSchemeListRetrievalEngine;

        /// <summary>
        ///     The data consumer scheme list retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<IDataConsumerSchemeMutableObject> _dataConsumerSchemeListRetrievalEngine;

        /// <summary>
        ///     The StructureSet retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<IStructureSetMutableObject> _structureSetRetrievalEngine;

        #endregion

        #region all other engine fields

        /// <summary>
        ///     The categorisation from dataflow retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<ICategorisationMutableObject> _categorisationRetrievalEngine;

        /// <summary>
        ///     The category scheme retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<ICategorySchemeMutableObject> _categorySchemeRetrievalEngine;

        /// <summary>
        ///     The code list retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<ICodelistMutableObject> _codeListRetrievalEngine;

        /// <summary>
        ///     The concept scheme retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<IConceptSchemeMutableObject> _conceptSchemeRetrievalEngine;

        /// <summary>
        ///     Gets the partial concept scheme retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<IConceptSchemeMutableObject> _partialConceptSchemeRetrievalEngine;

        /// <summary>
        ///     The dataflow retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<IDataflowMutableObject> _dataflowRetrievalEngine;

        /// <summary>
        ///     The DSD retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<IDataStructureMutableObject> _dsdRetrievalEngine;

        /// <summary>
        ///     The HCL retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<IHierarchicalCodelistMutableObject> _hclRetrievalEngine;

        /// <summary>
        ///     The _partial code list retrieval engine.
        /// </summary>
        private readonly IRetrievalEngine<ICodelistMutableObject> _partialCodeListRetrievalEngine;

        /// <summary>
        ///     The _subset code list retrieval engine.
        /// </summary>
        private readonly ISubsetCodelistRetrievalEngine _subsetCodeListRetrievalEngine;

        /// <summary>
        /// The constrained code list retrieval engine.
        /// </summary>
        private readonly IConstrainedCodeListRetrievalEngine _constrainedCodeListRetrievalEngine;

        /// <summary>
        ///     The header retrieval engine.
        /// </summary>
        private readonly IHeaderRetrieverEngine _headerRetrieverEngine;

        /// <summary>
        /// The MSD retriever
        /// </summary>
        private readonly IRetrievalEngine<IMetadataStructureDefinitionMutableObject> _msdRetrieverEngine;

        /// <summary>
        /// The metadata flow retrieval engine
        /// </summary>
        private readonly IRetrievalEngine<IMetadataFlowMutableObject> _metadataFlowRetrievalEngine;

        /// <summary>
        /// The partial category scheme retrieval engine
        /// </summary>
        private readonly IRetrievalEngine<ICategorySchemeMutableObject> _partialCategorySchemeRetrievalEngine;

        /// <summary>
        /// The retrieval engine for Provision Agreement
        /// </summary>
        private IRetrievalEngine<IProvisionAgreementMutableObject> _provisionAgreementRetrievalEngine;

        #endregion

        /// <summary>
        ///     Initializes a new instance of the <see cref="RetrievalEngineContainer" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping Store DB.
        /// </param>
        public RetrievalEngineContainer(Database mappingStoreDb)
        {
            DataflowFilter filter = DataflowFilterContext.CurrentFilter;
            var artefactTypeSqlBuilder = new ArtefactTypeSqlBuilder();
            this._categorisationRetrievalEngine = new CategorisationRetrievalEngine(mappingStoreDb, filter);
            this._categorySchemeRetrievalEngine = new CategorySchemeRetrievalEngine(mappingStoreDb);
            this._partialCategorySchemeRetrievalEngine = new PartialCategorySchemeRetrievalEngine(mappingStoreDb);
            this._codeListRetrievalEngine = new CodeListRetrievalEngine(mappingStoreDb);
            this._conceptSchemeRetrievalEngine = new ConceptSchemeRetrievalEngine(mappingStoreDb);
            this._partialConceptSchemeRetrievalEngine = new PartialConceptSchemeRetrievalEngine(mappingStoreDb);
            this._dataflowRetrievalEngine = new DataflowRetrievalEngine(mappingStoreDb, filter);
            this._dsdRetrievalEngine = new DsdRetrievalEngine(mappingStoreDb);
            this._hclRetrievalEngine = new HierarchicalCodeListRetrievealEngine(mappingStoreDb);
            this._partialCodeListRetrievalEngine = new PartialCodeListRetrievalEngine(mappingStoreDb);
            this._subsetCodeListRetrievalEngine = new SubsetCodelistRetrievalEngine(mappingStoreDb);
            this._constrainedCodeListRetrievalEngine = new ConstrainedCodeListRetrievalEngine(mappingStoreDb);
            this._headerRetrieverEngine = new HeaderRetrieverEngine(mappingStoreDb);
            this._msdRetrieverEngine = new MsdRetrievalEngine(mappingStoreDb, artefactTypeSqlBuilder);
            this._metadataFlowRetrievalEngine = new MetadataFlowRetriever(mappingStoreDb);
            this._provisionAgreementRetrievalEngine = new ProvisionAgreementRetriverEngine(mappingStoreDb);

            //// "10.2 ISTAT ENHANCEMENTS"

            this._agencySchemeListRetrievalEngine = new AgencySchemeRetrievalEngine(mappingStoreDb);
            this._organisationUnitSchemeListRetrievalEngine = new OrganisationUnitSchemeRetrievalEngine(mappingStoreDb);
            this._dataProviderSchemeListRetrievalEngine = new DataProviderSchemeRetrievalEngine(mappingStoreDb);
            this._dataConsumerSchemeListRetrievalEngine = new DataConsumerSchemeRetrievalEngine(mappingStoreDb);
            this._structureSetRetrievalEngine = new StructureSetRetrievalEngine(mappingStoreDb);
            this._contentConstraintListRetrievalEngine = new ContentConstraintRetrievalEngine(mappingStoreDb);
            //// END "10.2 ISTAT ENHANCEMENTS"
        }

        #region "10.3 ISTAT ENHANCEMENTS"

        /// <summary>
        ///     Gets the constraint list retrieval engine.
        /// </summary>
        public IRetrievalEngine<IContentConstraintMutableObject> ContentConstraintRetrievalEngine
        {
            get
            {
                return this._contentConstraintListRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the agency scheme list retrieval engine.
        /// </summary>
        public IRetrievalEngine<IAgencySchemeMutableObject> AgencySchemeRetrievalEngine
        {
            get
            {
                return this._agencySchemeListRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the organisation unit scheme list retrieval engine.
        /// </summary>
        public IRetrievalEngine<IOrganisationUnitSchemeMutableObject> OrganisationUnitSchemeRetrievalEngine
        {
            get
            {
                return this._organisationUnitSchemeListRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the data provider scheme list retrieval engine.
        /// </summary>
        public IRetrievalEngine<IDataProviderSchemeMutableObject> DataProviderSchemeRetrievalEngine
        {
            get
            {
                return this._dataProviderSchemeListRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the data consumer scheme list retrieval engine.
        /// </summary>
        public IRetrievalEngine<IDataConsumerSchemeMutableObject> DataConsumerSchemeRetrievalEngine
        {
            get
            {
                return this._dataConsumerSchemeListRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the StructureSet retrieval engine.
        /// </summary>
        public IRetrievalEngine<IStructureSetMutableObject> StructureSetRetrievalEngine
        {
            get
            {
                return this._structureSetRetrievalEngine;
            }
        }

        #endregion

        #region all other engine properties

        /// <summary>
        ///     Gets the categorisation from dataflow retrieval engine.
        /// </summary>
        public IRetrievalEngine<ICategorisationMutableObject> CategorisationRetrievalEngine
        {
            get
            {
                return this._categorisationRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the category scheme retrieval engine.
        /// </summary>
        public IRetrievalEngine<ICategorySchemeMutableObject> CategorySchemeRetrievalEngine
        {
            get
            {
                return this._categorySchemeRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the partial category scheme retrieval engine.
        /// </summary>
        public IRetrievalEngine<ICategorySchemeMutableObject> PartialCategorySchemeRetrievalEngine
        {
            get
            {
                return this._partialCategorySchemeRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the code list retrieval engine.
        /// </summary>
        public IRetrievalEngine<ICodelistMutableObject> CodeListRetrievalEngine
        {
            get
            {
                return this._codeListRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the concept scheme retrieval engine.
        /// </summary>
        public IRetrievalEngine<IConceptSchemeMutableObject> ConceptSchemeRetrievalEngine
        {
            get
            {
                return this._conceptSchemeRetrievalEngine;
            }
        }

        /// <summary>
        ///      Gets the partial concept scheme retrieval engine.
        /// </summary>
        public IRetrievalEngine<IConceptSchemeMutableObject> PartialConceptSchemeRetrievalEngine
        {
            get
            {
                return this._partialConceptSchemeRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the dataflow retrieval engine.
        /// </summary>
        public IRetrievalEngine<IDataflowMutableObject> DataflowRetrievalEngine
        {
            get
            {
                return this._dataflowRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the DSD retrieval engine.
        /// </summary>
        public IRetrievalEngine<IDataStructureMutableObject> DSDRetrievalEngine
        {
            get
            {
                return this._dsdRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the HCL retrieval engine.
        /// </summary>
        public IRetrievalEngine<IHierarchicalCodelistMutableObject> HclRetrievalEngine
        {
            get
            {
                return this._hclRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the partial code list retrieval engine.
        /// </summary>
        public IRetrievalEngine<ICodelistMutableObject> PartialCodeListRetrievalEngine
        {
            get
            {
                return this._partialCodeListRetrievalEngine;
            }
        }
        
        /// <summary>
        ///     Gets the subset code list retrieval engine.
        /// </summary>
        public ISubsetCodelistRetrievalEngine SubsetCodeListRetrievalEngine
        {
            get
            {
                return this._subsetCodeListRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the constrained code list retrieval engine.
        /// </summary>
        public IConstrainedCodeListRetrievalEngine ConstrainedCodeListRetrievalEngine
        {
            get
            {
                return this._constrainedCodeListRetrievalEngine;
            }
        }

        /// <summary>
        ///     Gets the header retrieval engine.
        /// </summary>
        public IHeaderRetrieverEngine HeaderRetrieverEngine
        {
            get
            {
                return this._headerRetrieverEngine;
            }
        }

        /// <summary>
        /// Gets the MSD retriever
        /// </summary>
        public IRetrievalEngine<IMetadataStructureDefinitionMutableObject> MsdRetrieverEngine
        {
            get
            {
                return this._msdRetrieverEngine;
            }
        }

        /// <summary>
        /// Gets the metadata flow retrieval engine
        /// </summary>
        public IRetrievalEngine<IMetadataFlowMutableObject> MetadataFlowRetrievalEngine
        {
            get
            {
                return this._metadataFlowRetrievalEngine;
            }
        }

        /// <summary>
        /// Gets the provision agreement retrieval engine.
        /// </summary>
        /// <value>
        /// The provision agreement retrieval engine.
        /// </value>
        public IRetrievalEngine<IProvisionAgreementMutableObject> ProvisionAgreementRetrievalEngine
        {
            get { return this._provisionAgreementRetrievalEngine; }
        }

        #endregion

        /// <inheritdoc/>
        public IRetrievalEngine<IMaintainableMutableObject> GetEngine(SdmxStructureType structureType)
        {
            switch (structureType.EnumType)
            {
                case SdmxStructureEnumType.Categorisation:
                    return CategorisationRetrievalEngine;
                case SdmxStructureEnumType.CategoryScheme:
                    return CategorySchemeRetrievalEngine;
                case SdmxStructureEnumType.CodeList:
                    return CodeListRetrievalEngine;
                case SdmxStructureEnumType.ConceptScheme:
                    return ConceptSchemeRetrievalEngine;
                case SdmxStructureEnumType.Dataflow:
                    return DataflowRetrievalEngine;
                case SdmxStructureEnumType.Dsd:
                    return DSDRetrievalEngine;
                case SdmxStructureEnumType.Msd:
                    return MsdRetrieverEngine;
                case SdmxStructureEnumType.MetadataFlow:
                    return MetadataFlowRetrievalEngine;
                case SdmxStructureEnumType.ProvisionAgreement:
                    return ProvisionAgreementRetrievalEngine;
                case SdmxStructureEnumType.ContentConstraint:
                case SdmxStructureEnumType.ActualConstraint:
                case SdmxStructureEnumType.AllowedConstraint:
                    return ContentConstraintRetrievalEngine;
                case SdmxStructureEnumType.AgencyScheme:
                    return AgencySchemeRetrievalEngine;
                case SdmxStructureEnumType.OrganisationUnitScheme:
                    return OrganisationUnitSchemeRetrievalEngine;
                case SdmxStructureEnumType.DataProviderScheme:
                    return DataProviderSchemeRetrievalEngine;
                case SdmxStructureEnumType.DataConsumerScheme:
                    return DataConsumerSchemeRetrievalEngine;
                case SdmxStructureEnumType.StructureSet:
                    return StructureSetRetrievalEngine;
                case SdmxStructureEnumType.HierarchicalCodelist:
                    return HclRetrievalEngine;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Will return the partial engine for those artefact types that support one,
        /// and the default engine for those artefact types that don't.
        /// </summary>
        /// <param name="structureType"></param>
        /// <returns></returns>
        public IRetrievalEngine<IMaintainableMutableObject> GetPartialEngine(SdmxStructureType structureType)
        {
            switch (structureType.EnumType)
            {
                case SdmxStructureEnumType.CategoryScheme:
                    return PartialCategorySchemeRetrievalEngine;
                case SdmxStructureEnumType.ConceptScheme:
                    return PartialConceptSchemeRetrievalEngine;
                case SdmxStructureEnumType.CodeList: 
                    return PartialCodeListRetrievalEngine;
                default:
                    return GetEngine(structureType);
            }
        }
    }
}