// -----------------------------------------------------------------------
// <copyright file="IRetrievalEngineContainer.cs" company="EUROSTAT">
//   Date Created : 2013-04-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Helper
{
    using System;
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;

    /// <summary>
    /// The retrieval engine container.
    /// </summary>
    public interface IRetrievalEngineContainer
    {

        #region "10.5 ISTAT ENHANCEMENTS"
        /// <summary>
        /// 
        /// </summary>
        IRetrievalEngine<IAgencySchemeMutableObject> AgencySchemeRetrievalEngine { get; }
        /// <summary>
        /// 
        /// </summary>
        IRetrievalEngine<IContentConstraintMutableObject> ContentConstraintRetrievalEngine { get; }
        /// <summary>
        /// 
        /// </summary>
        IRetrievalEngine<IDataConsumerSchemeMutableObject> DataConsumerSchemeRetrievalEngine { get; }
        /// <summary>
        /// 
        /// </summary>
        IRetrievalEngine<IDataProviderSchemeMutableObject> DataProviderSchemeRetrievalEngine { get; }
        /// <summary>
        /// 
        /// </summary>
        IRetrievalEngine<IOrganisationUnitSchemeMutableObject> OrganisationUnitSchemeRetrievalEngine { get; }
        /// <summary>
        /// 
        /// </summary>
        IRetrievalEngine<IStructureSetMutableObject> StructureSetRetrievalEngine { get; }

        #endregion

        /// <summary>
        ///     Gets the categorisation from dataflow retrieval engine.
        /// </summary>
        IRetrievalEngine<ICategorisationMutableObject> CategorisationRetrievalEngine { get; }

        /// <summary>
        ///     Gets the category scheme retrieval engine.
        /// </summary>
        IRetrievalEngine<ICategorySchemeMutableObject> CategorySchemeRetrievalEngine { get; }

        /// <summary>
        ///     Gets the code list retrieval engine.
        /// </summary>
        IRetrievalEngine<ICodelistMutableObject> CodeListRetrievalEngine { get; }

        /// <summary>
        ///     Gets the concept scheme retrieval engine.
        /// </summary>
        IRetrievalEngine<IConceptSchemeMutableObject> ConceptSchemeRetrievalEngine { get; }

        /// <summary>
        ///     Gets the partial concept scheme retrieval engine.
        /// </summary>
        IRetrievalEngine<IConceptSchemeMutableObject> PartialConceptSchemeRetrievalEngine { get; }

        /// <summary>
        ///     Gets the dataflow retrieval engine.
        /// </summary>
        IRetrievalEngine<IDataflowMutableObject> DataflowRetrievalEngine { get; }

        /// <summary>
        ///     Gets the DSD retrieval engine.
        /// </summary>
        IRetrievalEngine<IDataStructureMutableObject> DSDRetrievalEngine { get; }

        /// <summary>
        ///     Gets the HCL retrieval engine.
        /// </summary>
        IRetrievalEngine<IHierarchicalCodelistMutableObject> HclRetrievalEngine { get; }

        /// <summary>
        ///     Gets the partial code list retrieval engine.
        /// </summary>
        IRetrievalEngine<ICodelistMutableObject> PartialCodeListRetrievalEngine { get; }

        /// <summary>
        ///     Gets the subset code list retrieval engine.
        /// </summary>
        ISubsetCodelistRetrievalEngine SubsetCodeListRetrievalEngine { get; }

        /// <summary>
        ///     Gets the constrained code list retrieval engine.
        /// </summary>
        IConstrainedCodeListRetrievalEngine ConstrainedCodeListRetrievalEngine { get; }

        /// <summary>
        ///     Gets the header retrieval engine.
        /// </summary>
        IHeaderRetrieverEngine HeaderRetrieverEngine { get; }

        /// <summary>
        /// Gets the MSD retriever engine.
        /// </summary>
        /// <value>
        /// The MSD retriever engine.
        /// </value>
        IRetrievalEngine<IMetadataStructureDefinitionMutableObject> MsdRetrieverEngine { get; }

        /// <summary>
        /// Gets the metadata flow retrieval engine.
        /// </summary>
        /// <value>
        /// The metadata flow retrieval engine.
        /// </value>
        IRetrievalEngine<IMetadataFlowMutableObject> MetadataFlowRetrievalEngine { get; }

        /// <summary>
        /// Gets the provision agreement retrieval engine.
        /// </summary>
        /// <value>
        /// The provision agreement retrieval engine.
        /// </value>
        IRetrievalEngine<IProvisionAgreementMutableObject> ProvisionAgreementRetrievalEngine { get; }

        /// <summary>
        /// Gets the specific artefact engine based on a <paramref name="structureType"/>.
        /// </summary>
        /// <param name="structureType">The structure type to get the engine for.</param>
        /// <returns>The structure type engine.</returns>
        IRetrievalEngine<IMaintainableMutableObject> GetEngine(SdmxStructureType structureType);

        /// <summary>
        /// Gets the specific artefact engine for retrieving partial data
        /// </summary>
        /// <param name="structureType">The structure type to get the partial engine for.</param>
        /// <returns><c>null</c> if no partial engine for this structure type.</returns>
        IRetrievalEngine<IMaintainableMutableObject> GetPartialEngine(SdmxStructureType structureType);
    }
}