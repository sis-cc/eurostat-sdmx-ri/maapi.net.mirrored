using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Estat.Sri.Sdmx.MappingStore.Retrieve.Model.TextFormatValue;
using Newtonsoft.Json;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Helper
{
    public class TextFormatValueUtils
    {

        /**
         * helper method for creating a json for text format properties that are stored as json. Currently only the sentinel values are stored as json
         *
         * @param textFormatBean
         * @return
         */
        public static String CreateJsonFromTextFormatProperties(ITextFormat textFormatBean)
        {

            //currently only sentinel values are stored a json. If in the future we wish to store something else,
            // we can create a new subclass of TextFormatValue like we created SentinelTextFormatValue, specifying a different "type" value so that we can distinguish them.
            if (!textFormatBean.SentinelValues.Any())
            {
                return null;
            }

            var sentinelTextFormatValue = CreateSentinelTextFormatValue(textFormatBean.SentinelValues);

            string json = null;
            try
            {
                json = JsonConvert.SerializeObject(sentinelTextFormatValue);
            }
            catch (Exception e)
            {
                throw new SdmxException(e, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), "Error while writing text format values to json");
            }

            return json;
        }

        private static SentinelTextFormatValue CreateSentinelTextFormatValue(IList<ISentinelValue> sentinelValues)
        {
            var sentinelTextFormatValue = new SentinelTextFormatValue();
            sentinelTextFormatValue.SentinelItems = sentinelValues.Select(x => CreateSentinelItem(x)).ToList();

            return sentinelTextFormatValue;
        }

        private static SentinelItem CreateSentinelItem(ISentinelValue sentinelBean)
        {

            SentinelItem item = new SentinelItem();
            item.Value = sentinelBean.Value;
            item.Names = sentinelBean.Names.Select(x => CreateTextItem(x)).ToList();
            item.Descriptions = sentinelBean.Descriptions.Select(x => CreateTextItem(x)).ToList();

            return item;
        }

        private static TextItem CreateTextItem(ITextTypeWrapper textTypeWrapper)
        {

            TextItem textItem = new TextItem();
            textItem.Locale = textTypeWrapper.Locale;
            textItem.Value = textTypeWrapper.Value;
            return textItem;
        }

        public static void PopulateTextFormatPropertiesFromJson(ITextFormatMutableObject textFormatBean, String facetValueJson)
        {
            // only SentinelTextFormatValue currently support, so deserialize directly to that. (in the future we can add more classes and distinguish them using the TextFormatValue.type
            try
            {
                SentinelTextFormatValue sentinelTextFormatValue = JsonConvert.DeserializeObject<SentinelTextFormatValue>(facetValueJson);
                List<ISentinelValueMutableObject> sentinelBeanList = CreateSentinelBeans(sentinelTextFormatValue);
                textFormatBean.SentinelValues.AddAll(sentinelBeanList);
            }
            catch (Exception e)
            {
                throw new SdmxException(e, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), "Error while reading text format values from json");
            }
        }
        private static List<ISentinelValueMutableObject> CreateSentinelBeans(SentinelTextFormatValue sentinelTextFormatValue)
        {
            List<ISentinelValueMutableObject> sentinelMutableBeans = sentinelTextFormatValue.SentinelItems.Select(x=>CreateSentinelBean(x)).ToList();
            return sentinelMutableBeans;
        }

        private static ISentinelValueMutableObject CreateSentinelBean(SentinelItem sentinelItem)
        {
            var sentinelMutableBean = new SentinelValueMutableCore();
            sentinelMutableBean.Value = sentinelItem.Value;
            foreach(var name in sentinelItem.Names)
            {
                sentinelMutableBean.AddName(name.Locale, name.Value);
            }

            foreach (var description in sentinelItem.Descriptions)
            {
                sentinelMutableBean.AddDescription(description.Locale, description.Value);
            }
           
            return sentinelMutableBean;
        }
    }
}