// -----------------------------------------------------------------------
// <copyright file="SecurityHelper.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Utils.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     Helper class for adding allowed dataflows as clauses to prepared statements
    /// </summary>
    internal static class SecurityHelper
    {
        private const string And = " AND ";

        /// <summary>
        ///     Add where clauses and populate <paramref name="parameters" />
        /// </summary>
        /// <param name="maintainableRef">
        ///     The maintainable reference which may contain ID, AGENCY ID and/or VERSION.
        /// </param>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <param name="sqlCommand">
        ///     The SQL command.
        /// </param>
        /// <param name="parameters">
        ///     The parameters.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed dataflows.
        /// </param>
        /// <param name="whereState">the current state of the WHERE clause in <paramref name="sqlCommand" /></param>
        /// <returns>
        ///     The <paramref name="parameters" />
        /// </returns>
        public static IList<DbParameter> AddWhereClauses(
            IMaintainableRefObject maintainableRef, 
            Database database, 
            StringBuilder sqlCommand, 
            IList<DbParameter> parameters, 
            ICollection<IMaintainableRefObject> allowedDataflows, 
            WhereState whereState)
        {
            if (allowedDataflows == null)
            {
                return parameters;
            }

            maintainableRef = maintainableRef ?? new MaintainableRefObjectImpl();
            switch (whereState)
            {
                case WhereState.Nothing:
                    sqlCommand.Append(" WHERE (");
                    break;
                case WhereState.Where:
                    break;
                case WhereState.And:
                    sqlCommand.Append(" AND (");
                    break;
            }

            int lastClause = sqlCommand.Length;
            int count = 0;
            bool addedClauses = false;
            foreach (IMaintainableRefObject allowedDataflow in allowedDataflows)
            {
                // TODO check if allowed dataflow id is mandatory. If not we need to change this.
                if (!maintainableRef.HasMaintainableId()
                    || string.Equals(maintainableRef.MaintainableId, allowedDataflow.MaintainableId))
                {
                    string countString = count.ToString(CultureInfo.InvariantCulture);
                    sqlCommand.Append("(");

                    // id 
                    string idParam = ParameterNameConstants.IdParameter + countString;
                    sqlCommand.AppendFormat(" A.ID = {0} ", database.BuildParameterName(idParam));
                    parameters.Add(database.CreateInParameter(idParam, DbType.String, allowedDataflow.MaintainableId));

                    // version
                    var versionParameters = allowedDataflow.GenerateVersionParameters(
                        database, 
                        parameters, 
                        "A.VERSION", 
                        versionNumber =>
                        string.Format(
                            CultureInfo.InvariantCulture, 
                            "{0}{1}_{2}", 
                            ParameterNameConstants.VersionParameter, 
                            versionNumber, 
                            countString));
                    if (versionParameters.Length > 0)
                    {
                        sqlCommand.AppendFormat(CultureInfo.InvariantCulture, "AND {0}", versionParameters);
                    }

                    // agency
                    if (allowedDataflow.HasAgencyId())
                    {
                        string name = ParameterNameConstants.AgencyParameter + countString;
                        sqlCommand.AppendFormat(" AND A.AGENCY = {0} ", database.BuildParameterName(name));
                        parameters.Add(database.CreateInParameter(name, DbType.String, allowedDataflow.AgencyId));
                    }

                    sqlCommand.Append(" ) ");
                    lastClause = sqlCommand.Length;
                    sqlCommand.Append(" OR ");
                    count++;
                    addedClauses = true;
                }
            }

            sqlCommand.Length = lastClause;

            if (!addedClauses)
            {
                sqlCommand.Append("'ACCESS' = 'DENIED'");
            }

            sqlCommand.Append(" ) ");

            return parameters;
        }

        /// <summary>
        ///     Checks if there is a .
        /// </summary>
        /// <param name="allowedDataflows">
        ///     The allowed dataflows.
        /// </param>
        /// <param name="requestedObject">
        ///     The requested object.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public static bool Contains(
            ICollection<IMaintainableRefObject> allowedDataflows,
            IMaintainableRefObject requestedObject)
        {
            if (allowedDataflows == null)
            {
                return true;
            }

            foreach (IMaintainableRefObject allowedDataflow in allowedDataflows)
            {
                if ((!requestedObject.HasMaintainableId()
                     || string.Equals(allowedDataflow.MaintainableId, requestedObject.MaintainableId))
                    && ((!allowedDataflow.HasVersion() || !requestedObject.HasVersion()
                         || string.Equals(requestedObject.Version, allowedDataflow.Version))
                        && (!allowedDataflow.HasAgencyId() || !requestedObject.HasAgencyId()
                            || string.Equals(requestedObject.AgencyId, allowedDataflow.AgencyId))))
                {
                    return true;
                }
            }

            return false;
        }

        public static IList<DbParameter> AddWhereClauses(
           ICommonStructureQuery requestedArtefact, 
           Database database,
           StringBuilder sqlCommand,
           WhereState whereState)
        {
            Mapping.Api.Model.ISdmxAuthorization allowedStructures;
            using (var scope = new SdmxAuthorizationScope(Mapping.Api.Constant.Authorisation.Required))
            {
                allowedStructures = scope.CurrentSdmxAuthorisation;
            }

            SqlWhereBuilder currentStructureStatements = new SqlWhereBuilder(database);

            int count = 0;
            ISet<IStructureReference> allowedStructures1 = allowedStructures.GetAllowedStructureReferences();
            foreach (IStructureReference allowedStructure in allowedStructures1.Where(a => IsRelatedTo(a, requestedArtefact)))
            {
                // Allow everything
                if ((!allowedStructure.HasAgencyId() || allowedStructure.AgencyId.Equals("*")) &&
                    (!allowedStructure.HasVersion() || allowedStructure.Version.Equals("*")) &&
                    (!allowedStructure.HasMaintainableId() || allowedStructure.MaintainableId.Equals("*")) &&
                    allowedStructure.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.Any)
                {
                    return Array.Empty<DbParameter>();
                }

                string countString = count.ToString(CultureInfo.InvariantCulture);

                // id
                if (allowedStructure.HasMaintainableId() && !allowedStructure.MaintainableId.Equals("*"))
                {
                    string idParam = "sec_" + ParameterNameConstants.IdParameter + countString;
                    currentStructureStatements.Add(" AB.ID = {0} ", idParam, DbType.AnsiString, allowedStructure.MaintainableId);
                }

                // version
                if (allowedStructure.HasVersion() && !allowedStructure.Version.Equals("*"))
                {
                    allowedStructure.GenerateVersionParameters(
                        currentStructureStatements,
                    versionNumber =>
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "{0}{1}_{2}",
                        ParameterNameConstants.VersionParameter,
                        versionNumber,
                        countString));
                }

                // agency
                if (allowedStructure.HasAgencyId() && !allowedStructure.AgencyId.Equals("*"))
                {
                    string name = "sec_" + ParameterNameConstants.AgencyParameter + countString;
                    currentStructureStatements.Add(" AB.AGENCY = {0} ", name, DbType.AnsiString, allowedStructure.AgencyId);
                }

                // type
                if (allowedStructure.MaintainableStructureEnumType.EnumType != SdmxStructureEnumType.Any)
                {
                    string name = ParameterNameConstants.ArtefactTypeParameter + countString;
                    // TODO handle special ContentConstraint case
                    currentStructureStatements.Add(" AB.ARTEFACT_TYPE = {0} ", name, DbType.AnsiString, allowedStructure.MaintainableStructureEnumType.UrnClass);
                }

                if (currentStructureStatements.HasClauses())
                {
                    currentStructureStatements.Merge(SqlWhereBuilder.And);
                }
                count++;
            }

            switch (whereState)
            {
                case WhereState.Nothing:
                    sqlCommand.Append(" WHERE ");
                    break;

                case WhereState.Where:
                    break;

                case WhereState.And:
                    sqlCommand.Append(" AND ");
                    break;
            }
            if (currentStructureStatements.HasQueue())
            {
                sqlCommand.Append(currentStructureStatements.Build(SqlWhereBuilder.Or));
                return currentStructureStatements.DbParameters;
            }
            else
            {
                sqlCommand.Append("( 'ACCESS' = 'DENIED' )");
            }

            return Array.Empty<DbParameter>();
        }

        private static bool IsRelatedTo(IStructureReference a, ICommonStructureQuery requestedArtefact)
        {
            if (requestedArtefact == null)
            {
                return true;
            }

            return a.MaintainableStructureEnumType == requestedArtefact.MaintainableTarget 
                || (a.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.ContentConstraint && requestedArtefact.MaintainableTarget.EnumType.IsOneOf(SdmxStructureEnumType.AllowedConstraint, SdmxStructureEnumType.ActualConstraint))
                || (requestedArtefact.MaintainableTarget.EnumType == SdmxStructureEnumType.ContentConstraint && a.MaintainableStructureEnumType.EnumType.IsOneOf(SdmxStructureEnumType.AllowedConstraint, SdmxStructureEnumType.ActualConstraint))
                || requestedArtefact.MaintainableTarget.EnumType == SdmxStructureEnumType.Any
                || a.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.Any;
        }
    }
}