using System;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Helper
{
    /// <summary>
    /// Helper interface for setting ServiceURL or StructureURL of a maintainable
    /// </summary>
    public interface IStructureServiceUriHelper
    {
        /// <summary>
        /// Sets which type of uri should be added by this instance of the helper.
        /// </summary>
        /// <param name="uriType"></param>
        void SetStructureServiceUriType(StructureServiceUriType uriType);

        /// <summary>
        /// Gets which type of uri will be added to the maintainable by this instance.
        /// </summary>
        /// <returns></returns>
        StructureServiceUriType GetStructureServiceUriType();

        /// <summary>
        /// Sets base uri used to generate service or structure url.
        /// </summary>
        /// <param name="scheme"></param>
        /// <param name="host"></param>
        /// <param name="pathBase"></param>
        void SetBaseUri(string scheme, string host, string pathBase);

        /// <summary>
        /// Gets base uri used to generate service or structure url.
        /// </summary>
        /// <returns></returns>
        Uri GetBaseUri();

        /// <summary>
        /// Sets structure or service Uri of maintainable depending on the uriType set for the helper.
        /// </summary>
        /// <param name="maintainable"></param>
        /// <param name="overwriteExistingUri"></param>
        /// <returns></returns>
        void AddStructureServiceUri(IMaintainableMutableObject maintainable, bool overwriteExistingUri = false);
    }

    /// <summary>
    /// 
    /// </summary>
    public enum StructureServiceUriType
    {
        /// <summary>
        /// Structure Uri
        /// </summary>
        StructureUri = 0,

        /// <summary>
        /// Service Uri
        /// </summary>
        ServiceUri = 1
    }
}
