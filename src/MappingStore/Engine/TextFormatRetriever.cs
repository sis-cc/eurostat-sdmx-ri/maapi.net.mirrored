// -----------------------------------------------------------------------
// <copyright file="TextFormatRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-04-17
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Xml;

    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Helper;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    /// The text format retriever.
    /// </summary>
    public class TextFormatRetriever
    {
        /// <summary>
        /// The mapping store database
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        ///     Gets the SQL Query format/template for retrieving all text formats used by components in a DSD.
        /// </summary>
        protected virtual string TextFormatQueryFormat =>
            " select rs.SOURCE_CHILD_FULL_ID,  e.ENUM_NAME, e.ENUM_VALUE, t.FACET_VALUE " +
            "from REFERENCE_SOURCE rs " +
            "inner join TEXT_FORMAT t on rs.REF_SRC_ID  = t.REF_SRC_ID " +
            "inner join ENUMERATIONS e on t.FACET_TYPE_ENUM = e.ENUM_ID " +
            "where rs.SOURCE_ARTEFACT = {0}";

        /// <summary>
        /// Initializes a new instance of the <see cref="TextFormatRetriever"/> class.
        /// </summary>
        /// <param name="mappingStoreDb">The mapping store database.</param>
        public TextFormatRetriever(Database mappingStoreDb)
        {
            this._mappingStoreDb = mappingStoreDb;
        }
        
        /// <summary>
        /// Retrieves the Text Format for identifiable that parent has <paramref name="parentSysId"/> 
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <returns>The map between the component sys id and the <see cref="ITextFormatMutableObject"/> </returns>
        public Dictionary<string, ITextFormatMutableObject> Retrieve(long parentSysId)
        {
            return this.GetTextFormatInformation(parentSysId);
        }

        /// <summary>
        ///     Convert the <paramref name="facetValue" /> to decimal.
        /// </summary>
        /// <param name="facetValue">
        ///     The facet value.
        /// </param>
        /// <returns>
        ///     the <paramref name="facetValue" /> as decimal; otherwise null.
        /// </returns>
        private static decimal? FacetToDecimal(string facetValue)
        {
            if (string.IsNullOrWhiteSpace(facetValue))
            {
                return null;
            }

            decimal value;
            if (decimal.TryParse(facetValue, NumberStyles.Number, CultureInfo.InvariantCulture, out value))
            {
                return value;
            }

            return null;
        }

        /// <summary>
        ///     Convert the <paramref name="facetValue" /> to integer.
        /// </summary>
        /// <param name="facetValue">
        ///     The facet value.
        /// </param>
        /// <returns>
        ///     the <paramref name="facetValue" /> as integer; otherwise null.
        /// </returns>
        private static long? FacetToInteger(string facetValue)
        {
            if (string.IsNullOrWhiteSpace(facetValue))
            {
                return null;
            }

            long value;
            if (long.TryParse(facetValue, NumberStyles.Integer, CultureInfo.InvariantCulture, out value))
            {
                return value;
            }

            return null;
        }

        /// <summary>
        ///     Convert the <paramref name="facetValue" /> to <see cref="TertiaryBool" />
        /// </summary>
        /// <param name="facetValue">
        ///     The facet value.
        /// </param>
        /// <returns>
        ///     the <paramref name="facetValue" /> as <see cref="TertiaryBool" />
        /// </returns>
        private static TertiaryBool FacetToTristateBool(string facetValue)
        {
            if (string.IsNullOrWhiteSpace(facetValue))
            {
                return TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);
            }

            // PLEASE DO NOT CHANGE TO UPPER INVARIANT.
            return TertiaryBool.ParseBoolean(XmlConvert.ToBoolean(facetValue.ToLowerInvariant()));
        }

        /// <summary>
        ///     Populates the text format.
        /// </summary>
        /// <param name="enumName">
        ///     Name of the enumeration.
        /// </param>
        /// <param name="enumValue">
        ///     The enumeration value.
        /// </param>
        /// <param name="textFormat">
        ///     The text format.
        /// </param>
        /// <param name="facetValue">
        ///     The facet value.
        /// </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "It is OK. Big switch.")]
        private static void PopulateTextFormat(
            string enumName,
            string enumValue,
            ITextFormatMutableObject textFormat,
            string facetValue)
        {
            switch (enumName)
            {
                case "DataType":
                    {
                        TextEnumType textType;
                        if (Enum.TryParse(enumValue, true, out textType))
                        {
                            textFormat.TextType = TextType.GetFromEnum(textType);
                        }
                    }

                    break;
                case "FacetType":
                    {
                        switch (enumValue)
                        {
                            case "isSequence":
                                textFormat.Sequence = FacetToTristateBool(facetValue);
                                break;
                            case "minLength":
                                textFormat.MinLength = FacetToInteger(facetValue);
                                break;
                            case "maxLength":
                                textFormat.MaxLength = FacetToInteger(facetValue);
                                break;
                            case "minValue":
                                textFormat.MinValue = FacetToDecimal(facetValue);
                                break;
                            case "maxValue":
                                textFormat.MaxValue = FacetToDecimal(facetValue);
                                break;
                            case "startValue":
                                textFormat.StartValue = FacetToDecimal(facetValue);
                                break;
                            case "endValue":
                                textFormat.EndValue = FacetToDecimal(facetValue);
                                break;
                            case "decimals":
                                textFormat.Decimals = FacetToInteger(facetValue);
                                break;
                            case "interval":
                                textFormat.Interval = FacetToDecimal(facetValue);
                                break;
                            case "timeInterval":
                                textFormat.TimeInterval = facetValue;
                                break;
                            case "pattern":
                                textFormat.Pattern = facetValue;
                                break;
                            case "isMultiLingual":
                                textFormat.Multilingual = FacetToTristateBool(facetValue);
                                break;
                            case "startTime":
                                textFormat.StartTime = new SdmxDateCore(facetValue);
                                break;
                            case "endTime":
                                textFormat.EndTime = new SdmxDateCore(facetValue);
                                break;
                            case "jsonValue":
                                TextFormatValueUtils.PopulateTextFormatPropertiesFromJson(textFormat, facetValue);
                                break;
                                
                        }
                    }

                    break;
            }
        }

        /// <summary>
        /// Gets the text format information.
        /// </summary>
        /// <param name="parentSysId">The parent system unique identifier.</param>
        /// <returns>The map between component ID to Text Format Object</returns>
        private Dictionary<string, ITextFormatMutableObject> GetTextFormatInformation(long parentSysId)
        {
            var textFormats = new Dictionary<string, ITextFormatMutableObject>();
            using (var command = this._mappingStoreDb.GetSqlStringCommandFormat(TextFormatQueryFormat, this._mappingStoreDb.CreateInParameter("dsdId", DbType.Int64, parentSysId)))
            using (var reader = this._mappingStoreDb.ExecuteReader(command))
            {
                var compIdIdx = reader.GetOrdinal("SOURCE_CHILD_FULL_ID");
                var enumNameIdx = reader.GetOrdinal("ENUM_NAME");
                var enumValueIdx = reader.GetOrdinal("ENUM_VALUE");
                var facetValueIdx = reader.GetOrdinal("FACET_VALUE");

                while (reader.Read())
                {
                    var compId = reader.GetString(compIdIdx);

                    ITextFormatMutableObject textFormat;
                    if (!textFormats.TryGetValue(compId, out textFormat))
                    {
                        textFormat = new TextFormatMutableCore();
                        textFormats.Add(compId, textFormat);
                    }

                    var enumName = DataReaderHelper.GetString(reader, enumNameIdx);
                    var enumValue = DataReaderHelper.GetString(reader, enumValueIdx);
                    var facetValue = DataReaderHelper.GetString(reader, facetValueIdx);
                    PopulateTextFormat(enumName, enumValue, textFormat, facetValue);
                }
            }

            return textFormats;
        }
    }
}
