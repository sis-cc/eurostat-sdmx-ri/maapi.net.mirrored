// -----------------------------------------------------------------------
// <copyright file="IRetrievalEngine7.cs" company="EUROSTAT">
//   Date Created : 2022-06-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using Estat.Sri.StructureRetriever.Cache;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    /// <summary>
    /// Retrieves the structures requested.
    /// Compatible to MSDB 7.0
    /// </summary>
    /// <typeparam name="TMaintainable"></typeparam>
    public interface IRetrievalEngine<out TMaintainable>
        where TMaintainable : IMaintainableMutableObject
    {
        /// <summary>
        /// Retrieves the artefacts of a specific artefact type as instructed in the <paramref name="structureQuery"/>.
        /// </summary>
        /// <param name="structureQuery">The query for the structures.</param>
        /// <returns></returns>
        IEnumerable<TMaintainable> Retrieve(ICommonStructureQuery structureQuery);

        /// <summary>
        /// Retrieve artefacts that are parents of another artefact matching the <paramref name="structureQuery"/>.
        /// </summary>
        /// <param name="structureQuery">The query for retrieving the artefacts whose parents are requested.</param>
        /// <returns>The parents of the artefact specified in the <paramref name="structureQuery"/>.</returns>
        IEnumerable<TMaintainable> RetrieveAsParents(ICommonStructureQuery structureQuery);

        /// <summary>
        /// Retrieve artefacts that are children of another artefact matching the <paramref name="structureQuery"/>.
        /// </summary>
        /// <param name="structureQuery">The query for retrieving the artefacts whose children are requested.</param>
        /// <returns>The parents of the artefact specified in the <paramref name="structureQuery"/>.</returns>
        IEnumerable<TMaintainable> RetrieveAsChildren(ICommonStructureQuery structureQuery, HashSet<StructureQueryCacheKey> cache = null);

        /// <summary>
        /// Retrieve the parents that reference this artefact.
        /// </summary>
        /// <param name="structureQuery">The query for the artefact.</param>
        /// <returns>The <see cref="IStructureReference"/>s of this artefact.</returns>
        ISet<IStructureReference> RetrieveResolvedParents(ICommonStructureQuery structureQuery);


    }
}
