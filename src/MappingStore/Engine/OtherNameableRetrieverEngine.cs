// -----------------------------------------------------------------------
// <copyright file="OtherNameableRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2022-07-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
// initial implementation ISTAT
//
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// The engine for retrieving other nameables
    /// </summary>
    /// <typeparam name="TIdentifiable">The type of nameable artefact to retrieve</typeparam>
    public class OtherNameableRetrieverEngine<TIdentifiable> : BaseRetrievalEngine
        where TIdentifiable : INameableMutableObject, new()
    {
        // FIXME why is it public ?
        public static string OTHER_NAMEABLE_QUERY = "select O.OTH_ID as SYSID, O.ID , O.VALID_FROM , O.VALID_TO , O.VERSION1 , O.VERSION2, O.VERSION3, O.PARENT_ARTEFACT, " +
            "O.URN_CLASS , O.PARENT_PATH, " +
            "lso.IS_NAME , lso.LANGUAGE , lso.TEXT " +
            "from OTHER_NAMEABLE O " +
            "LEFT OUTER JOIN LOCALISED_STRING_OTHER lso ON O.OTH_ID  = lso.OTH_ID " +
            "WHERE O.PARENT_ARTEFACT  = {0} and O.URN_CLASS  = {1}";
        private readonly IdentifiableAnnotationRetrieverEngine _annotationRetriever;
//        private readonly TIdentifiable _identifiableObject;
        private readonly string _urnClass;
        private readonly Database _mappingStore;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="mappingStore"></param>
        /// <param name="structureType"></param>
        public OtherNameableRetrieverEngine(Database mappingStore, SdmxStructureEnumType structureType)
        {
            // This doesn't seem right but leaving it here for the PR in case I missed something
            // this._identifiableObject = new TIdentifiable();
            this._mappingStore = mappingStore;
            ItemTableInfo itemTableInfo = ItemTableInfo.Factory.NewOtherNameable(structureType);
            itemTableInfo.ParentItem = "PARENT_PATH";
            this._annotationRetriever = new IdentifiableAnnotationRetrieverEngine(mappingStore, itemTableInfo);
            _urnClass = SdmxStructureType.GetFromEnum(structureType).UrnClass;
        }

        /// <summary>
        /// Retrieve the other nameables belonging to a given parent.
        /// </summary>
        /// <param name="parentSysId">the PK of the parent artefact to get the other nameables for.</param>
        /// <returns></returns>
        public IDictionary<long, TIdentifiable> Retrieve(long parentSysId)
        {
            IDictionary<long, TIdentifiable> sysIdToMutableBean = new Dictionary<long, TIdentifiable>();
            var inParameter = _mappingStore.CreateInParameter(ParameterNameConstants.IdParameter, DbType.Int64, parentSysId);
            var urnClassParameter = _mappingStore.CreateInParameter(ParameterNameConstants.UrlClassParameter, DbType.AnsiString, _urnClass);
            using (var command = _mappingStore.GetSqlStringCommandFormat(GetSqlQuery(), inParameter, urnClassParameter))
            {
                using (var dr = this._mappingStore.ExecuteReader(command))
                {
                    int sysIdPos = dr.GetOrdinal("SYSID");
                    int idPos = dr.GetOrdinal("ID");
                    int isNamePos = dr.GetOrdinal("IS_NAME");
                    int langPos = dr.GetOrdinal("LANGUAGE");
                    int textPos = dr.GetOrdinal("TEXT");
                    while (dr.Read())
                    {
                        long sysId = dr.GetInt64(sysIdPos);
                        if (!sysIdToMutableBean.TryGetValue(sysId, out TIdentifiable reportStructure))
                        {
                            reportStructure = new TIdentifiable();
                            reportStructure.Id = dr.GetString(idPos);
                            sysIdToMutableBean.Put(sysId, reportStructure);
                            HandleExtraFields(reportStructure, dr);
                        }
                        ReadLocalisedString(reportStructure, isNamePos, textPos, langPos, dr);
                    }
                }

                this._annotationRetriever.RetrieveAnnotations(parentSysId, sysIdToMutableBean);
                HandleHierarchy(sysIdToMutableBean);
                return sysIdToMutableBean;
            }
        }

        protected virtual string GetSqlQuery() {
            return OTHER_NAMEABLE_QUERY;
        }

        protected virtual void HandleExtraFields(TIdentifiable identifiable, IDataReader dataReader) 
        {
        }

        protected virtual void HandleHierarchy(IDictionary<long, TIdentifiable> sysIdToMutableBean)
        {
        }
    }
}
