// -----------------------------------------------------------------------
// <copyright file="TargetObjectRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-04-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.Sdmx.MappingStore.Retrieve;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    internal class TargetObjectRetriever
    {
        /// <summary>
        /// The command builder
        /// </summary>
        private readonly MsdCommandBuilder _commandBuilder;

        /// <summary>
        /// The mapping store database
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        /// The annotation retriever
        /// </summary>
        private readonly IdentifiableAnnotationRetrieverEngine _annotationRetriever;

        /// <summary>
        /// The text retriever
        /// </summary>
        private readonly TextFormatRetriever _textRetriever;

        public const string TARGET_OBJECT_FORMAT_SDMX2 = "select TOB.TARGET_OBJ_ID, TOB.ID, TOB.TYPE, TOB.MDT_ID\n" +
            "from TARGET_OBJECT TOB \n" +
            "inner join OTHER_NAMEABLE  MT ON TOB.MDT_ID = MT.OTH_ID " +
            "WHERE MT.PARENT_ARTEFACT = {0} ";

        /// <summary>
        /// Initializes a new instance of the <see cref="TargetObjectRetriever"/> class.
        /// </summary>
        /// <param name="mappingStoreDb">The mapping store database.</param>
        public TargetObjectRetriever(Database mappingStoreDb)
        {
            this._mappingStoreDb = mappingStoreDb;
            this._annotationRetriever = new IdentifiableAnnotationRetrieverEngine(mappingStoreDb, MsdConstant.TargetObjectTableInfo);
            this._textRetriever = new TextFormatRetriever(mappingStoreDb);
        }

        /// <summary>
        /// Populates the specified parent system identifier.
        /// </summary>
        /// <param name="artefactWithPk">the parent artefact info.</param>
        /// <param name="metadataTargetMap">The metadata target map.</param>
        /// <exception cref="ArgumentException">metadataTargetMap - cannot find entry</exception>
        public void Populate(
            MaintainableWithPrimaryKey<IMetadataStructureDefinitionMutableObject> artefactWithPk,
            IDictionary<long, IMetadataTargetMutableObject> metadataTargetMap)
        {
            long parentSysId = artefactWithPk.PrimaryKey;
            var constraintContentTargets = new Dictionary<string, IConstraintContentTargetMutableObject>();
            var dataSetTargets = new Dictionary<string, IDataSetTargetMutableObject>();
            var keyValueDescriptorTargets = new Dictionary<string, IKeyDescriptorValuesTargetMutableObject>();
            var reportPeriodTargets = new Dictionary<string, IReportPeriodTargetMutableObject>();
            var identifiableTargets = new Dictionary<string, IIdentifiableTargetMutableObject>();
            var annotables = new Dictionary<long, IAnnotableMutableObject>();

            var inParameter = this._mappingStoreDb.CreateInParameter(
                ParameterNameConstants.IdParameter,
                DbType.Int64,
                parentSysId);
            using (var command = this._mappingStoreDb.GetSqlStringCommandFormat(TARGET_OBJECT_FORMAT_SDMX2, inParameter))
            using (var reader = this._mappingStoreDb.ExecuteReader(command))
            {
                while (reader.Read())
                {
                    var mdtID = reader.GetSafeInt64("MDT_ID");
                    IMetadataTargetMutableObject metadataTarget;
                    if (!metadataTargetMap.TryGetValue(mdtID, out metadataTarget))
                    {
                        throw new ArgumentException(ErrorMessages.CannotFindEntry + mdtID, "metadataTargetMap");
                    }

                    var targetObjID = reader.GetSafeInt64("TARGET_OBJ_ID");
                    var id = reader.GetSafeString("ID");
                    var type = reader.GetSafeString("TYPE");

                    // The ID of the target object must be unique per Metadata Target in SDMX 2.1
                    // https://github.com/sdmx-twg/sdmx-ml-v2_1/blob/master/schemas/SDMXStructureMetadataStructure.xsd#L71
                    string fullPath = string.Join(".", metadataTarget.Id, id);

                    if (type == ConstraintContentTarget.FixedId || 
                        string.Equals(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConstraintContentTarget).UrnClass, type))
                    {
                        metadataTarget.ConstraintContentTarget = new ConstraintContentTargetMutableCore
                        {
                            Id = id
                        };
                        constraintContentTargets.Add(fullPath, metadataTarget.ConstraintContentTarget);
                        annotables.Add(targetObjID, metadataTarget.ConstraintContentTarget);
                    }
                    else if (type == DataSetTarget.FixedId || 
                        string.Equals(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DatasetTarget).UrnClass, type))
                    {
                        metadataTarget.DataSetTarget = new DataSetTargetMutableCore
                        {
                            Id = id
                        };
                        dataSetTargets.Add(fullPath, metadataTarget.DataSetTarget);
                        annotables.Add(targetObjID, metadataTarget.DataSetTarget);
                    }
                    else if (type == ReportPeriodTarget.FixedId || 
                        string.Equals(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportPeriodTarget).UrnClass, type))
                    {
                        metadataTarget.ReportPeriodTarget = new ReportPeriodTargetMutableCore
                        {
                            Id = id
                        };
                        reportPeriodTargets.Add(fullPath, metadataTarget.ReportPeriodTarget);
                        annotables.Add(targetObjID, metadataTarget.ReportPeriodTarget);
                    }
                    else if (type == KeyDescriptorValuesTarget.FixedId || 
                        string.Equals(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DimensionDescriptorValuesTarget).UrnClass, type))
                    {
                        metadataTarget.KeyDescriptorValuesTarget = new KeyDescriptorValuesTargetMutableCore
                        {
                            Id = id
                        };
                        keyValueDescriptorTargets.Add(fullPath, metadataTarget.KeyDescriptorValuesTarget);
                        annotables.Add(targetObjID, metadataTarget.KeyDescriptorValuesTarget);
                    }
                    else
                    {
                        var urnClass = SdmxStructureType.ParseClass(type);
                        var identifiableTarget = new IdentifiableTargetMutableCore
                        {
                            Id = id
                        };
                        identifiableTarget.ReferencedStructureType = urnClass;

                        // get the enumeration of the target object
                        // it was already retrieved
                        ReferenceToOtherArtefact codedRepresentations = artefactWithPk.References.FirstOrDefault(r =>
                            r.SubStructureFullPath.Equals(fullPath) &&
                            r.ReferenceType.Equals(RefTypes.Enumeration));
                        if(codedRepresentations != null)
                        {
                            identifiableTarget.Representation = new RepresentationMutableCore()
                            {
                                Representation = codedRepresentations.Target
                            };
                        }

                        metadataTarget.IdentifiableTarget.Add(identifiableTarget);
                        identifiableTargets.Add(fullPath, identifiableTarget);
                        annotables.Add(targetObjID, identifiableTarget);
                    }
                }
            }

            var textFormats = this._textRetriever.Retrieve(parentSysId);
            foreach (var metadataTarget in metadataTargetMap)
            {
                this._annotationRetriever.RetrieveAnnotations(metadataTarget.Key, annotables);

                foreach (var textFormat in textFormats)
                {
                    var wasSet = SetTextType(textFormat, constraintContentTargets, (o, type) => o.TextType = type.TextType)
                        || SetTextType(textFormat, dataSetTargets, (o, type) => o.TextType = type.TextType)
                        || SetTextType(textFormat, keyValueDescriptorTargets, (o, type) => o.TextType = type.TextType)
                        || SetTextType(textFormat, reportPeriodTargets, SetTextFormat)
                        || SetTextType(textFormat, identifiableTargets, SetTextFormat);
                    // metaattribute text format, it has the same parent as target objects
                 //     Debug.Assert(wasSet, "Orphan Text Format found with key:" + textFormat.Key);
                }
            }
        }

        /// <summary>
        /// Sets the text format.
        /// </summary>
        /// <param name="identifiableTarget">The identifiable target.</param>
        /// <param name="textFormat">The text format.</param>
        private static void SetTextFormat(IIdentifiableTargetMutableObject identifiableTarget, ITextFormatMutableObject textFormat)
        {
            if (identifiableTarget.Representation == null)
            {
                identifiableTarget.Representation = new RepresentationMutableCore();
            }

            identifiableTarget.Representation.TextFormat = textFormat;
        }

        /// <summary>
        /// Sets the text format.
        /// </summary>
        /// <param name="reportPeriodTarget">The report period target.</param>
        /// <param name="textFormat">The text format.</param>
        private static void SetTextFormat(IReportPeriodTargetMutableObject reportPeriodTarget, ITextFormatMutableObject textFormat)
        {
            if (textFormat != null)
            {
                reportPeriodTarget.TextType = textFormat.TextType;

                if (textFormat.StartTime != null)
                {
                    reportPeriodTarget.StartTime = textFormat.StartTime.Date;
                }

                if (textFormat.EndTime != null)
                {
                    reportPeriodTarget.EndTime = textFormat.EndTime.Date;
                }
            }
        }

        /// <summary>
        /// Sets the type of the text.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="textFormat">The text format.</param>
        /// <param name="targets">The targets.</param>
        /// <param name="action">The action.</param>
        /// <returns>True if an entry from <paramref name="targets"/> matched the <paramref name="textFormat"/> key</returns>
        private static bool SetTextType<TTarget>(KeyValuePair<string, ITextFormatMutableObject> textFormat, IDictionary<string, TTarget> targets, Action<TTarget, ITextFormatMutableObject> action)
        {
            TTarget constraintContentTarget;
            if (targets.TryGetValue(textFormat.Key, out constraintContentTarget))
            {
                action(constraintContentTarget, textFormat.Value);
                return true;
            }

            return false;
        }
    }
}