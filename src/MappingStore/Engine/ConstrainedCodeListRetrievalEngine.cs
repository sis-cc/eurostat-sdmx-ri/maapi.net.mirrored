// -----------------------------------------------------------------------
// <copyright file="ConstrainedCodeListRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2018-05-30
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     The constrained code list retrieval engine.
    /// </summary>
    public class ConstrainedCodeListRetrievalEngine : CodeListRetrievalEngine, IConstrainedCodeListRetrievalEngine
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ConstrainedCodeListRetrievalEngine));
        
        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstrainedCodeListRetrievalEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="mappingStoreDb" /> is null
        /// </exception>
        public ConstrainedCodeListRetrievalEngine(Database mappingStoreDb) : base(mappingStoreDb)
        {
        }

        /// <summary>
        ///     Retrieve the <see cref="ICodelistMutableObject" />.
        /// </summary>
        /// <param name="codelistRef">
        ///     The codelist to be retrieved
        /// </param>
        /// <param name="requestedStructureRef">
        ///     The requested SDMX structure.(Can be one of the following: Provision Agreement, Dataflow, Data Structure Definition)
        /// </param>
        /// <returns>
        ///     A subset of the specified codelist <see cref="ICodelistMutableObject" />. This method returns always one codelist.
        /// </returns>
        [Obsolete("Since MSDB 7.0; This method should be replaced.")]
        public ISet<ICodelistMutableObject> Retrieve(IStructureReference codelistRef, IStructureReference requestedStructureRef)
        {
            //var sqlQuery = new ArtefactSqlQuery(this.SqlQueryInfoForAll, codelistRef);

            //return this.RetrieveArtefacts(
            //    sqlQuery,
            //    ComplexStructureQueryDetailEnumType.Full,
            //    retrieveDetails: (o, l,c) => this.FillCodes(o, l,c, requestedStructureRef));
            throw new InvalidOperationException("Retrieve method is obsolete.");
        }

        ///// <summary>
        /////     Get the Codes
        ///// </summary>
        ///// <param name="itemScheme">
        /////     The parent <see cref="ICodelistMutableObject" />
        ///// </param>
        ///// <param name="parentSysId">
        /////     The parent ItemScheme primary key in Mapping Store
        ///// </param>
        ///// <param name="childReferences"></param>
        ///// <param name="requestedStructureRef">
        /////     Reference of the requested structure
        ///// </param>
        ///// <returns>
        /////     The <see cref="ICodelistMutableObject" />.
        ///// </returns>
        //private ICodelistMutableObject FillCodes(ICodelistMutableObject itemScheme, long parentSysId,IEnumerable<string> childReferences, IStructureReference requestedStructureRef)
        //{
        //    long codesCount = this.CountItems(new ItemSchemeCountSqlQuery(CodeListConstant.CountCodesInfo, parentSysId));
            
        //    using (var connection = this.MappingStoreDb.CreateConnection())
        //    {
        //        connection.Open();
        //        using (var transaction = connection.BeginTransaction())
        //        {
        //            try
        //            {
        //                Database transactionalDatabase = new Database(this.MappingStoreDb, transaction);
        //                var processConstraintsEngine = new ProcessConstraintsEngine(transactionalDatabase);

        //                bool isCodelistPartial = processConstraintsEngine.StartProcessing(requestedStructureRef, parentSysId);

        //                if (!isCodelistPartial)
        //                {
        //                    this.FillItems(itemScheme, parentSysId,childReferences);
        //                }
        //                else
        //                {
        //                    this.FillConstrainedItems(transactionalDatabase, itemScheme, parentSysId);
        //                }
        //                // drop the temp table
        //                processConstraintsEngine.EndProcessing();

        //                transaction.Commit();
        //            }
        //            catch (Exception e)
        //            {
        //                transaction.Rollback();
        //                throw e;
        //            }
        //        }
        //    }
        //    if (itemScheme.Items != null)
        //    {
        //        itemScheme.IsPartial = itemScheme.Items.Count < codesCount;
        //    }
        //    return itemScheme;
        //}
        
        //private void FillConstrainedItems(Database transactionalDatabase, ICodelistMutableObject itemScheme, long parentSysId)
        //{
        //    var allItems = new Dictionary<long, ICodeMutableObject>();
        //    var orderedItems = new List<KeyValuePair<long, ICodeMutableObject>>();
        //    var childItems = new Dictionary<long, long>();

        //    var constrainedCodelistSqlQuery = new ConstrainedCodesSqlQuery(
        //           ConstrainedCodeListConstant.GetConstrainedCodelistSqlQueryInfo(transactionalDatabase.ProviderName));

        //    using (DbCommand command = transactionalDatabase.
        //        GetSqlStringCommand(constrainedCodelistSqlQuery.QueryInfo.QueryFormat, null))
        //    {
             
        //        using (IDataReader dataReader = transactionalDatabase.ExecuteReader(command))
        //        {
        //            int sysIdIdx = dataReader.GetOrdinal("SYSID");
        //            int idIdx = dataReader.GetOrdinal("ID");
        //            int parentIdx = dataReader.GetOrdinal("PARENT");
        //            int txtIdx = dataReader.GetOrdinal("TEXT");
        //            int langIdx = dataReader.GetOrdinal("LANGUAGE");
        //            int typeIdx = dataReader.GetOrdinal("TYPE");
        //            while (dataReader.Read())
        //            {
        //                long sysId = dataReader.GetInt64(sysIdIdx); // not a null.
        //                ICodeMutableObject code;
        //                if (!allItems.TryGetValue(sysId, out code))
        //                {
        //                    code = this.CreateItem();
        //                    code.Id = DataReaderHelper.GetString(dataReader, idIdx); // "ID"
        //                    this.HandleItemExtraFields(code, dataReader);
        //                    orderedItems.Add(new KeyValuePair<long, ICodeMutableObject>(sysId, code));

        //                    allItems.Add(sysId, code);
        //                    long parentItemId = DataReaderHelper.GetInt64(dataReader, parentIdx);
        //                    if (parentItemId > long.MinValue)
        //                    {
        //                        childItems.Add(sysId, parentItemId);
        //                    }
        //                }
        //                ReadLocalisedString(code, typeIdx, txtIdx, langIdx, dataReader);
        //            }
        //            transactionalDatabase.CancelSafe(command);
        //        }
        //    }
        //    this.FillParentItems(itemScheme, childItems, allItems, orderedItems);
        //    this.IdentifiableAnnotationRetrieverEngine.RetrieveAnnotations(parentSysId, allItems);
        //}
    }
}