// -----------------------------------------------------------------------
// <copyright file="DataConsumerSchemeRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2017-02-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
// initial implementation ISTAT
//
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System.Collections.Generic;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The Data Consumer scheme retrieval engine.
    /// </summary>
    internal class DataConsumerSchemeRetrievalEngine : ItemSchemeRetrieverEngine<IDataConsumerSchemeMutableObject, IDataConsumerMutableObject>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataConsumerSchemeRetrievalEngine"/> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        /// The mapping store DB.
        /// </param>
        public DataConsumerSchemeRetrievalEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
        }

        /// <summary>
        /// The Data Consumer Scheme
        /// </summary>
        protected override SdmxStructureType ItemSchemeStructureType => SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataConsumerScheme);

        /// <inheritdoc/>
        protected override void AddItem(IDataConsumerMutableObject item, string parentItemPath, IDataConsumerSchemeMutableObject itemScheme, Dictionary<string, IDataConsumerMutableObject> itemPathMap)
        {
            itemScheme.AddItem(item);
        }

        /// <summary>
        ///     Create a new instance of <see cref="IDataConsumerSchemeMutableObject" />.
        /// </summary>
        /// <returns>
        ///     The <see cref="IDataConsumerSchemeMutableObject" />.
        /// </returns>
        protected override IDataConsumerSchemeMutableObject CreateArtefact()
        {
            return new DataConsumerSchemeMutableCore();
        }

        /// <summary>
        /// The create item.
        /// </summary>
        /// <returns>
        /// The <see cref="IDataConsumerMutableObject"/>.
        /// </returns>
        protected override IDataConsumerMutableObject CreateItem()
        {
            return new DataConsumerMutableCore();
        }
    }
}