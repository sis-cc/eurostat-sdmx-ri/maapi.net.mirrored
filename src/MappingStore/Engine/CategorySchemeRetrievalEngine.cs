// -----------------------------------------------------------------------
// <copyright file="CategorySchemeRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The concept scheme retrieval engine.
    /// </summary>
    internal class CategorySchemeRetrievalEngine :
        ItemSchemeRetrieverEngine<ICategorySchemeMutableObject, ICategoryMutableObject>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorySchemeRetrievalEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="mappingStoreDb" /> is null
        /// </exception>
        public CategorySchemeRetrievalEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
        }

        /// <summary>
        /// The Category Scheme
        /// </summary>
        protected override SdmxStructureType ItemSchemeStructureType => SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme);

        /// <inheritdoc/>
        protected override void AddItem(ICategoryMutableObject item, string parentItemPath, ICategorySchemeMutableObject itemScheme, Dictionary<string, ICategoryMutableObject> itemPathMap)
        {
            if (!ObjectUtil.ValidString(parentItemPath))
            {
                itemScheme.AddItem(item);
            }
            // if we knew that parents are retrieved before children we could
        }

        /// <summary>
        ///     Create a new instance of <see cref="ICategorySchemeMutableObject" />.
        /// </summary>
        /// <returns>
        ///     The <see cref="ICategorySchemeMutableObject" />.
        /// </returns>
        protected override ICategorySchemeMutableObject CreateArtefact()
        {
            return new CategorySchemeMutableCore();
        }

        /// <summary>
        ///     Create an item.
        /// </summary>
        /// <returns>
        ///     The <see cref="ICategoryMutableObject" />.
        /// </returns>
        protected override ICategoryMutableObject CreateItem()
        {
            return new CategoryMutableCore();
        }


        protected override void AddNestedItems(Dictionary<string, ICategoryMutableObject> itemPathMap)
        {
            foreach (var categegoryPath in itemPathMap)
            {
                // check if there a parent
                int lastDotPos = categegoryPath.Key.LastIndexOf('.');
                if (lastDotPos > 0)
                {
                    // has at least one dot therefor it has a parent (and path doesn't start with a dot)
                    string parentPath = categegoryPath.Key.Substring(0, lastDotPos);
                    ICategoryMutableObject parent = itemPathMap[parentPath];
                    if (parent != null)
                    {
                        parent.AddItem(categegoryPath.Value);
                    }
                }
            }
        }

        
        protected override string GetUniqueId(string fullPath, string itemId)
        {
            return fullPath;
        }
    }
}