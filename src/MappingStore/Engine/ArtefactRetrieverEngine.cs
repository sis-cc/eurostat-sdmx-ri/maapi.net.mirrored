// -----------------------------------------------------------------------
// <copyright file="ArtefactRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;
    using DryIoc;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Helper;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model;
    using Estat.Sri.StructureRetriever.Cache;
    using System.Diagnostics;

    /// <summary>
    /// The base retrieval engine for artefacts.
    /// Compatible to MSDB 7.0
    /// </summary>
    /// <typeparam name="T">The type of the maintainable to retrieve.</typeparam>
    public abstract class ArtefactRetrieverEngine<T> : BaseRetrievalEngine, IRetrievalEngine<T>
        where T : IMaintainableMutableObject
    {
        /// <summary>
        ///     The _log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ArtefactRetrieverEngine<T>));

        /// <summary>
        ///     This field holds the Mapping Store Database object.
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        ///     The _annotation retriever engine
        /// </summary>
        private readonly MaintainableAnnotationRetrieverEngine _maintainableAnnotationRetrieverEngine;

        /// <summary>
        ///     The uri builder
        /// </summary>
        protected readonly IStructureServiceUriHelper structureServiceUriHelperUriBuilder;

        /// <summary>
        /// Gets the Mapping Store Database used for this retrieval
        /// </summary>
        protected Database MappingStoreDb => _mappingStoreDb;

        /// <summary>
        /// Creates a new instance of the class.
        /// </summary>
        /// <param name="database">The mapping store database object.</param>
        /// <exception cref="ArgumentNullException"><paramref name="database" /> is null.</exception>
        public ArtefactRetrieverEngine(Database database)
        {
            if (database == null)
            {
                throw new ArgumentNullException(nameof(database));
            }
            this._mappingStoreDb = database;
            this._maintainableAnnotationRetrieverEngine = new MaintainableAnnotationRetrieverEngine(database);
            this.structureServiceUriHelperUriBuilder = MappingStoreIoc.Container.Resolve<IStructureServiceUriHelper>();
        }

        #region Interface implementation

        /// <inheritdoc/>
        public virtual IEnumerable<T> Retrieve(ICommonStructureQuery structureQuery)
        {
            var commandBuilder = GetCommandBuilder(structureQuery);
            return RetrieveArtefactsOrParentsOrChildren(
                commandBuilder.BuildRetrievalQuery, commandBuilder.BuildReferencesQuery, structureQuery);
        }

        /// <inheritdoc/>
        public virtual IEnumerable<T> RetrieveAsParents(ICommonStructureQuery structureQuery)
        {
            var commandBuilder = GetCommandBuilder(structureQuery);
            return RetrieveArtefactsOrParentsOrChildren(
                (query) => commandBuilder.BuildParentsRetrievalQuery(query, GetStructureType()),
                commandBuilder.BuildParentsReferencesQuery, structureQuery);
        }

        /// <inheritdoc/>
        public virtual IEnumerable<T> RetrieveAsChildren(ICommonStructureQuery structureQuery, HashSet<StructureQueryCacheKey> cache = null)
        {
            var commandBuilder = GetCommandBuilder(structureQuery);
            return RetrieveArtefactsOrParentsOrChildren(
                (query) => commandBuilder.BuildChildrenRetrievalQuery(query, GetStructureType()),
                commandBuilder.BuildChildrenReferencesQuery, structureQuery,cache);
        }

        /// <inheritdoc/>
        public virtual ISet<IStructureReference> RetrieveResolvedParents(ICommonStructureQuery structureQuery)
        {
            ISet<IStructureReference> results = new HashSet<IStructureReference>();

            var commandBuilder = GetCommandBuilder(structureQuery);
            using (DbCommand command = commandBuilder.BuildResolveParentReferences(structureQuery))
            {
                _log.Info(string.Format("Executing Query: '{0}' with '{1}'", command.ToString(), structureQuery.ToString()));
                using (IDataReader reader = _mappingStoreDb.ExecuteReader(command))
                {
                    int idPos = reader.GetOrdinal("ID");
                    int versionPos1 = reader.GetOrdinal("VERSION1");
                    int versionPos2 = reader.GetOrdinal("VERSION2");
                    int versionPos3 = reader.GetOrdinal("VERSION3");
                    int versionExtPos = reader.GetOrdinal("EXTENSION");
                    int agencyPos = reader.GetOrdinal("AGENCY");
                    int artefactTypePos = reader.GetOrdinal("ARTEFACT_TYPE");

                    while (reader.Read())
                    {
                        string maintainableId = DataReaderHelper.GetString(reader, idPos);
                        string agency = DataReaderHelper.GetString(reader, agencyPos);
                        // version fields do not allow null
                        // to avoid errors like {"queue":{"status":"exception","message":"One or more errors occurred. (Version invalid : -9223372036854775808.-9223372036854775808.-9223372036854775808-draft)"}}
                        long version1 = reader.GetInt64(versionPos1);
                        long version2 = reader.GetInt64(versionPos2);
                        long version3 = reader.GetInt64(versionPos3);
                        string versionExtension = DataReaderHelper.GetString(reader, versionExtPos);
                        string artefactType = DataReaderHelper.GetString(reader, artefactTypePos);
                        SdmxStructureType structureType = SdmxStructureType.ParseClass(artefactType);

                        // target sub structure e.g. an item is not stored in StructureReferenceBean because
                        // we would need to store its Structure Type with no known added value
                        IStructureReference referenceBean = new StructureReferenceImpl(agency,
                                maintainableId,
                                BuildVersion(version1, version2, version3, versionExtension),
                                structureType);
                        results.Add(referenceBean);
                    }
                }
            }

            return results;
        }

        #endregion

        #region stuff to override

        /// <summary>
        /// This method is meant to create a new instance of a <see cref="IArtefactSqlQueryBuilder"/> implementation,
        /// to avoid messing with different queries. 
        /// </summary>
        /// <returns>A new instance of an <see cref="IArtefactSqlQueryBuilder"/> implementation.</returns>
        protected virtual IArtefactSqlQueryBuilder GetCommandBuilder(ICommonStructureQuery structureQuery)
        {
            return new ArtefactCommandBuilder(_mappingStoreDb);
        }

        /// <summary>
        /// This must be implemented per artefact.
        /// It should be used to get the sub-structures inside each artefact
        /// </summary>
        /// <param name="artefactPkPair">The artefact (with PK and references to other artefacts) for which to retrieve details.</param>
        /// <param name="structureQuery">Who's quering for the data. It can contain more info according the type of request.</param>
        /// <returns>The maintainable artefact.</returns>
        protected abstract T RetrieveDetails(MaintainableWithPrimaryKey<T> artefactPkPair, ICommonStructureQuery structureQuery);

        /// <summary>
        /// This must be implemented per artefact.
        /// </summary>
        /// <returns>A new intance of the artefact class.</returns>
        protected abstract T CreateArtefact();

        /// <summary>
        ///     When overridden handles the artefact extra fields. Does nothing by default.
        /// </summary>
        /// <param name="artefact">
        ///     The artefact.
        /// </param>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        protected virtual void HandleArtefactExtraFields(T artefact, IDataReader reader)
        {
        }

        /// <summary>
        /// When overridden handles the stub artefact extra fields. Does nothing by default.
        /// </summary>
        /// <param name="artefact">The stub artefact</param>
        /// <param name="reader">The data reader</param>
        protected virtual void HandleStubArtefactExtraFields(T artefact, IDataReader reader)
        {
        }

        #endregion

        #region private methods

        /// <summary>
        /// Manages all calls to retrieve artefacts
        /// </summary>
        /// <param name="artefactCommandBuilder">The method that will provide the <see cref="DbCommand"/> for the artefacts.</param>
        /// <param name="referencesCommandBuilder">The method that will provide the <see cref="DbCommand"/> for the references.</param>
        /// <param name="structureQuery">The structure query to get the artefacts for.</param>
        /// <returns>A set of the matching artefacts.</returns>
        private ISet<T> RetrieveArtefactsOrParentsOrChildren(
            Func<ICommonStructureQuery, DbCommand> artefactCommandBuilder,
            Func<ICommonStructureQuery, DbCommand> referencesCommandBuilder,
            ICommonStructureQuery structureQuery, HashSet<StructureQueryCacheKey> cache = null)
        {
            var artefactPkPairs = new List<MaintainableWithPrimaryKey<T>>();
            RetrieveArtefactsOfType(artefactCommandBuilder, structureQuery, artefactPkPairs);
            if (!artefactPkPairs.Any())
            {
                _log.Info(string.Format("No artefacts retrieved for : '{0}'", structureQuery.ToString()));
                return new HashSet<T>();
            }

            //This is done in order to remove child structures that were already retrieved from the database
            if (cache != null)
            {
                artefactPkPairs.RemoveAll(x => cache.Contains(CacheUtils.GetKeyFromMaintainable(x.Maintainable)));
            }
            if (structureQuery.RequestedDetail != ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.CompleteStub) &&
                structureQuery.RequestedDetail != ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Stub))
            {
                RetrieveReferencesToOtherArtefacts(referencesCommandBuilder, structureQuery, artefactPkPairs);
            }

            // get details, annotations, references to other artefacts, and internal items

            return this.HandleDetailLevel(structureQuery, artefactPkPairs, null);
        }


        /// <summary>
        /// Will retrieve artefacts.
        /// </summary>
        /// <param name="commandBuilder">The method that will provide the <see cref="DbCommand"/>.</param>
        /// <param name="structureQuery">The structure query to get the artefacts for</param>
        /// <param name="artefactPkPairs">The data list to add to.</param>
        private void RetrieveArtefactsOfType(
            Func<ICommonStructureQuery, DbCommand> commandBuilder,
            ICommonStructureQuery structureQuery,
            IList<MaintainableWithPrimaryKey<T>> artefactPkPairs)
        {
            using (DbCommand command = commandBuilder.Invoke(structureQuery))
            {
                _log.InfoFormat(
                    CultureInfo.InvariantCulture,
                    "Executing Query: '{0}' with '{1}'",
                    command.CommandText,
                    structureQuery.MaintainableIds);
                using (var dataReader = _mappingStoreDb.ExecuteReader(command))
                {
                    var sysIdIdx = dataReader.GetOrdinal("SYSID");
                    var idIdx = dataReader.GetOrdinal("ID");
                    var version1Idx = dataReader.GetOrdinal("VERSION1");
                    var version2Idx = dataReader.GetOrdinal("VERSION2");
                    var version3Idx = dataReader.GetOrdinal("VERSION3");
                    var versionExtIdx = dataReader.GetOrdinal("EXTENSION");
                    var agencyIdx = dataReader.GetOrdinal("AGENCY");
                    var validFromIdx = dataReader.GetOrdinal("VALID_FROM");
                    var validToIdx = dataReader.GetOrdinal("VALID_TO");
                    var uriIdx = dataReader.GetOrdinal("URI");
                    var txtIdx = dataReader.GetOrdinal("TEXT");
                    var langIdx = dataReader.GetOrdinal("LANGUAGE");
                    var isNameIdx = dataReader.GetOrdinal("IS_NAME");
                    var structureUrlIdx = dataReader.GetOrdinal("STRUCTURE_URL");
                    var serviceUrlIdx = dataReader.GetOrdinal("SERVICE_URL");
                    var isStubIdx = dataReader.GetOrdinal("IS_STUB");
                    var artefactMap = new Dictionary<long, T>();
                    while (dataReader.Read())
                    {
                        var sysId = DataReaderHelper.GetInt64(dataReader, sysIdIdx);
                        T artefact;
                        if (!artefactMap.TryGetValue(sysId, out artefact))
                        {
                            // we only need this information when creating the artefact not for every row
                            artefact = this.CreateArtefact();
                            artefact.Id = DataReaderHelper.GetString(dataReader, idIdx);
                            // version are not null. negative number means no version at that version part
                            long version1 = dataReader.GetInt64(version1Idx);
                            long version2 = dataReader.GetInt64(version2Idx);
                            long version3 = dataReader.GetInt64(version3Idx);
                            string versionExtension = DataReaderHelper.GetString(dataReader, versionExtIdx);
                            artefact.Version = BuildVersion(version1, version2, version3, versionExtension);
                            artefact.AgencyId = DataReaderHelper.GetString(dataReader, agencyIdx);
                            artefact.EndDate = DataReaderHelper.GetStringDate(dataReader, validToIdx);
                            artefact.StartDate = DataReaderHelper.GetStringDate(dataReader, validFromIdx);
                            //set final flag for compatibility reasons (final flag db column is not used any more)
                            bool isFinal = !ObjectUtil.ValidString(versionExtension);
                            artefact.FinalStructure = TertiaryBool.ParseBoolean(isFinal);
                            string structureUrl = DataReaderHelper.GetString(dataReader, structureUrlIdx);
                            string serviceUrl = DataReaderHelper.GetString(dataReader, serviceUrlIdx);
                            bool isStub = DataReaderHelper.GetBoolean(dataReader, isStubIdx);
                            var uri = DataReaderHelper.GetString(dataReader, uriIdx);
                            artefact.Uri = string.IsNullOrEmpty(uri)? null: new Uri(uri);
                            if (structureQuery.RequestedDetail.EnumType == ComplexStructureQueryDetailEnumType.Stub ||
                                structureQuery.RequestedDetail.EnumType == ComplexStructureQueryDetailEnumType.CompleteStub ||
                                isStub)
                            {
                                artefact.Stub = true;
                                artefact.ExternalReference = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);
                                AddStructureServiceUrl(artefact, structureUrl, serviceUrl);
                                this.HandleStubArtefactExtraFields(artefact, dataReader);
                            }
                            else
                            {
                                this.HandleArtefactExtraFields(artefact, dataReader);
                            }

                            artefactPkPairs.Add(new MaintainableWithPrimaryKey<T>(artefact, sysId));
                            artefactMap.Put(sysId, artefact);
                        }
                        else
                        {
                            artefact = artefactMap[sysId];
                        }

                        ReadLocalisedString(artefact, isNameIdx, txtIdx, langIdx, dataReader, structureQuery.RequestedDetail);
                    }
                }
            }
        }

        /// <summary>
        /// Will retrieve artefacts as references.
        /// </summary>
        /// <param name="commandBuilder">The method that will provide the <see cref="DbCommand"/>.</param>
        /// <param name="structureQuery">The structure query to get the artefacts for</param>
        /// <param name="artefactPkPairs">The data list to add to.</param>
        private void RetrieveReferencesToOtherArtefacts(
                Func<ICommonStructureQuery, DbCommand> commandBuilder,
                ICommonStructureQuery structureQuery,
                IList<MaintainableWithPrimaryKey<T>> artefactPkPairs)
        {
            using (DbCommand command = commandBuilder.Invoke(structureQuery))
            {
                _log.Info(string.Format("Executing Query: '{0}' with '{1}'", command.ToString(), structureQuery.ToString()));
                using (IDataReader reader = _mappingStoreDb.ExecuteReader(command))
                {
                    int sysIdPos = reader.GetOrdinal("SOURCE_ARTEFACT");
                    int idPos = reader.GetOrdinal("ID");
                    int versionPos1 = reader.GetOrdinal("MAJOR");
                    int versionPos2 = reader.GetOrdinal("MINOR");
                    int versionPos3 = reader.GetOrdinal("PATCH");
                    int versionExtPos = reader.GetOrdinal("EXTENSION");
                    int agencyPos = reader.GetOrdinal("AGENCY");
                    int artefactTypePos = reader.GetOrdinal("ARTEFACT_TYPE");
                    int sourceChildPos = reader.GetOrdinal("SOURCE_CHILD_FULL_ID");
                    int wildCardPosPos = reader.GetOrdinal("WILDCARD_POS");
                    int targetChildPos = reader.GetOrdinal("TARGET_CHILD_FULL_ID");
                    int refTypePos = reader.GetOrdinal("REF_TYPE");

                    while (reader.Read())
                    {
                        long sysId = DataReaderHelper.GetInt64(reader, sysIdPos);
                        var artefact = artefactPkPairs.SingleOrDefault(p => p.PrimaryKey.Equals(sysId));
                        if (artefact == null || artefact.Maintainable == null)
                        {
                            // TODO it possible to reach here for concurrent queries/additions
                            // no transaction is used
                            continue;
                        }
                        string maintainableId = DataReaderHelper.GetString(reader, idPos);
                        string agency = DataReaderHelper.GetString(reader, agencyPos);
                        long version1 = DataReaderHelper.GetInt64(reader, versionPos1);
                        long version2 = DataReaderHelper.GetInt64(reader, versionPos2);
                        long version3 = DataReaderHelper.GetInt64(reader, versionPos3);
                        string versionExtension = DataReaderHelper.GetString(reader, versionExtPos);
                        string artefactType = DataReaderHelper.GetString(reader, artefactTypePos);
                        string sourceSubStructurePath = DataReaderHelper.GetString(reader, sourceChildPos);
                        string targetSubStructurePath = DataReaderHelper.GetString(reader, targetChildPos);
                        string refType = DataReaderHelper.GetString(reader, refTypePos);
                        long wildCardPos = DataReaderHelper.GetInt64(reader, wildCardPosPos);
                        string version;
                        if (wildCardPos > 0)
                        {
                            version = BuildVersion(version1, version2, version3, wildCardPos);
                        }
                        else
                        {
                            version = BuildVersion(version1, version2, version3, versionExtension);
                        }

                        var structureType = SdmxStructureType.ParseClass(artefactType);

                        // target sub structure e.g. an item is not stored in StructureReferenceBean because
                        // we would need to store its Structure Type with no known added value
                        IStructureReference referenceBean =
                            new StructureReferenceImpl(agency, maintainableId, version, structureType);

                        ReferenceToOtherArtefact referenceToOtherArtefact = 
                            new ReferenceToOtherArtefact(referenceBean, refType, sourceSubStructurePath, targetSubStructurePath);
                        artefact.References.Add(referenceToOtherArtefact);
                    }
                }
            }
        }

        private ISet<T> HandleDetailLevel(
            ICommonStructureQuery structureQuery, IList<MaintainableWithPrimaryKey<T>> artefactPkPairs, IMaintainableMutableObject originalRequestedArtefact)
        {
            ISet<T> artefacts = new HashSet<T>();
            foreach (var pair in artefactPkPairs)
            {
                T artefact = pair.Maintainable;
                long sysId = pair.PrimaryKey;

                // TODO maybe get all artefact annotations in one SQL query per request ?
                // to avoid doing 1000 SQL queries
                IList<IAnnotationMutableObject> annotationMutableObjects = _maintainableAnnotationRetrieverEngine.RetrieveAnnotations(sysId);
                // It affects only SDMX SOAP 2.1 API
                if (AnnotationWhereHandler(structureQuery.AnnotationReference, annotationMutableObjects))
                {
                    continue;
                }

                else
                {
                    switch (structureQuery.RequestedDetail.EnumType)
                    {
                        case ComplexStructureQueryDetailEnumType.Full:
                            if (!artefact.Stub)
                            {
                                artefact = RetrieveDetails(pair, structureQuery);
                            }
                            break;
                        case ComplexStructureQueryDetailEnumType.Stub:
                        case ComplexStructureQueryDetailEnumType.CompleteStub:
                            // TODO why we do the following, why do we need categories for stub category scheme ?
                            if (artefact.StructureType.MaintainableStructureType.EnumType == SdmxStructureEnumType.CategoryScheme &&
                                ObjectUtil.ValidCollection(structureQuery.SpecificItems))
                            {
                                artefact = RetrieveDetails(pair, structureQuery);
                            }
                            break;
                    }
                }
                
                if (artefact != null)
                {
                    artefacts.Add(artefact);

                    if (structureQuery.RequestedDetail.EnumType != ComplexStructureQueryDetailEnumType.Stub)
                    {
                        foreach (IAnnotationMutableObject annotation in annotationMutableObjects)
                        {
                            artefact.AddAnnotation(annotation);
                        }
                    }
                }
            }

            return artefacts;
        }

        private bool AnnotationWhereHandler(IComplexAnnotationReference annotationWhere, IList<IAnnotationMutableObject> annotationMutableObjects)
        {
            if (annotationWhere != null)
            {
                if (!annotationMutableObjects.Any(a => AnnotationIsEqual(a, annotationWhere)))
                {
                    return true;
                }
            }

            return false;
        }

        private bool AnnotationIsEqual(IAnnotationMutableObject item, IComplexAnnotationReference annotationWhere)
        {
            return new AnnotationComparer().IsEqual(item, annotationWhere);
        }

        private void AddStructureServiceUrl(T artefact, string structureUrlValue, string serviceUrlValue)
        {
            if (string.IsNullOrWhiteSpace(structureUrlValue) && string.IsNullOrWhiteSpace(serviceUrlValue))
            {
                structureServiceUriHelperUriBuilder.AddStructureServiceUri(artefact);
            }
            else
            {
                Uri structureUrl, serviceUrl;
                if (Uri.TryCreate(structureUrlValue, UriKind.Absolute, out structureUrl))
                {
                    artefact.StructureURL = structureUrl;
                }
                if (Uri.TryCreate(serviceUrlValue, UriKind.Absolute, out serviceUrl))
                {
                    artefact.ServiceURL = serviceUrl;
                }
                if (structureUrl == null && serviceUrl == null)
                {
                    artefact.StructureURL = CreateDummyStructureUlr();
                }
            }
        }

        private static Uri CreateDummyStructureUlr()
        {
            Uri.TryCreate("http://dummy/url/need/to/changeit", UriKind.Absolute, out var dummyStructureUrl);
            return dummyStructureUrl;
        }

        private string BuildVersion(long version1, long version2, long version3, long wildCardPos)
        {
            // Wildcard supported only in semantic versioning without extension
            switch (wildCardPos)
            {
                case 1:
                    return string.Format("{0}+.{1}.{2}", version1, version2, version3);
                case 2:
                    return string.Format("{0}.{1}+.{2}", version1, version2, version3);
                case 3:
                    return string.Format("{0}.{1}.{2}+", version1, version2, version3);
                default:
                    // TODO should we throw an exception or just ignore the whole artefact ?
                    throw new SdmxSemmanticException("Invalid Wildcard position");
            }
        }

        /// <summary>
        /// Builds the version literal from it's components
        /// </summary>
        /// <param name="major"></param>
        /// <param name="minor"></param>
        /// <param name="patch"></param>
        /// <param name="versionExtension"></param>
        /// <returns></returns>
        protected string BuildVersion(long major, long minor, long patch, string versionExtension)
        {
            // -1 in any version part means not available.
            if (major == -1)
            {
                return null; // TODO how to work with version-less artefacts
                             // when version-less in SDMX 3.0.0 means 1.0
                             // but in next release will be nothing
            }

            if (minor == -1)
            {
                // FIXME NOT SUPPORTED, never supported
                return string.Format("{0:0}", major);
            }

            if (patch == -1)
            {
                // OLD Style versioning common in SDMX 2.0 and 2.1
                return string.Format("{0:0}.{1:0}", major, minor);
            }

            if (!ObjectUtil.ValidString(versionExtension))
            {
                // Semantic versioning stable
                return string.Format("{0:0}.{1:0}.{2:0}", major, minor, patch);
            }

            // Semantic versioning unstable stable
            return string.Format("{0:0}.{1:0}.{2:0}-{3}", major, minor, patch, versionExtension);
        }

        private SdmxStructureType GetStructureType()
        {
            //create an artefact so that we can find this artefact retriever's structure type
            T maintainable = CreateArtefact();//this is abstract method
            SdmxStructureType artefactType = maintainable.StructureType;
            return artefactType;
        }

        #endregion

        #region Obsolete method implementations



        #endregion
    }
}
