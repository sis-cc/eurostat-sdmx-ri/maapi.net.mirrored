// -----------------------------------------------------------------------
// <copyright file="DataProviderSchemeRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2017-02-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
// initial implementation ISTAT
//
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    /// The data provider scheme retrieval engine.
    /// </summary>
    internal class DataProviderSchemeRetrievalEngine : ItemSchemeRetrieverEngine<IDataProviderSchemeMutableObject, IDataProviderMutableObject>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataProviderSchemeRetrievalEngine"/> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        /// The mapping store DB.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="mappingStoreDb"/> is null
        /// </exception>
        public DataProviderSchemeRetrievalEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
        }

        protected override SdmxStructureType ItemSchemeStructureType => SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProviderScheme);

        protected override void AddItem(IDataProviderMutableObject item, string parentItemPath, IDataProviderSchemeMutableObject itemScheme, Dictionary<string, IDataProviderMutableObject> itemPathMap)
        {
            itemScheme.AddItem(item);
        }

        /// <summary>
        ///     Create a new instance of <see cref="IDataProviderSchemeMutableObject" />.
        /// </summary>
        /// <returns>
        ///     The <see cref="IDataProviderSchemeMutableObject" />.
        /// </returns>
        protected override IDataProviderSchemeMutableObject CreateArtefact()
        {
            return new DataProviderSchemeMutableCore();
        }

        /// <summary>
        /// The create item.
        /// </summary>
        /// <returns>
        /// The <see cref="IDataProviderMutableObject"/>.
        /// </returns>
        protected override IDataProviderMutableObject CreateItem()
        {
            return new DataProviderMutableCore();
        }
    }
}