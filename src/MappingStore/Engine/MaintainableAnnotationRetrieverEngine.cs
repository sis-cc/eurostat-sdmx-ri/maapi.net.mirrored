// -----------------------------------------------------------------------
// <copyright file="MaintainableAnnotationRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2014-11-06
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The annotation retriever engine.
    /// </summary>
    public class MaintainableAnnotationRetrieverEngine
    {
        /// <summary>
        ///     The _annotation command builder
        /// </summary>
        private readonly AnnotationCommandBuilder _annotationCommandBuilder;

        /// <summary>
        ///     The _annotation SQL query information
        /// </summary>
        private readonly SqlQueryInfo _annotationSqlQueryInfo;

        /// <summary>
        ///     This field holds the Mapping Store Database object.
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        /// Initializes a new instance of the <see cref="MaintainableAnnotationRetrieverEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">The mapping store database.</param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="mappingStoreDb" /> is null.
        /// </exception>
        public MaintainableAnnotationRetrieverEngine(Database mappingStoreDb)
        {
            if (mappingStoreDb == null)
            {
                throw new ArgumentNullException("mappingStoreDb");
            }

            this._mappingStoreDb = mappingStoreDb;

            string queryForSpecificPrimaryKey = string.Format("{0} WHERE AA.ART_ID = {1}", AnnotationConstants.ArtefactAnnotationQuery, "{0}");
            this._annotationSqlQueryInfo = new SqlQueryInfo()
            {
                QueryFormat = queryForSpecificPrimaryKey
            };

            this._annotationCommandBuilder = new AnnotationCommandBuilder(this._mappingStoreDb);
        }

        /// <summary>
        /// Retrieve annotations for the specified SDMX object specified by <paramref name="sysId"/>
        /// </summary>
        /// <param name="sysId">
        /// The artefact primary key value.
        /// </param>
        /// <returns>
        /// The <see cref="IList{IAnnotationMutableObject}"/>.
        /// </returns>
        public IList<IAnnotationMutableObject> RetrieveAnnotations(long sysId)
        {
            return RetrieveAnnotationsWithKey(sysId).Select(pair => pair.Value).ToArray();
        }

        /// <summary>
        /// Retrieve annotations with their primary key for the specified SDMX object specified by <paramref name="sysId"/>
        /// </summary>
        /// <param name="sysId">
        /// The artefact primary key value.
        /// </param>
        /// <returns>
        /// The <see cref="IList{IAnnotationMutableObject}"/>.
        /// </returns>
        public IList<KeyValuePair<long, IAnnotationMutableObject>> RetrieveAnnotationsWithKey(long sysId)
        {
            var annotations = new List<KeyValuePair<long, IAnnotationMutableObject>>();
            using (
                var command =
                    this._annotationCommandBuilder.Build(new PrimaryKeySqlQuery(this._annotationSqlQueryInfo, sysId)))
            using (var dataReader = this._mappingStoreDb.ExecuteReader(command))
            {
                int annIdIdx = dataReader.GetOrdinal("ANN_ID");
                int idIdx = dataReader.GetOrdinal("ID");
                int txtIdx = dataReader.GetOrdinal("TEXT");
                int langIdx = dataReader.GetOrdinal("LANGUAGE");
                int typeIdx = dataReader.GetOrdinal("TYPE");
                int titleIdx = dataReader.GetOrdinal("TITLE");
                int urlIdx = dataReader.GetOrdinal("URL");

                IDictionary<long, IAnnotationMutableObject> annotationMap =
                    new Dictionary<long, IAnnotationMutableObject>();

                while (dataReader.Read())
                {
                    var annId = dataReader.GetInt64(annIdIdx);
                    IAnnotationMutableObject annotation;
                    if (!annotationMap.TryGetValue(annId, out annotation))
                    {
                        annotation = new AnnotationMutableCore
                        {
                            Id = DataReaderHelper.GetStringWithNull(dataReader, idIdx),
                            Title =
                                                 DataReaderHelper.GetStringWithNull(dataReader, titleIdx),
                            Type = DataReaderHelper.GetStringWithNull(dataReader, typeIdx)
                        };

                        annotationMap.Add(annId, annotation);
                        var url = DataReaderHelper.GetString(dataReader, urlIdx);
                        Uri uri;
                        if (!string.IsNullOrWhiteSpace(url) && Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out uri))
                        {
                            annotation.Uri = uri;
                        }

                        annotations.Add(new KeyValuePair<long, IAnnotationMutableObject>(annId, annotation));
                    }

                    var text = DataReaderHelper.GetString(dataReader, txtIdx);
                    if (!string.IsNullOrWhiteSpace(text))
                    {
                        annotation.AddText(DataReaderHelper.GetString(dataReader, langIdx), text);
                    }
                }
            }

            return annotations;
        }
    }
}