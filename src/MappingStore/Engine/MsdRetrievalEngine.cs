// -----------------------------------------------------------------------
// <copyright file="MsdRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2017-04-17
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// The MSD retrieval engine.
    /// </summary>
    internal class MsdRetrievalEngine : ArtefactRetrieverEngine<IMetadataStructureDefinitionMutableObject>
    {
        /// <summary>
        /// The metadata retriever engine
        /// </summary>
        private readonly MetadataAttributeRetriever _metadataRetrieverEngine;

        /// <summary>
        /// The report structure retriever
        /// </summary>
        private readonly IdentifiableRetrieverEngine<IReportStructureMutableObject> _reportStructureRetriever;

        /// <summary>
        /// The metadata target retriever
        /// </summary>
        private readonly IdentifiableRetrieverEngine<IMetadataTargetMutableObject> _metadataTargetRetriever;

        /// <summary>
        /// The target obect retriever
        /// </summary>
        private readonly TargetObjectRetriever _targetObjectRetriever;

        /// <summary>
        /// The command builder
        /// </summary>
        private readonly MsdCommandBuilder _commandBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsdRetrievalEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">The mapping store DB.</param>
        /// <param name="typeSqlBuilder">The type SQL builder.</param>
        /// <param name="orderBy">The order By.</param>
        /// <exception cref="ArgumentNullException"><paramref name="mappingStoreDb" /> is null.</exception>
        public MsdRetrievalEngine(Database mappingStoreDb, ArtefactTypeSqlBuilder typeSqlBuilder)
            : base(mappingStoreDb)
        {
            this._commandBuilder = new MsdCommandBuilder(mappingStoreDb, typeSqlBuilder);
            this._metadataRetrieverEngine = new MetadataAttributeRetriever(mappingStoreDb, this._commandBuilder);
            this._reportStructureRetriever = new IdentifiableRetrieverEngine<IReportStructureMutableObject>(mappingStoreDb, () => new ReportStructureMutableCore(), this._commandBuilder.GetReportStructureCommand);
            this._metadataTargetRetriever = new IdentifiableRetrieverEngine<IMetadataTargetMutableObject>(mappingStoreDb, () => new MetadataTargetMutableCore(), this._commandBuilder.GetMetadataTargetCommand);
            this._targetObjectRetriever = new TargetObjectRetriever(mappingStoreDb);
        }

        /// <summary>
        ///     Create a new instance of <see cref="IMetadataFlowMutableObject"/>
        /// </summary>
        /// <returns>
        ///     The <see cref="IMetadataFlowMutableObject"/>
        /// </returns>
        protected override IMetadataStructureDefinitionMutableObject CreateArtefact()
        {
            return new MetadataStructureDefinitionMutableCore();
        }

        protected override IMetadataStructureDefinitionMutableObject RetrieveDetails(
            MaintainableWithPrimaryKey<IMetadataStructureDefinitionMutableObject> artefactPkPair,
            ICommonStructureQuery structureQuery)
        {
            var artefact = artefactPkPair.Maintainable;
            var sysId = artefactPkPair.PrimaryKey;

            // get report structures
            var reportStructures = this._reportStructureRetriever.Retrieve(sysId);
            artefact.ReportStructures.AddAll(reportStructures.Values);

            // get the metadata attributes and add them to report structures
            this._metadataRetrieverEngine.Retrieve(reportStructures, artefactPkPair);

            // get the metadata targets
            var metadataTargets = this._metadataTargetRetriever.Retrieve(sysId);
            artefact.MetadataTargets.AddAll(metadataTargets.Values);

            // get the target objects and add them to the corresponding metadata targets
            this._targetObjectRetriever.Populate(artefactPkPair, metadataTargets);

            // retrieve the link between report structures and metadata targets
            this.MapReportStructureWithMetadataTarget(sysId, reportStructures, metadataTargets);

            return artefact;
        }

        /// <summary>
        /// Maps the report structure with metadata target.
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <param name="reportStructures">The report structures.</param>
        /// <param name="metadataTargets">The metadata targets.</param>
        private void MapReportStructureWithMetadataTarget(long parentSysId, IDictionary<long, IReportStructureMutableObject> reportStructures, IDictionary<long, IMetadataTargetMutableObject> metadataTargets)
        {
            using (var command = this._commandBuilder.GetReportStructureMetadataTargetCommand(parentSysId))
            using (var reader = this.MappingStoreDb.ExecuteReader(command))
            {
                while (reader.Read())
                {
                    var reportStructureSysId = reader.GetSafeInt64("RS_ID");
                    var metadataTargetSysId = reader.GetSafeInt64("MDT_ID");

                    IReportStructureMutableObject reportStructure;
                    IMetadataTargetMutableObject metadataTarget;
                    if (reportStructures.TryGetValue(reportStructureSysId, out reportStructure) && metadataTargets.TryGetValue(metadataTargetSysId, out metadataTarget))
                    {
                        reportStructure.TargetMetadatas.Add(metadataTarget.Id);
                    }
                }
            }
        }
    }
}