// -----------------------------------------------------------------------
// <copyright file="ItemSchemeRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using Estat.Sri.Utils.Helper;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

    /// <summary>
    /// The engine to retrieve item schemes.
    /// Compatible to MSDB 7.0
    /// </summary>
    /// <typeparam name="TMaintainable">The item scheme type (e.g. Codelist)</typeparam>
    /// <typeparam name="TItem">The item type (e.g. Code)</typeparam>
    public abstract class ItemSchemeRetrieverEngine<TMaintainable, TItem> : ArtefactRetrieverEngine<TMaintainable>
        where TMaintainable : IItemSchemeMutableObject<TItem> where TItem : IItemMutableObject
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ItemSchemeRetrieverEngine<TMaintainable, TItem>));
        
        readonly ItemTableInfoBuilder itemTableInfoBuilder = new ItemTableInfoBuilder();

        /// <summary>
        /// Initialize a new instance of the class
        /// </summary>
        /// <param name="mappingStore">The msdb.</param>
        protected ItemSchemeRetrieverEngine(Database mappingStore)
            : base(mappingStore)
        {
        }

        #region stuff to override

        /// <summary>
        /// To be implemented by any specific item engines.
        /// </summary>
        /// <returns>The specific item.</returns>
        protected abstract TItem CreateItem();

        /// <summary>
        /// To be implemented by any specific item engine.
        /// Gets the parent structure type of the item (e.g. Codelist).
        /// </summary>
        protected abstract SdmxStructureType ItemSchemeStructureType { get; }

        /// <summary>
        /// Get the unique id, which is the item id for all cases except for Category Schemes and Reporting structure
        /// </summary>
        /// <param name="fullPath">The full path, parent_item_id.item_id</param>
        /// <param name="itemId">The item id</param>
        /// <returns></returns>
        protected virtual string GetUniqueId(string fullPath, string itemId)
        {
            return itemId;
        }

        ///<summary>
        /// Virtual method for retrieving extra information
        /// </summary>
        /// <param name="artefactPkPair">The artefact with primary key and references</param>
        /// <param name="itemPathMap">the map with the full path (parent + id) for nested item schemes or just id for</param>
        protected virtual void RetrieveItemDetails(MaintainableWithPrimaryKey<TMaintainable> artefactPkPair, Dictionary<string, TItem> itemPathMap)
        {
            // default don't do anything
        }

        /// <summary>
        /// Adds the nested items when retrieving the artefact details.
        /// Only affects nested item schemes, in SDMX RI only CategoryScheme and ReportingTaxonomy.
        /// </summary>
        /// <param name="itemPathMap"></param>
        protected virtual void AddNestedItems(Dictionary<string, TItem> itemPathMap)
        {
            // default don't do anything
        }

        /// <summary>
        /// Adds an item to item scheme.
        /// </summary>
        /// <param name="item">The item to add.</param>
        /// <param name="parentItemPath"></param>
        /// <param name="itemScheme">The item scheme to add to.</param>
        /// <param name="itemPathMap"></param>
        protected abstract void AddItem(TItem item, string parentItemPath, TMaintainable itemScheme, Dictionary<string, TItem> itemPathMap);

        #endregion

        /// <inheritdoc/>
        protected override IArtefactSqlQueryBuilder GetCommandBuilder(ICommonStructureQuery structureQuery)
        {
            return new ItemSchemeCommandBuilder(base.MappingStoreDb, ItemSchemeStructureType);
        }

        /// <summary>
        /// Sets the IsPartial property.
        /// </summary>
        /// <param name="artefact">The retrieved artefact.</param>
        /// <param name="dataReader">The reader with the data.</param>
        protected override void HandleArtefactExtraFields(TMaintainable artefact, IDataReader dataReader)
        {
            base.HandleArtefactExtraFields(artefact, dataReader);
            artefact.IsPartial = DataReaderHelper.GetBoolean(dataReader, "IS_PARTIAL");
        }

        /// <inheritdoc/>
        protected override TMaintainable RetrieveDetails(MaintainableWithPrimaryKey<TMaintainable> artefactPkPair, ICommonStructureQuery structureQuery)
        {
            return RetrieveDetails(artefactPkPair, structureQuery.SpecificItems);
        }

        /// <summary>
        /// Will return the item scheme with its items.
        /// Specific items if provided, otherwise all.
        /// </summary>
        /// <param name="artefactPkPair"></param>
        /// <param name="specificItems"></param>
        /// <returns></returns>
        protected TMaintainable RetrieveDetails(
            MaintainableWithPrimaryKey<TMaintainable> artefactPkPair, IList<IComplexIdentifiableReferenceObject> specificItems)
        {
            Func<string, string, bool> filter;
            // the itemCommandBuilder should reset after another query
            var itemCommandBuilder = new ItemCommandBuilder(MappingStoreDb);
            if (itemCommandBuilder.IsWithinLimits(specificItems))
            {
                // The items are filtered in the sql query,
                // To set the IsPartial, check that specific items are ACTUALLY less that the total.
                if (specificItems.Count > 0)
                {
                    int allCount = CountAllItems(itemCommandBuilder, artefactPkPair.PrimaryKey);
                    artefactPkPair.Maintainable.IsPartial = specificItems.Count < allCount;
                }
                if (IsNested())
                {
                    itemCommandBuilder.WithNestedSpecificItems(specificItems);
                }
                else
                {
                    itemCommandBuilder.WithNotNestedSpecificItems(specificItems);
                }
                filter = (s, f) => true;
            }
            else
            {
                filter = IsNested() ? FilterNestedBuilder(specificItems) : FilterBuilder(specificItems);
            }

            return this.RetrieveDetails(artefactPkPair, itemCommandBuilder.Build, filter);
        }

        /// <summary>
        /// An overload method that accepts a db-command build method and a filter for the items.
        /// Allows for handling the retrieving of items in various senarios, like the partial schemes.
        /// </summary>
        /// <param name="artefactPkPair">The ItemScheme artefact with PK.</param>
        /// <param name="buildMethod">The method to build the <see cref="DbCommand"/> with.</param>
        /// <param name="filterMethod">The filter for the items.</param>
        /// <returns></returns>
        protected virtual TMaintainable RetrieveDetails(MaintainableWithPrimaryKey<TMaintainable> artefactPkPair,
            Func<long, DbCommand> buildMethod, Func<string, string, bool> filterMethod)
        {
            var itemMap = new Dictionary<long, TItem>();

            // The key is the full path
            var itemPathMap = new Dictionary<string, TItem>();

            TMaintainable itemScheme = artefactPkPair.Maintainable;
            
            int dropped = 0;
            using (DbCommand command = buildMethod(artefactPkPair.PrimaryKey))
            {
                using (IDataReader dataReader = this.MappingStoreDb.ExecuteReader(command))
                {
                    int sysIdPos = dataReader.GetOrdinal("SYSID");
                    int idPos = dataReader.GetOrdinal("ID");
                    int parentItemPos = dataReader.GetOrdinal("PARENT_ITEM");
                    int namesItemPos = dataReader.GetOrdinal("ITEM_NAMES");
                    int descriptionsItemPos = dataReader.GetOrdinal("ITEM_DESCRIPTIONS");
                    int annotationsItemPos = dataReader.GetOrdinal("ITEM_ANNOTATIONS");

                    while (dataReader.Read())
                    {
                        long sysId = dataReader.GetInt64(sysIdPos);
                        TItem item;
                        if (!itemMap.ContainsKey(sysId))
                        {
                            string itemId = DataReaderHelper.GetString(dataReader, idPos);

                            // it contains the full path of the parents
                            string parentItemPath = DataReaderHelper.GetString(dataReader, parentItemPos);
                            string fullPath = BuildFullPath(itemId, parentItemPath);
                            // TODO parent item support
                            if (!filterMethod(itemId, fullPath))
                            {
                                // not nested or
                                /*
                                Not nested
                                If itemID is set and is a top-level id (e.g.: Code A (Annual)
                                in the Frequency Codelist),
                                and such an item exists in the matching item scheme,
                                the item scheme returned should contain only the matching item
                                also for nested:
                                 */
                                dropped++;
                                continue;
                            }
                            item = CreateItem();
                            item.Id = itemId;
                            AddItem(item, parentItemPath, itemScheme, itemPathMap);
                            itemMap.Add(sysId, item);
                            itemPathMap.Add(GetUniqueId(fullPath, itemId), item);
                        }
                        else
                        {
                            item = itemMap[sysId];
                        }

                        ReadItemDetails(item, namesItemPos,descriptionsItemPos,annotationsItemPos, dataReader);
                    }
                }
            }
            AddNestedItems(itemPathMap);
            PopulateMissingNames(itemMap);

            // ItemTableInfo itemTableInfo = itemTableInfoBuilder.Build(ItemSchemeStructureType);
            //IdentifiableAnnotationRetrieverEngine identifiableAnnotationRetrieverEngine =
            //    new IdentifiableAnnotationRetrieverEngine(this.MappingStoreDb, itemTableInfo);

            //identifiableAnnotationRetrieverEngine.RetrieveAnnotations(artefactPkPair.PrimaryKey, itemMap);

            RetrieveItemDetails(artefactPkPair, itemPathMap);

            if (dropped > 0)
            {
                itemScheme.IsPartial = true;
            }

            return itemScheme;
        }

        private void ReadItemDetails(TItem item,int namesItemPos,int descriptionsItemPos,int annotationsItemPos, IDataReader dataReader)
        {
            if (!dataReader.IsDBNull(namesItemPos))
            {
                var names = ItemJsonFieldsUtils.ParseItemLocalisedStrings(dataReader.GetString(namesItemPos));

                foreach (var name in names)
                {
                    item.AddName(name.Locale, name.Value);
                }
            }
            else if (!(item is IValueItem))
            { // name is mandatory in all items except ValueItem
                item.AddName("en", item.Id);
            }

            if (!dataReader.IsDBNull(descriptionsItemPos))
            {
                var descriptions = ItemJsonFieldsUtils.ParseItemLocalisedStrings(dataReader.GetString(descriptionsItemPos));

                foreach (var description in descriptions)
                {
                    item.AddDescription(description.Locale, description.Value);
                }
            }

            if (!dataReader.IsDBNull(annotationsItemPos))
            {
                var annotations = ItemJsonFieldsUtils.ParseItemAnnotations(dataReader.GetString(annotationsItemPos));
                foreach (var annotation in annotations)
                {
                    item.AddAnnotation(annotation);
                }
            }
        }

        private int CountAllItems(ItemCommandBuilder itemCommandBuilder, long itemSchemePK)
        {
            var commandBuilder = itemCommandBuilder.BuildCountAll(itemSchemePK);
            var count = MappingStoreDb.ExecuteScalar(commandBuilder.GetCommandQuery(), commandBuilder.DbParameters);
            return Convert.ToInt32(count, CultureInfo.InvariantCulture);
        } 

        private string BuildFullPath(string itemId, string parentItemPath)
        {
            string fullPath;
            if (ObjectUtil.ValidString(parentItemPath))
            {
                if (IsNested())
                {
                    fullPath = parentItemPath + "." + itemId;
                }
                else
                {
                    fullPath = parentItemPath;
                }
            }
            else
            {
                fullPath = itemId;
            }
            return fullPath;
        }

        private void PopulateMissingNames(Dictionary<long, TItem> itemMap)
        {
            foreach (TItem child in itemMap.Values)
            {
                // HACK Move descriptions to names.
                // In SDMX  v2.0 codes had only descriptions while in SDMX v2.1 they need to have names.
                // In mapping store (at least up to 2.7) only SDMX v2.0 was supported and code description were saved
                // as descriptions. So in case there are no names we move descriptions to names.
                if (!child.Names.Any())
                {
                    IList<ITextTypeWrapperMutableObject> description = child.Descriptions;
                    while (description.Any())
                    {
                        int index = description.Count - 1;
                        ITextTypeWrapperMutableObject last = description[index];
                        description.Remove(last);
                        child.Names.Add(last);
                    }
                    if (!child.Names.Any())
                    {
                        child.AddName("en", child.Id);
                    }
                }
            }
        }

        /// <summary>
        /// Build a function that accepts the item id and the path and returns true or false.
        /// </summary>
        /// <param name="specificItems">The list of specific items</param>
        /// <returns>the specific items</returns>
        protected Func<string, string, bool> FilterBuilder(IList<IComplexIdentifiableReferenceObject> specificItems)
        {
            // no specific items defined
            if (!ObjectUtil.ValidCollection(specificItems))
            {
                return (s, f) => true;
            }

            // SOAP 2.1 not nested we get at most one parent if any. But this is not supported in SDMX Source.
            ISet<string> notEqual = new HashSet<string>();
            ISet<string> equal = new HashSet<string>();

            ISet<string> parentNotEqual = new HashSet<string>();
            ISet<string> parentEqual = new HashSet<string>();
            foreach (IComplexIdentifiableReferenceObject itemRef in specificItems)
            {
                if (itemRef.Id != null && ObjectUtil.ValidString(itemRef.Id.SearchParameter))
                {
                    switch (itemRef.CascadeSelection)
                    {
                        case CascadeSelection.False:
                            AddCheckForId(notEqual, equal, itemRef);
                            break;
                        case CascadeSelection.True:
                            // retrieve children as well
                            // only possible in codelist extension, constraint and soap 2.1
                            // only one level
                            AddCheckForId(notEqual, equal, itemRef);
                            AddCheckForId(parentNotEqual, parentEqual, itemRef);
                            break;
                        case CascadeSelection.ExcludeRoot:
                            AddCheckForId(parentNotEqual, parentEqual, itemRef);
                            break;
                    }
                }
            }

            if (!parentEqual.Any() && !parentNotEqual.Any() && !equal.Any() && !notEqual.Any())
            {
                // not supported situation
                return (s, f) => true;
            }

            if (notEqual.Any())
            {
                return (s, f) => !notEqual.Contains(s) && !parentNotEqual.Contains(f);
            }

            return (s, f) => equal.Contains(s) || parentEqual.Contains(f);
        }

        /// <summary>
        /// Build a function that accepts the item id and the path and returns true or false.
        /// Should also return true for the parents of an item.
        /// </summary>
        /// <param name="specificItems"></param>
        /// <returns></returns>
        protected Func<string, string, bool> FilterNestedBuilder(IList<IComplexIdentifiableReferenceObject> specificItems)
        {
            // no specific items defined
            if (!ObjectUtil.ValidCollection<IComplexIdentifiableReferenceObject>(specificItems))
            {
                return (s, f) => true;
            }

            ISet<string> equal = new HashSet<string>();

            foreach (IComplexIdentifiableReferenceObject itemRef in specificItems)
            {
                if (itemRef.Id != null && ObjectUtil.ValidString(itemRef.Id.SearchParameter))
                {
                    // nested item schemes are not affected by codelist extensions or constraints
                    // so we keep the minimum
                    if (itemRef.Id.Operator.EnumType == TextSearchEnumType.Equal)
                    {
                        equal.AddAll(JoinItemPathNested(itemRef));
                    }
                }
            }

            if (!equal.Any())
            {
                // not supported situation
                return (s, f) => true;
            }

            return (s, f) => equal.Contains(f);
        }

        private void AddCheckForId(ISet<string> notEqual, ISet<string> equal, IComplexIdentifiableReferenceObject itemRef)
        {
            if (itemRef.Id.Operator.EnumType == TextSearchEnumType.Equal)
            {
                // Either one level or multiple via REST
                equal.UnionWith(JoinItemPathNotNested(itemRef));
            }
            else if (itemRef.Id.Operator.EnumType == TextSearchEnumType.NotEqual)
            {
                // One level only in Constraint or SOAP 2.1
                notEqual.Add(itemRef.Id.SearchParameter);
            }
        }

        /// <summary>
        /// This method will flatten the hierarchy since it applies to no nested without cascade.
        /// SOAP 2.1 doesn't support such configuration for no nested items, so we ignore any search criteria not supported by REST.
        /// </summary>
        /// <param name="referenceBean">The item reference</param>
        /// <returns>The accepted codes</returns>
        private static ISet<string> JoinItemPathNotNested(IComplexIdentifiableReferenceObject referenceBean)
        {
            if (referenceBean == null)
            {
                return new HashSet<string>();
            }
            ISet<string> items = new HashSet<string>();
            var stack = new LinkedList<IComplexIdentifiableReferenceObject>();
            stack.AddLast(referenceBean);
            while (stack.Any())
            {
                IComplexIdentifiableReferenceObject current = stack.First();
                stack.RemoveFirst();
                if (current.Id != null && ObjectUtil.ValidString(current.Id.SearchParameter))
                {
                    items.Add(current.Id.SearchParameter);
                }
                if (current.ChildReference != null)
                {
                    stack.AddFirst(referenceBean);
                }
            }

            return items;
        }

        /// <summary>
        /// This method will flatten the hierarchy since it applies to nested.
        /// SOAP 2.1 criteria are not supported and will be ignored.
        /// Constraints and Codelist extension do not apply to nested item schemes.
        /// </summary>
        /// <param name="referenceBean">The item reference</param>
        /// <returns>The accepted codes, including the parent paths.</returns>
        private static ISet<string> JoinItemPathNested(IComplexIdentifiableReferenceObject referenceBean)
        {
            if (referenceBean == null)
            {
                return new HashSet<string>();
            }
            ISet<string> items = new HashSet<string>();
            var stack = new LinkedList<IComplexIdentifiableReferenceObject>();
            stack.AddLast(referenceBean);
            while (stack.Any())
            {
                IComplexIdentifiableReferenceObject current = stack.First();
                stack.RemoveFirst();
                if (current.Id != null && ObjectUtil.ValidString(current.Id.SearchParameter))
                {
                    var parent = items.LastOrDefault();
                    if (!string.IsNullOrEmpty(parent))
                    {
                        items.Add(string.Join(".", parent, current.Id.SearchParameter));
                    }
                    else
                    {
                        items.Add(current.Id.SearchParameter);
                    }
                }
                if (current.ChildReference != null)
                {
                    stack.AddFirst(current.ChildReference);
                }
            }

            return items;
        }

        /// <summary>
        /// Indicates whether the item scheme supports nested items.
        /// </summary>
        /// <returns></returns>
        protected bool IsNested()
        {
            return 
                ItemSchemeStructureType.EnumType == SdmxStructureEnumType.CategoryScheme ||
                ItemSchemeStructureType.EnumType == SdmxStructureEnumType.ReportingTaxonomy;
        }
    }
}
