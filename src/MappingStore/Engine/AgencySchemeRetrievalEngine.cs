// -----------------------------------------------------------------------
// <copyright file="AgencySchemeRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2017-02-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
// initial implementation ISTAT
//
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The agency scheme retrieval engine.
    /// </summary>
    internal class AgencySchemeRetrievalEngine : ItemSchemeRetrieverEngine<IAgencySchemeMutableObject, IAgencyMutableObject>
    {
        /// <summary>
        ///     The _item <see cref="SqlQueryInfo" /> builder.
        /// </summary>
        private readonly ItemSqlQueryBuilder _itemSqlQueryBuilder;

        /// <summary>
        ///     The _item SQL query info.
        /// </summary>
        private readonly SqlQueryInfo _itemSqlQueryInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="AgencySchemeRetrievalEngine"/> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        /// The mapping store DB.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="mappingStoreDb"/> is null
        /// </exception>
        public AgencySchemeRetrievalEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
            this._itemSqlQueryBuilder = new ItemSqlQueryBuilder(mappingStoreDb, AgencySchemeConstant.ItemOrderBy);
            this._itemSqlQueryInfo = this._itemSqlQueryBuilder.Build(AgencySchemeConstant.ItemTableInfo);
        }

        /// <summary>
        ///     Create a new instance of <see cref="IAgencySchemeMutableObject" />.
        /// </summary>
        /// <returns>
        ///     The <see cref="IAgencySchemeMutableObject" />.
        /// </returns>
        protected override IAgencySchemeMutableObject CreateArtefact()
        {
            return new AgencySchemeMutableCore();
        }

        /// <summary>
        /// Create an item.
        /// </summary>
        /// <returns>
        /// The <see cref="IAgencyMutableObject" />.
        /// </returns>
        protected override IAgencyMutableObject CreateItem()
        {
            return new AgencyMutableCore();
        }

        /// <summary>
        /// The Agency Scheme type
        /// </summary>
        protected override SdmxStructureType ItemSchemeStructureType => SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AgencyScheme);

        /// <inheritdoc/>
        protected override void AddItem(
            IAgencyMutableObject item, string parentItemPath, 
            IAgencySchemeMutableObject itemScheme, Dictionary<string, IAgencyMutableObject> itemPathMap)
        {
            itemScheme.AddItem(item);
        }
    }
}