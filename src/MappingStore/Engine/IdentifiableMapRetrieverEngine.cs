// -----------------------------------------------------------------------
// <copyright file="IdentifiableMapRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-11-08
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The identifiable map retriever engine.
    /// </summary>
    public class IdentifiableMapRetrieverEngine
    {
        /// <summary>
        /// The log
        /// </summary>
        private readonly ILog _log = LogManager.GetLogger(typeof(IdentifiableMapRetrieverEngine));
        /// <summary>
        /// The SQL query builder
        /// </summary>
        private readonly IdentifiableSqlQueryBuilder _sqlQueryBuilder = new IdentifiableSqlQueryBuilder();

        /// <summary>
        /// The identifiable table information builder
        /// </summary>
        private readonly ParentItemTableBuilder _identifiableTableInfoBuilder = new ParentItemTableBuilder();

        private readonly string _idFieldName;
        private readonly SdmxStructureType _sdmxStructure;

        /// <summary>
        /// The SQL query information
        /// </summary>
        private readonly SqlQueryInfo _sqlQueryInfo;

        private readonly bool _hasParentItem;

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentifiableMapRetrieverEngine" /> class.
        /// </summary>
        /// <param name="idFieldName">Name of the identifier field.</param>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        public IdentifiableMapRetrieverEngine(string idFieldName, SdmxStructureType sdmxStructure)
        {
            if (idFieldName == null)
            {
                throw new ArgumentNullException("idFieldName");
            }

            if (sdmxStructure == null)
            {
                throw new ArgumentNullException("sdmxStructureType");
            }

            _idFieldName = idFieldName;
            _sdmxStructure = sdmxStructure;
            var childrenToParentTables = _identifiableTableInfoBuilder.Build(sdmxStructure).ToArray();
            _hasParentItem = childrenToParentTables.First().ParentItem != null;
            var parentTableThatHasId = sdmxStructure.GetParentTableWithId();
            if (parentTableThatHasId == null)
            {
                _sqlQueryInfo = _sqlQueryBuilder.Build(
                    _idFieldName,
                    childrenToParentTables);
            }
            else
            {
                _sqlQueryInfo = _sqlQueryBuilder.Build(
                  _idFieldName,
                  parentTableThatHasId,
                  childrenToParentTables);
            }
        }

        /// <summary>
        /// Retrieves the specified database.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <param name="parentItemColumn"></param>
        /// <param name="enumType"></param>
        /// <returns>The map between the primary key and the SDMX id</returns>
        /// Check notes for <see cref="https://citnet.tech.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/doc/CodeDocumentation.md">SDMXRI-1442</see>
        public IdentifiableMap Retrieve(Database database,
            long parentSysId, string parentItemColumn, SdmxStructureEnumType enumType)
        {
            _log.Debug("Started execution");
            var map = new IdentifiableMap(_sdmxStructure);
            var commandBuilder = new ItemCommandBuilder(database);
            var item = new ItemSqlQuery(_sqlQueryInfo, parentSysId);
            using (var command = commandBuilder.Build(item))
            {
                _log.DebugFormat(CultureInfo.InvariantCulture, "Started execution of sql query '{0}'", command.CommandText);
                using (var reader = database.ExecuteReader(command))
                {
                    var idIdx = reader.GetOrdinal(_idFieldName);
                    var sysIdx = reader.GetOrdinal(IdentifiableSqlQueryBuilder.SysIdFieldName);
                    while (reader.Read())
                    {
                        var id = reader.GetString(idIdx);
                        var sysId = reader.GetInt64(sysIdx);
                        string parentItemValue = null;
                        
                        if (enumType == SdmxStructureEnumType.MetadataAttribute)
                        {
                            map.Add(sysId, sysId.ToString(), parentItemValue);
                        }
                        else
                        {
                            var parentItemIdx = string.IsNullOrWhiteSpace(parentItemColumn) || !_hasParentItem ? -1 : reader.GetOrdinal(parentItemColumn);

                            if (parentItemIdx > -1 && !reader.IsDBNull(parentItemIdx))
                            {
                                parentItemValue = reader.GetString(parentItemIdx);
                            }
                            map.Add(sysId, id, parentItemValue);
                        }
                    }

                    map.Convert();
                }
            }
            _log.Debug("Ended execution");

            return map;
        }
    }
}