// -----------------------------------------------------------------------
// <copyright file="PartialConceptSchemeRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2018-06-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using System;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Engine;

    /// <summary>
    /// The engine to retrieve partial concept schemes
    /// </summary>
    public class PartialConceptSchemeRetrievalEngine : ConceptSchemeRetrievalEngine
    {
        private readonly PartialItemSchemeRetrievalEngine _partialItemSchemeRetrievalEngine;

        /// <summary>
        ///     Initializes a new instance of the <see cref="PartialConceptSchemeRetrievalEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="mappingStoreDb" /> is null
        /// </exception>
        public PartialConceptSchemeRetrievalEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
            this._partialItemSchemeRetrievalEngine = new PartialItemSchemeRetrievalEngine(mappingStoreDb);
        }

        /// <summary>
        /// Retrieves the items of the concept scheme that are used by the DSD in the <paramref name="structureQuery"/>.
        /// </summary>
        /// <param name="artefactPkPair">the Concept Scheme with PK.</param>
        /// <param name="structureQuery">The dsd querying for the partial concept scheme.</param>
        /// <returns>An <see cref="IConceptSchemeMutableObject"/> filled with the retrieved partial items.</returns>
        protected override IConceptSchemeMutableObject RetrieveDetails(
            MaintainableWithPrimaryKey<IConceptSchemeMutableObject> artefactPkPair, ICommonStructureQuery structureQuery)
        {
            var childReferences = _partialItemSchemeRetrievalEngine.RetrieveUsedItemIds(artefactPkPair.PrimaryKey, structureQuery,
                SdmxStructureEnumType.Concept);

            return RetrieveDetails(artefactPkPair, childReferences);
        }
    }
}
