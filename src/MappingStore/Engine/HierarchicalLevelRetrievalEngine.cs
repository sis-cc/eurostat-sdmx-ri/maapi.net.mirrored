// -----------------------------------------------------------------------
// <copyright file="HierarchicalCodeListRetrievealEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;

namespace Estat.Sri.MappingStoreRetrieval.Engine 
{
    internal class HierarchicalLevelRetrievalEngine : OtherNameableRetrieverEngine<LevelMutableCore>
    {
        private static readonly string SqlQuery = "select O.OTH_ID as SYSID, O.ID , O.VALID_FROM , O.VALID_TO , O.VERSION1 , O.VERSION2, O.VERSION3, O.PARENT_ARTEFACT, " +
            " O.URN_CLASS , O.PARENT_PATH, " +
            " hl.H_ID, " +
            " lso.IS_NAME , lso.LANGUAGE , lso.TEXT " +
            " from OTHER_NAMEABLE O " +
            " INNER JOIN HLEVEL hl on hl.LEVEL_ID = O.OTH_ID " +
            " LEFT OUTER JOIN LOCALISED_STRING_OTHER lso ON O.OTH_ID  = lso.OTH_ID " +
            " WHERE O.PARENT_ARTEFACT  = {0} and O.URN_CLASS  = {1}";

        private readonly IDictionary<long, HierarchyMutableCore> hierarchies;
        private readonly Dictionary<string, ILevelMutableObject> pathOfLevel = new Dictionary<string, ILevelMutableObject>(StringComparer.Ordinal);

        public HierarchicalLevelRetrievalEngine(Database mappingStore,  IDictionary<long, HierarchyMutableCore> hierarchies) : base(mappingStore, SdmxStructureEnumType.Level)
        {
            if (hierarchies is null)
            {
                throw new ArgumentNullException(nameof(hierarchies));
            }

            this.hierarchies = hierarchies;
        }

        public Dictionary<string, ILevelMutableObject> PathOfLevel => pathOfLevel;

        protected override string GetSqlQuery()
        {
            return SqlQuery;
        }

        protected override void HandleExtraFields(LevelMutableCore identifiable, IDataReader dataReader)
        {
            int parentPathIdx = dataReader.GetOrdinal("PARENT_PATH");
            if (dataReader.IsDBNull(parentPathIdx)) 
            {
                long hierarchyId = dataReader.GetInt64(dataReader.GetOrdinal("H_ID"));
                if (hierarchies.TryGetValue(hierarchyId, out HierarchyMutableCore hierarchy)) 
                {
                    if (hierarchy.ChildLevel != null) 
                    {
                        throw new SdmxSemmanticException("Hierarchy already has a level");
                    }
                    hierarchy.ChildLevel = identifiable;
                    hierarchy.FormalLevels = true;
                    pathOfLevel.Add(identifiable.Id, identifiable);
                }
            }
            else 
            {
                string parentPath = dataReader.GetString(parentPathIdx);
                pathOfLevel.Add(parentPath + "." + identifiable.Id, identifiable);
            }
        }
        
        protected override void HandleHierarchy(IDictionary<long, LevelMutableCore> sysIdToMutableBean) 
        {
            foreach (KeyValuePair<string, ILevelMutableObject> pair in pathOfLevel) 
            {
                int position = pair.Key.LastIndexOf("." + pair.Value.Id);
                // remove the current id from the path
                if (position > 0) 
                {
                    string pathWithoutCurrentId = pair.Key.Substring(0, position);
                    if (pathOfLevel.TryGetValue(pathWithoutCurrentId, out ILevelMutableObject parent)) 
                    {
                        parent.ChildLevel = pair.Value;
                    }
                }
            }
        }
        
    }
}