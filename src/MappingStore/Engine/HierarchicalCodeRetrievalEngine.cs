// -----------------------------------------------------------------------
// <copyright file="HierarchicalCodeListRetrievealEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.MappingStoreRetrieval.Manager;
using Estat.Sri.MappingStoreRetrieval.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Reference;

namespace Estat.Sri.MappingStoreRetrieval.Engine 
{
    internal class HierarchicalCodeRetrievalEngine : OtherIdentifiableRetrieverEngine<CodeRefMutableCore>
    {
        private static readonly string SqlQuery = "select O.OTH_ID as SYSID, O.ID , O.VALID_FROM , O.VALID_TO , O.VERSION1 , O.VERSION2, O.VERSION3, O.PARENT_ARTEFACT, " +
            " O.URN_CLASS , O.PARENT_PATH, " +
            " hc.H_ID, hl.PARENT_PATH as LEVEL_PATH, hl.ID as LEVEL_ID " +
            " from OTHER_NAMEABLE O " +
            " INNER JOIN HCL_CODE hc on hc.HCODE_ID = O.OTH_ID " +
            " LEFT OUTER JOIN OTHER_NAMEABLE hl on hc.LEVEL_ID = hl.OTH_ID " +
            " WHERE O.PARENT_ARTEFACT  = {0} and O.URN_CLASS  = {1}";

        private readonly IDictionary<long, HierarchyMutableCore> hierarchies;
        private readonly List<ReferenceToOtherArtefact> references;
        private readonly Dictionary<string, ICodeRefMutableObject> pathOfLevel = new Dictionary<string, ICodeRefMutableObject>(StringComparer.Ordinal);

        public HierarchicalCodeRetrievalEngine(Database mappingStore,  IDictionary<long, HierarchyMutableCore> hierarchies, List<ReferenceToOtherArtefact> references) : base(mappingStore, SdmxStructureEnumType.HierarchicalCode)
        {
            if (hierarchies is null)
            {
                throw new ArgumentNullException(nameof(hierarchies));
            }

            this.hierarchies = hierarchies;
            this.references = references;
        }


        protected override string GetSqlQuery()
        {
            return SqlQuery;
        }

        protected override void HandleExtraFields(CodeRefMutableCore identifiable, IDataReader dataReader)
        {
            int parentPathIdx = dataReader.GetOrdinal("PARENT_PATH");
            if (dataReader.IsDBNull(parentPathIdx)) 
            {
                long hierarchyId = dataReader.GetInt64(dataReader.GetOrdinal("H_ID"));
                if (hierarchies.TryGetValue(hierarchyId, out HierarchyMutableCore hierarchy)) 
                {
                    hierarchy.AddHierarchicalCode(identifiable);
                    pathOfLevel.Add(identifiable.Id, identifiable);

                }
            }
            else 
            {
                string parentPath = dataReader.GetString(parentPathIdx);
                pathOfLevel.Add(parentPath + "." + identifiable.Id, identifiable);
            }

            int levelIdx = dataReader.GetOrdinal("LEVEL_ID");
            if (!dataReader.IsDBNull(levelIdx)) 
            {
                var levelId = dataReader.GetString(levelIdx);
                var levelPath = DataReaderHelper.GetString(dataReader, "LEVEL_PATH");
                // TODO old code had normalized level where it started from the level that hierarchy started
                // we need to check first if this is something from the standard
                if (!string.IsNullOrEmpty(levelPath)) 
                {
                    identifiable.LevelReference = levelPath + "." + levelId;         
                }
                else 
                {
                    identifiable.LevelReference = levelId;
                }
            }
        }
        protected override void HandleHierarchy(IDictionary<long, CodeRefMutableCore> sysIdToMutableBean) 
        {
            foreach (KeyValuePair<string, ICodeRefMutableObject> pair in pathOfLevel) 
            {
                int position = pair.Key.LastIndexOf("." + pair.Value.Id);
                // remove the current id from the path
                if (position > 0) 
                {
                    string pathWithoutCurrentId = pair.Key.Substring(0, position);
                    if (pathOfLevel.TryGetValue(pathWithoutCurrentId, out ICodeRefMutableObject parent)) 
                    {
                        parent.AddCodeRef(pair.Value);
                    }
                }
            }

            if (references != null) 
            {
                foreach (ReferenceToOtherArtefact reference in references.Where(r => r.ReferenceType.Equals(RefTypes.Code)))
                {
                    if (pathOfLevel.TryGetValue(reference.SubStructureFullPath, out ICodeRefMutableObject codeRef)) 
                    {
                        codeRef.CodelistAliasRef = BuildCodelistRefAlias(reference);
                        codeRef.CodeId = reference.TargetSubStructurePath;
                    }
                }
            }
        }
        /// <summary>
        ///     Build a string suitable for <see cref="ICodelistRefMutableObject.Alias" /> and
        ///     <see cref="ICodeRefMutableObject.CodelistAliasRef" />
        /// </summary>
        /// <param name="reference">
        ///     The Codelist reference
        /// </param>
        /// <returns>
        ///     The build codelist ref alias.
        /// </returns>
        public static string BuildCodelistRefAlias(ReferenceToOtherArtefact reference)
        {
            return BuildCodelistRefAlias(reference.Target);
        }

        /// <summary>
        ///     Build a string suitable for <see cref="ICodelistRefMutableObject.Alias" /> and
        ///     <see cref="ICodeRefMutableObject.CodelistAliasRef" />
        /// </summary>
        /// <param name="reference">
        ///     The Codelist reference
        /// </param>
        /// <returns>
        ///     The build codelist ref alias.
        /// </returns>
        public static string BuildCodelistRefAlias(IStructureReference reference)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}@{1}@{2}", reference.MaintainableId, reference.AgencyId, reference.Version)
                .Replace(".", string.Empty);
        }

    }
}