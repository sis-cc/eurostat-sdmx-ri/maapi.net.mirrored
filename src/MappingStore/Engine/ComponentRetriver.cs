// -----------------------------------------------------------------------
// <copyright file="ComponentRetriver.cs" company="EUROSTAT">
//   Date Created : 2016-03-07
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;

    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Extensions;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    /// Component Retriever
    /// </summary>
    internal class ComponentRetriver
    {
        /// <summary>
        /// Gets for.
        /// </summary>
        /// <param name="crossSectionalMeasures">The cross sectional measures.</param>
        /// <param name="dataReader">The data reader.</param>
        /// <param name="parent">The parent.</param>
        /// <returns>the mutable object</returns>
        /// <exception cref="InvalidOperationException">Invalid component type exception</exception>
        public new Tuple<IComponentMutableObject, IMetadataAttributeUsageMutableObject> GetFor(
            ICollection<ICrossSectionalMeasureMutableObject> crossSectionalMeasures,
            IDataRecord dataReader,
            IDataStructureMutableObject parent)
        {
            var type = DataReaderHelper.GetString(dataReader, "TYPE");
            SdmxComponentType componentType;
            if (!Enum.TryParse(type, out componentType))
            {
                componentType = SdmxComponentType.None;
            }

            // the following are used only by Attributes and Measure (not primary)
            int minOccursIdx = dataReader.GetOrdinal("MIN_OCCURS");
            long? minOccurs = DataReaderHelper.GetInt64Nullable(dataReader, minOccursIdx);
            if (minOccurs == null)
            {
                minOccurs = 1; // SDMX 3.0.0 default
            }
            int maxOccursIdx = dataReader.GetOrdinal("MAX_OCCURS");
            long? maxOccurs = DataReaderHelper.GetInt64Nullable(dataReader, maxOccursIdx);
            if (maxOccurs == null)
            {
                maxOccurs = 1; // SDMX 3.0.0 default
            }
            bool isDefaultOccurences = minOccurs == 1 && maxOccurs == 1;

            IComponentMutableObject component = null;
            IMetadataAttributeUsageMutableObject metadataAttributeUsage = null;
            switch (componentType)
            {
                case SdmxComponentType.Dimension:
                    var dimension = new DimensionMutableCore
                    {
                        FrequencyDimension = DataReaderHelper.GetBoolean(dataReader, "IS_FREQ_DIM"),
                        MeasureDimension = DataReaderHelper.GetBoolean(dataReader, "IS_MEASURE_DIM")
                    };
                    component = dimension;
                    parent.AddDimension(dimension);
                    break;
                case SdmxComponentType.Attribute:
                    component = PopulateAttributes(parent, dataReader);
                    component.Representation = BuildRepresentation(isDefaultOccurences, maxOccurs, minOccurs);
                    break;
                case SdmxComponentType.TimeDimension:
                    var timeDimension = new DimensionMutableCore { TimeDimension = true };
                    component = timeDimension;
                    parent.AddDimension(timeDimension);
                    break;
                case SdmxComponentType.PrimaryMeasure:
                    var primaryMeasure = new PrimaryMeasureMutableCore();
                    component = primaryMeasure;
                    parent.PrimaryMeasure = primaryMeasure;
                    break;
                case SdmxComponentType.Measure: // SDMX 3.0.0 measures
                    var measure = new MeasureMutableCore();
                    measure.Representation = BuildRepresentation(isDefaultOccurences, maxOccurs, minOccurs);
                    // A text format may be added in a later stage.
                    component = measure;
                    parent.AddMeasure(measure);
                    break;
                case SdmxComponentType.CrossSectionalMeasure:
                    var crossSectionalMeasureBean = new CrossSectionalMeasureMutableCore
                    {
                        Code = DataReaderHelper.GetString(dataReader, "XS_MEASURE_CODE")
                    };
                    component = crossSectionalMeasureBean;
                    crossSectionalMeasures.Add(crossSectionalMeasureBean);
                    break;
                case SdmxComponentType.MetadataAttributeUsage:
                    IMetadataAttributeUsageMutableObject mdt = new MetadataAttributeUsageMutableCore()
                    {
                        MetadataAttributeReference = DataReaderHelper.GetString(dataReader, "ID")
                    };
                    SetAttachmentLevel(mdt, dataReader);
                    metadataAttributeUsage = mdt; 
                    parent.AttributeList.MetadataAttributes.Add(mdt);
                    break;
                default:
                    throw new InvalidOperationException(
                        string.Format(CultureInfo.InvariantCulture, "Invalid DSD Component type.  Mapping Store Database,  COMPONENT.TYPE = '{0}'", type));
            }

            return new Tuple<IComponentMutableObject,IMetadataAttributeUsageMutableObject> (component,metadataAttributeUsage);
        }

        /// <summary>
        /// Builds a representation object
        /// </summary>
        /// <param name="isDefaultOccurences"></param>
        /// <param name="maxOccurs"></param>
        /// <param name="minOccurs"></param>
        /// <returns><c>null</c> if <paramref name="isDefaultOccurences"/> is true.</returns>
        private static IRepresentationMutableObject BuildRepresentation(bool isDefaultOccurences, long? maxOccurs, long? minOccurs)
        {
            // A representation should also have an enumeration reference or a text format
            // In case of default min/max occurs there is no enumeration, so no representation should be created
            if (!isDefaultOccurences)
            {
                return new RepresentationMutableCore()
                {
                    MinOccurs = (int)minOccurs.Value,
                    MaxOccurs = maxOccurs < 1 ? UnboundedOccurenceObjectCore.Instance : new FiniteOccurenceObjectCore((int)maxOccurs.Value)
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        ///     The populate attributes.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="dataReader">
        ///     The data reader.
        /// </param>
        /// <returns>
        ///     The <see cref="IComponentMutableObject" />.
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">
        ///     Unsupported attachment level at COMPONENT.ATT_ASS_LEVEL
        /// </exception>
        private static IComponentMutableObject PopulateAttributes(
            IDataStructureMutableObject parent,
            IDataRecord dataReader)
        {
            var attribute = new AttributeMutableCore
            {
                Usage = ConvertAssignmentStatus(DataReaderHelper.GetString(dataReader, "ATT_STATUS"))
            };
            // TODO pending support already in Java
            //attribute.TimeFormat = DataReaderHelper.GetBoolean(dataReader, "ATT_IS_TIME_FORMAT")
            //                           ? "true"
            //                           : "false";
            SetAttachmentLevel(attribute, dataReader);

            IComponentMutableObject component = attribute;
            parent.AddAttribute(attribute);
            return component;
        }

        private static UsageType ConvertAssignmentStatus(string assignmentStatus)
        {
            if (!ObjectUtil.ValidString(assignmentStatus))
            {
                return UsageType.Optional;
            }

            switch (assignmentStatus.ToUpper())
            {
                case "MANDATORY":
                    return UsageType.Mandatory;
                case "CONDITIONAL":
                case "OPTIONAL": // in case we store USAGE_TYPE
                    return UsageType.Optional;
                default:
                    // TODO do we need to block a request because of this ?
                    throw new SdmxSemmanticException("Illegal assignment status " + assignmentStatus + "'. Valid values are 'Conditional' and 'Mandatory'.");
            }
        }

        private static void SetAttachmentLevel(IAttributeRelationshipMutableObject attribute, IDataRecord dataReader)
        {
            var attachmentLevel = DataReaderHelper.GetString(dataReader, "ATT_ASS_LEVEL");

            switch (attachmentLevel)
            {
                case AttachmentLevelConstants.DataSet:
                    attribute.AttachmentLevel = AttributeAttachmentLevel.DataSet;
                    break;
                case AttachmentLevelConstants.Group:
                    attribute.AttachmentLevel = AttributeAttachmentLevel.Group;
                    break;
                case AttachmentLevelConstants.Observation:
                    attribute.AttachmentLevel = AttributeAttachmentLevel.Observation;
                    break;
                case AttachmentLevelConstants.Series:
                    attribute.AttachmentLevel = AttributeAttachmentLevel.DimensionGroup;

                    break;
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "Attachment:" + attachmentLevel);
            }
        }
    }
}