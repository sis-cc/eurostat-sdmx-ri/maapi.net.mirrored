// -----------------------------------------------------------------------
// <copyright file="IPartialCodeListRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System.Collections.Generic;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// An interface that retrieves SDMX Header information.
    /// </summary>
    public interface IPartialCodeListRetrievalEngine :IRetrievalEngine<ICodelistMutableObject>
    {
        /// <summary>
        ///     Retrieve the <see cref="ICodelistMutableObject" />.
        /// </summary>
        /// <param name="maintainableRef">
        ///     The maintainable reference which may contain ID, AGENCY ID and/or VERSION.
        /// </param>
        /// <param name="detail">
        ///     The <see cref="StructureQueryDetail" /> which controls if the output will include details or not.
        /// </param>
        /// <param name="dataflowRef">
        ///     The dataflow Ref.
        /// </param>
        /// <param name="conceptId">
        ///     The concept Id.
        /// </param>
        /// <param name="isTranscoded">
        ///     The is Transcoded.
        /// </param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <returns>
        ///     The <see cref="ISet{ICodelistMutableObject}" />.
        /// </returns>
        ISet<ICodelistMutableObject> Retrieve(
            IMaintainableRefObject maintainableRef,
            ComplexStructureQueryDetailEnumType detail,
            IMaintainableRefObject dataflowRef,
            string conceptId,
            bool isTranscoded,
            IList<IMaintainableRefObject> allowedDataflows);

        /// <summary>
        /// Retrieves the available values for the <paramref name="conceptId"/> in a cube region.
        /// </summary>
        /// <param name="dataflowRef">The dataflow reference.</param>
        /// <param name="conceptId">The concept identifier.</param>
        /// <param name="transcodingType">Type of the transcoding.</param>
        /// <returns>The <see cref="IContentConstraintMutableObject" />.</returns>
        IContentConstraintMutableObject Retrieve(IMaintainableRefObject dataflowRef, string conceptId, TranscodingType transcodingType);
    }
}