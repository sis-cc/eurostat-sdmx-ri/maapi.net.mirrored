// -----------------------------------------------------------------------
// <copyright file="CategorisationRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Sdmxsource.Extension.Extension;

    /// <summary>
    ///     The categorisation retrieval engine.
    /// </summary>
    public class CategorisationRetrievalEngine : ArtefactRetrieverEngine<ICategorisationMutableObject>
    {
        /// <summary>
        ///     The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(CategorisationRetrievalEngine));

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorisationRetrievalEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <param name="filter">
        ///     The dataflow PRODUCTION filter.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="mappingStoreDb" /> is null.
        /// </exception>
        public CategorisationRetrievalEngine(Database mappingStoreDb, DataflowFilter filter)
            : base(mappingStoreDb)
        {
        }


        protected override ICategorisationMutableObject RetrieveDetails(
            MaintainableWithPrimaryKey<ICategorisationMutableObject> artefactPkPair, ICommonStructureQuery structureQuery)
        {
            if (artefactPkPair.Maintainable == null)
            {
                throw new ArgumentNullException("ICategorisationMutableObject");
            }
            ICategorisationMutableObject artefact = artefactPkPair.Maintainable;
            ReferenceToOtherArtefact source = artefactPkPair.References.FirstOrDefault(r => RefTypes.Source.Equals(r.ReferenceType));
            ReferenceToOtherArtefact target = artefactPkPair.References.FirstOrDefault(r => RefTypes.Target.Equals(r.ReferenceType));

            if (source == null)
            {
                // marking the CategorisationMutableBean as stub requires that a URI is provided
                _log.WarnFormat("Categorisation '{0}' is missing source reference", artefact.Id);
            }
            if (target == null || !ObjectUtil.ValidString(target.TargetSubStructurePath))
            {
                // marking the CategorisationMutableBean as stub requires that a URI is provided
                _log.WarnFormat("Categorisation '{0}' is missing target reference", artefact.Id);
            }
            if (source == null || target == null)
            {
                return null;
            }

            artefact.StructureReference = source.Target;
            artefact.CategoryReference = target.BuildSubStructureActualTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category));
            return artefact;
        }

        /// <inheritdoc/>
        public override IEnumerable<ICategorisationMutableObject> RetrieveAsParents(ICommonStructureQuery structureQuery)
        {
            IEnumerable<ICategorisationMutableObject> categorisationMutableBeans = base.RetrieveAsParents(structureQuery);

            if (ObjectUtil.ValidCollection(structureQuery.SpecificItems))
            {

                IEnumerable<string> itemPaths = structureQuery.SpecificItems.Select(i => JoinItemPathNested(i));

                return categorisationMutableBeans.Where(
                    categorisation => categorisation.CategoryReference != null &&
                    itemPaths.Contains(categorisation.CategoryReference.FullId));
            }

            return categorisationMutableBeans;
        }

        /// <summary>
        ///     Create a new instance of <see cref="ICategorisationMutableObject" />.
        /// </summary>
        /// <returns>
        ///     The <see cref="ICategorisationMutableObject" />.
        /// </returns>
        protected override ICategorisationMutableObject CreateArtefact()
        {
            return new CategorisationMutableCore();
        }

        /// <summary>
        /// This method will flatten the hierarchy since it applies to nested
	    /// SOAP 2.1 criteria are not supported and will be ignored
	    /// Constraints and Codelist extension do not apply to nested item schemes
        /// </summary>
        /// <param name="referenceObject">The item reference</param>
        /// <returns>The accepted codes</returns>
        private static string JoinItemPathNested(IComplexIdentifiableReferenceObject referenceObject)
        {
            if (referenceObject == null)
            {
                return null;
            }
            List<string> items = new List<string>();
            Stack<IComplexIdentifiableReferenceObject> stack = new Stack<IComplexIdentifiableReferenceObject>();
            stack.Push(referenceObject);
            while (stack.Any())
            {
                IComplexIdentifiableReferenceObject current = stack.Pop();
                if (current.Id != null && ObjectUtil.ValidString(current.Id.SearchParameter))
                {
                    items.Add(current.Id.SearchParameter);
                }
                if (current.ChildReference != null)
                {
                    stack.Push(current.ChildReference);
                }
            }

            return string.Join(".", items);
        }
    }
}