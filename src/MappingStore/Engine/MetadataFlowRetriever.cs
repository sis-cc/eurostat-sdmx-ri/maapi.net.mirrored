// -----------------------------------------------------------------------
// <copyright file="MetadataFlowRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-04-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;

    internal class MetadataFlowRetriever : ArtefactRetrieverEngine<IMetadataFlowMutableObject>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataFlowRetriever"/> class.
        /// </summary>
        /// <param name="mappingStoreDb">The mapping store DB.</param>
        public MetadataFlowRetriever(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
        }

        /// <summary>
        ///     Create a new instance of <see cref="IMetadataFlowMutableObject"/>.
        /// </summary>
        /// <returns>
        ///     The <see cref="IMetadataFlowMutableObject"/>.
        /// </returns>
        protected override IMetadataFlowMutableObject CreateArtefact()
        {
            return new MetadataflowMutableCore();
        }

        protected override IMetadataFlowMutableObject RetrieveDetails(
            MaintainableWithPrimaryKey<IMetadataFlowMutableObject> artefactPkPair, ICommonStructureQuery structureQuery)
        {
            if (artefactPkPair == null)
            {
                throw new ArgumentNullException("artefactPkPair");
            }

            var artefact = artefactPkPair.Maintainable;
            var sysId = artefactPkPair.PrimaryKey;
            ReferenceToOtherArtefact msdReference = artefactPkPair.References.FirstOrDefault(r => r.ReferenceType.Equals(RefTypes.Structure));
            if (msdReference == null)
            {
                throw new MappingStoreException(string.Format("Cannot find MetaData Structure Reference for dataflow {0}", artefact.Id));
            }

            artefact.MetadataStructureRef = msdReference.Target;
            if (artefact.MetadataStructureRef == null)
            {
                var message = string.Format(
                    CultureInfo.InvariantCulture,
                    "Cannot find MetaData Structure Reference for dataflow {0}",
                    artefact.Id);
                throw new MappingStoreException(message);
            }

            return artefact;
        }
    }
}
