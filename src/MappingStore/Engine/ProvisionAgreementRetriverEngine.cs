// -----------------------------------------------------------------------
// <copyright file="ProvisionAgreementRetriverEngine.cs" company="EUROSTAT">
//   Date Created : 2018-03-19
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;

    internal class ProvisionAgreementRetriverEngine : ArtefactRetrieverEngine<IProvisionAgreementMutableObject>
    {
        public ProvisionAgreementRetriverEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
        }

        protected override IProvisionAgreementMutableObject CreateArtefact()
        {
            return new ProvisionAgreementMutableCore();
        }

        protected override IProvisionAgreementMutableObject RetrieveDetails(
            MaintainableWithPrimaryKey<IProvisionAgreementMutableObject> artefactPkPair, ICommonStructureQuery structureQuery)
        {
            var artefact = artefactPkPair.Maintainable;
            var sysId = artefactPkPair.PrimaryKey;

            var dataProviderRef = artefactPkPair.References.FirstOrDefault(r => r.ReferenceType.Equals(RefTypes.DataProvider));
            artefact.DataproviderRef = dataProviderRef?.BuildSubStructureActualTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProvider));

            var structureUsage = artefactPkPair.References.FirstOrDefault(r =>
                r.ReferenceType.Equals(RefTypes.Dataflow) ||
                r.ReferenceType.Equals(RefTypes.Metadataflow));
            artefact.StructureUsage = structureUsage?.Target;

            return artefact;
        }
    }
}