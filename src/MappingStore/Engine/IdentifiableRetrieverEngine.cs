﻿// -----------------------------------------------------------------------
// <copyright file="IdentifiableRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2017-04-19
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;

    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    /// <summary>
    /// The identifiable retriever engine. It assumes the query returns two fields ID (string) and SYSID (long)
    /// </summary>
    /// <typeparam name="TIdentifiable">The type of the identifiable.</typeparam>
    internal class IdentifiableRetrieverEngine<TIdentifiable> where TIdentifiable : IIdentifiableMutableObject
    {
        /// <summary>
        /// The mapping store database
        /// </summary>
        private readonly Database _mappingStoreDatabase;

        /// <summary>
        /// The create instance
        /// </summary>
        private readonly Func<TIdentifiable> _createInstance;

        /// <summary>
        /// The command builder
        /// </summary>
        private readonly Func<long, DbCommand> _commandBuilder;

        /// <summary>
        /// The annotation retriever
        /// </summary>
        private readonly IdentifiableAnnotationRetrieverEngine _annotationRetriever;

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentifiableRetrieverEngine{TIdentifiable}" /> class.
        /// </summary>
        /// <param name="mappingStoreDatabase">The mapping store database.</param>
        /// <param name="createInstance">The create instance.</param>
        /// <param name="commandBuilder">The command builder.</param>
        public IdentifiableRetrieverEngine(Database mappingStoreDatabase, Func<TIdentifiable> createInstance, Func<long, DbCommand> commandBuilder)
        {
            this._mappingStoreDatabase = mappingStoreDatabase;
            this._createInstance = createInstance;
            this._commandBuilder = commandBuilder;
            var identifiableBuilder = new IdentifiableTableInfoBuilder();
            var tableInfos = identifiableBuilder.Build(createInstance().StructureType);
            this._annotationRetriever = new IdentifiableAnnotationRetrieverEngine(mappingStoreDatabase, tableInfos);
        }

        /// <summary>
        /// Retrieves the specified parent system identifier.
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <returns>The map between the primary key value and the corresponding SdmxSource identifiable mutable object</returns>
        public IDictionary<long, TIdentifiable> Retrieve(long parentSysId)
        {
            var sysIdToObject = new Dictionary<long, TIdentifiable>();
            using (var command = this._commandBuilder(parentSysId))
            using (var reader = this._mappingStoreDatabase.ExecuteReader(command))
            {
                int sysIdIdx = reader.GetOrdinal("SYSID");
                int idIdx = reader.GetOrdinal("ID");
                while (reader.Read())
                {
                    var sysId = reader.GetInt64(sysIdIdx);
                    var reportStructure = this._createInstance();
                    reportStructure.Id = reader.GetString(idIdx);
                    sysIdToObject.Add(sysId, reportStructure);
                }
            }

            this._annotationRetriever.RetrieveAnnotations(parentSysId, sysIdToObject);

            return sysIdToObject;
        }
    }
}