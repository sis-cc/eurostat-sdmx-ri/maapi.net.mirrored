// -----------------------------------------------------------------------
// <copyright file="DsdRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Collections;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The DSD retrieval engine.
    /// </summary>
    public class DsdRetrievalEngine : ArtefactRetrieverEngine<IDataStructureMutableObject>
    {
        /// <summary>
        ///     The _cross DSD builder.
        /// </summary>
        private static readonly CrossDsdBuilder _crossDsdBuilder = new CrossDsdBuilder();

        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DsdRetrievalEngine));

        /// <summary>
        ///     The component annotation retriever engine
        /// </summary>
        private readonly IdentifiableAnnotationRetrieverEngine _componentAnnotationRetrieverEngine;

        /// <summary>
        ///     The group annotation retriever engine
        /// </summary>
        private readonly IdentifiableAnnotationRetrieverEngine _groupAnnotationRetrieverEngine;

        /// <summary>
        /// The _SQL query information
        /// </summary>
        private readonly DsdCommandBuilder _dsdCommandBuilder;

        /// <summary>
        /// The text format retriever
        /// </summary>
        private readonly TextFormatRetriever _textFormatRetriever;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DsdRetrievalEngine" /> class.
        ///     Initializes a new instance of the <see cref="DataflowRetrievalEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="mappingStoreDb" /> is null.
        /// </exception>
        public DsdRetrievalEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
            this._dsdCommandBuilder = new DsdCommandBuilder(this.MappingStoreDb);

            var componentTableInfo = DsdConstant.ComponentTableInfo;
            this._textFormatRetriever = new TextFormatRetriever(this.MappingStoreDb);
            this._componentAnnotationRetrieverEngine = new IdentifiableAnnotationRetrieverEngine(
                mappingStoreDb,
                componentTableInfo);

            this._groupAnnotationRetrieverEngine = new IdentifiableAnnotationRetrieverEngine(
                mappingStoreDb,
                new ItemTableInfo(SdmxStructureEnumType.Group)
                {
                    ForeignKey = "DSD_ID",
                    PrimaryKey = "GR_ID",
                    Table = "DSD_GROUP"
                });
        }

        /// <summary>
        ///     Create a new instance of <see cref="IDataStructureMutableObject" />.
        /// </summary>
        /// <returns>
        ///     The <see cref="IDataStructureMutableObject" />.
        /// </returns>
        protected override IDataStructureMutableObject CreateArtefact()
        {
            return new DataStructureMutableCore();
        }

        /// <inheritdoc/>
        protected override IDataStructureMutableObject RetrieveDetails(
            MaintainableWithPrimaryKey<IDataStructureMutableObject> artefactPkPair, ICommonStructureQuery structureQuery)
        {
            // if part of a partial query for the codelists,
            // create also the map between codelists and DSD components
            if (structureQuery is IPartialCodelistStructureQuery partialCodelistQuery)
            {
                foreach (var referenceToOtherArtefact in artefactPkPair.References)
                {
                    if (referenceToOtherArtefact.Target.MaintainableStructureEnumType.EnumType != SdmxStructureEnumType.CodeList)
                    {
                        continue;
                    }
                    partialCodelistQuery.AddCodelistToComponent(referenceToOtherArtefact.Target.MaintainableId, referenceToOtherArtefact.SubStructureFullPath);
                }
            }

            var artefact = artefactPkPair.Maintainable;
            var sysId = artefactPkPair.PrimaryKey;

            // get metadata reference
            var msdReference = artefactPkPair.References
                .FirstOrDefault(r => r.ReferenceType.Equals(RefTypes.Metadata));
            artefact.MetadataStructure = msdReference?.Target;

            this.PopulateGroups(sysId, artefact);
            return this.FillComponents(artefactPkPair);
        }

        /// <summary>
        ///     Retrieve component type.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="dataReader">
        ///     The data reader.
        /// </param>
        /// <param name="crossSectionalMeasures">
        ///     The cross sectional measures.
        /// </param>
        /// <returns>
        ///     The <see cref="IComponentMutableObject" />.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        ///     Invalid DSD Component type.
        /// </exception>
        private static new Tuple<IComponentMutableObject, IMetadataAttributeUsageMutableObject> RetrieveComponentType(
            IDataStructureMutableObject parent,
            IDataRecord dataReader,
            ICollection<ICrossSectionalMeasureMutableObject> crossSectionalMeasures)
        {
            return new ComponentRetriver().GetFor(crossSectionalMeasures, dataReader, parent);
        }

        /// <summary>
        /// Retrieve the concept reference.
        /// </summary>
        /// <param name="allConceptRefs"></param>
        /// <param name="key"></param>
        /// <returns>
        /// The <see cref="IStructureReference"/>.
        /// </returns>
        private static IStructureReference RetrieveConceptRef(IDictionary<string, IStructureReference> allConceptRefs, string key)
        {
            if (allConceptRefs.TryGetValue(key, out IStructureReference structureReference)
                && structureReference != null)
            {
                return structureReference;
            }

            throw new SdmxConflictException($"No concept identity found for component {key}.");
        }

        /// <summary>
        ///     Retrieve cross sectional attachments.
        /// </summary>
        /// <param name="dataReader">
        ///     The data reader.
        /// </param>
        /// <param name="crossDataSet">
        ///     The list of components attached to cross-sectional data set.
        /// </param>
        /// <param name="conceptRef">
        ///     The concept id.
        /// </param>
        /// <param name="crossGroup">
        ///     The list of components attached to cross-sectional group.
        /// </param>
        /// <param name="crossSection">
        ///     The list of components attached to cross-sectional section.
        /// </param>
        /// <param name="crossObs">
        ///     The list of components attached to cross-sectional observation level
        /// </param>
        private static void RetrieveCrossSectionalAttachments(
            IDataRecord dataReader,
            ISet<string> crossDataSet,
            string conceptRef,
            ISet<string> crossGroup,
            ISet<string> crossSection,
            ISet<string> crossObs)
        {
            var crossSectionalAttachDataSet = DataReaderHelperEx.GetTristate(dataReader, "XS_ATTLEVEL_DS");
            if (crossSectionalAttachDataSet.IsTrue)
            {
                crossDataSet.Add(conceptRef);
            }

            var crossSectionalAttachGroup = DataReaderHelperEx.GetTristate(dataReader, "XS_ATTLEVEL_GROUP");
            if (crossSectionalAttachGroup.IsTrue)
            {
                crossGroup.Add(conceptRef);
            }

            var crossSectionalAttachSection = DataReaderHelperEx.GetTristate(dataReader, "XS_ATTLEVEL_SECTION");
            if (crossSectionalAttachSection.IsTrue)
            {
                crossSection.Add(conceptRef);
            }

            var crossSectionalAttachObservation = DataReaderHelperEx.GetTristate(dataReader, "XS_ATTLEVEL_OBS");
            if (crossSectionalAttachObservation.IsTrue)
            {
                crossObs.Add(conceptRef);
            }
        }

        /// <summary>
        ///     Sets the component representation reference.
        ///     This method addresses the enumeration types.
        /// </summary>
        /// <param name="artefactPkPair"></param>
        /// <param name="component">The component to set the enumeration reference for.</param>
        private void SetRepresentationReference(
            MaintainableWithPrimaryKey<IDataStructureMutableObject> artefactPkPair, IComponentMutableObject component)
        {
            // SDMX 2.1 measure dimension special case. Enumeration is concept scheme
            bool isMeasure = component is IDimensionMutableObject dimension && dimension.MeasureDimension;
            IDictionary<string, IStructureReference> enumerationMap = artefactPkPair.References
                .Where(r => r.ReferenceType.Equals(RefTypes.Enumeration) &&
                    (isMeasure ?
                    r.Target.TargetReference.EnumType == SdmxStructureEnumType.ConceptScheme :
                    r.Target.TargetReference.EnumType != SdmxStructureEnumType.ConceptScheme))
                .ToDictionary(
                    r => r.SubStructureFullPath,
                    r => r.Target);

            if (enumerationMap.TryGetValue(component.Id, out IStructureReference enumeration)
                && enumeration != null)
            {
                // a component (attribute) could already have a representaion
                // if has min/max occurs other than default
                // the representation could have been added in the ComponentRetriver.GetFor method
                if (component.Representation == null)
                {
                    component.Representation = new RepresentationMutableCore();
                }
                component.Representation.Representation = enumeration;
            }
        }

        /// <summary>
        ///     Setup attribute attachment level.
        /// </summary>
        /// <param name="componentMap"></param>
        /// <param name="metadataAttributeUsageMap"></param>
        /// <param name="parent">The parent.</param>
        /// <param name="dimensionRefs">The dimension references.</param>
        private static void SetupAttributeAttachmentLevel(
            IDictionary<long, IComponentMutableObject> componentMap,
            Dictionary<long, IMetadataAttributeUsageMutableObject> metadataAttributeUsageMap,
            IDataStructureMutableObject parent,
            IDictionary<long, ISet<string>> dimensionRefs)
        {
            foreach (var pair in dimensionRefs)
            {
                IAttributeRelationshipMutableObject attachmentLevelEntity = null;
                componentMap.TryGetValue(pair.Key, out var component);
                metadataAttributeUsageMap.TryGetValue(pair.Key, out var metadataAttribute);
                if (component != null)
                {
                    if (component is IAttributeRelationshipMutableObject attrRel)
                    {
                        attachmentLevelEntity = attrRel as IAttributeRelationshipMutableObject;
                    }
                }
                else if(metadataAttribute != null)
                {
                    attachmentLevelEntity = metadataAttribute;
                }
                
                if (attachmentLevelEntity != null && attachmentLevelEntity.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                {
                    // SDMX 2.1 and 3.0.0 dimension group
                    attachmentLevelEntity.DimensionReferences = new List<string>(pair.Value);
                }
            }

            // SDMX 2.0 Series attachment level
            List<string> seriesDimension = parent.Dimensions
                .Where(d => !d.TimeDimension)
                .Select(d => d.Id)
                .ToList();

            parent.Attributes
                .Where(a => a.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup &&
                    !ObjectUtil.ValidCollection(a.DimensionReferences)).ToList()
                .ForEach(a => a.DimensionReferences = seriesDimension);
        }

        /// <summary>
        ///     Setup cross sectional DSD.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="crossDataSet">
        ///     The list of components attached to cross-sectional data set.
        /// </param>
        /// <param name="crossGroup">
        ///     The list of components attached to cross-sectional group.
        /// </param>
        /// <param name="crossSection">
        ///     The list of components attached to cross-sectional section.
        /// </param>
        /// <param name="crossObs">
        ///     The list of components attached to cross-sectional observation level
        /// </param>
        /// <param name="crossSectionalMeasures">
        ///     The cross sectional measures.
        /// </param>
        /// <param name="measureCodelistRepresentation">
        ///     The SDMX v2.0 measure dimension codelist representation; otherwise null
        /// </param>
        /// <returns>
        ///     The <see cref="IDataStructureMutableObject" />.
        /// </returns>
        private static IDataStructureMutableObject SetupCrossSectionalDsd(
            List<ReferenceToOtherArtefact> references,
            IDataStructureMutableObject parent,
            ICollection<string> crossDataSet,
            ICollection<string> crossGroup,
            ICollection<string> crossSection,
            ICollection<string> crossObs,
            IEnumerable<ICrossSectionalMeasureMutableObject> crossSectionalMeasures
           )
        {
            if (parent.DimensionList == null || !ObjectUtil.ValidCollection(parent.Dimensions))
            {
                return parent;
            }

            if (
                parent.Dimensions.All(
                    o =>
                    o.FrequencyDimension || o.TimeDimension || o.MeasureDimension || crossDataSet.Contains(o.Id)
                    || crossGroup.Contains(o.Id) || crossSection.Contains(o.Id) || crossObs.Contains(o.Id)))
            {
                if (parent.AttributeList == null
                    || parent.AttributeList.Attributes.All(
                        o =>
                        crossDataSet.Contains(o.Id) || crossGroup.Contains(o.Id) || crossSection.Contains(o.Id)
                        || crossObs.Contains(o.Id)))
                {
                    var crossDsd = _crossDsdBuilder.Build(parent);
                    crossDsd.CrossSectionalAttachDataSet.AddAll(crossDataSet);
                    crossDsd.CrossSectionalAttachSection.AddAll(crossSection);
                    crossDsd.CrossSectionalAttachGroup.AddAll(crossGroup);
                    crossDsd.CrossSectionalAttachObservation.AddAll(crossObs);
                    var measure = crossDsd.Dimensions.FirstOrDefault(o => o.MeasureDimension);
                    if (measure != null)
                    {
                        foreach (var crossSectionalMeasureMutableObject in
                            crossSectionalMeasures)
                        {
                            crossSectionalMeasureMutableObject.MeasureDimension = measure.Id;
                            crossDsd.CrossSectionalMeasures.Add(crossSectionalMeasureMutableObject);
                        }

                        // Get the codelist reference to the measure dimension
                        // because in sdmx 2,0 measure dim 
                        IStructureReference measureCodelistRepresentation = references
                            .FirstOrDefault(r => r.Target.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.CodeList // SDMX 2.0 must have a codelist
                            && r.ReferenceType == RefTypes.Enumeration
                            && (string.Equals(r.SubStructureFullPath, measure.Id, StringComparison.Ordinal) // 
                               || string.Equals(r.SubStructureFullPath, measure.ConceptRef.ChildReference.Id, StringComparison.Ordinal)))?
                               .Target;
                        if (measureCodelistRepresentation == null && measure.Representation?.Representation == null)
                        {
                            // bug we should never reach here
                            throw new SdmxInternalServerException();

                        }
                        else if (measureCodelistRepresentation != null)
                        {
                            measure.Representation = new RepresentationMutableCore { Representation = measureCodelistRepresentation };
                            DataStructureUtil.ConvertMeasureRepresentation(crossDsd);
                        }
                    }

                    return crossDsd;
                }
            }

            return parent;
        }

        /// <summary>
        ///     Fill the components of a Data Structure object
        /// </summary>
        /// <param name="artefactPkPair"></param>
        /// <returns></returns>
        private IDataStructureMutableObject FillComponents(MaintainableWithPrimaryKey<IDataStructureMutableObject> artefactPkPair)
        {
            IDataStructureMutableObject parent = artefactPkPair.Maintainable;
            long parentSysId = artefactPkPair.PrimaryKey;
            if (parent.Stub || (parent.ExternalReference != null && parent.ExternalReference.IsTrue))
            {
                return parent;
            }

            var crossSectionalMeasures = new List<ICrossSectionalMeasureMutableObject>();
            var crossDataSet = new HashSet<string>();
            var crossGroup = new HashSet<string>();
            var crossSection = new HashSet<string>();
            var crossObs = new HashSet<string>();
            var componentMap = new Dictionary<long, IComponentMutableObject>();
            var metadataAttributeUsageMap = new Dictionary<long, IMetadataAttributeUsageMutableObject>();

            IDictionary<string, IStructureReference> allConceptRefs = artefactPkPair.References
                .Where(r => r.ReferenceType.Equals(RefTypes.ConceptIdentity))
                .ToDictionary(
                    r => r.SubStructureFullPath,
                    r => r.BuildSubStructureActualTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept)));

            using (var command = this._dsdCommandBuilder.GetComponentCommand(parentSysId))
            using (var dataReader = this.MappingStoreDb.ExecuteReader(command))
            {
                var idIdx = dataReader.GetOrdinal("ID");

                while (dataReader.Read())
                {
                    long sysId = dataReader.GetSafeInt64("COMP_ID");
                    var componentOrMetadata = RetrieveComponentType(parent, dataReader, crossSectionalMeasures);
                    // TODO: this whole procedure should be refactored to use a DSDbuilder like java,
                    // that receives all information and contains all the logc to build the DSD
                    var component = componentOrMetadata.Item1;
                    var metadataAttributeUsage = componentOrMetadata.Item2;
                    //the COMPONENT table also contains MetadataAttributeUsage, which is not a component
                    if (component != null)
                    {
                        component.Id = DataReaderHelper.GetString(dataReader, idIdx);
                        SetRepresentationReference(artefactPkPair, component);
                        component.ConceptRef = RetrieveConceptRef(allConceptRefs, component.Id);

                        RetrieveCrossSectionalAttachments(
                            dataReader,
                            crossDataSet,
                            component.Id,
                            crossGroup,
                            crossSection,
                            crossObs);
                        componentMap.Put(sysId, component);
                    }
                    else
                    {
                        metadataAttributeUsageMap.Put(sysId, metadataAttributeUsage);
                    }
                }
            }

            GetTextFormatInformation(parentSysId, componentMap);
            GetConceptRoleInformation(parentSysId, componentMap, 
                artefactPkPair.References.Where(r => r.ReferenceType.Equals(RefTypes.ConceptRoles)));

            if (parent.AttributeList != null)
            {
                this.SetupGroupAttributes(parent, parentSysId);

                SetupAttributeAttachmentLevel(componentMap,metadataAttributeUsageMap, parent, this.GetAttributeDimensionRefs(parentSysId));
            }

            parent = SetupCrossSectionalDsd(
                artefactPkPair.References,
                parent,
                crossDataSet,
                crossGroup,
                crossSection,
                crossObs,
                crossSectionalMeasures);
            this.SetupAttributesAttachmentCrossMeasures(parent as ICrossSectionalDataStructureMutableObject, parentSysId);
            
            this.SetupAttributesMeasureRelationships(parent, parentSysId);
            this._componentAnnotationRetrieverEngine.RetrieveAnnotations(parentSysId, componentMap);
            this._componentAnnotationRetrieverEngine.RetrieveAnnotations(parentSysId, metadataAttributeUsageMap);

            return parent;
        }

        private void SetupAttributesMeasureRelationships(IDataStructureMutableObject dataStructure, long parentSysId)
        {
            if (dataStructure is ICrossSectionalDataStructureMutableObject)
            {
                return;
            }

            using (var command = this._dsdCommandBuilder.GetAttributeMeasureCommand(parentSysId))
            {
                using (var dataReader = this.MappingStoreDb.ExecuteReader(command))
                {
                    while (dataReader.Read())
                    {
                        var measureId = DataReaderHelper.GetString(dataReader, "XSM_ID");
                        var attributeId = DataReaderHelper.GetString(dataReader, "ATTR_ID");

                        dataStructure.Attributes.First(x=>x.Id == attributeId).MeasureRelationships.Add(measureId);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the text format information.
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <param name="componentMap">The component map.</param>
        private void GetTextFormatInformation(long parentSysId, Dictionary<long, IComponentMutableObject> componentMap)
        {
            var textFormats = this._textFormatRetriever.Retrieve(parentSysId);
            foreach (var component in componentMap.Values)
            {
                if (textFormats.TryGetValue(component.Id, out ITextFormatMutableObject textFormat) && textFormat != null)
                {
                    if (component.Representation == null)
                    {
                        component.Representation = new RepresentationMutableCore();
                    }

                    component.Representation.TextFormat = textFormat;
                }
            }
        }

        /// <summary>
        /// Get the Concept Role information for the components of a DSD with primary key <paramref name="parentSysId"/>
        /// </summary>
        /// <param name="parentSysId">The DSD primary key value <c>DSD_ID</c></param>
        /// <param name="componentMap">A map between the <c>COMPONENT.COMP_ID</c> and the component object.</param>
        /// <param name="allConceptRoles">The concept roles references.</param>
        private void GetConceptRoleInformation(long parentSysId, Dictionary<long, IComponentMutableObject> componentMap, IEnumerable<ReferenceToOtherArtefact> allConceptRoles)
        {
            foreach(var keyValuePair in componentMap)
            {
                var component = keyValuePair.Value;
                var componentRoles = allConceptRoles.Where(r => r.SubStructureFullPath.Equals(component.Id))
                    .Select(x => x.BuildSubStructureActualTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept)));
                // a conceptRef can have multiple concept roles
                foreach (IStructureReference concept in componentRoles)
                {
                    // add concept roles (if any)
                    if (!component.TryAddConceptRole(concept))
                    {
                        _log.ErrorFormat("Tried to add concept role to a component that is neither a Dimension nor an Attribute. COMP_ID = {0}, DSD_ID = {1}.", keyValuePair.Key, parentSysId);
                    }
                }
            }
        }

        /// <summary>
        ///     Retrieve the Dimension references from mapping store
        /// </summary>
        /// <param name="parentSysId">
        ///     The DSD primary key in the Mapping Store Database
        /// </param>
        /// <returns>
        ///     The <see cref="IDictionaryOfSets{String, String}" />.
        /// </returns>
        private IDictionaryOfSets<long, string> GetAttributeDimensionRefs(long parentSysId)
        {
            IDictionaryOfSets<long, string> mappedAttributes =
                new DictionaryOfSets<long, string>();

            using (var command = this._dsdCommandBuilder.GetAttributeCommand(parentSysId))
            {
                using (var dataReader = this.MappingStoreDb.ExecuteReader(command))
                {
                    while (dataReader.Read())
                    {
                        var currGroupId = DataReaderHelper.GetInt64(dataReader, "ATTR_ID");

                        ISet<string> dimensionList;
                        if (!mappedAttributes.TryGetValue(currGroupId, out dimensionList))
                        {
                            dimensionList = new HashSet<string>(StringComparer.Ordinal);
                            mappedAttributes.Add(currGroupId, dimensionList);
                        }

                        dimensionList.Add(DataReaderHelper.GetString(dataReader, "ID"));
                    }
                }
            }

            return mappedAttributes;
        }

        /// <summary>
        ///     Retrieve the Groups from mapping store and populate <paramref name="artefact" />
        /// </summary>
        /// <param name="parentSysId">
        ///     The DSD primary key in the Mapping Store Database
        /// </param>
        /// <param name="artefact">
        ///     The <see cref="IDataStructureMutableObject" /> to add the groups
        /// </param>
        private void PopulateGroups(long parentSysId, IDataStructureMutableObject artefact)
        {
            var groupMap = new Dictionary<long, IGroupMutableObject>();
            using (var command = this._dsdCommandBuilder.GetGroupCommand(parentSysId))
            {
                using (var dataReader = this.MappingStoreDb.ExecuteReader(command))
                {
                    while (dataReader.Read())
                    {
                        var currGroupId = dataReader.GetSafeInt64("GR_ID");
                        IGroupMutableObject group;
                        if (!groupMap.TryGetValue(currGroupId, out group))
                        {
                            group = new GroupMutableCore { Id = DataReaderHelper.GetString(dataReader, "GROUP_ID") };
                            artefact.AddGroup(group);
                            groupMap.Add(currGroupId, group);
                        }

                        group.DimensionRef.Add(DataReaderHelper.GetString(dataReader, "DIMENSION_REF"));
                    }
                }
            }

            this._groupAnnotationRetrieverEngine.RetrieveAnnotations(parentSysId, groupMap);
        }

        /// <summary>
        ///     Setups the group attributes.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dsdId">The DSD identifier.</param>
        private void SetupAttributesAttachmentCrossMeasures(
            ICrossSectionalDataStructureMutableObject dataStructure,
            long dsdId)
        {
            if (dataStructure == null || !ObjectUtil.ValidCollection(dataStructure.Attributes)
                || (!ObjectUtil.ValidCollection(dataStructure.CrossSectionalMeasures)))
            {
                return;
            }

            Dictionary<string,IAttributeMutableObject> idToAttribureMap = new Dictionary<string,IAttributeMutableObject>();
            if (ObjectUtil.ValidCollection(dataStructure.CrossSectionalMeasures))
            {
                idToAttribureMap =
                    dataStructure.Attributes.Where(o => dataStructure.CrossSectionalAttachObservation.Contains(o.Id))
                        .ToDictionary(o => o.Id, StringComparer.Ordinal);
            }
            if (idToAttribureMap.Count == 0)
            {
                return;
            }

            using (var command = this._dsdCommandBuilder.GetAttributeMeasureCommand(dsdId))
            {
                using (var dataReader = this.MappingStoreDb.ExecuteReader(command))
                {
                    while (dataReader.Read())
                    {
                        var measureId = DataReaderHelper.GetString(dataReader, "XSM_ID");
                        var attributeId = DataReaderHelper.GetString(dataReader, "ATTR_ID");
                        IAttributeMutableObject attribute;
                        if (idToAttribureMap.TryGetValue(attributeId, out attribute))
                        {
                            Debug.Assert(attribute != null, "BUG. Attribute is null");
                            IList<string> measures;
                            if (!dataStructure.AttributeToMeasureMap.TryGetValue(attributeId, out measures))
                            {
                                measures = new List<string>();
                                dataStructure.AttributeToMeasureMap.Add(attributeId, measures);
                            }

                            measures.Add(measureId);
                        }
                        else
                        {
                            _log.WarnFormat(
                                "Attribute with ID {0} of DSD {1} has measure mappings but its attachment level is not observation",
                                attributeId,
                                dataStructure.Id);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Setups the group attributes.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dsdId">The DSD identifier.</param>
        private void SetupGroupAttributes(IDataStructureMutableObject dataStructure, long dsdId)
        {
            if (!ObjectUtil.ValidCollection(dataStructure.Attributes))
            {
                return;
            }

            var idToAttribureMap =
                dataStructure.Attributes.Where(o => o.AttachmentLevel == AttributeAttachmentLevel.Group)
                    .ToDictionary(o => o.Id, StringComparer.Ordinal);
            if (idToAttribureMap.Count == 0)
            {
                return;
            }

            using (var command = this._dsdCommandBuilder.GetGroupAttributeCommand(dsdId))
            {
                using (var dataReader = this.MappingStoreDb.ExecuteReader(command))
                {
                    while (dataReader.Read())
                    {
                        var groupId = DataReaderHelper.GetString(dataReader, "ID");
                        var attributeId = DataReaderHelper.GetString(dataReader, "ATTR_REF");
                        IAttributeMutableObject attribute;
                        if (idToAttribureMap.TryGetValue(attributeId, out attribute))
                        {
                            Debug.Assert(attribute != null, "BUG. Attribute is null");
                            attribute.AttachmentGroup = groupId;
                        }
                        else
                        {
                            _log.WarnFormat(
                                "Attribute with ID {0} of DSD {1} has group mappings but its attachment level is not group",
                                attributeId,
                                dataStructure.Id);
                        }
                    }
                }
            }
        }
    }
}