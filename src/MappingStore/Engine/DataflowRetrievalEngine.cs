// -----------------------------------------------------------------------
// <copyright file="DataflowRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;

    /// <summary>
    ///     The dataflow retrieval engine.
    /// </summary>
    public class DataflowRetrievalEngine : ArtefactRetrieverEngine<IDataflowMutableObject>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(DataflowRetrievalEngine));

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataflowRetrievalEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <param name="filter">
        ///     The filter. (Optional defaults to <see cref="DataflowFilter.Production" />
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="mappingStoreDb" /> is null.
        /// </exception>
        public DataflowRetrievalEngine(Database mappingStoreDb, DataflowFilter filter = DataflowFilter.Production)
            : base(mappingStoreDb)
        {
        }

        protected override IArtefactSqlQueryBuilder GetCommandBuilder(ICommonStructureQuery structureQuery)
        {
            //TODO check if retrieval works for parents/children (also check content constraint retrieval engine)
            DataflowFilter filter = DataflowFilterContext.CurrentFilter;
            return new DataflowCommandBuilder(MappingStoreDb, filter);
        }

        /// <summary>
        ///     Create a new instance of <see cref="IDataflowMutableObject" />.
        /// </summary>
        /// <returns>
        ///     The <see cref="IDataflowMutableObject" />.
        /// </returns>
        protected override IDataflowMutableObject CreateArtefact()
        {
            return new DataflowMutableCore();
        }

        /// <summary>
        ///     Handles the extra fields of Dataflows
        /// </summary>
        /// <param name="artefact">
        ///     The maintainable reference to add the annotations to.
        /// </param>
        /// <param name="reader">
        ///     The reader
        /// </param>
        protected override void HandleArtefactExtraFields(IDataflowMutableObject artefact, IDataReader reader)
        {
            base.HandleArtefactExtraFields(artefact, reader);

            if (reader.HasFieldName("PRODUCTION"))
            {
                bool isProduction = DataReaderHelper.GetBoolean(reader, "PRODUCTION");

                if (!isProduction)
                {
                    artefact.SetNonProduction();
                }
            }
        }

        /// <summary>
        ///     Retrieve details for the specified <paramref name="artefactPkPair" />.
        /// </summary>
        /// <param name="artefactPkPair"></param>
        /// <param name="structureQuery"></param>
        /// <returns>
        ///     The <see cref="IDataflowMutableObject" />.
        /// </returns>
        protected override IDataflowMutableObject RetrieveDetails(
            MaintainableWithPrimaryKey<IDataflowMutableObject> artefactPkPair, 
            ICommonStructureQuery structureQuery)
        {
            var artefact = artefactPkPair.Maintainable;
            if (artefact == null)
            {
                throw new ArgumentNullException("artefact");
            }

            artefact.DataStructureRef = artefactPkPair.References
                .Select(r => r.Target)
                .FirstOrDefault(r => r.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.Dsd);
            if (artefact.DataStructureRef == null)
            {
                var message = string.Format(
                    CultureInfo.InvariantCulture, 
                    "Cannot find Data Structure Reference for dataflow {0}", 
                    artefact.Id);
                _log.ErrorFormat(CultureInfo.CurrentCulture, message);
                throw new MappingStoreException(message);
            }

            return artefact;
        }

        public override ISet<IStructureReference> RetrieveResolvedParents(ICommonStructureQuery structureQuery)
        {
            return base.RetrieveResolvedParents(structureQuery);
        }
    }
}