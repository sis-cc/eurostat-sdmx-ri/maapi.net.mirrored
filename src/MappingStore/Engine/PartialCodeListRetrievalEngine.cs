// -----------------------------------------------------------------------
// <copyright file="PartialCodeListRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;

    /// <summary>
    ///     The partial code list retrieval engine.
    /// </summary>
    internal class PartialCodeListRetrievalEngine : CodeListRetrievalEngine
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PartialCodeListRetrievalEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="mappingStoreDb" /> is null
        /// </exception>
        public PartialCodeListRetrievalEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
        }

        /// <summary>
        /// Retrieves the items of the codelist that are used by the content constraints in the <paramref name="structureQuery"/>.
        /// </summary>
        /// <param name="artefactPkPair">the Codelist with PK.</param>
        /// <param name="structureQuery">The artefact querying for the partial codelist, and other info.</param>
        /// <returns>An <see cref="ICodelistMutableObject"/> filled with the retrieved partial items.</returns>
        protected override ICodelistMutableObject RetrieveDetails(
            MaintainableWithPrimaryKey<ICodelistMutableObject> artefactPkPair, ICommonStructureQuery structureQuery)
        {
            IList<IComplexIdentifiableReferenceObject> childReferences = new List<IComplexIdentifiableReferenceObject>();

            // if not a partial query return all codes (no childReferences)
            if (!(structureQuery is IPartialCodelistStructureQuery partialQuery))
            {
                return RetrieveDetails(artefactPkPair, childReferences);
            }

            // first get (if any) the components using the given codelist
            // if no components is using it, return all codes (no childReferences)
            if (!partialQuery.CodelistToComponents.TryGetValue(artefactPkPair.Maintainable.Id, out ISet<string> componentIds))
            {
                return RetrieveDetails(artefactPkPair, childReferences);
            }

            // create the list of codes to return as specific items
            // first try the included cube regions
            childReferences = partialQuery.ContentConstraints
                    .SelectMany(c => c.IncludedCubeRegion.KeyValues) // include these codes 
                    .Where(kv => componentIds.Contains(kv.Id)) // only for the Codelist given
                    .SelectMany(kv => kv.KeyValues) // the constrained code values
                    .Distinct()
                    .Select(v => ComplexIdentifiableReferenceCore.CreateForRest(v, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Code))) // create child reference
                    .ToList();
            // a component will either be constrained in included cube region(s) or excluded
            // so if included cube regions give any results, fetch the constrained codes
            if (childReferences.Any())
            {
                return RetrieveDetails(artefactPkPair, childReferences);
            }

            // otherwise, try for the excluded cube regions
            childReferences = partialQuery.ContentConstraints
                .SelectMany(c => c.ExcludedCubeRegion.KeyValues) // exclude these codes 
                .Where(kv => componentIds.Contains(kv.Id)) // only for the Codelist given
                .SelectMany(kv => kv.KeyValues) // the constrained code values
                .Distinct()
                // create child reference
                .Select(v => ComplexIdentifiableReferenceCore.CreateForConstraintOrCodelistExtension(
                    new ComplexTextReferenceCore("en", TextSearch.GetFromEnum(TextSearchEnumType.NotEqual), v),
                    CascadeSelection.False,
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Code))) 
                .ToList();

            // this will return either codes except the excluded ones,
            // or all the codes incase there is no contraint
            return RetrieveDetails(artefactPkPair, childReferences);
        }
    }
}