// --------------------------------------------------------------------------------------------------------------------
// 9.5 ISTAT ENHANCEMENT
// <summary>
//   The organisation unit scheme retrieval engine.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using System.Collections.Generic;

    /// <summary>
    ///     The organisation unit scheme retrieval engine.
    /// </summary>
    internal class OrganisationUnitSchemeRetrievalEngine : ItemSchemeRetrieverEngine<IOrganisationUnitSchemeMutableObject, IOrganisationUnitMutableObject>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrganisationUnitSchemeRetrievalEngine"/> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        /// The mapping store DB.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="mappingStoreDb"/> is null
        /// </exception>
        public OrganisationUnitSchemeRetrievalEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
        }

        protected override SdmxStructureType ItemSchemeStructureType => SdmxStructureType.GetFromEnum(SdmxStructureEnumType.OrganisationUnitScheme);

        protected override void AddItem(IOrganisationUnitMutableObject item, string parentItemPath, IOrganisationUnitSchemeMutableObject itemScheme, Dictionary<string, IOrganisationUnitMutableObject> itemPathMap)
        {
            item.ParentUnit = parentItemPath.Equals(string.Empty) ? null : parentItemPath;
            itemScheme.AddItem(item);
        }

        /// <summary>
        ///     Create a new instance of <see cref="IOrganisationUnitSchemeMutableObject" />.
        /// </summary>
        /// <returns>
        ///     The <see cref="IOrganisationUnitSchemeMutableObject" />.
        /// </returns>
        protected override IOrganisationUnitSchemeMutableObject CreateArtefact()
        {
            return new OrganisationUnitSchemeMutableCore();
        }


        protected override IOrganisationUnitMutableObject CreateItem()
        {
            return new OrganisationUnitMutableCore();
        }
    }
}