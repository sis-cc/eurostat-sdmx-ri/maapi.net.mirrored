// -----------------------------------------------------------------------
// <copyright file="ContentConstraintRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2017-02-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
// initial implementation ISTAT
//
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// The Content Constrain retriever 
    /// </summary>
    public class ContentConstraintRetrievalEngine : ArtefactRetrieverEngine<IContentConstraintMutableObject>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ContentConstraintRetrievalEngine));

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentConstraintRetrievalEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">The mapping store database.</param>
        public ContentConstraintRetrievalEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
        }

        protected override IArtefactSqlQueryBuilder GetCommandBuilder(ICommonStructureQuery structureQuery)
        {
            return new ContentConstraintCommandBuilder(this.MappingStoreDb, structureQuery);
        }

        /// <summary>
        /// Sets/resets the IsDefiningActualDataPresent.
        /// </summary>
        /// <param name="artefact">The artefact to handle extra fields for.</param>
        /// <param name="reader">The data reader</param>
        protected override void HandleStubArtefactExtraFields(IContentConstraintMutableObject artefact, IDataReader reader)
        {
            string artefactType = DataReaderHelper.GetString(reader, "ARTEFACT_TYPE");
            if (artefactType.Equals(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AllowedConstraint).UrnClass))
            {
                artefact.IsDefiningActualDataPresent = false;
            }
            else if (artefactType.Equals(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ContentConstraint).UrnClass)) 
            {
                // SDMXRI-2046
                // The user requests stub but the ContentConstraint is not stored as stub in the Mapping Store database
                // The ARTEFACT_TYPE is ContentConstraint and the IsDefiningActualDataPresent is defined in CONTENT_CONSTRAINT table 
                // at field ACTUAL_DATA
                bool actualData = DataReaderHelper.GetBoolean(reader, "ACTUAL_DATA");
                artefact.IsDefiningActualDataPresent = actualData;
            }
        }

        /// <summary>
        /// Sets the properties: IsDefiningActualDataPresent, ReleaseCalendar, ReferencePeriod, ConstraintAttachment
        /// </summary>
        /// <param name="artefact"></param>
        /// <param name="reader"></param>
        protected override void HandleArtefactExtraFields(IContentConstraintMutableObject artefact, IDataReader reader)
        {
            base.HandleArtefactExtraFields(artefact, reader);

            bool actualData = DataReaderHelper.GetBoolean(reader, "ACTUAL_DATA");
            artefact.IsDefiningActualDataPresent = actualData;

            string periodicity = DataReaderHelper.GetString(reader, "PERIODICITY");
            string offset = DataReaderHelper.GetString(reader, "OFFSET");
            string tolerance = DataReaderHelper.GetString(reader, "TOLERANCE");

            if (ObjectUtil.ValidOneString(periodicity, offset, tolerance))
            {
                artefact.ReleaseCalendar = new ReleaseCalendarMutableCore();
                artefact.ReleaseCalendar.Offset = offset;
                artefact.ReleaseCalendar.Periodicity = periodicity;
                artefact.ReleaseCalendar.Tolerance = tolerance;
            }
            int startDateIdx = reader.GetOrdinal("START_TIME");
            int endDateIdx = reader.GetOrdinal("END_TIME");
            DateTime? startDate = DataReaderHelper.GetStringDate(reader, startDateIdx);
            DateTime? endDate = DataReaderHelper.GetStringDate(reader, endDateIdx);
            if (startDate.HasValue && endDate.HasValue)
            {
                artefact.ReferencePeriod = new ReferencePeriodMutableCore();
                artefact.ReferencePeriod.EndTime = endDate;
                artefact.ReferencePeriod.StartTime = startDate;
            }

            PopulateConstraintAttachment(reader, artefact);
        }

        /// <summary>
        /// Create a new instance of <typeparamref name="T" />.
        /// </summary>
        /// <returns>
        /// The <see cref="IContentConstraintMutableObject" />.
        /// </returns>
        protected override IContentConstraintMutableObject CreateArtefact()
        {
            return new ContentConstraintMutableCore();
        }

        /// <summary>
        /// Retrieve details for the given Content Constraint
        /// <param name="artefactPkPair">the artefact to retrieve details for.</param>
        /// <param name="structureQuery">The structure query/param>
        /// </summary>
        /// <returns>
        /// The <see cref="IContentConstraintMutableObject" />.
        /// </returns>
        protected override IContentConstraintMutableObject RetrieveDetails(
            MaintainableWithPrimaryKey<IContentConstraintMutableObject> artefactPkPair,
            ICommonStructureQuery structureQuery)
        {
            IContentConstraintMutableObject artefact = artefactPkPair.Maintainable;

            // populate references
            List<ReferenceToOtherArtefact> references = artefactPkPair.References;
            if (references.Any() && artefact.ConstraintAttachment == null)
            {
                artefact.ConstraintAttachment = new ContentConstraintAttachmentMutableCore();
            }

            foreach (ReferenceToOtherArtefact referenceToOtherArtefact in references)
            {
                switch (referenceToOtherArtefact.ReferenceType)
                {
                    case RefTypes.ConstraintAttachment:

                        artefact.ConstraintAttachment.AddStructureReference(referenceToOtherArtefact.Target);
                        break;
                    case RefTypes.DataProvider:
                        IStructureReference dataProviderRef = referenceToOtherArtefact
                            .BuildSubStructureActualTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProvider));
                        if (artefact.ConstraintAttachment.DataOrMetadataSetReference != null)
                        {
                            artefact.ConstraintAttachment.DataOrMetadataSetReference.DataSetReference = dataProviderRef;
                        }
                        else
                        {
                            artefact.ConstraintAttachment.AddStructureReference(dataProviderRef);
                        }
                        break;
                }
            }

            long sysId = artefactPkPair.PrimaryKey;
            // get data sources only if there is a constraint attachment
            if (artefact.ConstraintAttachment != null)
            {
                AddDataSources(sysId, artefact.ConstraintAttachment);
            }

            PopulateCubeRegion(artefactPkPair.Maintainable, artefactPkPair.PrimaryKey);

            return artefact;
        }

        private void PopulateConstraintAttachment(IDataReader dataReader,
            IContentConstraintMutableObject artefact)
        {

            var attachTypeString = dataReader["ATTACH_TYPE"].ToString();
            ContentConstraintAttachType attachType;
            if (!Enum.TryParse(attachTypeString, true, out attachType))
            {
                _log.WarnFormat("Unsupported attach type '{0}' for content constraint.", attachTypeString);
                return;
            }
            
            switch (attachType)
            {
                case ContentConstraintAttachType.DataSet:
                case ContentConstraintAttachType.MetadataSet:
                    if (artefact.ConstraintAttachment == null)
                    {
                        artefact.ConstraintAttachment = new ContentConstraintAttachmentMutableCore();
                    }

                    artefact.ConstraintAttachment.DataOrMetadataSetReference =
                        new DataAndMetadataSetMutableReferenceImpl()
                        {
                            SetId = dataReader["SET_ID"].ToString(),
                            IsDataSetReference = attachType == ContentConstraintAttachType.DataSet
                        };
                    break;
                // dataprovider ref is handled in retrieve details
                case ContentConstraintAttachType.SimpleDataSource:
                    if (artefact.ConstraintAttachment == null)
                    {
                        artefact.ConstraintAttachment = new ContentConstraintAttachmentMutableCore();
                    }

                    artefact.ConstraintAttachment.DataSources.Add(new DataSourceMutableCore()
                    {
                        DataUrl = new Uri(dataReader["SIMPLE_DATA_SOURCE"].ToString()),
                        SimpleDatasource = true
                    });
                    break;
                default:
                    // Structure references to maintainable references are handled elsewhere
                    break;
            }
        }

        private void AddDataSources(long contentConstraintId,
            IConstraintAttachmentMutableObject artefactConstraintAttachment)
        {
            var inParameter =
                MappingStoreDb.CreateInParameter(ParameterNameConstants.IdParameter, DbType.Int64, contentConstraintId);
            using (var command =
                MappingStoreDb.GetSqlStringCommandFormat(ContentConstraintConstant.DataSourceSql, inParameter))
            {
                using (var dr = this.MappingStoreDb.ExecuteReader(command))
                {
                    while (dr.Read())
                    {
                        artefactConstraintAttachment.DataSources.Add(new DataSourceMutableCore()
                        {
                            DataUrl = !string.IsNullOrWhiteSpace(dr["DATA_URL"].ToString())
                                ? new Uri(dr["DATA_URL"].ToString())
                                : null,
                            WSDLUrl = !string.IsNullOrWhiteSpace(dr["WSDL_URL"].ToString())
                                ? new Uri(dr["WSDL_URL"].ToString())
                                : null,
                            WADLUrl = !string.IsNullOrWhiteSpace(dr["WADL_URL"].ToString())
                                ? new Uri(dr["WADL_URL"].ToString())
                                : null,
                            SimpleDatasource = DataReaderHelper.GetBoolean(dr, "IS_SIMPLE"),
                            RESTDatasource = DataReaderHelper.GetBoolean(dr, "IS_REST"),
                            WebServiceDatasource = DataReaderHelper.GetBoolean(dr, "IS_WS")
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Populates the cube region.
        /// </summary>
        /// <param name="artefact">The artefact.</param>
        /// <param name="sysId">The system identifier.</param>
        private void PopulateCubeRegion(IContentConstraintMutableObject artefact, long sysId)
        {
            var inParameter = MappingStoreDb.CreateInParameter(ParameterNameConstants.IdParameter, DbType.Int64, sysId);
            using (var command = MappingStoreDb.GetSqlStringCommandFormat(ContentConstraintConstant.SqlConsItem, inParameter))
            {
                using (var dataReader = this.MappingStoreDb.ExecuteReader(command))
                {
                    long keyValueIDCurr = long.MinValue;
                    var componentTypeCurr = string.Empty;
                    var includeCube = false;
                    artefact.IncludedCubeRegion = new CubeRegionMutableCore();
                    artefact.ExcludedCubeRegion = new CubeRegionMutableCore();

                    IKeyValuesMutable key = null;
                    
                    //this while actually adds the previous row
                    while (dataReader.Read())
                    {
                        var valueIDCurr = dataReader.GetSafeInt64("CUBE_REGION_KEY_VALUE_ID");
                        if (keyValueIDCurr == long.MinValue || valueIDCurr != keyValueIDCurr)
                        {
                            if (key != null)
                            {
                                if (componentTypeCurr == "Attribute")
                                {
                                    if (includeCube)
                                    {
                                        artefact.IncludedCubeRegion.AddAttributeValue(key);
                                    }
                                    else
                                    {
                                        artefact.ExcludedCubeRegion.AddAttributeValue(key);
                                    }
                                }

                                if (componentTypeCurr == "Dimension")
                                {
                                    if (includeCube)
                                    {
                                        artefact.IncludedCubeRegion.AddKeyValue(key);
                                    }
                                    else
                                    {
                                        artefact.ExcludedCubeRegion.AddKeyValue(key);
                                    }
                                }
                            }
                            includeCube = dataReader.GetSafeBool("CUBE_REGION_KEY_VALUE_INCLUDE");
                            keyValueIDCurr = valueIDCurr;
                            componentTypeCurr = dataReader["COMPONENT_TYPE"].ToString();

                            key = new KeyValuesMutableImpl();
                        }

                        if (string.IsNullOrEmpty(key.Id))
                        {
                            key.Id = dataReader["MEMBER_ID"].ToString();
                        }

                        var startPeriod = dataReader["START_PERIOD"].ToString();
                        var endPeriod = dataReader["END_PERIOD"].ToString();

                        bool includeStart = Convert.ToBoolean(dataReader["START_INCLUSIVE"]);
                        bool includeEnd = Convert.ToBoolean(dataReader["END_INCLUSIVE"]);

                        if (!string.IsNullOrWhiteSpace(startPeriod) || !string.IsNullOrWhiteSpace(endPeriod)) {
                            key.TimeRange = new TimeRangeMutableCore();
                            if (!string.IsNullOrWhiteSpace(startPeriod) && string.IsNullOrWhiteSpace(endPeriod))
                            {
                                key.TimeRange.StartDate = new SdmxDateCore(startPeriod);
                                key.TimeRange.IsStartInclusive = includeStart;
                            }
                            else if (string.IsNullOrWhiteSpace(startPeriod) && !string.IsNullOrWhiteSpace(endPeriod))
                            {
                                key.TimeRange.EndDate = new SdmxDateCore(endPeriod);
                                key.TimeRange.IsEndInclusive = includeEnd;
                            }
                            else {
                                key.TimeRange.StartDate = new SdmxDateCore(startPeriod);
                                key.TimeRange.IsStartInclusive = includeStart;
                                key.TimeRange.EndDate = new SdmxDateCore(endPeriod);
                                key.TimeRange.IsEndInclusive = includeEnd;
                                key.TimeRange.IsRange = true;
                            }
                        }
                        else 
                        {
                            string memberValue = dataReader["MEMBER_VALUE"].ToString();
                            key.AddValue(memberValue);
                            bool isCascadeValue = dataReader.GetSafeBool("CUBE_REGION_VALUE_CASCADE_VALUES");
                            if (isCascadeValue)
                            {
                                key.AddCascade(memberValue);
                            }
                        }
                    }

                    if (componentTypeCurr == "Attribute")
                    {
                        if (includeCube)
                        {
                            artefact.IncludedCubeRegion.AddAttributeValue(key);
                        }
                        else
                        {
                            artefact.ExcludedCubeRegion.AddAttributeValue(key);
                        }
                    }

                    if (componentTypeCurr == "Dimension")
                    {
                        if (includeCube)
                        {
                            artefact.IncludedCubeRegion.AddKeyValue(key);
                        }
                        else
                        {
                            artefact.ExcludedCubeRegion.AddKeyValue(key);
                        }
                    }
                }
            }
        }
    }
}
