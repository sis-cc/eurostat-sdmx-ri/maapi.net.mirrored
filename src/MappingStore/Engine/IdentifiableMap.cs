// -----------------------------------------------------------------------
// <copyright file="IdentifiableMap.cs" company="EUROSTAT">
//   Date Created : --
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Map that holds information linking database id with item and parent item id
    /// </summary>
    public class IdentifiableMap
    {
        private readonly Dictionary<long,Tuple<string,string>> _valuesFromDb = new Dictionary<long, Tuple<string, string>>();
        private readonly Dictionary<string,Tuple<long,string>> _dictionary = new Dictionary<string, Tuple<long, string>>();
        private readonly Dictionary<string,long> _oldDictionary = new Dictionary<string, long>();
        private readonly Dictionary<long, List<long>> _hierarchy = new Dictionary<long, List<long>>();
        private readonly SdmxStructureType _sdmxStructure;

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentifiableMap" /> class.
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        public IdentifiableMap(SdmxStructureType sdmxStructure)
        {
            _sdmxStructure = sdmxStructure;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sysId"></param>
        /// <param name="itemId"></param>
        /// <param name="parentItemId"></param>
        public void Add(long sysId, string itemId, string parentItemId)
        {
            _valuesFromDb.Add(sysId, new Tuple<string, string>(itemId, parentItemId));

            //if (_sdmxStructure.EnumType != SdmxStructureEnumType.Category || !parentItemId.HasValue)
            //{
            //    return;
            //}

            //if (!_hierarchy.ContainsKey(parentItemId.Value))
            //{
            //    _hierarchy.Add(parentItemId.Value, new List<long>() { sysId });
            //}
            //else
            //{
            //    _hierarchy[parentItemId.Value].Add(sysId);
            //}
        }


        /// <summary>
        /// Converts this instance.
        /// </summary>
        public void Convert()
        {
            if (_sdmxStructure.EnumType != SdmxStructureEnumType.Category)
            {
                foreach (var row in _valuesFromDb)
                {
                    AddItem(row.Value.Item1, row.Key, row.Value.Item2);
                }
            }
            else
            {
                foreach (var row in _valuesFromDb)
                {
                    var categoryId = string.Empty;
                    if (string.IsNullOrEmpty(row.Value.Item2))
                    {
                        categoryId = row.Value.Item1;
                    }
                    else
                    {
                        categoryId = row.Value.Item2 + "." + row.Value.Item1;
                    }
                    
                    AddItem(categoryId, row.Key, row.Value.Item2);
                }
            }
            //if (_sdmxStructure.EnumType != SdmxStructureEnumType.Category)
            //{
            //    foreach (var row in _valuesFromDb)
            //    {
            //        AddItem(row.Value.Item1, row.Key, row.Value.Item2 != null ? _valuesFromDb[row.Value.Item2.Value].Item1 : null);
            //    }
            //}
            //else
            //{
            //    foreach (var row in _valuesFromDb.Where(row => row.Value.Item2 == null))
            //    {
            //        // Add root level
            //        AddItem(row.Value.Item1, row.Key, null);

            //        if (!_hierarchy.ContainsKey(row.Key))
            //        {
            //            continue;
            //        }

            //        foreach (var childSysId in _hierarchy[row.Key])
            //        {
            //            AddChild(childSysId, row.Value.Item1);
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Determines whether [contains item identifier] [the specified item identifier].
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        /// <returns>
        ///   <c>true</c> if [contains item identifier] [the specified item identifier]; otherwise, <c>false</c>.
        /// </returns>
        public bool ContainsItemId(string itemId)
        {
            return _dictionary.ContainsKey(itemId);
        }

        /// <summary>
        /// Determines whether [has parent item identifier] [the specified item identifier].
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        /// <param name="parentItemId">The parent item identifier.</param>
        /// <returns>
        ///   <c>true</c> if [has parent item identifier] [the specified item identifier]; otherwise, <c>false</c>.
        /// </returns>
        public bool HasParentItemId(string itemId,string parentItemId)
        {
            if (!_dictionary.ContainsKey(itemId))
            {
                return false;
            }
            return !string.Equals(_dictionary[itemId].Item2, parentItemId, StringComparison.Ordinal);
        }

        public Dictionary<string, long> Dictionary => _oldDictionary;

        private void AddChild(long sysId, string parentItemId)
        {
            var newItemId = $"{parentItemId}.{_valuesFromDb[sysId].Item1}";

            AddItem(newItemId, sysId, parentItemId);

            if (!_hierarchy.ContainsKey(sysId))
            {
                return;
            }

            foreach (var childSysId in _hierarchy[sysId])
            {
                AddChild(childSysId, newItemId);
            }
        }

        private void AddItem(string itemId, long sysId, string parentItemId)
        {
            _dictionary.Add(itemId, new Tuple<long, string>(sysId, parentItemId));
            _oldDictionary.Add(itemId, sysId);
        }
    }


}