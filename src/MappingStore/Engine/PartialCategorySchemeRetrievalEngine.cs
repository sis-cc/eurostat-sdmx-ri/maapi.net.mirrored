// -----------------------------------------------------------------------
// <copyright file="PartialCategorySchemeRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2022-12-13
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using System;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model;
    using System.Collections.Generic;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    /// The engine to retrieve partial concept schemes
    /// </summary>
    internal class PartialCategorySchemeRetrievalEngine : CategorySchemeRetrievalEngine
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PartialConceptSchemeRetrievalEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="mappingStoreDb" /> is null
        /// </exception>
        public PartialCategorySchemeRetrievalEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
        }

        /// <summary>
        /// Retrieves the items of the category scheme that are used by the categorisations in the <paramref name="structureQuery"/>.
        /// </summary>
        /// <param name="artefactPkPair">the Category Scheme with PK.</param>
        /// <param name="structureQuery">The artefact querying for the partial category scheme, and other info.</param>
        /// <returns>An <see cref="ICategorySchemeMutableObject"/> filled with the retrieved partial items.</returns>
        protected override ICategorySchemeMutableObject RetrieveDetails(
            MaintainableWithPrimaryKey<ICategorySchemeMutableObject> artefactPkPair, ICommonStructureQuery structureQuery)
        {
            IList<IComplexIdentifiableReferenceObject> childReferences = new List<IComplexIdentifiableReferenceObject>();

            if (structureQuery is IPartialCategorySchemeStructureQuery partialQuery)
            {
                // create a list of distinct category items in the category scheme that are referenced by any categorisation
                childReferences = partialQuery.Categorisations
                    .Select(c => c.CategoryReference)
                    .Where(c => c.MaintainableId.Equals(artefactPkPair.Maintainable.Id)) // only for the category scheme given
                    .Distinct()
                    .Select(c => ComplexIdentifiableReferenceCore.CreateForRest(c.FullId, c.TargetReference)) // create child reference
                    .ToList();
            }

            // if not partial, will retrieve all items
            return RetrieveDetails(artefactPkPair, childReferences);
        }
    }
}
