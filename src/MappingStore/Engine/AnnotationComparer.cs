// -----------------------------------------------------------------------
// <copyright file="AnnotationComparer.cs" company="EUROSTAT">
//   Date Created : 2017-01-25
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    internal class AnnotationComparer
    {
        public bool IsEqual(IAnnotationMutableObject item, IComplexAnnotationReference annotationWhere)
        {
            return this.CheckComplexTextReference(item.Title, annotationWhere.TitleReference) &&
                   this.CheckComplexTextReference(item.Type, annotationWhere.TypeReference) &&
                   this.CheckTextRefernce(item.Text, annotationWhere.TextReference);
        }

        private bool CheckComplexTextReference(string valueToCompare, IComplexTextReference complexTextReference)
        {
            if (complexTextReference == null)
            {
                return true;
            }

            switch (complexTextReference.Operator.EnumType)
            {
                case TextSearchEnumType.Equal:
                    return valueToCompare.Equals(complexTextReference.SearchParameter, StringComparison.OrdinalIgnoreCase);
                case TextSearchEnumType.NotEqual:
                    return !valueToCompare.Equals(complexTextReference.SearchParameter, StringComparison.OrdinalIgnoreCase);
                case TextSearchEnumType.Contains:
                    return valueToCompare.Contains(complexTextReference.SearchParameter);
                case TextSearchEnumType.DoesNotContain:
                    return !valueToCompare.Contains(complexTextReference.SearchParameter);
                case TextSearchEnumType.EndsWith:
                    return valueToCompare.EndsWith(complexTextReference.SearchParameter, StringComparison.OrdinalIgnoreCase);
                case TextSearchEnumType.DoesNotEndWith:
                    return !valueToCompare.EndsWith(complexTextReference.SearchParameter, StringComparison.OrdinalIgnoreCase);
                case TextSearchEnumType.StartsWith:
                    return valueToCompare.StartsWith(complexTextReference.SearchParameter, StringComparison.OrdinalIgnoreCase);
                case TextSearchEnumType.DoesNotStartWith:
                    return !valueToCompare.StartsWith(complexTextReference.SearchParameter, StringComparison.OrdinalIgnoreCase);
                default:
                    return false;
            }
        }

        private bool CheckTextRefernce(IList<ITextTypeWrapperMutableObject> text, IComplexTextReference textReference)
        {
            if (text == null || text.Count == 0)
            {
                if (textReference == null)
                {
                    return true;
                }
                return false;
            }
            return text.Any(x => this.CheckComplexTextReference(x.Value, textReference) && x.Locale == textReference.Language);
        }
    }
}