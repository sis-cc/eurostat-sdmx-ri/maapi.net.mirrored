// -----------------------------------------------------------------------
// <copyright file="HeaderRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading;

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Text;

    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;
    using Estat.Sri.SdmxXmlConstants;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     An engine class that retrieves SDMX Header information from Mapping Store
    /// </summary>
    public class HeaderRetrieverEngine : IHeaderRetrieverEngine
    {
        /// <summary>
        ///     Mapping store localized text type Department
        /// </summary>
        public const string DepartmentText = "Department";

        /// <summary>
        ///     Mapping store localized text type Name
        /// </summary>
        public const string NameText = "Name";

        /// <summary>
        ///     The receiver text.
        /// </summary>
        public const string ReceiverText = "Receiver";

        /// <summary>
        ///     Mapping store localized text type Role
        /// </summary>
        public const string RoleText = "Role";

        /// <summary>
        ///     The sender text.
        /// </summary>
        public const string SenderText = "Sender";

        /// <summary>
        ///     Mapping store localized text type Source
        /// </summary>
        public const string SourceText = "Source";

        /// <summary>
        ///     The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(HeaderRetrieverEngine));

        /// <summary>
        ///     The _mapping store DB.
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        /// The last header Primary key <c>HEADER.HEADER_ID</c>.
        /// </summary>
        
        private static AsyncLocal<long> _lastHeaderSysId= new AsyncLocal<long>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="HeaderRetrieverEngine" /> class.
        /// </summary>
        /// <param name="settings">
        ///     The connection string settings to mapping store
        /// </param>
        public HeaderRetrieverEngine(ConnectionStringSettings settings)
            : this(new Database(settings))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="HeaderRetrieverEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        public HeaderRetrieverEngine(Database mappingStoreDb)
        {
            this._mappingStoreDb = mappingStoreDb;
        }

        /// <summary>
        /// Gets the last header Primary key <c>HEADER.HEADER_ID</c>.
        /// </summary>
        public static long LastHeaderSysId
        {
            get
            {
                return _lastHeaderSysId.Value;
            }
        }

        /// <summary>
        ///     This method queries the mapping store for header information for a specific dataflow
        /// </summary>
        /// <param name="dataQuery">
        ///     The <see cref="IDataQuery" /> object
        /// </param>
        /// <param name="beginDate">
        ///     For ReportingBegin element
        /// </param>
        /// <param name="endDate">
        ///     For ReportingEnd element
        /// </param>
        /// <returns>
        ///     A <see cref="IHeader" /> object. Otherwise null
        /// </returns>
        public IHeader GetHeader(IBaseDataQuery dataQuery, DateTime? beginDate, DateTime? endDate)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            return this.GetHeader(dataQuery.Dataflow, beginDate, endDate);
        }

        /// <summary>
        ///     This method queries the mapping store for header information for a specific dataflow
        /// </summary>
        /// <param name="dataflow">
        ///     The <see cref="IDataflowObject" /> object
        /// </param>
        /// <param name="beginDate">
        ///     For ReportingBegin element
        /// </param>
        /// <param name="endDate">
        ///     For ReportingEnd element
        /// </param>
        /// <returns>
        ///     A <see cref="IHeader" /> object. Otherwise null
        /// </returns>
        public IHeader GetHeader(IMaintainableObject dataflow, DateTime? beginDate, DateTime? endDate)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException("dataflow");
            }

            var dataflowId = dataflow.Id;
            var dataflowVersion = dataflow.Version;
            var dataflowAgency = dataflow.AgencyId;

            return this.GetHeader(
                beginDate, 
                endDate, 
                new MaintainableRefObjectImpl(dataflowAgency, dataflowId, dataflowVersion));
        }

        /// <summary>
        ///     This method queries the mapping store for header information for a specific dataflow
        /// </summary>
        /// <param name="dataflow">
        ///     The <see cref="DataflowEntity" /> object
        /// </param>
        /// <param name="beginDate">
        ///     For ReportingBegin element
        /// </param>
        /// <param name="endDate">
        ///     For ReportingEnd element
        /// </param>
        /// <returns>
        ///     A <see cref="IHeader" /> object. Otherwise null
        /// </returns>
        [Obsolete("Use other overloads")]
        public IHeader GetHeader(ArtefactEntity dataflow, DateTime? beginDate, DateTime? endDate)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException("dataflow");
            }

            var dataflowId = dataflow.Id;
            var dataflowVersion = dataflow.Version;
            var dataflowAgency = dataflow.Agency;

            return this.GetHeader(
                beginDate, 
                endDate, 
                new MaintainableRefObjectImpl(dataflowAgency, dataflowId, dataflowVersion));
        }


        /// <summary>
        ///     This method populates the contact details of a <see cref="IContactMutableObject" />
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The <see cref="Database" /> instance for Mapping Store database
        /// </param>
        /// <param name="contactSysId">
        ///     The contact system identifier. In the database the column CONTACT.CONTACT_ID
        /// </param>
        /// <param name="contact">
        ///     The <see cref="IContactMutableObject" /> for which the details will be populated
        /// </param>
        private static void PopulateContactDetails(
            Database mappingStoreDb, 
            long contactSysId, 
            IContactMutableObject contact)
        {
            string paramId = mappingStoreDb.BuildParameterName(ParameterNameConstants.IdParameter);

            var sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT CD.CD_ID, CD.CONTACT_ID, CD.TYPE, CD.VALUE ");
            sqlCommand.Append("FROM CONTACT_DETAIL CD ");
            sqlCommand.AppendFormat("WHERE CD.CONTACT_ID = {0} ", paramId);

            using (DbCommand command = mappingStoreDb.GetSqlStringCommand(sqlCommand.ToString()))
            {
                mappingStoreDb.AddInParameter(command, ParameterNameConstants.IdParameter, DbType.Int64, contactSysId);

                using (IDataReader dataReader = mappingStoreDb.ExecuteReader(command))
                {
                    while (dataReader.Read())
                    {
                        string coordinateType = DataReaderHelper.GetString(dataReader, "TYPE");
                        string coordinateData = DataReaderHelper.GetString(dataReader, "VALUE");

                        contact.AddCoordinateType(coordinateType, coordinateData);
                    }
                }
            }
        }

        /// <summary>
        ///     This method populates the Localized Strings (Names, Departments, Roles)
        ///     of a <see cref="IContactMutableObject" />
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The <see cref="Database" /> instance for Mapping Store database
        /// </param>
        /// <param name="contactSysId">
        ///     The contact system identifier. In the database the column CONTACT.CONTACT_ID
        /// </param>
        /// <param name="contact">
        ///     The <see cref="IContactMutableObject" /> to be populated in terms of Names, Departments, Roles
        /// </param>
        private static void PopulateContactLocalisedStrings(
            Database mappingStoreDb, 
            long contactSysId, 
            IContactMutableObject contact)
        {
            string paramId = mappingStoreDb.BuildParameterName(ParameterNameConstants.IdParameter);

            var sqlCommand = new StringBuilder();
            sqlCommand.Append(
                "SELECT HLS.HLS_ID, HLS.TYPE, HLS.HEADER_ID, HLS.PARTY_ID, HLS.CONTACT_ID, HLS.LANGUAGE, HLS.TEXT ");
            sqlCommand.Append("FROM HEADER_LOCALISED_STRING HLS ");
            sqlCommand.AppendFormat("WHERE HLS.CONTACT_ID = {0} ", paramId);

            using (DbCommand command = mappingStoreDb.GetSqlStringCommand(sqlCommand.ToString()))
            {
                mappingStoreDb.AddInParameter(command, ParameterNameConstants.IdParameter, DbType.Int64, contactSysId);

                using (IDataReader dataReader = mappingStoreDb.ExecuteReader(command))
                {
                    while (dataReader.Read())
                    {
                        var text = new TextTypeWrapperMutableCore
                                       {
                                           Locale =
                                               DataReaderHelper.GetString(
                                                   dataReader, 
                                                   "LANGUAGE"), 
                                           Value =
                                               DataReaderHelper.GetString(dataReader, "TEXT")
                                       };
                        if (!string.IsNullOrWhiteSpace(text.Value) && !string.IsNullOrWhiteSpace(text.Locale))
                        {
                            string textType = DataReaderHelper.GetString(dataReader, "TYPE");

                            if (textType.Equals(NameText, StringComparison.OrdinalIgnoreCase))
                            {
                                contact.Names.Add(text);
                            }
                            else if (textType.Equals(DepartmentText, StringComparison.OrdinalIgnoreCase))
                            {
                                contact.Departments.Add(text);
                            }
                            else if (textType.Equals(RoleText, StringComparison.OrdinalIgnoreCase))
                            {
                                contact.Roles.Add(text);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     This method populates the Localized Strings (Names, Sources) of a <see cref="IHeader" />
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The <see cref="Database" /> instance for Mapping Store database
        /// </param>
        /// <param name="headerSysId">
        ///     The header system identifier. In the database the column HEADER.HEADER_ID
        /// </param>
        /// <param name="header">
        ///     The <see cref="IHeader" /> to be populated in terms of Names and Sources
        /// </param>
        private static void PopulateHeaderLocalisedStrings(Database mappingStoreDb, long headerSysId, IHeader header)
        {
            string paramId = mappingStoreDb.BuildParameterName(ParameterNameConstants.IdParameter);

            var sqlCommand = new StringBuilder();
            sqlCommand.Append(
                "SELECT HLS.HLS_ID, HLS.TYPE, HLS.HEADER_ID, HLS.PARTY_ID, HLS.CONTACT_ID, HLS.LANGUAGE, HLS.TEXT ");
            sqlCommand.Append("FROM HEADER_LOCALISED_STRING HLS ");
            sqlCommand.AppendFormat("WHERE HLS.HEADER_ID = {0} ", paramId);

            using (DbCommand command = mappingStoreDb.GetSqlStringCommand(sqlCommand.ToString()))
            {
                mappingStoreDb.AddInParameter(command, ParameterNameConstants.IdParameter, DbType.Int64, headerSysId);

                using (IDataReader dataReader = mappingStoreDb.ExecuteReader(command))
                {
                    while (dataReader.Read())
                    {
                        var text = new TextTypeWrapperMutableCore
                                       {
                                           Locale =
                                               DataReaderHelper.GetString(
                                                   dataReader, 
                                                   "LANGUAGE"), 
                                           Value =
                                               DataReaderHelper.GetString(dataReader, "TEXT")
                                       };
                        string textType = DataReaderHelper.GetString(dataReader, "TYPE");

                        // is it a sender or a receiver?
                        if (textType.Equals(NameText, StringComparison.OrdinalIgnoreCase))
                        {
                            header.AddName(new TextTypeWrapperImpl(text, null));
                        }
                        else
                        {
                            if (textType.Equals(SourceText, StringComparison.OrdinalIgnoreCase))
                            {
                                header.AddSource(new TextTypeWrapperImpl(text, null));
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     This method populates the Contacts.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The <see cref="Database" /> instance for Mapping Store database
        /// </param>
        /// <param name="partySysId">
        ///     The party system identifier. In the database the column PARTY.PARTY_ID
        /// </param>
        /// <param name="contacts">
        ///     The list of contacts to be populated i
        /// </param>
        private static void PopulatePartyContacts(
            Database mappingStoreDb, 
            long partySysId, 
            ICollection<IContact> contacts)
        {
            string paramId = mappingStoreDb.BuildParameterName(ParameterNameConstants.IdParameter);

            var sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT CONTACT.CONTACT_ID, CONTACT.PARTY_ID ");
            sqlCommand.Append("FROM CONTACT ");
            sqlCommand.AppendFormat("WHERE CONTACT.PARTY_ID = {0} ", paramId);

            using (DbCommand command = mappingStoreDb.GetSqlStringCommand(sqlCommand.ToString()))
            {
                mappingStoreDb.AddInParameter(command, ParameterNameConstants.IdParameter, DbType.Int64, partySysId);

                using (IDataReader dataReader = mappingStoreDb.ExecuteReader(command))
                {
                    while (dataReader.Read())
                    {
                        var contact = new ContactMutableObjectCore();
                        long contactSysId = DataReaderHelper.GetInt64(dataReader, "CONTACT_ID");

                        PopulateContactLocalisedStrings(mappingStoreDb, contactSysId, contact);
                        PopulateContactDetails(mappingStoreDb, contactSysId, contact);

                        contacts.Add(new ContactCore(contact));
                    }
                }
            }
        }

        /// <summary>
        ///     This method populates the international strings
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The <see cref="Database" /> instance for Mapping Store database
        /// </param>
        /// <param name="partySysId">
        ///     The party system identifier. In the database the column PARTY.PARTY_ID
        /// </param>
        /// <param name="partyNames">
        ///     The <see cref="ITextTypeWrapper" />lists  to be populated in terms of Names
        /// </param>
        private static void PopulatePartyLocalisedStrings(
            Database mappingStoreDb, 
            long partySysId, 
            ICollection<ITextTypeWrapper> partyNames)
        {
            string paramId = mappingStoreDb.BuildParameterName(ParameterNameConstants.IdParameter);

            var sqlCommand = new StringBuilder();
            sqlCommand.Append(
                "SELECT HLS.HLS_ID, HLS.TYPE, HLS.HEADER_ID, HLS.PARTY_ID, HLS.CONTACT_ID, HLS.LANGUAGE, HLS.TEXT ");
            sqlCommand.Append("FROM HEADER_LOCALISED_STRING HLS ");
            sqlCommand.AppendFormat("WHERE HLS.PARTY_ID = {0} ", paramId);

            using (DbCommand command = mappingStoreDb.GetSqlStringCommand(sqlCommand.ToString()))
            {
                mappingStoreDb.AddInParameter(command, ParameterNameConstants.IdParameter, DbType.Int64, partySysId);

                using (IDataReader dataReader = mappingStoreDb.ExecuteReader(command))
                {
                    while (dataReader.Read())
                    {
                        var text = new TextTypeWrapperMutableCore
                                       {
                                           Locale =
                                               DataReaderHelper.GetString(
                                                   dataReader, 
                                                   "LANGUAGE"), 
                                           Value =
                                               DataReaderHelper.GetString(dataReader, "TEXT")
                                       };
                        string textType = DataReaderHelper.GetString(dataReader, "TYPE");

                        // is it a sender or a receiver?
                        if (textType.Equals(NameText, StringComparison.OrdinalIgnoreCase))
                        {
                            partyNames.Add(new TextTypeWrapperImpl(text, null));
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     This method populates the Senders and Receivers of a <see cref="IHeader" />
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The <see cref="Database" /> instance for Mapping Store database
        /// </param>
        /// <param name="headerSysId">
        ///     The header system identifier. In the database the column HEADER.HEADER_ID
        /// </param>
        /// <param name="header">
        ///     The <see cref="IHeader" /> to be populated in terms of Senders and Receivers
        /// </param>
        private static void PoulateHeaderSendersAndReceivers(Database mappingStoreDb, long headerSysId, IHeader header)
        {
            string paramId = mappingStoreDb.BuildParameterName(ParameterNameConstants.IdParameter);

            var sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT PARTY.PARTY_ID, PARTY.ID, PARTY.HEADER_ID, PARTY.TYPE ");
            sqlCommand.Append("FROM PARTY ");
            sqlCommand.AppendFormat("WHERE PARTY.HEADER_ID = {0} ", paramId);

            using (DbCommand command = mappingStoreDb.GetSqlStringCommand(sqlCommand.ToString()))
            {
                mappingStoreDb.AddInParameter(command, ParameterNameConstants.IdParameter, DbType.Int64, headerSysId);

                using (IDataReader dataReader = mappingStoreDb.ExecuteReader(command))
                {
                    while (dataReader.Read())
                    {
                        var id = DataReaderHelper.GetString(dataReader, "ID");

                        var partyId = ValidationUtil.CleanAndValidateId(id, true);

                        if (!id.Equals(partyId, StringComparison.OrdinalIgnoreCase))
                        {
                            _log.Warn($"Invalid partyId: '{id}' replaced with: '{partyId}'");
                        }

                        long partySysId = DataReaderHelper.GetInt64(dataReader, "PARTY_ID");
                        string partyType = DataReaderHelper.GetString(dataReader, "TYPE");

                        var names = new List<ITextTypeWrapper>();
                        PopulatePartyLocalisedStrings(mappingStoreDb, partySysId, names);

                        var contacts = new List<IContact>();
                        PopulatePartyContacts(mappingStoreDb, partySysId, contacts);

                        var party = new PartyCore(names, partyId, contacts, null);

                        // is it a sender or a receiver?
                        if (partyType.Equals(SenderText, StringComparison.OrdinalIgnoreCase))
                        {
                            header.Sender = party;
                        }
                        else if (partyType.Equals(ReceiverText, StringComparison.OrdinalIgnoreCase))
                        {
                            header.AddReciever(party);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     This method queries the mapping store for header information for a specific dataflow
        /// </summary>
        /// <param name="beginDate">For ReportingBegin element</param>
        /// <param name="endDate">For ReportingEnd element</param>
        /// <param name="dataflowReference">The dataflow reference.</param>
        /// <returns>
        ///     A <see cref="IHeader" /> object. Otherwise null
        /// </returns>
        private IHeader GetHeader(DateTime? beginDate, DateTime? endDate, IMaintainableRefObject dataflowReference)
        {
            long headerSysId;
            string paramId = this._mappingStoreDb.BuildParameterName(ParameterNameConstants.IdParameter);
            string version1 = this._mappingStoreDb.BuildParameterName(ParameterNameConstants.VersionParameter1);
            string version2 = this._mappingStoreDb.BuildParameterName(ParameterNameConstants.VersionParameter2);
            string version3 = this._mappingStoreDb.BuildParameterName(ParameterNameConstants.VersionParameter3);
            string agency = this._mappingStoreDb.BuildParameterName(ParameterNameConstants.AgencyParameter);

            var sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT HD.ENTITY_ID, HD.TEST, HD.DATASET_AGENCY, HD.STRUCTURE_TYPE, HD.DATASET_ID, HD.DATASET_ACTION ");
            sqlCommand.Append("FROM N_HEADER HD, DATAFLOW DF, N_ARTEFACT ART ");
            sqlCommand.Append("INNER JOIN ARTEFACT_BASE AB ON AB.ART_BASE_ID = ART.ART_BASE_ID ");
            sqlCommand.Append("INNER JOIN ENTITY_SDMX_REF ESR on ESR.TARGET_ARTEFACT = AB.ART_BASE_ID ");
            sqlCommand.Append("WHERE HD.ENTITY_ID = ESR.SOURCE_ENTITY_ID ");
            sqlCommand.Append("AND DF.DF_ID = ART.ART_ID ");
            sqlCommand.AppendFormat("AND AB.ID = {0} ", paramId);
            sqlCommand.AppendFormat(
                "AND ART.Version1 ={0} AND ART.Version2 = {1} AND (((ART.Version3 is null) and ({2} is null)) or (((ART.Version3 is not null) and ({2} is not null)) and ( ART.Version3 = {2} ))) ", 
                version1, 
                version2, 
                version3);
            sqlCommand.AppendFormat("AND AB.AGENCY = {0} ", agency);

            IDictionary<string, string> additionalAttributes = new Dictionary<string, string>(StringComparer.Ordinal);
            bool test;
            DateTime currentDate = DateTime.Now;
            var dataflowId = dataflowReference.MaintainableId;
            var dataflowAgency = dataflowReference.AgencyId;
            var version = dataflowReference.SplitVersion(3);
            using (DbCommand command = this._mappingStoreDb.GetSqlStringCommand(sqlCommand.ToString()))
            {
                this._mappingStoreDb.AddInParameter(
                    command, 
                    ParameterNameConstants.IdParameter, 
                    DbType.AnsiString, 
                    dataflowId);
                this._mappingStoreDb.AddInParameter(
                    command, 
                    ParameterNameConstants.VersionParameter1, 
                    DbType.Int64, 
                    version[0].HasValue ? version[0].Value : 0);
                this._mappingStoreDb.AddInParameter(
                    command, 
                    ParameterNameConstants.VersionParameter2, 
                    DbType.Int64, 
                    version[1].HasValue ? version[1].Value : 0);
                this._mappingStoreDb.AddInParameter(
                    command, 
                    ParameterNameConstants.VersionParameter3, 
                    DbType.Int64, 
                    version[2].HasValue ? version[2].Value : -1);
                this._mappingStoreDb.AddInParameter(
                    command, 
                    ParameterNameConstants.AgencyParameter, 
                    DbType.AnsiString, 
                    dataflowAgency);

                using (IDataReader dataReader = this._mappingStoreDb.ExecuteReader(command))
                {
                    // we expect only 1 record here
                    if (dataReader.Read())
                    {
                        headerSysId = DataReaderHelper.GetInt64(dataReader, "ENTITY_ID");
                        test = DataReaderHelper.GetBoolean(dataReader, "TEST");
                        additionalAttributes.Add(
                            "DataSetAgency", 
                            DataReaderHelper.GetString(dataReader, "DATASET_AGENCY"));
                        additionalAttributes.Add(
                            nameof(ElementNameTable.StructureUsage),
                            DataReaderHelper.GetString(dataReader, "STRUCTURE_TYPE"));
                        additionalAttributes.Add(
                            nameof(ElementNameTable.DataSetID),
                            DataReaderHelper.GetString(dataReader, "DATASET_ID"));
                        additionalAttributes.Add(
                            nameof(ElementNameTable.DataSetAction),
                            DataReaderHelper.GetString(dataReader, "DATASET_ACTION"));
                        _log.DebugFormat(
                            CultureInfo.InvariantCulture, 
                            "Found header information in mapping store for Dataflow {0}", 
                            dataflowId);
                    }
                    else
                    {
                        _log.DebugFormat(
                            CultureInfo.InvariantCulture, 
                            "No header information found in mapping store for Dataflow {0}", 
                            dataflowId);
                        return null;
                    }
                }
            }

            // DatasetAction: Information (case that is response to a query)
            DatasetAction datasetAction = DatasetAction.GetAction("Information");
            if (additionalAttributes.ContainsKey(nameof(ElementNameTable.DataSetAction)))
            {
                //override value with db value if present and not null
                var datasetActionInDb = additionalAttributes[nameof(ElementNameTable.DataSetAction)];
                if (!string.IsNullOrEmpty(datasetActionInDb))
                {
                    datasetAction = DatasetAction.GetAction(datasetActionInDb);
                }
            }
            
            

            IHeader ret = new HeaderImpl(
                additionalAttributes, 
                null, 
                null, 
                datasetAction, 
                dataflowId, 
                null, 
                null, 
                currentDate, 
                currentDate, 
                beginDate, 
                endDate, 
                null, 
                null, 
                null, 
                null, 
                test);

            PopulateHeaderLocalisedStrings(this._mappingStoreDb, headerSysId, ret);
            PoulateHeaderSendersAndReceivers(this._mappingStoreDb, headerSysId, ret);
            _lastHeaderSysId.Value = headerSysId;
            return ret;
        }
    }
}