// -----------------------------------------------------------------------
// <copyright file="ProcessConstraintsEngine.cs" company="EUROSTAT">
//   Date Created : 2018-07-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Data.Common;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model.StoredProcedure;
    using Estat.Sri.Utils.Helper;

    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    class ProcessConstraintsEngine
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ProcessConstraintsEngine));

        /// <summary>
        ///     The _stored procedures
        /// </summary>
        private static readonly StoredProcedures _storedProcedures;

        /// <summary>
        ///     Initializes static members of the <see cref="ProcessConstraintsEngine" /> class.
        /// </summary>
        static ProcessConstraintsEngine()
        {
            _storedProcedures = new StoredProcedures();
        }

        private Database mappingStoreDb;

        public ProcessConstraintsEngine(Database mappingStoreDb)
        {
            this.mappingStoreDb = mappingStoreDb;
        }

        public bool StartProcessing(IStructureReference requestedStructureRef, long codelistSysId)
        {
            this.CreateTempTable();

            bool isCodelistPartial = false;

            ProcessConstraintsProcedure processConstraintsProcedure = _storedProcedures.ProcessConstraintsProcedure;

            using (DbCommand command = processConstraintsProcedure.
                CreateCommand(mappingStoreDb.Transaction.Connection, mappingStoreDb.Transaction))
            {
                // TODO: this should be configurable from the ws.
                string constraintType = "ALLOWED";
                processConstraintsProcedure.CreateParameters(command, requestedStructureRef, codelistSysId, constraintType);

                command.ExecuteNonQueryAndLog();

                isCodelistPartial = Convert.ToBoolean(command.Parameters[ProcessConstraintsProcedure.IsClPartialParameterName].Value);
            }
            return isCodelistPartial;
        }

        public void EndProcessing()
        {
            this.DropTempTable();
        }

        private void CreateTempTable()
        {
            if (this.mappingStoreDb.ProviderName == MappingStoreDefaultConstants.SqlServerProvider)
            {
                string createTableSql = @" CREATE TABLE {0} (
                                           COMP_ID VARCHAR(50) NOT NULL,
                                           CODE_SYS_ID BIGINT NOT NULL,
                                           CODE_ID VARCHAR(255) NOT NULL,
                                           PARENT_CODE_SYS_ID BIGINT,
                                           INCLUDED BIT DEFAULT 0
                                           PRIMARY KEY(COMP_ID, CODE_SYS_ID)
                                        ) ";
                this.mappingStoreDb.UsingLogger().ExecuteNonQuery(String.Format(createTableSql, ConstrainedCodeListConstant.constrainedCodelistSqlTable), null);
            }
            else if (this.mappingStoreDb.ProviderName == MappingStoreDefaultConstants.MySqlProvider)
            {
                string createTableSql = @" CREATE TEMPORARY TABLE {0} (
                                           COMP_ID VARCHAR(50) NOT NULL,
                                           CODE_SYS_ID BIGINT NOT NULL,
                                           CODE_ID VARCHAR(255) NOT NULL,
                                           PARENT_CODE_SYS_ID BIGINT,
                                           INCLUDED BIT DEFAULT 0,
		                                   PRIMARY KEY PK_TEMP_PROCESS_CONSTRAINTS_TABLE(COMP_ID, CODE_SYS_ID)
                                        ) ";
                this.mappingStoreDb.UsingLogger().ExecuteNonQuery(String.Format(createTableSql, ConstrainedCodeListConstant.constrainedCodelistTable), null);
            }
        }

        private void DropTempTable()
        {
            if (this.mappingStoreDb.ProviderName == MappingStoreDefaultConstants.SqlServerProvider)
            {
                string dropTableSql = @" IF OBJECT_ID('tempdb..{0}') IS NOT NULL DROP TABLE {0} ";

                this.mappingStoreDb.UsingLogger().ExecuteNonQuery(String.Format(dropTableSql, ConstrainedCodeListConstant.constrainedCodelistSqlTable), null);
            }
            else if (this.mappingStoreDb.ProviderName == MappingStoreDefaultConstants.MySqlProvider)
            {
                string dropTableSql = "DROP TEMPORARY TABLE IF EXISTS {0} ";

                this.mappingStoreDb.UsingLogger().ExecuteNonQuery(String.Format(dropTableSql, ConstrainedCodeListConstant.constrainedCodelistTable), null);
            }
        }
    }
}
