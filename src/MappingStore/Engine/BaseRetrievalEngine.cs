// -----------------------------------------------------------------------
// <copyright file="BaseRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Data;
    using Estat.Sri.MappingStoreRetrieval.Helper;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    /// <summary>
    ///     The base retrieval engine from Mapping store.
    /// </summary>
    public abstract class BaseRetrievalEngine
    {
        /// <summary>
        ///     The default URI.
        /// </summary>
        private static readonly Uri _defaultUri = new Uri("http://need/to/changeit");

        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(BaseRetrievalEngine));

        /// <summary>
        ///     Gets the default URI.
        /// </summary>
        protected static Uri DefaultUri
        {
            get
            {
                return _defaultUri;
            }
        }

        /// <summary>
        ///     Read the localized string from <paramref name="dataReader" />
        /// </summary>
        /// <param name="item">
        ///     The <see cref="INameableMutableObject" /> .
        /// </param>
        /// <param name="isNameIdx">
        ///     The <c>LOCALISED_STRING.TYPE</c> ordinal
        /// </param>
        /// <param name="txtIdx">
        ///     The <c>LOCALISED_STRING.TEXT</c> ordinal
        /// </param>
        /// <param name="langIdx">
        ///     The <c>LOCALISED_STRING.LANGUAGE</c> ordinal
        /// </param>
        /// <param name="dataReader">
        ///     The MASTORE DB <see cref="IDataReader" />
        /// </param>
        /// <param name="detail">The Structure Query Detail</param>
        protected static void ReadLocalisedString(
            INameableMutableObject item,
            int isNameIdx,
            int txtIdx,
            int langIdx,
            IDataRecord dataReader,
            ComplexStructureQueryDetailEnumType detail = ComplexStructureQueryDetailEnumType.Full)
        {
            // TODO support SDMX-ML Query detail CompleteStub versus Stub when Common API supports it. 
            // When it is stub then only name should be returned. 
            // When it is complete stub both name and description.
            // Now we use StructureQueryDetail which is for REST queries only.
            // According to the http://sdmx.org/wp-content/uploads/2012/05/SDMX_2_1-SECTION_07_WebServicesGuidelines_May2012.pdf
            // page 10, footnotes 10-12 REST AllStubs == SDMX-ML Query Stub so in that case we skip description
            var text = DataReaderHelper.GetString(dataReader, txtIdx);
            var lang = DataReaderHelper.GetString(dataReader, langIdx);

            // optional name & description because there are cases of structure type
            // that are nameable in SDMX 2.0, identifiable in 2.1 and gone in 3.0.0
            // but the situation is not clear-cut and depends on the artefact
            // So in case we have name/description use it.
            if (!string.IsNullOrEmpty(text))
            {
                // The IS_NAME column is of type byte.
                // C# will convert the byte to bool by returning true for any non-0 value.
                // Which means that in case of any invalid entry > 1, it will return true, instead of an error.
                bool isName = DataReaderHelper.GetBoolean(dataReader, isNameIdx);
                if (isName)
                {
                    item.AddName(lang, text);
                }
                else
                {
                    //if the item is stub, then it should not add the description as
                    //only identification and name should be present
                    if (detail != ComplexStructureQueryDetailEnumType.Stub)
                    {
                        item.AddDescription(lang, text);
                    }
                }
            }
            else if (item.Names.Count == 0)
            {
                item.AddName("en", item.Id);
            }
        }
    }
}