// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     The concept scheme retrieval engine.
    /// </summary>
    public class ConceptSchemeRetrievalEngine :
        ItemSchemeRetrieverEngine<IConceptSchemeMutableObject, IConceptMutableObject>
    {
        private readonly TextFormatRetriever _textFormatRetriever;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeRetrievalEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="mappingStoreDb" /> is null
        /// </exception>
        public ConceptSchemeRetrievalEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
            this._textFormatRetriever = new TextFormatRetriever(mappingStoreDb);
        }

        /// <summary>
        /// The Concept Scheme
        /// </summary>
        protected override SdmxStructureType ItemSchemeStructureType => SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme);

        /// <inheritdoc/>
        protected override void AddItem(IConceptMutableObject item, string parentItemPath, IConceptSchemeMutableObject itemScheme, Dictionary<string, IConceptMutableObject> itemPathMap)
        {
            item.ParentConcept = parentItemPath.Equals(string.Empty) ? null : parentItemPath;
            itemScheme.AddItem(item);
        }

        /// <summary>
        ///     Create a new instance of <see cref="IConceptSchemeMutableObject" />.
        /// </summary>
        /// <returns>
        ///     The <see cref="IConceptSchemeMutableObject" />.
        /// </returns>
        protected override IConceptSchemeMutableObject CreateArtefact()
        {
            return new ConceptSchemeMutableCore();
        }

        /// <summary>
        ///     Create an item.
        /// </summary>
        /// <returns>
        ///     The <see cref="IConceptMutableObject" />
        /// </returns>
        protected override IConceptMutableObject CreateItem()
        {
            return new ConceptMutableCore();
        }

        /// <summary>
        /// Retrieves details for Text Format items and Coded Representation items.
        /// </summary>
        /// <param name="artefactPkPair">The Concept Scheme to retrieve details for.</param>
        /// <param name="itemPathMap">the items.</param>
        protected override void RetrieveItemDetails(
            MaintainableWithPrimaryKey<IConceptSchemeMutableObject> artefactPkPair,
            Dictionary<string, IConceptMutableObject> itemPathMap)
        {
            // retrieve text format info
            RetrieveTextFormatForConcepts(artefactPkPair, itemPathMap);

            // retrieve enumeration info
            RetrieveCodedRepresentation(artefactPkPair, itemPathMap);
        }

        private void RetrieveCodedRepresentation(
            MaintainableWithPrimaryKey<IConceptSchemeMutableObject> artefactPkPair,
            Dictionary<string, IConceptMutableObject> itemPathMap)
        {
            foreach (ReferenceToOtherArtefact reference in artefactPkPair.References)
            {
                if (reference.HasSubStructure && RefTypes.Enumeration.Equals(reference.ReferenceType))
                {
                    // in case for partial retrieval; not all references may be included in the itemMapPath
                    if (itemPathMap.TryGetValue(reference.SubStructureFullPath, out IConceptMutableObject conceptMutableBean) &&
                        conceptMutableBean != null)
                    {
                        if (conceptMutableBean.CoreRepresentation == null)
                        {
                            conceptMutableBean.CoreRepresentation = new RepresentationMutableCore();
                        }
                        conceptMutableBean.CoreRepresentation.Representation = reference.Target;
                    }
                }
            }
        }

        private void RetrieveTextFormatForConcepts(
            MaintainableWithPrimaryKey<IConceptSchemeMutableObject> artefactPkPair,
            Dictionary<string, IConceptMutableObject> itemPathMap)
        {
            Dictionary<string, ITextFormatMutableObject> textFormats = _textFormatRetriever.Retrieve(artefactPkPair.PrimaryKey);
            foreach (var textFormat in textFormats)
            {
                IConceptMutableObject conceptMutableBean = itemPathMap[textFormat.Key];
                if (conceptMutableBean != null)
                {
                    if (conceptMutableBean.CoreRepresentation == null)
                    {
                        conceptMutableBean.CoreRepresentation = new RepresentationMutableCore();
                    }
                    conceptMutableBean.CoreRepresentation.TextFormat = textFormat.Value;
                }
            }
        }
    }
}