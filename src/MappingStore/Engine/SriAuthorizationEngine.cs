// -----------------------------------------------------------------------
// <copyright file="SriAuthorizationEngine.cs" company="EUROSTAT">
//   Date Created : 2017-05-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Security.Principal;
    using System.Text.RegularExpressions;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// The Mapping Store authorization engine
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Engine.IDataflowAuthorizationEngine" />
    public class SriAuthorizationEngine : IDataflowAuthorizationEngine
    {
        /// <summary>
        /// The principal
        /// </summary>
        private readonly IPrincipal _principal;

        /// <summary>
        /// The urn retriever
        /// </summary>
        private readonly Func<SdmxStructureType, string, IDictionary<long, string>> _urnRetriever;

        /// <summary>
        /// The retrieval engine
        /// </summary>
        private readonly IRetrievalEngine<ICategorisationMutableObject> _retrievalEngine;

        /// <summary>
        /// The user categories synchronize object
        /// </summary>
        private readonly object _userCategoriesSyncObject = new object();

        /// <summary>
        /// The dataflow categories synchronize object
        /// </summary>
        private readonly object _dataflowCategoriesSyncObject = new object();

        /// <summary>
        /// The user categories
        /// </summary>
        private ReadOnlyCollection<IStructureReference> _userCategories;

        /// <summary>
        /// The user dataflows
        /// </summary>
        private ICollection<string> _userDataflows;

        /// <summary>
        /// Initializes a new instance of the <see cref="SriAuthorizationEngine" /> class.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <param name="urnRetriever">The urn retriever.</param>
        /// <param name="retrievalEngine">The retrieval engine.</param>
        public SriAuthorizationEngine(IPrincipal principal, Func<SdmxStructureType, string, IDictionary<long, string>> urnRetriever, IRetrievalEngine<ICategorisationMutableObject> retrievalEngine)
        {
            this._principal = principal;
            this._urnRetriever = urnRetriever;
            this._retrievalEngine = retrievalEngine;
        }

        /// <summary>
        /// Gets the authorization.
        /// </summary>
        /// <param name="dataflowReference">The dataflow reference.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sdmxsource.Extension.Constant.AuthorizationStatus"/>.
        /// </returns>
        public AuthorizationStatus GetAuthorization(IMaintainableRefObject dataflowReference)
        {
            // This is used only for data queries TODO rename method to be more clear
            var dataflowStructureReference = new StructureReferenceImpl(dataflowReference, SdmxStructureEnumType.Dataflow);
            return this.GetAuthorization(dataflowStructureReference, PermissionType.CanReadData);
        }

        /// <summary>
        /// Gets the authorization.
        /// </summary>
        /// <param name="structureReferences">The structure references.</param><param name="permission">The permission.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sdmxsource.Extension.Constant.AuthorizationStatus"/>.
        /// </returns>
        public AuthorizationStatus GetAuthorization(IEnumerable<IStructureReference> structureReferences, PermissionType permission)
        {
            // TODO rename method this is for checking if we can access an entity. The structure references are actually entities
            if (!this._principal.IsInRole(nameof(PermissionType.CanPerformInternalMappingConfig)))
            {
                return AuthorizationStatus.Denied;
            }

            return structureReferences.Any(reference => this.GetAuthorization(reference, permission) == AuthorizationStatus.Authorized) ? AuthorizationStatus.Authorized : AuthorizationStatus.Denied;
        }

        /// <summary>
        /// Gets the authorization.
        /// </summary>
        /// <param name="structureReference">The structure reference.</param><param name="permission">The permission.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sdmxsource.Extension.Constant.AuthorizationStatus"/>.
        /// </returns>
        public AuthorizationStatus GetAuthorization(IStructureReference structureReference, PermissionType permission)
        {
            if (!this._principal.IsInRole(permission.ToString()))
            {
                return AuthorizationStatus.Denied;
            }

            using (var scope = new SdmxAuthorizationScope(Mapping.Api.Constant.Authorisation.Skip))
            {
                if (structureReference.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.Dataflow)
                {
                    var dataflowUrn = this.RetrieveUserDataflows();
                    if (UrnContainsMaintainable(dataflowUrn, structureReference))
                    {
                        return AuthorizationStatus.Authorized;
                    }
                }

                var userCategories = this.RetrieveUserCategoriesAsStructure();

                IEnumerable<IStructureReference> requestedCategories;
                if (structureReference.TargetReference.EnumType == SdmxStructureEnumType.Category)
                {
                    requestedCategories = new[] { structureReference };
                }
                else
                {
                    Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common.ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                       .NewQuery(CommonStructureQueryType.Other, null)
                       .SetStructureIdentification(structureReference)
                       .SetRequestedDetail(ComplexStructureQueryDetailEnumType.Full)         
                       .Build();

                    requestedCategories = this._retrievalEngine.Retrieve(structureQuery).Select(o => o.CategoryReference);
                }

                var structureReferences =
                    from categorisationCategory in requestedCategories
                    from userCategory in userCategories
                    where categorisationCategory.IsCategoryChildOf(userCategory)
                    select categorisationCategory;

                if (structureReferences.Any())
                {
                    return AuthorizationStatus.Authorized;
                }
            }
            return AuthorizationStatus.Denied;
        }

        /// <summary>
        /// Checks if a <paramref name="structureReference"/> can be matched against the provided <paramref name="urn"/>.
        /// Allows the use of wildcards (SDMX REST 2.0)
        /// </summary>
        /// <param name="urn">A collection of urns to search into.</param>
        /// <param name="structureReference">The maintainable to search for.</param>
        /// <returns><c>true</c> if match found, <c>false</c> otherwise.</returns>
        private bool UrnContainsMaintainable(ICollection<string> urn, IStructureReference structureReference)
        {
            // The urn & the structureReference.MaintainableUrn.ToString() has the same format: {agencyID}:{resourceID}({version})
            // The difference is that while urn contains all actuall structures in the above format,
            // each of the parameters of structureReference.MaintainableUrn can either be a literal or one of the wildcards: *(any), ~(latest), +(latest stable)
            // For example *:NAMAIN_IDC_N(+) represents the latest stable version of NAMAIN_IDC_N for any agency.

            // In order to search for matches in the list of urn, a regex patten is build that will be able to match the wildcards against any literal.
            // For example the *:NAMAIN_IDC_N(+) MaintainableUrn should be matched with any of the urn that has the NAMAIN_IDC_N resource, with any agencyID and any version.
            // Note that a resource with any version specified will be matched with any of the version wildcards (*, ~, +). Meaning that if there is one or more versions,
            // this or these are also the 'latest' or the 'latest stable', or ofcourse 'any'.
            // So ESTAT:NAMAIN_IDC_N(1.9) will be matched with all three MaintainableUrn; ESTAT:NAMAIN_IDC_N(*), ESTAT:NAMAIN_IDC_N(~), ESTAT:NAMAIN_IDC_N(+)

            // escape regex key-characters 
            // the following part will escape all the characters in the structureReference.MaintainableUrn that are treated as special characters in regex.
            // Those characters should be taken literally in the search for matches.
            var setupRegex = new Regex(@"[.()]");
            MatchEvaluator evaluator = match => { return $"\\{match.Value}"; };
            string pattern = setupRegex.Replace(structureReference.MaintainableUrn.ToString(), evaluator);

            // replace SDMX REST 2.0 wildcards
            // In order to be able to match a wildcard to any literal in the urn, each wildcard found in the structureReference.MaintainableUrn
            // is expressed in the regex pattern as any character repeated more than once.
            var substitutionRegex = new Regex(@"[*~+]");
            pattern = substitutionRegex.Replace(pattern, @".+");

            // a regex object is created with the resulted pattern and a search is started until a match is found.
            var matchRegex = new Regex(pattern);
            foreach (string input in urn)
            {
                if (matchRegex.IsMatch(input))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the list of authorized dataflows.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Collections.Generic.IList`1"/>.
        /// </returns>
        public IList<IMaintainableRefObject> RetrieveAuthorizedDataflows()
        {
            using (var scope = new SdmxAuthorizationScope(Mapping.Api.Constant.Authorisation.Skip))
            {
                var dataflowRefs = this.RetrieveUserDataflows().Select(pair => new StructureReferenceImpl(pair)).Cast<IMaintainableRefObject>().ToList();

                //TODO: how can I tell the StructureOutputFormat ?
                var structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                    .SetMaintainableTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation))
                    .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                    .Build();
                var categorisations = this._retrievalEngine.Retrieve(structureQuery).Where(o => o.StructureReference.TargetReference.EnumType == SdmxStructureEnumType.Dataflow);

                var userCategories = this.RetrieveUserCategoriesAsStructure();
                var structureReferences = from categorisation in categorisations from userCategory in userCategories where categorisation.CategoryReference.IsCategoryChildOf(userCategory) select categorisation.StructureReference;
                dataflowRefs.AddRange(structureReferences);

                return dataflowRefs;
            }
        }

        /// <summary>
        /// Retrieves the user categories as <see cref="IStructureReference"/>.
        /// </summary>
        /// <returns>An array as <see cref="IStructureReference"/> pointing to the user categories</returns>
        private ReadOnlyCollection<IStructureReference> RetrieveUserCategoriesAsStructure()
        {
            if (this._userCategories == null)
            {
                lock (this._userCategoriesSyncObject)
                {
                    if (this._userCategories == null)
                    {
                        this._userCategories = new ReadOnlyCollection<IStructureReference>(this.RetrieveUserCategories().Values.Select(s => new StructureReferenceImpl(s)).Cast<IStructureReference>().ToArray());
                    }
                }
            }

            return this._userCategories;
        }

        /// <summary>
        /// Retrieves the user dataflows.
        /// </summary>
        /// <returns>The user dataflows</returns>
        private ICollection<string> RetrieveUserDataflows()
        {
            // TODO use IReadonlyCollection when we switch to .NET 4.5
            if (this._userDataflows == null)
            {
                lock (this._dataflowCategoriesSyncObject)
                {
                    if (this._userDataflows == null)
                    {
                        this._userDataflows = new HashSet<string>(this._urnRetriever(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow), this._principal.Identity.Name).Values, StringComparer.Ordinal);
                    }
                }
            }

            return this._userDataflows;
        }

        /// <summary>
        /// Retrieves the user categories.
        /// </summary>
        /// <returns>The user categories</returns>
        private IDictionary<long, string> RetrieveUserCategories()
        {
            return this._urnRetriever(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category), this._principal.Identity.Name);
        }
    }
}