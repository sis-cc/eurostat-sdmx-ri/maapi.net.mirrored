// -----------------------------------------------------------------------
// <copyright file="PartialItemSchemeRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2022-12-13
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Sdmx.MappingStore.Retrieve.Engine
{
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    internal class PartialItemSchemeRetrievalEngine
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(PartialConceptSchemeRetrievalEngine));

        /// <summary>
        ///     This field holds the Mapping Store Database object.
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        ///     The command builder for retrieving partial conceptschemes.
        /// </summary>
        private readonly PartialItemCommandBuilder _partialItemCommandBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="PartialItemSchemeRetrievalEngine" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        ///     The mapping store DB.
        /// </param>
        public PartialItemSchemeRetrievalEngine(Database mappingStoreDb)
        {
            _mappingStoreDb = mappingStoreDb;
            this._partialItemCommandBuilder = new PartialItemCommandBuilder(mappingStoreDb);
        }

        /// <summary>
        /// Retrieves the item Ids of the <paramref name="itemSchemePK"/> that are used by the parent in the <paramref name="structureQuery"/>.
        /// </summary>
        /// <param name="itemSchemePK"></param>
        /// <param name="structureQuery"></param>
        /// <param name="childType"></param>
        /// <returns></returns>
        internal IList<IComplexIdentifiableReferenceObject> RetrieveUsedItemIds(long itemSchemePK, ICommonStructureQuery structureQuery, SdmxStructureEnumType childType)
        {
            // the parent reference
            IStructureReference parentRef = new StructureReferenceImpl()
            {
                MaintainableId = structureQuery.MaintainableIds.First(),
                AgencyId = structureQuery.AgencyIds.First(),
                Version = structureQuery.VersionRequests.First().ToString(),
                MaintainableStructureEnumType = structureQuery.MaintainableTarget
            };

            // get the item ids that are used by the parent
            List<string> itemIdsUsed = new List<string>();
            using (DbCommand command = _partialItemCommandBuilder.BuildForItemIds(itemSchemePK, parentRef))
            {
                _log.InfoFormat(CultureInfo.InvariantCulture, "Executing query for partial {0}: {1}", childType, command.CommandText);

                using (IDataReader dataReader = _mappingStoreDb.ExecuteReader(command))
                {
                    int idIdx = dataReader.GetOrdinal("ID");
                    while (dataReader.Read())
                    {
                        itemIdsUsed.Add(DataReaderHelper.GetString(dataReader, idIdx));
                    }
                }
            }

            var itemType = SdmxStructureType.GetFromEnum(childType);
            return itemIdsUsed
                .Select(id => ComplexIdentifiableReferenceCore.CreateForRest(id, itemType))
                .ToList();
        }
    }
}
