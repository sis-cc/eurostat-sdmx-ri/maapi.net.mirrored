// -----------------------------------------------------------------------
// <copyright file="CodeListRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System.Collections.Generic;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;

    /// <summary>
    /// The engine for retrieving codelists.
    /// </summary>
    public class CodeListRetrievalEngine : ItemSchemeRetrieverEngine<ICodelistMutableObject, ICodeMutableObject>
    {
        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="mappingStore">The mastore db connection.</param>
        public CodeListRetrievalEngine(Database mappingStore)
            : base(mappingStore)
        {
        }

        /// <summary>
        ///     Create a new instance of <see cref="ICodelistMutableObject" />.
        /// </summary>
        /// <returns>
        ///     The <see cref="ICodelistMutableObject" />.
        /// </returns>
        protected override ICodelistMutableObject CreateArtefact()
        {
            return new CodelistMutableCore();
        }

        /// <summary>
        ///     Create an item.
        /// </summary>
        /// <returns>
        ///     The <see cref="ICodeMutableObject" />.
        /// </returns>
        protected override ICodeMutableObject CreateItem()
        {
            return new CodeMutableCore();
        }

        /// <summary>
        /// The codelist SDMX type
        /// </summary>
        protected override SdmxStructureType ItemSchemeStructureType => SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList);

        /// <inheritdoc/>
        protected override void AddItem(
            ICodeMutableObject item, string parentItemPath, ICodelistMutableObject itemScheme, Dictionary<string, ICodeMutableObject> itemPathMap)
        {
            item.ParentCode = parentItemPath.Equals(string.Empty) ? null : parentItemPath;
            itemScheme.AddItem(item);
        }
    }
}
