// -----------------------------------------------------------------------
// <copyright file="MappingAssistantUserRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-05-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The mapping assistant user retriever.
    /// </summary>
    public class MappingAssistantUserRetriever
    {
        /// <summary>
        /// The ma user password query
        /// </summary>
        private const string MaUserPasswordQuery = "select SALT, PASSWORD, ALGORITHM from MA_USER where USERNAME={0}";

        /// <summary>
        /// The ma user query
        /// </summary>
        private const string MaUserQuery = "select mu.USER_ID, mu.USER_TYPE from MA_USER mu where mu.USERNAME={0}";

        /// <summary>
        /// The name
        /// </summary>
        private static readonly string _name = typeof(MappingAssistantUser).Name;

        /// <summary>
        /// The hash builder
        /// </summary>
        private readonly HashBuilder _hashBuilder;

        /// <summary>
        /// The bytes converter
        /// </summary>
        private readonly BytesConverter _bytesConverter;

        /// <summary>
        /// The database
        /// </summary>
        private readonly Database _database;

        /// <summary>
        /// The store identifier
        /// </summary>
        private readonly string _storeId;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingAssistantUserRetriever" /> class.
        /// </summary>
        /// <param name="hashBuilder">The hash builder.</param>
        /// <param name="bytesConverter">The bytes converter.</param>
        /// <param name="database">The database.</param>
        /// <param name="storeId">The store identifier.</param>
        public MappingAssistantUserRetriever(HashBuilder hashBuilder, BytesConverter bytesConverter, Database database, string storeId)
        {
            this._hashBuilder = hashBuilder;
            this._bytesConverter = bytesConverter;
            this._database = database;
            this._storeId = storeId;
        }

        /// <summary>
        /// Gets the principal.
        /// </summary>
        /// <param name="userName">
        /// Name of the user.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// userName is null
        /// </exception>
        /// <returns>
        /// The <see cref="IPrincipal"/>.
        /// </returns>
        public IPrincipal GetPrincipal(string userName, string password)
        {
            if (userName == null)
            {
                throw new ArgumentNullException("userName");
            }

            if (this.CheckPassword(userName, password))
            {
                var principal = this.RetrievePrincipal(userName);
                if (principal != null)
                {
                    return principal;
                }
            }

            throw ThrowInvalidUserPass();
        }

        /// <summary>
        /// Throws the invalid user pass.
        /// </summary>
        /// <returns>A <see cref="SdmxUnauthorisedException"/></returns>
        private static Exception ThrowInvalidUserPass()
        {
            return new SdmxUnauthorisedException("Invalid user name and/or password. Or the account has been disabled");
        }

        /// <summary>
        /// Retrieves the principal.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>Gets the principal</returns>
        private IPrincipal RetrievePrincipal(string userName)
        {
            var parameter = this.CreateParameter(userName);
            using (var cmd = this._database.GetSqlStringCommandFormat(MaUserQuery, parameter))
            using (var reader = this._database.ExecuteReader(cmd))
            {
                var userIdIdx = reader.GetOrdinal("USER_ID");
                var userTypeIdx = reader.GetOrdinal("USER_TYPE");

                if (reader.Read())
                {
                    var userId = reader.GetInt64(userIdIdx);
                    var userType = reader.GetInteger(userTypeIdx);
                    if (userType > 0)
                    {
                        PermissionType permission = (PermissionType)userType;
                        return new MappingAssistantUser(new UserCoordinates(userId, this._storeId), new GenericIdentity(userName, _name), permission);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Checks the password.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="providedPassword">The provided password.</param>
        /// <returns>True if the user and password match</returns>
        private bool CheckPassword(string userName, string providedPassword)
        {
            string pass;
            string algorithm;
            string salt;
            var parameter = this.CreateParameter(userName);
            using (var cmd = this._database.GetSqlStringCommandFormat(MaUserPasswordQuery, parameter))
            using (var reader = this._database.ExecuteReader(cmd))
            {
                if (reader.Read())
                {
                    pass = reader.GetSafeString("PASSWORD");
                    salt = reader.GetSafeString("SALT");
                    algorithm = reader.GetSafeString("ALGORITHM");
                }
                else
                {
                    return false;
                }
            }

            // no password
            if (string.IsNullOrEmpty(pass) && string.IsNullOrEmpty(salt) && string.IsNullOrEmpty(providedPassword))
            {
                return true;
            }

            if (string.IsNullOrEmpty(pass))
            {
                return false;
            }

            byte[] saltBytes;
            if (string.IsNullOrEmpty(salt))
            {
                saltBytes = new byte[0];
            }
            else
            {
                saltBytes = this._bytesConverter.ConvertFromHex(salt);
            }

            if (string.IsNullOrEmpty(salt) && string.IsNullOrEmpty(algorithm))
            {
                algorithm = "MD5"; // Backwards compatibility with old Mapping Assistant
            }
            else
            {
                algorithm = algorithm ?? ConfigManager.Config.DefaultHashAlgorithm;
            }

            var providedPasswordBytes = this._hashBuilder.Build(providedPassword, saltBytes, algorithm);
            var providedPasswordHex = this._bytesConverter.ConvertToHex(providedPasswordBytes);
            return string.Equals(providedPasswordHex, pass);
        }

        /// <summary>
        /// Creates the parameter.
        /// </summary>
        /// <param name="userName">
        /// Name of the user.
        /// </param>
        /// <returns>
        /// The <see cref="DbParameter"/>.
        /// </returns>
        private DbParameter CreateParameter(string userName)
        {
            var parameter = this._database.CreateInParameter("user_param", DbType.AnsiString, userName);
            return parameter;
        }
    }
}