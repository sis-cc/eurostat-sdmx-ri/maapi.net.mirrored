// -----------------------------------------------------------------------
// <copyright file="MetadataAttributeRetriever.cs" company="EUROSTAT">
//   Date Created : 2017-04-18
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.Sdmx.MappingStore.Retrieve;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// Class MetadataAttributeRetriever.
    /// </summary>
    internal class MetadataAttributeRetriever
    {
        /// <summary>
        /// The mapping store database
        /// </summary>
        private readonly Database _mappingStoreDb;

        /// <summary>
        /// The text format retriever
        /// </summary>
        private readonly TextFormatRetriever _textFormatRetriever;

        /// <summary>
        /// The component annotation retriever engine
        /// </summary>
        private readonly IdentifiableAnnotationRetrieverEngine _componentAnnotationRetrieverEngine;

        /// <summary>
        /// The command builder
        /// </summary>
        private readonly MsdCommandBuilder _commandBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataAttributeRetriever" /> class.
        /// </summary>
        /// <param name="mappingStoreDb">The mapping store database.</param>
        /// <param name="commandBuilder">The command builder.</param>
        public MetadataAttributeRetriever(Database mappingStoreDb, MsdCommandBuilder commandBuilder)
        {
            this._mappingStoreDb = mappingStoreDb;
            this._textFormatRetriever = new TextFormatRetriever(this._mappingStoreDb);
            this._componentAnnotationRetrieverEngine = new IdentifiableAnnotationRetrieverEngine(mappingStoreDb, MsdConstant.MetadataAttributeTableInfo);
            this._commandBuilder = commandBuilder;
        }

        /// <summary>
        /// Retrieves the specified report structures.
        /// </summary>
        /// <param name="reportStructures">The report structures.</param>
        /// <param name="artefactPkPair">The parent artefact</param>
        /// <returns>A map between <c>METADATA_ATTRIBUTE.MDT_ATTR_ID</c> and the corresponding <see cref="IMetadataAttributeMutableObject"/></returns>
        /// <exception cref="MappingStoreException">Inconsistent state in Mapping Store database or bug. MetadataAttribute with negative MTD_ATTR_ID</exception>
        public Dictionary<long, IMetadataAttributeMutableObject> Retrieve(
            IDictionary<long, IReportStructureMutableObject> reportStructures,
            MaintainableWithPrimaryKey<IMetadataStructureDefinitionMutableObject> artefactPkPair)
        {
            Dictionary<long, IMetadataAttributeMutableObject> sysIdToObject = new Dictionary<long, IMetadataAttributeMutableObject>();
            Dictionary<string, IMetadataAttributeMutableObject> fullPathMap = new Dictionary<string, IMetadataAttributeMutableObject>();
            var parentSysIdChild = new List<KeyValuePair<string, IMetadataAttributeMutableObject>>();
            var parentSysId = artefactPkPair.PrimaryKey;

            using (var command = this._commandBuilder.GetAttributeCommand(parentSysId))
            using (var reader = this._mappingStoreDb.ExecuteReader(command))
            {
                while (reader.Read())
                {
                    var metadataAttribute = new MetadataAttributeMutableCore();

                    var sysId = reader.GetSafeInt64("MTD_ATTR_ID");
                    if (sysId < 0)
                    {
                        throw new MappingStoreException("Inconsistent state in Mapping Store database or bug. MetadataAttribute with negative MTD_ATTR_ID");
                    }

                    metadataAttribute.Id = DataReaderHelper.GetString(reader, "ID");
                    var parentId = DataReaderHelper.GetString(reader, "PARENT_PATH");
                    string fullPath;
                    if (!string.IsNullOrEmpty(parentId))
                    {
                        parentSysIdChild.Add(new KeyValuePair<string, IMetadataAttributeMutableObject>(parentId, metadataAttribute));
                        fullPath = $"{parentId}.{metadataAttribute.Id}";
                    }
                    else
                    {
                        AddToReportStructure(reportStructures, reader, metadataAttribute);
                        fullPath = metadataAttribute.Id;
                    }
                    fullPathMap.Put(fullPath, metadataAttribute);

                    metadataAttribute.Presentational = reader.GetTristate("IS_PRESENTATIONAL");
                    var maxOccurs = reader.GetSafeInt32("MAX_OCCURS");
                    if (maxOccurs > 0)
                    {
                        metadataAttribute.MaxOccurs = maxOccurs;
                    }

                    var minOccurs = reader.GetSafeInt32("MIN_OCCURS");
                    if (minOccurs >= 0)
                    {
                        metadataAttribute.MinOccurs = minOccurs;
                    }

                    sysIdToObject.Add(sysId, metadataAttribute);
                }
            }

            PopulateHierarchy(fullPathMap, parentSysIdChild);
            this.PopulateTextFormat(parentSysId, fullPathMap);
            foreach (var reportStructure in reportStructures)
            {
                this._componentAnnotationRetrieverEngine.RetrieveAnnotations(reportStructure.Key, sysIdToObject);
            }

            foreach (var entry in fullPathMap)
            {
                IMetadataAttributeMutableObject metadataAttribute = entry.Value;
                string fullPath = entry.Key;
                List<ReferenceToOtherArtefact> collect = artefactPkPair.References.Where(r => r.SubStructureFullPath.Equals(fullPath)).ToList();
                ReferenceToOtherArtefact conceptRef = collect.FirstOrDefault(r => RefTypes.ConceptIdentity.Equals(r.ReferenceType));
                if (conceptRef != null)
                {
                    metadataAttribute.ConceptRef = conceptRef.BuildSubStructureActualTarget(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept));
                }
                ReferenceToOtherArtefact enumeration = collect.FirstOrDefault(r => RefTypes.Enumeration.Equals(r.ReferenceType));
                if (enumeration != null)
                {
                    if (metadataAttribute.Representation == null)
                    {
                        metadataAttribute.Representation = new RepresentationMutableCore();
                    }
                    metadataAttribute.Representation.Representation = enumeration.Target;
                }
            }

            // TODO ? concepts and codelists

            return sysIdToObject;
        }

        /// <summary>
        /// Adds to report structure.
        /// </summary>
        /// <param name="reportStructures">The report structures.</param>
        /// <param name="reader">The reader.</param>
        /// <param name="metadataAttribute">The metadata attribute.</param>
        private static void AddToReportStructure(IDictionary<long, IReportStructureMutableObject> reportStructures, IDataReader reader, MetadataAttributeMutableCore metadataAttribute)
        {
            var reportStructureId = reader.GetSafeInt64("OTH_ID");

            IReportStructureMutableObject reportStructure;
            if (!reportStructures.TryGetValue(reportStructureId, out reportStructure))
            {
                throw new ArgumentException(ErrorMessages.CannotFindEntry + reportStructureId, "reportStructures");
            }

            reportStructure.MetadataAttributes.Add(metadataAttribute);
        }

        /// <summary>
        /// Populates the hierarchy.
        /// </summary>
        /// <param name="sysIdToObject">The system identifier to object.</param>
        /// <param name="parentSysIdChild">The parent system identifier child.</param>
        /// <exception cref="MappingStoreException">Could not find metadata attribute parent</exception>
        private static void PopulateHierarchy(Dictionary<string, IMetadataAttributeMutableObject> sysIdToObject, IEnumerable<KeyValuePair<string, IMetadataAttributeMutableObject>> parentSysIdChild)
        {
            foreach (var child in parentSysIdChild)
            {
                IMetadataAttributeMutableObject parent;
                if (!sysIdToObject.TryGetValue(child.Key, out parent))
                {
                    throw new MappingStoreException("Could not find metadata attribute parent");
                }

                parent.MetadataAttributes.Add(child.Value);
            }
        }

        /// <summary>
        ///     Retrieve the component representation reference.
        /// </summary>
        /// <param name="dataReader">
        ///     The data reader.
        /// </param>
        /// <returns>
        ///     The <see cref="RepresentationMutableCore" /> of the representation.
        /// </returns>
        private static RepresentationMutableCore RetrieveRepresentationReference(IDataRecord dataReader)
        {
            var codelist = DataReaderHelper.GetString(dataReader, "CODELIST_ID");
            if (!string.IsNullOrWhiteSpace(codelist))
            {
                var codelistAgency = DataReaderHelper.GetString(dataReader, "CODELIST_AGENCY");
                var codelistVersion = DataReaderHelper.GetString(dataReader, "CODELIST_VERSION");
                var codelistRepresentation = new StructureReferenceImpl(
                    codelistAgency,
                    codelist,
                    codelistVersion,
                    SdmxStructureEnumType.CodeList);
                return new RepresentationMutableCore { Representation = codelistRepresentation };
            }

            return null;
        }

        /// <summary>
        /// Retrieve the concept reference.
        /// </summary>
        /// <param name="dataReader">
        /// The data reader.
        /// </param>
        /// <returns>
        /// The <see cref="IStructureReference"/>.
        /// </returns>
        private static IStructureReference RetrieveConceptRef(IDataRecord dataReader)
        {
            var conceptRef = DataReaderHelper.GetString(dataReader, "CONCEPTREF");
            var conceptSchemeRef = DataReaderHelper.GetString(dataReader, "CONCEPTSCHEME_ID");
            var conceptSchemeVersion = DataReaderHelper.GetString(dataReader, "CONCEPT_VERSION");
            var conceptSchemeAgency = DataReaderHelper.GetString(dataReader, "CONCEPT_AGENCY");
            return new StructureReferenceImpl(conceptSchemeAgency, conceptSchemeRef, conceptSchemeVersion, SdmxStructureEnumType.Concept, conceptRef);
        }

        /// <summary>
        /// Populates the text format.
        /// </summary>
        /// <param name="parentSysId">The parent system identifier.</param>
        /// <param name="sysIdToObject">The system identifier to object.</param>
        /// <exception cref="MappingStoreException">Could not find metadata attribute with text format</exception>
        private void PopulateTextFormat(long parentSysId, Dictionary<string, IMetadataAttributeMutableObject> sysIdToObject)
        {
            var textFormats = this._textFormatRetriever.Retrieve(parentSysId);
            foreach (var textFormat in textFormats)
            {
                IMetadataAttributeMutableObject component;
                if (!sysIdToObject.TryGetValue(textFormat.Key, out component))
                {
                    //throw new MappingStoreException("Could not find metadata attribute with text format");
                    continue;
                }

                if (component.Representation == null)
                {
                    component.Representation = new RepresentationMutableCore();
                }

                component.Representation.TextFormat = textFormat.Value;
            }
        }
    }
}