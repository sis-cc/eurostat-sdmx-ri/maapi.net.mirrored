// -----------------------------------------------------------------------
// <copyright file="StructureSetRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2017-02-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
// initial implementation ISTAT
//
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Engine
{
    using System;
    using System.Data;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     The StructureSet engine.
    /// </summary>
    internal class StructureSetRetrievalEngine : ArtefactRetrieverEngine<IStructureSetMutableObject>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StructureSetRetrievalEngine"/> class.
        /// </summary>
        /// <param name="mappingStoreDb">
        /// The mapping store DB.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="mappingStoreDb"/> is null.
        /// </exception>
        public StructureSetRetrievalEngine(Database mappingStoreDb)
            : base(mappingStoreDb)
        {
        }

        /// <summary>
        ///     Create a new instance of <see cref="IStructureSetMutableObject" />.
        /// </summary>
        /// <returns>
        ///     The <see cref="IStructureSetMutableObject" />.
        /// </returns>
        protected override IStructureSetMutableObject CreateArtefact()
        {
            return new StructureSetMutableCore();
        }

        protected override IStructureSetMutableObject RetrieveDetails(
            MaintainableWithPrimaryKey<IStructureSetMutableObject> artefactPkPair, ICommonStructureQuery structureQuery)
        {
            var artefact = artefactPkPair.Maintainable;
            var sysId = artefactPkPair.PrimaryKey;

            PopulateCodeListMap(artefact, sysId);
            PopulateStructureMap(artefact, sysId);

            //when read from xml, if the structure set has no related structures it has an empty RelatedStructuresMutableBeanImpl. So we do the same for the retrieval from db
            //TODO implementation is missing in case there are related structures, since they are not put in the RelatedStructuresMutableBeanImpl
            artefact.RelatedStructures = new RelatedStructuresMutableCore();

            return artefact;
        }

        /// <summary>
        /// Gets the CLM item and reference.
        /// </summary>
        /// <param name="artefact">The artefact.</param>
        /// <param name="clm">The CLM.</param>
        /// <param name="clmID">The CLM identifier.</param>
        private void GetCLMItemAndReference(IStructureSetMutableObject artefact, ICodelistMapMutableObject clm, long clmID)
        {
            this.GetCodelistOrStructureMapCrossReference(clm, clmID);
            this.GetCodeListMapItems(clm, clmID);
            artefact.AddCodelistMap(clm);
        }

        /// <summary>
        /// Gets the code list map cross reference.
        /// </summary>
        /// <param name="clm">The CLM.</param>
        /// <param name="sysId">The system identifier.</param>
        private void GetCodelistOrStructureMapCrossReference(ISchemeMapMutableObject clm, long sysId)
        {
            var inParameter = MappingStoreDb.CreateInParameter(ParameterNameConstants.IdParameter, DbType.Int64, sysId);
            string urnClass;
            if(clm is IStructureMapMutableObject)
            {
                urnClass = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.StructureMapV30).UrnClass;
            }
            else if(clm is ICodelistMapMutableObject)
            {
                urnClass = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeListMap).UrnClass;
            }
            else
            {
                throw new ArgumentException("method can be used only for StructureMap and CodelistMap");
            }
            var urnClassParameter = MappingStoreDb.CreateInParameter(ParameterNameConstants.UrlClassParameter, DbType.AnsiString, urnClass);

            using (var command = MappingStoreDb.GetSqlStringCommandFormat(StructureSetConstant.SQL_CLM_OR_SM_REFERENCE, inParameter, urnClassParameter))
            {
                using (var dataReader = this.MappingStoreDb.ExecuteReader(command))
                {
                    int sysIdPos = dataReader.GetOrdinal("SOURCE_ARTEFACT");
                    int idPos = dataReader.GetOrdinal("ID");
                    int versionPos1 = dataReader.GetOrdinal("MAJOR");
                    int versionPos2 = dataReader.GetOrdinal("MINOR");
                    int versionPos3 = dataReader.GetOrdinal("PATCH");
                    int versionExtPos = dataReader.GetOrdinal("EXTENSION");
                    int agencyPos = dataReader.GetOrdinal("AGENCY");
                    int artefactTypePos = dataReader.GetOrdinal("ARTEFACT_TYPE");
                    int refTypePos = dataReader.GetOrdinal("REF_TYPE");

                    while (dataReader.Read())
                    {
                        string maintainableId = dataReader.GetString(idPos);
                        string agency = dataReader.GetString(agencyPos);
                        long version1 = dataReader.GetInt64(versionPos1);
                        long version2 = dataReader.GetInt64(versionPos2);
                        long version3 = dataReader.GetInt64(versionPos3);                        
                        string versionExtension = Helper.DataReaderHelper.GetString(dataReader, versionExtPos);
                        string artefactType = dataReader.GetString(artefactTypePos);
                        string refType = dataReader.GetString(refTypePos);
                        string version = BuildVersion(version1, version2, version3, versionExtension);

                        IStructureReference reference = new StructureReferenceImpl(agency, maintainableId, version, SdmxStructureType.ParseClass(artefactType), "");

                        if ("source".Equals(refType))
                        {
                            clm.SourceRef = reference;
                        }
                        else if ("target".Equals(refType))
                        {
                            clm.TargetRef = reference;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the code list map items.
        /// </summary>
        /// <param name="clm">The CLM.</param>
        /// <param name="sysId">The system identifier.</param>
        private void GetCodeListMapItems(ICodelistMapMutableObject clm, long sysId)
        {
            var inParameter = MappingStoreDb.CreateInParameter(ParameterNameConstants.IdParameter, DbType.Int64, sysId);

            using (var command = MappingStoreDb.GetSqlStringCommandFormat(StructureSetConstant.SQL_CLM_OR_SM_ITEM, inParameter))
            {
                using (var dataReader = this.MappingStoreDb.ExecuteReader(command))
                {
                    ItemMapMutableCore clmItem;

                    while (dataReader.Read())
                    {
                        clmItem = new ItemMapMutableCore();

                        clmItem.SourceId = dataReader["S_ID"].ToString();
                        clmItem.TargetId = dataReader["T_ID"].ToString();

                        clm.AddItem(clmItem);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the SM item and reference.
        /// </summary>
        /// <param name="artefact">The artefact.</param>
        /// <param name="sm">The SM.</param>
        /// <param name="smID">The SM identifier.</param>
        private void GetSMItemAndReference(IStructureSetMutableObject artefact, IStructureMapMutableObject sm, long smID)
        {
            GetCodelistOrStructureMapCrossReference(sm, smID);
            GetStructureMapItems(sm, smID);
            artefact.AddStructureMap(sm);
        }

        /// <summary>
        /// </summary>
        /// <param name="artefact">
        /// </param>
        /// <param name="sm">
        /// The sm.
        /// </param>
        /// <param name="sysId">
        /// </param>
        private void GetStructureMapItems(IStructureMapMutableObject sm, long sysId)
        {
            var inParameter = MappingStoreDb.CreateInParameter(ParameterNameConstants.IdParameter, DbType.Int64, sysId);

            using (var command = MappingStoreDb.GetSqlStringCommandFormat(StructureSetConstant.SQL_CLM_OR_SM_ITEM, inParameter))
            {
                using (var dataReader = this.MappingStoreDb.ExecuteReader(command))
                {
                    while (dataReader.Read())
                    {
                        var smComp = new ComponentMapMutableCore();

                        smComp.MapConceptRef = dataReader["S_ID"].ToString();
                        smComp.MapTargetConceptRef = dataReader["T_ID"].ToString();

                        sm.AddComponent(smComp);
                    }
                }
            }
        }

        /// <summary>
        /// Populates the code list map.
        /// </summary>
        /// <param name="artefact">The artefact.</param>
        /// <param name="sysId">The system identifier.</param>
        private void PopulateCodeListMap(IStructureSetMutableObject artefact, long sysId)
        {
            var codelistMapRetrievalEngine = new OtherNameableRetrieverEngine<CodelistMapMutableCore>(MappingStoreDb, SdmxStructureEnumType.CodeListMap);
            var itemMap = codelistMapRetrievalEngine.Retrieve(sysId);

            foreach (var codelistMapMutableObject in itemMap)
            {
                var clm = codelistMapMutableObject.Value;
                var currClm = codelistMapMutableObject.Key;

                this.GetCLMItemAndReference(artefact, clm, currClm);
            }
        }

        /// <summary>
        /// Populates the structure map.
        /// </summary>
        /// <param name="artefact">The artefact.</param>
        /// <param name="sysId">The system identifier.</param>
        private void PopulateStructureMap(IStructureSetMutableObject artefact, long sysId)
        {
            var structureMapRetrieverEngine = new OtherNameableRetrieverEngine<StructureMapMutableCore>(this.MappingStoreDb, SdmxStructureEnumType.StructureMap);
            var itemMap = structureMapRetrieverEngine.Retrieve(sysId);

            foreach (var structureMapMutableObject in itemMap)
            {
                this.GetSMItemAndReference(artefact, structureMapMutableObject.Value, structureMapMutableObject.Key);
            }
        }
    }
}