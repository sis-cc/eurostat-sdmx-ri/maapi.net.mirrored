using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

namespace Estat.Sri.StructureRetriever.Cache
{
    public class StructureQueryCacheKey
    {
        public IList<string> _agencyIds;

        public IList<string> _maintainableIds;
        public IList<IVersionRequest> _versionRequests;

        public SdmxStructureType _maintainableTarget;


    public StructureQueryCacheKey(IList<string> agencyIds, IList<string> maintainableIds, IList<IVersionRequest> versionRequests, SdmxStructureType maintainableTarget)
        {
            this._agencyIds = agencyIds;
            this._maintainableIds = maintainableIds;
            this._versionRequests = versionRequests;
            this._maintainableTarget = maintainableTarget;
        }

        public override int GetHashCode()
        {
            int versionsHash = -1;
            if (_versionRequests != null)
            {
                versionsHash = _versionRequests.Sum(x=>x.GetHashCode());
            }

            int hash = 397; // Prime number to start with

            // Combine hash codes of properties
            hash = hash * 397 + _agencyIds.Sum(x=>x.GetHashCode());
            hash = hash * 397 + (_maintainableIds != null ? _maintainableIds.Sum(x=>x.GetHashCode()):0);
            hash = hash * 397 + versionsHash;
            hash = hash * 397 + (_maintainableTarget != null ? _maintainableTarget.GetHashCode() : 0);
            
            return hash;
        }
        public override bool Equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (!(o is StructureQueryCacheKey)) {
                return false;
            }
            StructureQueryCacheKey other = (StructureQueryCacheKey)o;

            return this._agencyIds.OrderBy(x=>x).SequenceEqual(other._agencyIds.OrderBy(x=>x))
                && this._maintainableIds.OrderBy(x=>x).SequenceEqual(other._maintainableIds.OrderBy(x=>x))
                && Equals(this._maintainableTarget, other._maintainableTarget)
                //for the cache key, we care only about specific properties of VersionRequest when comparing equality
                && this._versionRequests.OrderBy(x => x).SequenceEqual(other._versionRequests.OrderBy(x => x));
        }

        public override string ToString()
        {
            var agencyString = _agencyIds.Any() ? _agencyIds.Aggregate((x, y) => x + "," + y).TrimEnd(',') : "";
            var idString = _maintainableIds.Any() ? _maintainableIds.Aggregate((x, y) => x + "," + y).TrimEnd(','):"";
            var versionsString = _versionRequests.Any() ? _versionRequests.Select(x => x.ToString()).Aggregate((x, y) => x + "," + y).TrimEnd(','):"";
            
            return "Agencies :" + agencyString +
                   "Ids : " + idString +
                   "Versions : " + versionsString +
                   "Structure : " + _maintainableTarget.ToString();
        }
    }
}
