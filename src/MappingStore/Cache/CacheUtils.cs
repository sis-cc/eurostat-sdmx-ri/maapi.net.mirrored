using System;
using System.Collections.Generic;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

namespace Estat.Sri.StructureRetriever.Cache
{
    public class CacheUtils
    {
        public static StructureQueryCacheKey GetKeyFromQuery(ICommonStructureQuery query)
        {
            return new StructureQueryCacheKey(query.AgencyIds, query.MaintainableIds, query.VersionRequests, query.MaintainableTarget);
        }

        public static StructureQueryCacheKey GetKeyFromMaintainable(IMaintainableMutableObject maintainable)
        {
            return new StructureQueryCacheKey(new[] { maintainable.AgencyId }, new[] { maintainable.Id },new[] { new VersionRequestCore(maintainable.Version) }, maintainable.StructureType);
        }

    }
}
