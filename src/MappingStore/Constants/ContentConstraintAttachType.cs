// -----------------------------------------------------------------------
// <copyright file="ContentConstraintAttachType.cs" company="EUROSTAT">
//   Date Created : 2019-07-10
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Constants
{
    /// <summary>
    /// An enumeration of the available Content Constraint attachments
    /// </summary>
    public enum ContentConstraintAttachType
    {
        /// <summary>
        /// Nothing. Not a valid option
        /// </summary>
        None,

        /// <summary>
        /// The DataProvider
        /// </summary>
        DataProvider = 1,

        /// <summary>
        /// The DataSet
        /// </summary>
        DataSet = 2,

        /// <summary>
        /// The MetaDataSet 
        /// </summary>
        MetadataSet = 3,

        /// <summary>
        /// The simple datasource attachment
        /// </summary>
        SimpleDataSource = 4,

        /// <summary>
        /// 
        /// </summary>
        DataStructure = 5,

        MetadataStructure = 6,

        Dataflow = 7,
        Metadataflow = 8,
        ProvisionAgreement = 9
    }
}