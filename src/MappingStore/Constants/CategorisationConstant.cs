// -----------------------------------------------------------------------
// <copyright file="CategorisationConstant.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Constants
{
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The categorisation constant.
    /// </summary>
    internal static class CategorisationConstant
    {
        //// TODO use ArtefactTypeSqlBuilder
        /// <summary>
        /// Gets the SQL Query format/template for retrieving the artefact reference from a categorisation id. Use with
        /// <see cref="string.Format(string,object)" /> and one parameter
        /// 1. the <see cref="ProductionWhereClause" /> or <see cref="string.Empty" />
        /// </summary>
        public const string ArtefactRefWithType = "SELECT D.DF_ID as SID, 'Dataflow' as STYPE FROM DATAFLOW D INNER JOIN ARTEFACT A ON A.ART_ID = D.DF_ID {0} " 
            + " UNION ALL " 
            + "SELECT D.DSD_ID as SID, 'Dsd' as STYPE FROM DSD D "
            + " UNION ALL " 
            + "SELECT D.CON_SCH_ID as SID, 'ConceptScheme' as STYPE FROM CONCEPT_SCHEME D"
            + " UNION ALL "
            + "SELECT D.CL_ID as SID, 'Codelist' as STYPE FROM CODELIST D"
            + " UNION ALL " 
            + "SELECT D.MSD_ID as SID, 'Msd' as STYPE FROM METADATA_STRUCTURE_DEFINITION D"
            + " UNION ALL " 
            + "SELECT D.MDF_ID as SID, 'Metadataflow' as STYPE FROM METADATAFLOW D"
            + " UNION ALL " 
            + "SELECT D.HCL_ID as SID, 'Hcl' as STYPE FROM HCL D";

        /// <summary>
        ///     Gets the SQL Query format/template for retrieving the artefact reference from a categorisation id. Use with
        ///     <see cref="string.Format(string,object)" /> and one parameter
        ///     1. the <see cref="ProductionWhereClause" /> or <see cref="string.Empty" />
        /// </summary>
        public const string ArtefactRefQueryFormat =
            "SELECT C.CATN_ID, AV.ID, AV.VERSION, AV.AGENCY, AV.ARTEFACT_TYPE as STYPE " +
            "FROM CATEGORISATION C " +
            "INNER JOIN ARTEFACT_VIEW AV ON C.ART_ID = AV.ART_ID " +
            // using DD to avoid conflicts
            "LEFT OUTER JOIN DATAFLOW DD ON C.ART_ID = DD.DF_ID " +
            "WHERE DD.DF_ID is null or " +
            "DD.DF_ID in (" +
            "SELECT A.ART_ID FROM ARTEFACT A INNER JOIN DATAFLOW D ON D.DF_ID = A.ART_ID {0})";

        /// <summary>
        ///     The categorisation version.
        /// </summary>
        public const string CategorisationVersion = "1.0";

        /// <summary>
        ///     Gets the SQL Query format/template for retrieving the category reference from a categorisation id.
        /// </summary>
        public const string CategoryRefQueryFormat =
            "SELECT C.CATN_ID, A.ID, A.VERSION, A.AGENCY, I.ITEM_ID, CY.PARENT_CAT_ID, I.ID as CATID, IP.ID as PARENT_ID FROM CATEGORY CY LEFT OUTER JOIN CATEGORISATION C ON C.CAT_ID = CY.CAT_ID INNER JOIN ARTEFACT_VIEW A ON CY.CAT_SCH_ID = A.ART_ID INNER JOIN ITEM I ON I.ITEM_ID = CY.CAT_ID LEFT OUTER JOIN ITEM IP ON IP.ITEM_ID = CY.PARENT_CAT_ID ";

        /// <summary>
        ///     Gets the not EXTERNAL_USAGE clause
        /// </summary>
        public const string InUsageWhereClause = " D.EXTERNAL_USAGE = 0 ";

        /// <summary>
        ///     Gets the PRODUCTION clause
        /// </summary>
        public const string ProductionWhereClause = " D.PRODUCTION = 1 ";

        /// <summary>
        ///     The referenced by CATEGORISATION P table the "parent" and A is the referenced
        ///     <see cref="ArtefactParentsSqlBuilder.SqlQueryFormat" />.
        /// </summary>
        public const string ReferencedByCategorisation =
            " INNER JOIN CATEGORISATION T ON T.CATN_ID = P.ART_ID INNER JOIN ARTEFACT A ON T.ART_ID = A.ART_ID ";

        /// <summary>
        ///     The _table info.
        /// </summary>
        private static readonly TableInfo _tableInfo = new TableInfo(SdmxStructureEnumType.Categorisation)
                                                           {
                                                               Table =
                                                                   "CATEGORISATION", 
                                                               PrimaryKey
                                                                   =
                                                                   "CATN_ID"
                                                           };

        /// <summary>
        ///     The _artefact reference.
        /// </summary>
        private static readonly SqlQueryInfo _artefactReference = new SqlQueryInfo
                                                                      {
                                                                          QueryFormat =
                                                                              ArtefactRefQueryFormat, 
                                                                          WhereStatus = WhereState.Nothing
                                                                      };

        /// <summary>
        ///     Gets the artefact reference.
        /// </summary>
        public static SqlQueryInfo ArtefactReference
        {
            get
            {
                return _artefactReference;
            }
        }

        /// <summary>
        ///     Gets the table info.
        /// </summary>
        public static TableInfo TableInfo
        {
            get
            {
                return _tableInfo;
            }
        }
    }
}