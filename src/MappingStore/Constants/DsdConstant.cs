// -----------------------------------------------------------------------
// <copyright file="DsdConstant.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Constants
{
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The DSD, N_COMPONENT, ATT_GROUP and DSD_GROUP constant.
    /// </summary>
    internal static class DsdConstant
    {
        /// <summary>
        ///     Gets the SQL Query format/template for retrieving the Attribute attachment groups from a DSD id. Use with
        ///     <see cref="string.Format(string,object)" /> and one parameter the <see cref="ParameterNameConstants.IdParameter" />
        /// </summary>
        public const string AttributeAttachmentGroupQueryFormat =
            "SELECT GR.ID, COMP.ID AS ATTR_REF FROM N_COMPONENT COMP INNER JOIN ATT_GROUP AG ON COMP.COMP_ID = AG.COMP_ID " +
            "INNER JOIN DSD_GROUP GR ON AG.GR_ID = GR.GR_ID WHERE COMP.DSD_ID = {0} ";

        /// <summary>
        ///     Gets the SQL Query format/template for retrieving the Attribute attachment measures from a DSD id. Use with
        ///     <see cref="string.Format(string,object)" /> and one parameter the <see cref="ParameterNameConstants.IdParameter" />
        /// </summary>
        public const string AttributeAttachmentMeasureQueryFormat =
            "SELECT XSM.ID as XSM_ID, ATTR.ID AS ATTR_ID FROM N_COMPONENT ATTR INNER JOIN ATT_MEASURE AM ON AM.ATT_COMP_ID = ATTR.COMP_ID " +
            "INNER JOIN N_COMPONENT XSM ON XSM.COMP_ID = AM.MEASURE_COMP_ID WHERE ATTR.DSD_ID = {0}";

        /// <summary>
        ///     Gets the SQL Query format/template for retrieving the attribute dimension references from the DSD id. Use with
        ///     <see cref="string.Format(string,object)" /> and one parameter the <see cref="ParameterNameConstants.IdParameter" />
        /// </summary>
        public const string AttributeDimensionFormat =
            "SELECT AD.ATTR_ID, D.ID FROM ATTR_DIMS AD INNER JOIN N_COMPONENT D ON D.COMP_ID = AD.DIM_ID where D.DSD_ID = {0} ORDER BY AD.ATTR_ID";

        /// <summary>
        ///     The component order by.
        /// </summary>
        public const string ComponentOrderBy = "ORDER BY COMP.COMP_ID ";

        /// <summary>
        ///     Gets the SQL Query format/template for retrieving the DSD components from a DSD id. Use with
        ///     <see cref="string.Format(string,object)" /> and one parameter the <see cref="ParameterNameConstants.IdParameter" />
        /// </summary>
        public const string ComponentQueryFormat =
            "SELECT COMP.COMP_ID, COMP.ID, COMP.TYPE, COMP.IS_FREQ_DIM, COMP.IS_MEASURE_DIM, COMP.ATT_ASS_LEVEL, COMP.ATT_STATUS, " +
            "COMP.ATT_IS_TIME_FORMAT, COMP.XS_ATTLEVEL_DS, COMP.XS_ATTLEVEL_GROUP, COMP.XS_ATTLEVEL_SECTION, COMP.XS_ATTLEVEL_OBS, " +
            "COMP.XS_MEASURE_CODE, COMP.MIN_OCCURS, COMP.MAX_OCCURS FROM N_COMPONENT COMP WHERE COMP.DSD_ID  = {0} ";

        /// <summary>
        ///     Gets the SQL Query format/template for retrieving the groups from the DSD id. Use with
        ///     <see cref="string.Format(string,object)" /> and one parameter the <see cref="ParameterNameConstants.IdParameter" />
        /// </summary>
        public const string GroupQueryFormat =
            "SELECT G.GR_ID AS GR_ID, G.ID AS GROUP_ID, C.ID as DIMENSION_REF FROM DSD D, DSD_GROUP G, DIM_GROUP DG, N_COMPONENT C " +
            "WHERE D.DSD_ID = G.DSD_ID AND G.GR_ID = DG.GR_ID AND DG.COMP_ID = C.COMP_ID AND D.DSD_ID = {0} ORDER BY G.GR_ID";

        /// <summary>
        ///     The referenced by dataflow P table the "parent" and A is the referenced
        ///     <see cref="ArtefactParentsSqlBuilder.SqlQueryFormat" />.
        /// </summary>
        public const string ReferencedByDataflow =
            " INNER JOIN DATAFLOW T ON T.DF_ID = P.ART_ID INNER JOIN ARTEFACT A ON T.DSD_ID = A.ART_ID ";

        /// <summary>
        ///     The referenced by content constraint: alias P used for the "parent" (the content constraint) and A for 
        ///     the referenced artefact which is the DSD
        ///     <see cref="ArtefactParentsSqlBuilder.SqlQueryFormat" />.
        /// </summary>
        public const string ReferencedByContentConstraint =
            " INNER JOIN CONTENT_CONSTRAINT CC ON CC.CONT_CONS_ID = P.ART_ID " +
            " INNER JOIN CONTENT_CONSTRAINT_ATTACHMENT CA ON CC.CONT_CONS_ID = CA.CONT_CONS_ID " +
            " INNER JOIN ARTEFACT A ON CA.ART_ID = A.ART_ID " +
            " INNER JOIN DSD DS ON DS.DSD_ID = A.ART_ID ";

        /// <summary>
        ///     Gets the SQL Query format/template for retrieving all concept roles used by components in a DSD.
        /// </summary>
        public const string ConceptRoleQueryFormat =
            "SELECT COMP.COMP_ID, I.ID as CONCEPTREF, CS.ID as CONCEPTSCHEME_ID, CS.VERSION as CONCEPT_VERSION, CS.AGENCY as CONCEPT_AGENCY " +
            "from N_COMPONENT COMP INNER JOIN CONCEPT_ROLE CR ON COMP.COMP_ID=CR.COMP_ID INNER JOIN CONCEPT C ON CR.CON_ID = C.CON_ID " +
            "INNER JOIN ITEM I ON C.CON_ID = I.ITEM_ID INNER JOIN ARTEFACT_VIEW CS ON C.CON_SCH_ID = CS.ART_ID WHERE COMP.DSD_ID = {0}";

        /// <summary>
        ///     The _table info.
        /// </summary>
        private static readonly TableInfo _tableInfo = new TableInfo(SdmxStructureEnumType.Dsd)
        {
            Table = "DSD",
            PrimaryKey = "DSD_ID"
        };

        /// <summary>
        /// The component table information
        /// </summary>
        private static readonly ItemTableInfo _componentTableInfo = new ItemTableInfo(SdmxStructureEnumType.Component)
        {
            ForeignKey = "DSD_ID",
            PrimaryKey = "COMP_ID",
            Table = "N_COMPONENT"
        };


        /// <summary>
        /// The group table information
        /// </summary>
        private static readonly ItemTableInfo _groupTableInfo = new ItemTableInfo(SdmxStructureEnumType.Group)
        {
            ForeignKey = "DSD_ID",
            PrimaryKey = "GR_ID",
            Table = "DSD_GROUP"
        };

        /// <summary>
        ///     Gets the table info.
        /// </summary>
        public static TableInfo TableInfo
        {
            get
            {
                return _tableInfo;
            }
        }

        /// <summary>
        /// Gets the component table information.
        /// </summary>
        /// <value>
        /// The component table information.
        /// </value>
        public static ItemTableInfo ComponentTableInfo
        {
            get
            {
                return _componentTableInfo;
            }
        }

        public static ItemTableInfo GroupTableInfo
        {
            get
            {
                return _groupTableInfo;
            }
        }
    }
}