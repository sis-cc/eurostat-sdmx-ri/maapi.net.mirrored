// -----------------------------------------------------------------------
// <copyright file="CodeListConstant.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Constants
{
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The code list and DSD_CODE tables constant.
    /// </summary>
    internal static class CodeListConstant
    {
        /// <summary>
        ///     Gets the item order by.
        /// </summary>
        public const string ItemOrderBy = null; // " ORDER BY T.LCD_ID";

        /// <summary>
        ///     The SQL query to build the partial codelist from LOCAL_CODES table.
        /// </summary>
        public const string LocalCodes =
            "SELECT I.ITEM_ID AS SYSID,I.ID, LOCALISED_STRING.TEXT, LOCALISED_STRING.LANGUAGE, LOCALISED_STRING.TYPE, T.PARENT_CODE_ID AS PARENT from  DSD_CODE T INNER JOIN ITEM I ON T.LCD_ID = I.ITEM_ID INNER JOIN  LOCALISED_STRING ON LOCALISED_STRING.ITEM_ID = I.ITEM_ID WHERE T.CL_ID = {0} and EXISTS ( select LCI.ID  from  LOCAL_CODE LC INNER JOIN  ITEM LCI ON LC.LCD_ID = LCI.ITEM_ID INNER JOIN COM_COL_MAPPING_COLUMN DCM ON DCM.COL_ID = LC.COLUMN_ID INNER JOIN COMPONENT_MAPPING CM ON CM.MAP_ID = DCM.MAP_ID INNER JOIN DATAFLOW D ON D.MAP_SET_ID = CM.MAP_SET_ID INNER JOIN ARTEFACT A ON A.ART_ID = D.DF_ID INNER JOIN COM_COL_MAPPING_COMPONENT CCM ON CCM.MAP_ID =CM.MAP_ID INNER JOIN COMPONENT C ON CCM.COMP_ID = C.COMP_ID INNER JOIN ITEM CI ON CI.ITEM_ID = C.CON_ID WHERE A.ID = {1} and A.AGENCY = {2} and (" +
            "A.Version1 ={3} AND A.Version2 = {4} AND (((A.Version3 is null) and ({5} is null)) or (((A.Version3 is not null) and ({5} is not null)) and ( A.Version3 = {5} )))" +
            " ) and CI.ID = {6} AND LCI.ID = I.ID {7}) ";

        /// <summary>
        ///     The <see cref="LocalCodes" /> order by.
        /// </summary>
        public const string LocalCodesOrderBy = " ORDER BY T.LCD_ID";

        /// <summary>
        ///     The referencing from DSD.P table the "parent" and A is the referenced
        ///     <see cref="ArtefactParentsSqlBuilder.SqlQueryFormat" />
        /// </summary>
        public const string ReferencingFromDsd =
            " INNER JOIN COMPONENT C ON C.DSD_ID = P.ART_ID INNER JOIN ARTEFACT A ON C.CL_ID = A.ART_ID";

        /// <summary>
        ///     The referenced by CONCEPT_SCHEME P table the "parent" and A is the referenced
        ///     <see cref="ArtefactParentsSqlBuilder.SqlQueryFormat" />.
        /// </summary>
        public const string ReferencingFromConceptScheme =
            " INNER JOIN CONCEPT_SCHEME CS ON P.ART_ID = CS.CON_SCH_ID INNER JOIN CONCEPT C ON CS.CON_SCH_ID = C.CON_SCH_ID INNER JOIN ARTEFACT A ON C.CL_ID = A.ART_ID";

        /// <summary>
        ///     The referencing from DSD.P table the "parent" and A is the referenced
        ///     <see cref="ArtefactParentsSqlBuilder.SqlQueryFormat" />
        /// </summary>
        public const string ReferencingFromMsd = @" INNER JOIN METADATA_STRUCTURE_DEFINITION MS ON MS.MSD_ID = P.ART_ID  
  INNER JOIN METADATA_TARGET MDT ON MS.MSD_ID = MDT.MSD_ID 
  INNER JOIN REPORT_STRUCTURE RS ON MS.MSD_ID = RS.MSD_ID 
  LEFT OUTER JOIN METADATA_ATTRIBUTE C ON C.RS_ID = RS.RS_ID
  LEFT OUTER JOIN TARGET_OBJECT TOB ON TOB.MDT_ID = MDT.MDT_ID
  INNER JOIN ARTEFACT A ON C.CL_ID = A.ART_ID OR TOB.ITEM_SCHEME_ID = A.ART_ID";


        /// <summary>
        ///     The referencing from HCL. P table the "parent" and A is the referenced
        ///     <see cref="ArtefactParentsSqlBuilder.SqlQueryFormat" />
        /// </summary>
        public const string ReferencingFromHcl =
            " INNER JOIN HCL ON HCL.HCL_ID = P.ART_ID INNER JOIN HIERARCHY h ON h.HCL_ID = HCL.HCL_ID INNER JOIN HCL_CODE hc ON hc.H_ID = h.H_ID INNER JOIN DSD_CODE dc ON dc.LCD_ID = hc.LCD_ID INNER JOIN ARTEFACT A ON dc.CL_ID = A.ART_ID";

        /// <summary>
        ///     The SQL query to build the partial codelist from transcoded codes.
        /// </summary>
        public const string TranscodedCodes =
            " SELECT DSD_CODE.LCD_ID AS SYSID, ITEM.ID, LOCALISED_STRING.TEXT, LOCALISED_STRING.LANGUAGE, LOCALISED_STRING.TYPE, DSD_CODE.PARENT_CODE_ID AS PARENT   FROM DSD_CODE  INNER JOIN ITEM ON DSD_CODE.LCD_ID = ITEM.ITEM_ID   INNER JOIN LOCALISED_STRING ON ITEM.ITEM_ID = LOCALISED_STRING.ITEM_ID   WHERE DSD_CODE.CL_ID = {0}  AND DSD_CODE.LCD_ID in (       select TRANSCODING_RULE_DSD_CODE.CD_ID FROM TRANSCODING_RULE       INNER JOIN TRANSCODING_RULE_DSD_CODE ON TRANSCODING_RULE.TR_RULE_ID = TRANSCODING_RULE_DSD_CODE.TR_RULE_ID       INNER JOIN TRANSCODING ON TRANSCODING_RULE.TR_ID = TRANSCODING.TR_ID       INNER JOIN COMPONENT_MAPPING ON TRANSCODING.MAP_ID = COMPONENT_MAPPING.MAP_ID       INNER JOIN COM_COL_MAPPING_COMPONENT ON COMPONENT_MAPPING.MAP_ID = COM_COL_MAPPING_COMPONENT.MAP_ID       INNER JOIN MAPPING_SET ON COMPONENT_MAPPING.MAP_SET_ID = MAPPING_SET.MAP_SET_ID       INNER JOIN COMPONENT ON COM_COL_MAPPING_COMPONENT.COMP_ID = COMPONENT.COMP_ID       INNER JOIN DATAFLOW ON MAPPING_SET.MAP_SET_ID = DATAFLOW.MAP_SET_ID       INNER JOIN ARTEFACT A ON DATAFLOW.DF_ID = A.ART_ID where ( A.ID = {1} ) AND ( A.AGENCY = {2} ) AND ( " +
            "A.Version1 ={3} AND A.Version2 = {4} AND (((A.Version3 is null) and ({5} is null)) or (((A.Version3 is not null) and ({5} is not null)) and ( A.Version3 = {5} )))" +
            " ) AND ( COMPONENT.ID = {6} ) {7} ) ";

        /// <summary>
        ///     The <see cref="TranscodedCodes" /> order by.
        /// </summary>
        public const string TranscodedCodesOrderBy = " ORDER BY DSD_CODE.LCD_ID";

        public const string CountCodes = "SELECT COUNT(1) AS COUNT FROM DSD_CODE WHERE CL_ID={0}";

        /// <summary>
        ///     The item table info. i.e. for table DSD_CODE
        /// </summary>
        private static readonly ItemTableInfo _itemTableInfo = ItemTableInfo.Factory.NewDefault(SdmxStructureEnumType.Code, true);

        /// <summary>
        ///     The _table info.
        /// </summary>
        private static readonly TableInfo _tableInfo = new TableInfo(SdmxStructureEnumType.CodeList)
                                                           {
                                                               Table =
                                                                   "CODELIST", 
                                                               PrimaryKey =
                                                                   "CL_ID", 
                                                               ExtraFields =
                                                                   ", IS_PARTIAL"
                                                           };

        /// <summary>
        ///     The <see cref="TranscodedCodes" /> <see cref="SqlQueryInfo" />
        /// </summary>
        private static readonly SqlQueryInfo _transcodedSqlQueryInfo = new SqlQueryInfo
        {
            QueryFormat = TranscodedCodes,
            WhereStatus = WhereState.And,
            OrderBy =
                                                                                   TranscodedCodesOrderBy
        };

        /// <summary>
        ///     The <see cref="LocalCodes" /> <see cref="SqlQueryInfo" />
        /// </summary>
        private static readonly SqlQueryInfo _localCodeSqlQueryInfo = new SqlQueryInfo
        {
            QueryFormat = LocalCodes,
            WhereStatus = WhereState.And,
            OrderBy = LocalCodesOrderBy
        };

        private static readonly SqlQueryInfo _countCodesSqlQueryInfo = new SqlQueryInfo
        {
            QueryFormat = CountCodes
        };

        /// <summary>
        ///     Gets the item table info. i.e. for table DSD_CODE
        /// </summary>
        public static ItemTableInfo ItemTableInfo
        {
            get
            {
                return _itemTableInfo;
            }
        }

        /// <summary>
        ///     Gets the <see cref="LocalCodes" /> <see cref="SqlQueryInfo" />
        /// </summary>
        public static SqlQueryInfo LocalCodeSqlQueryInfo
        {
            get
            {
                return _localCodeSqlQueryInfo;
            }
        }

        /// <summary>
        ///     Gets the table info.
        /// </summary>
        public static TableInfo TableInfo
        {
            get
            {
                return _tableInfo;
            }
        }

        /// <summary>
        ///     Gets the <see cref="TranscodedCodes" /> <see cref="SqlQueryInfo" />
        /// </summary>
        public static SqlQueryInfo TranscodedSqlQueryInfo
        {
            get
            {
                return _transcodedSqlQueryInfo;
            }
        }

        /// <summary>
        ///     Gets the <see cref="CountCodes" /> <see cref="SqlQueryInfo" />
        /// </summary>
        public static SqlQueryInfo CountCodesInfo
        {
            get
            {
                return _countCodesSqlQueryInfo;
            }
        }
    }
}