﻿// -----------------------------------------------------------------------
// <copyright file="MetadataFlowConstant.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Constants
{
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The Dataflow table constant.
    /// </summary>
    internal static class MetadataFlowConstant
    {
        /// <summary>
        ///     Gets the SQL Query format/template for retrieving the key family reference from a dataflow id. Use with
        ///     <see cref="string.Format(string,object)" /> and one parameter the <see cref="ParameterNameConstants.IdParameter" />
        /// </summary>
        public const string MsdRefQueryFormat =
            "SELECT MSD.MSD_ID, ART.ID, ART.VERSION, ART.AGENCY FROM METADATA_STRUCTURE_DEFINITION MSD, METADATAFLOW DF, ARTEFACT_VIEW ART WHERE DF.MSD_ID = MSD.MSD_ID AND MSD.MSD_ID = ART.ART_ID AND DF.MDF_ID = {0} ";

        /// <summary>
        ///     The _table info.
        /// </summary>
        private static readonly TableInfo _tableInfo = new TableInfo(SdmxStructureEnumType.MetadataFlow)
                                                           {
                                                               Table =
                                                                   "METADATAFLOW", 
                                                               PrimaryKey =
                                                                   "MDF_ID" 
                                                           };

        /// <summary>
        ///     Gets the table info.
        /// </summary>
        public static TableInfo TableInfo
        {
            get
            {
                return _tableInfo;
            }
        }
    }
}