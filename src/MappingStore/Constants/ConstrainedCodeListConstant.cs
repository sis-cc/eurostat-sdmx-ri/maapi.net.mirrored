// -----------------------------------------------------------------------
// <copyright file="ConstrainedCodeListConstant.cs" company="EUROSTAT">
//   Date Created : 2018-07-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or β€“ as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using System;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Model;

namespace Estat.Sri.MappingStoreRetrieval.Constants
{
    internal static class ConstrainedCodeListConstant
    {
        public const string constrainedCodelistSqlTable = "#TEMP_PROCESS_CONSTRAINTS_TABLE";
        public const string constrainedCodelistTable = "TEMP_PROCESS_CONSTRAINTS_TABLE";

        /// <summary>
        ///     The SQL query to build the partial codelist
        /// </summary>
        public const string ConstrainedCodelist =
            @"  SELECT DISTINCT T.CODE_SYS_ID AS SYSID, T.CODE_ID AS ID, T.PARENT_CODE_SYS_ID as PARENT, 
                LN.TEXT, LN.LANGUAGE, LN.TYPE 
                FROM {0} T LEFT OUTER JOIN LOCALISED_STRING LN ON LN.ITEM_ID = T.CODE_SYS_ID ";

        public const string ConstrainedCodelistOrderBy = null;
        
        /// <summary>
        ///     Gets the <see cref="ConstrainedCodelist" /> <see cref="SqlQueryInfo" />
        /// </summary>
        public static SqlQueryInfo GetConstrainedCodelistSqlQueryInfo(string providerName)
        {
            return new SqlQueryInfo
            {
                QueryFormat = String.Format(ConstrainedCodelist, getTableName(providerName)),
                WhereStatus = WhereState.And,
                OrderBy = ConstrainedCodelistOrderBy
            };
        }

        public static String getTableName(string providerName)
        {
            if (providerName == MappingStoreDefaultConstants.SqlServerProvider)
            {
                return constrainedCodelistSqlTable;
            }
            return constrainedCodelistTable;
        }
    }
}
