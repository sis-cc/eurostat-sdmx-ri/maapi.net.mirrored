// -----------------------------------------------------------------------
// <copyright file="ProvisionAgreementConstant.cs" company="EUROSTAT">
//   Date Created : 2017-04-14
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStoreRetrieval.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Sri.MappingStoreRetrieval.Constants
{
    internal static class ProvisionAgreementConstant
    {
        /// <summary>
        ///     The _table info.
        /// </summary>
        private static readonly TableInfo _tableInfo = new TableInfo(SdmxStructureEnumType.ProvisionAgreement)
        {
            Table
                =
                "PROVISION_AGREEMENT",
            PrimaryKey
                =
                "PA_ID"
        };


        public const string DataproviderRef =
            "SELECT ART.ID, ART.VERSION, ART.AGENCY, I.ID as CHILD_ID FROM PROVISION_AGREEMENT PA,DATAPROVIDER DP, ARTEFACT_VIEW ART, ITEM I WHERE PA.DP_ID = DP.DP_ID AND DP.DP_ID = I.ITEM_ID AND DP.DP_SCH_ID = ART.ART_ID AND PA.PA_ID = {0}";


        public const string StructureUsageRef =
            "SELECT ART.ID, ART.VERSION, ART.AGENCY, ART.ARTEFACT_TYPE FROM PROVISION_AGREEMENT PA, ARTEFACT_VIEW ART WHERE PA.SU_ID = ART.ART_ID AND PA.PA_ID = {0}";

        public const string ReferencingFromDataflow = "INNER JOIN PROVISION_AGREEMENT PA ON PA.PA_ID = P.ART_ID INNER JOIN ARTEFACT A ON PA.SU_ID = A.ART_ID AND A.ARTEFACT_TYPE = '{0}'";

        public const string ReferencingFromDataProvider = "INNER JOIN PROVISION_AGREEMENT PA ON PA.PA_ID = P.ART_ID INNER JOIN DATAPROVIDER DP ON PA.DP_ID = DP.DP_ID INNER JOIN ARTEFACT A ON DP.DP_SCH_ID = A.ART_ID";

        /// <summary>
        ///     Gets the table info.
        /// </summary>
        public static TableInfo TableInfo
        {
            get { return _tableInfo; }
        }
    }
}