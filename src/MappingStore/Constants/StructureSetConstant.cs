// -----------------------------------------------------------------------
// <copyright file="StructureSetConstant.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Constants
{
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The concept scheme and concept tables constant.
    /// </summary>
    internal static class StructureSetConstant
    {
        /// <summary>
        ///     The _table info.
        /// </summary>
        private static readonly TableInfo _tableInfo = new TableInfo(SdmxStructureEnumType.StructureSet)
                                                           {
                                                               Table =
                                                                   "STRUCTURE_SET", 
                                                               PrimaryKey
                                                                   =
                                                                   "SS_ID"
                                                           };

        #region 11.2 ISTAT ENHANCEMENT

        /// <summary>
        /// Gets the item order by.
        /// </summary>
        public const string ItemOrderBy = " ORDER BY T.SS_ID";

        /// <summary>
        /// The CodeListMap Item Table Info
        /// </summary>
        private static readonly ItemTableInfo _clmItemTableInfo = new ItemTableInfo(SdmxStructureEnumType.CodeListMap) { Table = "CODELIST_MAP", PrimaryKey = "CLM_ID", ForeignKey = "SS_ID" };

        /// <summary>
        /// The StructureMap Item Table Info
        /// </summary>
        private static readonly ItemTableInfo _smItemTableInfo = new ItemTableInfo(SdmxStructureEnumType.StructureMap) { Table = "STRUCTURE_MAP", PrimaryKey = "SM_ID", ForeignKey = "SS_ID" };

        #endregion
        //Andrea fine

        #region "CodeListMap"

        public const string SQL_CLM_OR_SM_REFERENCE = "select abr.ID, abr.AGENCY , abr.ARTEFACT_TYPE, sr.MAJOR , sr.MINOR , sr.PATCH , sr.EXTENSION ,  \n" +
            "rs.SOURCE_ARTEFACT , rs.REF_TYPE ,\n" +
            "o.URN_CLASS, o.ID\n" +
            "from STRUCTURE_REF sr inner join ARTEFACT_BASE abr on abr.ART_BASE_ID = sr.TARGET_ARTEFACT \n" +
            "inner join REFERENCE_SOURCE rs on rs.REF_SRC_ID  = sr.REF_SRC_ID\n" +
            "inner join OTHER_NAMEABLE o on o.PARENT_ARTEFACT = rs.SOURCE_ARTEFACT \n" +
            "where rs.SOURCE_CHILD_FULL_ID = o.ID \n" +
            "and o.OTH_ID = {0} \n" +
            "and o.URN_CLASS= {1} ";


    public const string SQL_CLM_OR_SM_ITEM = "SELECT " +
            "       SOURCE_ID AS S_ID, " +
            "       TARGET_ID AS T_ID " +
            "   FROM STRUCTURE_SET_COMMON_MAP2 A " +
            " WHERE A.PARENT_MAP = {0} ";

        /// <summary>
        /// StructureMap Info 
        /// </summary>
        /// 
        public const string SqlSMInfo = "SELECT B.ITEM_ID, ID, TYPE, TEXT, LANGUAGE " +
                                        "FROM STRUCTURE_MAP A  " +
                                        "    INNER JOIN ITEM B ON " +
                                            "        A.SM_ID = B.ITEM_ID	 " +
                                        "    LEFT OUTER JOIN LOCALISED_STRING C ON  " +
                                            "        A.SM_ID = C.ITEM_ID  " +
                                        "WHERE SS_ID = {0}";

        /// <summary>
        /// StructureMap Structure Reference
        /// </summary>
        public const string SqlSMReference = "SELECT B.ID AS S_ID, B.AGENCY AS S_AGENCY, B.VERSION AS S_VERSION, " +
                                                      "CASE WHEN D.DSD_ID IS NOT NULL THEN 'Dsd' ELSE 'Dataflow' END AS S_ARTEFACT_TYPE, " +
                                                      "C.ID AS T_ID, C.AGENCY AS T_AGENCY, C.VERSION AS T_VERSION, " +
                                                      "CASE WHEN E.DSD_ID IS NOT NULL THEN 'Dsd' ELSE 'Dataflow' END AS T_ARTEFACT_TYPE " +
                                               "FROM STRUCTURE_MAP A " +
                                               "	INNER JOIN ARTEFACT_VIEW B ON A.SOURCE_STR_ID = B.ART_ID" +
                                               "	INNER JOIN ARTEFACT_VIEW C ON A.TARGET_STR_ID = C.ART_ID " +
                                               "	LEFT OUTER JOIN DSD D ON D.DSD_ID = A.SOURCE_STR_ID " +
                                               "	LEFT OUTER JOIN DSD E ON E.DSD_ID = A.TARGET_STR_ID " +
                                               "WHERE A.SM_ID = {0}";


        /// <summary>
        /// StructureMap Item
        /// </summary>
        public const string SqlSMItem = " SELECT   C.ID AS S_ID,  D.ID AS T_ID " +
                                           " FROM STRUCTURE_MAP A " +
                                           "   INNER JOIN COMPONENT_MAP B  ON  B.SM_ID = A.SM_ID " +
                                           "   INNER JOIN COMPONENT C  ON  B.SOURCE_COMP_ID = C.COMP_ID " +
                                           "   INNER JOIN COMPONENT D  ON  B.TARGET_COMP_ID = D.COMP_ID " +
                                           " WHERE A.SM_ID = {0} ";


        // 1. Info CodeListMap(id, name, description)
        /// <summary>
        /// CodeListMap Info 
        /// </summary>
        public const string SqlCLMInfo = "SELECT B.ITEM_ID, ID, TYPE, TEXT, LANGUAGE " +
                                            "FROM CODELIST_MAP A " +
                                            "   INNER JOIN ITEM B ON " +
                                            "        A.CLM_ID = B.ITEM_ID	  " +
                                            "	LEFT OUTER JOIN LOCALISED_STRING C ON " +
                                            "        A.CLM_ID = C.ITEM_ID " +
                                            "WHERE A.SS_ID = {0}";


        /// 2. SourceRef.CrossReferenceImpl		
        /// <summary>
        /// CodeListMap CL Reference
        /// </summary>
        public const string SqlCLMReference = "SELECT " +
                                               "     B.ID AS S_ID, " +
                                               "     B.AGENCY AS S_AGENCY, " +
                                               "     B.VERSION AS S_VERSION, " +
                                               "     C.ID AS T_ID, " +
                                               "     C.AGENCY AS T_AGENCY, " +
                                               "     C.VERSION AS T_VERSION " +
                                               " FROM CODELIST_MAP A " +
                                               "     INNER JOIN ARTEFACT_VIEW B ON " +
                                               "         A.SOURCE_CL_ID = B.ART_ID " +
                                               "     INNER JOIN ARTEFACT_VIEW C ON " +
                                               "         A.TARGET_CL_ID = C.ART_ID " +
                                               " WHERE A.CLM_ID = {0}";


        /// <summary>
        /// CodeListMap Item
        /// </summary>
        public const string SqlCLMItem = "SELECT " +
                                          "       C.ID AS S_ID, " +
                                          "       D.ID AS T_ID " +
                                         "   FROM CODELIST_MAP A " +
                                         "       INNER JOIN CODE_MAP B ON " +
                                         "           B.CLM_ID = A.CLM_ID " +
                                         "       INNER JOIN ITEM C ON " +
                                         "           B.SOURCE_LCD_ID = C.ITEM_ID " +
                                         "       INNER JOIN ITEM D ON " +
                                         "           B.TARGET_LCD_ID = D.ITEM_ID " +
                                          " WHERE A.CLM_ID = {0} ";

        #endregion

        /// <summary>
        ///     Gets The CodeListMap Item Table Info
        /// </summary>
        public static ItemTableInfo CLMItemTableInfo
        {
            get
            {
                return _clmItemTableInfo;
            }
        }

        /// <summary>
        ///     Gets The StructureMap Item Table Info
        /// </summary>
        public static ItemTableInfo SMItemTableInfo
        {
            get
            {
                return _smItemTableInfo;
            }
        }

        /// <summary>
        ///     Gets the table info.
        /// </summary>
        public static TableInfo TableInfo
        {
            get
            {
                return _tableInfo;
            }
        }
    }
}