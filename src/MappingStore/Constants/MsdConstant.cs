// -----------------------------------------------------------------------
// <copyright file="MsdConstant.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Constants
{
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The DSD, COMPONENT, ATT_GROUP and DSD_GROUP constant.
    /// </summary>
    internal static class MsdConstant
    {
        /// <summary>
        ///     Gets the SQL Query format/template for retrieving the MSD Metadata Attributes from a MSD id. Use with
        ///     <see cref="string.Format(string,object)" /> and one parameter the <see cref="ParameterNameConstants.IdParameter" />
        /// </summary>
        public const string MetadataAttributesQueryFormat = 
            @"SELECT RS.OTH_ID, COMP.MTD_ATTR_ID, COMP.PARENT_PATH, COMP.ID, COMP.IS_PRESENTATIONAL, COMP.MAX_OCCURS, COMP.MIN_OCCURS " +
            "FROM OTHER_NAMEABLE RS " +
            "INNER JOIN METADATA_ATTRIBUTE COMP ON RS.OTH_ID = COMP.RS_ID " +
            "WHERE RS.PARENT_ARTEFACT = {0} " +
            "ORDER by RS.OTH_ID, COMP.MTD_ATTR_ID ";

        /// <summary>
        ///     Gets the SQL Query format/template for retrieving the Report Structure and Metadata Target relations from the MSD id. Use with
        ///     <see cref="string.Format(string,object)" /> and one parameter the <see cref="ParameterNameConstants.IdParameter" />
        /// </summary>
        public const string ReportStructureMetadataTargerFormat =
            @"select RM.RS_ID, RM.MDT_ID " +
            "from REPORT_STRUCTURE_META_TARGET RM " +
            "INNER JOIN OTHER_NAMEABLE RS on RS.OTH_ID = RM.RS_ID and RS.URN_CLASS = 'ReportStructure' " +
            "INNER JOIN OTHER_NAMEABLE MT ON MT.OTH_ID = RM.MDT_ID and RS.PARENT_ARTEFACT  = MT.PARENT_ARTEFACT and MT.URN_CLASS = 'MetadataTarget' " +
            "WHERE RS.PARENT_ARTEFACT = {0}";

        public const string InnerJoinPattern = 
            "INNER JOIN METADATA_STRUCTURE_DEFINITION MS ON MS.MSD_ID = P.ART_ID " +
            "INNER JOIN METADATA_TARGET MDT ON MS.MSD_ID = MDT.MSD_ID " +
            "LEFT OUTER JOIN TARGET_OBJECT TOB ON TOB.MDT_ID = MDT.MDT_ID " +
            "INNER JOIN ARTEFACT A ON A.ART_ID = TOB.ITEM_SCHEME_ID AND A.ARTEFACT_TYPE = '{0}'";

        /// <summary>
        ///     The referenced by MetadataFlow P table the "parent" and A is the referenced
        ///     <see cref="ArtefactParentsSqlBuilder.SqlQueryFormat" />.
        /// </summary>
        public const string ReferencedByMetadataDataflow =
            " INNER JOIN METADATAFLOW T ON T.MDF_ID = P.ART_ID INNER JOIN ARTEFACT A ON T.MSD_ID = A.ART_ID ";

        /// <summary>
        /// The target object format
        /// </summary>
        public const string TargetObjectFormat = 
            @"select TOB.TARGET_OBJ_ID, TOB.ID, TOB.TYPE, TOB.MDT_ID, AV.ID AID, AV.AGENCY, AV.VERSION, T.STYPE
            from TARGET_OBJECT TOB 
            inner join METADATA_TARGET MT ON TOB.MDT_ID = MT.MDT_ID
            LEFT OUTER JOIN ARTEFACT_VIEW AV ON AV.ART_ID = TOB.ITEM_SCHEME_ID
            LEFT OUTER JOIN ({0}) T ON T.SID = AV.ART_ID
            WHERE MT.MSD_ID = {{0}}";

        /// <summary>
        ///     The _table info.
        /// </summary>
        private static readonly TableInfo _tableInfo = new TableInfo(SdmxStructureEnumType.Msd)
        {
            Table = "METADATA_STRUCTURE_DEFINITION",
            PrimaryKey = "MSD_ID"
        };

        /// <summary>
        /// The report structure table information
        /// </summary>
        private static readonly ItemTableInfo _reportStructureTableInfo = ItemTableInfo.Factory.NewOtherNameable(SdmxStructureEnumType.ReportStructure);

        /// <summary>
        /// The metadata target table information
        /// </summary>
        private static readonly ItemTableInfo _metadataTargetTableInfo = ItemTableInfo.Factory.NewOtherNameable(SdmxStructureEnumType.MetadataTarget);

        /// <summary>
        /// The metadata target table information
        /// </summary>
        private static readonly ItemTableInfo _metadataAttributeTableInfo = new ItemTableInfo(SdmxStructureEnumType.MetadataAttribute)
        {
            ForeignKey = "MSD_ID",
            PrimaryKey = "MTD_ATTR_ID",
            Table = "METADATA_ATTRIBUTE",
            ParentItem = "PARENT_PATH"
        };

        /// <summary>
        /// The metadata target table information
        /// </summary>
        private static readonly ItemTableInfo _targetObjectTableInfo = new ItemTableInfo(SdmxStructureEnumType.Component)
        {
            ForeignKey = "MDT_ID",
            PrimaryKey = "TARGET_OBJ_ID",
            Table = "TARGET_OBJECT"
        };

        /// <summary>
        ///     Gets the table info.
        /// </summary>
        public static TableInfo TableInfo
        {
            get
            {
                return _tableInfo;
            }
        }

        /// <summary>
        /// Gets the metadata target table information.
        /// </summary>
        /// <value>
        /// The metadata target table information.
        /// </value>
        public static ItemTableInfo MetadataTargetTableInfo
        {
            get
            {
                return _metadataTargetTableInfo;
            }
        }

        /// <summary>
        /// Gets the report structure table information.
        /// </summary>
        /// <value>
        /// The report structure table information.
        /// </value>
        public static ItemTableInfo ReportStructureTableInfo
        {
            get
            {
                return _reportStructureTableInfo;
            }
        }

        /// <summary>
        /// Gets the metadata attribute table information.
        /// </summary>
        /// <value>
        /// The metadata attribute table information.
        /// </value>
        public static ItemTableInfo MetadataAttributeTableInfo
        {
            get
            {
                return _metadataAttributeTableInfo;
            }
        }

        /// <summary>
        /// Gets the target object table information.
        /// </summary>
        /// <value>
        /// The target object table information.
        /// </value>
        public static ItemTableInfo TargetObjectTableInfo
        {
            get
            {
                return _targetObjectTableInfo;
            }
        }
    }
}