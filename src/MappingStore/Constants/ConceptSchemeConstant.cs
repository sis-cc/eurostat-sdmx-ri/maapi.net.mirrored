// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeConstant.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Constants
{
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The concept scheme and concept tables constant.
    /// </summary>
    internal static class ConceptSchemeConstant
    {
        /// <summary>
        ///     Gets the item order by.
        /// </summary>
        public const string ItemOrderBy = " ORDER BY T.CON_ID";

        /// <summary>
        ///     The referencing from DSD.P table the "parent" and A is the referenced
        ///     <see cref="ArtefactParentsSqlBuilder.SqlQueryFormat" />
        /// </summary>
        public const string ReferencingFromDsd =
            " INNER JOIN COMPONENT C ON C.DSD_ID = P.ART_ID LEFT OUTER JOIN CONCEPT_ROLE CR ON C.COMP_ID = CR.COMP_ID INNER JOIN CONCEPT CON ON (C.CON_ID = CON.CON_ID OR CON.CON_ID = CR.CON_ID) INNER JOIN ARTEFACT A ON CON.CON_SCH_ID = A.ART_ID";

        /// <summary>
        ///     The referencing from METADATA_STRUCTURE_DEFINITION.P table the "parent" and A is the referenced
        ///     <see cref="ArtefactParentsSqlBuilder.SqlQueryFormat" />
        /// </summary>
        public const string ReferencingFromMsd = @"  INNER JOIN REPORT_STRUCTURE RS ON RS.MSD_ID = P.ART_ID 
                INNER JOIN METADATA_ATTRIBUTE C ON C.RS_ID = RS.RS_ID
                INNER JOIN CONCEPT CON ON C.CON_ID = CON.CON_ID 
                INNER JOIN ARTEFACT A ON CON.CON_SCH_ID = A.ART_ID";

        /// <summary>
        ///     The SQL query to build the partial conceptscheme for a given DSD.
        /// </summary>
        public const string PartialConcepts = @"
                SELECT CC.CON_ID AS SYSID, IT.ID AS ID, LN.TEXT, LN.LANGUAGE, LN.TYPE
                FROM ARTEFACT_VIEW DSDA
                INNER JOIN DSD DSD ON DSDA.ART_ID = DSD.DSD_ID
                INNER JOIN (
                    SELECT COMP.DSD_ID, CON.CON_SCH_ID, CON.CON_ID
                    FROM COMPONENT COMP
                    INNER JOIN CONCEPT CON ON COMP.CON_ID = CON.CON_ID
                    UNION
                    SELECT COMP.DSD_ID, CON.CON_SCH_ID, CON.CON_ID
                    FROM COMPONENT COMP
                    INNER JOIN CONCEPT_ROLE CR ON COMP.COMP_ID = CR.COMP_ID
                    INNER JOIN CONCEPT CON ON CR.CON_ID = CON.CON_ID
                ) CC ON CC.DSD_ID = DSD.DSD_ID
                INNER JOIN ITEM IT ON CC.CON_ID = IT.ITEM_ID
                LEFT JOIN LOCALISED_STRING LN ON LN.ITEM_ID = IT.ITEM_ID";

        /// <summary>
        ///     The SQL used to count the concepts of the conceptscheme.
        /// </summary>
        public const string CountConcepts = "SELECT COUNT(1) AS COUNT FROM CONCEPT WHERE CON_SCH_ID = {0}";

        /// <summary>
        ///     The item table info. i.e. for table CONCEPT
        /// </summary>
        private static readonly ItemTableInfo _itemTableInfo = ItemTableInfo.Factory.NewDefault(SdmxStructureEnumType.Concept, true);

        /// <summary>
        ///     The _table info.
        /// </summary>
        private static readonly TableInfo _tableInfo = new TableInfo(SdmxStructureEnumType.ConceptScheme)
        {
            Table =
                                                                   "CONCEPT_SCHEME",
            PrimaryKey
                                                                   =
                                                                   "CON_SCH_ID",
            ExtraFields
                                                                   =
                                                                   ", IS_PARTIAL"
        };

        private static readonly SqlQueryInfo _partialConceptsSqlQueryInfo = new SqlQueryInfo
        {
            QueryFormat = PartialConcepts,
            WhereStatus = WhereState.And
        };

        private static readonly SqlQueryInfo _countConcepts = new SqlQueryInfo
        {
            QueryFormat = CountConcepts
        };

        /// <summary>
        ///     Gets the item table info. i.e. for table CONCEPT
        /// </summary>
        public static ItemTableInfo ItemTableInfo
        {
            get
            {
                return _itemTableInfo;
            }
        }

        /// <summary>
        ///     Gets the table info.
        /// </summary>
        public static TableInfo TableInfo
        {
            get
            {
                return _tableInfo;
            }
        }

        /// <summary>
        ///     Gets the SqlQueryInfo for partial concepts
        /// </summary>
        public static SqlQueryInfo PartialConceptsInfo
        {
            get
            {
                return _partialConceptsSqlQueryInfo;
            }
        }

        /// <summary>
        ///     Gets the SqlQueryInfo for counting the concepts of a conceptscheme
        /// </summary>
        public static SqlQueryInfo CountConceptsInfo
        {
            get
            {
                return _countConcepts;
            }
        }
    }
}