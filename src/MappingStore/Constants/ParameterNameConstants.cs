// -----------------------------------------------------------------------
// <copyright file="ParameterNameConstants.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Constants
{
    /// <summary>
    ///     This class contains common parameter names
    /// </summary>
    public static class ParameterNameConstants
    {
        /// <summary>
        ///     Agency prepared statement named parameter
        /// </summary>
        public const string AgencyParameter = "Agency";

        /// <summary>
        ///     Agency prepared statement named parameter
        /// </summary>
        public const string ArtefactTypeParameter = "artefactType";

        /// <summary>
        ///     Prefix for code prepared statement named parameter
        /// </summary>
        public const string CodeParameterName = "code";

        /// <summary>
        ///     Concept ID prepared statement named parameter
        /// </summary>
        public const string ConceptIdParameter = "cid";

        /// <summary>
        /// Concept Scheme Id prepared statement named parameter
        /// </summary>
        public const string ConceptSchemeIdParameter = "cshId";

        /// <summary>
        ///     Dataflow ID prepared statement named parameter
        /// </summary>
        public const string DataflowIdParameter = "DfId";

        /// <summary>
        ///     ID/SysID prepared statement named parameter
        /// </summary>
        public const string IdParameter = "Id";

        /// <summary>
        ///     Production prepared statement named parameter
        /// </summary>
        public const string ProductionParameter = "Production";

        /// <summary>
        ///     Transcoding ID prepared statement named parameter
        /// </summary>
        public const string TranscodingId = "trid";

        /// <summary>
        ///     Version prepared statement named parameter
        /// </summary>
        public const string VersionParameter = "Version";

        /// <summary>
        ///     Version prepared statement named parameter
        /// </summary>
        public const string VersionParameter1 = "Version1";

        /// <summary>
        ///     Version prepared statement named parameter
        /// </summary>
        public const string VersionParameter2 = "Version2";

        /// <summary>
        ///     Version prepared statement named parameter
        /// </summary>
        public const string VersionParameter3 = "Version3";

        /// <summary>
        ///     Version extension db command named parameter
        /// </summary>
        public const string VersionExtension = "extension";

        /// <summary>
        ///     urn class named parameter
        /// </summary>
        public const string UrlClassParameter = "UrnClass";
    }
}