// -----------------------------------------------------------------------
// <copyright file="ContentConstraintConstant.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Constants
{
    using Estat.Sri.MappingStoreRetrieval.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The concept scheme and concept tables constant.
    /// </summary>
    public static class ContentConstraintConstant
    {
        /// <summary>
        ///     The _table info.
        /// </summary>
        private static readonly TableInfo _tableInfo = new TableInfo(SdmxStructureEnumType.ContentConstraint)
        {
            Table = "CONTENT_CONSTRAINT",
            PrimaryKey = "CONT_CONS_ID",
            ExtraFields = "ACTUAL_DATA,PERIODICITY,OFFSET,TOLERANCE,SET_ID,ATTACH_TYPE,SIMPLE_DATA_SOURCE,START_TIME, END_TIME"
        };

        /// <summary>
        /// Recupero dati Constraint Attachment + Recupero dati Release Calendar
        /// </summary>
        public const string SqlConsInfo = @"SELECT B.ID,
                                                   B.AGENCY,
                                                   B.VERSION AS VERSION,
                                                   E.CONT_CONS_ID,
                                                   E.ACTUAL_DATA,
                                                   E.PERIODICITY,
                                                   E.OFFSET,
                                                   E.TOLERANCE,
                                                   E.DP_ID,
                                                   E.ATTACH_TYPE,
                                                   E.SET_ID,
                                                   E.SIMPLE_DATA_SOURCE,
                                                   E.START_TIME,  
                                                   E.END_TIME  
                                                   FROM CONTENT_CONSTRAINT E
                                                   LEFT OUTER JOIN CONTENT_CONSTRAINT_ATTACHMENT CCA
                                                   ON E.CONT_CONS_ID = CCA.CONT_CONS_ID
                                                   LEFT OUTER JOIN ARTEFACT_VIEW B
                                                   ON B.ART_ID = CCA.ART_ID
                                                   WHERE E.CONT_CONS_ID = {0}";

        public const string DataProviderSql = @"select A.ID,A.VERSION,A.AGENCY,I.ID as DATAPROVIDERID
                                                From ARTEFACT_VIEW A
                                                Join DATAPROVIDER D on D.DP_SCH_ID = A.ART_ID
                                                Join ITEM I on I.ITEM_ID = D.DP_ID
                                                Join CONTENT_CONSTRAINT CC on CC.DP_ID = D.DP_ID
                                                Where D.DP_ID = {0}";

        public const string DataSourceSql = @"select DS.DATA_URL,DS.WSDL_URL,DS.WADL_URL,IS_SIMPLE,IS_REST,IS_WS
                                                FROM DATA_SOURCE DS
                                                Join CONTENT_CONSTRAINT_SOURCE CCS on DS.DATA_SOURCE_ID = CCS.DATA_SOURCE_ID  
                                                join CONTENT_CONSTRAINT CC on CCS.CONT_CONS_ID = CC.CONT_CONS_ID
                                                Where CC.CONT_CONS_ID = {0}";

        /// <summary>
        /// Recupero dati CubeRegion
        /// </summary>
        public const string SqlConsItem = "SELECT A.INCLUDE AS CUBE_REGION_INCLUDE, " +
                                                   " B.INCLUDE AS CUBE_REGION_KEY_VALUE_INCLUDE, " +
                                                   " C.INCLUDE AS CUBE_REGION_VALUE_INCLUDE, " +
                                                   " C.CASCADE_VALUES AS CUBE_REGION_VALUE_CASCADE_VALUES, " +
                                                   " B.CUBE_REGION_KEY_VALUE_ID, " +
                                                   " B.MEMBER_ID, B.COMPONENT_TYPE, C.MEMBER_VALUE, " +
                                                   " B.START_PERIOD, B.END_PERIOD, B.START_INCLUSIVE, B.END_INCLUSIVE " +
                                            "FROM CUBE_REGION A " +
                                            "    INNER JOIN CUBE_REGION_KEY_VALUE B ON " +
                                            "        A.CUBE_REGION_ID = B.CUBE_REGION_ID " +
                                            "    LEFT JOIN CUBE_REGION_VALUE C ON " +
                                            "        B.CUBE_REGION_KEY_VALUE_ID = C.CUBE_REGION_KEY_VALUE_ID " +
                                            "WHERE A.CONT_CONS_ID = {0} " +
                                            "ORDER BY CUBE_REGION_KEY_VALUE_ID ";

        /// <summary>
        /// The SQL query to build the available constraint for a specific component of a specific dataflow from LOCAL_CODES table.
        /// </summary>
        public const string LocalCodes =
            "SELECT LCI.ID from LOCAL_CODE LC " +
            "INNER JOIN ITEM LCI ON LC.LCD_ID = LCI.ITEM_ID " +
            "INNER JOIN COM_COL_MAPPING_COLUMN DCM ON DCM.COL_ID = LC.COLUMN_ID " +
            "INNER JOIN COMPONENT_MAPPING CM ON CM.MAP_ID = DCM.MAP_ID " +
            "INNER JOIN DATAFLOW D ON D.MAP_SET_ID = CM.MAP_SET_ID " +
            "INNER JOIN ARTEFACT A ON A.ART_ID = D.DF_ID " +
            "INNER JOIN COM_COL_MAPPING_COMPONENT CCM ON CCM.MAP_ID =CM.MAP_ID " +
            "INNER JOIN COMPONENT C ON CCM.COMP_ID = C.COMP_ID " +
            "WHERE A.ID = {0} and A.AGENCY = {1} and (" +
            "A.Version1 ={2} AND A.Version2 = {3} AND (((A.Version3 is null) and ({4} is null)) or (((A.Version3 is not null) and ({4} is not null)) and ( A.Version3 = {4} )))" +
            " ) and C.ID = {5} ";

        /// <summary>
        ///     The SQL query to build the partial codelist from transcoded codes.
        /// </summary>
        public const string TranscodedCodes =
            " SELECT ITEM.ID  FROM " +
            "ITEM " +
            "INNER JOIN TRANSCODING_RULE_DSD_CODE ON TRANSCODING_RULE_DSD_CODE.CD_ID = ITEM.ITEM_ID " +
            "INNER JOIN TRANSCODING_RULE ON TRANSCODING_RULE.TR_RULE_ID = TRANSCODING_RULE_DSD_CODE.TR_RULE_ID " +
            "INNER JOIN TRANSCODING ON TRANSCODING_RULE.TR_ID = TRANSCODING.TR_ID " +
            "INNER JOIN COMPONENT_MAPPING ON TRANSCODING.MAP_ID = COMPONENT_MAPPING.MAP_ID       " +
            "INNER JOIN COM_COL_MAPPING_COMPONENT ON COMPONENT_MAPPING.MAP_ID = COM_COL_MAPPING_COMPONENT.MAP_ID       " +
            "INNER JOIN MAPPING_SET ON COMPONENT_MAPPING.MAP_SET_ID = MAPPING_SET.MAP_SET_ID       " +
            "INNER JOIN COMPONENT ON COM_COL_MAPPING_COMPONENT.COMP_ID = COMPONENT.COMP_ID       " +
            "INNER JOIN DATAFLOW ON MAPPING_SET.MAP_SET_ID = DATAFLOW.MAP_SET_ID       " +
            "INNER JOIN ARTEFACT A ON DATAFLOW.DF_ID = A.ART_ID " +
            "where ( A.ID = {0} ) AND ( A.AGENCY = {1} ) AND ( " +
            "A.Version1 ={2} AND A.Version2 = {3} AND (((A.Version3 is null) and ({4} is null)) or (((A.Version3 is not null) and ({4} is not null)) and ( A.Version3 = {4} )))" +
            " ) AND ( COMPONENT.ID = {5} )";


        /// <summary>
        ///     The SQL query to build the partial codelist from transcoded codes.
        /// </summary>
        public const string TranscodedCodeUncoded =
            " SELECT TRANSCODING_RULE.UNCODED_VALUE as ID FROM " +
            "TRANSCODING_RULE " +
            "INNER JOIN TRANSCODING ON TRANSCODING_RULE.TR_ID = TRANSCODING.TR_ID " +
            "INNER JOIN COMPONENT_MAPPING ON TRANSCODING.MAP_ID = COMPONENT_MAPPING.MAP_ID       " +
            "INNER JOIN COM_COL_MAPPING_COMPONENT ON COMPONENT_MAPPING.MAP_ID = COM_COL_MAPPING_COMPONENT.MAP_ID       " +
            "INNER JOIN MAPPING_SET ON COMPONENT_MAPPING.MAP_SET_ID = MAPPING_SET.MAP_SET_ID       " +
            "INNER JOIN COMPONENT ON COM_COL_MAPPING_COMPONENT.COMP_ID = COMPONENT.COMP_ID       " +
            "INNER JOIN DATAFLOW ON MAPPING_SET.MAP_SET_ID = DATAFLOW.MAP_SET_ID       " +
            "INNER JOIN ARTEFACT A ON DATAFLOW.DF_ID = A.ART_ID " +
            "where ( A.ID = {0} ) AND ( A.AGENCY = {1} ) AND ( " +
            "A.Version1 ={2} AND A.Version2 = {3} AND (((A.Version3 is null) and ({4} is null)) or (((A.Version3 is not null) and ({4} is not null)) and ( A.Version3 = {4} )))" +
            " ) AND ( COMPONENT.ID = {5} )";

        public static string InnerJoinPattern = " INNER JOIN CONTENT_CONSTRAINT CC ON CC.CONT_CONS_ID = P.ART_ID " +
                                         " INNER JOIN CONTENT_CONSTRAINT_ATTACHMENT CA ON CC.CONT_CONS_ID = CA.CONT_CONS_ID " +
                                         " INNER JOIN ARTEFACT A ON CA.ART_ID = A.ART_ID AND A.ARTEFACT_TYPE= '{0}'";

        /// <summary>
        ///     The <see cref="TranscodedCodes" /> <see cref="SqlQueryInfo" />
        /// </summary>
        private static readonly SqlQueryInfo _transcodedSqlQueryInfo = new SqlQueryInfo
        {
            QueryFormat = TranscodedCodes,
            WhereStatus = WhereState.And
        };
        /// <summary>
        ///     The <see cref="TranscodedCodes" /> <see cref="SqlQueryInfo" />
        /// </summary>
        private static readonly SqlQueryInfo _transcodedUncodedSqlQueryInfo = new SqlQueryInfo
        {
            QueryFormat = TranscodedCodeUncoded,
            WhereStatus = WhereState.And
        };

        /// <summary>
        ///     The <see cref="LocalCodes" /> <see cref="SqlQueryInfo" />
        /// </summary>
        private static readonly SqlQueryInfo _localCodeSqlQueryInfo = new SqlQueryInfo
        {
            QueryFormat = LocalCodes,
            WhereStatus = WhereState.And
        };


        /// <summary>
        ///     Gets the table info.
        /// </summary>
        public static TableInfo TableInfo
        {
            get
            {
                return _tableInfo;
            }
        }

        /// <summary>
        ///     Gets the <see cref="TranscodedCodeUncoded" /> <see cref="SqlQueryInfo" />
        /// </summary>
        public static SqlQueryInfo TranscodedSqlQueryInfo
        {
            get
            {
                return _transcodedSqlQueryInfo;
            }
        }


        /// <summary>
        ///     Gets the <see cref="TranscodedCodes" /> <see cref="SqlQueryInfo" />
        /// </summary>
        public static SqlQueryInfo TranscodedUncodedSqlQueryInfo
        {
            get
            {
                return _transcodedUncodedSqlQueryInfo;
            }
        }

        /// <summary>
        ///     Gets the <see cref="LocalCodes" /> <see cref="SqlQueryInfo" />
        /// </summary>
        public static SqlQueryInfo LocalCodeSqlQueryInfo
        {
            get
            {
                return _localCodeSqlQueryInfo;
            }
        }
    }
}