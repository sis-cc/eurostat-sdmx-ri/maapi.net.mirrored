// -----------------------------------------------------------------------
// <copyright file="IMaintainableRegObjectExtension.cs" company="EUROSTAT">
//   Date Created : 2017-01-25
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.MappingStoreRetrieval.Extensions
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;

    /// <summary>
    /// MaintainableRefObjectExtension
    /// </summary>
    public static class MaintainableRefObjectExtension
    {
        /// <summary>
        /// Creates a IComplexStructureReferenceObject instance
        /// </summary>
        /// <param name="maintainableRefObject">The maintainable reference object.</param>
        /// <param name="sdmxStructureEnumType">Type of the SDMX structure enum.</param>
        /// <param name="returnLatest">indicates whether to return the latest version.</param>
        /// <param name="returnStable">indicates whether to return a stable version.</param>
        /// <returns></returns>
        public static IComplexStructureReferenceObject CreateComplexStructureReferenceObject(this IMaintainableRefObject maintainableRefObject, SdmxStructureEnumType sdmxStructureEnumType,bool returnLatest, bool returnStable = false)
        {
            var agencyId = string.IsNullOrWhiteSpace(maintainableRefObject.AgencyId) ? null : new ComplexTextReferenceCore(string.Empty, TextSearch.FromTextSearchEnumType(TextSearchEnumType.Equal), maintainableRefObject.AgencyId);
            var id = string.IsNullOrWhiteSpace(maintainableRefObject.MaintainableId) ? null : new ComplexTextReferenceCore(string.Empty, TextSearch.FromTextSearchEnumType(TextSearchEnumType.Equal), maintainableRefObject.MaintainableId);
            var versionRef = new ComplexVersionReferenceCore(TertiaryBool.ParseBoolean(returnLatest), TertiaryBool.ParseBoolean(returnStable), maintainableRefObject.Version, null, null);
            return new ComplexStructureReferenceCore(agencyId, id, versionRef, SdmxStructureType.GetFromEnum(sdmxStructureEnumType), null, null, null, null);
        }
    }
}