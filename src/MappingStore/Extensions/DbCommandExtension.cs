// -----------------------------------------------------------------------
// <copyright file="DbCommandExtension.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Extensions
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Runtime.CompilerServices;

    using Estat.Sri.MappingStoreRetrieval.Helper;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     Extensions for <see cref="DbCommand" />
    /// </summary>
    public static class DbCommandExtension
    {
        /// <summary>
        ///     The method retrieve a value from the reader and cast it to <see cref="TertiaryBool" /> data type
        /// </summary>
        /// <param name="reader">
        ///     The source for reading the data
        /// </param>
        /// <param name="fieldName">
        ///     The name of the column containing the value
        /// </param>
        /// <returns>
        ///     The extracted value as <see cref="TertiaryBool" />
        /// </returns>
        public static TertiaryBool GetTristate(this IDataReader reader, string fieldName)
        {
            return DataReaderHelperEx.GetTristate(reader, fieldName);
        }
    }
}