// -----------------------------------------------------------------------
// <copyright file="ContentConstraintAttachTypeExtension.cs" company="EUROSTAT">
//   Date Created : 2019-07-10
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sri.MappingStoreRetrieval.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Sri.MappingStoreRetrieval.Extensions
{
    /// <summary>
    /// A set of extension methods for <see cref="ContentConstraintAttachType"/>
    /// </summary>
    public static class ContentConstraintAttachTypeExtension
    {
        public static SdmxStructureType GetSdmxStructureType(this ContentConstraintAttachType attachType)
        {
            SdmxStructureEnumType enumType;
            switch (attachType)
            {
                case ContentConstraintAttachType.None:
                case ContentConstraintAttachType.SimpleDataSource:
                    return null;
                case ContentConstraintAttachType.DataProvider:
                    enumType = SdmxStructureEnumType.DataProvider;
                    break;
                case ContentConstraintAttachType.DataSet:
                    enumType = SdmxStructureEnumType.Dataset;
                    break;
                case ContentConstraintAttachType.MetadataSet:
                    enumType = SdmxStructureEnumType.MetadataSet;
                    break;
                case ContentConstraintAttachType.DataStructure:
                    enumType = SdmxStructureEnumType.Dsd;
                    break;
                case ContentConstraintAttachType.MetadataStructure:
                    enumType = SdmxStructureEnumType.Msd;
                    break;
                case ContentConstraintAttachType.Dataflow:
                    enumType = SdmxStructureEnumType.Dataflow;
                    break;
                case ContentConstraintAttachType.Metadataflow:
                    enumType = SdmxStructureEnumType.MetadataFlow;
                    break;
                case ContentConstraintAttachType.ProvisionAgreement:
                    enumType = SdmxStructureEnumType.ProvisionAgreement;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(attachType), attachType, null);
            }

            return SdmxStructureType.GetFromEnum(enumType);
        }

        /// <summary>
        /// Get a value indicating whether the <paramref name="attachType"/> is for a Maintainable SDMX
        /// </summary>
        /// <param name="attachType"></param>
        /// <returns><c>true</c> if it points to an Maintainable SDMX artefact</returns>
        public static bool IsMaintainable(this ContentConstraintAttachType attachType)
        {
            switch (attachType)
            {
                case ContentConstraintAttachType.DataStructure:
                case ContentConstraintAttachType.MetadataStructure:
                case ContentConstraintAttachType.Dataflow:
                case ContentConstraintAttachType.Metadataflow:
                case ContentConstraintAttachType.ProvisionAgreement:
                     return true;
                default:
                    return false;
            }
        }
    }
}