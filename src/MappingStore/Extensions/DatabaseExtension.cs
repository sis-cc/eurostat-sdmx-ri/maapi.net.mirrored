// -----------------------------------------------------------------------
// <copyright file="DatabaseExtension.cs" company="EUROSTAT">
//   Date Created : 2013-07-29
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Extensions
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;

    /// <summary>
    ///     This class contains various database extension methods.
    /// </summary>
    public static class DatabaseExtensionEx
    {
        /// <summary>
        ///     Convert the specified <paramref name="value" /> to a value suitable for Mapping Store.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public static object ToDbValue(this ISdmxDate value)
        {
            if (value == null)
            {
                return DBNull.Value;
            }

            return value.Date;
        }

        /// <summary>
        ///     Convert the specified <paramref name="value" /> to a value suitable for Mapping Store.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public static int ToDbValue(this TertiaryBool value)
        {
            if (value == null)
            {
                return 0;
            }

            return value.IsTrue.ToDbValue();
        }
    }
}