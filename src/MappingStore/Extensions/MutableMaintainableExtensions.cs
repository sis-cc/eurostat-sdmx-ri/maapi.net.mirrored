// -----------------------------------------------------------------------
// <copyright file="MutableMaintainableExtensions.cs" company="EUROSTAT">
//   Date Created : 2013-09-23
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Constant;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Annotation;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     Various extensions for <see cref="IMaintainableMutableObject" />
    /// </summary>
    public static class MutableMaintainableExtensions
    {
        /// <summary>
        ///     The _default URI
        /// </summary>
        private static readonly Uri _defaultUri = new Uri("http://need/to/changeit");

        private static string crossX21AnnotationText = "This is something that can be queried using only SDMX v2.0 endpoints";

        /// <summary>
        ///     Returns a new stub <see cref="IMaintainableMutableObject" /> object based on the specified
        ///     <paramref name="maintainable" />.
        ///     The new object is a shallow copy.
        /// </summary>
        /// <typeparam name="TInterface">The maintainable type (interface).</typeparam>
        /// <typeparam name="TImplementation">The maintainable type (concrete implementation).</typeparam>
        /// <param name="maintainable">The maintainable.</param>
        /// <returns>
        ///     A new stub <see cref="IMaintainableMutableObject" /> object based on the specified
        ///     <paramref name="maintainable" />; otherwise if <paramref name="maintainable" /> is null it returns null.
        /// </returns>
        public static TInterface CloneAsStub<TInterface, TImplementation>(this TInterface maintainable)
            where TInterface : IMaintainableMutableObject where TImplementation : TInterface, new()
        {
            if (maintainable.IsDefault())
            {
                return default(TInterface);
            }

            var stub = new TImplementation
                           {
                               Id = maintainable.Id, 
                               AgencyId = maintainable.AgencyId, 
                               Version = maintainable.Version, 
                               StructureURL = _defaultUri, 
                               StartDate = maintainable.StartDate, 
                               EndDate = maintainable.EndDate, 
                               FinalStructure = maintainable.FinalStructure, 
                               ServiceURL = maintainable.ServiceURL, 
                               Uri = maintainable.Uri, 
                               ExternalReference = TertiaryBool.ParseBoolean(true), 

                               // TODO following line when we remove when sync to CAPI v1.0
                               Stub = true
                           };

            stub.Annotations.AddAll(maintainable.Annotations);
            stub.Names.AddAll(maintainable.Names);

            return stub;
        }

        /// <summary>
        ///     Converts the specified <paramref name="crossDsd" /> to stub.
        /// </summary>
        /// <param name="crossDsd">The cross DSD.</param>
        public static void ConvertToStub(this ICrossSectionalDataStructureMutableObject crossDsd)
        {
            if (crossDsd == null)
            {
                throw new ArgumentNullException("crossDsd");
            }

            crossDsd.Stub = true;
            crossDsd.ExternalReference = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);
            crossDsd.StructureURL = _defaultUri;

            // remove components
            crossDsd.AttributeList = null;
            crossDsd.DimensionList = new DimensionListMutableCore();
            crossDsd.Dimensions.Clear();
            crossDsd.Groups.Clear();
            crossDsd.MeasureList = null;
            crossDsd.CrossSectionalAttachDataSet.Clear();
            crossDsd.CrossSectionalAttachGroup.Clear();
            crossDsd.CrossSectionalAttachSection.Clear();
            crossDsd.CrossSectionalAttachObservation.Clear();
            crossDsd.CrossSectionalMeasures.Clear();
            crossDsd.MeasureDimensionCodelistMapping.Clear();
            crossDsd.AttributeToMeasureMap.Clear();
        }

        /// <summary>
        ///     Gets the enumerated representation.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <returns>The representation <see cref="IStructureReference" /> of the <paramref name="component" />; otherwise null.</returns>
        public static IStructureReference GetEnumeratedRepresentation(this IComponentMutableObject component)
        {
            if (component == null)
            {
                throw new ArgumentNullException("component");
            }

            return component.Representation != null ? component.Representation.Representation : null;
        }

        /// <summary>
        ///     Gets the enumerated representation.
        /// </summary>
        /// <param name="dimension">The dimension.</param>
        /// <param name="dsd">The DSD.</param>
        /// <returns>The representation <see cref="IStructureReference" /> of the <paramref name="dimension" />; otherwise null.</returns>
        public static IStructureReference GetEnumeratedRepresentation(
            this IDimensionMutableObject dimension, 
            IDataStructureMutableObject dsd)
        {
            if (dimension == null)
            {
                throw new ArgumentNullException("dimension");
            }

            if (dimension.MeasureDimension)
            {
                var crossDsd = dsd as ICrossSectionalDataStructureMutableObject;
                if (crossDsd != null)
                {
                    IStructureReference reference = crossDsd.MeasureDimensionCodelistMapping[dimension.Id];
                    return reference;
                }
            }

            return dimension.GetEnumeratedRepresentation();
        }

        /// <summary>
        ///     Normalizes the SDMXV20 data structure.
        /// </summary>
        /// <param name="crossDsd">The cross DSD.</param>
        public static void NormalizeSdmxv20DataStructure(this ICrossSectionalDataStructureMutableObject crossDsd)
        {
            crossDsd.ConvertToStub();
            var annotation = CustomAnnotationType.SDMXv20Only.ToAnnotation<AnnotationMutableCore>();
            crossDsd.AddAnnotation(annotation);
        }

        /// <summary>
        ///     Normalizes the SDMXV20 data structure.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        public static void NormalizeSdmxv20DataStructure(this IDataStructureMutableObject dataStructure)
        {
            ApplyCodedTimeDimensionNormalization(dataStructure);

            var crossDsd = dataStructure as ICrossSectionalDataStructureMutableObject;
            if (crossDsd != null)
            {
                crossDsd.NormalizeSdmxv20DataStructure();
            }
        }

        public static IDataStructureMutableObject TransformCrossSectional(this IDataStructureMutableObject dataStructure)
        {
            var crossDsd = dataStructure as ICrossSectionalDataStructureMutableObject;
            if (crossDsd != null)
            {
                TransformMeasureDimension(crossDsd);

                AddConceptRolesForCrossSectionalAttachments(crossDsd);

                AddAttributeAttachmentMeasureAnnotations(crossDsd);

                var dataStructureMutableBean = CreateDataStructureMutableBean(crossDsd);

                AddDataStructureAnnotation(dataStructureMutableBean);

                return dataStructureMutableBean;
            }

            return dataStructure;
            
        }

        private static void AddDataStructureAnnotation(DataStructureMutableCore crossDsd)
        {
            //add annotation at data structure
            var annotation = CustomAnnotationType.SDMXv20Only.ToAnnotation<AnnotationMutableCore>();
            crossDsd.AddAnnotation(annotation);
        }

        private static DataStructureMutableCore CreateDataStructureMutableBean(ICrossSectionalDataStructureMutableObject crossDsd)
        {
            var dataStructure = new DataStructureMutableCore();
            dataStructure.Id = crossDsd.Id;
            dataStructure.Version = crossDsd.Version;
            dataStructure.AgencyId = crossDsd.AgencyId;
            foreach (var item in crossDsd.Annotations)
            { 
                dataStructure.AddAnnotation(item);
            }
            foreach(var item in crossDsd.Descriptions)
            {
                dataStructure.AddDescription(item.Locale,item.Value);
            }
            dataStructure.EndDate = crossDsd.EndDate;
            dataStructure.ExternalReference = crossDsd.ExternalReference;
            dataStructure.FinalStructure = crossDsd.FinalStructure;
            dataStructure.StartDate = crossDsd.StartDate;
            foreach(var item in crossDsd.Names)
            {
                dataStructure.AddName(item.Locale,item.Value);
            }
            dataStructure.Stub = crossDsd.Stub;
            dataStructure.ServiceURL = crossDsd.ServiceURL;
            if (crossDsd.AttributeList != null)
            {
                foreach (var item in crossDsd.AttributeList.Attributes)
                {
                    dataStructure.AddAttribute(item);
                }
            }
            dataStructure.StructureURL = crossDsd.StructureURL;
            if (crossDsd.DimensionList != null)
            {
                foreach (var item in crossDsd.DimensionList.Dimensions)
                {
                    dataStructure.AddDimension(item);
                }
            }
            foreach(var item in crossDsd.Groups)
            {
                dataStructure.AddGroup(item);
            }
            dataStructure.MeasureList = crossDsd.MeasureList;
            return dataStructure;
        }

        private static void AddAttributeAttachmentMeasureAnnotations(ICrossSectionalDataStructureMutableObject crossDsd)
        {
            var attributeToMeasureMap = crossDsd.AttributeToMeasureMap;

            foreach (var attributeMeasures in attributeToMeasureMap)
            {

                var attributeBean = crossDsd.GetAttribute(attributeMeasures.Key);
                if (attributeBean != null)
                {
                    foreach (var measure in attributeMeasures.Value)
                    {
                        AddAttachmentMeasureAnnotationToAttribute(attributeBean, measure);
                    }
                }
            }
        }

        private static void AddAttachmentMeasureAnnotationToAttribute(IAttributeMutableObject attributeBean, string measure)
        {
            var annotation = new AnnotationMutableCore();
            annotation.Id = measure;
            annotation.Type = "AttachmentMeasure";
            attributeBean.AddAnnotation(annotation);
        }

        private static void AddConceptRolesForCrossSectionalAttachments(ICrossSectionalDataStructureMutableObject crossDsd)
        {
            AddConceptRolesForEachCrossSectionalAttach(crossDsd.CrossSectionalAttachObservation, "OBSERVATION", crossDsd);

            AddConceptRolesForEachCrossSectionalAttach(crossDsd.CrossSectionalAttachSection, "SECTION", crossDsd);

            AddConceptRolesForEachCrossSectionalAttach(crossDsd.CrossSectionalAttachGroup, "GROUP", crossDsd);

            AddConceptRolesForEachCrossSectionalAttach(crossDsd.CrossSectionalAttachDataSet, "DATA_SET", crossDsd);
        }

        private static void AddConceptRolesForEachCrossSectionalAttach(IList<string> dimensionOrAttribute, String role, ICrossSectionalDataStructureMutableObject crossDsd)
        {
            foreach (var crossSectionalMeasure in dimensionOrAttribute)
            {
                var dimension = crossDsd.GetDimension(crossSectionalMeasure);
                if (dimension != null)
                {
                    dimension.ConceptRole.Add(CreateCrossSectionalAttachConceptRole(role));
                }
                else
                {
                    var attribute = crossDsd.GetAttribute(crossSectionalMeasure);
                    if (attribute != null)
                    {
                        attribute.AddConceptRole(CreateCrossSectionalAttachConceptRole(role));
                    }
                }
            }
        }


        private static void TransformMeasureDimension(ICrossSectionalDataStructureMutableObject crossDsd)
        {

            //get crossectional datastructure's measure dimension and transform it
            var measureDimensionCodelistMapping = crossDsd.MeasureDimensionCodelistMapping;
            //DimensionMutableBean measureDimension = null;
            if (measureDimensionCodelistMapping != null && measureDimensionCodelistMapping.Any())
            {
                //*replace the dimension id
                var measureDimension = crossDsd.GetDimension(measureDimensionCodelistMapping.Keys.First());
                //set this as normal dimension
                measureDimension.MeasureDimension = false;

                
                measureDimension.ConceptRole.Add(CreateCrossSectionalAttachConceptRole("MEASURE_DIMENSION"));

                //use the measure dimension codelist mapping as the representation
                var representationMutableBean = new RepresentationMutableCore();
                //measureDimensionCodelistMapping.values().iterator().next();
                representationMutableBean.Representation = measureDimensionCodelistMapping.Values.First();
                measureDimension.Representation = representationMutableBean;

                //for each Cross Sectional Measure add an annotation
                foreach (var crossSectionalMeasure in crossDsd.CrossSectionalMeasures)
                {

                    var crossSectionalMeasureAnnotation = new AnnotationMutableCore();
                    crossSectionalMeasureAnnotation.Type = "CROSS_SECTIONAL_MEASURE_CODE_CONCEPT";
                    crossSectionalMeasureAnnotation.Id = crossSectionalMeasure.Code;
                    crossSectionalMeasureAnnotation.Title = crossSectionalMeasure.ConceptRef.TargetUrn.ToString();

                    measureDimension.AddAnnotation(crossSectionalMeasureAnnotation);
                }
            }
        }

        private static IStructureReference CreateCrossSectionalAttachConceptRole(string role)
        {
            return new StructureReferenceImpl(
               "ESTAT",
               "CROSS_SECTIONAL_ATTACH", "1.0",
               SdmxStructureEnumType.Concept, role);
        }

        /// <summary>
        ///     Normalizes the SDMXV20 data structure.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        public static void NormalizeSdmxv20DataStructure(this IMaintainableMutableObject dataStructure)
        {
            var dsd = dataStructure as IDataStructureMutableObject;
            if (dsd != null)
            {
                dsd.NormalizeSdmxv20DataStructure();
            }
        }

        public static IMaintainableMutableObject TransformCrossSectional(this IMaintainableMutableObject dataStructure)
        {
            var dsd = dataStructure as IDataStructureMutableObject;
            if (dsd != null)
            {
                return dsd.TransformCrossSectional();
            }

            return dataStructure;
        }

        /// <summary>
        ///     Normalizes the SDMXV20 data structures.
        /// </summary>
        /// <param name="dataStructures">The data structures.</param>
        public static void NormalizeSdmxv20DataStructures(this IEnumerable<IDataStructureMutableObject> dataStructures)
        {
            if (dataStructures == null)
            {
                throw new ArgumentNullException("dataStructures");
            }

            foreach (var crossDsd in dataStructures)
            {
                crossDsd.NormalizeSdmxv20DataStructure();
            }
        }

        public static IEnumerable<IDataStructureMutableObject> TransformCrossSectional(this IEnumerable<IDataStructureMutableObject> dataStructures)
        {
            List<IDataStructureMutableObject> normalizedDataStructures = new List<IDataStructureMutableObject>();
            if (dataStructures == null)
            {
                throw new ArgumentNullException("dataStructures");
            }

            foreach (var crossDsd in dataStructures)
            {
                normalizedDataStructures.Add(crossDsd.TransformCrossSectional());
            }

            return normalizedDataStructures;
        }

        /// <summary>
        /// Try to add the <paramref name="conceptRef"/> to the <paramref name="component"/>.
        /// </summary>
        /// <param name="component">The mutable component object.</param>
        /// <param name="conceptRef">The structure reference to a Concept.</param>
        /// <returns>True if <paramref name="component"/> is either <see cref="IDimensionMutableObject"/> or <see cref="IAttributeMutableObject"/>; otherwise false.</returns>
        public static bool TryAddConceptRole(this IComponentMutableObject component, IStructureReference conceptRef)
        {
            IDimensionMutableObject dimension = component as IDimensionMutableObject;
            if (dimension != null)
            {
                dimension.ConceptRole.Add(conceptRef);
                return true;
            }

            IAttributeMutableObject attribute = component as IAttributeMutableObject;
            if (attribute != null)
            {
                attribute.AddConceptRole(conceptRef);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets a readonly list of concept roles references for the specified component
        /// </summary>
        /// <param name="component">The DSD component</param>
        /// <returns>The Concept Roles.</returns>
        public static IList<IStructureReference> GetConceptRoles(this IComponentMutableObject component)
        {
             IDimensionMutableObject dimension = component as IDimensionMutableObject;
            if (dimension != null)
            {
                return new ReadOnlyCollection<IStructureReference>(dimension.ConceptRole);
            }

            IAttributeMutableObject attribute = component as IAttributeMutableObject;
            if (attribute != null)
            {
                return new ReadOnlyCollection<IStructureReference>(attribute.ConceptRoles);
            }

            return new IStructureReference[0];
        }

        /// <summary>
        /// Gets the full path of the specified <paramref name="item"/>.
        /// </summary>
        /// <param name="childParentItemMap">The child parent item map.</param>
        /// <param name="item">The item.</param>
        /// <returns>The full path.</returns>
        public static IEnumerable<long> GetFullPath(this IDictionary<long, long> childParentItemMap, long item)
        {
            var categoryPath = new List<long> { item };
            while (childParentItemMap.TryGetValue(categoryPath.Last(), out long categoryParent))
            {
                categoryPath.Add(categoryParent);
            }

            categoryPath.Reverse();

            return categoryPath;
        }

        /// <summary>
        ///     Applies the coded time dimension normalization.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        private static void ApplyCodedTimeDimensionNormalization(IDataStructureMutableObject dataStructure)
        {
            var timeDimension = dataStructure.GetDimension(DimensionObject.TimeDimensionFixedId);
            if (timeDimension != null)
            {
                var codelist = timeDimension.GetEnumeratedRepresentation();
                if (codelist != null)
                {
                    timeDimension.Representation.Representation = null;
                    if (timeDimension.Representation.TextFormat == null)
                    {
                        timeDimension.Representation = null;
                    }

                    IAnnotationBuilder<IMaintainableRefObject> codedTimeAnnotationBuilder =
                        new CodedTimeDimensionAnnotationBuilder<AnnotationMutableCore>();
                    var annotation = codedTimeAnnotationBuilder.Build(codelist);
                    timeDimension.AddAnnotation(annotation);
                }
            }
        }
    }
}