// -----------------------------------------------------------------------
// <copyright file="SdmxStructureTypeExtensions.cs" company="EUROSTAT">
//   Date Created : 2013-04-15
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.MappingStoreRetrieval.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.MappingStoreRetrieval.Model;
    using Estat.Sri.Utils.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The SDMX structure type extensions.
    /// </summary>
    public static class SdmxStructureTypeExtensions
    {
        /// <summary>
        ///     Generate version parameters and update the <paramref name="sqlWhereBuilder"/>.
        /// </summary>
        /// <param name="maintainableRefObject">
        ///     The maintainable ref object.
        /// </param>
        /// <param name="sqlWhereBuilder"></param>
           /// <param name="parameterNameBuilder">
        ///     The parameter name builder.
        /// </param>
        public static void GenerateVersionParameters(
            this IMaintainableRefObject maintainableRefObject,
            SqlWhereBuilder sqlWhereBuilder,
            Func<string, string> parameterNameBuilder)
        {
            if (maintainableRefObject == null)
            {
                throw new ArgumentNullException("maintainableRefObject");
            }

            if (maintainableRefObject.HasVersion())
            {
                var version = new VersionRequestCore(maintainableRefObject.Version);

                if (version.Major.HasValue && version.SpecifiesVersion)
                {
                    var parameterName = parameterNameBuilder("1");
                    sqlWhereBuilder.Add("A.VERSION1 = {0}", parameterName, DbType.Int64, version.Major.Value);
                    parameterName = parameterNameBuilder("2");
                    sqlWhereBuilder.Add("A.VERSION2 = {0}", parameterName, DbType.Int64, version.Minor ?? -1);
                    parameterName = parameterNameBuilder("3");
                    sqlWhereBuilder.Add("A.VERSION3 = {0}", parameterName, DbType.Int64, version.Patch ?? -1);

                    if (version.Extension != null)
                    {
                        parameterName = parameterNameBuilder("4");
                        sqlWhereBuilder.Add("A.EXTENSION = {0}", parameterName, DbType.AnsiString, version.Extension);
                    }
                    else
                    {
                        sqlWhereBuilder.Add("A.EXTENSION is null");
                    }
                }
            }
        }

        /// <summary>
        ///     Generate version parameters.
        /// </summary>
        /// <param name="maintainableRefObject">
        ///     The maintainable ref object.
        /// </param>
        /// <param name="database">
        ///     The database.
        /// </param>
        /// <param name="parameters">
        ///     The parameters.
        /// </param>
        /// <param name="versionFieldPrefix">
        ///     The version field prefix.
        /// </param>
        /// <param name="parameterNameBuilder">
        ///     The parameter name builder.
        /// </param>
        /// <returns>
        ///     The WHERE clause that contains the where statement.
        /// </returns>
        public static string GenerateVersionParameters(
            this IMaintainableRefObject maintainableRefObject,
            Database database,
            IList<DbParameter> parameters,
            string versionFieldPrefix,
            Func<string, string> parameterNameBuilder)
        {
            var sql = new StringBuilder();
            if (maintainableRefObject == null)
            {
                throw new ArgumentNullException("maintainableRefObject");
            }

            if (maintainableRefObject.HasVersion())
            {
                var version = maintainableRefObject.SplitVersion();

                for (int i = 0; i < version.Count; i++)
                {
                    var versionNo = (i + 1).ToString(CultureInfo.InvariantCulture);
                    var parameterName = parameterNameBuilder(versionNo);
                    var inParameter = database.CreateInParameter(parameterName, DbType.Int64, version[i].ToDbValue());
                    if (i == 0)
                    {
                        sql.AppendFormat("A.Version1 ={0}", inParameter.ParameterName);
                    }

                    if (i == 1)
                    {
                        sql.AppendFormat(" AND A.Version2 = {0}", inParameter.ParameterName);
                    }

                    if (i == 2)
                    {
                        sql.AppendFormat(" AND (((A.Version3 is null) and ({0} is null)) or (((A.Version3 is not null) and ({0} is not null)) and ( A.Version3 = {0} )))", inParameter.ParameterName);
                    }

                    parameters.Add(inParameter);
                }

                // update this when Mapping Store stored function isEqualVersion changes.
                Debug.Assert(version.Count == 3, "Version particles not 3. Either update this assert or fix the code.");

            }

            return sql.ToString();
        }

        /// <summary>
        ///     Returns the <c>SDMX v2.1</c> artefacts.
        /// </summary>
        /// <param name="maintainables">
        ///     The maintainables.
        /// </param>
        /// <typeparam name="T">
        ///     The type of maintainable
        /// </typeparam>
        /// <returns>
        ///     The <see cref="IEnumerable{T}" />.
        /// </returns>
        public static IEnumerable<T> GetSdxmV21<T>(this IEnumerable<T> maintainables)
            where T : IMaintainableMutableObject
        {
            return maintainables.Where(maintainable => !maintainable.IsSdmxV20Only());
        }

        /// <summary>
        ///     Returns true if the the specified <paramref name="artefact" /> is it's the default value. Null for class based
        ///     objects.
        /// </summary>
        /// <typeparam name="T">The type of the object.</typeparam>
        /// <param name="artefact">The object. to check.</param>
        /// <returns>true if the the specified <paramref name="artefact" /> is it's the default value; otherwise false.</returns>
        public static bool IsDefault<T>(this T artefact)
        {
            return EqualityComparer<T>.Default.Equals(artefact, default(T));
        }

        /////// <summary>
        ///////     Check if <paramref name="structureType" /> is one of <paramref name="structureTypes" />.
        /////// </summary>
        /////// <param name="structureType">
        ///////     The structure type.
        /////// </param>
        /////// <param name="structureTypes">
        ///////     The structure types.
        /////// </param>
        /////// <returns>
        ///////     The <see cref="bool" />.
        /////// </returns>
        ////public static bool IsOneOf<TEnum>(
        ////    this TEnum structureType,
        ////    params TEnum[] structureTypes)
        ////    where TEnum : struct
        ////{
        ////    if (structureTypes != null && typeof(TEnum).IsEnum)
        ////    {
        ////        for (int i = 0; i < structureTypes.Length; i++)
        ////        {
        ////            if (structureType.Equals(structureTypes[i]))
        ////            {
        ////                return true;
        ////            }
        ////        }
        ////    }

        ////    return false;
        ////}

        /// <summary>
        ///     Check if <paramref name="structureType" /> is one of <paramref name="structureTypes" />.
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="structureTypes">
        ///     The structure types.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public static bool IsOneOf(
            this BaseConstantType<SdmxStructureEnumType> structureType,
            params SdmxStructureEnumType[] structureTypes)
        {
            return structureType != null && structureType.EnumType.IsOneOf(structureTypes);
        }

        /// <summary>
        ///     Check if the specified <paramref name="maintainable" /> is <c>SDMX v2.0</c> only.
        /// </summary>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <returns>
        ///     True if the specified <paramref name="maintainable" /> is <c>SDMX v2.0</c> only; otherwise false.
        /// </returns>
        public static bool IsSdmxV20Only(this IMaintainableMutableObject maintainable)
        {
            if (maintainable != null)
            {
                switch (maintainable.StructureType.EnumType)
                {
                    case SdmxStructureEnumType.Dsd:
                        var crossDsd = maintainable as ICrossSectionalDataStructureMutableObject;
                        if (crossDsd != null)
                        {
                            return true;
                        }

                        break;
                }
            }

            return false;
        }

        /// <summary>
        ///     Check if <paramref name="structureType" /> requires authentication
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public static bool NeedsAuth(this SdmxStructureEnumType structureType)
        {
            return structureType.IsOneOf(SdmxStructureEnumType.Dataflow, SdmxStructureEnumType.Categorisation);
        }

        /// <summary>
        ///     Check if <paramref name="structureType" /> requires authentication
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public static bool NeedsAuth(this BaseConstantType<SdmxStructureEnumType> structureType)
        {
            return structureType != null && NeedsAuth(structureType.EnumType);
        }

        /// <summary>
        ///     Returns a value indicating whether categorisations are needed. (For <c>SDMX v2.0</c>)
        /// </summary>
        /// <param name="queries">
        ///     The queries.
        /// </param>
        /// <returns>
        ///     True if categorisations are needed. (For <c>SDMX v2.0</c>); otherwise false.
        /// </returns>
        public static bool NeedsCategorisation(this IList<IStructureReference> queries)
        {
            return
                queries.Any(
                    structureReference => structureReference.MaintainableStructureEnumType.NeedsCategorisation());
        }

        /// <summary>
        ///     Returns a value indicating whether categorisations are needed. (For <c>SDMX v2.0</c>)
        /// </summary>
        /// <param name="structureType">
        ///     The structure Type.
        /// </param>
        /// <returns>
        ///     True if categorisations are needed. (For <c>SDMX v2.0</c>); otherwise false.
        /// </returns>
        public static bool NeedsCategorisation(this SdmxStructureType structureType)
        {
            return structureType.IsOneOf(SdmxStructureEnumType.Dataflow, SdmxStructureEnumType.CategoryScheme);
        }

        /// <summary>
        ///     Split version of the specified <paramref name="maintainableRefObject" />.
        /// </summary>
        /// <param name="maintainableRefObject">
        ///     The maintainable reference object.
        /// </param>
        /// <returns>
        ///     The list of version tokens
        /// </returns>
        public static IList<long?> SplitVersion(this IMaintainableRefObject maintainableRefObject)
        {
            var version = SplitVersion(maintainableRefObject, 3);
            version[0] = version[0] ?? 0;
            version[1] = version[1] ?? 0;
            return version;
        }

        /// <summary>
        ///     Split version of the specified <paramref name="maintainableRefObject" />.
        /// </summary>
        /// <param name="maintainableRefObject">
        ///     The maintainable reference object.
        /// </param>
        /// <param name="size">
        ///     The number of tokens to return.
        /// </param>
        /// <returns>
        ///     The list of version tokens
        /// </returns>
        public static IList<long?> SplitVersion(this IMaintainableRefObject maintainableRefObject, int size)
        {
            var version = new long?[size];
            if (maintainableRefObject == null)
            {
                throw new ArgumentNullException("maintainableRefObject");
            }

            if (maintainableRefObject.HasVersion())
            {
                for (int i = 0; i < version.Length; i++)
                {
                    version[i] = null;
                }

                var versionArray = maintainableRefObject.Version.Split(
                    new[] { '.' },
                    StringSplitOptions.RemoveEmptyEntries);
                int len = Math.Min(versionArray.Length, version.Length);
                for (int i = 0; i < len; i++)
                {
                    int value;
                    if (int.TryParse(versionArray[i], out value))
                    {
                        version[i] = value;
                    }
                }
            }
            else
            {
                for (int i = 0; i < version.Length; i++)
                {
                    version[i] = null;
                }
            }

            return version;
        }

        /// <summary>
        /// Determines whether  the specific <paramref name="structureType"/> is item of an item scheme
        /// </summary>
        /// <param name="structureType">Type of the structure.</param>
        /// <returns>
        ///   <c>true</c> if the specified structure type is an item of an item scheme; otherwise, <c>false</c>
        /// </returns>
        public static bool IsItemSchemeItem(this SdmxStructureType structureType)
        {
            if (structureType.IsMaintainable || !structureType.IsIdentifiable)
            {
                return false;
            }

            var itemSchemeMangledName = typeof(IItemSchemeObject<>).Name;
            return structureType.MaintainableInterface.GetInterface(itemSchemeMangledName) != null;
        }

        /// <summary>
        /// Determines whether the specified <paramref name="structureType"/> has the ARTEFACT table as parent.
        /// </summary>
        /// <param name="structureType">Type of the structure.</param>
        /// <returns>
        ///   <c>true</c> if the specified <paramref name="structureType"/> has the ARTEFACT table as parent; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasArtefactTableAsParent(this SdmxStructureType structureType)
        {
            return structureType.IsMaintainable
                    || structureType.IsOneOf(
                        SdmxStructureEnumType.HierarchicalCode,
                        SdmxStructureEnumType.Hierarchy,
                        SdmxStructureEnumType.Level);
        }

        /// <summary>
        /// Determines whether the specified <paramref name="structureType"/> has the ITEM table as parent.
        /// </summary>
        /// <param name="structureType">Type of the structure.</param>
        /// <returns>
        ///   <c>true</c> if the specified <paramref name="structureType"/> has the ITEM table as parent; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="structureType"/> is <c>null</c>.</exception>
        public static bool HasItemTableAsParent(this SdmxStructureType structureType)
        {
            if (structureType == null)
            {
                throw new ArgumentNullException(nameof(structureType));
            }

            return structureType.IsItemSchemeItem()
                || structureType.IsAssignableFrom<IStructureSetObject>()
                || structureType.IsOneOf(SdmxStructureEnumType.ReportStructure, SdmxStructureEnumType.MetadataTarget);
        }

        /// <summary>
        /// Determines whether the specified <paramref name="structureType"/> has the ITEM table as parent.
        /// </summary>
        /// <param name="structureType">Type of the structure.</param>
        /// <returns>
        ///   <c>true</c> if the specified <paramref name="structureType"/> has the ITEM table as parent; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasComponentCommonTableAsParent(this SdmxStructureType structureType)
        {
            return structureType.IsOneOf(
                    SdmxStructureEnumType.Component,
                    SdmxStructureEnumType.Dimension,
                    SdmxStructureEnumType.DataAttribute,
                    SdmxStructureEnumType.CrossSectionalMeasure,
                    SdmxStructureEnumType.PrimaryMeasure,
                    SdmxStructureEnumType.MetadataAttribute,
                    SdmxStructureEnumType.ReportPeriodTarget,
                    SdmxStructureEnumType.ConstraintContentTarget,
                    SdmxStructureEnumType.DimensionDescriptorValuesTarget,
                    SdmxStructureEnumType.IdentifiableObjectTarget,
                    SdmxStructureEnumType.DatasetTarget);
        }

        /// <summary>
        /// Determines whether the specified <paramref name="structureType"/> <see cref="SdmxStructureType.MaintainableInterface"/> is assignable from <typeparamref name="TMaintainable"/>
        /// </summary>
        /// <typeparam name="TMaintainable">The type of the maintainable.</typeparam>
        /// <param name="structureType">Type of the structure.</param>
        /// <returns>
        ///   <c>true</c> if the specified <paramref name="structureType"/> <see cref="SdmxStructureType.MaintainableInterface"/> is assignable from <typeparamref name="TMaintainable"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAssignableFrom<TMaintainable>(this SdmxStructureType structureType) where TMaintainable : IMaintainableObject
        {
            if (structureType?.MaintainableInterface == null)
            {
                return false;
            }

            return structureType.MaintainableInterface.IsAssignableFrom(typeof(TMaintainable));
        }

        /// <summary>
        /// Gets the parent table which contains the ID field. e.g. ITEM or ARTEFACT
        /// </summary>
        /// <param name="structureType">Type of the structure.</param>
        /// <returns>The <see cref="TableInfo"/> for the table with the ID field, if it is not contained in it's own table; otherwise null</returns>
        public static TableInfo GetParentTableWithId(this SdmxStructureType structureType)
        {
            if (!structureType.IsIdentifiable)
            {
                return null;
            }

            if (structureType.HasArtefactTableAsParent())
            {
                return new TableInfo(structureType) { PrimaryKey = "ART_ID", Table = "ARTEFACT" };
            }

            if (structureType.HasItemTableAsParent())
            {
                return new TableInfo(structureType) { PrimaryKey = "ITEM_ID", Table = "ITEM" };
            }

            return null;
        }

        /// <summary>
        /// Gets the parent table which contains the ID field. e.g. ITEM or ARTEFACT or <c>COMPONENT_COMMON</c>
        /// </summary>
        /// <param name="structureType">Type of the structure.</param>
        /// <returns>The <see cref="TableInfo"/> for the parent table, if it is not contained in it's own table; otherwise null</returns>
        public static TableInfo GetParentTable(this SdmxStructureType structureType)
        {
            if (structureType.HasArtefactTableAsParent())
            {
                return new TableInfo(structureType) { PrimaryKey = "ART_ID", Table = "ARTEFACT" };
            }

            if (structureType.HasItemTableAsParent())
            {
                return new TableInfo(structureType) { PrimaryKey = "ITEM_ID", Table = "ITEM" };
            }

            if (structureType.HasComponentCommonTableAsParent())
            {
                return new TableInfo(structureType) { PrimaryKey = "COMP_ID", Table = "COMPONENT_COMMON" };
            }

            return null;
        }

    }
}