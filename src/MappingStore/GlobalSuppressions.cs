// -----------------------------------------------------------------------
// <copyright file="GlobalSuppressions.cs" company="EUROSTAT">
//   Date Created : 2016-03-07
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;

[assembly:
    SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", 
        Target =
            "Estat.Sri.MappingStoreRetrieval.Extensions.SdmxStructureTypeExtensions.#SplitVersion(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject)", 
            Justification = "default")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", 
        Target =
            "Estat.Sri.MappingStoreRetrieval.Extensions.SdmxStructureTypeExtensions.#SplitVersion(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject,System.Int32)", 
            Justification = "default")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", 
        Target = "Estat.Sri.MappingStoreRetrieval.Constants.PeriodCodelist.#VersionParticles", Justification = "default")]
[assembly:
    SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", 
        Target =
            "Estat.Sri.MappingStoreRetrieval.Engine.SubsetCodelistRetrievalEngine.#FillCodes(Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist.ICodelistMutableObject,System.Int64,System.Collections.Generic.ICollection`1<System.String>)", 
            Justification = "default")]
[assembly:
    SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", 
        Target =
            "Estat.Sri.MappingStoreRetrieval.Manager.Database.#CreateCommand(System.Data.CommandType,System.String)", Justification = "default")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Scope = "member", 
        Target = "Estat.Sri.MappingStoreRetrieval.Extensions.MutableMaintainableExtensions.#CloneAsStub`2(!!0)", Justification = "default")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed", Scope = "member", 
        Target =
            "Estat.Sri.MappingStoreRetrieval.Engine.Mapping.IComponentMapping.#GenerateComponentWhere(System.String,System.String)", 
            Justification = "default")]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1501:AvoidExcessiveInheritance", Scope = "type", 
        Target = "Estat.Sri.MappingStoreRetrieval.Engine.SubsetCodelistRetrievalEngine", Justification = "default")]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1501:AvoidExcessiveInheritance", Scope = "type", 
        Target = "Estat.Sri.MappingStoreRetrieval.Engine.PartialCodeListRetrievalEngine", Justification = "default")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Scope = "member", 
        Target = "Estat.Sri.MappingStoreRetrieval.Constants.PeriodCodelist.#.cctor()", Justification = "default")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Scope = "type", Target = "Estat.Sri.MappingStoreRetrieval.Engine.DsdRetrievalEngine")]
