// -----------------------------------------------------------------------
// <copyright file="OracleSettingsBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-04-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Utils.Helper;
using Estat.Sri.Utils.Manager;

namespace Estat.Sri.Plugin.Oracle.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Plugin.Oracle.Constant;
    using Estat.Sri.Plugin.Oracle.Engine;
    using log4net;
    using Mapping.MappingStore.Extension;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The Oracle DB connection settings builder.
    /// </summary>
    public class OracleSettingsBuilder : IConnectionSettingsBuilder
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(OracleSettingsBuilder));
        /// <summary>
        /// The user identifier name
        /// </summary>
        private const string UserIdName = "USER ID";

        /// <summary>
        /// The password name
        /// </summary>
        private const string PasswordName = "PASSWORD";

        /// <summary>
        /// The data source name
        /// </summary>
        private const string DataSourceName = "DATA SOURCE";

        /// <summary>
        /// The database name
        /// </summary>
        private const string DatabaseName = "Database";

        /// <summary>
        /// The server name
        /// </summary>
        private const string ServerName = "Server";

        /// <summary>
        /// The port name
        /// </summary>
        private const string PortName = "Port";

        private const string JavaDatasourceWorkaround = "DATASOURCE";

        /// <summary>
        /// The data source parser
        /// </summary>
        private readonly DataSourceParser _dataSourceParser = new DataSourceParser();

        /// <summary>
        /// Initializes a new instance of the <see cref="OracleSettingsBuilder"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public OracleSettingsBuilder(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        private string Name { get; }

        /// <summary>
        /// Creates the connection entity.
        /// </summary>
        /// <returns>
        /// The connection entity
        /// </returns>
        public IConnectionEntity CreateConnectionEntity()
        {
            return CreateConnectionEntity((ConnectionStringSettings)null);
        }

        /// <summary>
        /// Creates the connection entity from the specified <see cref="T:System.Configuration.ConnectionStringSettings" />
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sri.Mapping.Api.Model.IConnectionEntity" />
        /// </returns>
        public IConnectionEntity CreateConnectionEntity(ConnectionStringSettings settings)
        {
            var providerName = settings?.ProviderName ?? DatabaseType.GetProviderName(Name);
            var factory = DbProviderFactories.GetFactory(providerName);
            var builder = factory.CreateConnectionStringBuilder();
            var connectionEntity = BuildConnectionEntity(builder, settings);

            return connectionEntity;
        }

        /// <summary>
        /// Creates the connection string.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>
        /// The <see cref="T:System.Configuration.ConnectionStringSettings" />
        /// </returns>
        public ConnectionStringSettings CreateConnectionString(IConnectionEntity connection)
        {
            var providerName = DatabaseType.GetProviderName(Name);

            var factory = DbProviderFactories.GetFactory(providerName);
            DbConnectionStringBuilder builder = factory.CreateConnectionStringBuilder();
            if (builder == null)
            {
                throw new InvalidOperationException("Cannot build DbConnectionStringBuilder for provider: " + providerName);
            }

            var connectionParameterEntities = new Dictionary<string, IConnectionParameterEntity>(connection.Settings, StringComparer.OrdinalIgnoreCase);
            IConnectionParameterEntity server;
            if (connectionParameterEntities.TryGetValue(ServerName, out server) && server?.Value != null)
            {
                var serverValue = server.Value.ToString();
                IConnectionParameterEntity port;
                if (connectionParameterEntities.TryGetValue(PortName, out port) && port?.Value != null)
                {
                    int portValue;
                    if (int.TryParse(port.Value.ToString(), NumberStyles.Integer, CultureInfo.InvariantCulture, out portValue) && portValue > 0 && portValue < 65536)
                    {
                        serverValue += ":" + port.Value;
                    }

                    connectionParameterEntities.Remove(PortName);
                }

                IConnectionParameterEntity databaseName;
                if (connectionParameterEntities.TryGetValue(DatabaseName, out databaseName) && databaseName?.Value != null)
                {
                    serverValue += "/" + databaseName.Value;
                    connectionParameterEntities.Remove(DatabaseName);
                }

                builder.Add(DataSourceName, serverValue);
                connectionParameterEntities.Remove(ServerName);
            }

            IConnectionParameterEntity dataSource;
            if (connectionParameterEntities.TryGetValue(JavaDatasourceWorkaround, out dataSource))
            {
                connectionParameterEntities.Remove(JavaDatasourceWorkaround);
                connectionParameterEntities.Add(DataSourceName, dataSource);
            }

            foreach (var connectionParameterEntity in connectionParameterEntities)
            {
                if (!connectionParameterEntity.Value.IsItDefault())
                {
                    builder.Add(connectionParameterEntity.Key, connectionParameterEntity.Value.Value);
                }
            }

            return new ConnectionStringSettings(connection.Name, builder.ConnectionString, providerName);
        }

        /// <summary>
        /// Creates the connection string.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>The <see cref="ConnectionStringSettings"/></returns>
        public ConnectionStringSettings CreateConnectionString(DdbConnectionEntity connection)
        {
            var providerName = DatabaseType.GetProviderName(Name);

            return new ConnectionStringSettings(connection.Name, connection.AdoConnString, providerName);
        }

        /// <summary>
        /// Creates the DDB connection entity.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>The <see cref="DdbConnectionEntity"/></returns>
        public DdbConnectionEntity CreateDdbConnectionEntity(IConnectionEntity connection)
        {
            if (connection == null)
            {
                throw new ArgumentNullException(nameof(connection));
            }

            DdbConnectionEntity connectionEntity = new DdbConnectionEntity();
            connectionEntity.Name = connection.Name;
            connectionEntity.EntityId = connection.EntityId;
            connectionEntity.DbType = connection.DatabaseVendorType;
            connectionEntity.AdoConnString = this.CreateConnectionString(connection).ConnectionString;
            var dbName = ExtractConnectionEntityDbName(connection);
            connectionEntity.DbUser = connection.GetValue<string>(UserIdName);
            connectionEntity.Password = connection.GetValue<string>(PasswordName);
            var server = connection.GetValue<string>(ServerName);
            var port = connection.GetNotNullValueAsString(PortName, 1521);
            connectionEntity.JdbcConnString = JdbcConnectionStringHelper.GenerateJdbcString(connectionEntity.DbType, dbName, server, port);
            return connectionEntity;
        }

        private static string ExtractConnectionEntityDbName(IConnectionEntity connection)
        {
            if (!string.IsNullOrWhiteSpace(connection.DbName))
            {
                return connection.DbName;
            }

            if (!string.IsNullOrWhiteSpace(connection.GetValue<string>(UserIdName)))
            {
                return connection.GetValue<string>(UserIdName);
            }

            if (!string.IsNullOrWhiteSpace(connection.GetValue<string>(DataSourceName)))
            {
                return connection.GetValue<string>(DataSourceName);
            }

            if (!string.IsNullOrWhiteSpace(connection.GetValue<string>(JavaDatasourceWorkaround)))
            {
                return connection.GetValue<string>(JavaDatasourceWorkaround);
            }

            if (!string.IsNullOrWhiteSpace(connection.GetValue<string>(DatabaseName)))
            {
                return connection.GetValue<string>(DatabaseName);
            }

            return null;
        }

        /// <summary>
        /// Creates the connection entity.
        /// </summary>
        /// <param name="ddbConnection">The DDB connection.</param>
        /// <returns>The <see cref="IConnectionEntity"/></returns>
        public IConnectionEntity CreateConnectionEntity(DdbConnectionEntity ddbConnection)
        {
            if (ddbConnection == null)
            {
                throw new ArgumentNullException(nameof(ddbConnection));
            }

            if (string.IsNullOrEmpty(ddbConnection.Name))
            {
                throw new SdmxException("Ddb connection name can not be empty", SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError));
            }

            var providerName = DatabaseType.GetProviderName(ddbConnection.DbType);
            var connectionEntity = this.CreateConnectionEntity(
               new ConnectionStringSettings(ddbConnection.Name, ddbConnection.AdoConnString, providerName));
            connectionEntity.EntityId = ddbConnection.EntityId;
            connectionEntity.Name = ddbConnection.Name;
            connectionEntity.Description = ddbConnection.Description;
            connectionEntity.StoreId = ddbConnection.StoreId;
            return connectionEntity;
        }

        /// <summary>
        /// Builds the connection entity.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The <see cref="IConnectionEntity" />
        /// </returns>
        /// <exception cref="System.ArgumentNullException">builder is null</exception>
        private IConnectionEntity BuildConnectionEntity(DbConnectionStringBuilder builder, ConnectionStringSettings settings)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            IConnectionEntity connectionEntity = new ConnectionEntity();
            connectionEntity.DatabaseVendorType = Name;
            var existingSettings = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            if (settings != null)
            {
                connectionEntity.Name = settings.Name;
                try
                {
                    builder.ConnectionString = settings.ConnectionString;
                }
                catch (Exception e)
                {
                    _logger.Error($"Trying to set connection string {settings.ConnectionString} failed", e);
                }
                existingSettings.UnionWith(builder.Cast<KeyValuePair<string, object>>().Select(pair => pair.Key));
            }
            // TODO Remove SubTypes

            //connectionEntity.SubTypeList.Add(OracleConnectionType.EasyConnect.ToString());
            //connectionEntity.SubTypeList.Add(OracleConnectionType.ConnectionDescription.ToString());
            //connectionEntity.SubTypeList.Add(OracleConnectionType.TnsAlias.ToString());

            var originalKeys = builder.Keys;
            if (originalKeys == null)
            {
                builder[ServerName] = string.Empty;
                builder[DataSourceName] = string.Empty;
                builder[UserIdName] = string.Empty;
                builder[PasswordName] = string.Empty;
                originalKeys = builder.Keys;
                if (originalKeys == null)
                {
                    return connectionEntity;
                }
            }

            foreach (string originalKey in originalKeys)
            {
                var key = originalKey.ToUpperInvariant();
                var entry = builder[originalKey];
                var value = entry?.ToString().Trim();
                switch (key)
                {
                    case UserIdName:
                        connectionEntity.AddSetting("User ID", ParameterType.String, value, required: true);
                        connectionEntity.DbName = value;
                        break;
                    case PasswordName:
                        connectionEntity.AddSetting("Password", ParameterType.Password, value, required: true);
                        break;
                    case DataSourceName:
                        //  Below is the EZ connection parser, it would split the Data Source value into Server, port and database.
                        // var dataSource = this._dataSourceParser.GetConnectionType(value);
                        //connectionEntity.SubType = dataSource.ConnectionType.ToString();

                        //if (dataSource.ConnectionType == OracleConnectionType.EasyConnect)
                        //{
                        //    connectionEntity.AddSetting(originalKey, ParameterType.String, value, advancedSetting: true);

                        //    var server = connectionEntity.AddSetting(ServerName, ParameterType.String, dataSource.DatabaseServer, required: true);
                        //    server.AllowedSubTypes.Add(OracleConnectionType.EasyConnect.ToString());

                        //    var db = connectionEntity.AddSetting(DatabaseName, ParameterType.String, dataSource.DatabaseName, required: true);
                        //    connectionEntity.DbName = dataSource.DatabaseName;
                        //    db.AllowedSubTypes.Add(OracleConnectionType.EasyConnect.ToString());
                        //    var port = connectionEntity.AddSetting(PortName, ParameterType.Number, dataSource.Port, required: true);
                        //    port.AllowedSubTypes.Add(OracleConnectionType.EasyConnect.ToString());
                        //}
                        //else
                        {
                            var setting = connectionEntity.AddSetting("Data Source", ParameterType.String, value, required: true);
                            //setting.AllowedSubTypes.Add(OracleConnectionType.TnsAlias.ToString());
                            //setting.AllowedSubTypes.Add(OracleConnectionType.ConnectionDescription.ToString());
                        }

                        break;
                    case "CONTEXT CONNECTION":
                        break;
                    default:
                        connectionEntity.AddSetting(originalKey, value, existingSettings, true);

                        break;
                }
            }

            return connectionEntity;
        }
    }
}
