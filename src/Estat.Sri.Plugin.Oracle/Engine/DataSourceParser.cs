﻿// -----------------------------------------------------------------------
// <copyright file="DataSourceParser.cs" company="EUROSTAT">
//   Date Created : 2015-01-30
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.Oracle.Engine
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;

    using Estat.Sri.Plugin.Oracle.Constant;
    using Estat.Sri.Plugin.Oracle.Model;

    /// <summary>
    ///     Class DataSource.
    /// </summary>
    /// <seealso href="http://docs.oracle.com/cd/E51173_01/win.122/e17732/featConnecting.htm#ODPNT166" />
    public class DataSourceParser
    {
        /// <summary>
        ///     The regular expression for matching Oracle's Easy Connection format.
        /// </summary>
        private static readonly Regex _easyConnectionRegex = new Regex("^(?<server>[0-9A-Za-z_\\-\\.]+)(:(?<port>[0-9]+))?/(?<database>[0-9A-Za-z_\\-\\.]+)$", RegexOptions.Compiled | RegexOptions.CultureInvariant);

        /// <summary>
        ///     Gets the type of the connection.
        /// </summary>
        /// <param name="dataSource">The data source.</param>
        /// <returns>The <see cref="OracleConnectionType" />.</returns>
        public DataSourceModel GetConnectionType(string dataSource)
        {
            if (string.IsNullOrWhiteSpace(dataSource))
            {
                return new DataSourceModel() { ConnectionType = OracleConnectionType.EasyConnect };
            }

            var match = _easyConnectionRegex.Match(dataSource);
            if (match.Success)
            {
                var server = match.Groups["server"].Value;
                var database = match.Groups["database"].Value;
                var portMasked = match.Groups["port"].Value;
                int port;
                if (string.IsNullOrWhiteSpace(portMasked) || !int.TryParse(portMasked, NumberStyles.Integer, CultureInfo.InvariantCulture, out port))
                {
                    port = 1521;
                }

                return new DataSourceModel { ConnectionType = OracleConnectionType.EasyConnect, DatabaseServer = server, Port = port, DatabaseName = database };
            }

            var noWhiteSpace = new string(dataSource.ToCharArray().Where(c => !char.IsWhiteSpace(c)).ToArray());

            if (noWhiteSpace.StartsWith("(DESCRIPTION", StringComparison.Ordinal))
            {
                return new DataSourceModel() { ConnectionType = OracleConnectionType.ConnectionDescription, DataSource = dataSource };
            }

            // TODO find what characters are allowed for TNS Alias
            return new DataSourceModel() { ConnectionType = OracleConnectionType.TnsAlias, DataSource = dataSource };
        }
    }
}
