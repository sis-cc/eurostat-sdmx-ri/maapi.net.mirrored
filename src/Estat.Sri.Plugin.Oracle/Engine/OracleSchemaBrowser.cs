// -----------------------------------------------------------------------
// <copyright file="OracleSchemaBrowser.cs" company="EUROSTAT">
//   Date Created : 2017-04-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.Oracle.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Common;
    using System.Linq;

    using Dapper;

    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Extension;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// Oracle Schema browser
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Engine.IDatabaseSchemaBrowser" />
    public class OracleSchemaBrowser : IDatabaseSchemaBrowser
    {
        /// <summary>
        /// Gets the database objects. E.g. Tables or Views
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The list of <see cref="T:Estat.Sri.Mapping.Api.Model.DatabaseObject" />
        /// </returns>
        /// <exception cref="System.ArgumentNullException">settings is null</exception>
        public IEnumerable<IDatabaseObject> GetDatabaseObjects(ConnectionStringSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            var database = new Database(settings);
            return database.Query<DatabaseObject>($"select table_name as {nameof(IDatabaseObject.Name)}, 'Table' as {nameof(IDatabaseObject.ObjectType)} from user_tables union all select view_name as {nameof(IDatabaseObject.Name)}, 'View' as {nameof(IDatabaseObject.ObjectType)} from user_views");
        }

        /// <summary>
        /// Gets the fields of a database object.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="databaseObject">The database object.</param>
        /// <returns>
        /// The list of <see cref="T:Estat.Sri.Mapping.Api.Model.IFieldInfo" />
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// settings
        /// or
        /// databaseObject
        /// </exception>
        public IEnumerable<IFieldInfo> GetSchema(ConnectionStringSettings settings, IDatabaseObject databaseObject)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            if (databaseObject == null)
            {
                throw new ArgumentNullException(nameof(databaseObject));
            }

            var database = new Database(settings);
            var dynamicParameters = new DynamicParameters();
            dynamicParameters.Add("tableName", databaseObject.Name);
            var parameterName = database.BuildParameterName(dynamicParameters.ParameterNames.First());
            return database.Query<FieldInfo>($"SELECT COLUMN_NAME as {nameof(IFieldInfo.Name)}, DATA_TYPE as {nameof(IFieldInfo.DataType)} FROM user_tab_columns where TABLE_NAME={parameterName}", dynamicParameters);
        }

        /// <summary>
        /// Creates a new connection.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>The <see cref="DbConnection"/></returns>
        public DbConnection CreateConnection(ConnectionStringSettings settings)
        {
            return new Database(settings).CreateConnection();
        }
    }
}