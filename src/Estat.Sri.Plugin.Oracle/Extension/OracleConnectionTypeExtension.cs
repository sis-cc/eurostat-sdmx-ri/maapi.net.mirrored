﻿// -----------------------------------------------------------------------
// <copyright file="OracleConnectionTypeExtension.cs" company="EUROSTAT">
//   Date Created : 2015-01-30
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.Oracle.Extension
{
    using System;

    using Estat.Sri.Plugin.Oracle.Constant;

    /// <summary>
    /// Class OracleConnectionType Extensions.
    /// </summary>
    public static class OracleConnectionTypeExtension
    {
        /// <summary>
        ///     Gets the user friendly name of a <seealso cref="OracleConnectionType" />.
        /// </summary>
        /// <param name="connectionType">Type of the connection.</param>
        /// <returns>The user friendly name of a <seealso cref="OracleConnectionType" />.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">connectionType;Not supported</exception>
        public static string GetName(this OracleConnectionType connectionType)
        {
            switch (connectionType)
            {
                case OracleConnectionType.EasyConnect:
                    return "Easy Connect (EZCONNECT)";
                case OracleConnectionType.TnsAlias:
                    return "TNS Alias";
                case OracleConnectionType.ConnectionDescription:
                    return "Connection Description";
                default:
                    throw new ArgumentOutOfRangeException("connectionType", connectionType, "Not supported");
            }
        }
    }
}