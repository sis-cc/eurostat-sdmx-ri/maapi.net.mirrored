﻿// -----------------------------------------------------------------------
// <copyright file="OracleConnectionEntityExtension.cs" company="EUROSTAT">
//   Date Created : 2015-02-03
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.Oracle.Extension
{
    using System;

    using Estat.Sri.Mapping.Api.Model;

    using log4net;

    /// <summary>
    /// Class OracleConnectionEntityExtension.
    /// </summary>
    public static class OracleConnectionEntityExtension
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log;

        /// <summary>
        /// Initializes static members of the <see cref="OracleConnectionEntityExtension" /> class.
        /// </summary>
        static OracleConnectionEntityExtension()
        {
            _log = LogManager.GetLogger(typeof(OracleConnectionEntityExtension));
        }

        /// <summary>
        /// Gets the data source.
        /// </summary>
        /// <param name="connectionEntity">The connection entity.</param>
        /// <returns>The <see cref="System.String" />.</returns>
        public static string GetDataSource(this IConnectionEntity connectionEntity)
        {
            if (connectionEntity == null)
            {
                throw new ArgumentNullException(nameof(connectionEntity));
            }

            try
            {
                IConnectionParameterEntity value;
                if (connectionEntity.Settings.TryGetValue("Data source", out value))
                {
                    return value?.Value as string;
                }

                return null;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                throw;
            }
        }
    }
}