﻿// -----------------------------------------------------------------------
// <copyright file="OracleDatabaseProviderFactory.cs" company="EUROSTAT">
//   Date Created : 2017-04-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Plugin.Oracle.Factory
{
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Plugin.Oracle.Engine;

    /// <summary>
    /// The oracle database provider factory.
    /// </summary>
    public class OracleDatabaseProviderFactory : BaseDataProviderFactory, IDatabaseProviderFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OracleDatabaseProviderFactory"/> class.
        /// </summary>
        public OracleDatabaseProviderFactory()
            : base(new OracleDatabaseProviderEngine())
        {
        }
    }
}