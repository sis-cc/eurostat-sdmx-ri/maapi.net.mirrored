# Updating an Entity
A new method for updating an Entity type has been added to the implementation of IEntityPersistenceManager.

### Examples

For updating properties with primitive types on the Entity derived classes a simple assignment is sufficient
```C#
///change entity
componentMappingEntity.ConstantValue = "new constant value";
///update entity
entityPersistenceManager.Update(componentMappingEntity);
```

For updating proprieties with LIST<T> type new methods for adding,removing and replacing have been added

```C#
///change entity
componentMappingEntity.ReplaceColumn("1",newDataSetColumn);
componentMappingEntity.RemoveColumn(columnEntityId);
///update entity
entityPersistenceManager.Update(componentMappingEntity);
```

```C#
For updating a series of entities(e.g.Transcoding,Header,TemplateMapping) a Remove/Add approach has been adopted
///change entity
templateMapping.TimeTranscoding.Add(timeTranscodingEntity);
///update entity
manager.Update(templateMapping);
NOTE:once we have updated the entity this way we need retrieve the entity again if we want to do additional updates because the old entity is deleted and the new entity will have a different entityId in the database
```
