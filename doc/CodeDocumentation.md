# General

# Tickets

## SDMXRI-1442

For [SDMXRI-1442](https://citnet.tech.ec.europa.eu/CITnet/jira/browse/SDMXRI-1442) we changed the requirements for inserting a MetadaStructure, previously a check was made to make sure the ConceptScheme referenced in the MetadataStructure were final, now this check is only performed if the MetadataStructure itself is final. Additionally, it was discovered that for some structure types,like MetadataAttribute, an update would generate an exception, and a new bug was created [SDMX-803](https://jira.intrasoft-intl.com/browse/SDMX-803) . The problem was that in the course of the update a map of database ids and structure ids is created for children of a structure, like codes for codelists, in the case of a MSD the metadata attributes can have the same structure id (e.g. 'DataSource') becauses they are under different ReportStructure, and this would throw an exception since the map was created assuming distinct ids. A fix was put in place for MetadataAttribute to create a [mock map](https://citnet.tech.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/commits/3531fc4b7c504183fb15a0d1c759b844582d6593#src/MappingStore/Engine/IdentifiableMapRetrieverEngine.cs) between the database id and the database id as string.

## SDMXRI-1452

Task [SDMXRI-1452](https://citnet.tech.ec.europa.eu/CITnet/jira/browse/SDMXRI-1452) referes to updating a non final DSD. 
For dsd the differences that we need to update can refer to components or groups, there can be 3 situations 
1.New components or groups in the request
	1.1 For components we need to insert them in the COMPONENT table
	1.2 For groups we need to insert them in the DSD_GROUP table
2.Components or groups that are not in the request but are in the database
	2.1 For components we need to check if they are used
		2.1.1 If they are used we need to ignore them
		2.1.2 If they are not used we delete them from COMPONENT
	2.2.For groups we need to delete them but first we need to delete the entries in DIM_GROUP and ATT_GROUP
3.Components that are both in the request and in db that have changes
We need to update the representation, concept identity and concept role if they changed
4.For all the situation above we need to update the data in the four related tables DIM_GROUP, ATT_GROUP, ATTR_DIMS, ATT_MEASURE where the relationships between Dimensions, Attributes and Groups is kept. 
Because the logic to update these tables can become very complicated the best solution was to just delete the data related to the DSD and reinsert based on the DSD in the request. 